Attribute VB_Name = "basLocalizationConst"
Option Explicit

'
'*********************************************************************************
' This file contains the String and Message constant definitions for this project
' based upon the LocalString and LocalMessage tables.
'
' Created On: 1/7/2004 3:49:19 PM
'*********************************************************************************
'
' Strings
'
Public Const kCheckingBatch = 6509                          ' Checking Batch
Public Const kCKJournalDesc = 6371                          ' Check Register
Public Const kJrnlBalanced = 654                        ' Balanced
Public Const kJrnlFatalErrors = 652                     ' Fatal Errors Exist
Public Const kJrnlNotBalanced = 653                     ' Out of Balance
Public Const kJrnlNotPrinted = 650                      ' Not Printed
Public Const kJrnlPosted = 656                          ' Posting Completed
Public Const kJrnlPosting = 655                         ' Posting
Public Const kJrnlPostingFailed = 657                   ' Posting Failed
Public Const kJrnlPrinted = 651                         ' Printed
Public Const kMaintAuditAddD = 5002                         ' Detail
Public Const kMaintAuditAddS = 5001                         ' Summary
Public Const kMCJournalDesc = 6372                          ' Manual Check Register
Public Const kModuleCleanup = 100197                        ' Module Cleanup
Public Const kModulePosted = 6506                           ' Module Posted
Public Const kPAJournalDesc = 6373                          ' Payment Application Register
Public Const kPostingComplete = 6504                        ' Posting Complete
Public Const kPostingModule = 6505                          ' Posting Module
Public Const kPostingToGL = 6554                            ' Posting to General Ledger
Public Const kPrintError = 6507                             ' Print Error
Public Const kPrintingRegister = 6502                       ' Printing Register
Public Const kProcessingGLReg = 6508                        ' Processing GL Register
Public Const kProcessingRegister = 6501                     ' Processing Register
Public Const kRegisterPrinted = 6503                        ' Register Printed
Public Const kValidating = 1612                             ' Validating
Public Const kVClosedPer = 151418                           ' Batch {0}: Posting to a closed period.
Public Const kVNonPer = 151434                              ' Batch {0}: Posting to a non-existent period.
Public Const kVOJournalDesc = 6370                          ' Voucher Register
Public Const kFASImportStatusReport = 141198                ' FA Import Status Report
'
' Messages
'
Public Const kmsgSotaErr = 1002                             ' Module: {0}  Company:  {1}  AppName:  {2}Error < {3} >  occurred at < {4} >  in Procedure < {5} >{6}                                    This application will now close.
Public Const kmsgUnexpectedOperation = 100015               ' Unexpected Operation Value: {0}
Public Const kmsgErrorGeneratingRegister = 100072           ' Errors were encountered generating the {0} Register.
Public Const kmsgCannotPostFatalErrors = 100073             ' {0} Register cannot post.  Invalid data encountered.Check for invalid accounts, closed periods and out-of-balance journals.
Public Const kmsgPostRegister = 100074                      ' Do you want to post the {0} Register?
Public Const kmsgNoBatches = 100076                         ' No {0} batches for posting.
Public Const kmsgBatchNotFoundError = 140033                ' The Accounts Payable batch you are attempting to access cannot be found at this time:Batch Key: {0}
Public Const kmsgFlagNotFound = 140074                      ' Batch Key not found during print flag reset.
Public Const kmsgCIPostXXStatus = 140301                    ' Post {0} Batches processing has completed.    {1} Registers Selected for Printing        {2} Registers Printed    {3} Batches Selected for Posting        {4} Batches Posted        {5} Batches Not Posted due to Errors
Public Const kmsgapInvalidReportPath = 140362               ' Report Path: {0} is not a valid directory on this workstation.
Public Const kmsgProc = 153223                              ' Unexpected Error running Stored Procedure {0}.
Public Const kmsgFatalReportInit = 153302                   ' Unexpected Error Initializing Report Engine.
Public Const kmsgPostErrors = 140072                        ' Errors found during Posting.  Would you like to print the Error Log?
Public Const kmsgCantLoadDDData = 153369                    ' Unable to load data from Data Dictionary.
Public Const kmsgGLYearNotSetup = 140475                    ' The necessary fiscal year for the company is not set up in GL. Click OK to have the system set up the fiscal year and post the batch. This may take a long time. Click Cancel to cancel posting. Use GL Setup to set up the fiscal year, then post the batch.
Public Const kmsgIMFASAPINotAvailable = 166207              ' Warning: The Fixed Asset System is currently unavailable. Assets will not be created.
Public Const kmsgIMFASInitializeFailed = 166208             ' Warning: Unexpected error while initializing interface to the Fixed Asset Accounting System. Assets will not be created.
Public Const kmsgAPGenFileSuccess = 140498                  ' Successfully completed {0} process. Did it generate the proper file format?
Public Const kmsgAPGenFileUnknownErr = 140499               ' Unknown error. {0} extract file cannot be generated.
Public Const kmsgAPGenFileConnectErr = 140500               ' Unable to connect to the database. {0} extract file cannot be generated.
Public Const kmsgAPGenFileDirErr = 140501                   ' Invalid directory. {0} extract file cannot be generated.
Public Const kmsgAPACHBlankAcct = 140502                    ' Bank account number from [{0}] default Remit-To address cannot be blank. ACH extract file cannot be generated.
Public Const kmsgAPACHBlankRouting = 140503                 ' Bank routing number from [{0}] default Remit-To address cannot be blank. ACH extract file cannot be generated.
Public Const kmsgAPACHBlankOptions = 140504                 ' Unable to retrieve records in option screen. You must complete entries in Set Up ACH Options. ACH extract file cannot be generated.
Public Const kmsgAPACHNoChecks = 140505                     ' Batch does not contain any valid checks (ACH can only process standard checks). ACH extract file cannot be generated.
Public Const kmsgAPACHNextFileNo = 140506                   ' Unable to retrieve the next available Canadian ACH/AFT file number. Canadian ACH/AFT extract file cannot be generated.
Public Const kmsgAPACHBlankRtrn = 140507                    ' Canadian ACH/AFT return account number or return routing number cannot be blank. Canadian ACH/AFT extract file cannot be generated.
Public Const kmsgAPACHPrenotePost = 140515                  ' Warning: This batch cannot be posted because one or more vendors have a Prenotification ACH transaction code. Before you can post the batch, you must change the ACH transaction code in Maintain Vendors to an option other than Prenotification.'
Public Const kmsgAPACHMixedPrenoteBatch = 140516            ' The ACH file cannot be generated and the batch cannot be posted because the batch contains both prenotification and non-prenotification transactions. Change the ACH transaction code in Maintain Vendors to the same type for all vendors in this batch.'
Public Const kmsgAPACHAlphaNumRouting = 140517              ' Invalid Routing Number. {0} default Remit-To address routing number of {1} cannot contain characters that are not aplhanumeric. ACH extract file cannot be generated.
