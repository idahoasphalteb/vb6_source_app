VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Begin VB.Form frmRegister 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Register/Post"
   ClientHeight    =   4155
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7755
   HelpContextID   =   10234
   Icon            =   "Apzjxdl1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4155
   ScaleWidth      =   7755
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   741
      Style           =   0
   End
   Begin VB.PictureBox CustomDate 
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Index           =   0
      Left            =   -30000
      ScaleHeight     =   225
      ScaleWidth      =   1155
      TabIndex        =   16
      Top             =   0
      Width           =   1215
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame fraRegister 
      Height          =   3300
      Left            =   45
      TabIndex        =   20
      Top             =   405
      Width           =   7590
      Begin VB.Frame fraACH 
         Caption         =   "ACH File &Extract"
         Height          =   585
         Left            =   3360
         TabIndex        =   28
         Top             =   1680
         Visible         =   0   'False
         Width           =   4095
         Begin VB.CommandButton cmdACHProperties 
            Caption         =   "&Set Directory ..."
            Height          =   300
            Left            =   1200
            TabIndex        =   29
            Top             =   200
            Width           =   1785
         End
      End
      Begin VB.Frame fraPosPay 
         Caption         =   "Positive Pay File &Extract"
         Height          =   585
         Left            =   3360
         TabIndex        =   30
         Top             =   1680
         Visible         =   0   'False
         Width           =   4095
         Begin VB.CheckBox chkPosPay 
            Caption         =   "Extract Check Information to File"
            Height          =   195
            Left            =   100
            TabIndex        =   38
            Top             =   240
            Width           =   2610
         End
         Begin VB.CommandButton cmdPosPayProperties 
            Caption         =   "&Set Directory ..."
            Height          =   315
            Left            =   2730
            TabIndex        =   31
            Top             =   180
            Width           =   1275
         End
      End
      Begin VB.CheckBox chkProjectDetails 
         Alignment       =   1  'Right Justify
         Caption         =   "Include Project Details"
         Height          =   375
         Left            =   90
         TabIndex        =   12
         Top             =   1680
         WhatsThisHelpID =   17777124
         Width           =   1635
      End
      Begin MSComCtl2.UpDown spnCopies 
         Height          =   285
         Left            =   2160
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   675
         WhatsThisHelpID =   10240
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtCopies"
         BuddyDispid     =   196629
         OrigLeft        =   2175
         OrigTop         =   675
         OrigRight       =   2415
         OrigBottom      =   960
         Max             =   10000
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.CommandButton cmdDest 
         Caption         =   "&Properties..."
         Height          =   300
         Left            =   5640
         TabIndex        =   3
         Top             =   195
         WhatsThisHelpID =   15
         Width           =   1785
      End
      Begin VB.ComboBox cboReportDest 
         Height          =   315
         Left            =   1545
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   180
         WhatsThisHelpID =   17
         Width           =   3990
      End
      Begin VB.TextBox txtMessageHeader 
         Height          =   285
         Index           =   0
         Left            =   1500
         TabIndex        =   15
         Top             =   2370
         WhatsThisHelpID =   18
         Width           =   5955
      End
      Begin VB.ComboBox cboFormat 
         Height          =   315
         ItemData        =   "Apzjxdl1.frx":23D2
         Left            =   1515
         List            =   "Apzjxdl1.frx":23D4
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   1290
         WhatsThisHelpID =   10255
         Width           =   1620
      End
      Begin VB.Frame fraOptions 
         Caption         =   "Register &Options"
         Height          =   1050
         Left            =   3345
         TabIndex        =   17
         Top             =   585
         Width           =   4095
         Begin VB.OptionButton optRegister 
            Caption         =   "Post Only"
            Height          =   195
            Index           =   4
            Left            =   2505
            TabIndex        =   27
            Top             =   495
            Value           =   -1  'True
            WhatsThisHelpID =   10253
            Width           =   1350
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Preview Only"
            Height          =   195
            Index           =   3
            Left            =   2505
            TabIndex        =   25
            Top             =   270
            WhatsThisHelpID =   10252
            Width           =   1350
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Print, Do Not Post"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   23
            Top             =   720
            WhatsThisHelpID =   10251
            Width           =   2280
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Print, then Prompt for Post"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   21
            Top             =   495
            WhatsThisHelpID =   10250
            Width           =   2280
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Print, then Post"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   19
            Top             =   270
            WhatsThisHelpID =   10249
            Width           =   2280
         End
      End
      Begin VB.CheckBox chkDefer 
         Alignment       =   1  'Right Justify
         Caption         =   "&Defer Report"
         Height          =   255
         Left            =   90
         TabIndex        =   9
         Top             =   1005
         Visible         =   0   'False
         WhatsThisHelpID =   10248
         Width           =   1635
      End
      Begin VB.TextBox txtCopies 
         Height          =   285
         Left            =   1545
         TabIndex        =   6
         Text            =   "1"
         Top             =   675
         WhatsThisHelpID =   7
         Width           =   615
      End
      Begin VB.Label lblRptMessage 
         AutoSize        =   -1  'True
         Caption         =   "Report &Message"
         Height          =   195
         Left            =   105
         TabIndex        =   13
         Top             =   2415
         Width           =   1170
      End
      Begin VB.Label lblFormat 
         Caption         =   "Register &Format"
         Height          =   210
         Left            =   120
         TabIndex        =   10
         Top             =   1350
         Width           =   1185
      End
      Begin VB.Label lblStatusMsg 
         Caption         =   "Pending"
         Height          =   480
         Left            =   2280
         TabIndex        =   7
         Top             =   2760
         Visible         =   0   'False
         Width           =   5175
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblStatus 
         AutoSize        =   -1  'True
         Caption         =   "Status:"
         Height          =   195
         Left            =   1515
         TabIndex        =   5
         Top             =   2760
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label lblCopies 
         AutoSize        =   -1  'True
         Caption         =   "&Copies to Print"
         Height          =   195
         Left            =   105
         TabIndex        =   4
         Top             =   720
         Width           =   1020
      End
      Begin VB.Label lblReportDest 
         AutoSize        =   -1  'True
         Caption         =   "&Report Destination"
         Height          =   195
         Left            =   105
         TabIndex        =   1
         Top             =   225
         Width           =   1320
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   35
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   22
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   3765
      WhatsThisHelpID =   73
      Width           =   7755
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   14
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: frmRegister
'     Desc: Post Transactions
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
'     Mods: {Date&Inits};
'***********************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Private variables of Form Properties
    Private moClass             As Object
    Private mlRunMode           As Long
    Private mbCancelShutDown    As Boolean
    Private mbEnterAsTab        As Boolean
    Private mlStatus            As Long
    Private mPostError          As Integer
    Private mlRetValue          As Long
    Private mbUnload            As Boolean
    Private miPreview           As Integer
    Private miRepType           As Integer
    Private miFiscPer           As Integer
    Private msFiscYear          As String
    Private mdStartDate         As Date
    Private mdEndDate           As Date
    Private msGLRegister        As String
    Private moContextMenu       As clsContextMenu
    Private moFrmSysSession     As Object
    Private msCRWSelect         As String
    Private msCompanyName       As String 'Used for intercompany registers
           
'Public Form Variables
    Public moSotaObjects        As New Collection
    Public mlBatchKey           As Long
    Public mlBatchType          As Long
    Public miCheckType          As Integer
    Public mdPostDate           As Date
    Private Const kFatalErrors  As Integer = 1
    Public moReport                 As clsReportEngine
    Public sRealTableCollection     As Collection
    Public sWorkTableCollection     As Collection
    Public IntegrateWithFAS         As Integer
    Public moOptions                As clsOptions
    Public moPrinter                As Printer
    
        
    
' // LLJ:  Added for Post XX Batches
    Public miMode               As Integer      ' 1 = single batch, 0 = multiple batches
    Public msBatchID            As String
    
'Private form variables
    Private moAppDAS            As Object
    Private moSysDAS            As Object
    Private msCompanyID         As String
    Private miFullGL            As Integer
    Private mbPAActivated       As Boolean
    Private miSessionId         As Long
    Private miSessionIDItem     As Integer
    Private miGLRgstrType       As Integer
    Private msReportName        As String
    Private msWorkTable         As String
    Private msSPCreateTable     As String
    Private msSPDeleteTable     As String
    Private msBatchType         As String
    Private guLocalizedStrings  As uLocalizedStrings
    Public mlTaskNumber         As Long
    Private mcSPInCreateTable   As New Collection
    Private mcSPOutCreateTable  As New Collection
    Private mcSPInDeleteTable   As New Collection
    Private mcSPOutDeleteTable  As New Collection
    Private mbRegChecked        As Boolean
    Private uRegStat            As RegStatus
    Private bCheckStatus        As Boolean
    Private mbPostError         As Boolean
    Private mbPostWarning         As Boolean
    Private msHomeCurrency      As String
    Private mlLanguageID        As Long
    Private mdBusinessDate      As Date
    Private msSortBy            As String
    Private miSortNum           As Integer
    Private mcReportCondition   As New Collection
    Private msFormulas()        As String
    Private mcSPGLRecapIn       As New Collection
    Private mcSPGLRecapOut      As New Collection
    Private mbProcessing        As Boolean
    Private miUseMultCurr       As Integer
    Private msGLReportPath      As String
    Private msAPReportPath      As String
    Private msReportPath        As String
    Private miSecurityLevel     As Integer
    Private mbCancelWithoutPrinting As Boolean
    Private mbDebitCreditNotEqual As Boolean
    Private mlSessionID         As Long
   
'PA start
    Private Const kVO_DETAIL_RPT_NAME_MC_PA As String = "APZJADL2_IPA.RPT"
    Private Const kVO_DETAIL_RPT_NAME_PA    As String = "APZJADL4_IPA.RPT"
'PA end

'FAS Report
    Private moDDData As clsDDData
    
'Options constants
    Private Const kPrintPost            As Integer = 0
    Private Const kPrintPromptPost      As Integer = 1
    Private Const kPrintNoPost          As Integer = 2
    Private Const kPreviewOnly          As Integer = 3
    Private Const KPostOnly             As Integer = 4
    
    Private Const kTxtFormatDefa        As Integer = 0
    Private Const kTxtFormatName        As Integer = 1
    Private Const kTxtFormatID          As Integer = 2
    Private Const kTxtFormatBoth        As Integer = 3
    
    Private Const kACHType_US               As Integer = 1
    Private Const kACHType_CAN_CPAStd005    As Integer = 2
    Private Const kACHType_CAN_BnkMntrl     As Integer = 3
    
'General Constants
    
    Private Const kERROR_LOG_RPT_NAME       As String = "APZJXDL3.rpt"
    Private Const kGL_REGS_RPT_SUM          As String = "GLZJE005.rpt"
    Private Const kGL_REGS_RPT_SUM_MC       As String = "GLZJE006.rpt"
    Private Const kGL_REGS_RPT_DET          As String = "GLZJE005.rpt"
    Private Const kGL_REGS_RPT_DET_MC       As String = "GLZJE006.rpt"
    
'Posting Procedure
    Private Const kVO_PRE_POSTING           As String = "spapPostGL"
        
'Voucher constants
    Private Const kVO_SUMMARY_RPT_NAME_MC   As String = "APZJADL1.RPT"
    Private Const kVO_DETAIL_RPT_NAME_MC    As String = "APZJADL2.RPT"
    Private Const kVO_SUMMARY_RPT_NAME      As String = "APZJADL3.RPT"
    Private Const kVO_DETAIL_RPT_NAME       As String = "APZJADL4.RPT"
    Private Const kVO_FAS_ASSET_RPT_NAME    As String = "APZJADL5.RPT"

    Private Const kVO_WRK_TABLE             As String = "tapVouchRegWrk"
    Private Const kVO_SP_CREATEWRK          As String = "spapRegVouch"
    Private Const kVO_SP_DELETEWRK          As String = "spapRegVouchDel"
    Private Const kVO_BATCH_TYPE            As Long = 401

'Check constants
    Private Const kCK_SUMMARY_RPT_NAME_MC   As String = "APZJBDL1.RPT"
    Private Const kCK_DETAIL_RPT_NAME_MC    As String = "APZJBDL2.RPT"
    Private Const kCK_SUMMARY_RPT_NAME      As String = "APZJBDL3.RPT"
    Private Const kCK_DETAIL_RPT_NAME       As String = "APZJBDL4.RPT"
    Private Const kCK_WRK_TABLE             As String = "tapPaymentRegWrk"
    Private Const kCK_SP_CREATEWRK          As String = "spapRegPayment"
    Private Const kCK_SP_DELETEWRK          As String = "spapRegPaymentDel"
    Private Const kCK_BATCH_TYPE            As Integer = 411
    
'Payment Appl constants
    Private Const kPA_RPT_NAME              As String = "APZJCDL1.RPT"
    Private Const kPA_SUMMARY_RPT_NAME_MC   As String = "APZJCDL1.RPT"
    Private Const kPA_SUMMARY_RPT_NAME      As String = "APZJCDL2.RPT"
    Private Const kPA_DETAIL_RPT_NAME       As String = "APZJCDL1.RPT"
    Private Const kPA_WRK_TABLE             As String = "tapPmtApplRgstrWrk"
    Private Const kPA_SP_CREATEWRK          As String = "spapRegPmtAppl"
    Private Const kPA_SP_DELETEWRK          As String = "spapRegPmtApplDel"
    Private Const kPA_BATCH_TYPE            As Integer = 404

'AP task number constants
    Private Const ktskVoucher               As Long = 67829765
    Private Const ktskSysCheck              As Long = 67829766
    Private Const ktskManCheck              As Long = 67829767
    Private Const ktskPmtAppl               As Long = 67829768

'FAS Object
    Private RegFASApp As FASLink.Application
    
    'Implement new ACH Process
    'Module level variables for ACH
    Private moCN                As ADODB.Connection

    'Flag to identify extract options
    ' 0 - None
    ' 1 - ACH
    ' 2 - Positive Pay
    Private miExtractType   As Integer
    
    'Positive Pay Flag will enable only if
    '(A) Non-EFT payment method
    '(B) System OR Manual Check Batch
    Private mbDoPosPay      As Boolean
    
    Private mbInitialLoad   As Boolean
    Private mbPrenote       As Boolean
    
    Private cypHelper       As Object 'SGS DEJ 1/12/12 - Orig Customized in 7-3
    
Const VBRIG_MODULE_ID_STRING = "APZJXDL1.FRM"

Private Sub cboFormat_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'PA start
Dim iProjectDetails As Integer

iProjectDetails = frmRegister.chkProjectDetails
'PA end

 Select Case mlTaskNumber
    
        Case ktskVoucher
        
           'Me.Caption = guLocalizedStrings.INJournalDesc
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0
                
                    'PA start
                    chkProjectDetails = False
                    chkProjectDetails.Enabled = False
                    'PA end
                    
                    If miUseMultCurr = 1 Then
                        msReportName = kVO_SUMMARY_RPT_NAME_MC
                    Else
                        msReportName = kVO_SUMMARY_RPT_NAME
                    End If
                Case 1
                'PA start
                    chkProjectDetails.Enabled = True
                    If iProjectDetails = vbChecked Then
                        If miUseMultCurr = 1 Then
                            msReportName = kVO_DETAIL_RPT_NAME_MC_PA
                        Else
                            msReportName = kVO_DETAIL_RPT_NAME_PA
                        End If
                    Else
                'PA end
                        If miUseMultCurr = 1 Then
                            msReportName = kVO_DETAIL_RPT_NAME_MC
                        Else
                            msReportName = kVO_DETAIL_RPT_NAME
                        End If
                'PA start
                    End If
                'PA end
            End Select
                                    
        Case ktskSysCheck, ktskManCheck
           
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0
                    If miUseMultCurr = 1 Then
                        msReportName = kCK_SUMMARY_RPT_NAME_MC
                    Else
                        msReportName = kCK_SUMMARY_RPT_NAME
                    End If
                Case 1
                    If miUseMultCurr = 1 Then
                        msReportName = kCK_DETAIL_RPT_NAME_MC
                    Else
                        msReportName = kCK_DETAIL_RPT_NAME
                    End If
            End Select
        
       Case ktskPmtAppl
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0
                    If miUseMultCurr = 1 Then
                        msReportName = kPA_SUMMARY_RPT_NAME_MC
                    Else
                        msReportName = kPA_SUMMARY_RPT_NAME
                    End If
            End Select
        
       Case Else    'Incorrect Task Number
            If miMode = 1 Then giSotaMsgBox Me, moFrmSysSession, _
                        kmsgUnexpectedOperation, mlTaskNumber
            moClass.lUIActive = kChildObjectInactive
            mlStatus = kFailure
            moClass.moFramework.SavePosSelf
            Me.Hide
            bCheckStatus = False

    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboFormat_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPosPay_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkPosPay, True
    #End If
'+++ End Customizer Code Push +++
    SetPosPayState
End Sub

Private Sub cmdACHProperties_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdACHProperties, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    frmProperties.mbProcessPosPay = False
    frmProperties.Caption = "ACH Properties"
    frmProperties.Show vbModal
End Sub

Private Sub cmdDest_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdDest, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Call DestSetup(Me)
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDest_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub cmdPosPayProperties_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPosPayProperties, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

'SGS DEJ 1/12/12 Added Custom Comment for upgrade from 7-3 to 7-4 (START)
'    frmProperties.mbProcessPosPay = True
'    frmProperties.Caption = "Positive Pay Properties"
'    frmProperties.Show vbModal
    
    Dim shell As Object
    Dim fldr As Object
    
    Set shell = CreateObject("Shell.Application")
    Set fldr = shell.BrowseForFolder(Me.hwnd, "Select the Export Directory", 1)
    
    If Not (fldr Is Nothing) Then
        frmProperties.sFileExtractDir = fldr.Self.Path
    End If
    
    Set fldr = Nothing
    Set shell = Nothing
'SGS DEJ 1/12/12 Added Custom Comment for upgrade from 7-3 to 7-4 (STOP)
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

  Dim sSQL As String
  Dim rs   As DASRecordSet
  Dim rsResult As Object
  Dim FASCompany As String
  Dim FASDatabase As String
  Dim lRetVal As Long
  
  Set sbrMain.Framework = moClass.moFramework
  sbrMain.BrowseVisible = False


    With moClass
        Set moAppDAS = .moAppDB
        Set moSysDAS = .moAppDB
        Set moFrmSysSession = .moSysSession
    End With
    
    SetupToolbar
    
    With moFrmSysSession
        msCompanyID = .CompanyID
        miFullGL = .FullGL
        mbEnterAsTab = .EnterAsTab
        mdBusinessDate = .BusinessDate
        msHomeCurrency = .CurrencyID
        mlLanguageID = .Language
        mbPAActivated = .IsModuleActivated(kModulePA)
    End With
    
    mbPostError = False
    mbPostWarning = False
    
    optRegister(kPrintPromptPost) = True ' Defaults the post
        
    ' // LLJ:  Added for Post XX Batches
    miMode = 1  'Single Batch as default,
                '  multiple batch mode is set later if applicable
              
    BuildLocalizedStrings moSysDAS, moFrmSysSession, guLocalizedStrings
    
    mlTaskNumber = moClass.moFramework.GetTaskID()

    SetVariables mlTaskNumber
    
'PA start
    If mlTaskNumber = ktskVoucher And mbPAActivated = True Then
        chkProjectDetails.Enabled = True
    Else
        chkProjectDetails.Enabled = False
    End If
    chkProjectDetails.Value = vbUnchecked
'PA end
     
    miSessionId = GetUniqueTableKey(moSysDAS, msWorkTable)
    
    GetModVariables
    GetReportPaths
             
   'populate list of print devices
    BuildPrintDeviceCombo Me, cboReportDest.Name
              
    'Format the combo box to specify report summary or detail
    With cboFormat
        .AddItem gsBuildString(kMaintAuditAddS, moSysDAS, moFrmSysSession)
        
        '-- No detail version if payment application
        If mlTaskNumber <> ktskPmtAppl Then
            .AddItem gsBuildString(kMaintAuditAddD, moSysDAS, moFrmSysSession)
        End If
        
        'Default to summary report ( only module not GL )
        .Text = gsBuildString(kMaintAuditAddS, moSysDAS, moFrmSysSession)
    End With
    
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain)

    If miSecurityLevel = sotaTB_DISPLAYONLY Then
        tbrMain.SetState (kStateNone)
        sbrMain.Status = SOTA_SB_LOCKED
    Else
        tbrMain.SetState (kStateStart)
        sbrMain.Status = SOTA_SB_START
    End If
    
    BindContextMenu

    

    sSQL = "IF OBJECT_ID('tempdb..#tapAddFASAssetWrk') IS NOT NULL DROP TABLE #tapAddFASAssetWrk "
    On Error Resume Next
    moClass.moAppDB.ExecuteSQL sSQL

    sSQL = " CREATE TABLE #tapAddFASAssetWrk ( "
    sSQL = sSQL + " SessionID   int NULL"
    sSQL = sSQL + " ,VouchNo        varchar(10) NULL"
    sSQL = sSQL + " ,VendID     varchar(12) NULL"
    sSQL = sSQL + " ,VendName   varchar(40) NULL"
    sSQL = sSQL + " ,AddedToFAS varchar(3) NULL"
    sSQL = sSQL + " ,FASSystemNo    varchar(999) NULL"
    sSQL = sSQL + " ,Template   varchar(999) NULL"
    sSQL = sSQL + " ,AcquisitionValue   decimal(15,3) NOT NULL DEFAULT 0"
    sSQL = sSQL + " ,ServiceDate    datetime NULL"
    sSQL = sSQL + " ,AcquisitionDate    datetime NULL"
    sSQL = sSQL + " ,Descript   varchar(40) NULL"
    sSQL = sSQL + " ,TranNoChngOrd  varchar(14) NULL"
    sSQL = sSQL + " ,FASCompany     varchar(32) NULL)"

    On Error Resume Next
    moClass.moAppDB.ExecuteSQL sSQL

    Set sRealTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data for the report.  Primary table should appear first.
    With sRealTableCollection
        .Add "tapAddFASAssetWrk" 'the "Driving Table" name, such as "tarCustomer"
    End With
    
    Set sWorkTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sWorkTableCollection for each table which
    'will serve as a work table for the report.
    With sWorkTableCollection
        .Add "tapAddFASAssetWrk "
    End With

    lRetVal = lInitializeReport("AP", kVO_SUMMARY_RPT_NAME_MC, "apzjxdl1")  'APZJADL1.RPT
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
        GoTo badexit
    End If
    
    mbInitialLoad = True
    
    'SGS-JMM 1/31/2011
    On Error Resume Next
    Set cypHelper = CreateObject("CypressIntegrationHelper.MAS500Form")
    If Err.Number = 0 Then
        With cypHelper
            .Init Me, moClass, msCompanyID
            .AddToolbarButton 0
        End With
    Else
        Set cypHelper = Nothing
    End If
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

badexit:

'+++ VB/Rig Begin Pop +++
        Exit Sub
        
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bNewBatch() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    SetUIStatus

    'Default for OutofBalance, Hold, InUse Batch differs from Balanced one DR
    If uRegStat.iBalanceStatus = 4 Or uRegStat.iBalanceStatus = 1 Then
        optRegister(kPrintPromptPost) = True
    Else
        optRegister(kPrintNoPost) = True
    End If

        
    If Not bRecordsExist() Then
        moClass.lUIActive = kChildObjectInactive
        moClass.moFramework.SavePosSelf
        bCheckStatus = False
        Me.Hide
        bNewBatch = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
                  
    If iAssignJrnlNo = kFailure Then
        moClass.lUIActive = kChildObjectInactive
        moClass.moFramework.SavePosSelf
        bCheckStatus = False
        Me.Hide
        bNewBatch = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    ' // LLJ:  06/21/96 added next 4 lines
    mbProcessing = False
    tbrMain.ButtonEnabled(kTbProceed) = True
    tbrMain.ButtonEnabled(kTbClose) = True
    If miMode = 1 Then sbrMain.Message = " "
    
    bNewBatch = True
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bNewBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iRet As Integer

    moReport.CleanupWorkTables
    
    If moClass.mlError = 0 Then
        'if a report preview window is active, query user to continue closing.
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
    End If
    
    If UnloadMode <> vbFormCode Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
        'Do Nothing
        
    End Select
    
    frmProperties.sFileExtractDir = ""
    frmProperties.bProcessExtract = False
    
    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.Dispose
    Set cypHelper = Nothing
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    Set moClass = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub optRegister_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick optRegister(Index), True
    #End If
'+++ End Customizer Code Push +++

    Select Case Index
    
        Case kPrintPost
        
            If optRegister(kPrintPost) Then
                chkDefer.Enabled = True
            Else
                chkDefer.Enabled = False
            End If
        Case kPrintPromptPost
        
            If optRegister(kPrintPromptPost) Then
                chkDefer.Enabled = True
            Else
                chkDefer.Enabled = False
            End If
        
        Case kPrintNoPost
        
            If optRegister(kPrintNoPost) Then
                chkDefer.Enabled = True
            Else
                chkDefer.Enabled = False
            End If
        
        Case kPreviewOnly
            
            If optRegister(kPreviewOnly) Then
                chkDefer = False
                chkDefer.Enabled = False
            Else
                chkDefer.Enabled = True
            End If
        
        Case KPostOnly
        
            If optRegister(KPostOnly) Then
                chkDefer = False
                chkDefer.Enabled = False
            Else
                chkDefer.Enabled = True
            End If
    
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optRegister_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Function ProcessRegister(lTaskNumber As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal         As Long
    Dim iMsgReturn      As Integer
    Dim lReturnCode     As Long
    
    ReDim msFormulas(0, 0)
    
              
    If lTaskNumber <> mlTaskNumber Then
        SetVariables lTaskNumber
    End If
        
    msReportPath = msAPReportPath
    moReport.ReportFileName = msReportName
    
    Select Case msReportName
            
        'Payment Appl constants
        Case kPA_RPT_NAME              'As String = "APZJCDL1.RPT"
            moReport.ReportTitle1 = "Payment Application Register"
        Case kPA_SUMMARY_RPT_NAME_MC   'As String = "APZJCDL1.RPT"
            moReport.ReportTitle1 = "Payment Application Register"
        Case kPA_SUMMARY_RPT_NAME      'As String = "APZJCDL2.RPT"
            moReport.ReportTitle1 = "Payment Application Register"
        Case kPA_DETAIL_RPT_NAME       'As String = "APZJCDL1.RPT"
            moReport.ReportTitle1 = "Payment Application Register"
        Case kVO_SUMMARY_RPT_NAME_MC   'As String = "APZJADL1.RPT"
            moReport.ReportTitle1 = "Voucher Register"
        Case kVO_DETAIL_RPT_NAME_MC    'As String = "APZJADL2.RPT"
            moReport.ReportTitle1 = "Voucher Register"
            
    End Select
    
    If Not optRegister(KPostOnly) Then
                 
         moClass.lUIActive = kChildObjectActive
         sbrMain.Status = SOTA_SB_BUSY
               
        'create work table to print register from
                      
         SetCreateSPCollections lTaskNumber
         SetReportVariables lTaskNumber
               
         If miMode = 1 Then sbrMain.Message = guLocalizedStrings.ProcessingRegister
         lRetVal = lCreateWorkTable(msSPCreateTable, mcSPInCreateTable, mcSPOutCreateTable)
                                    
         If lRetVal <> 0 Then
             WorkTableError miSessionId, lRetVal, "Create"
             sbrMain.Status = SOTA_SB_NONE
             moClass.lUIActive = kChildObjectInactive
             ProcessRegister = lRetVal
             mbPostError = True
        '+++ VB/Rig Begin Pop +++
        '+++ VB/Rig End +++
             Exit Function
         End If
                                   
        'prepare and queue registers
        'if we decide to allow preview, then set prn variable here ???
               
        If miMode = 1 Then
            sbrMain.Message = guLocalizedStrings.PrintingRegister
        Else
             moClass.UpdateStatus 150
        End If
        
        msSortBy = msSortBy & gsGetValidStr(mlBatchKey)
        lRetVal = PrintRegister(mlBatchKey, iRegisterOption, chkDefer.Value, cboReportDest.Text, _
                                              Val(txtCopies.Text), msSortBy, msFormulas, mcReportCondition, msReportName)
                  
        If miMode <> 1 Then
            If lRetVal <> kSuccess Then
                moClass.UpdateStatus 1175
            Else
                moClass.UpdateStatus 175
            End If
        End If
        ' SetPrinted status and status message conditioned on a return value?????
            
        If miMode = 1 Then
           sbrMain.Message = guLocalizedStrings.RegisterPrinted
        End If
                    
        SetDeleteSPCollections lTaskNumber
                
        lRetVal = lDeleteWorkTable(msSPDeleteTable, mcSPInDeleteTable, mcSPOutDeleteTable)
                                
        If lRetVal <> 0 Then
            WorkTableError miSessionId, lRetVal, "Delete"
            sbrMain.Status = SOTA_SB_NONE
            moClass.lUIActive = kChildObjectInactive
            ProcessRegister = lRetVal
            mbPostError = True
        '+++ VB/Rig Begin Pop +++
        '+++ VB/Rig End +++
            Exit Function
        End If
                
        sbrMain.Status = SOTA_SB_NONE
            
        moClass.lUIActive = kChildObjectInactive
    
    End If
    
'+++ VB/Rig Begin Pop +++
Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    HandleToolbarClick Button
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopies_Change()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCopies, True
    #End If
'+++ End Customizer Code Push +++
    
    On Error Resume Next
    
    If IsNumeric(txtCopies.Text) Then
        moPrinter.Copies = txtCopies.Text
    End If

End Sub

Private Sub txtCopies_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCopies, True
    #End If
'+++ End Customizer Code Push +++
On Error GoTo ExpectedErrorRoutine:

    txtCopies = IIf((Val(txtCopies) > spnCopies.Max) Or (Val(txtCopies) < spnCopies.Min), spnCopies.Min, txtCopies)
    Exit Sub
  
ExpectedErrorRoutine:
  txtCopies = spnCopies.Min
  Err.Clear
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    gUnloadChildForms Me
    
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    Set moSotaObjects = Nothing
    Set mcSPInCreateTable = Nothing
    Set mcSPOutCreateTable = Nothing
    Set mcSPInDeleteTable = Nothing
    Set mcSPOutDeleteTable = Nothing
    Set mcReportCondition = Nothing
    Set mcSPGLRecapIn = Nothing
    Set mcSPGLRecapOut = Nothing
    
    Set moReport = Nothing
    Set moPrinter = Nothing
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moAppDAS Is Nothing Then
        Set moAppDAS = Nothing
    End If

    If Not moSysDAS Is Nothing Then
        Set moSysDAS = Nothing
    End If
    
    If Not moFrmSysSession Is Nothing Then
        Set moFrmSysSession = Nothing
    End If
    
    'Clean up connection object
    If Not moCN Is Nothing Then
      Set moCN = Nothing
    End If
   
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


'This procedure retrieves the status of the batch and it returns the
'Print Status, Error Status, Records in Batch, Balance of  Batch
Public Sub SetUIStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iErrorStatus As Integer
    

        GetRegisterStatus moAppDAS, msCompanyID, mlBatchKey, mlBatchType, uRegStat
        
        mbRegChecked = True
        
        ' // LLJ:  06/21/96 Added next 9 lines
        iErrorStatus = moAppDAS.Lookup("PostError", "tciBatchLog WITH (NOLOCK)", "BatchKey = " & CStr(mlBatchKey))
        Select Case iErrorStatus
            Case 1
                uRegStat.iErrorStatus = 2
            Case 2
                uRegStat.iErrorStatus = 1
            Case Else
                uRegStat.iErrorStatus = 0
        End Select

        '-- SAM 03/17/98
        '-- Removed In Use code, this is now correctly checked on the batch form
        '-- If batch is OnHold(2) or OutOfBalance(3) print register only
        If uRegStat.iBalanceStatus = 3 Or uRegStat.iBalanceStatus = 2 Then
            optRegister(kPrintPost).Enabled = False
            optRegister(kPrintPromptPost).Enabled = False
            optRegister(KPostOnly).Enabled = False
            '+++ VB/Rig Begin Pop +++
            '+++ VB/Rig End +++
            Exit Sub
        Else
            If uRegStat.iBalanceStatus = 4 Or uRegStat.iBalanceStatus = 1 Then
                If uRegStat.iErrorStatus = 1 Or uRegStat.iPrintStatus = 0 Then
                   optRegister(KPostOnly).Enabled = False
                Else
                   optRegister(KPostOnly).Enabled = True
                End If
                'Check Security level and enable the default options
                If miSecurityLevel = sotaTB_DISPLAYONLY Then
                    optRegister(kPrintPost).Enabled = False
                    optRegister(kPrintPromptPost).Enabled = False
                    optRegister(KPostOnly).Enabled = False
                    If optRegister(kPrintPromptPost) Then optRegister(kPrintNoPost) = True
                Else
                    optRegister(kPrintPost).Enabled = True
                    optRegister(kPrintPromptPost).Enabled = True
                End If
            End If
        End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUIStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Sub SetVariables(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'PA start
Dim iProjectDetails As Integer
iProjectDetails = chkProjectDetails.Value
'PA end

    Select Case lTaskNumber
    
        Case ktskVoucher
        
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0
                    If miUseMultCurr = 1 Then
                        msReportName = kVO_SUMMARY_RPT_NAME_MC
                    Else
                        msReportName = kVO_SUMMARY_RPT_NAME
                    End If
                Case 1
'PA start
                    If iProjectDetails = vbChecked Then
                        If miUseMultCurr = 1 Then
                            msReportName = kVO_DETAIL_RPT_NAME_MC_PA
                        Else
                            msReportName = kVO_DETAIL_RPT_NAME_PA
                        End If
                    Else
'PA end
                        If miUseMultCurr = 1 Then
                            msReportName = kVO_DETAIL_RPT_NAME_MC
                        Else
                            msReportName = kVO_DETAIL_RPT_NAME
                        End If
'PA start
                    End If
'PA end
            End Select
            
            msWorkTable = kVO_WRK_TABLE
            msSPCreateTable = kVO_SP_CREATEWRK
            msSPDeleteTable = kVO_SP_DELETEWRK
            mlBatchType = kVO_BATCH_TYPE
              
        Case ktskSysCheck, ktskManCheck

            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0
                    If miUseMultCurr = 1 Then
                        msReportName = kCK_SUMMARY_RPT_NAME_MC
                    Else
                        msReportName = kCK_SUMMARY_RPT_NAME
                    End If
                Case 1
                    If miUseMultCurr = 1 Then
                        msReportName = kCK_DETAIL_RPT_NAME_MC
                    Else
                        msReportName = kCK_DETAIL_RPT_NAME
                    End If
            End Select
                      
            msWorkTable = kCK_WRK_TABLE
            msSPCreateTable = kCK_SP_CREATEWRK
            msSPDeleteTable = kCK_SP_DELETEWRK
            mlBatchType = kCK_BATCH_TYPE
        
       Case ktskPmtAppl
       
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0
                        msReportName = kPA_SUMMARY_RPT_NAME
                Case 1
                        msReportName = kPA_DETAIL_RPT_NAME
            End Select
            
            msReportName = kPA_RPT_NAME
            msWorkTable = kPA_WRK_TABLE
            msSPCreateTable = kPA_SP_CREATEWRK
            msSPDeleteTable = kPA_SP_DELETEWRK
            mlBatchType = kPA_BATCH_TYPE


        Case Else    'Incorrect Task Number
            If miMode = 1 Then giSotaMsgBox Me, moFrmSysSession, _
                            kmsgUnexpectedOperation, mlTaskNumber
            moClass.lUIActive = kChildObjectInactive
            mlStatus = kFailure
            moClass.moFramework.SavePosSelf
            Me.Hide
            bCheckStatus = False
    End Select
        
'+++ VB/Rig Begin Pop +++
Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bRecordsExist() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iMsgReturn As Integer
    
    bRecordsExist = True
    
    'no batch records exist
    '// LLJ: test 07/13/96
    If uRegStat.iRecordsExist <> 1 And miMode = 1 Then
        iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgNoBatches, guLocalizedStrings.JournalDesc)
        bRecordsExist = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRecordsExist", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub WorkTableError(oSessionID As Long, lRetVal As Long, sType As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iMsgReturn As Integer

'Change message after release to accept parameters for when error occurred and what
'the ret value was from the SP
If miMode = 1 Then
    Select Case sType
        Case "Create"
            iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgErrorGeneratingRegister, guLocalizedStrings.JournalDesc)
    
        Case "Delete"
            iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgErrorGeneratingRegister, guLocalizedStrings.JournalDesc)
        
    End Select
End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WorkTableError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    FormHelpPrefix = "CIZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    WhatHelpPrefix = "CIZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
  
Public Property Get Formulas() As String()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Formulas = msFormulas()
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Formulas", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get ReportFileName() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ReportFileName = msReportName
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ReportFileName", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get ReportPath() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ReportPath = msReportPath
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ReportPath", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
  
Public Property Let ReportPath(sPath As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msReportPath = sPath
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ReportPath", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
  
Public Property Get RestrictClause() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    RestrictClause = msCRWSelect
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "RestrictClause", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
  
Public Property Get CompanyID() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    CompanyID = msCompanyID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CompanyID", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

  Sub SetCreateSPCollections(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
  #If ERRORTRAPON Then
  On Error GoTo VBRigErrorRoutine
  #End If
'+++ VB/Rig End +++

    Dim lReturnValue As Long
    Dim iCommitFlag As Integer
    
    
    Dim vItem As Variant
    Dim vItemCount As Variant
    
    vItemCount = 1
    For Each vItem In mcSPInCreateTable
        mcSPInCreateTable.Remove vItemCount
    Next
    
    For Each vItem In mcSPOutCreateTable
        mcSPOutCreateTable.Remove vItemCount
    Next
    
    Select Case cboFormat.ListIndex
          Case -1 To 0 '
               miRepType = 0 'summary
          Case 1
               miRepType = 1 'detail
          End Select
    Select Case lTaskNumber
              
        Case ktskVoucher
            With mcSPInCreateTable
                .Add mlBatchKey
                .Add msCompanyID
                .Add miRepType
            End With
            mcSPOutCreateTable.Add lReturnValue
       
       Case ktskSysCheck, ktskManCheck
            With mcSPInCreateTable
                .Add mlBatchKey
                .Add msCompanyID
                .Add miRepType
            End With
            mcSPOutCreateTable.Add lReturnValue
       
       Case ktskPmtAppl
            With mcSPInCreateTable
                .Add mlBatchKey
                .Add msCompanyID
                .Add miRepType
            End With
            mcSPOutCreateTable.Add lReturnValue

        
        Case Else
          
    End Select
    
    miSessionIDItem = 1

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetCreateSPCollections", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Sub SetDeleteSPCollections(lTaskNumber)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lReturnValue As Long
    Dim iCommitFlag As Integer

    Dim vItem As Variant
    Dim vItemCount As Variant
    
    vItemCount = 1
    For Each vItem In mcSPInDeleteTable
        mcSPInDeleteTable.Remove vItemCount
    Next
    
    For Each vItem In mcSPOutDeleteTable
        mcSPOutDeleteTable.Remove vItemCount
    Next
    
    Select Case lTaskNumber
          
        Case ktskVoucher
            
            'Delete table in and out parameters
            
          mcSPInDeleteTable.Add mlBatchKey
          mcSPOutDeleteTable.Add lReturnValue
        
        Case ktskSysCheck, ktskManCheck
                  
          mcSPInDeleteTable.Add mlBatchKey
          mcSPOutDeleteTable.Add lReturnValue
                 
        Case ktskPmtAppl
            mcSPInDeleteTable.Add mlBatchKey
            mcSPOutDeleteTable.Add lReturnValue
            'Delete table in and out parameters
        
        Case Else
          
    End Select
          


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetDeleteSPCollections", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function iRegisterOption() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case True
        
        Case optRegister(kPrintPost)
            iRegisterOption = kPrintPost
        Case optRegister(kPrintPromptPost)
            iRegisterOption = kPrintPromptPost
        Case optRegister(kPrintNoPost)
            iRegisterOption = kPrintNoPost
        Case optRegister(kPreviewOnly)
            iRegisterOption = kPreviewOnly
        Case optRegister(KPostOnly)
            iRegisterOption = KPostOnly
    
    End Select
        

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iRegisterOption", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get lStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lStatus = mlStatus
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStatus_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lStatus(vNewValue)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mlStatus = vNewValue
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStatus_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get CompanyName() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    CompanyName = msCompanyName
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CompanyName_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get BatchKey() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    BatchKey = mlBatchKey
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BatchKey_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let BatchKey(lNewBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlBatchKey = lNewBatchKey
    
    ' // LLJ:  need to localize this
    Dim vBatchID As Variant
    
    vBatchID = moAppDAS.Lookup("BatchID", "tciBatchLog WITH (NOLOCK)", "SourceCompanyID = " & _
                gsQuoted(msCompanyID) & " AND BatchKey = " & CStr(mlBatchKey))
    
    If Not IsNull(vBatchID) Then
        Me.Caption = "Register/Post [Batch: " & CStr(vBatchID) & "]"
    Else
        Me.Caption = "Register/Post"
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BatchKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Private Sub SetupToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    
    With tbrMain
        .Build sotaTB_REGISTER
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupToolbar", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    On Error GoTo ExpectedErrorRoutine

    Dim lRetVal As Long
    Dim lMsgReturn As Long
    Dim iPostError As Integer
    
    Select Case sKey
    
        Case kTbProceed
        
            tbrMain.ButtonEnabled(kTbProceed) = False
            tbrMain.ButtonEnabled(kTbClose) = False
            fraRegister.Enabled = False
            Me.MousePointer = vbHourglass
            mbProcessing = True
            
            SetVariables mlTaskNumber
            
            moClass.lUIActive = kChildObjectActive
            
            'Show print dialog if Post Batches and first time through. HBS.
            If miMode <> 1 And Not gbSkipPrintDialog Then
                If gbCancelPrint Then
                    gClearSotaErr
                    mbProcessing = False
                    tbrMain.ButtonEnabled(kTbProceed) = True
                    tbrMain.ButtonEnabled(kTbClose) = True
                    Me.MousePointer = vbDefault
                    fraRegister.Enabled = True
                    Exit Sub
                Else
                    gbSkipPrintDialog = True    '   subsequent batches don't show dialog. HBS.
                End If
            End If

            'START PROCESSING
            If miMode = 1 Then
                sbrMain.Message = guLocalizedStrings.CheckingBatch
            Else
                moClass.UpdateStatus 100
            End If
        
            ' THIS IS PRE-POSTING AND VALIDATION ROUTINE
            mbPostError = False
            mbPostWarning = False
            mbCancelWithoutPrinting = False
                            
            ValidatePostDate
            
            If mbCancelWithoutPrinting Then
                    gClearSotaErr
                    mbProcessing = False
                    tbrMain.ButtonEnabled(kTbProceed) = True
                    tbrMain.ButtonEnabled(kTbClose) = True
                    Me.MousePointer = vbDefault
                    fraRegister.Enabled = True
                    sbrMain.Message = "Process Canceled"
                    Exit Sub
            End If
            
            If mlTaskNumber = ktskManCheck Then
                If bVoucherOnTheFly Then
'1
                    DoValidatePosting ktskVoucher
                    ProcessRegister ktskVoucher
                    SetVariables mlTaskNumber
                End If
            End If
        
        
            'Validate data before posting
            DoValidatePosting mlTaskNumber
            
            'Validate the Assets that are to be added in the FAS
            ValidateFasAsset

            'Create records in tglPosting and
            DoPrePost mlTaskNumber
            
            'Check the Specific Foreign Currency against Currency of soon to be posted records
            ValidateGLCurrencies
            
            'Create work table print and purge from work table
            moReport.ReportTitle1 = "Voucher Register"
            moReport.ReportTitle2 = ""
            ProcessRegister mlTaskNumber
            

            'Check the balance of the all transactions
            CheckBalance iPostError
            
            
            If mbPostError Or mbPostWarning Or iPostError > 0 Then
                With moClass.moAppDB
                .SetInParam mlBatchKey
                .SetInParam mlLanguageID
                .SetOutParam lRetVal
                .ExecuteSP ("spapPopulateErrorCmt")
                lRetVal = .GetOutParam(3)
                .ReleaseParams
                End With
                            
                PrintErrorLog
                ClearErrorLog
            End If
            
            UpdateBatchLog mlBatchKey, iPostError
            
            'In case of errors
            If mbPostError Then
                If mbDebitCreditNotEqual Then
                    PrintGLPostingRecap mlTaskNumber
                End If
                lRetVal = SetPrintedFlag(0)
                
                tbrMain.ButtonEnabled(kTbProceed) = True
                tbrMain.ButtonEnabled(kTbClose) = True
                fraRegister.Enabled = True
                Me.MousePointer = vbDefault
                mbProcessing = False
                
                If miMode = 0 Then
                    moClass.UpdateStatus 1100
                End If
            
                'SGS-JMM 1/31/2011
                On Error Resume Next
                If Not (cypHelper Is Nothing) Then
                    If cypHelper.IsCypressPrinter(cboReportDest.Text) Then
                        cypHelper.ShowViewer
                    End If
                End If
                On Error GoTo ExpectedErrorRoutine
                'End SGS-JMM 1/31/2011
                        
            Else  '-- If not an error
                            
                lRetVal = SetPrintedFlag(1)
                             
                'GL Posting Register,
                'Print the records off tglPosting
                Select Case miGLRgstrType
                    
                    Case 2
                        moReport.ReportTitle1 = "Voucher Register"
                        moReport.ReportTitle2 = "General Ledger Posting Register - Summary"
                        PrintGLPostingRecap mlTaskNumber
                    
                    Case 3
                        moReport.ReportTitle1 = "Voucher Register"
                        moReport.ReportTitle2 = "General Ledger Posting Register - Detail"
                        PrintGLPostingRecap mlTaskNumber
                
                End Select
                
                If miMode = 0 Then
                     moClass.UpdateStatus 176
                Else
                    sbrMain.Message = "GL Register Printed"
                End If
 
                'SGS-JMM 1/31/2011
                On Error Resume Next
                If Not (cypHelper Is Nothing) Then
                    If cypHelper.IsCypressPrinter(cboReportDest.Text) Then
                        cypHelper.ShowViewer
                    End If
                End If
                On Error GoTo ExpectedErrorRoutine
                'End SGS-JMM 1/31/2011
 
'3 & 4 & 5
                'Post if applicable
                If bDoPost(mlTaskNumber) Then
                    tbrMain.ButtonEnabled(kTbClose) = True
                    mbProcessing = False
                    If miMode = 1 Then sbrMain.Message = guLocalizedStrings.PostingComplete
                    sbrMain.Status = SOTA_SB_NONE
                    Me.MousePointer = vbDefault
                    fraRegister.Enabled = True
                        
                    If CreateFASAsset > 0 Then
                        PrintFASAsset kTbPreview, Me
                    End If

                Else
                    tbrMain.ButtonEnabled(kTbProceed) = True
                    tbrMain.ButtonEnabled(kTbClose) = True
                End If
            End If
           
            moClass.lUIActive = kChildObjectInactive
                        
            Me.MousePointer = vbDefault
            fraRegister.Enabled = True
            mbProcessing = False
        
        Case kTbClose
        
            If Not moReport.bShutdownEngine Then
                Exit Sub
            End If
            
            moClass.miShutDownRequester = kUnloadSelfShutDown

            Unload Me
                
        Case kTbHelp
            gDisplayFormLevelHelp Me
                
    End Select
        
    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.HandleToolbarClick sKey, 1
    On Error GoTo ExpectedErrorRoutine
    'End SGS-JMM 1/31/2011
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
    mbProcessing = False
    moClass.lUIActive = kChildObjectInactive
    
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    tbrMain.ButtonEnabled(kTbProceed) = True
    tbrMain.ButtonEnabled(kTbClose) = True
    Me.MousePointer = vbDefault
    fraRegister.Enabled = True

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function GetUniqueTableKey(SysDB As Object, TableName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim key As Long
    
    With SysDB
      .SetInParam TableName
      .SetOutParam key
      .ExecuteSP ("spGetNextSurrogateKey")
      key = .GetOutParamLong(2)
      .ReleaseParams
    End With
    
    GetUniqueTableKey = key

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetUniqueTableKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function gvCheckNull(vField, Optional vDataType As Variant, Optional vSetEmpty As Variant) As Variant
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If IsMissing(vDataType) Then
      vDataType = SQL_CHAR
    End If
    
    If IsMissing(vSetEmpty) Then
      vSetEmpty = False
    End If
    
    If IsEmpty(vField) Or Len(vField) = 0 Then
        If vSetEmpty Then
            gvCheckNull = Empty
        Else
            If vDataType = SQL_CHAR Then
                gvCheckNull = ""
            Else
                gvCheckNull = 0
            End If
       End If
    Else
        If IsNull(vField) Then
            If vSetEmpty Then
                gvCheckNull = Empty
            Else
                If vDataType = SQL_CHAR Then
                    gvCheckNull = ""
                Else
                    gvCheckNull = 0
                End If
            End If
        Else
            gvCheckNull = vField
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "gvCheckNull", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function SetReportVariables(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sortnum As Integer
Dim vItem As Variant
Dim lBatchType As Integer
Dim sAcctRefTitle As String


    'Remove all values
    For Each vItem In mcReportCondition
         mcReportCondition.Remove 1
    Next
    
    
    Select Case lTaskNumber
    
        Case ktskVoucher
            
            lBatchType = moClass.moAppDB.Lookup("BatchType", "tciBatchLog WITH (NOLOCK)", "BatchKey = " & mlBatchKey)
            
            If lBatchType = kBatchTypePOVO Then
                ReDim msFormulas(2, 4)
            Else
                ReDim msFormulas(2, 4) 'ReDim msFormulas(2, 3)
            End If
            
            With mcReportCondition
                msSortBy = "{tapVoucherRegWrk.BatchKey}="
                sortnum = 1
            End With
            
            'Set msformulas
            msFormulas(0, 0) = "sortType"
            msFormulas(1, 0) = sortnum
            msFormulas(0, 1) = "invcComments"
            msFormulas(1, 1) = 0
            msFormulas(0, 2) = "LineItemComments"
            msFormulas(1, 2) = 0
                                                          
            If lBatchType = kBatchTypePOVO Then
                msFormulas(0, 3) = "Module"
                msFormulas(1, 3) = """" & "Purchase Order" & """"
                msFormulas(0, 4) = "Title"
                msFormulas(1, 4) = """" & "Receipt of Invoice Register" & """"
            Else
                '-- Added by SAM 11/14/1997 for AP intercompany
                msFormulas(0, 3) = "AllowInterComp"
                msFormulas(1, 3) = gvCheckNull(moClass.moAppDB.Lookup("AllowInterComp", "tapOptions", "CompanyID = " & gsQuoted(msCompanyID)))
                '-- Added by MATT 03/06/1998 Bring Reports up new Design Scheme
                msFormulas(0, 4) = "AcctRefTitleStrNo"
                '03/09/98  Matt S.  Added to Bring up to v3 schema
                sAcctRefTitle = moClass.moAppDB.Lookup("s.LocalText", "tglOptions g, tsmLocalString s", "s.StringNo=g.AcctRefTitleStrNo AND g.CompanyID = " & gsQuoted(msCompanyID))
                msFormulas(1, 4) = """" & sAcctRefTitle & """"
            End If
            
        Case ktskSysCheck
        
            ReDim msFormulas(2, 3)

            With mcReportCondition
                msSortBy = "{tapPaymentRegWrk.BatchKey}="
                'Based upon listindex of sort combo
                'set miSortNum and add report condition
                sortnum = 1
            End With
            
            'Set msformulas
            msFormulas(0, 0) = "sortType"
            msFormulas(1, 0) = sortnum
            msFormulas(0, 1) = "invcComment"
            msFormulas(1, 1) = 0
            msFormulas(0, 2) = "paymentCmnt"
            msFormulas(1, 2) = 0
            msFormulas(0, 3) = "repType"
            msFormulas(1, 3) = 2
            miCheckType = 2
            
            moReport.ReportTitle1 = "System Check Register"
            
        Case ktskManCheck
            
            ReDim msFormulas(2, 3)
            
            With mcReportCondition
                 msSortBy = "{tapPaymentRegWrk.BatchKey}="
            'Based upon listindex of sort combo
            'set miSortNum and add report condition
                sortnum = 1
            End With
            
            'Set msformulas
            msFormulas(0, 0) = "sortType"
            msFormulas(1, 0) = sortnum
            msFormulas(0, 1) = "invcComment"
            msFormulas(1, 1) = 0
            msFormulas(0, 2) = "paymentCmnt"
            msFormulas(1, 2) = 0
            msFormulas(0, 3) = "repType"
            msFormulas(1, 3) = 1
            miCheckType = 1
                               
            moReport.ReportTitle1 = "Manual Check Register"
            
        Case ktskPmtAppl
            ReDim msFormulas(2, 2)
            
            With mcReportCondition
                 msSortBy = "{tapPmtApplRgstrWrk.BatchKey}="
            'Based upon listindex of sort combo
            'set miSortNum and add report condition
                sortnum = 1
            End With
            
             'Set msformulas
            msFormulas(0, 0) = "sortType"
            msFormulas(1, 0) = sortnum
            msFormulas(0, 1) = "invcComment"
            msFormulas(1, 1) = 0
            msFormulas(0, 2) = "LineItemComments"
            msFormulas(1, 2) = 0
        
    End Select
        
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetReportVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub PrintErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim vItem As Variant
    Dim vItemCount As Variant
    Dim ret As Integer
    
    ReDim msFormulas(1, 1)

    'set path and report to AP error report
    msReportName = kERROR_LOG_RPT_NAME
    moReport.ReportPath = msAPReportPath
    moReport.ReportFileName = msReportName
    moReport.ReportTitle2 = "Error Log Listing"
    
    'Remove all values set for Module register print
    vItemCount = 1
    For Each vItem In mcReportCondition
        mcReportCondition.Remove vItemCount
    Next
    
    If miMode = 1 Then sbrMain.Message = guLocalizedStrings.PrintError
    
    msFormulas(0, 0) = "ModuleNo"
    msFormulas(0, 1) = "Parent"
    msFormulas(1, 0) = 4   'AP
    
    Select Case mlTaskNumber
        Case ktskVoucher
            msFormulas(1, 1) = 1
        Case ktskSysCheck
            msFormulas(1, 1) = 2
        Case ktskManCheck
            msFormulas(1, 1) = 3
        Case ktskPmtAppl
            msFormulas(1, 1) = 4
    End Select
    
    msSortBy = ""

    msCRWSelect = sGetWhere(mlBatchKey)
       
    If iRegisterOption = kPreviewOnly Then
        lStartReport kTbPreview, Me, False, "", "", "", "", ""
    ElseIf chkDefer.Value Then  'deferred
        lStartReport kTbDefer, Me, False, "", "", "", "", ""
    Else
        lStartReport kTbPrint, Me, False, "", "", "", "", ""
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function SetPrintedFlag(iFlag As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lRetVal As Long
Dim response As Integer


    'Exit Sub
    With moAppDAS
    
        .SetInParam mlBatchKey
        .SetInParam iFlag
        .SetInParam msCompanyID
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP ("spapSetRegPrintFlg")
     
        Sleep 0&
        
        lRetVal = .GetOutParam(4)
        .ReleaseParams

    End With
    
    
    If lRetVal <> 0 Then
        sbrMain.Status = SOTA_SB_NONE
        Me.MousePointer = vbDefault
        If miMode = 1 Then giSotaMsgBox Me, moFrmSysSession, kmsgFlagNotFound, guLocalizedStrings.JournalDesc
    End If

    SetPrintedFlag = lRetVal

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPrintedFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function iAssignJrnlNo() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetVal As Long
Dim response As Integer

    iAssignJrnlNo = kSuccess
    
    
    With moAppDAS
    
        .SetInParam msCompanyID
        .SetInParam mlBatchKey
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP ("spapMakeBatchJrnl")
        
        Sleep 0&
        
        lRetVal = .GetOutParam(3)
        .ReleaseParams

    End With
    
    
    If lRetVal <> 0 Then
        sbrMain.Status = SOTA_SB_NONE
        Me.MousePointer = vbDefault
        iAssignJrnlNo = kFailure
        If miMode = 1 Then giSotaMsgBox Me, moFrmSysSession, kmsgBatchNotFoundError, guLocalizedStrings.JournalDesc
    End If
    


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iAssignJrnlNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub PrintGLRegisters()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim rs          As DASRecordSet
    Dim rsRpt       As DASRecordSet
    Dim sSQL        As String
    Dim lBatchKey   As Long
    Dim msOrigCompanyID As String
    Dim iTxtFormat As Integer
    
    iTxtFormat = kTxtFormatDefa
    ' Get the Report Text Format from tsmGlobalRptFormatDetail table for applying the selected format
    sSQL = "SELECT RptDet.TextFormat FROM tsmGlobalRptFormatDetail RptDet WITH (NOLOCK) JOIN tsmGlobalRptFormat RptHdr " _
    & " WITH (NOLOCK) ON RptHdr.GlobalRptFormatKey = RptDet.GlobalRptFormatKey WHERE RptHdr.CompanyID IS NULL " _
    & " OR RptHdr.CompanyID =" & gsQuoted(moClass.moSysSession.CompanyID)

    Set rsRpt = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not IsNull(rsRpt.Field("TextFormat")) Then
        iTxtFormat = rsRpt.Field("TextFormat")
    End If
    rsRpt.Close
    Set rsRpt = Nothing
    '-- Get all distinct batches from tciBatchLink and print
    '-- a register for each one. This is necessary for intercompany processing
    sSQL = "SELECT DISTINCT p.BatchKey, b.PostCompanyID "
    sSQL = sSQL & " FROM tglPosting p WITH (NOLOCK), tciBatchLog  b WITH (NOLOCK) "
    sSQL = sSQL & " WHERE (b.BatchKey = " & mlBatchKey
    sSQL = sSQL & " OR b.BatchKey IN (SELECT LinkedBatchKey FROM tciBatchLink WHERE BatchKey = " & mlBatchKey & ")) "
    sSQL = sSQL & " AND b.BatchKey = p.BatchKey "
    sSQL = sSQL & " ORDER BY p.BatchKey "
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    While Not rs.IsEOF
        lBatchKey = rs.Field("BatchKey")
        
        msCompanyName = moClass.moAppDB.Lookup("CompanyName", "tsmCompany", "CompanyID = " & gsQuoted(rs.Field("PostCompanyID")))
        
        '-- Set Company name to print in report header
        Select Case iTxtFormat
        Case kTxtFormatDefa '0 Default
            msCompanyName = moClass.moAppDB.Lookup("CompanyName", "tsmCompany", "CompanyID = " & gsQuoted(rs.Field("PostCompanyID")))
        Case kTxtFormatName '1 Name only
            msCompanyName = moClass.moAppDB.Lookup("CompanyName", "tsmCompany", "CompanyID = " & gsQuoted(rs.Field("PostCompanyID")))
        Case kTxtFormatID  '2 ID only
            msCompanyName = gsQuoted(rs.Field("PostCompanyID"))
        Case kTxtFormatBoth '3 ID - Name
            msCompanyName = rs.Field("PostCompanyID") & " - " & moClass.moAppDB.Lookup("CompanyName", "tsmCompany", "CompanyID = " _
            & gsQuoted(rs.Field("PostCompanyID")))
        End Select
        
        '-- Add the selection criteria, for some reason
        '-- sent through sort by
        msSortBy = "{tglPosting.BatchKey} = " & lBatchKey
    
        moReport.ReportTitle1 = Mid(msFormulas(1, 1), 2, Len(msFormulas(1, 1)) - 2)
        moReport.ReportFileName = msGLRegister
        
        '-- Print the register for this batch
        PrintRegister lBatchKey, iRegisterOption, chkDefer.Value, cboReportDest.Text, _
                                    Val(txtCopies.Text), msSortBy, msFormulas, mcReportCondition, _
                                    msGLRegister
                                    
        rs.MoveNext
    Wend
    
    '-- Clear out the company name so that the company name from the session object is used.
    msCompanyName = ""


    rs.Close: Set rs = Nothing

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintGLRegisters", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function PrintGLPostingRecap(lTaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal     As Long
    Dim vItem       As Variant

    ReDim msFormulas(1, 2)

    If optRegister(KPostOnly) And uRegStat.iPrintStatus = 1 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    PrintGLPostingRecap = 0

    sbrMain.Status = SOTA_SB_BUSY

    If miMode = 1 Then sbrMain.Message = guLocalizedStrings.ProcessingGLRegister

    '-- Set formulas for gl posting recap
    msFormulas(0, 0) = "module"
    msFormulas(0, 1) = "RptParent"
    msFormulas(1, 0) = """" & "Accounts Payable" & """"

    If mlBatchType = kVO_BATCH_TYPE Then
        msFormulas(1, 1) = """" & "Voucher Register" & """"
    ElseIf mlBatchType = kCK_BATCH_TYPE And miCheckType = 1 Then
        msFormulas(1, 1) = """" & "Manual Check" & """"
    ElseIf mlBatchType = kCK_BATCH_TYPE And miCheckType = 2 Then
        msFormulas(1, 1) = """" & "System Payment" & """"
    ElseIf mlBatchType = kPA_BATCH_TYPE Then
        msFormulas(1, 1) = """" & "Payment Applications" & """"
    End If

    msFormulas(0, 2) = "ReportFormat"
    Select Case cboFormat.ListIndex
        Case -1 To 0 '
            'summary
            msFormulas(1, 2) = "0"
        Case 1
            'detail
            msFormulas(1, 2) = "1"
    End Select

    'Remove all values set for Module register print
    For Each vItem In mcReportCondition
        mcReportCondition.Remove 1
    Next

    ReportPath = msGLReportPath
    moReport.ReportPath = msGLReportPath
    
    moReport.ReportTitle1 = Mid(msFormulas(1, 1), 2, Len(msFormulas(1, 1)) - 2)
    moReport.ReportFileName = msGLRegister

    '-- Added on 11/19/1997 by SAM for intercompany processing
    '-- This function will print a GL Register for each distinct
    '-- company batch in the tglPosting table
    PrintGLRegisters

    ReportPath = msAPReportPath
    moReport.ReportPath = msAPReportPath

    sbrMain.Status = SOTA_SB_NONE

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintGLPostingRecap", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub SetGLRecapSPCollections(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lReturnValue As Long
    Dim iCommitFlag As Integer
    
    
    Dim vItem As Variant
    Dim vItemCount As Variant
    
    vItemCount = 1
     
     For Each vItem In mcSPGLRecapIn
        mcSPGLRecapIn.Remove vItemCount
    Next
    
    For Each vItem In mcSPGLRecapOut
        mcSPGLRecapOut.Remove vItemCount
    Next
    
   'Create table in and out parameters
    
    Select Case lTaskNumber
              
        Case ktskVoucher
      
            With mcSPGLRecapIn
                .Add msCompanyID
                .Add mlBatchKey
                .Add ""
                .Add ""
                .Add 4
                .Add miSessionId
            End With
            mcSPGLRecapOut.Add mlRetValue
        
        Case ktskSysCheck, ktskManCheck
            With mcSPGLRecapIn
                .Add msCompanyID
                .Add mlBatchKey
                .Add ""
                .Add ""
                .Add 4
                .Add miSessionId
            End With
            mcSPGLRecapOut.Add mlRetValue
                            
        Case ktskPmtAppl
            With mcSPGLRecapIn
                .Add msCompanyID
                .Add mlBatchKey
                .Add ""
                .Add ""
                .Add 4
                .Add miSessionId
            End With
            mcSPGLRecapOut.Add mlRetValue
        
        Case Else
          
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGLRecapSPCollections", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function CheckBalance(iPostError As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lRetVal As Long
    Dim rs          As DASRecordSet
    Dim sSQL        As String
    Dim lBatchKey   As Long
    
    '-- default to no post errors
    iPostError = 0
    
    '-- Get all distinct batches from tciBatchLink and check
    '-- balance for each one. This is necessary for intercompany processing
    sSQL = "SELECT DISTINCT p.BatchKey, b.PostCompanyID "
    sSQL = sSQL & " FROM tglPosting p WITH (NOLOCK), tciBatchLog  b WITH (NOLOCK) "
    sSQL = sSQL & " WHERE (b.BatchKey = " & mlBatchKey
    sSQL = sSQL & " OR b.BatchKey IN (SELECT LinkedBatchKey FROM tciBatchLink WHERE BatchKey = " & mlBatchKey & ")) "
    sSQL = sSQL & " AND b.BatchKey = p.BatchKey "
    sSQL = sSQL & " ORDER BY p.BatchKey "
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    While Not rs.IsEOF
        
        lBatchKey = rs.Field("BatchKey")
        
        '-- Check Balance of batch
        With moAppDAS
            
            .SetInParam lBatchKey
            .SetInParam mlBatchKey
            .SetOutParam lRetVal
            
            ' run the stored procedure
            .ExecuteSP ("spapCheckBalance")
            
            lRetVal = .GetOutParam(3)
            
            .ReleaseParams
        
        End With
        
        If lRetVal <> 0 Then
            mbPostError = True
            mbDebitCreditNotEqual = True
            If miMode = 1 Then
                sbrMain.Status = SOTA_SB_NONE
            End If
        Else
            mbDebitCreditNotEqual = False
        End If
       
        '-- Check batch for errors
        If gvCheckNull(moClass.moAppDB.Lookup("Count(*)", "tciErrorLog", "SessionID = " & lBatchKey & " AND Severity = 2")) Then
            mbPostError = True
            iPostError = 2
        End If
        
        '-- Check batch for warnings
        If gvCheckNull(moClass.moAppDB.Lookup("Count(*)", "tciErrorLog", "SessionID = " & lBatchKey & " AND Severity = 1")) Then
            If iPostError <> 2 Then iPostError = 1
        End If
        
        rs.MoveNext
    
    Wend

    rs.Close: Set rs = Nothing

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckBalance", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function ClearErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    
        sSQL = "DELETE tciErrorLog WHERE BatchKey = " & Str(mlBatchKey) _
             & " OR BatchKey IN (SELECT LinkedBatchKey FROM tciBatchLink WHERE BatchKey = " & Str(mlBatchKey) & ")"
        moAppDAS.ExecuteSQL (sSQL)
        
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
  
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmRegister"
End Function

Public Sub ValidateGLCurrencies()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRet As Long

    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam msHomeCurrency
        .SetOutParam lRet
        .ExecuteSP ("spAPValGLCurr")
        lRet = .GetOutParam(3)
        .ReleaseParams
    End With
     
    If lRet <> 0 Then
        sbrMain.Status = SOTA_SB_NONE
        mbPostError = True
        moClass.lUIActive = kChildObjectInactive
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
        Exit Sub
    End If
  
  '+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValidateGLCurrencies", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ValidatePostDate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL    As String
    Dim rs      As DASRecordSet
    Dim iMsgReturn As Integer

    sSQL = "SELECT Status FROM tglFiscalPeriod "
    sSQL = sSQL & " WHERE CompanyID = " & gsQuoted(msCompanyID) & " AND " & gsQuoted(gsFormatDateToDB(Str(mdPostDate))) & " BETWEEN StartDate AND EndDate"

    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        msBatchID = sGetBatchID(Me)
        rs.Close: Set rs = Nothing
        iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgGLYearNotSetup)
        
        If iMsgReturn = vbCancel Then
            mbCancelWithoutPrinting = True
        End If
        
        iLogError mlBatchKey, kVNonPer, msBatchID, "", "", "", "", 1, 2
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    If rs.Field("Status") = 2 Then
        msBatchID = sGetBatchID(Me)
        iLogError mlBatchKey, kVClosedPer, msBatchID, "", "", "", "", 1, 2
        mbPostError = True
    End If
    
    rs.Close: Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "setDates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function sGetBatchID(frm As Form) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rs As Object

    sGetBatchID = ""
    
    sSQL = "SELECT BatchID FROM tciBatchLog WITH (NOLOCK) WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        sGetBatchID = rs.Field("BatchID")
    End If
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetBatchID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Function iLogError(lBatchKey As Long, lStringNo As Long, s1 As String, _
    s2 As String, s3 As String, s4 As String, s5 As String, iErrorType As Integer, _
    iSeverity As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL        As String
Dim rs          As Object
Dim lRet        As Long
Dim lEntryNo    As Long


    iLogError = 0
    
    'first get max entry no for this batch
    sSQL = "SELECT MAX(EntryNo) MaxEntryNo FROM tciErrorLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        If Not IsNull(rs.Field("MaxEntryNo")) Then
        lEntryNo = rs.Field("MaxEntryNo") + 1
        Else
            lEntryNo = 1
        End If
    End If
    
    Set rs = Nothing

         
    'Run stored proc to populate commission register work table
      With moClass.moAppDB
          .SetInParam lBatchKey
          .SetInParam lEntryNo
          .SetInParam lStringNo
          .SetInParam s1
          .SetInParam s2
          .SetInParam s3
          .SetInParam s4
          .SetInParam s5
          .SetInParam iErrorType
          .SetInParam iSeverity
          
          .SetOutParam lRet
          .ExecuteSP ("spglCreateErrSuspenseLog")
          lRet = .GetOutParam(11)
          .ReleaseParams
      End With
      
    If lRet <> 0 Then
        iLogError = lRet
    Else           ' populate error comment
        With moClass.moAppDB
            .SetInParam lBatchKey
            .SetInParam mlLanguageID
            .SetOutParam lRet
            
            .ExecuteSP ("spapPopulateErrorCmt")
            lRet = .GetOutParam(3)
            .ReleaseParams
        End With
        iLogError = lRet
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iLogError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set moContextMenu = New clsContextMenu
    
    With moContextMenu
   
        ' **PRESTO ** Set .Hook = WinHook1
        Set .Form = frmRegister
        
        .Init
    End With

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub GetModVariables()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    Dim sSQL As String
    Dim rsResult As Object
   
   ' The tapOptions determines the GL register format
     sSQL = "SELECT GLPostRgstrFormat,UseMultCurr FROM tapOptions WHERE CompanyID = " & gsQuoted(msCompanyID)
    
    Set rsResult = moAppDAS.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rsResult.IsEOF Then
        miGLRgstrType = rsResult.Field("GLPostRgstrFormat")
        miUseMultCurr = rsResult.Field("UseMultCurr")
    End If
    
    If miUseMultCurr = 1 Then
        If miGLRgstrType = 2 Then
            msGLRegister = kGL_REGS_RPT_SUM_MC
        ElseIf miGLRgstrType = 3 Then
            msGLRegister = kGL_REGS_RPT_DET_MC
        End If
    Else
        If miGLRgstrType = 2 Then
            msGLRegister = kGL_REGS_RPT_SUM
        ElseIf miGLRgstrType = 3 Then
            msGLRegister = kGL_REGS_RPT_DET
        End If
    End If
    
    Set rsResult = Nothing

'+++ VB/Rig Begin Pop +++
Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetModVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub GetReportPaths()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRet As Long
    
    msAPReportPath = moFrmSysSession.ModuleReportPath("AP")
    lRet = lValidateReportPath(msAPReportPath)
    msGLReportPath = moFrmSysSession.ModuleReportPath("GL")
    lRet = lValidateReportPath(msGLReportPath)
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetReportPaths", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lValidateReportPath(sPath As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine
    
    Dim iAttr As Integer
    
        lValidateReportPath = kFailure
        
        If Not Len(sPath) > 0 Then
            GoTo ExpectedErrorRoutine
        Else
            If Right(sPath, 1) = "\" Then
                iAttr = GetAttr(Mid(sPath, 1, Len(sPath) - 1)) And vbDirectory
            Else
                iAttr = GetAttr(sPath) And vbDirectory
                sPath = sPath & "\"
            End If
            If iAttr <> vbDirectory Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgapInvalidReportPath, sPath
                GoTo ExpectedErrorRoutine
            End If
        End If
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
        
ExpectedErrorRoutine:

    gClearSotaErr
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateReportPath", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bVoucherOnTheFly() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rsResult As Object
   
   
   ' Check the tapPendVoucher table to see if any rows exist with this batch key
     sSQL = "SELECT VoucherKey FROM tapPendVoucher WHERE CompanyID = " & _
              gsQuoted(msCompanyID) & " AND BatchKey = " & CStr(mlBatchKey)
    
    Set rsResult = moAppDAS.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rsResult.IsEOF Then
        bVoucherOnTheFly = True
    Else
        bVoucherOnTheFly = False
    End If
    
    Set rsResult = Nothing

  

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bVoucherOnTheFly", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If miMode = 1 Then
        If mbInitialLoad Then
            fraPosPay.Visible = False
            fraACH.Visible = False
            miExtractType = kExtractNone
  
            If mlTaskNumber = ktskSysCheck Then
                If bValidateEFT Then
                    miExtractType = kExtractACH
                    fraACH.Visible = True
                    gEnableControls cmdACHProperties
                    IsFileDirSet
                Else
                    miExtractType = kExtractPosPay
                    chkPosPay.Value = vbUnchecked
                    fraPosPay.Visible = True
                    SetPosPayState
                    
                    If PosPayAllowed Then chkPosPay.Value = vbChecked 'SGS-JMM 4/28/2011
                End If
            
            ElseIf mlTaskNumber = ktskManCheck Then
                miExtractType = kExtractPosPay
                chkPosPay.Value = vbUnchecked
                fraPosPay.Visible = True
                SetPosPayState
                
                If PosPayAllowed Then chkPosPay.Value = vbChecked 'SGS-JMM 4/28/2011
            End If
    
           mbInitialLoad = False
        End If
    End If
    
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub cmdACHProperties_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdACHProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdACHProperties_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdACHProperties_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdACHProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdACHProperties_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPosPayProperties_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPosPayProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPosPayProperties_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPosPayProperties_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPosPayProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPosPayProperties_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDest_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDest_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDest_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDest_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtMessageHeader_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMessageHeader(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopies_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCopies, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopies_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopies_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCopies, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopies_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkPosPay_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPosPay, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPosPay_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPosPay_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPosPay, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPosPay_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkProjectDetails_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkProjectDetails, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkProjectDetails_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkProjectDetails_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkProjectDetails, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkProjectDetails_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDefer_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkDefer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDefer_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDefer_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkDefer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDefer_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDefer_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkDefer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDefer_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optRegister(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRegister_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optRegister(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRegister_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optRegister(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRegister_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick cboReportDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboReportDest, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboReportDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboReportDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboFormat, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Private Function bExecModulePost() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    
    lRetVal = -1
    
    With moClass.moAppDB
      
      .SetInParam mlBatchKey      'BatchKey
      .SetInParamNull SQL_INTEGER 'SessionID
      .SetInParam kModuleAP       'SourceModule
      .SetOutParam lRetVal        'RetVal
      
      On Error Resume Next
      
      .ExecuteSP ("spciModPost")
      
      If Err.Number <> 0 Then
         lRetVal = -1
         MsgBox Err.Description, vbCritical
      Else
         lRetVal = .GetOutParamLong(4)
      End If
      
      On Error GoTo VBRigErrorRoutine
      
      .ReleaseParams
    
    End With
    
    '-- set the return value
    bExecModulePost = (lRetVal <> -1)

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExecModulePost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bExecGLPost() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    
    lRetVal = -1
        
    With moClass.moAppDB
      
      .SetInParam mlBatchKey      'BatchKey
      .SetInParam kModuleAP       'SourceModule
      .SetOutParam lRetVal        'RetVal
      
      On Error Resume Next
      .ExecuteSP ("spciGLPost")
      If Err.Number <> 0 Then
         lRetVal = -1
         MsgBox Err.Description, vbCritical
      Else
         lRetVal = .GetOutParamLong(3)
      End If
      On Error GoTo VBRigErrorRoutine
      
      .ReleaseParams
    
    End With

    '-- set the return value
    bExecGLPost = (lRetVal <> -1)

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExecGLPost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bExecPrePost() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    
    lRetVal = -1
        
    With moClass.moAppDB
      
      .SetInParam mlBatchKey      'BatchKey
      .SetInParamNull SQL_INTEGER 'SessionID
      .SetInParam kModuleAP       'SourceModule
      .SetOutParam lRetVal        'RetVal
      
      On Error Resume Next
      .ExecuteSP ("spciPrePost")
      If Err.Number <> 0 Then
         lRetVal = -1
         MsgBox Err.Description, vbCritical
      Else
         lRetVal = .GetOutParamLong(4)
      End If
      On Error GoTo VBRigErrorRoutine
      
      .ReleaseParams
    
    End With

    '-- set the return value
    bExecPrePost = (lRetVal <> -1)

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExecPrePost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bExecValidatePosting(lTaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    Dim iBatchType As Integer
    
    lRetVal = -1
        
    '-- Set the batch type based on the task number passed in
    Select Case lTaskNumber
    Case ktskVoucher
        iBatchType = kBatchTypeAPVO
    Case ktskManCheck
        iBatchType = kBatchTypeAPMC
    Case ktskSysCheck
        iBatchType = kBatchTypeAPSC
    Case ktskPmtAppl
        iBatchType = kBatchTypeAPPA
    End Select
    
    '-- execute the validation stored procedure
    With moClass.moAppDB
      
      .SetInParam mlBatchKey      'BatchKey
      .SetInParamNull SQL_INTEGER 'SessionID
      .SetInParam kModuleAP       'SourceModule
      .SetInParam iBatchType      'BatchType
      .SetOutParam lRetVal        'RetVal
      
      On Error Resume Next
      .ExecuteSP ("spciValidatePosting")
      If Err.Number <> 0 Then
         lRetVal = -1
         MsgBox Err.Description, vbCritical
      Else
         lRetVal = .GetOutParamLong(5)
      End If
      On Error GoTo VBRigErrorRoutine
      
      .ReleaseParams
    
    End With

    '-- set the return value
    bExecValidatePosting = lRetVal

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExecValidatePosting", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bExecModuleCleanup() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    
    lRetVal = -1
        
    With moClass.moAppDB
      
      .SetInParam mlBatchKey      'BatchKey
      .SetInParam kModuleAP       'SourceModule
      .SetOutParam lRetVal        'RetVal
      
      On Error Resume Next
      .ExecuteSP ("spciModCleanup")
      If Err.Number <> 0 Then
         lRetVal = -1
         MsgBox Err.Description, vbCritical
      Else
         lRetVal = .GetOutParamLong(3)
      End If
      On Error GoTo VBRigErrorRoutine
      
      .ReleaseParams
    
    End With

    '-- set the return value
    bExecModuleCleanup = (lRetVal <> -1)

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExecModuleCleanup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub DoValidatePosting(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    Dim ValidatePostingVal As Integer
                                                                                           
    SetUIStatus             'Checks the register status and if records exist
        
    If Not bRecordsExist() Then
        moClass.lUIActive = kChildObjectInactive
        moClass.moFramework.SavePosSelf
        bCheckStatus = False
        Me.Hide
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
        
    mbRegChecked = False
    
    If miMode = 1 Then sbrMain.Message = guLocalizedStrings.Validating
             
    sbrMain.Status = SOTA_SB_BUSY
                     
    '-- VALIDATION !!!
    ValidatePostingVal = bExecValidatePosting(lTaskNumber)
    If (ValidatePostingVal <> 0) Then
        sbrMain.Status = SOTA_SB_NONE
        If (ValidatePostingVal = 1) Then
            mbPostWarning = True
        Else
            mbPostError = True
        End If
        moClass.lUIActive = kChildObjectInactive
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
                    
    sbrMain.Status = SOTA_SB_NONE
    
    moClass.lUIActive = kChildObjectInactive
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoValidatePosting", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DoPrePost(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lMsgReturn      As Long
    Dim lReturnCode     As Long
      
    moClass.lUIActive = kChildObjectActive
    
    If lTaskNumber <> mlTaskNumber Then
        SetVariables lTaskNumber
    End If
    
    sbrMain.Status = SOTA_SB_BUSY
    
    'run sp to create tglPosting rows
    If miMode = 1 Then sbrMain.Message = guLocalizedStrings.ProcessingGLRegister
    
    '-- PRE-POST !!!
    If Not bExecPrePost Then
        mbPostError = True
    End If

    sbrMain.Status = SOTA_SB_NONE
    moClass.lUIActive = kChildObjectInactive
            
'+++ VB/Rig Begin Pop +++
Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoPrePost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bDoPost(lTaskNumber As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal              As Long
    Dim iMsgReturn           As Integer
    Dim lReturnCode          As Long
    Dim sSQL                 As String
    Dim sql                 As String   'SGS-JMM 4/25/2011
    Dim lBatchNo            As Long     'SGS-JMM 4/25/2011
   
    bDoPost = False
    
    'POST only if user does not have it set on Preview only or Print but not post
    If Not optRegister(kPreviewOnly) And Not optRegister(kPrintNoPost) Then
        
        'check for errors first using the spapCheckRegStatus
        'only if it wasn't checked before
        If Not mbRegChecked Then
            SetUIStatus
        End If
       
        If uRegStat.iErrorStatus = 1 And miMode = 1 Then    ' fatal errors exist
            iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
            moClass.lUIActive = kChildObjectInactive
            Exit Function
        End If
        
        'If user wants to be asked for permission to post
        If optRegister(kPrintPromptPost) Then
            Me.MousePointer = vbDefault
            fraRegister.Enabled = True
            iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgPostRegister, guLocalizedStrings.JournalDesc)
            If iMsgReturn = kretNo Then Exit Function
            Me.MousePointer = vbHourglass
            fraRegister.Enabled = False
        End If
        
        'Execute the main routine that generates the extract file
        If miMode = 1 Then 'Must be a single batch
            If miExtractType = kExtractACH Then
                If bValidateRemitTo Then
                    mbPrenote = False
                    If bValidateACHPrenote Then
                        If Not ProcessACHFile Then
                            Me.MousePointer = vbDefault
                            fraRegister.Enabled = True
                            Exit Function
                        Else
                            iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgAPGenFileSuccess, "ACH")
                            If iMsgReturn = vbNo Then
                                Exit Function
                            Else
                                If mbPrenote Then
                                    giSotaMsgBox Me, moFrmSysSession, kmsgAPACHPrenotePost
                                    Exit Function
                                End If
                            End If
                        End If
                    Else
                        giSotaMsgBox Me, moFrmSysSession, kmsgAPACHMixedPrenoteBatch
                        Exit Function
                    End If
                Else
                    Exit Function
                End If
            
            ElseIf miExtractType = kExtractPosPay And mbDoPosPay Then
                If Not ProcessPositivePay Then
                    Me.MousePointer = vbDefault
                    fraRegister.Enabled = True
                    Exit Function
                Else
                    iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgAPGenFileSuccess, "Positive Pay")
                    If iMsgReturn = vbNo Then Exit Function
                    
                    'SGS-JMM 4/25/2011 Pos pay exported successfully. Log for KS.
                    lBatchNo = glGetValidLong(oClass.moAppDB.Lookup("BatchNo", "tciBatchLog WITH (NOLOCK)", "BatchKey = " & mlBatchKey))
                    
                    sql = ""
                    sql = sql & "INSERT INTO tapPosPayExpLog_SGS (" & vbCrLf
                    sql = sql & "       BatchKey," & vbCrLf
                    sql = sql & "       BatchID," & vbCrLf
                    sql = sql & "       CompanyID," & vbCrLf
                    sql = sql & "       CreateDate," & vbCrLf
                    sql = sql & "       CreateUserID," & vbCrLf
                    sql = sql & "       ExportPath)" & vbCrLf
                    sql = sql & "VALUES (" & vbCrLf
                    sql = sql & "       " & mlBatchKey & "," & vbCrLf
                    sql = sql & "       " & gsQuoted(sGetBatchID(Me)) & "," & vbCrLf
                    sql = sql & "       " & gsQuoted(msCompanyID) & "," & vbCrLf
                    sql = sql & "       GETDATE()," & vbCrLf
                    sql = sql & "       " & gsQuoted(sbrMain.UserName) & "," & vbCrLf
                    sql = sql & "       " & gsQuoted(frmProperties.sFileExtractDir & "\PosPayBatchNo" & lBatchNo & ".ext") & ")" & vbCrLf
                    
                    On Error Resume Next
                    moClass.moAppDB.ExecuteSQL sql
                    If Err.Number <> 0 Then
                        MsgBox "Error while writing KnowledgeSync record: " & vbCrLf & vbCrLf & Err.Description, vbCritical, Me.Caption
                        Err.Clear
                    End If
                    On Error GoTo VBRigErrorRoutine
                    'End SGS-JMM
                
                End If
            End If
        End If
        
        '-- Start posting......
        
        moClass.lUIActive = kChildObjectActive
        
        sbrMain.Status = SOTA_SB_BUSY

        '-- MODULE POST !!!
        If miMode = 1 Then
            sbrMain.Message = guLocalizedStrings.PostingModule
        Else
            moClass.UpdateStatus 200
        End If
        
        If Not bExecModulePost Then
            mbPostError = True
            If miMode = 1 Then
                iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
                If iMsgReturn = kretOK Then
                    With moClass.moAppDB
                        .SetInParam mlBatchKey
                        .SetInParam mlLanguageID
                        .SetOutParam lRetVal
                        .ExecuteSP ("spapPopulateErrorCmt")
                        lRetVal = .GetOutParam(3)
                        .ReleaseParams
                    End With
                    PrintErrorLog
                    ClearErrorLog
                End If
                sbrMain.Message = guLocalizedStrings.PostingFailed
            Else
                PrintErrorLog
                ClearErrorLog
                moClass.UpdateStatus 1250
            End If
            sbrMain.Status = SOTA_SB_NONE
            moClass.lUIActive = kChildObjectInactive
            Exit Function
        End If
        
        '-- GL POST !!!
        If miMode = 1 Then
            sbrMain.Message = guLocalizedStrings.PostingToGL
        Else
            moClass.UpdateStatus 300
        End If
        
        If Not bExecGLPost Then
            mbPostError = True
            If miMode = 1 Then
                iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
                If iMsgReturn = kretOK Then
                    PrintErrorLog
                    ClearErrorLog
                End If
                sbrMain.Message = guLocalizedStrings.PostingFailed
            Else
                PrintErrorLog
                ClearErrorLog
                moClass.UpdateStatus 1250
            End If
            sbrMain.Status = SOTA_SB_NONE
            moClass.lUIActive = kChildObjectInactive
            Exit Function
        End If
        
        '-- MODULE CLEANUP !!!
        If miMode = 1 Then
            sbrMain.Message = guLocalizedStrings.ModuleCleanup
        Else
            moClass.UpdateStatus 400
        End If
        
        If Not bExecModuleCleanup Then
            mbPostError = True
            If miMode = 1 Then
                iMsgReturn = giSotaMsgBox(Me, moFrmSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
                If iMsgReturn = kretOK Then
                    PrintErrorLog
                    ClearErrorLog
                End If
                sbrMain.Message = guLocalizedStrings.PostingFailed
            Else
                PrintErrorLog
                ClearErrorLog
                moClass.UpdateStatus 1250
            End If
            sbrMain.Status = SOTA_SB_NONE
            moClass.lUIActive = kChildObjectInactive
            Exit Function
        End If
        
        If miMode = 1 Then
            sbrMain.Message = guLocalizedStrings.PostingComplete
        Else
            moClass.UpdateStatus 500
        End If
        
        bDoPost = True
        
        '-- Print any warnings that occurred during posting
        If gvCheckNull(moClass.moAppDB.Lookup("Count(*)", "tciErrorLog", "SessionID = " & mlBatchKey & " AND Severity = 1")) Then
            With moClass.moAppDB
                .SetInParam mlBatchKey
                .SetInParam mlLanguageID
                .SetOutParam lRetVal
                .ExecuteSP ("spapPopulateErrorCmt")
                lRetVal = .GetOutParam(3)
                .ReleaseParams
            End With
            PrintErrorLog
            ClearErrorLog
        End If

            
    End If
    
    sbrMain.Status = SOTA_SB_NONE
    moClass.lUIActive = kChildObjectInactive

    SetUIStatus
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDoPost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub UpdateBatchLog(lBatchKey As Long, iPostError As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim sSQL As String
    
    sSQL = "EXEC spciUpdateBatchLog @iBatchKey = " & lBatchKey & ", @iPostError = " & iPostError
  
    moAppDAS.ExecuteSQL (sSQL)
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UpdateBatchLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

'PA start
Private Sub chkProjectDetails_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkProjectDetails, True
    #End If
'+++ End Customizer Code Push +++
Dim iProjectDetails As Integer 'Added for PA Mods
iProjectDetails = chkProjectDetails.Value    'Include ProjectDetails = 1  Added for PA

    If miUseMultCurr = 1 And iProjectDetails = vbUnchecked Then
            msReportName = kVO_DETAIL_RPT_NAME_MC
        ElseIf miUseMultCurr = 1 And iProjectDetails = vbChecked Then
            msReportName = kVO_DETAIL_RPT_NAME_MC_PA
        ElseIf iProjectDetails = vbChecked Then
            msReportName = kVO_DETAIL_RPT_NAME_PA
        Else: msReportName = kVO_DETAIL_RPT_NAME
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkProjectDetails_Click", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
'PA end


Public Function CreateFASAsset() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim oAssetOverride   As FASLink.AssetOverride
    Dim sSQL As String
    Dim rs          As DASRecordSet
    Dim lVoucherLineKey As Integer
    Dim lTemplateID As String
    Dim FASSystemNo As Long
    Dim VouchNo As String
    Dim VendID As String
    Dim VendName As String
    Dim LineAmt As Double
    Dim TranDate As Date
    Dim Description As String
    Dim TranNoChngOrd As String
    Dim FASDatabase As String
    Dim FASCompany  As String
    Dim IntegrateWithFAS As Integer
    Dim POLineDistKey As Integer
    Dim FASLinkIsInitialized As Boolean


    FASLinkIsInitialized = False
    
    Set RegFASApp = New FASLink.Application
    
    ' *** Setup various field overrides to the template
    Set oAssetOverride = New FASLink.AssetOverride

    CreateFASAsset = 0


    sSQL = "SELECT o.IntegrateWithFAS, o.FASDatabase, o.FASCompany, "
    sSQL = sSQL & "b.BatchID, v.VouchNo, vd.VendID, vd.VendName, l.VoucherLineKey, "
    sSQL = sSQL & "l.Description, v.TranDate, t.FASAssetTemplate, TranNoChngOrd = COALESCE(po.TranNoChngOrd,''), "
    sSQL = sSQL & "LineAmt = t.ExtAmt + COALESCE(t.FreightAmt,0) + COALESCE(SUM(tx.ActSTaxAmt),0) "
    sSQL = sSQL & "FROM tapVoucher v WITH (NOLOCK) "
    sSQL = sSQL & "INNER JOIN tciBatchLog b WITH (NOLOCK) ON (b.BatchKey = v.BatchKey) "
    sSQL = sSQL & "INNER JOIN tapVoucherDetl l WITH (NOLOCK) ON (v.VoucherKey = l.VoucherKey) "
    sSQL = sSQL & "INNER JOIN tapVoucherLineDist t WITH (NOLOCK) ON (l.VoucherLineKey = t.VoucherLineKey) "
    sSQL = sSQL & "INNER JOIN tapOptions o WITH (NOLOCK) ON (o.CompanyID = l.TargetCompanyID) "
    sSQL = sSQL & "INNER JOIN tapVendor vd WITH (NOLOCK) ON (v.VendKey = vd.VendKey) "
    sSQL = sSQL & "LEFT OUTER JOIN tciSTaxCodeTran tx WITH (NOLOCK) ON (tx.STaxTranKey = t.STaxTranKey) "
    sSQL = sSQL & "LEFT OUTER JOIN tpoPOLineDist pd WITH (NOLOCK) ON ( t.POLineDistKey = pd.POLineDistKey) "
    sSQL = sSQL & "LEFT OUTER JOIN tpoPOLine pl WITH (NOLOCK) ON ( pd.POLineKey = pl.POLineKey ) "
    sSQL = sSQL & "LEFT OUTER JOIN tpoPurchOrder po WITH (NOLOCK) ON (po.POKey = pl.POKey ) "
    sSQL = sSQL & "Where v.BatchKey = " & mlBatchKey & " "
    sSQL = sSQL & "AND COALESCE(LTRIM(t.FASAssetTemplate),'') <> '' "
    sSQL = sSQL & "GROUP BY o.IntegrateWithFAS, o.FASDatabase, o.FASCompany, b.BatchID, "
    sSQL = sSQL & "v.VouchNo, vd.VendID, l.VoucherLineKey, l.Description, v.TranDate, vd.VendName, "
    sSQL = sSQL & "t.FASAssetTemplate , po.TranNoChngOrd, t.ExtAmt, t.FreightAmt "


    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
    While Not rs.IsEOF
    
        lVoucherLineKey = rs.Field("VoucherLineKey")
        If IsNull(rs.Field("FASAssetTemplate")) Then
            lTemplateID = ""
        Else
            lTemplateID = rs.Field("FASAssetTemplate")
        End If

        VouchNo = rs.Field("VouchNo")
        VendID = rs.Field("VendID")
        VendName = rs.Field("VendName")
        LineAmt = rs.Field("LineAmt")
        TranDate = rs.Field("TranDate")
        Description = rs.Field("Description")
        TranNoChngOrd = rs.Field("TranNoChngOrd")
        
        IntegrateWithFAS = rs.Field("IntegrateWithFAS")

        If IntegrateWithFAS = 1 And Not IsNull(lTemplateID) And LTrim(lTemplateID) <> "" Then

            If FASLinkIsInitialized = False Then
                
                If Not RegFASApp.IsAvailable Then
            
                        ' *** Unable to initialize FAS Link
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMFASAPINotAvailable
                        Set RegFASApp = Nothing
                        CreateFASAsset = 0
                        Exit Function
                End If
                
                ' *** Attempt to initialize FAS Link
                If (RegFASApp.Initialize) Then
                                
                        FASLinkIsInitialized = True
                            
                Else
                        
                       ' *** Unable to initialize FAS Link
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMFASInitializeFailed
                        Set RegFASApp = Nothing
                        CreateFASAsset = 0
                        Exit Function
                
                End If
            
            End If
            
            'Calclate the tax amount on the line

            With oAssetOverride
                .Field(bfAcquisitionValue) = LineAmt
                .Field(gfAcquisitionDate) = TranDate
                .Field(bfPlacedInService) = TranDate
                .Field(gfDescription) = Description
                .Field(gfVendorMFG) = VendID
                .Field(gfPONumber) = TranNoChngOrd
            End With

            FASDatabase = rs.Field("FASDatabase")
            FASCompany = rs.Field("FASCompany")

            If Not IsNull(FASDatabase) Then
                RegFASApp.Database = FASDatabase
            End If

            If Not IsNull(FASCompany) Then
                RegFASApp.Company = FASCompany
            End If

            FASSystemNo = RegFASApp.AddAsset(lTemplateID, oAssetOverride)
                
            If FASSystemNo > 0 Then
                    sSQL = "Update tapVoucherLineDist "
                    sSQL = sSQL & "SET FASAssetNumber = " & FASSystemNo & " "
                    sSQL = sSQL & "WHERE VoucherLineKey = " & lVoucherLineKey
                    moAppDAS.ExecuteSQL (sSQL)
                    
                    sSQL = "INSERT INTO #tapAddFASAssetWrk "
                    sSQL = sSQL & "(SessionID, VouchNo, VendID, VendName, AddedToFAS, FASSystemNo, "
                    sSQL = sSQL & "Template, AcquisitionValue, ServiceDate,  AcquisitionDate,  Descript, TranNoChngOrd, FASCompany)"
                    sSQL = sSQL & "Values (" & mlBatchKey & ", '" & VouchNo & "', '" & VendID & "', '" & VendName & "',"
                    sSQL = sSQL & "'Yes', '" & FASSystemNo & "', '" & lTemplateID & "', '" & LineAmt & "',"
                    sSQL = sSQL & "'" & gsFormatDateToDB(Str(TranDate)) & "', '" & gsFormatDateToDB(Str(TranDate)) & "', '" & Description & "', '" & TranNoChngOrd & "', '" & FASCompany & "')"

                    moAppDAS.ExecuteSQL (sSQL)
            Else
                    sSQL = "INSERT INTO #tapAddFASAssetWrk "
                    sSQL = sSQL & "(SessionID, VouchNo, VendID, VendName, AddedToFAS, FASSystemNo, "
                    sSQL = sSQL & "Template, AcquisitionValue, ServiceDate,  AcquisitionDate,  Descript, TranNoChngOrd, FASCompany)"
                    sSQL = sSQL & "Values (" & mlBatchKey & ", '" & VouchNo & "', '" & VendID & "', '" & VendName & "',"
                    sSQL = sSQL & "'No', '', '" & lTemplateID & "', '" & LineAmt & "',"
                    sSQL = sSQL & "'" & gsFormatDateToDB(Str(TranDate)) & "', '" & gsFormatDateToDB(Str(TranDate)) & "', '" & Description & "', '" & TranNoChngOrd & "', '" & FASCompany & "')"

                    moAppDAS.ExecuteSQL (sSQL)
            End If

            CreateFASAsset = CreateFASAsset + 1

        End If
    
        rs.MoveNext
    
    Wend

    If FASLinkIsInitialized Then
        RegFASApp.Terminate
        Set RegFASApp = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "CreateFASAsset", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function


Public Sub ValidateFasAsset()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim oAssetOverride   As FASLink.AssetOverride
    Dim sSQL As String
    Dim rs   As DASRecordSet
    Dim lVoucherLineKey As Integer
    Dim lTemplateID As String
    Dim FASSystemNo As String
    Dim lRetVal As Integer
    Dim FASDatabase As String
    Dim FASCompany  As String
    Dim IntegrateWithFAS As Integer
    Dim FASLinkIsInitialized As Boolean


    FASLinkIsInitialized = False
    
    Set RegFASApp = New FASLink.Application
    
  
    ' *** Setup various field overrides to the template
    Set oAssetOverride = New FASLink.AssetOverride


    sSQL = "SELECT o.IntegrateWithFAS, o.FASDatabase, o.FASCompany, "
    sSQL = sSQL & "b.BatchID, v.VouchNo, vd.VendID, l.VoucherLineKey, "
    sSQL = sSQL & "l.Description, v.TranDate, t.FASAssetTemplate, TranNoChngOrd = COALESCE(po.TranNoChngOrd,''), "
    sSQL = sSQL & "LineAmt = t.ExtAmt + t.FreightAmt + Coalesce(Sum(Coalesce(tx.ActSTaxAmt,0)),0) "
    sSQL = sSQL & "FROM tapPendVoucher v WITH (NOLOCK) "
    sSQL = sSQL & "INNER JOIN tciBatchLog b WITH (NOLOCK) ON (b.BatchKey = v.BatchKey) "
    sSQL = sSQL & "INNER JOIN tapVoucherDetl l WITH (NOLOCK) ON (v.VoucherKey = l.VoucherKey) "
    sSQL = sSQL & "INNER JOIN tapVoucherLineDist t WITH (NOLOCK) ON (l.VoucherLineKey = t.VoucherLineKey) "
    sSQL = sSQL & "INNER JOIN tapOptions o WITH (NOLOCK) ON (o.CompanyID = l.TargetCompanyID) "
    sSQL = sSQL & "INNER JOIN tapVendor vd WITH (NOLOCK) ON (v.VendKey = vd.VendKey) "
    sSQL = sSQL & "LEFT OUTER JOIN tciSTaxCodeTran tx WITH (NOLOCK) ON (tx.STaxTranKey = t.STaxTranKey) "
    sSQL = sSQL & "LEFT OUTER JOIN tpoPOLineDist pd WITH (NOLOCK) ON ( t.POLineDistKey = pd.POLineDistKey) "
    sSQL = sSQL & "LEFT OUTER JOIN tpoPOLine pl WITH (NOLOCK) ON ( pd.POLineKey = pl.POLineKey ) "
    sSQL = sSQL & "LEFT OUTER JOIN tpoPurchOrder po WITH (NOLOCK) ON (po.POKey = pl.POKey ) "
    sSQL = sSQL & "Where v.BatchKey = " & mlBatchKey & " "
    sSQL = sSQL & "AND COALESCE(LTRIM(t.FASAssetTemplate),'') <> '' "
    sSQL = sSQL & "GROUP BY o.IntegrateWithFAS, o.FASDatabase, o.FASCompany, b.BatchID, "
    sSQL = sSQL & "v.VouchNo, vd.VendID, l.VoucherLineKey, l.Description, v.TranDate, "
    sSQL = sSQL & "t.FASAssetTemplate , po.TranNoChngOrd, t.ExtAmt, t.FreightAmt "

    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
    While Not rs.IsEOF
    
        lVoucherLineKey = rs.Field("VoucherLineKey")
        
        If IsNull(rs.Field("FASAssetTemplate")) Then
            lTemplateID = ""
        Else
            lTemplateID = rs.Field("FASAssetTemplate")
        End If
        
        IntegrateWithFAS = rs.Field("IntegrateWithFAS")

        If IntegrateWithFAS = 1 And Not IsNull(lTemplateID) And LTrim(lTemplateID) <> "" Then

            If FASLinkIsInitialized = False Then
                
                If Not RegFASApp.IsAvailable Then
            
                        ' *** Unable to initialize FAS Link
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMFASAPINotAvailable
                        Set RegFASApp = Nothing
                        Exit Sub
                End If
                
                ' *** Attempt to initialize FAS Link
                If (RegFASApp.Initialize) Then
                                
                        FASLinkIsInitialized = True
                            
                Else
                        
                       ' *** Unable to initialize FAS Link
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMFASInitializeFailed
                        Set RegFASApp = Nothing
                        
                        Exit Sub
                
                End If
            
            End If
            
            FASDatabase = rs.Field("FASDatabase")
            FASCompany = rs.Field("FASCompany")

               
            If Not IsNull(FASDatabase) And LTrim(FASDatabase) <> "" Then
                RegFASApp.Database = FASDatabase
            Else
                MsgBox "FAS Database Name Does Not Exist: " & FASDatabase
            End If

            If Not IsNull(FASCompany) And LTrim(FASCompany) <> "" Then
                RegFASApp.Company = FASCompany
            Else

                MsgBox "FAS Company Name Does Not Exist:" & FASCompany
            End If

            With oAssetOverride
                .Field(bfAcquisitionValue) = rs.Field("LineAmt")
                .Field(gfAcquisitionDate) = rs.Field("TranDate")
                .Field(gfDescription) = rs.Field("Description")
                .Field(gfVendorMFG) = rs.Field("VendID")
                .Field(gfPONumber) = rs.Field("TranNoChngOrd")
            End With

                    
           If Not (RegFASApp.ValidAsset(lTemplateID, oAssetOverride)) Then

                With moClass.moAppDB
                .SetInParam mlBatchKey
                .SetInParam mlBatchKey
                .SetInParam Null
                .SetInParam 141167
                .SetInParam Null
                .SetInParam rs.Field("BatchID")
                .SetInParam rs.Field("VouchNo")
                .SetInParam rs.Field("Description")
                .SetInParam Null
                .SetInParam Null
                .SetInParam Null
                .SetInParam 2
                .SetInParam 1
                .SetOutParam lRetVal
                .ExecuteSP ("spglAPIErrorLog")
                lRetVal = .GetOutParam(14)
                .ReleaseParams
                End With


            End If
                
        End If
    
        rs.MoveNext
    
    Wend


    If FASLinkIsInitialized Then
        RegFASApp.Terminate
        Set RegFASApp = Nothing
    End If
    
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ValidateFasAsset", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub




Public Function PrintFASAsset(sButton As String, frm As Form, Optional iFileType As Variant, Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetVal As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
Dim ReportObj As clsReportEngine
Dim DBObj As Object
Dim ReportOption As Integer
Dim ReportOptionText As String

    On Error GoTo badexit

    PrintFASAsset = kFailure
    
    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
        
    'CUSTOMIZE:  Build a string for selection of key field and session ID.
    sSelect = "SELECT " & ReportObj.SessionID
    sSelect = sSelect & ", VouchNo, VendID, VendName, AddedToFAS, FASSystemNo, Template, "
    sSelect = sSelect & " AcquisitionValue, ServiceDate,  AcquisitionDate,  Descript, TranNoChngOrd, FASCompany "
    sSelect = sSelect & " FROM #tapAddFASAssetWrk "
    sSelect = sSelect & " " & sWhereClause

    sInsert = "INSERT INTO tapAddFASAssetWrk (SessionID, VouchNo, VendID, VendName, AddedToFAS, FASSystemNo, Template, AcquisitionValue, ServiceDate,  AcquisitionDate,  Descript, TranNoChngOrd, FASCompany ) " & sSelect
    
    On Error Resume Next

    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = kVO_FAS_ASSET_RPT_NAME
    ReportObj.ReportTitle1 = "Fixed Asset Register"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas.
    ReportObj.ReportTitle1() = gsBuildString(kFASImportStatusReport, goClass.moAppDB, goClass.moSysSession)
    ReportObj.ReportTitle2() = ""
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
        
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    If (ReportObj.lRestrictBy("{tapAddFASAssetWrk.SessionID} = " & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
    
    ReportObj.bSetReportFormula "BatchID", "'" & sGetBatchID(Me) & "'"
    ReportObj.bSetReportFormula "PostDate", "'" & gsFormatDateToDB(Str(mdPostDate)) & "'"
    ReportObj.bSetReportFormula "CompanyName", gsQuoted(moClass.moSysSession.CompanyName)

    'Always show the summary section
    ReportObj.lSetSummarySection 1, moOptions
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName, True
            
    ShowStatusNone frm
    
    PrintFASAsset = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintFASAsset", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function



Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lInitializeReport = -1
    
    Set moSotaObjects = New Collection
    Set moReport = New clsReportEngine
    Set moDDData = New clsDDData
    Set moPrinter = Printer
    
    If Not moDDData.lInitDDData(sWorkTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If Not (moDDData.lDDBuildData(App.EXEName) = kSuccess) Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, False) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
   
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lCreateWorkTable(SP_CREATEWRK As String, colInSPParams As Collection, ByRef colOutSPParams As Collection) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iLoop As Variant
Dim vCollectionItem As Variant
Dim iNoOfOutParams As Variant
Dim iNoOfInParams As Long
Dim vOutParam As Variant

'run register stored procedure to create work table and tglPosting table rows
    moClass.lUIActive = kChildObjectActive
    iNoOfInParams = colInSPParams.Count
    iNoOfOutParams = colOutSPParams.Count
    
    With moAppDAS
    
        ' set the "in" parameters from the "in" collection
        iLoop = 1
        For Each vCollectionItem In colInSPParams
            .SetInParam colInSPParams.Item(iLoop)
            iLoop = iLoop + 1
        Next
        
        ' set the "out" parameters from the "out" collection
        For Each vCollectionItem In colOutSPParams
            vOutParam = vCollectionItem
            ColSetOutParam vOutParam
        Next
        
        ' run the stored procedure
        .ExecuteSP (SP_CREATEWRK)
        
        ' clear out all elements of the out collection
        iLoop = 1
        For Each vCollectionItem In colOutSPParams
            colOutSPParams.Remove iLoop
        Next
        
        ' get the out param values from the stored procedure
        For iLoop = iNoOfInParams + 1 To iNoOfInParams + iNoOfOutParams
            colOutSPParams.Add .GetOutParam(iLoop)
        Next iLoop
        
        .ReleaseParams

    End With
    
    ' set the return value of this function to the last out param value
    lCreateWorkTable = colOutSPParams.Item(iNoOfOutParams)
        
    moClass.lUIActive = kChildObjectInactive
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lCreateWorkTable", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function


Public Function lDeleteWorkTable(SP_DELETEWRK As String, colInSPParams As Collection, ByRef colOutSPParams As Collection) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iLoop As Variant
Dim vCollectionItem As Variant
Dim iNoOfInParams As Long
Dim iNoOfOutParams As Variant
Dim vOutParam As Variant

    'run register stored procedure to delete work table
    moClass.lUIActive = kChildObjectActive
    iNoOfInParams = colInSPParams.Count
    iNoOfOutParams = colOutSPParams.Count
    
    With moAppDAS
    
        ' set the "in" parameters from the "in" collection
        iLoop = 1
        For Each vCollectionItem In colInSPParams
            .SetInParam colInSPParams.Item(iLoop)
            iLoop = iLoop + 1
        Next
        
        ' set the "out" parameters from the "out" collection
        
        For Each vCollectionItem In colOutSPParams
            vOutParam = vCollectionItem
            ColSetOutParam vOutParam
        Next
        
          ' run the stored procedure
        .ExecuteSP (SP_DELETEWRK)
        
        ' clear out all elements of the out collection
        iLoop = 1
        For Each vCollectionItem In colOutSPParams
            colOutSPParams.Remove iLoop
        Next
        
        ' get the out param values from the stored procedure
        For iLoop = iNoOfInParams + 1 To iNoOfInParams + iNoOfOutParams
            colOutSPParams.Add .GetOutParam(iLoop)
        Next iLoop
        
        .ReleaseParams

    End With
    
    ' set the return value of this function to the last out param value
    lDeleteWorkTable = colOutSPParams.Item(iNoOfOutParams)
    
    moClass.lUIActive = kChildObjectInactive

    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lDeleteWorkTable", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function


Public Function PrintRegister(ByRef oSessionID As Long, iRegOption As Integer, bDeferred As Boolean, _
                            sPrinter As String, iCopies As Integer, sSortBy As String, sFormulas() As String, _
                                ReportCondition As Collection, sReportName As String) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    
    moClass.lUIActive = kChildObjectActive
    
    msReportName = sReportName
    PrintRegister = kFailure
    
    'Build select statements for lookup and for crystal
    msCRWSelect = sSortBy
    sSortBy = ""
    
    moReport.PrinterName = sPrinter
    
    If iRegOption = kPreviewOnly Then
        PrintRegister = lStartReport(kTbPreview, Me, False, "", "", "", "", "")
    ElseIf bDeferred Then
        PrintRegister = lStartReport(kTbDefer, Me, False, "", "", "", "", "")
    Else
        PrintRegister = lStartReport(kTbPrint, Me, False, "", "", "", "", "")
    End If
    
    'Exit this function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "PrintRegister", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Private Sub ColSetOutParam(vColItem As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case TypeName(vColItem)
    
        Case "String"
            Dim sColItem As String
            sColItem = CStr(vColItem)
            moAppDAS.SetOutParam sColItem
        Case "Integer"
            Dim iColItem As Integer
            iColItem = CInt(vColItem)
            moAppDAS.SetOutParam iColItem
        Case "Long"
            Dim lColItem As Long
            lColItem = CLng(vColItem)
            moAppDAS.SetOutParam lColItem
        Case "Single"
            Dim sngColItem As Single
            sngColItem = CSng(vColItem)
            moAppDAS.SetOutParam sngColItem
        Case "Double"
            Dim dblColItem
            dblColItem = CDbl(vColItem)
            moAppDAS.SetOutParam dblColItem
        Case "Date"
            Dim dteColItem As Date
            dteColItem = CDate(vColItem)
            moAppDAS.SetOutParam dteColItem
        Case "Boolean"
            Dim boolColItem As Boolean
            boolColItem = CBool(vColItem)
            moAppDAS.SetOutParam boolColItem
        Case "Byte"
            Dim byteColItem As Byte
            byteColItem = CByte(vColItem)
            moAppDAS.SetOutParam byteColItem
        Case "Currency"
            Dim curColItem As Currency
            curColItem = CCur(vColItem)
            moAppDAS.SetOutParam curColItem
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ColSetOutParam", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Function sGetWhere(oSessionID As Long) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sWhere As String
    Dim sSQL   As String
    Dim rs As Object

    sWhere = "({tciErrorLog.BatchKey} = " & Str(oSessionID)
    
    sSQL = "SELECT LinkedBatchKey FROM tciBatchLink WHERE BatchKey = " & Str(oSessionID)
    
    Set rs = moAppDAS.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    With rs
        .MoveFirst
        While Not .IsEOF
            sWhere = sWhere & " OR {tciErrorLog.BatchKey} = " & .Field("LinkedBatchKey")
          .MoveNext
        Wend
      .Close
    End With
    
    sWhere = sWhere & ")"
    
    Set rs = Nothing
        
    sGetWhere = sWhere

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sGetWhere", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'Populate Positive Pay temp tables
Private Sub CreatePosPayTemp()
  Dim sSQL As String
  
  On Error GoTo CreatePosPayTemp_ErrHandler
    
'SGS DEJ 1/12/12 Added Comment for customiztion upgrade 7-3 to 7-4 (START)
'  sSQL = "CREATE TABLE #tapCheckRecordWRK "
'  sSQL = sSQL & "(Indicator Char(2) NULL, "
'  sSQL = sSQL & "BankNumber Char(3) NULL, "
'  sSQL = sSQL & "CashAcctNo Char(10) NULL, "
'  sSQL = sSQL & "CheckNo Char(10) NULL, "
'  sSQL = sSQL & "CheckDate Datetime NULL, "
'  sSQL = sSQL & "CheckAmt Dec(15,3) NULL, "
'  sSQL = sSQL & "AddlData Char(15) NULL, "
'  sSQL = sSQL & "VoidIndicator Char(1) NULL, "
'  sSQL = sSQL & "PayeeName Char(80) NULL)"
  
  sSQL = "CREATE TABLE #tapCheckRecordWRK "
  sSQL = sSQL & "(CashAcctNo Char(12) NULL, "   'SGS-JMM 5/2/2011
  sSQL = sSQL & "VoidIndicator Char(1) NULL, "
  sSQL = sSQL & "CheckNo Char(10) NULL, "
  sSQL = sSQL & "CheckAmt Dec(15,3) NULL, "
  sSQL = sSQL & "CheckDate Datetime NULL, "
  sSQL = sSQL & "Filler CHAR(1) NOT NULL DEFAULT(SPACE(1)), "   'SGS-JMM 5/2/2011
  sSQL = sSQL & "PayeeName Char(80) NULL, "
  sSQL = sSQL & "Filler2 CHAR(16) NOT NULL DEFAULT(SPACE(16)), "    'SGS-JMM 5/2/2011
  sSQL = sSQL & "Indicator Char(2) NULL, "
  sSQL = sSQL & "BankNumber Char(3) NULL, "
  sSQL = sSQL & "AddlData Char(15) NULL) "
'SGS DEJ 1/12/12 Added Comment for customiztion upgrade 7-3 to 7-4 (STOP)
  
  
  
  moCN.Execute sSQL, , adCmdText
    
  sSQL = "CREATE TABLE #tapAcctTotalRecordWRK "
  sSQL = sSQL & "(Indicator Char(2) NULL, "
  sSQL = sSQL & "BankNumber Char(3) NULL, "
'  sSQL = sSQL & "CashAcctNo Char(10) NULL, "   'SGS-JMM 5/2/2011
  sSQL = sSQL & "CashAcctNo Char(12) NULL, "   'SGS-JMM 5/2/2011
  sSQL = sSQL & "CheckCount Integer NULL, "
  sSQL = sSQL & "RunDate Datetime NULL, "
  sSQL = sSQL & "TotalCheckAmt Dec(15,3) NULL, "
  sSQL = sSQL & "Filler1 Char(96) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapFileTotalRecordWRK  "
  sSQL = sSQL & "(Indicator Char(2) NULL, "
  sSQL = sSQL & "BankNumber Char(3) NULL, "
  sSQL = sSQL & "Filler1 Char(10) NULL, "
  sSQL = sSQL & "CheckCount Integer NULL, "
  sSQL = sSQL & "RunDate Datetime NULL, "
  sSQL = sSQL & "TotalCheckAmt Dec(15,3) NULL, "
  sSQL = sSQL & "Filler2 Char(96) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  Exit Sub
  
CreatePosPayTemp_ErrHandler:
    MsgBox "The following error occurred in CreatePosPayTemp: " & _
    vbCrLf & "Error Number: " & Err.Number & _
    vbCrLf & "Error Description: " & Err.Description, _
    vbExclamation + vbOKOnly, Me.Caption
End Sub
'PSG End

'Clean up and Truncate Positive Pay temp tables
Private Sub TruncatePosPayTemp()
  Dim sSQL As String
  
  On Error GoTo TruncatePosPayTemp_ErrHandler
    
  sSQL = "TRUNCATE TABLE #tapCheckRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapAcctTotalRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapFileTotalRecordWRK" & vbCrLf
  
  moCN.Execute sSQL, , adCmdText
  
  Exit Sub
  
TruncatePosPayTemp_ErrHandler:
    MsgBox "The following error occurred in TruncatePosPayTemp: " & _
    vbCrLf & "Error Number: " & Err.Number & _
    vbCrLf & "Error Description: " & Err.Description, _
    vbExclamation + vbOKOnly, Me.Caption
End Sub
'PSG End

'Positive Pay Process
'This routine sends the check information onto a temp table
'and then it generates a text delimited file from the temp table.
Private Function ProcessPositivePay() As Boolean

  Dim oCmd As ADODB.Command
  Dim oRS1 As ADODB.Recordset
  Dim oExt As clsExtract
  
  Dim sSQL As String
  Dim sTmp As String
  Dim sFileDir As String

  Dim lBatchNo As Long
  Dim bRetVal As Boolean
  Dim iFile As Integer
  Dim vLengths1 As Variant

  Dim lRetVal As Long
  Dim sErr As String

  On Error GoTo GotErr
  
  ProcessPositivePay = False
  Set oCmd = New ADODB.Command
  Set oExt = New clsExtract
  
'SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-4 (START)
  If Not PosPayAllowed Then
    chkPosPay.Value = vbUnchecked
    MsgBox "You are not authorized to export positive pay files.", vbOKOnly + vbInformation, Me.Caption
    Exit Function
  End If
'SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-4 (START)
    
  sErr = "Positive Pay Extract file cannot be generated."
  
  If moCN Is Nothing Then
    If Not ConnectADO Then
      giSotaMsgBox Me, moFrmSysSession, kmsgAPGenFileConnectErr, "Positive Pay"
      Exit Function
    End If
  End If
  
  'Clear out temp tables
  TruncatePosPayTemp

  'Run the stored procedure
  With oCmd
  Set .ActiveConnection = moCN
      .CommandText = "spapPopulatePosPay"
      .CommandType = adCmdStoredProc
      .Parameters.Refresh
      .Parameters("@_iBatchKey") = mlBatchKey
      .Execute
      lRetVal = glGetValidLong(.Parameters("@_oRetVal"))
  End With
  
  If lRetVal <> 1 Then
    giSotaMsgBox Me, moFrmSysSession, kmsgAPGenFileUnknownErr, "Positive Pay"
    Exit Function
  End If
        
  Set oCmd = Nothing
  
  sFileDir = Trim(frmProperties.sFileExtractDir)
  
  'sFileDir will be blank if user pressed Cancel or a bad error occurred
  If Trim(sFileDir) = "" Then
    giSotaMsgBox Me, moFrmSysSession, kmsgAPGenFileDirErr, "Positive Pay"
    Exit Function
  End If
  
  lBatchNo = glGetValidLong(oClass.moAppDB.Lookup("BatchNo", "tciBatchLog WITH (NOLOCK)", "BatchKey = " & mlBatchKey))
  sFileDir = sFileDir & "\PosPayBatchNo" & lBatchNo & ".ext"
    
  'Open the file
  iFile = FreeFile
  Open sFileDir For Output As iFile
  On Error GoTo ErrCloseFile

  'Rec 10: Detail Record
'SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-3 (START)
'sTmp = "SELECT * FROM #tapCheckRecordWRK"
  
  sTmp = ""
  sTmp = sTmp & "SELECT CAST(RTRIM(LTRIM(CashAcctNo)) AS BIGINT)," & vbCrLf
  sTmp = sTmp & "       CASE VoidIndicator WHEN ' ' THEN 'O' ELSE VoidIndicator END," & vbCrLf
  sTmp = sTmp & "       CheckNo," & vbCrLf
  sTmp = sTmp & "       CheckAmt," & vbCrLf
  sTmp = sTmp & "       CheckDate," & vbCrLf
  sTmp = sTmp & "       Filler," & vbCrLf
  sTmp = sTmp & "       PayeeName," & vbCrLf
  sTmp = sTmp & "       Filler2" & vbCrLf
  sTmp = sTmp & "FROM   #tapCheckRecordWRK" & vbCrLf
'SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-3 (STOP)
    
  Set oRS1 = New ADODB.Recordset
  oRS1.CursorLocation = adUseClient
  oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText

  'Do not write to the file if there are no records in Check Record worktable (Rec 10 - #tapCheckRecordWRK)
  If oRS1.RecordCount <> 0 Then
    'SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-3 (START)
      'vLengths1 = Array(2, 3, 10, 10, 8, 11, 15, 1, 80)
      vLengths1 = Array(12, 1, 10, 12, 8, 1, 50, 16)
    'SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-3 (STOP)


    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateYYYYMMDD, 2)
                           
    If Not bRetVal Then
      Close #iFile
      Kill sFileDir
      GoTo ShutDown
    End If
  End If
  
  oRS1.Close
  Set oRS1 = Nothing
  
  'Rec 20: Account Total Record
''SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-3 all base code was commented out from the customiztion (START)
'  sTmp = "SELECT * FROM #tapAcctTotalRecordWRK"
'  Set oRS1 = New ADODB.Recordset
'  oRS1.CursorLocation = adUseClient
'  oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
'
'  'Do not write to the file if there are no records in Account Total worktable (Rec 20 - #tapAcctTotalRecordWRK)
'  If oRS1.RecordCount <> 0 Then
'    vLengths1 = Array(2, 3, 10, 10, 8, 11, 96)
'
'    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, _
'                           kNumZeroPad, vLengths1, kDateYYYYMMDD, 2)
'
'    If Not bRetVal Then
'      Close #iFile
'      Kill sFileDir
'      GoTo ShutDown
'    End If
'  End If
'
'  oRS1.Close
'  Set oRS1 = Nothing
'
'  'Rec 30: File Total Record
'  sTmp = "SELECT * FROM #tapFileTotalRecordWRK"
'  Set oRS1 = New ADODB.Recordset
'  oRS1.CursorLocation = adUseClient
'  oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
'
'  'Do not write to the file if there are no records in File Total worktable (Rec 30 - #tapFileTotalRecordWRK)
'  If oRS1.RecordCount <> 0 Then
'    vLengths1 = Array(2, 3, 10, 10, 8, 11, 96)
'    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, _
'                           kNumZeroPad, vLengths1, kDateYYYYMMDD, 2)
'
'    If Not bRetVal Then
'      Close #iFile
'      Kill sFileDir
'      GoTo ShutDown
'    End If
'  End If
'
'  oRS1.Close
'  Set oRS1 = Nothing
''SGS DEJ 1/12/12 Added Comments for Custom Upgrade 7-3 to 7-3 all base code was commented out from the customiztion (STOP)
  Set oExt = Nothing
  
  Close #iFile
  
  ProcessPositivePay = True
  Exit Function

ErrCloseFile:
  MsgBox "ProcessPositivePay: " & Err.Description & ". " & sErr, vbOKOnly + vbExclamation, "Error " & Err.Number
  Close #iFile
  GoTo ShutDown
  
GotErr:
  MsgBox "ProcessPositivePay: " & Err.Description & ". " & sErr, vbOKOnly + vbExclamation, "Error " & Err.Number
  
ShutDown:
  If Not oRS1 Is Nothing Then
    If oRS1.State = adStateOpen Then
      oRS1.Close
    End If
    Set oRS1 = Nothing
  End If
  
  If Not oCmd Is Nothing Then
    Set oCmd = Nothing
  End If
  
  If Not oExt Is Nothing Then
    Set oExt = Nothing
  End If
End Function

'ACH Process
'This routine sends the check information onto a temp table
'and then it generates a text delimited file from the temp table.
Private Function ProcessACHFile() As Boolean

  Dim oCmd As ADODB.Command
  Dim oRS1 As ADODB.Recordset
  Dim oExt As clsExtract
  
  Dim sSQL As String
  Dim sTmp As String
  Dim sFileDir As String
  Dim sFolder As String
  Dim lBatchNo As Long
  Dim bRetVal As Boolean
  Dim iFile As Integer
  Dim vLengths1 As Variant
  Dim vLengths2 As Variant
  Dim lACHType As Long
  Dim lRetVal As Long
  Dim sACHErr As String
  
  On Error GoTo GotErr
  
  ProcessACHFile = False
  Set oCmd = New ADODB.Command
  Set oExt = New clsExtract
  
  sACHErr = "ACH extract file cannot be generated."
  
  If moCN Is Nothing Then
    If Not ConnectADO Then
      giSotaMsgBox Me, moFrmSysSession, kmsgAPGenFileConnectErr, "ACH"
      Exit Function
    End If
  End If
  
  'Clear out temp tables
  TruncateACHTemp
  
  'Run the stored procedure
  With oCmd
  Set .ActiveConnection = moCN
      .CommandText = "spapPopulateACH"
      .CommandType = adCmdStoredProc
      .Parameters.Refresh
      .Parameters("@_iCompanyID") = msCompanyID
      .Parameters("@_iBatchKey") = mlBatchKey
      .Execute
      lACHType = glGetValidLong(.Parameters("@_oACHType"))
      lRetVal = glGetValidLong(.Parameters("@_oRetVal"))
  End With
  
  If lRetVal <> 1 Then
    Select Case lRetVal
      Case 2
        giSotaMsgBox Me, moFrmSysSession, kmsgAPACHBlankOptions
                'MsgBox "Unable to retrieve records from ACH Options. " & sACHErr, vbOKOnly + vbExclamation, "Sage 500 ERP"
      Case 3
        giSotaMsgBox Me, moFrmSysSession, kmsgAPACHNoChecks
                'MsgBox "Batch does not contain any valid checks (ACH only process standard checks). " & sACHErr, vbOKOnly + vbExclamation, "Sage 500 ERP"
      Case 4
        giSotaMsgBox Me, moFrmSysSession, kmsgAPACHNextFileNo
                'MsgBox "Unable to retrieve the next available Canadian ACH/AFT File No. " & sACHErr, vbOKOnly + vbExclamation, "Sage 500 ERP"
      Case 5
        giSotaMsgBox Me, moFrmSysSession, kmsgAPACHBlankRtrn
                'MsgBox "Canadian ACH/AFT Return BankAcctNo & Routing No cannot be blank. " & sACHErr, vbOKOnly + vbExclamation, "Sage 500 ERP"
      Case Else
        giSotaMsgBox Me, moFrmSysSession, kmsgAPGenFileUnknownErr, "ACH"
    End Select
    Exit Function
  End If
        
  Set oCmd = Nothing
  
  sFileDir = Trim(frmProperties.sFileExtractDir)
  
  If Trim(sFileDir) = "" Then
    giSotaMsgBox Me, moFrmSysSession, kmsgAPGenFileDirErr, "ACH"
    Exit Function
  End If
  
  lBatchNo = glGetValidLong(oClass.moAppDB.Lookup("BatchNo", "tciBatchLog WITH (NOLOCK)", "BatchKey = " & mlBatchKey))
  
  'US ACH
  If lACHType = kACHType_US Then
    sFileDir = sFileDir & "\ACHBatchNo" & lBatchNo & ".ext"
    
    'Open the file
    iFile = FreeFile
    Open sFileDir For Output As iFile
    On Error GoTo ErrCloseFile

    'Rec 1:
    sTmp = "SELECT * FROM #tapFileHeaderRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 2, 1, 9, 10, 6, 2, 2, 1, 3, 2, 1, 23, 23, 8)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateYYMMDD, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Rec 5: Batch Header
    sTmp = "SELECT * FROM #tapBatchHeaderRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 3, 16, 20, 10, 3, 10, 6, 6, 3, 1, 8, 7, 0)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, _
                           kNumZeroPad, vLengths1, kDateYYMMDD, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Rec 6: Payments
    sTmp = "SELECT * FROM #tapDetailRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 2, 8, 1, 17, 10, 15, 22, 2, 1, 15, 0)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, _
                           kNumZeroPad, vLengths1, kDateYYMMDD, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Rec 7 is not used now
  
    'Rec 8: Batch Trailer
    sTmp = "SELECT * FROM #tapBatchControlRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 3, 6, 10, 12, 12, 10, 19, 6, 8, 7)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateYYMMDD, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Rec 9: File Trailer
    sTmp = "SELECT * FROM #tapFileControlRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 6, 6, 8, 10, 12, 12, 39)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateYYMMDD, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Nine-filler
    sTmp = "SELECT * FROM #tapBlockFillerWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    'Do not write to the file if there are no records in nine-filler worktable (#tapBlockFillerWRK)
    If oRS1.RecordCount <> 0 Then
        vLengths1 = Array(94)
        bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateYYMMDD, 2)
                           
        If Not bRetVal Then
            Close #iFile
            Kill sFileDir
            GoTo ShutDown
        End If
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    Set oExt = Nothing
    Close #iFile
    
  'Canadian AFT CPA Standard 005
  ElseIf lACHType = kACHType_CAN_CPAStd005 Then
    sFileDir = sFileDir & "\AFTBatchNo" & lBatchNo & ".ext"
    
    'Open the file
    iFile = FreeFile
    Open sFileDir For Output As iFile
    On Error GoTo ErrCloseFile
    
    'Canadian AFT CPA Standard 005 Rec A
    sTmp = "SELECT * FROM #tapCPAStd5_TypeARecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 9, 10, 4, 1, 5, 5, 20, 3, 1406)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Canadian AFT CPA Standard 005 Rec D
    sTmp = "SELECT * FROM #tapCPAStd5_TypeDRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 9, 10, 4, 3, 10, 1, 5, 1, 8, 12, 22, 3, 15, 30, 30, 10, 12, 7, 1, 8, 12, 39, 11, 1200)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, _
                           kNumZeroPad, vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
  
    'Canadian AFT CPA Standard 005 Rec Z
    sTmp = "SELECT * FROM #tapCPAStd5_TypeZRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 9, 10, 4, 14, 8, 14, 8, 44, 1352)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, _
                           kNumZeroPad, vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    'Canadian AFT Bank of Montreal
  ElseIf lACHType = kACHType_CAN_BnkMntrl Then
    sFileDir = sFileDir & "\AFTBatchNo" & lBatchNo & ".ext"
    
    'Open the file
    iFile = FreeFile
    Open sFileDir For Output As iFile
    On Error GoTo ErrCloseFile
    
    'Canadian AFT Bank of Montreal Rec A
    sTmp = "SELECT * FROM #tapBnkMntrl_TypeARecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 10, 4, 1, 5, 5, 54)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    'Canadian AFT Bank of Montreal Rec X
    sTmp = "SELECT * FROM #tapBnkMntrl_TypeXRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 1, 3, 1, 5, 15, 30, 1, 8, 12, 3)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    'Canadian AFT Bank of Montreal Rec C
    sTmp = "SELECT * FROM #tapBnkMntrl_TypeCRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 10, 1, 8, 12, 29, 12, 7)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    'Canadian AFT Bank of Montreal Rec Y
    sTmp = "SELECT * FROM #tapBnkMntrl_TypeYRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 1, 8, 14, 56)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    'Canadian AFT Bank of Montreal Rec Z
    sTmp = "SELECT * FROM #tapBnkMntrl_TypeZRecordWRK"
    Set oRS1 = New ADODB.Recordset
    oRS1.CursorLocation = adUseClient
    oRS1.Open sTmp, moCN, adOpenStatic, adLockOptimistic, adCmdText
  
    vLengths1 = Array(1, 14, 5, 14, 5, 41)
    bRetVal = oExt.DoExtract(oRS1, iFile, kFileTypeFix, , kStrJustifyLeft, kNumZeroPad, _
                           vLengths1, kDateJulian, 2)
                           
    If Not bRetVal Then
        Close #iFile
        Kill sFileDir
        GoTo ShutDown
    End If
  
    oRS1.Close
    Set oRS1 = Nothing
    
    Set oExt = Nothing
    Close #iFile
  End If
  
  ProcessACHFile = True
  Exit Function

ErrCloseFile:
  MsgBox "ProcessACHFile: " & Err.Description & ". " & sACHErr, vbOKOnly + vbExclamation, "Error " & Err.Number
  Close #iFile
  GoTo ShutDown
  
GotErr:
  MsgBox "ProcessACHFile: " & Err.Description & ". " & sACHErr, vbOKOnly + vbExclamation, "Error " & Err.Number
  
ShutDown:
  If Not oRS1 Is Nothing Then
    If oRS1.State = adStateOpen Then
      oRS1.Close
    End If
    Set oRS1 = Nothing
  End If
  
  If Not oCmd Is Nothing Then
    Set oCmd = Nothing
  End If

  If Not oExt Is Nothing Then
    Set oExt = Nothing
  End If
End Function

'Introduce ADO Connection
Private Function ConnectADO() As Boolean

  Dim sConnect As String
  Dim sBeg As String
  Dim sEnd As String
  Dim iLoc As Integer
  Dim sTmp As String
  
  ConnectADO = False
  On Error GoTo GotErr
  
'  sTmp = moClass.moSysSession.AppODBCConnect
'  sEnd = ""
'  iLoc = InStr(sTmp, "ODBC;")
'  If iLoc > 0 Then
'    sBeg = Left$(sTmp, iLoc - 1)
'    If iLoc + 5 < Len(sTmp) Then 'if anything is after "ODBC;"
'      sEnd = Right$(sTmp, Len(sTmp) - iLoc - 4)
'    End If
'  Else
'    sBeg = sTmp
'  End If
'
'  sTmp = sBeg & sEnd
'  sEnd = ""
'  iLoc = InStr(sTmp, "DSN=")
'  If iLoc > 0 Then
'    sBeg = Left$(sTmp, iLoc - 1)
'    sEnd = Mid$(sTmp, InStr(Mid$(sTmp, iLoc), ";") + 1)
'  Else
'    sBeg = sTmp
'  End If
'
'  sConnect = sBeg & sEnd
'  Set moCN = New ADODB.Connection
'
'  moCN.Provider = "SQLOLEDB"
'  moCN.Open sConnect
  
  'Open ADO connection
  Set moCN = ADOConnection(moClass.moSysSession.AppADOConnect, moClass, App.ProductName, _
                               App.Title, True, adUseClient)
  
  If miExtractType = kExtractPosPay Then
    CreatePosPayTemp
  Else
    CreateACHTemp
  End If
  
  ConnectADO = True
  Exit Function
  
GotErr:
  MsgBox "ConnectADO: " & Err.Description, vbOKOnly + vbExclamation, "Error " & Err.Number
  
End Function

'Create ACH temp tables
Private Sub CreateACHTemp()
  Dim sSQL As String
  
  On Error GoTo CreateACHTemp_ErrHandler
    
  'Create temp table for File Header Record
  sSQL = "CREATE TABLE #tapFileHeaderRecordWRK"
  sSQL = sSQL & "(RecordTypeCode char(1) NULL, "
  sSQL = sSQL & "PriorityCode Char(2) NULL, "
  sSQL = sSQL & "Blank1 char(1) NULL, "
  sSQL = sSQL & "ImmDest char(10) NULL, "
  sSQL = sSQL & "ImmOrigin char(10) NULL, "
  sSQL = sSQL & "CreateDate datetime NULL, "
  sSQL = sSQL & "CreateTimeHR char(2) NULL, "
  sSQL = sSQL & "CreateTimeMIN char(2) NULL, "
  sSQL = sSQL & "FileIDMod char(1) NULL, "
  sSQL = sSQL & "RecordSize char(3) NULL,"
  sSQL = sSQL & "BlockingFactor char(2) NULL, "
  sSQL = sSQL & "FormatCode char(1) NULL,"
  sSQL = sSQL & "ImmDestName char(23) NULL, "
  sSQL = sSQL & "ImmOriginName char(23) NULL,"
  sSQL = sSQL & "ReferenceCode char(8) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Create temp table for Batch Header Record
  sSQL = "CREATE TABLE #tapBatchHeaderRecordWRK "
  sSQL = sSQL & "(RecordTypeCode char(1) NULL, "
  sSQL = sSQL & "ServiceClassCode char(3) NULL, "
  sSQL = sSQL & "CompanyName char(16) NULL, "
  sSQL = sSQL & "CompanyDiscData char(20) NULL, "
  sSQL = sSQL & "ImmOrigin char(10) NULL, "
  sSQL = sSQL & "StandEntryClassCode char(3) NULL, "
  sSQL = sSQL & "CompanyEntryDesc char(10) NULL, "
  sSQL = sSQL & "CompanyDescDate char(6) NULL, "
  sSQL = sSQL & "EffectiveEntryDate datetime NULL, "
  sSQL = sSQL & "SettlementDate char(3) NULL, "
  sSQL = sSQL & "OriginatorStatusCode char(1) NULL, "
  sSQL = sSQL & "OriginatingDFIID char(8) NULL, "
  sSQL = sSQL & "BatchNum integer NULL, "
  sSQL = sSQL & "BatchKey integer NOT NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Create temp Table for Detail Record
  sSQL = "CREATE TABLE #tapDetailRecordWRK "
  sSQL = sSQL & "(RecordTypeCode char(1) NULL, "
  sSQL = sSQL & "TranCode integer NULL, "
  sSQL = sSQL & "RecBankTransitABANum char(8) NULL, "
  sSQL = sSQL & "RecBankCheckDigit smallint NULL, "
  sSQL = sSQL & "DFIBankAcctNum char(17) NULL, "
  sSQL = sSQL & "Amount decimal(15,3) NULL, "
  sSQL = sSQL & "IndividualIDNum char(15) NULL, "
  sSQL = sSQL & "IndividualName char(22) NULL, "
  sSQL = sSQL & "DiscretionaryData char(2) NULL, "
  sSQL = sSQL & "AddendaRecordIndicator smallint NULL, "
  sSQL = sSQL & "TraceNum char (15) NULL, "
  sSQL = sSQL & "BatchKey integer NOT NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Crete temp table for Addenda record
  sSQL = "CREATE TABLE #tapAddendaRecordWRK "
  sSQL = sSQL & "(RecordTypeCode char(1) NULL, AddendaTypeCode char(2) NULL, "
  sSQL = sSQL & "PaymentRelatedInfo char(80) NULL, AddendaSeqNum char(4) NULL, "
  sSQL = sSQL & "EntryDetailSeqNum char(7) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Create temp table for Batch Control Record
  sSQL = "CREATE TABLE #tapBatchControlRecordWRK "
  sSQL = sSQL & "(RecordTypeCode char(1) NULL, ServiceClassCode char(3) NULL, "
  sSQL = sSQL & "EntryAddendaCount integer NULL, EntryHashTotals char(10) NULL, "
  sSQL = sSQL & "TotalEntryDebit decimal(15,3) NULL, TotalEntryCredit decimal(15,3) NULL, "
  sSQL = sSQL & "ImmOrigin char(10) NULL, MessageAuthCode char(19) NULL, "
  sSQL = sSQL & "Reserved char(6) NULL, OriginatingDFIID char(10) NULL, "
  sSQL = sSQL & "BatchNum char(10) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Create temp table for File Control Record
  sSQL = "CREATE TABLE #tapFileControlRecordWRK "
  sSQL = sSQL & "(RecordTypeCode char(1) NULL, BatchCount smallint NULL, "
  sSQL = sSQL & "BlockCount smallint NULL, EntryAddendaCount smallint NULL, "
  sSQL = sSQL & "EntryHash char(10) NULL, TotalEntryDebit decimal(15,3) NULL, "
  sSQL = sSQL & "TotalEntryCredit decimal(15,3) NULL, Reserved char(39) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Create temp table for block filler
  sSQL = "CREATE TABLE #tapBlockFillerWRK "
  sSQL = sSQL & "(Nine char(94) NULL)"
  
  moCN.Execute sSQL, , adCmdText
  
  'Create Canadian AFT - CPA Standard 005 temp tables
  sSQL = "CREATE TABLE #tapCPAStd5_TypeARecordWRK"  'A Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "        '01
  sSQL = sSQL & "RecordCount smallint NULL, "       '02 Size=9
  sSQL = sSQL & "OriginatorID char(10) NULL, "      '03
  sSQL = sSQL & "FileCreationNo integer NULL, "     '04 Size=4
  sSQL = sSQL & "TypeAZero1 smallint NULL, "        '05 Size=1
  sSQL = sSQL & "CreateDate datetime NULL, "        '05 Size=5
  sSQL = sSQL & "DestinationData char(5) NULL, "    '06
  sSQL = sSQL & "TypeABlank1 char(1) NULL, "        '07 Size=20
  sSQL = sSQL & "CurrencyID char(3) NULL, "         '08
  sSQL = sSQL & "TypeABlank2 char(1) NULL) "        '09 Size=1406
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapCPAStd5_TypeDRecordWRK"      'D Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01
  sSQL = sSQL & "RecordCount smallint NULL, "           '02 Size=9
  sSQL = sSQL & "OriginatorID1 char(10) NULL, "         '03
  sSQL = sSQL & "FileCreationNo integer NULL, "         '03 Size=4
  sSQL = sSQL & "TranType char(3) NULL, "               '04
  sSQL = sSQL & "Amount decimal(15,3) NULL, "           '05 Size=10
  sSQL = sSQL & "TypeDZero1 smallint NULL, "            '06 Size=1
  sSQL = sSQL & "DueDate datetime NULL, "               '06 Size=5
  sSQL = sSQL & "TypeDZero2 smallint NULL, "            '07 Size=1
  sSQL = sSQL & "InstitutionalIDNo char(8) NULL, "      '07 MUST BE ZERO FILLED RIGHT JUSIFIED
  sSQL = sSQL & "AccountNumber char(12) NULL, "         '08
  sSQL = sSQL & "TypeDBlank1 char(22) NULL, "            '09 Size=22
  sSQL = sSQL & "TypeDZero3 smallint NULL, "            '10 Size=3
  sSQL = sSQL & "OriginatorShrtName char(15) NULL, "    '11
  sSQL = sSQL & "Name char(30) NULL, "                  '12
  sSQL = sSQL & "OriginatorLngName char(30) NULL, "     '13
  sSQL = sSQL & "OriginatorID2 char(10) NULL, "         '14
  sSQL = sSQL & "OriginatorCrossRef char(12) NULL, "    '15
  sSQL = sSQL & "TypeDBlank2 char(1) NULL, "            '15 Size=7
  sSQL = sSQL & "TypeDZero4 smallint NULL, "            '16 Size=1
  sSQL = sSQL & "InstitutionalIDNoRtrn char(8) NULL, "  '16 MUST BE ZERO FILLED RIGHT JUSIFIED
  sSQL = sSQL & "AccountNumberRtrn char(12), "          '17
  sSQL = sSQL & "TypeDBlank3 char(1) NULL, "            '18,19,20 Size=39
  sSQL = sSQL & "TypeDZero5 smallint NULL, "            '21 Size=11
  sSQL = sSQL & "TypeDBlank4 char(1) NULL) "            '22 Size=1200
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapCPAStd5_TypeZRecordWRK"      'Z Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01
  sSQL = sSQL & "RecordCount smallint NULL, "           '02 Size=9
  sSQL = sSQL & "OriginatorID char(10) NULL, "          '03
  sSQL = sSQL & "FileCreationNo integer NULL, "         '03 Size=4
  sSQL = sSQL & "TotalDebitAmt decimal(15,3) NULL, "    '04 Size=14
  sSQL = sSQL & "TotalNoOfDebit smallint NULL, "        '05 Size=8
  sSQL = sSQL & "TotalCreditAmt decimal(15,3) NULL, "   '06 Size=14
  sSQL = sSQL & "TotalNoOfCredit smallint NULL, "       '07 Size=8
  sSQL = sSQL & "TypeZZero1 smallint NULL, "            '08,09,10,11 Size=44
  sSQL = sSQL & "TypeZBlank1 char(1) NULL) "            '12 Size=1352
  
  moCN.Execute sSQL, , adCmdText
  
  'Create Canadian AFT - Bank of Montreal temp tables
  sSQL = "CREATE TABLE #tapBnkMntrl_TypeARecordWRK"     'A Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01 Size=1
  sSQL = sSQL & "OriginatorID char(10) NULL, "          '02 Size=10
  sSQL = sSQL & "FileCreationNo integer NULL, "         '03 Size=4
  sSQL = sSQL & "TypeAZero1 smallint NULL, "            '04 Size=1
  sSQL = sSQL & "CreateDate datetime NULL, "            '04 Size=5
  sSQL = sSQL & "DestinationData char(5) NULL, "        '05 Size=5
  sSQL = sSQL & "TypeABlank1 char(1) NULL) "            '06 Size=54
   
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapBnkMntrl_TypeXRecordWRK"     'X Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01 Size=1
  sSQL = sSQL & "BatchPmtType char(1) NULL, "           '02 Size=1
  sSQL = sSQL & "TranType char(3) NULL, "               '03 Size=3
  sSQL = sSQL & "TypeXZero1 smallint NULL, "            '04 Size=1
  sSQL = sSQL & "CreateDate datetime NULL, "            '04 Size=5
  sSQL = sSQL & "OriginatorShrtName char(15) NULL, "    '05 Size=15
  sSQL = sSQL & "OriginatorLngName char(30) NULL, "     '06 Size=30
  sSQL = sSQL & "TypeXZero2 smallint NULL, "            '07 Size=1
  sSQL = sSQL & "InstitutionalIDNoRtrn char(8) NULL, "  '07 MUST BE ZERO FILLED RIGHT JUSIFIED
  sSQL = sSQL & "AccountNumberRtrn char(12), "          '08 Size=12
  sSQL = sSQL & "TypeXBlank1 char(1) NULL) "            '09 Size=3
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapBnkMntrl_TypeCRecordWRK"     'C Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01 Size=1
  sSQL = sSQL & "Amount decimal(15,3) NULL, "           '02 Size=10
  sSQL = sSQL & "TypeCZero1 smallint NULL, "            '03 Size=1
  sSQL = sSQL & "InstitutionalIDNo char(8) NULL, "      '03 MUST BE ZERO FILLED RIGHT JUSIFIED
  sSQL = sSQL & "AccountNumber char(12) NULL, "         '04 Size=12
  sSQL = sSQL & "Name char(29) NULL, "                  '05 Size=29
  sSQL = sSQL & "OriginatorCrossRef char(12) NULL, "    '06 Size=12
  sSQL = sSQL & "TypeCBlank1 char(1) NULL) "            '06 Size=7
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapBnkMntrl_TypeYRecordWRK"     'Y Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01 Size=1
  sSQL = sSQL & "BatchPmtType char(1) NULL, "           '02 Size=1
  sSQL = sSQL & "BatchRecordCount smallint NULL, "      '03 Size=8
  sSQL = sSQL & "BatchAmt decimal(15,3) NULL, "         '04 Size=14
  sSQL = sSQL & "TypeYBlank1 char(1) NULL) "            '05 Size=56
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "CREATE TABLE #tapBnkMntrl_TypeZRecordWRK"     'Z Record Data Element
  sSQL = sSQL & "(RecordType char(1) NULL, "            '01 Size=1
  sSQL = sSQL & "TotalDebitAmt decimal(15,3) NULL, "    '02 Size=14
  sSQL = sSQL & "TotalNoOfDebit smallint NULL, "        '03 Size=5
  sSQL = sSQL & "TotalCreditAmt decimal(15,3) NULL, "   '04 Size=14
  sSQL = sSQL & "TotalNoOfCredit smallint NULL, "       '05 Size=5
  sSQL = sSQL & "TypeZBlank1 char(1) NULL) "            '06 Size=41
  
  moCN.Execute sSQL, , adCmdText
  
  Exit Sub
  
CreateACHTemp_ErrHandler:
    MsgBox "The following error occurred in CreateACHTemp: " & _
    vbCrLf & "Error Number: " & Err.Number & _
    vbCrLf & "Error Description: " & Err.Description, _
    vbExclamation + vbOKOnly, Me.Caption
End Sub

'Clean up and Truncate ACH temp tables
Private Sub TruncateACHTemp()
  Dim sSQL As String
  
  On Error GoTo TruncateACHTemp_ErrHandler
    
  sSQL = "TRUNCATE TABLE #tapFileHeaderRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapBatchHeaderRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapDetailRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapAddendaRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapBatchControlRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapFileControlRecordWRK" & vbCrLf
  
  moCN.Execute sSQL, , adCmdText
  
  'Truncate Canadian ACH/AFT temp tables
  sSQL = "TRUNCATE TABLE #tapCPAStd5_TypeARecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapCPAStd5_TypeDRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapCPAStd5_TypeZRecordWRK"
  
  moCN.Execute sSQL, , adCmdText
  
  sSQL = "TRUNCATE TABLE #tapBnkMntrl_TypeARecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapBnkMntrl_TypeXRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapBnkMntrl_TypeCRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapBnkMntrl_TypeYRecordWRK" & vbCrLf
  sSQL = sSQL & "TRUNCATE TABLE #tapBnkMntrl_TypeZRecordWRK"
  
  moCN.Execute sSQL, , adCmdText
  
  Exit Sub
  
TruncateACHTemp_ErrHandler:
    MsgBox "The following error occurred in TruncateACHTemp: " & _
    vbCrLf & "Error Number: " & Err.Number & _
    vbCrLf & "Error Description: " & Err.Description, _
    vbExclamation + vbOKOnly, Me.Caption
End Sub

'Look to see if the property is set, if not read it from the registry.
'If the registry turns up blank the user will be warned down the road
'before posting occurs in the bDoPost routine.
Private Sub IsFileDirSet()
    Dim sKeyString As String
    Dim fso As Object   'SGS-JMM 4/25/2011
    
    If miExtractType = kExtractACH Then
        sKeyString = "ACH"
    ElseIf miExtractType = kExtractPosPay Then
        sKeyString = "PositivePay"
    End If

    On Error GoTo IsFileDirSet_ErrHandler
    
    If IsNull(frmProperties.sFileExtractDir) Or Len(Trim(frmProperties.sFileExtractDir)) = 0 Then
        'SGS-JMM 4/25/2011
        frmProperties.sFileExtractDir = GetDefaultPosPayExportDir
        If Len(Trim(frmProperties.sFileExtractDir)) <= 0 Then
            MsgBox "Warning: the positive pay export directory could not be retrieved from the database.", vbExclamation, Me.Caption
            frmProperties.sFileExtractDir = GetSetting(sKeyString, "DefaultSettings", "Dir", "")
        Else
            Set fso = CreateObject("Scripting.FileSystemObject")
            If Not fso.FolderExists(frmProperties.sFileExtractDir) Then
                MsgBox "Warning: the positive pay export directory " & vbCrLf & vbCrLf & frmProperties.sFileExtractDir & vbCrLf & vbCrLf & " is not accessible.", vbExclamation, Me.Caption
            End If
            Set fso = Nothing
        End If
        'End SGS-JMM
        
    End If
    If Len(Trim(frmProperties.sFileExtractDir)) > 1 Then
        frmProperties.bProcessExtract = True
    Else
        frmProperties.bProcessExtract = False
    End If
    
    Exit Sub
  
IsFileDirSet_ErrHandler:
    MsgBox "The following error occurred in IsFileDirSet: " & _
    vbCrLf & "Error Number: " & Err.Number & _
    vbCrLf & "Error Description: " & Err.Description, _
    vbExclamation + vbOKOnly, Me.Caption
End Sub


'Validate EFT Payment Method is turned on
Private Function bValidateEFT() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lBatchVendPmtMethKey As Long
    Dim lEFTVendPmtMethKey As Long
    
    bValidateEFT = False
       
    lEFTVendPmtMethKey = glGetValidLong(moClass.moAppDB.Lookup("VendPmtMethKey", "tapVendPmtMethod WITH (NOLOCK)", "TranType = 416 AND CompanyID = " & gsQuoted(msCompanyID)))
    
    If lEFTVendPmtMethKey <> 0 Then
      lBatchVendPmtMethKey = glGetValidLong(moClass.moAppDB.Lookup("VendPmtMethKey", "tapBatch WITH (NOLOCK)", "BatchKey =" & mlBatchKey))
      If lBatchVendPmtMethKey <> 0 Then
        If lEFTVendPmtMethKey = lBatchVendPmtMethKey Then
          bValidateEFT = True
        End If
      End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidateEFT", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'Validate both Bank Routing Transit No and Bank Account No Exist
Private Function bValidateRemitTo() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs As Object
    Dim bValid As Boolean
    Dim sVendID As String
    Dim lRemitToAddrKey As Long
    Dim sBankRoutingTrnstNo As String
    Dim sBankAcctNo As String
    Dim iLen As Integer
    Dim J As Integer
    Dim sChr As String
    
    bValidateRemitTo = False
    
    sSQL = "SELECT DISTINCT tapVendor.VendID, tapVendor.DfltRemitToAddrKey "
    sSQL = sSQL & "FROM tapPendVendPmt WITH (NOLOCK) INNER JOIN tapVendor WITH (NOLOCK) "
    sSQL = sSQL & "ON tapPendVendPmt.VendKey = tapVendor.VendKey "
    sSQL = sSQL & "AND tapPendVendPmt.BatchKey = " & Str(mlBatchKey)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    While Not rs.IsEOF
      sVendID = rs.Field("VendID")
      lRemitToAddrKey = glGetValidLong(rs.Field("DfltRemitToAddrKey"))
      
      If lRemitToAddrKey <> 0 Then
        bValid = False
        sBankRoutingTrnstNo = gsGetValidStr(moClass.moAppDB.Lookup("BankRoutingTrnstNo", "tapVendAddr WITH (NOLOCK)", "AddrKey = " & lRemitToAddrKey))
      
        If Not IsNull(sBankRoutingTrnstNo) Then
          sBankRoutingTrnstNo = Trim(sBankRoutingTrnstNo)
          iLen = Len(sBankRoutingTrnstNo)
          If iLen <> 0 Then
            bValid = True
          End If
        End If
        
        If Not bValid Then
          giSotaMsgBox Me, moFrmSysSession, kmsgAPACHBlankRouting, sVendID
          GoTo Invalid_RemitTo
        End If
        
        bValid = True 'Reinitialize variable to True this time
        
        For J = 1 To iLen
          sChr = Mid(sBankRoutingTrnstNo, J, 1)
            
          If Not (UCase(sChr) = LCase(sChr)) Then 'Check for Apha charater
          Else
            If Not IsNumeric(sChr) Then 'Check for Numeric
                bValid = False
                Exit For
            End If
          End If
        Next J
        
        If Not bValid Then
          giSotaMsgBox Me, moFrmSysSession, kmsgAPACHAlphaNumRouting, sVendID, sBankRoutingTrnstNo
          GoTo Invalid_RemitTo
        End If
        
        bValid = False
        sBankAcctNo = gsGetValidStr(moClass.moAppDB.Lookup("BankAcctNo", "tapVendAddr WITH (NOLOCK)", "AddrKey = " & lRemitToAddrKey))
        
        If Not IsNull(sBankAcctNo) Then
          If Len(Trim(sBankAcctNo)) <> 0 Then
            bValid = True
          End If
        End If
        
        If Not bValid Then
          giSotaMsgBox Me, moFrmSysSession, kmsgAPACHBlankAcct, sVendID
          GoTo Invalid_RemitTo
        End If
      Else
        GoTo Invalid_RemitTo
      End If
      
      rs.MoveNext
    Wend
    
    bValidateRemitTo = True
    
Invalid_RemitTo:
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidateRemitTo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SetPosPayState()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If chkPosPay.Value = vbChecked Then
        
        'SGS-JMM 4/26/2011
        If PosPayAllowed Then
            mbDoPosPay = True
            gEnableControls cmdPosPayProperties
            IsFileDirSet
        Else
            chkPosPay.Value = vbUnchecked
            MsgBox "You are not authorized to export positive pay files.", vbOKOnly + vbInformation, Me.Caption
        End If  'End SGS-JMM
    Else
        mbDoPosPay = False
        gDisableControls cmdPosPayProperties
    End If
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPosPayState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bValidateACHPrenote() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs As Object
    Dim iACHTranCode As Integer
    Dim iPrenoteCount As Integer
    Dim iLiveCount As Integer
    
    bValidateACHPrenote = False
    
    'In order to continue processing the batch must be either ALL live checks or ALL prenotification
    sSQL = "SELECT DISTINCT tapVendor.ACHTranCode "
    sSQL = sSQL & "FROM tapPendVendPmt WITH (NOLOCK) INNER JOIN tapVendor WITH (NOLOCK) "
    sSQL = sSQL & "ON tapPendVendPmt.VendKey = tapVendor.VendKey "
    sSQL = sSQL & "AND tapPendVendPmt.BatchKey = " & Str(mlBatchKey)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    iPrenoteCount = 0
    iLiveCount = 0
    
    While Not rs.IsEOF
        iACHTranCode = giGetValidInt(rs.Field("ACHTranCode"))
              
        'Check for Prenotification ACH Tran Code
        '23 - Checking Credit Prenotification
        '28 - Checking Debit Prenotification
        '33 - Savings Credit Prenotification
        '38 - Savings Debit Prenotification
        
        If iACHTranCode = 23 Or iACHTranCode = 28 Or iACHTranCode = 33 Or iACHTranCode = 38 Then
            iPrenoteCount = iPrenoteCount + 1
            mbPrenote = True
        Else
           iLiveCount = iLiveCount + 1
        End If
        
        rs.MoveNext
    Wend
    
    If iPrenoteCount > 0 And iLiveCount > 0 Then
        'Batch contains both prenote and live checks, validation should fail here
        Exit Function
    Else
        bValidateACHPrenote = True
    End If
    
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidateACHPrenote", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'SGS-JMM 1/31/2011
Public Function GetCypressQuery() As String
    Dim batchID As String
    
    On Error Resume Next
    batchID = moClass.moSysSession.AppDatabase.Lookup("BatchID", "tciBatchLog", "BatchKey = " & Me.CONTROLS(0).Parent.BatchKey)
    GetCypressQuery = "[Company ID] = """ & moClass.moSysSession.CompanyID & """ and [Batch ID] = """ & batchID & """"
End Function

Private Function GetDefaultPosPayExportDir() As String
    Dim retVal As String
    
    retVal = gsGetValidStr(moClass.moAppDB.Lookup("ExportDir", "tapPosPayExpDir_SGS", "CompanyID = " & gsQuoted(msCompanyID)))
    If Right(retVal, 1) = "\" Then
        retVal = Left(retVal, Len(retVal) - 1)
    End If
    
    GetDefaultPosPayExportDir = retVal
End Function

Private Function PosPayAllowed() As Boolean
    Dim prmpt As Variant
    Dim evtID As String
    Dim evtUser As String
    
    prmpt = False
    evtID = "APPOSPAYEXP_SGS"
    evtUser = CStr(moClass.moSysSession.UserId)
        
    PosPayAllowed = CBool(moClass.moFramework.GetSecurityEventPerm(evtID, evtUser, prmpt))
End Function
'End SGS-JMM


