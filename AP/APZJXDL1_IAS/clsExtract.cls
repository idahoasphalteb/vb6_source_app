VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsExtract"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private msNumFiller As String
Private Const kbParseString As Boolean = True

Public Function DoExtract(oRecordset As ADODB.Recordset, _
                iFileNum As Integer, iFileType As Integer, _
                Optional sDelimChar As String, _
                Optional iStrType As Integer, _
                Optional iNumType As Integer, _
                Optional vLengths As Variant, _
                Optional iDateType As Integer = kDateYYMMDD, _
                Optional iImpliedDec As Integer = 0) As Boolean
  
  'oRecordset contains the recordset to extract
  'iFileNum is the file handle of the open extract file
  'iFileType is either kFileTypeDel or kFileTypeFix (delimited or fixed-len)
  'sDelimChar is the delimiter character if type delim
  'iStrType is either kStrJustifyLeft or kStrJustifyRight if fixed-len
  'iNumType is either kNumZeroPad or kNumJustifyRight if fixed-len
  'vLengths is an array of field lengths representing the
  '  lengths of each field in the order they appear in the recordset
  'iDateType is either kDateYYMMDD for standard date format or kDateMMDDYYYY or kDateMMDDYY format
  'iImpliedDec is the number of implied decimal places (or 0 for none).  Example:
  '  Set to 2, $147.50 (padded to 10 chars) would be formatted to 0000014750.
  '  Set to 4, $147.50 (padded to 10 chars) would be formatted to 0001475000.
  '  Set to 0, $147.50 (padded to 10 chars) would be formatted to 0000147.50.
  
  Dim iCols As Integer
  Dim J As Integer
  Dim sStr As String
  
  DoExtract = False
  On Error GoTo ExtErr
  
  'If delimited, sDelimChar must exist.  If fixed then iStrType,
  ' iNumType, and vLengths must exist.
  If iFileType = kFileTypeDel Then
    If Len(sDelimChar) < 1 Then
      sStr = "Delimited File:  Delimiter Character must be set"
      GoTo BadStuff
    End If
  ElseIf iFileType = kFileTypeFix Then
    If iStrType <> kStrJustifyLeft And iStrType <> kStrJustifyRight Then
      sStr = "Fixed-length File:  String Type must be set"
      GoTo BadStuff
    ElseIf iNumType <> kNumZeroPad And iNumType <> kNumJustifyRight Then
      sStr = "Fixed-length File:  Number Type must be set"
      GoTo BadStuff
    ElseIf Not IsArray(vLengths) Then
      sStr = "Fixed-length File:  Lengths array is required"
      GoTo BadStuff
    End If
  Else 'bad filetype
    sStr = "Invalid FileType"
    GoTo BadStuff
  End If
  
  'Set Number filler based on iNumType
  If iNumType = kNumZeroPad Then
    msNumFiller = "0"
  Else
    msNumFiller = " "
  End If
  
  'see if we have any records
  If (oRecordset.EOF And oRecordset.BOF) Then Exit Function
  
  oRecordset.MoveFirst
  iCols = oRecordset.Fields.Count
  
  While Not oRecordset.EOF
    sStr = ""
    For J = 0 To iCols - 1
      If iFileType = kFileTypeDel Then  'Delimited file
        sStr = sStr & FixNull(oRecordset.Fields(J).Value, oRecordset.Fields(J).Type, _
                              iDateType, iImpliedDec)
        If J < iCols - 1 Then sStr = sStr & sDelimChar
      Else 'Fixed-length file
        If Not bValidateAmt(oRecordset.Fields(J).Value, vLengths(J), _
            oRecordset.Fields(J).Type, iImpliedDec, oRecordset.Fields(J).Name) Then
                Exit Function
        End If
      
        'Right Justify Immediate Origin field
        If oRecordset.Fields(J).Name = "ImmOrigin" Then
          sStr = sStr & Right("          " & Trim(CStr(oRecordset.Fields(J).Value)), vLengths(J))
        Else
          sStr = sStr & FormatField(oRecordset.Fields(J).Value, _
                vLengths(J), oRecordset.Fields(J).Type, iStrType, iDateType, iImpliedDec)
        End If
      End If
    Next J
    
    Print #iFileNum, sStr
    oRecordset.MoveNext
  Wend
  
  DoExtract = True
  
  Exit Function
  
ExtErr:
  
  MsgBox Err.Description, vbOKOnly + vbExclamation, "Error " & Err.Number
  Exit Function
  
BadStuff:
  MsgBox sStr, vbOKOnly + vbExclamation, "Error"
  Exit Function
  
End Function

Private Function FormatField(vValue As Variant, ByVal iLen As Integer, _
                   iType As ADODB.DataTypeEnum, iStrType As Integer, _
                   iDateType As Integer, iImpliedDec As Integer, Optional bParseString As Boolean = False) As String
  Dim sFill As String
  Dim sTmp As String
  Dim iCurrLen As Integer
  
  Select Case iType
    Case adBigInt, adBinary, adInteger, adLongVarBinary, _
         adUnsignedBigInt, adUnsignedInt, adVarBinary, adSmallInt, _
         adTinyInt, adUnsignedSmallInt, adUnsignedTinyInt, _
         adBoolean, adCurrency, adDecimal, adDouble, adNumeric, _
         adSingle, adVarNumeric, adCurrency
    
      sFill = Rep(msNumFiller, iLen)
      
      'Strip out "-" sign
      iCurrLen = Len(Trim(FixNull(vValue, iType, iDateType, iImpliedDec)))
      
      If Left(Trim(FixNull(vValue, iType, iDateType, iImpliedDec)), 1) = "-" Then
        sTmp = msNumFiller & sFill & Right(Trim(FixNull(vValue, iType, iDateType, iImpliedDec)), (iCurrLen - 1))
      Else
        sTmp = sFill & Trim(FixNull(vValue, iType, iDateType, iImpliedDec))
      End If
      
      sTmp = Right$(sTmp, iLen)
      
    Case Else
      
      If Not IsNull(vValue) Then
        sTmp = vValue
        If bParseString Then
            sTmp = Discard_Dashes(sTmp)
            sFill = Rep(msNumFiller, iLen)
        Else
            sFill = Rep(" ", iLen)
        End If
        If iStrType = kStrJustifyLeft Then
            sTmp = FixNull(sTmp, iType, iDateType, iImpliedDec) & sFill
            sTmp = Left$(sTmp, iLen)
        Else
            sTmp = sFill & FixNull(sTmp, iType, iDateType, iImpliedDec)
            sTmp = Right$(sTmp, iLen)
        End If
      Else
        sFill = Rep(" ", iLen)
        sTmp = FixNull(sTmp, iType, iDateType, iImpliedDec) & sFill
        sTmp = Right$(sTmp, iLen)
      End If

  End Select
               
  FormatField = sTmp
                   
End Function

Private Function Discard_Dashes(sStr As String) As String
    Dim J As Integer
    Dim sTmp As String
    Dim sChr As String
    Dim iNewLen As Integer
    
    sTmp = ""
    iNewLen = Len(sStr)
    For J = 1 To iNewLen
        sChr = Mid(sStr, J, 1)
        If sChr <> "-" Then
            sTmp = sTmp + sChr
        Else
            If iNewLen < Len(sStr) Then
                iNewLen = iNewLen + 1
            End If
        End If
    Next J
    
    Discard_Dashes = sTmp
End Function

Private Function Rep(sStr As String, iLen As Integer) As String
  Dim J As Integer
  Dim sTmp As String
  
  sTmp = ""
  If iLen < 1 Then Exit Function
  If Len(sStr) <> 1 Then Exit Function
  
  For J = 1 To iLen
    sTmp = sTmp & sStr
  Next J
  
  Rep = sTmp
  
End Function

Private Function FixNull(vValue As Variant, _
                         iType As ADODB.DataTypeEnum, _
                         iDateType As Integer, iImpliedDec As Integer) As String
  Dim sStr As String
  Dim fTmp As Double
  Dim dTmp As Double
  
  Select Case iType
    Case adBigInt, adBinary, adInteger, adLongVarBinary, _
         adUnsignedBigInt, adUnsignedInt, adVarBinary, adSmallInt, _
         adTinyInt, adUnsignedSmallInt, adUnsignedTinyInt, _
         adBoolean
      
      If IsNull(vValue) Then
        sStr = "0"
      Else
        sStr = Trim(CStr(vValue))
      End If
      
    Case adCurrency, adDecimal, adDouble, adNumeric, _
         adSingle, adVarNumeric, adCurrency
      
      If IsNull(vValue) Then
        sStr = "0"
      Else
        If IsNumeric(vValue) And iImpliedDec > 0 Then
          fTmp = CDbl(vValue)
          fTmp = fTmp * (10 ^ iImpliedDec)
          dTmp = CDbl(fTmp)
          sStr = Trim(CStr(dTmp))
        Else
          sStr = Trim(CStr(vValue))
        End If
      End If
      
    Case adDate, adDBDate, adDBTime, adDBTimeStamp, adFileTime 'adDBFileTime
         
      If IsNull(vValue) Then
        sStr = ""
      Else
        Select Case iDateType
          Case kDateYYMMDD
            sStr = Format(vValue, "YYMMDD")
          Case kDateYYYYMMDD
            sStr = Format(vValue, "YYYYMMDD")
          Case kDateMMDDYY
            sStr = Format(vValue, "MMDDYY")
          Case kDateMMDDYYYY
            sStr = Format(vValue, "MMDDYYYY")
          Case kDateJulian
            sStr = ConvertToJulian(CDate(vValue))
          Case Else
            sStr = Trim(CStr(vValue))
        End Select
      End If
     
    Case Else
      If IsNull(vValue) Then
        sStr = ""
      Else
        sStr = Trim(CStr(vValue))
      End If
      
  End Select
  
  FixNull = sStr
  
End Function


Private Function ConvertToJulian(dteNormalDate As Date) As String
    Dim DateYear As String      'The year of the serial date.
    Dim JulianDay As String
    Dim JulianDate As String    'The converted Julian date value

    'Assign DateYear the year number
    DateYear = Format(dteNormalDate, "yy")

    'Find the day number for NormalDate
    JulianDay = Format(Str(dteNormalDate - DateValue("1/1/" & _
    Str(DateYear)) + 1), "000")

    'Combine the year and day to get the value for JulianDate.
    ConvertToJulian = DateYear & JulianDay
End Function


Private Function bValidateAmt(vValue As Variant, ByVal iLen As Integer, iType As ADODB.DataTypeEnum, _
                            iImpliedDec As Integer, sName As String) As Boolean
                            
    'Validate all amount fields to ensure that the numerical data does not get truncated. For example, if the amount field
    'contains 10 digits and the extract file was only alloted to accept 9 digits and then display error message
                            
    Dim sStr As String
    Dim dAmt As Double
    
    bValidateAmt = True
                            
    Select Case iType
        Case adCurrency, adDecimal, adDouble, adNumeric, adSingle, adVarNumeric, adCurrency
            dAmt = Abs(CDbl(vValue))
        
            If iImpliedDec > 0 Then
                sStr = Trim(CStr(dAmt * (10 ^ iImpliedDec)))
            Else
                sStr = Trim(dAmt)
            End If
            
            If iLen < Len(sStr) Then
                bValidateAmt = False
                GoTo AmtExceedErr
            End If
    End Select
    
    Exit Function
    
AmtExceedErr:
    MsgBox "The file cannot be generated and the batch cannot be posted because the " & sName _
        & " field contains a value of " & sStr & " that exceeds the predefined field length of " & CStr(iLen) _
        & " spaces.", vbOKOnly + vbExclamation, "Sage 500 ERP"
End Function




