Attribute VB_Name = "basDupPayments"
Option Explicit


Const VBRIG_MODULE_ID_STRING = "apzxx001_SGS.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog




Public Enum ReportVrsn
    InvcDtVendAmt = 1
    InvcDtVend = 2
    DtVendAmt = 3
    InvcDtAmt = 4
End Enum
Function GetSelect(RptVrn As ReportVrsn, sWhere As String, sSelect As String, iSessionID As Integer) As Boolean
    On Error GoTo Error
    
    Select Case RptVrn
        Case ReportVrsn.InvcDtVendAmt
            sSelect = "Select distinct " & iSessionID & " 'SessionID', v1.*, IsNull(MinAmt,0) DupAmt, Case When MinVoucherKey is null Then 0 Else 1 End DupCount "
            sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
            sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
            sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
            sSelect = sSelect & "    And v1.VendInvcNo = v2.VendInvcNo "
            sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
            sSelect = sSelect & "    And v1.VendID = v2.VendID "
            sSelect = sSelect & "    And v1.Amount = v2.Amount "
            
                'Sub Query Start
                sSelect = sSelect & "Left Outer Join ( "
                sSelect = sSelect & "Select distinct v1.VendInvcNo, v1.InvcDate, v1.VendID, v1.Amount, Min(v1.VoucherKey) MinVoucherKey, Min(IsNull(v1.Amount,0)) MinAmt "
                sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
                sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
                sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
                sSelect = sSelect & "    And v1.VendInvcNo = v2.VendInvcNo "
                sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
                sSelect = sSelect & "    And v1.VendID = v2.VendID "
                sSelect = sSelect & "    And v1.Amount = v2.Amount "
                sSelect = sSelect & sWhere
                sSelect = sSelect & " Group By v1.VendInvcNo, v1.InvcDate, v1.VendID, v1.Amount "
                sSelect = sSelect & ") t On v1.VoucherKey = t.MinVoucherKey  "
                'Sub Query End
            
            sSelect = sSelect & sWhere
            sSelect = sSelect & "Order By v1.VendInvcNo, v1.InvcDate,  v1.VendID, v1.Amount "
            
        Case ReportVrsn.InvcDtVend
            sSelect = "Select distinct " & iSessionID & " 'SessionID', v1.*, IsNull(MinAmt,0) DupAmt, Case When MinVoucherKey is null Then 0 Else 1 End DupCount "
            sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
            sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
            sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
            sSelect = sSelect & "    And v1.VendInvcNo = v2.VendInvcNo "
            sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
            sSelect = sSelect & "    And v1.VendID = v2.VendID "
            sSelect = sSelect & "    And v1.Amount <> v2.Amount "
            
                'Sub Query Start
                sSelect = sSelect & "Left Outer Join ( "
                sSelect = sSelect & "Select distinct v1.VendInvcNo, v1.InvcDate, v1.VendID, Min(v1.VoucherKey) MinVoucherKey, Min(IsNull(v1.Amount,0)) MinAmt "
                sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
                sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
                sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
                sSelect = sSelect & "    And v1.VendInvcNo = v2.VendInvcNo "
                sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
                sSelect = sSelect & "    And v1.VendID = v2.VendID "
                sSelect = sSelect & "    And v1.Amount <> v2.Amount "
                sSelect = sSelect & sWhere
                sSelect = sSelect & " Group By v1.VendInvcNo, v1.InvcDate, v1.VendID "
                sSelect = sSelect & ") t On v1.VoucherKey = t.MinVoucherKey  "
                'Sub Query End
            
            sSelect = sSelect & sWhere
            sSelect = sSelect & "Order By v1.VendInvcNo, v1.InvcDate,  v1.VendID "
        
        Case ReportVrsn.DtVendAmt
            sSelect = "Select distinct " & iSessionID & " 'SessionID', v1.*, IsNull(MinAmt,0) DupAmt, Case When MinVoucherKey is null Then 0 Else 1 End DupCount "
            sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
            sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
            sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
            sSelect = sSelect & "    And v1.VendInvcNo <> v2.VendInvcNo "
            sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
            sSelect = sSelect & "    And v1.VendID = v2.VendID "
            sSelect = sSelect & "    And v1.Amount = v2.Amount "
            
                'Sub Query Start
                sSelect = sSelect & "Left Outer Join ( "
                sSelect = sSelect & "Select distinct v1.InvcDate, v1.VendID, v1.Amount, Min(v1.VoucherKey) MinVoucherKey, Min(IsNull(v1.Amount,0)) MinAmt "
                sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
                sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
                sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
                sSelect = sSelect & "    And v1.VendInvcNo <> v2.VendInvcNo "
                sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
                sSelect = sSelect & "    And v1.VendID = v2.VendID "
                sSelect = sSelect & "    And v1.Amount = v2.Amount "
                sSelect = sSelect & sWhere
                sSelect = sSelect & " Group By v1.InvcDate, v1.VendID, v1.Amount "
                sSelect = sSelect & ") t On v1.VoucherKey = t.MinVoucherKey  "
                'Sub Query End
            
            sSelect = sSelect & sWhere
            sSelect = sSelect & "Order By v1.InvcDate,  v1.VendID, v1.Amount "
        
        Case ReportVrsn.InvcDtAmt
            sSelect = "Select distinct " & iSessionID & " 'SessionID', v1.*, IsNull(MinAmt,0) DupAmt, Case When MinVoucherKey is null Then 0 Else 1 End DupCount "
            sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
            sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
            sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
            sSelect = sSelect & "    And v1.VendInvcNo = v2.VendInvcNo "
            sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
            sSelect = sSelect & "    And v1.VendID <> v2.VendID "
            sSelect = sSelect & "    And v1.Amount = v2.Amount "
            
                'Sub Query Start
                sSelect = sSelect & "Left Outer Join ( "
                sSelect = sSelect & "Select distinct v1.VendInvcNo, v1.InvcDate, v1.Amount, Min(v1.VoucherKey) MinVoucherKey, Min(IsNull(v1.Amount,0)) MinAmt "
                sSelect = sSelect & "From vluDupPayments_SGS v1 with(Nolock) "
                sSelect = sSelect & "Inner Join vluDupPayments_SGS v2 with(Nolock) "
                sSelect = sSelect & "    On v1.VoucherKey <> v2.VoucherKey "
                sSelect = sSelect & "    And v1.VendInvcNo = v2.VendInvcNo "
                sSelect = sSelect & "    And v1.InvcDate = v2.InvcDate "
                sSelect = sSelect & "    And v1.VendID <> v2.VendID "
                sSelect = sSelect & "    And v1.Amount = v2.Amount "
                sSelect = sSelect & sWhere
                sSelect = sSelect & " Group By v1.VendInvcNo, v1.InvcDate, v1.Amount "
                sSelect = sSelect & ") t On v1.VoucherKey = t.MinVoucherKey  "
                'Sub Query End
            
            sSelect = sSelect & sWhere
            sSelect = sSelect & "Order By v1.VendInvcNo, v1.InvcDate, v1.Amount "
                    
    End Select
    
    GetSelect = True
    
    Exit Function
Error:
    MsgBox "The following error occured obtaining report data: " & vbCrLf & vbCrLf & _
    "basDupPayments.GetSelect(ReporVrsn)" & vbCrLf & _
    "Error: " & Err.Number & Err.Description, vbExclamation, "MAS 500"
End Function

Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("apzxx001_SGS.clsDupPayments")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basDupPayments"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant, Optional RptVrsn As ReportVrsn = InvcDtVendAmt, Optional bAllCompanies As Boolean = True) As Long
    
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetval As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    Dim SortObj As clsSort
    Dim DBObj As Object
    Dim SettingsObj As clsSettings

    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    'Append criteria to restrict data returned to the current company.
'    SelectObj.AppendToWhereClause sWhereClause, "tapDupPayments_SGS.CompanyID=" & gsQuoted(frm.msCompanyID)
    If bAllCompanies = False Then
        SelectObj.AppendToWhereClause sWhereClause, "v1.CompanyID=" & gsQuoted(frm.msCompanyID)
        SelectObj.AppendToWhereClause sWhereClause, "v2.CompanyID=" & gsQuoted(frm.msCompanyID)
    End If
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"

    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
        GoTo badexit
    End If

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT tapDupPayments_SGS., " & ReportObj.SessionID & " FROM " & sTablesUsed
    
    If GetSelect(RptVrsn, sWhereClause, sSelect, ReportObj.SessionID) = False Then
        Exit Function
    End If
    
    #If RPTDEBUG Then
        sInsert = "INSERT INTO tapDupPaymentsWrk_SGS  " & sSelect
    #Else
        sInsert = "INSERT INTO #tapDupPaymentsWrk_SGS  " & sSelect
    #End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist("tapDupPaymentsWrk_SGS", "SessionID", ReportObj.SessionID, True) Then
        #Else
            If Not SelectObj.bRecsToPrintExist("#tapDupPaymentsWrk_SGS", "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("CommentOutThisProcCode")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
'
'    DBObj.ReleaseParams

    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    ReportObj.ReportFileName() = "DuplicatePayments.rpt"
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""
    Select Case RptVrsn
        Case InvcDtVendAmt
            ReportObj.ReportTitle1() = "Potential Duplicate Payments: " & frmDupPayments.Optn1.Caption
        Case InvcDtVend
            ReportObj.ReportTitle1() = "Potential Duplicate Payments: " & frmDupPayments.Optn2.Caption
        Case DtVendAmt
            ReportObj.ReportTitle1() = "Potential Duplicate Payments: " & frmDupPayments.Optn3.Caption
        Case InvcDtAmt
            ReportObj.ReportTitle1() = "Potential Duplicate Payments: " & frmDupPayments.Optn4.Caption
        Case Else
            ReportObj.ReportTitle1() = "Potential Duplicate Payments"
    End Select

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
    If (ReportObj.lSetSortCriteria(frm.moSort, 1) = kFailure) Then
        GoTo badexit
    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    If (ReportObj.lRestrictBy("{tapDupPaymentsWrk_SGS.SessionID} = " & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function



