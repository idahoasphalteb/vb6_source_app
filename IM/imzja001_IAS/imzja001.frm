VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Begin VB.Form frmRegister2 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7080
   ClientLeft      =   1200
   ClientTop       =   1620
   ClientWidth     =   9390
   HelpContextID   =   1012775
   Icon            =   "imzja001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7080
   ScaleWidth      =   9390
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6690
      WhatsThisHelpID =   73
      Width           =   9390
      _ExtentX        =   16563
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin VB.PictureBox CommonDialog1 
      Height          =   480
      Left            =   15000
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   36
      Top             =   600
      Width           =   1200
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   72
      Width           =   9390
      _ExtentX        =   16563
      _ExtentY        =   741
      Style           =   5
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   27
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame fraMessage 
      Caption         =   "&Message"
      Height          =   1695
      Left            =   60
      TabIndex        =   16
      Top             =   4860
      Width           =   9195
      Begin VB.PictureBox pctMessage 
         Height          =   1320
         Left            =   135
         ScaleHeight     =   1260
         ScaleWidth      =   8775
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   270
         Width           =   8835
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   0
            Left            =   0
            MaxLength       =   50
            TabIndex        =   17
            Top             =   0
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   1
            Left            =   0
            MaxLength       =   50
            TabIndex        =   18
            Top             =   255
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   2
            Left            =   0
            MaxLength       =   50
            TabIndex        =   19
            Top             =   510
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   3
            Left            =   0
            MaxLength       =   50
            TabIndex        =   20
            Top             =   765
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   4
            Left            =   0
            MaxLength       =   50
            TabIndex        =   21
            Top             =   1020
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.Line Line1 
            Index           =   3
            X1              =   0
            X2              =   8775
            Y1              =   1010
            Y2              =   1010
         End
         Begin VB.Line Line1 
            Index           =   2
            X1              =   0
            X2              =   8775
            Y1              =   750
            Y2              =   750
         End
         Begin VB.Line Line1 
            Index           =   1
            X1              =   0
            X2              =   8775
            Y1              =   500
            Y2              =   500
         End
         Begin VB.Line Line1 
            Index           =   0
            X1              =   0
            X2              =   8775
            Y1              =   240
            Y2              =   240
         End
      End
   End
   Begin VB.Frame fraSort 
      Caption         =   "&Sort"
      Height          =   1905
      Left            =   60
      TabIndex        =   14
      Top             =   2880
      Width           =   9195
      Begin FPSpreadADO.fpSpread grdSort 
         Height          =   1545
         Left            =   90
         TabIndex        =   15
         Top             =   270
         WhatsThisHelpID =   25
         Width           =   9015
         _Version        =   524288
         _ExtentX        =   15901
         _ExtentY        =   2725
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "imzja001.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame frmRegOption 
      Caption         =   "Register &Options"
      Height          =   1815
      Left            =   4620
      TabIndex        =   11
      Top             =   960
      Width           =   4665
      Begin SOTADropDownControl.SOTADropDown ddnFormat 
         Height          =   315
         Left            =   1800
         TabIndex        =   13
         Top             =   300
         WhatsThisHelpID =   1012796
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   ""
      End
      Begin VB.Label lblFormat 
         Caption         =   "Format"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   330
         Width           =   1095
      End
   End
   Begin VB.Frame frmPrintPost 
      Caption         =   "&Print/Post"
      Height          =   1815
      Left            =   60
      TabIndex        =   5
      Top             =   960
      Width           =   4425
      Begin VB.CheckBox chkConfirm 
         Caption         =   "Confirm Before Posting"
         Height          =   285
         Left            =   450
         TabIndex        =   10
         Top             =   1290
         Value           =   1  'Checked
         WhatsThisHelpID =   1012793
         Width           =   2785
      End
      Begin VB.CheckBox chkPost 
         Caption         =   "Post"
         Height          =   285
         Left            =   180
         TabIndex        =   9
         Top             =   930
         Value           =   1  'Checked
         WhatsThisHelpID =   1012792
         Width           =   1125
      End
      Begin VB.CheckBox chkRegister 
         Caption         =   "Register"
         Height          =   285
         Left            =   180
         TabIndex        =   6
         Top             =   270
         Value           =   1  'Checked
         WhatsThisHelpID =   1012791
         Width           =   1515
      End
      Begin SOTADropDownControl.SOTADropDown ddnOutput 
         Height          =   315
         Left            =   1260
         TabIndex        =   8
         Top             =   600
         WhatsThisHelpID =   1012790
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   ""
      End
      Begin VB.Label Label4 
         Caption         =   "Output"
         Height          =   195
         Left            =   450
         TabIndex        =   7
         Top             =   630
         Width           =   915
      End
   End
   Begin VB.CheckBox chkSummary 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   4590
      TabIndex        =   4
      Top             =   540
      WhatsThisHelpID =   31
      Width           =   3225
   End
   Begin VB.ComboBox cboReportSettings 
      Height          =   315
      Left            =   1020
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   510
      WhatsThisHelpID =   23
      Width           =   3465
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   28
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.PictureBox dlgCreateRPTFile 
      Height          =   480
      Left            =   10200
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   37
      Top             =   1785
      Width           =   1200
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   1000
      Left            =   -30000
      TabIndex        =   34
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin MSComctlLib.ImageList imlToolbar 
      Left            =   9510
      Top             =   510
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CommonDialog2 
      Left            =   1440
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblStatusMsg 
      Caption         =   "Pending"
      Height          =   375
      Left            =   10860
      TabIndex        =   1
      Top             =   3180
      Width           =   975
   End
   Begin VB.Label lblSetting 
      Caption         =   "S&etting"
      Height          =   195
      Left            =   60
      TabIndex        =   2
      Top             =   570
      Width           =   795
   End
End
Attribute VB_Name = "frmRegister2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: frmReport
'     Desc: Inventory Transaction Register
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: EDT
'     Mods: 05/09/97 HBS Multi-select for selection grid.
'***********************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

' Privates
    Private mlRunMode               As Long
    Private mbSaved                 As Boolean
    Private mbCancelShutDown        As Boolean
    Private mbLoading               As Boolean
    Private mbPeriodEnd             As Boolean
    Private mbLoadSuccess           As Boolean
    Private mbEnterAsTab            As Boolean
    Private miSecurityLevel         As Integer
    Private moClass                 As Object
    Private moContextMenu           As clsContextMenu
    Private mbLoadingSettings       As Boolean
    
' For resizability
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    
' Publics
    Public moSotaObjects            As Collection
    Public msCompanyID              As String
    Public msUserID                 As String
    Public mlLanguage               As Long
    Public moReport                 As clsReportEngine
    Public moSettings               As clsSettings
    Public moDDData                 As clsDDData
    Public moOptions                As clsOptions
    Public moSort                   As clsSort
    Public moPrinter                As Printer
    Public sRealTableCollection     As Collection
    Public sWorkTableCollection     As Collection
    
    Public mlBatchKey               As Long
    Public mbErrorOccured           As Boolean

'Lock types
    Private Const lLockTypeShared    As Integer = 1
    Private Const lLockTypeExclusive As Integer = 2
    
    Dim mlLockID                    As Long
    Dim miLockType                  As Integer      ' 1 Shared, 2 Exclusive, 0 Nolock
    Dim lRetVal                     As Integer
    Public miMode                   As Integer      ' 0 for register 1 for Post IM Batches

    Private Const kPostXXBatches    As Integer = 0
    Private Const kInteractiveBatch As Integer = 1

' Fiscalperiod validation constants
    Private Const kIMNoBatchFound As Integer = -2
    Private Const kIMNoGLFiscPer As Integer = -3
    Private Const kIMClosedFiscPer As Integer = -4
    Private Const kIMMoreThanOneGLFiscPer As Integer = -5

' Invt period validation constants
    Private Const kIMNoInvtPer As Integer = -2
    Private Const kIMClosedInvtPer As Integer = -4
    
' For Register and Post flags
    Public mbRegisterFlag  As Boolean          ' Holds a flag to indicate whether register is needed or not
    Public mbPostFlag      As Boolean          ' Holds a flag to indicate whether posting is to be done or not.
    Public mbConfirmFlag    As Boolean          ' Holds a flag for confirmation required.
    
    Public mbDetailFlag    As Boolean          ' Holds a flag If true then Detail,  else summary
    Public msPrintButton    As String           ' Preview or Print
    Public miUseMultCurr   As Boolean          ' Holds if multi currency is enabled or not
    Public miGLPostDetailFlag   As Integer      '  1  None, 2 Summary,  Detail
    
' GL Register options
    Private Const kGLPostNone       As Integer = 1
    Private Const kGLPostSummary    As Integer = 2
    Private Const kGLPostDetail     As Integer = 3
    
    Public mlPostDate               As Date         ' Holds the post date
' Format and Output dropdown constants
    Private Const kOutputScreen     As Integer = 2
    Private Const kOutputPrinter    As Integer = 1
    Private Const kFormatSummary    As Integer = 1
    Private Const kOutputDetail     As Integer = 2
    
    ' Constants for batch type
    Private Const kBatchTypeIMPC As Integer = 702   '  Physical count batch
    Private Const kBatchTypeIMCA As Integer = 703   '  Cost tier adjustment batch
    Private Const kBatchTypeIMKA As Integer = 704   '  Kits Assembly batch
    Private Const kBatchTypeIMIT As Integer = 701   '  Inventory Transaction
    Private Const kBatchTypeIMTR As Integer = 705   '  Transfer
    Private Const kBatchTypeMFMB As Integer = 9004  '  Manufacturing BOM Cost Rollup batch
    
' Report path variables
    Public msGLReportPath       As String           ' Stores a validated report path for GL
    Public msIMReportPath       As String           ' Stores a validated report path for IM
    Public msCIReportPath       As String           ' stores a validated report path for CM

    Public msEntity As String                       ' Holds the Batch Entity ID

' Added by Gil to fix bug 5005 (in case of validation error,
' both IM and Error registers should be displayed
    Private mbPreprocessingError As Boolean
    
' Added by Ashish on 11 Dec 1998 for fixing 6225
    Public mbBatchOnHold As Boolean
    
' Added by Gil to reorganize the sorts from one register to another
'-- Grid columns
    Private Const kColSort = 1
    Private Const kColAscending = 2
    Private Const kColSubtotal = 3
    Private Const kColPageBreak = 4
    
    Private Const kWorkTableIT = 1
    Private Const kWorkTableTR = 2
    Private Const kWorkTableKA = 3
    
    Public msSortField As Collection
    Public msSortOrder As Collection
    Public mlSubTotal As Collection
    Public mlPageBreak As Collection
    
    Public mbKitsRegNeeded As Boolean    ' If Kit Reg needed, we need to remove ItemType & TranType sorts.
    
'Intellisol Start
    Public mbCalledFromPA       As Boolean
'Intellisol End

    Private cypHelper       As Object   'SGS-JMM 2/10/2011

Const VBRIG_MODULE_ID_STRING = "IMZJA001.FRM"

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = "CIZ"
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = "CIZ"
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
    Set oClass = moClass
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get oreport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oreport = moReport
End Property

Public Property Set oreport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Public Property Get bCancelShutDown() As Boolean
'+++ VB/Rig Skip +++
  bCancelShutDown = mbCancelShutDown
End Property

Public Property Let bCancelShutDown(bCancel As Boolean)
'+++ VB/Rig Skip +++
    mbCancelShutDown = bCancel
End Property

Private Sub chkPost_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPost, True
    #End If
'+++ End Customizer Code Push +++
    ' If no posting required then dont ask user whether he wants confirmation or not
    If Not chkPost.Value = vbChecked Then
        chkConfirm.Enabled = False
    Else
        chkConfirm.Enabled = True
    End If
End Sub

Private Sub chkRegister_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkRegister, True
    #End If
'+++ End Customizer Code Push +++
    ' If no register is to be printed, then don't allow selection of register destination
    ' And no need of style (Summary or Detail)
    If Not chkRegister.Value = vbChecked Then
        ddnFormat.Enabled = False
        ddnOutput.Enabled = False
    Else
        ddnFormat.Enabled = True
        ddnOutput.Enabled = True
    End If

    ' Now take care of the post checkbox
    Dim lRetVal As Long

    lRetVal = lValidateBatch(mlBatchKey)
    If lRetVal = 9 Then
        mbLoadingSettings = False
        Exit Sub  ' Out of balance
    End If
    
    Dim iRegisterPrinted As Integer
    iRegisterPrinted = iCheckRegisterPrinted(Me)
    
    If chkRegister = 0 Then
        If iRegisterPrinted = 0 Then
            EnablePost False, iRegisterPrinted
        Else
            EnablePost True, iRegisterPrinted
        End If
    Else
        EnablePost True, iRegisterPrinted
    End If
    
    mbLoadingSettings = False
End Sub

Private Sub EnablePost(bState As Boolean, iRegPrinted As Integer)
    '-- Enable post controls if not already enabled
    '-- and passed state is true
    '-- If passed state is false, disable controls
    If bState = True And chkPost.Enabled = False Then
        If Not mbBatchOnHold Then
                chkPost.Enabled = True
                If Not mbLoadingSettings Then
                    chkPost.Value = vbChecked
                End If
        End If
    ElseIf bState = False And iRegPrinted = 0 Then
        If Not mbBatchOnHold Then
            chkPost.Value = vbUnchecked
            chkPost.Enabled = False
            chkConfirm.Enabled = False
        End If
    End If
End Sub

Private Sub ddnFormat_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnFormat, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRetVal As Long
    Dim sModule As String
    Dim sDefaultReport As String
    Dim sRptProgramName As String

    If glBatchType <> kBatchTypeIMIT And glBatchType <> kBatchTypeIMCA And glBatchType <> kBatchTypeMFMB Then 'And glBatchType <> kBatchTypeIMPC Then
        Exit Sub
    End If

    sRptProgramName = "IMZTG005a" 'such as rpt001
    sModule = "IM"
    sDefaultReport = "IMZTG005a.RPT" 'such as rpt001

    If PrevIndex <> NewIndex And PrevIndex <> -1 Then
        Me.MousePointer = vbHourglass
'Intellisol Start
        If mbCalledFromPA Then
            On Error Resume Next
            If Not moReport.bShutdownEngine Then
            End If
        End If
'Intellisol End
        lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
        Me.MousePointer = vbNormal
        If lRetVal <> 0 Then
            moReport.ReportError lRetVal
        End If
    End If
End Sub

Private Sub fraMessage_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "fraMessage_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "mskControl_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "nbrControl_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curControl_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navControl_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navControl_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'******************** End Selection Stuff ****************************
Private Sub SysInfo1_SysColorsChanged()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SysInfo1_SysColorsChanged", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolbarClick(sKey As String, Optional iFileType As Variant, Optional sFileName As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If

    Dim sPrevStatusMsg As String
    Dim bBatchOnHold As Boolean
    Dim sSQL As String
    Dim DummyParamArray As Variant
    Dim oIMTmpTbl As New clsIMTempTable

    mbErrorOccured = False
    mbPreprocessingError = False
    
    If Me.Visible Then
        Me.SetFocus
    End If

    Select Case sKey
        Case kTbSave
             moSettings.ReportSettingsSaveAs
             
        Case kTbProceed, kTbFinish
            'validate check boxes
            ' Added by Gil to fix bug 4528
            If chkRegister = 0 Then
                If chkPost = 0 Then
                    MsgBox "Either Register or Post checkbox should be checked!", vbOKOnly, "Register/Post"
                    Exit Sub
                End If
            End If
            
            'Initialize to show print dialog. HBS.
            If miMode <> kPostXXBatches Then
                gbSkipPrintDialog = False
            End If
            
            gbCancelPrint = False
            
            'Get current message on status bar so can set back if user cancels from print dialog. HBS
            If miMode <> kPostXXBatches Then
                sPrevStatusMsg = sbrMain.Message
            Else
                sPrevStatusMsg = frmPrintPostStat.lblStatusMsg
            End If
           
            'This is where processing will start.

            'Validate the posting date / fiscal period.
            If bIsValidFiscPeriod(mlBatchKey) = False Then
                Exit Sub
            End If

            'Get the batch parameters only when this is called from Post IM Batches.
            'Otherwise do the same in Form_Load.
            If gbPostIMBatchesFlag Then
                ' This is the new function that gets all batch parameters.
                If Not gbGetIMBatchParameters(moClass.moAppDB, mlBatchKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If
            End If
            
            ' Load the print post flags
            If Not gbPostIMBatchesFlag Then
                GetPrintPostFlags
            End If
            
            'Clean the error log
            CleanErrorLog mlBatchKey
            
            'Put logical shared lock on the batch
            msEntity = kLockEntBatch & Format$(mlBatchKey)
           
            'Now validate the batch
            lRetVal = lValidateBatch(mlBatchKey)
            
            ' If batch is not a valid one, then release locks and stop processing.
            ' Changed by Ashish for allowing register of batches on hold.
            ' The posting of batches on hold is stopped as the option is not enabled.
            If (lRetVal <> 0 _
            And lRetVal <> 2 _
            And lRetVal <> 9) Then
                iLogError mlBatchKey, kIMInvalidBatch, gsBatchID, "", "", "", "", 1, 2
                ' Warn user that we have fatal errors.
                sbrMain.Message = gsBuildString(652, moClass.moAppDB, moClass.moSysSession)

                ' Stop the processing
                EndProcessing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
    
            ' The posting of batches on hold will be stopped and error displayed only if posting was requested.
            ' Do not do it for Cost Tier Adj since it is done by lPostCostTiers.
            If lRetVal = 2 And mbPostFlag And glBatchType <> kBatchTypeIMCA And glBatchType <> kBatchTypeMFMB Then
                ' Cannot log an error now because preprocessing cleans up error log
                bBatchOnHold = True
            End If
            
            ' Set the batch status to PreProcessing started
            ' No need of error checking as batch existance is validated and batch has
            ' a shared lock
            bUpdateBatchStatus mlBatchKey, 100
            
            'Create temp tables for posting
            If oIMTmpTbl.gbCreateTmpTblIMInvPosting(moClass.moAppDB) = False Then
                Exit Sub
            End If
            
            If glBatchType = kBatchTypeIMCA Or glBatchType = kBatchTypeMFMB Then
                ' Post the cost tiers
                ' For cost tiers, preprocessing and module posting must be part of
                ' the same SQL transaction
                lRetVal = lPostCostTiers(mlBatchKey, mbPostFlag)
            Else
                ' Now call the preprocessing SP
                lRetVal = lPreProcessing(mlBatchKey)
            End If
            
            ' If Preprocessing has failed
            If Not (lRetVal = 0) Then
                mbPreprocessingError = True
            End If

            If bBatchOnHold Then
                ' Log error now
                iLogError mlBatchKey, kVBatchHold, gsBatchID, "", "", "", "", 1, 2
                mbPreprocessingError = True
            End If
            
            If Not mbPostFlag Then
                UndoCostTiers mlBatchKey
                UnloadPAGLPosting mlBatchKey
            End If
                         
            ' Check balance
            If glBatchType <> kBatchTypeIMCA And glBatchType <> kBatchTypeMFMB Then
                If CheckBalance = kFailure Then
                    mbPreprocessingError = True
                End If
            End If
            
            ' Now print the register
            ' Modified by Gil to fix bug 5005: bPrintRegister will not print
            ' GL register if mbPreprocessingError = True
            If Not bPrintRegister Then
                If Not gbCancelPrint Then
                    ' Warn user that we have fatal errors.
                    sbrMain.Message = gsBuildString(652, moClass.moAppDB, moClass.moSysSession)
                End If
                
                ' The error report is already printed in bPrintRegister so just exit.
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                If glBatchType <> kBatchTypeIMCA And glBatchType <> kBatchTypeMFMB Then
                    ' Not a cost tier adjustment, exit
                    Exit Sub
                End If
            End If

            ' Check tciErrorLog for the existence of warnings.
            lRetVal = iGetErrorStatus(1)      ' 1 stands for error

            If (mbPreprocessingError Or lRetVal = 2) Then
                'Set the RgstrPrinted Status to 0 / PostStatus to 2.
                Call iSetPrintedFlag(2)
                
                ' Warn user that we have fatal errors.
                sbrMain.Message = gsBuildString(652, moClass.moAppDB, moClass.moSysSession)
   
                PrintErrorLog
                
                UndoCostTiers mlBatchKey
                UnloadPhysCounts mlBatchKey
                UnloadPAGLPosting mlBatchKey
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                ' Did we get any warnings?
                If lRetVal = 1 Then
                    'Set the RgstrPrinted Status to 1 / PostStatus to 1
                    Call iSetPrintedFlag(1)
                Else
                    'Set the RgstrPrinted Status to 1 / PostStatus to 0
                    Call iSetPrintedFlag(0)
                End If
                
                ' Preprocessing and registers completed
                bUpdateBatchStatus mlBatchKey, 150
            End If
            
            ' Before user goes for posting show him the error log in case of warnings.
            ' The Print error log will print the error log only if required.
            PrintErrorLog
            
            'SGS-JMM 1/31/2011
            On Error Resume Next
            If Not (cypHelper Is Nothing) Then
                If cypHelper.IsCypressPrinter(moReport.PrinterName) Then
                    cypHelper.ShowViewer
                End If
            End If
            On Error GoTo VBRigErrorRoutine
            'End SGS-JMM 1/31/2011
                        
            ' Now for posting
            If mbPostFlag Then
                ' Check if any error has occured.
                lRetVal = iGetErrorStatus(2)      ' 2 stands for error
                
                ' If error in getting error status then
                If lRetVal > 0 Then
                    ' 0 stands for no error 1 for error present and > 1 for malfunctioning
                    ' Stop the processing
                    EndProcessing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If

                ' If confirmation is required, then ask the confirmation here.
                Dim iMsgReturn As Integer               ' Return from the user.
                If mbConfirmFlag Then
                    ' Ask for the confirmation here
                    DoEvents
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostRegister, DummyParamArray)

                    If iMsgReturn = kretNo Then
                        UndoCostTiers mlBatchKey
                        UnloadPhysCounts mlBatchKey
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                End If
                
                ' Set the batch status to Module posting started.
                bUpdateBatchStatus mlBatchKey, 200
                
                ' Start the posting here.
                ' Call SP for populating IM Posting.
                If Not PopulateIMPostTable(mlBatchKey) Then
                    ' Stop the processing
                    EndProcessing
                    
                    ' Change message on status bar
                    bUpdateBatchStatus mlBatchKey, 1000
                    
                    ' Reset the batch status to Balanced, PreProc Complete
                    SetBatchStatus mlBatchKey, 4, 150
                    
                    UndoCostTiers mlBatchKey
                    UnloadPhysCounts mlBatchKey
                    UnloadPAGLPosting mlBatchKey
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If

                ' Set the batch status to GL posting started.
                ' No need of error checking as batch existance is validated and batch has a shared lock.
                bUpdateBatchStatus mlBatchKey, 300

                ' Call an SP for populating the GL posting table.
                If Not PopulateGLTable(mlBatchKey) Then
                    ' Stop the processing
                    EndProcessing
                    
                    ' Change message on status bar
                    bUpdateBatchStatus mlBatchKey, 1000
                    
                    ' Reset the batch status to Posting, Mod Posting Complete
                    SetBatchStatus mlBatchKey, 5, 250
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If

                ' Set the batch status to Posting completed.
                ' No need of error checking as batch existance is validated and batch has a shared lock.
                bUpdateBatchStatus mlBatchKey, 500
                
                ' Now posting is over so print the error log and exit.
                ' This condition is added for printing error log only if errors are present.
                If mbErrorOccured Then
                    PrintErrorLog
                End If
 
                CleanErrorLog mlBatchKey
                
                tbrMain.Enabled = False
                tbrMain.ButtonEnabled(kTbProceed) = False
            Else
                UnloadPhysCounts mlBatchKey
            End If
                
        Case kTbCancel, kTbDelete
            moSettings.ReportSettingsDelete
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
    End Select

    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.HandleToolbarClick sKey, 1
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM 1/31/2011

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
                
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bIsErrorLogReq(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bIsErrorLogReq
    ' Written By        Ashish
    ' Purpose           Checks if error log is required to be printed or not
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    '
    ' ------------------------------------
    Dim sSQL As String
    Dim rs As Object
    Dim lRet As Long
    Dim iEntryNo As Integer

    bIsErrorLogReq = 0
    
    'first get max entry no for this batch
    sSQL = "SELECT count(*) cnt  FROM tciErrorLog WHERE BatchKey = " & Str(lBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
        If IsNull(rs.Field("cnt")) Then
            bIsErrorLogReq = False
        Else
            If rs.Field("cnt") > 0 Then
                bIsErrorLogReq = True
            Else
                bIsErrorLogReq = False
            End If
        End If
    End If
    
    Set rs = Nothing

                                    ' Posting process.
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsErrorLogReq", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub GetPrintPostFlags()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     GetPrintPostFlags
    ' Written By        Ashish
    ' Purpose           This sub loads
    ' Input Args        Loads the Print Post flags based on UI
    ' ------------------------------------
    If chkRegister.Value = 1 Then
        mbRegisterFlag = True
    Else
        mbRegisterFlag = False
    End If
    
    If chkPost.Value = 1 Then
        mbPostFlag = True
    Else
        mbPostFlag = False
    End If
    
    If chkConfirm.Value = 1 Then
        mbConfirmFlag = True
    Else
        mbConfirmFlag = False
    End If
    
    ' For detail and ummary
    If ddnOutput.ListIndex = 0 Then
        msPrintButton = kTbPrint
    Else
        msPrintButton = kTbPreview
    End If
    
    ' For Format
    If ddnFormat.ListIndex = 0 Then
        mbDetailFlag = True
    Else
        mbDetailFlag = False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetPrintPostFlags", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function iCheckRegisterFlag(lBatchKey As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     iCheckRegisterFlag
    ' Written By        Ashish
    ' Purpose           This function checks whether the register is printed or not
    ' Input Args        lbatchKey   Batch for which the register flag is to be checked
    ' Return Args       0   Register not Printed
    '                   1   Register printed
    '                   2   Malfunctioning( Recordset is empty)
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string
    
    sSQL = "select RgstrPrinted from tciBatchLog where BatchKey = " & lBatchKey
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
        ' If not null then return the rgstr printed flag
        If Not IsNull(rs.Field("RgstrPrinted")) Then
            iCheckRegisterFlag = rs.Field("RgstrPrinted")
        Else
            iCheckRegisterFlag = 2
        End If
    Else
        iCheckRegisterFlag = 2
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckRegisterFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub EndProcessing()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ----------------------------------------------------------------------------------
    ' Sub Name          EndProcessing
    ' Written By        Ashish
    ' Purpose           This sub will be called during intermediate exiting of the task.
    ' ----------------------------------------------------------------------------------
    PrintErrorLog
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EndProcessing", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function iGetErrorStatus(ErrorType As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bGetErrorStatus
    ' Written By        Ashish
    ' Purpose           This function Gets the error status for the current batch
    ' Input Args        Error Type  1   Warning
    '                               2   Error
    ' Return Args       0   No errors
    '                   1   Errors present
    '                   2   Malfunctioning( Recordset is empty)
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string
    
    sSQL = "select count(*) Cnt from tciErrorLog where BatchKey = " & mlBatchKey & " and Severity = " & ErrorType
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
        ' No need to check if null because count will be 0 or more
        If rs.Field("Cnt") = 0 Then
            iGetErrorStatus = 0
        Else
            iGetErrorStatus = 1
        End If
    Else
        iGetErrorStatus = 2
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetErrorStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function iCheckRegisterPrinted(frm As Form) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rs As Object

    iCheckRegisterPrinted = 0
    
    sSQL = "SELECT RgstrPrinted FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iCheckRegisterPrinted = rs.Field("RgstrPrinted")
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckRegisterPrinted", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
            End Select
'+++ VB/Rig End +++
End Function

Public Function bPrintRegister() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Sub Name          bPrintRegister
    ' Written By        Ashish
    ' Purpose           This function Prints the IM Common register
    ' Input Args        None
    ' Return Args       False   if fails
    '                   True    if suceeds
    ' ------------------------------------
    Dim lRetVal As Integer              ' Holds return values from functions.
    
    bPrintRegister = False
    
    ' Now print the register  Do not move the gbcancelprint check out of this function
    ' This is because the function is called from multiple places
    If mbRegisterFlag And Not (gbCancelPrint) Then
        If glBatchType = kBatchTypeIMPC Then
            ' Call here physical count report
            Dim iFileType
            Dim sFileName
            lRetVal = lStartReportPC(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
            
            If lRetVal = kFailure Then
                bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            
            'Check if there are Adjustment transactions or not
            ' If there are then call the Inventory register report
            If bCheckForAdjustments(mlBatchKey) Then
                ' Call the inventory register here
                lRetVal = lStartReportIT(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
                
                If lRetVal = kFailure Then
                    bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
             End If
         ' If the batch is kits assembly
        ElseIf (glBatchType = kBatchTypeIMKA) Then
            ' Call the Kits Assembly Register
            'lStartReport sKey, Me, False, "", "", "", iFileType, sFileName
            lRetVal = lStartReportKA(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
            
            If lRetVal = kFailure Then
                bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
         ElseIf glBatchType = kBatchTypeIMCA Or glBatchType = kBatchTypeMFMB Then
            ' Call the Cost Tier Adjustment register.
            lRetVal = lStartReportCA(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
            
            If lRetVal = kFailure Then
                bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
         Else
            ' First save sorts from the grid into the collections
            SaveCurrentSorts
                       
            ' If sales of BTO kits have occured then
            If bKitsRegNeeded = True Then
                ReinitializeSorts kWorkTableKA
            
                ' Call kits assembly register here
                lRetVal = lStartReportKA(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
                
                If lRetVal = kFailure Then
                    bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
             End If
            
            ' If transfer transactions have taken place then call transfer register
            If bTransferRegNeeded = True Then
                ReinitializeSorts kWorkTableTR

                ' Call Transfer register here
                lRetVal = lStartReportTR(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
                
                If lRetVal = kFailure Then
                    bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
             End If
             
            ' Call the Inventory transaction register.
            If bCheckForTranRegs(mlBatchKey) = True Then
                ReinitializeSorts kWorkTableIT
                
                lRetVal = lStartReportIT(msPrintButton, Me, False, "", "", "", iFileType, sFileName)
                
                If lRetVal = kFailure Then
                    bPrintRegister = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
         End If
            
        ' Do this only if there was no error with preprocessing (bug 5005)
        If Not mbPreprocessingError Then
            ' This has been shifted here to allow concurrency.
            ' Populate the GL register parameters
            lRetVal = iPopulateGLRegParams
            
            ' Print the GL Register.  Table for this is populated while populating tables for
            ' inventory registers
            If glBatchType = kBatchTypeMFMB Then
                ' Check to see if the GL register needs to be printed
                Dim sSQL As String
                Dim rs As Object
            
                sSQL = "SELECT * FROM tglPosting WHERE BatchKey = " & glGetValidLong(mlBatchKey)
                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
                If Not rs.IsEmpty Then
                    lRetVal = PrintGLRegister(moClass.moFramework.GetTaskID)
                Else
                    iLogError mlBatchKey, kPostGenericBad, gsBatchID, " No GL Posting Necessary ", "", "", "", 0, 1
                    lRetVal = 1
                End If
            Else
                lRetVal = PrintGLRegister(moClass.moFramework.GetTaskID)
            End If
        
            ' If error in printing GL register
            If lRetVal = 0 Then
                ' Print the error log and stop processing
                iLogError mlBatchKey, kIMGlRegErr, gsBatchID, "", "", "", "", 1, 2
            
                ' Stop the processing
                EndProcessing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If  ' If register is required.
    End If ' No Preprocessing error

    bPrintRegister = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bPrintRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function bKitsRegNeeded() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bKitsRegNeeded
    ' Written By        Ashish
    ' Purpose           This function returns a flag to indicate whether kits register
    '                   is needed in batches other than kits assembly batches
    ' Note              This function checks if there are any transactions of Type IMSale
    '                   involving BTO kits then it calls Kits assembly register.
    ' Input Args        None
    ' Return Args       True   If Kits register is needed
    '                   False  If Kits register is not needed
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string
    
    
    sSQL = " select count(*) cnt from  timItem, timPendInvtTran where timPendInvtTran.BatchKey = " & mlBatchKey & " and timItem.ItemType = 7 and timPendInvtTran.TranType = 701 and timPendInvtTran.ItemKey = timItem.ItemKey"
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    bKitsRegNeeded = False        'Initialized for safety purpose
    
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
    
        ' No need to check if null
        If rs.Field("cnt") > 0 Then
            bKitsRegNeeded = True
        Else
            bKitsRegNeeded = False
        End If
        
    Else
        bKitsRegNeeded = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bKitsRegNeeded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function bTransferRegNeeded() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bTransferRegNeeded
    ' Written By        Ashish
    ' Purpose           This function returns a flag to indicate whether Transfer register
    '                   is needed in batches other than Transfer assembly batches
    ' Note              This function checks if there are any transactions Transfer then
    '                   it calls Transfer  register.
    ' Input Args        None
    ' Return Args       True   If Transfer register is needed
    '                   False  If Transfer register is not needed
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string
    
    
    sSQL = " select count(*) cnt from   timPendInvtTran where timPendInvtTran.BatchKey = " & mlBatchKey & " and ( timPendInvtTran.TranType = 705 or timPendInvtTran.TranType = 705 ) "
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    bTransferRegNeeded = False        'Initialized for safety purpose
    
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
    
        ' No need to check if null
        If rs.Field("cnt") > 0 Then
            bTransferRegNeeded = True
        Else
            bTransferRegNeeded = False
        End If
        
    Else
        bTransferRegNeeded = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bTransferRegNeeded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bEscalateLogicalLock() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bEscalateLogicalLock
    ' Written By        Ashish
    ' Purpose           This function escalates logical lock to exclusive
    ' Note              This function does not assumes a previous lock on batch
    '                   if batch is locked previously then it realeases
    '                   It picks up global sEntity
    '                   It  Updates global LockID
    ' Input Args        None
    ' Return Args       True   Successful execution
    '                   False  Options were not set  ( No Options record)
    ' ------------------------------------
    'So first release the logical lock that you have
    
    bEscalateLogicalLock = False            ' Initialized
    
    ' if no lock previously then dont release the lock
    If Not (miLockType = 0) Then
        lRetVal = glUnlockLogical(moClass.moAppDB, mlLockID)

        ' If there is an error in releasing locks
        If Not (lRetVal = 0) Then
            ' This is a critical error that you are not able to release the lock
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    If miLockType = lLockTypeExclusive Then
        bEscalateLogicalLock = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    ' Now put a logical  exclusive lock
    lRetVal = glLockLogical(moClass.moAppDB, msEntity, lLockTypeExclusive, mlLockID)
        
    ' If the locking is an unsuccessful attempt then stop processing
    If lRetVal > 0 Then
        iLogError mlBatchKey, kIMExLockNotAchvd, gsBatchID, "", "", "", "", 1, 2
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    Else
        miLockType = lLockTypeExclusive
    End If
    bEscalateLogicalLock = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bEscalateLogicalLock", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function bUpdateRegisterFlag(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Sub Name          bUpdateRegisterFlag
    ' Written By        Ashish
    ' Purpose           This function updates the PrintRgstar flag in tciBatchLog
    ' Note              This function expets a logical esclusive lock on the batch
    ' Input Args        None
    ' Return Args       True   Successful execution
    '                   False  Options were not set  ( No Options record)
    ' ------------------------------------
    Dim sSQL As String              ' Holds the sql string
    
    bUpdateRegisterFlag = True         ' For future use if required
    sSQL = "update tciBatchLog set RgstrPrinted = 1 where  BatchKey = " & lBatchKey & ""
    moClass.moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateRegisterFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function bUpdateBatchStatus(lBatchKey As Long, iStatus As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Sub Name          bUpdateBatchStatus
    ' Written By        Ashish
    ' Purpose           This function updates the PrintRgstar flag in tciBatchLog
    ' Note              This function expets a logical esclusive lock on the batch
    '                   This function also changes the Status field in tciBatchLog
    ' Input Args        None
    ' Return Args       True   Successful execution
    '                   False  Options were not set  ( No Options record)
    ' ------------------------------------
    Dim sSQL As String              ' Holds the sql string
    
    bUpdateBatchStatus = True         ' For future use if required
    
    ' Change the Status and PostStatus based on input iStatus
    ' If iStatus is 500 then set Status  = 6 ie posted.
    ' Otherwise set to 5 ie posting
    
    ' This part is moved to SP that posts transactions
'    If iStatus = 500 Then
'        sSQL = "update tciBatchLog set Status = 6, PostStatus = " & iStatus & " where  BatchKey = " & lBatchKey & ""
'        moClass.moAppDB.ExecuteSQL sSQL
'    Else
'        sSQL = "update tciBatchLog set Status = 5, PostStatus = " & iStatus & " where  BatchKey = " & lBatchKey & ""
'        moClass.moAppDB.ExecuteSQL sSQL
'
'    End If
    
    ' Now display appropriate status on the screen
    If miMode <> kPostXXBatches Then
    
        Select Case iStatus
            Case 100
                'sbrSOTAStatus.message = gsBuildString(kIMPreProcStarted, moClass.moAppDB, moClass.moSysSession)
                'sbrSOTAStatus.message = gsBuildString(kstrRegPreprocStarted, moClass.moAppDB, moClass.moSysSession)
                sbrMain.Message = gsBuildString(160303, moClass.moAppDB, moClass.moSysSession)
                             
            Case 150
                'sbrSOTAStatus.message = gsBuildString(kstrRegPreprocEnded, moClass.moAppDB, moClass.moSysSession)
                sbrMain.Message = gsBuildString(160304, moClass.moAppDB, moClass.moSysSession)
                             
            Case 200
                sbrMain.Message = gsBuildString(kIMModPostingStarted, moClass.moAppDB, moClass.moSysSession)
    
            Case 300
                sbrMain.Message = gsBuildString(kIMGLPostingStarted, moClass.moAppDB, moClass.moSysSession)
    
            Case 350
                sbrMain.Message = gsBuildString(kimGLPostingOver, moClass.moAppDB, moClass.moSysSession)
        
            Case 500
                sbrMain.Message = gsBuildString(kIMPostingCompleted, moClass.moAppDB, moClass.moSysSession)
            
            Case Else
                sbrMain.Message = ""
        End Select
    Else
        Select Case iStatus
            Case 100
                'frmPrintPostStat.lblStatusMsg = gsBuildString(kIMPreProcStarted, moClass.moAppDB, moClass.moSysSession)
                'frmPrintPostStat.lblStatusMsg = gsBuildString(kstrRegPreprocStarted, moClass.moAppDB, moClass.moSysSession)
                frmPrintPostStat.lblStatusMsg = gsBuildString(160303, moClass.moAppDB, moClass.moSysSession)
                             
            Case 150
                'frmPrintPostStat.lblStatusMsg = gsBuildString(kstrRegPreprocEnded, moClass.moAppDB, moClass.moSysSession)
                frmPrintPostStat.lblStatusMsg = gsBuildString(160304, moClass.moAppDB, moClass.moSysSession)
                         
            Case 200
                frmPrintPostStat.lblStatusMsg = gsBuildString(kIMModPostingStarted, moClass.moAppDB, moClass.moSysSession)
    
            Case 300
                frmPrintPostStat.lblStatusMsg = gsBuildString(kIMGLPostingStarted, moClass.moAppDB, moClass.moSysSession)
    
            Case 350
                frmPrintPostStat.lblStatusMsg = gsBuildString(kimGLPostingOver, moClass.moAppDB, moClass.moSysSession)
        
            Case 500
                frmPrintPostStat.lblStatusMsg = gsBuildString(kIMPostingCompleted, moClass.moAppDB, moClass.moSysSession)
            
            Case Else
                sbrMain.Message = ""
        End Select
    
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateBatchStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function iPopulateGLRegParams() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Sub Name          PopulateGLRegParams
    ' Written By        Ashish
    ' Purpose           This function will populate GL Register format options based on
    '                   IM Options
    ' Input Args        None
    ' Return Args       0   Successful execution
    '                   1   Options were not set  ( No Options record)
    '                   2   Integrate with GL not set
    '                   3   GLPostRgstrFormat not set
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string

'Intellisol Start
    Dim rs2 As Object            ' Holds the recordset object
    Dim rs3 As Object            ' Holds the recordset object
    Dim sSQL2 As String          ' Holds the SQL string
    Dim sSQL3 As String          ' Holds the SQL string
    Dim ipostgl As Integer       ' Holds the GL integration option
'Intellisol End
    
    sSQL = "select IntegrateWithGL, GLPostRgstrFormat, UseMultCurr from timOptions where CompanyID = " & gsQuoted(msCompanyID)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    miGLPostDetailFlag = kGLPostNone        'Initialized for safety purpose
    
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
    
'Intellisol Start
        ipostgl = rs.Field("IntegrateWithGL")
        
        sSQL2 = "select BatchKey from paimBatch where BatchKey = " & mlBatchKey
        Set rs2 = moClass.moAppDB.OpenRecordset(sSQL2, kSnapshot, kOptionNone)
        
        If Not rs2.IsEmpty Then
            sSQL3 = "select siPostIMGL from tPA00402 where CompanyID = " & gsQuoted(msCompanyID)
            Set rs3 = moClass.moAppDB.OpenRecordset(sSQL3, kSnapshot, kOptionNone)
            If Not rs3.IsEmpty Then
                'Intellisol post has precedence for integration
                ipostgl = rs3.Field("siPostIMGL")
            End If
        End If
        
        ' No need to check if null
'        If rs.Field("IntegrateWithGL") = 1 Then
        If ipostgl = 1 Then
'Intellisol End
            If rs.Field("GLPostRgstrFormat") = 2 Then
                miGLPostDetailFlag = kGLPostSummary
            ElseIf rs.Field("GLPostRgstrFormat") = 3 Then
                miGLPostDetailFlag = kGLPostDetail
            Else
                miGLPostDetailFlag = kGLPostNone
            End If
        Else
            ' Dont know whether it should be none or summary
            miGLPostDetailFlag = kGLPostNone
            iPopulateGLRegParams = 2
        End If
        
        'Check for the multy currency now.
        If rs.Field("UseMultCurr") = 1 Then
            miUseMultCurr = True
        Else
            miUseMultCurr = False
        End If
    Else
        iPopulateGLRegParams = kGLPostNone
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iPopulateGLRegParams", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function PrintGLRegister(lTaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     PrintGLRegister
    ' Written By        Ashish
    ' Purpose           Prints the GL register
    ' Input Args        lTaskNumer      The invoking task id
    ' Return values     0 Failure
    '                   1 Success
    ' ------------------------------------

Dim lRetVal As Long
Dim response As Integer
Dim sortby As String
Dim iLoop As Integer
Dim prn As Integer
Dim ReportCondition As New Collection
Dim sReportFile As String
Dim sFormulas() As String

    'if IM options is set to no gl register, exit
    If miGLPostDetailFlag = kGLPostNone Then
        PrintGLRegister = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    ' For Physical Counts with TranQty = 0
    ' there is no GL register
    If Not bGLRegNeeded() Then
        PrintGLRegister = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    PrintGLRegister = 0         ' Initializing
               
    ' Set report path to the report object
    moReport.ReportPath = msGLReportPath
    
    ' Print the GL reports
    StartGLPostReg msPrintButton, Me
    
    moReport.CascadePreview
    
    moReport.ReportPath = msIMReportPath
    
    
    Screen.MousePointer = vbArrow
    PrintGLRegister = 1
       
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintGLRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function GetReportPaths() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     GetReportPaths
    ' Written By        Ashish
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return values     True if         GL Table is populated successfully
    '                   False if        GL Table population fails.
    ' ------------------------------------

Dim iAttr As Integer
Dim lRet As Long
    
    GetReportPaths = True
    
    msIMReportPath = moClass.moSysSession.ModuleReportPath("IM")
    lRet = lValidateReportPath(msIMReportPath)
    If lRet = 0 Then
        GetReportPaths = False
        Exit Function
    End If
    
    msGLReportPath = moClass.moSysSession.ModuleReportPath("GL")
    lRet = lValidateReportPath(msGLReportPath)
    If lRet = 0 Then
        GetReportPaths = False
        Exit Function
    End If
    
    msCIReportPath = moClass.moSysSession.ModuleReportPath("CI")
    lRet = lValidateReportPath(msGLReportPath)
    If lRet = 0 Then
        GetReportPaths = False
        Exit Function
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetReportPaths", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function lValidateReportPath(sPath As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     lValidateReportPath
    ' Written By        Ashish
    ' Purpose           Validates the path on the current machine
    ' Input Args        sPath           Path to be validated
    ' Return values     0       Failure
    ' ------------------------------------

Dim iAttr As Integer
On Error GoTo ExpectedErrorRoutine

    lValidateReportPath = kFailure          ' Initializing
    
    ' Path length should be greater than 0
    If Not Len(sPath) > 0 Then
        GoTo ExpectedErrorRoutine
    Else
        ' Till the final element curtail the path elements
        If Right(sPath, 1) = BACKSLASH Then
            iAttr = GetAttr(Mid(sPath, 1, Len(sPath) - 1))
            
        Else
            ' Get attributes of the final element of the path
            iAttr = GetAttr(sPath)
            sPath = sPath & BACKSLASH
        End If
        
        ' If the final element is not a directory then
        ' Gil: Made the change at Loretta Johnson request (for compressed disk)
        ' If iAttr <> vbDirectory Then
        If iAttr And vbDirectory = 0 Then
            'MsgBox "Report Path: " & sPath & " is not a valid directory on this workstation;" & vbCr & " may not exist on this client workstation", vbCritical, "frmRegister.lValidateReportPath"
            GoTo ExpectedErrorRoutine
        End If
    End If
    lValidateReportPath = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
ExpectedErrorRoutine:
    If Err.Number = 53 Then
        ' raise error message here
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidReportPaths

        ' No need to continue with the error
        lValidateReportPath = 0
        Exit Function
    End If
    
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateReportPath", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function PopulateGLTable(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ------------------------------------------------------------------------------
    ' Function Name     PopulateGLTable
    ' Written By        Ashish
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return values     True if         GL Table is populated successfully
    '                   False if        GL Table population fails.
    ' ------------------------------------------------------------------------------
    Dim sErrDesc As String
    Dim iResult As Integer

    On Error GoTo PopulateGLTableError:
    
    Screen.MousePointer = vbHourglass

    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr moClass.moSysSession.CompanyId
        .SetInParamInt 7
        .SetOutParam lRetVal
        
        ' Run the stored procedure
        .ExecuteSP ("spimPostGLPosting")
               
        lRetVal = .GetOutParam(4)
        
        ' Check if Inventory period is valid or not.
        If lRetVal <> -1 Then
            PopulateGLTable = False
            ' Log a bug in tciError Log
            iLogError mlBatchKey, 160308, "", "", "", "", "", 1, 2
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Else
            PopulateGLTable = True
        End If
        
        .ReleaseParams
    End With

    PopulateGLTable = True
    Screen.MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
        Exit Function

PopulateGLTableError:
    sErrDesc = Err.Description
    Screen.MousePointer = vbDefault
    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, sErrDesc
    gClearSotaErr
    PopulateGLTable = False
    iLogError mlBatchKey, 160308, "", "", "", "", "", 1, 2
    Exit Function
  
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PopulateGLTable", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function PopulateIMPostTable(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     PopulateIMPostTable
    ' Written By        Ashish
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return values     True if         GL Table is populated successfully
    '                   False if        GL Table population fails.
    ' ------------------------------------
    
' Added for error handling at Tony's recommendation
Dim sErrDesc As String
Dim iResult As Integer

    On Error GoTo PopulateIMPostTableError
    
    Screen.MousePointer = vbHourglass

    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr moClass.moSysSession.CompanyId
        .SetInParamInt 7
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP ("spimPostModulePosting")
               
        lRetVal = .GetOutParam(4)
        
        ' Check if Invt period is valid or not
        If lRetVal <> -1 Then
            PopulateIMPostTable = False
            
            ' Log a bug in tciError Log
            iLogError mlBatchKey, 160307, "", "", "", "", "", 1, 2
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Else
            PopulateIMPostTable = True
        End If
        
        .ReleaseParams
    End With
    PopulateIMPostTable = True
    
    Screen.MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
        Exit Function

' Added this at Tony's recommendation
PopulateIMPostTableError:
    sErrDesc = Err.Description
    Screen.MousePointer = vbDefault
    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, sErrDesc
    gClearSotaErr
    PopulateIMPostTable = False
    iLogError mlBatchKey, 160307, "", "", "", "", "", 1, 2
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PopulateIMPostTable", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bCheckForTranRegs(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bCheckForTranRegs
    ' Written By        Ashish
    ' Purpose           Checks if batch has any inventory transactions or not
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return Values     True    If Batch has TranRegs Transactions
    '                   False   If Batch has no TranRegs Transaction
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string
    
    sSQL = " select count(*) Cnt from timPendInvtTran where BatchKey = " & lBatchKey & " and TranType NOT IN (705, 706,708,711 ) and InvtTranKey not in ( select InvtTranKey from timPendInvtTran a, timItem b where  a.ItemKey = b.ItemKey and TranType = 701 and b.ItemType = 7 and a.BatchKey = " & lBatchKey & ")"
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if there is any record in the record set or not
    ' Infact this is not needed
    If Not rs.IsEmpty Then
    
        ' No need to check if null because count will return 0 or mre but null is impossible
        If rs.Field("Cnt") >= 1 Then
            bCheckForTranRegs = True
        Else
            bCheckForTranRegs = False
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCheckForTranRegs", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bCheckForAdjustments(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     bCheckForAdjustments
    ' Written By        Ashish
    ' Purpose           Checks if batch has any adjustment transactions or not
    ' Assumption        The batch key send must be a physical count type of batch
    '                   This function simply will check if there are any rows in
    '                   timPendInvtTran for the current batch or not
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return Values     True    If Batch has adjustments
    '                   False   If Batch has no adjustments.
    ' ------------------------------------
    Dim rs As Object            ' Holds the recordset object
    Dim sSQL As String          ' Holds the SQL string
    
    sSQL = " select count(*) Cnt from timPendInvtTran where BatchKey = " & lBatchKey & ""
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if there is any record in the record set or not
    ' Infact this is not needed
    If Not rs.IsEmpty Then
    
        ' No need to check if null because count will return 0 or mre but null is impossible
        If rs.Field("Cnt") >= 1 Then
            bCheckForAdjustments = True
        Else
            bCheckForAdjustments = False
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCheckForAdjustments", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function iBatchType(lBatchKey As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     iBatchKey
    ' Written By        Ashish
    ' Purpose           Gets a the batch type for the given batch key
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return Values     0   If Invalid batch
    '                   Batch Type if batch is located
    ' ------------------------------------
    Dim rs As Object            ' Recordset object
    Dim sSQL As String          ' Holds the Sql string to be fired
    
    sSQL = "select BatchType,BatchID from tciBatchLog where BatchKey = " & lBatchKey & ""
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
    
        ' If the batch type is not null then
        If Not IsNull(rs.Field("BatchType")) Then
            iBatchType = rs.Field("BatchType")
        Else
            iBatchType = 0
        End If
        
        ' If the batch ID is not null then
        If Not IsNull(rs.Field("BatchID")) Then
            gsBatchID = rs.Field("BatchID")
        Else
            gsBatchID = ""
        End If

    Else
        iBatchType = 0
        gsBatchID = ""
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iBatchType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function iLogError(lBatchKey As Long, _
                            iStringNo As Long, _
                            s1 As String, _
                            s2 As String, _
                            s3 As String, _
                            s4 As String, _
                            s5 As String, _
                            iErrorType As Integer, _
                            iSeverity As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    ' -----------------------------------
    ' Function Name     iLogError
    ' Written By        Ashish
    ' Purpose           Logs an error
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    '                   S1 to S5        Error data
    ' ------------------------------------
    
Dim sSQL As String
Dim rs As Object
Dim lRet As Long
Dim iEntryNo As Integer

    iLogError = 0
    
    'first get max entry no for this batch
    sSQL = "SELECT MAX(EntryNo) MaxEntryNo FROM tciErrorLog WHERE BatchKey = " & Str(lBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if there is any record in the record set or not
    If Not rs.IsEmpty Then
        If Not IsNull(rs.Field("MaxEntryNo")) Then
            iEntryNo = rs.Field("MaxEntryNo") + 1
        Else
            iEntryNo = 1
        End If
    End If
    
    Set rs = Nothing
         
    'Run stored proc to populate commission register work table
      With moClass.moAppDB
          .SetInParam lBatchKey
          .SetInParam iEntryNo
          .SetInParam iStringNo
          .SetInParam s1
          .SetInParam s2
          .SetInParam s3
          .SetInParam s4
          .SetInParam s5
          .SetInParam iErrorType
          .SetInParam iSeverity
          
          .SetOutParam lRet
          .ExecuteSP ("spglCreateErrSuspenseLog")
          lRet = .GetOutParam(11)
          .ReleaseParams
      End With
     
    iLogError = lRet
    
    ' Set the error flag to true
    mbErrorOccured = True           ' This is used for printing the error log at end of the
                                    ' Posting process.
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iLogError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub PrintErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    ' -----------------------------------
    ' Function Name     PrintErrorLog
    ' Written By        Ashish
    ' Purpose           Prints the Error log
    ' Input Args
    ' ------------------------------------

    ' This part is added by Ashish to print the error log only when required
    If Not bIsErrorLogReq(mlBatchKey) Then
        Exit Sub
    End If
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam mlLanguage
        .SetOutParam lRetVal
        .ExecuteSP ("spGLPopulateErrorCmt")
        
        lRetVal = .GetOutParam(3)
    End With

    ' Set report path to the report object
    moReport.ReportPath = msIMReportPath
    
    ' Print the error  reports
    StartErrorReport msPrintButton, Me
    
    moReport.CascadePreview
    moReport.ReportPath = msIMReportPath
    
    Screen.MousePointer = vbArrow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub CleanErrorLog(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Sub Name          cleanErrorLog
    ' Written By        Ashish
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' ------------------------------------

    Dim sSQL As String

    sSQL = "delete tciErrorLog where BatchKey = " & Str(lBatchKey)
    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CleanErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function lValidateBatch(lBatchKey As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ------------------------------------------------------------------------------
    ' Function Name     lValidateBatch
    ' Written By        Ashish
    ' Input Args        lBatchKey       BatchKey of the batch to be validated
    ' Return values     0   Valid batch
    '                   1   Already Posted Batch
    '                   2   Batch on Hold
    '                   3   Batch is private to another user
    '                   4   No Such Batch exists
    '                   5   PostStatus is null in table
    '                   6   Hold was settled to null
    '                   7   Orig User was set to null
    '                   8   Private not specified
    '                   9   Batch Out of balance
    ' ------------------------------------------------------------------------------
    Dim sSQL As String              ' Holds the sql string to be executed for getting a record set
    Dim rs As Object                ' Holds the recordset object
    Dim iPostStatus As Integer      ' Post Status from database
    Dim iHold As Integer            ' Holds status of the batch
    Dim sOrigUserID As String       ' Original user for comparison with the current user
    Dim iPrivate As Integer         ' Stores the private flag
    Dim iStatus As Integer          ' Batch status
    
    mbBatchOnHold = False
    
    sSQL = "select BatchID, Status, PostStatus from tciBatchLog where BatchKey = " & lBatchKey & ""
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if the recordset is empty one or what
    If rs.IsEmpty Then
        iLogError mlBatchKey, kIMNoBatch, gsBatchID, "", "", "", "", 1, 2

        lValidateBatch = 4
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    ' No need of error handling
    gsBatchID = rs.Field("BatchID")
    
    ' Get the Post status
    iPostStatus = rs.Field("PostStatus")
    
    ' Get the status
    iStatus = rs.Field("Status")
    
    ' Added by Gil to fix bug 8470
    If iStatus = 3 Then ' Out of balance
        lValidateBatch = 9
        chkPost.Enabled = False
        chkPost.Value = 0
        mbPostFlag = False
        chkConfirm.Enabled = False
        sbrMain.Message = gsBuildString(kJrnlNotBalanced, moClass.moAppDB, moClass.moSysSession)
    End If
    
    ' If the PostStatus is null then
    If IsNull(iPostStatus) Then
        iLogError mlBatchKey, kIMInvalidPostStatus, gsBatchID, "", "", "", "", 1, 2

        lValidateBatch = 5
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    ' Now compare the Post Status
    If iPostStatus = 500 Then
        iLogError mlBatchKey, kIMPostedBatch, gsBatchID, "", "", "", "", 1, 2

        lValidateBatch = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Look for hold and origuser of the batch
    sSQL = "SELECT Hold, OrigUserID, Private FROM timBatch WITH (NOLOCK) WHERE BatchKey = " & lBatchKey
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if the recordset is empty one or what
    If rs.IsEmpty Then
        iLogError mlBatchKey, kIMNoBatch, gsBatchID, "", "", "", "", 1, 2

        lValidateBatch = 4
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    ' Get the Hold flag
    iHold = rs.Field("Hold")

    ' Now check if the batch is on hold or not
    If iHold = 1 Then
        lValidateBatch = 2
        mbBatchOnHold = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    ' Get the private flag
    iPrivate = rs.Field("Private")
    
    ' Now check if the batch is private or not
    If iPrivate = 1 Then
        ' Now get the original user
        sOrigUserID = rs.Field("OrigUserID")
       
        ' Orig user can not be null so no need to check for it
        ' Now check if the batch is on hold or not
        If iPrivate = 1 Then
            If (Trim(sOrigUserID) = Trim(moClass.moSysSession.UserId)) Then
                lValidateBatch = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            Else
                If giGetSecurityLevel(moClass.moFramework, moClass.moFramework.GetTaskID) = kSecLevelSupervisory Then
                    ' This means that the user has supervisory permissions on this task so allow him to register and post.
                Else
                    iLogError mlBatchKey, kIMPrivateBatch, gsBatchID, "", "", "", "", 1, 2

                    lValidateBatch = 3
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lValidateFiscPeriod(lBatchKey As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ------------------------------------------------------------------------------------
    ' Function Name     lValidateFiscPeriod
    ' Written By        Ashish
    ' Input Args        lBatchKey       Batch Key of the batch to be validated.
    ' Purpose           Validates the fiscal period, if open or not, exists or not.
    ' Return values     0   Valid Fiscal Period
    ' ------------------------------------------------------------------------------------
    Dim lRetVal As Long         ' Holds SP return value
    Dim iFiscPer As Integer     ' Holds the fiscal period key
    
    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr msCompanyID
        .SetOutParam lRetVal
        .SetOutParam iFiscPer
        
        .ExecuteSP ("spimIsValidFiscPer")
               
        lRetVal = .GetOutParam(3)

        lValidateFiscPeriod = 1

        ' Check if fiscal period is valid or not.
        If Not (lRetVal = 0) Then
            ' If Fiscal period is not found, then log an error in tciErrorLog.
            If lRetVal = kIMNoGLFiscPer Then
                ' Log an error in tciErrorLog.
                iLogError mlBatchKey, kIMNoFiscalPeriod, gsBatchID, "", "", "", "", 1, 2
                lValidateFiscPeriod = 0
            End If

            If lRetVal = kIMMoreThanOneGLFiscPer Then
                ' Log an error in tciErrorLog.
                iLogError mlBatchKey, kIMMoreThan1FiscPer, gsBatchID, "", "", "", "", 1, 2
                lValidateFiscPeriod = 0
            End If
            
            ' If the fiscal period for the post date is closed one, then log an error.
            If lRetVal = kIMClosedFiscPer Then
                ' Log an error in tciErrorLog.
                iLogError mlBatchKey, kVClosedPer, gsBatchID, "", "", "", "", 1, 2
                lValidateFiscPeriod = 0
            End If
        Else
            lValidateFiscPeriod = 0
        End If
        
        .ReleaseParams
    End With

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateFiscPeriod", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lValidateInvtPeriod(lBatchKey As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ----------------------------------------------------------------------------
    ' Function Name     lValidateInvtPeriod
    ' Written By        Ashish
    ' Input Args        lBatchKey       Batch Key of the batch to be validated.
    ' Purpose           Validates the Inventory period, exists or not.
    ' Return values     0   Valid Inventory Period
    ' ----------------------------------------------------------------------------
    Dim lRetVal As Long         ' Holds SP return value
    Dim iInvtPer As Integer     ' Holds the Invt period key
    
    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr msCompanyID
        .SetOutParam lRetVal
        .SetOutParam iInvtPer
        
        ' run the stored procedure
        .ExecuteSP ("spimIsValidIMPer")
               
        lRetVal = .GetOutParam(3)
        
        ' Check if Invt period is valid or not
        If Not (lRetVal = 0) Then
            lValidateInvtPeriod = 1
            
            ' If Invt period is not found then
            If lRetVal = kIMNoInvtPer Then
                ' Log a bug in tciError Log
                iLogError mlBatchKey, kIMNoPeriod, gsBatchID, "", "", "", "", 1, 1
                lValidateInvtPeriod = 0
            End If
                        
            ' Remember that Invt period can not be closed
        Else
            lValidateInvtPeriod = 0
        End If
        
        .ReleaseParams
    End With

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateInvtPeriod", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lPreProcessing(lBatchKey As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' -----------------------------------------------------------------------------
    ' Function Name     lPreProcessing
    ' Written By        Ashish
    ' Input Args        lBatchKey       Batch Key of the batch to be preprocessed.
    ' Purpose           Carries out preprocessing for the specified batch.
    ' Return values     0   Valid Inventory Period
    ' -----------------------------------------------------------------------------
    Dim lRetVal As Long             ' Holds the return value
    Dim sErrDesc As String
    Dim iResult As Integer
    
    On Error GoTo lPreProcessingError
    
    ' Me.MousePointer = vbHourglass does not work the second time around
    Screen.MousePointer = vbHourglass
    
    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr moClass.moSysSession.CompanyId
        .SetInParamInt 7
        .SetInParamInt 0
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP ("spimPostPreprocessing")
               
        lRetVal = .GetOutParam(5)
        
        ' Check if Invt period is valid or not
        If lRetVal <> -1 Then
            lPreProcessing = 1

            ' Track error
            iLogError mlBatchKey, 160312, "", "", "", "", "", 1, 2

            ' Log a bug in tciError Log
        Else
            lPreProcessing = 0
        End If
        
        .ReleaseParams
    End With

    Screen.MousePointer = vbDefault
    
'+++ VB/Rig Begin Pop +++
        Exit Function
    
' Added this at Tony's recommendation
lPreProcessingError:
    sErrDesc = Err.Description
    Screen.MousePointer = vbDefault
    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, sErrDesc
    gClearSotaErr
    lPreProcessing = 1
    iLogError mlBatchKey, 160312, "", "", "", "", "", 1, 2
    Exit Function
      
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lPreProcessing", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lInitializeReport = -1
    Dim lRetVal As Long
    
    ' Added by Gil
    If Not moSort Is Nothing Then
        moSort.CleanSortGrid
        Set moSort = Nothing
    End If
    Set moSort = New clsSort
   
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    Set moDDData = New clsDDData
    
    If Not moReport Is Nothing Then
        Set moReport = Nothing
    End If
    
    Set moReport = New clsReportEngine
    
    If moSettings Is Nothing Then
        Set moSettings = New clsSettings
    End If
    
    If moOptions Is Nothing Then
        Set moOptions = New clsOptions
    End If
    
    If moPrinter Is Nothing Then
        Set moPrinter = Printer
    End If
    
    If moSotaObjects Is Nothing Then
        Set moSotaObjects = New Collection
    End If
    
    Dim i As Integer
    
    If Not sWorkTableCollection Is Nothing Then
        Set sWorkTableCollection = Nothing
    End If

    Set sWorkTableCollection = New Collection

    With sWorkTableCollection
        Select Case glBatchType
            Case kBatchTypeIMCA, kBatchTypeMFMB
                'Cost tier adjustment batch
                .Add "timCostTierRegWrk"    'such as "tarCustListWrk"

            Case kBatchTypeIMPC
                ' Physical Count batch There will be two work tables.
                .Add "timPhysCountRegWrk"    'such as "tarCustListWrk"
                .Add "timTranRegDetWrk"    'such as "tarCustListWrk"
                'Table added by Atul, was not created when a detailed report is printed.
                'Cause for Fatal Error. Bug No : 6311
                .Add "timTranRegWrk"    'such as "tarCustListWrk"

            Case kBatchTypeIMKA
                ' Kits assembly batches
                .Add "timKitRegItemWrk"    'such as "tarCustListWrk"
                .Add "timKitRegCompWrk"    'such as "tarCustListWrk"

            Case kBatchTypeIMIT, kBatchTypeIMTA
                ' Inventory transactions and Transfer Adjustments
                ' Put three work tables.  For Kits assembly, Transfers and for Invt Tran
                ' Changed by Atul to support Sorting criteria
                If ddnFormat.ListIndex = 0 Then
                    .Add "timTranRegDetWrk"    'such as "tarCustListWrk"
                    .Add "timTranRegWrk"    'such as "tarCustListWrk"
                Else
                    .Add "timTranRegWrk"    'such as "tarCustListWrk"
                    .Add "timTranRegDetWrk"    'such as "tarCustListWrk"
                End If
                'Since Sorting is implemented only on Transaction register transfer worktable
                'is made the second worktable in the collection
                .Add "timTransRegWrk"    'such as "tarCustListWrk"
                .Add "timKitRegItemWrk"    'such as "tarCustListWrk"
                .Add "timKitRegCompWrk"    'such as "tarCustListWrk"
                
                If mbCalledFromPA Then
                    .Add "tPAInvtRegPostWrk"
                End If
                
            Case Else
                ' This case will be traced when the task gets invoked from PostIMBatches
                .Add "timTranRegWrk"    'such as "tarCustListWrk"
                .Add "timKitRegItemWrk"    'such as "tarCustListWrk"
                .Add "timKitRegCompWrk"    'such as "tarCustListWrk"
                .Add "timTransRegWrk"    'such as "tarCustListWrk"
                .Add "timCostTierRegWrk"    'such as "tarCustListWrk"
                .Add "timPhysCountRegWrk"   ' Added by Gil to support Phys Counts in PostIMBatches
        End Select
    End With

    If mbPeriodEnd Then
        moReport.UI = False
    End If

    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    'CUSTOMIZE: set Groups and Sorts as required
    'note there are two optional parameters: iNumGroups,iNumSorts for lInitSort
    If (moSort.lInitSort(Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFataSortInit
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
   
    'Added by Atul to display only the required Sort fields
    Select Case glBatchType
        Case kBatchTypeIMCA, kBatchTypeMFMB
            If ddnFormat.ListIndex = 0 Then    ' Detail
                moSort.lDeleteSort "TranAmt", "timPendInvtTran"
                moSort.lDeleteSort "TranID", "timPendInvtTran"
                moSort.lDeleteSort "UnitMeasID", "tciUnitMeasure"
                moSort.lDeleteSort "WhseID", "timWarehouse"
                moSort.lDeleteSort "ValuationMeth", "timItem"
            Else                               ' Summary
                moSort.lDeleteSort "TranAmt", "timPendInvtTran"
                moSort.lDeleteSort "TranDate", "timPendInvtTran"
                moSort.lDeleteSort "TranID", "timPendInvtTran"
                moSort.lDeleteSort "UnitMeasID", "tciUnitMeasure"
                moSort.lDeleteSort "WhseID", "timWarehouse"
                moSort.lDeleteSort "ValuationMeth", "timItem"
                moSort.lDeleteSort "ItemID", "timItem"
            End If

        Case kBatchTypeIMPC
            moSort.lDeleteSort "WhseID", "timWarehouse"
            moSort.lInsSort "TranQty", "timPendInvtTran", moDDData, gsBuildString(kIMQuantity, moClass.moAppDB, moClass.moSysSession)
            moSort.lDeleteSort "ValuationMeth", "timItem"
            moSort.lInsSort "ItemType", "timItem", moDDData

        Case kBatchTypeIMKA
            moSort.lDeleteSort "WhseID", "timWarehouse"

        Case kBatchTypeIMIT, kBatchTypeIMTA
            If ddnFormat.ListIndex = 0 Then
                moSort.lDeleteSort "TranAmt", "timPendInvtTran"
                moSort.lDeleteSort "SalesAmt", "timPendInvtTran"
                moSort.lDeleteSort "UnitMeasID", "tciUnitMeasure"
                moSort.lDeleteSort "ReasonCodeID", "tciReasonCode"
                moSort.lDeleteSort "WhseID", "timWarehouse"
            Else
                moSort.lDeleteSort "TranAmt", "timPendInvtTran"
                moSort.lDeleteSort "SalesAmt", "timPendInvtTran"
                moSort.lDeleteSort "UnitMeasID", "tciUnitMeasure"
                moSort.lDeleteSort "ReasonCodeID", "tciReasonCode"
                moSort.lDeleteSort "WhseID", "timWarehouse"
            End If
            If mbKitsRegNeeded Then
                moSort.lDeleteSort "TranType", "timPendInvtTran"
                moSort.lDeleteSort "ItemType", "timItem"
            Else
                moSort.lInsSort "ItemType", "timItem", moDDData
            End If
        Case Else
    End Select

    ' Lock some columns
    If glBatchType = kBatchTypeIMCA Or glBatchType = kBatchTypeMFMB Then
        If ddnFormat.ListIndex = 0 Then
            ' Cost Tier Adjustements and Detail
            gGridLockColumn grdSort, 3      ' Subtotals column
            gGridLockColumn grdSort, 4      ' Page Breaks column
        Else
            ' Cost Tier Adjustements and Summary
            gGridUnlockColumn grdSort, 3      ' Subtotals column
            gGridUnlockColumn grdSort, 4      ' Page Breaks column
        End If
    End If
    
    If glBatchType = kBatchTypeIMPC Then
        gGridLockColumn grdSort, 3      ' Subtotals column
        gGridLockColumn grdSort, 4      ' Page Breaks column
    End If
    
    lInitializeReport = 0

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmRegister2"
End Function

Private Sub cboReportSettings_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++
    mbLoadingSettings = True
    moSettings.bLoadReportSettings
    mbLoadingSettings = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReportSettings_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboReportSettings, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If Len(cboReportSettings.Text) > 40 Then
        Beep
        KeyAscii = 0
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReportSettings_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Skip +++
    mbCancelShutDown = False
    mbLoadSuccess = False
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iNavControl As Integer

#If CUSTOMIZER Then
    If (Shift = 4) Then
        gProcessFKeys Me, keycode, Shift
    End If
#End If

    If Shift = 0 Then
        Select Case keycode
            Case vbKeyF1
                If Me.ActiveControl.Name = "tbrMain" Then
                    ' gDisplayWhatsThisHelp uses the control's hwnd property
                    ' but tbrSOTAMain does not have such property
                    gDisplayFormLevelHelp Me
                Else
                    'gDisplayWhatsThisHelp Me, Me.ActiveControl
                    gProcessFKeys Me, keycode, Shift
                End If
            Case vbKeyF5
                'iNavControl = moSelect.SelectionGridF5KeyDown(Me.ActiveControl)
                If iNavControl > 0 Then
                    navControl_GotFocus iNavControl
                End If
        End Select
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Skip +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long             'Captures Long function return value(s).
    Dim iRetVal As Integer          'Captures Integer function return value(s).
    Dim sRptProgramName As String   'Stores base filename for identification purposes.
    Dim sModule As String           'The report's module: AR, AP, SM, etc.
    Dim sDefaultReport As String    'The default .RPT file name
    
    mbLoading = True
    mbLoadSuccess = False
           
    'Assign system object properties to module-level variables for easier access
    With moClass.moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
    End With
    
    ' Get the batch parameters only when this is called from register post button of a batch
    ' Other wise do the same in Handle tool bar click
    If Not gbPostIMBatchesFlag Then
        ' This is the new function that gets all batch parameters.
        If Not gbGetIMBatchParameters(moClass.moAppDB, mlBatchKey) Then
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
            Exit Sub
        End If
    End If

    'CUSTOMIZE: assign defaults for this project
    sRptProgramName = "IMZTG005a" 'such as rpt001
    sModule = "IM"
    sDefaultReport = "IMZTG005a.RPT" 'such as rpt001
    
    'Set local flag to value of class module's bPeriodEnd flag.
    mbPeriodEnd = moClass.bPeriodEnd
    
    Set sRealTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data for the report.  Primary table should appear first.

    With sRealTableCollection
        .Add "timPhysCountTran"
        .Add "timPendInvtTran"
        .Add "timItem"
    End With

    With sRealTableCollection
        Select Case glBatchType
            Case kBatchTypeIMCA, kBatchTypeMFMB
                'Cost tier adjustment batch

            Case kBatchTypeIMPC
                ' Physical Count batch There will be two work tables.
                With sRealTableCollection
                    .Add "timInvtLot"
                    .Add "timInvtSerial"
                    .Add "timWhseBin"
                End With
    
            Case kBatchTypeIMKA
                ' Kits assembly batches
    
            Case kBatchTypeIMIT
                ' Inventory transactions
                
            Case Else
                ' This case will be traced when the task gets invoked from PostIMBatches
        End Select
    End With

    ' Getting the report paths
    If Not GetReportPaths Then
        Exit Sub
    End If
   
    'CUSTOMIZE:  Invoke Add method of sWorkTableCollection for each table which
    'will serve as a work table for the report.
    
    'Moved from here to lInitializeReport because on changing the type of report format,
    'Sort options needs to be changed.(Making Report table as the first table in
    'sWorkTableCollection etc..)
    If Not gbPostIMBatchesFlag Then
        ' Get the batch type and based on it display changes.
        ' Since the Batch ID will be available only if called from Register button and not
        ' when called by Post IM Batches.
        MakeUIChanges
    End If

    ' We must determine if the Kit Register is needed because the sorts will depend on it.
    mbKitsRegNeeded = bKitsRegNeeded
    
    lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
        GoTo badexit
    End If

    'Perform initialization of form and control properties only if you need to display UI.
    If Not mbPeriodEnd Then
        miOldFormHeight = 7500
        miOldFormWidth = 9600
        miMinFormHeight = 7500
        miMinFormWidth = 9600
        
        Me.Height = 7500 'max height: 7200
        Me.Width = 9600  'max width: 9600
        
        'Set form caption using localized text.
        'CUSTOMIZE:  Pass in appropriate string constant from StrConst.bas as first argument.
        Select Case glBatchType
        
            Case kBatchTypeIMCA, kBatchTypeMFMB
                'Cost tier adjustment batch
                Me.Caption = gsBuildString(kIMCostTierAdjReg, moClass.moAppDB, moClass.moSysSession) + " (" + gsBatchID + ")"
                
            Case kBatchTypeIMPC
                ' Physical Count batch There will be two work tables.
                Me.Caption = gsBuildString(kimPhysCountTranReg, moClass.moAppDB, moClass.moSysSession) + " (" + gsBatchID + ")"
                
            Case kBatchTypeIMIT
                ' Inventory transactions
                Me.Caption = gsBuildString(kIMInvtTranReg, moClass.moAppDB, moClass.moSysSession) + " (" + gsBatchID + ")"
                
            Case kBatchTypeIMKA
                ' all others will probably have the same work table.
                Me.Caption = gsBuildString(kIMKitsAssemblyReg, moClass.moAppDB, moClass.moSysSession) + " (" + gsBatchID + ")"
                
            Case kBatchTypeIMTA
                'Transfer Discrepancy Adjustment Batch
                Me.Caption = gsBuildString(kIMTransferDiscrepAdjReg, moClass.moAppDB, moClass.moSysSession) + " (" + gsBatchID + ")"
        End Select
        
        'Initialize context menu.  WinHook2 will capture right mouse clicks to activate
        'context-sensitive help.
        Set moContextMenu = New clsContextMenu
        With moContextMenu
            ' **PRESTO ** Set .Hook = Me.WinHook1
            Set .Form = frmRegister2
            .Init
        End With
    End If
    
    'CUSTOMIZE:  Add all controls on Options page for which you will want to save settings
    'to the moOptions collection, using its Add method.
    With moOptions
         .Add chkConfirm
         .Add chkPost
         .Add chkRegister
         .Add ddnOutput
         .Add ddnFormat
    End With
    
    'Initialize the report settings combo box.
    If Not gbPostIMBatchesFlag Then
        moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, , tbrMain
    Else
        'as the moSort will not be populated when control comes
        moSettings.Initialize Me, cboReportSettings, , , moOptions, , tbrMain
    End If
    
    'If the report is being invoked from the Period End Processing task, attempt to load the
    'setting named "Period End".  If such setting does not exist, or if not being invoked from
    'the Period End Processing task, load last used setting.
    If mbPeriodEnd Then
        moSettings.lLoadPeriodEndSettings
    End If
    
    '--Setup Sage MAS 500 Status bar control
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.BrowseVisible = False
    sbrMain.Status = SOTA_SB_START

    ' Added by Gil to support new cost tier adjustment posting
    ' Now the preprocessing and posting must be part of the same SQL transaction
    ' So no confirm check box
    If glBatchType = kBatchTypeIMCA Or glBatchType = kBatchTypeMFMB Then
        chkConfirm.Value = vbUnchecked
        chkConfirm.Enabled = False
        chkConfirm.Visible = False
    End If
    
    'Update status flags to indicate successful load has completed.
    mbLoadSuccess = True
    mbLoading = False

    'SGS-JMM 1/31/2011
    On Error Resume Next
    Set cypHelper = CreateObject("CypressIntegrationHelper.MAS500Form")
    If Err.Number = 0 Then
        With cypHelper
            .Init Me, moClass, msCompanyID
            .AddToolbarButton 0
        End With
    Else
        Set cypHelper = Nothing
    End If
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
badexit:

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub MakeUIChanges()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ----------------------------------------------------------------------------
    ' Sub Name          MakeUIChanges
    ' Written By        Ashish
    ' Input Args        No Args
    ' Purpose           Visiblity logic based on batch type
    ' ----------------------------------------------------------------------------
    ' Making the tool bar changes.
    ' Adding Delete, Save, Defer buttons and removing cancel and Exit button
    tbrMain.AddButton kTbSave, 3
    tbrMain.AddButton kTbDelete, 4
    tbrMain.RemoveButton kTbClose
    tbrMain.RemoveButton kTbPrintSetup
    
    'If batch type is IMPC then enable two check boxes in the UI.
    ddnOutput.AddItem gsBuildString(kIMPrinter, moClass.moAppDB, moClass.moSysSession), 0
    ddnOutput.ItemData(0) = kIMPrinter
    ddnOutput.AddItem gsBuildString(kImScreen, moClass.moAppDB, moClass.moSysSession), 1
    ddnOutput.ItemData(1) = kImScreen
    ddnOutput.ListIndex = 0
            
    ddnFormat.AddItem gsBuildString(kIMSummary, moClass.moAppDB, moClass.moSysSession), 2
    ddnFormat.ItemData(0) = kIMSummary
    ddnFormat.AddItem gsBuildString(kIMDetail, moClass.moAppDB, moClass.moSysSession), 3
    ddnFormat.ItemData(1) = kIMDetail
    ddnFormat.ListIndex = 0
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MakeUIChanges", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If

    Dim iRet As Integer
   
    UndoCostTiers mlBatchKey
    UnloadPAGLPosting mlBatchKey
    
    mbCancelShutDown = False
    
    moReport.CleanupWorkTables

    'Unlock batch if in UI mode
    If Not (miMode = kPostXXBatches) Then
      glUnlockLogical moClass.moAppDB, mlLockID
    End If
    
    If Not mbPeriodEnd Then
        moSettings.SaveLastSettingKey
    End If
    
    If moClass.mlError = 0 Then
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
        If Not mbPeriodEnd And gbActiveChildForms(Me) Then
            GoTo CancelShutDown
        End If
    End If
    
    If UnloadMode <> vbFormCode Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing

        Case Else
            'kFrameworkShutDown
            'Do Nothing
    End Select
    
    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.Dispose
    Set cypHelper = Nothing
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    gUnloadChildForms Me
    giCollectionDel moClass.moFramework, moSotaObjects, -1
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Skip +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    UnloadObjects
End Sub

Private Sub UnloadObjects()
'+++ VB/Rig Skip +++
    Set moSettings = Nothing
    Set moSort = Nothing
    Set moReport = Nothing
    Set moContextMenu = Nothing
    Set moClass = Nothing
    Set moDDData = Nothing
    Set moOptions = Nothing
    Set moPrinter = Nothing
    Set sRealTableCollection = Nothing
    Set sWorkTableCollection = Nothing
    Set moSotaObjects = Nothing
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
            grdSort, fraSort, fraMessage, pctMessage, txtMessageHeader(0), txtMessageHeader(1), txtMessageHeader(2), txtMessageHeader(3), txtMessageHeader(4)

        'Resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
            grdSort, fraSort, fraMessage, pctMessage, txtMessageHeader(0), txtMessageHeader(1), txtMessageHeader(2), txtMessageHeader(3), txtMessageHeader(4)
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub txtMessageHeader_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMessageHeader(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkConfirm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRegister_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkRegister, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRegister_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRegister_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkRegister, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRegister_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportSettings_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportSettings_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFormat_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnFormat, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFormat_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFormat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFormat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFormat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFormat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnOutput, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnOutput, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

'************************ Sort Stuff *******************************
Private Sub grdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_KeyUp(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridKey keycode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridChange Col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Added by Atul on Dec'18 to disable click on CheckBox For Physical Count Batch
    'No Subtotalling and PageBreaks in PC trantype
    If (Col = 3 Or Col = 4) And glBatchType = kBatchTypeIMPC Then
        Exit Sub
    End If
  
    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.SortGridGotFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.SortGridKeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'*********************** End Sort Stuff *****************************

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Public Sub UndoCostTiers(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long             'Holds the return value.
    
    On Error GoTo UndoCostTiersError
    
    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr moClass.moSysSession.CompanyId
        .SetInParamInt 7
        .SetOutParam lRetVal
        
        .ExecuteSP ("spimPostAPIUndoCostTiersUpdate")
               
        lRetVal = .GetOutParam(4)
        
        .ReleaseParams
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub
    
UndoCostTiersError:
    Exit Sub
      
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUndoCostTiers", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function CheckBalance() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long

    CheckBalance = kSuccess
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam mlBatchKey
        .SetOutParam lRetVal

        .ExecuteSP ("sparCheckBalance")
        
        lRetVal = .GetOutParam(3)
        .ReleaseParams
    End With
    
    CheckBalance = lRetVal
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckBalance", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lPostCostTiers(lBatchKey As Long, bPostFlag As Boolean) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long             'Holds the return value.
    Dim iPostFlag As Integer
    Dim sErrDesc As String
    Dim iResult As Integer
    
    On Error GoTo lPostCostTiersError
    
    Screen.MousePointer = vbHourglass

    If bPostFlag Then
        iPostFlag = 1
    Else
        iPostFlag = 0
    End If
    
    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr moClass.moSysSession.CompanyId
        .SetInParamInt 7
        .SetInParamInt iPostFlag
        .SetOutParam lRetVal

        .ExecuteSP ("spimPostCostTierAdjBatch")

        lRetVal = .GetOutParam(5)

        ' Check if Invt period is valid or not.
        If lRetVal <> -1 Then
            lPostCostTiers = 1

            ' Log a bug in tciError Log.
            iLogError mlBatchKey, 160306, "", "", "", "", "", 1, 2
        Else
            lPostCostTiers = 0
        End If
        
        .ReleaseParams
    End With

    Screen.MousePointer = vbDefault

'+++ VB/Rig Begin Pop +++
        Exit Function
    
lPostCostTiersError:
    sErrDesc = Err.Description
    Screen.MousePointer = vbDefault
    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, sErrDesc
    gClearSotaErr
    lPostCostTiers = 1
    iLogError mlBatchKey, 160306, "", "", "", "", "", 1, 2
    Exit Function
      
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lPostCostTiers", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function LockBatch() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sEntity As String       'Entity name to be passed to logical lock routines

    LockBatch = False
    sEntity = kLockEntBatch & Format$(mlBatchKey)

    If (glLockLogical(moClass.moAppDB, sEntity, kLockTypeExclusive, mlLockID) <> 0) Then
           giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, gsBatchID
           LockBatch = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        LockBatch = True
        gSetSotaErr Err, sMyName, "LockBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub ReorganizeWorkTables(iReportType As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If gbPostIMBatchesFlag Then Exit Sub

    If Not sWorkTableCollection Is Nothing Then
        Set sWorkTableCollection = Nothing
    End If

    Set sWorkTableCollection = New Collection

    With sWorkTableCollection
        Select Case iReportType
           Case kWorkTableIT     ' Inventory Transaction Register
               ' Put three work tables.  For Kits assembly, Transfers and for Invt Tran
                If ddnFormat.ListIndex = 0 Then
                    .Add "timTranRegDetWrk"
                    .Add "timTranRegWrk"
                Else
                    .Add "timTranRegWrk"
                    .Add "timTranRegDetWrk"
                End If
    
                .Add "timTransRegWrk"
                .Add "timKitRegItemWrk"
                .Add "timKitRegCompWrk"
            
            Case kWorkTableTR     ' Transfer Register
                ' Put three work tables.  For Kits assembly, Transfers and for Invt Tran
                .Add "timTransRegWrk"
    
                If ddnFormat.ListIndex = 0 Then
                    .Add "timTranRegDetWrk"
                    .Add "timTranRegWrk"
                Else
                    .Add "timTranRegWrk"
                    .Add "timTranRegDetWrk"
                End If
    
                .Add "timKitRegItemWrk"
                .Add "timKitRegCompWrk"
                            
            Case kWorkTableKA     ' Kit Assembly Register
                ' Put three work tables.  For Kits assembly, Transfers and for Invt Tran
                .Add "timKitRegItemWrk"
                .Add "timKitRegCompWrk"
                
                If ddnFormat.ListIndex = 0 Then
                    .Add "timTranRegDetWrk"
                    .Add "timTranRegWrk"
                Else
                    .Add "timTranRegWrk"
                    .Add "timTranRegDetWrk"
                End If
                
                .Add "timTransRegWrk"
            
            Case Else
                ' This case will be traced when the task gets invoked from PostIMBatches
                .Add "timTranRegWrk"
                .Add "timKitRegItemWrk"
                .Add "timKitRegCompWrk"
                .Add "timTransRegWrk"
                .Add "timCostTierRegWrk"
                .Add "timPhysCountRegWrk"
        End Select
    End With
    
    Exit Sub
   
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ReorganizeWorkTables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SaveCurrentSorts()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sSortField As String
    Dim sSortOrder As String
    Dim lSubTotal As Long
    Dim lPageBreak As Long
    
    If gbPostIMBatchesFlag Then Exit Sub

    Set msSortField = Nothing
    Set msSortOrder = Nothing
    Set mlSubTotal = Nothing
    Set mlPageBreak = Nothing
    
    Set msSortField = New Collection
    Set msSortOrder = New Collection
    Set mlSubTotal = New Collection
    Set mlPageBreak = New Collection
    
    For lRow = 1 To grdSort.MaxRows
        sSortField = Trim$(gsGridReadCellText(grdSort, lRow, kColSort))

        If Len(sSortField) <> 0 Then
            sSortOrder = Trim$(gsGridReadCellText(grdSort, lRow, kColAscending))
            lSubTotal = CLng(gsGridReadCell(grdSort, lRow, kColSubtotal))
            lPageBreak = CLng(gsGridReadCell(grdSort, lRow, kColPageBreak))
            
            msSortField.Add sSortField
            msSortOrder.Add sSortOrder
            mlSubTotal.Add lSubTotal
            mlPageBreak.Add lPageBreak
        End If
    Next lRow

    Exit Sub
   
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SaveCurrentSorts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub ReinitializeSorts(iReportType As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If gbPostIMBatchesFlag Then Exit Sub
    
    ReorganizeWorkTables iReportType
    
    If Not moSort Is Nothing Then
        moSort.CleanSortGrid
    End If

    Set moSort = New clsSort
    
    'CUSTOMIZE: Set Groups and Sorts as required.
    'Note there are two optional parameters: iNumGroups,iNumSorts for lInitSort.
    If (moSort.lInitSort(Me, moDDData) = kFailure) Then
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
        Exit Sub
    End If
     
    If ddnFormat.ListIndex = 0 Then
        moSort.lDeleteSort "TranAmt", "timPendInvtTran"
        moSort.lDeleteSort "SalesAmt", "timPendInvtTran"
        moSort.lDeleteSort "UnitMeasID", "tciUnitMeasure"
        moSort.lDeleteSort "ReasonCodeID", "tciReasonCode"
        moSort.lDeleteSort "WhseID", "timWarehouse"
    Else
        moSort.lDeleteSort "TranAmt", "timPendInvtTran"
        moSort.lDeleteSort "SalesAmt", "timPendInvtTran"
        moSort.lDeleteSort "TranAmt", "timPendInvtTran"
        moSort.lDeleteSort "SalesAmt", "timPendInvtTran"
        moSort.lDeleteSort "UnitMeasID", "tciUnitMeasure"
        moSort.lDeleteSort "ReasonCodeID", "tciReasonCode"
        moSort.lDeleteSort "WhseID", "timWarehouse"
    End If

    If mbKitsRegNeeded Then
        moSort.lDeleteSort "TranType", "timPendInvtTran"
        moSort.lDeleteSort "ItemType", "timItem"
    Else
        moSort.lInsSort "ItemType", "timItem", moDDData
    End If
           
    RestoreCurrentSorts
    
    Exit Sub
   
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ReinitializeSorts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub RestoreCurrentSorts()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sSortField As String
    Dim sSortOrder As String
    Dim lSubTotal As Long
    Dim lPageBreak As Long
    
    If gbPostIMBatchesFlag Then Exit Sub

    For lRow = 1 To msSortField.Count
        sSortField = msSortField.Item(lRow)
        sSortOrder = msSortOrder.Item(lRow)
        lSubTotal = mlSubTotal.Item(lRow)
        lPageBreak = mlPageBreak.Item(lRow)
        
        gGridUpdateCellText grdSort, lRow, kColSort, sSortField
        gGridUpdateCellText grdSort, lRow, kColAscending, sSortOrder
        gGridUpdateCell grdSort, lRow, kColSubtotal, CLng(lSubTotal)
        gGridUpdateCell grdSort, lRow, kColPageBreak, CLng(lPageBreak)
        gGridUnlockCell grdSort, kColAscending, lRow
    
    Next lRow

    Exit Sub
   
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RestoreCurrentSorts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub UnloadPhysCounts(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long
    
    If glBatchType <> kBatchTypeIMPC Then Exit Sub
    
    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetOutParam lRetVal
        .ExecuteSP ("spimPhysCountDeleteTransactions")
               
        lRetVal = .GetOutParam(2)
        .ReleaseParams
        
        If lRetVal <> 1 Then
            ' Log a bug in tciError Log
            iLogError mlBatchKey, kPhysCountPurgeFailed, "", "", "", "", "", 1, 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
 
    End With


'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UnloadPhysCounts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetBatchStatus(lBatchKey As Long, iStatus As Integer, iPostStatus As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    
    sSQL = "UPDATE tciBatchLog "
    sSQL = sSQL & "SET Status = " & CStr(iStatus)
    sSQL = sSQL & ", PostStatus = " & CStr(iPostStatus) & " "
    sSQL = sSQL & "WHERE BatchKey = " & CStr(lBatchKey)
    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetBatchStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bGLRegNeeded() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim rs As Object            ' Holds the recordset object.
    Dim sSQL As String          ' Holds the SQL string.

    If glBatchType <> kBatchTypeIMPC Then
        bGLRegNeeded = True
        Exit Function
    End If

    bGLRegNeeded = False

    sSQL = "SELECT COUNT(*) CNT FROM tglPosting WHERE BatchKey = " & mlBatchKey
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    ' Check if there is any record in the record set or not.
    If Not rs.IsEmpty Then
        ' No need to check if null.
        If rs.Field("CNT") > 0 Then
            bGLRegNeeded = True
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGLRegNeeded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Private Function iSetPrintedFlag(iFlag As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long
    Dim iResponse As Integer
    Dim sMessageText As String

    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam iFlag
        .SetInParam msCompanyID
        .SetOutParam lRetVal

        .ExecuteSP ("spSetRegPrintFlg")

        Sleep 0&

        lRetVal = .GetOutParam(4)
        .ReleaseParams
    End With

    If lRetVal <> 0 Then
        If miMode <> kPostXXBatches Then
            sbrMain.Status = SOTA_SB_START
            Screen.MousePointer = vbArrow
            sMessageText = gsBuildString(160303, moClass.moAppDB, moClass.moSysSession)
            iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgFlagNotFound)
        End If
    End If

    iSetPrintedFlag = lRetVal

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iSetPrintedFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidFiscPeriod(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' ----------------------------------------------------------------------------------
    ' Function Name     bIsValidFiscPeriod
    ' Input Args        lBatchKey -- Batch Key of the batch to be validated.
    ' Purpose           Validates the Fiscal Period.  If open or not, exists or not.
    '                   Raise a message that posting may take longer if user chooses to
    '                   create the next Fiscal Year.
    ' ----------------------------------------------------------------------------------
    Dim lRetVal As Long         ' Holds the SP return value.
    Dim iFiscPer As Integer     ' Holds the Fiscal Period key.
    Dim iMsgReturn As Integer

    With moClass.moAppDB
        .SetInParam lBatchKey
        .SetInParamStr msCompanyID
        .SetOutParam lRetVal
        .SetOutParam iFiscPer

        .ExecuteSP ("spimIsValidFiscPer")

        lRetVal = .GetOutParam(3)

        'Check if the fiscal period is valid or not.
        If Not (lRetVal = 0) Then
            Select Case lRetVal
                Case kIMNoBatchFound
                    'The Batch row in timBatch does not exist (should never happen).
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidBatchKey

                    'Log the error in tciErrorLog too.
                    iLogError mlBatchKey, kIMNoBatch, gsBatchID, "", "", "", "", 1, 2
                    bIsValidFiscPeriod = False
                
                Case kIMNoGLFiscPer
                    'The General Ledger fiscal period that corresponds to the Post Date does not exist.
                    'Please use Set Up Fiscal Calendar in General Ledger to set up the appropriate fiscal period.
                    giSotaMsgBox Me, moClass.moSysSession, kmsgIMNoGLFiscalPeriodExists
                    bIsValidFiscPeriod = False

                Case kIMMoreThanOneGLFiscPer
                    'More than one General Ledger Fiscal Period exists corresponding to the Posting Date.
                    giSotaMsgBox Me, moClass.moSysSession, kmsgIMMoreThan1GLFiscPerExists

                    'Log the error in tciErrorLog too.
                    iLogError mlBatchKey, kIMMoreThan1FiscPer, gsBatchID, "", "", "", "", 1, 2
                    bIsValidFiscPeriod = False

                Case kIMClosedFiscPer
                    'The General Ledger Fiscal Period for the Posting Date is closed.  Please use Set Up Fiscal Calendar to open the fiscal period.
                    giSotaMsgBox Me, moClass.moSysSession, kmsgIMGLFiscPerClosed

                    'Log the error in tciErrorLog too.
                    iLogError mlBatchKey, kVClosedPer, gsBatchID, "", "", "", "", 1, 2
                    bIsValidFiscPeriod = False
            End Select
        Else
            bIsValidFiscPeriod = True
        End If

        .ReleaseParams
    End With

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidFiscPeriod", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub UnloadPAGLPosting(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '****  N O T E  ****
    'If only the register prints...before the table tpaglposting was not being cleared with info from batch
    'When the batch is posted the table tpaglposting is cleared ...however tpaglposting now has duplicate values and tpagltranref is populated with values from tpaglposting
    'causing duplicate entries in the tpagltranref table.
    'This procedure will always remove the entries from tpaglposting for the specific batch.
    Dim sSQL As String

    sSQL = "DELETE tPAGLPosting WHERE BatchKey = " & Str(lBatchKey)
    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UnloadPAGLPosting", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'SGS-JMM 1/31/2011
Public Function GetCypressQuery() As String
    Dim batchID As String
    
    On Error Resume Next
    batchID = moClass.moSysSession.AppDatabase.Lookup("BatchID", "tciBatchLog", "BatchKey = " & Me.CONTROLS(0).Parent.mlBatchKey)
    GetCypressQuery = "[Company ID] = """ & moClass.moSysSession.CompanyId & """ and [Batch ID] = """ & batchID & """"
End Function
'End SGS-JMM



