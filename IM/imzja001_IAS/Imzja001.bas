Attribute VB_Name = "basInvtRegister"
Attribute VB_Name = "basInvtRegister"
'***********************************************************************
'     Name: basReport
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: EDT
'     Mods: xx/xx/xx
'***********************************************************************
Option Explicit

' GL Register options
    Private Const kGLPostNone       As Integer = 1
    Private Const kGLPostSummary    As Integer = 2
    Private Const kGLPostDetail     As Integer = 3
    Private Const kBatchTypeMFMB    As Integer = 9004
    Private Const kTranTypeMFMB     As Integer = 9004
    Public Const BACKSLASH = "\"

Public gbSkipPrintDialog As Boolean      '   whether to show Print Dialog window. HBS.
Public gbCancelPrint As Boolean     '   whether user selected cancel on print dialog. HBS.
Public gbPostIMBatchesFlag As Boolean   ' Holds whether the invocation is from Post Im bates or not

Const VBRIG_MODULE_ID_STRING = "IMZJA001.BAS"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basInvtRegister"
End Function

Public Sub Main()
'+++ VB/Rig Skip +++
   Exit Sub
End Sub

Public Sub StartErrorReport(sButton As String, frm As Form)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim iJob As Integer
    Dim iRetVal As Integer
    Dim sTitle1 As String, sTitle2 As String

    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    frm.moReport.ReportFileName() = "imzja001.rpt"
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
    
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    'DO NOT HARDCODE ANY TEXT
    'If frm.glBatchType = BATCH_TYPE_CM Then
        sTitle1 = "Inventory Transaction Register"
   ' ElseIf frm.glBatchType = BATCH_TYPE_DE Then
       ' sTitle1 = "Deposit Register"
    'Else
        'sTitle1 = "Bank Account Reconciliation Register"
    'End If
    
    frm.moReport.ReportTitle1() = sTitle1
    frm.moReport.ReportTitle2() = "Error Log Listing"
    
    'automated means of labeling subtotal fields.
    'this is not manditory if you choose not to use this feature
    frm.moReport.UseSubTotalCaptions() = 0
    frm.moReport.UseHeaderCaptions() = 0
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If

   
    '********* the following is specific to ARZRA001 *************'
    'you can set report formulas and such here
        
    '********* End of ARZRA001 *************'
    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BYs with new ones. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    'this sub will set any labels for the table fields on the RPT file if they exist.
    'Labels must match the column name from the parent table as well as prefixed by "lbl".
    'For example, to label customer ID from tarCustomer table, a formula on the report
    'must exist with the name, lblCustID. At design time fill this formula with empty quotes.
    'this is not a manitory function. Often the captions from the DD will not be exactly
    'what you want to appear on the report.  If this is the case, there is a function
    'available in the Crystal design environment called FreeFormText that takes a string
    'table constant as a parameter to place text on the report.
    frm.moReport.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
'    frm.moReport.SelectString = frm.moSelect.sGetUserReadableWhereClause(kLenPortrait)
    
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    'if using work tables, it's necessary to restrict the query to the data produced by your
    '"run".  The engine used to do this behind the scenes but as the engine is used in more
    'different ways now, it's better to make the call explicitly yourself.  This permits
    'restriction by any column and doesn't force the inclusing of a column, SessionID in the
    'work tables.
    'only for the report that not use sort grid
    'frm.moReport.SortsUsed() = 0
    If (frm.moReport.lRestrictBy("{tciErrorLog.SessionID} = " & frm.mlBatchKey) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If

    frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint   '   Added parameters. HBS.

    If sButton = kTbPrint Then
        gbSkipPrintDialog = True    '   don't show print dialog more than once. HBS
    End If

    'frm.aniStatus.SetStatusStart
    'frm.moReport.ProcessReport frm, sButton
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartErrorReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Function lStartReportCA(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional lSettingKey As Variant, _
    Optional iFileType As Variant, Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim sSelect As String
    Dim sInsert As String
    Dim lRetVal As Long
    Dim bValid As Boolean
    Dim iNumTablesUsed As Integer
    Dim RptFileName As String
    Dim lBadRow As Long
    Dim ReportObj As clsReportEngine
    Dim DBObj As Object
    Dim SettingsObj As clsSettings
    Dim sTitle As String
    Dim lTranType As Integer

    On Error GoTo badexit

    lStartReportCA = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'If web, get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            If (SettingsObj.bLoadSettingsByKey(CLng(lSettingKey)) = False) Then
                GoTo badexit
            End If
        End If
    End If

    'sTablesUsed = "timPosting"
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    sTablesUsed = "timPosting, timInvtTranCost"
    
    'Set the tran type based on the batch type (IM Cost Tier Adj or MF BOM Cost Rollup)
    If glBatchType = kBatchTypeMFMB Then
        lTranType = kTranTypeMFMB
    Else
        lTranType = kTranTypeIMCA
    End If

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
    sSelect = "SELECT timPosting.InvtTranKey, timPosting.BatchKey, timPosting.WhseKey, " _
    & "timPosting.ItemKey, timPosting.JrnlKey, timPosting.InvtTranKey, " & ReportObj.SessionID & " FROM " & sTablesUsed & " WHERE timPosting.InvtTranKey = timInvtTranCost.InvtTranKey and timPosting.TranType = " & lTranType & " and timPosting.BatchKey = " & frm.mlBatchKey
    
    sSelect = sSelect & " " & sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO timCostTierRegWrk(InvtTranKey, BatchKey, WhseKey, ItemKey , JrnlKey, InvtTranDistKey, SessionID) " & sSelect
    #Else
        sInsert = "INSERT INTO #timCostTierRegWrk(InvtTranKey, BatchKey, WhseKey, ItemKey , JrnlKey, InvtTranDistKey, SessionID) " & sSelect
    #End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
 
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (ReportObj.SessionID)
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spimCostTierReg")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetVal = DBObj.GetOutParam(2)
        End If
    End With
     
    If lRetVal <> 0 Then
        ReportObj.ReportError kmsgProc, "spIMCostTierReg"
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    sTitle = gsBuildString(kIMCostTierAdjReg, frm.oClass.moAppDB, frm.oClass.moSysSession)
    If Not frmRegister2.mbDetailFlag Then
        ReportObj.ReportFileName() = "IMZTG005a.rpt"
        ReportObj.Orientation() = vbPRORPortrait
    Else
        ReportObj.ReportFileName() = "IMZTG005b.rpt"
        ReportObj.Orientation() = vbPRORLandscape
    End If
    sTitle = sTitle & " - " & frm.ddnFormat.Text

    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    'ReportObj.Orientation() = vbPRORLandscape
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    ReportObj.ReportTitle1() = sTitle
    ReportObj.ReportTitle2() = ""
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
    If (ReportObj.lSetSortCriteria(frm.moSort) = kFailure) Then
        GoTo badexit
    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    'ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    If (ReportObj.lRestrictBy("{timCostTierRegWrk.SessionID} = " & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
    
    ReportObj.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint   '   Added parameters. HBS.

    If sButton = kTbPrint Then
        gbSkipPrintDialog = True     'Don't show print dialog more than once.
    End If
    
    If Not bPeriodEnd Then ShowStatusNone frm
    
    ' Check on gbCancelPrint added by Gil to fix bug 4528
    If Not gbCancelPrint Then lStartReportCA = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReportCA", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub StartGLPostReg(sButton As String, frm As Form)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim iJob As Integer
Dim iRetVal As Integer
Dim sTitle1 As String, sTitle2 As String

    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    
    frm.moReport.CleanupWorkTables
        
    If frm.miUseMultCurr = 1 Then
        frm.moReport.ReportFileName() = "glzje006.rpt"
    Else
        frm.moReport.ReportFileName() = "glzje005.rpt"
    End If
            
    If frm.miGLPostDetailFlag = kGLPostSummary Then
    ' summary
        frm.moReport.ReportTitle2() = "General Ledger Posting Register"
    Else
    ' Gil: Detail use the same
        frm.moReport.ReportTitle2() = "General Ledger Posting Register"
    End If
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORPortrait
    
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    'DO NOT HARDCODE ANY TEXT
    'If frm.glBatchType = BATCH_TYPE_CM Then
        'sTitle1 = "Bank Account Transaction"
    'ElseIf frm.glBatchType = BATCH_TYPE_DE Then
        'sTitle1 = "Deposit"
    'Else
        sTitle1 = "Inventory Register"
    'End If
    
    frm.moReport.ReportTitle1() = sTitle1
'    frm.moReport.ReportTitle2() = ""
    
    'automated means of labeling subtotal fields.
    'this is not manditory if you choose not to use this feature
    frm.moReport.UseSubTotalCaptions() = 0
    frm.moReport.UseHeaderCaptions() = 0
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If

    '********* the following is specific to ARZRA001 *************'
    'you can set report formulas and such here
        
    '********* End of ARZRA001 *************'
    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BYs with new ones. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    'this sub will set any labels for the table fields on the RPT file if they exist.
    'Labels must match the column name from the parent table as well as prefixed by "lbl".
    'For example, to label customer ID from tarCustomer table, a formula on the report
    'must exist with the name, lblCustID. At design time fill this formula with empty quotes.
    'this is not a manitory function. Often the captions from the DD will not be exactly
    'what you want to appear on the report.  If this is the case, there is a function
    'available in the Crystal design environment called FreeFormText that takes a string
    'table constant as a parameter to place text on the report.
    frm.moReport.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
'    frm.moReport.SelectString = frm.moSelect.sGetUserReadableWhereClause(kLenPortrait)
    
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    'if using work tables, it's necessary to restrict the query to the data produced by your
    '"run".  The engine used to do this behind the scenes but as the engine is used in more
    'different ways now, it's better to make the call explicitly yourself.  This permits
    'restriction by any column and doesn't force the inclusing of a column, SessionID in the
    'work tables.
    'only for the report that not use sort grid
    'frm.moSort.SortsUsed()
    If (frm.moReport.lRestrictBy("{tglPosting.BatchKey} = " & frm.mlBatchKey) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If
    
    frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint   '   Added parameters. HBS.
    If sButton = kTbPrint Then
        gbSkipPrintDialog = True      'Don't show print dialog more than once.
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

badexit:
    frm.moReport.CleanupWorkTables
    frm.sbrMain.Status = SOTA_SB_START
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartGLPostReg", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function lStartReportIT(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional lSettingKey As Variant, _
    Optional iFileType As Variant, Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetVal As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
'Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim DBObj As Object
Dim SettingsObj As clsSettings
Dim iUnitCostDecPlaces As Integer 'Used to fetch decimal places for Qty, UnitCost, tranamount
Dim iQtyDecPlaces As Integer
Dim iDigitsAfterDecimal As Integer
Dim sCurrID As String
Dim iUnitPriceDecPlaces As Integer
Dim sFormula As String

    On Error GoTo badexit

    lStartReportIT = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    'Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    '    If web, get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            If (SettingsObj.bLoadSettingsByKey(CLng(lSettingKey)) = False) Then
                GoTo badexit
            End If
        End If
    End If

    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'CUSTOMIZE:  Build a string for selection of key field and session ID.
    
    If Not frm.mbDetailFlag Then
    
        sSelect = "SELECT timPosting.InvtTranKey, timPosting.BatchKey, timPosting.WhseKey, timPosting.ItemKey, timPosting.JrnlKey, " & _
            "timPosting.TranType, " & ReportObj.SessionID & _
            " FROM timPosting  " & _
            " JOIN timItem ON timItem.ItemKey = timPosting.ItemKey " & _
            " WHERE  timPosting.TranType not in (705, 706, 708, 709, 711, 712)  " & _
            " AND timPosting.BatchKey = " & frm.mlBatchKey & _
            " AND (SourceInvtTranKey IS NULL OR SourceInvtTranKey = 0)"
    
        If glBatchType = kBatchTypeIMPC Then
            'Phsyical count transactions excluded above, and may include transactions that are not posted, so they must be pulled in from
            'timPendInvtTran instead of timPosting as timPosting will not include transfers.
            sSelect = "SELECT timPendInvtTran.InvtTranKey, " & frm.mlBatchKey & ", timBatch.WhseKey, timPendInvtTran.ItemKey, timPosting.JrnlKey, " & _
                "timPendInvtTran.TranType, " & ReportObj.SessionID & _
                " FROM timPendInvtTran" & _
                " INNER JOIN timItem ON timItem.ItemKey = timPendInvtTran.ItemKey " & _
                " INNER JOIN timBatch ON timBatch.BatchKey = timPendInvtTran.BatchKey" & _
                " LEFT OUTER JOIN timPosting ON timPosting.BatchKey = timPendInvtTran.BatchKey AND timPosting.InvtTranKey = timPendInvtTran.InvtTranKey " & _
                " WHERE timPendInvtTran.BatchKey = " & frm.mlBatchKey & _
                " ORDER BY timPendInvtTran.InvtTranKey"
        End If
    Else
    
        sSelect = "SELECT timPosting.InvtTranKey, timPosting.BatchKey, timPosting.WhseKey, timPosting.ItemKey, timPosting.JrnlKey, " & _
            "timPosting.TranType, timPosting.UnitMeasKey, timInvtTranDist.InvtTranDistKey, timPosting.ReasonCodeKey, " & ReportObj.SessionID & _
            " FROM timPosting  " & _
            " LEFT OUTER JOIN timInvtTranDist ON timPosting.InvtTranKey = timInvtTranDist.InvtTranKey " & _
            " JOIN timItem ON timItem.ItemKey = timPosting.ItemKey " & _
            " WHERE timPosting.TranType not in ( 705, 706, 708, 709, 711, 712) " & _
            " AND timPosting.BatchKey = " & frm.mlBatchKey & _
            " ORDER BY timPosting.InvtTranKey, timInvtTranDist.InvtTranDistKey"
    
        If glBatchType = kBatchTypeIMPC Then
            'Phsyical count transactions excluded above, and may include transactions that are not posted, so they must be pulled in from
            'timPendInvtTran instead of timPosting as timPosting will not include transfers.
            sSelect = "SELECT timPendInvtTran.InvtTranKey, " & frm.mlBatchKey & ", timBatch.WhseKey, timPendInvtTran.ItemKey, timPosting.JrnlKey, " & _
                "timPendInvtTran.TranType, timPendInvtTran.UnitMeasKey, timInvtTranDist.InvtTranDistKey, NULL, " & ReportObj.SessionID & _
                " FROM timPendInvtTran" & _
                " INNER JOIN timItem ON timItem.ItemKey = timPendInvtTran.ItemKey " & _
                " INNER JOIN timBatch ON timBatch.BatchKey = timPendInvtTran.BatchKey" & _
                " LEFT OUTER JOIN timInvtTranDist ON timPendInvtTran.InvtTranKey = timInvtTranDist.InvtTranKey " & _
                " LEFT OUTER JOIN timPosting ON timPosting.BatchKey = timPendInvtTran.BatchKey AND timPosting.InvtTranKey = timPendInvtTran.InvtTranKey " & _
                " WHERE timPendInvtTran.BatchKey = " & frm.mlBatchKey & _
                " ORDER BY timPendInvtTran.InvtTranKey, timInvtTranDist.InvtTranDistKey"
        End If
    End If
    
    sSelect = sSelect & " " & sWhereClause
    #If RPTDEBUG Then
    If Not frm.mbDetailFlag Then
        sInsert = "INSERT INTO timTranRegWrk (InvtTranKey, BatchKey, WhseKey, ItemKey, JrnlKey, TranType, SessionID) " & sSelect
    Else
    'Changed By Atul to display ReasonCode in Register
        sInsert = "INSERT INTO timTranRegDetWrk (InvtTranKey, BatchKey, WhseKey, ItemKey , JrnlKey, TranType, UnitMeasKey, InvtTranDistKey, ReasonCodeKey, SessionID) " & sSelect
    End If
    #Else
    If Not frm.mbDetailFlag Then
        sInsert = "INSERT INTO #timTranRegWrk (InvtTranKey, BatchKey, WhseKey, ItemKey , JrnlKey, TranType, SessionID) " & sSelect
    Else
    'Changed By Atul to display ReasonCode in Register
        sInsert = "INSERT INTO #timTranRegDetWrk (InvtTranKey, BatchKey, WhseKey, ItemKey , JrnlKey, TranType, UnitMeasKey, InvtTranDistKey, ReasonCodeKey, SessionID) " & sSelect
    End If
    #End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (ReportObj.SessionID)
        .SetInParamStr (ReportObj.CompanyId)
        If Not frm.mbDetailFlag Then
            .SetInParam 0
        Else
            .SetInParam 1
        End If
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spimTranReg")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetVal = DBObj.GetOutParam(4)
        End If
    End With
     
    If lRetVal <> 0 Then
        ReportObj.ReportError kmsgProc, "spimTranReg"
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
'Intellisol Start
    If frmRegister2.mbCalledFromPA Then
        If frm.mbDetailFlag Then
            sInsert = "DELETE FROM #tPAInvtRegPostWrk"
            On Error Resume Next
            DBObj.ExecuteSQL sInsert
            If Err.Number <> 0 Then
                ReportObj.ReportError kmsgProc, sInsert
                GoTo badexit
            End If
            gClearSotaErr
            On Error GoTo badexit
        
    
            sInsert = "INSERT INTO #tPAInvtRegPostWrk (InvtTranKey, BatchKey, WhseKey, ItemKey , JrnlKey, TranType, UnitMeasKey, InvtTranDistKey, ReasonCodeKey, SessionID) " & sSelect

            On Error Resume Next
            DBObj.ExecuteSQL sInsert
            If Err.Number <> 0 Then
                ReportObj.ReportError kmsgProc, sInsert
                GoTo badexit
            End If
            gClearSotaErr
            On Error GoTo badexit
        Else
            sInsert = "DELETE FROM #tPAInvtRegPostWrk"
            On Error Resume Next
            DBObj.ExecuteSQL sInsert
            If Err.Number <> 0 Then
                ReportObj.ReportError kmsgProc, sInsert
                GoTo badexit
            End If
            gClearSotaErr
            On Error GoTo badexit
        
            sInsert = "INSERT INTO #tPAInvtRegPostWrk (InvtTranKey, SessionID) " & _
                      "SELECT timPosting.InvtTranKey, " & ReportObj.SessionID & _
                      " FROM  timPosting WITH (NOLOCK), timItem WITH (NOLOCK) " & _
                      " WHERE timPosting.ItemKey = timItem.ItemKey " & _
                      " And timPosting.TranType not in ( 705, 706, 708, 709, 711, 712) " & _
                      " And timPosting.BatchKey = " & frm.mlBatchKey & _
                      " order by timPosting.InvtTranKey"

            On Error Resume Next
            DBObj.ExecuteSQL sInsert
            If Err.Number <> 0 Then
                ReportObj.ReportError kmsgProc, sInsert
                GoTo badexit
            End If
            gClearSotaErr
            On Error GoTo badexit
        End If
        
        With DBObj
            .SetInParam (ReportObj.SessionID)
            .SetInParamStr (ReportObj.CompanyId)
            If Not frm.mbDetailFlag Then
                .SetInParam 0
            Else
                .SetInParam 1
            End If
            .SetOutParam lRetVal
            On Error Resume Next
            .ExecuteSP ("spPAInvtTranReg")
            If Err.Number <> 0 Then
                lRetVal = Err.Number
            Else
                lRetVal = DBObj.GetOutParam(4)
            End If
        End With
     
        If lRetVal <> 0 Then
            ReportObj.ReportError kmsgProc, "spPAInvtTranReg"
            DBObj.ReleaseParams
            GoTo badexit
        End If
        
        DBObj.ReleaseParams
        gClearSotaErr
    End If
'Intellisol End
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    'If Not frmRegister2.mbDetailFlag Then
    If Not frm.mbDetailFlag Then
        'Intellisol Start
        If frmRegister2.mbCalledFromPA Then
            ReportObj.ReportFileName() = "IMZTGPA1a.rpt"
            ReportObj.Orientation() = vbPRORLandscape
        Else
            If glBatchType = kBatchTypeIMPC Then
                ReportObj.ReportFileName() = "IMZTG001c.rpt"
                ReportObj.Orientation() = vbPRORPortrait
            Else
                ReportObj.ReportFileName() = "IMZTG001a.rpt"
                ReportObj.Orientation() = vbPRORPortrait
            End If
        End If
        'Intellisol End
    Else
        'Intellisol Start
        If frmRegister2.mbCalledFromPA Then
            ReportObj.ReportFileName() = "IMZTGPA1b.rpt"
            ReportObj.Orientation() = vbPRORLandscape
        Else
            If glBatchType = kBatchTypeIMPC Then
                ReportObj.ReportFileName() = "IMZTG001d.rpt"
                ReportObj.Orientation() = vbPRORPortrait
            Else
                ReportObj.ReportFileName() = "IMZTG001b.rpt"
                ReportObj.Orientation() = vbPRORLandscape
            End If
        End If
        'Intellisol End
    End If
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If

    'Populate Decimal Variables based on CI option setting.
    iUnitCostDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("UnitCostDecPlaces", _
                         "tciOptions", "CompanyID = " & gsQuoted(ReportObj.CompanyId)))
    iQtyDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", _
                    "CompanyID = " & gsQuoted(ReportObj.CompanyId)))
    sCurrID = frm.oClass.moAppDB.Lookup("CurrID", "tsmCompany", "CompanyID = " & gsQuoted(ReportObj.CompanyId))
    iDigitsAfterDecimal = giGetValidInt(frm.oClass.moAppDB.Lookup("DigitsAfterDecimal", _
                          "tmcCurrency", "CurrID = " & gsQuoted(sCurrID)))
    iUnitPriceDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("UnitPriceDecPlaces", _
                         "tciOptions", "CompanyID = " & gsQuoted(ReportObj.CompanyId)))

    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    If glBatchType = kBatchTypeIMPC Then
        ReportObj.ReportTitle1() = gsBuildString(kIMInvtTranRegPC, frm.oClass.moAppDB, frm.oClass.moSysSession)        '"Vendor Item List"
    Else
        ReportObj.ReportTitle1() = gsBuildString(kIMInvtTranReg, frm.oClass.moAppDB, frm.oClass.moSysSession)        '"Vendor Item List"
    End If
    ReportObj.ReportTitle2() = ""
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If
        
    If frmRegister2.mbCalledFromPA Then
        sFormula = gsBuildString(kPAModule, frm.oClass.moAppDB, frm.oClass.moSysSession)         '"Project Accounting"
        If Not ReportObj.bSetReportFormula("Module", QUOTECON & sFormula & QUOTECON) Then GoTo badexit
    End If
    
    'The physical count version fo the inventory transaction register does not support sorting.
    If glBatchType <> kBatchTypeIMPC Then
        If (ReportObj.lSetSortCriteria(frm.moSort) = kFailure) Then
            GoTo badexit
        End If
    End If
 
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'Pass the values for the Decimal Places formulas.
    If frm.mbDetailFlag Then
        ReportObj.bSetReportFormula "UnitCostDecPlaces", CInt(iUnitCostDecPlaces)
        ReportObj.bSetReportFormula "QtyDecPlaces", CInt(iQtyDecPlaces)
        ReportObj.bSetReportFormula "DigitsAfterDecimal", CInt(iDigitsAfterDecimal)
        If glBatchType <> kBatchTypeIMPC Then ReportObj.bSetReportFormula "UnitPriceDecPlaces", CInt(iUnitPriceDecPlaces)
    End If
    
    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    'ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    #If RPTDEBUG Then
        If Not frm.mbDetailFlag Then
            If (ReportObj.lRestrictBy("{timTranRegWrk.SessionID} = " & ReportObj.SessionID) = kFailure) Then
                GoTo badexit
            End If
        Else
            If (ReportObj.lRestrictBy("{timTranRegDetWrk.SessionID} = " & ReportObj.SessionID) = kFailure) Then
                GoTo badexit
            End If
        End If
    #Else
    #End If
    
    ReportObj.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint   '   Added parameters. HBS.

    If sButton = kTbPrint Then
        gbSkipPrintDialog = True    '   don't show print dialog more than once. HBS
    End If
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    If Not gbCancelPrint Then lStartReportIT = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReportIT", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function lStartReportPC(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional lSettingKey As Variant, _
    Optional iFileType As Variant, Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetVal As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
'Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim DBObj As Object
Dim SettingsObj As clsSettings

    On Error GoTo badexit

    lStartReportPC = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    'Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    '    If web, get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            If (SettingsObj.bLoadSettingsByKey(CLng(lSettingKey)) = False) Then
                GoTo badexit
            End If
        End If
    End If

    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    sTablesUsed = "timPhysCountTran"
    
    'Create a clustered index on the temp table if it does not have one.  This will allow the report to come up in
    'control number order by default.
    sInsert = "IF NOT EXISTS(SELECT 1 FROM tempdb..sysindexes WITH (NOLOCK)" & _
        " WHERE ID = OBJECT_ID('tempdb..#timPhysCountRegWrk') AND name = '#clsIDX_timPhysCountRegWrk')" & _
        " BEGIN CREATE CLUSTERED INDEX #clsIDX_timPhysCountRegWrk ON #timPhysCountRegWrk(CtrlNo,PhysCountTranKey) END"
        
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
        
    'CUSTOMIZE:  Build a string for selection of key field and session ID.
    sSelect = "SELECT timPhysCountTran.PhysCountTranKey," & frm.mlBatchKey & "," & ReportObj.SessionID & _
        " FROM " & sTablesUsed & " WHERE timPhysCountTran.CountQty IS NOT NULL AND timPhysCountTran.BatchKey = " & frm.mlBatchKey
    
    'sSelect = "SELECT timPhysCountTran.PhysCountTranKey, timPhysCountTran.BatchKey, timWhseBin.WhseKey, timPhysCountTran.ItemKey, " _
    '& ReportObj.SessionID & " FROM timPhysCountTran WITH (NOLOCK), timWhseBin WITH (NOLOCK) WHERE timPhysCountTran.BatchKey = " & frm.mlBatchKey & " AND timPhysCountTran.WhseBinKey = timWhseBin.WhseBinKey AND timPhysCountTran.FreezeQty IS NOT NULL AND timPhysCountTran.CountQty IS NOT NULL"
    
    sSelect = sSelect & " " & sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO timPhysCountRegWrk (PhysCountTranKey, BatchKey, SessionID) " & sSelect
    #Else
        sInsert = "INSERT INTO #timPhysCountRegWrk (PhysCountTranKey, BatchKey, SessionID) " & sSelect
    #End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
       
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (ReportObj.SessionID)
        '.SetInParamStr (ReportObj.CompanyID)
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spimPhysCountReg")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetVal = DBObj.GetOutParam(2)
        End If
    End With
     
    If lRetVal <> 0 Then
        ReportObj.ReportError kmsgProc, "spimPhysCountReg"
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    If Not frmRegister2.mbDetailFlag Then
        ReportObj.ReportFileName() = "IMZTG004a.rpt"
        ReportObj.Orientation() = vbPRORPortrait
    Else
        ReportObj.ReportFileName() = "IMZTG004b.rpt"
        ReportObj.Orientation() = vbPRORLandscape
    End If
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    ReportObj.ReportTitle1() = gsBuildString(kimPhysCountTranReg, frm.oClass.moAppDB, frm.oClass.moSysSession)        '"Vendor Item List"
    ReportObj.ReportTitle2() = ""
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Added by Atul on Dec'15, for sorting records in register
    If (ReportObj.lSetSortCriteria(frm.moSort) = kFailure) Then
        GoTo badexit
    End If

    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    ReportObj.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint    '   Added parameters. HBS.

    If sButton = kTbPrint Then
        gbSkipPrintDialog = True    '   don't show print dialog more than once. HBS
    End If
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    ' Check on gbCancelPrint added by Gil to fix bug 4528
    If Not gbCancelPrint Then lStartReportPC = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReportPC", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function lStartReportKA(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional lSettingKey As Variant, _
    Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetVal As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
'Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim DBObj As Object
Dim SettingsObj As clsSettings
Dim iUnitCostDecPlaces As Integer 'Used to fetch decimal places for Qty, UnitCost, tranamount
Dim iQtyDecPlaces As Integer
Dim iDigitsAfterDecimal As Integer
Dim sCurrID As String

    On Error GoTo badexit
    
    lStartReportKA = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    'Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    '    If web, get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            If (SettingsObj.bLoadSettingsByKey(CLng(lSettingKey)) = False) Then
                GoTo badexit
            End If
        End If
    End If

    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    ' Modified by Gil to handle Sale of BTO Kits
    If glBatchType = 704 Then ' Kit Assembly
        sTablesUsed = "timPosting, timInvtTranDist"
        sWhereClause = " WHERE timPosting.TranType IN (711, 713)"  ' Kit Assembly / Disassembly
        sWhereClause = sWhereClause & " AND timPosting.SourceInvtTranKey Is Null"
        sWhereClause = sWhereClause & " AND timPosting.InvtTranKey = timInvtTranDist.InvtTranKey"
        sWhereClause = sWhereClause & " AND timPosting.CompanyID = " & gsQuoted(ReportObj.CompanyId)
        sWhereClause = sWhereClause & " AND timPosting.BatchKey = " & CStr(frm.mlBatchKey)
        
        ' CUSTOMIZE:  Build a string for selection of key field and session ID.
        sSelect = "SELECT timInvtTranDist.InvtTranDistKey, timPosting.InvtTranKey, "
        sSelect = sSelect & ReportObj.SessionID & ", " & gsQuoted(ReportObj.CompanyId)
        sSelect = sSelect & " FROM " & sTablesUsed
        sSelect = sSelect & " " & sWhereClause
        #If RPTDEBUG Then
            sInsert = "INSERT INTO timKitRegItemWrk (InvtTranDistKey, InvtTranKey, SessionID, CompanyID) " & sSelect
        #Else
            sInsert = "INSERT INTO #timKitRegItemWrk (InvtTranDistKey, InvtTranKey, SessionID, CompanyID) " & sSelect
        #End If
    
    Else    ' BTO Kits Sale
        sTablesUsed = "timPosting, timItem"
        sWhereClause = " WHERE timPosting.TranType = 701 "  ' Sale
        sWhereClause = sWhereClause & " AND timItem.ItemType = 7 " ' BTO Kit
        sWhereClause = sWhereClause & " AND timPosting.ItemKey = timItem.ItemKey"
        sWhereClause = sWhereClause & " AND timPosting.CompanyID = " & gsQuoted(ReportObj.CompanyId)
        sWhereClause = sWhereClause & " AND timPosting.BatchKey = " & CStr(frm.mlBatchKey)
        
        ' CUSTOMIZE:  Build a string for selection of key field and session ID.
        sSelect = "SELECT timPosting.InvtTranKey, "
        sSelect = sSelect & ReportObj.SessionID & ", " & gsQuoted(ReportObj.CompanyId)
        sSelect = sSelect & " FROM " & sTablesUsed
        sSelect = sSelect & " " & sWhereClause
        #If RPTDEBUG Then
            sInsert = "INSERT INTO timKitRegItemWrk (InvtTranKey, SessionID, CompanyID) " & sSelect
        #Else
            sInsert = "INSERT INTO #timKitRegItemWrk (InvtTranKey, SessionID, CompanyID) " & sSelect
        #End If
    End If
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    'Raminder
    
    With DBObj
        .SetInParam (ReportObj.SessionID)
        .SetInParamStr (ReportObj.CompanyId)
        If frmRegister2.mbDetailFlag Then
            .SetInParamInt (1)
        Else
            .SetInParamInt (0)
        End If
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spimKitRegister")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetVal = DBObj.GetOutParam(4)
        End If
    End With
     
    If lRetVal <> 0 Then
        ReportObj.ReportError kmsgProc, "(spimKitRegister)"
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    If frmRegister2.mbDetailFlag Then
        ReportObj.ReportFileName() = "imztg003b.rpt"
        ReportObj.Orientation() = vbPRORLandscape
    Else
        ReportObj.ReportFileName() = "imztg003a.rpt"
        ReportObj.Orientation() = vbPRORLandscape

    End If

    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    
    'Gil: Uncommented this, there was a problem with the sorts
    'For the time being Sorting on Kit Register disabled
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    ReportObj.ReportTitle1() = gsBuildString(kIMKitsAssemblyReg, frm.oClass.moAppDB, frm.oClass.moSysSession)
    ReportObj.ReportTitle1() = "Kits Register" ' Temporary fix
    ReportObj.ReportTitle2() = ""
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If
    'Populate Decimal Variables based on CI option setting.
    iUnitCostDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("UnitCostDecPlaces", _
                         "tciOptions", "CompanyID = " & gsQuoted(ReportObj.CompanyId)))
    iQtyDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", _
                    "CompanyID = " & gsQuoted(ReportObj.CompanyId)))
    sCurrID = frm.oClass.moAppDB.Lookup("CurrID", "tsmCompany", "CompanyID = " & gsQuoted(ReportObj.CompanyId))
    iDigitsAfterDecimal = giGetValidInt(frm.oClass.moAppDB.Lookup("DigitsAfterDecimal", _
                          "tmcCurrency", "CurrID = " & gsQuoted(sCurrID)))

    ReportObj.bSetReportFormula "UnitCostDecPlaces", CInt(iUnitCostDecPlaces)
    ReportObj.bSetReportFormula "QtyDecPlaces", CInt(iQtyDecPlaces)
    ReportObj.bSetReportFormula "DigitsAfterDecimal", CInt(iDigitsAfterDecimal)
    'Uncommented by Atul on Dec'15 so as to sort records in a register
    'Set sort order in .RPT file according to user selections in the Sort grid.
    If (ReportObj.lSetSortCriteria(frm.moSort) = kFailure) Then
        GoTo badexit
    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
    If (ReportObj.lRestrictBy("{timKitRegItemWrk.SessionID} = " & ReportObj.SessionID) = kFailure) Then
        GoTo badexit
    End If
    
    ReportObj.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint    '   Added parameters. HBS.

    If sButton = kTbPrint Then
        gbSkipPrintDialog = True    '   don't show print dialog more than once. HBS
    End If
        
    If Not bPeriodEnd Then ShowStatusNone frm
    
    ' Check on gbCancelPrint added by Gil to fix bug 4528
    If Not gbCancelPrint Then lStartReportKA = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReportKA", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function lStartReportTR(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional lSettingKey As Variant, _
    Optional iFileType As Variant, Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetVal As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
'Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim DBObj As Object
Dim SettingsObj As clsSettings
Dim iUnitCostDecPlaces As Integer 'Used to fetch decimal places for Qty, UnitCost, tranamount
Dim iQtyDecPlaces As Integer
Dim iDigitsAfterDecimal As Integer
Dim sCurrID As String

    On Error GoTo badexit
      
    
    lStartReportTR = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    'Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    '    If web, get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            If (SettingsObj.bLoadSettingsByKey(CLng(lSettingKey)) = False) Then
                GoTo badexit
            End If
        End If
    End If

    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Append criteria to restrict data returned to the current company.
    'SelectObj.AppendToWhereClause sWhereClause, "timPosting.TranType = " & kTranTypeIMPC
    sTablesUsed = ""
    sTablesUsed = sTablesUsed & "timPosting,timInvtTranDist"
    
    'CUSTOMIZE:  Build a string for selection of key field and session ID.'(705,706)
    If frm.mbDetailFlag Then
        sWhereClause = " WHERE (timPosting.TranType  = 706 ) and timPosting.BatchKey = " & frm.mlBatchKey & " and timPosting.InvtTranKey = timInvtTranDist.InvtTranKey"
        sSelect = "SELECT timPosting.InvtTranKey, timPosting.JrnlKey, timPosting.BatchKey, timPosting.WhseKey, timPosting.ItemKey,timInvtTranDist.InvtTranDistKey ," & ReportObj.SessionID & "," & gsQuoted(ReportObj.CompanyId) & " FROM " & sTablesUsed
        sSelect = sSelect & " " & sWhereClause
    Else
        sWhereClause = " WHERE timPosting.TranType  = 706"
        sWhereClause = sWhereClause & " AND timPosting.BatchKey = " & frm.mlBatchKey '& " and timPosting.InvtTranKey = timInvtTranDist.InvtTranKey"
        sSelect = "SELECT timPosting.InvtTranKey, timPosting.JrnlKey, timPosting.BatchKey, timPosting.WhseKey, timPosting.ItemKey, 0," & ReportObj.SessionID & "," & gsQuoted(ReportObj.CompanyId)
        sSelect = sSelect & " FROM timPosting" ' & sTablesUsed
        sSelect = sSelect & " " & sWhereClause
    End If
    #If RPTDEBUG Then
        sInsert = "INSERT INTO timTransRegWrk (InvtTranKey,JrnlKey,BatchKey, WhseKey, ItemKey ,InvtTranDistKey, SessionID,CompanyID) " & sSelect
    #Else
        sInsert = "INSERT INTO #timTransRegWrk (InvtTranKey,JrnlKey,BatchKey, WhseKey, ItemKey ,InvtTranDistKey, SessionID,CompanyID) " & sSelect
    #End If
    

    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
       
    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
    'custom stored procedure to populate remaining fields in primary work table and
    'insert records into secondary work tables.
    With DBObj
        .SetInParam (ReportObj.SessionID)
        .SetInParamStr (ReportObj.CompanyId)
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("spimTransReg")
        If Err.Number <> 0 Then
            lRetVal = Err.Number
        Else
            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
            'errors after each attempt.  After storing all return values in local variables OR
            'encountering an error, release parameters.
            lRetVal = DBObj.GetOutParam(3)
        End If
    End With
     
    If lRetVal <> 0 Then
        ReportObj.ReportError kmsgProc, "spimTransReg"
        DBObj.ReleaseParams
        GoTo badexit
    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    If Not frm.mbDetailFlag Then
    
        ReportObj.ReportFileName() = "IMZTG002a.rpt"
        ReportObj.Orientation() = vbPRORLandscape
    
    Else
        ReportObj.ReportFileName() = "IMZTG002b.rpt"
        ReportObj.Orientation() = vbPRORPortrait

    End If
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in StrConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    ReportObj.ReportTitle1() = gsBuildString(kIMInvTransReg, frm.oClass.moAppDB, frm.oClass.moSysSession)        '"Inventory Transfer Register"
    ReportObj.ReportTitle2() = ""
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 0
    ReportObj.UseHeaderCaptions() = 0
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Populate Decimal Variables based on CI option setting.
    iUnitCostDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("UnitCostDecPlaces", _
                         "tciOptions", "CompanyID = " & gsQuoted(ReportObj.CompanyId)))
    iQtyDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", _
                    "CompanyID = " & gsQuoted(ReportObj.CompanyId)))
    sCurrID = frm.oClass.moAppDB.Lookup("CurrID", "tsmCompany", "CompanyID = " & gsQuoted(ReportObj.CompanyId))
    iDigitsAfterDecimal = giGetValidInt(frm.oClass.moAppDB.Lookup("DigitsAfterDecimal", _
                          "tmcCurrency", "CurrID = " & gsQuoted(sCurrID)))
    If frm.mbDetailFlag Then
    ReportObj.bSetReportFormula "UnitCostDecPlaces", CInt(iUnitCostDecPlaces)
    ReportObj.bSetReportFormula "QtyDecPlaces", CInt(iQtyDecPlaces)
    ReportObj.bSetReportFormula "DigitsAfterDecimal", CInt(iDigitsAfterDecimal)
    End If


    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
   ' ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    ReportObj.ProcessReport frm, sButton, , , gbSkipPrintDialog, gbCancelPrint   '   Added parameters. HBS.

    If sButton = kTbPrint Then
        gbSkipPrintDialog = True      'Don't show print dialog more than once.
    End If

    If Not bPeriodEnd Then ShowStatusNone frm
    
    If Not gbCancelPrint Then lStartReportTR = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReportTR", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function




