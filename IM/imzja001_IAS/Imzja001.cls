VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsInvtRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'************************************************************************************
'     Name: clsReport
'     Desc: The clsReport class handles the interface for
'           the framework and the VB server
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: EDT
'     Mods:
'************************************************************************************
Option Explicit

    '=======================================================================
    '           Public Variables for use by other modules
    '           -----------------------------------------
    '
    '   moFramework     - required to give other modules the ablitiy to
    '                     call the framework methods and properties, such
    '                     as UnloadSelf, LoadSotaObject, . . .
    '                     Object reference is set in InitializeObject.
    '
    '   mlContext       - Contains information of how this class object
    '                     was invoked.  It is set in InitializeObject
    
    '
    '
    '   moSysSession    - system Manager Session Object which contains
    '                     information specific to the user's session with
    '                     this accounting application.  For example, current
    '                     CompanyID, Business date, UserID, UserName, . . .
    '
    '   miShutDownRequester
    '                   - The ShutDownRequester is the object responsible
    '                     for requesting this object to shut down.  There are
    '                     two possible choices: 1) the Framework requests the
    '                     object to shut itself down (kFrameworkShutDown; and
    '                     2) the User Interface or UnloadSelf call requests
    '                     the framework to shut this object down (kUnloadSelfShutDown).
    '                     The main purpose of this flag is to
    '                     ensure that the framework reference isn't removed (or
    '                     set to nothing) before the UI.
    '=======================================================================
    Public moFramework          As Object
    Public mlContext            As Long
    Public moDasSession         As Object
    Public moAppDB              As Object
    Public moSysSession         As Object
    Public miShutDownRequester  As Integer
    Public mlError              As Long
    
    'Private Variables for Class Properties
    Private mlRunFlags          As Long
    Private mlUIActive          As Long
    Private mbPeriodEnd         As Boolean

    
    'Private variables for use within this class only
    Public mfrmMain            As Form
    Public moCTI As Object

    ' Public property mlBatchKey
    Public glBatchKey          As Long
Const VBRIG_MODULE_ID_STRING = "IMZJA001.CLS"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
#If InProc = 1 Then
    sMyName = "clsInvtRegister"
#Else
    sMyName = "clsInvtRegister"
#End If
End Function

Public Property Let lUIActive(lNewActive As Long)
'+++ VB/Rig Skip +++
    mlUIActive = lNewActive
End Property

Public Property Get lUIActive() As Long
'+++ VB/Rig Skip +++
    lUIActive = mlUIActive
End Property

Public Property Get lRunFlags() As Long
'+++ VB/Rig Skip +++
    lRunFlags = mlRunFlags
End Property

Public Property Let lRunFlags(ltRunFlags As Long)
'+++ VB/Rig Skip +++
    mlRunFlags = ltRunFlags
End Property

Public Property Set frmMain(frm As Object)
'+++ VB/Rig Skip +++
    Set mfrmMain = frm
End Property

Public Property Get frmMain() As Object
'+++ VB/Rig Skip +++
    Set frmMain = mfrmMain
End Property

Public Property Let bPeriodEnd(bSetPeriodEnd As Boolean)
'+++ VB/Rig Skip +++
    mbPeriodEnd = bSetPeriodEnd
End Property

Public Property Get bPeriodEnd() As Boolean
'+++ VB/Rig Skip +++
    bPeriodEnd = mbPeriodEnd
End Property

Private Sub Class_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ClassInitialize Me
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Initialize", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Sub Class_Terminate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ClassTerminate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Terminate", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Sub PostBatches(frmObj As Object, dPostDate() As Date, lBatchKey() As Long, iPrint() As Integer, _
    iPost() As Integer, sBatchID() As String, lBatchRow() As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
Dim i As Integer
Dim iNumBatches As Integer

    gbPostIMBatchesFlag = True
    If mfrmMain Is Nothing Then
        If moFramework.LoadUI(Me) = kFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    
    
    mfrmMain.miMode = 0
    Set frmPrintPostStat.mfrmMain = mfrmMain
    
    'If UBound(dPostDate()) = 0 Then
    '    Exit Sub
    'End If
    
    frmPrintPostStat.Initialize dPostDate(), lBatchKey(), iPrint(), iPost(), sBatchID()
    frmPrintPostStat.ShowMe
    
    If mlError Then
        Err.Raise mlError
    End If
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "PostBatches", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    InitializeObject = kFailure
    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title
    mbPeriodEnd = False
    InitializeObject = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "InitializeObject", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function LoadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    LoadUI = kFailure

    Set mfrmMain = frmRegister2
    Set mfrmMain.oClass = Me
    mfrmMain.lRunMode = mlContext And kRunModeMask
    
    'Intellisol Start
    If mfrmMain.mbCalledFromPA = False Then
        If glGetValidLong(moAppDB.Lookup("COUNT(*)", "paimBatch, timBatch", _
            "paimBatch.BatchKey=timBatch.BatchKey AND paimBatch.BatchKey=" & _
            Trim(Str(glBatchKey)))) > 0 Then
            mfrmMain.mbCalledFromPA = True
        End If
    End If
    'Intellsol End
    
    'Dont move this line anywhere.
    mfrmMain.mlBatchKey = glBatchKey
    
    mfrmMain.miMode = 1
    
    Load mfrmMain
    If mlError Then
        Err.Raise mlError
    End If
    
    If mfrmMain.bLoadSuccess Then
        LoadUI = kSuccess
    Else
        LoadUI = EFW_C_FAIL
    End If

    'Exit this function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "LoadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function GetUIHandle(ByVal ltContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    GetUIHandle = kFailure
    If Not ClassGetUIHandle(Me, ltContext) > 0 Then Exit Function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "GetUIHandle", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function DisplayUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
    DisplayUI = EFW_CT_MODALEXIT
#Else
    DisplayUI = kFailure
    If ClassDisplayUI(Me, lContext) = kFailure Then Exit Function
    DisplayUI = kSuccess
#End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "DisplayUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function ShowUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ShowUI = kFailure
    If ClassShowUI(Me, lContext) = kFailure Then Exit Function
    ShowUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ShowUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function QueryShutDown(lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    QueryShutDown = kFailure
    If ClassQueryShutdown(Me, lContext) = kFailure Then Exit Function
    QueryShutDown = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "QueryShutDown", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function MinimizeUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
    mfrmMain.WindowState = vbMinimized
#Else
    MinimizeUI = kFailure
    If ClassMinimizeUI(Me, lContext) = kFailure Then Exit Function
    MinimizeUI = kSuccess
#End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "MinimizeUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function RestoreUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    RestoreUI = kFailure
    If ClassRestoreUI(Me, lContext) = kFailure Then Exit Function
    RestoreUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "RestoreUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function HideUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
#If InProc = 1 Then
    HideUI = kSuccess
#Else
    HideUI = kFailure
    If ClassHideUI(Me, lContext) = kFailure Then Exit Function
    HideUI = kSuccess
#End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "HideUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function UnloadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    UnloadUI = kFailure
    If ClassUnloadUI(Me, lContext) = kFailure Then Exit Function
    UnloadUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "UnloadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function TerminateObject(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    TerminateObject = kFailure
    If ClassTermObj(Me, lContext) = kFailure Then Exit Function
    TerminateObject = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "TerminateObject", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

'Public Function PrintNoUI(lContext As Long, Optional iPerNo, Optional sPerYear, Optional sPeriodEndDate) As Long
''+++ VB/Rig Begin Push +++
'#If ERRORTRAPON Then
'On Error GoTo VBRigLogErrorRoutine
'#End If
''+++ VB/Rig End +++
'    PrintNoUI = kFailure
'
'    Set mfrmMain = frmRegister2
'
'    Set mfrmMain.oClass = Me
'    mfrmMain.lRunMode = lContext And kRunModeMask
'    mbPeriodEnd = True
'    Load mfrmMain
'
'    If mfrmMain.bLoadSuccess Then
'       'PrintNoUI = lStartReport(kTbPrint, mfrmMain, True, sPeriodEndDate, iPerNo, sPerYear)
'    Else
'        PrintNoUI = EFW_C_FAIL
'    End If
''+++ VB/Rig Begin Pop +++
'        Exit Function
'
'VBRigLogErrorRoutine:
'        gMainClassErr Err, sMyName, "PrintNoUI", VBRIG_IS_CLASS
'        Resume Next
''+++ VB/Rig End +++
'End Function



Public Function lGetSettings(ByRef sSetting, ByRef sSelect, Optional oResponse As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Retrieve the report settings as a string.
'     Parms: sSetting - Output; String of saved setting id/setting key pairs.
'            sSelect - Output; String of saved setting key/select info pairs.
'            oResponse as Variant - Optional object used for debugging
'   Returns: kSuccess; kFailure
'            sSetting - Each setting is a string and a key. Each string and key is
'            separated by a tab (vbTab). Each setting is delimited by a CRLF (vbCrLf).


'            The first setting in the list is the most recently used by the user.
'            If no settings class exists, then empty string " is returned.
'            If error, then empty string " is returned.
'            sSelect - Each setting is a key and selection row information.
'            Each row is separated by a semicolon. Values within a row
'            are separated by commas.  Each row contains setting key,
'            caption, operator, start value, end value.  E.g.,
'            <setting key>, <caption>, <opertor>,<start value>, <end value>;
'            <setting key>, <caption>, <opertor>,<start value>, <end value>.
'            If no settings, then empty string (") is returned.
'   Assumes: Settings class proc Initialize has been run.
'            Selection control proc bInitSelect has been run.
'************************************************************************************
    lGetSettings = kFailure

    On Error Resume Next
    sSetting = mfrmMain.moSettings.sGetSettings()
    If Err <> 0 Then
        sSetting = ""
    End If
    
    sSelect = mfrmMain.moSelect.sGetSettings()
    If Err <> 0 Then
        sSelect = ""
    End If
    
    lGetSettings = kSuccess
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lGetSettings", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Sub GenerateReport(iFileType As Integer, sFileName As String, _
                          Optional lSettingKey As Variant, _
                          Optional sSelect As Variant, _
                          Optional oResponse As Variant)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Generate web report file by calling HandleToolbarClick.
'     Parms: iFileType as Integer - File type to create (0 = HTML, 1 = RPT)
'            sFileName as String - File name to create (including path)
'            lSettingKey as Variant - Optional; setting selected by user
'            sSelect as Variant - Optional; selection grid values
'            oResponse as Variant - Optional object used for debugging
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine

    'Get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            mfrmMain.moSettings.bLoadSettingsByKey (CLng(lSettingKey))
        End If
    End If
    
    'Put Web Client Selection Grid values on Web Server Selection Grid.
    If Not IsMissing(sSelect) Then
        On Error Resume Next    'If selection grid does not exist.
        mfrmMain.moSelect.lPutSelectGrid sSelect
        On Error GoTo 0     'Normal error processing
    End If
    
    mfrmMain.HandleToolbarClick kTbPreview, iFileType, sFileName
    
    'Exit this subroutine
    Exit Sub
    
ExpectedErrorRoutine:
    gClearSotaErr

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GenerateReport", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Sub startIMRegister(BatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
   
    ' Set the batchkey here itself so that it is available in Load UI
    glBatchKey = BatchKey
    gbPostIMBatchesFlag = False
    
    ' Fix Customizer problem in Form Unload
    Set goClass = Me
    
    If mfrmMain Is Nothing Then
        If moFramework.LoadUI(Me) = kFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    
    'Intellisol Start
    If mfrmMain.mbCalledFromPA = False Then
        If glGetValidLong(moAppDB.Lookup("COUNT(*)", "paimBatch, timBatch", _
            "paimBatch.BatchKey=timBatch.BatchKey AND paimBatch.BatchKey=" & _
            Trim(Str(BatchKey)))) > 0 Then
            mfrmMain.mbCalledFromPA = True
        End If
    End If
    'Intellsol End
    
    mfrmMain.miMode = 1
    'mfrmMain.mlPostDate = PostDate
    'mfrmMain.SetRegPrintedFlag
    
    If mfrmMain.LockBatch Then Exit Sub
    
    mfrmMain.Show vbModal
    
    If mlError Then
        Err.Raise mlError
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "startIMRegister", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Function sGetSelection(Optional oResponse As Variant) As String
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Retrieve the report selection grid information as a string.
'     Parms: oResponse as Variant - Optional object used for debugging
'   Returns: String of selection grid information.
'            String of variable values, selection array values, and selection grid values.
'            Variable values, selection array values, and selection grid values
'            are separated from each other by two semicolons.
'            Variable values are separated from each other by a comma.
'            Each selection array row is separated by a semicolon.  Values in the
'            selection array are separated by a comma.
'            Each grid row is separated by a semicolon. Values in the selection
'            grid row are separated by a comma.
'            Assumes: Selection Grid's proc bInitSelect has been run.
'************************************************************************************
    On Error Resume Next
    sGetSelection = mfrmMain.moSelect.sGetSelection()
    If Err <> 0 Then
        sGetSelection = ""
    End If
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetSelection", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

