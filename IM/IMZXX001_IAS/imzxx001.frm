VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Begin VB.Form frmimzxx001 
   Caption         =   "COA Test Results"
   ClientHeight    =   8835
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9990
   Icon            =   "imzxx001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8835
   ScaleWidth      =   9990
   Begin VB.CommandButton cmdCopy 
      Caption         =   "Duplicate"
      Height          =   375
      Left            =   8520
      TabIndex        =   46
      ToolTipText     =   "Copy the COA data to a new item.  Including the Lot, Tank, Warehouse, and test results."
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear All"
      Height          =   375
      Left            =   8520
      TabIndex        =   44
      ToolTipText     =   "Clear all data."
      Top             =   600
      Width           =   1335
   End
   Begin VB.Frame FrameKey 
      Height          =   1575
      Left            =   120
      TabIndex        =   43
      Top             =   480
      Width           =   7215
      Begin NEWSOTALib.SOTAMaskedEdit txtTank 
         Height          =   255
         Left            =   1200
         TabIndex        =   5
         Top             =   600
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   75
      End
      Begin EntryLookupControls.TextLookup lkuItem 
         Height          =   285
         Left            =   4320
         TabIndex        =   9
         Top             =   600
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         ForeColor       =   -2147483640
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Item"
         ParentIDColumn  =   "ItemID"
         ParentKeyColumn =   "ItemKey"
         ParentTable     =   "timItem"
         BoundColumn     =   "ItemKey"
         BoundTable      =   "timSGSDEJTestOnly_SGS"
         IsForeignKey    =   -1  'True
         Datatype        =   0
         sSQLReturnCols  =   "ItemID,lkuitem,;ShortDesc,lblItemDesc,;ItemKey,,;"
      End
      Begin EntryLookupControls.TextLookup lkuWhse 
         Height          =   285
         Left            =   1200
         TabIndex        =   7
         Top             =   1020
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         ForeColor       =   -2147483640
         IsSurrogateKey  =   -1  'True
         LookupID        =   "WarehouseAndKey"
         ParentIDColumn  =   "WhseID"
         ParentKeyColumn =   "WhseKey"
         ParentTable     =   "timWarehouse"
         BoundColumn     =   "WhseKey"
         BoundTable      =   "timSGSDEJTestOnly_SGS"
         IsForeignKey    =   -1  'True
         Datatype        =   0
         sSQLReturnCols  =   "WhseID,lkuWhse,;Description,,;WhseKey,,;"
      End
      Begin LookupViewControl.LookupView LookupView1 
         Height          =   285
         Left            =   4680
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Lookup Existing COA Test Record(s)"
         Top             =   240
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupID        =   "COATestResults"
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtLotNumber 
         Height          =   255
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   75
      End
      Begin VB.Label lblLotNumber 
         Caption         =   "&Lot Number"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1020
      End
      Begin VB.Label lblItemDesc 
         Height          =   255
         Left            =   3840
         TabIndex        =   45
         Top             =   960
         Width           =   3255
      End
      Begin VB.Label Label1 
         Caption         =   "Lookup Record:"
         Height          =   255
         Left            =   3480
         TabIndex        =   0
         ToolTipText     =   "Lookup Existing COA Test Record(s)"
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label lblItemID 
         AutoSize        =   -1  'True
         Caption         =   "Ite&m"
         Height          =   195
         Left            =   3840
         TabIndex        =   8
         Top             =   600
         Width           =   495
      End
      Begin VB.Label lblWhse 
         AutoSize        =   -1  'True
         Caption         =   "&Whse"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   1020
         Width           =   1095
      End
      Begin VB.Label lblTank 
         Caption         =   "&Tank"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   1095
      End
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   6135
      Left            =   120
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   2160
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   10821
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "COA Lot Data"
      TabPicture(0)   =   "imzxx001.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "pnlTab(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "COA Test Results"
      TabPicture(1)   =   "imzxx001.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "pnlTab(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "COA Report Template"
      TabPicture(2)   =   "imzxx001.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "pnlTab(2)"
      Tab(2).ControlCount=   1
      Begin Threed.SSPanel pnlTab 
         Height          =   5700
         Index           =   1
         Left            =   -74880
         TabIndex        =   41
         Top             =   360
         Width           =   9480
         _Version        =   65536
         _ExtentX        =   16722
         _ExtentY        =   10054
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin FPSpreadADO.fpSpread grdMain 
            Height          =   5475
            Left            =   60
            TabIndex        =   42
            Top             =   120
            Width           =   9315
            _Version        =   524288
            _ExtentX        =   16431
            _ExtentY        =   9657
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "imzxx001.frx":2426
            AppearanceStyle =   0
            CellNoteIndicatorColor=   2
            HighlightAlphaBlendColor=   0
         End
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   5700
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   9480
         _Version        =   65536
         _ExtentX        =   16722
         _ExtentY        =   10054
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.TextBox txtComments 
            Height          =   1035
            Left            =   1320
            MaxLength       =   500
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   14
            Top             =   960
            Width           =   7740
         End
         Begin Threed.SSCheck chActive 
            Height          =   255
            Left            =   1320
            TabIndex        =   12
            Top             =   600
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   450
            _StockProps     =   78
            Caption         =   "Is Active"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtSampleID 
            Height          =   255
            Left            =   7560
            TabIndex        =   20
            Top             =   2280
            Visible         =   0   'False
            Width           =   1575
            _Version        =   65536
            _ExtentX        =   2778
            _ExtentY        =   450
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            lMaxLength      =   150
         End
         Begin NEWSOTALib.SOTANumber txtProdQty 
            Height          =   300
            Left            =   1320
            TabIndex        =   16
            Top             =   2280
            Width           =   1575
            _Version        =   65536
            _ExtentX        =   2778
            _ExtentY        =   529
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
            text            =   "           0"
            sDecimalPlaces  =   0
         End
         Begin SOTACalendarControl.SOTACalendar calCmptDate 
            Height          =   255
            Left            =   1320
            TabIndex        =   22
            Top             =   3000
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   450
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaskedText      =   "  /  /    "
            Text            =   "  /  /    "
         End
         Begin SOTACalendarControl.SOTACalendar calTestDate 
            Height          =   255
            Left            =   4560
            TabIndex        =   24
            Top             =   3000
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   450
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaskedText      =   "  /  /    "
            Text            =   "  /  /    "
         End
         Begin SOTACalendarControl.SOTACalendar calSampleDate 
            Height          =   255
            Left            =   7560
            TabIndex        =   26
            Top             =   3000
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   450
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaskedText      =   "  /  /    "
            Text            =   "  /  /    "
         End
         Begin NEWSOTALib.SOTANumber txtShippedQty 
            Height          =   300
            Left            =   4320
            TabIndex        =   18
            Top             =   2280
            Width           =   1575
            _Version        =   65536
            _ExtentX        =   2778
            _ExtentY        =   529
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
            text            =   "           0"
            sDecimalPlaces  =   0
         End
         Begin VB.Label lblShippedQty 
            Caption         =   "Qty Shipped"
            Height          =   255
            Left            =   3360
            TabIndex        =   17
            Top             =   2280
            Width           =   975
         End
         Begin VB.Label lblCmptDate 
            Caption         =   "Complete Date"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   3000
            Width           =   1455
         End
         Begin VB.Label lblTestDate 
            Caption         =   "Test Date"
            Height          =   255
            Left            =   3360
            TabIndex        =   23
            Top             =   3000
            Width           =   1215
         End
         Begin VB.Label lblSampleDate 
            Caption         =   "Sample Date"
            Height          =   255
            Left            =   6480
            TabIndex        =   25
            Top             =   3000
            Width           =   975
         End
         Begin VB.Label lblProdQty 
            Caption         =   "Production Qty"
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   2280
            Width           =   1215
         End
         Begin VB.Label lblSampleID 
            Caption         =   "Sample ID"
            Height          =   255
            Left            =   6360
            TabIndex        =   19
            Top             =   2280
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.Label lblComments 
            Caption         =   "Description"
            Height          =   315
            Left            =   120
            TabIndex        =   13
            Top             =   960
            Width           =   1500
         End
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   5700
         Index           =   2
         Left            =   -74880
         TabIndex        =   48
         Top             =   360
         Width           =   9480
         _Version        =   65536
         _ExtentX        =   16722
         _ExtentY        =   10054
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin TabDlg.SSTab SSTab2 
            Height          =   5535
            Left            =   0
            TabIndex        =   49
            Top             =   120
            Width           =   9495
            _ExtentX        =   16748
            _ExtentY        =   9763
            _Version        =   393216
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   520
            TabCaption(0)   =   "Report Template"
            TabPicture(0)   =   "imzxx001.frx":2810
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblCOATemplate"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblRptFtr"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "txtUserFldAlign(8)"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "txtUserFldAlign(7)"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "txtUserFldAlign(6)"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "txtUserFldAlign(5)"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).Control(6)=   "txtUserFldAlign(4)"
            Tab(0).Control(6).Enabled=   0   'False
            Tab(0).Control(7)=   "txtUserFldAlign(3)"
            Tab(0).Control(7).Enabled=   0   'False
            Tab(0).Control(8)=   "txtUserFldAlign(2)"
            Tab(0).Control(8).Enabled=   0   'False
            Tab(0).Control(9)=   "txtUserFldAlign(1)"
            Tab(0).Control(9).Enabled=   0   'False
            Tab(0).Control(10)=   "txtPgFtrAlign"
            Tab(0).Control(10).Enabled=   0   'False
            Tab(0).Control(11)=   "ddnCOARpts"
            Tab(0).Control(11).Enabled=   0   'False
            Tab(0).Control(12)=   "cmdCOALayout"
            Tab(0).Control(12).Enabled=   0   'False
            Tab(0).Control(13)=   "txtRptFtr"
            Tab(0).Control(13).Enabled=   0   'False
            Tab(0).Control(14)=   "chRptFtrBold"
            Tab(0).Control(14).Enabled=   0   'False
            Tab(0).Control(15)=   "chRptFtrUndrln"
            Tab(0).Control(15).Enabled=   0   'False
            Tab(0).Control(16)=   "FrmeAlign(8)"
            Tab(0).Control(16).Enabled=   0   'False
            Tab(0).Control(17)=   "cmdGetDefaultFtr"
            Tab(0).Control(17).Enabled=   0   'False
            Tab(0).Control(18)=   "ChOverideRptFtr"
            Tab(0).Control(18).Enabled=   0   'False
            Tab(0).ControlCount=   19
            TabCaption(1)   =   "User Fields"
            TabPicture(1)   =   "imzxx001.frx":282C
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblUserFld(1)"
            Tab(1).Control(1)=   "lblUserFld(2)"
            Tab(1).Control(2)=   "lblUserFld(3)"
            Tab(1).Control(3)=   "lblUserFld(4)"
            Tab(1).Control(4)=   "lblUserFld(5)"
            Tab(1).Control(5)=   "lblUserFld(6)"
            Tab(1).Control(6)=   "lblUserFld(7)"
            Tab(1).Control(7)=   "lblUserFld(8)"
            Tab(1).Control(8)=   "txtUser(1)"
            Tab(1).Control(9)=   "chUserBold(1)"
            Tab(1).Control(10)=   "chUserUndrln(1)"
            Tab(1).Control(11)=   "FrmeAlign(0)"
            Tab(1).Control(12)=   "txtUser(2)"
            Tab(1).Control(13)=   "chUserBold(2)"
            Tab(1).Control(14)=   "chUserUndrln(2)"
            Tab(1).Control(15)=   "FrmeAlign(1)"
            Tab(1).Control(16)=   "txtUser(3)"
            Tab(1).Control(17)=   "chUserBold(3)"
            Tab(1).Control(18)=   "chUserUndrln(3)"
            Tab(1).Control(19)=   "FrmeAlign(2)"
            Tab(1).Control(20)=   "txtUser(4)"
            Tab(1).Control(21)=   "chUserBold(4)"
            Tab(1).Control(22)=   "chUserUndrln(4)"
            Tab(1).Control(23)=   "FrmeAlign(3)"
            Tab(1).Control(24)=   "txtUser(5)"
            Tab(1).Control(25)=   "chUserBold(5)"
            Tab(1).Control(26)=   "chUserUndrln(5)"
            Tab(1).Control(27)=   "FrmeAlign(4)"
            Tab(1).Control(28)=   "txtUser(6)"
            Tab(1).Control(29)=   "chUserBold(6)"
            Tab(1).Control(30)=   "chUserUndrln(6)"
            Tab(1).Control(31)=   "FrmeAlign(5)"
            Tab(1).Control(32)=   "txtUser(7)"
            Tab(1).Control(33)=   "chUserBold(7)"
            Tab(1).Control(34)=   "chUserUndrln(7)"
            Tab(1).Control(35)=   "FrmeAlign(6)"
            Tab(1).Control(36)=   "txtUser(8)"
            Tab(1).Control(37)=   "chUserBold(8)"
            Tab(1).Control(38)=   "chUserUndrln(8)"
            Tab(1).Control(39)=   "FrmeAlign(7)"
            Tab(1).Control(40)=   "chUseUserFlds"
            Tab(1).ControlCount=   41
            Begin VB.CheckBox chUseUserFlds 
               Caption         =   "Use User Fields on the COA Report."
               Height          =   255
               Left            =   -73920
               TabIndex        =   127
               ToolTipText     =   "Use the user fields on the COA reports."
               Top             =   360
               Width           =   3015
            End
            Begin VB.CheckBox ChOverideRptFtr 
               Caption         =   "Use this as the COA Report Footer."
               Height          =   255
               Left            =   6360
               TabIndex        =   126
               ToolTipText     =   "User the Repor Footer on this screen for the COA Report."
               Top             =   2460
               Width           =   3015
            End
            Begin VB.CommandButton cmdGetDefaultFtr 
               Caption         =   "Get Default"
               Height          =   315
               Left            =   4440
               TabIndex        =   125
               ToolTipText     =   "Get the Report Footer Default Value."
               Top             =   540
               Width           =   1215
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   8
               Left            =   1200
               TabIndex        =   121
               Top             =   2340
               Width           =   2775
               Begin VB.OptionButton OptnRptFtrAlign 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   124
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnRptFtrAlign 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   123
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnRptFtrAlign 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   122
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chRptFtrUndrln 
               Caption         =   "Underline"
               Height          =   315
               Left            =   4200
               TabIndex        =   120
               Top             =   2430
               Width           =   975
            End
            Begin VB.CheckBox chRptFtrBold 
               Caption         =   "Bold"
               Height          =   255
               Left            =   5520
               TabIndex        =   119
               Top             =   2460
               Width           =   615
            End
            Begin VB.TextBox txtRptFtr 
               Height          =   1005
               Left            =   1200
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   118
               Top             =   1230
               Width           =   8175
            End
            Begin VB.CommandButton cmdCOALayout 
               Caption         =   "COA Layout"
               Height          =   315
               Left            =   1200
               TabIndex        =   116
               ToolTipText     =   "Show image of report layout or format."
               Top             =   3240
               Width           =   1695
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   7
               Left            =   -69480
               TabIndex        =   110
               Top             =   4740
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign08 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   113
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign08 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   112
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign08 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   111
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   8
               Left            =   -66600
               TabIndex        =   109
               Top             =   4830
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   8
               Left            =   -66600
               TabIndex        =   108
               Top             =   5100
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   8
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   107
               Top             =   4830
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   6
               Left            =   -69480
               TabIndex        =   102
               Top             =   4140
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign07 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   105
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign07 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   104
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign07 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   103
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   7
               Left            =   -66600
               TabIndex        =   101
               Top             =   4230
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   7
               Left            =   -66600
               TabIndex        =   100
               Top             =   4500
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   7
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   99
               Top             =   4230
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   5
               Left            =   -69480
               TabIndex        =   94
               Top             =   3540
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign06 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   97
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign06 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   96
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign06 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   95
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   6
               Left            =   -66600
               TabIndex        =   93
               Top             =   3630
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   6
               Left            =   -66600
               TabIndex        =   92
               Top             =   3900
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   6
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   91
               Top             =   3630
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   4
               Left            =   -69480
               TabIndex        =   86
               Top             =   2940
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign05 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   89
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign05 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   88
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign05 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   87
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   5
               Left            =   -66600
               TabIndex        =   85
               Top             =   3030
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   5
               Left            =   -66600
               TabIndex        =   84
               Top             =   3300
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   5
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   83
               Top             =   3030
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   3
               Left            =   -69480
               TabIndex        =   78
               Top             =   2340
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign04 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   81
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign04 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   80
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign04 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   79
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   4
               Left            =   -66600
               TabIndex        =   77
               Top             =   2430
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   4
               Left            =   -66600
               TabIndex        =   76
               Top             =   2700
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   4
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   75
               Top             =   2430
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   2
               Left            =   -69480
               TabIndex        =   70
               Top             =   1740
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign03 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   73
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign03 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   72
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign03 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   71
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   3
               Left            =   -66600
               TabIndex        =   69
               Top             =   1830
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   3
               Left            =   -66600
               TabIndex        =   68
               Top             =   2100
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   3
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   67
               Top             =   1830
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   1
               Left            =   -69480
               TabIndex        =   62
               Top             =   1140
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign02 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   65
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign02 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   64
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign02 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   63
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   2
               Left            =   -66600
               TabIndex        =   61
               Top             =   1230
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   2
               Left            =   -66600
               TabIndex        =   60
               Top             =   1500
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   2
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   59
               Top             =   1230
               Width           =   4335
            End
            Begin VB.Frame FrmeAlign 
               Caption         =   "Alignment"
               Height          =   615
               Index           =   0
               Left            =   -69480
               TabIndex        =   54
               Top             =   540
               Width           =   2775
               Begin VB.OptionButton OptnUserAlign01 
                  Caption         =   "Left"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   57
                  Top             =   240
                  Width           =   735
               End
               Begin VB.OptionButton OptnUserAlign01 
                  Caption         =   "Center"
                  Height          =   255
                  Index           =   2
                  Left            =   840
                  TabIndex        =   56
                  Top             =   240
                  Width           =   855
               End
               Begin VB.OptionButton OptnUserAlign01 
                  Caption         =   "Right"
                  Height          =   255
                  Index           =   3
                  Left            =   1800
                  TabIndex        =   55
                  Top             =   240
                  Width           =   735
               End
            End
            Begin VB.CheckBox chUserUndrln 
               Caption         =   "Underline"
               Height          =   315
               Index           =   1
               Left            =   -66600
               TabIndex        =   53
               Top             =   630
               Width           =   975
            End
            Begin VB.CheckBox chUserBold 
               Caption         =   "Bold"
               Height          =   255
               Index           =   1
               Left            =   -66600
               TabIndex        =   52
               Top             =   900
               Width           =   615
            End
            Begin VB.TextBox txtUser 
               Height          =   525
               Index           =   1
               Left            =   -73920
               MaxLength       =   4000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   51
               Top             =   630
               Width           =   4335
            End
            Begin SOTADropDownControl.SOTADropDown ddnCOARpts 
               Height          =   315
               Left            =   1320
               TabIndex        =   114
               TabStop         =   0   'False
               Top             =   540
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnCOARpts"
            End
            Begin NEWSOTALib.SOTANumber txtPgFtrAlign 
               Height          =   255
               Left            =   4080
               TabIndex        =   128
               Top             =   2760
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   1
               Left            =   1200
               TabIndex        =   129
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   2
               Left            =   1680
               TabIndex        =   130
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   3
               Left            =   2160
               TabIndex        =   131
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   4
               Left            =   2640
               TabIndex        =   132
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   5
               Left            =   3120
               TabIndex        =   133
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   6
               Left            =   3600
               TabIndex        =   134
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   7
               Left            =   4080
               TabIndex        =   135
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber txtUserFldAlign 
               Height          =   255
               Index           =   8
               Left            =   4560
               TabIndex        =   136
               Top             =   3960
               Visible         =   0   'False
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
               text            =   "           1"
               dMinValue       =   1
               dMaxValue       =   3
               sDecimalPlaces  =   0
            End
            Begin VB.Label lblRptFtr 
               Caption         =   "Report Footer:"
               Height          =   255
               Left            =   120
               TabIndex        =   117
               Top             =   1230
               Width           =   1095
            End
            Begin VB.Label lblCOATemplate 
               Caption         =   "COA Template:"
               Height          =   255
               Left            =   120
               TabIndex        =   115
               Top             =   540
               Width           =   1215
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField08:"
               Height          =   255
               Index           =   8
               Left            =   -74880
               TabIndex        =   106
               Top             =   4830
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField07:"
               Height          =   255
               Index           =   7
               Left            =   -74880
               TabIndex        =   98
               Top             =   4230
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField06:"
               Height          =   255
               Index           =   6
               Left            =   -74880
               TabIndex        =   90
               Top             =   3630
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField05:"
               Height          =   255
               Index           =   5
               Left            =   -74880
               TabIndex        =   82
               Top             =   3030
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField04:"
               Height          =   255
               Index           =   4
               Left            =   -74880
               TabIndex        =   74
               Top             =   2430
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField03:"
               Height          =   255
               Index           =   3
               Left            =   -74880
               TabIndex        =   66
               Top             =   1830
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField02:"
               Height          =   255
               Index           =   2
               Left            =   -74880
               TabIndex        =   58
               Top             =   1230
               Width           =   975
            End
            Begin VB.Label lblUserFld 
               Caption         =   "UserField01:"
               Height          =   255
               Index           =   1
               Left            =   -74880
               TabIndex        =   50
               Top             =   630
               Width           =   975
            End
         End
      End
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   30
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   35
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   8445
      Width           =   9990
      _ExtentX        =   17621
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   0
      Width           =   9990
      _ExtentX        =   17621
      _ExtentY        =   741
   End
   Begin VB.PictureBox ofcOffice 
      Height          =   345
      Left            =   6480
      ScaleHeight     =   285
      ScaleWidth      =   195
      TabIndex        =   28
      Top             =   60
      Visible         =   0   'False
      Width           =   255
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   31
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin EntryLookupControls.TextLookup lkuNewItem 
      Height          =   285
      Left            =   7680
      TabIndex        =   47
      Top             =   1560
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   503
      ForeColor       =   -2147483640
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Item"
      ParentIDColumn  =   "ItemID"
      ParentKeyColumn =   "ItemKey"
      ParentTable     =   "timItem"
      BoundColumn     =   "ItemKey"
      BoundTable      =   "timSGSDEJTestOnly_SGS"
      IsForeignKey    =   -1  'True
      Datatype        =   0
      sSQLReturnCols  =   "ItemID,lkuNewItem,;ShortDesc,,;ItemKey,,;"
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   38
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmimzxx001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Const mlUserFld01 = 1
Const mlUserFld02 = 2
Const mlUserFld03 = 3
Const mlUserFld04 = 4
Const mlUserFld05 = 5
Const mlUserFld06 = 6
Const mlUserFld07 = 7
Const mlUserFld08 = 8

Const mlAlgnmntLeft = 1
Const mlAlgnmntCenter = 2
Const mlAlgnmntRight = 3


Public moSotaObjects            As New Collection

Private moContextMenu           As clsContextMenu

Private WithEvents moGMCOATests     As clsGridMgr       'Grid Manager
Attribute moGMCOATests.VB_VarHelpID = -1

Private moOptions               As New clsModuleOptions

Private mbEnterAsTab            As Boolean

Private msCompanyID             As String

Private msUserID                As String

Private miSecurityLevel         As Integer

Private miFilter                As Integer

Private mlLanguage              As Long

Private miMinFormHeight As Long

Private miMinFormWidth As Long

Private miOldFormHeight As Long

Private miOldFormWidth As Long

Public moClass                  As Object

Private moAppDB                 As Object

Private moSysSession            As Object

Private mbCancelShutDown        As Boolean

Private mbLoadSuccess           As Boolean

Private mbSaved                 As Boolean

Private mlRunMode               As Long

Public WithEvents moDmHeader    As clsDmForm
Attribute moDmHeader.VB_VarHelpID = -1

Private WithEvents moDmDetl     As clsDmGrid
Attribute moDmDetl.VB_VarHelpID = -1

Private Const kiHeaderTab = 0 '
Private Const kiDetailTab = 1

Const VBRIG_MODULE_ID_STRING = "frmimzxx001.FRM"

Private mbInBrowse As Boolean

Private msNatCurrID As String

Private msHomeCurrID As String

Private muHomeCurrInfo As CurrencyInfo

Private muNatCurrInfo As CurrencyInfo

Private msLookupRestrict As String
      
Private Const kColExtCOATestRsltsKey = 1
Private Const kColExtLabTestKey = 2
Private Const kColExtLabTestID = 3
Private Const kColExtTestResults = 4
Private Const kColExtLabUOMKey = 5
Private Const kColExtLabUOMID = 6
Private Const kColExtMDL = 7
Private Const kColExtMethodStandardKey = 8
Private Const kColExtMethodStandardID = 9
Private Const kColExtComparisonOperator = 10
Private Const kColExtValueRange01 = 11
Private Const kColExtValueRange02 = 12
Private Const kColExtCOATestSortOrder = 13
Private Const kMAXCol = 13
    
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bLoadSuccess = mbLoadSuccess
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bLoadSuccess", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaved", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbSaved = bNewSaved
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaved", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub SetupDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim SQL As String
    
    '-- Set up drop down properties (see APZDA001 for reference)

    With ddnCOARpts
        SQL = "Select COATemplateReportID, COATemplateReportKey From timCOATemplateReports_SGS "
        
        .SQLStatement = SQL
        
        .InitDynamicList moClass.moAppDB, .SQLStatement
        
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupDropDowns", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim oFramework As Object
    '-- Set up lookup properties (see APZDA001 for reference)
    Set oFramework = moClass.moFramework

    Set oFramework = Nothing
    
    With lkuItem
        Set .Framework = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = msLookupRestrict
    End With
    
    
    With lkuWhse
        Set .Framework = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = msLookupRestrict
    End With
    
    'RKL DEJ (START) 9/25/13
    With lkuNewItem
        Set .Framework = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = msLookupRestrict
    End With
    'RKL DEJ (STOP) 9/25/13

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub SetupBars()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set sbrMain.Framework = moClass.moFramework
    

    With tbrMain
        .Style = sotaTB_TRANSACTION
        .RemoveButton kTbMemo
        .LocaleID = mlLanguage
        .RemoveButton kTbCopyFrom
        .RemoveButton kTbNextNumber
'        .RemoveButton kTbRenameId
'        .AddButton kTbOffice, .GetIndex(kTbHelp)
'        .AddSeparator .GetIndex(kTbHelp)
        sbrMain.BrowseVisible = True

        .LocaleID = mlLanguage
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupOffice()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '-- Entity type/title must be changed to fit your application
    'moOffice.Init moClass, moDmHeader, Me, ofcOffice, _
    '    kEntTypeAPVoucher, "timCOATestResults_SGS.COATestRsltsKey", _
    '    lkuVouchNo, kEntTitleAPVoucher


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupOffice", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub SetupModuleVars()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- Currency variables
    msNatCurrID = msHomeCurrID
    
    '-- Assign the initial filter value
    miFilter = RSID_UNFILTERED

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupModuleVars", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    BindHeader
    BindDetail

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindHeader()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- Bind the parent DM object
    Set moDmHeader = New clsDmForm

    With moDmHeader
        Set .Form = frmimzxx001
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Toolbar = tbrMain
        Set .SOTAStatusBar = sbrMain
        Set .ValidationMgr = valMgr
        .AppName = gsStripChar(Me.Caption, ".")
        .SaveOrder = 1
        
'        .UniqueKey = "Tank, WhseKey, ItemKey"
        .UniqueKey = "LotNumber, Tank, WhseKey, ItemKey, COATestRsltsKey"
        
        .OrderBy = "COATestRsltsKey"
        .AccessType = kDmBuildQueries
        .Table = "timCOATestResults_SGS"

        .Where = "CompanyID = " & gsQuoted(msCompanyID) & " And IsNull(IsHistoryRecord,0) = 0 "
        
        .Bind Nothing, "COATestRsltsKey", SQL_INTEGER
        
        .Bind txtLotNumber, "LotNumber", SQL_VARCHAR
        .Bind txtTank, "Tank", SQL_VARCHAR
        .BindLookup lkuWhse
        .BindLookup lkuItem
        
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind Nothing, "CreateUserID", SQL_VARCHAR
        .Bind Nothing, "CreateDate", SQL_DATE
        .Bind Nothing, "UpdateUserID", SQL_VARCHAR
        .Bind Nothing, "UpdateDate", SQL_DATE
        
        .Bind txtComments, "Comments", SQL_VARCHAR
        .Bind chActive, "IsActive", SQL_INTEGER
        .Bind txtSampleID, "SampleID", SQL_VARCHAR
        
        .Bind calCmptDate, "CompletedDate", SQL_DATE
        .Bind calTestDate, "TestDate", SQL_DATE
        .Bind calSampleDate, "SampleDate", SQL_DATE
        
        .Bind txtProdQty, "ProductionQty", SQL_DECIMAL
        .Bind txtSampleID, "SampleID", SQL_VARCHAR
        
        .BindComboBox ddnCOARpts, "COAReportKey", SQL_INTEGER, kDmUseItemData

        .Bind txtRptFtr, "COATemplatePgFtr", SQL_VARCHAR
        .Bind chRptFtrUndrln, "COATemplatePgFtrUnderline", SQL_INTEGER
        .Bind chRptFtrBold, "COATemplatePgFtrBold", SQL_INTEGER
        .Bind txtPgFtrAlign, "COATemplatePgFtrLeftRightCenter", SQL_INTEGER
        .Bind ChOverideRptFtr, "OverridePgFtr", SQL_INTEGER
        
        .Bind chUseUserFlds, "UseUserFields", SQL_INTEGER
        
        .Bind txtUser(mlUserFld01), "UserField01", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld01), "UserField01LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld01), "UserField01Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld01), "UserField01Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld02), "UserField02", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld02), "UserField02LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld02), "UserField02Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld02), "UserField02Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld03), "UserField03", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld03), "UserField03LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld03), "UserField03Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld03), "UserField03Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld04), "UserField04", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld04), "UserField04LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld04), "UserField04Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld04), "UserField04Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld05), "UserField05", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld05), "UserField05LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld05), "UserField05Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld05), "UserField05Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld06), "UserField06", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld06), "UserField06LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld06), "UserField06Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld06), "UserField06Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld07), "UserField07", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld07), "UserField07LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld07), "UserField07Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld07), "UserField07Underline", SQL_INTEGER
        
        .Bind txtUser(mlUserFld08), "UserField08", SQL_VARCHAR
        .Bind txtUserFldAlign(mlUserFld08), "UserField08LeftRightCenter", SQL_INTEGER
        .Bind chUserBold(mlUserFld08), "UserField08Bold", SQL_INTEGER
        .Bind chUserUndrln(mlUserFld08), "UserField08Underline", SQL_INTEGER
        
        .LinkSource "vluCOATestQtyShipped_EB_RKL", "COATestRsltsKey = <<COATestRsltsKey>>"
        .Link txtShippedQty, "TotalQtyShipped", SQL_DOUBLE
        
        .Init
    End With
    
    If miSecurityLevel <> SecurityLevels.kSecLevelSupervisory Then
        chActive.Enabled = False
        txtSampleID.Enabled = False
        calCmptDate.Enabled = False
        calTestDate.Enabled = False
        calSampleDate.Enabled = False
        ddnCOARpts.Enabled = False
    End If

    If miSecurityLevel = SecurityLevels.kSecLevelDisplayOnly Then
        txtComments.Enabled = False
        txtProdQty.Enabled = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindHeader", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub BindDetail()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Set moDmDetl = New clsDmGrid

    With moDmDetl
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmimzxx001
        Set .Grid = grdMain
        .Table = "timCOATestResultsTests_SGS"
        .UniqueKey = "COATestRsltsKey, LabTestKey"
        .OrderBy = "COATestSortOrder"
        .SaveOrder = 2

        .NoAppend = True
        .NoDelete = True
        .NoInsert = True

        Set .Parent = moDmHeader
        .ParentLink "COATestRsltsKey", "COATestRsltsKey", SQL_INTEGER
        
        '-- Bind the detail columns
        
'        .BindColumn "COATestRsltsKey", kColExtCOATestRsltsKey, SQL_INTEGER
        .BindColumn "LabTestKey", kColExtLabTestKey, SQL_INTEGER
        .BindColumn "LabTestID", kColExtLabTestID, SQL_VARCHAR
        .BindColumn "MethodStandardKey", kColExtMethodStandardKey, SQL_INTEGER
        .BindColumn "MethodStandardID", kColExtMethodStandardID, SQL_VARCHAR
        .BindColumn "LabUOMKey", kColExtLabUOMKey, SQL_INTEGER
        .BindColumn "LabUOMID", kColExtLabUOMID, SQL_VARCHAR
        .BindColumn "ComparisonOperator", kColExtComparisonOperator, SQL_VARCHAR
        .BindColumn "ValueRange01", kColExtValueRange01, SQL_VARCHAR
        .BindColumn "ValueRange02", kColExtValueRange02, SQL_VARCHAR
        .BindColumn "COATestSortOrder", kColExtCOATestSortOrder, SQL_INTEGER
        .BindColumn "MDL", kColExtMDL, SQL_VARCHAR
        .BindColumn "TestResults", kColExtTestResults, SQL_VARCHAR


        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindDetail", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

' ********************************************************************************
'    Desc:  Binds the Grid Manager class to the Grid Manager object.
'   Parms:  None
' ********************************************************************************
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moGMCOATests = New clsGridMgr
    'Bind the Item COA Test Grid Manager
    With moGMCOATests
        Set .Grid = grdMain
        Set .DM = moDmDetl
        Set .Form = Me

        If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        .GridSortEnabled = True
        
        .AllowAdd = False
        .AllowDelete = False
        .AllowInsert = False
        .AllowMoveRow = False
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = New clsContextMenu

    With moContextMenu
'        .BindGrid moLE, grdMain.hWnd
        Set .Form = frmimzxx001
        
        .BindGrid moGMCOATests, grdMain.hwnd
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim iActionCode As Integer
    Dim bGetTests As Boolean
    Dim lcCOATestRsltsKey As Long
    
    lcCOATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
    
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            If moDmHeader.State = kDmStateAdd Then
                bGetTests = True
            End If
        
            If Not valMgr.ValidateForm Then Exit Sub
            
            If IsCOAValid = False Then Exit Sub
            
            If bGetTests = True Then
                HandleToolbarClick kTbSave
            End If
            
            iActionCode = moDmHeader.Action(kDmFinish)
            
            If iActionCode = DMActionResults.kDmSuccess Then
                'If this Lot record is active then update all other lot records to in active.
                Call UpdateLotActive(lcCOATestRsltsKey)
            End If
            
        Case kTbSave
            If moDmHeader.State = kDmStateAdd Then
                bGetTests = True
            End If
            
            If Not valMgr.ValidateForm Then Exit Sub
            
            If IsCOAValid = False Then Exit Sub
            
            iActionCode = moDmHeader.Save(True)

            If bGetTests = True Then
                If GetCOATestResultsTests(True, True) = False Then
                    MsgBox "Could not obtain the test records.", vbExclamation, "MAS 500"
                End If
            End If
            
            If iActionCode = DMActionResults.kDmSuccess Then
                'If this Lot record is active then update all other lot records to in active.
                Call UpdateLotActive(lcCOATestRsltsKey)
            End If

        Case kTbCancel, kTbCancelExit
            ProcessCancel
        
        Case kTbDelete
            'Verify ability to delete
            If CanDeleteCOATest(moDmHeader.GetColumnValue("COATestRsltsKey")) = False Then
                'Cannot Delete This competitor
'                MsgBox "These COA Test Results are associated with one or more Sales Orders.  You cannot delete these COA Test Results.", vbInformation, "MAS 500"
                Exit Sub
            End If
            
            iActionCode = moDmHeader.Action(kDmDelete)
            
        Case kTbNextNumber
            HandleNextNumberClick
        
        Case kTbMemo
            '-- The Memo button was pressed by the user
            'CMMemoSelected lkuVendID
        
        Case kTbOffice
            'Refer to documentation to determine the steps necessary
            'to correctly initialize Office
            'moOffice.Start

        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'moDmHeader.CopyFrom

        Case kTbRenameId
            moDmHeader.RenameID
            
        Case kTbFilter
            miFilter = giToggleLookupFilter(miFilter)
            
        '-- Trap the browse control buttons
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            HandleBrowseClick sKey
                        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmHeader, moClass
            
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Function CanDeleteCOATest(ByVal COATestRsltsKey As Long) As Boolean
    On Error GoTo Error
    Dim SQL As String
    Dim lCnt As Long
    
    If glGetValidLong(moDmHeader.GetColumnValue("IsActive")) <> 0 Then
        'is active = true
        If miSecurityLevel <> SecurityLevels.kSecLevelSupervisory And miSecurityLevel <> SecurityLevels.kSecLevelNormal Then
            CanDeleteCOATest = False
            MsgBox "You do not have permissions to delete this record.", vbExclamation, "MAS 500"
            Exit Function
        End If
    End If
    
'    SQL = "Select count(*) cnt from tsoCompetitorBids_SGS Where COATestRsltsKey = " & COATestRsltsKey
'
'    lCnt = glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "tsoCompetitorBids_SGS", "COATestRsltsKey = " & glGetValidLong(COATestRsltsKey)))
'
'    If lCnt > 0 Then
'        CanDeleteCOATest = False
'        MsgBox "These COA Test Results are associated with one or more Sales Orders.  You cannot delete these COA Test Results.", vbInformation, "MAS 500"
'    Else
'        CanDeleteCOATest = True
'    End If
    
    CanDeleteCOATest = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".CanDeleteCOATest(COATestRsltsKey) as Boolean " & vbCrLf & _
    "COATestRsltsKey = " & CStr(COATestRsltsKey) & vbCrLf & _
    "The following error occurred while validating the deletion: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbInformation, "MAS 500"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CanDeleteCOATest", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub HandleNextNumberClick()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sNextNo As String
    
    If Not bConfirmUnload(0) Then Exit Sub
    
    '-- Clear the form
    ProcessCancel
    
    '-- Retrieve the next Voucher Number now
    'sNextNo = sGetNextNo()
    MsgBox "Sage MAS 500 Repository" & vbNewLine & "The Next Number procedure needs to be coded manually, please refer to the MAS 500 documentation for advice."
    If (Len(Trim$(sNextNo)) > 0) Then
        '-- Assign the next No to the control
        'lkuKeyControl = sNextNo
        
        '-- Fire the KeyChange event now

        valMgr_KeyChange
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleNextNumberClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleBrowseClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lRet                  As Long
    Dim sNewKey          As String
    Dim iConfirmUnload  As Integer
    
    Select Case sKey

        Case kTbFilter

            miFilter = giToggleLookupFilter(miFilter)

        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            bSetupLookupView1
            
            bConfirmUnload iConfirmUnload, True
            If Not iConfirmUnload = kDmSuccess Then Exit Sub
                
                lRet = glLookupBrowse(LookupView1, sKey, miFilter, sNewKey)
           
                Select Case lRet

                    Case MS_SUCCESS

                        If LookupView1.ReturnColumnValues.Count = 0 Then Exit Sub

                        '!!! TO DO: Replace ReturnColumnValues Key "CustID" with ColumnName to be placed into the control.
'                        If StrComp(Trim(txtLotNumber.Text), Trim(LookupView1.ReturnColumnValues("LotNumber")), vbTextCompare) <> 0 Or _
'                            StrComp(Trim(lkuWhse.Text), Trim(LookupView1.ReturnColumnValues("WhseID")), vbTextCompare) <> 0 Or _
'                            StrComp(Trim(lkuItem.Text), Trim(LookupView1.ReturnColumnValues("ItemID")), vbTextCompare) <> 0 Or _
'                            StrComp(Trim(txtTank.Text), Trim(LookupView1.ReturnColumnValues("Tank")), vbTextCompare) <> 0 Then
             
                        If StrComp(Trim(lkuWhse.Text), Trim(LookupView1.ReturnColumnValues("WhseID")), vbTextCompare) <> 0 Or _
                            StrComp(Trim(lkuItem.Text), Trim(LookupView1.ReturnColumnValues("ItemID")), vbTextCompare) <> 0 Or _
                            StrComp(Trim(txtTank.Text), Trim(LookupView1.ReturnColumnValues("Tank")), vbTextCompare) <> 0 Or _
                            StrComp(Trim(txtLotNumber.Text), Trim(LookupView1.ReturnColumnValues("LotNumber")), vbTextCompare) <> 0 Then
                                
                                lkuWhse.Text = Trim(LookupView1.ReturnColumnValues("WhseID"))
                                lkuItem.Text = Trim(LookupView1.ReturnColumnValues("ItemID"))
                                txtTank.Text = Trim(LookupView1.ReturnColumnValues("Tank"))
                                txtLotNumber.Text = Trim(LookupView1.ReturnColumnValues("LotNumber"))
                                
                                moDmHeader.SetColumnValue "COATestRsltsKey", glGetValidLong(LookupView1.ReturnColumnValues("COATestRsltsKey"))
                                
                                valMgr_KeyChange
                        End If
           
                    Case Else

                        gLookupBrowseError lRet, Me, moClass

                End Select

        End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleBrowseClick", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmHeader.Action kDmCancel

    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ProcessCancel", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bConfirmUnload(iConfirmUnload As Integer, Optional ByVal bNoClear As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    If Not valMgr.ValidateForm Then Exit Function
    
    iConfirmUnload = moDmHeader.ConfirmUnload(bNoClear)

    If (iConfirmUnload = kDmSuccess) Then
        bConfirmUnload = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bConfirmUnload", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    Dim sMenuText As String

    CMAppendContextMenu = False

'-- This will need to be specific to your application

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim lVoucherKey As Long
    Dim iPos As Integer
    Dim oDDN As Object
    Dim sClassID As String
    Dim lRealTaskID As Long
    
    CMMenuSelected = False

'-- This will need to be specific to your application
   
    CMMenuSelected = True

    Exit Function

ExpectedErrorRoutine:
    SetHourglass False
    
    If oDDN Is Nothing Then
        iPos = InStr(sClassID, ".")
        giSotaMsgBox Nothing, moClass.moSysSession, kmsgDDNNotRegistered, _
            Left$(sClassID, iPos) & "dll"
    
        gClearSotaErr
        Exit Function
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Sub CMMemoSelected(oCtl As Control)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'-- This will need to be specific to your application


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMMemoSelected", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function bSetupNumerics() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*************************************************************************
' This routine will properly format all the currency controls on the form.
'*************************************************************************
    Dim bValid As Boolean
    Dim iIntegralPlaces As Integer
    
    bSetupNumerics = False

    '-- Set attributes for home currency controls
    If (Len(Trim$(msHomeCurrID)) > 0) Then
        SetHomeCurrCtrls msHomeCurrID
    Else
        '-- Apparently, the home currency hasn't been set
        giSotaMsgBox Nothing, moSysSession, kmsgSetCurrControlsError, _
            gsBuildString(kNone, moAppDB, moSysSession)
        Exit Function
    End If

    '-- Set attributes for natural currency controls
    If (Len(Trim$(msNatCurrID)) > 0) Then
        SetNatCurrCtrls msNatCurrID, False
    Else
        '-- Apparently, the natural currency hasn't been set
        giSotaMsgBox Nothing, moSysSession, kmsgSetCurrControlsError, _
            gsBuildString(kNone, moAppDB, moSysSession)
        Exit Function
    End If

    '-- Set properties for number fields
    'nbrQuantity.DecimalPlaces = moOptions.CI("QtyDecPlaces")
    
    '-- Setting the integral places at run-time sets it back to 12!! We only want 8.
    'iIntegralPlaces = IIf(kiQtyLen - moOptions.CI("QtyDecPlaces") > kiQtyIntegralPlaces, _
    '    kiQtyIntegralPlaces, kiQtyLen - moOptions.CI("QtyDecPlaces"))
    'nbrQuantity.IntegralPlaces = iIntegralPlaces
    
    bSetupNumerics = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetupNumerics", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub SetHomeCurrCtrls(sCurrID As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim bValid As Boolean
    Dim iIntegralPlaces As Integer

'-- The HC controls on the form need to be identified here
    

    '-- Set attributes for home currency controls
    'bValid = gbSetCurrCtls(moClass, sCurrID, muHomeCurrInfo, _
                'curInvTotalHC, curInvTotalHCS, curDetlAmtHC, curFormatHC, curBatchTotal)
    
    If bValid Then
        '-- Set integral places for Decimal(15,3) fields
    End If

    '-- Display the currency descriptions
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetHomeCurrCtrls", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub SetNatCurrCtrls(sCurrID As String, Optional bRefresh As Boolean = True)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim bValid As Boolean
    Dim iIntegralPlaces As Integer
    
'-- The NC controls on the form need to be identified here
    
    '-- Set attributes for natural currency controls
    
    If bValid Then
        '-- Set integral places for Decimal(15,3) fields

    End If
    
    '-- Display the currency descriptions
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetNatCurrCtrls", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub InitializeDetlGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************
' Desc: Initializes/formats the Detail grid.
'*******************************************
    Dim iGridType As Integer
    Dim sTitle As String

    '-- Set default grid properties
    gGridSetProperties grdMain, kMAXCol, kGridDataSheet  ' use datasheet grid formating????
 
    'Added so that you locked cells cannot get focus
    grdMain.EditModePermanent = True
  
    'Setup maximum number of rows and columns
    gGridSetMaxRows grdMain, 1
    gGridSetMaxCols grdMain, kMAXCol

    'Set column widths
    grdMain.UnitType = SS_CELL_UNIT_TWIPS
    
    gGridSetHeader grdMain, kColExtCOATestRsltsKey, "COATestRsltsKey"
    gGridSetHeader grdMain, kColExtLabTestKey, "LabTestKey"
    gGridSetHeader grdMain, kColExtLabTestID, "COA Test"
    gGridSetHeader grdMain, kColExtMethodStandardKey, "MethodStandardKey"
    gGridSetHeader grdMain, kColExtMethodStandardID, "Method Standard"
    gGridSetHeader grdMain, kColExtLabUOMKey, "LabUOMKey"
    gGridSetHeader grdMain, kColExtLabUOMID, "Unit Of Measure"
    gGridSetHeader grdMain, kColExtComparisonOperator, "Compare Operator"
    gGridSetHeader grdMain, kColExtValueRange01, "Value One"
    gGridSetHeader grdMain, kColExtValueRange02, "Value Two"
    gGridSetHeader grdMain, kColExtCOATestSortOrder, "COA Sort Order"
    gGridSetHeader grdMain, kColExtMDL, "MDL"
    gGridSetHeader grdMain, kColExtTestResults, "Test Results"
    
    gGridSetColumnWidth grdMain, kColExtCOATestRsltsKey, 1000
    gGridSetColumnWidth grdMain, kColExtLabTestKey, 1000
    gGridSetColumnWidth grdMain, kColExtLabTestID, 3500
    gGridSetColumnWidth grdMain, kColExtMethodStandardKey, 1000
    gGridSetColumnWidth grdMain, kColExtMethodStandardID, 2500
    gGridSetColumnWidth grdMain, kColExtLabUOMKey, 1000
    gGridSetColumnWidth grdMain, kColExtLabUOMID, 1500
    gGridSetColumnWidth grdMain, kColExtComparisonOperator, 1500
    gGridSetColumnWidth grdMain, kColExtValueRange01, 1500
    gGridSetColumnWidth grdMain, kColExtValueRange02, 1500
    gGridSetColumnWidth grdMain, kColExtCOATestSortOrder, 1000
    gGridSetColumnWidth grdMain, kColExtMDL, 3500
    gGridSetColumnWidth grdMain, kColExtTestResults, 1500
    
    gGridSetColumnType grdMain, kColExtCOATestRsltsKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColExtLabTestKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColExtLabTestID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtMethodStandardKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColExtMethodStandardID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtLabUOMKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColExtLabUOMID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtComparisonOperator, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtValueRange01, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtValueRange02, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtCOATestSortOrder, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColExtMDL, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColExtTestResults, SS_CELL_TYPE_EDIT, 100
    
    gGridLockColumn grdMain, kColExtCOATestRsltsKey
    gGridLockColumn grdMain, kColExtLabTestKey
    gGridLockColumn grdMain, kColExtLabTestID
    gGridLockColumn grdMain, kColExtMethodStandardKey
    gGridLockColumn grdMain, kColExtMethodStandardID
    gGridLockColumn grdMain, kColExtLabUOMKey
    gGridLockColumn grdMain, kColExtLabUOMID
    gGridLockColumn grdMain, kColExtComparisonOperator
    gGridLockColumn grdMain, kColExtValueRange01
    gGridLockColumn grdMain, kColExtValueRange02
    gGridLockColumn grdMain, kColExtCOATestSortOrder
    gGridLockColumn grdMain, kColExtMDL
    If miSecurityLevel <> SecurityLevels.kSecLevelSupervisory Then
        gGridLockColumn grdMain, kColExtTestResults
    End If
    
    gGridHideColumn grdMain, kColExtCOATestRsltsKey
    gGridHideColumn grdMain, kColExtLabTestKey
'    gGridHideColumn grdMain, kColExtLabTestID
    gGridHideColumn grdMain, kColExtMethodStandardKey
'    gGridHideColumn grdMain, kColExtMethodStandardID
    gGridHideColumn grdMain, kColExtLabUOMKey
'    gGridHideColumn grdMain, kColExtLabUOMID
    gGridHideColumn grdMain, kColExtComparisonOperator
    gGridHideColumn grdMain, kColExtValueRange01
    gGridHideColumn grdMain, kColExtValueRange02
'    gGridHideColumn grdMain, kColExtCOATestSortOrder
'    gGridHideColumn grdMain, kColExtMDL
'    gGridHideColumn grdMain, kColExtTestResults

    'Let the user resize the columns.
    grdMain.UserResizeCol = UserResizeOn
    
    'Scroll bars
    grdMain.ScrollBars = ScrollBarsBoth

    '-- Setup the grid's Currency columns based on the Natural Currency ID
    bSetupGridNumerics
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "InitializeDetlGrid", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bSetupGridNumerics() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bSetupGridNumerics = True
    
'-- The numeric columns in the grid need to be identified here
    

    '-- Setup the grid's Currency columns based on the Natural Currency ID

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetupGridNumerics", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function



Private Sub chActive_Validate(Cancel As Boolean)
    On Error GoTo Error
    Dim i As Long
    Dim TestResult As String
    Dim bValid As Boolean
    Dim lCOATestRsltsKey As Long
    Dim lActiveRecCount As Long
    Dim sCurrentActiveLot As String
    Dim lOldCOATestRsltsKey As Long
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim sTank As String
    Dim SQLFilter As String
    
    'Check that all tests have a result
    If chActive.Value = True Then
        If Trim(txtLotNumber.Text) = Empty Then
            MsgBox "The Lot Number is required before activation.", vbExclamation, "MAS 500"
            chActive.Value = False
            Exit Sub
        End If
        
        If IsDate(calCmptDate.Value) = False Then
            MsgBox "The Complete Date is required before activation.", vbExclamation, "MAS 500"
            chActive.Value = False
            Exit Sub
        End If
        
        For i = 1 To grdMain.MaxRows
            
            'Set default to True
            bValid = True
            'Validate one test results
            Call moDmDetl_DMGridBeforeUpdate(i, bValid)
            
            If bValid = False Then
                chActive.Value = False
                Exit Sub
            End If
            
            TestResult = Trim(gsGetValidStr(moDmDetl.GetColumnValue(i, "TestResults")))
            
            If TestResult = Empty Then
'                Cancel = True
                chActive.Value = False
                
                MsgBox "Not all test results have been entered.  You have to enter all test results before activating.", vbExclamation, "MAS 500"
                
                SSTab1.Tab = kiDetailTab
                
                Exit Sub
            End If
            
            TestResult = Empty
            
        Next
        
        'Make sure there is not already a Item, Warehouse, Tank that is active.
        'Get Current Record Key
        lCOATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
        
        sTank = Trim(txtTank.Text)
        lItemKey = glGetValidLong(lkuItem.KeyValue)
        lWhseKey = glGetValidLong(lkuWhse.KeyValue)
        
        SQLFilter = "IsNull(IsActive,0) = 1 And IsNull(IsHistoryRecord,0) <> 1 and COATestRsltsKey <> " & lCOATestRsltsKey
        SQLFilter = SQLFilter & " And ItemKey = " & lItemKey & " And WhseKey = " & lWhseKey & " And Tank = " & gsQuoted(sTank)
        
        lActiveRecCount = glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "timCOATestResults_SGS", SQLFilter))
        
        
        If lActiveRecCount > 0 Then
            lOldCOATestRsltsKey = glGetValidLong(moClass.moAppDB.Lookup("COATestRsltsKey", "timCOATestResults_SGS", SQLFilter))
            sCurrentActiveLot = gsGetValidStr(moClass.moAppDB.Lookup("LotNumber", "timCOATestResults_SGS", "COATestRsltsKey = " & lOldCOATestRsltsKey))
            
            If MsgBox("This item, warehouse, and tank are already active with lot number " & sCurrentActiveLot & ".  Do you want to archive Lot " & sCurrentActiveLot & " and make this Lot (" & Trim(txtLotNumber.Text) & ") the active lot?", vbYesNo, "Confirm Lot Activation") <> vbYes Then
                'No not active
                chActive.Value = False
                Exit Sub
            Else
                HandleToolbarClick kTbSave
                
                'Archive the current active lots
                If ClearAll(lOldCOATestRsltsKey) = False Then
                    'The clear all failed
                End If

            End If
            
        End If
        
    Else
        lCOATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
        If 1 = glGetValidLong(moClass.moAppDB.Lookup("IsNull(IsActive,0)", "timCOATestResults_SGS", "COATestRsltsKey = " & lCOATestRsltsKey)) Then
            'No Need to ask for archive if not already active in the database
    
            If MsgBox("Do you want to archive this record?" & vbCrLf & vbCrLf & _
            "If you choose yes this record will not be available anymore.", vbYesNo, "Archive this record?") = vbYes Then
                'Save the record
                HandleToolbarClick kTbFinishExit
    
                'Archive the current active lots
                If ClearAll(lCOATestRsltsKey) = False Then
                    'The clear all failed
                End If
            End If
        End If
    End If
    Exit Sub
Error:
    MsgBox Me.Name & ".chActive_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub

Private Sub cmdClear_Click()
    On Error GoTo Error
    
    Dim lcCOATestRsltsKey As Long
    Dim lcWarehouseID As String
    Dim lcItemID As String
    Dim lcTank As String
    Dim bValid As Boolean

    
    lcWarehouseID = lkuWhse.Text
    lcItemID = lkuItem.Text
    lcTank = txtTank.Text
    
    'Check to see that a record has been seleted to be cleared.
    If lcWarehouseID = Empty Or lcItemID = Empty Or lcTank = Empty Then Exit Sub
    
    lcCOATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
    If lcCOATestRsltsKey <= 0 Then Exit Sub
    
    'Make sure that the record can be cleared/sent to history.
    If Trim(txtLotNumber.Text) = Empty Then
        MsgBox "You can only clear records that have a Lot Number and are Active.", vbExclamation, "MAS 500"
        Exit Sub
    End If
    
    If chActive.Value <> True Then
        MsgBox "You can only clear Active records.", vbExclamation, "MAS 500"
        Exit Sub
    End If
    
    'Make sure that the user really wants to clear the record
    If MsgBox("Are you sure you want to clear this record?  Clearing the record will remove all the test results for this tank, warehouse, and item.", vbYesNo, "MAS 500") = vbYes Then
        
        'Save the record
        HandleToolbarClick kTbFinishExit
        
        'Clear all data from the record and make a backup/historical
        If ClearAll(lcCOATestRsltsKey) = False Then
            'The clear all failed
        End If
        
        'Reload the record
        lkuWhse.Text = lcWarehouseID
        lkuItem.Text = lcItemID
        txtTank.Text = lcTank
        
        'Made Lot part of the key so after we reset user needs to provide the New Lot Number
        If Not Me.ActiveControl Is txtLotNumber And txtLotNumber.Enabled = True Then
            txtLotNumber.SetFocus
        End If
        
'        moDmHeader_DMBeforeInsert bValid
'        If bValid = True Then
'            valMgr_KeyChange
'        Else
'            MsgBox "Could not obtain the new key (after clearing the data) for the new record.", vbCritical, "MAS 500"
'        End If

    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdClear_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub cmdCOALayout_Click()
    On Error Resume Next
    
    frmCOALayout.Show vbModal, Me
    
    Err.Clear
End Sub

Private Sub cmdCopy_Click()
    On Error GoTo Error
    
    Dim ItemKey As Long
    Dim ItemID As String
    Dim COATestRsltsKey As Long
'
'    lkuNewItem.ClearData
'
'    lkuNewItem.DoLookupClick
'
'    ItemKey = glGetValidLong(lkuNewItem.KeyValue)
'    ItemID = gsGetValidStr(lkuNewItem.ReturnColumnValues("ItemID"))
'
'    If ItemKey <= 0 Then
'        Debug.Print lkuNewItem.LookupID
'        Debug.Print lkuNewItem.Text
'        Exit Sub
'    End If
    
    If CopyCOARecord(ItemKey, ItemID, COATestRsltsKey) = False Then
        Exit Sub
    End If
    
'    lkuWhse.Text = LookupView1.ReturnColumnValues("WhseID")
'    txtTank.Text = LookupView1.ReturnColumnValues("Tank")
'    txtLotNumber.Text = LookupView1.ReturnColumnValues("LotNumber")
    
    lkuItem.Text = ItemID

    moDmHeader.SetColumnValue "COATestRsltsKey", COATestRsltsKey
    
    valMgr_KeyChange
    
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdCopy_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub cmdGetDefaultFtr_Click()
    On Error GoTo Error
    
    Dim lRptKey As Long
    Dim lLeftRightCenter As Long
    
    lRptKey = glGetValidLong(ddnCOARpts.ItemData)
    
    If lRptKey <= 0 Then
        Exit Sub
    End If
    
    txtRptFtr.Text = gsGetValidStr(moClass.moAppDB.Lookup("COATemplatePgFtr", "timCOATemplateReports_SGS", "COATemplateReportKey = " & lRptKey))
    
    lLeftRightCenter = glGetValidLong(moClass.moAppDB.Lookup("COATemplatePgFtrLeftRightCenter", "timCOATemplateReports_SGS", "COATemplateReportKey = " & lRptKey))
    Select Case lLeftRightCenter
        Case 1, 2, 3
            txtPgFtrAlign.Value = lLeftRightCenter
        Case Else
            txtPgFtrAlign.Value = 1
    End Select
    
    chRptFtrUndrln.Value = glGetValidLong(moClass.moAppDB.Lookup("COATemplatePgFtrUnderline", "timCOATemplateReports_SGS", "COATemplateReportKey = " & lRptKey))
    
    chRptFtrBold.Value = glGetValidLong(moClass.moAppDB.Lookup("COATemplatePgFtrBold", "timCOATemplateReports_SGS", "COATemplateReportKey = " & lRptKey))
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdGetDefaultFtr_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
         
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
    
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbLoadSuccess = False
    
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msHomeCurrID = .CurrencyID
    End With
    Set moAppDB = moClass.moAppDB
    msLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)

    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With

'    Me.Caption = "COA Test Results"
        
     With moOptions
        Set .oSysSession = moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    
     SetupModuleVars
    
    '-- Setup currency controls
    If Not bSetupNumerics() Then Exit Sub
    
     SetupBars
    
     SetupLookups
    
     SetupDropDowns
    
     miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmHeader, moDmDetl)
     
     BindForm
    
     BindGM
     
     InitializeDetlGrid
    
     BindContextMenu
    
     SetupOffice
        
     'SetFieldStates
    
     'SetTBButtonStates
    
    '-- Make sure the header tab is shown to the user first
    'tabVoucher.Tab = kiHeaderTab
    
'    tabDataEntry.Tab = kiHeaderTab
 
  '-- Disable controls on hidden tabs
    Dim i As Integer
'   pnlTab(kiHeaderTab).Enabled = True
'   For i = 0 To pnlTab.Count - 1
'        pnlTab(i).Enabled = False
'   Next i
 
    '-- Grid starts out with 500 rows.Remove the rows then add the header
    grdMain.MaxRows = 0
    grdMain.MaxRows = 1
    
    moGMCOATests.GridSortEnabled = False
    
    '-- We made it through Form_Load successfully
    mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
    Dim bValid As Boolean
   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If (moClass.mlError = 0) Then
        '-- If the form is dirty, prompt the user to save the record
         bValid = bConfirmUnload(iConfirmUnload)
        
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                
            Case kDmFailure, kDmError
                GoTo CancelShutDown
        
        End Select
      
        '-- Clear the form
        ProcessCancel
        
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                moClass.miShutDownRequester = kUnloadSelfShutDown
        End Select
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.


    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

'-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    '-- Clean up object references
    If Not moDmHeader Is Nothing Then
        moDmHeader.UnloadSelf
        Set moDmHeader = Nothing
    End If
    
    If Not moDmDetl Is Nothing Then
        moDmDetl.UnloadSelf
        Set moDmDetl = Nothing
    End If
    
    If Not (moGMCOATests Is Nothing) Then
        moGMCOATests.UnloadSelf
        Set moGMCOATests = Nothing
    End If

    '-- Fire Terminate event for lookups
    TerminateControls Me

    '-- Clean up other objects
    Set moOptions = Nothing
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing
    Set moAppDB = Nothing
    Set moSysSession = Nothing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
    '    '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
                        pnlTab(kiDetailTab), _
                        grdMain, SSTab1, pnlTab(kiHeaderTab)

        '-- Move the controls to the right
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
                        pnlTab(kiDetailTab), _
                        grdMain, SSTab1, pnlTab(kiHeaderTab)
        
        If SSTab1.Tab = 1 Then
            moGMCOATests.Scroll
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdMain_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    moGMCOATests_CellChange Row, Col
End Sub

Private Sub grdMain_Change(ByVal Col As Long, ByVal Row As Long)
    moGMCOATests.Grid_Change Col, Row

End Sub

Private Sub grdMain_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
    moGMCOATests.Grid_ColWidthChange Col1

End Sub

Private Sub grdMain_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    moGMCOATests.Grid_EditMode Col, Row, Mode, ChangeMade

End Sub

Private Sub grdMain_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    
    moGMCOATests.Grid_LeaveCell Col, Row, NewCol, NewRow

End Sub

Private Sub grdMain_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
    moGMCOATests.Grid_LeaveRow Row, NewRow

End Sub

Private Sub grdMain_RightClick(ByVal ClickType As Integer, ByVal Col As Long, ByVal Row As Long, ByVal MouseX As Long, ByVal MouseY As Long)
    moGMCOATests.MenuDelete = False

End Sub

Private Sub grdMain_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    moGMCOATests.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

End Sub


Private Sub lkuItem_LostFocusText()
    CheckKeyComplete
End Sub

Private Sub lkuWhse_LostFocusText()
    CheckKeyComplete
End Sub

Private Sub LookupView1_Click()
    
    Dim iConfirmUnload As Integer
    
    bSetupLookupView1
    
    bConfirmUnload iConfirmUnload, True
    If Not iConfirmUnload = kDmSuccess Then Exit Sub
    
    gcLookupClick Me, LookupView1, txtTank, "Tank"
    
    If LookupView1.ReturnColumnValues.Count > 0 Then
        lkuWhse.Text = LookupView1.ReturnColumnValues("WhseID")
        lkuItem.Text = LookupView1.ReturnColumnValues("ItemID")
        txtTank.Text = LookupView1.ReturnColumnValues("Tank")
        txtLotNumber.Text = LookupView1.ReturnColumnValues("LotNumber")

        moDmHeader.SetColumnValue "COATestRsltsKey", glGetValidLong(LookupView1.ReturnColumnValues("COATestRsltsKey"))
        
        valMgr_KeyChange
    End If

End Sub

Private Sub moDmDetl_DMGridAfterDelete(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False

'-- Perform processing after a row is deleted from the detail table here


    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridAfterDelete", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmDetl_DMGridAfterInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False

'-- Perform processing after a row is inserted into the detail table here

    
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridAfterInsert", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmDetl_DMGridAfterUpdate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False

'-- Perform processing after a row is updated in the detail table here

    
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridAfterUpdate", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmDetl_DMGridAppend(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- This adds a new row to the grandchild detail grid


'-- Perform processing after a row is appended to the detail grid here

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridAppend", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmDetl_DMGridBeforeUpdate(lRow As Long, bValid As Boolean)
    
    
'************************************************************************************************
'EB DEJ 2/18/13 (Start)
'************************************************************************************************
    'EB DEJ 2/15/13 Testing code only
    On Error GoTo Error
    
    Dim NbrRsltsVal As Double
    Dim StrRsltsVal As String
    
    Dim NbrValRange01 As Double
    Dim NbrValRange02 As Double
    Dim StrValRange01 As String
    
    Dim LabTestID As String
    Dim ComparisonOperator As String

    LabTestID = gsGetValidStr(moDmDetl.GetColumnValue(lRow, "LabTestID"))
    ComparisonOperator = Trim(UCase(gsGetValidStr(moDmDetl.GetColumnValue(lRow, "ComparisonOperator"))))
    
    Select Case ComparisonOperator
        Case ">"
            If IsNumeric(moDmDetl.GetColumnValue(lRow, "ValueRange01")) = True Then
                If IsNumeric(moDmDetl.GetColumnValue(lRow, "TestResults")) = True Then
                    NbrValRange01 = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "ValueRange01"))
                    NbrRsltsVal = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "TestResults"))
                    
                    If NbrRsltsVal <= NbrValRange01 Then
                        'Not a valid value
                        bValid = False
                        MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result is not greater than " & NbrValRange01 & ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                        Exit Sub
                    End If
                Else
                    'Not a valid value
                    bValid = False
                    MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result is not a valid number.  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                    Exit Sub
                End If
            Else
                'The item was not set up correctly... (<, >, Between, all are required to be valid numbers).
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' was not setup correctly.  A number was expected: The Compare Operators (>, <, Between) require numeric values.  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
        
        Case "<"
            If IsNumeric(moDmDetl.GetColumnValue(lRow, "ValueRange01")) = True Then
                If IsNumeric(moDmDetl.GetColumnValue(lRow, "TestResults")) = True Then
                    NbrValRange01 = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "ValueRange01"))
                    NbrRsltsVal = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "TestResults"))
                    
                    If NbrRsltsVal >= NbrValRange01 Then
                        'Not a valid value
                        bValid = False
                        MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result is not less than " & NbrValRange01 & ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                        Exit Sub
                    End If
                Else
                    'Not a valid value
                    bValid = False
                    MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result is not a valid number.  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                    Exit Sub
                End If
            Else
                'The item was not set up correctly... (<, >, Between, all are required to be valid numbers).
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' was not setup correctly.  A number was expected: The Compare Operators (>, <, Between) require numeric values.  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
        
        Case "BETWEEN"
            'Get Test Parameter 1
            If IsNumeric(moDmDetl.GetColumnValue(lRow, "ValueRange01")) = True Then
                NbrValRange01 = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "ValueRange01"))
            Else
                'The item was not set up correctly... (<, >, Between, all are required to be valid numbers).
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' was not setup correctly.  A number was expected: " & _
                "The Compare Operators (>, <, Between) require numeric values.  The value provided is: " & _
                gsGetValidStr(moDmDetl.GetColumnValue(lRow, "ValueRange01")) & _
                ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
            
            'Get Test Parameter 2
            If IsNumeric(moDmDetl.GetColumnValue(lRow, "ValueRange02")) = True Then
                NbrValRange02 = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "ValueRange02"))
            Else
                'The item was not set up correctly... (<, >, Between, all are required to be valid numbers).
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' was not setup correctly.  A number was expected: " & _
                "The Compare Operators (>, <, Between) require numeric values.  The value provided is: " & _
                gsGetValidStr(moDmDetl.GetColumnValue(lRow, "ValueRange02")) & _
                ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
            
            'Get Test Result
            If IsNumeric(moDmDetl.GetColumnValue(lRow, "TestResults")) = True Then
                NbrRsltsVal = gdGetValidDbl(moDmDetl.GetColumnValue(lRow, "TestResults"))
            Else
                'Not a valid value
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result is not a valid number.  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
            
            'Validate Result falls within range.
            If Not (NbrRsltsVal >= NbrValRange01 And NbrRsltsVal <= NbrValRange02) Then
                'Not a valid value
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result '" & NbrRsltsVal & "' is not Between " & NbrValRange01 & " and " & NbrValRange02 & ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
            
        Case "=", ""
            'Get Test Parameter
            StrValRange01 = UCase(Trim(gsGetValidStr(moDmDetl.GetColumnValue(lRow, "ValueRange01"))))
            
            'Get Test Result
            StrRsltsVal = UCase(Trim(gsGetValidStr(moDmDetl.GetColumnValue(lRow, "TestResults"))))
            
            'Validate Result
            If StrRsltsVal <> StrValRange01 Then
                'Not a valid value
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result '" & StrRsltsVal & "' does not equal " & StrValRange01 & ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
            End If
            
        Case Else
                bValid = False
                MsgBox "Line #" & lRow & ", Test:'" & LabTestID & "' result '" & StrRsltsVal & "' does not equal " & StrValRange01 & ".  You will need to fix this issue before saving.", vbInformation, "MAS 500"
                Exit Sub
    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".sldfj()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
'************************************************************************************************
'EB DEJ 2/18/13 (Stop)
'************************************************************************************************
End Sub

Private Sub moDmDetl_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'   Description:
'      Fires when each row of data is loaded by the DM into the grid.
'      This is a manual movement of a copy of the data from the DM to the correct
'      grid cell on the same row.
'************************************************************************************
Dim DBValue As Integer

'-- Perform processing as each row is loaded into the grid
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridRowLoaded", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMAfterDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False
    
'-- Perform processing after a record is deleted from the main table here

    
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMAfterDelete", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMBeforeInsert(bValid As Boolean)
    Dim lKey As Long
    On Error GoTo CancelInsert
    bValid = True

    With moClass.moAppDB
        .SetInParam "timCOATestResults_SGS"
        .SetOutParam lKey
        .ExecuteSP ("spGetNextSurrogateKey")
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With
    
    moDmHeader.SetColumnValue "COATestRsltsKey", lKey
    moDmHeader.SetColumnValue "CompanyID", msCompanyID
    moDmHeader.SetColumnValue "CreateUserID", msUserID
    moDmHeader.SetColumnValue "UpdateUserID", msUserID
    moDmHeader.SetColumnValue "CreateDate", Date + Time
    moDmHeader.SetColumnValue "UpdateDate", Date + Time
    
    Exit Sub
CancelInsert:
    bValid = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeInsert", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMBeforeUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False
    
'-- Perform processing before a record is updated in the main table here
    moDmHeader.SetColumnValue "UpdateUserID", msUserID
    moDmHeader.SetColumnValue "UpdateDate", Date + Time

    
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeUpdate", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMAfterUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False

'-- Perform processing after a record is updated in the main table here

    
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMAfterUpdate", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMBeforeDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False

'-- Perform processing before a record is deleted from the main table here


    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeDelete", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If oChild Is moDmHeader Then

'-- Perform processing as a record is loaded onto the form after data is displayed here


    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMDataDisplayed", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMPostSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = False

'-- Perform processing after a record is saved to the main table here

    
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMPostSave", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMPreSave(bValid As Boolean)
    bValid = IsValidData()
End Sub

Private Sub moDmHeader_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If oChild Is moDmHeader Then

'-- Perform processing as a record is loaded onto the form before data is displayed here
        
        If glGetValidLong(moDmHeader.GetColumnValue("IsActive")) <> 0 Then
            'is active = true
            gGridLockColumn grdMain, kColExtTestResults
            
            If miSecurityLevel = SecurityLevels.kSecLevelSupervisory Then
                pnlTab(kiHeaderTab).Enabled = True
            Else
                'Once active don't allow non supervisors to edit any data
'                pnlTab(kiHeaderTab).Enabled = False
                
            End If
        Else
            'Is Active = False
            If miSecurityLevel = SecurityLevels.kSecLevelSupervisory Then
                gGridUnlockColumn grdMain, kColExtTestResults
            End If
        End If
            
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMReposition", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '
    If iNewState = kDmStateNone Then
'        If Not moLE Is Nothing Then
'            If moLE.State <> kGridNone Then
'                On Error Resume Next
'                moLE.InitDataReset
'
'            End If
'        End If
        lblItemDesc.Caption = Empty
        
    ElseIf iNewState = kDmStateAdd Then
        SSTab1.Tab = kiHeaderTab
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMStateChange", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmHeader_DMBeforeTransaction(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************************
' Description:
'    This routine will be called by Data Manager before the record
'    is saved. This is where form-level validation should occur.
'*******************************************************************
    bValid = False

'-- Perform validations and other processes that need to occur before the DB transaction here

        
    bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeTransaction", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuChildCOATestRsltsKey_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bCancel = Not bConfirmUnload(0, True)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuChildCOATestRsltsKey_LookupClick", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub moGMCOATests_CellChange(ByVal lRow As Long, ByVal lCol As Long)
    '
End Sub

Private Sub OptnRptFtrAlign_Click(Index As Integer)
    On Error GoTo Error
    
    txtPgFtrAlign.Value = Index
    
'    Select Case True
'        Case OptnRptFtrAlign(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("COATemplatePgFtrLeftRightCenter") = mlAlgnmntLeft
'        Case OptnRptFtrAlign(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("COATemplatePgFtrLeftRightCenter") = mlAlgnmntCenter
'        Case OptnRptFtrAlign(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("COATemplatePgFtrLeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnRptFtrAlign_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub OptnUserAlign01_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld01).Value = Index
    
'    Select Case True
'        Case OptnUserAlign01(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField01LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign01(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField01LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign01(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField01LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign01_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub OptnUserAlign02_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld02).Value = Index
    
'    Select Case True
'        Case OptnUserAlign02(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField02LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign02(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField02LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign02(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField02LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign02_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub OptnUserAlign03_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld03).Value = Index
    
'    Select Case True
'        Case OptnUserAlign03(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField03LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign03(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField03LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign03(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField03LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign03_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub OptnUserAlign04_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld04).Value = Index
    
'    Select Case True
'        Case OptnUserAlign04(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField04LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign04(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField04LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign04(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField04LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign04_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub

Private Sub OptnUserAlign05_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld05).Value = Index
    
'    Select Case True
'        Case OptnUserAlign05(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField05LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign05(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField05LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign05(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField05LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign05_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub OptnUserAlign06_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld06).Value = Index
    
'    Select Case True
'        Case OptnUserAlign06(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField06LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign06(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField06LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign06(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField06LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign06_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub OptnUserAlign07_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld07).Value = Index
    
'    Select Case True
'        Case OptnUserAlign07(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField07LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign07(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField07LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign07(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField07LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign07_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub OptnUserAlign08_Click(Index As Integer)
    On Error GoTo Error
    
    txtUserFldAlign(mlUserFld08).Value = Index
    
'    Select Case True
'        Case OptnUserAlign08(mlAlgnmntLeft).Value
'            moDmHeader.SetColumnValue("UserField08LeftRightCenter") = mlAlgnmntLeft
'        Case OptnUserAlign08(mlAlgnmntCenter).Value
'            moDmHeader.SetColumnValue("UserField08LeftRightCenter") = mlAlgnmntCenter
'        Case OptnUserAlign08(mlAlgnmntRight).Value
'            moDmHeader.SetColumnValue("UserField08LeftRightCenter") = mlAlgnmntRight
'    End Select
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OptnUserAlign08_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick sButton
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub SSTab1_Click(PreviousTab As Integer)
    On Error GoTo Error
    
    If SSTab1.Tab = 1 Then
        'Get all the tests
        If GetCOATestResultsTests() = False Then
            SSTab1.Tab = PreviousTab
        End If
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SSTab1_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub


Private Sub txtLotNumber_LostFocus()
    CheckKeyComplete
End Sub

Private Sub txtLotNumber_Validate(Cancel As Boolean)
    
    If chActive.Value = True Then
        If Trim(txtLotNumber.Text) = Empty Then
            MsgBox "The Lot Number is required for activation.  The Is Active flag will be turned off.", vbExclamation, "MAS 500"
            chActive.Value = False
            Exit Sub
        End If
    End If

End Sub

Private Sub txtPgFtrAlign_Change()
    On Error GoTo Error
    
    If txtPgFtrAlign.Value >= 1 And txtPgFtrAlign.Value <= 3 Then
        OptnRptFtrAlign(txtPgFtrAlign.Value).Value = True
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".txtPgFtrAlign_Change()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Private Sub txtTank_LostFocus()
    CheckKeyComplete
End Sub

Private Sub txtUserFldAlign_Change(Index As Integer)
    On Error GoTo Error
    
    If txtUserFldAlign(Index).Value >= 1 And txtUserFldAlign(Index).Value <= 3 Then
    Select Case Index
        Case mlUserFld01
            OptnUserAlign01(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld02
            OptnUserAlign02(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld03
            OptnUserAlign03(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld04
            OptnUserAlign04(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld05
            OptnUserAlign05(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld06
            OptnUserAlign06(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld07
            OptnUserAlign07(txtUserFldAlign(Index).Value).Value = True
        Case mlUserFld08
            OptnUserAlign08(txtUserFldAlign(Index).Value).Value = True
    End Select
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".txtUserFldAlign_Change()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub


Sub valMgr_KeyChange()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iKeyChangeCode  As Integer
    Dim COAReportKey As Long
    Dim COAReportID As String

    moDmHeader.SetColumnValue "CompanyID", msCompanyID
    
    If lkuItem.Text = Empty Then Exit Sub
    If lkuWhse.Text = Empty Then Exit Sub
    If txtTank.Text = Empty Then Exit Sub
    If txtLotNumber.Text = Empty Then Exit Sub
    
    
    If lkuItem.IsValid = False Then Exit Sub
    If lkuWhse.IsValid = False Then Exit Sub
    
    iKeyChangeCode = moDmHeader.KeyChange

    Select Case iKeyChangeCode
        Case kDmKeyFound, kDmKeyNotFound
            lkuWhse.EnabledLookup = False
            lkuItem.EnabledLookup = False
            
            If miSecurityLevel = SecurityLevels.kSecLevelSupervisory Then
'                If Not Me.ActiveControl Is chActive And chActive.Enabled = True Then
'                    chActive.SetFocus
'                End If
            End If
            
            If iKeyChangeCode = kDmKeyNotFound Then
                'Get Default Report/Template
                COAReportKey = glGetValidLong(moClass.moAppDB.Lookup("COAReportKey", "timItemExt_SGS", "ItemKey = " & glGetValidLong(lkuItem.KeyValue)))
                If COAReportKey > 0 Then
                    COAReportID = gsGetValidStr(moClass.moAppDB.Lookup("COATemplateReportID", "timCOATemplateReports_SGS", "COATemplateReportKey = " & COAReportKey))
                    
                    If Trim(COAReportID) <> Empty Then
                        ddnCOARpts.Text = COAReportID
                    End If
                End If
            End If

        Case kDmKeyNotComplete
            MsgBox "valMgr_KeyChange - key  not complete"
            
'            FrameKey.Enabled = True
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "valMgr_KeyChange", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub valMgr_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'mbFromCode = True
    
    Dim i As Long
    Dim TestResult As String
    
    'Default to Not valid
    iReturn = SOTA_INVALID
    
    Select Case oControl.Name
        Case chActive.Name
            'Check that all tests have a result
            If chActive.Value = True Then
                If Trim(txtLotNumber.Text) = Empty Then
                    sMessage = "The Lot Number is required before activation."
'                    MsgBox "The Lot Number is required before activation.", vbExclamation, "MAS 500"
                    chActive.Value = False
                    Exit Sub
                End If
                
                If IsDate(calCmptDate.Value) = False Then
                    sMessage = "The Complete Date is required before activation."
'                    MsgBox "The Complete Date is required before activation.", vbExclamation, "MAS 500"
                    chActive.Value = False
                    Exit Sub
                End If
                
                For i = 1 To grdMain.MaxRows
                    TestResult = Trim(gsGetValidStr(moDmDetl.GetColumnValue(i, "TestResults")))
                    
                    If TestResult = Empty Then
                        chActive.Value = False
                        
                        sMessage = "Not all test results have been entered.  You have to enter all test results before activating."
'                        MsgBox "Not all test results have been entered.  You have to enter all test results before activating.", vbExclamation, "MAS 500"
                        
                        SSTab1.Tab = kiDetailTab
                        
                        Exit Sub
                    End If
                    
                    TestResult = Empty
                    
                Next
            End If
        Case Else
            'UnValidated Obj
            
    End Select
    
    iReturn = SOTA_VALID
    
'-- Call individual validation routines for controls here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "valMgr_Validate", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ClearDetlFields()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'-- Clear out controls that are not bound to Line Entry here



    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ClearDetlFields", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub HandleTabClick()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    
    '-- Disable all panels
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleTabClick", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub HandleSubTabClick()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    
    '-- Disable all panels
'    '-- Enable the visible panel
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleSubTabClick", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmHeader
                moFormCust.ApplyFormCust
        End If
    End If
#End If

    Dim i As Integer

    '-- Setup the form (validation) manager control
    With valMgr
        Set .Framework = moClass.moFramework
        
'        .Keys.Add txtLotNumber
        .Keys.Add txtTank
        .Keys.Add lkuWhse
        .Keys.Add lkuItem
        
        .Keys.Add LookupView1
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Public Sub grdMain_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMCOATests.Grid_Click Col, Row
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_Click", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_DblClick(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_DblClick", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_DragDrop", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_DragOver(Source As Control, x As Single, y As Single, State As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_DragOver", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_GotFocus", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub grdMain_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMCOATests.Grid_KeyDown KeyCode, Shift
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_KeyDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_LostFocus", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdMain_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdMain_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmDetl_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

 bValid = False
 Dim lKey As Long
 
' With moClass.moAppDB
'    .SetInParamStr "timCOATestResultsTests_SGS"
'    .SetOutParam lKey
'    .ExecuteSP "spGetNextSurrogateKey"
'    lKey = .GetOutParam(2)
'    .ReleaseParams
' End With
' moDmDetl.SetColumnValue lRow, "CompetitorDetlKey", lKey
 moDmDetl.SetColumnValue lRow, "COATestRsltsKey", moDmHeader.GetColumnValue("COATestRsltsKey")
 bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridBeforeInsert", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    ETWhereClause = ""
    
    ' Specific checks go here
    
    Err.Clear
    
End Function



Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Function GetCOATestResultsTests(Optional bForce As Boolean = False, Optional bInSave As Boolean = False) As Boolean
    On Error GoTo Error
    
    Dim COATestRsltsKey As Long
    Dim ItemKey As Long
    Dim lcSaveRslts As Integer
    
    If moDmHeader.State <> kDmStateAdd And bForce <> True Then
        'Only get tests if this is a new record.
        GetCOATestResultsTests = True
        Exit Function
    End If
    
    If moDmHeader.IsDirty = True And bInSave = False Then
        If MsgBox("You need to save before continuing.  Do you want to save?", vbYesNo, "MAS 500") <> vbYes Then
            GetCOATestResultsTests = False
            Exit Function
        Else
            If Not valMgr.ValidateForm Then
                GetCOATestResultsTests = False
                Exit Function
            End If
            
            If IsCOAValid = False Then
                Exit Function
            End If
            
            lcSaveRslts = moDmHeader.Save(True)
            
            If lcSaveRslts <> kDmSuccess Then
                GetCOATestResultsTests = False
                Exit Function
            End If
        
            If lcSaveRslts = DMActionResults.kDmSuccess Then
                'If this Lot record is active then update all other lot records to in active.
                COATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
                Call UpdateLotActive(COATestRsltsKey)
            End If
        End If
    End If
    
    COATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
    ItemKey = glGetValidLong(moDmHeader.GetColumnValue("ItemKey"))
    
    
    'spIMGetCOATestResultsTests_SGS
    With moClass.moAppDB
        .SetInParam COATestRsltsKey
        .SetInParam ItemKey
        .ExecuteSP ("spIMGetCOATestResultsTests_SGS")
        
        .ReleaseParams
    End With
    
    moDmDetl.Refresh
    
    GetCOATestResultsTests = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".GetCOATestResultsTests()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Function


Private Function bSetupLookupView1() As Boolean
    On Error GoTo Error
    
    Dim lcFilter As String
    
    lcFilter = msLookupRestrict & " And IsNull(IsHistoryRecord,0) = 0 "
'    "CompanyID = " & gsQuoted(msCompanyID) & " And IsNull(IsHistoryRecord,0) = 0 "
    
    bSetupLookupView1 = False
    
    If Len(Trim(LookupView1.Tag)) = 0 Then
        bSetupLookupView1 = gbLookupInit(LookupView1, moClass, moClass.moAppDB, "COATestResults", lcFilter)
        
        If bSetupLookupView1 Then
            LookupView1.Tag = 1
        End If
    Else
        bSetupLookupView1 = True
    End If
    
    Exit Function
Error:
    MsgBox Me.Name & ".bSetupLookupView1()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
        
End Function

Sub CheckKeyComplete()
    On Error GoTo Error
    
    Dim COATestRsltsKey As Long
    Dim bValid As Boolean
    
    If Trim(txtLotNumber.Text) = Empty Then Exit Sub
    If Trim(lkuWhse.Text) = Empty Then Exit Sub
    If Trim(lkuItem.Text) = Empty Then Exit Sub
    If Trim(txtTank.Text) = Empty Then Exit Sub
    
    If IsValidTank(txtTank.Text, glGetValidLong(lkuWhse.KeyValue)) = False Then
        MsgBox "The tank you specified is not vail with this warehouse.", vbExclamation, "Invalide Tank"
        Exit Sub
    End If
    
    COATestRsltsKey = glGetValidLong(moClass.moAppDB.Lookup("COATestRsltsKey", "vluIMCOATestResultsHdr_SGS", "IsNull(IsHistoryRecord,0) = 0 And Tank = " & gsQuoted(Trim(txtTank.Text)) & " And ItemID = " & gsQuoted(lkuItem.Text) & " And WhseID = " & gsQuoted(lkuWhse.Text) & " And LotNumber = " & gsQuoted(Trim(txtLotNumber.Text))))
    
    If COATestRsltsKey > 0 Then
        moDmHeader.SetColumnValue "COATestRsltsKey", COATestRsltsKey
    Else
        
        moDmHeader_DMBeforeInsert bValid
        If bValid <> True Then
            'Could not get key
            MsgBox "Cannot get new table key for this new record.", vbExclamation, "MAS 500"
            Exit Sub
        End If
        
    End If
    
    valMgr_KeyChange
    
    Exit Sub
Error:
    MsgBox Me.Name & ".CheckKeyComplete()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub

Function IsCOAValid() As Boolean
    On Error GoTo Error
    Dim i As Long
    Dim TestResult As String
    
    'Default to Not valid
    IsCOAValid = False
    
    'Check that all tests have a result
    If chActive.Value = True Then
        
        'Make sure all lines/tests have a test result
        For i = 1 To grdMain.MaxRows
            TestResult = Trim(gsGetValidStr(moDmDetl.GetColumnValue(i, "TestResults")))
            
            If TestResult = Empty Then
                
                MsgBox "Not all test results have been entered.  You have to enter all test results before activating.", vbExclamation, "MAS 500"
                
                SSTab1.Tab = kiDetailTab
                
                Exit Function
            End If
            
            TestResult = Empty
            
        Next
        
        'Check for a Report Template
        If Trim(ddnCOARpts.Text) = Empty Then
            MsgBox "The COA Template is required.", vbExclamation, "MAS 500"
            If Not Me.ActiveControl Is ddnCOARpts Then
                SSTab1.Tab = kiHeaderTab
                ddnCOARpts.SetFocus
            End If
            Exit Function
        End If
    End If
    
    IsCOAValid = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".chActive_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear

End Function

Sub UpdateLotActive(lcCOATestRsltsKey As Long)
    On Error GoTo Error
    Dim lSuccess As Long
    Dim lErrorNo As Long
    Dim lErrorMsg As String

    With moClass.moAppDB
        .SetInParam lcCOATestRsltsKey
        .SetOutParam lErrorNo
        .SetOutParam lErrorMsg
        .SetOutParam lSuccess
        .ExecuteSP ("spIMUpdateLotActive_SGS")
        lErrorNo = .GetOutParam(2)
        lErrorMsg = .GetOutParam(3)
        lSuccess = .GetOutParam(4)
        .ReleaseParams
    
    End With
    
    Select Case lSuccess
        Case 1  'Success
        Case Else 'unknown
            MsgBox "Unable to update the Active status to the Lots.  The Proc 'spIMUpdateLotActive_SGS' spawned the following error:" & vbCrLf & _
            "Error: " & lErrorNo & " " & lErrorMsg, vbExclamation, "MAS 500"
            
            Err.Clear
    End Select
    
    
    Exit Sub
Error:
    MsgBox Me.Name & ".UpdateLotActive()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
    moClass.moAppDB.ReleaseParams
End Sub

Function ClearAll(lcCOATestRsltsKey As Long) As Boolean
    On Error GoTo Error
    Dim lSuccess As Long
    Dim lErrorNo As Long
    Dim lErrorMsg As String
    
    'Default to Fail
    ClearAll = False
    
    With moClass.moAppDB
        .SetInParam lcCOATestRsltsKey
        .SetOutParam lErrorNo
        .SetOutParam lErrorMsg
        .SetOutParam lSuccess
        .ExecuteSP ("spIMCpyCOALotDataToHstry_SGS")
        lErrorNo = .GetOutParam(2)
        lErrorMsg = .GetOutParam(3)
        lSuccess = .GetOutParam(4)
        .ReleaseParams
    
    End With
    
    Select Case lSuccess
        Case 1  'Success
            ClearAll = True
            
        Case Else 'unknown
            MsgBox "Unable to Clear All.  The Proc 'spIMCpyCOALotDataToHstry_SGS' spawned the following error:" & vbCrLf & _
            "Error: " & lErrorNo & " " & lErrorMsg, vbExclamation, "MAS 500"
            
            Err.Clear
    End Select
    
        
    Exit Function
Error:
    MsgBox Me.Name & ".ClearAll()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
    moClass.moAppDB.ReleaseParams
End Function


Function IsValidData() As Boolean
    On Error GoTo Error
    
    Dim i As Long
    Dim TestResult As String
    
    'Default to Not valid
    IsValidData = False
    
    'Check that all tests have a result
    If chActive.Value = True Then
        If Trim(txtLotNumber.Text) = Empty Then
            MsgBox "The Lot Number is required before activation.", vbExclamation, "MAS 500"
            chActive.Value = False
            Exit Function
        End If
        
        If IsDate(calCmptDate.Value) = False Then
            MsgBox "The Complete Date is required before activation.", vbExclamation, "MAS 500"
            chActive.Value = False
            Exit Function
        End If
        
        For i = 1 To grdMain.MaxRows
            TestResult = Trim(gsGetValidStr(moDmDetl.GetColumnValue(i, "TestResults")))
            
            If TestResult = Empty Then
                chActive.Value = False
                
                MsgBox "Not all test results have been entered.  You have to enter all test results before activating.", vbExclamation, "MAS 500"
                
                SSTab1.Tab = kiDetailTab
                
                Exit Function
            End If
            
            TestResult = Empty
            
        Next
        
        'EB DEJ 12/5/12 - Kevin wanted an automated increase of the tons on save.
        txtProdQty.Value = txtProdQty.Value + 10
    End If
    
    IsValidData = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".IsValidData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Function


Function IsValidTank(lsTank As String, lcWhseKey As Long) As Boolean
    On Error GoTo Error
    
    'Default to not valid
    IsValidTank = False
    
    Dim WhseBinKey As Long
    
    
    WhseBinKey = glGetValidLong(moClass.moAppDB.Lookup("WhseBinKey", "timWhseBin", "WhseKey = " & glGetValidLong(lcWhseKey) & " And WhseBinID = " & gsQuoted(lsTank)))
    
    If WhseBinKey > 0 Then
        IsValidTank = True
    Else
        IsValidTank = False
    End If
    
    Exit Function
Error:
    MsgBox Me.Name & ".IsValidTank()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Function

'*********************************************************************************
'*********************************************************************************
'RKL DEJ 9/25/13 (START)
'*********************************************************************************
'*********************************************************************************
Function CopyCOARecord(ByRef ItemKey As Long, ByRef ItemID As String, ByRef NewCOATestRsltsKey As Long) As Boolean
    On Error GoTo Error
    
    Dim COATestRsltsKey As Long
    Dim lcSaveRslts As Integer
    Dim lResults As Long
    

    If moDmHeader.IsDirty = True Then
        If MsgBox("You need to save before continuing.  Do you want to save?", vbYesNo, "MAS 500") <> vbYes Then
            CopyCOARecord = False
            Exit Function
        Else
            If Not valMgr.ValidateForm Then
                CopyCOARecord = False
                Exit Function
            End If

            If IsCOAValid = False Then
                Exit Function
            End If

            lcSaveRslts = moDmHeader.Save(True)

            If lcSaveRslts <> kDmSuccess Then
                CopyCOARecord = False
                Exit Function
            End If

            If lcSaveRslts = DMActionResults.kDmSuccess Then
                'If this Lot record is active then update all other lot records to inactive.
                COATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
                Call UpdateLotActive(COATestRsltsKey)
            End If
        End If
    End If

    COATestRsltsKey = glGetValidLong(moDmHeader.GetColumnValue("COATestRsltsKey"))
    
    lkuNewItem.ClearData
    
    lkuNewItem.DoLookupClick
    
    ItemKey = glGetValidLong(lkuNewItem.KeyValue)
    
    If ItemKey <= 0 Then
'        Debug.Print lkuNewItem.LookupID
'        Debug.Print lkuNewItem.Text
        CopyCOARecord = False
        Exit Function
    End If
    
    ItemID = gsGetValidStr(lkuNewItem.ReturnColumnValues("ItemID"))
    If UCase(Trim(ItemID)) = UCase(Trim(lkuItem.Text)) Then
        MsgBox "The COA Item you selected is the same as the current record.", vbInformation, "Same Item Record"
        CopyCOARecord = False
        Exit Function
    End If


    With moClass.moAppDB
        .SetInParam COATestRsltsKey
        .SetInParam ItemKey             'This is the new item to save to
        .SetOutParam NewCOATestRsltsKey
        .SetOutParam lResults
        
        .ExecuteSP ("spIMCopyCOATestResults_SGS")

        NewCOATestRsltsKey = glGetValidLong(.GetOutParam(3))
        lResults = glGetValidLong(.GetOutParam(4))
        
        .ReleaseParams
    End With

'@oResult Values
'0 = Unknown System Error
'1 = Success
'2 = Bad Item
'3 = Same Item and Record
'4 = Record Already Exists
    
    Select Case lResults
        Case 0 'Unknown System Error
            MsgBox "Unknown System Error.  The record was not copied.", vbInformation, "Error Occurred"
            CopyCOARecord = False
        Case 1 'Success
            CopyCOARecord = True
        Case 2 'Bad Item
            MsgBox "The Item you selected is not valid.", vbInformation, "Bad Item"
            CopyCOARecord = False
        Case 3 'Same Item and Record
            MsgBox "You selected the same item as the current record.  No action was taken.", vbInformation, "Same Item"
            CopyCOARecord = False
        Case 4 'Record Already Exists
            MsgBox "There is already a record with that item.", vbInformation, "Record Exists"
            CopyCOARecord = False
        Case Else 'Unknown System Error
            MsgBox "Unknown System Error/Results.  The record was not copied.", vbInformation, "Error Occurred"
            CopyCOARecord = False
    End Select
    
    
    Exit Function
Error:
    MsgBox Me.Name & ".CopyCOARecord()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Function

'*********************************************************************************
'*********************************************************************************
'RKL DEJ 9/25/13 (STOP)
'*********************************************************************************
'*********************************************************************************


