VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmCOAPrinting 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "COA Out Of Process Printing"
   ClientHeight    =   7140
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11475
   HelpContextID   =   17770001
   Icon            =   "frmCOAPrinting.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   11475
   Begin NEWSOTALib.SOTANumber txtNbrOfCopies 
      Height          =   255
      Left            =   1800
      TabIndex        =   46
      ToolTipText     =   "Number of Copies to Print"
      Top             =   5400
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
      text            =   "           1"
      dMinValue       =   1
      dMaxValue       =   100
      sDecimalPlaces  =   0
   End
   Begin VB.ComboBox cmboPrinter 
      Height          =   315
      Left            =   120
      Style           =   2  'Dropdown List
      TabIndex        =   44
      Top             =   4920
      Width           =   3015
   End
   Begin VB.Frame FrameKey 
      Caption         =   "COA Test Data"
      Height          =   3975
      Left            =   120
      TabIndex        =   60
      Top             =   600
      Width           =   3855
      Begin NEWSOTALib.SOTAMaskedEdit txtTank 
         Height          =   255
         Left            =   1320
         TabIndex        =   5
         Top             =   960
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         lMaxLength      =   75
      End
      Begin EntryLookupControls.TextLookup lkuItem 
         Height          =   285
         Left            =   1320
         TabIndex        =   9
         Top             =   1800
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         ForeColor       =   -2147483640
         EnabledLookup   =   0   'False
         EnabledText     =   0   'False
         Protected       =   -1  'True
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Item"
         ParentIDColumn  =   "ItemID"
         ParentKeyColumn =   "ItemKey"
         ParentTable     =   "timItem"
         BoundColumn     =   "ItemKey"
         BoundTable      =   "timSGSDEJTestOnly_SGS"
         IsForeignKey    =   -1  'True
         Datatype        =   0
         sSQLReturnCols  =   "ItemID,lkuitem,;ShortDesc,lblItemDesc,;ItemKey,,;"
      End
      Begin EntryLookupControls.TextLookup lkuWhse 
         Height          =   285
         Left            =   1320
         TabIndex        =   7
         Top             =   1380
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   503
         ForeColor       =   -2147483640
         EnabledLookup   =   0   'False
         EnabledText     =   0   'False
         Protected       =   -1  'True
         IsSurrogateKey  =   -1  'True
         LookupID        =   "WarehouseAndKey"
         ParentIDColumn  =   "WhseID"
         ParentKeyColumn =   "WhseKey"
         ParentTable     =   "timWarehouse"
         BoundColumn     =   "WhseKey"
         BoundTable      =   "timSGSDEJTestOnly_SGS"
         IsForeignKey    =   -1  'True
         Datatype        =   0
         sSQLReturnCols  =   "WhseID,lkuWhse,;Description,,;WhseKey,,;"
      End
      Begin LookupViewControl.LookupView lkuCOA 
         Height          =   285
         Left            =   2640
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Lookup Existing COA Test Record(s)"
         Top             =   240
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
         LookupID        =   "COATestResults"
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtLotNumber 
         Height          =   255
         Left            =   1320
         TabIndex        =   3
         Top             =   600
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         lMaxLength      =   75
      End
      Begin Threed.SSCheck chIsActive 
         Height          =   255
         Left            =   1320
         TabIndex        =   11
         Top             =   2520
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "Is Active"
         ForeColor       =   -2147483632
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin SOTACalendarControl.SOTACalendar calCmptDate 
         Height          =   255
         Left            =   1320
         TabIndex        =   14
         Top             =   2880
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   450
         BackColor       =   -2147483633
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Protected       =   -1  'True
         Text            =   "  /  /    "
      End
      Begin Threed.SSCheck chIsHistory 
         Height          =   255
         Left            =   2400
         TabIndex        =   12
         Top             =   2520
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   450
         _StockProps     =   78
         Caption         =   "Is History"
         ForeColor       =   -2147483632
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin SOTADropDownControl.SOTADropDown ddnCOARpts 
         Height          =   315
         Left            =   120
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   3480
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Style           =   2
         Text            =   "ddnCOARpts"
      End
      Begin VB.Label lblCOATemplate 
         Caption         =   "COA Template:"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label lblTank 
         Caption         =   "&Tank"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label lblWhse 
         AutoSize        =   -1  'True
         Caption         =   "&Whse"
         Height          =   195
         Left            =   120
         TabIndex        =   6
         Top             =   1380
         Width           =   1095
      End
      Begin VB.Label lblItemID 
         AutoSize        =   -1  'True
         Caption         =   "Ite&m"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label lblLookupCOA 
         Caption         =   "Lookup COA Test Results Record:"
         Height          =   255
         Left            =   120
         TabIndex        =   0
         ToolTipText     =   "Lookup Existing COA Test Record(s)"
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label lblItemDesc 
         Caption         =   "Item Desc goes here"
         Height          =   255
         Left            =   1320
         TabIndex        =   10
         Top             =   2160
         Width           =   2055
      End
      Begin VB.Label lblLotNumber 
         Caption         =   "&Lot Number"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   1020
      End
      Begin VB.Label lblCmptDate 
         Caption         =   "Complete Date"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   2880
         Width           =   1455
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Contract Data"
      Height          =   6015
      Left            =   4200
      TabIndex        =   59
      Top             =   600
      Width           =   7095
      Begin NEWSOTALib.SOTAMaskedEdit txtTranID 
         Height          =   255
         Left            =   1920
         TabIndex        =   62
         Top             =   840
         Width           =   2175
         _Version        =   65536
         _ExtentX        =   3836
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
      End
      Begin LookupViewControl.LookupView lkuSO 
         Height          =   285
         Left            =   1320
         TabIndex        =   61
         TabStop         =   0   'False
         Top             =   720
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
         LookupID        =   "SalesOrderWithCustomer"
      End
      Begin VB.CheckBox chLookData 
         Caption         =   "Use Data From MAS BSO, SO, or Quote"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Value           =   1  'Checked
         Width           =   3255
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtCustName 
         Height          =   300
         Left            =   1920
         TabIndex        =   17
         Top             =   1560
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   40
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtBOLNo 
         Height          =   300
         Left            =   1920
         TabIndex        =   30
         Top             =   3720
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   20
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtContractNbr 
         Height          =   300
         Left            =   1920
         TabIndex        =   22
         Top             =   2280
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   500
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtProjNbr 
         Height          =   300
         Left            =   1920
         TabIndex        =   32
         Top             =   4080
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   60
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtKeyNbr 
         Height          =   300
         Left            =   1920
         TabIndex        =   34
         Top             =   4440
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   50
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtProjDesc 
         Height          =   300
         Left            =   1920
         TabIndex        =   36
         Top             =   4800
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   255
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtJobState 
         Height          =   300
         Left            =   1920
         TabIndex        =   38
         Top             =   5160
         Width           =   750
         _Version        =   65536
         _ExtentX        =   1323
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   2
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtIASContrctNbr 
         Height          =   300
         Left            =   1920
         TabIndex        =   20
         Top             =   1920
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   10
      End
      Begin NEWSOTALib.SOTANumber txtBidQty 
         Height          =   300
         Left            =   1920
         TabIndex        =   24
         Top             =   2640
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
         text            =   "           0.000"
         sDecimalPlaces  =   3
      End
      Begin NEWSOTALib.SOTANumber txtProdQty 
         Height          =   300
         Left            =   1920
         TabIndex        =   26
         Top             =   3000
         Width           =   2055
         _Version        =   65536
         _ExtentX        =   3625
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
         text            =   "           0.000"
         sDecimalPlaces  =   3
      End
      Begin SOTACalendarControl.SOTACalendar calDtOfShiping 
         Height          =   300
         Left            =   1920
         TabIndex        =   28
         Top             =   3360
         Width           =   2055
         _ExtentX        =   3625
         _ExtentY        =   529
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtTranNoRel 
         Height          =   255
         Left            =   4200
         TabIndex        =   64
         Top             =   840
         Width           =   2175
         _Version        =   65536
         _ExtentX        =   3836
         _ExtentY        =   450
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtDelLoc 
         Height          =   300
         Left            =   1920
         TabIndex        =   40
         Top             =   5520
         Width           =   5055
         _Version        =   65536
         _ExtentX        =   8916
         _ExtentY        =   529
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   50
      End
      Begin VB.Label lblDeliveryLoc 
         Caption         =   "Delivery Location"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   5520
         Width           =   1260
      End
      Begin VB.Label Label1 
         Caption         =   "Tran No Rel"
         Height          =   255
         Left            =   4200
         TabIndex        =   65
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label lblTranID 
         Caption         =   "Tran ID"
         Height          =   255
         Left            =   1920
         TabIndex        =   63
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label2 
         Caption         =   "Lookup Data"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Cust Name"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1560
         Width           =   1020
      End
      Begin VB.Label Label4 
         Caption         =   "Bill of Lading #"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   3720
         Width           =   1140
      End
      Begin VB.Label Label5 
         Caption         =   "Contract #"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   2280
         Width           =   1020
      End
      Begin VB.Label Label6 
         Caption         =   "Bid Qty"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   2640
         Width           =   1020
      End
      Begin VB.Label Label7 
         Caption         =   "Production Qty"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   3000
         Width           =   1140
      End
      Begin VB.Label Label8 
         Caption         =   "Date Of Shipping"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   3360
         Width           =   1260
      End
      Begin VB.Label Label9 
         Caption         =   "Project Number"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   4080
         Width           =   1260
      End
      Begin VB.Label Label10 
         Caption         =   "Key #"
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   4440
         Width           =   1020
      End
      Begin VB.Label Label11 
         Caption         =   "Project Description"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   4800
         Width           =   1380
      End
      Begin VB.Label Label12 
         Caption         =   "State"
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   5160
         Width           =   1020
      End
      Begin VB.Label Label13 
         Caption         =   "IAS Contract #"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1920
         Width           =   1140
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   52
      Top             =   4320
      Visible         =   0   'False
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   51
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -10000
      Style           =   2  'Dropdown List
      TabIndex        =   47
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      Width           =   1245
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   53
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   55
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   56
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   57
      Top             =   645
      Visible         =   0   'False
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6750
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   688
      BrowseVisible   =   0   'False
      MessageVisible  =   -1  'True
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   54
      TabStop         =   0   'False
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   741
      Style           =   3
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.Label lblPrinter 
      Caption         =   "Select Printer"
      Height          =   375
      Left            =   120
      TabIndex        =   43
      Top             =   4680
      Width           =   3015
   End
   Begin VB.Label lblNbrCopies 
      Caption         =   "Number of Copies:"
      Height          =   255
      Left            =   120
      TabIndex        =   45
      Top             =   5400
      Width           =   1455
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   58
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmCOAPrinting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Private miSecurityLevel     As Integer

Public moSotaObjects        As New Collection

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

Private moContextMenu       As New clsContextMenu

Private miOldFormHeight     As Long

Private miOldFormWidth      As Long

Private miMinFormHeight     As Long

Private miMinFormWidth      As Long

Private mbLoadSuccess       As Boolean

Public mlLanguage As Long

Private msLookupRestrict As String

Private mlCOATestRsltsKey As Long
Private mlSOKey As Long
Private mlTranType As Long
Private mlPrintLogKey As Long

Private ReportObj As clsReportEngine
Private ReportName As String


Private Sub calDtOfShiping_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    If calDtOfShiping.Tag <> calDtOfShiping.Text Then
        calDtOfShiping.Tag = calDtOfShiping.Text
        mlPrintLogKey = 0
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "calDtOfShiping_Validate", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chLookData_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If chLookData.Value = vbUnchecked Then
        lkuSO.Enabled = False
    Else
        lkuSO.Enabled = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chLookData_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnCOARpts_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnCOARpts_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    mbLoadSuccess = False

    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab

    BindToolbar
    Set sbrMain.FrameWork = moClass.moFramework

    msLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)
    
    
    SetupLookups
    
    SetupDropDowns
    
    SetupPrinters
    
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain)

    ' set form size variables
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iConfirmUnload  As Integer


   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
    
        'Check all other forms  that may have been loaded from this main form.
        'If there are any visible forms, then this means the form is active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
           'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is Normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                        
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
                
        End Select
        
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
            'Do nothing
            
    End Select
    
Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    'Unload all forms loaded from this main form
    gUnloadChildForms Me

    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me

    ' perform proper cleanup
    Set moContextMenu = Nothing         ' context menu class
    Set moSotaObjects = Nothing         ' SOTA Child objects collection

    If Not ReportObj Is Nothing Then
        If Not ReportObj.bShutdownEngine Then
            'MsgBox "Cancel Shutdown"
        End If
    
        Set ReportObj = Nothing
    End If

Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set moClass = Nothing
     
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    PrintCOA sKey
    
'    Select Case sKey
'
'      'Print The COA
'        Case kTbPrint
'            PrintCOA sKey
'
'      'Preview the COA
'        Case kTbPreview
'            PrintCOA sKey
'
'        Case Else
'
'    End Select
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuSO_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lcTranNo As String
    Dim lcTranNoRelChngOrd As String
    Dim lcTranNoRel As String
    
    Dim sWhere As String
    
    bSetuplkuSO
        
    gcLookupClick Me, lkuSO, txtTranNoRel, "TranNoRel"
    
    If lkuSO.ReturnColumnValues.Count > 0 Then
        mlPrintLogKey = 0
        
        lcTranNo = gsGetValidStr(lkuSO.ReturnColumnValues("TranNo"))
        lcTranNoRelChngOrd = gsGetValidStr(lkuSO.ReturnColumnValues("TranNoRelChngOrd"))
        lcTranNoRel = gsGetValidStr(lkuSO.ReturnColumnValues("TranNoRel"))
        mlTranType = glGetValidLong(lkuSO.ReturnColumnValues("TranType"))
        
        sWhere = "CompanyID = " & gsQuoted(msCompanyID)
        sWhere = sWhere & " And TranNo = " & gsQuoted(lcTranNo)
        sWhere = sWhere & " And IsNull(TranNoRelChngOrd,'') = " & gsQuoted(lcTranNoRelChngOrd)
        sWhere = sWhere & " And IsNull(TranNoRel,'') = " & gsQuoted(lcTranNoRel)
        sWhere = sWhere & " And IsNull(TranType,'') = " & mlTranType
        
        mlSOKey = glGetValidLong(moClass.moAppDB.Lookup("SOKey", "tsoSalesOrder with(NoLock)", sWhere))
        txtTranID.Text = gsGetValidStr(moClass.moAppDB.Lookup("TranID", "tsoSalesOrder with(NoLock)", sWhere))
        
        Call GetSOData
        
        calDtOfShiping.Tag = calDtOfShiping.Text
        txtBidQty.Tag = gdGetValidDbl(txtBidQty.Value)
        txtProdQty.Tag = gdGetValidDbl(txtProdQty.Value)
        
    End If



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuSO_Click", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
   HandleToolBarClick Button
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'Initialize toolbar.
    
    tbrMain.Init sotaTB_REPORT, mlLanguage
    
    With tbrMain
        .RemoveSeparator kTbHelp
        .RemoveSeparator kTbSave
        
        .RemoveButton kTbSave
        .RemoveButton kTbHelp
        .RemoveButton kTbDelete
        .RemoveButton kTbDefer
        .RemoveButton kTbOffice
        
    End With
    
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
    End With
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = Me.Name
End Function

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "IMZ"   ' Place your help prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "IMZ"    ' Put your identifier here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaved", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbSaved = bNewSaved
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaved", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bLoadSuccess = mbLoadSuccess
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bLoadSuccess", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get UseHTMLHelp() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    UseHTMLHelp = True   ' Form uses HTML Help (True/False)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UseHTMLHelp", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property



Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim oFramework As Object
    '-- Set up lookup properties (see APZDA001 for reference)
    Set oFramework = moClass.moFramework

    Set oFramework = Nothing
    
    With lkuItem
        Set .FrameWork = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = msLookupRestrict
    End With
    
    
    With lkuWhse
        Set .FrameWork = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = msLookupRestrict
    End With
    

    Call bSetuplkuCOA
    Call bSetuplkuSO
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function bSetuplkuCOA() As Boolean
    On Error GoTo Error
    
    Dim lcFilter As String
    
    lcFilter = msLookupRestrict & " And (IsNull(IsHistoryRecord,0) = 1 Or isNull(IsActive,0) = 1 )"
'    lcFilter = msLookupRestrict & " And (IsNull(IsHistoryRecord,0) + isNull(IsActive,0)) >= 1 "
    
    bSetuplkuCOA = False
    
    If Len(Trim(lkuCOA.Tag)) = 0 Then
        bSetuplkuCOA = gbLookupInit(lkuCOA, moClass, moClass.moAppDB, "COATestResults", lcFilter)
        
        If bSetuplkuCOA Then
            lkuCOA.Tag = 1
        End If
    Else
        bSetuplkuCOA = True
    End If
    
    
    Exit Function
Error:
    MsgBox Me.Name & ".bSetuplkuCOA()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetuplkuCOA", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bSetuplkuSO() As Boolean
    On Error GoTo Error
    
    Dim lcFilter As String
    
    lcFilter = msLookupRestrict
    
    bSetuplkuSO = False
    
    
    If Len(Trim(lkuSO.Tag)) = 0 Then
        bSetuplkuSO = gbLookupInit(lkuSO, moClass, moClass.moAppDB, "SalesOrderWithCustomer", lcFilter)
        
        If bSetuplkuSO Then
            lkuSO.Tag = 1
        End If
    Else
        bSetuplkuSO = True
    End If
    
    
    Exit Function
Error:
    MsgBox Me.Name & ".bSetuplkuSO()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetuplkuSO", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub lkuCOA_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lcItemKey As Long
    Dim lcCOAReportKey As Long
    
    bSetuplkuCOA
        
    gcLookupClick Me, lkuCOA, txtTank, "Tank"
    
    If lkuCOA.ReturnColumnValues.Count > 0 Then
        mlPrintLogKey = 0
        
        lkuWhse.Text = lkuCOA.ReturnColumnValues("WhseID")
        lkuItem.Text = lkuCOA.ReturnColumnValues("ItemID")
        txtTank.Text = lkuCOA.ReturnColumnValues("Tank")
        txtLotNumber.Text = lkuCOA.ReturnColumnValues("LotNumber")

        mlCOATestRsltsKey = glGetValidLong(lkuCOA.ReturnColumnValues("COATestRsltsKey"))
        
        lcItemKey = glGetValidLong(lkuCOA.ReturnColumnValues("ItemKey"))
        
        lblItemDesc.Caption = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription with(NoLock)", "ItemKey = " & lcItemKey))
        
        chIsActive.Value = CBool(glGetValidLong(moClass.moAppDB.Lookup("IsActive", "timCOATestResults_SGS with(NoLock)", "COATestRsltsKey = " & mlCOATestRsltsKey)))
        chIsHistory.Value = CBool(glGetValidLong(moClass.moAppDB.Lookup("IsHistoryRecord", "timCOATestResults_SGS with(NoLock)", "COATestRsltsKey = " & mlCOATestRsltsKey)))
        
        calCmptDate.Value = gsGetValidStr(moClass.moAppDB.Lookup("CompletedDate", "timCOATestResults_SGS with(NoLock)", "COATestRsltsKey = " & mlCOATestRsltsKey))
        
        txtProdQty.Value = gdGetValidDbl(moClass.moAppDB.Lookup("ProductionQty", "timCOATestResults_SGS with(NoLock)", "COATestRsltsKey = " & mlCOATestRsltsKey))
        
        lcCOAReportKey = glGetValidLong(moClass.moAppDB.Lookup("COAReportKey", "timCOATestResults_SGS with(NoLock)", "COATestRsltsKey = " & mlCOATestRsltsKey))
        If lcCOAReportKey > 0 Then
            ddnCOARpts.ItemData = lcCOAReportKey
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuCOA_Click", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub SetupPrinters()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim Prntr As Printer
    
    cmboPrinter.Clear
    
    For Each Prntr In Printers
        cmboPrinter.AddItem Prntr.DeviceName
    Next
    
    cmboPrinter.Text = Printer.DeviceName

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupPrinters", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub GetSOData()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Get the data from the Order/quote/BSO
    Call GetCustName
    Call GetIASContrctNbr
    Call GetContractNbr
    Call GetBidQty
    Call GetDateOfShipping
    Call GetBOLNo
    Call GetProjNbr
    Call GetKeyNbr
    Call GetProjDesc
    Call GetJobState
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetSOData", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub GetCustName()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcCustName As String
    
    sFrom = "tsoSalesOrder with(NoLock) Inner Join tarCustomer with(NoLock) On tsoSalesOrder.CustKey = tarCustomer.CustKey"
    sWhere = "SOKey = " & mlSOKey
    sField = "tarCustomer.CustName"
    
    lcCustName = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtCustName.Text = lcCustName

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetCustName", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Sub GetIASContrctNbr()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcIASContrctNbr As String
    
    Select Case mlTranType
        Case 802    'BSO
            sFrom = "tsoSalesOrder with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "TranNo"
        Case 801    'SO
            sFrom = "tsoSalesOrder SO with(NoLock) Left Outer Join tsoSalesOrder BSO with(NoLock) On so.BlnktSOKey = bso.SOKey"
            sWhere = "SO.SOKey = " & mlSOKey
            sField = "ISNULL(bso.TranNo,SO.TranNO)"
        Case 840    'Quote
            sFrom = "tsoSalesOrder with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "TranNo"
        Case Else
            sFrom = "tsoSalesOrder with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "TranNo"
            'Unknown - Same as BSO
    End Select
    
    lcIASContrctNbr = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtIASContrctNbr.Text = lcIASContrctNbr

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetIASContrctNbr", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Sub GetContractNbr()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcContractNbr As String
    
    sFrom = "tsoSalesOrder SO with(NoLock) Left Outer Join tsoSalesOrderExt_SGS sx with(NoLock) On so.BlnktSOKey = sx.SOKey"
    sWhere = "so.SOKey = " & mlSOKey
    sField = "sx.COAStateCntrct"
    
    lcContractNbr = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtContractNbr.Text = lcContractNbr

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetContractNbr", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Sub GetBidQty()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcBidQty As Double
    
    Dim lcItemKey As Long
    Dim lcWhseKey As Long
    
    lcItemKey = glGetValidLong(lkuItem.KeyValue)
    lcWhseKey = glGetValidLong(lkuWhse.KeyValue)
    
    Select Case mlTranType
        Case 802    'BSO
            sFrom = "tsoSalesOrder SO with(NoLock) Inner Join tsoSOLine sl with(NoLock) on so.SOKey = sl.SOKey Inner Join tsoSOLineDist sd with(NoLock) on sl.SOLineKey=  sd.SOLineKey Inner Join timItem i with(NoLock) on sl.ItemKey = i.ItemKey Inner Join timItemDescription ids with(NoLock) on i.ItemKey = ids.ItemKey Inner Join timWarehouse w with(NoLock) on sd.WhseKey = w.WhseKey "
            sWhere = "so.SOKey = " & mlSOKey & " And i.ItemKey = " & lcItemKey & " And w.WhseKey = " & lcWhseKey
            sField = "Sum(sd.QtyOrd)"
        Case 801    'SO
            sFrom = "tsoSalesOrder S with(NoLock) Inner Join tsoSalesOrder SO with(NoLock) On s.BlnktSOKey = so.SOKey  Inner Join tsoSOLine sl with(NoLock) on so.SOKey = sl.SOKey Inner Join tsoSOLineDist sd with(NoLock) on sl.SOLineKey=  sd.SOLineKey Inner Join timItem i with(NoLock) on sl.ItemKey = i.ItemKey Inner Join timItemDescription ids with(NoLock) on i.ItemKey = ids.ItemKey Inner Join timWarehouse w with(NoLock) on sd.WhseKey = w.WhseKey "
            sWhere = "s.SOKey = " & mlSOKey & " And i.ItemKey = " & lcItemKey & " And w.WhseKey = " & lcWhseKey
            sField = "Sum(sd.QtyOrd)"
        Case 840    'Quote
            sFrom = "tsoSalesOrder SO with(NoLock) Inner Join tsoSOLine sl with(NoLock) on so.SOKey = sl.SOKey Inner Join tsoSOLineDist sd with(NoLock) on sl.SOLineKey=  sd.SOLineKey Inner Join timItem i with(NoLock) on sl.ItemKey = i.ItemKey Inner Join timItemDescription ids with(NoLock) on i.ItemKey = ids.ItemKey Inner Join timWarehouse w with(NoLock) on sd.WhseKey = w.WhseKey "
            sWhere = "so.SOKey = " & mlSOKey & " And i.ItemKey = " & lcItemKey & " And w.WhseKey = " & lcWhseKey
            sField = "Sum(sd.QtyOrd)"
        Case Else
            'Unknown - Same as BSO
            sFrom = "tsoSalesOrder SO with(NoLock) Inner Join tsoSOLine sl with(NoLock) on so.SOKey = sl.SOKey Inner Join tsoSOLineDist sd with(NoLock) on sl.SOLineKey=  sd.SOLineKey Inner Join timItem i with(NoLock) on sl.ItemKey = i.ItemKey Inner Join timItemDescription ids with(NoLock) on i.ItemKey = ids.ItemKey Inner Join timWarehouse w with(NoLock) on sd.WhseKey = w.WhseKey "
            sWhere = "so.SOKey = " & mlSOKey & " And i.ItemKey = " & lcItemKey & " And w.WhseKey = " & lcWhseKey
            sField = "Sum(sd.QtyOrd)"
    End Select
    
    lcBidQty = gdGetValidDbl(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtBidQty.Value = lcBidQty

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetBidQty", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub GetDateOfShipping()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcDateOfShipping As String
    
    sFrom = "tsoSalesOrder SO with(NoLock)"
    sWhere = "so.SOKey = " & mlSOKey
    sField = "TranDate"
    
    lcDateOfShipping = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    If IsDate(lcDateOfShipping) Then
        calDtOfShiping.Text = CDate(lcDateOfShipping)
    Else
        calDtOfShiping.Text = Empty
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetDateOfShipping", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Sub GetBOLNo()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcBOLNo As String
    
    sFrom = "tsoSalesOrderExt_SGS SO with(NoLock)"
    sWhere = "so.SOKey = " & mlSOKey
    sField = "bolno"
    
    lcBOLNo = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtBOLNo.Text = lcBOLNo

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetBOLNo", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Sub GetProjNbr()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcProjNbr As String
    
    Select Case mlTranType
        Case 802    'BSO
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "ProjectNumber"
        Case 801    'SO
            sFrom = "tsoSalesOrder SO with(NoLock) Left Outer Join tsoSalesOrder BSO with(NoLock) On so.BlnktSOKey = bso.SOKey Left Outer Join tsoSalesOrderExt_SGS bsx with(NoLock) On bso.SOKey = bsx.SOKey"
            sWhere = "SO.SOKey = " & mlSOKey
            sField = "bsx.ProjectNumber"
        Case 840    'Quote
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "ProjectNumber"
        Case Else
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "ProjectNumber"
            'Unknown - Same as BSO
    End Select
    
    lcProjNbr = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtProjNbr.Text = lcProjNbr

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetProjNbr", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub GetKeyNbr()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcKeyNbr As String
    
    Select Case mlTranType
        Case 802    'BSO
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "KeyNbr"
        Case 801    'SO
            sFrom = "tsoSalesOrder SO with(NoLock) Left Outer Join tsoSalesOrder BSO with(NoLock) On so.BlnktSOKey = bso.SOKey Left Outer Join tsoSalesOrderExt_SGS bsx with(NoLock) On bso.SOKey = bsx.SOKey"
            sWhere = "SO.SOKey = " & mlSOKey
            sField = "bsx.KeyNbr"
        Case 840    'Quote
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "KeyNbr"
        Case Else
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "KeyNbr"
            'Unknown - Same as BSO
    End Select
    
    lcKeyNbr = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtKeyNbr.Text = lcKeyNbr

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetKeyNbr", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub GetProjDesc()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcProjDesc As String
    
    Select Case mlTranType
        Case 802    'BSO
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "ProjectDesc"
        Case 801    'SO
            sFrom = "tsoSalesOrder SO with(NoLock) Left Outer Join tsoSalesOrder BSO with(NoLock) On so.BlnktSOKey = bso.SOKey Left Outer Join tsoSalesOrderExt_SGS bsx with(NoLock) On bso.SOKey = bsx.SOKey"
            sWhere = "SO.SOKey = " & mlSOKey
            sField = "bsx.ProjectDesc"
        Case 840    'Quote
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "ProjectDesc"
        Case Else
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "ProjectDesc"
            'Unknown - Same as BSO
    End Select
    
    lcProjDesc = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtProjDesc.Text = lcProjDesc

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetProjDesc", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Sub GetJobState()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sFrom As String
    Dim sWhere As String
    Dim sField As String
    Dim lcJobState As String
    
    Select Case mlTranType
        Case 802    'BSO
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "JobState"
        Case 801    'SO
            sFrom = "tsoSalesOrder SO with(NoLock) Left Outer Join tsoSalesOrder BSO with(NoLock) On so.BlnktSOKey = bso.SOKey Left Outer Join tsoSalesOrderExt_SGS bsx with(NoLock) On bso.SOKey = bsx.SOKey"
            sWhere = "SO.SOKey = " & mlSOKey
            sField = "bsx.JobState"
        Case 840    'Quote
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "JobState"
        Case Else
            sFrom = "tsoSalesOrderExt_SGS with(NoLock) "
            sWhere = "SOKey = " & mlSOKey
            sField = "JobState"
            'Unknown - Same as BSO
    End Select
    
    lcJobState = gsGetValidStr(moClass.moAppDB.Lookup(sField, sFrom, sWhere))
    
    txtJobState.Text = lcJobState

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetJobState", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim SQL As String
    
    '-- Set up drop down properties (see APZDA001 for reference)

    With ddnCOARpts
        SQL = "Select COATemplateReportID, COATemplateReportKey From timCOATemplateReports_SGS "
        
        .SQLStatement = SQL
        
        .InitDynamicList moClass.moAppDB, .SQLStatement
        
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupDropDowns", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function GetNextKey() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lKey As Long

    With moClass.moAppDB
        .SetInParam "timCOAPrintOutOfProcessLog_SGS"
        .SetOutParam lKey
        .ExecuteSP ("spGetNextSurrogateKey")
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With
    
    mlPrintLogKey = lKey
    
    If Err.Number <= 0 Then
        GetNextKey = True
    Else
        GetNextKey = False
        
        MsgBox "The following error happend while getting the next key value:" & vbCrLf & _
        "GetNextKey()" & vbCrLf & "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
        
        Err.Clear
    End If
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetNextKey", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function CreateCOALogRec() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lcPrintLogKey As Long
    Dim lcResult As Long
    Dim lcSOKey As Long
    
    CreateCOALogRec = False
    
    If chLookData.Value = vbChecked Then
        lcSOKey = mlSOKey
    Else
        lcSOKey = 0
    End If
    
    If mlPrintLogKey > 0 Then
        'The same data is being printed again.
        CreateCOALogRec = True
        Exit Function
    End If
    
    If GetNextKey() = False Then
        Exit Function
    End If

    With moClass.moAppDB
        .SetInParam mlPrintLogKey  '@PrintLogKey
        .SetInParam mlCOATestRsltsKey  '@COATestRsltsKey
        .SetInParam glGetValidLong(lkuWhse.KeyValue)  '@WhseKey
        .SetInParam glGetValidLong(lkuItem.KeyValue)  '@ItemKey
        .SetInParam txtTank.Text  '@Tank
        .SetInParam txtLotNumber.Text  '@LotNumber
        .SetInParam msCompanyID  '@CompanyID
        .SetInParam txtProdQty.Value  '@ProductionQty
        .SetInParam calCmptDate.Text  '@CompletedDate
        .SetInParam glGetValidLong(ddnCOARpts.ItemData)  '@COAReportKey
        .SetInParam chLookData.Value  '@UseDataFromSO
        .SetInParam lcSOKey  '@SOKey
        .SetInParam txtCustName.Text  '@CustName
        .SetInParam txtIASContrctNbr.Text  '@IASContractNbr
        .SetInParam txtContractNbr.Text  '@StateContractNbr
        .SetInParam txtBidQty.Value  '@BidQty
        .SetInParam calDtOfShiping.Text '@DateOfShipping
        .SetInParam txtBOLNo.Text  '@BOLNbr
        .SetInParam txtProjNbr.Text  '@ProjectNbr
        .SetInParam txtKeyNbr.Text  '@KeyNbr
        .SetInParam txtProjDesc.Text  '@ProjDesc
        .SetInParam txtJobState.Text  '@JobState
        .SetInParam cmboPrinter.Text  '@PrintDeviceName
        .SetInParam txtDelLoc.Text  '@DeliveryLoc
        .SetOutParam lcResult   '@Result
        .ExecuteSP ("spIMInsCOAPrntOutOfPrcsLog_RKL")
        lcResult = glGetValidLong(.GetOutParam(25))
        .ReleaseParams
    End With
    
    If Err.Number > 0 Then
        CreateCOALogRec = False
    End If
    
    Select Case lcResult
        Case 1  'Success
            CreateCOALogRec = True
        Case Else    'Some Error
            CreateCOALogRec = False
            
            MsgBox "Was not able to create the data to print the COA.", vbExclamation, "MAS 500"
            
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CreateCOALogRec", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Function PrintCOA(sButton As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lcPrintLogKey As Long
    
    PrintCOA = False
    
    'Validate Date Before Printing
    If ValidateDate = False Then
        Exit Function
    End If
    
    'Save Data to the Database
    If CreateCOALogRec() = False Then
        Exit Function
    End If
    
    'Print Data
    If PrintCOALogic(sButton) = False Then
        PrintCOA = False
    Else
        PrintCOA = True
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PrintCOA", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Function ValidateDate() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    If mlCOATestRsltsKey <= 0 Then
        MsgBox "You need to select a COA Test Results Record First.", vbInformation, "MAS 500"
        ValidateDate = False
        Exit Function
    End If
    
    If mlSOKey <= 0 And chLookData.Value = vbChecked Then
        MsgBox "You have the 'Use Data From MAS BSO, SO, or Quote' checkbox checked but have not selected a record.  Please select the record first or unselect the checkbox.", vbInformation, "MAS 500"
        ValidateDate = False
        Exit Function
    End If
    
    If Trim(ddnCOARpts.Text) = Empty Then
        MsgBox "You must select a COA Template first.", vbInformation, "MAS 500"
        ValidateDate = False
        Exit Function
    End If
    
    If Trim(cmboPrinter.Text) = Empty Then
        MsgBox "You must select a printer first.", vbInformation, "MAS 500"
        ValidateDate = False
        Exit Function
    End If
    
    If txtNbrOfCopies.Value <= 0 Then
        MsgBox "You must enter a number copies greater than 0.", vbInformation, "MAS 500"
        ValidateDate = False
        Exit Function
    End If
    
    If txtNbrOfCopies.Value > 7 Then
        If MsgBox("You have specified to print " & txtNbrOfCopies.Value & " are you sure you want to print that many copies?", vbYesNo, "Confirm Number of Copies") <> vbYes Then
            ValidateDate = False
            Exit Function
        End If
    End If
    
    ValidateDate = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ValidateDate", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Function PrintCOALogic(sButton As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'    Dim ReportObj As clsReportEngine
    Dim moDDData As clsDDData
    Dim sRealTableCollection     As Collection
'    Dim ReportName As String
    
    
    'RKL DEJ 2017-05-16 (START)
    'Hard Code the Report Name.  There is only one report.
'    Call GetReportName
    ReportName = "0000 COA Template Out Of Process.rpt"
    'RKL DEJ 2017-05-16 (START)
    
    Set sRealTableCollection = New Collection
'    sRealTableCollection.Add "vluIMCOAPrintOutOfProcessLogData_SGS"    'RKL DEJ 2017-05-16 This was the original
'    sRealTableCollection.Add "vluCOAOutOfProcess_RKL"                  'RKL DEJ 2017-05-16 Would be this but just leaving it off
    
    Set moDDData = New clsDDData
    
    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
'        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
'        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If

    
    Set ReportObj = New clsReportEngine
    ReportObj.UI = False
    ReportObj.AppOrSysDB = kAppDB
    
    If (ReportObj.lInitReport("IM", ReportName, Me, moDDData, False) = kFailure) Then
'        MsgBox "Report Obj error"
        Exit Function
    End If
    
    ReportObj.ReportFileName() = ReportName
    ReportObj.PrinterName = cmboPrinter.Text
    ReportObj.Copies = txtNbrOfCopies.Value
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        MsgBox "Error setting up the report."
    End If
    
    ReportObj.Orientation() = vbPRORPortrait
    

    ReportObj.UseSubTotalCaptions() = 0
    ReportObj.UseHeaderCaptions() = 0
            
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL


'    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
'    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'RKL DEJ 2017-05-16 Changed the filter from vluIMCOAPrintOutOfProcessLogData_SGS to vluCOAOutOfProcess_RKL
    If (ReportObj.lRestrictBy("{vluCOAOutOfProcess_RKL.PrintLogKey} = " & mlPrintLogKey) = kFailure) Then
        MsgBox "Filter Error"
    End If
    
'    ReportObj.ProcessReport Me, kTbPreview, , sFileName
    ReportObj.ProcessReport Me, sButton
    
            
    ShowStatusNone Me

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PrintCOALogic", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function




Private Sub txtBidQty_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    If gdGetValidDbl(txtBidQty.Tag) <> gdGetValidDbl(txtBidQty.Value) Then
        mlPrintLogKey = 0
        txtBidQty.Tag = gdGetValidDbl(txtBidQty.Value)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtBidQty_Validate", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtBOLNo_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtBOLNo_Change", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtContractNbr_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtContractNbr_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtCustName_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtCustName_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtDelLoc_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtDelLoc_Change", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtIASContrctNbr_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtIASContrctNbr_Change", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtJobState_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtJobState_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtKeyNbr_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtKeyNbr_Change", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub txtProdQty_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        
    If gdGetValidDbl(txtProdQty.Tag) <> gdGetValidDbl(txtProdQty.Value) Then
        mlPrintLogKey = 0
        txtProdQty.Tag = gdGetValidDbl(txtProdQty.Value)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtProdQty_Validate", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtProjDesc_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtProjDesc_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtProjNbr_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlPrintLogKey = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtProjNbr_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Sub GetReportName()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sWhere As String
    
    sWhere = "COATemplateReportKey = " & glGetValidLong(ddnCOARpts.ItemData)
    
    ReportName = UCase(gsGetValidStr(moClass.moAppDB.Lookup("CrystalRptFileName", "timCOATemplateReports_SGS with(NoLock)", sWhere)))
    
    ReportName = Replace(ReportName, ".RPT", " OUT OF PROCESS.RPT")

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetReportName", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

