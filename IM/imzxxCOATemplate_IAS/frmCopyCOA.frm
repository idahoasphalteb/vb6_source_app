VERSION 5.00
Begin VB.Form frmCopyCOA 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "New COA Template ID"
   ClientHeight    =   1110
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   Icon            =   "frmCopyCOA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1110
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtNewID 
      Height          =   285
      Left            =   120
      MaxLength       =   60
      TabIndex        =   0
      Top             =   360
      Width           =   4335
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmCopyCOA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
    gbNewCOATemplateID = Empty
    Unload Me
End Sub

Private Sub OKButton_Click()
    gbNewCOATemplateID = Trim(txtNewID.Text)
    Unload Me
End Sub
