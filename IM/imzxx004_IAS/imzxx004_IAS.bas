Attribute VB_Name = "basIMStatCOAReport"
Option Explicit


Const VBRIG_MODULE_ID_STRING = "imzxx004_IAS.BAS"

Public gbSkipPrintDialog As Boolean     '   Show print dialog




Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("imzxx004_IAS.clsSOShpmntReport")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

'SGS DEJ
Function PrepSelect(SessionID As String) As String
    Dim sSelect As String
    Dim DtVal As Variant
    Dim iFristRow As Integer
    Dim iLastRow As Integer
    Dim i As Integer
    Dim StrVal1 As Variant
    Dim StrVal2 As Variant
    
    'SGS DEJ Added to Display the Date Range on the Report START
    frmIMStatCOAReport.msShpDtRnge = Empty
    
    
    For i = 1 To frmIMStatCOAReport.grdSelection.MaxRows
        iFristRow = frmIMStatCOAReport.moSelect.lGetGrdRowFromCaption("Ship Date", i, False)
        If iFristRow > 0 Then
            Exit For
        End If
    Next
    
    iLastRow = frmIMStatCOAReport.moSelect.lGetGrdRowFromCaption("Ship Date", frmIMStatCOAReport.grdSelection.MaxRows, False)
'    iLastRow = frmIMStatCOAReport.moSelect.lGetGrdRowFromTblCol("vluIMCOATestResultsDetl_RKL", "ShipDate", frmIMStatCOAReport.grdSelection.MaxRows)
    
    For i = iFristRow To iLastRow
        StrVal1 = Empty
        StrVal2 = Empty
        DtVal = Empty
        
        'Get the Filter Condition / Clause
        frmIMStatCOAReport.grdSelection.GetText 1, i, StrVal1
        frmIMStatCOAReport.grdSelection.GetText 2, i, StrVal2
        
        If InStr(1, UCase(StrVal1), "OR") > 0 Then
            StrVal1 = " OR "
        ElseIf InStr(1, UCase(StrVal1), "AND") > 0 Then
            StrVal1 = " AND "
        Else
            StrVal1 = Empty
        End If
        
        If UCase(Trim(StrVal2)) = "ALL" Then
            StrVal2 = Empty
        End If
        
        'Get First Date Value
        frmIMStatCOAReport.grdSelection.GetText 3, i, DtVal
        If Trim(CStr(DtVal)) <> Empty Then
            frmIMStatCOAReport.msShpDtRnge = frmIMStatCOAReport.msShpDtRnge & StrVal1 & StrVal2 & " " & Trim(CStr(DtVal))
'            If Trim(frmIMStatCOAReport.msShpDtRnge) = Empty Then
'                frmIMStatCOAReport.msShpDtRnge = StrVal2 & " " & frmIMStatCOAReport.msShpDtRnge & Trim(CStr(DtVal))
'            Else
'                frmIMStatCOAReport.msShpDtRnge = frmIMStatCOAReport.msShpDtRnge & StrVal1 & StrVal2 & " " & Trim(CStr(DtVal))
'            End If
        End If
        
        DtVal = Empty
        
        'Get Second Date Value
        frmIMStatCOAReport.grdSelection.GetText 4, i, DtVal
        If Trim(CStr(DtVal)) <> Empty Then
            frmIMStatCOAReport.msShpDtRnge = frmIMStatCOAReport.msShpDtRnge & " - " & Trim(CStr(DtVal))
        End If
    Next
    
    'The Field length is 500
    frmIMStatCOAReport.msShpDtRnge = Left(frmIMStatCOAReport.msShpDtRnge, 500)
        
  
'    sSelect = _
'    "Select " & SessionID & "'SessionID', " & _
'    "'" & frmIMStatCOAReport.msUserID & "' 'User', " & _
'    "ShipmentType, ShipmentTypeID, CompanyID, CompanyName, SOKey, SOLineKey, " & _
'    "SOLineDistKey, CustKey, ShipKey, ShipLineKey, ShipLineDistKey, " & _
'    "BlnktSOKey, WhseKey, VendKey, ItemKey, ShipDate, CustID, CustName, " & _
'    "BTranNo, ContractNo, Lading, Truck, Facility, FacilityDesc, Vendor, " & _
'    "VendorName, Item, ItemLongDesc, ItemShortDesc, QtyShipped, ItemClassKey, " & _
'    "ItemClassID, ReleaseNumber, SOTranNo, SOTranNoRel, " & _
'    "'" & frmIMStatCOAReport.msShpDtRnge & "' " & _
'    "From vluIMCOATestResultsDetl_RKL Where 1=1 "

    sSelect = _
    "(SessionID, CompanyID, COATestRsltsKey, LotNumber, Tank, ItemID, ItemShortDesc, ItemLongDesc, ItemKey, WhseID, WhseKey, WhseDesc, SampleDate, SampleID, TestDate, CompletedDate, Comments, ProductionQty, CreateUserID, CreateDate, UpdateUserID, UpdateDate, IsActive, IsHistoryRecord, LabTestKey, LabTestID, LabTestDesc, PGBinderGroupKey, PGBinderGroup, PGBinderGroupDesc, PGBinderSortOrder, MethodStandardKey, MethodStandardID, LabUOMKey, LabUOMID, ComparisonOperator, ValueRange01, ValueRange02, COATestSortOrder, MDL, TestResults, COAReportKey, COATemplateReportID, COATemplateReportDesc, COATemplateReportName, COATemplatePgHdrOne, COATemplatePgHdrTwo, COATemplatePgHdrThree, COATemplatePgFtrOne, COATemplatePgFtrTwo, COATemplatePgFtrThree, WhichIconHdrToShow, WhichSigFtrToShow, LotNoLable, ShowSubsidiary, SortByLotNumber, SortByTank, SortByItemID, SortByWhseID, SortByCompletedDate, NumericTestResults )" & _
    "Select " & SessionID & "'SessionID', " & _
    "CompanyID, COATestRsltsKey, LotNumber, Tank, ItemID, ItemShortDesc, ItemLongDesc, ItemKey, WhseID, WhseKey, WhseDesc, SampleDate, SampleID, TestDate, CompletedDate, Comments, ProductionQty, CreateUserID, CreateDate, UpdateUserID, UpdateDate, IsActive, IsHistoryRecord, LabTestKey, LabTestID, LabTestDesc, PGBinderGroupKey, PGBinderGroup, PGBinderGroupDesc, PGBinderSortOrder, MethodStandardKey, MethodStandardID, LabUOMKey, LabUOMID, ComparisonOperator, ValueRange01, ValueRange02, COATestSortOrder, MDL, TestResults, COAReportKey, COATemplateReportID, COATemplateReportDesc, COATemplateReportName, COATemplatePgHdrOne, COATemplatePgHdrTwo, COATemplatePgHdrThree, COATemplatePgFtrOne, COATemplatePgFtrTwo, COATemplatePgFtrThree, WhichIconHdrToShow, WhichSigFtrToShow, LotNoLable, ShowSubsidiary, SortByLotNumber, SortByTank, SortByItemID, SortByWhseID, SortByCompletedDate, NumericTestResults " & _
    "From vluIMCOATestResultsDetl_RKL Where 1=1 "

    

    PrepSelect = sSelect
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSOShpmntReport"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, _
    sPerEndDate, iPerNo, sPerYear, Optional iFileType As Variant, Optional sFileName As Variant) As Long
    
Dim sWhereClause As String
Dim sTablesUsed As String
Dim sSelect As String
Dim sInsert As String
Dim lRetval As Long
Dim bValid As Boolean
Dim iNumTablesUsed As Integer
Dim RptFileName As String
Dim lBadRow As Long
Dim SelectObj As clsSelection
Dim ReportObj As clsReportEngine
Dim SortObj As clsSort
Dim DBObj As Object
Dim SettingsObj As clsSettings

    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set SelectObj = frm.moSelect
    Set ReportObj = frm.moReport
    Set SortObj = frm.moSort

    Set DBObj = frm.oClass.moAppDB
    Set SettingsObj = frm.moSettings
   
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    'Correct bad user input in the Selection grid.
    'lBadRow contains the failed row if the parameter passed is False
    lBadRow = SelectObj.lValidateGrid(True)
    
    'Create the WHERE clause based on user input in the Selection grid & Session ID.
    bValid = SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)

    'Append criteria to restrict data returned to the current company.
    SelectObj.AppendToWhereClause sWhereClause, "vluIMCOATestResultsDetl_RKL.CompanyID=" & gsQuoted(frm.msCompanyID)
    
    If frmIMStatCOAReport.OptnDetail.Value = False Then
        sWhereClause = sWhereClause & " And CoalEsce(TestResults,'') <> '' "
    End If
    
    'CUSTOMIZE:  Add join specifications to WHERE clause based on appearance of a given table
    'in the sTablesUsed string (built as a result of call to bGetWhereClause above).
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciAddress", "tarCustomer.PrimaryAddrKey=tciAddress.AddrKey"
    'SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tarCustomer.CustKey=tarCustAddr.CustKey"
    
'    'SGS DEJ Commented out because we do not need the sintax of 'Where'
'    'Perform appropriate checks on WHERE clause; prepend "WHERE " if necessary.
'    If Not SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True) Then
'        GoTo badexit
'    End If

    'CUSTOMIZE:  Build a string for selection of key field and session ID.
'    sSelect = "SELECT DISTINCT timItem., " & ReportObj.SessionID & " FROM " & sTablesUsed
'    sSelect = sSelect & " " & sWhereClause
Debug.Print sWhereClause
    #If RPTDEBUG Then
        sInsert = "INSERT INTO timCOAStatisticalRptWrk_RKL " & PrepSelect(ReportObj.SessionID) & " And " & sWhereClause
    #Else
        sInsert = "INSERT INTO #timCOAStatisticalRptWrk_RKL " & PrepSelect(ReportObj.SessionID) & " And " & sWhereClause
    #End If
Debug.Print sInsert
    
    
    On Error Resume Next
    DBObj.ExecuteSQL sInsert
    If Err.Number <> 0 Then
        ReportObj.ReportError kmsgProc, sInsert
        GoTo badexit
    End If
    gClearSotaErr
    On Error GoTo badexit
    
    'Check whether any records were inserted; if not, go to error handler.
    If IsMissing(iFileType) Then
        #If RPTDEBUG Then
            If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'            If Not SelectObj.bRecsToPrintExist(timCOAStatisticalRptWrk_RKL, "SessionID", ReportObj.SessionID, True) Then
        #Else
'            If Not SelectObj.bRecsToPrintExist("#timCOAStatisticalRptWrk_RKL", "SessionID", ReportObj.SessionID, True) Then
            If Not SelectObj.bRecsToPrintExist("#" & frm.sWorkTableCollection(1), "SessionID", ReportObj.SessionID, True) Then
'            If Not SelectObj.bRecsToPrintExist("#" & Left(frm.sWorkTableCollection(1), 19), "SessionID", ReportObj.SessionID, True) Then
        #End If
            GoTo badexit
        End If
    End If
   
'    'CUSTOMIZE:  Set input and output parameters as needed and execute appropriate
'    'custom stored procedure to populate remaining fields in primary work table and
'    'insert records into secondary work tables.
'    With DBObj
'        .SetInParam (ReportObj.SessionID)
'        .SetOutParam lRetval
'        On Error Resume Next
'        .ExecuteSP ("spSGSReportTest")
'        If Err.Number <> 0 Then
'            lRetval = Err.Number
'        Else
'            'CUSTOMIZE:  If procedure has output parameters, attempt to retrieve them, checking for
'            'errors after each attempt.  After storing all return values in local variables OR
'            'encountering an error, release parameters.
'            lRetval = DBObj.GetOutParam(2)
'        End If
'    End With
'
'    If lRetval <> 0 Then
'        ReportObj.ReportError kmsgProc, ""
'        DBObj.ReleaseParams
'        GoTo badexit
'    End If
        
    DBObj.ReleaseParams
    gClearSotaErr
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
    'SGS DEJ Start report (summary vs Detail)
    If frmIMStatCOAReport.OptnDetail.Value = True Then
        ReportObj.ReportFileName() = "IMCOAStatisticalReport001.rpt"
        ReportObj.ReportTitle1() = "COA Tests Report Detail"
    ElseIf frmIMStatCOAReport.optnDetailStats.Value = True Then
        ReportObj.ReportFileName() = "IMCOAStatisticalReport002.rpt"
        ReportObj.ReportTitle1() = "COA Tests Statistical Report Detail"
    Else
'        ReportObj.ReportFileName() = "IMCOAStatisticalReport003.rpt"
        ReportObj.ReportFileName() = "IMCOAStatisticalReport002.rpt"
        ReportObj.ReportTitle1() = "COA Tests Statistical Report Summary"
    End If
    'SGS DEJ Start report (summary vs Detail)
    
    'Set Report Date Range if there is a filter on Completed Date
    ReportObj.ReportTitle2() = frmIMStatCOAReport.GetDateRange()
    
    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
'    'SGS DEJ Start report (summary vs Detail)
'    If frmIMStatCOAReport.OptnDetail.Value = True Then
'        ReportObj.Orientation() = vbPRORLandscape
'    Else
'        ReportObj.Orientation() = vbPRORLandscape
'    End If
'    'SGS DEJ Start report (summary vs Detail)
    
    'CUSTOMIZE:  Set report titles to localized text from tsmLocalString table using call
    'to gsBuildString with a VB constant defined in LocalizationConst.bas. The subtitles should
    'not include the format selected by the user, i.e., "Detail" or "Summary".
    'ReportObj.ReportTitle2() = ""

    
    ReportObj.UseHeaderCaptions = False
    
'    ReportObj.SetReportCaptions

    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 1
    ReportObj.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    'Set sort order in .RPT file according to user selections in the Sort grid.
    'SGS DEJ added the number of hard coded groupings.
    If frmIMStatCOAReport.OptnDetail.Value = True Then
        'This is the First Report... Hardcoded to sort by Lot first.
        If (ReportObj.lSetSortCriteria(frm.moSort, , True) = kFailure) Then
            GoTo badexit
        End If
    Else
        If (ReportObj.lSetSortCriteria(frm.moSort, 2, True) = kFailure) Then
            GoTo badexit
        End If
    End If
    
    '********* the following is specific to your report *************'
    Select Case RptFileName
        Case "XXZYY001.RPT"
        Case Else
    End Select
    '********* End of special processing *************'
    
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
    ReportObj.SelectString = SelectObj.sGetUserReadableWhereClause(kLenPortrait)
    
    'If user has chosen to print report settings, set text for summary section.
    If (ReportObj.lSetSummarySection(frm.chkSummary.Value, frm.moOptions) = kFailure) Then
        GoTo badexit
    End If
    
    'CUSTOMIZE:  If using work tables, restrict report data to current Session ID.  If using
    'real tables, might restrict report data to current company or other criteria.
'    If (ReportObj.lRestrictBy("{.SessionID} = " & ReportObj.SessionID) = kFailure) Then
'SGS DEJ change to map to the actual table.
    If (ReportObj.lRestrictBy("{" & frm.sWorkTableCollection(1) & ".SessionID} = " & ReportObj.SessionID) = kFailure) Then

    'frm.sWorkTableCollection(1)
        GoTo badexit
    End If
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   don't show print dialog
    End If
    
    ReportObj.ProcessReport frm, sButton, iFileType, sFileName
    gbSkipPrintDialog = True    '   if multiple rpts, show print dialog one time only.
            
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    
    Exit Function
    
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    Set SortObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
    Exit Function
End Function

