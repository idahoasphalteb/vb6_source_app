VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmValeroExport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valero Export"
   ClientHeight    =   4515
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6405
   HelpContextID   =   17770001
   Icon            =   "frmValeroExport.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4515
   ScaleWidth      =   6405
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   255
      Left            =   5760
      TabIndex        =   18
      ToolTipText     =   "Browse to the Access Database"
      Top             =   2280
      Width           =   495
   End
   Begin VB.TextBox txtSavePath 
      Height          =   285
      Left            =   120
      TabIndex        =   17
      Top             =   2280
      Width           =   5535
   End
   Begin MSComCtl2.DTPicker dtMinDate 
      Height          =   375
      Left            =   360
      TabIndex        =   12
      Top             =   1440
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   190971905
      CurrentDate     =   43102
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   5
      Top             =   4320
      Visible         =   0   'False
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   4
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -10000
      Style           =   2  'Dropdown List
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      Width           =   1245
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   10
      Top             =   645
      Visible         =   0   'False
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4125
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   0
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin MSComCtl2.DTPicker dtMaxDate 
      Height          =   375
      Left            =   2160
      TabIndex        =   14
      Top             =   1440
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   190971905
      CurrentDate     =   43102
   End
   Begin MSComDlg.CommonDialog cdExportPath 
      Left            =   5400
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DefaultExt      =   """*.mdb | *.*"""
      Filter          =   "MS Access (*.mdb)|*.mdb;All (*.*):*.*"
   End
   Begin VB.Label lblSavePath 
      Caption         =   "Export To This Path:"
      Height          =   255
      Left            =   120
      TabIndex        =   16
      Top             =   2040
      Width           =   2055
   End
   Begin VB.Label lblMaxDate 
      Caption         =   "Max Date"
      Height          =   255
      Left            =   2160
      TabIndex        =   15
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label lblMinDate 
      Caption         =   "Min Date"
      Height          =   255
      Left            =   360
      TabIndex        =   13
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmValeroExport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Private miSecurityLevel     As Integer

Public moSotaObjects        As New Collection

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

Private moContextMenu       As New clsContextMenu

Private miOldFormHeight     As Long

Private miOldFormWidth      As Long

Private miMinFormHeight     As Long

Private miMinFormWidth      As Long

Private mbLoadSuccess       As Boolean

Public mlLanguage As Long

Private FSO As New Scripting.FileSystemObject


Private Sub cmdBrowse_Click()
    On Error GoTo Error
    Dim FilePath As String
    
    cdExportPath.Filter = "Comma-Separated (*.csv)|*.csv|All (*.*)|*.*"

    cdExportPath.FileName = txtSavePath.Text
    cdExportPath.DialogTitle = "Save Exort As"
'    cdExportPath.DefaultExt = "*.csv"
    cdExportPath.ShowOpen
    
    If cdExportPath.FileName = Empty Then Exit Sub
    
    FilePath = cdExportPath.FileName

    txtSavePath.Text = FilePath
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdBrowse_Click()" & vbCrLf & "Browsing Error: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    Err.Clear
End Sub

Private Sub Form_Initialize()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mbCancelShutDown = False
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) Form_Initialize - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) Form_KeyDown - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) Form_KeyPress - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_Load()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    mbLoadSuccess = False

    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab

    BindToolbar
    Set sbrMain.FrameWork = moClass.moFramework


    ' set form size variables
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    mbLoadSuccess = True
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) Form_Load - " & Err.Description 'Generic Error Rig
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Dim iConfirmUnload  As Integer


   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
    
        'Check all other forms  that may have been loaded from this main form.
        'If there are any visible forms, then this means the form is active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
           'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is Normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                        
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
                
        End Select
        
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
            'Do nothing
            
    End Select
    
Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) Form_QueryUnload - " & Err.Description    'Generic Error Rig
End Sub

Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    'Unload all forms loaded from this main form
    gUnloadChildForms Me

    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me

    ' perform proper cleanup
    Set moContextMenu = Nothing         ' context menu class
    Set moSotaObjects = Nothing         ' SOTA Child objects collection
    
    Set FSO = Nothing

Exit Sub

ExpectedErrorRoutine:

End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Set moClass = Nothing
     
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) Form_Unload - " & Err.Description    'Generic Error Rig
End Sub

Public Sub HandleToolBarClick(sKey As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Dim ErrMsg As String
    
    Select Case sKey
        
      'Save changes and close the form
        Case kTbFinishExit
        
            If IsValid() = False Then
                'Did not pass validation so stop here
                Exit Sub
            End If
            
            'Create the export ActiveX Object
            Dim exp As Object   ' New ValeroExport.clsValeroExport
            Set exp = CreateObject("ValeroExport.clsValeroExport")
            
            'Set Export Parameters
            exp.ConnStr = ADOConnectionString(moClass.moSysSession.SysADOConnect, oClass, "MAS 500 Valero Export", "Valero Export")
            exp.ExportFilePath = txtSavePath.Text
            exp.MinDate = dtMinDate.Value
            exp.MaxDate = dtMaxDate.Value
            exp.UseMinMaxDate = True
            
            'Call the export
            If Not exp.ExportData(ErrMsg) = True Then
                MsgBox "The Export failed with this error message:" & ErrMsg, vbInformation, "Failure"
            Else
                MsgBox "The export was successfull.", vbInformation, "Success"
            End If
            
            Set exp = Nothing
            
'            'The following method is to test getting the connection string
'            Call TestDB
            
      'Cancel changes and close the form
        Case kTbCancelExit
                        
      'Display form level help
        Case kTbHelp
            gDisplayFormLevelHelp Me
            
      'Example code to save changes and display report form
        Case kTbPrint
            
        
        Case Else
                
    End Select
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) HandleToolBarClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
   HandleToolBarClick Button
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) tbrMain_ButtonClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub BindToolbar()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    'Initialize toolbar.
'    tbrMain.Init sotaTB_DATASHEET, mlLanguage
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
    End With
    tbrMain.AddButton kTbFinishExit
'    tbrMain.RemoveButton kTbCancelExit
    tbrMain.SetButtonProperties kTbFinishExit, "Export Data"
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) BindToolbar - " & Err.Description    'Generic Error Rig
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = Me.Name
End Function

Public Property Get FormHelpPrefix() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    FormHelpPrefix = "IMZ"   ' Place your help prefix here
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) FormHelpPrefix - " & Err.Description    'Generic Error Rig
End Property

Public Property Get WhatHelpPrefix() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    WhatHelpPrefix = "IMZ"    ' Put your identifier here
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) WhatHelpPrefix - " & Err.Description    'Generic Error Rig
End Property

Public Property Get oClass() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set oClass = moClass
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) oClass - " & Err.Description 'Generic Error Rig
End Property
Public Property Set oClass(oNewClass As Object)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set moClass = oNewClass
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Set) oClass - " & Err.Description 'Generic Error Rig
End Property

Public Property Get lRunMode() As Long
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    lRunMode = mlRunMode
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) lRunMode - " & Err.Description    'Generic Error Rig
End Property
Public Property Let lRunMode(lNewRunMode As Long)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mlRunMode = lNewRunMode
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Let) lRunMode - " & Err.Description    'Generic Error Rig
End Property

Public Property Get bSaved() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bSaved = mbSaved
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) bSaved - " & Err.Description 'Generic Error Rig
End Property
Public Property Let bSaved(bNewSaved As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mbSaved = bNewSaved
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Let) bSaved - " & Err.Description 'Generic Error Rig
End Property

Public Property Get bCancelShutDown()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bCancelShutDown = mbCancelShutDown
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) bCancelShutDown - " & Err.Description    'Generic Error Rig
End Property

Public Property Get bLoadSuccess() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bLoadSuccess = mbLoadSuccess
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) bLoadSuccess - " & Err.Description    'Generic Error Rig
End Property

Public Property Get MyApp() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set MyApp = App
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) MyApp - " & Err.Description  'Generic Error Rig
End Property

Public Property Get MyForms() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set MyForms = Forms
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) MyForms - " & Err.Description    'Generic Error Rig
End Property

Public Property Get UseHTMLHelp() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    UseHTMLHelp = True   ' Form uses HTML Help (True/False)
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Property Get) UseHTMLHelp - " & Err.Description    'Generic Error Rig
End Property


Function IsValid() As Boolean
    On Error GoTo Error
    
    Dim FilePath As String
    Dim FolderPath As String
    
    '*******************************************************
    'Validate the Date Range
    '*******************************************************
    If dtMinDate.Value > dtMaxDate.Value Then
        MsgBox "The Min Date cannot be after the Max Date.", vbInformation, "Date Range Not Valid"
        IsValid = False
        Exit Function
    End If
    
    '*******************************************************
    'validate the File path
    '*******************************************************
    FilePath = Trim(txtSavePath.Text)
    
    If Len(FilePath) <= 0 Then
        MsgBox "You need to specify a export file path first.", vbInformation, "Missing Export Path"
        IsValid = False
        Exit Function
    Else
        If FSO.FileExists(FilePath) = True Then
            If MsgBox("The file '" & vbCrLf & FilePath & "'" & vbCrLf & "already exists.  Do you want to overwrite the existing file?", vbYesNo) <> vbYes Then
                IsValid = False
                Exit Function
            End If
        Else
            'Validate the file path
            FolderPath = FSO.FolderExists(FilePath)
            FolderPath = FSO.GetParentFolderName(FilePath)
            If FSO.FolderExists(FolderPath) = False Then
                MsgBox "The path to that file either does not exist or is invalid.  Fix the file path before proceding.", vbInformation, "Not a Valid Path"
                IsValid = False
                Exit Function
            End If
        End If
    End If
    
    'If we get to this point then the data is valid
    IsValid = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".IsValid()" & vbCrLf & "Browsing Error: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
End Function


Sub TestDB()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Dim ConnStr As String
    
    ConnStr = ADOConnectionString(moClass.moSysSession.SysADOConnect, oClass, "MAS 500 Valero Export", "Valero Export")
    'Provider=SQLOLEDB;User ID=admin;Data Source=testdb2012;Initial Catalog=mas500_app_IAS02;Trusted_Connection=No;OLE DB Services= -2;Application Name=Sage 500 ERP/Valero Export/1010014013/WEC/RES-DJONES/2;

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmValeroExport,  (Procedure) TestDB - " & Err.Description    'Generic Error Rig
End Sub
