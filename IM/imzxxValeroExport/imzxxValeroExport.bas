Attribute VB_Name = "basValeroExport"
Option Explicit

Const VBRIG_MODULE_ID_STRING = "imzxxValeroExport.BAS"


Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("imzxxValeroExport.clsValeroExport")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basValeroExport"
End Function
