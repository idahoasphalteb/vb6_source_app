Attribute VB_Name = "basSetupBins"

Const VBRIG_MODULE_ID_STRING = "imzmn001.BAS"


Public Sub Main()

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("imzmn001.clsSetupBins")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the Enterprise
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If

End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSetupBins"
End Function
