VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmSetupBins 
   Caption         =   "Set Up Bins"
   ClientHeight    =   6825
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10980
   HelpContextID   =   17773917
   Icon            =   "imzmn001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6825
   ScaleWidth      =   10980
   Begin VB.Frame frmBatchTicket 
      Caption         =   "Batch Ticket Data"
      Height          =   4770
      Left            =   5520
      TabIndex        =   26
      Top             =   1560
      Width           =   5265
      Begin VB.TextBox txtComments 
         Height          =   1575
         Left            =   120
         MaxLength       =   5000
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   35
         Top             =   3120
         Width           =   4935
      End
      Begin VB.CheckBox chIsHorizontal 
         Alignment       =   1  'Right Justify
         Caption         =   "Tank is Horizontal"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   1440
         Width           =   2295
      End
      Begin NEWSOTALib.SOTANumber numDiameter 
         Height          =   315
         Left            =   1680
         TabIndex        =   28
         Top             =   300
         WhatsThisHelpID =   17773930
         Width           =   1605
         _Version        =   65536
         _ExtentX        =   2831
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#####"
         text            =   "       0.00000"
         sIntegralPlaces =   8
         sDecimalPlaces  =   5
      End
      Begin NEWSOTALib.SOTANumber numHeigth 
         Height          =   315
         Left            =   1680
         TabIndex        =   30
         Top             =   660
         WhatsThisHelpID =   17773930
         Width           =   1605
         _Version        =   65536
         _ExtentX        =   2831
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#####"
         text            =   "       0.00000"
         sIntegralPlaces =   8
         sDecimalPlaces  =   5
      End
      Begin NEWSOTALib.SOTANumber numLevelAmt 
         Height          =   315
         Left            =   1680
         TabIndex        =   32
         Top             =   1020
         WhatsThisHelpID =   17773930
         Width           =   1605
         _Version        =   65536
         _ExtentX        =   2831
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#####"
         text            =   "       0.00000"
         sIntegralPlaces =   8
         sDecimalPlaces  =   5
      End
      Begin VB.Label lblComments 
         Caption         =   "Comments:"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   2880
         Width           =   1095
      End
      Begin VB.Label lblLevelAmt 
         Caption         =   "Min Inches To Mixer:"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label lblHeigth 
         Caption         =   "Tank Heigth:"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label lblDiameter 
         Caption         =   "Tank Diameter:"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame fraInfo 
      Height          =   4770
      Left            =   45
      TabIndex        =   3
      Top             =   1560
      Width           =   5265
      Begin NEWSOTALib.SOTANumber numSortOrder 
         Height          =   315
         Left            =   2085
         TabIndex        =   18
         Top             =   3495
         WhatsThisHelpID =   17773918
         Width           =   1410
         _Version        =   65536
         _ExtentX        =   2487
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>###|,###|,###|,##<ILp0>#"
         text            =   "           0"
         sDecimalPlaces  =   0
      End
      Begin VB.CheckBox chkTempBin 
         Alignment       =   1  'Right Justify
         Caption         =   "Temporary"
         Height          =   195
         Left            =   195
         TabIndex        =   14
         Top             =   2430
         WhatsThisHelpID =   17773919
         Width           =   2085
      End
      Begin VB.CheckBox chkLockedForPutaway 
         Alignment       =   1  'Right Justify
         Caption         =   "Locked For Put Away"
         Height          =   195
         Left            =   195
         TabIndex        =   12
         Top             =   1800
         WhatsThisHelpID =   17773920
         Width           =   2085
      End
      Begin VB.CheckBox chkLockedForPick 
         Alignment       =   1  'Right Justify
         Caption         =   "Locked For Pick"
         Height          =   195
         Left            =   195
         TabIndex        =   13
         Top             =   2115
         WhatsThisHelpID =   17773921
         Width           =   2085
      End
      Begin VB.CheckBox chkExclFromAvail 
         Alignment       =   1  'Right Justify
         Caption         =   "Exclude From Available"
         Height          =   195
         Left            =   195
         TabIndex        =   15
         Top             =   2730
         WhatsThisHelpID =   17773922
         Width           =   2085
      End
      Begin EntryLookupControls.TextLookup lkuVolCapUnitMeasKey 
         Height          =   315
         Left            =   3700
         TabIndex        =   22
         Top             =   3915
         WhatsThisHelpID =   17773923
         Width           =   1400
         _ExtentX        =   2461
         _ExtentY        =   556
         ForeColor       =   -2147483640
         MaxLength       =   6
         WildCardChar    =   "?"
         IsSurrogateKey  =   -1  'True
         LookupID        =   "UnitOfMeasure"
         ParentIDColumn  =   "UnitMeasID"
         ParentKeyColumn =   "UnitMeasKey"
         ParentTable     =   "tciUnitMeasure"
         BoundColumn     =   "VolCapUnitMeasKey"
         BoundTable      =   "timWhseBin"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "UnitMeasID,,;"
      End
      Begin EntryLookupControls.TextLookup lkuWghtCapUnitMeasKey 
         Height          =   315
         Left            =   3700
         TabIndex        =   24
         Top             =   4305
         WhatsThisHelpID =   17773924
         Width           =   1400
         _ExtentX        =   2461
         _ExtentY        =   556
         ForeColor       =   -2147483640
         MaxLength       =   6
         WildCardChar    =   "?"
         IsSurrogateKey  =   -1  'True
         LookupID        =   "UnitOfMeasure"
         ParentIDColumn  =   "UnitMeasID"
         ParentKeyColumn =   "UnitMeasKey"
         ParentTable     =   "tciUnitMeasure"
         BoundColumn     =   "WghtCapUnitMeasKey"
         BoundTable      =   "timWhseBin"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "UnitMeasID,,;"
      End
      Begin EntryLookupControls.TextLookup lkuWhseZoneKey 
         Height          =   315
         Left            =   2085
         TabIndex        =   9
         Top             =   1035
         WhatsThisHelpID =   17773925
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         ForeColor       =   -2147483640
         MaxLength       =   15
         WildCardChar    =   "?"
         IsSurrogateKey  =   -1  'True
         LookupID        =   "WarehouseZone"
         ParentIDColumn  =   "WhseZoneID"
         ParentKeyColumn =   "WhseZoneKey"
         ParentTable     =   "timWhseZone"
         BoundColumn     =   "WhseZoneKey"
         BoundTable      =   "timWhseBin"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "WhseZoneID,,;Description,,;"
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtLocation 
         Height          =   315
         Left            =   2085
         TabIndex        =   7
         Top             =   645
         WhatsThisHelpID =   17773926
         Width           =   1500
         _Version        =   65536
         _ExtentX        =   2646
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         lMaxLength      =   10
      End
      Begin EntryLookupControls.TextLookup lkuPhysCountCycleKey 
         Height          =   315
         Left            =   2085
         TabIndex        =   17
         Top             =   3075
         WhatsThisHelpID =   17773927
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         ForeColor       =   -2147483640
         MaxLength       =   15
         WildCardChar    =   "?"
         IsSurrogateKey  =   -1  'True
         LookupID        =   "ProcCycle"
         ParentIDColumn  =   "ProcCycleID"
         ParentKeyColumn =   "ProcCycleKey"
         ParentTable     =   "tciProcCycle"
         BoundColumn     =   "PhysCountCycleKey"
         BoundTable      =   "timWhseBin"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "ProcCycleID,,;"
      End
      Begin SOTADropDownControl.SOTADropDown ddnStatus 
         Height          =   315
         Left            =   2085
         TabIndex        =   5
         Top             =   270
         WhatsThisHelpID =   17773928
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ddnStatus"
      End
      Begin SOTADropDownControl.SOTADropDown ddnType 
         Height          =   315
         Left            =   2085
         TabIndex        =   11
         Top             =   1425
         WhatsThisHelpID =   17773929
         Width           =   1500
         _ExtentX        =   2646
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ddnType"
      End
      Begin NEWSOTALib.SOTANumber numVolCapacity 
         Height          =   315
         Left            =   2085
         TabIndex        =   21
         Top             =   3900
         WhatsThisHelpID =   17773930
         Width           =   1600
         _Version        =   65536
         _ExtentX        =   2822
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
         text            =   "       0.0000000"
         sIntegralPlaces =   8
         sDecimalPlaces  =   7
      End
      Begin NEWSOTALib.SOTANumber numWghtCapacity 
         Height          =   315
         Left            =   2085
         TabIndex        =   25
         Top             =   4305
         WhatsThisHelpID =   17773931
         Width           =   1600
         _Version        =   65536
         _ExtentX        =   2822
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
         text            =   "       0.0000000"
         sIntegralPlaces =   8
         sDecimalPlaces  =   7
      End
      Begin VB.Label lblWghtCapacity 
         Caption         =   "Weight Capacity"
         Height          =   195
         Left            =   195
         TabIndex        =   23
         Top             =   4305
         Width           =   1500
      End
      Begin VB.Label lblVolCapacity 
         Caption         =   "Volume Capacity"
         Height          =   195
         Left            =   195
         TabIndex        =   20
         Top             =   3885
         Width           =   1500
      End
      Begin VB.Label lblType 
         Caption         =   "Type"
         Height          =   195
         Left            =   195
         TabIndex        =   10
         Top             =   1380
         Width           =   1035
      End
      Begin VB.Label lblStatus 
         Caption         =   "Status"
         Height          =   195
         Left            =   195
         TabIndex        =   4
         Top             =   330
         Width           =   1500
      End
      Begin VB.Label lblSortOrder 
         Caption         =   "Sort Order"
         Height          =   195
         Left            =   195
         TabIndex        =   19
         Top             =   3525
         Width           =   1230
      End
      Begin VB.Label lblCountCycle 
         Caption         =   "Count Cycle"
         Height          =   195
         Left            =   195
         TabIndex        =   16
         Top             =   3150
         Width           =   1095
      End
      Begin VB.Label lblLocation 
         Caption         =   "Location"
         Height          =   195
         Left            =   195
         TabIndex        =   6
         Top             =   675
         Width           =   975
      End
      Begin VB.Label lbZone 
         Caption         =   "Zone"
         Height          =   195
         Left            =   195
         TabIndex        =   8
         Top             =   1020
         Width           =   825
      End
      Begin VB.Label lblZoneDesc 
         Height          =   435
         Left            =   3645
         TabIndex        =   50
         Top             =   1005
         Width           =   1530
         WordWrap        =   -1  'True
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   39
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   38
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   45
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   44
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6435
      WhatsThisHelpID =   73
      Width           =   10980
      _ExtentX        =   19368
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10980
      _ExtentX        =   19368
      _ExtentY        =   741
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   41
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   46
      TabStop         =   0   'False
      Top             =   2880
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   47
      TabStop         =   0   'False
      Top             =   720
      WhatsThisHelpID =   70
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin EntryLookupControls.TextLookup lkuWhseBinID 
      Height          =   315
      Left            =   555
      TabIndex        =   2
      Top             =   990
      WhatsThisHelpID =   17773944
      Width           =   2115
      _ExtentX        =   3731
      _ExtentY        =   556
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   15
      WildCardChar    =   "?"
      LookupID        =   "Bin"
      BoundColumn     =   "WhseBinID"
      BoundTable      =   "timWhseBin"
      sSQLReturnCols  =   "WhseBinID,,;WhseBinKey,,;"
   End
   Begin EntryLookupControls.TextLookup lkuWhseKey 
      Height          =   315
      Left            =   1140
      TabIndex        =   1
      Top             =   615
      WhatsThisHelpID =   17773945
      Width           =   1500
      _ExtentX        =   2646
      _ExtentY        =   556
      ForeColor       =   -2147483640
      MaxLength       =   6
      WildCardChar    =   "?"
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Warehouse"
      ParentIDColumn  =   "WhseID"
      ParentKeyColumn =   "WhseKey"
      ParentTable     =   "timWarehouse"
      BoundColumn     =   "WhseKey"
      BoundTable      =   "timWhseBin"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "WhseID,,;Description,,;"
   End
   Begin VB.Label lblWhseDesc 
      Height          =   630
      Left            =   2760
      TabIndex        =   51
      Top             =   615
      Width           =   2550
   End
   Begin VB.Label lblWhseKey 
      Caption         =   "Warehouse"
      Height          =   315
      Left            =   105
      TabIndex        =   49
      Top             =   675
      Width           =   1500
   End
   Begin VB.Label lblWhseBinID 
      Caption         =   " Bin"
      Height          =   255
      Left            =   105
      TabIndex        =   48
      Top             =   1065
      Width           =   1140
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   36
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmSetupBins"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmSetupBins
'     Desc: Set Up Bins
' Original: 11-20-2003
'     Mods:
'************************************************************************************
Option Explicit

Public moFormCust As Object

Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Public moSotaObjects        As New Collection

Private miFilter            As Integer

Private moContextMenu        As clsContextMenu   '-- Right Click Context Menu Class

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

Private msUserID            As String

Private mlLanguage          As Long

Private msLookupRestrict       As String

Private miSecurityLevel     As Integer

Private miOldFormHeight As Long

Private miMinFormHeight As Long

Private miOldFormWidth As Long

Private miMinFormWidth As Long

Private mbTrackQtyAtBin As Boolean

Private mbDfltBin As Boolean

Private mbUseBins As Boolean

Private mbUseZones As Boolean

Private mbMsgDispl As Boolean

Private mbWMIsLicensed     As Boolean

'Status Constants
Private Const kBinStatActive = 1       'Active
Private Const kBinStatInactive = 2     'Inactive
Private Const kBinStatDeleted = 3      'Deleted
'Type Constants
Private Const kBinTypeFixed = 1        'Fixed
Private Const kBinTypeRandom = 2       'Random
Private Const kBinTypeStaging = 3      'Staging Area

Private WithEvents moDmForm            As clsDmForm
Attribute moDmForm.VB_VarHelpID = -1
Private WithEvents moDmFormExt            As clsDmForm  'RKL DEJ 2017-05-10
Attribute moDmFormExt.VB_VarHelpID = -1

Private Const ktskBinListing = 118096002

Const VBRIG_MODULE_ID_STRING = "frmSetupBins.FRM"


Private mbLoadSuccess As Boolean


Private Sub txtLocation_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLocation, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLocation_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLocation_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLocation, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLocation_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLocation_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLocation, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLocation_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLocation_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLocation, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLocation_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVolCapUnitMeasKey_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuVolCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVolCapUnitMeasKey_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuVolCapUnitMeasKey, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVolCapUnitMeasKey_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuVolCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVolCapUnitMeasKey_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuVolCapUnitMeasKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVolCapUnitMeasKey_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuVolCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWghtCapUnitMeasKey_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWghtCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWghtCapUnitMeasKey_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWghtCapUnitMeasKey, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWghtCapUnitMeasKey_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWghtCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWghtCapUnitMeasKey_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWghtCapUnitMeasKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWghtCapUnitMeasKey_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWghtCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseZoneKey_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhseZoneKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseZoneKey_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseZoneKey_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhseZoneKey, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseZoneKey_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseZoneKey_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhseZoneKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseZoneKey_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseZoneKey_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhseZoneKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseZoneKey_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseZoneKey_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhseZoneKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseZoneKey_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhysCountCycleKey_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPhysCountCycleKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhysCountCycleKey_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPhysCountCycleKey, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhysCountCycleKey_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPhysCountCycleKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhysCountCycleKey_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPhysCountCycleKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseBinID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhseBinID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseBinID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseBinID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhseBinID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseBinID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseBinID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhseBinID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseBinID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseBinID_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhseBinID, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseBinID_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseBinID_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhseBinID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseBinID_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseKey_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhseKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseKey_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseKey_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhseKey, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseKey_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseKey_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhseKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseKey_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseKey_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhseKey, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseKey_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub numSortOrder_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numSortOrder, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numSortOrder_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numSortOrder_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numSortOrder, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numSortOrder_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numSortOrder_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numSortOrder, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numSortOrder_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numVolCapacity_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numVolCapacity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numVolCapacity_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numVolCapacity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numVolCapacity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numVolCapacity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numVolCapacity_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numVolCapacity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numVolCapacity_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numWghtCapacity_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numWghtCapacity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numWghtCapacity_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numWghtCapacity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numWghtCapacity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numWghtCapacity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numWghtCapacity_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numWghtCapacity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numWghtCapacity_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkTempBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkTempBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkTempBin_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkTempBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkTempBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkTempBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkTempBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkTempBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkTempBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkLockedForPutaway_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkLockedForPutaway, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkLockedForPutaway_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkLockedForPutaway_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkLockedForPutaway, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkLockedForPutaway_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkLockedForPutaway_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkLockedForPutaway, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkLockedForPutaway_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkLockedForPick_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkLockedForPick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkLockedForPick_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkLockedForPick_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkLockedForPick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkLockedForPick_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkLockedForPick_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkLockedForPick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkLockedForPick_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkExclFromAvail_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkExclFromAvail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkExclFromAvail_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkExclFromAvail_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkExclFromAvail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkExclFromAvail_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkExclFromAvail_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkExclFromAvail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkExclFromAvail_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnStatus, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnStatus, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnType, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnType_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnType, True
    #End If
'+++ End Customizer Code Push +++
ddnType.Tag = ddnType.Text
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnType_GotFocus", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        'Resize Height
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraInfo
          
        'Resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraInfo, lblZoneDesc
               
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
        
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPhysCountCycleKey_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPhysCountCycleKey, True
    #End If
'+++ End Customizer Code Push +++
lkuPhysCountCycleKey.Tag = lkuPhysCountCycleKey.Text
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_GotFocus", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPhysCountCycleKey_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPhysCountCycleKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
lkuPhysCountCycleKey.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_LookupClick", VBRIG_IS_FORM       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPhysCountCycleKey_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPhysCountCycleKey, True
    #End If
'+++ End Customizer Code Push +++
If Me.ActiveControl.Name = lkuPhysCountCycleKey.Name Then
    If Not lkuPhysCountCycleKey.IsValid Then
        If gvCheckNull(moClass.moAppDB.Lookup("ProcCycleKey", "tciProcCycle", "tciProcCycle.ProcCycleID = " & gsQuoted(lkuPhysCountCycleKey.Text) _
                        & " And CompanyID = " & gsQuoted(msCompanyID)), 0) <> 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblCountCycle
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, gsStripChar(lblCountCycle, "&"), ":" & lkuPhysCountCycleKey.Text
        End If

        lkuPhysCountCycleKey.Text = lkuPhysCountCycleKey.Tag

        If lkuPhysCountCycleKey.Enabled Then
            lkuPhysCountCycleKey.SetFocus
        End If

     End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPhysCountCycleKey_LostFocus", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuVolCapUnitMeasKey_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lKey As Long
If lkuVolCapUnitMeasKey.Text <> lkuVolCapUnitMeasKey.Tag Then
    lKey = glGetValidLong(moClass.moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure", _
                "CompanyID = " & gsQuoted(msCompanyID) & " AND UnitMeasID = " & _
                gsQuoted(lkuVolCapUnitMeasKey.Text) & " AND MeasType = 2"))
    If lKey <> 0 Then
        lkuVolCapUnitMeasKey.KeyValue = lKey
    Else
        iReturn = EL_CTRL_VALID
        Exit Sub
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_BeforeValidate", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuVolCapUnitMeasKey_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuVolCapUnitMeasKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
lkuVolCapUnitMeasKey.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND MeasType = 2"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_LookupClick", VBRIG_IS_FORM       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuVolCapUnitMeasKey_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuVolCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++
Dim lKey As Long
    If Len(Trim(lkuVolCapUnitMeasKey)) <> 0 Then
        lKey = glGetValidLong(moClass.moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure", _
                "CompanyID = " & gsQuoted(msCompanyID) & " AND UnitMeasID = " & _
                gsQuoted(lkuVolCapUnitMeasKey.Text) & " AND MeasType = 2"))
        If lKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, "Unit of Measure", ":" & lkuVolCapUnitMeasKey.Text
            lkuVolCapUnitMeasKey.Text = lkuVolCapUnitMeasKey.Tag

            If lkuVolCapUnitMeasKey.Enabled Then
                lkuVolCapUnitMeasKey.SetFocus
            End If
        Else
            lkuVolCapUnitMeasKey.KeyValue = lKey
        End If
        
     End If


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuVolCapUnitMeasKey_LostFocus", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWghtCapUnitMeasKey_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lKey As Long
If lkuWghtCapUnitMeasKey.Text <> lkuWghtCapUnitMeasKey.Tag Then
    lKey = glGetValidLong(moClass.moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure", _
                "CompanyID = " & gsQuoted(msCompanyID) & " AND UnitMeasID = " & _
                gsQuoted(lkuWghtCapUnitMeasKey.Text) & " AND MeasType = 3"))
    If lKey <> 0 Then
        lkuWghtCapUnitMeasKey.KeyValue = lKey
    Else
        iReturn = EL_CTRL_VALID
    Exit Sub
    End If
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_BeforeValidate", VBRIG_IS_FORM   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWghtCapUnitMeasKey_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWghtCapUnitMeasKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
lkuWghtCapUnitMeasKey.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND MeasType = 3"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_LookupClick", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWghtCapUnitMeasKey_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWghtCapUnitMeasKey, True
    #End If
'+++ End Customizer Code Push +++
Dim lKey As Long
If Me.ActiveControl.Name = lkuWghtCapUnitMeasKey.Name Then
    If numVolCapacity > 0 Then
        lKey = glGetValidLong(moClass.moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure", _
                "CompanyID = " & gsQuoted(msCompanyID) & " AND UnitMeasID = " & _
                gsQuoted(lkuWghtCapUnitMeasKey.Text) & " AND MeasType = 3"))
        If lKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, "Unit of Measure", ":" & lkuWghtCapUnitMeasKey.Text
            lkuWghtCapUnitMeasKey.Text = lkuWghtCapUnitMeasKey.Tag

            If lkuWghtCapUnitMeasKey.Enabled Then
                lkuWghtCapUnitMeasKey.SetFocus
            End If
        Else
            lkuWghtCapUnitMeasKey.KeyValue = lKey
        End If

     End If
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWghtCapUnitMeasKey_LostFocus", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseBinID_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhseBinID, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = Not bConfirmUnload(0, True)
    If Len(Trim(lkuWhseKey.Text)) <> 0 Then
        lkuWhseBinID.RestrictClause = " WhseKey = " & glGetValidLong(lkuWhseKey.KeyValue)
    Else
        lkuWhseBinID.RestrictClause = "WhseKey IN (SELECT WhseKey FROM timWarehouse WITH (NOLOCK)" _
                                  & " WHERE timWarehouse.CompanyID = " & gsQuoted(msCompanyID) _
                                  & " AND timWarehouse.UseBins = 1)"
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseBinID_LookupClick", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseBinID_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhseBinID, True
    #End If
'+++ End Customizer Code Push +++

    If glGetValidLong(lkuWhseBinID.ReturnColumnValues.Count) <> 0 Then
        lkuWhseBinID.KeyValue = glGetValidLong(lkuWhseBinID.ReturnColumnValues("WhseBinKey"))
        lkuWhseKey.KeyValue = glGetValidLong(moClass.moAppDB.Lookup("WhseKey", "timWhseBin", "WhseBinKey = " & glGetValidLong(lkuWhseBinID.ReturnColumnValues("WhseBinKey"))))
    Else
        If glGetValidLong(lkuWhseBinID.KeyValue) = 0 Then
            If lkuWhseKey.KeyValue <> 0 And Len(Trim(lkuWhseBinID)) <> 0 Then
                lkuWhseBinID.KeyValue = glGetValidLong(moClass.moAppDB.Lookup("WhseBinKey", "timWhseBin", _
                    "WhseKey = " & lkuWhseKey.KeyValue & " AND WhseBinID = " & gsQuoted(lkuWhseBinID.Text)))
            End If
        End If
    End If
   
    If Len(Trim(lkuWhseBinID.Text)) <> 0 And Len(Trim(lkuWhseKey.Text)) <> 0 Then
        valMgr_KeyChange
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseBinID_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuWhseKey_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhseKey, True
    #End If
'+++ End Customizer Code Push +++
lkuWhseKey.Tag = lkuWhseKey.Text
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseKey_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseKey_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhseKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
lkuWhseKey.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND Transit = 0 AND UseBins = 1"
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseKey_LookupClick", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseKey_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhseKey, True
    #End If
'+++ End Customizer Code Push +++
    Dim sID As String
    If Len(Trim$(lkuWhseKey.Text)) = 0 Then
        lkuWhseKey.Tag = lkuWhseKey.Text
        lblWhseDesc.Caption = ""
        'Reset the restrict clause to the least restrictive allowed so if the user hits the main navigator
        'all bins for all the warehouses will show.
        lkuWhseBinID.RestrictClause = "WhseKey IN (SELECT WhseKey FROM timWarehouse WITH (NOLOCK)" _
                                  & " WHERE timWarehouse.CompanyID = " & gsQuoted(msCompanyID) _
                                  & " AND timWarehouse.UseBins = 1)"

        Exit Sub
    End If
    
    If Me.ActiveControl.Name = lkuWhseBinID.Name Then
        If Not lkuWhseKey.IsValid Then
            If gvCheckNull(moClass.moAppDB.Lookup("WhseKey", "timWarehouse", "timWarehouse.WhseID = " & gsQuoted(lkuWhseKey.Text) & " And CompanyID = " & gsQuoted(msCompanyID)), 0) <> 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgInvWhouse, lkuWhseKey.Text
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, gsStripChar(lblWhseKey, "&"), ":" & lkuWhseKey.Text
            End If

            lkuWhseKey.Text = lkuWhseKey.Tag
            

            If lkuWhseKey.Enabled Then
                lkuWhseKey.SetFocus
            End If

            Exit Sub
        Else
                'For Case insensitive database pick exact ID ( As defined by user)
                sID = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseID = " & gsQuoted(Trim$(lkuWhseKey.Text)) & " AND CompanyID = " & gsQuoted(msCompanyID))
                lkuWhseKey.Text = sID
                lblWhseDesc.Caption = gvCheckNull(moClass.moAppDB.Lookup("Description", "timWarehouse", "WhseKey = " & Format$(lkuWhseKey.KeyValue)))
                
                'Because a Warehouse has been selected, restrict the bins by warehouse.
                'In order to get all the bins for the all the warehouses, the user would have to blank out the warehouse text.
                lkuWhseBinID.RestrictClause = "WhseKey = " & lkuWhseKey.KeyValue
    
                'Temporary Bin should be allowed to be created
                'only in Warehouse where TrackQtyAtBin is yes
                mbTrackQtyAtBin = gbGetValidBoolean(moClass.moAppDB.Lookup _
                        ("TrackQtyAtBin", "timWarehouse", "CompanyID = " & _
                        gsQuoted(msCompanyID) & " AND WhseKey = " & lkuWhseKey.KeyValue))
                If Not mbTrackQtyAtBin Then
                    chkTempBin.Enabled = False
                End If
                mbUseZones = gbGetValidBoolean(moClass.moAppDB.Lookup _
                            ("UseZones", "timWarehouse", "CompanyID = " & _
                            gsQuoted(msCompanyID) & " AND WhseKey = " & lkuWhseKey.KeyValue))
                If Not mbUseZones Then
                    lkuWhseZoneKey.Enabled = False
                Else
                    lkuWhseZoneKey.Enabled = mbWMIsLicensed
                End If
                        
                valMgr_KeyChange
                
                Exit Sub
            End If
            
        End If
    
    
    If Not lkuWhseKey.IsValid Then
        lkuWhseKey.Text = lkuWhseKey.Tag
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseKey_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseZoneKey_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lKey As Long
If lkuWhseZoneKey.Text <> lkuWhseZoneKey.Tag Then
    lKey = glGetValidLong(moClass.moAppDB.Lookup("WhseZoneKey", "timWhseZone", _
                 "WhseKey = " & lkuWhseKey.KeyValue & " AND WhseZoneID = " & gsQuoted(lkuWhseZoneKey.Text)))
        If lKey <> 0 Then
            lkuWhseZoneKey.KeyValue = lKey
        Else
            iReturn = EL_CTRL_VALID
            Exit Sub
        End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseZoneKey_BeforeValidate", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseZoneKey_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhseZoneKey, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
lkuWhseZoneKey.RestrictClause = "WhseKey = " & lkuWhseKey.KeyValue


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseZoneKey_LookupClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseZoneKey_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhseZoneKey, True
    #End If
'+++ End Customizer Code Push +++
Dim lKey As Long
    If Len(Trim(lkuWhseZoneKey)) <> 0 Then
        lKey = glGetValidLong(moClass.moAppDB.Lookup("WhseZoneKey", "timWhseZone", _
                 "WhseKey = " & lkuWhseKey.KeyValue & " AND WhseZoneID = " & gsQuoted(lkuWhseZoneKey.Text)))
        If lKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, lbZone, ":" & lkuWhseZoneKey.Text
            lkuWhseZoneKey.Text = lkuWhseZoneKey.Tag
            If lkuWhseZoneKey.Enabled Then
                lkuWhseZoneKey.SetFocus
                lblZoneDesc = ""
            End If
        Else
            lkuWhseZoneKey.KeyValue = lKey
'            If lblZoneDesc = "" Then
                lblZoneDesc = gsGetValidStr(moClass.moAppDB.Lookup _
                        ("Description", "timWhseZone", "WhseZoneKey = " & lKey))
'            End If
        End If
     End If


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseZoneKey_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMBeforeDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lWhseBinKey As Long
    bValid = False
    
    If (Not mbTrackQtyAtBin And mbUseBins And mbDfltBin) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMDfltBinExist
        Exit Sub
    End If
    
    If ddnStatus.ItemData <> kBinStatDeleted Then
        'If this bin is currently used as a preferred bin for inventory in this warehouse and may not be deleted
        If bBinIsPreferredBin(lkuWhseBinID.KeyValue) Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgPrefBin
            ddnStatus.ItemData = kBinStatActive
            Exit Sub
        End If
        
        'If Qty is greater than zero, we cannot delete the bin
        If bBinHasQtyOnHand(lkuWhseBinID.KeyValue) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMQtyExist
            ddnStatus.ItemData = kBinStatActive
            Exit Sub
        End If
        
        'If it is a pending transaction for this bin, we cannot delete the bin
        If bBinHasPendingTrans(lkuWhseBinID.KeyValue) Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgBinPendTran
            ddnStatus.ItemData = kBinStatActive
            Exit Sub
        End If
    End If

    'distributions referenced this bin on posted transactions (has historical activity)
    If bHasHistTrans(lkuWhseBinID.KeyValue) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMLogicalBinDel
        ddnStatus.ItemData = kBinStatDeleted
        Exit Sub
    End If
    
bValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeDelete", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbTrackQtyAtBin = gbGetValidBoolean(moClass.moAppDB.Lookup _
                        ("TrackQtyAtBin", "timWarehouse", "CompanyID = " & _
                        gsQuoted(msCompanyID) & " AND WhseKey = " & lkuWhseKey.KeyValue))
    mbUseBins = gbGetValidBoolean(moClass.moAppDB.Lookup _
                        ("UseBins", "timWarehouse", "CompanyID = " & _
                        gsQuoted(msCompanyID) & " AND WhseKey = " & lkuWhseKey.KeyValue))
    mbUseZones = gbGetValidBoolean(moClass.moAppDB.Lookup _
                        ("UseZones", "timWarehouse", "CompanyID = " & _
                        gsQuoted(msCompanyID) & " AND WhseKey = " & lkuWhseKey.KeyValue))
    If lkuWhseBinID.Text <> "" Then
        lkuWhseBinID.KeyValue = glGetValidLong(moClass.moAppDB.Lookup("WhseBinKey", "timWhseBin", _
                                 "WhseBinID = " & gsQuoted(lkuWhseBinID.Text) & " AND WhseKey = " & lkuWhseKey.KeyValue))
        mbDfltBin = gbGetValidBoolean(moClass.moAppDB.Lookup _
                        ("DfltBin", "timWhseBin", "WhseBinKey = " & lkuWhseBinID.KeyValue))
    End If
    If (Not mbTrackQtyAtBin And mbUseBins And mbDfltBin) Then
        DisableControls
    Else
        EnableControls
    End If
    'Temporary Bin should be allowed to be created
    'only in Warehouse where TrackQtyAtBin is yes
    If Not mbTrackQtyAtBin Then
        chkTempBin.Enabled = False
    End If
    If Not mbUseZones Then
        lkuWhseZoneKey.Enabled = False
    Else
        lkuWhseZoneKey.Enabled = mbWMIsLicensed
    End If
 
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMDataDisplayed", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMStateChange(iOldState As Integer, iNewState As Integer)
    If iNewState = kDmStateAdd Or iNewState = kDmStateNone Then ' Reset the Warehouse Bin ID field.
        lkuWhseBinID.KeyValue = 0
    End If
                
End Sub

Private Sub numSortOrder_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numSortOrder, True
    #End If
'+++ End Customizer Code Push +++
If numSortOrder.Value < 0 Then
    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblSortOrder
    gbSetFocus Me, numSortOrder
    numSortOrder = 0
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "numSortOrder_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub numVolCapacity_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numVolCapacity, True
    #End If
'+++ End Customizer Code Push +++
Dim lKey As Long
If numVolCapacity.Value < 0 Then
    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblVolCapacity
    gbSetFocus Me, numVolCapacity
    numVolCapacity = 0
End If
If numVolCapacity.Value > 0 Then
    If Len(Trim(lkuVolCapUnitMeasKey.Text)) = 0 Then
        lKey = glGetValidLong(moClass.moAppDB.Lookup("VolumeUnitMeasKey", "timOptions", _
                "CompanyID = " & gsQuoted(msCompanyID)))
        lkuVolCapUnitMeasKey.Text = gsGetValidStr(moClass.moAppDB.Lookup("UnitMeasID", "tciUnitMeasure", _
                "CompanyID = " & gsQuoted(msCompanyID) & " AND UnitMeasKey = " & _
                glGetValidLong(lKey)))
    End If
End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "numVolCapacity_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub numWghtCapacity_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numWghtCapacity, True
    #End If
'+++ End Customizer Code Push +++
Dim lKey As Long
If numWghtCapacity.Value < 0 Then
    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblWghtCapacity
    gbSetFocus Me, numWghtCapacity
    numWghtCapacity = 0
End If
If numWghtCapacity.Value > 0 Then
    If Len(Trim(lkuWghtCapUnitMeasKey.Text)) = 0 Then
        lKey = glGetValidLong(moClass.moAppDB.Lookup("WeightUnitMeasKey", "timOptions", _
                "CompanyID = " & gsQuoted(msCompanyID)))
        lkuWghtCapUnitMeasKey.Text = gsGetValidStr(moClass.moAppDB.Lookup("UnitMeasID", "tciUnitMeasure", _
                "CompanyID = " & gsQuoted(msCompanyID) & " AND UnitMeasKey = " & _
                glGetValidLong(lKey)))
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "numWghtCapacity_LostFocus", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picdrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picdrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmSetupBins"
End Function

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************
' Description:
'    This routine will bind Right-Click context menus to
'    the form and controls.
'*******************************************************

    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmSetupBins
        .Bind "IMWHSE", lkuWhseKey.hwnd, kEntTypeIMWarehouse
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
' Description:
'    This routine will bind fields on the form to fields in the database.
'************************************************************************
    '-- create a new data Manager Form class
    Set moDmForm = New clsDmForm
    
    With moDmForm
        Set .Form = frmSetupBins
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        

        .AppName = Me.Caption
        .Table = "timWhseBin"
        
        Set .ValidationMgr = valMgr

        .OrderBy = "WhseKey, WhseBinID"
        .UniqueKey = "WhseKey, WhseBinID"
        .Bind lkuWhseBinID, "WhseBinID", SQL_CHAR
        .Bind Nothing, "WhseBinKey", SQL_INTEGER
        .Bind lkuWhseZoneKey, "WhseZoneKey", SQL_INTEGER, kDmSetNull
        .Bind chkExclFromAvail, "ExclFromAvail", SQL_INTEGER
'        .Bind lkuImportLogKey, "ImportLogKey", SQL_INTEGER, kDmSetNull
        .Bind txtLocation, "Location", SQL_CHAR, kDmSetNull
        .Bind chkLockedForPick, "LockedForPick", SQL_INTEGER
        .Bind chkLockedForPutaway, "LockedForPutaway", SQL_INTEGER
        .Bind lkuPhysCountCycleKey, "PhysCountCycleKey", SQL_INTEGER, kDmSetNull
        .Bind numSortOrder, "SortOrder", SQL_INTEGER
        .Bind ddnStatus, "Status", SQL_INTEGER, kDmUseItemData
        .Bind chkTempBin, "TempBin", SQL_INTEGER
        .Bind ddnType, "Type", SQL_INTEGER, kDmUseItemData + kDmSetNull
        .Bind numVolCapacity, "VolCapacity", SQL_DECIMAL, kDmSetNull
        .Bind lkuVolCapUnitMeasKey, "VolCapUnitMeasKey", SQL_INTEGER, kDmSetNull
        .Bind numWghtCapacity, "WghtCapacity", SQL_DECIMAL, kDmSetNull
        .Bind lkuWghtCapUnitMeasKey, "WghtCapUnitMeasKey", SQL_INTEGER, kDmSetNull
        .Bind lkuWhseKey, "WhseKey", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        
        .LinkSource "timWarehouse", "WhseKey=<<WhseKey>>"
        .Link lblWhseDesc, "Description"

        .LinkSource "timWhseZone", "WhseZoneKey=<<WhseZoneKey>>"
        .Link lblZoneDesc, "Description"
        
        .Init
    End With
    
    '*****************************************************************
    'RKL DEJ 2017-05-10 (START)
    '*****************************************************************
    Set moDmFormExt = New clsDmForm
    
    With moDmFormExt
        Set .Form = frmSetupBins
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .AppName = Me.Caption
        .Table = "timWhseBinExt_RKL"
        .UniqueKey = "WhseBinKey"
        Set .Parent = moDmForm
        Set .SOTAStatusBar = sbrMain
        Set .Toolbar = tbrMain

'        .Bind Nothing, "WhseBinKey", SQL_INTEGER, kDmSetNull
        .Bind txtComments, "Comments", SQL_VARCHAR, kDmSetNull
        .Bind chIsHorizontal, "TankIsHorizontal", SQL_INTEGER, kDmSetNull
        .Bind numDiameter, "Diameter", SQL_DECIMAL, kDmSetNull
        .Bind numHeigth, "Height", SQL_DECIMAL, kDmSetNull
        .Bind numLevelAmt, "LevelAmt", SQL_DECIMAL, kDmSetNull

        .ParentLink "WhseBinKey", "WhseBinKey", SQL_INTEGER

        .Init
    End With
    '*****************************************************************
    'RKL DEJ 2017-05-10 (STOP)
    '*****************************************************************

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim iActionCode As Integer
    Dim iConfirmUnload As Integer
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant
 
    iActionCode = kDmFailure
    
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            If Not valMgr.ValidateForm Then Exit Sub
            moDmForm.Action (kDmFinish)
            EnableControls
            gbSetFocus Me, lkuWhseKey
        
        Case kTbSave
            If Not valMgr.ValidateForm Then Exit Sub
            moDmForm.Save (True)

        Case kTbCancel, kTbCancelExit
            iActionCode = moDmForm.Action(kDmCancel)
            EnableControls
            gbSetFocus Me, lkuWhseKey
        
        Case kTbDelete
            iActionCode = moDmForm.Action(kDmDelete)
            If iActionCode = kDmSuccess Then
                EnableControls
            End If
            gbSetFocus Me, lkuWhseKey

        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'iActionCode = moDmForm.CopyFrom

        Case kTbRenameId
            moDmForm.RenameID
            
        Case kTbPrint
            If moDmForm.ConfirmUnload(True) Then                 'Have user save changes
                gPrintTask moClass.moFramework, ktskBinListing
            End If
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmForm, moClass
            
    End Select

    Select Case iActionCode
        Case kDmSuccess
            moClass.lUIActive = kChildObjectInactive
            
        Case kDmFailure, kDmError
            Exit Sub
             
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
' Description:
'    Form_KeyPress traps the keys pressed when the KeyPreview property
'    of the form is set to True.
'*********************************************************************
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
    
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- assign system object properties to module-level variables
    mbLoadSuccess = False
    msCompanyID = moClass.moSysSession.CompanyId
    msUserID = moClass.moSysSession.UserId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mlLanguage = moClass.moSysSession.Language
        
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    '--Set the form caption
    Me.Caption = "Set Up Bins"

'-- setup and bind right click context menus to controls
    BindContextMenu
    '-- setup navigator restrict clause

    msLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)
   
    If CBool(moClass.moSysSession.IsModuleActivated(kModuleWM)) = True Then
        mbWMIsLicensed = True
    Else
        mbWMIsLicensed = False
    End If
   
    SetupLookups
    SetupDropDowns
    
    '-- bind form fields to database fields
    BindForm
    
    '--adjust the tool bar
    With tbrMain
        .RemoveButton kTbMemo
        .RemoveButton kTbRenameId
        .RemoveSeparator kTbPrint
        .LocaleID = mlLanguage
    End With
    

    chkLockedForPutaway.Enabled = mbWMIsLicensed
    chkLockedForPick.Enabled = mbWMIsLicensed
    chkExclFromAvail.Enabled = mbWMIsLicensed
    lkuWhseZoneKey.Enabled = mbWMIsLicensed

    
    'status bar settings
    sbrMain.MessageVisible = False
    Set sbrMain.Framework = oClass.moFramework
    Set moDmForm.SOTAStatusBar = sbrMain

    '-- initialize security level
    'RKl DEJ 2017-05-10 Added moDmFormExt to security
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm, moDmFormExt)
    
    '-- initialize browse filter tracking
    miFilter = RSID_UNFILTERED

    
    moDmForm.SetUIStatusNone
    mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
   
    '-- reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then

        '-- if the form is dirty, prompt the user to save the record
        bConfirmUnload iConfirmUnload, True
        
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                
            Case Else
                giSotaMsgBox Nothing, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
                
        End Select
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlRunMode
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
        End Select
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    
    'RKL DEJ 2017-05-10 (START)
    If Not moDmFormExt Is Nothing Then
        moDmFormExt.UnloadSelf
        Set moDmFormExt = Nothing
    End If
    'RKL DEJ 2017-05-10 (STOP)
    
    If Not moDmForm Is Nothing Then
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If

    
    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me

    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1

   TerminateControls Me
    ddnType.Terminate

    ddnStatus.Terminate


   '-- If this form loads any other modal objects
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMBeforeInsert(bValid As Boolean)
'********************************************************************
' Description:
'    This routine will be called by Data Manager just before an
'    Insert. We need to get the next surrogate key for the table.
'********************************************************************
    Dim lKey As Long

    On Error GoTo CancelInsert

    bValid = True


    With moClass.moAppDB
        .SetInParam "timWhseBin"
        .SetOutParam lKey
        .ExecuteSP ("spGetNextSurrogateKey")
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With

    moDmForm.SetColumnValue "WhseBinKey", lKey
    bValid = True

    Exit Sub
CancelInsert:
    bValid = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeInsert", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMValidate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sList As String
    Dim lWhseBinKey As Long
    Dim iResponse As Integer
    Dim sSQL As String
    Dim sMsgList As String

    bValid = False

    '------------------------------------------------------------------------
    'A bin cannot be a random bin if the warehouse does not track qty at bin.
    '------------------------------------------------------------------------
    
    If ddnType.ItemData = kBinTypeRandom Then
        If (Not mbTrackQtyAtBin) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMNoTrkBinQtyDisallwRandom
            Exit Sub
        End If
    End If

    'If changing to a random bin, verify the following:
    If ddnType.Text <> ddnType.Tag And moDmForm.State = kDmStateEdit Then
        
        If ddnType.ItemData = kBinTypeRandom Then
            'Check to see if it references more than one item.  If so, restore and notify.
            If (moClass.moAppDB.Lookup("COUNT(ItemKey)", "timWhseBinInvt", "WhseBinKey = " & glGetValidLong(lkuWhseBinID.KeyValue) & _
                " AND ((PendQtyDecrease <> 0) Or (QtyOnHand <> 0) Or (PendQtyIncrease <> 0))")) > 1 Then
                
                sSQL = "SELECT i.ItemID as [ListingValue]" & _
                       " FROM timWhseBinInvt wbi WITH (NOLOCK)" & _
                       " JOIN timItem i WITH (NOLOCK) ON wbi.ItemKey = i.ItemKey" & _
                       " WHERE wbi.WhseBinKey = " & lkuWhseBinID.KeyValue & _
                       " AND (wbi.PendQtyDecrease <> 0 Or wbi.QtyOnHand <> 0 Or wbi.PendQtyIncrease <> 0)"
                       
                sMsgList = gsGetSqlResultList(oClass, sSQL)
                
                'Random bins can only be associated with one item at a time.  This bin currently stocks the following items.{0}
                giSotaMsgBox Me, moClass.moSysSession, kmsgRandomBinValStocksMultiItm, sMsgList
                ddnType.Text = ddnType.Tag
                Exit Sub
                
             End If
        
            'Check to see if it is set as a preferred bin.  If so, restore and notify.
            If bBinIsPreferredBin(glGetValidLong(lkuWhseBinID.KeyValue)) Then
                
                sSQL = "SELECT i.ItemID as [ListingValue]" & _
                       " FROM timInvtBinList ibl WITH (NOLOCK)" & _
                       " JOIN timItem i WITH (NOLOCK) ON ibl.ItemKey = i.ItemKey" & _
                       " WHERE ibl.WhseBinKey = " & lkuWhseBinID.KeyValue
                       
                sMsgList = gsGetSqlResultList(oClass, sSQL)
                
                'Random bins cannot be a preferred bin.  This bin is set as a preferred bin for the following items.{0}
                giSotaMsgBox Me, moClass.moSysSession, kmsgRandomBinValUsedAsPrefBin, sMsgList
                ddnType.Text = ddnType.Tag
                Exit Sub
            End If
        End If
        
        ddnType.Tag = ddnType.Text
    End If
        
    '-----------------------------------------------------
    'Validations based upon the setting of the bin status.
    '-----------------------------------------------------
    lWhseBinKey = lkuWhseBinID.KeyValue
    
    If ddnStatus.ItemData = kBinStatInactive Then
    
        If bBinIsPreferredBin(lWhseBinKey) Then
            iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgIMBinIsPreferredBin)
            If iResponse <> vbYes Then
                Exit Sub
            End If
        End If
    
        If bBinHasQtyOnHand(lWhseBinKey) Then
            iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgIMBinHasQtyOnHand)
            If iResponse <> vbYes Then
                Exit Sub
            End If
        End If
            
        If bBinHasPendingTrans(lWhseBinKey) Then
            iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgIMBinUsedInPendingTran)
            If iResponse <> vbYes Then
                Exit Sub
            End If
        End If
        
        If bBinIsBeingUsedAsRoutingBackflushBin(lWhseBinKey, sList) Then
            iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgIMBinIsRoutingBflushBin)
            If iResponse <> vbYes Then
                Exit Sub
            End If
        End If
        
        If bBinIsBeingUsedAsWorkOrderBackflushBin(lWhseBinKey, sList) Then
            iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgIMBinIsWorkOrdBflushBin)
            If iResponse <> vbYes Then
                Exit Sub
            End If
        End If
            
    ElseIf ddnStatus.ItemData = kBinStatDeleted Then
    
        'If this bin is currently used as a preferred bin for inventory in this warehouse and may not be deleted
        If bBinIsPreferredBin(lWhseBinKey) Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgPrefBin
            ddnStatus.ItemData = kBinStatActive
            Exit Sub
        End If
        
        'If Qty is greater than zero, we cannot delete the bin
        If bBinHasQtyOnHand(lWhseBinKey) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMQtyExist
            ddnStatus.ItemData = kBinStatActive
            Exit Sub
        End If
        
        'If it is a pending transaction for this bin, we cannot delete the bin
        If bBinHasPendingTrans(lWhseBinKey) Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgBinPendTran
            ddnStatus.ItemData = kBinStatActive
            Exit Sub
        End If
        
        If bBinIsBeingUsedAsRoutingBackflushBin(lWhseBinKey, sList) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMBinIsRoutingBackflushBin, sList
            Exit Sub
        End If
        
        If bBinIsBeingUsedAsWorkOrderBackflushBin(lWhseBinKey, sList) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMBinIsWorkOrdBackflushBin, sList
            Exit Sub
        End If
        
    End If
    
   '---------------------------------------------------
   'Check for Volume not Negative and corresponding UOM
   '---------------------------------------------------
   
    If numVolCapacity.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, lblVolCapacity
        gbSetFocus Me, numVolCapacity
        numVolCapacity = 0
    Exit Sub
    End If
    If numVolCapacity.Value > 0 Then
        If Len(Trim(lkuVolCapUnitMeasKey.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgVolUOMReq
            gbSetFocus Me, lkuVolCapUnitMeasKey
        Exit Sub
        End If
    
    End If
    
   '------------------------------------------------------
   'Check for Weight not less then 0 and corresponding UOM
   '------------------------------------------------------
   
    If numWghtCapacity.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, lblWghtCapacity
        gbSetFocus Me, numWghtCapacity
        numWghtCapacity = 0
    End If
    If numWghtCapacity.Value > 0 Then
        If Len(Trim(lkuWghtCapUnitMeasKey.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgWghtUOMReq
            gbSetFocus Me, lkuWghtCapUnitMeasKey
        Exit Sub
        End If
    End If
    bValid = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMValidate", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bBinHasPendingTrans(lWhseBinKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bBinHasPendingTrans = False
    'If it is a pending transaction for this bin
    If lWhseBinKey > 0 Then
        If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "timWhseBinInvt", "WhseBinKey = " & lWhseBinKey & _
                " AND ( PendQtyDecrease  <> 0 OR PendQtyIncrease <> 0 )")) > 0 Then
        bBinHasPendingTrans = True
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bBinHasPendingTrans", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bHasHistTrans(lWhseBinKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
   
    bHasHistTrans = False
    'distributions referenced this bin on posted transactions (has historical activity)
    If lWhseBinKey > 0 Then
        If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "timInvtTranDist", "WhseBinKey = " & lWhseBinKey)) > 0 Then
             bHasHistTrans = True
        End If
    End If
     
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bHasHistTrans", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bBinHasQtyOnHand(lWhseBinKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
   
    bBinHasQtyOnHand = False
    'If Qty is greater than zero, we cannot delete the bin
    
    If lWhseBinKey > 0 Then
        If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "timWhseBinInvt", "WhseBinKey = " & lWhseBinKey & " AND  QtyOnHand <> 0 ")) > 0 Then
            bBinHasQtyOnHand = True
        End If
     End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bBinHasQtyOnHand", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bBinIsPreferredBin(lWhseBinKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
   
    bBinIsPreferredBin = False
    'If this bin being used in any records,we cannot delete the bin
    
    If lWhseBinKey > 0 Then
        If glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "timInvtBinList", "WhseBinKey = " & lWhseBinKey)) > 0 Then
              bBinIsPreferredBin = True
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bBinIsPreferredBin", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lRet                  As Long
    Dim sNewKey               As String
    Dim iConfirmUnload        As Integer

    Select Case sButton

        Case kTbFilter

            miFilter = giToggleLookupFilter(miFilter)

        Case kTbFirst, kTbPrevious, kTbLast, kTbNext

            bConfirmUnload iConfirmUnload, True
            If Not iConfirmUnload = kDmSuccess Then Exit Sub
                'The restrict clause needs to change to the least restrictive allowed so we can browse on all records
                'and not just those for a specific warehouse.
                lkuWhseBinID.RestrictClause = "WhseKey IN (SELECT WhseKey FROM timWarehouse WITH (NOLOCK)" _
                                  & " WHERE timWarehouse.CompanyID = " & gsQuoted(msCompanyID) _
                                  & " AND timWarehouse.UseBins = 1)"

                
                lRet = glLookupBrowse(lkuWhseBinID, sButton, miFilter, sNewKey)
           
                Select Case lRet

                    Case MS_SUCCESS

                        If lkuWhseBinID.ReturnColumnValues.Count = 0 Then Exit Sub

                        'If StrComp(Trim(lkuWhseBinID.Text), Trim(lkuWhseBinID.ReturnColumnValues("WhseBinID")), vbTextCompare) <> 0 Then
             
                            lkuWhseBinID.Text = Trim(lkuWhseBinID.ReturnColumnValues("WhseBinID"))
                            lkuWhseBinID.KeyValue = glGetValidLong(lkuWhseBinID.ReturnColumnValues("WhseBinKey"))
                            lkuWhseKey.KeyValue = glGetValidLong(moClass.moAppDB.Lookup("WhseKey", "timWhseBin", "WhseBinKey = " & glGetValidLong(lkuWhseBinID.KeyValue)))
                            valMgr_KeyChange
                            lkuWhseBinID.Enabled = False
                            lkuWhseKey.Enabled = False
                           
                        'End If
           
                    Case Else

                        gLookupBrowseError lRet, Me, moClass

                End Select

        End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub valMgr_KeyChange()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmForm.SetColumnValue "WhseKey", lkuWhseKey.KeyValue
    
    If glGetValidLong(lkuWhseBinID.KeyValue) = 0 Then
        moDmForm.SetColumnValue "WhseBinKey", 0
        moDmForm.SetColumnValue "WhseBinID", ""
    Else
        moDmForm.SetColumnValue "WhseBinKey", lkuWhseBinID.KeyValue
    End If
    
    If Len(Trim(lkuWhseBinID.Text)) <> 0 And Len(Trim(lkuWhseKey.Text)) <> 0 Then
        lkuWhseBinID.Enabled = False
        lkuWhseKey.Enabled = False
    End If
    
    moDmForm.KeyChange
    SetDefValues

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "valMgr_KeyChange", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}


    With valMgr
        Set .Framework = moClass.moFramework
    .Keys.Add lkuWhseBinID
    .Keys.Add lkuWhseKey
        .Init
    End With

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmForm
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the SOTA identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "IMZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the SOTA identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "IMZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    With lkuWhseBinID
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "WhseKey IN (SELECT WhseKey FROM timWarehouse WITH (NOLOCK)" _
                                  & " WHERE timWarehouse.CompanyID = " & gsQuoted(msCompanyID) _
                                  & " AND timWarehouse.UseBins = 1)"
    End With
    With lkuPhysCountCycleKey
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
    With lkuVolCapUnitMeasKey
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
    With lkuWghtCapUnitMeasKey
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
    With lkuWhseKey
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND Transit = 0 AND UseBins = 1"
    End With
    With lkuWhseZoneKey
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




Private Sub SetupDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '-- Set up drop down properties (see APZDA001 for reference)


    With ddnStatus
        Set .DBObject = moClass.moAppDB
        .Style = sotaDD_STATIC_LIST
        .StaticListTableName = "timWhseBin"
        .StaticListColumnName = "Status"
        .LocaleID = mlLanguage
    End With

    With ddnType
        Set .DBObject = moClass.moAppDB
        .Style = sotaDD_STATIC_LIST
        .StaticListTableName = "timWhseBin"
        .StaticListColumnName = "Type"
        .LocaleID = mlLanguage
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupDropDowns", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Function bConfirmUnload(iConfirmUnload As Integer, Optional ByVal bNoClear As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    If Not valMgr.ValidateForm() Then Exit Function
    
    iConfirmUnload = moDmForm.ConfirmUnload(bNoClear)

    If (iConfirmUnload = kDmSuccess) Then
        bConfirmUnload = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bConfirmUnload", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moDmForm.Action kDmCancel

    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ProcessCancel", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub DisableControls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    txtLocation.Enabled = False
    lkuWhseZoneKey.Enabled = False
    ddnType.Enabled = False
    chkLockedForPutaway.Enabled = False
    chkLockedForPick.Enabled = False
    chkExclFromAvail.Enabled = False
    chkTempBin.Enabled = False
    lkuPhysCountCycleKey.Enabled = False
    lkuVolCapUnitMeasKey.Enabled = False
    lkuWghtCapUnitMeasKey.Enabled = False
    numSortOrder.Enabled = False
    numVolCapacity.Enabled = False
    numWghtCapacity.Enabled = False
    ddnStatus.Enabled = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisableControls", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableControls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    txtLocation.Enabled = True
    ddnType.Enabled = True
    chkTempBin.Enabled = True
    lkuPhysCountCycleKey.Enabled = True
    lkuVolCapUnitMeasKey.Enabled = True
    lkuWghtCapUnitMeasKey.Enabled = True
    numSortOrder.Enabled = True
    numVolCapacity.Enabled = True
    numWghtCapacity.Enabled = True
    ddnStatus.Enabled = True
    
    lkuWhseZoneKey.Enabled = mbWMIsLicensed
    chkLockedForPutaway.Enabled = mbWMIsLicensed
    chkLockedForPick.Enabled = mbWMIsLicensed
    chkExclFromAvail.Enabled = mbWMIsLicensed
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "EnableControls", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetDefValues()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If moDmForm.State = sotaTB_ADD Then
        'Set default status to Active.
        ddnStatus.DefaultItemData = kBinStatActive   'Active
        
        'Set here the tag as well so that the security event handler in Validate will not give a GPF.
        ddnStatus.Tag = kBinStatActive
        ddnStatus.SetToDefault True
        
        'Set default type to Fixed
        ddnType.DefaultItemData = kBinTypeFixed
        ddnType.Tag = kBinTypeFixed
        ddnType.SetToDefault True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetDefValues", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function bBinIsBeingUsedAsRoutingBackflushBin(ByVal lWhseBinKey As Long, ByRef sRoutingStepList As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '------------------------------------------------------------------------------
    ' This function checks to see if the passed WhseBinKey exists on an active
    ' routing step as a bacflush bin.  If it does, the function is set to true and
    ' the first five steps found are returned in sRoutingStepList.
    '------------------------------------------------------------------------------

    'Verify that there is a WhseBinKey to process.
    If lWhseBinKey > 0 Then
    
        Dim sSQL As String
        Dim iCounter As Integer
        Dim oRS As Object
        
        sSQL = "SELECT RH.RoutingID, RH.VersionID, RD.StepID" & _
            " FROM tmfRoutDetl_HAI RD WITH (NOLOCK)" & _
            " INNER JOIN tmfRoutHead_HAI RH WITH (NOLOCK) ON RH.RoutingKey = RD.RoutingKey" & _
            " WHERE RH.Active = 1 AND RD.BackFlushWhseBinKey = " & lWhseBinKey
            
        'Query the database for active routing steps that use the bin as a backflush bin.
        Set oRS = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not oRS.IsEmpty Then
        
            bBinIsBeingUsedAsRoutingBackflushBin = True
            
            sRoutingStepList = vbCrLf
            
            'Build list of routing steps the contain the bin (up to the first five).
            While (Not oRS.IsEOF) And iCounter < 5
            
                sRoutingStepList = sRoutingStepList & vbCrLf & gsGetValidStr(oRS.Field("RoutingID")) & _
                    " / " & gsGetValidStr(oRS.Field("VersionID")) & " STEP: " & gsGetValidStr(oRS.Field("StepID"))
            
                iCounter = iCounter + 1
                oRS.MoveNext
            Wend
        
        End If
        
        oRS.Close
        Set oRS = Nothing
    
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bBinIsBeingUsedAsRoutingBackflushBin", VBRIG_IS_FORM   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Function

Private Function bBinIsBeingUsedAsWorkOrderBackflushBin(ByVal lWhseBinKey As Long, ByRef sRoutingStepList As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '-------------------------------------------------------------------------
    ' This function checks to see if the passed WhseBinKey exists on an open
    ' work order as a bacflush bin.  If it does, the function is set to true
    ' and the first five steps found are returned in sRoutingStepList.
    '
    ' The StepComplete flag on the detail lines is not considered when
    ' evaluating the work order.  Only the status of the header is considered.
    '-------------------------------------------------------------------------

    'Verify that there is a WhseBinKey to process.
    If lWhseBinKey > 0 Then
    
        Dim sSQL As String
        Dim iCounter As Integer
        Dim oRS As Object
        
        sSQL = "SELECT WOH.WorkOrderNo, WOD.StepID" & _
            " FROM tmfWorkOrdHead_HAI WOH WITH (NOLOCK)" & _
            " INNER JOIN tmfWorkOrdDetl_HAI WOD WITH (NOLOCK) ON WOD.WorkOrderKey = WOH.WorkOrderKey" & _
            " WHERE WOH.Complete = 0" & _
            " AND WOD.BackFlushWhseBinKey = " & lWhseBinKey
            
        'Query the database for active routing steps that use the bin as a backflush bin.
        Set oRS = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not oRS.IsEmpty Then
        
            bBinIsBeingUsedAsWorkOrderBackflushBin = True
            
            sRoutingStepList = vbCrLf
            
            'Build list of work order steps the contain the bin (up to the first five).
            While (Not oRS.IsEOF) And iCounter < 5
            
                sRoutingStepList = sRoutingStepList & vbCrLf & gsGetValidStr(oRS.Field("WorkOrderNo")) & _
                    " STEP: " & gsGetValidStr(oRS.Field("StepID"))
            
                iCounter = iCounter + 1
                oRS.MoveNext
            Wend
        
        End If
        
        oRS.Close
        Set oRS = Nothing
    
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bBinIsBeingUsedAsWorkOrderBackflushBin", VBRIG_IS_FORM 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Function


