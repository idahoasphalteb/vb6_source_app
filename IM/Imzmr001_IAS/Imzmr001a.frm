VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{48E59290-9880-11CF-9754-00AA00C00908}#1.0#0"; "MSINET.OCX"
Begin VB.Form frmGetImages 
   Caption         =   "Choose Internet Image"
   ClientHeight    =   7440
   ClientLeft      =   180
   ClientTop       =   345
   ClientWidth     =   9630
   HelpContextID   =   17374109
   Icon            =   "Imzmr001a.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   9630
   StartUpPosition =   1  'CenterOwner
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   741
      Style           =   6
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7050
      WhatsThisHelpID =   73
      Width           =   9630
      _ExtentX        =   16986
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin VB.Frame fraItemImage 
      Height          =   3585
      Left            =   150
      TabIndex        =   4
      Top             =   3450
      Width           =   9495
      Begin VB.Frame fraAvailableImage 
         Caption         =   "A&vailable Images"
         Height          =   2730
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   5650
         Begin VB.ListBox lstAvailableImage 
            Height          =   2400
            ItemData        =   "Imzmr001a.frx":23D2
            Left            =   150
            List            =   "Imzmr001a.frx":23D9
            TabIndex        =   14
            Top             =   240
            WhatsThisHelpID =   17374112
            Width           =   5400
         End
      End
      Begin VB.Frame fraSelectImage 
         Caption         =   "Selected &Image"
         Enabled         =   0   'False
         Height          =   2745
         Left            =   5900
         TabIndex        =   11
         Top             =   120
         Width           =   3495
         Begin SHDocVwCtl.WebBrowser webItemImage 
            Height          =   2430
            Left            =   120
            TabIndex        =   12
            Top             =   225
            WhatsThisHelpID =   17374215
            Width           =   3255
            ExtentX         =   5741
            ExtentY         =   4286
            ViewMode        =   0
            Offline         =   0
            Silent          =   0
            RegisterAsBrowser=   0
            RegisterAsDropTarget=   1
            AutoArrange     =   0   'False
            NoClientEdge    =   0   'False
            AlignLeft       =   0   'False
            NoWebView       =   0   'False
            HideFileNames   =   0   'False
            SingleClick     =   0   'False
            SingleSelection =   0   'False
            NoFolders       =   0   'False
            Transparent     =   0   'False
            ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
            Location        =   "http:///"
         End
         Begin VB.Label lblSelectImage 
            Caption         =   "No Image have been choosen."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   765
            Left            =   120
            TabIndex        =   15
            Top             =   960
            Width           =   3255
         End
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "&OK"
         Height          =   350
         Left            =   6235
         TabIndex        =   9
         Top             =   3180
         WhatsThisHelpID =   17374113
         Width           =   1300
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "&Cancel"
         Height          =   350
         Left            =   7905
         TabIndex        =   8
         Top             =   3180
         WhatsThisHelpID =   17374114
         Width           =   1300
      End
      Begin VB.Frame fraCopyImage 
         Height          =   700
         Left            =   2280
         TabIndex        =   5
         Top             =   2820
         Width           =   3495
         Begin VB.OptionButton optImage 
            Caption         =   "&Use Internet Image "
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   7
            Top             =   390
            WhatsThisHelpID =   17374115
            Width           =   2475
         End
         Begin VB.OptionButton optImage 
            Caption         =   "Co&py to Image Directory"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Top             =   150
            Value           =   -1  'True
            WhatsThisHelpID =   17374116
            Width           =   2295
         End
      End
      Begin InetCtlsObjects.Inet InetItemImage 
         Left            =   8160
         Top             =   3720
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
      End
      Begin MSComDlg.CommonDialog cdgItemLocation 
         Left            =   8760
         Top             =   3720
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DialogTitle     =   "Copy Image "
         Filter          =   "Pictures (*.ico; *.gif; *.jpg)|*.ico;*.gif;*.jpg"
      End
   End
   Begin VB.Frame fraWebSite 
      Height          =   2505
      Left            =   120
      TabIndex        =   2
      Top             =   950
      Width           =   9495
      Begin SHDocVwCtl.WebBrowser webInternetSite 
         Height          =   2195
         Left            =   120
         TabIndex        =   3
         Top             =   200
         WhatsThisHelpID =   17374216
         Width           =   9255
         ExtentX         =   16325
         ExtentY         =   3872
         ViewMode        =   0
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   1
         RegisterAsDropTarget=   1
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         NoWebView       =   0   'False
         HideFileNames   =   0   'False
         SingleClick     =   0   'False
         SingleSelection =   0   'False
         NoFolders       =   0   'False
         Transparent     =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   "http:///"
      End
   End
   Begin VB.Frame fraAddress 
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   380
      Width           =   9395
      Begin VB.ComboBox cboAddress 
         Height          =   315
         Left            =   885
         TabIndex        =   1
         Top             =   120
         WhatsThisHelpID =   17374117
         Width           =   8215
      End
      Begin VB.Label lblAddress 
         Caption         =   "&Address"
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   180
         Width           =   735
      End
   End
End
Attribute VB_Name = "frmGetImages"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private Variables of Form Properties
Private mfrmParent          As Form
Private moClass             As Object
Private moContextMenu       As New clsContextMenu   'Context Menu Object

Dim collImages As Collection
Dim sFile As String
Dim miGetStatus As Long
Dim sInetLocation As String
Dim sItemImageLocation As String

'Private moClass             As Object           'Class object that loaded this form.
Private Const kCopytoImageDir = 0   '
Private Const kUseInternetImage = 1
Private Const kValidProtocolHTTP = "HTTP:"
Private Const kValidProtocolHTTPS = "HTTPS:"
Private Const kValidProtocolABOUT = "ABOUT:"



Private Sub cboAddress_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If KeyCode = vbKeyReturn Then
        doGoWebBrowser
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCancel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboAddress_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    doGoWebBrowser

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboAddress_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub cmdCancel_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    iGetStatus = kNo
    Unload Me

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCancel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub cmdCancel_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    cmdCancel_Click
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCancel_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub cmdExit_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    
    If optImage(kCopytoImageDir) Then
        If Not SetSaveFile() Then
           Exit Sub
        End If
    ElseIf optImage(kUseInternetImage) Then
        ItemImageLocation = lstAvailableImage.ToolTipText
    End If
    iGetStatus = kYes
    Unload Me

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdExit_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function SetSaveFile() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' Retrieve latest software update from ftp.mycompany.com.
    Dim bFile() As Byte         ' Retrieving a binary file.
    Dim sFile As String
    Dim sFolder As String
    
    SetSaveFile = False
    On Error Resume Next
    With cdgItemLocation
        .CancelError = True
        .FileName = lstAvailableImage.Text
        .ShowSave
        sFile = .FileName
        .InitDir = .FileName
    End With
    If Err.Number = cdlCancel Then Exit Function
    
    ' Check to see if Suffix and graphic file type are match.
    If InStrRev(cdgItemLocation.FileName, ".") > 0 Then
        If Mid$(lstAvailableImage.Text, InStrRev(lstAvailableImage.Text, ".") + 1) <> _
            Mid$(cdgItemLocation.FileName, InStrRev(cdgItemLocation.FileName, ".") + 1) Then
            giSotaMsgBox Me, moClass.moSysSession, kIMGrahphicSuffxNotMatch, _
                    Mid$(lstAvailableImage.Text, InStrRev(lstAvailableImage.Text, ".") + 1), _
                    Mid$(cdgItemLocation.FileName, InStrRev(cdgItemLocation.FileName, ".") + 1)
                    cdgItemLocation.InitDir = ItemImageLocation
            Exit Function
        End If
    End If
    
    ' Check to see if the location and file is valid to copy.
    Dim sStrLocation As String
    If StrComp(ItemImageLocation, Left(cdgItemLocation.FileName, Len(ItemImageLocation)), vbTextCompare) <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMInvalidFileToCopy, cdgItemLocation.FileName
        cdgItemLocation.InitDir = ItemImageLocation
        Exit Function
    End If
    If Len(Dir(Left$(cdgItemLocation.FileName, InStrRev(cdgItemLocation.FileName, "\")))) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMInvalidFileToCopy, cdgItemLocation.FileName
        cdgItemLocation.InitDir = ItemImageLocation
        Exit Function
    End If
    
    
    
    
    
    
    
    
    #If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
    #End If
       
    ' Retrieve the file.
    bFile() = InetItemImage.OpenURL _
        (CStr(collImages(lstAvailableImage.ListIndex + 1)), _
        icByteArray)
        
    ' Write file to disk.
    sFile = IIf(InStr(sFile, ".") <> 0, sFile, sFile & "." & Mid$(lstAvailableImage.Text, InStrRev(lstAvailableImage.Text, ".") + 1))
    On Error Resume Next
    Open sFile For Binary Access Write As #1
    If Err.Number = 75 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMPathFileAcessError, sFile
        Close #1
        Exit Function
    End If
    Put #1, , bFile()
    Close #1
    
    ItemImageLocation = Mid$(sFile, Len(ItemImageLocation) + 1)
    SetSaveFile = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetSaveFile", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub Form_Activate()
    cboAddress.SelStart = Len(cboAddress.Text)
End Sub

Private Function sMyName() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
       sMyName = "frmGetImages"
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    webInternetSite.Navigate2 kINETAboutBlank ' Initialize webInternetSite control  to ABOUT:BLANK
    webItemImage.Navigate2 kINETAboutBlank          ' Initialize webItemImage control to ABOUT:BLANK
    
    sbrMain.BusinessDate = moClass.moSysSession.BusinessDate
    sbrMain.CompanyId = moClass.moSysSession.CompanyId
    sbrMain.UserName = moClass.moSysSession.UserName
   
    tbrMain.ButtonEnabled(kTbFinishExit) = False
    tbrMain.RemoveButton kTbPrint
    tbrMain.RemoveButton kTbHelp
    tbrMain.AddButton kTbProceed
    tbrMain.AddButton kTbStop
    tbrMain.ButtonEnabled(kTbStop) = False
    tbrMain.AddButton kTbHelp


    sbrMain.Status = SOTA_SB_INQUIRY
    SetInternetVisible (False)
    With cdgItemLocation
        '.DefaultExt = kINETDefltExt
        .Filter = kINETFilterExt
        .CancelError = True
        .InitDir = ItemImageLocation
        .FileName = sFile
    End With
    lblSelectImage.Caption = ""
    'webInternetSite.MenuBar = True
    'webInternetSite.Navigate2 kINETAboutBlank
    
    webItemImage.ToolBar = 0
    
    'Initialize the Context Menu object.
    BindContextMenu
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub SetInternetVisible(bValue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    webInternetSite.Visible = bValue
    lstAvailableImage.Visible = bValue
    SetPictureImage False
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetInternetVisible", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Property Let iGetStatus(pValue As Long)
     miGetStatus = pValue
End Property
Public Property Get iGetStatus() As Long
    iGetStatus = miGetStatus
End Property
Public Property Let ItemImageLocation(pValue As String)
     sItemImageLocation = pValue
End Property
Public Property Get ItemImageLocation() As String
    ItemImageLocation = sItemImageLocation
End Property
Public Property Let InetLocation(pValue As String)
     sInetLocation = pValue
End Property
Public Property Get InetLocation() As String
    InetLocation = sInetLocation
End Property
Private Sub SetPictureImage(bValue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    webItemImage.Visible = bValue
    fraCopyImage.Enabled = bValue
    cmdExit.Enabled = bValue
    lblSelectImage.Visible = Not bValue
    tbrMain.ButtonEnabled(kTbFinishExit) = bValue

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPictureImage", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  
    InetLocation = lstAvailableImage.ToolTipText
    Set moContextMenu = Nothing         ' context menu class
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub lstAvailableImage_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If lstAvailableImage.ListIndex >= 0 Then
        ShowImage webItemImage, collImages(lstAvailableImage.ListIndex + 1)
        lstAvailableImage.ToolTipText = collImages(lstAvailableImage.ListIndex + 1)
        
        SetPictureImage True
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailableImage_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAvailableImage_DblClick()
''+++ VB/Rig Begin Push +++
'#If ERRORTRAPON Then
'On Error GoTo VBRigErrorRoutine
'#End If
''+++ VB/Rig End +++
'
'    webInternetSite.Navigate2 collImages(lstAvailableImage.ListIndex + 1)
'
''+++ VB/Rig Begin Pop +++
'    Exit Sub
'
'VBRigErrorRoutine:
'        gSetSotaErr Err, sMyName, "lstAvailableImage_DblClick", VBRIG_IS_FORM
'        Select Case VBRIG_IS_FORM_EVENT
'        Case VBRIG_IS_NON_EVENT
'                Err.Raise guSotaErr.Number
'        Case Else
'                Call giErrorHandler: Exit Sub
'        End Select
''+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sString As String
    sString = Button
    Select Case UCase(Button)
    Case kTbCancelExit
        cmdCancel_Click
    Case kTbFinishExit
        cmdExit_Click
    Case kTbProceed
        Call doGoWebBrowser
    Case kTbStop
        Call doStopWebBrowser
    Case kTbHelp
        gDisplayFormLevelHelp Me
    Case Else
    End Select
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub doGoWebBrowser()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

   lstAvailableImage.Clear
   webInternetSite.Visible = False
   SetHourglass True
   lblSelectImage.Caption = "Please choose an image."
   fraSelectImage.Enabled = True
   SetInternetVisible True
   tbrMain.ButtonEnabled(kTbStop) = True
   tbrMain.ButtonEnabled(kTbProceed) = False
   'webInternetSite.Application.Silent = True
   On Error Resume Next
   webInternetSite.Navigate2 cboAddress.Text, , Top
   If Err.Number <> 0 Then
        tbrMain.ButtonEnabled(kTbStop) = False
        tbrMain.ButtonEnabled(kTbProceed) = True
        SetHourglass False
        Exit Sub
   End If
   #If ERRORTRAPON Then
        On Error GoTo VBRigErrorRoutine
   #End If
   
   webInternetSite.SetFocus
   
   SetHourglass False
   sbrMain.Status = SOTA_SB_START
   ' While webInternetSite.ReadyState <> READYSTATE_COMPLETE
   '    DoEvents
   ' Wend
   'tbrMain.ButtonEnabled(kTbStop) = False
   'tbrMain.ButtonEnabled(kTbProceed) = True
   'SetHourglass False
   
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "doGoWebBrowser", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub doStopWebBrowser()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim i As Integer
    webInternetSite.Navigate2 kINETAboutBlank
    tbrMain.ButtonEnabled(kTbStop) = False
    webInternetSite.Stop
    For i = 1 To 4
        SetHourglass False
    Next i
    
    sbrMain.Status = SOTA_SB_INQUIRY
    tbrMain.ButtonEnabled(kTbProceed) = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "doStopWebBrowser", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub webInternetSite_BeforeNavigate2(ByVal pDisp As Object, URL As Variant, Flags As Variant, TargetFrameName As Variant, PostData As Variant, Headers As Variant, Cancel As Boolean)
    Dim sProtocol As String
    sProtocol = UCase(Left(URL, (InStr(1, URL, ":"))))
    Select Case sProtocol
        Case kValidProtocolHTTP, kValidProtocolHTTPS, kValidProtocolABOUT
            Cancel = False
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kIMInvalidProtocol, URL
            Cancel = True
    End Select
    
    lblSelectImage.Caption = "Please choose an image."
    SetInternetVisible True
    SetHourglass True
End Sub

Private Sub webInternetSite_DocumentComplete(ByVal pDisp As Object, URL As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim vitem
    Dim i As Integer

    Set collImages = New Collection
    lstAvailableImage.Clear
    On Error Resume Next
    For i = 0 To webInternetSite.Document.images.Length - 1
        vitem = Null
        On Error Resume Next
        vitem = collImages(webInternetSite.Document.images(i).src)
        If IsNull(vitem) Then
            ' Filter only the file extenstion which includes only in kINETFilterExt
            If InStr(kINETFilterExt, Mid$(webInternetSite.Document.images(i).src, InStrRev(webInternetSite.Document.images(i).src, ".") + 1)) > 0 Then
                lstAvailableImage.AddItem Mid$(webInternetSite.Document.images(i).src, InStrRev(webInternetSite.Document.images(i).src, "/") + 1)
                collImages.Add webInternetSite.Document.images(i).src, webInternetSite.Document.images(i).src
            End If
        End If
    Next
    SetHourglass False
    SetHourglass False
    SetHourglass False
    tbrMain.ButtonEnabled(kTbStop) = False
    tbrMain.ButtonEnabled(kTbProceed) = True
    sbrMain.Status = SOTA_SB_INQUIRY
'+++ VB/Rig Begin Pop +++
   

    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "webInternetSite_DocumentComplete", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

'************************************************************************
'   ParentForm contains the reference to the Form that has loaded it.
'   This may be used when the ParentForm's grid needs to be updated.
'************************************************************************
Public Property Set ParentForm(frmParent As Form)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set mfrmParent = frmParent
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ParentForm_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub webItemImage_NavigateComplete2(ByVal pDisp As Object, URL As Variant)
    SetHourglass False
End Sub

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If frmMaintainItem.miModule = kModuleIM Then
        FormHelpPrefix = "IMz"    ' Put your identifier here
    ElseIf frmMaintainItem.miModule = kModuleCI Then
        FormHelpPrefix = "CIz"    ' Put your identifier here
    End If

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If frmMaintainItem.miModule = kModuleIM Then
        WhatHelpPrefix = "IMz"    ' Put your identifier here
    ElseIf frmMaintainItem.miModule = kModuleCI Then
        WhatHelpPrefix = "CIz"    ' Put your identifier here
    End If

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

' *************************************************************************
'   DESC:  This subroutine binds the context menu object to the form.
'   PARMS:  None.
' *************************************************************************

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Bind Context Menus.
    With moContextMenu
        Set .Form = frmGetImages

        'Init will set properties of the Winhook control.
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
        




