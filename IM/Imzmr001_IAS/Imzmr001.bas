Attribute VB_Name = "basMaintainItem"
'*********************************************
'* NOTE: This module has NOT been VB/Rigged! *
'*********************************************

Option Explicit
'**********************************************************************
'     Name: basimRankMaintain
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 03-12-1998
'     Mods: mm/dd/yy XXX
'**********************************************************************
'-- Object Task Constants
    '-- if this maintenance program has an associated report/listing,
    '-- here is where you would define the report/listing's Task ID so
    '-- that it may be launched when the Print button is pressed on the
    '-- toolbar
    Public Const ktskPrint1 = 118096022
    Public Const ktskPrint2 = 34209793
       
    Public giItemKey            As Long
    Public ItemKeyFlag          As Boolean
    
'-- Static List Constants
' timItem | Status
    Public Const kvActive As Integer = 1                'Active                         Str=50028
    Public Const kvInactive As Integer = 2              'Inactive                       Str=50029
    Public Const kvDiscontinued As Integer = 3          'Discontinued                   Str=50387
    Public Const kvDeleted As Integer = 4               'Deleted                        Str=50033

    Public Const kItemRunDD = 3
        
    Public Const kINETAboutBlank As String = "ABOUT:BLANK"
    Public Const kINETProtocol As String = "HTTP" ' Web Protocol
    Public Const kINETFilterExt As String = "*.tif;*.jpg;*.jpeg;*.bmp;*.gif;*.png;*.pcx;*.pcd, *.wmf"
    Public Const kINETDefltExt As String = "*.jpg"
    Private Const kINETDocWidth As Integer = 140
    Private Const kINETDocHeighth As Integer = 140
    Private Const kINETDocLeftMargin As Integer = 0.05
    Private Const kINETDocTopMargin As Integer = 0.05
    

Const VBRIG_MODULE_ID_STRING = "IMZMR001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("imzmr001.clsMaintainItem")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sText As String

    If lErr = guSotaErr.Number And Trim(guSotaErr.Description) <> Trim(sDesc) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If

    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyId
        sText = sText & "  AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyErrMsg", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Sub gDisableControls(ParamArray Controls())
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iControl As Variant

    For iControl = 0 To UBound(Controls)
        With Controls(iControl)
            Select Case TypeName(Controls(iControl))
            Case "TextBox", "ComboBox", "SotaMaskedEdit", "SOTAMaskedEdit", "SotaCurr", "SotaNumber", "SOTANumber", "SOTACurrency"
                .Enabled = False
                .BackColor = vbButtonFace
            Case Else
                .Enabled = False
            End Select
        End With
    Next iControl
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "gDisableControls", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Sub gEnableControls(ParamArray Controls())
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iControl As Variant

    For iControl = 0 To UBound(Controls)
        With Controls(iControl)
            Select Case TypeName(Controls(iControl))
            Case "TextBox", "ComboBox", "SotaMaskedEdit", "SOTAMaskedEdit", "SotaCurr", "SotaNumber", "SOTANumber", "SOTACurrency"
                .Enabled = True
                .BackColor = vbWindowBackground
            Case Else
                .Enabled = True
            End Select
        End With
    Next iControl
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "gEnableControls", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    sMyName = "basMaintainItem"
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Public Function bDeleteAssignedProdCatsforItem(oDB As Object, _
                                               ByVal lProdCategoryKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String

    bDeleteAssignedProdCatsforItem = False

    If lProdCategoryKey > 0 Then
        sSQL = "DELETE FROM timProdCatItem "
        sSQL = sSQL & "WHERE  ItemKey = " & Format$(lProdCategoryKey)
        oDB.ExecuteSQL sSQL
    End If
    
    bDeleteAssignedProdCatsforItem = True

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteAssignedProdCatsforItem", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Public Function bInsertAssignedProdcatsforItem(oDB As Object, _
                                              ByVal lItemKey As Long, _
                                              ByVal lProdCategoryKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
    On Error GoTo ExpectedErrorRoutine
'+++ VB/Rig End +++


    Dim sSQL As String

    bInsertAssignedProdcatsforItem = False

    If (lProdCategoryKey > 0 _
    And lItemKey > 0) Then
        sSQL = "INSERT INTO timProdCatItem ("
        sSQL = sSQL & "ProdCategoryKey, ItemKey) "
        sSQL = sSQL & "VALUES ("
        sSQL = sSQL & Format$(lProdCategoryKey) & ", "
        sSQL = sSQL & Format$(lItemKey) & ")"
        oDB.ExecuteSQL sSQL
    End If
    
    bInsertAssignedProdcatsforItem = True

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInsertAssignedProdcatsforItem", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub ShowImage(objINETControl As WebBrowser, sImage As String)
'-------------------------------------------------------------------------------------
' Purpose: This procedure show the image picture and also rezide its image.
' It accepts two parameters
' objINETControl --- name of internet control object.
' sImage         --- Location and Name of the image.
'
'-------------------------------------------------------------------------------------
'+++ VB/Rig Begin Push +++
    On Error GoTo ExpectedErrorRoutine
'+++ VB/Rig End +++
    Dim oDoc As Object
    Dim ix As Integer
    Set oDoc = objINETControl.Document
    If oDoc.images.Length = 0 Then
        oDoc.Body.InnerHTML = "<img border='0' src='' >"
    End If
    ' Link to this image location and name.
    oDoc.images(0).src = sImage
    oDoc.images(0).Width = kINETDocWidth
    oDoc.images(0).Height = kINETDocHeighth
    oDoc.Body.LeftMargin = kINETDocLeftMargin
    oDoc.Body.TopMargin = kINETDocTopMargin
    SetHourglass False
    SetHourglass False
    SetHourglass False
    
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowImage", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
    
End Sub

