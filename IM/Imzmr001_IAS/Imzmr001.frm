VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMaintainItem 
   ClientHeight    =   7935
   ClientLeft      =   855
   ClientTop       =   660
   ClientWidth     =   9870
   HelpContextID   =   17374118
   Icon            =   "Imzmr001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7935
   ScaleWidth      =   9870
   Begin MSComDlg.CommonDialog cdgGetImage 
      Left            =   3960
      Top             =   900
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7545
      WhatsThisHelpID =   73
      Width           =   9870
      _ExtentX        =   17410
      _ExtentY        =   688
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   102
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin EntryLookupControls.TextLookup lkuMain 
      Height          =   285
      Left            =   930
      TabIndex        =   1
      Top             =   540
      WhatsThisHelpID =   17374121
      Width           =   3530
      _ExtentX        =   6218
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      LookupID        =   "Item"
      BoundColumn     =   "ItemID"
      BoundTable      =   "timItem"
      sSQLReturnCols  =   "ItemID,lkuMain,;ShortDesc,,;ItemKey,,;"
   End
   Begin SOTAVM.SOTAValidationMgr SOTAVM 
      Left            =   15000
      Top             =   720
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   18
      Ctl1            =   "lkuDefltWarehouse;-1;lblDefWareHouse;-1"
      Ctl2            =   "lkuItemClass;-1;lblItemClass;-1"
      Ctl3            =   "lkuCommoCode;-1;lblCommCode;-1"
      Ctl4            =   "lkuStockUOM;-1;lblStockUOM;-1"
      Ctl5            =   "lkuPurchaseUOM;-1;lblPurchase;-1"
      Ctl6            =   "lkuSales;-1;lblSales;-1"
      Ctl7            =   "lkuPrice;-1;lblPrice;-1"
      Ctl8            =   "lkuProdLine;-1;lblProdLine;-1"
      Ctl9            =   "lkuCustPriceGroup;-1;lblCustPriceGroup;-1"
      Ctl10           =   "lkuCommCls;-1;lblCommClass;-1"
      Ctl11           =   "lkuPuchProdLine;-1;lblPurchProdLine;-1"
      Ctl12           =   "lkuPOMatchTolerance;-1;lblPOMatchTolerance;-1"
      Ctl13           =   "glaCOS;-1;lblCOS;-1"
      Ctl14           =   "glaExpense;-1;lblExpense;-1"
      Ctl15           =   "glaSales;-1;lblSalesAcct;-1"
      Ctl16           =   "glaReturns;-1;lblReturns;-1"
      Ctl17           =   "lkuFreightClass;-1;lblFreightCls;-1"
      Ctl18           =   "lkuMain;-1;lblItem;-1"
   End
   Begin VB.TextBox txtLongDesc 
      Height          =   492
      Left            =   6300
      MaxLength       =   255
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   870
      WhatsThisHelpID =   17374122
      Width           =   3495
   End
   Begin VB.TextBox txtShortDesc 
      Height          =   285
      Left            =   6300
      MaxLength       =   40
      TabIndex        =   3
      Top             =   540
      WhatsThisHelpID =   17374123
      Width           =   3495
   End
   Begin TabDlg.SSTab tabMaintainItem 
      Height          =   5970
      Left            =   45
      TabIndex        =   6
      Top             =   1470
      WhatsThisHelpID =   17374124
      Width           =   9720
      _ExtentX        =   17145
      _ExtentY        =   10530
      _Version        =   393216
      Tabs            =   9
      Tab             =   7
      TabsPerRow      =   9
      TabHeight       =   529
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "Imzmr001.frx":23D2
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "SSPanel1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&UOM"
      TabPicture(1)   =   "Imzmr001.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "SSPanel1(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Kits"
      TabPicture(2)   =   "Imzmr001.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "SSPanel1(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&Purchasing"
      TabPicture(3)   =   "Imzmr001.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "SSPanel1(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "&Sales"
      TabPicture(4)   =   "Imzmr001.frx":2442
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "SSPanel1(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "&GL Accounts"
      TabPicture(5)   =   "Imzmr001.frx":245E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "SSPanel1(5)"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "&Categories"
      TabPicture(6)   =   "Imzmr001.frx":247A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "SSPanel1(6)"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "COA &Tests"
      TabPicture(7)   =   "Imzmr001.frx":2496
      Tab(7).ControlEnabled=   -1  'True
      Tab(7).Control(0)=   "SSPanel1(7)"
      Tab(7).Control(0).Enabled=   0   'False
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "&CustomTab"
      TabPicture(8)   =   "Imzmr001.frx":24B2
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "SSPanel1(8)"
      Tab(8).ControlCount=   1
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   3
         Left            =   -74910
         TabIndex        =   77
         Top             =   420
         Width           =   9495
         _Version        =   65536
         _ExtentX        =   16748
         _ExtentY        =   9560
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraOptions 
            Caption         =   "&Options"
            Height          =   2055
            Left            =   4890
            TabIndex        =   85
            Top             =   165
            Width           =   4470
            Begin VB.CheckBox chkRcptRequired 
               Caption         =   "Require Receipt of Goods"
               Height          =   285
               Left            =   165
               TabIndex        =   88
               Top             =   1425
               WhatsThisHelpID =   17374135
               Width           =   4240
            End
            Begin VB.CheckBox chkInternalDelvrRequired 
               Caption         =   "Require Internal Delivery"
               Height          =   285
               Left            =   165
               TabIndex        =   87
               Top             =   900
               WhatsThisHelpID =   17374136
               Width           =   4240
            End
            Begin VB.CheckBox chkAllowCostOvrd 
               Caption         =   "Allow Cost Override"
               Height          =   285
               Left            =   165
               TabIndex        =   86
               Top             =   375
               WhatsThisHelpID =   17374140
               Width           =   4240
            End
         End
         Begin VB.Frame fraPurchProdLine 
            Caption         =   "Pur&chases"
            Height          =   2055
            Left            =   75
            TabIndex        =   78
            Top             =   165
            Width           =   4755
            Begin NEWSOTALib.SOTANumber numPeriodUsage 
               Height          =   285
               Left            =   2340
               TabIndex        =   84
               Top             =   1365
               WhatsThisHelpID =   17374153
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,##<ILp0>#"
               text            =   "     0"
               dMaxValue       =   32767
               sIntegralPlaces =   6
               sDecimalPlaces  =   0
            End
            Begin EntryLookupControls.TextLookup lkuPOMatchTolerance 
               Height          =   285
               Left            =   2340
               TabIndex        =   82
               Top             =   840
               WhatsThisHelpID =   17374154
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMMatchT"
               ParentIDColumn  =   "MatchToleranceID"
               ParentKeyColumn =   "MatchToleranceKey"
               ParentTable     =   "tpoMatchTolerance"
               BoundColumn     =   "MatchToleranceKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "MatchToleranceID,lkuPOMatchTolerance,;"
            End
            Begin EntryLookupControls.TextLookup lkuPuchProdLine 
               Height          =   285
               Left            =   2340
               TabIndex        =   80
               Top             =   345
               WhatsThisHelpID =   17374155
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMPurchP"
               ParentIDColumn  =   "PurchProdLineID"
               ParentKeyColumn =   "PurchProdLineKey"
               ParentTable     =   "timPurchProdLine"
               BoundColumn     =   "PurchProdLineKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "PurchProdLineID,lkuPuchProdLine,;"
            End
            Begin VB.Label lblPurchProdLine 
               Caption         =   "Purchase Product Line"
               Height          =   255
               Left            =   180
               TabIndex        =   79
               Top             =   375
               Width           =   2055
            End
            Begin VB.Label lblPOMatchTolerance 
               Caption         =   "PO Match Tolerance"
               Height          =   255
               Left            =   180
               TabIndex        =   81
               Top             =   900
               Width           =   2055
            End
            Begin VB.Label lblPeriodUsageOrderLmt 
               Caption         =   "Period Usage Order Limit"
               Height          =   195
               Left            =   180
               TabIndex        =   83
               Top             =   1425
               Width           =   2055
            End
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   2
         Left            =   -74910
         TabIndex        =   72
         Top             =   420
         Width           =   9495
         _Version        =   65536
         _ExtentX        =   16748
         _ExtentY        =   9560
         _StockProps     =   15
         Caption         =   "SSPanel3"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.CheckBox chkReturnsAllowedMirror 
            Caption         =   "Allow Returns"
            Enabled         =   0   'False
            Height          =   270
            Left            =   5025
            TabIndex        =   107
            Top             =   60
            WhatsThisHelpID =   17374156
            Width           =   4100
         End
         Begin VB.CheckBox chkAllowRtrnsPartialBTO 
            Caption         =   "Allow &Return of Partial BTO Kits"
            Height          =   252
            Left            =   5025
            TabIndex        =   109
            Top             =   645
            WhatsThisHelpID =   17374157
            Width           =   4100
         End
         Begin VB.CheckBox chkAllowRtrnsCustomBTO 
            Caption         =   "&Allow Return of Customized BTO Kits"
            Height          =   252
            Left            =   5025
            TabIndex        =   108
            Top             =   345
            WhatsThisHelpID =   17374158
            Width           =   4100
         End
         Begin VB.TextBox txtNavReturnKit 
            Height          =   285
            Left            =   60
            TabIndex        =   106
            Top             =   1620
            Visible         =   0   'False
            WhatsThisHelpID =   17374159
            Width           =   1392
         End
         Begin VB.CheckBox chkEplodeOnInvoice 
            Caption         =   "&Explode BTO Kit on Packing List/Invoice"
            Height          =   252
            Left            =   120
            TabIndex        =   73
            Top             =   60
            WhatsThisHelpID =   17374160
            Width           =   4530
         End
         Begin VB.CheckBox chkExplodeOnPickTicket 
            Caption         =   "Explode On Pick &Ticket"
            Height          =   192
            Left            =   120
            TabIndex        =   75
            Top             =   660
            Visible         =   0   'False
            WhatsThisHelpID =   17374161
            Width           =   4530
         End
         Begin VB.CheckBox chkPriceAsCompTotal 
            Caption         =   "Set Standard Price to Sum of &Component Prices"
            Height          =   252
            Left            =   120
            TabIndex        =   74
            Top             =   360
            WhatsThisHelpID =   17374162
            Width           =   4530
         End
         Begin LookupViewControl.LookupView navGridKit 
            Height          =   285
            Left            =   1500
            TabIndex        =   105
            Top             =   1620
            WhatsThisHelpID =   17374163
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
         Begin FPSpreadADO.fpSpread grdCompKits 
            Height          =   4485
            Left            =   60
            TabIndex        =   76
            Top             =   930
            WhatsThisHelpID =   17374164
            Width           =   9435
            _Version        =   524288
            _ExtentX        =   16642
            _ExtentY        =   7911
            _StockProps     =   64
            AllowCellOverflow=   -1  'True
            DisplayRowHeaders=   0   'False
            EditModeReplace =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GrayAreaBackColor=   -2147483633
            ScrollBarExtMode=   -1  'True
            SpreadDesigner  =   "Imzmr001.frx":24CE
            AppearanceStyle =   0
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   1
         Left            =   -74925
         TabIndex        =   35
         Top             =   420
         Width           =   9495
         _Version        =   65536
         _ExtentX        =   16748
         _ExtentY        =   9560
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame Frame2 
            Height          =   1275
            Left            =   4920
            TabIndex        =   173
            Top             =   120
            Width           =   4575
            Begin VB.CheckBox chkAllowDecQuantities 
               Caption         =   "Allow &Decimal Quantities"
               Height          =   285
               Left            =   240
               TabIndex        =   67
               Top             =   360
               WhatsThisHelpID =   17374141
               Width           =   2595
            End
            Begin SOTADropDownControl.SOTADropDown ddnAdjQtyRoundMeth 
               Height          =   315
               Left            =   1545
               TabIndex        =   69
               Top             =   840
               Width           =   2940
               _ExtentX        =   5186
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnAdjQtyRoundMeth"
               Sorted          =   0   'False
            End
            Begin VB.Label lblRoundMeth 
               Caption         =   "Round &Method"
               Height          =   255
               Left            =   240
               TabIndex        =   68
               Top             =   870
               Width           =   1095
            End
         End
         Begin VB.Frame fraStdUOM 
            Caption         =   "&Units of Measure"
            Height          =   1275
            Left            =   30
            TabIndex        =   71
            Top             =   120
            Width           =   4725
            Begin EntryLookupControls.TextLookup lkuPrice 
               Height          =   285
               Left            =   3120
               TabIndex        =   66
               Top             =   840
               WhatsThisHelpID =   17374165
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "UnitOfMeasure"
               ParentIDColumn  =   "UnitMeasID"
               ParentKeyColumn =   "UnitMeasKey"
               ParentTable     =   "tciUnitMeasure"
               BoundColumn     =   "PriceUnitMeasKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "UnitMeasID,lkuPrice,;"
            End
            Begin EntryLookupControls.TextLookup lkuSales 
               Height          =   285
               Left            =   720
               TabIndex        =   64
               Top             =   840
               WhatsThisHelpID =   17374166
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "UnitOfMeasure"
               ParentIDColumn  =   "UnitMeasID"
               ParentKeyColumn =   "UnitMeasKey"
               ParentTable     =   "tciUnitMeasure"
               BoundColumn     =   "SalesUnitMeasKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "UnitMeasID,,;"
            End
            Begin EntryLookupControls.TextLookup lkuPurchaseUOM 
               Height          =   285
               Left            =   3120
               TabIndex        =   62
               Top             =   360
               WhatsThisHelpID =   17374167
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "UnitOfMeasure"
               ParentIDColumn  =   "UnitMeasID"
               ParentKeyColumn =   "UnitMeasKey"
               ParentTable     =   "tciUnitMeasure"
               BoundColumn     =   "PurchUnitMeasKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "UnitMeasID,lkuPurchaseUOM,;"
            End
            Begin EntryLookupControls.TextLookup lkuStockUOM 
               Height          =   285
               Left            =   720
               TabIndex        =   60
               Top             =   360
               WhatsThisHelpID =   17374171
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "UnitOfMeasure"
               ParentIDColumn  =   "UnitMeasID"
               ParentKeyColumn =   "UnitMeasKey"
               ParentTable     =   "tciUnitMeasure"
               BoundColumn     =   "StockUnitMeasKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "UnitMeasID,lkuStockUOM,;"
            End
            Begin VB.Label lblStockUOM 
               Caption         =   "&Stock"
               Height          =   255
               Left            =   120
               TabIndex        =   59
               Top             =   420
               Width           =   1245
            End
            Begin VB.Label lblPurchase 
               Caption         =   "&Purchase"
               Height          =   195
               Left            =   2280
               TabIndex        =   61
               Top             =   420
               Width           =   855
            End
            Begin VB.Label lblSales 
               Caption         =   "S&ales"
               Height          =   195
               Left            =   120
               TabIndex        =   63
               Top             =   900
               Width           =   990
            End
            Begin VB.Label lblPrice 
               Caption         =   "P&rice"
               Height          =   195
               Left            =   2280
               TabIndex        =   65
               Top             =   900
               Width           =   720
            End
         End
         Begin VB.TextBox txtNavReturn 
            Height          =   288
            Left            =   1620
            TabIndex        =   103
            Top             =   3420
            Visible         =   0   'False
            WhatsThisHelpID =   17374168
            Width           =   1452
         End
         Begin LookupViewControl.LookupView navGrid 
            Height          =   285
            Left            =   1140
            TabIndex        =   104
            Top             =   3420
            Visible         =   0   'False
            WhatsThisHelpID =   17374169
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
         Begin FPSpreadADO.fpSpread grdUOM 
            Height          =   3735
            Left            =   30
            TabIndex        =   70
            Top             =   1560
            WhatsThisHelpID =   17374170
            Width           =   9435
            _Version        =   524288
            _ExtentX        =   16642
            _ExtentY        =   6588
            _StockProps     =   64
            AllowCellOverflow=   -1  'True
            DisplayRowHeaders=   0   'False
            EditModeReplace =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            GrayAreaBackColor=   -2147483633
            ScrollBarExtMode=   -1  'True
            SpreadDesigner  =   "Imzmr001.frx":291E
            AppearanceStyle =   0
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   0
         Left            =   -74865
         TabIndex        =   7
         Top             =   495
         Width           =   9495
         _Version        =   65536
         _ExtentX        =   16748
         _ExtentY        =   9560
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.CommandButton cmdCustField 
            Caption         =   "Cu&stom Fields..."
            Height          =   350
            Left            =   4785
            TabIndex        =   56
            Top             =   4890
            WhatsThisHelpID =   17776506
            Width           =   1515
         End
         Begin VB.Frame fraItemInfo 
            Caption         =   "Item Inf&ormation"
            Height          =   3510
            Left            =   60
            TabIndex        =   8
            Top             =   30
            Width           =   9375
            Begin VB.CheckBox chkHazMat 
               Alignment       =   1  'Right Justify
               Caption         =   "Hazardous Material"
               Height          =   300
               Left            =   4710
               TabIndex        =   39
               Top             =   2865
               WhatsThisHelpID =   17774037
               Width           =   1845
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtSerialNumberIncrement 
               Height          =   285
               Left            =   2115
               TabIndex        =   20
               Top             =   2175
               WhatsThisHelpID =   17774038
               Width           =   2280
               _Version        =   65536
               _ExtentX        =   4022
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   20
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtSerialNumberMask 
               Height          =   285
               Left            =   2115
               TabIndex        =   18
               Top             =   1785
               WhatsThisHelpID =   17774039
               Width           =   2280
               _Version        =   65536
               _ExtentX        =   4022
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   20
            End
            Begin SOTADropDownControl.SOTADropDown ddnFixedAssetTemplate 
               Height          =   315
               Left            =   6360
               TabIndex        =   36
               Top             =   1770
               WhatsThisHelpID =   17374172
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   556
               BackColor       =   -2147483633
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnFixedAssetTemplate"
            End
            Begin VB.CheckBox chkInternal 
               Alignment       =   1  'Right Justify
               Caption         =   "Internal Long Desc"
               Height          =   285
               Left            =   4710
               TabIndex        =   37
               Top             =   2175
               WhatsThisHelpID =   17374173
               Width           =   1850
            End
            Begin VB.CheckBox chkSeasonal 
               Alignment       =   1  'Right Justify
               Caption         =   "Seasonal Item"
               Height          =   285
               Left            =   4710
               TabIndex        =   38
               Top             =   2520
               WhatsThisHelpID =   17374174
               Width           =   1850
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtDateEstb 
               Height          =   285
               Left            =   2115
               TabIndex        =   25
               Top             =   2970
               WhatsThisHelpID =   17374175
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SOTADropDownControl.SOTADropDown ddnSalesTaxClass 
               Height          =   315
               Left            =   6360
               TabIndex        =   33
               Top             =   1380
               WhatsThisHelpID =   17374176
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnSalesTaxClass"
            End
            Begin EntryLookupControls.TextLookup lkuFreightClass 
               Height          =   285
               Left            =   6360
               TabIndex        =   29
               Top             =   645
               WhatsThisHelpID =   17374177
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMFreigh"
               ParentIDColumn  =   "FreightClassID"
               ParentKeyColumn =   "FreightClassKey"
               ParentTable     =   "timFreightClass"
               BoundColumn     =   "FreightClassKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "FreightClassID,lkuFreightClass,;"
            End
            Begin EntryLookupControls.TextLookup lkuCommoCode 
               Height          =   285
               Left            =   6360
               TabIndex        =   31
               Top             =   1020
               WhatsThisHelpID =   17374178
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMCmCdDes   "
               ParentIDColumn  =   "CommodityCodeID"
               ParentKeyColumn =   "CommodityCodeKey"
               ParentTable     =   "timCommodityCode"
               BoundColumn     =   "CommodityCodeKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "CommodityCodeID,lkuCommoCode,;Description,,;"
            End
            Begin EntryLookupControls.TextLookup lkuDefltWarehouse 
               Height          =   285
               Left            =   6360
               TabIndex        =   27
               Top             =   300
               WhatsThisHelpID =   17374179
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   503
               ForeColor       =   -2147483640
               Enabled         =   0   'False
               MaxLength       =   6
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Warehouse"
               ParentIDColumn  =   "WhseID"
               ParentKeyColumn =   "WhseKey"
               ParentTable     =   "timWarehouse"
               BoundColumn     =   "DfltWhseKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "WhseID,lkuDefltWarehouse,;Description,,;"
            End
            Begin NEWSOTALib.SOTANumber numShelfLife 
               Height          =   285
               Left            =   2115
               TabIndex        =   22
               Top             =   2580
               WhatsThisHelpID =   17374180
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#"
               text            =   "    0"
               dMaxValue       =   9999
               sIntegralPlaces =   5
               sDecimalPlaces  =   0
            End
            Begin SOTADropDownControl.SOTADropDown ddnTrackMethod 
               Height          =   315
               Left            =   2115
               TabIndex        =   16
               Top             =   1380
               WhatsThisHelpID =   17374181
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnTrackMethod"
            End
            Begin SOTADropDownControl.SOTADropDown ddnStatus 
               Height          =   315
               Left            =   2115
               TabIndex        =   12
               Top             =   645
               WhatsThisHelpID =   17374182
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnStatus"
            End
            Begin EntryLookupControls.TextLookup lkuItemClass 
               Height          =   285
               Left            =   2115
               TabIndex        =   14
               Top             =   1020
               WhatsThisHelpID =   17374184
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   12
               IsSurrogateKey  =   -1  'True
               LookupID        =   "ItemClass"
               ParentIDColumn  =   "ItemClassID"
               ParentKeyColumn =   "ItemClassKey"
               ParentTable     =   "timItemClass"
               BoundColumn     =   "ItemClassKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "ItemClassID,lkuItemClass,;"
            End
            Begin SOTADropDownControl.SOTADropDown ddnItemType 
               Height          =   315
               Left            =   2115
               TabIndex        =   10
               Top             =   270
               WhatsThisHelpID =   17374185
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnItemType"
            End
            Begin NEWSOTALib.SOTANumber nbrPercentResidue 
               Height          =   300
               Left            =   7080
               TabIndex        =   41
               Top             =   2445
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "         0.00"
               sIntegralPlaces =   10
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber nbrHeatCapacity 
               Height          =   300
               Left            =   7080
               TabIndex        =   43
               Top             =   3045
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "         0.00"
               sIntegralPlaces =   10
               sDecimalPlaces  =   2
            End
            Begin VB.Label Label9 
               Caption         =   "Heat Capacity (BTU/Lb F)"
               Height          =   225
               Left            =   7080
               TabIndex        =   42
               Top             =   2805
               Width           =   2190
            End
            Begin VB.Label Label8 
               Caption         =   "% Residue"
               Height          =   225
               Left            =   7080
               TabIndex        =   40
               Top             =   2260
               Width           =   1110
            End
            Begin VB.Label lblHazMat 
               Height          =   255
               Left            =   4680
               TabIndex        =   170
               Top             =   2880
               Width           =   1695
            End
            Begin VB.Label lblSerialNumberIncrement 
               Caption         =   "Serial Number Increment"
               Height          =   240
               Left            =   180
               TabIndex        =   19
               Top             =   2190
               Width           =   2070
            End
            Begin VB.Label lblSerialNumberMask 
               Caption         =   "Serial Number Mask"
               Height          =   225
               Left            =   180
               TabIndex        =   17
               Top             =   1800
               Width           =   1515
            End
            Begin VB.Label lblFixedAssetTemplate 
               Caption         =   "Fixed Asset Template"
               Height          =   195
               Left            =   4710
               TabIndex        =   34
               Top             =   1800
               Width           =   1515
            End
            Begin VB.Label lblItemType 
               Caption         =   "Item Type"
               Height          =   195
               Left            =   180
               TabIndex        =   9
               Top             =   330
               Width           =   1830
            End
            Begin VB.Label lblStatus 
               Caption         =   "Status"
               Height          =   195
               Left            =   180
               TabIndex        =   11
               Top             =   705
               Width           =   1830
            End
            Begin VB.Label lblItemClass 
               Caption         =   "Item Class"
               Height          =   195
               Left            =   180
               TabIndex        =   13
               Top             =   1080
               Width           =   1830
            End
            Begin VB.Label lblTrackMeth 
               Caption         =   "Track Method"
               Height          =   195
               Left            =   180
               TabIndex        =   15
               Top             =   1440
               Width           =   1830
            End
            Begin VB.Label lblShelfLife 
               Caption         =   "Shelf Life"
               Height          =   195
               Left            =   180
               TabIndex        =   21
               Top             =   2595
               Width           =   1830
            End
            Begin VB.Label lblDefWareHouse 
               Caption         =   "Default Warehouse"
               Height          =   195
               Left            =   4710
               TabIndex        =   26
               Top             =   330
               Width           =   1455
            End
            Begin VB.Label lblCommCode 
               Caption         =   "Commodity Code"
               Height          =   195
               Left            =   4710
               TabIndex        =   30
               Top             =   1080
               Width           =   1395
            End
            Begin VB.Label lblSTaxCls 
               Caption         =   "Sales Tax Class"
               Height          =   195
               Left            =   4710
               TabIndex        =   32
               Top             =   1440
               Width           =   1275
            End
            Begin VB.Label lblFreightCls 
               Caption         =   "Freight Class"
               Height          =   195
               Left            =   4710
               TabIndex        =   28
               Top             =   705
               Width           =   1335
            End
            Begin VB.Label lblDateEstb 
               Caption         =   "Date Established"
               Height          =   195
               Left            =   180
               TabIndex        =   24
               Top             =   3000
               Width           =   1830
            End
            Begin VB.Label lblDays 
               Caption         =   "Days"
               Height          =   195
               Left            =   4110
               TabIndex        =   23
               Top             =   2625
               Width           =   435
            End
         End
         Begin VB.Frame fraCostValuation 
            Caption         =   "Cost/&Valuation"
            Height          =   1665
            Left            =   75
            TabIndex        =   44
            Top             =   3600
            Width           =   4575
            Begin SOTADropDownControl.SOTADropDown ddnValuation 
               Height          =   315
               Left            =   2100
               TabIndex        =   46
               ToolTipText     =   " "
               Top             =   270
               WhatsThisHelpID =   17374186
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnValuation"
            End
            Begin NEWSOTALib.SOTANumber numStdPrice 
               Height          =   285
               Left            =   2100
               TabIndex        =   50
               Top             =   1020
               WhatsThisHelpID =   17374188
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.#####"
               text            =   "         0.00000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   5
            End
            Begin NEWSOTALib.SOTANumber numStdCost 
               Height          =   285
               Left            =   2100
               TabIndex        =   48
               Top             =   660
               WhatsThisHelpID =   17374189
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.#####"
               text            =   "         0.00000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   5
            End
            Begin VB.Label lblValMeth 
               Caption         =   "Valuation Method"
               Height          =   195
               Left            =   180
               TabIndex        =   45
               Top             =   330
               Width           =   1810
            End
            Begin VB.Label lblStdCost 
               Caption         =   "Standard Cost"
               Height          =   195
               Left            =   180
               TabIndex        =   47
               Top             =   690
               Width           =   1815
            End
            Begin VB.Label lblStdPrice 
               Caption         =   "Standard Price"
               Height          =   195
               Left            =   180
               TabIndex        =   49
               Top             =   1050
               Width           =   1815
            End
         End
         Begin VB.Frame fraWarranty 
            Caption         =   "&Warranty"
            Height          =   1170
            Left            =   4770
            TabIndex        =   51
            Top             =   3600
            Width           =   4680
            Begin SOTADropDownControl.SOTADropDown ddnProvider 
               Height          =   315
               Left            =   1530
               TabIndex        =   53
               Top             =   270
               WhatsThisHelpID =   17374190
               Width           =   2940
               _ExtentX        =   5186
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnProvider"
            End
            Begin NEWSOTALib.SOTANumber numNOfDays 
               Height          =   285
               Left            =   1530
               TabIndex        =   55
               Top             =   660
               WhatsThisHelpID =   17374191
               Width           =   2940
               _Version        =   65536
               _ExtentX        =   5177
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#"
               text            =   "    0"
               dMaxValue       =   32676
               sIntegralPlaces =   5
               sDecimalPlaces  =   0
            End
            Begin VB.Label lblProvider 
               Caption         =   "Provider"
               Height          =   195
               Left            =   180
               TabIndex        =   52
               Top             =   330
               Width           =   1260
            End
            Begin VB.Label lblNOfDays 
               Caption         =   "Number of Days"
               Height          =   255
               Left            =   180
               TabIndex        =   54
               Top             =   690
               Width           =   1260
            End
         End
         Begin VB.CommandButton cmdSubstitution 
            Caption         =   "&Substitutions..."
            Height          =   350
            Left            =   6367
            TabIndex        =   57
            Top             =   4890
            WhatsThisHelpID =   17374192
            Width           =   1515
         End
         Begin VB.CommandButton cmdLandCost 
            Caption         =   "L&anded Cost..."
            Height          =   350
            Left            =   7950
            TabIndex        =   58
            Top             =   4890
            WhatsThisHelpID =   17374193
            Width           =   1515
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   4
         Left            =   -74955
         TabIndex        =   110
         Top             =   465
         Width           =   9525
         _Version        =   65536
         _ExtentX        =   16801
         _ExtentY        =   9560
         _StockProps     =   15
         Caption         =   " "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraSalesOptions 
            Caption         =   "Options"
            Height          =   2505
            Left            =   5265
            TabIndex        =   133
            Top             =   90
            Width           =   4140
            Begin VB.CheckBox chkDropShipFlg 
               Caption         =   "Allow Drop Ships"
               Height          =   285
               Left            =   120
               TabIndex        =   135
               Top             =   760
               WhatsThisHelpID =   17374142
               Width           =   3270
            End
            Begin VB.CheckBox chkReturnsAllowed 
               Caption         =   "Allow Returns"
               Height          =   285
               Left            =   120
               TabIndex        =   134
               Top             =   360
               WhatsThisHelpID =   17374139
               Width           =   3510
            End
            Begin VB.CheckBox chkAllowPriceOvrd 
               Caption         =   "Allow Price Override"
               Height          =   285
               Left            =   120
               TabIndex        =   136
               Top             =   1160
               WhatsThisHelpID =   17374138
               Width           =   3720
            End
            Begin VB.CheckBox chkSubToTradeDisc 
               Caption         =   "Subject to Trade Discount"
               Height          =   285
               Left            =   120
               TabIndex        =   137
               Top             =   1560
               WhatsThisHelpID =   17374137
               Width           =   3285
            End
            Begin VB.CheckBox chkInclOnPackList 
               Caption         =   "Include On Packing List"
               Height          =   285
               Left            =   120
               TabIndex        =   138
               Top             =   1960
               WhatsThisHelpID =   17374134
               Width           =   3195
            End
         End
         Begin VB.Frame fraSalesProdLine 
            Caption         =   "Sal&es "
            Height          =   4155
            Left            =   150
            TabIndex        =   111
            Top             =   90
            Width           =   4920
            Begin NEWSOTALib.SOTANumber numDfltSaleQty 
               Height          =   285
               Left            =   2340
               TabIndex        =   130
               Top             =   3370
               WhatsThisHelpID =   17774040
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "       1.0000"
               sIntegralPlaces =   8
               sDecimalPlaces  =   4
               dValue          =   1
            End
            Begin NEWSOTALib.SOTANumber numMinGrossProfitPct 
               Height          =   285
               Left            =   2340
               TabIndex        =   120
               Top             =   1440
               WhatsThisHelpID =   17774041
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numTargetMargin 
               Height          =   285
               Left            =   2340
               TabIndex        =   118
               Top             =   1050
               WhatsThisHelpID =   17374145
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin EntryLookupControls.TextLookup lkuCommCls 
               Height          =   285
               Left            =   2340
               TabIndex        =   116
               Top             =   660
               WhatsThisHelpID =   17374146
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "CommissionClass"
               ParentIDColumn  =   "CommClassID"
               ParentKeyColumn =   "CommClassKey"
               ParentTable     =   "tarCommClass"
               BoundColumn     =   "CommClassKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "CommClassID,,;"
            End
            Begin EntryLookupControls.TextLookup lkuProdLine 
               Height          =   285
               Left            =   2340
               TabIndex        =   114
               Top             =   270
               WhatsThisHelpID =   17374147
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMSaleP"
               ParentIDColumn  =   "SalesProdLineID"
               ParentKeyColumn =   "SalesProdLineKey"
               ParentTable     =   "timSalesProdLine"
               BoundColumn     =   "SalesProdLineKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "SalesProdLineID,lkuProdLine,;"
            End
            Begin NEWSOTALib.SOTANumber numPriceSeq 
               Height          =   285
               Left            =   2340
               TabIndex        =   122
               Top             =   1830
               WhatsThisHelpID =   17374148
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,###|,##<ILp0>#"
               text            =   "          0"
               dMaxValue       =   2147483647
               sIntegralPlaces =   11
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber numMandatorySalesMulti 
               Height          =   285
               Left            =   2340
               TabIndex        =   132
               Top             =   3760
               WhatsThisHelpID =   17374149
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#"
               text            =   "    0"
               dMaxValue       =   32767
               sIntegralPlaces =   5
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber numMinSalesQty 
               Height          =   285
               Left            =   2340
               TabIndex        =   128
               Top             =   2990
               WhatsThisHelpID =   17374150
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "       0.0000"
               sIntegralPlaces =   8
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber numRestockingRates 
               Height          =   285
               Left            =   2340
               TabIndex        =   126
               Top             =   2600
               WhatsThisHelpID =   17374151
               Width           =   2295
               _Version        =   65536
               _ExtentX        =   4048
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,##<ILp0>#<IRp0>|.######"
               text            =   "     0.000000"
               sIntegralPlaces =   6
               sDecimalPlaces  =   6
            End
            Begin EntryLookupControls.TextLookup lkuCustPriceGroup 
               Height          =   285
               Left            =   2340
               TabIndex        =   124
               Top             =   2220
               WhatsThisHelpID =   17374152
               Width           =   2295
               _ExtentX        =   4048
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "ProductPriceGroup"
               ParentIDColumn  =   "ProdPriceGroupID"
               ParentKeyColumn =   "ProdPriceGroupKey"
               ParentTable     =   "timProdPriceGroup"
               BoundColumn     =   "ProdPriceGroupKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "ProdPriceGroupID,lkuCustPriceGroup,;"
            End
            Begin NEWSOTALib.SOTANumber numTargetMarginHide 
               Height          =   225
               Left            =   60
               TabIndex        =   112
               TabStop         =   0   'False
               Top             =   1260
               Visible         =   0   'False
               WhatsThisHelpID =   17374144
               Width           =   2175
               _Version        =   65536
               _ExtentX        =   3836
               _ExtentY        =   397
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               mask            =   "<ILH>###|,##<ILp0>#<IRp0>|.######"
               text            =   "     0.000000"
               sIntegralPlaces =   6
               sDecimalPlaces  =   6
            End
            Begin NEWSOTALib.SOTANumber numMinGrossProfitPctHide 
               Height          =   225
               Left            =   60
               TabIndex        =   171
               TabStop         =   0   'False
               Top             =   1650
               Visible         =   0   'False
               WhatsThisHelpID =   17776507
               Width           =   2175
               _Version        =   65536
               _ExtentX        =   3836
               _ExtentY        =   397
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               mask            =   "<ILH>###|,##<ILp0>#<IRp0>|.######"
               text            =   "     0.000000"
               sIntegralPlaces =   6
               sDecimalPlaces  =   6
            End
            Begin NEWSOTALib.SOTANumber numRestockingRatesHide 
               Height          =   225
               Left            =   60
               TabIndex        =   172
               TabStop         =   0   'False
               Top             =   2805
               Visible         =   0   'False
               WhatsThisHelpID =   17374143
               Width           =   2175
               _Version        =   65536
               _ExtentX        =   3836
               _ExtentY        =   397
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               mask            =   "<ILH>###|,##<ILp0>#<IRp0>|.######"
               text            =   "     0.000000"
               sIntegralPlaces =   6
               sDecimalPlaces  =   6
            End
            Begin VB.Label lbDfltSaleQty 
               Caption         =   "Default Sales Quantity"
               Height          =   270
               Left            =   180
               TabIndex        =   129
               Top             =   3400
               Width           =   1890
            End
            Begin VB.Label lblMinGrossProfitPct 
               Caption         =   "Minimum Gross Profit Pct"
               Height          =   255
               Left            =   180
               TabIndex        =   119
               Top             =   1470
               Width           =   1890
            End
            Begin VB.Label lblTargetMargin 
               Caption         =   "Target Margin"
               Height          =   195
               Left            =   180
               TabIndex        =   117
               Top             =   1080
               Width           =   1890
            End
            Begin VB.Label lblCommClass 
               Caption         =   "Commission Class"
               Height          =   255
               Left            =   180
               TabIndex        =   115
               Top             =   690
               Width           =   1890
            End
            Begin VB.Label lblProdLine 
               Caption         =   "Product Line"
               Height          =   195
               Left            =   180
               TabIndex        =   113
               Top             =   300
               Width           =   1890
            End
            Begin VB.Label lblPrioceSeq 
               Caption         =   "Price Sequence"
               Height          =   255
               Left            =   180
               TabIndex        =   121
               Top             =   1860
               Width           =   1890
            End
            Begin VB.Label lblCustPriceGroup 
               Caption         =   "Product Price Group"
               Height          =   255
               Left            =   180
               TabIndex        =   123
               Top             =   2250
               Width           =   1890
            End
            Begin VB.Label lblRestockingRates 
               Caption         =   "Restocking Rate"
               Height          =   255
               Left            =   180
               TabIndex        =   125
               Top             =   2630
               Width           =   1890
            End
            Begin VB.Label lblMinSalesQty 
               Caption         =   "Minimum Sales Quantity"
               Height          =   195
               Left            =   180
               TabIndex        =   127
               Top             =   3020
               Width           =   1890
            End
            Begin VB.Label lblManSalesMulti 
               Caption         =   "Mandatory Sales Multiple"
               Height          =   195
               Left            =   180
               TabIndex        =   131
               Top             =   3790
               Width           =   1890
            End
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   5
         Left            =   -74985
         TabIndex        =   139
         Top             =   390
         Width           =   9585
         _Version        =   65536
         _ExtentX        =   16907
         _ExtentY        =   9551
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraGLAcct 
            Caption         =   "GL &Accounts"
            Height          =   1785
            Left            =   90
            TabIndex        =   140
            Top             =   300
            Width           =   9375
            Begin EntryLookupControls.GLAcctLookup glaReturns 
               Height          =   285
               Left            =   2595
               TabIndex        =   141
               Top             =   1320
               WhatsThisHelpID =   17374125
               Width           =   5430
               _ExtentX        =   9578
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "ReturnsAcctKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   2325
               WidthAcct       =   2445
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaReturns,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaSales 
               Height          =   285
               Left            =   2595
               TabIndex        =   142
               Top             =   960
               WhatsThisHelpID =   17374126
               Width           =   5430
               _ExtentX        =   9578
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesAcctKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   2325
               WidthAcct       =   2445
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaSales,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaExpense 
               Height          =   285
               Left            =   2595
               TabIndex        =   143
               Top             =   600
               WhatsThisHelpID =   17374127
               Width           =   5430
               _ExtentX        =   9578
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "ExpAcctKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   2325
               WidthAcct       =   2445
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaExpense,;Description,,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaCOS 
               Height          =   285
               Left            =   2595
               TabIndex        =   144
               Top             =   225
               WhatsThisHelpID =   17374128
               Width           =   5430
               _ExtentX        =   9578
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "COSAcctKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   2325
               WidthAcct       =   2445
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaCOS,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaInventory 
               Height          =   285
               Left            =   2595
               TabIndex        =   145
               Top             =   600
               Visible         =   0   'False
               WhatsThisHelpID =   17374129
               Width           =   5430
               _ExtentX        =   9578
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "ExpAcctKey"
               BoundTable      =   "timItem"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   2325
               WidthAcct       =   2445
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaExpense,;"
            End
            Begin VB.Label lblInventoryGL 
               Caption         =   "Inventory"
               Height          =   195
               Left            =   180
               TabIndex        =   150
               Top             =   660
               Visible         =   0   'False
               Width           =   2300
            End
            Begin VB.Label lblReturns 
               Caption         =   "Returns"
               Height          =   195
               Left            =   180
               TabIndex        =   149
               Top             =   1380
               Width           =   2300
            End
            Begin VB.Label lblSalesAcct 
               Caption         =   "Sales"
               Height          =   195
               Left            =   180
               TabIndex        =   148
               Top             =   1020
               Width           =   2300
            End
            Begin VB.Label lblExpense 
               Caption         =   "Expense"
               Height          =   195
               Left            =   180
               TabIndex        =   147
               Top             =   660
               Width           =   2300
            End
            Begin VB.Label lblCOS 
               Caption         =   "COGS"
               Height          =   195
               Left            =   180
               TabIndex        =   146
               Top             =   300
               Width           =   2300
            End
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5310
         Index           =   6
         Left            =   -74925
         TabIndex        =   151
         Top             =   555
         Width           =   9495
         _Version        =   65536
         _ExtentX        =   16748
         _ExtentY        =   9366
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraProductCatogories 
            Caption         =   "Product Categories"
            Height          =   2110
            Left            =   0
            TabIndex        =   161
            Top             =   0
            Width           =   9465
            Begin VB.ListBox lstAvailProdCatID 
               Height          =   1425
               Left            =   1545
               MultiSelect     =   2  'Extended
               Sorted          =   -1  'True
               TabIndex        =   167
               Top             =   525
               WhatsThisHelpID =   17374204
               Width           =   2625
            End
            Begin VB.ListBox lstAssignProdCatID 
               Height          =   1425
               Left            =   5175
               MultiSelect     =   2  'Extended
               Sorted          =   -1  'True
               TabIndex        =   169
               Top             =   525
               WhatsThisHelpID =   17374203
               Width           =   2625
            End
            Begin VB.CommandButton cmdMoveItems 
               Caption         =   ">>"
               Height          =   315
               Index           =   0
               Left            =   4395
               TabIndex        =   165
               Top             =   480
               WhatsThisHelpID =   17374202
               Width           =   555
            End
            Begin VB.CommandButton cmdMoveItems 
               Caption         =   ">"
               Height          =   315
               Index           =   1
               Left            =   4395
               TabIndex        =   164
               Top             =   885
               WhatsThisHelpID =   17374201
               Width           =   555
            End
            Begin VB.CommandButton cmdMoveItems 
               Caption         =   "<"
               Height          =   315
               Index           =   2
               Left            =   4395
               TabIndex        =   163
               Top             =   1275
               WhatsThisHelpID =   17374200
               Width           =   555
            End
            Begin VB.CommandButton cmdMoveItems 
               Caption         =   "<<"
               Height          =   315
               Index           =   3
               Left            =   4395
               TabIndex        =   162
               Top             =   1680
               WhatsThisHelpID =   17374199
               Width           =   555
            End
            Begin VB.Label lblAvailItemID 
               Caption         =   "Available Product Categories "
               Height          =   195
               Left            =   1545
               TabIndex        =   166
               Top             =   285
               Width           =   2535
            End
            Begin VB.Label lblCategoriesAssignedtoItem 
               Caption         =   "Categories Assigned to Item"
               Height          =   195
               Left            =   5175
               TabIndex        =   168
               Top             =   285
               Width           =   2895
            End
         End
         Begin VB.Frame fraItemImages 
            Caption         =   "Item Images"
            Height          =   2640
            Left            =   0
            TabIndex        =   152
            Top             =   2370
            Width           =   9465
            Begin VB.CommandButton cmdMoveUp 
               Caption         =   "Move U&p"
               Height          =   360
               Left            =   5325
               TabIndex        =   158
               Top             =   780
               WhatsThisHelpID =   17374197
               Width           =   1080
            End
            Begin VB.CommandButton cmdMoveDown 
               Caption         =   "Move &Down"
               Height          =   360
               Left            =   5325
               TabIndex        =   157
               Top             =   1260
               WhatsThisHelpID =   17374196
               Width           =   1080
            End
            Begin VB.CommandButton cmdGetImage 
               Caption         =   "B&rowse Network"
               Height          =   350
               Left            =   3150
               TabIndex        =   156
               Top             =   2200
               WhatsThisHelpID =   17374195
               Width           =   1375
            End
            Begin VB.CommandButton cmdSearchInternet 
               Caption         =   "Browse In&ternet "
               Height          =   350
               Left            =   4740
               TabIndex        =   155
               Top             =   2200
               WhatsThisHelpID =   17374194
               Width           =   1375
            End
            Begin VB.Frame fraItemImageDisplay 
               Enabled         =   0   'False
               Height          =   2375
               Left            =   6480
               TabIndex        =   153
               Top             =   160
               Width           =   2865
               Begin SHDocVwCtl.WebBrowser webItemImage 
                  Height          =   2145
                  Left            =   90
                  TabIndex        =   154
                  TabStop         =   0   'False
                  Top             =   180
                  WhatsThisHelpID =   17374219
                  Width           =   2685
                  ExtentX         =   4736
                  ExtentY         =   3784
                  ViewMode        =   0
                  Offline         =   0
                  Silent          =   0
                  RegisterAsBrowser=   0
                  RegisterAsDropTarget=   1
                  AutoArrange     =   0   'False
                  NoClientEdge    =   0   'False
                  AlignLeft       =   0   'False
                  NoWebView       =   0   'False
                  HideFileNames   =   0   'False
                  SingleClick     =   0   'False
                  SingleSelection =   0   'False
                  NoFolders       =   0   'False
                  Transparent     =   0   'False
                  ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
                  Location        =   "http:///"
               End
            End
            Begin FPSpreadADO.fpSpread grdItemImages 
               Height          =   1845
               Left            =   150
               TabIndex        =   159
               TabStop         =   0   'False
               Top             =   240
               WhatsThisHelpID =   17374198
               Width           =   5100
               _Version        =   524288
               _ExtentX        =   8996
               _ExtentY        =   3254
               _StockProps     =   64
               AllowCellOverflow=   -1  'True
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               RowHeaderDisplay=   0
               ScrollBars      =   2
               SpreadDesigner  =   "Imzmr001.frx":2D67
               AppearanceStyle =   0
            End
            Begin VB.Label lblItemImage 
               Caption         =   "Please choose an image."
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   375
               Left            =   6600
               TabIndex        =   160
               Top             =   960
               Width           =   2535
            End
         End
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5415
         Index           =   8
         Left            =   -74880
         TabIndex        =   174
         Top             =   435
         Width           =   9495
         _Version        =   65536
         _ExtentX        =   16748
         _ExtentY        =   9551
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   5430
         Index           =   7
         Left            =   120
         TabIndex        =   175
         Top             =   435
         Width           =   9480
         _Version        =   65536
         _ExtentX        =   16722
         _ExtentY        =   9578
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin FPSpreadADO.fpSpread grdCOALabTests 
            Height          =   4695
            Left            =   0
            TabIndex        =   180
            Top             =   720
            Width           =   9495
            _Version        =   524288
            _ExtentX        =   16748
            _ExtentY        =   8281
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "Imzmr001.frx":3198
         End
         Begin SOTADropDownControl.SOTADropDown ddnCOARpts 
            Height          =   315
            Left            =   6600
            TabIndex        =   179
            TabStop         =   0   'False
            Top             =   120
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Style           =   2
            Text            =   "ddnCOARpts"
         End
         Begin NEWSOTALib.SOTANumber txtViscosityTemp 
            Height          =   285
            Left            =   1680
            TabIndex        =   177
            Top             =   120
            WhatsThisHelpID =   17374189
            Width           =   1575
            _Version        =   65536
            _ExtentX        =   2778
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.#####"
            text            =   "         0.00000"
            sIntegralPlaces =   10
            sDecimalPlaces  =   5
         End
         Begin SOTADropDownControl.SOTADropDown ddnMethStd 
            Height          =   315
            Left            =   0
            TabIndex        =   181
            TabStop         =   0   'False
            Top             =   1200
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Style           =   2
            Text            =   "ddnMethStd"
         End
         Begin SOTADropDownControl.SOTADropDown ddnUOM 
            Height          =   315
            Left            =   2280
            TabIndex        =   182
            TabStop         =   0   'False
            Top             =   1200
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Style           =   2
            Text            =   "ddnUOM"
         End
         Begin VB.Label lblViscosityTemp 
            Caption         =   "Viscosity Temp (F):"
            Height          =   195
            Left            =   240
            TabIndex        =   176
            Top             =   150
            Width           =   1335
         End
         Begin VB.Label lblCOATemplate 
            Caption         =   "COA Template:"
            Height          =   255
            Left            =   5400
            TabIndex        =   178
            Top             =   120
            Width           =   1095
         End
      End
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   101
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9870
      _ExtentX        =   17410
      _ExtentY        =   741
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   60
      Left            =   -645
      TabIndex        =   100
      Top             =   0
      Width           =   6945
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   89
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   90
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   91
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   92
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   93
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   94
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   95
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   96
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   97
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   99
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin MSComctlLib.ImageList imlToolbar 
      Left            =   3270
      Top             =   885
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Shape shpFocusRect 
      Height          =   300
      Left            =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   555
   End
   Begin VB.Label lblLongDesc 
      Caption         =   "Lo&ng Description"
      Height          =   195
      Left            =   4800
      TabIndex        =   4
      Top             =   930
      Width           =   1395
   End
   Begin VB.Label lblShortDesc 
      Caption         =   "S&hort Description"
      Height          =   195
      Left            =   4800
      TabIndex        =   2
      Top             =   600
      Width           =   1395
   End
   Begin VB.Label lblItem 
      Caption         =   "Item"
      Height          =   195
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   530
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   98
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Image imgNoDrop 
      Height          =   480
      Left            =   2055
      Top             =   930
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgDrop 
      Height          =   480
      Left            =   1575
      Top             =   915
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgDrag 
      Appearance      =   0  'Flat
      Height          =   480
      Left            =   960
      Picture         =   "Imzmr001.frx":35AC
      Top             =   945
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "frmMaintainItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmMaintainItem
'     Desc: Maintain Items
'     Original: 03-24-1998
'     Mods:
'****************************************************************************************************************
'modifications
'****************************************************************************************************************
'11-26-07   Defect 36792        ddellomo        Change to set Sales multiple = 0 if Allow decimal Qty Checked
'                                               This will prevent the sales multiple from ever being checked
'                                               when allowing decimals
'****************************************************************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    'Private Variables of Form Properties
    Private moClass             As Object
    Private mlRunMode           As Long
    Private mlItemKey           As Long         'Item Key in edit case
    Private mlMyKey             As Long         'Item Key of a new Item to be used to enter UOM values for BTO
    Private mlCompKeyReturn     As Long         'Key for the Component Item returned from the Component Validation procedure
    Private mlUOMKeyReturn      As Long         'Key for the UOM returned from the UOM Validation procedure
    Private miSecurityLevel     As Integer
    Private miFilter            As Integer
    Private miMinShelfLife      As Integer      'Shelflife variable used while entering Kit Items
    Private miSavedLife         As Integer      'Value of saved shelf life according to Kit
    Private miStat              As Integer      'Status at entry
    Private miHoldSaveTab       As Integer      'Tab when saving the record
    Private miUOMOrKit          As Integer      'For distinguishing between UOM and KIT grid
    Private msPriceUOM          As String
    Private msUOMValue          As String       'Value of the UOM lookup text to use with AddUOMRow
    Private msGotValue          As String       'Got focus value of Stock UOM
    Private msItemClass         As String       'Value of the ItemClass when entering the control
    Private msCommVal           As String       'Commodity Code on selection
    Private msKitCompVal        As String       'Component Item if entered for the row. Used when changing values to avoid invalid entry
    Private msUOMVal            As String       'UOM if entered for the row. Used when changing values to avoid invalid entry
    Private msItemHistory       As String
    Private msStockStatus       As String
    Private msLotSummary        As String
    Private msSerialSummary     As String
    Private msInvtTran          As String
    Private msCostInq           As String
    Private msPriceInq          As String
    Private msUOMIDReturn       As String       'UOM ID that has been validated
    Private mbSaved             As Boolean      'Proterty
    Private mbCancelShutDown    As Boolean      'Property
    Private mbDmLoad            As Boolean      'While loading DataManager
    Private mbExistingID        As Boolean      'Made true when ID found and made false at Query unload
    Private mbBrowseRec         As Boolean      'Used only using browse button
    Private mbOnKitGrid         As Boolean      'True than right click on kit grid
    Private mbOnUOMGrid         As Boolean      'True than right click on UOM grid
    Private mbIRIsActivated     As Boolean
    Private moUF                As Object
    Private msUserFld(5)        As String
    Private mbUOMChangeByEvent  As Boolean      'Used to determine when to fire messages on UseStandardConversion check
    Private mbAddDefRow         As Boolean      'Used on row append to differentiate between from lookup entry vs grid entry
    Private mbInSaveRoutine     As Boolean
    
    'Constants for cboMoveItems Button Icons
    Private Const kSelectAll = 0       '>>
    Private Const kSelect = 1          '>
    Private Const kDeselect = 2        '<
    Private Const kDeselectAll = 3     '<<
    
    Private msItemImageLocation As String       'Used for image location
    Private mbAssignedProdCatsSaveIsNeeded As Boolean
    Private mbClearListBox As Boolean
    Private miRowHeight As Integer
    Private msImageName As String
    Private mbLoadAvailProd As Boolean
    Private mbLoadAssignProd As Boolean
    Private mbIMIsActivated As Boolean   'IM Module has been activated?
    Private mbECIsActivated As Boolean   'EC Module has been activated?
    Private mbSOIsActivated As Boolean   'SO Module has been activated?

    Private mbDontChkclick      As Boolean      'Global don't run click logic
    Private mbEnterID           As Boolean      'CHECKOUT
    Private mbStatChange        As Boolean      'Status has been changed (ss)
    Private mbPPLtoType         As Boolean      'On changing PPL set ItemType -- prevents other changes on item type change
    Private mbOnce              As Boolean
    Private mbValChange         As Boolean      'For Catalog Items when changed
    Private mbShelfValChange    As Boolean
    Private mbKitValMiss        As Boolean
    Private mbNoComp            As Boolean
    Private mbNoUOM             As Boolean
    Private mbNoKit             As Boolean
    Private mbInvalidComponent  As Boolean
    Private mbInvalidUOM        As Boolean
    Private mbStillOnUOM        As Boolean
    Private mbStillOnKit        As Boolean
    Private mbFromPreSave       As Boolean
    Private mbRename            As Boolean
    Private mbSaveDone          As Boolean
    Private mbRedoUOM           As Boolean
    Private mbIsPressF5         As Boolean       'True if F5 key is pressed
    Private mbNewRecord         As Boolean
    Private mbIsCancelButton    As Boolean
    Private mbDelUOM            As Boolean
    Private mbTBRejectUOM       As Boolean
    Private mbKitMessageDisplayed As Boolean
    Private mbFromInvt          As Boolean
    Private mbCallFromSave      As Boolean
    Private mbIsErrorMessage    As Boolean
    Private mbAlreadyValidated  As Boolean
    Private bChkLookupClick     As Boolean
    Private mbDefaultValuation  As Boolean
    Private miHoldValuation     As Integer
    Private mbValuationChanged  As Boolean
    Private mbFromTrack         As Boolean
    Private mbCatTypeChanged    As Boolean
    Private mlPASalesItemKey    As Long
    Private mbIsClickCancel     As Boolean
    Private mfQty               As Double
    Private mfPriceAsGot        As Double           'Unit price as entered
    Private mdDate              As Date

    Private moConvTypeList      As clsStaticList
    
    'Miscellaneous Variables
    Private mbEnterAsTab          As Boolean
    Private miTrackSTaxOnSales    As Integer          'TrackSalesTax flag in AROptions
    Private msCompanyID           As String
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private miPriceDecimal        As Integer          'For decimal values
    Private miCostDecimal         As Integer          'For decimal values
    Private miQtyDecimal          As Integer          'For decimal values
    Private miValidateUPCBarCodes As Integer
    Private msConversionTypeOld   As String
    Private mbUseStdConvOld       As Boolean
    Private mbDontGiveWarningMsg  As Boolean
    Private mbNoInvtImgShare      As Boolean          'To track if an inventory image share path exists in CI Options.

    'Grid Navigator Class
    Private moGridNav           As clsGridLookup
    Private moGridNavKit        As clsGridLookup

    Private moOptions           As New clsModuleOptions
    Private moContextMenu       As New clsContextMenu   'Context Menu Object

    'Public Form Variables
    Public moSotaObjects        As New Collection
    Public miModule             As Integer               'Calling module

    'Binding Form Object Variables
    Private WithEvents moDmItemForm     As clsDmForm
Attribute moDmItemForm.VB_VarHelpID = -1
    Private WithEvents moDmItemDescForm As clsDmForm
Attribute moDmItemDescForm.VB_VarHelpID = -1
    Private WithEvents moDmKitForm      As clsDmForm
Attribute moDmKitForm.VB_VarHelpID = -1
    Private WithEvents m_CopyMgr        As clsCopyMgr
Attribute m_CopyMgr.VB_VarHelpID = -1

    'Binding Grid Object Variables
    Private WithEvents moDmUOMGrid      As clsDmGrid
Attribute moDmUOMGrid.VB_VarHelpID = -1
    Private WithEvents moDmKitCompGrid  As clsDmGrid
Attribute moDmKitCompGrid.VB_VarHelpID = -1

'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (START)
'*****************************************************************************************************
'*****************************************************************************************************
    Private WithEvents moDmItemExt  As clsDmForm
Attribute moDmItemExt.VB_VarHelpID = -1
    Private WithEvents moDmCOATestsGrid  As clsDmGrid
Attribute moDmCOATestsGrid.VB_VarHelpID = -1
'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (STOP)
'*****************************************************************************************************
'*****************************************************************************************************

    Private WithEvents moDmItemImage    As clsDmGrid
Attribute moDmItemImage.VB_VarHelpID = -1
    Private miImageCurrentRow As Long
    
    'Grid Managers
    Private WithEvents moGMUOM          As clsGridMgr       'Grid Manager
Attribute moGMUOM.VB_VarHelpID = -1
    Private WithEvents moGMKitComp      As clsGridMgr       'Grid Manager
Attribute moGMKitComp.VB_VarHelpID = -1
    Private WithEvents moGMItemImage    As clsGridMgr
Attribute moGMItemImage.VB_VarHelpID = -1
     
    '************************************************************************************************
    '************************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '************************************************************************************************
    '************************************************************************************************
    Private WithEvents moGMCOATests     As clsGridMgr       'Grid Manager
Attribute moGMCOATests.VB_VarHelpID = -1
    '************************************************************************************************
    '************************************************************************************************
    'SGS DEJ 5/18/12    (STOP)
    '************************************************************************************************
    '************************************************************************************************
     
    'Constants for UOM Grid columns.
    Private Const kMaxUOMCols = 14
    Private Const kUOMColTargetUOM = 1
    Private Const kUOMColUseStdConv = 2
    Private Const kUOMColUseForPurchases = 3
    Private Const kUOMColUseForSales = 4
    Private Const kUOMColConversionType = 5
    Private Const kUOMColConversionTypeInt = 6
    Private Const kUOMColConversionFactor = 7
    Private Const kUOMColConversionFactorDBValue = 8
    Private Const kUOMColVolume = 9
    Private Const kUOMColWeight = 10
    Private Const kUOMColUPC = 11
    Private Const kUOMColUOMKey = 12
    Private Const kUOMColRowFromDB = 13
    Private Const kUOMColIsSmallestUOM = 14
    
    'Constants for Kit Grid columns.
    Private Const kMaxKitCols = 8
    Private Const kKitColComponentItem = 1
    Private Const kKitColDesc = 2
    Private Const kKitColQuantity = 3
    Private Const kKitColstkUOM = 4
    Private Const kKitColSeqNo = 5
    Private Const kKitColCompKey = 6
    Private Const kKitColType = 7
    Private Const kKitColDQAllow = 8
         
    '************************************************************************************************
    '************************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '************************************************************************************************
    '************************************************************************************************
    'Constants for Kit Grid columns.
    Private Const kMaxCOACols = 11
    Private Const kCOAColLabTestRqd = 1
    Private Const kCOAColLabTestKey = 2
    Private Const kCOAColLabTestID = 3
    Private Const kCOAColMethStdKey = 4
    Private Const kCOAColMethStdID = 5
    Private Const kCOAColUOMKey = 6
    Private Const kCOAColUOMID = 7
    Private Const kCOAColCommOpp = 8
    Private Const kCOAColValRng01 = 9
    Private Const kCOAColValRng02 = 10
    Private Const kCOAColTestSort = 11
    
'    Private Const kCOAColRptKey = 11
'    Private Const kCOAColRptID = 12
    '************************************************************************************************
    '************************************************************************************************
    'SGS DEJ 5/18/12    (STOP)
    '************************************************************************************************
    '************************************************************************************************
    
    'Constants for Item CreateType
    Private Const kCreateTypeStandard = 1
         
    'Constants for Item Images grid columns.
    Private Const kMaxItemImageCols = 4
    Private Const kItemIColImageKey = 1
    Private Const kItemIColImageNo = 2
    Private Const kItemIColImageName = 3
    Private Const kItemIColImageNameLength = 20
    Private Const kItemIColImagePath = 4
    Private Const kItemIColImagePathLength = 128
   
    'Constants for processes in PopulateVal.
    Private Const kProcessSalesProdLine = 1
    Private Const kProcessPurchProdLine = 2
    Private Const kProcessItem = 3
    Private Const kProcessItemClass = 4
    Private Const kProcessVolWght = 5
    Private Const kProcessTrackMethod = 7
    Private Const kProcessGL = 10
    Private Const kProcessKitComp = 11
    Private Const kProcessRestockRate = 12
    Private Const kProcessStatus = 13
    Private Const kProcessItemType = 14
    Private Const kProcessCommCode = 15
    Private Const kProcessKitDeletes = 16
    Private Const kProcessUOMDeletes = 17
    Private Const kProcessKitCompDelete = 18
    Private Const kProcessGetItemClass = 20
    Private Const kProcessGetSerialDefault = 21
    
    'Item Type Constants
    Private Const kItemTypeMiscItem = 1
    Private Const kItemTypeService = 2
    Private Const kItemTypeExpense = 3
    Private Const kItemTypeCommentOnly = 4
    Private Const kItemTypeFinishedGood = 5
    Private Const kItemTypeRawMaterial = 6
    Private Const kItemTypeBTOKit = 7
    Private Const kItemTypeAssembledKit = 8
    Private Const kItemTypeCatalog = 9
    
    'Tab Constants
    Private Const kTabDesc = 0                         'Item Description Tab
    Private Const kTabUOM = 1                          'UOM Tab
    Private Const kTabKit = 2                          'Kit Component Tab
    Private Const kTabPurch = 3                        'Purchase Tab
    Private Const kTabSales = 4                        'Sales Tab
    Private Const kTabGLAcct = 5                       'GL Tab
    Private Const kTabCategories = 6                   'Categories Tab
'SGS DEJ (START)
    Private Const kTabCOATests = 7                   'COA Tests Tab
'SGS DEJ (STOP)
    
    'Miscellaneous Constants
    Private Const kNotMeasure = 10
    Private Const kChecked = 1                         'Checkbox checked
    Private Const kUnchecked = 0                       'Checkbox unchecked
    Private Const kFirstCell = 1                       'First cell on grid
    Private Const kModuleIR = 16

    'Tracking Method Constants
    Private Const kTrackNone = 0            'None
    Private Const kTrackLot = 1             'Lot
    Private Const kTrackSerial = 2          'Serial
    Private Const kTrackBoth = 3            'Both
    Private Const kNoValue = 5              'When tracking method not found.
    
    'Valuation Method Constants
    Private Const kValNone = 0              'None
    Private Const kValAvg = 1               'Avg
    Private Const kValStd = 2               'Std
    Private Const kValFIFO = 3              'FIFO
    Private Const kValLIFO = 4              'LIFO
    Private Const kValActual = 5            'Actual
    
    Private Const kProvNone = 0             'None
    Private Const kProvVendor = 1           'Vendor
    Private Const kProvCompany = 2          'Company
    
    'Status Constants
    Private Const kItemStatActive = 1       'Active
    Private Const kItemStatInactive = 2     'Inactive
    Private Const kItemStatDiscontinued = 3 'Discontinued
    Private Const kItemStatDeleted = 4      'Deleted
    
    Private Const kDAItemHistory = 2000
    Private Const kDAStockStatus = 3000
    Private Const kDASerialSummary = 4000
    Private Const kDALotSummary = 5000
    Private Const kDAInvtTran = 6000
    Private Const kDACostInq = 7000
    Private Const kDAPriceInq = 8000
    
    'Enum for AdjQtyRoundMeth
    Private Enum RoundMeth
        DownToPrec = 0
        DownToWhole = 1
        ToNearestPrec = 2
        ToNearestWhole = 3
        None = 4
    End Enum
    
    'Enum for UOM Lookup controls
    Private Enum UOMLookUpControl
        Stock = 1
        Purch = 2
        Sales = 3
        Price = 4
    End Enum
    
    Const ktskItemHistoryInquiry = 117637190
    Const ktskStockStausInquiry = 117637200
    Const ktskLotSummary = 117637150
    Const ktskSerialSummary = 117637140
    Const ktskPriceInquiry = 117637170       'imzqb001.clsPriceInquiry
    
    'Serial Mask Constants
    Const kNumeric = "#"        'Numeric Only
    Const kIncrRuleChar = "^"   'Increment Rule character
    Const kAlpha = "a"
    Const kAlphaNumeric = "x"
    
    Private mbCompBlankMessageDisplayed As Boolean
    Private tempStr As String
    Private ReadOnlyUI As Boolean                       'For indicating that the UI is display only

    'Security Events Constants
    Private Const ksecChangeCost = "CHGITMCOST"
    Private Const ksecChangePrice = "CHGITMPRC"
    Private Const ksecChangeItStat = "CHGITMSTAT"
    
    Private mbErrorFlashed As Boolean
    Private mbMFActivated As Boolean                   'Whether Manufacturing Module is Activated
    
    'FAS Integration
    Private mbIntegrateWithFAS As Boolean
   
    Const VBRIG_MODULE_ID_STRING = "IMZMR001.FRM"
    
Private Function bDenyControlFocus() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bDenyControlFocus = False
    
    If (Len(Trim$(lkuMain)) = 0) Then
        If lkuMain.Enabled And lkuMain.Visible Then
            lkuMain.SetFocus
            bDenyControlFocus = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDenyControlFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'****************************************************************
'This function determines whether or not there are any pending
'transactions for this Item.  It does so only by checking the
'QtyOnXX flags in timInventory for all warehouses.  This is not
'the best way to do this, but it was chosen because the bug
'surfaced late in the SES 6.0 development cycle.  A SCOPUS
'Defect was written up to improve this technique in the next
'SES release.  This was done for SCOPUS Defect 21153 -- CEL.
'****************************************************************
Private Function bItemHasPendingTrans() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rsInventory As DASRecordSet
    Dim lInventoryCount As Long

    bItemHasPendingTrans = False
    lInventoryCount = 0

    sSQL = "SELECT COUNT(*) InvCount FROM timInventory "
    sSQL = sSQL & "WHERE ItemKey = " & Format$(mlItemKey) & Space$(1)
    sSQL = sSQL & "AND (QtyOnBO > 0 OR QtyOnPO > 0 OR QtyOnSO > 0 OR QtyOnWO > 0 "
    sSQL = sSQL & "OR QtyOnTrnsfr > 0 OR QtyReqForTrnsfr > 0 OR QtyReqForWO > 0)"
    Set rsInventory = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    With rsInventory
        'Did we get a row back (we should have)?
        If Not .IsEOF Then
            'Yes, we DID get a row back.
            lInventoryCount = glGetValidLong(.Field("InvCount"))
        Else
            'Error processing
            If Not rsInventory Is Nothing Then
                rsInventory.Close
                Set rsInventory = Nothing
            End If

            Exit Function
        End If

        'Close the recordset opened above.
        If Not rsInventory Is Nothing Then
            rsInventory.Close
            Set rsInventory = Nothing
        End If
    End With

    'If the count is greater than zero, then this Item has pending transactions.
    If lInventoryCount > 0 Then
        bItemHasPendingTrans = True
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bItemHasPendingTrans", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'SGS DEJ    (START)
Private Sub grdCOALabTests_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    moGMCOATests_CellChange Row, Col
End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_Change(ByVal Col As Long, ByVal Row As Long)
    On Error GoTo Error
    
    moGMCOATests.Grid_Change Col, Row
'    moGMCOATests_CellChange lRow, lCol

'    Private Const kCOAColLabTestRqd = 1
'    Private Const kCOAColLabTestKey = 2
'    Private Const kCOAColLabTestID = 3

'    Private Const kCOAColMethStdKey = 4
'    Private Const kCOAColMethStdID = 5

'    Private Const kCOAColUOMKey = 6
'    Private Const kCOAColUOMID = 7

'    Private Const kCOAColCommOpp = 8
'    Private Const kCOAColValRng01 = 9
'    Private Const kCOAColValRng02 = 10

'    Private Const kCOAColTestSort = 11
    
    Dim lcKeyVal As Long
    Dim lcIDVal As String
    
    Select Case Col
        Case kCOAColMethStdID
            'Get the Method Standar Key
            lcIDVal = gsGridReadCellText(grdCOALabTests, Row, kCOAColMethStdID)
            lcKeyVal = glGetValidLong(moClass.moAppDB.Lookup("MethodStandardKey", "timLabMethodStandards_SGS", "MethodStandardID = " & gsQuoted(lcIDVal)))
            
            'Update the grid and DM with the Key
            gGridUpdateCell grdCOALabTests, Row, kCOAColMethStdKey, CStr(lcKeyVal)
            
            'Set the DM dirty
            moDmCOATestsGrid.SetRowDirty Row
            
        Case kCOAColUOMID
            'Get the Unit Of Measure Key
            lcIDVal = gsGridReadCellText(grdCOALabTests, Row, kCOAColUOMID)
            lcKeyVal = glGetValidLong(moClass.moAppDB.Lookup("LabUOMKey", "timLabUOM_SGS", "LabUOMID = " & gsQuoted(lcIDVal)))
            
            'Update the grid and DM with the Key
            gGridUpdateCell grdCOALabTests, Row, kCOAColUOMKey, CStr(lcKeyVal)
            
            'Set the DM dirty
            moDmCOATestsGrid.SetRowDirty Row
            
'        Case kCOAColRptID
'            'Get the Crystal Report Template Key
'            lcIDVal = gsGridReadCellText(grdCOALabTests, Row, kCOAColRptID)
'            lcKeyVal = glGetValidLong(moClass.moAppDB.Lookup("COATemplateReportKey", "timCOATemplateReports_SGS", "COATemplateReportID = " & gsQuoted(lcIDVal)))
'
'            'Update the grid and DM with the Key
'            gGridUpdateCell grdCOALabTests, Row, kCOAColRptKey, CStr(lcKeyVal)
'
'            'Set the DM dirty
'            moDmCOATestsGrid.SetRowDirty Row
            
        Case kCOAColCommOpp
            'Check the value and lock the range 02 cell if not between
            lcIDVal = UCase(gsGridReadCellText(grdCOALabTests, Row, kCOAColCommOpp))
            
            If lcIDVal = "BETWEEN" Then
                gGridUnlockCell grdCOALabTests, kCOAColValRng02, Row
            Else
                gGridLockCell grdCOALabTests, kCOAColValRng02, Row
                
                'Set the Range Value 2 to 0
                gGridUpdateCell grdCOALabTests, Row, kCOAColValRng02, CStr(0)
            End If
            
            'Set the DM dirty
            moDmCOATestsGrid.SetRowDirty Row
    End Select
        
'    If lCol = kUOMColConversionType Then
'        iConvType = moConvTypeList.DBValue(gsGridReadCellText(grdUOM, lRow, kUOMColConversionType))
'        gGridUpdateCell grdUOM, lRow, kUOMColConversionTypeInt, CStr(iConvType)
'        moDmUOMGrid.SetRowDirty lRow  'The row is not being set to dirty on change of Conversion type - force that here
'    End If

    Exit Sub
Error:
    MsgBox Me.Name & ".grdCOALabTests_Change() " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_Click(ByVal Col As Long, ByVal Row As Long)
    
    moGMCOATests.Grid_Click Col, Row
    

End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
    moGMCOATests.Grid_ColWidthChange Col1

End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    moGMCOATests.Grid_EditMode Col, Row, Mode, ChangeMade

End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_KeyDown(KeyCode As Integer, Shift As Integer)
    moGMCOATests.Grid_KeyDown KeyCode, Shift

End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    moGMCOATests.Grid_LeaveCell Col, Row, NewCol, NewRow
    
End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
    moGMCOATests.Grid_LeaveRow Row, NewRow

End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_RightClick(ByVal ClickType As Integer, ByVal Col As Long, ByVal Row As Long, ByVal MouseX As Long, ByVal MouseY As Long)
    moGMCOATests.MenuDelete = False
End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub grdCOALabTests_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    moGMCOATests.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

End Sub
'SGS DEJ    (STOP)

'SGS DEJ    (START)
Private Sub moDmCOATestsGrid_DMGridRowLoaded(lRow As Long)
    On Error GoTo Error
    
'    Private Const kCOAColLabTestRqd = 1
'    Private Const kCOAColLabTestKey = 2
'    Private Const kCOAColLabTestID = 3

'    Private Const kCOAColMethStdKey = 4
'    Private Const kCOAColMethStdID = 5

'    Private Const kCOAColUOMKey = 6
'    Private Const kCOAColUOMID = 7

'    Private Const kCOAColCommOpp = 8
'    Private Const kCOAColValRng01 = 9
'    Private Const kCOAColValRng02 = 10

'    Private Const kCOAColTestSort = 11
    
    
    DoEvents
    
    Dim lcKeyVal As Long
    Dim lcIDVal As String
    
    'COA Test
    lcKeyVal = glGetValidLong(moDmCOATestsGrid.GetColumnValue(lRow, "LabTestKey"))
    lcIDVal = gsGetValidStr(moClass.moAppDB.Lookup("LabTestID", "timLabTests_SGS", "LabTestKey = " & lcKeyVal))
    gGridUpdateCell grdCOALabTests, lRow, kCOAColLabTestID, lcIDVal
    
'    grdCOALabTests.Row = lRow
'    grdCOALabTests.Col = kCOAColLabTestID
    
    'Method Standard
    lcKeyVal = glGetValidLong(moDmCOATestsGrid.GetColumnValue(lRow, "MethodStandardKey"))
    lcIDVal = gsGetValidStr(moClass.moAppDB.Lookup("MethodStandardID", "timLabMethodStandards_SGS", "MethodStandardKey = " & lcKeyVal))
    gGridUpdateCellText grdCOALabTests, lRow, kCOAColMethStdID, lcIDVal

    
    'Unit Measure
    lcKeyVal = glGetValidLong(moDmCOATestsGrid.GetColumnValue(lRow, "UnitOfMeasureKey"))
    lcIDVal = gsGetValidStr(moClass.moAppDB.Lookup("LabUOMID", "timLabUOM_SGS", "LabUOMKey = " & lcKeyVal))
    gGridUpdateCellText grdCOALabTests, lRow, kCOAColUOMID, lcIDVal
    
'    'Crystal Reports COA Template
'    lcKeyVal = glGetValidLong(moDmCOATestsGrid.GetColumnValue(lRow, "COACrystalReportKey"))
'    lcIDVal = gsGetValidStr(moClass.moAppDB.Lookup("COATemplateReportID", "timCOATemplateReports_SGS", "COATemplateReportKey = " & lcKeyVal))
'    gGridUpdateCellText grdCOALabTests, lRow, kCOAColRptID, lcIDVal
    
    'Check for the range Operator
    lcIDVal = UCase(gsGetValidStr(moDmCOATestsGrid.GetColumnValue(lRow, "ComparisonOperator")))
    If lcIDVal = "BETWEEN" Then
        gGridUnlockCell grdCOALabTests, kCOAColValRng02, lRow
    Else
        gGridLockCell grdCOALabTests, kCOAColValRng02, lRow
    End If

    DoEvents
    Exit Sub
Error:
    MsgBox Me.Name & ".moDmCOATestsGrid_DMGridRowLoaded() " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub
'SGS DEJ    (STOP)

Private Sub moDmUOMGrid_DMGridBeforeUpdate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    bValid = False
    
    If Not bIsValidConvFactor(lRow) Then
        Exit Sub
    End If
    
    SetConvFactorDBValue (lRow)
    
    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "moDmUOMGrid_DMGridBeforeUpdate", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMCOATests_CellChange(ByVal lRow As Long, ByVal lCol As Long)
    'SGS DEJ (Add Code here)
End Sub


Private Sub moGMCOATests_GridBeforeDelete(bContinue As Boolean)
    'SGS DEJ (Add Code here)

End Sub

Private Sub moGMItemImage_GridAfterDelete()
    grdItemImages.Refresh
    SetItemImagePict False
End Sub

Private Sub moGMKitComp_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lUOMKey As Long
    Dim lKey As Long
    Dim lPrCnt As Long

    If (lCol <> kKitColComponentItem _
    And lCol <> kKitColQuantity) Then
            Exit Sub
        End If

    If lCol = kKitColQuantity _
    And (chkPriceAsCompTotal.Value = 1) _
    And Len(Trim$(gsGridReadCellText(grdCompKits, lRow, kKitColComponentItem))) > 0 Then
        numStdPrice.ClearData
        
        For lPrCnt = 1 To grdCompKits.MaxRows - 1
            AddPrice lPrCnt
        Next
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    'close move
    If bIsValidKitComponent(lRow) Then
        lUOMKey = mlCompKeyReturn
        mlCompKeyReturn = Empty
        
        If lCol = kKitColQuantity Then
            SpecialKitValue lRow
        End If

        'Calculate the Price for the Kit even if the Item or quantity is changed.
        If mbKitValMiss = True Then
            mbKitValMiss = False
            mbNoComp = True
            Exit Sub
        Else
            mbNoComp = False
        End If

        'If not on component item column, exit.
        If lCol = kKitColComponentItem Then
            'Check for recursion.
            With moClass.moAppDB
                .SetInParamLong lUOMKey
                .SetInParamLong mlItemKey
                .SetOutParam lKey
                .ExecuteSP "spimKitCompCheck"
                 lKey = .GetOutParam(3)
                .ReleaseParams
             End With
    
            'Prompt the user of the error and disallow the choice.
            If lKey <> 0 Then
                giSotaMsgBox Me, moClass.moSysSession, 160121
                grdCompKits.Value = ""
                grdCompKits.SetCellDirtyFlag kKitColComponentItem, lRow, False
                grdCompKits.Action = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
    
            moDmKitCompGrid.SetColumnValue lRow, "CompItemKey", IIf(lUOMKey = 0, 1, lUOMKey)
            moDmKitCompGrid.SetColumnValue lRow, "SeqNo", IIf(lRow = 0, 1, lRow)
            moDmKitCompGrid.DoLinks lRow
    
            mfQty = gdGetValidDbl(Trim$(gsGridReadCell(grdCompKits, lRow, kKitColQuantity)))
            PopulateVal kProcessKitComp
    
            'This code will chec for decimal quantity option for
            'component and change the cell type accordingly.
            grdCompKits.Col = kKitColDQAllow
            If grdCompKits.Value = 0 Then
                'This means that AllowDecimal = 0.
                gGridSetCellType grdCompKits, lRow, kKitColQuantity, SS_CELL_TYPE_FLOAT, 0
            End If
        End If
    Else
        'Added by Ashish on March 05, 1999 for supressing duplicate message in case of Lostfocus
        'and g_navcell change in cascade.
        mbCompBlankMessageDisplayed = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMKitComp_CellChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
                
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub moGMKitComp_GridAfterDelete()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    ' Clear that row with blank on component kit.
    grdCompKits.Col = kKitColComponentItem
    grdCompKits.Value = ""
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMUOM_GridAfterDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub moGMKitComp_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

   bContinue = False
    
     'Ccheck if Kit value is to be the total of the component values
     'and there is a right click on the grid.
    If (chkPriceAsCompTotal.Value = 1) _
    And (mbOnKitGrid Or tabMaintainItem.Tab = kTabKit) Then 'CHANGE SS
         'Work out the StdPrice value for the kit cumalatively
         mfQty = gsGridReadCell(grdCompKits, grdCompKits.ActiveRow, kKitColQuantity)
         PopulateVal kProcessKitCompDelete
         mbOnKitGrid = False
     End If
     
     'Attempt to shift rows up to adjust the Seq no
     If Not bMoveRecords Then
         Exit Sub
     End If


    bContinue = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMKitComp_GridBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub moGMUOM_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lUOMKey As Long
    Dim lUOMID As String

    If lCol = kUOMColConversionFactor Or lCol = kUOMColConversionType Then
        'Validate that no existing transactions exist; if so, disallow the change.
        'If this is an existing UOM (not one just added), then check to see
        'if there is any existing transaction activity using this UOM.
        grdUOM.Row = lRow
        grdUOM.Col = kUOMColRowFromDB
    
        If grdUOM.Value = "1" Then
            'This means we ARE dealing with an existing UOM.
            'So, now check for activity in existing transactions using timInventory flags.
            If bItemHasPendingTrans() = True Then
                'Give an error message to the user.
                giSotaMsgBox Me, moClass.moSysSession, kmsgCantChgUOMDueToTrans
            
                'Put the old Conversion Factor and Type back
                gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, CDec(gsGridReadCell(grdUOM, lRow, kUOMColConversionFactorDBValue))
                gGridUpdateCellText grdUOM, lRow, kUOMColConversionType, msConversionTypeOld
                
                grdUOM.Action = 0
                Exit Sub
            End If
        End If
    End If

    If (lCol <> kUOMColTargetUOM _
    And lCol <> kUOMColUseStdConv) Then
        Exit Sub
    End If

    'If the column is Use Standard Conversion.
    If lCol = kUOMColUseStdConv Then
        If mbDontGiveWarningMsg = False Then
            'Validate that no existing transactions exist; if so, disallow the change.
            'If this is an existing UOM (not one just added), then check to see
            'if there is any existing transaction activity using this UOM.
            grdUOM.Row = lRow
            grdUOM.Col = kUOMColRowFromDB
        
            If grdUOM.Value = "1" Then
                'This means we ARE dealing with an existing UOM.
                'So, now check for activity in existing transactions using timInventory flags.
                If bItemHasPendingTrans() = True Then
                    'Give an error message to the user.
                    giSotaMsgBox Me, moClass.moSysSession, kmsgCantChgUOMDueToTrans

                    'Put the old Use Standard Conversion value back.
                    mbDontGiveWarningMsg = True
                    grdUOM.Col = kUOMColUseStdConv
                    grdUOM.Value = mbUseStdConvOld
                    mbDontGiveWarningMsg = False

                    grdUOM.Action = 0
                    Exit Sub
                End If
            End If
        
            'Check if Use Std Conversion is checked.
            If gsGridReadCell(grdUOM, lRow, kUOMColUseStdConv) = kChecked Then
                'If Use Standard is checked by the user - not from within code - warn that we will override the entered factor
                'If user agrees, remove conversion factor and disable cell,
                'else uncheck the std checkbox.
                If Not mbUOMChangeByEvent Then
                    If giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgStdUsed) = vbYes Then
                        'Set the conversion type to the default value
                        gGridUpdateCell grdUOM, lRow, kUOMColConversionTypeInt, moConvTypeList.DefaultDBValue
                        gGridUpdateCellText grdUOM, lRow, kUOMColConversionType, moConvTypeList.LocalText(moConvTypeList.DefaultDBValue)
                        
                        'Show the rate that will be used for the standard conversion
                        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, CDec(dGetStandardUOMConv(lRow))
                        
                        'Lock the rate and type fields
                        gGridLockCell grdUOM, kUOMColConversionFactor, lRow
                        gGridLockCell grdUOM, kUOMColConversionType, lRow
                    Else
                        'Use said no - so revert back to unchecked
                        gGridUpdateCell grdUOM, lRow, kUOMColUseStdConv, "0"
                        mbUseStdConvOld = False
                    End If
                Else
                    'The box was checked by code - no asking - just perform the action if the user had said yes
                    'Set the conversion type to the default value
                    gGridUpdateCell grdUOM, lRow, kUOMColConversionTypeInt, moConvTypeList.DefaultDBValue
                    gGridUpdateCellText grdUOM, lRow, kUOMColConversionType, moConvTypeList.LocalText(moConvTypeList.DefaultDBValue)
                    
                    'Show the rate that will be used for the standard conversion
                    gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, CDec(dGetStandardUOMConv(lRow))
                    
                    'Lock the rate and type fields
                    gGridLockCell grdUOM, kUOMColConversionFactor, lRow
                    gGridLockCell grdUOM, kUOMColConversionType, lRow
                End If
            Else
                'Unlock the conversion factor cell - the standard factor will already be there
                gGridUnlockCell grdUOM, kUOMColConversionFactor, lRow
                gGridUnlockCell grdUOM, kUOMColConversionType, lRow
            End If
        End If
    End If

    If lCol <> kUOMColTargetUOM Then
        Exit Sub
    End If

    'When changing values in the Target UOM column.
    If lCol = kUOMColTargetUOM Then
        If bIsValidUOMID(lRow) Then
            lUOMKey = mlUOMKeyReturn
            lUOMID = msUOMIDReturn
            mlUOMKeyReturn = Empty
            msUOMIDReturn = ""
            moDmUOMGrid.SetColumnValue lRow, "TargetUnitMeasKey", IIf(lUOMKey = 0, 1, lUOMKey)
            moDmUOMGrid.DoLinks lRow
            'This code is getting called from save - we do not want to set the defaults in that case
            If Not mbInSaveRoutine Then
                SetUOMDefaultValues lRow
            End If
            DisableEnable lRow
        End If
    End If



'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMUOM_CellChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
                
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'****************************************************************
'This function will call everytime when user deletes row from grid
'If row is last then only row can be deleted else can't be deleted.
'****************************************************************
Private Sub moGMUOM_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lCounter As Long
    Dim lHoldCounter As Long
    Dim bFound As Boolean
    Dim lColValue As Long
    Dim lRowValue As Long
     

    'SS added 0907
    bContinue = False
        
    'Set the main datamanager to dirty
    If StrComp(Trim$(gsGridReadCellText(grdUOM, grdUOM.ActiveRow, kUOMColTargetUOM)), Trim$(lkuStockUOM.Text), vbTextCompare) = 0 Then
        lHoldCounter = grdUOM.ActiveRow
            
        For lCounter = 1 To grdUOM.MaxRows - 1
            If Not lCounter = lHoldCounter Then
                If StrComp(Trim$(gsGridReadCellText(grdUOM, lCounter, kUOMColTargetUOM)), Trim$(lkuStockUOM.Text), vbTextCompare) = 0 Then
                    bFound = True
                    Exit For
                End If
            End If
        Next
        
        If Not bFound Then
            MsgBox "Stock UOM"
            Exit Sub
        Else
            If lHoldCounter > lCounter Then
                'Do Nothing
            Else
                MsgBox "Delete this row as the other row might be in use already"
                gGridSetActiveCell grdUOM, lCounter, kUOMColTargetUOM
                Exit Sub
            End If
        End If
    End If
    
    'This code is added by Ashish On 26 Feb For delete in UOM grid
    'The 9 th column is set to "1" if the row is loaded from database.
    'You can not delete those rows.
    'Otherwise one can delete the added rows.
  
    lColValue = grdUOM.ActiveCol
    lRowValue = grdUOM.ActiveRow
    grdUOM.Col = kUOMColRowFromDB
    
    If grdUOM.Value <> "1" And lRowValue <> 1 Then
          
        mbOnUOMGrid = False
        moDmItemForm.SetDirty True
        
        'If the UOM row being deleted is also the purchases default, clear it
        If StrComp(Trim$(gsGridReadCellText(grdUOM, lRowValue, kUOMColTargetUOM)), lkuPurchaseUOM.Text, vbTextCompare) = 0 Then
            lkuPurchaseUOM.Text = ""
        End If
                     
        'If the UOM row being deleted is also the sales order default, clear it
        If StrComp(Trim$(gsGridReadCellText(grdUOM, lRowValue, kUOMColTargetUOM)), lkuSales.Text, vbTextCompare) = 0 Then
            lkuSales.Text = ""
        End If
    
    Else
        mbOnUOMGrid = False
        moDmItemForm.SetDirty False
        bContinue = False
        Exit Sub
    End If
      
    
    bContinue = True
    
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGMUOM_GridBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupNavigators()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
'  Desc: Sets up the Main navigator.
'  Set the rest up when they are clicked.
'**********************************************************************
    '-- Only need to set up one navigator for the accounts.
    gbLookupInit navGrid, moClass, moClass.moAppDB, "UnitOfMeasure", "CompanyID = " & gsQuoted(msCompanyID)

    '-- BTO and catalog items not to appear in the search window.
    gbLookupInit navGridKit, moClass, moClass.moAppDB, "Item", "CompanyID = " & gsQuoted(msCompanyID) & " AND NOT (ItemType = 7 OR ItemType = 9)"    'Earlier it was 8,3,9

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupNavigators", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindVM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    With SOTAVM
        Set .Framework = moClass.moFramework
        .Keys.Add lkuMain
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindVM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bIsValidItemID(Optional ItemID As String = "", _
                               Optional ErrNo As Long = 0, _
                               Optional ErrData As Variant = "") As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************
' Description:
'    This is the field validation routine. It loads the controls
'    if a valid ID is entered; otherwise it sets the form state to
'    Add.
' Param:   ItemID  Optional for Copy feature to valid the entered ItemID not
'                  the item id in the itemid control
'          ErrNo   Optional for Copy feature to return error number
'          ErrData Optional for Copy feature to return error data
' Returns:
'   True  if valid key is entered (new or existing record)
'   False if incomplete key or an error occurs
'**************************************************************************
    Dim bValid As Boolean
    Dim iKeyChangeCode As Integer
    Dim sVar As String
    Dim iItemType As Integer
    Dim lCopyItemKey    As Long
    
    Static bInHere As Boolean
    

    
    bIsValidItemID = True
    
    If bInHere Then
        Exit Function
    End If
    
    
    bInHere = True
    
    
    ' Determine if the CopyFrom ID entered is already a valid ID.
    If (Len(Trim(ItemID)) > 0) Then
        lCopyItemKey = glGetValidLong(moClass.moAppDB.Lookup("ItemKey", "timItem", _
                                      "ItemID = " & gsQuoted(ItemID) & _
                                      " AND ItemType = " & ddnItemType.ItemData & _
                                      " AND CompanyID = " & gsQuoted(msCompanyID)))
        If (lCopyItemKey = 0) Then
            bIsValidItemID = True
            GoTo ResetInHere
        Else
            bIsValidItemID = False
            ErrNo = kmsgDMNotUniqueGen
            GoTo ResetInHere
        End If
    Else
    
    
    
    
        mbDmLoad = True
        SetHourglass True
        
        moDmItemForm.SetDirty False
                
        'True if click on cancel button.
        mbIsClickCancel = False
        mbPPLtoType = False
        
        DoEvents
        
    If (mbECIsActivated Or mbSOIsActivated) Then
    
        mbLoadAvailProd = False
        mbLoadAssignProd = False
    End If
                    
    
        
        iKeyChangeCode = moDmItemForm.KeyChange()
        Select Case iKeyChangeCode
            Case Is = kDmKeyNotFound
                moDmItemForm.SetDirty True
                mbNewRecord = True
                moClass.lUIActive = kChildObjectActive
                mlItemKey = 0
                
                'Load product category into the list boxes.
                If (mbECIsActivated Or mbSOIsActivated) Then
                    If bLoadProductCategory(mlItemKey) = False Then
                        bIsValidItemID = False
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                        GoTo ResetInHere
                    End If
                    
                    mbLoadAvailProd = False
                    mbLoadAssignProd = False
                End If
                
            Case Is = kDmKeyFound
                'If choosing an inventory item from CI, do not allow.
                tabMaintainItem.Tab = kTabDesc
                mbNewRecord = False
                
                'Get the Item Type for this Item.
                iItemType = giGetValidInt(moClass.moAppDB.Lookup("ItemType", "timItem", "timItem.ItemID = " & gsQuoted(lkuMain.Text) & " AND timItem.CompanyID = " & gsQuoted(msCompanyID)))
    
                If miModule = kModuleCI Then
                    If iItemType > kItemTypeCommentOnly Then
                        giSotaMsgBox Me, moClass.moSysSession, kIMmsgExistingItem, gsQuoted(lkuMain.Text)
                        bIsValidItemID = False
                        HandleToolBarClick kTbCancel
                        SetHourglass False
    
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                        GoTo ResetInHere
                    End If
                End If
                
                'Set the Item Key for this item.
                mlItemKey = gvCheckNull(moClass.moAppDB.Lookup _
    ("ItemKey", "timItem", "ItemID = " & gsQuoted(lkuMain.Text) & _
    " AND ItemType = " & ddnItemType.ItemData & " AND CompanyID = " & _
    gsQuoted(msCompanyID)), SQL_INTEGER)
    
                'Load product category into the list boxes.
                If (mbECIsActivated Or mbSOIsActivated) Then
                    If bLoadProductCategory(mlItemKey) = False Then
                        bIsValidItemID = False
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                        GoTo ResetInHere
                    End If
                End If
                
                moClass.lUIActive = kChildObjectActive
                mbExistingID = True
                GetMyValues
                lkuPuchProdLine.Tag = lkuPuchProdLine.Text
                moDmItemForm.SetDirty False, True
           
            Case Is = kDmKeyNotComplete
                'Key not completely filled in.
                bIsValidItemID = False
                
            Case Is = kDmError
                'Database error occurred trying to get row.
                bIsValidItemID = False
             
            Case Is = kDmNotAllowed
                bIsValidItemID = False
                ReadOnlyUI = True
                
            Case Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, iKeyChangeCode
                bIsValidItemID = False
        End Select
        
        If bIsValidItemID Then
            '**********************************************************************************************
            'SGS DEJ    (START)
            '**********************************************************************************************
            Call GetMissingLabTests
            '**********************************************************************************************
            'SGS DEJ    (STOP)
            '**********************************************************************************************
            ValuesHere
        End If
        
        mbDmLoad = False
        SetHourglass False
    End If
    
    SetTBMemoState
    
ResetInHere:
    bInHere = False
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        bInHere = False
        
        gSetSotaErr Err, sMyName, "bIsValidItemID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select

'+++ VB/Rig End +++
End Function
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    FormHelpPrefix = "IMZ"    ' Put your identifier here
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    WhatHelpPrefix = "IMZ"    ' Put your identifier here

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   lRunMode contains the user-defined run mode context in which the
'   class object was Initialzed. The run mode is extracted from the
'   context passed in through the InitializeObject.
'************************************************************************
Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************************
'   bSaved is used specifically for Add-On-The-Fly.  It tells the
'   the calling object whether the record was saved or not.
'************************************************************************************
Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbSaved = bNewSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property




Private Sub chkAllowRtrnsPartialBTO_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowRtrnsPartialBTO, True
    #End If
'+++ End Customizer Code Push +++
    
     If (moDmItemForm.State <> sotaTB_NONE) And _
(ddnItemType.ItemData = kItemTypeBTOKit) Then
        If chkAllowRtrnsPartialBTO.Value = kUnchecked And _
chkReturnsAllowed.Value = kChecked Then
            If Not bCheckAllowReturnStatusForAllComponent(chkAllowRtrnsPartialBTO) Then
                chkAllowRtrnsPartialBTO.Value = kChecked
            End If
        End If
     End If

     
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkAllowRtrnsPartialBTO_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPriceAsCompTotal_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPriceAsCompTotal, True
    #End If
'+++ End Customizer Code Push +++

    Dim lPrCnt As Long
    Dim iResponse As Integer

    If (chkPriceAsCompTotal.Value = 1) Then
        numStdPrice.Enabled = IIf(chkPriceAsCompTotal.Value = 1, False, True)
        numStdPrice.ClearData

        For lPrCnt = 1 To grdCompKits.MaxRows - 1
            AddPrice lPrCnt
        Next
    Else
        numStdPrice.Enabled = True
        
        If ((moDmItemForm.State = sotaTB_EDIT) Or CDec(gdGetValidDbl(numStdPrice.Value)) > 0) And _
(ddnItemType.ItemData = kItemTypeBTOKit Or ddnItemType.ItemData = kItemTypeAssembledKit) Then
            iResponse = giSotaMsgBox(Nothing, moClass.moSysSession, kmsgBTOComponentPriceRollUp, _
Format(CDec(gdGetValidDbl(numStdPrice.Value)), "###,###,##0.00"))
   
            If iResponse = kretCancel Then
                chkPriceAsCompTotal = kChecked
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkPriceAsCompTotal_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkReturnsAllowed_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkReturnsAllowed, True
    #End If
'+++ End Customizer Code Push +++
    
     If (moDmItemForm.State <> sotaTB_NONE) Then
        If (ddnItemType.ItemData = kItemTypeBTOKit) Then
            If chkAllowRtrnsPartialBTO.Value = kUnchecked And _
chkReturnsAllowed.Value = kChecked Then
                If Not bCheckAllowReturnStatusForAllComponent(chkReturnsAllowed) Then
                    chkReturnsAllowed.Value = kUnchecked
                End If
            End If
        ElseIf (ddnItemType.ItemData > 0) Then
            If chkReturnsAllowed.Value = kUnchecked Then
                If Not bCheckKitParentAllowReturnAndAllowReturnPartialBTO() Then
                    chkReturnsAllowed.Value = kChecked
                End If
            End If
        End If
     End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkReturnsAllowed_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCustField_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdCustField, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim bChangesMade As Boolean
    
    'Save the values prior to showing the User-Defined Fields form.
    msUserFld(0) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld1"))
    msUserFld(1) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld2"))
    msUserFld(2) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld3"))
    msUserFld(3) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld4"))
    msUserFld(4) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld5"))
    msUserFld(5) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld6"))
    
    moUF.ShowUserflds kEntTypeIMItem, 6, msUserFld(), False, bChangesMade

    If (bChangesMade = True) Then
        moDmItemForm.SetColumnValue "UserFld1", msUserFld(0)
        moDmItemForm.SetColumnValue "UserFld2", msUserFld(1)
        moDmItemForm.SetColumnValue "UserFld3", msUserFld(2)
        moDmItemForm.SetColumnValue "UserFld4", msUserFld(3)
        moDmItemForm.SetColumnValue "UserFld5", msUserFld(4)
        moDmItemForm.SetColumnValue "UserFld6", msUserFld(5)
    End If
 
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdCustField_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdGetImage_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdGetImage, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'            cmdGetImage_Click pops up a common dialog and retrieves a location name
'           and Populate data into  grid Item Image.
'
'   Parameters:
'           None.
'************************************************************************
    Dim sFile As String
    Dim sItemImageShareName As String
    
    On Error Resume Next
    
    'Check if the Inventory Images Share Name has been defined in CI Options.  This flag is set when
    'the user click the "Categories" tab.
    If mbNoInvtImgShare = True Then
        giSotaMsgBox Me, moClass.moSysSession, kIMMsgShareNameNotFound, _
gsBuildString(kCISetUpInvtImagesShareName, moClass.moAppDB, moClass.moSysSession) & " path"
        Exit Sub
    End If

    With cdgGetImage
        .DefaultExt = kINETDefltExt
        .Filter = kINETFilterExt
        .CancelError = True
        .Flags = cdlOFNExplorer
        .ShowOpen
        sFile = .FileName
        .InitDir = .FileName
        If Err = cdlCancel Then Exit Sub
       
        ' Check to see if the location and file is valid to copy.
        If StrComp(LCase(msItemImageLocation), Left(LCase(.FileName), Len(LCase(msItemImageLocation))), vbTextCompare) <> 0 Then
             giSotaMsgBox Me, moClass.moSysSession, kIMInvalidFileToCopy, .FileName
             .InitDir = msItemImageLocation
             .FileName = ""
             Exit Sub
        End If
        If Len(Dir(Left$(.FileName, InStrRev(.FileName, "\")))) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInvalidFileToCopy, .FileName
            .InitDir = msItemImageLocation
            .FileName = ""
            Exit Sub
        End If
        
    End With

    On Error GoTo VBRigErrorRoutine
    With grdItemImages
          SetHourglass True
          miImageCurrentRow = grdItemImages.ActiveRow
          If (.ActiveRow > .MaxRows - 1) Then
            moDmItemImage.AppendRow
          End If
          
          sItemImageShareName = Mid$(sFile, Len(msItemImageLocation) + 1, Len(sFile))
          msImageName = sItemImageShareName
          gGridUpdateCell grdItemImages, miImageCurrentRow, kItemIColImagePath, _
sItemImageShareName

         gGridUpdateCell grdItemImages, miImageCurrentRow, kItemIColImageName, _
            gsGridReadCell(grdItemImages, miImageCurrentRow, kItemIColImagePath)
          'webItemImage.Navigate2 IIf(InStr(UCase(sItemImageShareName), kINETProtocol) = 0, msItemImageLocation, "") & sItemImageShareName
          ShowImage webItemImage, IIf(InStr(UCase(sItemImageShareName), kINETProtocol) = 0, msItemImageLocation, "") & sItemImageShareName
          moDmItemImage.SetRowDirty .Row
          ' Refresh the Image.
          DoEvents
          SetHourglass False
          DoEvents
          gGridSetActiveCell grdItemImages, miImageCurrentRow, kItemIColImagePath

    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "btnGetImage_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub cmdSearchInternet_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdSearchInternet, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'           cmdSearchInternet_Click loads the Internet form and
'           retrieves a image location from the Internet form.
'   Parameters:
'           None.
'************************************************************************
    Dim sItemImageShareName As String
    
    'Check if the Inventory Images Share Name has been defined in CI Options.  This flag is set when
    'the user click the "Categories" tab.
    If mbNoInvtImgShare = True Then
        giSotaMsgBox Me, moClass.moSysSession, kIMMsgShareNameNotFound, _
gsBuildString(kCISetUpInvtImagesShareName, moClass.moAppDB, moClass.moSysSession) & " path"
        Exit Sub
    End If

    With frmGetImages
        Set .ParentForm = frmMaintainItem
        Set .oClass = moClass
        .ItemImageLocation = msItemImageLocation
    End With

    frmGetImages.Show vbModal
    SetHourglass False
    
    If frmGetImages.iGetStatus = kYes Then
        With grdItemImages
            SetHourglass True
            miImageCurrentRow = grdItemImages.ActiveRow

            If (.ActiveRow > .MaxRows - 1) Then
                moDmItemImage.AppendRow
            End If

            sItemImageShareName = frmGetImages.ItemImageLocation
            msImageName = sItemImageShareName
            gGridUpdateCell grdItemImages, miImageCurrentRow, kItemIColImagePath, _
sItemImageShareName
            ShowImage webItemImage, IIf(InStr(UCase(sItemImageShareName), kINETProtocol) = 0, msItemImageLocation, "") & sItemImageShareName
            moDmItemImage.SetRowDirty .Row
            SetHourglass False
        End With
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "btnSearchInternet_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveDown_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdMoveDown, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'           cmdMoveDown moves a row in the Product Category grid down one
'           row, essentially swapping the selected row with the row
'           beneath it.  If the selected row is the last row, nothing
'           happens.
'   Parameters:
'           None.
'************************************************************************
    Dim lNbrOfDataRows As Long
    Dim lSelectedRow As Long
    Dim sItemIColImageNameSel As String
    Dim sItemIColImageNameBelow As String
    Dim sItemIColImagePathSel As String
    Dim sItemIColImagePathBelow As String
    Dim lRowIndex1 As Long
    Dim lRowIndex2 As Long
    Dim sNewDispAcctGroupKey As String
    Dim sOldDispPrintOrder As String
    Dim sDMAcctGroupKey As String

    If Not gbGotFocus(Me, cmdMoveDown) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    With grdItemImages
        'Determine the number of rows with data in them
        lNbrOfDataRows = .DataRowCnt
        
        'Determine which row is selected by the user
        lSelectedRow = .ActiveRow
        
        'Has the user selected the last row (or beyond) in the grid?
        If lSelectedRow >= lNbrOfDataRows Then
            'Yes, the user HAS selected the last row (or beyond) in the grid
            'Do nothing

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    
        'At this point, we have determined we WILL move the selected row down

        'First, save the values in the selected row in local variables
        sItemIColImageNameSel = gsGridReadCell(grdItemImages, lSelectedRow, kItemIColImageName)
        sItemIColImagePathSel = gsGridReadCell(grdItemImages, lSelectedRow, kItemIColImagePath)
                
        'Second, save the values in the row below the selected row
        sItemIColImageNameBelow = gsGridReadCell(grdItemImages, lSelectedRow + 1, kItemIColImageName)
        sItemIColImagePathBelow = gsGridReadCell(grdItemImages, lSelectedRow + 1, kItemIColImagePath)
        
        'Now place the selected row values into the row below
        gGridUpdateCell grdItemImages, lSelectedRow + 1, kItemIColImageName, sItemIColImageNameSel
        gGridUpdateCell grdItemImages, lSelectedRow + 1, kItemIColImagePath, sItemIColImagePathSel
                
        'Now place the row below values into the selected row
        gGridUpdateCell grdItemImages, lSelectedRow, kItemIColImageName, sItemIColImageNameBelow
        gGridUpdateCell grdItemImages, lSelectedRow, kItemIColImagePath, sItemIColImagePathBelow
        moDmItemImage.SetDirty True
        'Set both affected rows dirty in Data Manager
        With moDmItemImage
            .SetRowDirty (lSelectedRow)
            .SetRowDirty (lSelectedRow + 1)
        End With
        
        'Finally, select the row below the current selected row
        gGridSetSelectRow grdItemImages, lSelectedRow + 1

        'Always turn on the Move Up button at this point
        cmdMoveUp.Enabled = True
        
        'Turn off the Move Down button if appropriate
        If lSelectedRow + 1 >= lNbrOfDataRows Then
            cmdMoveDown.Enabled = False
        Else
            cmdMoveDown.Enabled = True
        End If
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdMoveDown_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveItems_Click(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdMoveItems(iIndex), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'***********************************************************************
'     Desc: Buttons to move items from/to the Assigned listbox.
'    Parms: Index - options[kSelectAll, kSelect, kDeselect, kDeselectAll]
'  Returns: N/A
'***********************************************************************
    Dim lPlace As Long
    Dim iTimes As Integer
    Dim iButtonIndex As Integer

    If Not gbGotFocus(Me, cmdMoveItems(iIndex)) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    'Crazy but without disabling the opposite lstbox's GotFocus event gets fired
    '(in non-debug mode) after this procedure ends, and GotFocus shows no call stack.
    lstAvailProdCatID.Enabled = False
    lstAssignProdCatID.Enabled = False

    Select Case iIndex
        Case Is = kSelectAll
            SelectAllItems

        Case Is = kSelect
            'FutureHighlightPosition = (LastItemToBeMoved + Next) - ItemsToBeMoved
            lPlace = lLastPosOfAssignedItems(lstAvailProdCatID) + 1 - lstAvailProdCatID.SelCount
            SelectSomeItems
            
            'Highlight on next logical position
            HighlightAnItem lstAvailProdCatID, lPlace
        
        Case Is = kDeselect
            'FutureHighlightPosition = (LastItemToBeMoved + Next) - ItemsToBeMoved
            lPlace = lLastPosOfAssignedItems(lstAssignProdCatID) + 1 - lstAssignProdCatID.SelCount
            DeselectSomeItems
            
            'Highlight on next logical position
            HighlightAnItem lstAssignProdCatID, lPlace
        
        Case Is = kDeselectAll
            DeselectAllItems
    End Select

    'SetCmdMoveItemsOptions was already fired when an item position was selected (highlighted) by each of the above cases.
    
    'Keep focus here in button array
    iTimes = 1
    
    'Set focus to next enabled button in array starting with current one
    For iButtonIndex = iIndex To 5
        If cmdMoveItems(iButtonIndex).Enabled Then
           cmdMoveItems(iButtonIndex).SetFocus
           Exit For
        End If
        
        'If last button (3), start at first one (0)
        If iButtonIndex = 3 Then
            'Becomes 0 when we hit 'Next'
            iButtonIndex = -1
            
            If iTimes = 2 Then
                'Avoid an endless loop
                Exit For
            End If

            iTimes = iTimes + 1
        End If
    Next
     
    'Re-enabling the lstBoxes we disabled at the beginning of this routine
    lstAvailProdCatID.Enabled = True
    lstAssignProdCatID.Enabled = True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdMoveItems_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DeselectAllItems()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Moves Selected items from lstAssignProdCatID back to lstAvailProdCatID listbox.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************
    Dim lItemCount As Long

    'Remove highlight from current items.
    UnhighlightItems lstAvailProdCatID
    
    'Go through lstAvailProdCatID listbox backwards because it is
    'easier to remove (this way indexes don't change on you).
    With lstAvailProdCatID
        For lItemCount = lstAssignProdCatID.ListCount - 1 To 0 Step -1
            .AddItem lstAssignProdCatID.List(lItemCount)
            .ItemData(.NewIndex) = lstAssignProdCatID.ItemData(lItemCount)
            
            'Remove item from the source listbox.
            lstAssignProdCatID.RemoveItem (lItemCount)
        Next lItemCount
    End With
        
    'Select the first item in the target list.
    HighlightAnItem lstAvailProdCatID, 0
    
    mbAssignedProdCatsSaveIsNeeded = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeselectAllItems", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SelectAllItems()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Moves selected items from lstAvailProdCatID to lstAssignProdCatID listbox.
'    Parms: None.
'  Returns: N/A
'***********************************************************************
    Dim lItemCount As Long

    'Remove highlight from current items
    UnhighlightItems lstAssignProdCatID
    
    'Go through Source listbox backwards because it is
    'easier to remove (this way indexes don't change on you).
    With lstAssignProdCatID
        For lItemCount = lstAvailProdCatID.ListCount - 1 To 0 Step -1
            .AddItem lstAvailProdCatID.List(lItemCount)
            .ItemData(.NewIndex) = lstAvailProdCatID.ItemData(lItemCount)
          
            'Remove item from the source listbox
            lstAvailProdCatID.RemoveItem (lItemCount)

        Next lItemCount

        'Select the first item in the target list
        HighlightAnItem lstAssignProdCatID, 0
    End With

    mbAssignedProdCatsSaveIsNeeded = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SelectAllItems", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveUp_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdMoveUp, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'           cmdMoveUp moves a row in the Product Category grid up one
'           row, essentially swapping the selected row with the row
'           above it.  If the selected row is the first row, nothing
'           happens.
'   Parameters:
'           None.
'************************************************************************
    Dim lNbrOfDataRows As Long
    Dim lSelectedRow As Long
    Dim sItemIColImageNameSel As String
    Dim sItemIColImageNameAbove As String
    Dim sItemIColImagePathSel As String
    Dim sItemIColImagePathAbove As String
    Dim lRowIndex1 As Long
    Dim lRowIndex2 As Long
    Dim sNewDispAcctGroupKey As String
    Dim sOldDispPrintOrder As String
    Dim sDMAcctGroupKey As String

    If Not gbGotFocus(Me, cmdMoveUp) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    With grdItemImages
        'Determine the number of rows with data in them
        lNbrOfDataRows = .DataRowCnt
        
        'Determine which row is selected by the user
        lSelectedRow = .ActiveRow
        
        'Has the user selected the first row in the grid?
        If lSelectedRow = 1 Then
            'Yes, the user HAS selected the first row in the grid
            'Do nothing

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    
        'At this point, we have determined we WILL move the selected row up

        'First, save the values in the selected row in local variables
        sItemIColImageNameSel = gsGridReadCell(grdItemImages, lSelectedRow, kItemIColImageName)
        sItemIColImagePathSel = gsGridReadCell(grdItemImages, lSelectedRow, kItemIColImagePath)
                  
        'Second, save the values in the row above the selected row
        sItemIColImageNameAbove = gsGridReadCell(grdItemImages, lSelectedRow - 1, kItemIColImageName)
        sItemIColImagePathAbove = gsGridReadCell(grdItemImages, lSelectedRow - 1, kItemIColImagePath)
         
        'Now place the selected row values into the row above
        gGridUpdateCell grdItemImages, lSelectedRow - 1, kItemIColImageName, sItemIColImageNameSel
        gGridUpdateCell grdItemImages, lSelectedRow - 1, kItemIColImagePath, sItemIColImagePathSel
                
        'Now place the row above values into the selected row
        gGridUpdateCell grdItemImages, lSelectedRow, kItemIColImageName, sItemIColImageNameAbove
        gGridUpdateCell grdItemImages, lSelectedRow, kItemIColImagePath, sItemIColImagePathAbove
        moDmItemImage.SetDirty True
 
        'Set both affected rows dirty in Data Manager
        With moDmItemImage
            .SetRowDirty (lSelectedRow)
            .SetRowDirty (lSelectedRow - 1)
        End With

        'Finally, select the row above the current selected row
        gGridSetSelectRow grdItemImages, lSelectedRow - 1

        'Turn off the Move Up button if appropriate
        If lSelectedRow - 1 > 1 Then
            cmdMoveUp.Enabled = True
        Else
            cmdMoveUp.Enabled = False
        End If
    
        'Always turn on the Move Down button at this point
        cmdMoveDown.Enabled = True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdMoveUp_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnItemType_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnItemType, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim bNoFurtherChkReq As Boolean

    If mbIsClickCancel = True Then
        Exit Sub
    End If
    
    ' In case of browsing the event gets forked because of setting the list index.
    ' So exit the sub.  The commented code here is not understood.
    ' Since we dont now why it was placed here we are keeping it as it is for reference.
    If mbBrowseRec Then
        Exit Sub
    End If

    ' In case of loading the data from database this event will get fired because of setting up values
    ' so if the form state is not add then just exit from here.  Corresponding code for enabling and
    ' disabling is written on validation managers KeyChange event
    If moDmItemForm.State <> sotaTB_ADD Then
        Exit Sub
    ElseIf ddnItemType.Enabled = False Then
        ddnItemType.Enabled = True
    End If

    ' The following code is common to all the Item types
    ' so dont move it to any of the cases
    Dim UserResponse As Integer
    
    ' ConfirmUnload will set the list index to -1. (To clear the data in the control)
    ' This will cause the click event to get forked. and hence avoide processing if the liatindex = -1
    If ddnItemType.ListIndex = -1 Then
        Exit Sub
    End If

    ValuesRemove ("Valuation")

    ' If the new item does not allow values in the UOM grid - warn the user that we will clear the grid
    If (moDmItemForm.State = sotaTB_EDIT) Or (moDmItemForm.State = sotaTB_ADD And Len(Trim(lkuStockUOM.Text)) > 0) Then
        If ddnItemType.ItemData(NewIndex) = kItemTypeCommentOnly Or ddnItemType.ItemData(NewIndex) = kItemTypeBTOKit Then
            UserResponse = giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgEffUOM)
            If UserResponse <> kretYes Then
                Cancel = True
                Exit Sub
            End If
        End If
    End If
    
    ' If we are changing for kit to non kit then warn user about losing the components defined
    If ((ddnItemType.ItemData(PrevIndex) = kItemTypeBTOKit Or ddnItemType.ItemData(PrevIndex) = kItemTypeAssembledKit)) Then
        UserResponse = giSotaMsgBox(Nothing, moClass.moSysSession, 160145)
        
        If UserResponse <> vbOK Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    ' The chk
    If (ddnItemType.ItemData(NewIndex) <> kItemTypeBTOKit And ddnItemType.ItemData(NewIndex) <> kItemTypeAssembledKit) Then
        chkPriceAsCompTotal.Value = kUnchecked
    End If
    
    Select Case ddnItemType.ItemData(NewIndex, True)
        Case Is = kItemTypeBTOKit
        
            ' General settings for inventory / non inventory items
            EnableControlsFromItemType kItemTypeBTOKit, True
            chkEplodeOnInvoice.Value = kChecked
            chkPriceAsCompTotal.Value = kChecked

            If chkReturnsAllowed.Value = kChecked Then
                chkAllowRtrnsCustomBTO.Value = kChecked
                chkAllowRtrnsPartialBTO.Value = kChecked
            End If
            
            ' If the stock UOM is specified the there is a chance that the Purchase UOM is specified
            ' Hence do the stock UOM processing here.  This function takes care of
            ' disabling the Purchase UOM.
            If Not lkuStockUOM.Text = "" Then
                StkUOMProcessing Trim$(lkuStockUOM.Text)
            End If
            
            ' BTO's Dont allow drop ship as they cant be purchased
            chkDropShipFlg.Value = kUnchecked

        Case Is = kItemTypeCommentOnly
            If NewIndex <> PrevIndex Then
               
                ' General settings for inventory / non inventory items
                EnableControlsFromItemType kItemTypeCommentOnly, True
            
                lkuStockUOM.KeyValue = Empty
                lkuPurchaseUOM.KeyValue = Empty
                lkuSales.KeyValue = Empty
                lkuPrice.KeyValue = Empty
                
                ' For comment only items the allow dropship should be NO
                chkDropShipFlg.Value = kUnchecked

                ' Disabled for all item types other than kit.
                chkPriceAsCompTotal.Value = kUnchecked
                
                ' This part is added specifically for commentonly items.
                lkuCommCls.KeyValue = Empty
                glaCOS.KeyValue = Empty
                glaExpense.KeyValue = Empty
                glaInventory.KeyValue = Empty
                glaReturns.KeyValue = Empty
                glaSales.KeyValue = Empty
                tabMaintainItem.TabEnabled(kTabGLAcct) = False
                
                chkSeasonal.Value = kUnchecked
                chkAllowCostOvrd.Value = kUnchecked
                
                chkAllowDecQuantities.Value = kUnchecked
                With numMandatorySalesMulti
                    .Enabled = False
                    .Value = 0
                End With
                
                chkInternal.Value = kUnchecked
            End If

        Case Is = kItemTypeFinishedGood
            ' General settings for inventory / non inventory items
            EnableControlsFromItemType kItemTypeFinishedGood, True
        
            ' We allow dropship of finished goods and raw material
            chkDropShipFlg.Value = kUnchecked
            
            ' Disabled for all item types other than kit.
            chkPriceAsCompTotal.Value = kUnchecked

        Case Is = kItemTypeRawMaterial
            ' General settings for inventory / non inventory items
            EnableControlsFromItemType kItemTypeRawMaterial, True
        
            ' We allow dropship of finished goods and raw material and non inventory items
            chkDropShipFlg.Value = kUnchecked
        
            ' Disabled for all item types other than kit.
            chkPriceAsCompTotal.Value = kUnchecked

        Case Is = kItemTypeExpense
            ' General settings for inventory / non inventory items
            EnableControlsFromItemType kItemTypeExpense, True
            
            ' We allow dropship of finished goods and raw material and non inventory items
            chkDropShipFlg.Value = kUnchecked
            
            ' Disabled for all item types other than kit.
            chkPriceAsCompTotal.Value = kUnchecked

        Case Is = kItemTypeAssembledKit
            ' General settings for inventory / non inventory items
            StkItemSetting
            EnableControlsFromItemType kItemTypeAssembledKit, True
            chkEplodeOnInvoice.Value = kUnchecked
            chkPriceAsCompTotal.Value = kChecked
            chkAllowRtrnsCustomBTO.Value = kUnchecked
            chkAllowRtrnsPartialBTO.Value = kUnchecked
        
            ' We allow dropship of finished goods and raw material and non inventory items
            chkDropShipFlg.Value = kUnchecked
            
            ' We keep this flag hidden and always checked.  It will be used in the next version
            chkPriceAsCompTotal.Value = kChecked

        Case Is = kItemTypeMiscItem
            ' General settings for inventory / non inventory items
            EnableControlsFromItemType kItemTypeMiscItem, True
        
            ' We allow dropship of finished goods and raw material and non inventory items
            chkDropShipFlg.Value = kUnchecked
        
            ' Disabled for all item types other than kit.
            chkPriceAsCompTotal.Value = kUnchecked

        Case Is = kItemTypeService
            ' General settings for inventory / non inventory items
            EnableControlsFromItemType kItemTypeService, True
        
            ' We allow dropship of finished goods and raw material and non inventory items
            ' Services can not be drop shipped
            chkDropShipFlg.Value = kUnchecked
            
            ' Disabled for all item types other than kit.
            chkPriceAsCompTotal.Value = kUnchecked
    End Select
    
    'If we are changing from BTO to Non BTO the we need to get the stock UOM.
    'So, the best way is to default it to Stock UOM.
    If (Not ddnItemType.ItemData(NewIndex) = kItemTypeBTOKit And ddnItemType.ItemData(PrevIndex) = kItemTypeBTOKit) Then
        lkuPurchaseUOM.Text = lkuStockUOM.Text
    End If
    
    ' Call Lost focus of all the UOM look ups -  This will refresh the UOM grid.
    RedoUOMGrid
    
    'Default GL Acct
    If Not bIsInventory(ddnItemType.ItemData) Then
        PopulateVal kProcessItemClass
        chkInclOnPackList.Value = kUnchecked
    Else
        chkInclOnPackList.Value = kChecked
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnItemType_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnProvider_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnProvider, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If mbIsClickCancel = True Then
        Exit Sub
    End If

    If NewIndex = -1 Then
        Exit Sub
    End If

    If PrevIndex = -1 Then
        Exit Sub
    End If
    
    'Enable the Number Of Days field only if Provide is not equal to none.
    If ddnProvider.ItemData = 0 Then
        numNOfDays.Value = 0
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        numNOfDays.Value = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnProvider_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub ddnStatus_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnStatus, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRetVal As Long
    
    'If the user wants to change the Status of an Item.
    If mbIsClickCancel = True Then Exit Sub
    If NewIndex = -1 Then Exit Sub
    If PrevIndex = -1 Then Exit Sub
    
    If ddnStatus.ItemData = kItemStatDeleted Then ' User has chosen to mark the item for deletion
        With moClass.moAppDB
            .SetInParam 0                           ' Check Item
            .SetInParam glGetValidLong(mlItemKey)
            .SetInParam 0
            .SetOutParam lRetVal
            .ExecuteSP "spimCheckAllowDelete"
            lRetVal = .GetOutParam(4)
            .ReleaseParams
        End With
        
        If lRetVal <> 1 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMInvStatDelNoAllowed
            Cancel = True
            Exit Sub
        End If
    End If
    
    mbStatChange = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnStatus_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnTrackMethod_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnTrackMethod, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lThisRow As Long

    If mbIsClickCancel = True Then
        Exit Sub
    End If
    
    'The Valuation method can be used only if Track Meth is not (None).
    If Not mbValuationChanged Then
        mbFromTrack = True
        
        'Default Valuation method from timOptions
        PopulateVal kProcessTrackMethod
    End If

    'If inventory but not BTO
    If (ddnItemType.Text <> "" _
        And moDmItemForm.State = sotaTB_ADD _
        And Len(Trim$(gsGridReadCellText(grdUOM, 1, kUOMColTargetUOM))) > 0) Then
        If bIsInventory(ddnItemType.ItemData) = True _
        And Not ddnItemType.ItemData = kItemTypeBTOKit Then
            'If changing from none/lot to serial/both OR vice versa.
            If (ddnTrackMethod.ItemData(PrevIndex) = kTrackNone Or ddnTrackMethod.ItemData(PrevIndex) = kTrackLot) _
            And (ddnTrackMethod.ItemData(NewIndex) = kTrackSerial Or (ddnTrackMethod.ItemData(NewIndex) = kTrackBoth)) Then
                If giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgTrackChange) = vbYes Then
                    For lThisRow = 1 To grdUOM.MaxRows - 1
                        DisableEnable lThisRow
                    Next
                Else
                    ddnTrackMethod.ItemData = ddnTrackMethod.ItemData(PrevIndex)
                    bSetFocus ddnTrackMethod
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If
            Else
                If (ddnTrackMethod.ItemData(PrevIndex) = kTrackSerial Or ddnTrackMethod.ItemData(PrevIndex) = kTrackBoth) _
                    And (ddnTrackMethod.ItemData(NewIndex) = kTrackNone Or ddnTrackMethod.ItemData(NewIndex) = kTrackLot) Then
                    For lThisRow = 1 To grdUOM.MaxRows - 1
                        DisableEnable lThisRow
                    Next
                End If
            End If
        End If
    End If
        
    'If serial/Both and FG/RM or Kits allow decimal is OFF and disabled.
    
    TrackMethodRelatedControls ddnTrackMethod.ItemData(NewIndex), moDmItemForm.State
    
    If (ddnTrackMethod.ItemData(NewIndex) = kTrackSerial _
        Or ddnTrackMethod.ItemData(NewIndex) = kTrackBoth) Then
        chkAllowDecQuantities.Value = kUnchecked
        With numMandatorySalesMulti
            .Enabled = True
            .Value = 0
        End With
        chkAllowDecQuantities.Enabled = False
        SetupAdjQtyRoundMeth
    Else
        chkAllowDecQuantities.Enabled = True
        SetupAdjQtyRoundMeth
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnTrackMethod_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub TrackMethodRelatedControls(ByVal iTrackMethodItemData As Integer, ByVal iFormState As Integer)
    If iTrackMethodItemData = kTrackSerial Or iTrackMethodItemData = kTrackBoth Then
        txtSerialNumberMask.Enabled = True
        txtSerialNumberIncrement.Enabled = True
        ' If the form is in the add state and the SerialNumberMask hasn't been set
        ' then set it to the default (from inventory options).
        If Len(Trim(txtSerialNumberMask)) = 0 And iFormState = kDmStateAdd Then
            PopulateVal kProcessGetSerialDefault
        End If
    Else
        txtSerialNumberIncrement.Enabled = False
        txtSerialNumberIncrement.Text = ""
        txtSerialNumberMask.Enabled = False
        txtSerialNumberMask.Text = ""
    End If
End Sub
Private Sub chkAllowDecQuantities_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowDecQuantities, True
    #End If
'+++ End Customizer Code Push +++
    Dim sSQL As String
    Dim lMismatchCount As Long
    Dim rsItemSubstitute As Object
    Dim iUserChoice As Integer

    If Not gbGotFocus(Me, chkAllowDecQuantities) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    'Check the substitute items for this Item to see if Allow Decimal Quantities is the same
    If mlItemKey > 0 Then
        If chkAllowDecQuantities.Value = kChecked Then
            'The Main Item now DOES allow decimal quantities.
            'Build a SQL Query to check the substitute items for this Main Item.
            sSQL = "SELECT COUNT(*) MismatchCount "
            sSQL = sSQL & "FROM timItemSubstitute a WITH (NOLOCK), "
            sSQL = sSQL & "timItem b WITH (NOLOCK) "
            sSQL = sSQL & "WHERE a.SubstItemKey = b.ItemKey "
            sSQL = sSQL & "AND a.ItemKey = " & Format$(mlItemKey) & Space$(1)
            sSQL = sSQL & "AND b.AllowDecimalQty = 0"

            'Execute the SQL Query
            Set rsItemSubstitute = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
            With rsItemSubstitute
                'Did we find the record count?
                If Not .IsEmpty Then
                    'Yes, we DID find the record count
                    lMismatchCount = glGetValidLong(.Field("MismatchCount"))
                Else
                    'No, we did NOT find the record count
                    lMismatchCount = 0
                End If
    
                'Close the recordset opened above
                If Not rsItemSubstitute Is Nothing Then
                    .Close
                    Set rsItemSubstitute = Nothing
                End If
            End With
    
            If lMismatchCount > 0 Then
                'Give the user a warning
                iUserChoice = MsgBox("This item has been changed to allow decimal quantities but one or more substitute items does not allow decimal quantities.  Do you want to continue with this change?", vbYesNo, "Sage 500 ERP")
                
                'Does the user want to continue with the change?
                If iUserChoice = vbNo Then
                    'No, the user does NOT want to continue with the change.
                    With numMandatorySalesMulti
                        .Enabled = True
                        .Value = 0
                    End With
                    
                    'We must get the focus away from this checkbox
                    'in order for the gbGotFocus statement above to work.
                    gbSetFocus Me, txtShortDesc

                    'So, set the value back to its original value.
                    'Uncheck the Allow Decimal Quantities checkbox
                    chkAllowDecQuantities.Value = kUnchecked
                    Exit Sub
                End If
            End If
        Else
            'The Main Item now DOES NOT allow decimal quantities.
            'Build a SQL Query to check items that this Main Item is a substitute for.
            sSQL = "SELECT COUNT(*) MismatchCount "
            sSQL = sSQL & "FROM timItemSubstitute a WITH (NOLOCK), "
            sSQL = sSQL & "timItem b WITH (NOLOCK) "
            sSQL = sSQL & "WHERE a.ItemKey = b.ItemKey "
            sSQL = sSQL & "AND a.SubstItemKey = " & Format$(mlItemKey) & Space$(1)
            sSQL = sSQL & "AND b.AllowDecimalQty = 1"

            'Execute the SQL Query
            Set rsItemSubstitute = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
            With rsItemSubstitute
                'Did we find the record count?
                If Not .IsEmpty Then
                    'Yes, we DID find the record count
                    lMismatchCount = glGetValidLong(.Field("MismatchCount"))
                Else
                    'No, we did NOT find the record count
                    lMismatchCount = 0
                End If
    
                'Close the recordset opened above
                If Not rsItemSubstitute Is Nothing Then
                    .Close
                    Set rsItemSubstitute = Nothing
                End If
            End With
    
            If lMismatchCount > 0 Then
                'Give the user a warning
                iUserChoice = MsgBox("This item has been changed to not allow decimal quantities but one or more items that it is a substitute item for does allow decimal quantities.  Do you want to continue with this change?", vbYesNo, "Sage 500 ERP")
                
                'Does the user want to continue with the change?
                If iUserChoice = vbNo Then
                    'No, the user does NOT want to continue with the change.
                    With numMandatorySalesMulti
                        .Value = 0
                        .Enabled = False
                    End With
                    
                    'We must get the focus away from this checkbox
                    'in order for the gbGotFocus statement above to work.
                    gbSetFocus Me, txtShortDesc

                    'So, set the value back to its original value.
                    'Check the Allow Decimal Quantities checkbox
                    chkAllowDecQuantities.Value = kChecked
                    Exit Sub
                End If
            End If
        End If
    End If
        
    'If this field is set to 'Yes', then the Mandatory Sales Multiple must be set to 1.
    'Also filter the Round Method drop down
    If chkAllowDecQuantities.Value = kChecked Then
        With numMandatorySalesMulti
            .Value = 0
            .Enabled = False
        End With
    Else
        With numMandatorySalesMulti
            .Enabled = True
            .Value = 0
        End With
    End If

    SetupAdjQtyRoundMeth
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkAllowDecQuantities_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLandCost_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdLandCost, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'--Call the Landed Cost form
    Dim oChild         As Object
    
    ' Changed by Ashish on 23 Feb 1999.
    ' To fix the bug 12351
    If moDmItemForm.State = 0 Then
        Exit Sub
    End If
    
    If moDmItemForm.IsDirty Then
        If giSotaMsgBox(Me, moClass.moSysSession, kmsgDMSaveChanges, "Item Maintenance", "") = vbYes Then
           HandleToolBarClick kTbSave
           If Not mbSaveDone Then Exit Sub
        Else
            Exit Sub
        End If
    End If
    
    Set oChild = goGetSOTAChild(moClass.moFramework, moSotaObjects, "IMZMQ002.clsApplyLandedCost", 117506218, kAOFRunFlags, kContextAOF)
    
    SetHourglass False
    Me.Enabled = False
    
    'Pass in the Item Key from here.
    If Not oChild Is Nothing Then
        Call oChild.bAddOnTheFly(moDmItemForm.GetColumnValue("ItemKey"), 0)
    End If

    'Send startup parameters to CustModule obj
    Me.Enabled = True
    Me.SetFocus
        
    Set oChild = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdLandCost_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSubstitution_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdSubstitution, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'--call the Substitutions Form
    Dim oChild As Object
    Dim bConfirm As Boolean
    
    'Ask user to save the master record before you call this form

    ' Changed by Ashish on 23 Feb 1999.
    ' To fix the bug 12351
    If moDmItemForm.State = 0 Then
        Exit Sub
    End If
    
    If moDmItemForm.IsDirty Then
        If giSotaMsgBox(Me, moClass.moSysSession, kmsgDMSaveChanges, "Item Maintenance", "") = vbYes Then
           HandleToolBarClick kTbSave
           If Not mbSaveDone Then Exit Sub
        Else
            Exit Sub
        End If
    End If
    
    Set oChild = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
"imzmr002.clsSubstitutes", 117506208, kAOFRunFlags, kContextAOF)
        
    SetHourglass False
    Me.Enabled = False
    
    If Not oChild Is Nothing Then
    
    oChild.ItemKey = gvCheckNull(moClass.moAppDB.Lookup _
("ItemKey", "timItem", "ItemID = " & gsQuoted(lkuMain.Text) & _
" AND ItemType= " & ddnItemType.ItemData & " AND CompanyID = " & _
gsQuoted(msCompanyID)), SQL_INTEGER)

        'load the child form
        oChild.bAddOnTheFly
    End If
    
    'Send startup parameters to ItemModule obj
    Me.Enabled = True
    Me.SetFocus
    
    Set oChild = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdSubstitution_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnValuation_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnValuation, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If mbIsClickCancel = True Then
        Exit Sub
    End If
    
    'If not in add mode, exit.
    If Not moDmItemForm.State = sotaTB_ADD Then
        Exit Sub
    End If
    
    'If Non-Inventory or BTO, then exit.
    If (bIsInventory(ddnItemType.ItemData) = False _
        Or ddnItemType.ItemData = kItemTypeBTOKit) Then
        Exit Sub
    End If
    
    'If called from track method, then reset track method flag and exit.
    If mbFromTrack Then
        mbFromTrack = False
        Exit Sub
    End If
    
    'Reset default flag.
    mbDefaultValuation = False
    
    'Set change flag.
    mbValuationChanged = True

    If ddnValuation.ItemData(NewIndex) = kValStd Then
        cmdLandCost.Enabled = False
    Else
        cmdLandCost.Enabled = bCheckEnableLandCost
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnValuation_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'********************************************************************
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            If KeyCode = vbKeyF5 Then
                mbIsPressF5 = True
            Else
                mbIsPressF5 = False
                gProcessFKeys Me, KeyCode, Shift
            End If
    End Select
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'
'********************************************************************
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                If (Not (TypeOf Me.ActiveControl Is fpSpread) _
And Not (TypeOf Me.ActiveControl Is ListBox)) Then
                    gProcessSendKeys "{Tab}"
                End If
            End If
        Case Else
            'Other KeyAscii routines.
    End Select
    mbIsPressF5 = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'********************************************************************
'   Description:
'       Form_Load initializes variables to be used throughout the
'       form's user interface.  It also sets up control
'       specific properties.  For example, here you would fill a
'       combo box with static/dynamic list information.
'********************************************************************
Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sItem As String
    Dim iButtonIndex As Integer

    'Setup form level variables.
    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mdDate = moClass.moSysSession.BusinessDate
    mbMFActivated = moClass.moSysSession.IsModuleActivated(kModuleMF)
    
    'Activate the Module Options.
    Set moOptions.oAppDB = moClass.moAppDB
    moOptions.sCompanyID = msCompanyID

    miPriceDecimal = moOptions.CI("UnitPriceDecPlaces")
    miCostDecimal = moOptions.CI("UnitCostDecPlaces")
    miQtyDecimal = moOptions.CI("QtyDecPlaces")
    SetDecimalValues

    msConversionTypeOld = ""
    mbUseStdConvOld = False
    mbAddDefRow = False

    'Get the PA Sales Item.
    mlPASalesItemKey = 0

    'Add the Copy From Button
    tbrMain.AddButton kTbCopyFrom, tbrMain.GetIndex(kTbMemo) + 1

    If (mlRunMode <> kContextDA _
    Or miModule = 0) Then
        miModule = moClass.moFramework.GetTaskModule
    End If

    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width

    mbEnterID = True
    bChkLookupClick = True
    
    If miModule = kModuleCI Then         '2 for CI
        Me.Caption = gsBuildString(kNonStkItem, moClass.moAppDB, moClass.moSysSession) '"Maintain Non Stock Item"
    ElseIf miModule = kModuleIM Then     '7 for IM
        Me.Caption = gsBuildString(kMaintainItems, moClass.moAppDB, moClass.moSysSession) '"Maintain Items"
    End If

    lkuMain.MaxLength = 30

    SSPanel1(0).Enabled = True
    SSPanel1(1).Enabled = False
    SSPanel1(2).Enabled = False
    SSPanel1(3).Enabled = False
    SSPanel1(4).Enabled = False
    SSPanel1(5).Enabled = False
    SSPanel1(6).Enabled = False
    SSPanel1(7).Enabled = False     'SGS DEJ 5/18/12    added SSPanel1(7)

    'Check if we need to validate UPC Bar Codes in IM Options.
    miValidateUPCBarCodes = giGetValidInt(moClass.moAppDB.Lookup("ValidateUPCBarCode", "timOptions", "CompanyID = " & gsQuoted(msCompanyID)))

    'Check if Sales Tax is being tracked in AR since Sales Tax Class will then be a required field
    miTrackSTaxOnSales = giGetValidInt(moClass.moAppDB.Lookup("TrackSTaxOnSales", "tarOptions", "CompanyID = " & gsQuoted(msCompanyID)))

    'Is IM activated?
    mbIMIsActivated = moClass.moSysSession.IsModuleActivated(kModuleIM)

    'Is EC activated for this Company?
    If moClass.moSysSession.IsModuleCompanyIDActivated(kModuleEC, msCompanyID) = kOptionTrue Then
        'Yes, EC IS activated for this Company.
        mbECIsActivated = True
    Else
        'No, EC is NOT activated for this Company.
        mbECIsActivated = False
    End If

    'Is SO activated for this Company?
    If moClass.moSysSession.IsModuleCompanyIDActivated(kModuleSO, msCompanyID) = kOptionTrue Then
        'Yes, SO IS activated for this Company.
        mbSOIsActivated = True
    Else
        'No, SO is NOT activated for this Company.
        mbSOIsActivated = False
    End If

    '-- Check if the IR (Replenishment) module is activated and licensed.
    mbIRIsActivated = moClass.moSysSession.IsModuleActivated(kModuleIR) = 1

    'If IR is not activated, then disable the Seasonal Item Check Box and Period Usage Order Limit field
    If mbIRIsActivated Then
        numPeriodUsage.Enabled = True
    Else
        numPeriodUsage.Enabled = False
    End If

    'Initialize a Grid Manager for Item Image grid.
    If (mbECIsActivated Or mbSOIsActivated) Then
        Set moGMItemImage = New clsGridMgr
    End If

    'Init the lku's.
    InitLookup

    'Init the drop downs.
    InitDropDown

    '***************************************************************************************
    '***************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '***************************************************************************************
    '***************************************************************************************
    SetupDropDownCtrls
    '***************************************************************************************
    '***************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '***************************************************************************************
    '***************************************************************************************

    'Init the Fixed Asset Template Combobox
    SetupFixedAssetTemplates

    'Bind Data Manager to the form.
    BindForm

    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmItemForm, moDmItemDescForm, moDmKitCompGrid, moDmKitForm, moDmUOMGrid)
    If miSecurityLevel = sotaTB_DISPLAYONLY Then
        ' Copy From
        tbrMain.Buttons(kTbCopyFrom).Enabled = False
    End If

    'Bind the Grid Manager.
    BindGM

    'Initialize the Context Menu object.
    BindContextMenu
    
    'For initializing the grid navigators.
    SetupNavigators
    
    'Format the UOM grid.
    FormatUOMGrid
    
    'Format the Kit Component grid.
    FormatKitCompGrid
        
    '***************************************************************************************
    '***************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '***************************************************************************************
    '***************************************************************************************
    FormatCOALabGrid
    '***************************************************************************************
    '***************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '***************************************************************************************
    '***************************************************************************************
        
    sbrMain.MessageVisible = False
    Set sbrMain.Framework = moClass.moFramework
    Set moDmItemForm.SOTAStatusBar = sbrMain
    
    'Set the StatusBar status.
    sbrMain.Status = SOTA_SB_START
    
    'Disable sorting in grid.
    moGMUOM.GridSortEnabled = False
    moGMKitComp.GridSortEnabled = False
    
    '***************************************************************************************
    '***************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '***************************************************************************************
    '***************************************************************************************
    moGMCOATests.GridSortEnabled = False
    '***************************************************************************************
    '***************************************************************************************
    'SGS DEJ 5/18/12    (START)
    '***************************************************************************************
    '***************************************************************************************
    
    'Check if the IM Module has been activated or not.
    IMActivatedChk
    
    'Do all the settings of controls over here.
    CntrlSettings

    If (mbECIsActivated Or mbSOIsActivated) Then
        'Clear the list boxes on the Assign Items tab now.
        lstAvailProdCatID.Clear
        lstAssignProdCatID.Clear
        For iButtonIndex = 0 To cmdMoveItems.Count - 1
            cmdMoveItems(iButtonIndex).Enabled = False
        Next
        
        'Necessary settings to include.
        'Determines the size of listboxes' row size in the current font.
        miRowHeight = TextHeight("A")
        msItemImageLocation = RTrim(moOptions.CI("InvtImagesShareName"))
        
        If Len(Trim(msItemImageLocation)) <> 0 Then
            If Not (Right(msItemImageLocation, 1) = "\") Then
                msItemImageLocation = msItemImageLocation & "\"
            End If
        End If
        
        'Format the Item Images Grid.
        FormatItemImagesGrid
        
        SetItemImagePict False
        SetcdgGetImage
        
        tabMaintainItem.TabVisible(kTabCategories) = True
        
        If (mbECIsActivated Or mbSOIsActivated) Then
            tabMaintainItem.TabVisible(kTabCategories) = True
        Else
            tabMaintainItem.TabVisible(kTabCategories) = False
        End If

        'Initialize webItemImage to ABOUT:BLANK.
        webItemImage.Navigate2 kINETAboutBlank
    Else
        tabMaintainItem.TabVisible(kTabCategories) = False
    End If

    tabMaintainItem.Tab = kTabDesc
    
    If ItemKeyFlag Then
        sItem = gvCheckNull(moClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey = " & Format$(giItemKey)), SQL_CHAR)
        lkuMain.Text = sItem
        mbFromInvt = True
    Else
        lkuMain.Enabled = True
    End If
    
    If Len(Trim(txtSerialNumberMask)) = 0 Then
        txtSerialNumberIncrement.Text = ""
        txtSerialNumberIncrement.Enabled = False
    Else
        txtSerialNumberIncrement.Enabled = True
    End If
    
    'Set the Customer Fields object.
    Set moUF = CreateObject("cizdbdl1.clsUserFld")
    moUF.Init moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, _
                moSotaObjects, moClass.moFramework

    mbIsPressF5 = False

    'Bind the Copy Manager to the Data Manager and establish questions.
    BindCopyMgr

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
                
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetcdgGetImage()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           SetcdgGetImage set up image location for common dialog
'
'   Parameters:
'           None.
'************************************************************************
    With cdgGetImage
        .InitDir = msItemImageLocation
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetcdgGetImage", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'Desc: If the data has changed, then prompt to save changes.
'***********************************************************************
    Dim iConfirmUnload As Integer

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If

   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If ((mbECIsActivated Or mbSOIsActivated) _
And mbAssignedProdCatsSaveIsNeeded) Then
        moDmItemForm.SetDirty True
    End If

    If (moClass.mlError = 0 _
And (Not moDmItemForm Is Nothing)) Then
        'Validate before saving.
        If bPreSave() = False Then
            GoTo CancelShutDown
        End If

        'Perform Data Manager save operation now.
        iConfirmUnload = moDmItemForm.ConfirmUnload()
    
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                mbExistingID = False
                
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                
            Case Else
                MsgBox "Unexpected Confirm Unload Return Value: " & CVar(iConfirmUnload)
        End Select
    End If
    
    'Check all other forms  that may have been loaded from this main form.
    'If there are any visible forms, then this means the form is active.
    'Therefore, cancel the shutdown.
    If gbActiveChildForms(Me) Then
        GoTo CancelShutDown
    End If
    
    Select Case UnloadMode
        Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
                    
        Case Else
            'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is Normal or Drill-Around, have the object unload itself.
             Select Case mlRunMode
                Case kContextAOF
                    Me.Hide
                    GoTo CancelShutDown
                
                Case kItemRunDD
                    Me.Hide
                    GoTo CancelShutDown
                    
                Case Else
                    moClass.miShutDownRequester = kUnloadSelfShutDown
            End Select
    End Select
        
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'kFrameworkShutDown
            'Do nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next


    'Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1


    

    Set moSotaObjects = Nothing         ' Sage MAS 500 Child objects collection
    Set moOptions = Nothing
    
    If Not moUF Is Nothing Then
        moUF.UnloadMe
        Set moUF = Nothing
    End If
     
    
    Set moContextMenu = Nothing         ' context menu class
    Set moGridNav = Nothing
    Set moGridNavKit = Nothing
    
'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (START)
'*****************************************************************************************************
'*****************************************************************************************************
    
    If (Not (moDmItemExt Is Nothing)) Then
        moDmItemExt.UnloadSelf
        Set moDmItemExt = Nothing
    End If
    
    If (Not (moDmCOATestsGrid Is Nothing)) Then
        moDmCOATestsGrid.UnloadSelf
        Set moDmCOATestsGrid = Nothing
    End If
    
    
    If Not (moGMCOATests Is Nothing) Then
        moGMCOATests.UnloadSelf
        Set moGMCOATests = Nothing
    End If
'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (STOP)
'*****************************************************************************************************
'*****************************************************************************************************
 
    If Not (moGMUOM Is Nothing) Then
        moGMUOM.UnloadSelf
        Set moGMUOM = Nothing
    End If
    
        
    If Not (moGMKitComp Is Nothing) Then
        moGMKitComp.UnloadSelf
        Set moGMKitComp = Nothing
    End If
    
    If Not (moGMItemImage Is Nothing) Then
        moGMItemImage.UnloadSelf
        Set moGMItemImage = Nothing
    End If

    
    If (Not (moDmKitForm Is Nothing)) Then
        moDmKitForm.UnloadSelf
        Set moDmKitForm = Nothing
    End If
    
    If (Not (moDmUOMGrid Is Nothing)) Then
        moDmUOMGrid.UnloadSelf
        Set moDmUOMGrid = Nothing
    End If
    
    If (Not (moDmKitCompGrid Is Nothing)) Then
        moDmKitCompGrid.UnloadSelf
        Set moDmKitCompGrid = Nothing
    End If
    
    If Not moDmItemImage Is Nothing Then
        moDmItemImage.UnloadSelf
        Set moDmItemImage = Nothing
    End If
   

    If (Not (moDmItemDescForm Is Nothing)) Then
        moDmItemDescForm.UnloadSelf
        Set moDmItemDescForm = Nothing
    End If
    
    If (Not (moDmItemForm Is Nothing)) Then
        moDmItemForm.UnloadSelf
        Set moDmItemForm = Nothing
    End If
    
    'Clear up copy manager object
    If (Not (IsEmpty(m_CopyMgr))) Then
        m_CopyMgr.UnloadSelf
        Set m_CopyMgr = Nothing
    End If
    
     #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
    
       TerminateControls Me

#End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then

       gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
tabMaintainItem, SSPanel1(0), SSPanel1(1), SSPanel1(2), _
SSPanel1(3), SSPanel1(4), SSPanel1(5), SSPanel1(6), SSPanel1(7), grdCOALabTests, grdUOM, grdCompKits    'SGS DEJ 5/18/12    added SSPanel1(7) and grdCOALabTests
           
      'resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
tabMaintainItem, SSPanel1(0), SSPanel1(1), SSPanel1(2), _
SSPanel1(3), SSPanel1(4), SSPanel1(5), SSPanel1(6), SSPanel1(7), grdCOALabTests, grdUOM, grdCompKits    'SGS DEJ 5/18/12    added SSPanel1(7) and grdCOALabTests
       
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
        
        ' The following part is added by Ashish on March 12 1999 for locating the
        ' navigators at proper place when UI gets resized.
        If tabMaintainItem.Tab = kTabKit Then
            moGMKitComp.Scroll
        ElseIf tabMaintainItem.Tab = kTabUOM Then
            moGMUOM.Scroll
'SGS DEJ    (START)
        ElseIf tabMaintainItem.Tab = kTabCOATests Then
            moGMCOATests.Scroll
'SGS DEJ    (STOP)
        End If

    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "Form_Resize", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    ' perform proper cleanup
    ' *** remember to clean up any objects you create here
    Set moClass = Nothing               ' class object
   
    On Error GoTo ExpectedErrorRoutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
        
ExpectedErrorRoutine:
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

' *************************************************************************
'    DESC:  This sub contains the code that binds the Data Manager object
'           to the forms's controls.
'   PARMS:  None.
' *************************************************************************
Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDmItemForm = New clsDmForm
    
    With moDmItemForm
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
            .CompanyId = msCompanyID
            .Table = "timItem"
            .UniqueKey = "ItemID"
            .SaveOrder = 1
        Set .ValidationMgr = SOTAVM
            
        'Specific to Item Main tab
        .Bind Nothing, "ItemKey", SQL_INTEGER
        .Bind lkuMain, "ItemID", SQL_CHAR
        .Bind ddnItemType, "ItemType", SQL_SMALLINT, kDmUseItemData
        .Bind ddnStatus, "Status", SQL_SMALLINT, kDmUseItemData
        .BindLookup lkuItemClass
        .Bind ddnTrackMethod, "TrackMeth", SQL_SMALLINT, kDmUseItemData
        .Bind numShelfLife, "ShelfLife", SQL_SMALLINT
        .BindLookup lkuDefltWarehouse, kDmSetNull
        .BindLookup lkuCommoCode, kDmSetNull
        .Bind ddnSalesTaxClass, "STaxClassKey", SQL_INTEGER, kDmUseItemData Or kDmSetNull
        .BindLookup lkuFreightClass, kDmSetNull
        .Bind txtDateEstb, "DateEstab", SQL_DATE
        .Bind chkInternal, "InternalLongDesc", SQL_SMALLINT
        .Bind chkSeasonal, "Seasonal", SQL_SMALLINT
        .Bind chkHazMat, "HazMat", SQL_SMALLINT
        .Bind ddnValuation, "ValuationMeth", SQL_SMALLINT, kDmUseItemData
        .Bind numStdCost, "StdUnitCost", SQL_DECIMAL
        .Bind numStdPrice, "StdPrice", SQL_DECIMAL
        .Bind ddnProvider, "WarrantyProvider", SQL_SMALLINT, kDmUseItemData
        .Bind numNOfDays, "WarrantyDays", SQL_SMALLINT
        .Bind ddnFixedAssetTemplate, "FASAssetTemplate", SQL_CHAR
        .Bind txtSerialNumberMask, "SerialMask", SQL_CHAR
        .Bind txtSerialNumberIncrement, "SerialIncrement", SQL_CHAR
        
        'Specific to UOM
        .BindLookup lkuStockUOM, kDmSetNull
        .BindLookup lkuPurchaseUOM, kDmSetNull
        .BindLookup lkuSales, kDmSetNull
        .BindLookup lkuPrice, kDmSetNull
        .Bind ddnAdjQtyRoundMeth, "AdjQtyRoundMeth", SQL_SMALLINT, kDmUseItemData
        .Bind chkAllowDecQuantities, "AllowDecimalQty", SQL_SMALLINT
        
        'Specific to Sales Product Line
        .BindLookup lkuCustPriceGroup, kDmSetNull
        .Bind numRestockingRatesHide, "RestockRate", SQL_DECIMAL
        .Bind numMinSalesQty, "MinSaleQty", SQL_DECIMAL
        .Bind numMandatorySalesMulti, "SaleMultiple", SQL_SMALLINT
        .Bind chkReturnsAllowed, "AllowRtrns", SQL_SMALLINT
        .Bind chkAllowCostOvrd, "AllowCostOvrd", SQL_SMALLINT
        .Bind chkInclOnPackList, "InclOnPackList", SQL_SMALLINT

        .BindLookup lkuProdLine, kDmSetNull
        .BindLookup lkuCommCls, kDmSetNull
        .Bind numTargetMarginHide, "TargetMargin", SQL_DECIMAL
        .Bind chkAllowPriceOvrd, "AllowPriceOvrd", SQL_SMALLINT
        .Bind chkSubToTradeDisc, "SubjToTradeDisc", SQL_SMALLINT
        .Bind chkDropShipFlg, "AllowDropShip", SQL_SMALLINT
                
        'Specific to Purchase Product Line
        .BindLookup lkuPuchProdLine, kDmSetNull
        .BindLookup lkuPOMatchTolerance, kDmSetNull
        .Bind numPriceSeq, "PriceSeq", SQL_NUMERIC
        .Bind numPeriodUsage, "PerUsageOrdLimit", SQL_SMALLINT
        .Bind chkInternalDelvrRequired, "IntrnlDeliveryReq", SQL_SMALLINT
        .Bind chkRcptRequired, "RcptReq", SQL_SMALLINT
        
        .Bind numDfltSaleQty, "DfltSaleQty", SQL_DECIMAL
        .Bind numMinGrossProfitPctHide, "MinGrossProfitPct", SQL_DECIMAL

        'Specific to GL Accounts
        .BindLookup glaCOS, kDmSetNull
        .BindLookup glaExpense, kDmSetNull
        .BindLookup glaReturns, kDmSetNull
        .BindLookup glaSales, kDmSetNull

        'User Fields
        If mbIMIsActivated Then
            .Bind Nothing, "UserFld1", SQL_CHAR, kDmSetNull
            .Bind Nothing, "UserFld2", SQL_CHAR, kDmSetNull
            .Bind Nothing, "UserFld3", SQL_CHAR, kDmSetNull
            .Bind Nothing, "UserFld4", SQL_CHAR, kDmSetNull
            .Bind Nothing, "UserFld5", SQL_CHAR, kDmSetNull
            .Bind Nothing, "UserFld6", SQL_CHAR, kDmSetNull
        End If

        'UpdateCounter
        .Bind Nothing, "UpdateCounter", SQL_INTEGER

        'CreateType
        .Bind Nothing, "CreateType", SQL_SMALLINT
        
        .Init
    End With

    'Link the timItemDescription table.
    Set moDmItemDescForm = New clsDmForm
    With moDmItemDescForm
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .Table = "timItemDescription"
        .UniqueKey = "ItemKey"
        .SaveOrder = 2
        Set .Parent = moDmItemForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER

        .Bind Nothing, "LanguageID", SQL_INTEGER
        .Bind txtLongDesc, "LongDesc", SQL_CHAR
        .Bind txtShortDesc, "ShortDesc", SQL_CHAR

        .Init
    End With

    'Link the timKit table.
    Set moDmKitForm = New clsDmForm

    With moDmKitForm
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .Table = "timKit"
        .UniqueKey = "KitItemKey"
        .SaveOrder = 3
        Set .Parent = moDmItemForm
        .ParentLink "KitItemKey", "ItemKey", SQL_INTEGER

        .Bind chkExplodeOnPickTicket, "ExplOnPickTckt", SQL_SMALLINT
        .Bind chkEplodeOnInvoice, "ExplOnInvc", SQL_SMALLINT
        .Bind chkPriceAsCompTotal, "PriceAsCompTotal", SQL_SMALLINT
        .Bind chkAllowRtrnsCustomBTO, "AllowRtrnsCustomBTO", SQL_SMALLINT
        .Bind chkAllowRtrnsPartialBTO, "AllowRtrnsPartialBTO", SQL_SMALLINT
        
        .Init
    End With

    'Link the timItemUnitOfMeas table.
    Set moDmUOMGrid = New clsDmGrid

    With moDmUOMGrid
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdUOM
        .Table = "timItemUnitOfMeas"
        .UniqueKey = "ItemKey , TargetUnitMeasKey"
        .SaveOrder = 4
        Set .Parent = moDmItemForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER

        .BindColumn "TargetUnitMeasKey", Nothing, SQL_INTEGER
        .BindColumn "UseStdConv", kUOMColUseStdConv, SQL_SMALLINT
        .BindColumn "ConversionFactor", kUOMColConversionFactorDBValue, SQL_DECIMAL
        .BindColumn "ConversionType", kUOMColConversionTypeInt, SQL_INTEGER
        .BindColumn "UnitVolume", kUOMColVolume, SQL_FLOAT
        .BindColumn "UnitWeight", kUOMColWeight, SQL_FLOAT
        .BindColumn "UPC", kUOMColUPC, SQL_CHAR
        .BindColumn "UseForPurchases", kUOMColUseForPurchases, SQL_SMALLINT
        .BindColumn "UseForSales", kUOMColUseForSales, SQL_SMALLINT
        .BindColumn "IsSmallestUOM", kUOMColIsSmallestUOM, SQL_SMALLINT
        
        .LinkSource "tciUnitMeasure", "tciUnitMeasure.UnitMeasKey = timItemUnitOfMeas.TargetUnitMeasKey", kDmJoin
        .Link kUOMColTargetUOM, "UnitMeasID"
        
        .Init
    End With

    'Link the timKitCompList table.
    Set moDmKitCompGrid = New clsDmGrid

    With moDmKitCompGrid
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdCompKits
        .Table = "timKitCompList"
        .UniqueKey = "KitItemKey, SeqNo"
        .SaveOrder = 5
        Set .Parent = moDmItemForm
        .ParentLink "KitItemKey", "ItemKey", SQL_INTEGER

        .BindColumn "SeqNo", kKitColSeqNo, SQL_INTEGER      'added SS on 0916
        .BindColumn "CompItemQty", kKitColQuantity, SQL_INTEGER
        .BindColumn "CompItemKey", kKitColCompKey, SQL_INTEGER     'added SS on 0916

        .LinkSource "timItem", "timItem.ItemKey = timKitCompList.CompItemKey", kDmJoin
        .Link kKitColComponentItem, "ItemID"
                
        .Init
    End With
    
    'Link the timItemImage table.
    If (mbECIsActivated Or mbSOIsActivated) Then
        Set moDmItemImage = New clsDmGrid
        
        With moDmItemImage
            Set .Form = Me
            Set .Session = moClass.moSysSession
            Set .Database = moClass.moAppDB
            Set .Grid = grdItemImages
            .Table = "timItemImage"
            .UniqueKey = "ItemKey, ImageNo " 'ItemImageKey"
            .SaveOrder = 7
            Set .Parent = moDmItemForm
            .ParentLink "ItemKey", "ItemKey", SQL_INTEGER
            
            .BindColumn "ImageName", kItemIColImageName, SQL_CHAR
            .BindColumn "ImagePath", kItemIColImagePath, SQL_CHAR
            .BindColumn "ImageNo", kItemIColImageNo, SQL_SMALLINT

            .Init
        End With
    End If
    
'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (START)
'*****************************************************************************************************
'*****************************************************************************************************
    Call SGSBindDM
'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (STOP)
'*****************************************************************************************************
'*****************************************************************************************************

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

' *************************************************************************
'
'    DESC:  This sub contains the code that is called in responce to a
'           toolbar button being pressed
'
'   PARMS:  sKey = String identifier of button that was clicked
'
' *************************************************************************
Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim iActionCode As Integer
    Dim iConfirmUnload As Integer
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant
    Dim miVal As Integer
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If

    'VB 5 does not automatically fire LostFocus event when pressing toolbar.
    Me.SetFocus
    DoEvents

    'True if user clicks on the Cancel button.
    mbIsClickCancel = False
    mbSaveDone = False
    mbCallFromSave = True
    
    iActionCode = kDmFailure
    miHoldSaveTab = tabMaintainItem.Tab
    
    If mbTBRejectUOM = True Then
        mbTBRejectUOM = False
        Exit Sub
    End If
    
    Select Case sKey
        Case kTbFinish, kTbFinishExit
             mbInSaveRoutine = True
             iActionCode = doTBFinish()   'Call Finish Function to do a finish task.
             SetTBMemoState
             mbInSaveRoutine = False
             
        Case kTbSave
             mbInSaveRoutine = True
             iActionCode = doTBSave()     'Call Save Function to do a save task.
             mbInSaveRoutine = False
             
        Case kTbCancel, kTbCancelExit     'Call Cancel Function to do a cancel task.
            iActionCode = doTBCancel()
            SetTBMemoState
        
        Case kTbDelete
            iActionCode = doTBDelete()    'Call Delete Function to do a delete task.
            SetTBMemoState
        
        Case kTbCopyFrom                'Copy From

            'Validate before saving.
            If bPreSave() = False Then
                Exit Sub
            End If
            
            'The concerned task has to be launched from here.
            If moDmItemForm.ConfirmUnload(True) Then                 'Have user save changes
                ' Display Copy Form
                m_CopyMgr.Copy
            End If
            SetTBMemoState
        
        Case kTbRenameId
            'Show security event form.
            'Check the user id typed in can override (no cancel hit).
            If miModule = kModuleCI Then
                sID = CStr("CICHANGEID")
            Else
                sID = CStr("CHGITMID")
            End If
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) <> 0 Then
                mbRename = True
                moDmItemForm.RenameID
            End If
            
        Case kTbPrint
            'Validate before saving.
            If bPreSave() = False Then
                Exit Sub
            End If
            
            'The concerned task has to be launched from here.
            If moDmItemForm.ConfirmUnload(True) Then                 'Have user save changes
                If miModule = kModuleCI Then          '2 for CI
                    gPrintTask moClass.moFramework, ktskPrint2
                ElseIf miModule = kModuleIM Then      '7 for IM
                    gPrintTask moClass.moFramework, ktskPrint1
                End If
            End If
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
            
        Case kTbMemo
            iActionCode = doTBMemo()
        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmItemForm, moClass
    End Select
    
    Select Case iActionCode
        Case kDmSuccess
            moClass.lUIActive = kChildObjectInactive
            SetMyValues sKey
            mbSaveDone = True
            mbNewRecord = False
            
            If (sKey = kTbFinish) Or (sKey = kTbCancel) Then
                mlItemKey = 0
            End If
            
            mfPriceAsGot = 0
            mbCatTypeChanged = False         'Added 0111 for Cat change.
            mbIsClickCancel = False          'Moved up on 0111.

            If sKey = kTbSave Then
                miStat = ddnStatus.ItemData
                msPriceUOM = lkuPrice.Text

                If Not ddnItemType.ItemData = kItemTypeBTOKit Then '(bIsInventory(ddnItemType.ItemData) And
                    FillHiddenKey
                End If

                If (ddnItemType.ItemData = kItemTypeBTOKit _
                    Or ddnItemType.ItemData = kItemTypeAssembledKit) Then
                    If Not tabMaintainItem.Tab = kTabKit Then
                        gGridSetActiveCell grdCompKits, kFirstCell, kKitColComponentItem
                    End If

                    msKitCompVal = Trim$(gsGridReadCellText(grdCompKits, grdCompKits.ActiveRow, kKitColComponentItem))
                End If

                If Not tabMaintainItem.Tab = kTabUOM Then
                    gGridSetActiveCell grdCompKits, kFirstCell, kKitColComponentItem
                End If
                
                msUOMVal = Trim$(gsGridReadCellText(grdUOM, grdUOM.ActiveRow, kUOMColTargetUOM))
            Else
                msPriceUOM = ""
                msUOMVal = ""
                msKitCompVal = ""
            End If
            
        Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
    End Select
    
    mbFromInvt = False
    mbCallFromSave = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function doTBFinish() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer

    If (mbECIsActivated Or mbSOIsActivated) Then
       SetItemImagePict False
       
       If mbAssignedProdCatsSaveIsNeeded Then
            moDmItemForm.SetDirty True
       End If
       
       mbClearListBox = True
    End If
    
    'Validate before saving.
    If bPreSave() Then
       FirstSave
       
       iActionCode = SecondSave
       
       If iActionCode = kDmSuccess Then
          iActionCode = moDmItemForm.Action(kDmFinish)
          
          If iActionCode = kDmSuccess Then
             ClearForm
          End If
       End If
    End If
    
    doTBFinish = iActionCode

'+++ VB/Rig Begin Pop +++
            Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveAssignedProdCatsForItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

Private Function doTBSave() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer
    Dim lItemImageRow As String
    Dim lCounter As Integer
    Dim sUOMID As String
    
    lItemImageRow = grdItemImages.Row

    If (mbECIsActivated Or mbSOIsActivated) Then
        If mbAssignedProdCatsSaveIsNeeded Then
            moDmItemForm.SetDirty True
        End If
    End If
                   
    'Validate before saving.
    If bPreSave() Then
        DoEvents

        FirstSave
        
        iActionCode = SecondSave
        If iActionCode = kDmSuccess Then
            iActionCode = moDmItemForm.Save(True)
            
            If iActionCode = kDmSuccess Then
                'Set the 'Row From DB' Column value to 1 so that every row is marked as old.
                For lCounter = 1 To grdUOM.MaxRows - 1
                    grdUOM.Row = lCounter
                    grdUOM.Col = kUOMColTargetUOM
                    sUOMID = grdUOM.Value
                    
                    If Len(Trim$(sUOMID)) > 0 Then
                        grdUOM.Col = kUOMColRowFromDB
                        grdUOM.Value = 1
                        'UOMIDs in the DB can no longer be changed.
                        gGridLockCell grdUOM, kUOMColTargetUOM, CLng(lCounter)
                    End If
                Next
                If navGrid.Visible And glGridGetActiveRow(grdUOM) < lCounter Then
                    navGrid.Visible = False
                End If
            End If
        End If
    End If

    If (mbECIsActivated Or mbSOIsActivated) Then
        grdItemImages.Row = lItemImageRow
    End If
      
    doTBSave = iActionCode
            
'+++ VB/Rig Begin Pop +++
        Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveAssignedProdCatsForItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function doTBMemo() As Integer
'Called from  HandleToolBarClick
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iReply As Integer
    Dim lItemKey As Long
    Dim iActionCode As Integer
    Dim iEntityType As Integer
                
    iEntityType = kEntTypeIMItem
            
    Me.Enabled = False

    If Len(Trim(lkuMain)) = 0 Then Exit Function 'Abort if empty
    If moDmItemForm.State = kDmStateAdd Then
        iReply = giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveBeforeAdd, CVar(txtShortDesc))
        If iReply = kretYes Then
            iActionCode = doTBSave()
            If iActionCode <> kDmSuccess Then
                GoTo Finish
            End If
        Else
            GoTo Finish 'Replied "No" to "Save?"
        End If
    End If
    lItemKey = gvCheckNull(moDmItemForm.GetColumnValue("ItemKey"), SQL_INTEGER)
    gLaunchMemo Me, moClass.moFramework, moSotaObjects, iEntityType, lItemKey, _
                lkuMain, txtShortDesc, moClass.moSysSession.BusinessDate
    SetTBMemoState
    
'+++ VB/Rig Begin Pop +++
Finish:
    doTBMemo = iActionCode
    Me.Enabled = True
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "doTBMemo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SetTBMemoState()
    Dim lItemKey As Long
    Dim iEntityType As Integer
                
    iEntityType = kEntTypeIMItem
    
    lItemKey = gvCheckNull(moDmItemForm.GetColumnValue("ItemKey"), SQL_INTEGER)
    gSetMemoToolBarState moClass.moAppDB, lItemKey, iEntityType, msCompanyID, tbrMain

End Sub

Private Function doTBCancel() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer

    If navGrid.Visible = True Then
        navGrid.Visible = False
    End If

    If navGridKit.Visible = True Then
        navGridKit.Visible = False
    End If
    
    mbIsCancelButton = True
     
    'True if click on cancel button
    mbIsClickCancel = True
    moDmItemForm.SetDirty False, True
     
    iActionCode = moDmItemForm.Action(kDmCancel)
    If iActionCode = kDmSuccess Then
        ClearForm

        If (mbECIsActivated Or mbSOIsActivated) Then
            SetItemImagePict False
            lstAvailProdCatID.Clear
            lstAssignProdCatID.Clear
        End If
    End If
     
    doTBCancel = iActionCode
            
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveAssignedProdCatsForItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
            
End Function

Private Function doTBDelete() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer
    
    DoEvents
    
    If (Me.ActiveControl.Name = lkuMain.Name _
      And lkuMain.EnabledText = False) Then
        gbSetFocus Me, txtShortDesc
    End If
    
    DoEvents
    
    iActionCode = moDmItemForm.Action(kDmDelete)

    If (mbECIsActivated Or mbSOIsActivated) Then
        SetItemImagePict False
        lstAvailProdCatID.Clear
        lstAssignProdCatID.Clear
        ShowImage webItemImage, kINETAboutBlank
    End If
            
'+++ VB/Rig Begin Pop +++
        Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveAssignedProdCatsForItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function
Private Function bSaveAssignedProdCatsForItem(ByVal lItemKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
'************************************************************************
'   Description:
'           bSaveAssignedProdCatsForItem removes all product category that have
'           a current item key and insert it back in from the Categories Assigned to
'           item list box.
'   Parameters:
'           None.
'************************************************************************

    Dim lAssignedItems As Long
    Dim lProdCatIndex As Long
    Dim lProdCatKey As Long
    
    Dim lItemKeyA As Long
    If lItemKey = 0 Then
       lItemKeyA = gvCheckNull(moClass.moAppDB.Lookup _
("ItemKey", "timItem", "ItemID = " & gsQuoted(lkuMain.Text) & _
" AND ItemType= " & ddnItemType.ItemData & " AND CompanyID = " & _
gsQuoted(msCompanyID)), SQL_INTEGER)
    Else
        lItemKeyA = lItemKey
    End If
    

    bSaveAssignedProdCatsForItem = False
    
    'Begin a database transaction
    moClass.moAppDB.BeginTrans

    'First, delete all assigned items for this Product Category
    If bDeleteAssignedProdCatsforItem(moClass.moAppDB, lItemKey) = False Then
        'Rollback the database transaction
        moClass.moAppDB.RollbackTrans

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    'Loop through all of the items in the Assigned Items list box
    lAssignedItems = lstAssignProdCatID.ListCount
    If lAssignedItems > 0 Then
        For lProdCatIndex = 0 To lAssignedItems - 1
            'Get the Item Key for the Assigned Item
            lProdCatKey = lstAssignProdCatID.ItemData(lProdCatIndex)

            If lProdCatKey > 0 Then
                'Now insert the assigned items for this Product Category
                If bInsertAssignedProdcatsforItem(moClass.moAppDB, lItemKeyA, lProdCatKey) = False Then
                    'Rollback the database transaction
                    moClass.moAppDB.RollbackTrans
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
        Next lProdCatIndex
    End If
    
    'Commit the database transaction now
    moClass.moAppDB.CommitTrans

    bSaveAssignedProdCatsForItem = True

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaveAssignedProdCatsForItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

' *************************************************************************
'    DESC:  This subroutine binds the context menu object to the form.
'   PARMS:  None.
' *************************************************************************
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With moContextMenu
        Set .Form = frmMaintainItem
    
        .Bind "*APPEND", lkuMain.hwnd, kEntTypeIMItem
     
        .BindGrid moGMUOM, grdUOM.hwnd
        .Bind "*APPEND", grdUOM.hwnd
        .BindGrid moGMKitComp, grdCompKits.hwnd

        .Bind "IMDA08", lkuDefltWarehouse.hwnd, kEntTypeIMWarehouse

        If (mbECIsActivated Or mbSOIsActivated) Then
           .BindGrid moGMItemImage, grdItemImages.hwnd
        End If

        'SGS DEJ    (START)
        .BindGrid moGMCOATests, grdCOALabTests.hwnd
        'SGS DEJ    (STOP)

        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindCopyMgr()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc:  Bind the Copy Manager to the appropriate data manager
'        and establish questions, if any
'************************************************************************

    Set m_CopyMgr = New clsCopyMgr
    
    With m_CopyMgr
        ' Add question(s)
        .AddQuestion "Copy Categories", vbUnchecked, True
        .AddQuestion "Copy Images", vbUnchecked, True
        
        ' Init the Copy Manager
        .Init moDmItemForm, "Copy Item", "New Item", False, True
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindCopyMgr", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

' ********************************************************************************
'    Desc:  This procedure formats the UOM grid.
'   Parms:  None.
' ********************************************************************************
Private Sub FormatUOMGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim VolumeUnitMeasKey As Long
    Dim VolumeUnitMeasKeyStr As String
    Dim WeightUnitMeasKey As Long
    Dim WeightUnitMeasKeyStr As String
    
    gGridSetProperties grdUOM, kMaxUOMCols, kGridDataSheet

    'Setup maximum number of rows and columns.
    gGridSetMaxRows grdUOM, 1

    gGridSetColumnType grdUOM, kUOMColTargetUOM, SS_CELL_TYPE_EDIT, 6
    gGridSetHeader grdUOM, kUOMColTargetUOM, gsBuildString(kIMUnitOfMeasure, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdUOM, kUOMColTargetUOM, 1300

    gGridSetColumnType grdUOM, kUOMColUseStdConv, SS_CELL_TYPE_CHECKBOX
    gGridSetHeader grdUOM, kUOMColUseStdConv, gsBuildString(kIMUseStdConv, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdUOM, kUOMColUseStdConv, 1000
    gGridLockColumn grdUOM, kUOMColUseStdConv

    gGridSetColumnType grdUOM, kUOMColUseForPurchases, SS_CELL_TYPE_CHECKBOX
    gGridSetHeader grdUOM, kUOMColUseForPurchases, gsStripChar(lblPurchase, kAmpersand)
    gGridSetColumnWidth grdUOM, kUOMColUseForPurchases, 900

    gGridSetColumnType grdUOM, kUOMColUseForSales, SS_CELL_TYPE_CHECKBOX
    gGridSetHeader grdUOM, kUOMColUseForSales, gsStripChar(lblSales, kAmpersand)
    gGridSetColumnWidth grdUOM, kUOMColUseForSales, 900
    
    gGridSetColumnType grdUOM, kUOMColConversionType, SS_CELL_TYPE_COMBOBOX
    gGridSetHeader grdUOM, kUOMColConversionType, gsBuildString(kConversionType, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdUOM, kUOMColConversionType, 2500

    gGridSetColumnType grdUOM, kUOMColConversionTypeInt, SS_CELL_TYPE_INTEGER
    gGridSetHeader grdUOM, kUOMColConversionTypeInt, "Conversion Type Int"
    gGridSetColumnWidth grdUOM, kUOMColConversionTypeInt, 2500
    gGridHideColumn grdUOM, kUOMColConversionTypeInt
    
    gGridSetColumnType grdUOM, kUOMColConversionFactor, SS_CELL_TYPE_EDIT, 26
    gGridSetHeader grdUOM, kUOMColConversionFactor, gsBuildString(kConversionFactor, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdUOM, kUOMColConversionFactor, 2500

    'This column is used to hold the value that is stored in the DB.  For non-std, this and the visible column are equal
    'For std conversions - this column holds a 0 and the visible one displays the standard conversion
    'This column is updated with any changes prior to update or insert
    gGridSetColumnType grdUOM, kUOMColConversionFactorDBValue, SS_CELL_TYPE_EDIT, 26
    gGridSetHeader grdUOM, kUOMColConversionFactorDBValue, "Conversion Factor - Hidden"
    gGridSetColumnWidth grdUOM, kUOMColConversionFactorDBValue, 2500
    gGridHideColumn grdUOM, kUOMColConversionFactorDBValue

    'These captions should get populated from the PopulateVal procedure.
    'Get the system unit of measures and put their ID as labels for CuFt. and Lbs. columns.
    VolumeUnitMeasKeyStr = gsGetValidStr(moClass.moAppDB.Lookup("UnitMeasID", "timOptions WITH (NOLOCK), tciUnitMeasure WITH (NOLOCK)", "timOptions.CompanyID = " & gsQuoted(msCompanyID) & " AND tciUnitMeasure.UnitMeasKey = timOptions.VolumeUnitMeasKey"))
    If Len(Trim$(VolumeUnitMeasKeyStr)) = 0 Then
        VolumeUnitMeasKeyStr = gsBuildString(kVolume, moClass.moAppDB, moClass.moSysSession)
    End If

    WeightUnitMeasKeyStr = gsGetValidStr(moClass.moAppDB.Lookup("UnitMeasID", "timOptions WITH (NOLOCK), tciUnitMeasure WITH (NOLOCK)", "timOptions.CompanyID = " & gsQuoted(msCompanyID) & " AND tciUnitMeasure.UnitMeasKey = timOptions.WeightUnitMeasKey"))
    If Len(Trim$(WeightUnitMeasKeyStr)) = 0 Then
        WeightUnitMeasKeyStr = gsBuildString(kWeight, moClass.moAppDB, moClass.moSysSession)
    End If

    gGridSetColumnType grdUOM, kUOMColVolume, SS_CELL_TYPE_EDIT, 26
    gGridSetHeader grdUOM, kUOMColVolume, VolumeUnitMeasKeyStr
    gGridSetColumnWidth grdUOM, kUOMColVolume, 2500

    gGridSetColumnType grdUOM, kUOMColWeight, SS_CELL_TYPE_EDIT, 26
    gGridSetHeader grdUOM, kUOMColWeight, WeightUnitMeasKeyStr
    gGridSetColumnWidth grdUOM, kUOMColWeight, 2500

    gGridSetColumnType grdUOM, kUOMColUPC, SS_CELL_TYPE_EDIT, 15
    gGridSetHeader grdUOM, kUOMColUPC, gsBuildString(kUPC, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdUOM, kUOMColUPC, 2000
    
    gGridSetColumnType grdUOM, kUOMColUOMKey, SS_CELL_TYPE_EDIT, 6
    gGridHideColumn grdUOM, kUOMColUOMKey

    gGridSetColumnType grdUOM, kUOMColIsSmallestUOM, SS_CELL_TYPE_INTEGER
    gGridHideColumn grdUOM, kUOMColIsSmallestUOM
    
    With grdUOM
        'Scroll bars
        .ScrollBars = ScrollBarsBoth
    
        'Let the user resize the columns.
        .UserResizeCol = SS_USER_RESIZE_ON

        'Added so that you cannot get to locked columns.
        .EditModePermanent = True

        'Set column widths.
        .UnitType = SS_CELL_UNIT_TWIPS

        .Row = -1

        .Col = kUOMColConversionFactor
        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT

        .Col = kUOMColVolume
        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT

        .Col = kUOMColWeight
        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT
    
        .Col = kUOMColConversionType
        .TypeComboBoxList = moConvTypeList.ListData
    End With

    'Hide certain columns.
    gGridSetColumnType grdUOM, kUOMColRowFromDB, SS_CELL_TYPE_INTEGER
    gGridHideColumn grdUOM, kUOMColRowFromDB

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatUOMGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub FormatItemImagesGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
' ********************************************************************************
'    Desc:  This procedure formats the Item Images grid.
'   Parms:  None.
' ********************************************************************************
'+++ VB/Rig End +++
    'Define misc settings, maximum rows, columns and unit type.
    If miSecurityLevel = kSecLevelDisplayOnly Then
        gGridSetProperties grdItemImages, kMaxItemImageCols, kGridDataSheetNoAppend
    Else
        'Use datasheet grid formatting.
        gGridSetProperties grdItemImages, kMaxItemImageCols, kGridDataSheet
    End If
    
    grdItemImages.UserResizeCol = SS_USER_RESIZE_ON
    
    'Added so that you cannot get to locked columns.
    grdItemImages.EditModePermanent = True
    
    'Setup maximum number of rows and columns.
    gGridSetMaxRows grdItemImages, 1
    
    'Set column widths.
    grdItemImages.UnitType = SS_CELL_UNIT_TWIPS
    
    gGridSetColumnType grdItemImages, kItemIColImageKey, SS_CELL_TYPE_STATIC_TEXT
    gGridHideColumn grdItemImages, kItemIColImageKey
    
    gGridSetColumnType grdItemImages, kItemIColImageNo, SS_CELL_TYPE_STATIC_TEXT
    gGridSetHeader grdItemImages, kItemIColImageNo, "Image No" 'gsBuildString(kIMItemImagesLocation, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdItemImages, kItemIColImageNo, 550
    gGridHideColumn grdItemImages, kItemIColImageNo
    
    gGridSetColumnType grdItemImages, kItemIColImageName, SS_CELL_TYPE_EDIT, kItemIColImageNameLength '201
    gGridSetHeader grdItemImages, kItemIColImageName, gsBuildString(kIMItemImagesName, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdItemImages, kItemIColImageName, 1200
    
    gGridSetColumnType grdItemImages, kItemIColImagePath, SS_CELL_TYPE_EDIT, kItemIColImagePathLength '128
    gGridSetHeader grdItemImages, kItemIColImagePath, gsBuildString(kIMItemImagesLocation, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdItemImages, kItemIColImagePath, 3600  '6000
    
    grdItemImages.ScrollBars = ScrollBarsVertical
    grdItemImages.DisplayRowHeaders = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatItemImagesGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

' ********************************************************************************
'    Desc:  Binds the Grid Manager class to the Grid Manager object.
'   Parms:  None
' ********************************************************************************
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Bind the UOM Grid Manager
    Set moGMUOM = New clsGridMgr
    With moGMUOM
        Set .Grid = grdUOM
        Set .dm = moDmUOMGrid
        Set .Form = Me
        
        If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        Set moGridNav = .BindColumn(kUOMColTargetUOM, navGrid)
        Set moGridNav.ReturnControl = txtNavReturn
        .Init
    End With
    
    'Bind the KitComponent Grid Manager
    Set moGMKitComp = New clsGridMgr
    With moGMKitComp
        Set .Grid = grdCompKits
        Set .dm = moDmKitCompGrid
        Set .Form = Me
        
        If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        Set moGridNavKit = .BindColumn(kKitColComponentItem, navGridKit)
        Set moGridNavKit.ReturnControl = txtNavReturnKit
        .Init
    End With

    'Bind the Item Image Grid Manager
    If (mbECIsActivated Or mbSOIsActivated) Then
        With moGMItemImage
            Set .Grid = grdItemImages
            Set .dm = moDmItemImage
            Set .Form = Me
            
            If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
                .GridType = kGridDataSheetNoAppend
            Else
                .GridType = kGridDataSheet
            End If
            
            .Init
        End With
    End If

'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (START)
'*****************************************************************************************************
'*****************************************************************************************************

    Set moGMCOATests = New clsGridMgr
    'Bind the Item COA Test Grid Manager
    With moGMCOATests
        Set .Grid = grdCOALabTests
        Set .dm = moDmCOATestsGrid
        Set .Form = Me

        If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        .GridSortEnabled = True
        
        .AllowAdd = False
        .AllowDelete = False
        .AllowInsert = False
        .AllowMoveRow = False
        
        .Init
    End With
'*****************************************************************************************************
'*****************************************************************************************************
'SGS DEJ 5/21/12    (STOP)
'*****************************************************************************************************
'*****************************************************************************************************

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
       sMyName = "frmMaintainItem"
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sMyName", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub glaCOS_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaCOS_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaExpense_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaExpense_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaReturns_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaReturns_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaSales_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaSales_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim mlcounter As Long
    Dim iHold As Integer
      
    If Col = kKitColComponentItem _
Or Col = kKitColQuantity Then
        iHold = miUOMOrKit
        miUOMOrKit = kTabKit
        moGMKitComp_CellChange Row, Col
        miUOMOrKit = iHold
    Else
        moGMKitComp.Grid_Change Col, Row
    End If
     
    If Col = kKitColComponentItem Then
        If Not msKitCompVal = Trim$(gsGridReadCellText(grdCompKits, Row, kKitColComponentItem)) Then
            gGridUpdateCellText grdCompKits, Row, kKitColQuantity, "0"
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Row > 0 Then
        miUOMOrKit = kTabKit
        
        msKitCompVal = Trim$(gsGridReadCellText(grdCompKits, Row, kKitColComponentItem))
        
        If Col = kKitColComponentItem Then
            moGMKitComp.Scroll
        Else
            'Grid Manager click action
            moGMKitComp.Grid_Click Col, Row
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' The following If condition is necessary because in the readonly mode if the number of rows  =0 then it
    ' sets the lRow = -32767.  This causes a GPF in read only mode
    If grdCompKits.Row > 0 Then
        moGMKitComp.Grid_ColWidthChange Col1
    End If

    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub grdCompKits_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'added on 1208 to fix bug 7232
    If glGridGetActiveCol(grdCompKits) = kFirstCell And glGridGetActiveRow(grdCompKits) = 1 Then
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "grdCompKits_GotFocus", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompKits_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lMyCol As Long
    Dim lMyRow As Long
    Dim iHold As Integer

    lMyRow = grdCompKits.ActiveRow
    lMyCol = grdCompKits.ActiveCol

    If Len(Trim$(gsGridReadCellText(grdCompKits, lMyRow, lMyCol))) = 0 _
And KeyCode = 32 Then
        Exit Sub
    End If

    iHold = miUOMOrKit
    miUOMOrKit = kTabKit
   
   'Grid Manager Key down action
    moGMKitComp.Grid_KeyDown KeyCode, Shift

    miUOMOrKit = iHold

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If (grdCompKits.Col = kKitColQuantity _
And KeyAscii = 45) Then
        KeyAscii = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lItemID As Long
    Dim lItemKey As Long
    Dim mlcounter As Long
    Dim iMovement As Integer
    Dim lNextCol As Long
    Dim lNextRow As Long
    Dim bLocked As Boolean

    moGMKitComp.Grid_LeaveCell Col, Row, NewCol, NewRow
    If NewCol = kKitColComponentItem And Not NewRow = grdCompKits.MaxRows Then
        msKitCompVal = Trim$(gsGridReadCellText(grdCompKits, NewRow, kKitColComponentItem))
    Else
        ' The If condition is added by Ashish on March 05, 1999.
        ' This will cause the navigator to pick up the characters entered in the grid for search
        If Me.ActiveControl Is navGridKit Then
            ' Dont do anything here.
        Else
            If NewCol = kKitColComponentItem Then
            msKitCompVal = ""
            End If
        End If
    End If
    
    If glGridGetActiveCol(grdCompKits) = kKitColQuantity _
And mbNoComp = True Then
        mbNoComp = False
        Cancel = True
    End If
    
    If glGridGetActiveCol(grdCompKits) = kKitColComponentItem Then
       If mbInvalidComponent = True Then
            mbInvalidComponent = False
            Cancel = True
            Exit Sub
        End If
    End If

    If (glGridGetActiveCol(grdCompKits) = kKitColComponentItem _
Or NewCol = kKitColComponentItem) Then
        moGMKitComp.Grid_LeaveCell Col, Row, NewCol, NewRow
    End If
    
    'If grid has focus on quantity and it leaves that cell then set focus on navigator.
    'If not done so the active col is different and this causes a GPF.
    If glGridGetActiveCol(grdCompKits) = kKitColQuantity Then
        grdCompKits.Col = kKitColComponentItem
        
        If grdCompKits.Value = "" Then
            gGridSetActiveCell grdCompKits, grdCompKits.Row, kKitColComponentItem
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMKitComp.Grid_LeaveRow Row, NewRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_LeaveRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ' --------------------------
    ' This event is necessary only in one case.
    ' When the user selects Item Type as a kit item ( Assembled or BTO) and then defines rows and
    ' then changes the Item type to non kit all the rows but the one that has no item defined gets deleted.
    ' And at the time of save there appears a message Kit item can not be blank and focus gets set on kit
    ' tab which is disabled because of the Item Type.  To avoide this what we need to do is to force the
    ' user to feed in the correct information before he/she looses focus from kits tab.  This ensures that
    ' upper mentioned problem does not occur
    ' ---------------------------
    Dim LoopVar As Long

    'The lost focus gets called if the focus is lost from grid to grid navigator.
    'Here we skip the grid row validation.
    If Me.ActiveControl Is navGridKit Then
        Exit Sub
    End If
    
    For LoopVar = 1 To (grdCompKits.MaxRows - 1)
        'Do not reset the mbCompBlankMessageDisplayed here as this is the case for which it
        'is brought in picture.
         
        mbCompBlankMessageDisplayed = True
        bIsValidKitComponent (LoopVar)
    Next LoopVar

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "grdCompKits_LostFocus", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompKits_RightClick(ByVal ClickType As Integer, ByVal Col As Long, ByVal Row As Long, ByVal MouseX As Long, ByVal MouseY As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbOnKitGrid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_RightClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdCompKits_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Grid manager change of top or left row action
    moGMKitComp.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdCompKits_TopLeftChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdItemImages_Change(ByVal Col As Long, ByVal Row As Long)
   ' moDmItemImage.ProcessEventChange Col, Row
   #If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Grid Manager Change action
    Dim iHold As Integer

    If Col = kItemIColImageName Or Col = kItemIColImagePath Then
        DoEvents
    Else
        moGMItemImage.Grid_Change Col, Row
    End If
 
    If Col = kItemIColImageName Then
        moDmItemImage.SetDirty True
    End If

    If Col = kItemIColImagePath Then
        gGridUpdateCell grdItemImages, Row, kItemIColImageName, _
gsGridReadCell(grdItemImages, Row, kItemIColImagePath)
        moDmItemImage.SetDirty True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdItemImages_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdItemImages_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lNbrOfDataRows As Long
    Dim sGetImageName As String
    Dim sWebLocation As String
    Dim bFileExists As Boolean
    
    If mbErrorFlashed Then
        mbErrorFlashed = False
        Exit Sub
    End If
    
    SetItemImagePict True, True
    With grdItemImages
        lNbrOfDataRows = .DataRowCnt

        ' If not at top and more than 1 row , then enable.
        If (Row > 1 And lNbrOfDataRows > 1) Then
            cmdMoveUp.Enabled = True
        Else
            cmdMoveUp.Enabled = False
        End If
    
        If (Row < lNbrOfDataRows And lNbrOfDataRows > 1) Then
            cmdMoveDown.Enabled = True
        Else
            cmdMoveDown.Enabled = False
        End If
        If (.ActiveRow > .MaxRows - 1) Then
            cmdMoveUp.Enabled = False
        End If
    End With
    ' If the user select the header then do not navigate to the image.
    If Row > 0 Then
        sGetImageName = gsGridReadCellText(grdItemImages, Row, kItemIColImagePath)
        If RTrim$(msImageName) = sGetImageName Then Exit Sub
            
        msImageName = sGetImageName
        If grdItemImages.Row <= grdItemImages.MaxRows - 1 Then
            SetHourglass True
            sWebLocation = IIf(InStr(UCase(msImageName), _
kINETProtocol) = 0, msItemImageLocation, "") & msImageName
            If UCase(Left(sWebLocation, 4)) = kINETProtocol Then
                ShowImage webItemImage, sWebLocation
            Else
                'Validate the filename's path.  If the file does not exists, raise an error.
                'Use On Error Resume Next in case the path contains a bad path or file name.
                On Error Resume Next
                
                bFileExists = Len(Trim(Dir(sWebLocation)))
                
                If bFileExists Then
                    ShowImage webItemImage, sWebLocation
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kIMMsgShareNameNotFound, sWebLocation
                    ShowImage webItemImage, kINETAboutBlank
                    mbErrorFlashed = True
                End If
                
                On Error GoTo VBRigErrorRoutine
                    
            End If
            DoEvents
            gGridSetActiveCell grdItemImages, Row, Col
            
        Else
            ShowImage webItemImage, kINETAboutBlank
        End If
    End If
    moGMItemImage.Grid_Click Col, Row
   
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdItemImages_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdItemImages_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Check if the Inventory Images Share Name has been defined in CI Options.  This flag is set when
    'the user click the "Categories" tab.
    If mbNoInvtImgShare = True And Me.ActiveControl.Name = "grdItemImages" Then
        giSotaMsgBox Me, moClass.moSysSession, kIMMsgShareNameNotFound, _
gsBuildString(kCISetUpInvtImagesShareName, moClass.moAppDB, moClass.moSysSession) & " path"
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdItemImages_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdItemImages_KeyDown(KeyCode As Integer, Shift As Integer)
    'moGMItemImage.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lMyCol As Long
    Dim lMyRow As Long

    lMyRow = grdItemImages.ActiveRow
    lMyCol = grdItemImages.ActiveCol
    
    If Len(Trim$(gsGridReadCellText(grdItemImages, lMyRow, lMyCol))) = 0 _
And KeyCode = vbKeySpace Then
        Exit Sub
    End If
   
    'Do not call the KeyDown event if there is no inventory image share path
    'so a new grid line is not created.
    If Not mbNoInvtImgShare Then
        moGMItemImage.Grid_KeyDown KeyCode, Shift
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdItemImages_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdItemImages_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lNbrOfDataRows As Long
    Dim sLocation As String
    Dim lRow As Long
    
    Dim sGetImageName As String
    Dim sWebLocation As String
    Dim bFileExists As Boolean
    
    lRow = Row
    ' Valid the Location Path should not be blank when leaving a row.
    If (NewRow <> Row) Then
        If (Me.ActiveControl.Name = grdItemImages.Name And Row <= grdItemImages.MaxRows - 1) Then
            With grdItemImages
                .Col = kItemIColImagePath
                If Len(Trim(.Text)) = 0 Then
                   .Row = 0
                   giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, gsStripChar(.Text, kAmpersand)
                   mbErrorFlashed = True
                   Cancel = True

                   DoEvents

                   grdItemImages.SetFocus
                   gGridSetActiveCell grdItemImages, lRow, Col
                   Exit Sub
                End If
            End With
        End If
    End If


    
    ' Navigate the Location for Item Image
    If NewRow <= 0 Then
        mbErrorFlashed = True
        moGMItemImage.Grid_LeaveCell Col, Row, NewCol, NewRow
        Exit Sub
    End If
    
    SetItemImagePict True, True
    
    With grdItemImages
        lNbrOfDataRows = .DataRowCnt
        ' If not at top and more than 1 row , then enable.
        If (NewRow > 1 And lNbrOfDataRows > 1) Then
            cmdMoveUp.Enabled = True
        Else
            cmdMoveUp.Enabled = False
        End If
    
        If (NewRow < lNbrOfDataRows And lNbrOfDataRows > 1) Then
            cmdMoveDown.Enabled = True
        Else
            cmdMoveDown.Enabled = False
        End If
        If (NewRow > .MaxRows - 1) Then
            cmdMoveUp.Enabled = False
        End If
    End With

    moGMItemImage.Grid_LeaveCell Col, Row, NewCol, NewRow

    sGetImageName = gsGridReadCellText(grdItemImages, NewRow, kItemIColImagePath)
    If RTrim$(msImageName) = sGetImageName Then Exit Sub

    msImageName = sGetImageName
    If NewRow <= grdItemImages.MaxRows - 1 Then
        SetHourglass True
        sWebLocation = IIf(InStr(UCase(msImageName), _
kINETProtocol) = 0, msItemImageLocation, "") & msImageName
        If UCase(Left(sWebLocation, 4)) = kINETProtocol Then
            ShowImage webItemImage, sWebLocation
        Else
            'Validate the filename's path.  If the file does not exists, raise an error.
            'Use On Error Resume Next in case the path contains a bad path or file name.
            On Error Resume Next
            
            bFileExists = Len(Trim(Dir(sWebLocation)))
            
            If bFileExists Then
                ShowImage webItemImage, sWebLocation
            Else
                giSotaMsgBox Me, moClass.moSysSession, kIMMsgShareNameNotFound, sWebLocation
                ShowImage webItemImage, kINETAboutBlank
                mbErrorFlashed = True
            End If
            
            On Error GoTo VBRigErrorRoutine
            
        End If
        gGridSetActiveCell grdItemImages, NewRow, Col
    Else
        ShowImage webItemImage, kINETAboutBlank
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdItemImages_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_ButtonClicked(ByVal lCol As Long, _
ByVal lRow As Long, _
ByVal iButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iHold As Integer
    
    Select Case lCol
        Case kUOMColUseStdConv
            DoEvents
        
            If iButtonDown = 1 Then
                mbUseStdConvOld = False
            Else
                mbUseStdConvOld = True
            End If

            iHold = miUOMOrKit
            miUOMOrKit = kTabUOM
            moGMUOM_CellChange lRow, lCol
            miUOMOrKit = iHold
    
        Case kUOMColUseForPurchases
            'If unchecking the Purchases option, if this is the purchase default, clear it.
            If Me.ActiveControl Is grdUOM Then
                If iButtonDown = 0 Then
                    If StrComp(Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)), lkuPurchaseUOM.Text, vbTextCompare) = 0 Then
                        lkuPurchaseUOM.Text = ""
                    End If
                End If
            End If
            
        Case kUOMColUseForSales
            'If unchecking the Sales option, if this is the sale default, clear it.
            If Me.ActiveControl Is grdUOM Then
                If iButtonDown = 0 Then
                    If StrComp(Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)), lkuSales.Text, vbTextCompare) = 0 Then
                        lkuSales.Text = ""
                    End If
                End If
            End If
    
        
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_Change(ByVal lCol As Long, ByVal lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iHold As Integer
    Dim iConvType As Integer
    Dim strTargetUOM As String
    
    If Me.ActiveControl Is navGrid Then
        Exit Sub
    End If
    
    mbUOMChangeByEvent = False
    
    If (lCol = kUOMColTargetUOM _
        Or lCol = kUOMColConversionFactor _
        Or lCol = kUOMColConversionType) Then
        DoEvents
        iHold = miUOMOrKit
        miUOMOrKit = kTabUOM
        moGMUOM_CellChange lRow, lCol
        miUOMOrKit = iHold
    Else
        moGMUOM.Grid_Change lCol, lRow
    End If
    
    If lCol = kUOMColVolume Then
        gGridUpdateCell grdUOM, lRow, kUOMColVolume, _
        CDec(gdGetValidDbl(gsGridReadCell(grdUOM, lRow, kUOMColVolume)))
    End If

    If lCol = kUOMColWeight Then
        gGridUpdateCell grdUOM, lRow, kUOMColWeight, _
        CDec(gdGetValidDbl(gsGridReadCell(grdUOM, lRow, kUOMColWeight)))
    End If
    
    If lCol = kUOMColConversionType Then
        iConvType = moConvTypeList.DBValue(gsGridReadCellText(grdUOM, lRow, kUOMColConversionType))
        gGridUpdateCell grdUOM, lRow, kUOMColConversionTypeInt, CStr(iConvType)
        moDmUOMGrid.SetRowDirty lRow  'The row is not being set to dirty on change of Conversion type - force that here
    End If
    
    If lCol = kUOMColTargetUOM Then
        strTargetUOM = Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM))
        
        'Check to see if the UOM has changed
        If Not msUOMVal = strTargetUOM Then
            'Sets the correct default for UseStd and ConversionFactor
            SetUOMDefaultValues lRow

            gGridUpdateCellText grdUOM, lRow, kUOMColVolume, "0"

            gGridUpdateCell grdUOM, lRow, kUOMColVolume, _
            CDec(gdGetValidDbl(gsGridReadCell(grdUOM, lRow, kUOMColVolume)))
            
            gGridUpdateCellText grdUOM, lRow, kUOMColWeight, "0"
            
            gGridUpdateCell grdUOM, lRow, kUOMColWeight, _
            CDec(gdGetValidDbl(gsGridReadCell(grdUOM, lRow, kUOMColWeight)))

            gGridUpdateCellText grdUOM, lRow, kUOMColUPC, ""

            DisableEnable lRow
            
            mbUOMChangeByEvent = False

        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Row > 0 Then
        miUOMOrKit = kTabUOM
        msUOMVal = Trim$(gsGridReadCellText(grdUOM, Row, kUOMColTargetUOM))
        
        grdUOM.Col = Col
        grdUOM.Row = Row
        
        If grdUOM.Lock Then
            msUOMVal = ""
            Exit Sub
        End If
        
        If Col = kUOMColTargetUOM Then
             moGMUOM.Scroll
            If (bIsInventory(ddnItemType.ItemData) = False) Then
                navGrid.Visible = False
                navGrid.Enabled = False
                If Row = grdUOM.MaxRows Then
                    gGridLockRow grdUOM, Row
                End If
            End If
                        
        Else
            moGMUOM.Grid_Click Col, Row
        End If
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMUOM.Grid_ColWidthChange Col1

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_EditMode(ByVal lCol As Long, _
ByVal lRow As Long, _
ByVal iMode As Integer, _
ByVal bChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMUOM.Grid_EditMode lCol, lRow, iMode, bChangeMade
    grdUOM.Row = lRow
   
    'Get the current value for factor and type in case we need to revert
    'This would be due to failed validations or pending transactions
    If lCol = kUOMColConversionType Or lCol = kUOMColConversionFactor Then
        grdUOM.Col = kUOMColRowFromDB

        'Get the current conversion type so we can set it back when changes are not allowed (transactions exist)
        'No need to get the current factor, we will jsut set back using the value in kUOMColConversionFactorDBValue
        If grdUOM.Value = "1" Then
            grdUOM.Col = kUOMColConversionType
            msConversionTypeOld = grdUOM.Text
        End If
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_EditMode", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lMyCol As Long
    Dim lMyRow As Long

    lMyRow = grdUOM.ActiveRow
    lMyCol = grdUOM.ActiveCol

    If Len(Trim$(gsGridReadCellText(grdUOM, lMyRow, lMyCol))) = 0 _
And KeyCode = 32 Then
        Exit Sub
    End If
    
    moGMUOM.Grid_KeyDown KeyCode, Shift

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If (grdUOM.Col = kUOMColConversionFactor _
    Or grdUOM.Col = kUOMColVolume _
    Or grdUOM.Col = kUOMColWeight) _
    And KeyAscii = 45 Then
        KeyAscii = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_LeaveCell(ByVal lCol As Long, ByVal lRow As Long, ByVal lNewCol As Long, ByVal lNewRow As Long, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'
' If an invalid value was put into a cell, we will cancel the leave cell event, so it can be corrected.
'
    If glGridGetActiveCol(grdUOM) = kUOMColTargetUOM Then
        If (mbInvalidUOM Or mbNoUOM) Then
            mbInvalidUOM = False
            mbNoUOM = False
            bCancel = True
            Exit Sub
        End If
    End If
    If (glGridGetActiveCol(grdUOM) = kUOMColTargetUOM _
        Or lNewCol = kUOMColTargetUOM) Then
        moGMUOM.Grid_LeaveCell lCol, lRow, lNewCol, lNewRow
    End If
    
    If (bIsInventory(ddnItemType.ItemData) = False) Then
        If lNewRow = grdUOM.MaxRows Then
            gGridLockRow grdUOM, lNewRow
        End If
    End If
    moGMUOM.Grid_LeaveCell lCol, lRow, lNewCol, lNewRow

    If (bIsInventory(ddnItemType.ItemData) = False) Then
        navGrid.Visible = False
        navGrid.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMUOM.Grid_LeaveRow Row, NewRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_LeaveRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_RightClick(ByVal ClickType As Integer, ByVal Col As Long, ByVal Row As Long, ByVal MouseX As Long, ByVal MouseY As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'The 9th column is set to "1" if the row is loaded from database.
    'You can not delete those rows.
    'Otherwise one can delete the added rows.
    Dim lColValue As Integer
    Dim lRowValue As Integer
    Dim bDeleteFlag As Boolean
    
    lColValue = grdUOM.ActiveCol
    lRowValue = grdUOM.ActiveRow
    grdUOM.Col = kUOMColRowFromDB
    If (grdUOM.Value <> "1" And lRowValue <> 1) Then
        bDeleteFlag = True
    End If
    
    If bDeleteFlag Then
        moGMUOM.MenuDelete = True
    Else
        moGMUOM.MenuDelete = False
    End If

    mbOnUOMGrid = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_RightClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdUOM_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMUOM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdUOM_TopLeftChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuCommCls, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCommCls_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuCommoCode, True
    #End If
'+++ End Customizer Code Push +++
    msCommVal = lkuCommoCode.Text
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCommoCode_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuCommoCode, True
    #End If
'+++ End Customizer Code Push +++
    Dim lngFeedBack  As Long
    lngFeedBack = vbYes
    If Trim$(lkuCommoCode) <> "" And Trim$(lkuCommoCode.Text) <> msCommVal Then
        If lkuCommoCode.Enabled = True Then
            If ddnSalesTaxClass.ListIndex <> -1 Then
            'prompt the user that the value will be lost , do u want to continue
                lngFeedBack = giSotaMsgBox(Nothing, moClass.moSysSession, 160130)
                If lngFeedBack = vbYes Then PopulateVal kProcessCommCode
            Else
             PopulateVal kProcessCommCode
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCommoCode_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuCommoCode, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCommoCode_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuCustPriceGroup, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCustPriceGroup_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuDefltWarehouse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuDefltWarehouse_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuFreightClass, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuFreightClass_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuItemClass, True
    #End If
'+++ End Customizer Code Push +++
    msItemClass = lkuItemClass.Text
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuItemClass_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuItemClass, True
    #End If
'+++ End Customizer Code Push +++
    Dim lngFeedBack  As Long

    'If ItemClass changed and new ItemClass is not blank
    'and ItemClass now entered is valid.
    If (msItemClass <> lkuItemClass.Text _
        And Len(Trim$(lkuItemClass)) > 0 _
        And (lkuItemClass.IsValid)) Then
        'If glaAccount's are not blank
        If (Len(Trim$(glaSales.Text)) > 0 _
            Or Len(Trim$(glaInventory.Text)) > 0 _
            Or Len(Trim$(glaCOS.Text)) > 0 _
            Or Len(Trim$(glaReturns.Text)) > 0) Then
            'If Non-Inventory
            If (bIsInventory(ddnItemType.ItemData) = False _
                And Not ddnItemType.ItemData = kItemTypeCommentOnly) Then
                'Prompt the user that the value will be lost , do you want to continue
                lngFeedBack = giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgItmClsChange)
                
                If lngFeedBack = vbYes Then
                    'Clear the GLAccts so that they default to blanks.
                    glaCOS.Text = ""
                    glaExpense.Text = ""
                    glaReturns.Text = ""
                    glaSales.Text = ""

                    If (Len(Trim$(lkuItemClass)) > 0 _
                        And (lkuItemClass.IsValid)) Then
                        PopulateVal kProcessItemClass
                    End If
                End If
            Else
            'Don't default the GL Accts in case of Inventory Stock Items.
                If bIsInventory(ddnItemType.ItemData) = True Then
                    glaCOS.Text = ""
                    glaExpense.Text = ""
                    glaReturns.Text = ""
                    glaSales.Text = ""
                End If
            End If
        Else
            'Default the GL Accts in case of Non-Inventory Items.
            If bIsInventory(ddnItemType.ItemData) = False Then
                PopulateVal kProcessItemClass
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuItemClass_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuItemClass, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuItemClass_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'This function gets called double in case of display only UI causing the message to be displayed  twice.
    'to stop this use the ReadOnlyUI flag.
    If ReadOnlyUI Then
        Exit Sub
    End If

    bIsValidItemID
    mbEnterID = True
    mbAlreadyValidated = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "lkuMain_BeforeValidate", VBRIG_IS_FORM       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuMain_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuMain, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim iConfirmUnload As Integer
    
    mbEnterID = False
    
    iConfirmUnload = moDmItemForm.ConfirmUnload(True)

    Select Case iConfirmUnload   'Check Value of Confirm Unload
        Case Is = kDmSuccess
           'User Made No Changes,
           'Answered Yes and Save was Successful or
           'Answered No
        
        Case Is = kDmFailure
            'Customer Pressed Cancel
            'Or something Failed in Data Manager
            bCancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        
        Case Is = kDmError
            'An Error Occurred
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        
        Case Else
           'Unknown return Value
           giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
CVar(iConfirmUnload)
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMain_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPOMatchTolerance, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPrice, True
    #End If
'+++ End Customizer Code Push +++
    Dim lCnt As Long
    Dim bDont As Boolean

    'Validate the entered data over here.
    bDont = False
    
    If moDmItemForm.State = sotaTB_EDIT Then
        If Not msPriceUOM = Trim$(lkuPrice.Text) And Not msPriceUOM = "" Then
            If giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgChangedPriceUOM) <> vbYes Then
                bDont = True
                lkuPrice.Text = msPriceUOM
            End If
        End If
    End If
    
    If Not bDont Then
        If Len(Trim$(lkuPrice)) > 0 _
            And (lkuPrice.IsValid) _
            And Not ddnItemType.ItemData = kItemTypeBTOKit Then
            If Not lkuStockUOM.Text = lkuPrice.Text Then
                msUOMValue = lkuPrice.Text
            Else
                msUOMValue = ""
            End If
            
            AddDefUOMRow lkuPrice.Text, UOMLookUpControl.Price
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPrice_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPrice, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPrice_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuProdLine, True
    #End If
'+++ End Customizer Code Push +++
    If Trim$(lkuProdLine) <> "" Then
        'set the default values for all controls in this frame from the database corresponding to the value of SalesProdLineID
        PopulateVal kProcessSalesProdLine
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuProdLine_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuProdLine, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuProdLine_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPuchProdLine, True
    #End If
'+++ End Customizer Code Push +++
    ' Set the tag property of the PPL lookup
    If Len(Trim$(lkuPuchProdLine)) > 0 Then
        PopulateVal kProcessPurchProdLine
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPuchProdLine_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPuchProdLine, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPuchProdLine_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPurchaseUOM, True
    #End If
'+++ End Customizer Code Push +++
    DoEvents
    
    If Len(Trim$(lkuPurchaseUOM)) > 0 _
        And (lkuPurchaseUOM.IsValid) _
        And Not ddnItemType.ItemData = kItemTypeBTOKit Then
        If Not lkuStockUOM.Text = lkuPurchaseUOM.Text Then
            msUOMValue = lkuPurchaseUOM.Text
        Else
            msUOMValue = ""
        End If
        
        DoEvents
        
        If Not lkuPurchaseUOM.Text = "" Then
            AddDefUOMRow lkuPurchaseUOM.Text, UOMLookUpControl.Purch
        End If
    Else
        If (Len(Trim$(lkuPurchaseUOM)) = 0 _
            And moDmItemForm.State = sotaTB_EDIT) Then
            moDmItemForm.SetColumnValue "PurchUnitMeasKey", Null
            lkuPurchaseUOM.KeyValue = Empty
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPurchaseUOM_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPurchaseUOM, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPurchaseUOM_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSales, True
    #End If
'+++ End Customizer Code Push +++
    If Trim$(lkuSales) <> "" And (lkuSales.IsValid) And _
       Not ddnItemType.ItemData = kItemTypeBTOKit Then
       If Not lkuStockUOM.Text = lkuSales.Text Then
            msUOMValue = lkuSales.Text
       Else
            msUOMValue = ""
       End If
       AddDefUOMRow lkuSales.Text, UOMLookUpControl.Sales
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSales_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuSales, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSales_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuStockUOM, True
    #End If
'+++ End Customizer Code Push +++
    If Not lkuStockUOM.Text = "" Then
        msGotValue = lkuStockUOM.Text
    Else
        msGotValue = ""
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuStockUOM_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuStockUOM, True
    #End If
'+++ End Customizer Code Push +++
    Dim lCounter As Long
    Dim sValue As String

    If (Len(Trim$(lkuStockUOM.Text)) > 0 _
        And (lkuStockUOM.IsValid)) Then
        StkUOMProcessing lkuStockUOM.Text

        If Not ddnItemType.ItemData = kItemTypeBTOKit Then
            grdUOM.Enabled = True
            
            If Trim$(lkuStockUOM) <> msGotValue And Len(msGotValue) > 0 Then
                SetConversionZero
            End If

            msGotValue = ""
            msUOMValue = lkuStockUOM.Text
            
            AddDefUOMRow lkuStockUOM.Text, UOMLookUpControl.Stock
            
            msUOMValue = ""
            
            For lCounter = 1 To grdUOM.MaxRows - 1
                 DisableEnable lCounter
            Next
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuStockUOM_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuStockUOM, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuStockUOM_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetCmdMoveItemsOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Called when items are moved, or selected so that the buttons
'           will diplay realistic available options.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************
    'From lstAvailProdCatID buttons
    'Items in source list?
    cmdMoveItems(kSelectAll).Enabled = lstAvailProdCatID.ListCount
    
    'Items selected in source list?
    cmdMoveItems(kSelect).Enabled = lstAvailProdCatID.SelCount

    'From lstAssignProdCatID buttons
    'Items selected in source list?
    cmdMoveItems(kDeselect).Enabled = lstAssignProdCatID.SelCount

    'Items in source list?
    cmdMoveItems(kDeselectAll).Enabled = lstAssignProdCatID.ListCount

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetCmdMoveItemsOptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAssignProdCatID_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Determine if an item was selected to set button options.
'  Returns: N/A
'***********************************************************************
    SetCmdMoveItemsOptions

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAssignProdCatID_DblClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Moves item from lstAssignProdCatID back to lstAvailProdCatID listbox.
'  Returns: N/A
'***********************************************************************
    Dim lOldListIndex As Long

    'Insert into sorted listbox.
    With lstAvailProdCatID
        .AddItem lstAssignProdCatID.List(lstAssignProdCatID.ListIndex)
        .ItemData(.NewIndex) = lstAssignProdCatID.ItemData(lstAssignProdCatID.ListIndex)
    End With

    'Remove item from the source listbox.
    lOldListIndex = lstAssignProdCatID.ListIndex
    lstAssignProdCatID.RemoveItem (lstAssignProdCatID.ListIndex)
    
    SetCmdMoveItemsOptions

    mbAssignedProdCatsSaveIsNeeded = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SelectSomeItems()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Moves assigned items from lstAvailProdCatID to lstAssignProdCatID listbox.
'    Parms: None.
'  Returns: N/A
'***********************************************************************
    Dim lItemCount As Long

    'If no items selected to be moved, then exit.
    If lstAvailProdCatID.SelCount = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    'Go through Source listbox backwards because it is
    'easier to remove (this way indexes don't change on you).
    With lstAssignProdCatID
        For lItemCount = lstAvailProdCatID.ListCount - 1 To 0 Step -1
            If lstAvailProdCatID.Selected(lItemCount) Then
                'Insert into sorted listbox
                .AddItem lstAvailProdCatID.List(lItemCount)
                .ItemData(.NewIndex) = lstAvailProdCatID.ItemData(lItemCount)
                
                'Remove item from the source listbox
                lstAvailProdCatID.RemoveItem (lItemCount)
            End If
        Next lItemCount
    End With
    mbAssignedProdCatsSaveIsNeeded = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SelectSomeItems", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAssignProdCatID_DragDrop(Source As Control, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Fired when a dragged object gets dropped over lstAssignProdCatID.
'           Moves over the selected items from the source listbox to a specific line
'           Or repositions a single item within the listbox.
'    Parms: Source = object being dropped over us.
'           Y = y coordinate to determine the exact 'dropping' / placement
'               of the source items within the list.
'  Returns: N/A
'***********************************************************************
    'Dragged in from other object
    If Source = lstAvailProdCatID Then
        'Move over the selected items from the source listbox.
        SelectSomeItems
        SetCmdMoveItemsOptions
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAssignProdCatID_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Fired when a dragged object gets dragged over lstAssignProdCatID.
'           Sets the drag icons to display:
'               If this listbox is a target for the items being dragged.
'               If this listbox is the source of the items being dragged.
'           Scrolls the listbox when dragging items over its edges.
'    Parms: Source = object being dropped over us.
'           Y = helps determine if the user is attempting to scroll
'               the listbox while dragging an item over it.
'           State = Dragging an object In(vbEnter), Around(vbOver) or Out(vbLeave).
'  Returns: N/A
'***********************************************************************
    Dim lVisibleRows As Long
    Dim lVisibleDropRow As Long

    'In either list box, it is possible in VB to initiate dragging with no items.
    'It is unlikely but by selecting two consecutive items and while holding the
    'ctrl button to deselect and simultaneously move the mouse (left-button down)
    'over the two selected items.
    If TypeOf Source Is ListBox Then
        'Cancel if no items selected.
        If Source.SelCount = 0 Then
            Source.Drag vbCancel
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    
    'Determine how and what we are dragging.
    Select Case State
        Case Is = vbOver
            'Listbox Scrolling
            'Check if we should scroll up.
            If (Y < 0 _
And lstAssignProdCatID.TopIndex > 0) Then
                lstAssignProdCatID.TopIndex = lstAssignProdCatID.TopIndex - 1
            Else
                If miRowHeight = 0 Then
                    lVisibleRows = 0
                    lVisibleDropRow = 0
                Else
                    lVisibleRows = Int(lstAssignProdCatID.Height / miRowHeight)
                    lVisibleDropRow = Int(Y / miRowHeight)
                End If
                
                'Check if we should scroll down.
                If (lVisibleDropRow = lVisibleRows _
And lstAssignProdCatID.ListCount > lstAssignProdCatID.TopIndex + lVisibleRows) Then
                    lstAssignProdCatID.TopIndex = lstAssignProdCatID.TopIndex + 1
                End If
             End If
        
        Case Is = vbEnter
            If Source = lstAvailProdCatID Then
                'If importing, then use imgDrop icon.
                Source.DragIcon = imgDrop
            ElseIf Source = lstAssignProdCatID Then
                'Set Reposition or Drag Icon
                Source.DragIcon = imgDrag
            End If
        
        Case Is = vbLeave
            'If one of the two lists change to No-Drop icon outside this area
            If (Source = lstAvailProdCatID _
Or Source = lstAssignProdCatID) Then
                Source.DragIcon = imgNoDrop
            End If
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_DragOver", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAssignProdCatID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Remove Selected items from opposite listbox.
'           Select(highlight) last item that had focus in this listbox.
'  Returns: N/A
'***********************************************************************
    UnhighlightItems lstAvailProdCatID
    
    'If not empty
    If lstAssignProdCatID.ListCount Then
        'Highlight last item with focus or first item if none.
        lstAssignProdCatID.Selected(lstAssignProdCatID.ListIndex) = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub UnhighlightItems(lstBox As ListBox)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Removes Highlight from a listbox's items.
'    Parms: lstBox - ListBox in which to remove the highlight.
'  Returns: N/A
'***********************************************************************
    Dim lItemCount As Long
    
    If lstBox.SelCount = 0 Then
        'None highlighted, so exit.
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    For lItemCount = 0 To lstBox.ListCount - 1
        lstBox.Selected(lItemCount) = False
    Next lItemCount

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UnHighlightItems", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAssignProdCatID_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Capture and re-route keys if neccessary.
'  Returns: N/A
'***********************************************************************
    Dim lPlace As Long

    'Deselect all highlighted items if the user hits return.
    If KeyCode = vbKeyReturn Then
        'Duplicate code in cmdMoveItems(kSelect) otherwise this becomes a focus nightmare.
        'FutureHighlightPosition = (LastItemToBeMoved + Next) - ItemsToBeMoved
        lPlace = lLastPosOfAssignedItems(lstAssignProdCatID) + 1 - lstAssignProdCatID.SelCount
        DeselectSomeItems
        
        'Highlight on next logical position.
        HighlightAnItem lstAssignProdCatID, lPlace

        
        If lstAssignProdCatID.ListCount = 0 Then
            'No items, go to other listbox.
            lstAvailProdCatID.SetFocus

        End If
        
     
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub HighlightAnItem(lstBox As ListBox, lPlace As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Selects (highlights) a particular item in a listbox.
'    Parms: lstBox - ListBox in which to set the selected item.
'           lPlace - Desired index item to select.
'                    Will be changed to the last available item
'                    if index is missing in list.
'  Returns: N/A
'***********************************************************************
    If lPlace > lstBox.ListCount - 1 Then
        'Can't exceed last item
        lPlace = lstBox.ListCount - 1
    End If
    
    If (lPlace < 0 And lstBox.ListCount) Then
        'If less than 0 but has items, then make 0.
        lPlace = 0
    End If
    
    'Select, if still a valid line number.
    If lPlace >= 0 Then
        lstBox.Selected(lPlace) = True
    Else
        'Lstbox is out of items, so set buttons.
        SetCmdMoveItemsOptions
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HighlightAnItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DeselectSomeItems()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Moves Selected items from lstSelected back to lstAvailProdCatID listbox.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************
    Dim lItemCount As Long

    'If no items selected to be moved, then exit.
    If lstAssignProdCatID.SelCount = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    'Go through lstAvailProdCatID listbox backwards because it is
    'easier to remove (this way indexes don't change on you).
    With lstAvailProdCatID
        For lItemCount = lstAssignProdCatID.ListCount - 1 To 0 Step -1
            If lstAssignProdCatID.Selected(lItemCount) Then
                'Insert into sorted listbox
                .AddItem lstAssignProdCatID.List(lItemCount)
                .ItemData(.NewIndex) = lstAssignProdCatID.ItemData(lItemCount)
               
                'Remove item from the source listbox
                lstAssignProdCatID.RemoveItem (lItemCount)
            End If
        Next lItemCount
    End With
    mbAssignedProdCatsSaveIsNeeded = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeselectSomeItems", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lLastPosOfAssignedItems(lstBox As ListBox) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Finds last position of a selected item within a list box.
'    Parms: lstBox - Source List Box
'  Returns: N/A
'***********************************************************************
    For lLastPosOfAssignedItems = lstBox.ListCount - 1 To 0 Step -1
        If lstBox.Selected(lLastPosOfAssignedItems) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    Next

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lLastPosOfAssignedItems", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub lstAssignProdCatID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Initiate dragging or repositioning if button down and something is selected.
'           Sets the drag icons to display:
'               Repositioning - When list has more than one item, and only one item is selected.
'               Dragging - Enforced if more than one item is selected, or if listbox only has one item.
'    Parms: Button = vbLeftButton or Right
'  Returns: N/A
'***********************************************************************
    'Initiate dragging or repositioning if button down.
    If Button = vbLeftButton Then
        With lstAssignProdCatID
            'If items are selected.
            If .SelCount > 0 Then
                'Drag or Reposition
                'Need more then one in list to reposition
                'Set Icon for dragging
                .DragIcon = imgDrag
                .Drag vbBeginDrag
            End If
        End With
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAssignProdCatID_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lstAvailProdCatID_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Determine if an item was selected to set button options.
'  Returns: N/A
'***********************************************************************
    SetCmdMoveItemsOptions

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAvailProdCatID_DblClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Moves selected item from lstAvailProdCatID to lstAssignProdCatID listbox.
'  Returns: N/A
'***********************************************************************
    Dim lOldListIndex As Long

    'Insert into sorted listbox.
    With lstAssignProdCatID
        .AddItem lstAvailProdCatID.List(lstAvailProdCatID.ListIndex)
        .ItemData(.NewIndex) = lstAvailProdCatID.ItemData(lstAvailProdCatID.ListIndex)
    End With

    'Remove item from the source listbox
    lOldListIndex = lstAvailProdCatID.ListIndex
    lstAvailProdCatID.RemoveItem (lstAvailProdCatID.ListIndex)
    
    SetCmdMoveItemsOptions
    mbAssignedProdCatsSaveIsNeeded = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lstAvailProdCatID_DragDrop(Source As Control, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Fired when a dragged object gets dropped over lstAvailProdCatID.
'           Moves back the selected items from the source listbox.
'    Parms: Source = object being dropped over us.
'           Y = we don't care exactly where it is being dropped
'               since this listbox is sorted.
'  Returns: N/A
'***********************************************************************
    'Dragged in from other object
    If Source = lstAssignProdCatID Then
        'Move back the selected items from the source listbox.
        DeselectSomeItems
        SetCmdMoveItemsOptions
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lstAvailProdCatID_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Fired when a dragged object gets dragged over lstAssignProdCatID.
'           Sets the drag icons to display:
'               If this listbox is a target for the items being dragged.
'               If this listbox is the source of the items being dragged.
'           Scolls the listbox when dragging items over its edges.
'    Parms: Source = object being dropped over us.
'           Y = helps determine if the user is attempting to scroll
'               the listbox while dragging an item over it.
'           State = Dragging an object In(vbEnter), Around(vbOver) or Out(vbLeave).
'  Returns: N/A
'***********************************************************************
    Dim lVisibleRows As Long
    Dim lVisibleDropRow As Long
    
    'In either list box, it is possible in VB to initiate dragging with no items.
    'It is unlikely, but by selecting two consecutive items and while holding the
    'ctrl button to deselect and simultaneously move the mouse (left-button down)
    'over the two selected items.
    If TypeOf Source Is ListBox Then
        'Cancel if no items selected
        If Source.SelCount = 0 Then
            Source.Drag vbCancel
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    
    'Determine how and what we are dragging
    Select Case State
        Case Is = vbOver
            'Listbox Scrolling
            'Check if we should scroll up.
            If (Y < 0 _
And lstAvailProdCatID.TopIndex > 0) Then
                lstAvailProdCatID.TopIndex = lstAvailProdCatID.TopIndex - 1
            Else
                If miRowHeight = 0 Then
                    lVisibleRows = 0
                    lVisibleDropRow = 0
                Else
                    lVisibleRows = Int(lstAvailProdCatID.Height / miRowHeight)
                    lVisibleDropRow = Int(Y / miRowHeight)
                End If
                
                'Check if we should scroll down.
                If (lVisibleDropRow = lVisibleRows _
And lstAvailProdCatID.ListCount > lstAvailProdCatID.TopIndex + lVisibleRows) Then
                    lstAvailProdCatID.TopIndex = lstAvailProdCatID.TopIndex + 1
                End If
             End If
       
        Case Is = vbEnter
            If Source = lstAssignProdCatID Then
                Source.DragIcon = imgDrop            'if importing then use imgDrop icon
            ElseIf Source = lstAvailProdCatID Then
                Source.DragIcon = imgDrag
            End If
            
        Case Is = vbLeave
            'If one of the two lists change to No-Drop icon outside this area
            If (Source = lstAvailProdCatID _
Or Source = lstAssignProdCatID) Then
                Source.DragIcon = imgNoDrop
            End If
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_DragOver", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lstAvailProdCatID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Remove Selected items from opposite lstBox.
'           Select(highlight) last item that had focus in this listbox.
'  Returns: N/A
'***********************************************************************
    UnhighlightItems lstAssignProdCatID
    
    'If not empty
    If lstAvailProdCatID.ListCount Then
        'Highlight last item with focus, or first item if none.
        lstAvailProdCatID.Selected(lstAvailProdCatID.ListIndex) = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lstAvailProdCatID_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Capture and re-route keys if neccessary.
'  Returns: N/A
'***********************************************************************
    Dim lPlace As Long

    'Select all highlighted items if the user hits return.
    If KeyCode = vbKeyReturn Then
        'Duplicate code in cmdMoveItems(kDeselect), otherwise this becomes a focus nightmare.
        'FutureHighlightPosition = (LastItemToBeMoved + Next) - ItemsToBeMoved
        
        lPlace = lLastPosOfAssignedItems(lstAvailProdCatID) + 1 - lstAvailProdCatID.SelCount
        SelectSomeItems

        'Highlight on next logical position.
        HighlightAnItem lstAvailProdCatID, lPlace
       
        If lstAvailProdCatID.ListCount = 0 Then
            'No items, go to other listbox.
            lstAssignProdCatID.SetFocus
        End If
    
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lstAvailProdCatID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Initiate dragging if button down and something is selected.
'           Sets the drag icons to diplay:
'               Dragging - Enforced if more than one item is selected, or if listbox only has one item.
'               Repositioning - NOT ALLOWED IN lstAvailProdCatID SORTED LIST BOX.
'    Parms: Button = vbLeftButton or Right
'  Returns: N/A
'***********************************************************************
    'Initiate dragging if button down
    If Button = vbLeftButton Then
        'If an item(s) is selected
        If lstAvailProdCatID.SelCount > 0 Then
            With lstAvailProdCatID
                .DragIcon = imgDrag
                .Drag vbBeginDrag
            End With
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lstAvailProdCatID_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub moDmItemDescForm_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
     'assign the LanguageID
     moDmItemDescForm.SetColumnValue "LanguageID", moClass.moSysSession.Language
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemDescForm_DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemDescForm_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case iNewState
        Case 0:
            tbrMain.SetState sotaTB_NONE
            moDmItemDescForm.SetUIStatusNone

        Case 1:
            tbrMain.SetState sotaTB_ADD
            moDmItemDescForm.SetUIStatusAdd

        Case 2:
            tbrMain.SetState sotaTB_EDIT
            moDmItemDescForm.SetUIStatusEdit

    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemDescForm_DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMAfterInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim lUOMKey As Long
  
    'Since the UOM grid is not populated on BTO Kit items - we have to manually insert to the table here
    If ddnItemType.ItemData = kItemTypeBTOKit Then
        'Try to get the UOM key from the lookup first - if not - fetch from the DB
        lUOMKey = glGetValidLong(lkuStockUOM.KeyValue)
        If lUOMKey = 0 Then
            lUOMKey = gvCheckNull(moClass.moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure WITH (NOLOCK)", _
                "UnitMeasID = " & gsQuoted(lkuStockUOM.Text) & " AND CompanyId = " & gsQuoted(msCompanyID)), SQL_INTEGER)
        End If
                
        sSQL = "INSERT INTO timItemUnitOfMeas "
        sSQL = sSQL & " (ItemKey, TargetUnitMeasKey, ConversionFactor, ConversionType)"
        sSQL = sSQL & " VALUES(" & mlMyKey & "," & lUOMKey & ", 1, " & moConvTypeList.DefaultDBValue & ")"
        moClass.moAppDB.ExecuteSQL sSQL
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemForm_DMAfterInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMAfterUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    
'    REMOVED: Now the logic to update the kit's std price is in the update trigger of timItem.
'    If (numStdPrice.Value <> moDmItemForm.GetColumnValue("StdPrice") _
'And ddnItemType.ItemData <> kItemTypeBTOKit) Then
'        sSQL = "UPDATE timItem SET StdPrice = I.StdPrice - "
'        sSQL = sSQL & "(" & moDmItemForm.GetColumnValue("StdPrice") & " * KC.CompItemQty) + "
'        sSQL = sSQL & "(" & numStdPrice.Value & " * KC.CompItemQty) "
'        sSQL = sSQL & "FROM timItem I JOIN timKitCompList KC WITH (NOLOCK) ON KC.KitItemKey = I.ItemKey "
'        sSQL = sSQL & "JOIN timKit K WITH (NOLOCK) ON KC.KitItemKey = K.KitItemKey "
'        sSQL = sSQL & "WHERE KC.CompItemKey = " & Format$(mlItemKey) & " AND "
'        sSQL = sSQL & "K.PriceAsCompTotal = 1"
'
'        moClass.moAppDB.ExecuteSQL sSQL
'    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemForm_DMAfterUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub moDmItemForm_DMBeforeDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rst As Object

    sSQL = "SELECT * FROM timInventory WHERE ItemKey = (SELECT ItemKey FROM timItem "
    sSQL = sSQL & " WHERE ItemID= " & gsQuoted(lkuMain.Text)
    sSQL = sSQL & " AND CompanyID= " & gsQuoted(msCompanyID) & " )"

    Set rst = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    With rst
        .MoveLast
        .MoveFirst
        If .RecordCount > 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMInventoryExists, gsQuoted(lkuMain.Text)
            bValid = False
        End If
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemForm_DMBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'This is where we assign the ItemKey to timItem.
    Dim lKey As Long

    bValid = False

    With moClass.moAppDB
        .SetInParamStr "timItem"
        .SetOutParam lKey
        .ExecuteSP "spGetNextSurrogateKey"
         lKey = .GetOutParam(2)
        .ReleaseParams
    End With
     
    mlMyKey = lKey
     
    'assign the ItemKey
    moDmItemForm.SetColumnValue "Itemkey", lKey
    moDmItemDescForm.SetColumnValue "Itemkey", lKey
     
    'assign the LanguageID
    moDmItemDescForm.SetColumnValue "LanguageID", moClass.moSysSession.Language
    
    'assign value to RestockRate
    moDmItemForm.SetColumnValue "RestockRate", numRestockingRatesHide
    
    'assign the CreateType
    moDmItemForm.SetColumnValue "CreateType", kCreateTypeStandard
     
    'assign numTargetMarginHide value from numTargetMargin
    numTargetMarginHide.Value = (numTargetMargin.Value / 100)

    'assign numMinGrossProfitPctHide value from numMinGrossProfitPct
    numMinGrossProfitPctHide.Value = (numMinGrossProfitPct.Value / 100)

    'check for Not Null fields And Numeric validations
    bValid = ChkNotNullFlds
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemForm_DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMBeforeTransaction(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Check if Item is the PA Sales Item, and ensure it has a Sales UOM.
    If ((mlItemKey = mlPASalesItemKey) _
    And (Len(Trim$(lkuSales.Text)) = 0) _
    And mlPASalesItemKey <> 0) Then
        giSotaMsgBox Me, moClass.moSysSession, 160155
        bValid = False
        Exit Sub
    End If

    'Save the current values in module-level variables.
    msUserFld(0) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld1"))
    msUserFld(1) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld2"))
    msUserFld(2) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld3"))
    msUserFld(3) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld4"))
    msUserFld(4) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld5"))
    msUserFld(5) = gsGetValidStr(moDmItemForm.GetColumnValue("UserFld6"))

    If (mbIMIsActivated) Then
        If (moUF.bValidateUserFlds(kEntTypeIMItem, 6, True, msUserFld())) Then
            'Save the values just set in the User-Defined Fields form.
            moDmItemForm.SetColumnValue "UserFld1", msUserFld(0)
            moDmItemForm.SetColumnValue "UserFld2", msUserFld(1)
            moDmItemForm.SetColumnValue "UserFld3", msUserFld(2)
            moDmItemForm.SetColumnValue "UserFld4", msUserFld(3)
            moDmItemForm.SetColumnValue "UserFld5", msUserFld(4)
            moDmItemForm.SetColumnValue "UserFld6", msUserFld(5)
        Else
            bValid = False
            Exit Sub
        End If
    End If

    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "moDmItemForm_DMBeforeTransaction", VBRIG_IS_FORM 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmItemForm_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lCharPosition   As Long
    Dim sString         As String
    Dim sSaveString     As String
    Dim lUOMKey         As Long

    'If the control is Long Desc text field
    'Get the value of the column in the Data Manager object
    sSaveString = moDmItemDescForm.GetColumnValue("LongDesc")
    
    'Find the line feed char
    lCharPosition = InStr(sSaveString, Chr$(10))
    
    'If the point is found
    While lCharPosition <> 0
        'Collect the value before the line feed char and append with a carriage return char and then the line feed
        sString = sString & Mid(sSaveString, 1, lCharPosition - 1) & Chr$(13) & Chr$(10)
        
        'Shorten the string value of the column to that yet to be checked string
        sSaveString = Mid(sSaveString, lCharPosition + 1)
        
        'Again search for the line feed char
        lCharPosition = InStr(sSaveString, Chr$(10))
    Wend
    
    'Replace the text with what is now collected
    txtLongDesc.Text = sString + sSaveString
    
    If ddnProvider.ItemData = 0 Then
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
    End If
    
    lUOMKey = moDmItemForm.GetColumnValue("PriceUnitMeasKey")
        
    msPriceUOM = gvCheckNull(moClass.moAppDB.Lookup _
    ("UnitMeasID", "tciUnitMeasure", "UnitMeasKey = " & lUOMKey), SQL_CHAR)

    numStdCost.Tag = numStdCost.Value        'Credit Limit
    numStdPrice.Tag = numStdPrice.Value      'Credit Limit
    ddnStatus.Tag = ddnStatus.ItemData
    
    If chkAllowDecQuantities.Value = kChecked Then
        With numMandatorySalesMulti
            .Value = 0
            .Enabled = False
        End With
    End If

    SetupAdjQtyRoundMeth
    mbAssignedProdCatsSaveIsNeeded = False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "moDmItemForm_DMDataDisplayed", VBRIG_IS_FORM 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmItemForm_DMPostSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
    If mbAssignedProdCatsSaveIsNeeded Then
        If bSaveAssignedProdCatsForItem(mlItemKey) = False Then
            'An error occurred, but a message was already given to the user
            Exit Sub
        End If

        'The save was successful, so no save is needed anymore.
        mbAssignedProdCatsSaveIsNeeded = False
    End If
    
    If mbClearListBox Then
        lstAvailProdCatID.Clear
        lstAssignProdCatID.Clear
    End If
    
    mbClearListBox = False
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "moDmItemForm_DMReposition", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMPreSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
    Dim sCompItem As String
    Dim lRow As Long

    SetIsSmallestUOM
    
    'Validate Kit Components exist.
    If (ddnItemType.ItemData = kItemTypeAssembledKit) Or (ddnItemType.ItemData = kItemTypeBTOKit) Then
        sCompItem = gsGetValidStr(gsGridReadCell(grdCompKits, 1, kKitColComponentItem))

        If Len(Trim$(sCompItem)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, 160153
            bValid = False
        End If
    End If

    'Validate the Location for Grid Item Images.
    If (mbECIsActivated Or mbSOIsActivated) Then
        If moDmItemImage.IsDirty Then
            For lRow = 1 To grdItemImages.MaxRows - 1
                If Len(RTrim$(gsGridReadCell(grdItemImages, lRow, kItemIColImagePath))) = 0 Then
                    With grdItemImages
                        .Col = kItemIColImagePath

                        If Len(Trim$(.Text)) = 0 Then
                           .Row = 0
                           giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, gsStripChar(.Text, kAmpersand)
                           mbErrorFlashed = True
                           grdItemImages.SetFocus
                           DoEvents
                           gGridSetActiveCell grdItemImages, lRow, .Col
                        End If
                    End With
                    
                    bValid = False
                    cmdGetImage.Enabled = True
                    cmdSearchInternet.Enabled = True
                    Exit Sub
                End If
            Next lRow
        End If
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "moDmItemForm_DMPreSave", "moDmItemForm_DMReposition", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    numStdCost.Tag = numStdCost.Value      'Credit Limit
    numStdPrice.Tag = numStdPrice.Value    'Credit Limit
    ddnStatus.Tag = ddnStatus.ItemData
    mbClearListBox = False
    
    'Clear the excluded values before we reposition so that the item being
    'loaded can use its existing value.  This will be setup again in display data routine
    ddnAdjQtyRoundMeth.StaticListExcludeValues = ""
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "moDmItemForm_DMReposition", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmItemForm_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case iNewState
        Case Is = 0
            tbrMain.SetState sotaTB_NONE
            moDmItemForm.SetUIStatusNone
            sbrMain.Status = SOTA_SB_NONE
        
            tbrMain.Buttons(kTbCopyFrom).Enabled = False
        
        Case Is = 1
            tbrMain.SetState sotaTB_ADD
            moDmItemForm.SetUIStatusAdd
            sbrMain.Status = SOTA_SB_ADD
    
            If miSecurityLevel = sotaTB_DISPLAYONLY Then
                tbrMain.Buttons(kTbCopyFrom).Enabled = False
            Else
                tbrMain.Buttons(kTbCopyFrom).Enabled = True
            End If

        Case Is = 2
            tbrMain.SetState sotaTB_EDIT
            moDmItemForm.SetUIStatusEdit
            sbrMain.Status = SOTA_SB_EDIT
            
            If miSecurityLevel = sotaTB_DISPLAYONLY Then
                tbrMain.Buttons(kTbCopyFrom).Enabled = False
            Else
                tbrMain.Buttons(kTbCopyFrom).Enabled = True
            End If
    
    End Select
    
    numStdCost.Tag = numStdCost.Value      'Credit Limit
    numStdPrice.Tag = numStdPrice.Value    'Credit Limit
    ddnStatus.Tag = ddnStatus.ItemData
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemForm_DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemForm_DMValidate(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Check for Not Null fields And Numeric validations.
    bValid = ChkNotNullFlds()
     
    If bValid = False Then
        Exit Sub
    End If
     
    If bIsInventory(ddnItemType.ItemData) = False Then
        bValid = bIsRightMeasure()

        If bValid = False Then
            Exit Sub
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmItemForm_DMValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmItemImage_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mbErrorFlashed = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmKitCompGrid_DMGridAfterUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub moDmKitCompGrid_DMGridAfterUpdate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lKeyVal As Long
    Dim sVal As String

    grdCompKits.Col = kKitColComponentItem
    grdCompKits.Row = lRow
    
    sVal = IIf(Trim$(grdCompKits.Value) = "", "", Trim$(grdCompKits.Value))
    If Len(sVal) > 0 Then
        moDmKitCompGrid.SetColumnValue lRow, "CompItemKey", _
        glGetValidLong(moClass.moAppDB.Lookup("ItemKey", "timItem", "timItem.ItemID= " & _
        gsQuoted(sVal) & " AND timItem.CompanyID = " & gsQuoted(msCompanyID)))
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmKitCompGrid_DMGridAfterUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmKitCompGrid_DMGridAppend(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If lRow = 1 And grdCompKits.ActiveCol = kKitColQuantity Then
        numShelfLife.Value = ""
        mbShelfValChange = True
    End If
    
    grdCompKits.Col = kKitColQuantity
    grdCompKits.Value = 0

    grdCompKits.Col = kKitColComponentItem
    If Len(Trim$(grdCompKits.Value)) > 0 Then
        PopulateVal kProcessKitComp
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmKitCompGrid_DMGridAppend", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmKitCompGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lPrCnt As Long
    
    grdCompKits.Col = kKitColComponentItem
    PopulateVal kProcessKitComp

    'This code will check for decimal quantity option for
    'component and change the cell type accordingly.
    grdCompKits.Col = kKitColDQAllow
    
    If grdCompKits.Value = 0 Then
        'This means that AllowDecimal = 0.
        gGridSetCellType grdCompKits, lRow, 3, SS_CELL_TYPE_FLOAT, 0
    End If
    
    numStdPrice.Value = moDmItemForm.GetColumnValue("StdPrice")
    
    If (chkPriceAsCompTotal.Value = 1) Then            'AddPrice lRow
        If grdCompKits.MaxRows > 1 Then
            If moDmItemForm.State <> sotaTB_EDIT Then
                numStdPrice.ClearData

                For lPrCnt = 1 To grdCompKits.MaxRows - 1
                    AddPrice lPrCnt
                Next
            End If
        End If

        numStdPrice.Enabled = False
    Else
        numStdPrice.Enabled = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmKitCompGrid_DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmKitCompGrid_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim s1 As String
    Dim l1 As Long
    Dim lCounter As Long
    Dim sSQL As String
    Dim rst As Object
    Dim lPrCnt As Long
    
    bValid = False

    tempStr = " DMGridValidate"
    
    If bIsValidKitComponent(lRow) Then
        If bCheckDupCompInTheKitGrid(lRow) Then
            bValid = True
        Else
            bValid = False
        End If
    End If

    'For supressing the duplicate message in case of lostfocus and
    'GNav_CellChange in cascade.
    mbCompBlankMessageDisplayed = True

    If (chkPriceAsCompTotal.Value = 1) Then
        numStdPrice.ClearData

        For lPrCnt = 1 To grdCompKits.MaxRows - 1
            AddPrice lPrCnt
        Next
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmKitCompGrid_DMGridValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmKitForm_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case iNewState
        Case Is = 0
            tbrMain.SetState sotaTB_NONE
            moDmKitForm.SetUIStatusNone

        Case Is = 1
            tbrMain.SetState sotaTB_ADD
            moDmKitForm.SetUIStatusAdd

        Case Is = 2
            tbrMain.SetState sotaTB_EDIT
            moDmKitForm.SetUIStatusEdit
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmKitForm_DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmUOMGrid_DMGridAppend(lDMRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lGridRow As Long

    With grdUOM
        If mbAddDefRow Then
            lGridRow = lDMRow - 1
            .Row = lGridRow
        Else
            lGridRow = .Row
        End If

        'Default this column to zero.
        .Col = kUOMColRowFromDB
        .Value = 0
        
        'If this variable has a valus - we are adding this from a new UOM in one of the top lookup controls
        'Sotck, Price, Purch or Sales UOM
        If Len(Trim$(msUOMValue)) > 0 Then
            'Set the UOM = to what was entered in the lookup
            .Col = kUOMColTargetUOM
            .Value = msUOMValue
            msUOMValue = ""
        
            mbUOMChangeByEvent = True
            
            'If same type = Use Std is checked and conversion factor is standard.
            'Otherwise unchecked and 1
            If bIsSameMeasureType(lkuStockUOM.Text, .Value) And _
              (ddnTrackMethod.ItemData <> kTrackSerial And ddnTrackMethod.ItemData <> kTrackBoth) Then
                gGridUpdateCell grdUOM, lGridRow, kUOMColConversionFactor, CDec(dGetStandardUOMConv(lGridRow))
                gGridUpdateCell grdUOM, lGridRow, kUOMColUseStdConv, kChecked
                mbUseStdConvOld = False
            Else
                gGridUpdateCell grdUOM, lGridRow, kUOMColConversionFactor, CDec(1)
                gGridUpdateCell grdUOM, lGridRow, kUOMColUseStdConv, kUnchecked
                mbUseStdConvOld = True
            End If
        
            mbUOMChangeByEvent = False
        Else
            'Sets the correct default for UseStd and ConversionFactor
            SetUOMDefaultValues lGridRow
        End If
        
        .Col = kUOMColConversionTypeInt
        .Value = moConvTypeList.DefaultDBValue
        
        .Col = kUOMColConversionType
        .Value = moConvTypeList.LocalText(moConvTypeList.DefaultDBValue)
        
        .Col = kUOMColVolume
        .Value = 0
        
        .Col = kUOMColWeight
        .Value = 0
        
        .Col = kUOMColUOMKey
        .Value = 0
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmUOMGrid_DMGridAppend", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmUOMGrid_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    bValid = False
    
    If Not bIsValidUOMID(lRow) Or Not bIsValidConvFactor(lRow) Then
        Exit Sub
    End If
    
    SetConvFactorDBValue (lRow)
    
    bValid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "moDmUOMGrid_DMGridBeforeInsert", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmUOMGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iUseStd As Integer
    Dim lUOMKey As Long
    Dim sValue As String

    'The 9th column is set to "1" if the row is loaded from database.
    'You can not delete those rows.
    'Otherwise one can delete the added rows.
    moDmUOMGrid.SetCellValue lRow, kUOMColRowFromDB, SQL_INTEGER, 1
    
    'All Target UOM's loaded from DB cannot be changed.
    gGridLockCell grdUOM, kUOMColTargetUOM, lRow
    
    sValue = gsGridReadCell(grdUOM, lRow, kUOMColVolume)
    If Len(Trim$(sValue)) > 0 Then
        gGridUpdateCell grdUOM, lRow, kUOMColVolume, gsGetValidStr(CDec(gdGetValidDbl(sValue)))
    End If
    
    sValue = gsGridReadCell(grdUOM, lRow, kUOMColWeight)
    If Len(Trim$(sValue)) > 0 Then
        gGridUpdateCell grdUOM, lRow, kUOMColWeight, gsGetValidStr(CDec(gdGetValidDbl(sValue)))
    End If
    
    lUOMKey = moDmUOMGrid.GetColumnValue(lRow, "TargetUnitMeasKey")
    gGridUpdateCellText grdUOM, lRow, kUOMColUOMKey, gsGetValidStr(lUOMKey)
    
    'If this is the stock UOM - set the Conversion Type = "Stock UOM"
    'Otherwise select the appropriate value in the drop down
    If Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)) <> lkuStockUOM.Text Then
        gGridUpdateCellText grdUOM, lRow, kUOMColConversionType, moConvTypeList.LocalText(giGetValidInt(gsGridReadCell(grdUOM, lRow, kUOMColConversionTypeInt)))
    Else
        gGridSetCellType grdUOM, lRow, kUOMColConversionType, SS_CELL_TYPE_STATIC_TEXT, 25
        gGridUpdateCellText grdUOM, lRow, kUOMColConversionType, gsBuildString(kIMStkUOM, moClass.moAppDB, moClass.moSysSession)
    End If
    
    'If UseStd = checked, set the ConvFactor to the standard - otherwise show the stored value
    iUseStd = giGetValidInt(gsGridReadCell(grdUOM, lRow, kUOMColUseStdConv))
    If iUseStd = kChecked Then
        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, dGetStandardUOMConv(lRow)
    Else
        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, CDec(gsGridReadCell(grdUOM, lRow, kUOMColConversionFactorDBValue))
    End If
    
    DisableEnable lRow

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmUOMGrid_DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'********************************************************************
'   Description:
'       DMGridValidate is called from the GridManager before it attempts
'       to save the record.  Here, entries will be checked for validity.
'       If DMGridValidate returns true, the Data Manager will save the
'       record.  Otherwise, it will stop processing, and the form
'       will remain in the same state as if the use had not pressed
'       the finish button.
'
'   Parameters:
'       lRow    - row number in the grid whose data is to be validated
'   Return Values:
'       bValid :
'           True    - the entry is valid
'           False   - the Entry is invalid because anyone of the components
'                     tested for validity is invalid.
'********************************************************************
Private Sub moDmUOMGrid_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bValid = UOMValidate(lRow)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmUOMGrid_DMGridValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub NavGrid_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iHold As Integer
    Dim EntryRow As Long
    Dim EntryCol As Long
    Dim sTempSearchStr As String
    
    If bDenyControlFocus Then
        Exit Sub
    End If
    
    msUOMValue = ""
    iHold = miUOMOrKit
    miUOMOrKit = kTabUOM
    EntryRow = grdUOM.ActiveRow
    
    'This part will copy the text from the grid to the text box
    'It also stores the search filter into a string for comparism to return from navigator.
    grdUOM.Col = kUOMColTargetUOM
    grdUOM.Row = grdUOM.ActiveRow
    txtNavReturn = Trim$(grdUOM.Value)
    sTempSearchStr = Trim$(txtNavReturn)

    gcLookupClick Me, navGrid, txtNavReturn, "UnitMeasID"
    
    'The following If statement will check if the user has not selected any of the comp.
    'In that case the text box value will be same as previous. then delete the row that gets added
    If (Trim$(txtNavReturn.Text) = sTempSearchStr _
And Len(sTempSearchStr) > 0) Then
         grdUOM.Col = kUOMColTargetUOM
         grdUOM.Value = ""
         txtNavReturn = ""
         
         'If we delete the only row present in the grid then it will cause a GPF down the line
         If (grdUOM.MaxRows <> 1) Then
            gGridDeleteRow grdUOM, grdUOM.Row
        End If
    End If
    
    grdUOM.Col = kUOMColTargetUOM
    gGridSetActiveCell grdUOM, EntryRow, kUOMColTargetUOM
    
    moGMUOM.LookupClicked
    
    miUOMOrKit = iHold

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "NavGrid_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navGridKit_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iHold As Integer
    Dim sTempSearchStr As String

    If bDenyControlFocus Then
        Exit Sub
    End If

    iHold = miUOMOrKit
    miUOMOrKit = kTabKit
    
    'This part will copy the text from the grid to the text box.
    'It also stores the search filter into a string for comparison to return from navigator.
    grdCompKits.Col = kKitColComponentItem
    txtNavReturnKit = Trim(grdCompKits.Value)
    sTempSearchStr = Trim(txtNavReturnKit.Text)

    gcLookupClick Me, navGridKit, txtNavReturnKit, "ItemID"

    If navGridKit.ReturnColumnValues.Count > 0 Then
        txtNavReturnKit.Text = Trim$(navGridKit.ReturnColumnValues("ItemID"))
    End If
    
    'The following If statement will check if the user has not selected any of the comp.
    'In that case the text box value will be same as previous. then delete the row that gets added.
    If (Trim(txtNavReturnKit.Text) = sTempSearchStr _
And Len(Trim$(sTempSearchStr)) > 0) Then
         grdCompKits.Col = kKitColComponentItem
         grdCompKits.Value = txtNavReturnKit.Text
         txtNavReturnKit = txtNavReturnKit.Text

         'We need to delete the extra row.
         DeleteExtraRow
         Exit Sub
    End If
    
    'Don't call the navClicked if the user did not select any values from the search list.
    If Len(Trim$(txtNavReturnKit.Text)) = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    moGMKitComp.LookupClicked
    miUOMOrKit = iHold
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navGridKit_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub




Private Sub numMandatorySalesMulti_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMandatorySalesMulti, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then
        KeyAscii = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numMandatorySalesMulti_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinGrossProfitPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then
        KeyAscii = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numMinGrossProfitPct_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub numNOfDays_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numNOfDays, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numNOfDays_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPeriodUsage_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numPeriodUsage, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numPeriodUsage_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPriceSeq_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numPriceSeq, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numPriceSeq_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub numRestockingRates_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numRestockingRates, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numRestockingRates_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShelfLife_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numShelfLife, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numShelfLife_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub numStdCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numStdCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numStdCost_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numStdCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numStdCost, True
    #End If
'+++ End Customizer Code Push +++
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
        
    If numStdCost.Tag <> numStdCost.Value Then
        'Show security event form.
        'Check the User ID typed in can override (no cancel hit).
        sID = CStr(ksecChangeCost)
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True
        If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
            tabMaintainItem.Tab = kTabDesc
          
            'No Override reset Value
            numStdCost.Value = numStdCost.Tag
            gbSetFocus Me, numStdCost
            Exit Sub
        End If   'Security Event Overridden
        
        numStdCost.Tag = numStdCost.Value
    End If       'Click Event Called from Code

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numStdCost_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub numStdPrice_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numStdPrice, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then
        KeyAscii = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numStdPrice_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numStdPrice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numStdPrice, True
    #End If
'+++ End Customizer Code Push +++
    'This part is moved out from Validation Manager to lost focus and in bPreSave().
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
        
    If numStdPrice.Tag <> numStdPrice.Value Then
        'Show security event form.
        'Check the User ID typed in can override (no cancel hit).
        sID = CStr(ksecChangePrice)
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True

        If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
            tabMaintainItem.Tab = kTabDesc
          
            'No Override Reset Value
            numStdPrice.Value = numStdPrice.Tag
            gbSetFocus Me, numStdPrice
            Exit Sub
        End If   'Security Event Overridden
        
        numStdPrice.Tag = numStdPrice.Value
    End If       'Click Event Called from Code

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numStdPrice_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub numTargetMargin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTargetMargin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii = 45 Then
        KeyAscii = 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numTargetMargin_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRet           As Long
    Dim sNewKey        As String
    Dim vParseRet      As Variant
    Dim iConfirmUnload As Integer
    Dim bMovetoFirst   As Boolean
    

    If sButton = kTbFilter Then
        If sbrMain.Filtered Then
            miFilter = RSID_FILTERED
        Else
            miFilter = RSID_UNFILTERED
        End If
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    mbDmLoad = True

    If ((mbECIsActivated Or mbSOIsActivated) _
And mbAssignedProdCatsSaveIsNeeded) Then
        moDmItemForm.SetDirty True
    End If

    If moDmItemForm.IsDirty = True Then
        If bPreSave() = False Then
            Exit Sub
        End If

        iConfirmUnload = moDmItemForm.ConfirmUnload()
        If iConfirmUnload = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If

    bMovetoFirst = False
    
    If sbrMain.Filtered And Len(Trim(lkuMain.Text)) = 0 Then
        If sButton = kTbPrevious Or sButton = kTbNext Then
            lRet = glLookupBrowse(lkuMain, kTbNext, miFilter, sNewKey)

            Select Case lRet
                Case MS_SUCCESS

                Case MS_BORS
                    
                Case MS_EMPTY_RS
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavEmptyRecordSet

                Case MS_EORS
                    bMovetoFirst = True

                Case MS_ERROR
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavBrowseError

                Case MS_NO_CURRENT_KEY
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavNoCurrentKey

                Case MS_UNDEF_RS
                    giSotaMsgBox Me, moClass.moSysSession, kmsgNavUndefinedError

                Case Else
                    gLookupBrowseError lRet, Me, moClass
    
            End Select
        End If
    End If
 
    mbDmLoad = False
    mbBrowseRec = True
    
    If sbrMain.Filtered Then
        miFilter = RSID_FILTERED
    Else
        miFilter = RSID_UNFILTERED
    End If
    
    Select Case sButton
        Case Is = kTbFirst
            lRet = lkuMain.MoveFirst(miFilter, sNewKey)
        
        Case Is = kTbPrevious
            If bMovetoFirst Then
                lRet = lkuMain.MoveFirst(miFilter, sNewKey)
            Else
                lRet = lkuMain.MovePrevious(miFilter, sNewKey)
            End If
        
        Case Is = kTbNext
            If bMovetoFirst Then
                lRet = lkuMain.MoveFirst(miFilter, sNewKey)
            Else
                lRet = lkuMain.MoveNext(miFilter, sNewKey)
            End If

        Case Is = kTbLast
            lRet = lkuMain.MoveLast(miFilter, sNewKey)
    End Select
            
   Select Case lRet
        Case Is = MS_SUCCESS
            vParseRet = gvParseLookupReturn(sNewKey)
            If IsNull(vParseRet) Then
                Exit Sub
            End If
                
            lkuMain.Text = vParseRet(1)

            If (mbECIsActivated Or mbSOIsActivated) Then
                SetItemImagePict False
                mbLoadAvailProd = False
                mbLoadAssignProd = False
            End If

            bIsValidItemID
            numTargetMargin = numTargetMarginHide * 100
            numMinGrossProfitPct = numMinGrossProfitPctHide * 100
            numRestockingRates.Value = numRestockingRatesHide.Value
            moDmItemForm.SetDirty False
  
        Case Else
            gLookupBrowseError lRet, Me, moClass
    End Select

    mbAssignedProdCatsSaveIsNeeded = False
    mbBrowseRec = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Resume Next
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub SOTAVM_KeyChange()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Call bIsValidItemID

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SOTAVM_KeyChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SOTAVM_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant

    iReturn = SOTA_VALID
    
    Select Case oControl.Name
         Case lkuMain.Name
             tempStr = "SotaVM_Validate"

             If Not bIsValidItemID Then
                 SOTAVM.SetControlFocus oControl
                 iReturn = SOTA_INVALID
             End If
    End Select
    
    If oControl Is ddnStatus Then
        If ddnStatus.Tag <> ddnStatus.ItemData Then
            'Show security event form
            'Check the user id typed in can override (no cancel hit)
            If miModule = kModuleCI Then
                sID = CStr("CICHGSTAT")
            Else
                sID = CStr(ksecChangeItStat)
            End If
            
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            
            If moClass.moFramework.GetSecurityEventPerm(sID, _
sUser, vPrompt) = 0 Then
                tabMaintainItem.Tab = kTabDesc
              
                'No Override reset Value
                ddnStatus.ItemData = ddnStatus.Tag
                gbSetFocus Me, ddnStatus
            End If   'Security Event Overridden
        End If       'Click Event Called from Code
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "SOTAVM_Validate", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tabMaintainItem_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim intI As Integer
    Dim lRow As Long

    '***********************************************************************************************
    'SGS DEJ    (START)
    '***********************************************************************************************
    Dim lcItemKey As Long
    
    If tabMaintainItem.Tab = kTabCOATests And moDmItemForm.State <> sotaTB_NONE Then
        If PreviousTab = kTabCOATests Then
            'Do Nothing... means has already been saved
        Else
            'Get Item Key
            lcItemKey = glGetValidLong(moDmItemForm.GetColumnValue("ItemKey"))
            
'            moDmItemForm.State
            If moDmItemForm.State = sotaTB_ADD Or lcItemKey <= 0 Then
                'This is a new record must save first
                If MsgBox("You must save the record first.  Do you want to save?", vbYesNo, "MAS 500") = vbYes Then
                    'Save the record
                    HandleToolBarClick kTbSave
                    
                    lcItemKey = glGetValidLong(moDmItemForm.GetColumnValue("ItemKey"))
                    
                    If moDmItemForm.State = sotaTB_ADD Or lcItemKey <= 0 Then
                        'Return to previouse tab
                        tabMaintainItem.Tab = PreviousTab
                        Exit Sub
                    Else
                        Call GetMissingLabTests
                    End If
                Else
                    'Return to previouse tab
                    tabMaintainItem.Tab = PreviousTab
                    Exit Sub
                End If
            End If
        End If
    End If
    '***********************************************************************************************
    'SGS DEJ    (STOP)
    '***********************************************************************************************

    'as solution to setting of focus with a number of tabs
    For intI = 0 To tabMaintainItem.Tabs - 1
        SSPanel1(intI).Enabled = False
    Next

    SSPanel1(tabMaintainItem.Tab).Enabled = True

    If tabMaintainItem.Tab = kTabUOM And PreviousTab <> kTabUOM Then
        mbStillOnUOM = True
    
        'ADD MODE
        If moDmItemForm.State = sotaTB_ADD Then
            'if Stock UOM is yet to be selected
            If lkuStockUOM.Text = "" Then
                'keep grid disabled
                grdUOM.Enabled = False
                lkuStockUOM.Enabled = bCheckEnableStockUOM
                lkuPrice.Enabled = False
                lkuPurchaseUOM.Enabled = False
                lkuSales.Enabled = False
            Else
                grdUOM.Enabled = True
                EnableUOMTabControls ddnItemType.ItemData
                lkuStockUOM.Enabled = False
                If (bIsInventory(ddnItemType.ItemData) = False And _
                    (lkuStockUOM.Text = "") And _
                    (ddnItemType.ItemData = kItemTypeMiscItem Or ddnItemType.ItemData = kItemTypeService Or ddnItemType.ItemData = kItemTypeExpense)) Then
                    lkuStockUOM.Enabled = True
                End If
            End If
        'Controls regarding UOM at all times
        Else
             If moDmItemForm.State = sotaTB_EDIT Then
                
                EnableUOMTabControls ddnItemType.ItemData
                lkuStockUOM.Enabled = False
                
                'If upgrading from an older version of Sage MAS 500 Enterprise (2.5/3.0),
                'it is possible that the StockUOM was NULL for Non-Invt
                'items. Since 4.x requires a StockUOM, this change allows
                'the user to enter a StockUOM in Item Maintenance for
                'existing items, only when the StockUOM is NULL and
                'the item is a non-inventory type 1,2 or 3
                If (bIsInventory(ddnItemType.ItemData) = False And _
                    (lkuStockUOM.Text = "") And _
                    (ddnItemType.ItemData = kItemTypeMiscItem Or ddnItemType.ItemData = kItemTypeService Or ddnItemType.ItemData = kItemTypeExpense)) Then
                    lkuStockUOM.Enabled = True
                    moDmItemForm.SetDirty True, True
                End If
                
                grdUOM.Enabled = True
             End If
        End If

        If ddnItemType.ItemData = kItemTypeBTOKit Then
            moDmUOMGrid.Clear True
            moDmUOMGrid.DeleteBlock 1, grdUOM.MaxRows
            grdUOM.Enabled = False
            lkuSales.Enabled = False
            lkuPrice.Enabled = False
        End If
        
        Exit Sub
    End If

    If PreviousTab = kTabUOM And tabMaintainItem.Tab <> kTabUOM Then mbStillOnUOM = False
    If PreviousTab = kTabKit And tabMaintainItem.Tab <> kTabKit Then mbStillOnKit = False

    If tabMaintainItem.Tab = kTabKit And PreviousTab <> kTabKit Then
        mbStillOnKit = True
        If Not lkuMain.Text = "" Then
            grdCompKits.Enabled = True
        Else
            grdCompKits.Enabled = False
            navGridKit.Visible = False
            grdCompKits.Enabled = False
        End If

        'For BTO Kit items, enable the controls if the BTO kit allows returns.
        If ddnItemType.ItemData = kItemTypeBTOKit Then
            chkReturnsAllowedMirror.Value = chkReturnsAllowed.Value
            If chkReturnsAllowedMirror.Value = kChecked Then
                chkAllowRtrnsCustomBTO.Enabled = True
                chkAllowRtrnsPartialBTO.Enabled = True
            Else
                'Disable controls and uncheck.
                chkAllowRtrnsCustomBTO.Value = kUnchecked
                chkAllowRtrnsPartialBTO.Value = kUnchecked
                chkAllowRtrnsCustomBTO.Enabled = False
                chkAllowRtrnsPartialBTO.Enabled = False
            End If
        Else
            'Not a BTO Kit.  Uncheck and disable.
            chkReturnsAllowedMirror.Value = kUnchecked
            chkReturnsAllowedMirror.Enabled = False
        End If
        
    End If
    
    If tabMaintainItem.Tab = kTabCategories Then
        'Check if the Inventory Images Share Name has been defined in CI Options.  If not, raise a flag.
        msItemImageLocation = gsGetValidStr(moClass.moAppDB.Lookup("InvtImagesShareName", "tciOptions", "tciOptions.CompanyID = " & gsQuoted(msCompanyID)))
        
        If Len(Trim(msItemImageLocation)) <> 0 Then
            If Not (Right(msItemImageLocation, 1) = "\") Then
                msItemImageLocation = msItemImageLocation & "\"
            End If
        End If
        
        If Len(Trim(msItemImageLocation)) = 0 Then
            mbNoInvtImgShare = True
            'Delete the extra row.
            If grdItemImages.MaxRows <> grdItemImages.DataRowCnt Then
                gGridDeleteRow grdItemImages, grdItemImages.MaxRows
            End If
            
            'Lock down the Image Location column in the grid.
            For lRow = 1 To grdItemImages.DataRowCnt
                gGridSetCellType grdItemImages, lRow, kItemIColImagePath, SS_CELL_TYPE_STATIC_TEXT
            Next
            
        Else
            mbNoInvtImgShare = False
            'Add an extra row for data entry if needed.
            If grdItemImages.MaxRows = grdItemImages.DataRowCnt Then
                gGridInsertRow grdItemImages, grdItemImages.MaxRows + 1
            End If
            
            'Unlock the Image Location column in the grid.
            For lRow = 1 To grdItemImages.DataRowCnt
                gGridSetCellType grdItemImages, lRow, kItemIColImagePath, SS_CELL_TYPE_EDIT
            Next
            
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabMaintainItem_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick Button
    mbIsCancelButton = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub lkuMain_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuMain, True
    #End If
'+++ End Customizer Code Push +++
    'This function gets called doubly in case of display only UI causing the message to be displayed twice.
    'to stop this use the ReadOnlyUI flag
    If ReadOnlyUI Then
        Exit Sub
    End If
    
    lkuMain.Text = Trim$(lkuMain.Text)
    
    If moDmItemForm.State <> kDmStateNone Then
        ' Do not allow user to have focus on a tab that may be disabled
        tabMaintainItem.Tab = kTabDesc
        Exit Sub
    End If
    
    If (Len(Trim$(lkuMain.Text)) = 0 _
And mbEnterID) Then
        mbEnterID = True
        lkuMain.Text = ""
        bSetFocus lkuMain
    Else
        If (mbECIsActivated Or mbSOIsActivated) Then
            mbLoadAvailProd = False
            mbLoadAssignProd = False
        End If

        SetHourglass True
        
        mbEnterID = True
        If Not mbAlreadyValidated Then
            tempStr = "lkuMain_LostFocus"
            bIsValidItemID
        Else
            mbAlreadyValidated = False
        End If
        
        SetHourglass False

        'Do not allow user to have focus on a tab that may be disabled.
        tabMaintainItem.Tab = kTabDesc
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMain_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bLoadProductCategory(ByVal lItemKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bLoadProductCategory = False
    
    If (mlItemKey > 0) _
And (Not mbLoadAssignProd) Then
        If bLoadProdCatsAssignToItem(lItemKey) = False Then
            Exit Function
        Else
            mbLoadAssignProd = True
        End If
    End If
    
    If Not mbLoadAvailProd Then
        If bLoadProdCatAvailtoAssign() = False Then
            Exit Function
        Else
            mbLoadAvailProd = True
        End If
    End If
    
    bLoadProductCategory = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadProductCategory", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bLoadProdCatsAssignToItem(ByVal lItemKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String

    bLoadProdCatsAssignToItem = False
    
    'Create the basic SQL statement to be used for the Assigned listbox.
    If lItemKey > 0 Then
        sSQL = "SELECT timProdCatItem.ItemKey, timProdCategory.ProdCategoryKey, timProdCategory.ProdCategoryID "
        sSQL = sSQL & "FROM timProdCatItem WITH (NOLOCK), timProdCategory WITH (NOLOCK) "
        sSQL = sSQL & "WHERE timProdCatItem.ProdCategoryKey = timProdCategory.ProdCategoryKey "
        sSQL = sSQL & "AND timProdCatItem.ItemKey = " & Format$(lItemKey)
        
        'Fill the Available list box with selected items
        If bFillComboBox(lstAssignProdCatID, moClass.moAppDB, sSQL) = False Then
            'An error message was already given to the user
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    bLoadProdCatsAssignToItem = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadProdCatsAssignToItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bLoadProdCatAvailtoAssign() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim sStrWhere As String
    Dim lIndex As Long

    bLoadProdCatAvailtoAssign = False
    
    'Create the basic SQL statement to be used for the Assigned listbox.
    sSQL = "SELECT DISTINCT ProdCategoryID, ProdCategoryKey "
    sSQL = sSQL & "FROM timProdCategory WITH (NOLOCK) "
    sSQL = sSQL & "WHERE CompanyID = " & gsQuoted(msCompanyID) & Space$(1)
    sSQL = sSQL & "AND ProdCategoryKey NOT IN "
    sSQL = sSQL & "(SELECT ParentProdCatKey FROM timProdCategory WITH (NOLOCK) "
    sSQL = sSQL & "WHERE ParentProdCatKey IS NOT NULL "
    sSQL = sSQL & "AND CompanyID = " & gsQuoted(msCompanyID) & ") "

    sStrWhere = Empty

    For lIndex = 0 To lstAssignProdCatID.ListCount - 1
        sStrWhere = sStrWhere & gsQuoted(lstAssignProdCatID.List(lIndex))
        sStrWhere = sStrWhere & IIf(lIndex < lstAssignProdCatID.ListCount - 1, ",", "")
    Next lIndex
    
    If lstAssignProdCatID.ListCount - 1 >= 0 Then
        sStrWhere = "AND ProdCategoryID NOT IN (" & sStrWhere & ") "

        If Len(LTrim$(sStrWhere)) > 0 Then
            sSQL = sSQL & sStrWhere
        End If
    End If

    'Add the Order By Clause now
    sSQL = sSQL & "ORDER BY ProdCategoryID"

    'Fill the Available list box with selected items.
    If bFillComboBox(lstAvailProdCatID, moClass.moAppDB, sSQL) = False Then
        'An error message was already given to the user.
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    bLoadProdCatAvailtoAssign = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadProdCatAvailtoAssign", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bFillComboBox(cboctlID As Control, _
oDB As Object, _
sSQLSource As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************************
'   Description: Fills the contents of a dynamic list combo box with the
'                List and Item Data column values from a table.
'
'   Parameters:
'       cboctlID <in,out> - control whose list data and item data (optional) is
'                           populated with column information from the
'                           resulting recordset.
'
'       cboctlDesc <in,out> - control whose list data is populated with column
'                             information from the resulting recordset.
'
'       oDB <in> - Database object.  This can be either the application
'                  or system database.
'
'       sSQLSource <in> - This is the SQL Source.  It may be a table, SQLString
'                         with parameters attached, a select statement, or any
'                         string which is valid for the DAS.
'****************************************************************************
    Dim rsItem As Object

    bFillComboBox = False

    'We need this for list-k prob no but just leave
    SendMessage cboctlID.hwnd, CB_SHOWDROPDOWN, 0, ByVal 0&
    cboctlID.Clear

    Set rsItem = oDB.OpenRecordset(sSQLSource, kSnapshot, kOptionNone)
    With rsItem
        While Not .IsEOF
            gComboAddItem cboctlID, gsGetValidStr(.Field("ProdCategoryID")), glGetValidLong(.Field("ProdCategoryKey"))
            .MoveNext
        Wend
        .Close
    End With

    If Not rsItem Is Nothing Then
        Set rsItem = Nothing
    End If

    bFillComboBox = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bFillComboBox", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub ValuesHere()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    SetDefValues

    navGridKit.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & _
" AND (Status = " & Format$(kItemStatActive) & " OR Status = " & Format$(kItemStatDiscontinued) & ") AND NOT (ItemType = " & Format$(kItemTypeBTOKit) & " OR ItemType = " & Format$(kItemTypeCatalog) & " OR ItemType = " & Format$(kItemTypeExpense) & " OR ItemType = " & Format$(kItemTypeCommentOnly) & " OR ItemType = " & Format$(kItemTypeService) & ") "
    navGridKit.RestrictClause = navGridKit.RestrictClause & " AND ItemID <> " & gsQuoted(lkuMain.Text)

    numTargetMargin.Value = numTargetMarginHide * 100
    numMinGrossProfitPct.Value = numMinGrossProfitPctHide * 100
    numRestockingRates.Value = numRestockingRatesHide

    If Not mbIsErrorMessage Then
        gbSetFocus Me, txtShortDesc
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "ValuesHere", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, X, Y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, X, Y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, X, Y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Init the Sage MAS 500 Validation Manager.
    BindVM

    mbClearListBox = False

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")

        If Not moFormCust Is Nothing Then
            'Before we initialize, set the custom tab to invisible.
            'The initialize will make visible if needed.
            'Pass into the initialize the name of the tab control and the index of the custom tab
            tabMaintainItem.TabVisible(tabMaintainItem.Tabs - 1) = False
            tabMaintainItem.TabsPerRow = tabMaintainItem.Tabs - 1
            moFormCust.Initialize Me, goClass, tabMaintainItem.Name & ";" & tabMaintainItem.Tabs - 1

            Set moFormCust.CustToolbarMgr = tbrMain

            With moFormCust
                .ApplyDataBindings moDmItemForm
                .ApplyFormCust
            End With

            'The CtrlsReset enables all the controls and this causes the controls to
            'become enabled and thus causes a problem.  Hence, call this function only when
            'form state is NOT 'New' or 'Edit'.
            If moDmItemForm.State <> sotaTB_ADD And moDmItemForm.State <> sotaTB_EDIT Then
                CtrlsReset
            End If

            lkuMain.SetFocus

            If mbFromInvt Then
                lkuMain_LostFocus
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If


Private Sub cmdCustField_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCustField, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCustField_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCustField_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCustField, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCustField_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSubstitution_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSubstitution, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSubstitution_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSubstitution_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSubstitution, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSubstitution_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLandCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdLandCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLandCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLandCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdLandCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLandCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveItems_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdMoveItems(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMoveItems_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveItems_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdMoveItems(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMoveItems_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveUp_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdMoveUp, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMoveUp_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveUp_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdMoveUp, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMoveUp_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveDown_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdMoveDown, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMoveDown_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMoveDown_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdMoveDown, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMoveDown_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdGetImage_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdGetImage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdGetImage_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdGetImage_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdGetImage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdGetImage_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSearchInternet_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSearchInternet, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSearchInternet_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSearchInternet_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSearchInternet, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSearchInternet_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtLongDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLongDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLongDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLongDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLongDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShortDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShortDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShortDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShortDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShortDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShortDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShortDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShortDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShortDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturnKit_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavReturnKit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturnKit_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturnKit_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavReturnKit, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturnKit_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturnKit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavReturnKit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturnKit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturnKit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavReturnKit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturnKit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturn_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNavReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturn_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturn_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNavReturn, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturn_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturn_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNavReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturn_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNavReturn_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNavReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNavReturn_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSerialNumberIncrement_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSerialNumberIncrement, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSerialNumberIncrement_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSerialNumberIncrement_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSerialNumberIncrement, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSerialNumberIncrement_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSerialNumberMask_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSerialNumberMask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSerialNumberMask_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSerialNumberMask_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSerialNumberMask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSerialNumberMask_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstb_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDateEstb, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstb_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstb_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDateEstb, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstb_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstb_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDateEstb, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstb_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstb_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDateEstb, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstb_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuMain, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuMain, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuMain, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuMain, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuMain, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPOMatchTolerance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPOMatchTolerance, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPOMatchTolerance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPOMatchTolerance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPOMatchTolerance, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPOMatchTolerance_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPOMatchTolerance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPOMatchTolerance_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPuchProdLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPuchProdLine_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPuchProdLine, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPuchProdLine_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPuchProdLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPuchProdLine_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPuchProdLine, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPuchProdLine_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPuchProdLine_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPuchProdLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPuchProdLine_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrice_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPrice, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrice_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPrice, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrice_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrice_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrice_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuSales, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSales_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuSales, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSales_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuSales, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSales_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuSales, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSales_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSales_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuSales, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSales_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPurchaseUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPurchaseUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPurchaseUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPurchaseUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPurchaseUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPurchaseUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPurchaseUOM, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPurchaseUOM_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPurchaseUOM_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPurchaseUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPurchaseUOM_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuStockUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuStockUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuStockUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuStockUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuStockUOM, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuStockUOM_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuStockUOM_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuStockUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuStockUOM_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuFreightClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuFreightClass_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuFreightClass, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuFreightClass_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuFreightClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuFreightClass_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuFreightClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuFreightClass_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuFreightClass, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuFreightClass_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuFreightClass_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuFreightClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuFreightClass_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuCommoCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommoCode_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuCommoCode, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommoCode_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuCommoCode, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommoCode_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommoCode_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuCommoCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommoCode_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuDefltWarehouse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefltWarehouse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuDefltWarehouse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefltWarehouse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuDefltWarehouse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefltWarehouse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuDefltWarehouse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefltWarehouse_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuDefltWarehouse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefltWarehouse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefltWarehouse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuDefltWarehouse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefltWarehouse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuItemClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemClass_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuItemClass, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemClass_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuItemClass, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemClass_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItemClass_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuItemClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItemClass_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuCommCls, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommCls_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuCommCls, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommCls_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuCommCls, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommCls_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuCommCls, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommCls_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuCommCls, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommCls_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCommCls_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuCommCls, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCommCls_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuProdLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdLine_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuProdLine, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdLine_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuProdLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdLine_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuProdLine, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdLine_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProdLine_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuProdLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProdLine_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuCustPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCustPriceGroup_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuCustPriceGroup, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCustPriceGroup_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuCustPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCustPriceGroup_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuCustPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCustPriceGroup_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuCustPriceGroup, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCustPriceGroup_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCustPriceGroup_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuCustPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCustPriceGroup_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub numPeriodUsage_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numPeriodUsage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numPeriodUsage_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPeriodUsage_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numPeriodUsage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numPeriodUsage_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPeriodUsage_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numPeriodUsage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numPeriodUsage_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShelfLife_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numShelfLife, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numShelfLife_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShelfLife_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numShelfLife, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numShelfLife_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShelfLife_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numShelfLife, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numShelfLife_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numStdPrice_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numStdPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numStdPrice_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numStdPrice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numStdPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numStdPrice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numStdCost_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numStdCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numStdCost_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numStdCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numStdCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numStdCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numNOfDays_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numNOfDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numNOfDays_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numNOfDays_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numNOfDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numNOfDays_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numNOfDays_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numNOfDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numNOfDays_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDfltSaleQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numDfltSaleQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDfltSaleQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDfltSaleQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numDfltSaleQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDfltSaleQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDfltSaleQty_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numDfltSaleQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDfltSaleQty_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDfltSaleQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numDfltSaleQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDfltSaleQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinGrossProfitPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinGrossProfitPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinGrossProfitPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMargin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTargetMargin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMargin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMargin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTargetMargin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMargin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMargin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTargetMargin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMargin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPriceSeq_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numPriceSeq, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numPriceSeq_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPriceSeq_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numPriceSeq, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numPriceSeq_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numPriceSeq_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numPriceSeq, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numPriceSeq_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMandatorySalesMulti_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMandatorySalesMulti, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMandatorySalesMulti_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMandatorySalesMulti_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMandatorySalesMulti, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMandatorySalesMulti_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMandatorySalesMulti_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMandatorySalesMulti, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMandatorySalesMulti_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinSalesQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinSalesQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinSalesQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinSalesQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinSalesQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinSalesQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinSalesQty_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinSalesQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinSalesQty_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinSalesQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinSalesQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinSalesQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRates_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numRestockingRates, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRates_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRates_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numRestockingRates, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRates_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRates_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numRestockingRates, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRates_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMarginHide_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTargetMarginHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMarginHide_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMarginHide_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTargetMarginHide, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMarginHide_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMarginHide_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTargetMarginHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMarginHide_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTargetMarginHide_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTargetMarginHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTargetMarginHide_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPctHide_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinGrossProfitPctHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPctHide_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPctHide_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinGrossProfitPctHide, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPctHide_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPctHide_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinGrossProfitPctHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPctHide_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinGrossProfitPctHide_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinGrossProfitPctHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinGrossProfitPctHide_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRatesHide_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numRestockingRatesHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRatesHide_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRatesHide_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numRestockingRatesHide, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRatesHide_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRatesHide_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numRestockingRatesHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRatesHide_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockingRatesHide_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numRestockingRatesHide, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockingRatesHide_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRcptRequired_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkRcptRequired, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRcptRequired_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRcptRequired_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkRcptRequired, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRcptRequired_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRcptRequired_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkRcptRequired, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRcptRequired_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInternalDelvrRequired_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInternalDelvrRequired, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInternalDelvrRequired_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInternalDelvrRequired_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInternalDelvrRequired, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInternalDelvrRequired_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInternalDelvrRequired_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInternalDelvrRequired, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInternalDelvrRequired_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowCostOvrd_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowCostOvrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowCostOvrd_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowCostOvrd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowCostOvrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowCostOvrd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowCostOvrd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowCostOvrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowCostOvrd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReturnsAllowedMirror_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkReturnsAllowedMirror, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReturnsAllowedMirror_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReturnsAllowedMirror_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkReturnsAllowedMirror, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReturnsAllowedMirror_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReturnsAllowedMirror_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkReturnsAllowedMirror, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReturnsAllowedMirror_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowRtrnsPartialBTO_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowRtrnsPartialBTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowRtrnsPartialBTO_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowRtrnsPartialBTO_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowRtrnsPartialBTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowRtrnsPartialBTO_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowRtrnsCustomBTO_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowRtrnsCustomBTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowRtrnsCustomBTO_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowRtrnsCustomBTO_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowRtrnsCustomBTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowRtrnsCustomBTO_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowRtrnsCustomBTO_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowRtrnsCustomBTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowRtrnsCustomBTO_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEplodeOnInvoice_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkEplodeOnInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEplodeOnInvoice_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEplodeOnInvoice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkEplodeOnInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEplodeOnInvoice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEplodeOnInvoice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkEplodeOnInvoice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEplodeOnInvoice_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkExplodeOnPickTicket_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkExplodeOnPickTicket, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkExplodeOnPickTicket_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkExplodeOnPickTicket_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkExplodeOnPickTicket, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkExplodeOnPickTicket_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkExplodeOnPickTicket_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkExplodeOnPickTicket, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkExplodeOnPickTicket_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPriceAsCompTotal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPriceAsCompTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPriceAsCompTotal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPriceAsCompTotal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPriceAsCompTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPriceAsCompTotal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowDecQuantities_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowDecQuantities, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowDecQuantities_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowDecQuantities_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowDecQuantities, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowDecQuantities_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkHazMat_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkHazMat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkHazMat_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkHazMat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkHazMat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkHazMat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkHazMat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkHazMat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkHazMat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInternal_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInternal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInternal_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInternal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInternal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInternal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInternal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInternal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInternal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSeasonal_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkSeasonal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSeasonal_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSeasonal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkSeasonal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSeasonal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSeasonal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkSeasonal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSeasonal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDropShipFlg_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkDropShipFlg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDropShipFlg_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDropShipFlg_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkDropShipFlg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDropShipFlg_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDropShipFlg_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkDropShipFlg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDropShipFlg_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReturnsAllowed_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkReturnsAllowed, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReturnsAllowed_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReturnsAllowed_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkReturnsAllowed, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReturnsAllowed_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowPriceOvrd_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowPriceOvrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowPriceOvrd_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowPriceOvrd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowPriceOvrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowPriceOvrd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowPriceOvrd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowPriceOvrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowPriceOvrd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSubToTradeDisc_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkSubToTradeDisc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSubToTradeDisc_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSubToTradeDisc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkSubToTradeDisc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSubToTradeDisc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSubToTradeDisc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkSubToTradeDisc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSubToTradeDisc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclOnPackList_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInclOnPackList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclOnPackList_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclOnPackList_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInclOnPackList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclOnPackList_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclOnPackList_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInclOnPackList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclOnPackList_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnAdjQtyRoundMeth_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnAdjQtyRoundMeth, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnAdjQtyRoundMeth_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnAdjQtyRoundMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnAdjQtyRoundMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnAdjQtyRoundMeth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnAdjQtyRoundMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnAdjQtyRoundMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnAdjQtyRoundMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnAdjQtyRoundMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnAdjQtyRoundMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnAdjQtyRoundMeth_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFixedAssetTemplate_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnFixedAssetTemplate, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFixedAssetTemplate_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFixedAssetTemplate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnFixedAssetTemplate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFixedAssetTemplate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFixedAssetTemplate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnFixedAssetTemplate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFixedAssetTemplate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFixedAssetTemplate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnFixedAssetTemplate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFixedAssetTemplate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSalesTaxClass_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnSalesTaxClass, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSalesTaxClass_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSalesTaxClass_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnSalesTaxClass, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSalesTaxClass_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSalesTaxClass_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnSalesTaxClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSalesTaxClass_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSalesTaxClass_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnSalesTaxClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSalesTaxClass_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnTrackMethod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnTrackMethod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnTrackMethod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnTrackMethod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnTrackMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnTrackMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnTrackMethod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnTrackMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnTrackMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnStatus, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnItemType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnItemType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnItemType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnItemType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnItemType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnItemType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnItemType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnItemType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnItemType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnValuation_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnValuation, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnValuation_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnValuation_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnValuation, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnValuation_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnValuation_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnValuation, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnValuation_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnProvider_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnProvider, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnProvider_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnProvider_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnProvider, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnProvider_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnProvider_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnProvider, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnProvider_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And Controls Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetItemImagePict(bValue As Boolean, Optional biNet As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    webItemImage.Visible = bValue
    cmdMoveUp.Enabled = False
    cmdMoveDown.Enabled = False
      
    If Not IsMissing(biNet) Then
        cmdGetImage.Enabled = bValue
        cmdSearchInternet.Enabled = bValue
    Else
        cmdGetImage.Enabled = Not bValue
        cmdSearchInternet.Enabled = Not bValue
    End If
      
    If Not bValue Then
        lblItemImage.Visible = Not bValue
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetItemImagePict", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Public Sub FormatKitCompGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    gGridSetProperties grdCompKits, kMaxKitCols, kGridDataSheet  ' use datasheet grid formating
 
    'Added so that you locked cells cannot get focus
    grdCompKits.EditModePermanent = True
  
    'Setup maximum number of rows and columns
    gGridSetMaxRows grdCompKits, 1
    gGridSetMaxCols grdCompKits, kMaxKitCols

    'Set column widths
    grdCompKits.UnitType = SS_CELL_UNIT_TWIPS
    
    gGridSetHeader grdCompKits, kKitColComponentItem, gsBuildString(kIMCompoItem, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompKits, kKitColComponentItem, 3300
    gGridSetColumnType grdCompKits, kKitColComponentItem, SS_CELL_TYPE_EDIT, 30
    
    gGridSetHeader grdCompKits, kKitColDesc, gsBuildString(kDescri, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompKits, kKitColDesc, 4600
    gGridLockColumn grdCompKits, kKitColDesc
    
    gGridSetColumnType grdCompKits, kKitColQuantity, SS_CELL_TYPE_FLOAT, miQtyDecimal, 7
    gGridSetHeader grdCompKits, kKitColQuantity, gsBuildString(kQuantity, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompKits, kKitColQuantity, 1500
    
    gGridSetHeader grdCompKits, kKitColstkUOM, gsBuildString(kIMStkUOM, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompKits, kKitColstkUOM, 1000
    gGridLockColumn grdCompKits, kKitColstkUOM
    
    gGridHideColumn grdCompKits, kKitColSeqNo
    gGridHideColumn grdCompKits, kKitColCompKey
    
    gGridSetColumnWidth grdCompKits, kKitColType, 1000
    gGridSetColumnType grdCompKits, kKitColType, SS_CELL_TYPE_STATIC_TEXT
    gGridHideColumn grdCompKits, kKitColType
    
    gGridSetHeader grdCompKits, kKitColDQAllow, "AllowDecQty"
    gGridSetColumnWidth grdCompKits, kKitColDQAllow, 100
    gGridSetColumnType grdCompKits, kKitColDQAllow, SS_CELL_TYPE_STATIC_TEXT
    gGridHideColumn grdCompKits, kKitColDQAllow
    
    'Scroll bars
    grdCompKits.ScrollBars = ScrollBarsBoth

    With grdCompKits
        .Row = -1
        .Col = kKitColQuantity
        .TypeFloatMax = String$(7, "9") & "." & String$(miQtyDecimal, "9")
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatKitCompGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub InitLookup()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sRestrictClause As String

    'This sub is used to initialize all of the look-ups.
    Set lkuCommCls.Framework = moClass.moFramework
    Set lkuCommCls.SysDB = moClass.moAppDB
    Set lkuCommCls.AppDatabase = moClass.moAppDB
    lkuCommCls.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuCommoCode.Framework = moClass.moFramework
    Set lkuCommoCode.SysDB = moClass.moAppDB
    Set lkuCommoCode.AppDatabase = moClass.moAppDB
    
    Set lkuCustPriceGroup.Framework = moClass.moFramework
    Set lkuCustPriceGroup.SysDB = moClass.moAppDB
    Set lkuCustPriceGroup.AppDatabase = moClass.moAppDB
    lkuCustPriceGroup.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuDefltWarehouse.Framework = moClass.moFramework
    Set lkuDefltWarehouse.SysDB = moClass.moAppDB
    Set lkuDefltWarehouse.AppDatabase = moClass.moAppDB
    lkuDefltWarehouse.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuFreightClass.Framework = moClass.moFramework
    Set lkuFreightClass.SysDB = moClass.moAppDB
    Set lkuFreightClass.AppDatabase = moClass.moAppDB
        
    Set lkuItemClass.Framework = moClass.moFramework
    Set lkuItemClass.SysDB = moClass.moAppDB
    Set lkuItemClass.AppDatabase = moClass.moAppDB
    lkuItemClass.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)

    Set lkuPOMatchTolerance.Framework = moClass.moFramework
    Set lkuPOMatchTolerance.SysDB = moClass.moAppDB
    Set lkuPOMatchTolerance.AppDatabase = moClass.moAppDB
    lkuPOMatchTolerance.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuPrice.Framework = moClass.moFramework
    Set lkuPrice.SysDB = moClass.moAppDB
    Set lkuPrice.AppDatabase = moClass.moAppDB
    lkuPrice.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuPuchProdLine.Framework = moClass.moFramework
    Set lkuPuchProdLine.SysDB = moClass.moAppDB
    Set lkuPuchProdLine.AppDatabase = moClass.moAppDB
    lkuPuchProdLine.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuSales.Framework = moClass.moFramework
    Set lkuSales.SysDB = moClass.moAppDB
    Set lkuSales.AppDatabase = moClass.moAppDB
    lkuSales.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuStockUOM.Framework = moClass.moFramework
    Set lkuStockUOM.SysDB = moClass.moAppDB
    Set lkuStockUOM.AppDatabase = moClass.moAppDB
    lkuStockUOM.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuPurchaseUOM.Framework = moClass.moFramework
    Set lkuPurchaseUOM.SysDB = moClass.moAppDB
    Set lkuPurchaseUOM.AppDatabase = moClass.moAppDB
    lkuPurchaseUOM.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    Set lkuProdLine.Framework = moClass.moFramework
    Set lkuProdLine.SysDB = moClass.moAppDB
    Set lkuProdLine.AppDatabase = moClass.moAppDB
    lkuProdLine.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)

    Set lkuMain.Framework = moClass.moFramework
    Set lkuMain.SysDB = moClass.moAppDB
    Set lkuMain.AppDatabase = moClass.moAppDB
    sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    If miModule = kModuleCI Then
        sRestrictClause = sRestrictClause & " AND ItemType < 5"
    End If
    lkuMain.RestrictClause = sRestrictClause

    glaCOS.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu
    glaCOS.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    glaCOS.RestrictClause = glaCOS.RestrictClause & " AND Status = 1"
    
    glaExpense.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu
    glaExpense.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    glaExpense.RestrictClause = glaExpense.RestrictClause & " AND Status = 1"
    
    glaSales.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu
    glaSales.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    glaSales.RestrictClause = glaSales.RestrictClause & " AND Status = 1"
    
    glaReturns.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu
    glaReturns.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    glaReturns.RestrictClause = glaReturns.RestrictClause & " AND Status = 1"

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitLookup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub InitDropDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Here the drop downs are initialized.
    With ddnItemType
        .InitStaticList moClass.moAppDB, "timItem", "ItemType"

        'Set default Item Type to Finished goods for Inventory Item and Misc Item for non-inventory items.
        If miModule = kModuleCI Then                   '2 for CI
            cmdSubstitution.Enabled = False
            cmdLandCost.Enabled = False
            .DefaultItemData = kItemTypeMiscItem       'Misc Item
            .StaticListExcludeValues = "5,6,7,8,9"
        ElseIf miModule = kModuleIM Then               '7 for IM
            .DefaultItemData = kItemTypeFinishedGood   'Finished goods
            
            'Removed Catalog Items.
            .StaticListExcludeValues = "9"
        End If
    End With
    
    With ddnProvider
        .InitStaticList moClass.moAppDB, "timItem", "WarrantyProvider"
    End With
    
    With ddnStatus
        .InitStaticList moClass.moAppDB, "timItem", "Status"
        .StaticListExcludeValues = "5"
    End With
    
    With ddnTrackMethod
        .InitStaticList moClass.moAppDB, "timItem", "TrackMeth"
        .IgnoreSameValue = False
    End With
    
    With ddnValuation
        .InitStaticList moClass.moAppDB, "timItem", "ValuationMeth"
    End With
    
    With ddnSalesTaxClass
        .SQLStatement = "SELECT tciSTaxClass.STaxClassID , tciSTaxClass.STaxClassKey FROM tciSTaxClass WHERE tciSTaxClass.STaxCodeClass <> 1"
        .InitDynamicList moClass.moAppDB, .SQLStatement
    End With
    
    'Static list used on UOM grid
    Set moConvTypeList = New clsStaticList
    moConvTypeList.InitStaticList moClass.moAppDB, moClass.moSysSession.Language, "timItemUnitOfMeas", "ConversionType"

    With ddnAdjQtyRoundMeth
        .InitStaticList moClass.moAppDB, "timItem", "AdjQtyRoundMeth"
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitDropDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub IMActivatedChk()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'If the IM module has not been activated then disable the required controls.
    If mbIMIsActivated = False Then
        NonStkItemSetting
        
        'Restrict values in the ddnItemType control.
        ddnItemType.StaticListExcludeValues = "5, 6, 7, 8, 9"

        'If IM is not activated, disable the Custom Fields button.
        cmdCustField.Enabled = False
    Else
        ddnItemType.StaticListExcludeValues = "9"
        StkItemSetting
        cmdCustField.Enabled = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IMActivatedChk", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub CntrlSettings()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'All the controls settings have to be done here.
    mbBrowseRec = False
       
    'Disable the Date Established control and No of days the provider provides warranty for.
    txtDateEstb.Enabled = False
    numNOfDays.Enabled = False
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CntrlSettings", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetDefValues()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Used for setting the default values.
    Dim sDateString As String
   
    If moDmItemForm.State = sotaTB_ADD Then
        'Set default status to Active.
        ddnStatus.DefaultItemData = kItemStatActive   'Active
        
        'Set here the tag as well so that the security event handler in Validate will not give a GPF.
        ddnStatus.Tag = kItemStatActive
        ddnStatus.SetToDefault True
        
        'Set default Item Type to Finished goods for Inventory Item and Misc Item for non-inventory items
        If miModule = kModuleCI Then          '2 for CI
            cmdSubstitution.Enabled = False
            cmdLandCost.Enabled = False
            ddnItemType.DefaultItemData = kItemTypeMiscItem 'Misc Item
            ddnItemType.StaticListExcludeValues = "5,6,7,8,9"
        ElseIf miModule = kModuleIM Then     '7 for IM
            ddnItemType.DefaultItemData = kItemTypeFinishedGood 'Finished Good
            ddnItemType.StaticListExcludeValues = "9"
            
            tabMaintainItem.TabEnabled(kTabKit) = False
            tabMaintainItem.TabEnabled(kTabGLAcct) = False
        End If
        
        ddnItemType.SetToDefault True

        'Set the dafault date to Business Date.
        txtDateEstb.Text = Format(mdDate, gsGetLocalVBDateMask())
       
        'Get the default ItemClass from timOptions.
        PopulateVal kProcessGetItemClass
        PopulateVal kProcessItemClass
        
        SetupAdjQtyRoundMeth
        
        '************************************************************************
        'SGS DEJ 9/7/2012   (START)
        '************************************************************************
        chkAllowDecQuantities.Value = kChecked
        Call chkAllowDecQuantities_Click
        '************************************************************************
        'SGS DEJ 9/7/2012   (STOP)
        '************************************************************************
        
    End If

    mbShelfValChange = False
    miSavedLife = 0

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetDefValues", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function ChkNotNullFlds() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lPPLKey As Long
    Dim lWKey As Long
    Dim sSQL As String
    Dim rst As Object
    Dim dSalesMult As Double
    Dim dDfltSaleQty As Double

    mbIsErrorMessage = True

    'Here we check for Not Null fields and Numeric Validations.
    
    'Not Null Field Validation
    If Len(Trim$(ddnStatus)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
gsStripChar(lblStatus.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        bSetFocus ddnStatus
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Len(Trim$(ddnItemType)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
gsStripChar(lblItemType.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        bSetFocus ddnItemType
        ChkNotNullFlds = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Len(Trim$(lkuItemClass.Text)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
gsStripChar(lblItemClass.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        bSetFocus lkuItemClass
        ChkNotNullFlds = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Len(Trim$(ddnTrackMethod)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
gsStripChar(lblTrackMeth.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        bSetFocus ddnTrackMethod
        ChkNotNullFlds = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If ddnValuation.Enabled Then
        If Len(Trim$(ddnValuation)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
gsStripChar(lblValMeth.Caption, kAmpersand)
            tabMaintainItem.Tab = kTabDesc
            ddnValuation.SetFocus
            ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If

    If (ddnTrackMethod.ItemData = kTrackNone _
And ddnValuation.ItemData = kValActual) Then
       giSotaMsgBox Me, moClass.moSysSession, kIMmsgValuation
        tabMaintainItem.Tab = kTabDesc
        bSetFocus ddnValuation
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Len(Trim$(ddnProvider)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
gsStripChar(lblProvider.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        bSetFocus ddnProvider
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    'Mandatory fields in case of AROptions having the TrackSTaxOnSales flag on for this company
    If miTrackSTaxOnSales = 1 Then
        If Len(Trim$(ddnSalesTaxClass)) = 0 And ddnItemType.ItemData <> kItemTypeCommentOnly Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
"Sales Tax Class"
            tabMaintainItem.Tab = kTabDesc
            bSetFocus ddnSalesTaxClass
            ChkNotNullFlds = False

            Exit Function
        End If
    End If
    'Mandatory fields in case of both Non Inventory and Inventory Items.
    If (Len(Trim$(lkuStockUOM)) = 0 _
And ddnItemType.ItemData <> kItemTypeCommentOnly) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
"Stock UOM"
        tabMaintainItem.Tab = kTabUOM
        bSetFocus lkuStockUOM
        ChkNotNullFlds = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    Else
        If (Len(Trim$(lkuPrice)) = 0 _
          And Not (ddnItemType.ItemData = kItemTypeBTOKit Or _
                    ddnItemType.ItemData = kItemTypeCommentOnly Or _
                    ddnItemType.ItemData = kItemTypeExpense)) Then
            If lkuPrice.Enabled = True Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
"Price UOM"
            Else
                giSotaMsgBox Me, moClass.moSysSession, 160140
            End If
            
            tabMaintainItem.Tab = kTabUOM
            lkuPrice.Enabled = True
            
            If lkuPrice.Enabled Then
                lkuPrice.SetFocus
            End If
            
            ChkNotNullFlds = False
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Else
            If (Len(Trim$(lkuPrice)) = 0 _
And ddnItemType.ItemData = kItemTypeBTOKit) Then
                lkuPrice.Text = lkuStockUOM.Text
            End If
        End If
    End If

    'Mandatory fields in case of Inventory Items
    'Stock UOM and Purchase Product Line
    If bIsInventory(ddnItemType.ItemData) = True Then
        If Len(Trim$(lkuSales.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
            "Sales UOM"
            tabMaintainItem.Tab = kTabUOM
            
            If lkuSales.Enabled Then
                lkuSales.SetFocus
            End If
            
            ChkNotNullFlds = False
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If

        'If Inventory but not BTO.
        If (ddnItemType.ItemData <> kItemTypeBTOKit _
And ddnItemType.ItemData <> kItemTypeService) Then
            'Purchase UOM
            If Len(Trim$(lkuPurchaseUOM.Text)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgDateMustBeValid, _
"Purchase UOM"
                tabMaintainItem.Tab = kTabUOM
                
                If lkuPurchaseUOM.Enabled Then
                    lkuPurchaseUOM.SetFocus
                End If
                
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
    
            'Check that values exist in the UOM grid for Stock, Purchase, Price, Sales.
            If bUOMConvAvailable(lkuStockUOM) = False Then
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            
            If bUOMConvAvailable(lkuSales) = False Then
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            
            If bUOMConvAvailable(lkuPrice) = False Then
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If

            If bUOMConvAvailable(lkuPurchaseUOM) = False Then
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
    Else
        'If the item is a Catalog Item then --
        If ddnItemType.ItemData = kItemTypeCatalog Then
            'The default warehouse should be entered.
            If Len(Trim$(lkuDefltWarehouse.Text)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kMsgUnspecifiedWarehouse
                ChkNotNullFlds = False
                tabMaintainItem.Tab = kTabDesc
                
                If lkuDefltWarehouse.Enabled = True Then
                    lkuDefltWarehouse.SetFocus
                End If
                
                Exit Function
            End If
            
            'And the PPL should be entered.
            If Len(Trim$(lkuPuchProdLine.Text)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgPPLneeded
                ChkNotNullFlds = False
                tabMaintainItem.Tab = kTabPurch
                lkuPuchProdLine.Enabled = True
                bSetFocus lkuPuchProdLine
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If

        If Not ddnItemType.ItemData = kItemTypeCommentOnly Then
            If Len(Trim$(glaCOS.Text)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgInvGL, _
"COGS"
                tabMaintainItem.Tab = kTabGLAcct
                
                If glaCOS.Enabled Then
                    glaCOS.SetFocus
                End If
                
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            
            If Len(Trim$(glaReturns.Text)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgInvGL, _
"Returns"
                tabMaintainItem.Tab = kTabGLAcct
                
                If glaReturns.Enabled Then
                    glaReturns.SetFocus
                End If
                
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
    End If

    'Numeric Validations

    If numShelfLife.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
gsStripChar(lblShelfLife.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        
        If numShelfLife.Enabled = True Then
            numShelfLife.SetFocus
        End If
        
        numShelfLife.Value = 0
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If numStdPrice.Value < 0 Then
        numStdPrice.Value = 0

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If (numNOfDays.Enabled _
And numNOfDays.Value < 0) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMValCheck, _
gsStripChar(lblNOfDays.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabDesc
        bSetFocus numNOfDays
        numNOfDays.Value = 0
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If numMinSalesQty.Value < 0 And bIsInventory(ddnItemType.ItemData) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
        gsStripChar(lblMinSalesQty.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabSales
        bSetFocus numMinSalesQty
        numMinSalesQty.ClearData
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    Else
        
        If (numMinSalesQty.Value > numDfltSaleQty.Value) And numMinSalesQty.Value <> 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
            lblMinSalesQty.Caption, lbDfltSaleQty.Caption
            tabMaintainItem.Tab = kTabSales
            bSetFocus numMinSalesQty
            numMinSalesQty.ClearData
            ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    If numMandatorySalesMulti.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
        gsStripChar(lblManSalesMulti.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabSales
        bSetFocus numMandatorySalesMulti
        numMandatorySalesMulti.Value = 0
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If numTargetMargin.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
        gsStripChar(lblTargetMargin.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabSales
        bSetFocus numTargetMargin
        numTargetMargin.Value = 0
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If numMinGrossProfitPct.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
        gsStripChar(lblMinGrossProfitPct.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabSales
        bSetFocus numMinGrossProfitPct
        numMinGrossProfitPct.Value = 0
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If numDfltSaleQty.Value < 0 And bIsInventory(ddnItemType.ItemData) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
        gsStripChar(lbDfltSaleQty.Caption, kAmpersand)
        tabMaintainItem.Tab = kTabSales
        bSetFocus numDfltSaleQty
        numDfltSaleQty.Value = 1
        ChkNotNullFlds = False
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If numDfltSaleQty.Value = 0 Then
       numDfltSaleQty.Value = 1
    End If
    
    If mbShelfValChange = True Then
        If (Not miSavedLife = CInt(Trim$(numShelfLife.Text)) _
            And miSavedLife < CInt(Trim$(numShelfLife.Text))) Then
             If giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgMinShelfLife) = vbNo Then
                tabMaintainItem.Tab = kTabDesc
                bSetFocus numShelfLife
                numShelfLife.Value = miSavedLife
                ChkNotNullFlds = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
             Else
                mbShelfValChange = False
             End If
        End If
    End If
    
    If Len(Trim(txtSerialNumberIncrement)) <> 0 Then
        If Len(Trim(txtSerialNumberMask)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgSerialMaskReq
            gbSetFocus Me, txtSerialNumberMask
            ChkNotNullFlds = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Else
            If Not mbCheckSerialMatch Then
                gbSetFocus Me, txtSerialNumberIncrement
                ChkNotNullFlds = False
                Exit Function
            End If
        End If
     
    End If
    
    'Default Sales Quantity must follow the rules of the mandatory sales qty
    'and be evenly divisible by the mandatory sales multiple qty
     dSalesMult = numMandatorySalesMulti.Value
     dDfltSaleQty = numDfltSaleQty.Value
     dDfltSaleQty = gfRound(dDfltSaleQty, 1, numDfltSaleQty.DecimalPlaces, 1)
        If dSalesMult <> 0 Then
            If dDfltSaleQty Mod dSalesMult <> 0 Then
                'Qty entered is not a multiple of the Sales Multiple value...
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgEvenlyDivis, _
                lbDfltSaleQty.Caption, lblManSalesMulti
                tabMaintainItem.Tab = kTabSales
                bSetFocus numDfltSaleQty
                numDfltSaleQty.Value = 1
                ChkNotNullFlds = False
               
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If

    'Since everything is in order, set to true.
    ChkNotNullFlds = True
    mbIsErrorMessage = False

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ChkNotNullFlds", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub PopulateVal(intProcess As Integer, Optional sqlIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'strProcess is used to identify which process we want to launch
    'this is used to populate values in the controls at run time
    
    Dim sSQLSource  As String       'used to make a SQL string
    Dim sSQLSource1 As String
    Dim sSQLSource2 As String
    Dim sSQLSource3 As String
    Dim sWhereClause As String
    Dim rs As Object          'used for local recordset creation
    Dim rsFetch As Object     'used to fetch foreign ID values
    Dim bOldClickCancel As Boolean
    
    Select Case intProcess
        Case kProcessSalesProdLine
            sSQLSource = "SELECT SalesProdLineKey, AllowPriceOvrd, CommClassKey, MinGrossProfitPct, SalesProdLineID, SubjToTradeDisc, TargetMargin FROM timSalesProdLine WITH (NOLOCK) WHERE SalesProdLineID = " & gsQuoted(lkuProdLine.Text) & " AND CompanyID = " & gsQuoted(msCompanyID)
    
        Case kProcessPurchProdLine
            sSQLSource = "SELECT ItemType,PurchProdLineKey,MatchToleranceKey FROM timPurchProdLine WITH (NOLOCK) WHERE PurchProdLineID = " & gsQuoted(lkuPuchProdLine.Text) & " AND CompanyID = " & gsQuoted(msCompanyID)
        
        Case kProcessItemClass
            sWhereClause = gsQuoted(lkuItemClass.Text) & " AND timItemClass.CompanyID =" & gsQuoted(msCompanyID)

            sSQLSource = "SELECT tglAccount.GLAcctNo, tglAccount.Description FROM tglAccount, timItemClass " _
& "WHERE tglAccount.GLAcctKey = timItemClass.COSAcctKey AND ItemClassID = " & sWhereClause
            
            sSQLSource1 = "SELECT tglAccount.GLAcctNo, tglAccount.Description FROM tglAccount, timItemClass " _
& "WHERE tglAccount.GLAcctKey = timItemClass.InvtAcctKey AND ItemClassID = " & sWhereClause
    
            sSQLSource2 = "SELECT tglAccount.GLAcctNo, tglAccount.Description FROM tglAccount, timItemClass " _
& "WHERE tglAccount.GLAcctKey = timItemClass.SalesReturnAcctKey AND ItemClassID = " & sWhereClause
    
            sSQLSource3 = "SELECT tglAccount.GLAcctNo, tglAccount.Description FROM tglAccount, timItemClass " _
& "WHERE tglAccount.GLAcctKey = timItemClass.SalesAcctKey AND ItemClassID = " & sWhereClause

        Case kProcessTrackMethod
            sSQLSource = "SELECT LotValuationMeth,ValuationMeth FROM timOptions WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID)
        
        Case kProcessGetSerialDefault
            sSQLSource = "SELECT SerialMask,SerialIncrement FROM timOptions WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID)
            
        Case kProcessStatus, kProcessItemType, kProcessKitDeletes, kProcessUOMDeletes
            sWhereClause = gsQuoted(lkuMain.Text) & " AND timItem.CompanyID =" & gsQuoted(msCompanyID)
            sSQLSource = "SELECT ItemKey FROM timItem WITH (NOLOCK) WHERE ItemID = " & sWhereClause
    
        Case kProcessKitComp, kProcessKitCompDelete
            grdCompKits.Col = kKitColComponentItem
            sWhereClause = gsQuoted(grdCompKits.Value) & " AND timItem.CompanyID =" & gsQuoted(msCompanyID)
            sSQLSource = "SELECT ItemKey,StockUnitMeasKey,ShelfLife,TrackMeth,StdPrice,AllowDecimalQty FROM timItem WITH (NOLOCK) WHERE ItemID = " & sWhereClause
    
        Case kProcessCommCode
            sSQLSource = "SELECT STaxClassKey FROM timCommodityCode WITH (NOLOCK) WHERE CommodityCodeID = " & gsQuoted(lkuCommoCode.Text) '& " AND CompanyID = " & gsQuoted(msCompanyID)
    
        Case kProcessGetItemClass
            sSQLSource = "SELECT ItemClassKey,ItemClassID FROM timItemClass WITH (NOLOCK) WHERE ItemClassKey = " & IIf(IsNull(moOptions.IM("DfltItemClassKey")), 0, moOptions.IM("DfltItemClassKey"))
    End Select

    Set rs = moClass.moAppDB.OpenRecordset(sSQLSource, kSnapshot, kOptionNone)
    With rs
        .MoveLast
        .MoveFirst
    
        Select Case intProcess
            Case kProcessPurchProdLine
                If Not .IsEOF Then
                ' if ItemType has been entered (Raw Material or Finished Goods) then give warning
                ' If user wants then return to main tab,
                ' ELSE continue without changing Item Type or returning to Main tab
                    If ddnItemType.ItemData = kItemTypeRawMaterial Or ddnItemType.ItemData = kItemTypeFinishedGood Then
                        If Not ddnItemType.ItemData = .Field("ItemType") Then
                            If moDmItemForm.State = sotaTB_ADD Then
                                If giSotaMsgBox(Nothing, moClass.moSysSession, kIMmsgChangeItem) = vbYes Then
                                    mbPPLtoType = True
                                    bOldClickCancel = mbIsClickCancel
                                    mbIsClickCancel = True
                                    ddnItemType.ItemData = .Field("ItemType")
                                    mbIsClickCancel = bOldClickCancel
                                    tabMaintainItem.Tab = 0
                                End If
                                
                                lkuPuchProdLine.Tag = lkuPuchProdLine.Text
                            End If
                        End If
                    End If
            
                    If Not IsNull(.Field("MatchToleranceKey")) And (lkuPOMatchTolerance.Enabled = True) Then
                        If Not lkuPOMatchTolerance.KeyValue = .Field("MatchToleranceKey") Then
                            If giSotaMsgBox(Nothing, moClass.moSysSession, 160131) = vbYes Then
                                sSQLSource = "SELECT MatchToleranceKey,MatchToleranceID FROM tpoMatchTolerance WHERE MatchToleranceKey = " & .Field("MatchToleranceKey")
                                Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource, kSnapshot, kOptionNone)
                                With rsFetch
                                    .MoveLast
                                    .MoveFirst
                                    If Not .IsEOF Then _
lkuPOMatchTolerance.SetTextAndKeyValue .Field("MatchToleranceID"), .Field("MatchToleranceKey")
                                    .Close
                                End With
                            End If
                        End If
                    End If
                End If
        
            Case kProcessSalesProdLine
                If Not .IsEOF Then
                    If Not IsNull(.Field("CommClassKey")) Then
                        sSQLSource = "SELECT CommClassKey,CommClassID FROM tarCommClass WHERE CommClassKey = " & .Field("CommClassKey")
                        Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource, kSnapshot, kOptionNone)
                        With rsFetch
                            .MoveLast
                            .MoveFirst
                            If Not .IsEOF Then
                                lkuCommCls.SetTextAndKeyValue .Field("CommClassID"), glGetValidLong(.Field("CommClassKey"))
                            End If
                            .Close
                        End With
                    End If
            
                    chkAllowPriceOvrd.Value = giGetValidInt(.Field("AllowPriceOvrd"))
                    chkSubToTradeDisc.Value = giGetValidInt(.Field("SubjToTradeDisc"))
                    numTargetMarginHide.Value = gdGetValidDbl(.Field("TargetMargin"))
                    numMinGrossProfitPctHide.Value = gdGetValidDbl(.Field("MinGrossProfitPct"))
                    numTargetMargin.Value = numTargetMarginHide * 100
                    numMinGrossProfitPct.Value = numMinGrossProfitPctHide * 100
                End If

            Case kProcessGetItemClass
                If Not .IsEOF Then
                    lkuItemClass.SetTextAndKeyValue .Field("ItemClassID"), .Field("ItemClassKey")
                End If
                
            Case kProcessGetSerialDefault
                If Not .IsEmpty Then
                    txtSerialNumberMask.Enabled = True
                    txtSerialNumberIncrement.Enabled = True
                    txtSerialNumberMask.Text = gsGetValidStr(.Field("SerialMask"))
                    txtSerialNumberIncrement.Text = gsGetValidStr(.Field("SerialIncrement"))
                End If
                
            Case kProcessItemClass
                If Not .IsEOF Then
                    glaCOS.Text = .Field("GLAcctNo")
                    Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource2, kSnapshot, kOptionNone)
                    With rsFetch
                        .MoveLast
                        .MoveFirst
                        If Not .IsEOF Then
                            If intProcess = kProcessItemClass Then glaReturns.Text = .Field("GLAcctNo")
                        End If
                        .Close
                    End With
                    Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource3, kSnapshot, kOptionNone)
                    With rsFetch
                        .MoveLast
                        .MoveFirst
                        If Not .IsEOF Then
                            If intProcess = kProcessItemClass Then glaSales.Text = .Field("GLAcctNo")
                        End If
                        .Close
                    End With
                    Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource1, kSnapshot, kOptionNone)
                    With rsFetch
                        .MoveLast
                        .MoveFirst
                        If Not .IsEOF Then
                            If intProcess = kProcessItemClass Then glaExpense.Text = .Field("GLAcctNo")
                        End If
                        .Close
                    End With
                End If

            Case kProcessTrackMethod
                If Not .IsEOF Then
                    If ddnTrackMethod.ItemData = kTrackLot Or ddnTrackMethod.ItemData = kTrackBoth Then
                        ddnValuation.ItemData = IIf(IsNull(.Field("LotValuationMeth")), 2, .Field("LotValuationMeth"))
                    Else
                        If ddnTrackMethod.ItemData = kTrackNone And _
                                (IsNull(.Field("ValuationMeth")) Or (.Field("ValuationMeth") = 5)) Then
                            ddnValuation.ItemData = kValStd
                        Else
                            If ddnTrackMethod.ItemData = kTrackNone Or ddnTrackMethod.ItemData = kTrackSerial Then _
                                ddnValuation.ItemData = IIf(IsNull(.Field("ValuationMeth")), 2, .Field("ValuationMeth"))
                        End If
                    End If
                    mbDefaultValuation = True
                End If

            Case kProcessCommCode
                If Not .IsEOF Then
                    sSQLSource1 = "SELECT STaxClassID FROM tciSTaxClass WHERE STaxClassKey = " & .Field("STaxClassKey")
                    Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource1, kSnapshot, kOptionNone)
                    With rsFetch
                        .MoveLast
                        .MoveFirst
                        If Not .IsEOF Then ddnSalesTaxClass.Text = .Field("STaxClassID")
                        .Close
                    End With
                End If
        
            Case kProcessStatus, kProcessItemType, kProcessKitDeletes, kProcessUOMDeletes
                If Not .IsEOF Then
                    If intProcess = kProcessStatus Then
                        sSQLSource1 = "UPDATE timInventory SET Status =" & ddnStatus.GetItemDataByListItem(ddnStatus.Text) & ",UpdateCounter = UpdateCounter + 1 WHERE ItemKey =" & .Field("ItemKey")
                        moClass.moAppDB.ExecuteSQL sSQLSource1
                    ElseIf intProcess = kProcessItemType Then
                        mlItemKey = .Field("ItemKey")
                    ElseIf intProcess = kProcessKitDeletes Then
                        'Delete the records from timKit and timKitCompList
                        sSQLSource1 = "DELETE FROM timKit WHERE KitItemKey = " & .Field("ItemKey")
                        moClass.moAppDB.ExecuteSQL sSQLSource1
        
                        sSQLSource2 = "DELETE FROM timKitCompList WHERE KitItemKey = " & .Field("ItemKey")
                        moClass.moAppDB.ExecuteSQL sSQLSource2
                    ElseIf intProcess = kProcessUOMDeletes Then
                        'Delete the records from timItemUnitOfMEas
                        sSQLSource1 = "DELETE FROM timItemUnitOfMeas WHERE ItemKey = " & .Field("ItemKey")
                        moClass.moAppDB.ExecuteSQL sSQLSource1
                    End If
                End If

            Case kProcessKitComp, kProcessKitCompDelete
                If Not .IsEOF Then
                    If intProcess = kProcessKitComp Then
                        sSQLSource1 = "SELECT ShortDesc FROM timItemDescription WHERE ItemKey = " & .Field("ItemKey")
                        Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource1, kSnapshot, kOptionNone)
                        grdCompKits.Col = kKitColDesc
                        With rsFetch
                            .MoveLast
                            .MoveFirst
                            If Not .IsEOF Then
                                grdCompKits.Value = IIf(IsNull(.Field("ShortDesc")), "", .Field("ShortDesc"))
                            End If
                            .Close
                        End With
                        If Not .Field("StockUnitMeasKey") Then
                            sSQLSource2 = "SELECT UnitMeasID FROM tciUnitMeasure WHERE UnitMeasKey = " & .Field("StockUnitMeasKey")
                            Set rsFetch = moClass.moAppDB.OpenRecordset(sSQLSource2, kSnapshot, kOptionNone)
                            grdCompKits.Col = kKitColstkUOM
                            With rsFetch
                                .MoveLast
                                .MoveFirst
                                If Not .IsEOF Then
                                    grdCompKits.Value = .Field("UnitMeasID")
                                End If
                                .Close
                            End With
                        End If
                        
                        miMinShelfLife = .Field("ShelfLife")
                        SetShelfLife miMinShelfLife
                        
                        grdCompKits.Col = kKitColType
                        grdCompKits.Value = IIf(IsNull(.Field("TrackMeth")), kNoValue, .Field("TrackMeth"))
                        
                        grdCompKits.Col = kKitColDQAllow
                        grdCompKits.Value = IIf(IsNull(.Field("AllowDecimalQty")), kNoValue, .Field("AllowDecimalQty"))
                        
                        grdCompKits.Col = kKitColComponentItem
                   Else
                        'Subtract from the price.
                        If (chkPriceAsCompTotal.Value = 1) Then
                            numStdPrice.Value = numStdPrice.Value - ((.Field("StdPrice")) * mfQty)
                            mfQty = 1
                        End If
                    End If
                End If
        End Select
    
        .Close
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PopulateVal", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub ClearUnboundCtrls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Clear all unbound controls over here.
    'Clear the numTargetMargin.
    numTargetMargin.Value = 0

    'Clear the SalesProdLine lookup.
    lkuProdLine.Text = ""

    'Clear the numRestockingRates.
    numRestockingRates.Value = 0

    'Clear the numMinGrossProfitPct.
    numMinGrossProfitPct.Value = 0
    
    lstAssignProdCatID.Clear
    lstAvailProdCatID.Clear
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearUnBoundCtrls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetValUnboundCols()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'This is used to set value to Unbound cols if they have been updated.
    numTargetMarginHide.Value = (numTargetMargin / 100)
    numRestockingRatesHide.Value = numRestockingRates
    numMinGrossProfitPctHide.Value = (numMinGrossProfitPct / 100)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetValUnboundCols", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function UOMValidate(ByVal lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iColumnNo As Integer
    Dim lCounter As Long

    miUOMOrKit = kTabUOM
    moGMUOM_CellChange lRow, kUOMColTargetUOM

    If (miHoldSaveTab = kTabUOM _
    Or miHoldSaveTab = kTabKit) Then
        miUOMOrKit = miHoldSaveTab
    End If

    If (mbNoUOM = True) Then
        Exit Function
    End If

    'Check the conversion value as per the Allow Decimal Quantity.
    If (Not lRow = grdUOM.MaxRows) Then
        If bIsValidConvFactor(lRow) Then
            SetConvFactorDBValue lRow
        Else
            UOMValidate = False
            Exit Function
        End If
    Else
        UOMValidate = False
        Exit Function
    End If

    'Check for duplicate UOM codes in the grid.
    If (Not bCheckDupUOMInTheUOMGrid(lRow)) Then
        Exit Function
    End If

    'Check to see if we need to validate UPC Bar Codes (per IM Options).
    If (miValidateUPCBarCodes = 1) Then
        'Check for duplicate UPC Bar Codes in the grid
        'with the UPC Bar Code on this row.
        If (Not bCheckDupUPCInTheUOMGrid(lRow)) Then
            Exit Function
        End If

        'Check to make sure that this UPC Bar Code is numeric.
        If (Not bCheckNumericUPCInTheUOMGrid(lRow)) Then
            Exit Function
        End If
        
        'Check to see if the UPC Bar Code on this row duplicates any UPC Bar Codes
        'in the database for this MAS 500 Company.
        If (Not bCheckDupUPCInThisCompany(lRow)) Then
            Exit Function
        End If
    End If

    'If every check is passed, then true.
    UOMValidate = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UOMValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub StkUOMProcessing(sValue As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lKeyValue As Long

    'Here we do all the processing required at the lost focus of lkuStockUOM
    lKeyValue = glGetValidLong(gvCheckNull(moClass.moAppDB.Lookup _
("UnitMeasKey", "tciUnitMeasure", "UnitMeasID = " & gsQuoted(lkuStockUOM.Text) & _
" AND CompanyID = " & gsQuoted(msCompanyID)), SQL_INTEGER))

    If Not ddnItemType.ItemData = kItemTypeBTOKit Then
        If ddnItemType.ItemData = kItemTypeService Then
            lkuPurchaseUOM.Enabled = False
            lkuPurchaseUOM.ClearData
        Else
            lkuPurchaseUOM.Enabled = True
            If Trim$(lkuPurchaseUOM.Text) = "" Then lkuPurchaseUOM.SetTextAndKeyValue sValue, lKeyValue
        End If
        If ddnItemType.ItemData = kItemTypeExpense Then
            lkuSales.Enabled = False
            lkuSales.ClearData
        Else
            lkuSales.Enabled = True
            If Trim$(lkuSales.Text) = "" Then lkuSales.SetTextAndKeyValue sValue, lKeyValue
        End If
        
        If ddnItemType.ItemData = kItemTypeExpense Then
            lkuPrice.Enabled = False
            lkuPrice.ClearData
        Else
            lkuPrice.Enabled = True
        If Trim$(lkuPrice.Text) = "" Then lkuPrice.SetTextAndKeyValue sValue, lKeyValue
        End If
        If tabMaintainItem.Tab = kTabUOM Then
            If Not bSetFocus(lkuPurchaseUOM) Then
                If Not bSetFocus(lkuSales) Then
                    bSetFocus lkuPrice
                End If
            End If
        End If
        
    Else
        lkuPrice.Enabled = False
        lkuPurchaseUOM.Enabled = False
        lkuSales.Enabled = False
        
        lkuPurchaseUOM.Text = ""
        lkuSales.SetTextAndKeyValue sValue, lKeyValue
        lkuPrice.SetTextAndKeyValue sValue, lKeyValue
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StkUOMProcessing", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub AddDefUOMRow(ByVal sValue As String, Optional ctrl As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'-------------------------------------------------------------------------
'adds a row to the UOM grid when a UOM is selected through the lookup
'This is called only for Inventory Items (excluding BTO Kits)
'-------------------------------------------------------------------------

    Dim lngCount As Long
    Dim strStockUOM As String
    Dim lUOMKey As Long

    With grdUOM
        .Col = kUOMColTargetUOM
        
        strStockUOM = gsBuildString(kIMStkUOM, moClass.moAppDB, moClass.moSysSession)
        
        'If Stock UOM is the one changed - look for other rows where Conversion Type = Stock UOM
        'Reset them to the default values re-populate the Type drop down
        If ctrl = UOMLookUpControl.Stock Then
            'Look for another UOM labeled Stock UOM and change the type back to a drop down
            For lngCount = 1 To .DataRowCnt
                If StrComp(Trim$(gsGridReadCellText(grdUOM, lngCount, kUOMColConversionType)), strStockUOM, vbTextCompare) = 0 Then
                    gGridUpdateCell grdUOM, lngCount, kUOMColConversionTypeInt, moConvTypeList.DefaultDBValue
                        
                    gGridSetCellType grdUOM, lngCount, kUOMColConversionType, SS_CELL_TYPE_COMBOBOX, moConvTypeList.ListData
                    gGridUpdateCellText grdUOM, lngCount, kUOMColConversionType, moConvTypeList.LocalText(moConvTypeList.DefaultDBValue)
                    
                    gGridUpdateCell grdUOM, lngCount, kUOMColConversionFactor, CDec(dGetStandardUOMConv(lngCount))
                    
                    'Mark Use standard for the UOM
                    mbUOMChangeByEvent = True
                    gGridUpdateCell grdUOM, lngCount, kUOMColUseStdConv, kChecked
                    mbUOMChangeByEvent = False

                End If
            Next
        End If
        
        'Check to see if the UOM is already in the grid
        'If it is and we are changing the Stock UOM - we need to set the "Stock UOM" label in Conversion type and check UseStd
        'Exit the Sub - nothing to do it if already exists
        For lngCount = 1 To .DataRowCnt
            If StrComp(Trim$(gsGridReadCellText(grdUOM, lngCount, kUOMColTargetUOM)), sValue, vbTextCompare) = 0 Then
                'If this is the stock UOM - Update the cell text for Conv Type to read Stock UOM
                If ctrl = UOMLookUpControl.Stock Then
                    mbUOMChangeByEvent = True
                    
                    'Un-check Use standard for the Stock UOM
                    gGridUpdateCell grdUOM, lngCount, kUOMColUseStdConv, kUnchecked
                    
                    gGridSetCellType grdUOM, lngCount, kUOMColConversionType, SS_CELL_TYPE_STATIC_TEXT, 25
                    gGridUpdateCellText grdUOM, lngCount, kUOMColConversionType, strStockUOM
                    
                    gGridUpdateCell grdUOM, lngCount, kUOMColConversionTypeInt, moConvTypeList.DefaultDBValue
                    
                    mbUOMChangeByEvent = False
                End If
                
                'If the UOM was already in the grid, and is the default sales UOM, check the Sales option
                If StrComp(Trim$(gsGridReadCellText(grdUOM, lngCount, kUOMColTargetUOM)), lkuSales.Text, vbTextCompare) = 0 Then
                    gGridUpdateCell grdUOM, lngCount, kUOMColUseForSales, 1
                End If
            
                'If the UOM was already in the grid, and is the default purchases UOM, check the Sales option
                If StrComp(Trim$(gsGridReadCellText(grdUOM, lngCount, kUOMColTargetUOM)), lkuPurchaseUOM.Text, vbTextCompare) = 0 Then
                    gGridUpdateCell grdUOM, lngCount, kUOMColUseForPurchases, 1
                End If
                
                DisableEnable lngCount
                
                Exit Sub
            End If
        Next
        
        'Create a row also for the Data Manager.
        'This should be one less than the grid max row as
        'the Data Manager is a 1 base array
        'while the grid is a 0 based array.
        'Also, set the row dirty.
        mbAddDefRow = True
        moDmUOMGrid.AppendRow
        mbAddDefRow = False
        
        .Row = grdUOM.MaxRows - 1
        .Col = kUOMColTargetUOM
        .Action = 0
        
        moDmUOMGrid.SetRowDirty .Row
        miUOMOrKit = kTabUOM
        
        'After adding new row to the grid, if the unit of measure is the default sales UOM, check the Sales option in the grid.
        If StrComp(Trim$(gsGridReadCellText(grdUOM, .Row, kUOMColTargetUOM)), lkuSales.Text, vbTextCompare) = 0 Then
            gGridUpdateCell grdUOM, .Row, kUOMColUseForSales, 1
        End If
            
        'After adding new row to the grid, if the unit of measure is the default purchases UOM, check the Purchases option in the grid.
        If StrComp(Trim$(gsGridReadCellText(grdUOM, .Row, kUOMColTargetUOM)), lkuPurchaseUOM.Text, vbTextCompare) = 0 Then
            gGridUpdateCell grdUOM, .Row, kUOMColUseForPurchases, 1
        End If
        
        If (ddnTrackMethod.ItemData = 2 Or ddnTrackMethod.ItemData = 3) Then
            For lngCount = 1 To .Row
                DisableEnable lngCount
            Next
        Else
            DisableEnable .Row
        End If
        
        If ctrl = UOMLookUpControl.Stock Then
            'Since we added a new row for Stock UOM - we need to set the Conv Type = "Stock UOM" and the cell to static text
            gGridSetCellType grdUOM, .Row, kUOMColConversionType, SS_CELL_TYPE_STATIC_TEXT, 25
            gGridUpdateCellText grdUOM, .Row, kUOMColConversionType, gsBuildString(kIMStkUOM, moClass.moAppDB, moClass.moSysSession)
            
            gGridUpdateCell grdUOM, .Row, kUOMColConversionTypeInt, moConvTypeList.DefaultDBValue
            gGridUpdateCell grdUOM, .Row, kUOMColUseStdConv, kUnchecked

        End If
        
        'Set the display for standard conversion factor
        If ddnTrackMethod.ItemData <> kTrackSerial And ddnTrackMethod.ItemData <> kTrackBoth Then
            gGridUpdateCell grdUOM, .Row, kUOMColConversionFactor, CDec(dGetStandardUOMConv(.Row))
        End If
    End With
    
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "AddDefUOMRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub NonStkItemSetting()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'The control settings for non-stocked items are done over here.
     
    'Set TrackMethod = None for non-stock items And the Evaluation Method to be Standard
    ddnTrackMethod.ListIndex = ddnTrackMethod.GetIndexByItemData(0)
    ddnTrackMethod.ItemData = kUnchecked
    ddnValuation.ListIndex = kUnchecked
    ddnValuation.ItemData = kValNone
     
    lkuDefltWarehouse.ClearData
     
    'In case of Non Stock items default value for Warehouse from tciOptions.MiscItemWhseKey
    lkuCommoCode.ClearData
    lkuFreightClass.ClearData
             
    'Replacement Cost is possible for only Catalog Items
    'editable
'    numReplacementCost.ClearData
     
    ddnProvider.ItemData = kProvNone
    numNOfDays.ClearData
     
    'We only want to clear the UOM grid for BTO or Comment only
    If ddnItemType.ItemData = kItemTypeBTOKit Or ddnItemType.ItemData = kItemTypeCommentOnly Then
        moDmUOMGrid.Clear True
        moDmUOMGrid.DeleteBlock kFirstCell, grdUOM.MaxRows
    End If
     
    chkEplodeOnInvoice.Value = kUnchecked
    chkExplodeOnPickTicket.Value = kUnchecked
    chkPriceAsCompTotal.Value = kUnchecked
    chkAllowRtrnsCustomBTO.Value = kUnchecked
    chkAllowRtrnsPartialBTO.Value = kUnchecked
    
    moDmKitCompGrid.Clear True
     
    '--controls regarding Sales and Purchase
    'editable
    chkReturnsAllowed.Value = kUnchecked
     
    'editable
    chkAllowCostOvrd.Value = kUnchecked
    chkRcptRequired.Value = kUnchecked
     
    'for Non Inventory items let the Allow Decimal Quantites be enabled
    'editable
    chkAllowDecQuantities.Value = kUnchecked
    With numMandatorySalesMulti
        .Enabled = True
        .Value = 0
    End With
     
    chkSubToTradeDisc.Value = kUnchecked
    chkAllowPriceOvrd.Value = kUnchecked

    If ddnItemType.ItemData = kItemTypeCatalog Then
        chkDropShipFlg.Value = kChecked
    Else
        chkDropShipFlg.Value = kUnchecked
    End If

    'editable
    chkInternalDelvrRequired.Value = kUnchecked
    
    lkuCustPriceGroup.ClearData
             
    'editable
    lkuPuchProdLine.ClearData
     
    'can have a PurchProdLine with Catalog Items though entry is optional
    If Not ddnItemType.ItemData = kItemTypeCatalog Then
        'Do Nothing
    Else
        'editable
        numShelfLife.ClearData
    End If
     
    'editable
    numPriceSeq.ClearData
    lkuPOMatchTolerance.ClearData
    lkuProdLine.ClearData
     
    'editable
    lkuCommCls.ClearData
    numPeriodUsage.ClearData
             
    'editable
    numTargetMargin.ClearData
    numMinSalesQty.ClearData
    numMinSalesQty.Value = 0
    numMandatorySalesMulti.ClearData
    numMandatorySalesMulti.Value = 0
    numMinGrossProfitPct.ClearData

    'For non-stocked items Restocking rates would be disabled.
    numRestockingRates.ClearData
     
    glaCOS.ClearData
    glaExpense.ClearData
    glaSales.ClearData
    glaReturns.ClearData
            
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "NonStkItemSetting", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub StkItemSetting()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Controls regarding Stock Item
    lkuDefltWarehouse.ClearData
    lkuCommoCode.ClearData
    lkuFreightClass.ClearData
    ddnProvider.ItemData = 0
    
    If Not ddnItemType.ItemData = kItemTypeBTOKit Then
        If Not lkuStockUOM.Text = "" Then
            RedoUOMGrid
        End If
    End If
        
    chkEplodeOnInvoice.Value = 0
    chkExplodeOnPickTicket.Value = 0
    chkPriceAsCompTotal.Value = 0
    chkAllowRtrnsCustomBTO.Value = 0
    chkAllowRtrnsPartialBTO.Value = 0
        
    moDmKitCompGrid.Clear
        
    'Controls regarding Sales and Purchase
    chkInternalDelvrRequired.Value = 0
    lkuCustPriceGroup.ClearData
        

    If (mbPPLtoType = False _
And (moDmItemForm.State = sotaTB_ADD And Len(Trim$(lkuPuchProdLine.Text)) = 0)) Then
        lkuPuchProdLine.ClearData
    End If
        
    numPriceSeq.ClearData
    numPeriodUsage.Value = 0
    numMinSalesQty.ClearData
    numMinSalesQty.Value = 0
    
    chkRcptRequired.Value = kChecked
    chkAllowCostOvrd.Value = kUnchecked
    
    chkAllowDecQuantities.Value = kUnchecked
    numMandatorySalesMulti.ClearData
    numMandatorySalesMulti.Value = 0
    
    numRestockingRates.Value = 0
    chkDropShipFlg.Value = kUnchecked
           
    lkuProdLine.ClearData
    lkuPuchProdLine.ClearData
                       
    If Not (moDmItemForm.State = sotaTB_EDIT) Then
        ddnTrackMethod.ItemData = 0
        
        If Not ddnItemType.ItemData = kItemTypeBTOKit Then
            PopulateVal kProcessTrackMethod
            chkReturnsAllowed.Value = 1
        Else
            ddnValuation.ItemData = 0
            chkReturnsAllowed.Value = 0
            numShelfLife.Value = 0
        End If
    End If
        
    'WHY IS THIS HERE??
    If numNOfDays.Value = 0 Then
        numNOfDays.Enabled = False
    End If
        
    glaCOS.ClearData
    glaExpense.ClearData
    glaSales.ClearData
    glaReturns.ClearData

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StkItemSetting", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PromptUserOfFailure(ByVal Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Here we keep the msgbox for the user if changing of Item Type changes.
    Select Case Index
        Case Is = 1
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemPendTrans

        Case Is = 2
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemPostTrans

        Case Is = 3
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemSO

        Case Is = 7
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemPO

        Case Is = 4
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemQtyOnHand

        Case Is = 5
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemLotQty

        Case Is = 6
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgItemSerialQty

        Case Is = 8
            giSotaMsgBox Me, moClass.moSysSession, 160121

        Case Is = 10
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgNotSameMeasure
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PromptUserOfFailure", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub KitTabClear()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Changed by ashish on Feb 22 1999 to fix a bug. The focus should be on cell 1,1
    'after saving the kit.
    gGridSetActiveCell grdCompKits, 1, 1
     
    moDmKitCompGrid.Clear True
    
    chkEplodeOnInvoice.Value = 0
    chkExplodeOnPickTicket.Value = 0
    chkPriceAsCompTotal.Value = 0
    chkAllowRtrnsCustomBTO.Value = 0
    chkAllowRtrnsPartialBTO.Value = 0

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "KitTabClear", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function TypeChangeCheck(intProcess As Integer) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lKey As Long
      
    TypeChangeCheck = False
    
    'Check if any pending trans / posted trans / others exist for the item.
    'Use the stored procedure for this purpose.
    With moClass.moAppDB
       .SetInParamLong (mlItemKey)
       .SetOutParam lKey
       .ExecuteSP "spimItemTypeProc"
        lKey = .GetOutParam(2)
       .ReleaseParams
    End With
    
    'Do the normal settings for non-stocked items.
    If Not lKey = 0 Then
        'Prompt user about the failure
        PromptUserOfFailure lKey
        Exit Function
    End If

    TypeChangeCheck = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "TypeChangeCheck", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub DeleteKitProc()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Here we delete the corresponding records from timKit and timKitCompList tbls.
    PopulateVal kProcessKitDeletes

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteKitProc", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DeleteUOMProc()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Here we delete the corresponding records from timItemUnitOfMeas
    PopulateVal kProcessUOMDeletes

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteUOMProc", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub MakeGridActive()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Here we make the grid manager active by touching the grid.
    'This has been done because if we don't touch the grid in UOM grid than it bombs.
    grdUOM_Click kUOMColUseStdConv, 1
    grdUOM_Click kUOMColUseForPurchases, 1
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MakeGridActive", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetConversionZero()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'if the Stock UOM has been changed than make the Conversion factor for existing rows = 0
    'if track method is both or serial
    'check if the value of the UOM on the grid is same as sales or purchase
    'if so set conversion to 1 and disable both stdConv and Conversion factor
    Dim intRows As Long

    For intRows = 1 To grdUOM.MaxRows - 1
        If msGotValue = gsGridReadCell(grdUOM, intRows, kUOMColTargetUOM) Then
            gGridUpdateCellText grdUOM, intRows, kUOMColVolume, "0"
            
            gGridUpdateCell grdUOM, intRows, kUOMColVolume, _
            CDec(gdGetValidDbl(gsGridReadCell(grdUOM, intRows, kUOMColVolume)))
            
            gGridUpdateCellText grdUOM, intRows, kUOMColWeight, "0"
        
            gGridUpdateCell grdUOM, intRows, kUOMColWeight, _
            CDec(gdGetValidDbl(gsGridReadCell(grdUOM, intRows, kUOMColWeight)))
        End If
        
        If bIsSameMeasureType(lkuStockUOM.Text, gsGridReadCell(grdUOM, intRows, kUOMColTargetUOM)) Then
            gGridUnlockCell grdUOM, kUOMColUseStdConv, intRows
        Else
            If gsGridReadCell(grdUOM, intRows, kUOMColUseStdConv) = "1" Then
                gGridUpdateCell grdUOM, intRows, kUOMColUseStdConv, "0"
                mbUseStdConvOld = False
                gGridUnlockCell grdUOM, kUOMColConversionFactor, intRows
                gGridUnlockCell grdUOM, kUOMColConversionType, intRows
            End If
            
            gGridLockCell grdUOM, kUOMColUseStdConv, intRows
        End If
               
        gGridUpdateCellText grdUOM, intRows, kUOMColConversionFactor, CDec(dGetStandardUOMConv(intRows))
        
        grdUOM_Change 3, intRows
    Next

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetConversionZero", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim oDrillDown  As Object
    Dim bRetCode    As Boolean

    CMMenuSelected = False

    If IsEmpty(mlItemKey) Then
        Exit Function
    End If

    If (ctl Is lkuMain) Then
        Select Case lTaskID
            Case ktskMFBillOFMat
                Set oDrillDown = goGetSOTAChild(moClass.moFramework, moSotaObjects, kclsBillOfMatDD, ktskMFBillOFMat, kDDRunFlags, kContextDD)
                If oDrillDown Is Nothing Then
                   Exit Function
                End If
                bRetCode = oDrillDown.bBillOfMatInv(mlItemKey)
                Set oDrillDown = Nothing
        End Select
    End If
    
    CMMenuSelected = True

'+++ VB/Rig Begin Pop +++
        Exit Function

ExpectedErrorRoutine:
    Me.Enabled = True
    
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim wflagsPO As Long
    Dim wFlags As Long
    Dim msGridCellText As String
    Dim sDeleteLine As String
    Dim lColValue As Integer
    Dim lRowValue As Integer
    Dim bDeleteFlag As Boolean

    CMAppendContextMenu = False

    If Len(Trim$(lkuMain.Text)) = 0 Then
        Exit Function
    End If
    
    If (ctl Is lkuMain) Then
        If (mbMFActivated) Then
            AppendMenu hmenu, MF_ENABLED, ktskMFBillOFMat, "&Bill of Material"
        End If
    ElseIf (ctl Is grdUOM) Then
        'The 9th column is set to "1" if the row is loaded from database.
        'You can not delete those rows.
        'Otherwise one can delete the added rows.
        lColValue = grdUOM.ActiveCol
        lRowValue = grdUOM.ActiveRow
        grdUOM.Col = kUOMColRowFromDB
        
        If (grdUOM.Value <> "1" _
And lRowValue <> 1) Then
            bDeleteFlag = True
        End If
        
        If bDeleteFlag Then
            moGMUOM.MenuDelete = True
        Else
            moGMUOM.MenuDelete = False
        End If
        
        CMAppendContextMenu = False
        Exit Function
    End If
    
    DoEvents
    CMAppendContextMenu = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
                Resume Next
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub ChkAndSetDefault()

End Sub

#If CUSTOMIZER And Controls Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Public Sub QueryStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'--------------------------------------------------------------------------------------------
' Checks whether the value of the Item status of an existing record has changed
' If so, It asks whether the Inventory Status needs to be changed to
' If the user agrees to do so the Inventory record for the Item is also modified
'--------------------------------------------------------------------------------------------

Dim lngFeedBack As Long
Dim lItemType As Long

'collect the item type
lItemType = ddnItemType.ItemData
'If datamanager in edit state and the state has been changed and it is an inventory item
If (moDmItemForm.State = sotaTB_EDIT) And _
(Not mbBrowseRec) And mbStatChange And _
    bIsInventory(ddnItemType.ItemData) = True Then
    If Not miStat = ddnStatus.ItemData Then
        If giSotaMsgBox(Nothing, moClass.moSysSession, kIMItemStatus) = vbYes Then
            PopulateVal kProcessStatus
        Else
            ddnStatus.ItemData = miStat
        End If
        
        mbStatChange = False
    End If
End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "QueryStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lGetMeasure(lMeasure As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lGetMeasure = glGetValidLong(moClass.moAppDB.Lookup("MeasType", "tciUnitMeasure", "tciUnitMeasure.UnitMeasKey = " & _
                  lMeasure))
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lGetMeasure", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsRightMeasure() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lMeasureThis As Long
    Dim lMeasureThat As Long

    bIsRightMeasure = True
    
    If lkuStockUOM.Text <> "" Then
        lMeasureThis = lGetMeasure(lkuStockUOM.KeyValue)
        If lkuPrice.Text <> "" Then
            lMeasureThat = lGetMeasure(lkuPrice.KeyValue)
            If lMeasureThis <> lMeasureThat Then
                PromptUserOfFailure kNotMeasure
                Me.tabMaintainItem.Tab = 1
                DeleteGridRow lkuPrice.Text
                bSetFocus lkuPrice
                bIsRightMeasure = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
        
        If lkuPurchaseUOM.Text <> "" Then
            lMeasureThat = lGetMeasure(lkuPurchaseUOM.KeyValue)
            If lMeasureThis <> lMeasureThat Then
                PromptUserOfFailure kNotMeasure
                Me.tabMaintainItem.Tab = 1
                DeleteGridRow lkuPurchaseUOM.Text
                bSetFocus lkuPurchaseUOM
                bIsRightMeasure = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
        
        If lkuSales.Text <> "" Then
            lMeasureThat = lGetMeasure(lkuSales.KeyValue)
            If lMeasureThis <> lMeasureThat Then
                PromptUserOfFailure kNotMeasure
                Me.tabMaintainItem.Tab = 1
                DeleteGridRow lkuSales.Text
                bSetFocus lkuSales
                bIsRightMeasure = False
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
    Else
        If lkuPrice.Text <> "" Then
            lMeasureThis = lGetMeasure(lkuPrice.KeyValue)
            If lkuPurchaseUOM.Text <> "" Then
                lMeasureThat = lGetMeasure(lkuPurchaseUOM.KeyValue)
                If lMeasureThis <> lMeasureThat Then
                    PromptUserOfFailure kNotMeasure
                    Me.tabMaintainItem.Tab = 1
                    DeleteGridRow lkuPurchaseUOM.Text
                    bSetFocus lkuPurchaseUOM
                    bIsRightMeasure = False
                    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
        
            If lkuSales.Text <> "" Then
                lMeasureThat = lGetMeasure(lkuSales.KeyValue)
                If lMeasureThis <> lMeasureThat Then
                    PromptUserOfFailure kNotMeasure
                    Me.tabMaintainItem.Tab = 1
                    DeleteGridRow lkuSales.Text
                    bSetFocus lkuSales
                    bIsRightMeasure = False
                    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                   Exit Function
               End If
           End If
        Else
            If lkuPurchaseUOM.Text <> "" Then
               lMeasureThis = lGetMeasure(lkuPurchaseUOM.KeyValue)
               If lkuSales.Text <> "" Then
                   lMeasureThat = lGetMeasure(lkuSales.KeyValue)
                   If lMeasureThis <> lMeasureThat Then
                       PromptUserOfFailure kNotMeasure
                       Me.tabMaintainItem.Tab = 1
                       DeleteGridRow lkuSales.Text
                       bSetFocus lkuSales
                       bIsRightMeasure = False
                       
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                       Exit Function
                   End If
               End If
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsRightMeasure", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bIsInventory(iKeyVal As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If iKeyVal < 5 Or iKeyVal = 9 Then
        bIsInventory = False
    Else
        bIsInventory = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsInventory", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub DeleteGridRow(sUOM As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long
For lRow = 1 To grdUOM.MaxRows - 1
    If (gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)) = sUOM Then
        gGridDeleteRow grdUOM, lRow
        Exit For
    End If
Next
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteGridRow", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bUOMConvAvailable(oCont As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lCounter As Long
    
    bUOMConvAvailable = False
    
    For lCounter = 1 To grdUOM.MaxRows - 1
        If UCase(Trim$(gsGridReadCell(grdUOM, lCounter, kUOMColTargetUOM))) = UCase(Trim$(oCont.Text)) Then
            bUOMConvAvailable = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    Next

    'Message that conversion does not exist.
    giSotaMsgBox Me, moClass.moSysSession, kIMmsgCFNotAvailable, lkuStockUOM.Text, Trim$(oCont.Text)

    'Set focus to the grid on maxrows, kUOMColTargetUOM.
    tabMaintainItem.Tab = 1
    With grdUOM
        .Col = kUOMColTargetUOM
        .Row = grdUOM.MaxRows
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUOMConvAvailable", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub SetDecimalValues()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    numStdCost.DecimalPlaces = miCostDecimal
    numStdPrice.DecimalPlaces = miPriceDecimal
    numMinSalesQty.IntegralPlaces = 7
    numMinSalesQty.DecimalPlaces = miQtyDecimal
    numMinGrossProfitPct.IntegralPlaces = 3
    numMinGrossProfitPct.DecimalPlaces = 2

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetDecVal", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub CtrlsReset()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Clear the Desc tab
    ddnItemType.Enabled = True
    
    ddnTrackMethod.Enabled = True
    txtSerialNumberMask.Enabled = True
    txtSerialNumberIncrement.Enabled = True
    
    numShelfLife.Value = 0
    numShelfLife.Enabled = True
    
    If miModule = kModuleIM And mbMFActivated Then
        lkuDefltWarehouse.Enabled = True
    End If
    
    lkuFreightClass.Enabled = True
    lkuCommoCode.Enabled = True
    chkInternal.Value = 0
    chkSeasonal.Value = 0
    chkHazMat.Value = 0
    
    ddnValuation.Enabled = True
    numStdPrice.Value = 0
    numStdPrice.Enabled = True
    numNOfDays.Value = 0
    numNOfDays.Enabled = False
    
    grdUOM.Enabled = False
    lkuSales.Enabled = False
    lkuPurchaseUOM.Enabled = False
    lkuPrice.Enabled = False
    
    KitTabClear
    
    If navGrid.Visible = True Then
        navGrid.Visible = False
    End If

    If navGridKit.Visible = True Then
        navGridKit.Visible = False
    End If
    
    chkInternalDelvrRequired.Value = kUnchecked
    chkRcptRequired.Value = kUnchecked
    chkSubToTradeDisc.Value = kUnchecked
    chkAllowPriceOvrd.Value = kUnchecked
    chkReturnsAllowed.Value = kUnchecked
    chkAllowCostOvrd.Value = kUnchecked
    chkDropShipFlg.Value = kUnchecked
    chkReturnsAllowed.Enabled = kChecked
    chkAllowCostOvrd.Enabled = kChecked
    chkAllowDecQuantities.Enabled = kChecked
    chkDropShipFlg.Enabled = kChecked
    chkInternalDelvrRequired.Enabled = kChecked
    chkRcptRequired.Enabled = kChecked
    chkSubToTradeDisc.Enabled = kChecked
    chkAllowPriceOvrd.Enabled = kChecked
    
    chkInclOnPackList.Enabled = True

    tabMaintainItem.TabEnabled(kTabKit) = True
    tabMaintainItem.TabEnabled(kTabGLAcct) = True
    tabMaintainItem.TabEnabled(kTabCategories) = True
    
    numDfltSaleQty.Enabled = True
    numTargetMargin.Value = 0
    numPriceSeq.ClearData
    numRestockingRates.Value = 0
    numMinSalesQty.ClearData
    numMinSalesQty.Value = 0
    numMandatorySalesMulti.Value = 0
    numMinGrossProfitPct.Value = 0
    numMinGrossProfitPct.Enabled = True
    lkuProdLine.Enabled = True
    lkuCommCls.Enabled = True
    lkuCustPriceGroup.Enabled = True
    numTargetMargin.Enabled = True
    numPriceSeq.Enabled = True
    numRestockingRates.Enabled = True
    numMinSalesQty.Enabled = True
    numMandatorySalesMulti.Enabled = False
    
    lkuPuchProdLine.Enabled = True
    lkuPOMatchTolerance.ClearData
    numPeriodUsage.Value = 0
    
    If mbIRIsActivated Then
        numPeriodUsage.Enabled = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CtrlsReset", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetShelfLife(iVal As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iShelfLife
    iShelfLife = CInt(Trim$(numShelfLife.Text))
    If Not iShelfLife = 0 Then
        If Not iVal = 0 Then
            numShelfLife.Value = IIf(iShelfLife < iVal, iShelfLife, iVal)
        Else
            numShelfLife.Value = iShelfLife
        End If
    Else
        numShelfLife.Value = iVal
    End If
    miSavedLife = numShelfLife.Value
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetShelfLife", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub StdOrSpecial(lUOMKeyVal As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StdOrSpecial", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ShiftHiddenKey()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRowToDelete As Long
    Dim lRowToReplace As Long
    Dim lMaxRow As Long

    lRowToDelete = grdUOM.ActiveRow
    lMaxRow = glGridGetMaxRows(grdUOM)
    grdUOM.redraw = False
    
    'Move next row's data into the previous row, after the row marked for deletion
    'upto the last row
    For lRowToReplace = lRowToDelete To lMaxRow - 2
        gGridUpdateCell grdUOM, lRowToReplace, kUOMColUOMKey, gsGridReadCell(grdUOM, lRowToReplace + 1, kUOMColUOMKey)
    Next lRowToReplace

    grdUOM.Row = lMaxRow - 1
    grdUOM.Row2 = lMaxRow - 1
    grdUOM.Action = SS_ACTION_SELECT_BLOCK
    grdUOM.redraw = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "ShiftHiddenKey", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bMoveRecords() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRowToDelete As Long
    Dim lRowToReplace As Long
    Dim lMaxRow As Long

    bMoveRecords = False
    
    lRowToDelete = grdCompKits.ActiveRow
    lMaxRow = glGridGetMaxRows(grdCompKits)
    grdCompKits.redraw = False
    
    'Move next row's data into the previous row, after the row marked for deletion
    'upto the last row
    For lRowToReplace = lRowToDelete To lMaxRow - 2
        gGridUpdateCell grdCompKits, lRowToReplace, kKitColComponentItem, gsGridReadCell(grdCompKits, lRowToReplace + 1, kKitColComponentItem)
        gGridUpdateCell grdCompKits, lRowToReplace, kKitColCompKey, gsGridReadCell(grdCompKits, lRowToReplace + 1, kKitColCompKey)
        gGridUpdateCell grdCompKits, lRowToReplace, kKitColDesc, gsGridReadCell(grdCompKits, lRowToReplace + 1, kKitColDesc)
        gGridUpdateCell grdCompKits, lRowToReplace, kKitColQuantity, gsGridReadCell(grdCompKits, lRowToReplace + 1, kKitColQuantity)
        gGridUpdateCell grdCompKits, lRowToReplace, kKitColstkUOM, gsGridReadCell(grdCompKits, lRowToReplace + 1, kKitColstkUOM)
        gGridUpdateCell grdCompKits, lRowToReplace, kKitColSeqNo, gsGridReadCell(grdCompKits, lRowToReplace, kKitColSeqNo)
        moDmKitCompGrid.SetRowDirty lRowToReplace
    Next lRowToReplace

    grdCompKits.Row = lMaxRow - 1
    grdCompKits.Row2 = lMaxRow - 1
    
    ' Some how column has shift to column 6 set it back to kKitColComponentItem.
    grdCompKits.Col = kKitColComponentItem
      
    grdCompKits.Action = SS_ACTION_SELECT_BLOCK
    grdCompKits.redraw = True
    
    bMoveRecords = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bMoveRecords", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidUOMID(lUOMRowInGrid As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sUOMID As String
    Dim lUOMKey As Long

    'Verify that the UOM is a valid one.
    bIsValidUOMID = False
    mbNoUOM = False
    
    sUOMID = gsGridReadCell(grdUOM, lUOMRowInGrid, kUOMColTargetUOM)
    msUOMIDReturn = sUOMID
    
    If Len(Trim$(sUOMID)) = 0 Then
        mbNoUOM = True
        
        giSotaMsgBox Me, moClass.moSysSession, kIMUOMBlank

        tabMaintainItem.Tab = kTabUOM
        gGridSetActiveCell grdUOM, lUOMRowInGrid, kUOMColTargetUOM
        moGMUOM.Scroll
        grdUOM.Action = 0
        Exit Function
    Else
        'Get the UOM key.
        lUOMKey = gvCheckNull(moClass.moAppDB.Lookup _
("UnitMeasKey", "tciUnitMeasure", "UnitMeasID = " & gsQuoted(sUOMID) & _
" AND CompanyID = " & gsQuoted(msCompanyID)), SQL_INTEGER)
        
        ' If an invalid unit of measure was entered, inform the user, reset the value
        ' and cancel the leave cell event.
        '
        If lUOMKey = 0 Then
            If (Not mbIsPressF5 _
And Not Me.ActiveControl.Name = navGrid.Name) Then
                If Not mbIsCancelButton Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidUOMID
                End If
            End If
            
            DoEvents
            tabMaintainItem.Tab = kTabUOM
            gGridSetActiveCell grdUOM, lUOMRowInGrid, kUOMColTargetUOM
            
            If (Not lUOMRowInGrid = grdUOM.MaxRows - 1 _
And Not Len(Trim$(msUOMVal)) = 0) Then
                gGridUpdateCell grdUOM, lUOMRowInGrid, kUOMColTargetUOM, msUOMVal
            Else
                If (lUOMRowInGrid = grdUOM.MaxRows - 1 _
And Len(Trim$(msUOMVal)) = 0) Then
                    'If the active control is navGrid then the entered text is used as filter
                    'so don't clear that.
                    If Me.ActiveControl Is navGrid Then
                        'Do Nothing
                    Else
                        gGridUpdateCell grdUOM, lUOMRowInGrid, kUOMColTargetUOM, ""
                    End If
                    
                    mbNoUOM = True
                Else
                    gGridUpdateCell grdUOM, lUOMRowInGrid, kUOMColTargetUOM, msUOMVal
                End If
            End If
            
            grdUOM.Action = 0
            Exit Function
        Else
            'The UOM is valid.
            mlUOMKeyReturn = lUOMKey
        End If
    End If

    bIsValidUOMID = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bIsValidUOMID", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidConvFactor(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sConvFactor As String
    Dim dConvFactor As Double
    Dim iDecPos     As Integer
    Dim iLen        As Integer
    Dim iRight      As Integer
    Dim iLeft       As Integer
    
    bIsValidConvFactor = False
    sConvFactor = Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColConversionFactor))
    
    'Check that the conversion factor is numeric
    If Not IsNumeric(sConvFactor) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCIUOMMustBeNumber, gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)
        tabMaintainItem.Tab = 1
        grdUOM.Action = 0
        Exit Function
    End If
    
    'Find the number of decimal places to the left and the right of the decimal
    iDecPos = InStr(sConvFactor, ".")
    iLen = Len(sConvFactor)
    If iDecPos > 0 Then
        iRight = iLen - iDecPos
        iLeft = iLen - iRight - 1
    Else
        iRight = 0
        iLeft = iLen
    End If
    
    'Validate that the number of digits to the right is not greater than 13
    If iRight > 13 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCIUOMMaxPrecision, gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)
        tabMaintainItem.Tab = 1
        grdUOM.Action = 0
        Exit Function
    End If

    'Validate that the number of digits to the left is not greater than 12 and that the factor is greater than 0
    dConvFactor = CDec(gsGridReadCell(grdUOM, lRow, kUOMColConversionFactor))
    If iLeft > 12 Or dConvFactor <= 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCIUOMMaxValue, gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM)
        tabMaintainItem.Tab = 1
        grdUOM.Action = 0
        Exit Function
    End If
        
    'For track method Both or Serial - only whole numbers are allowed for conversion factors
    If ddnTrackMethod.ItemData = 2 Or ddnTrackMethod.ItemData = 3 Then
        If dConvFactor <> 1 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMConvWN
            tabMaintainItem.Tab = 1
            grdUOM.Action = 0
            Exit Function
        End If
    End If
    
    bIsValidConvFactor = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bIsValidConvFactor", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidKitComponent(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sKitID As String
    Dim lKitKey As Long

    If (ddnItemType.ItemData <> kItemTypeBTOKit _
And ddnItemType.ItemData <> kItemTypeAssembledKit) Then
        Exit Function
    End If

    'Verify that the UOM is a valid one.
    bIsValidKitComponent = False
    mbNoKit = False

    sKitID = gsGridReadCell(grdCompKits, lRow, kKitColComponentItem)

    If Len(Trim$(sKitID)) = 0 Then
        mbNoKit = True
        mbKitMessageDisplayed = True
    
        'supress duplicate message
        'in case of lostfocus and gnavcellchange in cascade.  The flag is set
        'here and reset in moDmKitCompGrid_DMGridValidate, bPreSave(), GNav_CellChange.
        If mbCompBlankMessageDisplayed Then
            MsgBox "Component item cannot be left blank", vbOKOnly + vbCritical, "Sage 500 ERP"
            mbCompBlankMessageDisplayed = False
        End If
    
        tabMaintainItem.Tab = kTabKit
    
        gGridUpdateCell grdCompKits, lRow, kKitColComponentItem, msKitCompVal
        gGridSetActiveCell grdCompKits, lRow, kKitColComponentItem
    
        'set the focus on the component grid.
        On Error Resume Next
        bSetFocus grdCompKits
    
        moGMKitComp.Scroll
        grdCompKits.Action = 0
        msKitCompVal = Trim$(gsGridReadCellText(grdCompKits, lRow, kKitColComponentItem))
        Exit Function
    Else
        'Get the key.
        If ddnItemType.ItemData = kItemTypeBTOKit Then
            lKitKey = gvCheckNull(moClass.moAppDB.Lookup _
("ItemKey", "timItem", "ItemID = " & gsQuoted(sKitID) & _
" AND (Status = " & Format$(kItemStatActive) & " OR Status = " & Format$(kItemStatDiscontinued) & ")" & _
" AND (Not (ItemType = 7 OR ItemType = 3 OR ItemType = 4 OR ItemType = 2)) AND CompanyID = " & gsQuoted(msCompanyID)), SQL_INTEGER)
        Else
            lKitKey = gvCheckNull(moClass.moAppDB.Lookup _
("ItemKey", "timItem", "ItemID = " & gsQuoted(sKitID) & _
" AND (Status = " & Format$(kItemStatActive) & " OR Status = " & Format$(kItemStatDiscontinued) & ")" & _
" AND Not (ItemType = 7 Or ItemType = 9 OR ItemType = 3 OR ItemType = 4 OR ItemType = 2) AND CompanyID = " & gsQuoted(msCompanyID)), SQL_INTEGER)
        End If
    
        If lKitKey = 0 Then
            If (mbIsPressF5 = False _
And Not Me.ActiveControl.Name = navGridKit.Name) Then
                If mbKitMessageDisplayed Then
                    mbKitMessageDisplayed = False
                Else
                    If Not mbIsCancelButton Then
                        giSotaMsgBox Me, moClass.moSysSession, kIMmsgInvItem
                    End If
                End If
            End If
            
            tabMaintainItem.Tab = kTabKit
            gGridSetActiveCell grdCompKits, lRow, kKitColComponentItem
            If lRow = grdCompKits.MaxRows - 1 Then
                If msKitCompVal = "" Then
                    If Me.ActiveControl Is navGridKit Then
                        'Do Nothing
                    Else
                        gGridUpdateCell grdCompKits, lRow, kKitColComponentItem, ""
                    End If
                Else
                    gGridUpdateCell grdCompKits, lRow, kKitColComponentItem, msKitCompVal
                End If
            Else
                gGridUpdateCell grdCompKits, lRow, kKitColComponentItem, msKitCompVal
            End If
            
            mbNoKit = True
            grdCompKits.Action = 0
            msKitCompVal = Trim$(gsGridReadCellText(grdCompKits, lRow, kKitColComponentItem))
            Exit Function
        End If
            
        If (moDmItemForm.State <> sotaTB_NONE) And _
(ddnItemType.ItemData = kItemTypeBTOKit) Then
           If chkAllowRtrnsPartialBTO.Value = kUnchecked And _
chkReturnsAllowed.Value = kChecked Then
               If Not bCheckAllowReturnStatusForAllComponent(grdCompKits) Then
                   If msKitCompVal = "" Then
                       If Me.ActiveControl Is navGridKit Then
                           'Do Nothing
                       Else
                           gGridUpdateCell grdCompKits, lRow, kKitColComponentItem, ""
                       End If
                   Else
                       gGridUpdateCell grdCompKits, lRow, kKitColComponentItem, msKitCompVal
                   End If
                   
                   Exit Function
               End If
           End If
        End If
            
        mlCompKeyReturn = lKitKey
    End If

    bIsValidKitComponent = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bIsValidKitComponent", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bPreSave() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iTabOnTop As Integer
    Dim lActiveCol As Long
    Dim lActiveRow As Long
    Dim sCheckString As String
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim lPrCnt As Long

    bPreSave = False
    
    mbFromPreSave = True
    iTabOnTop = tabMaintainItem.Tab

    'If it is a  case of Actual None then give a message here and set focus back at ddnValuation.
    If Not bTrueValue(ddnValuation.ItemData(, True)) Then
        giSotaMsgBox Me, moClass.moSysSession, kIMmsgValuation
        
        'On error resume is required in case if the drop down is disabled.
        ddnValuation.Enabled = True
        bSetFocus ddnValuation
        bPreSave = False
        Exit Function
    End If

    'If the Item is a kit item then check if the quantity entered is 0 or what.
    If (ddnItemType.ItemData(, True) = kItemTypeBTOKit _
Or ddnItemType.ItemData(, True) = kItemTypeAssembledKit) Then
        If Not bDealWithQty(lActiveRow) Then
            Exit Function
        End If
    End If

    'In fact it is moved out from Validation Manager to lost focus and in bPreSave().
    If numStdCost.Tag <> numStdCost.Value Then  'field Was Changed
        'Show security event form.
        'Check the user id typed in can override (no cancel hit).
        sID = CStr(ksecChangeCost)
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True

        If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
            tabMaintainItem.Tab = kTabDesc
            
            'No Override reset Value
            numStdCost.Value = numStdCost.Tag
            gbSetFocus Me, numStdCost
            bPreSave = False
            Exit Function
        End If   'Security Event Overridden
    End If       'Click Event Called from Code

    If numStdPrice.Tag <> numStdPrice.Value Then
        'Show security event form.
        'Check the user id typed in can override (no cancel hit).
        sID = CStr(ksecChangePrice)
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True
      
        If moClass.moFramework.GetSecurityEventPerm(sID, _
sUser, vPrompt) = 0 Then
            tabMaintainItem.Tab = kTabDesc
          
            'No Override Reset Value.
            numStdPrice.Value = numStdPrice.Tag
            gbSetFocus Me, numStdPrice
            bPreSave = False
            Exit Function
        Else
            numStdPrice.Tag = numStdPrice.Value
             
        End If   'Security Event Overridden
    End If       'Click Event Called from Code

    Select Case iTabOnTop
        Case Is = kTabDesc
            'Do Nothing

        Case Is = kTabUOM
            If Not ddnItemType.ItemData = kItemTypeBTOKit Then  '(bIsInventory(ddnItemType.ItemData) = True
                lActiveCol = glGridGetActiveCol(grdUOM)
                lActiveRow = glGridGetActiveRow(grdUOM)
                
                If lActiveRow = grdUOM.MaxRows Then
                    bPreSave = True
                    Exit Function
                End If

                If Not lActiveCol = kUOMColTargetUOM Then
                    With grdUOM
                        .Col = kUOMColTargetUOM
                        .SetFocus
                    End With
                End If

                DoEvents

                If Not bIsValidUOMID(lActiveRow) Then
                    Exit Function
                End If
            End If

        Case Is = kTabKit
            If (ddnItemType.ItemData = kItemTypeBTOKit _
Or ddnItemType.ItemData = kItemTypeAssembledKit) Then
                lActiveCol = glGridGetActiveCol(grdCompKits)
                lActiveRow = glGridGetActiveRow(grdCompKits)
        
                If lActiveRow = grdCompKits.MaxRows Then
                    If grdCompKits.MaxRows > 1 Then
                        gGridSetActiveCell grdCompKits, grdCompKits.MaxRows - 1, kKitColComponentItem
                    End If

                    bPreSave = True
                    Exit Function
                End If
            
                If Not lActiveCol = kKitColComponentItem Then
                    With grdCompKits
                        .Col = kKitColComponentItem
                        .SetFocus
                        msKitCompVal = Trim$(gsGridReadCell(grdCompKits, lActiveRow, kKitColComponentItem))
                    End With
                End If
                     
                tempStr = " bPreSave"
                mbKitMessageDisplayed = True
                DoEvents
            
                If Not bIsValidKitComponent(lActiveRow) Then
                    Exit Function
                Else
                    If Not Trim$(msKitCompVal) = Trim$(gsGridReadCell(grdCompKits, lActiveRow, kKitColComponentItem)) Then
                        PopulateVal kProcessKitComp
                        msKitCompVal = ""
                        If navGridKit.Visible = True Then navGridKit.Visible = False
                    End If

                    If Not bCheckDupCompInTheKitGrid(lActiveRow) Then
                        Exit Function
                    End If
                    
                    If Not bDealWithQty(lActiveRow) Then
                        Exit Function
                    End If
                    
                End If
            
                'supress duplicate message in case of LostFocus
                'and GNav_CellChange in cascade.
                mbCompBlankMessageDisplayed = True
            
                'Recalculate the Kit Price from the components, if necessary.
                If (chkPriceAsCompTotal.Value = 1) Then
                    numStdPrice.ClearData
            
                    For lPrCnt = 1 To grdCompKits.MaxRows - 1
                        AddPrice lPrCnt
                    Next
                End If
            End If
        
        Case Is = kTabSales
            'Do Nothing

        Case Is = kTabGLAcct
            'Do Nothing
    End Select

    '**********************************************************************************************
    'SGS DEJ    (START)
    '**********************************************************************************************
    If ValidateCOATest = False Then
        bPreSave = False
        Exit Function
    End If
    '**********************************************************************************************
    'SGS DEJ    (STOP)
    '**********************************************************************************************

    bPreSave = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bPreSave", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub FirstSave()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    QueryStatus

    'Set value to unbound columns.
    SetValUnboundCols

    If (Me.ActiveControl.Name = lkuPrice.Name _
        And moDmItemForm.State = sotaTB_EDIT _
        And lkuPrice.Enabled = True) Then
        lkuPrice_LostFocus
    End If

    If ddnItemType.ItemData = kItemTypeBTOKit Then
        moDmUOMGrid.SetDirty False
    End If

    If Not (ddnItemType.ItemData = kItemTypeBTOKit _
        Or ddnItemType.ItemData = kItemTypeAssembledKit) Then
        moDmKitForm.SetDirty False
    End If

    If (bIsInventory(ddnItemType.ItemData) = True _
        Or ddnItemType.ItemData = kItemTypeCommentOnly) Then
        If Not glaSales.Text = "" Then
            glaSales.Text = ""
        End If

        If Not glaCOS.Text = "" Then
            glaCOS.Text = ""
        End If

        If Not glaExpense.Text = "" Then
            glaExpense.Text = ""
        End If

        If Not glaReturns.Text = "" Then
            glaReturns.Text = ""
        End If
    End If

    'True if click on Cancel button.
    mbIsClickCancel = False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "FirstSave", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function SecondSave() As Integer
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    SecondSave = kDmSuccess

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "SecondSave", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bDealWithQty(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bDealWithQty = False
        
    grdCompKits.Col = kKitColQuantity

    Dim iLoopVar As Long
    
    For iLoopVar = 1 To grdCompKits.MaxRows - 1
        If Val(Trim$(gsGridReadCell(grdCompKits, iLoopVar, kKitColQuantity))) <= 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidSelQty
            tabMaintainItem.Tab = kTabKit
            
            With grdCompKits
                .Action = 0
                .Col = kKitColQuantity
                .Row = iLoopVar
                .SetFocus
                
                gGridSetActiveCell grdCompKits, iLoopVar, kKitColQuantity
                
                'For some reason (Unknown??) the code here makes the look up on grid visible
                'Since the column on which focus is is the quantity column,
                'If lookup is used it gives a GPF.  So make invisible.
                navGridKit.Visible = False
            End With
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    Next iLoopVar
        
    bDealWithQty = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bDealWithQty", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckDupCompInTheKitGrid(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim s1 As String
    Dim lCounter As Long

    bCheckDupCompInTheKitGrid = False

    s1 = Trim$(gsGridReadCell(grdCompKits, lRow, kKitColComponentItem))

    If (grdCompKits.MaxRows > 2) Then                  'There are more than 1 kit components.
        grdCompKits.Col = kKitColComponentItem

        For lCounter = 1 To grdCompKits.MaxRows - 1
            If (lRow <> lCounter) Then
                If (Trim$(gsGridReadCell(grdCompKits, lCounter, kKitColComponentItem)) = s1 _
                And Not s1 = "") Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMmsgDupKitItem
                    grdCompKits.Row = lRow
                    tabMaintainItem.Tab = kTabKit
                    grdCompKits.Action = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
        Next lCounter
    End If

    bCheckDupCompInTheKitGrid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckDupCompInTheKitGrid", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckDupUOMInTheUOMGrid(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'--------------------------------------------------------------------------------------------
'This procedure checks for the existence of duplicate TargetUOM on the UOM grid.
'It accepts the row for which the TargetUOM is to be checked and goes through
'all the other rows to see if the ID is repeated.
'--------------------------------------------------------------------------------------------
    Dim s1 As String
    Dim lCounter As Long

    bCheckDupUOMInTheUOMGrid = False

    s1 = Trim$(gsGridReadCell(grdUOM, lRow, kUOMColTargetUOM))

    If (grdUOM.MaxRows > 2) Then                  'There are more than 1 UOM components.
        grdUOM.Col = kUOMColTargetUOM

        For lCounter = 1 To grdUOM.MaxRows - 1
            If (lRow <> lCounter) Then
                If (Trim$(gsGridReadCell(grdUOM, lCounter, kUOMColTargetUOM)) = s1 _
                And Not s1 = "") Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMDupUOM
                    grdUOM.Row = lRow
                    tabMaintainItem.Tab = kTabUOM
                    grdUOM.Action = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If
        Next lCounter
    End If

    bCheckDupUOMInTheUOMGrid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckDupUOMInTheUOMGrid", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckDupUPCInTheUOMGrid(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'--------------------------------------------------------------------------------------------
'This procedure checks for the existence of duplicate UPC on the UOM grid.
'It accepts the row for which the UPC is to be checked and goes through
'all the other rows to see if the UPC is repeated.
'--------------------------------------------------------------------------------------------
    Dim s1 As String
    Dim lCounter As Long

    bCheckDupUPCInTheUOMGrid = False

    s1 = Trim$(gsGridReadCell(grdUOM, lRow, kUOMColUPC))

    If (Len(s1) > 0) Then
        If (grdUOM.MaxRows > 2) Then                  'There are more than 1 UOM components.
            grdUOM.Col = kUOMColUPC

            For lCounter = 1 To grdUOM.MaxRows - 1
                If (lRow <> lCounter) Then
                    If (Trim$(gsGridReadCell(grdUOM, lCounter, kUOMColUPC)) = s1) Then
                        giSotaMsgBox Me, moClass.moSysSession, kIMDupUPC
                        grdUOM.Row = lRow
                        tabMaintainItem.Tab = kTabUOM
                        grdUOM.Action = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                End If
            Next lCounter
        End If
    End If

    bCheckDupUPCInTheUOMGrid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckDupUPCInTheUOMGrid", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckNumericUPCInTheUOMGrid(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'--------------------------------------------------------------------------------------------
'This procedure checks to make sure that the entered UPC on the UOM grid is numeric.
'It accepts the row for which the UPC is to be checked.
'--------------------------------------------------------------------------------------------
    Dim s1 As String
    Dim iCharIndex As Integer
    Dim sChar As String

    bCheckNumericUPCInTheUOMGrid = False

    s1 = Trim$(gsGridReadCell(grdUOM, lRow, kUOMColUPC))

    If (Len(s1) > 0) Then
        For iCharIndex = 1 To Len(s1)
            sChar = Mid$(s1, iCharIndex, 1)

            If (sChar <> "0" _
            And sChar <> "1" _
            And sChar <> "2" _
            And sChar <> "3" _
            And sChar <> "4" _
            And sChar <> "5" _
            And sChar <> "6" _
            And sChar <> "7" _
            And sChar <> "8" _
            And sChar <> "9") Then
                giSotaMsgBox Me, moClass.moSysSession, kIMUPCMustBeNumeric
                grdUOM.Col = kUOMColUPC
                grdUOM.Row = lRow
                tabMaintainItem.Tab = kTabUOM
                grdUOM.Action = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        Next iCharIndex
    End If

    bCheckNumericUPCInTheUOMGrid = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckNumericUPCInTheUOMGrid", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckDupUPCInThisCompany(ByVal lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'--------------------------------------------------------------------------------------------
'This procedure checks for the existence of duplicate UPC on the UOM grid.
'It accepts the row for which the UPC is to be checked and checks to see
'if that UPC is already being used by a different Item in the MAS 500 Company.
'--------------------------------------------------------------------------------------------
    Dim s1 As String
    Dim sSQL As String
    Dim rsItemUnitOfMeas As DASRecordSet
    Dim sItemID As String

    bCheckDupUPCInThisCompany = False

    If (Len(Trim$(lkuMain.Text)) > 0) Then
        s1 = Trim$(gsGridReadCell(grdUOM, lRow, kUOMColUPC))

        If Len(s1) > 0 Then
            sSQL = "SELECT itm.ItemID"
            sSQL = sSQL & " FROM timItemUnitOfMeas itmuom WITH (NOLOCK)"
            sSQL = sSQL & " INNER JOIN timItem itm WITH (NOLOCK) ON (itmuom.ItemKey = itm.ItemKey)"
            sSQL = sSQL & " WHERE (itm.CompanyID = " & gsQuoted(msCompanyID)
            sSQL = sSQL & " AND COALESCE(DATALENGTH(LTRIM(RTRIM(itmuom.UPC))), 0) > 0"
            sSQL = sSQL & " AND LTRIM(RTRIM(itmuom.UPC)) = " & gsQuoted(s1)
            sSQL = sSQL & " AND LTRIM(RTRIM(itm.ItemID)) <> " & gsQuoted(Trim$(lkuMain.Text)) & ")"

            Set rsItemUnitOfMeas = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

            If (Not rsItemUnitOfMeas.IsEOF) Then
                sItemID = Trim$(gsGetValidStr(rsItemUnitOfMeas.Field("ItemID")))

                'Close the recordset opened above.
                If Not rsItemUnitOfMeas Is Nothing Then
                    rsItemUnitOfMeas.Close
                    Set rsItemUnitOfMeas = Nothing
                End If

                If Len(sItemID) > 0 Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUPCNotUniqueInCompany, sItemID
                    grdUOM.Col = kUOMColUPC
                    grdUOM.Row = lRow
                    tabMaintainItem.Tab = kTabUOM
                    grdUOM.Action = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Function
                End If
            End If

            'Close the recordset opened above.
            If Not rsItemUnitOfMeas Is Nothing Then
                rsItemUnitOfMeas.Close
                Set rsItemUnitOfMeas = Nothing
            End If
        End If
    End If

    bCheckDupUPCInThisCompany = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckDupUPCInThisCompany", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub ValuesRemove(sVal As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If sVal = "Type" Then
        If ddnItemType.ItemData = 9 Then
            ddnItemType.StaticListExcludeValues = "1,2,3,4"
        Else
            ddnItemType.StaticListExcludeValues = ""
            ddnItemType.Refresh
        End If
    Else
        If sVal = "Valuation" Then
            If bIsInventory(ddnItemType.ItemData) = True _
            And Not ddnItemType.ItemData = kItemTypeBTOKit Then
                ddnValuation.StaticListExcludeValues = "0"
                ddnValuation.Refresh
            Else
                ddnValuation.StaticListExcludeValues = ""
                ddnValuation.Refresh
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValuesRemove", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bIsSameMeasureType(MeasureOne As String, MeasureTwo As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim MeasOneType As Integer
    Dim MeasTwoType As Integer

    bIsSameMeasureType = False

    If (MeasureOne = "" Or MeasureTwo = "") Then
        Exit Function
    End If

    MeasOneType = gvCheckNull(moClass.moAppDB.Lookup _
        ("MeasType", "tciUnitMeasure", "UnitMeasID = " & gsQuoted(MeasureOne) & _
        " AND CompanyID = " & gsQuoted(msCompanyID)), SQL_INTEGER)

    MeasTwoType = gvCheckNull(moClass.moAppDB.Lookup _
        ("MeasType", "tciUnitMeasure", "UnitMeasID = " & gsQuoted(MeasureTwo) & _
        " AND CompanyID = " & gsQuoted(msCompanyID)), SQL_INTEGER)

    If MeasOneType = MeasTwoType Then
        bIsSameMeasureType = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsSameMeasureType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub DisableEnable(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lCnt As Long
    Dim sStock As String
    Dim sPrice As String
    Dim sPurch As String
    Dim sSales As String
    Dim sTarget As String
    Dim sCntTarget As String
    
    sStock = UCase(Trim$(lkuStockUOM.Text))
    sPrice = UCase(Trim$(lkuPrice.Text))
    sPurch = UCase(Trim$(lkuPurchaseUOM.Text))
    sSales = UCase(Trim$(lkuSales.Text))
    sTarget = UCase(Trim$(gsGridReadCell(grdUOM, lRow, kUOMColTargetUOM)))

    If (ddnTrackMethod.ItemData = 2 _
        Or ddnTrackMethod.ItemData = 3) Then
        If Not gsGridReadCell(grdUOM, lRow, kUOMColUseStdConv) = "0" Then
            gGridUpdateCellText grdUOM, lRow, kUOMColUseStdConv, "0"
            mbUseStdConvOld = False
        End If
        
        gGridLockCell grdUOM, kUOMColUseStdConv, lRow
            
        If Not gsGridReadCell(grdUOM, lRow, kUOMColConversionFactor) = "1" Then
            gGridUpdateCellText grdUOM, lRow, kUOMColConversionFactor, "1"
            gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, _
            CDec(gdGetValidDbl(gsGridReadCell(grdUOM, lRow, kUOMColConversionFactor)))
        End If
        
        gGridLockCell grdUOM, kUOMColConversionFactor, lRow
        gGridLockCell grdUOM, kUOMColConversionType, lRow
        gGridLockCell grdUOM, kUOMColTargetUOM, lRow
        
        navGrid.Visible = False
    Else
        If sTarget = sStock Then
            'if it is in the add state (stock uom changed after other uoms chosen)
            If moDmItemForm.State = sotaTB_ADD Then
                If Trim$(lkuStockUOM) <> msGotValue And msGotValue <> "" Then SetConversionZero
                msGotValue = ""
                
                'set conversion factor to 1
                gGridUpdateCellText grdUOM, lRow, kUOMColConversionFactor, "1"
                gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, _
                CDec(gdGetValidDbl(gsGridReadCell(grdUOM, lRow, kUOMColConversionFactor)))
            End If
            
            'Lock all four columns
            gGridLockCell grdUOM, kUOMColUseStdConv, lRow
            gGridLockCell grdUOM, kUOMColConversionFactor, lRow
            gGridLockCell grdUOM, kUOMColConversionType, lRow
            gGridLockCell grdUOM, kUOMColTargetUOM, lRow
        Else
            'If Target margin is other than StockUOM
            gGridUnlockCell grdUOM, kUOMColConversionFactor, lRow
            gGridUnlockCell grdUOM, kUOMColConversionType, lRow
            gGridUnlockCell grdUOM, kUOMColVolume, lRow
            gGridUnlockCell grdUOM, kUOMColWeight, lRow
            gGridUnlockCell grdUOM, kUOMColUPC, lRow

            If bIsSameMeasureType(sTarget, sStock) = True Then
                gGridUnlockCell grdUOM, kUOMColUseStdConv, lRow
                gGridUnlockCell grdUOM, kUOMColUseForPurchases, lRow
            Else
                gGridLockCell grdUOM, kUOMColUseStdConv, lRow
            End If
        End If

        For lCnt = 1 To grdUOM.MaxRows - 1
            sCntTarget = UCase(Trim$(gsGridReadCell(grdUOM, lCnt, kUOMColTargetUOM)))
            
            If Not sCntTarget = sStock _
                And Not sCntTarget = sSales _
                And Not sCntTarget = sPurch _
                And Not sCntTarget = sPrice Then
                gGridUnlockCell grdUOM, kUOMColConversionFactor, lCnt
                gGridUnlockCell grdUOM, kUOMColConversionType, lCnt
            End If
            
            If gsGridReadCell(grdUOM, lCnt, kUOMColUseStdConv) = "1" Then
                'gGridUpdateCell grdUOM, lCnt, kUOMColConversionFactor, CDec(dGetStandardUOMConv(lCnt))
                gGridLockCell grdUOM, kUOMColConversionFactor, lCnt
                gGridLockCell grdUOM, kUOMColConversionType, lCnt
            End If
        Next
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisableEnable", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetMyValues(sButtonString As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If sButtonString <> kTbSave And sButtonString <> kTbMemo Then
        'Clear unbound controls.
        ValuesRemove ("Type")
        ValuesRemove ("Valuation")
        ClearUnboundCtrls
        CtrlsReset
        lkuMain.SetFocus
        numStdPrice.Value = 0
     Else
        If ddnItemType.ItemData = kItemTypeCatalog Then
            mbValChange = True
            ValuesRemove ("Type")
            ValuesRemove ("Valuation")
        End If
        
        lkuStockUOM.Enabled = False
        
        If ddnItemType.ItemData = kItemTypeBTOKit Then
            lkuSales.Enabled = False
            lkuPurchaseUOM.Enabled = False
            lkuPrice.Enabled = False
        Else
            lkuPrice.Enabled = True
        End If
    End If
    
    ' If the main lookup is populated, then the form state is such (edit mode) that
    ' these controls must not be changed - they can only be changed on new items being added.
    If Not lkuMain.Text = "" Then
        ddnItemType.Enabled = True

        If Not ddnItemType.ItemData = kItemTypeCatalog Then
            ddnItemType.Enabled = False
        End If

        ddnTrackMethod.Enabled = False
        ddnValuation.Enabled = False
     End If
            
    'Shift the focus to Main Tab after any operation.
    If sButtonString <> kTbSave And sButtonString <> kTbMemo Then
        tabMaintainItem.Tab = kTabDesc
    End If

    miHoldSaveTab = Empty
            
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMyValues", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SpecialKitValue(Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim fQty As Double

    fQty = gdGetValidDbl(Trim$(gsGridReadCell(grdCompKits, Row, kKitColQuantity)))
    

    If (fQty - Int(fQty)) <> 0 _
And ((Trim$(gsGridReadCellText(grdCompKits, Row, kKitColType)) = kTrackSerial _
Or Trim$(gsGridReadCellText(grdCompKits, Row, kKitColType)) = kTrackBoth) _
Or (Trim$(gsGridReadCellText(grdCompKits, Row, kKitColDQAllow)) = kUnchecked)) Then
        tabMaintainItem.Tab = kTabKit
        giSotaMsgBox Me, moClass.moSysSession, 160133, _
gsQuoted(Trim$(gsGridReadCellText(grdCompKits, Row, kKitColComponentItem)))
        gGridUpdateCell grdCompKits, Row, kKitColQuantity, 0
        grdCompKits.Action = 0
        mbKitValMiss = True
    Else
        mbKitValMiss = False
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SpecialKitValue", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub RedoUOMGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbRedoUOM = True
    lkuStockUOM_LostFocus
    lkuSales_LostFocus
    lkuPrice_LostFocus
    lkuPurchaseUOM_LostFocus
    mbRedoUOM = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RedoUOMGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub GetMyValues()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ddnValuation.Enabled = False
    ddnTrackMethod.Enabled = False
    TrackMethodRelatedControls ddnTrackMethod.ItemData, moDmItemForm.State
    lkuStockUOM.Enabled = False
    
    If Not ddnItemType.ItemData = kItemTypeCatalog Then
        ddnItemType.Enabled = False
    End If
    
    If ddnItemType.ItemData = kItemTypeBTOKit Then
        chkDropShipFlg.Value = kUnchecked
    End If
    
    If ddnItemType.ItemData = kItemTypeService Then
        chkDropShipFlg.Value = kUnchecked
    End If
    
    EnableControlsFromItemType ddnItemType.ItemData, False
    
    miStat = ddnStatus.ItemData

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetMyValues", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'--------------------------------------------------------------------------------------------
'This procedure adds the value of the kit component in case the Price as Component Total
'option is selected by the user. It accepts the row on which the component item is and
'calculates the value of the quantity required and adds it to the Standard Price.
'--------------------------------------------------------------------------------------------
Public Sub AddPrice(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim fCost As Double

    'Read the value in the quantity column of the row passed
    mfQty = gdGetValidDbl(Trim$(gsGridReadCell(grdCompKits, lRow, kKitColQuantity)))
    
    'Ffind the unit cost of the component Item
    fCost = gvCheckNull(moClass.moAppDB.Lookup _
("StdPrice", "timItem", "ItemID = " & gsQuoted(Trim$(gsGridReadCell(grdCompKits, lRow, kKitColComponentItem))) & _
" AND CompanyID = " & gsQuoted(msCompanyID)), SQL_FLOAT)
    
    'Replace the value of the std price control with the total value
    numStdPrice.Value = numStdPrice.Value + (mfQty * fCost)
    
    'Reset the Quantity counter
    mfQty = 1

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "AddPrice", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub FillHiddenKey()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lCnt        As Long
    Dim lTotRow     As Long
    Dim lKeyVal     As Long
    
    lTotRow = grdUOM.MaxRows - 1
    
    For lCnt = 1 To lTotRow
        lKeyVal = moDmUOMGrid.GetColumnValue(lCnt, "TargetUnitMeasKey")
        gGridUpdateCell grdUOM, lCnt, kUOMColUOMKey, CStr(lKeyVal)
    Next

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "FillHiddenKey", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtSerialNumberIncrement_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSerialNumberIncrement, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
'allow all the special characters
'the range from 48 to 57 there are numbers from 0 to 10 not allowed
'the range from 65 to 90 there are Upper Case Letters from A to Z not allowed
'The range from 98 to 122 there are lower case letters from b to z not allowed
' 120 - "x" ;32 -  blanks
If (KeyAscii > 47 And KeyAscii < 58) _
Or (KeyAscii > 64 And KeyAscii < 91) _
Or (KeyAscii > 97 And KeyAscii <> 120 And KeyAscii < 123) _
Or (KeyAscii = 32) Then KeyAscii = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtSerialNumberIncrement_KeyPress", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtSerialNumberIncrement_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSerialNumberIncrement, True
    #End If
'+++ End Customizer Code Push +++
If Len(Trim(txtSerialNumberIncrement)) <> 0 Then
    If Len(Trim(txtSerialNumberMask)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMmsgSerialMaskReq
        gbSetFocus Me, txtSerialNumberMask
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtSerialNumberIncrement_LostFocus", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function mbCheckSerialMatch() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSerialNumberMask As String
    Dim sSerialNumberIncrement As String
    Dim iPosStart As Integer
    Dim sPos As String
    Dim iPosEnd As Integer
    Dim sMaskChar As String
    Dim sIncrChar As String
    Dim sData As String
    Dim bIncrementFound As Boolean
    Dim bIncrementEnd As Boolean
    
    mbCheckSerialMatch = False
    bIncrementFound = False
    bIncrementEnd = False
    
    sSerialNumberMask = Trim(txtSerialNumberMask)
    sSerialNumberIncrement = Trim(txtSerialNumberIncrement)
    
    ' Check  the  length
    If Len(Trim(txtSerialNumberIncrement)) <> Len(Trim(txtSerialNumberMask)) Then
        giSotaMsgBox Me, moClass.moSysSession, kIMmsgSerialLength
        gbSetFocus Me, txtSerialNumberIncrement
        Exit Function
    Else
        'Check format
        iPosEnd = Len(Trim(txtSerialNumberMask))
        For iPosStart = 1 To iPosEnd
            sMaskChar = Mid(sSerialNumberMask, iPosStart, 1)
            sIncrChar = Mid(sSerialNumberIncrement, iPosStart, 1)
            
            If (sMaskChar <> sIncrChar) And sIncrChar <> kIncrRuleChar Then
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgSerialFormat
                gbSetFocus Me, txtSerialNumberIncrement
                Exit Function
            End If
            'Only numerics may be increment
            If (sMaskChar <> kNumeric And sIncrChar = kIncrRuleChar) Then
                giSotaMsgBox Me, moClass.moSysSession, kIMmsgSerialIncrement
                gbSetFocus Me, txtSerialNumberIncrement
                Exit Function
            End If
            
            If sIncrChar = kIncrRuleChar Then
                bIncrementFound = True
                If bIncrementEnd Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMmsgNonContiguous
                    gbSetFocus Me, txtSerialNumberIncrement
                    Exit Function
                End If
            Else
                If bIncrementFound Then
                    If sIncrChar = kNumeric Or sIncrChar = kAlpha Or sIncrChar = kAlphaNumeric Then
                       bIncrementEnd = True
                    End If
                End If
            End If
        Next
    
    End If

   
    mbCheckSerialMatch = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "mbCheckSerialMatch", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub txtSerialNumberMask_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSerialNumberMask, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
'allow all the special characters
'the range from 48 to 57 there are numbers from 0 to 10 not allowed
'the range from 65 to 90 there are Upper Case Letters from A to Z not allowed
'The range from 98 to 122 there are lower case letters from b to z not allowed
' 120 - "x" ; 94 - "^" ; 32 -  blanks
If (KeyAscii > 47 And KeyAscii < 58) _
Or (KeyAscii > 64 And KeyAscii < 91) _
Or (KeyAscii > 97 And KeyAscii <> 120 And KeyAscii < 123) _
Or (KeyAscii = 94) Or (KeyAscii = 32) Then KeyAscii = 0

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtSerialNumberMask_KeyPress", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtSerialNumberMask_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSerialNumberMask, True
    #End If
'+++ End Customizer Code Push +++


    If Len(Trim(txtSerialNumberMask)) = 0 Then
        txtSerialNumberIncrement.Text = ""
        txtSerialNumberIncrement.Enabled = False
    Else
        txtSerialNumberIncrement.Enabled = True
        gbSetFocus Me, txtSerialNumberIncrement
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtSerialNumberMask_LostFocus", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtShortDesc_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShortDesc, True
    #End If
'+++ End Customizer Code Push +++
    If lkuMain.Text = "" And lkuMain.Enabled = True Then
        txtShortDesc.Text = ""
        lkuMain.SetFocus
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "txtShortDesc_Change", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bCheckDuplicateHidden(lThisKey As Long, lThisRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lCnt As Long
    Dim sUOMID As String
    
    bCheckDuplicateHidden = True

    For lCnt = 1 To grdUOM.MaxRows - 1
        If Not lCnt = lThisRow Then
            If lThisKey = glGetValidLong(Trim$(gsGridReadCell(grdUOM, lCnt, kUOMColUOMKey))) Then
                MsgBox "This value cannot be used as it exists in the database."
                
                sUOMID = gvCheckNull(moClass.moAppDB.Lookup _
("UnitMeasID", "tciUnitMeasure", "UnitMeasKey = " & _
glGetValidLong(Trim$(gsGridReadCell(grdUOM, lThisRow, kUOMColUOMKey)))), SQL_INTEGER)
                
                gGridUpdateCell grdUOM, lThisRow, kUOMColTargetUOM, sUOMID
                Exit Function
            End If
        End If
    Next

    bCheckDuplicateHidden = False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckDuplicateHidden", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bTrueValue(iThisValue As Integer) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
bTrueValue = False
    If iThisValue = kValActual And ddnTrackMethod.ItemData = kTrackNone Then Exit Function
bTrueValue = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bTrueValue", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bLoseFocus() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bLoseFocus = True
    
    If (Me.ActiveControl Is sbrMain _
Or Me.ActiveControl Is tbrMain) Then
        bLoseFocus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoseFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub ClearForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

tabMaintainItem.Tab = kTabDesc
cmdSubstitution.Enabled = True
cmdLandCost.Enabled = True
chkDropShipFlg.Enabled = True
chkAllowPriceOvrd.Enabled = True
chkSubToTradeDisc.Enabled = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "ClearForm", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bCheckEnableLandCost() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

bCheckEnableLandCost = False

If ddnItemType.ItemData <> kItemTypeBTOKit And ddnItemType.ItemData <> kItemTypeAssembledKit Then
    If ddnValuation.ItemData <> kValStd Then
        bCheckEnableLandCost = True
    End If
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckEnableLandCost", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckEnableDropShip() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bCheckEnableDropShip = False

    If (ddnItemType.ItemData <> kItemTypeBTOKit _
And ddnItemType.ItemData <> kItemTypeAssembledKit _
And ddnItemType.ItemData <> kItemTypeService _
And ddnItemType.ItemData <> kItemTypeExpense _
And ddnItemType.ItemData <> kItemTypeCommentOnly _
And ddnItemType.ItemData <> kItemTypeCatalog) Then
        bCheckEnableDropShip = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckEnableDropShip", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckEnableStockUOM() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

bCheckEnableStockUOM = False

If ddnItemType.ItemData <> kItemTypeCommentOnly And moDmItemForm.State <> sotaTB_EDIT Then
    bCheckEnableStockUOM = True
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckEnableStockUOM", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckEnableOtherUOM() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

bCheckEnableOtherUOM = False

If ddnItemType.ItemData <> kItemTypeCommentOnly Then
    If moDmItemForm.State = sotaTB_ADD And Len(Trim(lkuStockUOM.Text)) = 0 Then
        Exit Function
    Else
        bCheckEnableOtherUOM = True
    End If
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckEnableOtherUOM", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckEnableAllowPrOvrd() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

bCheckEnableAllowPrOvrd = False

If ddnItemType.ItemData <> kItemTypeExpense And ddnItemType.ItemData <> kItemTypeCommentOnly Then
    bCheckEnableAllowPrOvrd = True
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckEnableAllowPrOvrd", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bCheckEnableSubjToTrDisc() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

bCheckEnableSubjToTrDisc = False

If ddnItemType.ItemData <> kItemTypeExpense And ddnItemType.ItemData <> kItemTypeCommentOnly Then
    bCheckEnableSubjToTrDisc = True
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckEnableSubjToTrDisc", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub EnableControlsFromItemType(iItemType As Integer, bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If tabMaintainItem.TabVisible(kTabCategories) = True Then
        If tabMaintainItem.TabEnabled(kTabCategories) = True Then
            fraProductCatogories.Enabled = True
            lstAvailProdCatID.Enabled = True
            lstAssignProdCatID.Enabled = True
        End If
    End If
    
    Select Case iItemType
        Case Is = kItemTypeAssembledKit
            EnableAssembledKit bResetCntrls

        Case Is = kItemTypeBTOKit
            EnableBTO bResetCntrls
        
        Case Is = kItemTypeFinishedGood
            EnableFinishedGoods bResetCntrls
        
        Case Is = kItemTypeRawMaterial
            EnableRawMaterial bResetCntrls
        
        Case Is = kItemTypeExpense
            EnableExpenseItem bResetCntrls

            'Clear out the Assigned Product Categories information.
            DeselectAllItems

            'The Product Categories grid should be disabled for Expense Items.
            If tabMaintainItem.TabVisible(kTabCategories) = True Then
                If tabMaintainItem.TabEnabled(kTabCategories) = True Then
                    lstAvailProdCatID.Enabled = False
                    lstAssignProdCatID.Enabled = False
                    fraProductCatogories.Enabled = False
                End If
            End If

        Case Is = kItemTypeService
            EnableServiceItem bResetCntrls
        
        Case Is = kItemTypeMiscItem
            EnableMiscItem bResetCntrls

        Case Is = kItemTypeCommentOnly
            EnableCommentOnlyItem bResetCntrls

            'Clear out the Assigned Product Categories information.
            DeselectAllItems
            
            'The Product Categories grid should be disabled for Comment-Only Items.
            If tabMaintainItem.TabVisible(kTabCategories) = True Then
                If tabMaintainItem.TabEnabled(kTabCategories) = True Then
                    lstAvailProdCatID.Enabled = False
                    lstAssignProdCatID.Enabled = False
                    fraProductCatogories.Enabled = False
                End If
            End If
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableControlsFromItemType", VBRIG_IS_FORM   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableAssembledKit(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    ' The tracking method cannot be enabled unless the form is in the edit state.
    If Not (moDmItemForm.State = kDmStateEdit) Then
        ddnTrackMethod.Enabled = True
    Else
        ddnTrackMethod.Enabled = False
    End If
    
    If chkAllowDecQuantities.Value = kUnchecked Then
        numMandatorySalesMulti.Enabled = True
    End If
    
    ' The serial # increment and mask fields must, and can only be enabled for tracking methods
    ' "both" and "serial".
    TrackMethodRelatedControls ddnTrackMethod.ItemData, moDmItemForm.State
    If (ddnTrackMethod.ItemData = kTrackSerial Or ddnTrackMethod.ItemData = kTrackBoth) Then
        chkAllowDecQuantities.Value = kUnchecked
        With numMandatorySalesMulti
             .Enabled = True
             .Value = 0
        End With
    End If

    chkAllowDecQuantities.Enabled = False
    SetupAdjQtyRoundMeth
    numShelfLife.Enabled = True
    
    lkuDefltWarehouse.Enabled = False

    lkuCommoCode.Enabled = True
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    If Not (moDmItemForm.State = sotaTB_EDIT) Then
        ddnValuation.Enabled = True
    Else
        ddnValuation.Enabled = False
    End If
    
    numStdCost.Enabled = True
'    numReplacementCost.Enabled = False
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
    
    cmdSubstitution.Enabled = True
    cmdLandCost.Enabled = True
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True

    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = True
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = True
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = True
    lkuCommCls.Enabled = True
    numTargetMargin.Enabled = True
    numMinGrossProfitPct.Enabled = True
    numPriceSeq.Enabled = True
    lkuCustPriceGroup.Enabled = True
    numRestockingRates.Enabled = True
    numMinSalesQty.Enabled = True
    
    lkuPuchProdLine.Enabled = True
    lkuPOMatchTolerance.Enabled = True
    If mbIRIsActivated Then numPeriodUsage.Enabled = True
    chkReturnsAllowed.Enabled = True
    chkAllowCostOvrd.Enabled = False
    

    chkDropShipFlg.Enabled = True
    chkAllowPriceOvrd.Enabled = True
    chkSubToTradeDisc.Enabled = True
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = False
        
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = False
    'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True
        
    'Default Values
    If bResetCntrls Then
        chkRcptRequired.Value = kUnchecked
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kChecked
        StkItemSetting
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableAssKit", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableBTO(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    ddnTrackMethod.Enabled = False
    
    ' The serial # increment and mask fields must, and can only be enabled for tracking methods
    ' "both" and "serial".
    TrackMethodRelatedControls ddnTrackMethod.ItemData, moDmItemForm.State
    
    numShelfLife.Enabled = True
    
    lkuDefltWarehouse.Enabled = False
    lkuCommoCode.Enabled = True
    
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    ddnValuation.Enabled = False
    
    numStdCost.Enabled = False
'    numReplacementCost.Enabled = False
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
    
    cmdSubstitution.Enabled = False
    cmdLandCost.Enabled = False
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True
    chkAllowDecQuantities.Enabled = False
    SetupAdjQtyRoundMeth
    
    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = True
    chkEplodeOnInvoice.Enabled = True
    chkPriceAsCompTotal.Enabled = True
    chkAllowRtrnsCustomBTO.Enabled = True
    chkAllowRtrnsPartialBTO.Enabled = True
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = True
    lkuCommCls.Enabled = True
    
    numTargetMargin.Enabled = True
    numMinGrossProfitPct.Enabled = True
    numPriceSeq.Enabled = True
    
    lkuCustPriceGroup.Enabled = True
    
    numRestockingRates.Enabled = True
    numMinSalesQty.Enabled = True
    
    If chkAllowDecQuantities.Value = kUnchecked Then
        numMandatorySalesMulti.Enabled = True
    End If
    
    lkuPuchProdLine.Enabled = False
    lkuPOMatchTolerance.Enabled = False
    
    numPeriodUsage.Enabled = False
    
    chkAllowCostOvrd.Enabled = False
    chkDropShipFlg.Enabled = False
    chkAllowPriceOvrd.Enabled = True
    chkSubToTradeDisc.Enabled = True
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = False
        
    chkReturnsAllowed.Enabled = True
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True

    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = False
        
   'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True
    
    'Default Values
    If bResetCntrls Then
        chkRcptRequired.Value = kUnchecked
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kChecked
        StkItemSetting
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableBTO", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableFinishedGoods(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    If Not (moDmItemForm.State = kDmStateEdit) Then
        ddnTrackMethod.Enabled = True
    Else
        ddnTrackMethod.Enabled = False
    End If
    
    If chkAllowDecQuantities.Value = kUnchecked Then
        numMandatorySalesMulti.Enabled = True
    End If
    
    ' The serial # increment and mask fields must, and can only be enabled for tracking methods
    ' "both" and "serial".
    TrackMethodRelatedControls ddnTrackMethod.ItemData, moDmItemForm.State
    If (ddnTrackMethod.ItemData = kTrackSerial Or ddnTrackMethod.ItemData = kTrackBoth) Then
        If bResetCntrls Then
            chkAllowDecQuantities.Value = kUnchecked
            With numMandatorySalesMulti
                .Enabled = True
                .Value = 0
            End With
        End If
        chkAllowDecQuantities.Enabled = False
        SetupAdjQtyRoundMeth
    Else
        chkAllowDecQuantities.Enabled = True
        SetupAdjQtyRoundMeth
    End If
    
    numShelfLife.Enabled = True
    
    If miModule = kModuleIM And mbMFActivated Then
        lkuDefltWarehouse.Enabled = True
    Else
        lkuDefltWarehouse.Enabled = False
    End If
    
    lkuCommoCode.Enabled = True
    
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    If Not (moDmItemForm.State = sotaTB_EDIT) Then
        ddnValuation.Enabled = True
    Else
        ddnValuation.Enabled = False
    End If
    
    numStdCost.Enabled = True
    numStdPrice.Enabled = True
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = True
    End If
    
    cmdSubstitution.Enabled = True
    cmdLandCost.Enabled = bCheckEnableLandCost
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True
    
    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = False
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = False
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
        
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = True
    lkuCommCls.Enabled = True
    
    numTargetMargin.Enabled = True
    numMinGrossProfitPct.Enabled = True
    numPriceSeq.Enabled = True
    
    lkuCustPriceGroup.Enabled = True
    
    numRestockingRates.Enabled = True
    numMinSalesQty.Enabled = True
    
    lkuPuchProdLine.Enabled = True
    
    If (moOptions.PO("MatchInvcToPO") = 1 _
Or moOptions.PO("MatchRcvrToPO") = 1) Then
        lkuPOMatchTolerance.Enabled = True
    Else
        lkuPOMatchTolerance.Enabled = False
    End If
    
    If mbIRIsActivated Then numPeriodUsage.Enabled = True
    
    chkReturnsAllowed.Enabled = True
    chkAllowCostOvrd.Enabled = False
    
    chkDropShipFlg.Enabled = True
    chkAllowPriceOvrd.Enabled = True
    chkSubToTradeDisc.Enabled = True
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = False
        
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = False
        
    'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True
    
    'Default Values
    If bResetCntrls Then
        chkRcptRequired.Value = kChecked
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kUnchecked
        StkItemSetting
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableFinishedGoods", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableRawMaterial(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    If Not (moDmItemForm.State = kDmStateEdit) Then
        ddnTrackMethod.Enabled = True
    Else
        ddnTrackMethod.Enabled = False
    End If
    
    numShelfLife.Enabled = True
    
    If miModule = kModuleIM And mbMFActivated Then
        lkuDefltWarehouse.Enabled = True
    Else
        lkuDefltWarehouse.Enabled = False
    End If
    
    lkuCommoCode.Enabled = True
    
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    If Not (moDmItemForm.State = kDmStateEdit) Then
        ddnValuation.Enabled = True
    Else
        ddnValuation.Enabled = False
    End If
    
    numStdCost.Enabled = True
    numStdPrice.Enabled = True
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
    
    cmdSubstitution.Enabled = True
    cmdLandCost.Enabled = bCheckEnableLandCost
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True
    SetupAdjQtyRoundMeth
    
    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = False
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = False
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = True
    lkuCommCls.Enabled = True
    
    numTargetMargin.Enabled = True
    numMinGrossProfitPct.Enabled = True
    numPriceSeq.Enabled = True
    
    lkuCustPriceGroup.Enabled = True
    
    numRestockingRates.Enabled = True
    numMinSalesQty.Enabled = True

    If chkAllowDecQuantities.Value = kUnchecked Then
        numMandatorySalesMulti.Enabled = True
    End If
    
    lkuPuchProdLine.Enabled = True
    
    If (moOptions.PO("MatchInvcToPO") = 1 _
Or moOptions.PO("MatchRcvrToPO") = 1) Then
        lkuPOMatchTolerance.Enabled = True
    Else
        lkuPOMatchTolerance.Enabled = False
    End If
    
    If mbIRIsActivated Then numPeriodUsage.Enabled = True
    
    chkReturnsAllowed.Enabled = True
    chkAllowCostOvrd.Enabled = False
    
    TrackMethodRelatedControls ddnTrackMethod.ItemData, moDmItemForm.State
    If (ddnTrackMethod.ItemData = kTrackSerial Or ddnTrackMethod.ItemData = kTrackBoth) Then
            If bResetCntrls Then
            chkAllowDecQuantities.Value = kUnchecked
            With numMandatorySalesMulti
                .Enabled = True
                .Value = 0
            End With
        End If
        
        chkAllowDecQuantities.Enabled = False
    Else
        chkAllowDecQuantities.Enabled = True
    End If
    
    chkDropShipFlg.Enabled = True
    chkAllowPriceOvrd.Enabled = True
    chkSubToTradeDisc.Enabled = True
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = False
            
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = False
        
    'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True

    'Default Values
    If bResetCntrls Then
        chkRcptRequired.Value = kChecked
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kUnchecked
        StkItemSetting
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableRawMaterial", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableExpenseItem(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    ddnTrackMethod.Enabled = False
    
    numShelfLife.Enabled = True
    
    lkuDefltWarehouse.Enabled = False
    lkuCommoCode.Enabled = False
    
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    ddnValuation.Enabled = False
    
    numStdCost.Enabled = True
    numStdPrice.Enabled = True
'    numReplacementCost.Enabled = False
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
    
    cmdSubstitution.Enabled = False
    cmdLandCost.Enabled = True
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True
    chkAllowDecQuantities.Enabled = True
    SetupAdjQtyRoundMeth

    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = False
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = False
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = False
    lkuCommCls.Enabled = True
    
    numTargetMargin.Enabled = False
    numMinGrossProfitPct.Enabled = False
    numPriceSeq.Enabled = True
    
    lkuCustPriceGroup.Enabled = False
    
    numRestockingRates.Enabled = False
    numMinSalesQty.Enabled = False
    numMandatorySalesMulti.Enabled = False
    
    lkuPuchProdLine.Enabled = False
    
    If moOptions.PO("MatchInvcToPO") = 1 Then
        lkuPOMatchTolerance.Enabled = True
    Else
        lkuPOMatchTolerance.Enabled = False
    End If
    
    numPeriodUsage.Enabled = False
    
    chkReturnsAllowed.Enabled = False
    chkAllowCostOvrd.Enabled = True
    chkDropShipFlg.Enabled = True
    chkAllowPriceOvrd.Enabled = False
    chkSubToTradeDisc.Enabled = False
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = True
            
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = True
    
    'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True
    'Default Values
    If bResetCntrls Then
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kUnchecked
        NonStkItemSetting
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableExpenseItem", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableServiceItem(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    ddnTrackMethod.Enabled = False
    
    numShelfLife.Enabled = True
    
    lkuDefltWarehouse.Enabled = False
    lkuCommoCode.Enabled = False
    
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    ddnValuation.Enabled = False
    
    numStdCost.Enabled = True
    numStdPrice.Enabled = True
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
    
    cmdSubstitution.Enabled = False
    cmdLandCost.Enabled = False
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True
    SetupAdjQtyRoundMeth
    
    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = False
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = False
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
    
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = False
    lkuCommCls.Enabled = True
    
    numTargetMargin.Enabled = True
    numMinGrossProfitPct.Enabled = True
    numPriceSeq.Enabled = True
    
    lkuCustPriceGroup.Enabled = False
    
    numRestockingRates.Enabled = False
    numMinSalesQty.Enabled = True
    
    If chkAllowDecQuantities.Value = kUnchecked Then
        numMandatorySalesMulti.Enabled = True
    End If
    
    lkuPuchProdLine.Enabled = False
    lkuPOMatchTolerance.Enabled = False
    
    numPeriodUsage.Enabled = False
    
    chkReturnsAllowed.Enabled = True
    chkAllowCostOvrd.Enabled = True
    chkAllowDecQuantities.Enabled = True
    chkDropShipFlg.Enabled = False
    chkAllowPriceOvrd.Enabled = True
    chkSubToTradeDisc.Enabled = True
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = False
        
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = True
    
    'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True

    'Default Values
    If bResetCntrls Then
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kUnchecked
        NonStkItemSetting
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableServiceItem", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableMiscItem(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    ddnTrackMethod.Enabled = False
    
    numShelfLife.Enabled = True
    
    If miModule = kModuleIM And mbMFActivated Then
        lkuDefltWarehouse.Enabled = True
    Else
        lkuDefltWarehouse.Enabled = False
    End If
    
    lkuCommoCode.Enabled = False
    
    ddnSalesTaxClass.Enabled = True
    
    lkuFreightClass.Enabled = True
    
    txtDateEstb.Enabled = False
    
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    
    ddnValuation.Enabled = False
    
    numStdCost.Enabled = True
    numStdPrice.Enabled = True
    
    ddnProvider.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
    
    cmdSubstitution.Enabled = False
    cmdLandCost.Enabled = True
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = True
    SetupAdjQtyRoundMeth
    
    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = False
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = False
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = False
    lkuCommCls.Enabled = True
    
    numTargetMargin.Enabled = True
    numMinGrossProfitPct.Enabled = True
    numPriceSeq.Enabled = True
    
    lkuCustPriceGroup.Enabled = False
    
    numRestockingRates.Enabled = False
    numMinSalesQty.Enabled = True
    
    If chkAllowDecQuantities.Value = kUnchecked Then
        numMandatorySalesMulti.Enabled = True
    End If
    
    lkuPuchProdLine.Enabled = False
    
    If moOptions.PO("MatchInvcToPO") = 1 Then
        lkuPOMatchTolerance.Enabled = True
    Else
        lkuPOMatchTolerance.Enabled = False
    End If
    
    numPeriodUsage.Enabled = False
    
    chkReturnsAllowed.Enabled = True
    chkAllowCostOvrd.Enabled = True
    chkAllowDecQuantities.Enabled = True
    chkDropShipFlg.Enabled = True
    chkAllowPriceOvrd.Enabled = True
    chkSubToTradeDisc.Enabled = True
    chkInternalDelvrRequired.Enabled = True
    chkRcptRequired.Enabled = True
        
    chkInclOnPackList.Enabled = True
    numDfltSaleQty.Enabled = True
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = True
    
   'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = True
    'Default Values
    If bResetCntrls Then
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kUnchecked
        NonStkItemSetting
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableMiscItem", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableCommentOnlyItem(bResetCntrls As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Main Tab
    ddnStatus.Enabled = True
    lkuItemClass.Enabled = True
    
    ddnTrackMethod.Enabled = False
    ddnValuation.Enabled = False
    ddnProvider.Enabled = False
    ddnSalesTaxClass.Enabled = False
    
    lkuDefltWarehouse.Enabled = False
    lkuCommoCode.Enabled = False
    lkuFreightClass.Enabled = False
    
    txtDateEstb.Enabled = False
    numShelfLife.Enabled = False
    numStdCost.Enabled = False
    numStdPrice.Enabled = False
        
    chkInternal.Enabled = True
    chkSeasonal.Enabled = True
    chkHazMat.Enabled = True
    
    If ddnProvider.ItemData = 0 Then
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
        numNOfDays.Enabled = False
    Else
        numNOfDays.Enabled = True
        If bResetCntrls Then
            numNOfDays.Value = 0
        End If
    End If
        
    cmdSubstitution.Enabled = False
    cmdLandCost.Enabled = False
    
    'UOM Tab
    tabMaintainItem.TabEnabled(kTabUOM) = False
    chkAllowDecQuantities.Enabled = False
    SetupAdjQtyRoundMeth
    
    'Kits Tab
    tabMaintainItem.TabEnabled(kTabKit) = False
    
    chkEplodeOnInvoice.Enabled = False
    chkPriceAsCompTotal.Enabled = False
    chkAllowRtrnsCustomBTO.Enabled = False
    chkAllowRtrnsPartialBTO.Enabled = False
    
    'Sales & Purchasing Tab
    lkuProdLine.Enabled = False
    lkuCommCls.Enabled = False

    chkInclOnPackList.Enabled = False
    
    numDfltSaleQty.Enabled = False
    numTargetMargin.Enabled = False
    numMinGrossProfitPct.Enabled = False
    numPriceSeq.Enabled = False
    numRestockingRates.Enabled = False
    numMinSalesQty.Enabled = False
    numPeriodUsage.Enabled = False
    
    lkuCustPriceGroup.Enabled = False
    lkuPuchProdLine.Enabled = False
    lkuPOMatchTolerance.Enabled = False
    lkuCommCls.Enabled = False
    
    chkReturnsAllowed.Enabled = False
    chkAllowCostOvrd.Enabled = False
    chkDropShipFlg.Enabled = False
    chkAllowPriceOvrd.Enabled = False
    chkSubToTradeDisc.Enabled = False
    chkInternalDelvrRequired.Enabled = False
    chkRcptRequired.Enabled = False
    chkAllowDecQuantities.Enabled = False
    chkInclOnPackList.Enabled = False
    
    'GL Accounts Tab
    tabMaintainItem.TabEnabled(kTabGLAcct) = False
    
    'Categories Tab
    tabMaintainItem.TabEnabled(kTabCategories) = False
    'Default Values
    If bResetCntrls Then
        chkDropShipFlg.Value = kUnchecked
        chkPriceAsCompTotal.Value = kUnchecked
        NonStkItemSetting
    End If


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableCommentOnlyItem", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub EnableUOMTabControls(iItemType As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case iItemType
        Case kItemTypeAssembledKit
            lkuPurchaseUOM.Enabled = True
            lkuSales.Enabled = True
            lkuPrice.Enabled = True
            
        Case kItemTypeBTOKit
            lkuPurchaseUOM.Enabled = False
            lkuSales.Enabled = False
            lkuPrice.Enabled = False
    
        Case kItemTypeFinishedGood
            lkuPurchaseUOM.Enabled = True
            lkuSales.Enabled = True
            lkuPrice.Enabled = True
        
        Case kItemTypeRawMaterial
            lkuPurchaseUOM.Enabled = True
            lkuSales.Enabled = True
            lkuPrice.Enabled = True
    
        Case kItemTypeExpense
            lkuPurchaseUOM.Enabled = True
            lkuSales.Enabled = False
            lkuPrice.Enabled = False
            
        Case kItemTypeService
            lkuPurchaseUOM.Enabled = False
            lkuSales.Enabled = True
            lkuPrice.Enabled = True
            
        Case kItemTypeCommentOnly
            lkuPurchaseUOM.Enabled = False
            lkuSales.Enabled = False
            lkuPrice.Enabled = False
            
        Case kItemTypeMiscItem
            lkuPurchaseUOM.Enabled = True
            lkuSales.Enabled = True
            lkuPrice.Enabled = True
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "EnableUOMTabControls", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function DMBeforeInsert(oDm As Object)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
    
    Dim sSQL As String
    Dim sErr As String
    Dim lErr As Long

    If oDm Is moDmItemForm Then
        If Not bPreSave() Then
            DMBeforeInsert = False
            moDmItemForm.CancelAction
            Exit Function
        End If
    End If

    DMBeforeInsert = True

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    sErr = Err.Description
    lErr = Err
    moDmItemForm.CancelAction
    MyErrMsg moClass, sErr, lErr, sMyName, "DMBeforeInsert"
    gClearSotaErr

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub DMGridAppend(oDm As Object, lRow As Long)
    If oDm Is moDmItemImage Then
        'There is a problem with this when the row is appended by selecting a floating
        'navigator.  If this gets fixed, the overriding of the row should be changed.
        lRow = grdItemImages.Row
        
        'The Request date is originally made into a char column to accomodate the loading
        'of data from the temp table.  When creating a new row, default it to date type.
        LoadLineDefaultsForNewOrders (lRow)
    End If
End Sub

Private Sub LoadLineDefaultsForNewOrders(lRow As Long)
    Dim lImageNo As String
    
    'This routine loads the required line defaults.
    'Both the visible and hidden grids must have the same values defaulted.

    'gGridUpdateCell grdItemImages, lRow, kColTOLQtyOrd, 1
    If lRow > 1 Then
        lImageNo = gsGridReadCell(grdItemImages, lRow - 1, kItemIColImageNo) + 1
    Else
        lImageNo = CStr(lRow)
    End If
    'DoEvents
    gGridUpdateCell grdItemImages, lRow, kItemIColImageNo, lImageNo
    gGridUpdateCell grdItemImages, lRow, kItemIColImageKey, glGetNextSurrogateKey(moClass.moAppDB, "timItemImage")
End Sub


Public Function bSetFocus(oControl As Object) As Boolean
'+++ VB/Rig Skip +++

On Error GoTo ExpectedErrorRoutine

    If Not (oControl Is Nothing) Then
        
        If oControl.Enabled = True Then
            
            If oControl.Visible = True Then
                
                On Error Resume Next
                oControl.SetFocus
                On Error GoTo ExpectedErrorRoutine
                
            End If
        
        End If
    
    End If
    
    bSetFocus = True

ExpectedErrorRoutine:


End Function

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Private Sub webItemImage_NavigateComplete2(ByVal pDisp As Object, URL As Variant)
    SetHourglass False
End Sub

Private Sub DeleteExtraRow()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'If the user typed into the ComponentID field, a new row would have been created.
    'This procedure is create so that if two (non-data) rows are in the grid, it will
    'delete the last row.

    If grdCompKits.MaxRows - grdCompKits.DataRowCnt > 1 Then
        gGridDeleteRow grdCompKits, grdCompKits.MaxRows
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteExtraRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function bCheckKitParentAllowReturnAndAllowReturnPartialBTO() As Boolean
    '+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rs As DASRecordSet
    Dim bNotAllowReturn As Boolean
    Dim lCharPosition As Long
    Dim lLastCharPostion As Long
    Dim sErrStringLastPart As String
    Dim sErrString As String
    Dim sItemID As String

    bNotAllowReturn = False
    bCheckKitParentAllowReturnAndAllowReturnPartialBTO = False
    
    sSQL = "SELECT I.ItemID "
    sSQL = sSQL & "FROM timItem I WITH (NOLOCK) JOIN timKit K WITH (NOLOCK) "
    sSQL = sSQL & "ON I.ItemKey = K.KitItemKey "
    sSQL = sSQL & "WHERE I.CompanyID = " & gsQuoted(msCompanyID) & " AND "
    sSQL = sSQL & "I.AllowRtrns <> 0 And K.AllowRtrnsPartialBTO <> 1 AND "
    sSQL = sSQL & "I.ItemKey IN (SELECT KitItemKey FROM timKitCompList "
    sSQL = sSQL & "WHERE CompItemKey = " & Format$(mlItemKey) & ")"
      
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
       
    While Not rs.IsEOF
        sItemID = Trim$(gsGetValidStr(rs.Field("ItemID")))
        
        If Len(sItemID) > 0 Then
            If Len(Trim$(sErrString)) > 0 Then
                sErrString = sErrString & ","
            End If
            
            sErrString = sErrString & Space$(1) & sItemID
            bNotAllowReturn = True
        End If
        
        rs.MoveNext
    Wend
    
    'Close the recordset opened above.
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    'If one or more component not Allow Return
    If bNotAllowReturn Then
        'Find the comma char
        lCharPosition = InStr(sErrString, ",")

        'If the comma char is found
        While lCharPosition <> 0
            'Again search for the last comma char
            lLastCharPostion = lCharPosition
            lCharPosition = InStr(lCharPosition + 1, sErrString, ",")
        Wend
        
        If lLastCharPostion <> 0 Then
            'Save the string after the last comma
            sErrStringLastPart = Mid(sErrString, lLastCharPostion + 1)
            sErrString = Left$(sErrString, lLastCharPostion)
            sErrString = sErrString & " and " & sErrStringLastPart
        End If
            
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMCompReturnAllow, sErrString
    Else
        bCheckKitParentAllowReturnAndAllowReturnPartialBTO = True
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckKitParentAllowReturnAndAllowReturnPartialBTO", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function

Private Function bCheckAllowReturnStatusForAllComponent(oControl As Object) As Boolean
    '+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rs As DASRecordSet
    Dim bNotAllowReturn As Boolean
    Dim sItemID As String
    Dim sCompItemIDs As String
    Dim lCharPosition As Long
    Dim sErrStringLastPart As String
    Dim lLastCharPostion As Long
    Dim lDataRow As Long
    Dim lRowNum As Long
    Dim sErrString As String
    Dim sGridItemID As String
    
    bNotAllowReturn = False
    bCheckAllowReturnStatusForAllComponent = False
    
    lDataRow = grdCompKits.DataRowCnt
    If lDataRow > 0 Then
        For lRowNum = 1 To lDataRow
            sGridItemID = Trim$(gsGridReadCellText(grdCompKits, lRowNum, kKitColComponentItem))
            If Len(sGridItemID) > 0 Then
                If Len(Trim$(sCompItemIDs)) > 0 Then
                    sCompItemIDs = sCompItemIDs & ","
                End If
                sCompItemIDs = sCompItemIDs & gsQuoted(sGridItemID)
            End If
        Next
    
        sSQL = " SELECT I.ItemID FROM timItem I WITH (NOLOCK) "
        sSQL = sSQL & "WHERE I.ItemID in (" & sCompItemIDs & ")" & Space$(1)
        sSQL = sSQL & "AND I.CompanyID = " & gsQuoted(msCompanyID) & Space$(1)
        sSQL = sSQL & "AND I.AllowRtrns = 0"
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                      
        While Not rs.IsEOF
            sItemID = Trim$(gsGetValidStr(rs.Field("ItemID")))
            If Len(sItemID) > 0 Then
                 If Len(Trim$(sErrString)) > 0 Then
                     sErrString = sErrString & ","
                 End If
                 
                sErrString = sErrString & Space$(1) & sItemID
                bNotAllowReturn = True
            End If
        
            rs.MoveNext
        Wend
    
        'Close the recordset opened above.
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    End If
    
    'If one or more component not Allow Return
    If bNotAllowReturn Then
        'Find the comma char
       lCharPosition = InStr(sErrString, ",")

        'If the comma char is found
        While lCharPosition <> 0
            'Again search for the last comma char
            lLastCharPostion = lCharPosition
            lCharPosition = InStr(lCharPosition + 1, sErrString, ",")
        Wend
        
        If lLastCharPostion <> 0 Then
            'add word and to the string after the last comma
            sErrStringLastPart = Mid(sErrString, lLastCharPostion + 1)
            sErrString = Left$(sErrString, lLastCharPostion)
            sErrString = sErrString & " and " & sErrStringLastPart
        End If
        
        'Display the appropriate message based on the control which invoked the message.
        Select Case oControl.Name
            Case chkAllowRtrnsPartialBTO.Name
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMPartialBTOKit, sErrString
            Case chkReturnsAllowed.Name
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMKitReturnAllow, sErrString
            Case grdCompKits.Name
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidCompDueToRetSetting
        End Select
        
    Else
        bCheckAllowReturnStatusForAllComponent = True
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "bCheckAllowReturnStatusForAllComponent", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function

Private Sub SetupFixedAssetTemplates()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'**********************************************************************************
'FAS Integration
'This routine sets up the controls used for integration with Fixed Asset Accounting
'**********************************************************************************
    
    Dim sSQL As String
    Dim rs As Object
    Dim sFASCompany As String
    Dim sFASDatabase As String
    Dim oFASApp As FASLink.Application
    Dim oTemplates As Collection
    Dim vTemplateName As Variant

    'Get AP Options for FAS Integration
     sSQL = "SELECT IntegrateWithFAS, FASCompany, FASDatabase FROM tapOptions WHERE CompanyID = " & gsQuoted(msCompanyID)
     Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
     
     If Not rs.IsEOF Then
        mbIntegrateWithFAS = gbGetValidBoolean(rs.Field("IntegrateWithFAS"))
        sFASDatabase = gsGetValidStr(rs.Field("FASDatabase"))
        sFASCompany = gsGetValidStr(rs.Field("FASCompany"))
    End If
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
          
    'Get the template info from the FAS system
    If mbIntegrateWithFAS Then
        
        'Initialize the interface to the FAS system
        Set oFASApp = New FASLink.Application
        
        If oFASApp.IsAvailable() Then
            If (oFASApp.Initialize(sFASDatabase, sFASCompany)) Then
         
                'Populate the list of templates
                Set oTemplates = oFASApp.Templates
                
                If Not (oTemplates Is Nothing) Then
                    For Each vTemplateName In oTemplates
                        ddnFixedAssetTemplate.AddItem CStr(vTemplateName)
                    Next
                End If
                           
                oFASApp.Terminate
                
                'Set control properties based on FAS Integration properties
                ddnFixedAssetTemplate.Enabled = True
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMFASInitError
            End If
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMFASAPIUnavailable
        End If
        
        If Not oFASApp Is Nothing Then
            Set oFASApp = Nothing
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupFixedAssetTemplates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub





Private Sub grdCompKits_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
    moGMKitComp.Grid_EditMode Col, Row, Mode, ChangeMade
End Sub
 Private Sub grdItemImages_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
    moGMItemImage.Grid_ColWidthChange Col1
End Sub
 Private Sub grdItemImages_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    moGMItemImage.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
End Sub

Private Sub m_CopyMgr_ManualChanges(Cancel As Boolean, ErrNo As Long, ErrData As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: The Copy Manager has requested the module to make any changes necessary
'       for the copy to be successful. Use logic already established in this module
'*****************************************************************************

    Dim ItemID                  As String
    Dim CategoriesQuestion      As CheckBoxConstants
    Dim ImagesQuestion          As CheckBoxConstants
    Dim lRow                    As Long
    
    ' Get the entered item id from the Copy Manager
    ItemID = m_CopyMgr.ID(0)
    
    ' Get the answers to the Copy questions
    CategoriesQuestion = m_CopyMgr.Value(1)
    ImagesQuestion = m_CopyMgr.Value(2)

    ' Perform surrogate key logic here
    mlItemKey = 0
    moDmItemForm.SetColumnValue "ItemKey", 0
    moDmItemDescForm.SetColumnValue "Itemkey", 0

    ' Perform exclusions per design
    
    ' Perform assignments per design
    txtDateEstb.Text = Format(mdDate, gsGetLocalVBDateMask())
    
    ' Perform exclusion questions
    
    If (CategoriesQuestion = vbUnchecked) Then
        lstAssignProdCatID.Clear
    End If
    
    If (ImagesQuestion = vbUnchecked) Then
        If (grdItemImages.DataRowCnt > 0) Then
            For lRow = grdItemImages.DataRowCnt To 1 Step -1
                gGridDeleteRow grdItemImages, lRow
            Next
            'Clear the DMobject for itemimage
            moDmItemImage.Clear
        End If
    End If
    
    ' Perform misc here
    On Error Resume Next
    ClearDMRowLoaded moDmItemForm
    ClearDMRowLoaded moDmItemDescForm
    ClearDMRowLoaded moDmKitForm
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
    
    mbAssignedProdCatsSaveIsNeeded = True

    moDmItemForm.SetColumnValue "UpdateCounter", 0

    moDmUOMGrid.SetNew
    
    moDmKitCompGrid.SetNew
    
    If (mbECIsActivated Or mbSOIsActivated) Then
        moDmItemImage.SetNew
    End If
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "m_CopyMgr_ManualChanges", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub m_CopyMgr_ValidateNewID(Cancel As Boolean, ErrNo As Long, ErrData As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: The Copy Manager has requested the item to be validated. So, reuse
'       logic already established in this module
'*****************************************************************************

    Dim ItemID  As String
    
    ' Get the entered item id from the Copy Manager
    ItemID = m_CopyMgr.ID(0)
    
    ' Validate the entered item, must not be on file
    If (bIsValidItemID(ItemID, ErrNo, ErrData) = False) Then
        ' Set the cancel flag if the item id was invalid
        Cancel = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "m_CopyMgr_ValidateNewID", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ClearDMRowLoaded(ByVal dm As clsDmForm)
    Dim oChild As Object
    For Each oChild In dm.Child
        If (TypeName(oChild) = "clsDmForm") Then
            oChild.RowLoaded = False
            If (oChild.Child.Count > 0) Then
                ClearDMRowLoaded oChild
            End If
        End If
    Next
End Sub

Public Sub OfficeInitialization()
'+++ VB/Rig Skip +++
On Error Resume Next
'*************************************************************************
'      Desc:  The Office class has been initialized elsewhere and has
'             already received this form�s reference. Therefore,
'             this method is additive if the client task wishes to add
'             clsDMForm and/or clsDMGrid references.
'     Parms:  N/A
'   Returns:  N/A
'************************************************************************
    With tbrMain.Office
        .AddFormObject moDmItemForm, "Item"
        .AddFormObject moDmItemDescForm, "ItemDesc"
        .AddFormObject moDmKitForm, "Kit"
        .AddGridObject moDmUOMGrid, "UOM"
        .AddGridObject moDmKitCompGrid, "Components"
        .AddGridObject moDmItemImage, "Images"
    End With

    Err.Clear
End Sub

Private Function dGetStandardUOMConv(lRow As Long) As Double
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Const kGetStandardFactor As Integer = -1
    Dim strUOMID As String
    Dim lUOMKey As Long
    Dim lStockUOMKey As Long
    Dim sSQL As String
    Dim rs As DASRecordSet
        
    'Try to get the UOM key from the grid - if not, look it up from the database using the id
    lUOMKey = glGetValidLong(moDmUOMGrid.GetColumnValue(lRow, "TargetUnitMeasKey"))
    If lUOMKey = 0 Then
        strUOMID = Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM))
        lUOMKey = gvCheckNull(moClass.moAppDB.Lookup("UnitMeasKey", "tciUnitMeasure WITH (NOLOCK)", "UnitMeasID = " & gsQuoted(strUOMID) & " AND CompanyId = " & gsQuoted(msCompanyID)), SQL_INTEGER)
    End If
    
    lStockUOMKey = lkuStockUOM.KeyValue
    
    'If both UOM keys are valid and not equal - lookup up the factor - otherwise just return 1
    If lStockUOMKey > 0 And lUOMKey > 0 And lStockUOMKey <> lUOMKey Then
        sSQL = "SELECT dbo.fnIMItemUOMConvFactor(" & kGetStandardFactor & ", " & lUOMKey & ", " & lkuStockUOM.KeyValue & ")"
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEOF Then
            dGetStandardUOMConv = rs.Field(0)
            rs.Close
        End If
    
        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
    
        If dGetStandardUOMConv <= 0 Then
            dGetStandardUOMConv = 1
        End If
    Else
        dGetStandardUOMConv = 1
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "dGetStandardUOMConv", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetConvFactorDBValue(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'If UseStd is checked - set the factor to 0 in the DB
    'Otherwise set to what the user entered.
    If giGetValidInt(gsGridReadCell(grdUOM, lRow, kUOMColUseStdConv)) = kChecked Then
        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactorDBValue, CDec(0)
    Else
        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactorDBValue, CDec(gsGridReadCell(grdUOM, lRow, kUOMColConversionFactor))
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "SetConvFactorDBValue", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetUOMDefaultValues(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
    'Set this so we don't ask for verification on checking UseStd
    mbUOMChangeByEvent = True
    
    'New UOM - set the defaults for Conversion Factor and Use Std Conversion
    'If same type and not Serialized - Use Std is checked and conversion factor is standard.
    'Otherwise unchecked and 1
    If bIsSameMeasureType(lkuStockUOM.Text, Trim$(gsGridReadCellText(grdUOM, lRow, kUOMColTargetUOM))) And _
        (ddnTrackMethod.ItemData <> kTrackSerial And ddnTrackMethod.ItemData <> kTrackBoth) Then
        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, CDec(dGetStandardUOMConv(lRow))

        gGridUpdateCellText grdUOM, lRow, kUOMColUseStdConv, CDec(kChecked)
        mbUseStdConvOld = False
    Else
        gGridUpdateCell grdUOM, lRow, kUOMColConversionFactor, CDec(1)
        gGridUpdateCellText grdUOM, lRow, kUOMColUseStdConv, CDec(kUnchecked)
        mbUseStdConvOld = True
    End If

    mbUOMChangeByEvent = False
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "SetUOMDefaultValues", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub SetIsSmallestUOM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lRow As Long
Dim iConvType As Integer
Dim dConvFactor As Double
Dim dMinConvFactor As Double
Dim iIsSmallestUOM As Integer
Dim bIsSmallestUOMSet As Boolean
Dim strStockUOM As String

    'Do not set for non-inventory item types
    If ddnItemType.ItemData = kItemTypeMiscItem Or ddnItemType.ItemData = kItemTypeService Or ddnItemType.ItemData = kItemTypeExpense Or ddnItemType.ItemData = kItemTypeCommentOnly Then
        Exit Sub
    End If
    
    bIsSmallestUOMSet = False
    
    'Start with the stock uom as the smallest UOM
    dMinConvFactor = 1

    'If this is a BTO or serailized item - all UOMs will be the same - so all will be the smallest
    'just get the smallest for other types
    If ddnItemType.ItemData <> kItemTypeBTOKit And ddnTrackMethod.ItemData <> kTrackSerial And ddnTrackMethod.ItemData <> kTrackBoth Then
        For lRow = 1 To grdUOM.DataRowCnt
            'Get the conversion factor and type for this row
            dConvFactor = gdGetValidDbl(gsGridReadCellText(grdUOM, lRow, kUOMColConversionFactor))
            iConvType = giGetValidInt(gsGridReadCellText(grdUOM, lRow, kUOMColConversionTypeInt))
            
            'If the conversion factor is 0, skip this row - it will fail the save validation later.
            If dConvFactor <> 0 Then
                If iConvType = 2 Then 'Units Per Stock - so we need to invert the number in the grid
                    dConvFactor = 1 / dConvFactor
                End If
                
                If dConvFactor < dMinConvFactor Then
                    dMinConvFactor = dConvFactor
                End If
            End If
        Next lRow
    End If
    
    For lRow = 1 To grdUOM.DataRowCnt
        'For BTO or serialized, all will get set to true, for all other types, if Factor = Smallest, set to true
        'This is since Serialized items can not have UOM factors other than 1:1, BTO can only have one UOM record
        If ddnItemType.ItemData <> kItemTypeBTOKit And ddnTrackMethod.ItemData <> kTrackSerial And ddnTrackMethod.ItemData <> kTrackBoth Then
            'Get the conversion factor and type for this row
            dConvFactor = gdGetValidDbl(gsGridReadCellText(grdUOM, lRow, kUOMColConversionFactor))
            iConvType = giGetValidInt(gsGridReadCellText(grdUOM, lRow, kUOMColConversionTypeInt))
        
            'If the conversion factor is 0, skip this row - it will fail the save validation later.
            If dConvFactor <> 0 Then
                If iConvType = 2 Then 'Units Per Stock - so we need to invert the number in the grid
                    dConvFactor = 1 / dConvFactor
                End If
            
                If dConvFactor = dMinConvFactor Then
                    iIsSmallestUOM = 1
                    bIsSmallestUOMSet = True
                Else
                    iIsSmallestUOM = 0
                End If
            Else
                iIsSmallestUOM = 0
            End If
        Else
            iIsSmallestUOM = 1
            bIsSmallestUOMSet = True
        End If
            
        If giGetValidInt(gsGridReadCellText(grdUOM, lRow, kUOMColIsSmallestUOM)) <> iIsSmallestUOM Then
            gGridUpdateCell grdUOM, lRow, kUOMColIsSmallestUOM, CStr(iIsSmallestUOM)
            moDmUOMGrid.SetRowDirty lRow
        End If
    Next lRow
            
    'If we did not find a smallest uom - set stock to smallest
    If Not bIsSmallestUOMSet Then
        strStockUOM = Trim$(lkuStockUOM.Text)
        
        For lRow = 1 To grdUOM.DataRowCnt
            If strStockUOM = Trim$(gsGridReadCell(grdUOM, lRow, kUOMColTargetUOM)) Then
                gGridUpdateCell grdUOM, lRow, kUOMColIsSmallestUOM, CStr(1)
                moDmUOMGrid.SetRowDirty lRow
                Exit For
            End If
            
        Next lRow
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmMaintainItem", "SetIsSmallestUOM", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub SetupAdjQtyRoundMeth()

    'List values for AdjQtyRoundMeth:
        '0   Down to unit in decimal precision
        '1   Down to whole unit
        '2   To nearest unit in decimal precision
        '3   To nearest whole unit
        '4   None

    'Non-inventory and serialized items - None is the only option available
    'Items that allow decimal quantities, only the options ending in "to unit in decimal precision" are allowed
    'Item that do not allow decimal quantities - all but None are allowed
    'When done applying the new filter - if the current item is not allowed, reset as needed.
    'When None is the only option - disable the control.
    'The refresh at the end verifies that any changed value will be saved properly
    If (Not bIsInventory(ddnItemType.ItemData)) Or ddnItemType.ItemData = kItemTypeBTOKit _
      Or (ddnTrackMethod.ItemData = kTrackBoth Or ddnTrackMethod.ItemData = kTrackSerial) _
      Or ddnItemType.ItemData = kItemTypeAssembledKit Then
        With ddnAdjQtyRoundMeth
            .StaticListExcludeValues = "0,1,2,3"
            If .ItemData <> RoundMeth.None Then
               .ItemData = RoundMeth.None
            End If
            .Enabled = False
        End With
    Else
        If chkAllowDecQuantities.Value = kChecked Then
            With ddnAdjQtyRoundMeth
                .StaticListExcludeValues = "1,3,4"
                Select Case .ItemData
                Case RoundMeth.DownToWhole
                    .ItemData = RoundMeth.DownToPrec
                Case RoundMeth.ToNearestWhole
                    .ItemData = RoundMeth.ToNearestPrec
                Case RoundMeth.None
                    .SetToDefault
                End Select
            End With
        Else
            With ddnAdjQtyRoundMeth
                .StaticListExcludeValues = "4"
                If .ItemData = RoundMeth.None Then
                    .SetToDefault
                End If
            End With
        End If
        
        ddnAdjQtyRoundMeth.Enabled = True
    
    End If

    ddnAdjQtyRoundMeth.Refresh
    
End Sub

'************************************************************************************************
'************************************************************************************************
'SGS DEJ 5/18/12    (START)
'************************************************************************************************
'************************************************************************************************
Public Sub FormatCOALabGrid()
    On Error GoTo Error
    
    gGridSetProperties grdCOALabTests, kMaxCOACols, kGridDataSheet  ' use datasheet grid formating????
 
    'Added so that you locked cells cannot get focus
    grdCOALabTests.EditModePermanent = True
  
    'Setup maximum number of rows and columns
    gGridSetMaxRows grdCOALabTests, 1
    gGridSetMaxCols grdCOALabTests, kMaxCOACols

    'Set column widths
    grdCOALabTests.UnitType = SS_CELL_UNIT_TWIPS
    
    gGridSetHeader grdCOALabTests, kCOAColLabTestRqd, "Required"
    gGridSetColumnWidth grdCOALabTests, kCOAColLabTestRqd, 1200
    gGridSetColumnType grdCOALabTests, kCOAColLabTestRqd, SS_CELL_TYPE_CHECKBOX
    
    gGridSetHeader grdCOALabTests, kCOAColLabTestKey, "LabTestKey"
    gGridSetColumnWidth grdCOALabTests, kCOAColLabTestKey, 1000
    gGridSetColumnType grdCOALabTests, kCOAColLabTestKey, SS_CELL_TYPE_EDIT, 30
    gGridHideColumn grdCOALabTests, kCOAColLabTestKey
    
    gGridSetHeader grdCOALabTests, kCOAColLabTestID, "COA Test"
    gGridSetColumnWidth grdCOALabTests, kCOAColLabTestID, 3500
'    gGridSetColumnType grdCOALabTests, kCOAColLabTestID, SS_CELL_TYPE_EDIT, 30
    gGridSetColumnType grdCOALabTests, kCOAColLabTestID, SS_CELL_TYPE_EDIT, 120
    gGridLockColumn grdCOALabTests, kCOAColLabTestID
    
    gGridSetHeader grdCOALabTests, kCOAColMethStdKey, "Method Standard Key"
    gGridSetColumnWidth grdCOALabTests, kCOAColMethStdKey, 1000
    gGridSetColumnType grdCOALabTests, kCOAColMethStdKey, SS_CELL_TYPE_EDIT, 30
    gGridHideColumn grdCOALabTests, kCOAColMethStdKey
    
    gGridSetHeader grdCOALabTests, kCOAColMethStdID, "Method Standard"
    gGridSetColumnWidth grdCOALabTests, kCOAColMethStdID, 2500
    gGridSetColumnType grdCOALabTests, kCOAColMethStdID, SS_CELL_TYPE_COMBOBOX, sComboBoxList(ddnMethStd)
    
    gGridSetHeader grdCOALabTests, kCOAColUOMKey, "Unit Of Measure Key"
    gGridSetColumnWidth grdCOALabTests, kCOAColUOMKey, 1000
    gGridSetColumnType grdCOALabTests, kCOAColUOMKey, SS_CELL_TYPE_EDIT, 30
    gGridHideColumn grdCOALabTests, kCOAColUOMKey
    
    gGridSetHeader grdCOALabTests, kCOAColUOMID, "Unit Of Measure"
    gGridSetColumnWidth grdCOALabTests, kCOAColUOMID, 1500
    gGridSetColumnType grdCOALabTests, kCOAColUOMID, SS_CELL_TYPE_COMBOBOX, sComboBoxList(ddnUOM)
    
    gGridSetHeader grdCOALabTests, kCOAColCommOpp, "Compare Operator"
    gGridSetColumnWidth grdCOALabTests, kCOAColCommOpp, 1500
    gGridSetColumnType grdCOALabTests, kCOAColCommOpp, SS_CELL_TYPE_COMBOBOX, "Between" & vbTab & ">" & vbTab & "<" & vbTab & "=" & vbTab & " "
    
    gGridSetHeader grdCOALabTests, kCOAColValRng01, "Value One"
    gGridSetColumnWidth grdCOALabTests, kCOAColValRng01, 1500
    gGridSetColumnType grdCOALabTests, kCOAColValRng01, SS_CELL_TYPE_EDIT, 100
'    gGridSetColumnType grdCOALabTests, kCOAColValRng01, SS_CELL_TYPE_FLOAT, miQtyDecimal, 7
    
    gGridSetHeader grdCOALabTests, kCOAColValRng02, "Value Two"
    gGridSetColumnWidth grdCOALabTests, kCOAColValRng02, 1500
    gGridSetColumnType grdCOALabTests, kCOAColValRng02, SS_CELL_TYPE_EDIT, 100
'    gGridSetColumnType grdCOALabTests, kCOAColValRng02, SS_CELL_TYPE_FLOAT, miQtyDecimal, 7
    
    gGridSetHeader grdCOALabTests, kCOAColTestSort, "COA Sort Order"
    gGridSetColumnWidth grdCOALabTests, kCOAColTestSort, 1000
    gGridSetColumnType grdCOALabTests, kCOAColTestSort, SS_CELL_TYPE_COMBOBOX, GetCOASortValues()
    
    'Let the user resize the columns.
    grdCOALabTests.UserResizeCol = UserResizeOn
    
    'Scroll bars
    grdCOALabTests.ScrollBars = ScrollBarsBoth

    Exit Sub
Error:
    MsgBox Me.Name & ".FormatCOALabGrid() " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
        
End Sub

Sub SGSBindDM()
    On Error GoTo Error
    
    'Item Extention Table
    Set moDmItemExt = New clsDmForm
    
    With moDmItemExt
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .Table = "timItemExt_SGS"
        .UniqueKey = "ItemKey"
        .SaveOrder = 8
        Set .Parent = moDmItemForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER
        
        .BindComboBox ddnCOARpts, "COAReportKey", SQL_INTEGER, kDmUseItemData
        .Bind txtViscosityTemp, "ViscosityTemperature", SQL_DECIMAL
        .Bind nbrPercentResidue, "PercentResidue", SQL_DECIMAL, kDmSetNull   'RKL DEJ 2018-04-06
        .Bind nbrHeatCapacity, "HeatCapacity", SQL_DECIMAL, kDmSetNull   'RKL DEJ 2018-04-06
        
        .Init
    End With
    
   'Link the timItemsLabTest_SGS table.
    Set moDmCOATestsGrid = New clsDmGrid

    With moDmCOATestsGrid
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdCOALabTests
        .Table = "timItemsLabTests_SGS"
        .UniqueKey = "ItemKey, LabTestKey"
        .SaveOrder = 9
        
        .NoAppend = True
        .NoDelete = True
        .NoInsert = True
        
        Set .Parent = moDmItemForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER

        .BindColumn "LabTestKey", Nothing, SQL_INTEGER

        .BindColumn "TestIsRequired", kCOAColLabTestRqd, SQL_BIT

        .BindColumn "MethodStandardKey", kCOAColMethStdKey, SQL_INTEGER
        .BindColumn "UnitOfMeasureKey", kCOAColUOMKey, SQL_INTEGER
        .BindColumn "ComparisonOperator", kCOAColCommOpp, SQL_VARCHAR
        .BindColumn "ValueRange01", kCOAColValRng01, SQL_VARCHAR
        .BindColumn "ValueRange02", kCOAColValRng02, SQL_VARCHAR
        .BindColumn "COATestSortOrder", kCOAColTestSort, SQL_INTEGER

        .Init
    End With
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SGSBindDM() " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
        
End Sub

Private Function sComboBoxList(oCtl As Control) As String
    On Error GoTo Error
    
    Dim lListIndex As Long

    With oCtl
        
        For lListIndex = 0 To .ListCount
            If Len(sComboBoxList) <> 0 Then
                sComboBoxList = sComboBoxList & vbTab
            End If
            
            sComboBoxList = sComboBoxList & gsGetValidStr(.List(lListIndex))
            
        Next lListIndex
        
    End With
    
    Exit Function
Error:
    MsgBox Me.Name & ".sComboBoxList() " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Function

Sub SetupDropDownCtrls()
    On Error GoTo Error
    
    Dim SQL As String
    
    
    With ddnUOM
        SQL = "Select LabUOMID, LabUOMKey From timLabUOM_SGS "
        
        .SQLStatement = SQL
        
        .InitDynamicList moClass.moAppDB, .SQLStatement
    End With
    
    
    With ddnMethStd
        SQL = "Select MethodStandardID, MethodStandardKey From timLabMethodStandards_SGS "
        
        .SQLStatement = SQL
        
        .InitDynamicList moClass.moAppDB, .SQLStatement
    
    End With
    
    With ddnCOARpts
        SQL = "Select COATemplateReportID, COATemplateReportKey From timCOATemplateReports_SGS "
        
        .SQLStatement = SQL
        
        .InitDynamicList moClass.moAppDB, .SQLStatement
        
    End With
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SetupDropDownCtrls() " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Sub GetMissingLabTests()
    On Error GoTo Error
    Dim lcItemKey As Long
    
    lcItemKey = glGetValidLong(moDmItemForm.GetColumnValue("ItemKey"))
    
    If lcItemKey <= 0 Then Exit Sub
    
    If moDmItemForm.State = sotaTB_ADD Then Exit Sub
    
    With moClass.moAppDB
        .SetInParam lcItemKey
        .ExecuteSP "spIMAddMissingLabTest_SGS"
        .ReleaseParams
    End With
    
    While moClass.moAppDB.IsExecuting
        DoEvents
    Wend
    
    moDmCOATestsGrid.Refresh
    
    Exit Sub
Error:
    MsgBox Me.Name & ".GetMissingLabTests()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Sub

Function ValidateCOATest() As Boolean
    On Error GoTo Error
    
    Dim lcNbrRows As Long
    
    For lcNbrRows = 1 To grdCOALabTests.MaxRows - 1
        If IsValidCOAData(lcNbrRows) = False Then
            ValidateCOATest = False
            If tabMaintainItem.Tab <> kTabCOATests Then
                tabMaintainItem.Tab = kTabCOATests
            End If
            
            Exit Function
        End If
    Next
    
    ValidateCOATest = True
    Exit Function
Error:
    MsgBox Me.Name & ".ValidateCOATest()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Function

Function IsValidCOAData(lRow As Long) As Boolean
    On Error GoTo Error
    
    'SGS DEJ (Add Code here)???
    Dim lcTestRequired As Long
    Dim lcMethStdKey As Long
    Dim lcUOMKey As Long
    Dim lcCompOpp As String
    Dim lcVal001 As String
    Dim lcVal002 As String
    Dim lcTestSortOrder As Long
    Dim lcTestID As String
    
    'Default to not valid
    IsValidCOAData = False
    
    lcTestRequired = glGetValidLong(gsGridReadCellText(grdCOALabTests, lRow, kCOAColLabTestRqd))
    lcTestID = Trim(gsGetValidStr(gsGridReadCellText(grdCOALabTests, lRow, kCOAColLabTestID)))
    
    If lcTestRequired <> 0 Then
        'Get Values to save
        
        lcMethStdKey = glGetValidLong(gsGridReadCellText(grdCOALabTests, lRow, kCOAColMethStdKey))
        lcUOMKey = glGetValidLong(gsGridReadCellText(grdCOALabTests, lRow, kCOAColUOMKey))
        lcCompOpp = Trim(UCase(gsGetValidStr(gsGridReadCellText(grdCOALabTests, lRow, kCOAColCommOpp))))
        lcVal001 = Trim(gsGetValidStr(gsGridReadCellText(grdCOALabTests, lRow, kCOAColValRng01)))
        lcVal002 = Trim(gsGetValidStr(gsGridReadCellText(grdCOALabTests, lRow, kCOAColValRng02)))
        lcTestSortOrder = glGetValidLong(gsGridReadCellText(grdCOALabTests, lRow, kCOAColTestSort))
'Private Const kCOAColLabTestRqd = 1
'Private Const kCOAColLabTestKey = 2
'Private Const kCOAColLabTestID = 3
'Private Const kCOAColMethStdKey = 4
'Private Const kCOAColMethStdID = 5
'Private Const kCOAColUOMKey = 6
'Private Const kCOAColUOMID = 7
'Private Const kCOAColCommOpp = 8
'Private Const kCOAColValRng01 = 9
'Private Const kCOAColValRng02 = 10
'Private Const kCOAColTestSort = 11
        
        'Validate Method Standard
        If lcMethStdKey <= 0 Then
            MsgBox "The Method Standard is required", vbExclamation, "MAS 500"
            
            Exit Function
        End If
        
        'Validate UOM
        If lcUOMKey <= 0 Then
            MsgBox "The Unit Of Measure is required", vbExclamation, "MAS 500"
            
            Exit Function
        End If
        
'        'Validate Value 001
'        If Len(lcVal001) <= 0 Then
'            MsgBox "Value One is required.", vbExclamation, "MAS 500"
'
'            Exit Function
'        End If
        
        'Validate Test Sort Order
        If lcTestSortOrder < 0 Then
            MsgBox "The COA Test Sort Order is required", vbExclamation, "MAS 500"
            
            Exit Function
        End If
        
        'Validate Comparison Operatore
        Select Case Trim(UCase(lcCompOpp))
            Case ">"
                If IsNumeric(lcVal001) = False Or Len(Trim(lcVal001)) <= 0 Then
                    MsgBox "Test '" & lcTestID & "' using the Comparison Operator of '" & lcCompOpp & "' requires a numeric test value.  The Compare Operators (>, <, and Between) require numeric values.  The value entered for Value One is: '" & lcVal001 & "'.", vbExclamation, "MAS 500"
                    Exit Function
                End If
                
            Case "<"
                If IsNumeric(lcVal001) = False Or Len(Trim(lcVal001)) <= 0 Then
                    MsgBox "Test '" & lcTestID & "' using the Comparison Operator of '" & lcCompOpp & "' requires a numeric test value.  The Compare Operators (>, <, and Between) require numeric values.  The value entered for Value Two is: '" & lcVal001 & "'.", vbExclamation, "MAS 500"
                    Exit Function
                End If
            
            Case "BETWEEN"
                If IsNumeric(lcVal001) = False Or Len(Trim(lcVal001)) <= 0 Then
                    MsgBox "Test '" & lcTestID & "' using the Comparison Operator of '" & lcCompOpp & "' requires a numeric test value.  The Compare Operators (>, <, and Between) require numeric values.  The value entered for Value One is: '" & lcVal001 & "'.", vbExclamation, "MAS 500"
                    Exit Function
                End If
                
                If IsNumeric(lcVal002) = False Or Len(Trim(lcVal002)) <= 0 Then
                    MsgBox "Test '" & lcTestID & "' using the Comparison Operator of '" & lcCompOpp & "' requires a numeric test value.  The Compare Operators (>, <, and Between) require numeric values.  The value entered for Value Two is: '" & lcVal002 & "'.", vbExclamation, "MAS 500"
                    Exit Function
                End If
                
                If gdGetValidDbl(lcVal001) > gdGetValidDbl(lcVal002) Then
                    MsgBox "Test '" & lcTestID & "' using the Comparison Operator of '" & lcCompOpp & "' Value One must be less than Value Two.  The Compare Operators (>, <, and Between) require numeric values.  " & vbCrLf & "The value entered for Value One is: '" & lcVal001 & "'.  " & vbCrLf & "The value entered for Value Two is: '" & lcVal002 & "'.", vbExclamation, "MAS 500"
                    Exit Function
                End If
                
            Case "", "="
                'When entering the test results on the Test Results screen these values must match the test result values.
                If Len(Trim(lcVal001)) <= 0 Then
                    MsgBox "Test '" & lcTestID & "' using the Comparison Operator of '" & lcCompOpp & "' requires a Value One value.", vbExclamation, "MAS 500"
                    Exit Function
                End If
            
            Case Else
                'Missing all the validation data... user needs to set it up.
                MsgBox "Test '" & lcTestID & "' unknown Comparison Operator.  Please contact the system administrator.", vbExclamation, "MAS 500"
                Exit Function
        End Select
        
    End If
    
    IsValidCOAData = True

    Exit Function
Error:
    MsgBox Me.Name & ".IsValidCOAData()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Function

Function GetCOASortValues() As String
    On Error GoTo Error
    
    Dim RtnVal As String
    Dim i As Integer
    
    For i = 0 To 100
        If i = 100 Then
            RtnVal = RtnVal & i
        Else
            RtnVal = RtnVal & i & vbTab
        End If
    Next
    
    GetCOASortValues = RtnVal
    
    Exit Function
Error:
    MsgBox Me.Name & ".GetCOASortValues()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
End Function
'************************************************************************************************
'************************************************************************************************
'SGS DEJ 5/18/12    (STOP)
'************************************************************************************************
'************************************************************************************************




