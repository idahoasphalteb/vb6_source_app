Attribute VB_Name = "basWareHouse"
Option Explicit
'**********************************************************************
'     Name: basIMZMK001
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 03-26-1998
'     Mods: mm/dd/yy XXX
'**********************************************************************

'-- Static List Constants
' timItem | Status
    Public Const kvActive As Integer = 1                'Active                         Str=50028
    Public Const kvInactive As Integer = 2              'Inactive                       Str=50029
    Public Const kvDiscontinued As Integer = 3          'Discontinued                   Str=50387
    Public Const kvDeleted As Integer = 4               'Deleted                        Str=50033
    
    Public Const kWhseRunNormal As Integer = 1          'Normal Mode
    Public Const kWhseRunDD As Integer = 2              'Drill Down Mode

Const VBRIG_MODULE_ID_STRING = "IMZMK001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("imzmk001.clsWarehouse")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
    	Exit Sub

VBRigErrorRoutine:
    	gSetSotaErr Err, sMyName, "Main",  VBRIG_IS_MODULE
    	Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basWareHouse"
End Function
