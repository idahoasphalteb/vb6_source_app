VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{5CD3F513-DD26-4914-BBBF-95A33A72F67A}#40.0#0"; "ButtonControls.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmWareHouseMNT 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6285
   ClientLeft      =   1170
   ClientTop       =   1455
   ClientWidth     =   9570
   HelpContextID   =   69498
   Icon            =   "imzmk001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   6285
   ScaleWidth      =   9570
   Begin NEWSOTALib.SOTACustomizer PicDrag 
      Height          =   435
      Index           =   0
      Left            =   -30000
      TabIndex        =   157
      TabStop         =   0   'False
      Top             =   960
      WhatsThisHelpID =   70
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   767
      _StockProps     =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   156
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   255
      Index           =   0
      Left            =   -30000
      TabIndex        =   155
      Top             =   0
      WhatsThisHelpID =   69
      Width           =   255
      _ExtentX        =   423
      _ExtentY        =   450
      _Version        =   393216
      Enabled         =   -1  'True
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -3000
      TabIndex        =   154
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -3000
      TabIndex        =   150
      Top             =   0
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -3000
      Style           =   2  'Dropdown List
      TabIndex        =   148
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -3000
      TabIndex        =   147
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -3000
      TabIndex        =   146
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin EntryLookupControls.TextLookup lkuMain 
      Height          =   285
      Left            =   1320
      TabIndex        =   1
      Top             =   600
      WhatsThisHelpID =   69651
      Width           =   1665
      _ExtentX        =   2937
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   6
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Warehouse"
      ParentIDColumn  =   "WhseID"
      ParentKeyColumn =   "WhseKey"
      ParentTable     =   "timWarehouse"
      BoundColumn     =   "WhseKey"
      BoundTable      =   "timWarehouse"
      sSQLReturnCols  =   "WhseID,lkuMain,;Description,,;"
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5895
      WhatsThisHelpID =   73
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   135
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   741
   End
   Begin VB.CheckBox chkIntransitWhs 
      Alignment       =   1  'Right Justify
      Caption         =   "Transit &Warehouse"
      Height          =   285
      Left            =   3330
      TabIndex        =   4
      Top             =   930
      WhatsThisHelpID =   69648
      Width           =   2025
   End
   Begin VB.TextBox txtDescription 
      Height          =   285
      Left            =   5160
      MaxLength       =   30
      TabIndex        =   3
      Top             =   600
      WhatsThisHelpID =   69647
      Width           =   4092
   End
   Begin TabDlg.SSTab tabWarehouse 
      Height          =   4455
      Left            =   90
      TabIndex        =   5
      Top             =   1320
      WhatsThisHelpID =   69646
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   7858
      _Version        =   393216
      Tabs            =   7
      TabsPerRow      =   7
      TabHeight       =   529
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "imzmk001.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "pnlTabWarehouse(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Address"
      TabPicture(1)   =   "imzmk001.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "pnlTabWarehouse(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Replenishment"
      TabPicture(2)   =   "imzmk001.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "pnlTabWarehouse(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&GL Accounts"
      TabPicture(3)   =   "imzmk001.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "pnlTabWarehouse(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "&Transfers"
      TabPicture(4)   =   "imzmk001.frx":2442
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "pnlTabWarehouse(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "&Pick/Ship"
      TabPicture(5)   =   "imzmk001.frx":245E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "pnlTabWarehouse(5)"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "&Zones"
      TabPicture(6)   =   "imzmk001.frx":247A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "pnlTabWarehouse(6)"
      Tab(6).ControlCount=   1
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3900
         Index           =   6
         Left            =   -74910
         TabIndex        =   176
         Top             =   435
         Width           =   9090
         _Version        =   65536
         _ExtentX        =   16034
         _ExtentY        =   6879
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   0
         BorderWidth     =   0
         Begin Threed.SSFrame fraZones 
            Height          =   3615
            Left            =   405
            TabIndex        =   197
            Top             =   120
            Width           =   7965
            _Version        =   65536
            _ExtentX        =   14049
            _ExtentY        =   6376
            _StockProps     =   14
            Caption         =   "Zones"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MouseIcon       =   "imzmk001.frx":2496
            Begin FPSpreadADO.fpSpread grdWhseZones 
               Height          =   2955
               Left            =   345
               TabIndex        =   198
               Top             =   315
               WhatsThisHelpID =   17773887
               Width           =   7365
               _Version        =   524288
               _ExtentX        =   12991
               _ExtentY        =   5212
               _StockProps     =   64
               DisplayRowHeaders=   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "imzmk001.frx":24B2
               AppearanceStyle =   0
            End
         End
      End
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3870
         Index           =   5
         Left            =   -74895
         TabIndex        =   174
         Top             =   405
         Width           =   9060
         _Version        =   65536
         _ExtentX        =   15981
         _ExtentY        =   6826
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   0
         Begin Threed.SSFrame fraPicking 
            Height          =   3675
            Left            =   5010
            TabIndex        =   189
            Top             =   90
            Width           =   4005
            _Version        =   65536
            _ExtentX        =   7064
            _ExtentY        =   6482
            _StockProps     =   14
            Caption         =   "Picking"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Begin VB.CheckBox chkShipComplete 
               Caption         =   "Ship Complete"
               Height          =   285
               Left            =   105
               TabIndex        =   190
               Top             =   375
               WhatsThisHelpID =   17771461
               Width           =   3045
            End
            Begin VB.CheckBox chkPickFromSO 
               Caption         =   "Allow Immediate Picking From Sales Order"
               Height          =   330
               Left            =   105
               TabIndex        =   191
               Top             =   735
               WhatsThisHelpID =   17773888
               Width           =   3480
            End
            Begin VB.CheckBox chkShipFromPick 
               Caption         =   "Allow Shipping From Picking"
               Height          =   255
               Left            =   105
               TabIndex        =   192
               Top             =   1170
               WhatsThisHelpID =   17773889
               Width           =   3330
            End
            Begin SOTADropDownControl.SOTADropDown ddnDefLotPickMethod 
               Height          =   315
               Left            =   1980
               TabIndex        =   196
               Top             =   2070
               WhatsThisHelpID =   17773890
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnDefLotPickMethod"
            End
            Begin SOTADropDownControl.SOTADropDown ddnDefPickMethod 
               Height          =   315
               Left            =   1965
               TabIndex        =   194
               Top             =   1605
               WhatsThisHelpID =   17773891
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnDefPickMethod"
            End
            Begin VB.Label lblDefPickMethod 
               Caption         =   "Default Pick Method"
               Height          =   255
               Left            =   135
               TabIndex        =   193
               Top             =   1695
               Width           =   1515
            End
            Begin VB.Label lblDefLotPickMethod 
               Caption         =   "Default Lot Pick Method"
               Height          =   270
               Left            =   135
               TabIndex        =   195
               Top             =   2160
               Width           =   1740
            End
         End
         Begin Threed.SSFrame FraPrint 
            Height          =   3675
            Left            =   75
            TabIndex        =   175
            Top             =   90
            Width           =   4875
            _Version        =   65536
            _ExtentX        =   8599
            _ExtentY        =   6482
            _StockProps     =   14
            Caption         =   "Print Defaults"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Begin SOTADropDownControl.SOTADropDown ddnImInvPrintSetting 
               Height          =   315
               Left            =   2565
               TabIndex        =   188
               Top             =   2940
               WhatsThisHelpID =   17773892
               Width           =   2145
               _ExtentX        =   3784
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnImInvPrintSetting"
            End
            Begin SOTADropDownControl.SOTADropDown ddnImInvoiceDefPrinter 
               Height          =   315
               Left            =   2565
               TabIndex        =   186
               Top             =   2370
               WhatsThisHelpID =   17773893
               Width           =   2145
               _ExtentX        =   3784
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnImInvoiceDefPrinter"
            End
            Begin SOTADropDownControl.SOTADropDown ddnCallPickPrintSetting 
               Height          =   315
               Left            =   2550
               TabIndex        =   184
               Top             =   1875
               WhatsThisHelpID =   17773894
               Width           =   2145
               _ExtentX        =   3784
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnCallPickPrintSetting"
            End
            Begin SOTADropDownControl.SOTADropDown ddnCallPickDefPrinter 
               Height          =   315
               Left            =   2565
               TabIndex        =   182
               Top             =   1350
               WhatsThisHelpID =   17773895
               Width           =   2145
               _ExtentX        =   3784
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnCallPickDefPrinter"
            End
            Begin SOTADropDownControl.SOTADropDown ddnImPickPrintSetting 
               Height          =   315
               Left            =   2565
               TabIndex        =   180
               Top             =   855
               WhatsThisHelpID =   17773896
               Width           =   2145
               _ExtentX        =   3784
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnImPickPrintSetting"
            End
            Begin SOTADropDownControl.SOTADropDown ddnImPickDefPrinter 
               Height          =   315
               Left            =   2565
               TabIndex        =   178
               Top             =   300
               WhatsThisHelpID =   17773897
               Width           =   2145
               _ExtentX        =   3784
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnImPickDefPrinter"
            End
            Begin VB.Label lblImInvPrintSetting 
               Caption         =   "Immediate Invoice Print Setting"
               Height          =   315
               Left            =   135
               TabIndex        =   187
               Top             =   3030
               Width           =   2235
            End
            Begin VB.Label lblImInvoiceDefPrinter 
               Caption         =   "Immediate Invoice Default Printer"
               Height          =   285
               Left            =   135
               TabIndex        =   185
               Top             =   2445
               Width           =   2415
            End
            Begin VB.Label lblCallPickPrintSetting 
               Caption         =   "Will Call Picking Print Setting"
               Height          =   270
               Left            =   135
               TabIndex        =   183
               Top             =   1965
               Width           =   2130
            End
            Begin VB.Label lblCallPickDefPrinter 
               Caption         =   "Will Call Picking Default Printer"
               Height          =   240
               Left            =   135
               TabIndex        =   181
               Top             =   1440
               Width           =   2370
            End
            Begin VB.Label lblImPickPrintSetting 
               Caption         =   "Immediate Picking Print Setting"
               Height          =   330
               Left            =   135
               TabIndex        =   179
               Top             =   900
               Width           =   2445
            End
            Begin VB.Label lblImPickDefPrinter 
               Caption         =   "Immediate Picking Default Printer"
               Height          =   225
               Left            =   135
               TabIndex        =   177
               Top             =   405
               Width           =   2415
            End
         End
      End
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3855
         Index           =   4
         Left            =   -74880
         TabIndex        =   160
         Top             =   360
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   6800
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.Frame Frame4 
            Caption         =   "Inbound"
            Height          =   1335
            Left            =   0
            TabIndex        =   163
            Top             =   0
            Width           =   9015
            Begin EntryLookupControls.GLAcctLookup glaExpenseAcct 
               Height          =   615
               Left            =   3360
               TabIndex        =   132
               Top             =   480
               WhatsThisHelpID =   69643
               Width           =   3120
               _ExtentX        =   5503
               _ExtentY        =   1085
               ForeColor       =   -2147483640
               Enabled         =   0   'False
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "TrnsfrExpAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               AlignDesc       =   1
               WidthDesc       =   3120
               WidthAcct       =   2460
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,,;"
            End
            Begin VB.Frame Frame5 
               Caption         =   "&Freight Charges"
               Height          =   975
               Left            =   120
               TabIndex        =   165
               Top             =   240
               Width           =   1335
               Begin VB.OptionButton optFreightCharges 
                  Caption         =   "Capitalize"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   128
                  Top             =   240
                  Value           =   -1  'True
                  WhatsThisHelpID =   69641
                  Width           =   1095
               End
               Begin VB.OptionButton optFreightCharges 
                  Caption         =   "Expense"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   129
                  Top             =   600
                  WhatsThisHelpID =   69640
                  Width           =   975
               End
            End
            Begin VB.Frame Frame6 
               Caption         =   "&Surcharges"
               Height          =   975
               Left            =   1680
               TabIndex        =   164
               Top             =   240
               Width           =   1335
               Begin VB.OptionButton optSurcharges 
                  Caption         =   "Capitalize"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   130
                  Top             =   240
                  Value           =   -1  'True
                  WhatsThisHelpID =   69638
                  Width           =   1095
               End
               Begin VB.OptionButton optSurcharges 
                  Caption         =   "Expense"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   131
                  Top             =   600
                  WhatsThisHelpID =   69637
                  Width           =   1095
               End
            End
            Begin VB.Label lblExpenseAcct 
               Caption         =   "Expense Account"
               Height          =   255
               Left            =   3360
               TabIndex        =   166
               Top             =   240
               Width           =   1575
            End
         End
         Begin VB.Frame Frame7 
            Caption         =   "Outbound"
            Height          =   2295
            Left            =   0
            TabIndex        =   161
            Top             =   1440
            Width           =   9015
            Begin LookupViewControl.LookupView navShipMethod 
               Height          =   285
               Left            =   3000
               TabIndex        =   158
               TabStop         =   0   'False
               Top             =   1080
               Visible         =   0   'False
               WhatsThisHelpID =   69634
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.TextBox txtGrdNavShipMethod 
               Height          =   375
               Left            =   2040
               TabIndex        =   171
               Top             =   1080
               Visible         =   0   'False
               WhatsThisHelpID =   69633
               Width           =   975
            End
            Begin LookupViewControl.LookupView navTransitWhse 
               Height          =   285
               Left            =   5640
               TabIndex        =   170
               TabStop         =   0   'False
               Top             =   1080
               Visible         =   0   'False
               WhatsThisHelpID =   69632
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.TextBox txtGrdNavTransitWhse 
               Height          =   375
               Left            =   4560
               TabIndex        =   169
               Top             =   1080
               Visible         =   0   'False
               WhatsThisHelpID =   69631
               Width           =   1095
            End
            Begin LookupViewControl.LookupView navRcvgWhse 
               Height          =   285
               Left            =   1080
               TabIndex        =   168
               TabStop         =   0   'False
               Top             =   1080
               Visible         =   0   'False
               WhatsThisHelpID =   69630
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.TextBox txtGrdNavRcvgWhse 
               Height          =   375
               Left            =   120
               TabIndex        =   167
               Top             =   1080
               Visible         =   0   'False
               WhatsThisHelpID =   69629
               Width           =   975
            End
            Begin EntryLookupControls.GLAcctLookup glaFreightSurcharge 
               Height          =   285
               Left            =   1680
               TabIndex        =   133
               Top             =   240
               WhatsThisHelpID =   69628
               Width           =   5475
               _ExtentX        =   9657
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "TrnsfrMrkupAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   2325
               WidthAcct       =   2490
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,,;"
            End
            Begin FPSpreadADO.fpSpread grdTransferRules 
               Height          =   1650
               Left            =   120
               TabIndex        =   134
               Top             =   600
               WhatsThisHelpID =   69627
               Width           =   8775
               _Version        =   524288
               _ExtentX        =   15478
               _ExtentY        =   2910
               _StockProps     =   64
               DisplayRowHeaders=   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "imzmk001.frx":28D8
               AppearanceStyle =   0
            End
            Begin VB.Label lblFreightSurcharge 
               Caption         =   "Freight && Surcharge"
               Height          =   255
               Left            =   120
               TabIndex        =   162
               Top             =   240
               Width           =   1455
            End
         End
      End
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3615
         Index           =   3
         Left            =   -74880
         TabIndex        =   106
         Top             =   480
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   6376
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraGLAccounts 
            Height          =   3615
            Left            =   0
            TabIndex        =   107
            Top             =   0
            Width           =   9015
            Begin EntryLookupControls.GLAcctLookup glaPurchases 
               Height          =   285
               Left            =   1800
               TabIndex        =   109
               Top             =   240
               WhatsThisHelpID =   69623
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "PurchAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaSales 
               Height          =   285
               Left            =   1800
               TabIndex        =   111
               Top             =   570
               WhatsThisHelpID =   69622
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaSalesOffset 
               Height          =   285
               Left            =   1800
               TabIndex        =   113
               Top             =   900
               WhatsThisHelpID =   69621
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesOffsetAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaReturns 
               Height          =   285
               Left            =   1800
               TabIndex        =   115
               Top             =   1230
               WhatsThisHelpID =   69620
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesReturnAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaInventory 
               Height          =   285
               Left            =   1800
               TabIndex        =   117
               Top             =   1560
               WhatsThisHelpID =   69619
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "InvtAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaCostOfSales 
               Height          =   285
               Left            =   1800
               TabIndex        =   119
               Top             =   1890
               WhatsThisHelpID =   69618
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "COSAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaIssues 
               Height          =   285
               Left            =   1800
               TabIndex        =   121
               Top             =   2220
               WhatsThisHelpID =   69617
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "IssueAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaMiscAdj 
               Height          =   285
               Left            =   1800
               TabIndex        =   123
               Top             =   2550
               WhatsThisHelpID =   69616
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "MiscAdjAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaCostTierAdj 
               Height          =   285
               Left            =   1800
               TabIndex        =   125
               Top             =   2880
               WhatsThisHelpID =   69615
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "CostTierAdjAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaPhysicalCount 
               Height          =   285
               Left            =   1800
               TabIndex        =   127
               Top             =   3210
               WhatsThisHelpID =   69614
               Width           =   7140
               _ExtentX        =   12594
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "PhysCountAcctKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2580
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,lkuPurchases,;"
            End
            Begin VB.Label lblPurchases 
               Caption         =   "&Purchases"
               Height          =   195
               Left            =   120
               TabIndex        =   108
               Top             =   300
               Width           =   1635
            End
            Begin VB.Label lblSales 
               Caption         =   "Sa&les"
               Height          =   195
               Left            =   120
               TabIndex        =   110
               Top             =   630
               Width           =   1635
            End
            Begin VB.Label lblSalesOffset 
               Caption         =   "Sales O&ffset"
               Height          =   195
               Left            =   120
               TabIndex        =   112
               Top             =   960
               Width           =   1635
            End
            Begin VB.Label lblReturns 
               Caption         =   "R&eturns"
               Height          =   195
               Left            =   120
               TabIndex        =   114
               Top             =   1290
               Width           =   1635
            End
            Begin VB.Label lblInventory 
               Caption         =   "&Inventory"
               Height          =   195
               Left            =   120
               TabIndex        =   116
               Top             =   1620
               Width           =   1635
            End
            Begin VB.Label lblIssues 
               Caption         =   "Iss&ues"
               Height          =   195
               Left            =   120
               TabIndex        =   120
               Top             =   2280
               Width           =   1635
            End
            Begin VB.Label lblMiscAdj 
               Caption         =   "Mi&sc Adj"
               Height          =   195
               Left            =   120
               TabIndex        =   122
               Top             =   2610
               Width           =   1635
            End
            Begin VB.Label lblCostTierAdj 
               Caption         =   "C&ost Tier Adj"
               Height          =   195
               Left            =   120
               TabIndex        =   124
               Top             =   2940
               Width           =   1635
            End
            Begin VB.Label lblPhysical 
               Caption         =   "P&hysical Count"
               Height          =   195
               Left            =   120
               TabIndex        =   126
               Top             =   3270
               Width           =   1635
            End
            Begin VB.Label lblCostOfSales 
               Caption         =   "&Cost of Goods Sold"
               Height          =   195
               Left            =   120
               TabIndex        =   118
               Top             =   1950
               Width           =   1635
            End
         End
      End
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3615
         Index           =   2
         Left            =   -74880
         TabIndex        =   78
         Top             =   480
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   6376
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame Frame3 
            Caption         =   "Last Inventory Ranking"
            Height          =   1005
            Left            =   20
            TabIndex        =   86
            Top             =   1440
            Width           =   4095
            Begin SOTACalendarControl.SOTACalendar calLastRankDate 
               Height          =   315
               Left            =   2790
               TabIndex        =   88
               Top             =   240
               WhatsThisHelpID =   69601
               Width           =   1065
               _ExtentX        =   1879
               _ExtentY        =   556
               Alignment       =   1
               BackColor       =   -2147483633
               BorderStyle     =   0
               DisplayButton   =   0   'False
               DisplayOnly     =   -1  'True
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Protected       =   -1  'True
               Text            =   "  /  /    "
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtLastRankPeriod 
               Height          =   285
               Left            =   2820
               TabIndex        =   90
               Top             =   600
               WhatsThisHelpID =   69600
               Width           =   450
               _Version        =   65536
               lAlignment      =   1
               _ExtentX        =   794
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bAutoSelect     =   0   'False
               bProtected      =   -1  'True
               mask            =   "###"
               text            =   "   "
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtLastRankYear 
               Height          =   285
               Left            =   3300
               TabIndex        =   91
               Top             =   600
               WhatsThisHelpID =   69599
               Width           =   540
               _Version        =   65536
               lAlignment      =   1
               _ExtentX        =   952
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bAutoSelect     =   0   'False
               bProtected      =   -1  'True
               mask            =   "####"
               text            =   "    "
            End
            Begin VB.Label lblLastRankDate 
               AutoSize        =   -1  'True
               Caption         =   "Ranked On"
               Height          =   195
               Left            =   150
               TabIndex        =   87
               Top             =   300
               Width           =   825
            End
            Begin VB.Label lblLastRankPeriod 
               AutoSize        =   -1  'True
               Caption         =   "Through Inventory Period"
               Height          =   195
               Left            =   120
               TabIndex        =   89
               Top             =   630
               Width           =   1800
            End
            Begin VB.Label Label1 
               AutoSize        =   -1  'True
               Height          =   195
               Left            =   120
               TabIndex        =   159
               Top             =   630
               Width           =   45
            End
         End
         Begin VB.Frame frmReordering 
            Caption         =   "Re&ordering"
            Height          =   1395
            Left            =   0
            TabIndex        =   79
            Top             =   0
            Width           =   4095
            Begin NEWSOTALib.SOTANumber numCostOfReplenishment 
               Height          =   285
               Left            =   2040
               TabIndex        =   83
               Top             =   600
               WhatsThisHelpID =   69594
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999.999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber numCostOfCarryingPct 
               Height          =   285
               Left            =   2040
               TabIndex        =   85
               Top             =   930
               WhatsThisHelpID =   69593
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#<ILp0>#<IRp0>|.##"
               text            =   " 0.00"
               dMaxValue       =   100
               sIntegralPlaces =   2
               sDecimalPlaces  =   2
            End
            Begin SOTADropDownControl.SOTADropDown ddnReorderMeth 
               Height          =   315
               Left            =   2040
               TabIndex        =   81
               Top             =   240
               WhatsThisHelpID =   69592
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnReorderMeth"
               StaticListTableName=   "timWarehouse      "
               StaticListColumnName=   "ReordMeth         "
               AllowClear      =   0   'False
               Sorted          =   0   'False
            End
            Begin VB.Label lblReorderMeth 
               AutoSize        =   -1  'True
               Caption         =   "Method"
               Height          =   195
               Left            =   120
               TabIndex        =   80
               Top             =   300
               Width           =   540
            End
            Begin VB.Label lblCostOfCarrying 
               AutoSize        =   -1  'True
               Caption         =   "Carrying Cost Percentage"
               Height          =   195
               Left            =   120
               TabIndex        =   84
               Top             =   960
               Width           =   1800
            End
            Begin VB.Label lblCostOfReplenishment 
               AutoSize        =   -1  'True
               Caption         =   "Cost Of Replenishment"
               Height          =   195
               Left            =   120
               TabIndex        =   82
               Top             =   630
               Width           =   1620
            End
         End
         Begin VB.Frame frmGeneral 
            Height          =   2475
            Left            =   4510
            TabIndex        =   92
            Top             =   0
            Width           =   4500
            Begin NEWSOTALib.SOTANumber numTrendPct 
               Height          =   285
               Left            =   2760
               TabIndex        =   94
               Top             =   240
               WhatsThisHelpID =   69587
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,##<ILp0>#<IRp0>|.##"
               text            =   "   0.00"
               dMinValue       =   -999.99
               dMaxValue       =   999.99
               sIntegralPlaces =   4
               sDecimalPlaces  =   2
               dValue          =   0
            End
            Begin NEWSOTALib.SOTANumber numMaxSenlDmdMultiple 
               Height          =   285
               Left            =   2760
               TabIndex        =   104
               Top             =   1890
               WhatsThisHelpID =   69586
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numMinDmdMultiple 
               Height          =   285
               Left            =   2760
               TabIndex        =   102
               Top             =   1560
               WhatsThisHelpID =   69585
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numMaxDmdMultiple 
               Height          =   285
               Left            =   2760
               TabIndex        =   100
               Top             =   1230
               WhatsThisHelpID =   69584
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numMinOrdCycle 
               Height          =   285
               Left            =   2760
               TabIndex        =   98
               Top             =   900
               WhatsThisHelpID =   69583
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#"
               text            =   "  0"
               dMaxValue       =   999
               sIntegralPlaces =   3
               sDecimalPlaces  =   0
            End
            Begin NEWSOTALib.SOTANumber numMaxOrdCycle 
               Height          =   285
               Left            =   2760
               TabIndex        =   96
               Top             =   570
               WhatsThisHelpID =   69582
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#"
               text            =   "  0"
               dMaxValue       =   999
               sIntegralPlaces =   3
               sDecimalPlaces  =   0
            End
            Begin VB.Label lblTrendPct 
               AutoSize        =   -1  'True
               Caption         =   "Trend &Percentage"
               Height          =   195
               Left            =   120
               TabIndex        =   93
               Top             =   300
               Width           =   1290
            End
            Begin VB.Label lblMaxDmdMultiple 
               AutoSize        =   -1  'True
               Caption         =   "Ma&x Demand Multiple"
               Height          =   195
               Left            =   120
               TabIndex        =   99
               Top             =   1290
               Width           =   1530
            End
            Begin VB.Label lblMinDmdMultiple 
               AutoSize        =   -1  'True
               Caption         =   "Mi&n Demand Multiple"
               Height          =   195
               Left            =   120
               TabIndex        =   101
               Top             =   1620
               Width           =   1485
            End
            Begin VB.Label lblMaxOrdCycle 
               AutoSize        =   -1  'True
               Caption         =   "Max Order &Cycle"
               Height          =   195
               Left            =   120
               TabIndex        =   95
               Top             =   630
               Width           =   1170
            End
            Begin VB.Label lblMinOrdCycle 
               AutoSize        =   -1  'True
               Caption         =   "M&in Order Cycle"
               Height          =   195
               Left            =   120
               TabIndex        =   97
               Top             =   960
               Width           =   1125
            End
            Begin VB.Label lblMaxSenlDmdMultiple 
               Caption         =   "Max S&easonal Demand Multiple"
               Height          =   255
               Left            =   120
               TabIndex        =   103
               Top             =   1950
               Width           =   2355
            End
         End
         Begin VB.CommandButton cmdDemandVar 
            Caption         =   "Demand &Variance..."
            Height          =   375
            Left            =   7320
            TabIndex        =   105
            Top             =   3210
            WhatsThisHelpID =   69575
            Width           =   1695
         End
      End
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3495
         Index           =   1
         Left            =   -74880
         TabIndex        =   35
         Top             =   480
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   6165
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame frmMailTo 
            Caption         =   "&Ship To Address"
            Height          =   3300
            Left            =   4560
            TabIndex        =   57
            Top             =   0
            Width           =   4480
            Begin VB.TextBox txtSLongitude 
               Height          =   285
               Left            =   1185
               MaxLength       =   11
               TabIndex        =   77
               Top             =   2875
               Width           =   1000
            End
            Begin VB.TextBox txtSLatitude 
               Height          =   285
               Left            =   1185
               MaxLength       =   10
               TabIndex        =   75
               Top             =   2550
               Width           =   1000
            End
            Begin ButtonControls.MapButton cmdGetShipToMap 
               Height          =   285
               Left            =   880
               TabIndex        =   172
               TabStop         =   0   'False
               Top             =   580
               WhatsThisHelpID =   17778299
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
            End
            Begin VB.TextBox txtSAddrName 
               Height          =   285
               Left            =   1180
               MaxLength       =   40
               TabIndex        =   59
               Top             =   240
               WhatsThisHelpID =   69567
               Width           =   3200
            End
            Begin VB.TextBox txtSCity 
               Height          =   285
               Left            =   1180
               MaxLength       =   20
               TabIndex        =   67
               Top             =   1895
               WhatsThisHelpID =   69566
               Width           =   1380
            End
            Begin VB.PictureBox Picture2 
               Height          =   1280
               Left            =   1180
               ScaleHeight     =   1215
               ScaleWidth      =   3135
               TabIndex        =   137
               TabStop         =   0   'False
               Top             =   570
               Width           =   3200
               Begin VB.TextBox txtSAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   4
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   65
                  Top             =   960
                  WhatsThisHelpID =   69564
                  Width           =   3200
               End
               Begin VB.TextBox txtSAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   3
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   64
                  Top             =   720
                  WhatsThisHelpID =   69563
                  Width           =   3200
               End
               Begin VB.TextBox txtSAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   2
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   63
                  Top             =   480
                  WhatsThisHelpID =   69562
                  Width           =   3200
               End
               Begin VB.TextBox txtSAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   1
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   62
                  Top             =   240
                  WhatsThisHelpID =   69561
                  Width           =   3200
               End
               Begin VB.TextBox txtSAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   0
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   61
                  Top             =   -10
                  WhatsThisHelpID =   69560
                  Width           =   3200
               End
            End
            Begin EntryLookupControls.TextLookup lkuSPostalCode 
               Height          =   285
               Left            =   1185
               TabIndex        =   71
               Top             =   2220
               WhatsThisHelpID =   69559
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   9
               LookupID        =   "Postal"
               BoundColumn     =   "PostalCode"
               BoundTable      =   "tciAddress"
               sSQLReturnCols  =   "PostalCode,lkuSPostalCode,;"
            End
            Begin SOTADropDownControl.SOTADropDown ddnSState 
               Height          =   315
               Left            =   3375
               TabIndex        =   69
               Top             =   1890
               WhatsThisHelpID =   69558
               Width           =   1035
               _ExtentX        =   1826
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnSState"
               SQLStatement    =   "Select StateID from tsmState"
            End
            Begin SOTADropDownControl.SOTADropDown ddnSCountry 
               Height          =   315
               Left            =   3375
               TabIndex        =   73
               Top             =   2220
               WhatsThisHelpID =   69557
               Width           =   1035
               _ExtentX        =   1826
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnSCountry"
               SQLStatement    =   "Select CountryID from tsmCountry"
            End
            Begin VB.Label lblSLatitude 
               Caption         =   "Latitude"
               Height          =   195
               Left            =   60
               TabIndex        =   74
               Top             =   2550
               Width           =   855
            End
            Begin VB.Label lblSLongitude 
               Caption         =   "Longitude"
               Height          =   195
               Left            =   60
               TabIndex        =   76
               Top             =   2875
               Width           =   750
            End
            Begin VB.Label lblAddrName 
               AutoSize        =   -1  'True
               Caption         =   "Name"
               Height          =   195
               Left            =   60
               TabIndex        =   58
               Top             =   300
               Width           =   420
            End
            Begin VB.Label lblAddrLine1 
               AutoSize        =   -1  'True
               Caption         =   "Street Addr"
               Height          =   195
               Left            =   60
               TabIndex        =   60
               Top             =   630
               Width           =   795
            End
            Begin VB.Label lblCity 
               AutoSize        =   -1  'True
               Caption         =   "City"
               Height          =   195
               Left            =   60
               TabIndex        =   66
               Top             =   1955
               Width           =   255
            End
            Begin VB.Label lblstate 
               AutoSize        =   -1  'True
               Caption         =   "State"
               Height          =   195
               Left            =   2695
               TabIndex        =   68
               Top             =   1955
               Width           =   375
            End
            Begin VB.Label lblCountry 
               AutoSize        =   -1  'True
               Caption         =   "Country"
               Height          =   195
               Left            =   2695
               TabIndex        =   72
               Top             =   2285
               Width           =   540
            End
            Begin VB.Label lblPostalCode 
               AutoSize        =   -1  'True
               Caption         =   "Postal Code"
               Height          =   195
               Left            =   60
               TabIndex        =   70
               Top             =   2285
               Width           =   855
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Mail T&o Address"
            Height          =   3250
            Left            =   0
            TabIndex        =   36
            Top             =   0
            Width           =   4480
            Begin VB.TextBox txtMLongitude 
               Height          =   285
               Left            =   1185
               MaxLength       =   11
               TabIndex        =   56
               Top             =   2875
               Width           =   1000
            End
            Begin VB.TextBox txtMLatitude 
               Height          =   285
               Left            =   1185
               MaxLength       =   10
               TabIndex        =   54
               Top             =   2550
               Width           =   1000
            End
            Begin ButtonControls.MapButton cmdGetMailToMap 
               Height          =   285
               Left            =   880
               TabIndex        =   173
               TabStop         =   0   'False
               Top             =   580
               WhatsThisHelpID =   17778300
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
            End
            Begin VB.PictureBox Picture1 
               Height          =   1280
               Left            =   1180
               ScaleHeight     =   1215
               ScaleWidth      =   3135
               TabIndex        =   136
               TabStop         =   0   'False
               Top             =   570
               Width           =   3200
               Begin VB.TextBox txtMAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   4
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   44
                  Top             =   960
                  WhatsThisHelpID =   69548
                  Width           =   3200
               End
               Begin VB.TextBox txtMAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   3
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   43
                  Top             =   720
                  WhatsThisHelpID =   69547
                  Width           =   3200
               End
               Begin VB.TextBox txtMAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   2
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   42
                  Top             =   480
                  WhatsThisHelpID =   69546
                  Width           =   3200
               End
               Begin VB.TextBox txtMAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   1
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   41
                  Top             =   240
                  WhatsThisHelpID =   69545
                  Width           =   3200
               End
               Begin VB.TextBox txtMAddress 
                  Appearance      =   0  'Flat
                  Height          =   315
                  Index           =   0
                  Left            =   -10
                  MaxLength       =   40
                  TabIndex        =   40
                  Top             =   -10
                  WhatsThisHelpID =   69544
                  Width           =   3200
               End
            End
            Begin VB.TextBox txtMCity 
               Height          =   285
               Left            =   1180
               MaxLength       =   20
               TabIndex        =   46
               Top             =   1895
               WhatsThisHelpID =   69543
               Width           =   1380
            End
            Begin VB.TextBox txtMAddrName 
               Height          =   285
               Left            =   1180
               MaxLength       =   40
               TabIndex        =   38
               Top             =   240
               WhatsThisHelpID =   69542
               Width           =   3200
            End
            Begin EntryLookupControls.TextLookup lkuMPostalCode 
               Height          =   285
               Left            =   1185
               TabIndex        =   50
               Top             =   2220
               WhatsThisHelpID =   69541
               Width           =   1395
               _ExtentX        =   2461
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   9
               LookupID        =   "Postal"
               BoundColumn     =   "PostalCode"
               BoundTable      =   "tciAddress"
               sSQLReturnCols  =   "PostalCode,lkuMPostalCode,;"
            End
            Begin SOTADropDownControl.SOTADropDown ddnMState 
               Height          =   315
               Left            =   3375
               TabIndex        =   48
               Top             =   1890
               WhatsThisHelpID =   69540
               Width           =   1035
               _ExtentX        =   1826
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnMState"
               SQLStatement    =   "Select StateID from tsmState"
            End
            Begin SOTADropDownControl.SOTADropDown ddnMCountry 
               Height          =   315
               Left            =   3375
               TabIndex        =   52
               Top             =   2220
               WhatsThisHelpID =   69539
               Width           =   1035
               _ExtentX        =   1826
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnMCountry"
               SQLStatement    =   "Select CountryID from tsmCountry"
               DefaultItemData =   "1"
            End
            Begin VB.Label lblMLatitude 
               Caption         =   "Latitude"
               Height          =   255
               Left            =   60
               TabIndex        =   53
               Top             =   2550
               Width           =   855
            End
            Begin VB.Label lblMLongitude 
               Caption         =   "Longitude"
               Height          =   195
               Left            =   60
               TabIndex        =   55
               Top             =   2875
               Width           =   750
            End
            Begin VB.Label lblMPostalCode 
               AutoSize        =   -1  'True
               Caption         =   "Postal Code"
               Height          =   195
               Left            =   60
               TabIndex        =   49
               Top             =   2285
               Width           =   855
            End
            Begin VB.Label lblMailCountry 
               AutoSize        =   -1  'True
               Caption         =   "Country"
               Height          =   195
               Left            =   2695
               TabIndex        =   51
               Top             =   2285
               Width           =   540
            End
            Begin VB.Label lblMailState 
               AutoSize        =   -1  'True
               Caption         =   "State"
               Height          =   195
               Left            =   2695
               TabIndex        =   47
               Top             =   1955
               Width           =   375
            End
            Begin VB.Label lblMCity 
               AutoSize        =   -1  'True
               Caption         =   "City"
               Height          =   195
               Left            =   60
               TabIndex        =   45
               Top             =   1955
               Width           =   255
            End
            Begin VB.Label lblMAddrLine1 
               AutoSize        =   -1  'True
               Caption         =   "Street Addr"
               Height          =   195
               Left            =   60
               TabIndex        =   39
               Top             =   630
               Width           =   795
            End
            Begin VB.Label lblMAddrName 
               AutoSize        =   -1  'True
               Caption         =   "Name"
               Height          =   195
               Left            =   60
               TabIndex        =   37
               Top             =   300
               Width           =   420
            End
         End
      End
      Begin Threed.SSPanel pnlTabWarehouse 
         Height          =   3855
         Index           =   0
         Left            =   135
         TabIndex        =   6
         Top             =   480
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   6800
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame frmRestocking 
            Caption         =   "Re&stocking"
            Height          =   1005
            Left            =   0
            TabIndex        =   17
            Top             =   2160
            Width           =   4095
            Begin NEWSOTALib.SOTANumber numRestockRate 
               Height          =   285
               Left            =   2040
               TabIndex        =   21
               Top             =   600
               WhatsThisHelpID =   69573
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#<IRp0>|.##"
               text            =   "    0.00"
               sIntegralPlaces =   5
               sDecimalPlaces  =   2
            End
            Begin SOTADropDownControl.SOTADropDown ddnRestockMeth 
               Height          =   315
               Left            =   2040
               TabIndex        =   19
               Top             =   240
               WhatsThisHelpID =   69572
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnRestockMeth"
               StaticListTableName=   "timWarehouse"
               StaticListColumnName=   "RestockMeth"
               AllowClear      =   0   'False
               Sorted          =   0   'False
            End
            Begin VB.Label lblRestockMeth 
               AutoSize        =   -1  'True
               Caption         =   "Method"
               Height          =   195
               Left            =   120
               TabIndex        =   18
               Top             =   300
               Width           =   540
            End
            Begin VB.Label lblRstkgRate 
               AutoSize        =   -1  'True
               Caption         =   "Rate"
               Height          =   195
               Left            =   120
               TabIndex        =   20
               Top             =   630
               Width           =   345
            End
         End
         Begin VB.Frame frmLeadTimes 
            Caption         =   "Qualified &Lead Times"
            Height          =   1035
            Left            =   0
            TabIndex        =   7
            Top             =   0
            Width           =   4000
            Begin NEWSOTALib.SOTANumber numMaxQulLeadTimePct 
               Height          =   285
               Left            =   2220
               TabIndex        =   9
               Top             =   240
               WhatsThisHelpID =   69530
               Width           =   1455
               _Version        =   65536
               _ExtentX        =   2566
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               locale          =   1033
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numMinQulLeadTimePct 
               Height          =   285
               Left            =   2220
               TabIndex        =   11
               Top             =   570
               WhatsThisHelpID =   69529
               Width           =   1455
               _Version        =   65536
               _ExtentX        =   2566
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   3
               locale          =   1033
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblMaxQulLeadTimePct 
               AutoSize        =   -1  'True
               Caption         =   "Maximum %"
               Height          =   195
               Left            =   120
               TabIndex        =   8
               Top             =   300
               Width           =   825
            End
            Begin VB.Label lblMinQulLeadTimePct 
               AutoSize        =   -1  'True
               Caption         =   "Minimum %"
               Height          =   195
               Left            =   120
               TabIndex        =   10
               Top             =   630
               Width           =   780
            End
         End
         Begin VB.Frame Frame1 
            Height          =   2355
            Left            =   4260
            TabIndex        =   22
            Top             =   0
            Width           =   4750
            Begin VB.CheckBox chkTrackQtyAtBin 
               Alignment       =   1  'Right Justify
               Caption         =   "Track &Quantity At Bin"
               Enabled         =   0   'False
               Height          =   285
               Left            =   105
               TabIndex        =   26
               Top             =   990
               WhatsThisHelpID =   69525
               Width           =   3045
            End
            Begin VB.CheckBox chkUseZones 
               Alignment       =   1  'Right Justify
               Caption         =   "Us&e Zones"
               Height          =   255
               Left            =   105
               TabIndex        =   27
               Top             =   1335
               WhatsThisHelpID =   17773898
               Width           =   3045
            End
            Begin VB.CheckBox chkUseBins 
               Alignment       =   1  'Right Justify
               Caption         =   "&Use Bins"
               Height          =   255
               Left            =   105
               TabIndex        =   25
               Top             =   675
               WhatsThisHelpID =   69523
               Width           =   3045
            End
            Begin EntryLookupControls.TextLookup lkuSTaxSchedule 
               Height          =   285
               Left            =   2940
               TabIndex        =   24
               Top             =   240
               WhatsThisHelpID =   69522
               Width           =   1680
               _ExtentX        =   2963
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "STaxSchedule"
               ParentIDColumn  =   "STaxSchdID"
               ParentKeyColumn =   "STaxSchdKey"
               ParentTable     =   "tciSTaxSchedule"
               BoundColumn     =   "STaxSchdKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "STaxSchdID,lkuSTaxSchedule,;"
            End
            Begin EntryLookupControls.TextLookup lkuWhsOvrdSegVal 
               Height          =   285
               Left            =   2940
               TabIndex        =   29
               Top             =   1665
               WhatsThisHelpID =   69521
               Width           =   1680
               _ExtentX        =   2963
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               LookupID        =   "SegmentCode"
               BoundColumn     =   "WhseOvrdSegValue"
               BoundTable      =   "timWarehouse"
               sSQLReturnCols  =   "AcctSegValue,lkuWhsOvrdSegVal,;Description,,;"
            End
            Begin VB.Label lblWhsOvrdSegVal 
               AutoSize        =   -1  'True
               Caption         =   "Warehouse Override Se&gment Value"
               Height          =   225
               Left            =   135
               TabIndex        =   28
               Top             =   1725
               Width           =   2595
            End
            Begin VB.Label lblStaxSchedule 
               AutoSize        =   -1  'True
               Caption         =   "&Sales Tax Schedule"
               Height          =   195
               Left            =   120
               TabIndex        =   23
               Top             =   300
               Width           =   1450
            End
         End
         Begin VB.Frame frmAsstdWhs 
            Caption         =   "Associated Ware&houses"
            Height          =   1000
            Left            =   0
            TabIndex        =   12
            Top             =   1110
            Width           =   4000
            Begin EntryLookupControls.TextLookup lkuCostWhse 
               Height          =   285
               Left            =   2220
               TabIndex        =   14
               Top             =   240
               WhatsThisHelpID =   69517
               Width           =   1455
               _ExtentX        =   2566
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Warehouse"
               ParentIDColumn  =   "WhseID"
               ParentKeyColumn =   "WhseKey"
               ParentTable     =   "timWarehouse"
               BoundColumn     =   "CostWhseKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "WhseID,lkuCostWhse,;Description,,;"
            End
            Begin EntryLookupControls.TextLookup lkuPriceWhse 
               Height          =   285
               Left            =   2220
               TabIndex        =   16
               Top             =   570
               WhatsThisHelpID =   69516
               Width           =   1455
               _ExtentX        =   2566
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Warehouse"
               ParentIDColumn  =   "WhseID"
               ParentKeyColumn =   "WhseKey"
               ParentTable     =   "timWarehouse"
               BoundColumn     =   "PriceWhseKey"
               BoundTable      =   "timWarehouse"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "WhseID,lkuPriceWhse,;Description,,;"
            End
            Begin VB.Label lblCostWhs 
               AutoSize        =   -1  'True
               Caption         =   "Cost "
               Height          =   195
               Left            =   120
               TabIndex        =   13
               Top             =   300
               Width           =   360
            End
            Begin VB.Label lblPriceWhs 
               AutoSize        =   -1  'True
               Caption         =   "Price "
               Height          =   195
               Left            =   120
               TabIndex        =   15
               Top             =   630
               Width           =   405
            End
         End
         Begin VB.CommandButton cmdContact 
            Caption         =   "Warehouse Manager ..."
            Height          =   375
            Left            =   7080
            TabIndex        =   34
            Top             =   3450
            WhatsThisHelpID =   69513
            Width           =   1935
         End
         Begin EntryLookupControls.TextLookup lkuCOASig 
            Height          =   285
            Left            =   4320
            TabIndex        =   31
            Top             =   2640
            WhatsThisHelpID =   69541
            Width           =   4635
            _ExtentX        =   8176
            _ExtentY        =   503
            ForeColor       =   -2147483640
            IsSurrogateKey  =   -1  'True
            LookupID        =   "WhseSiganture"
            ParentIDColumn  =   "SignatureID"
            ParentKeyColumn =   "SignatureKey"
            ParentTable     =   "timWhseSignatures_RKL"
            BoundColumn     =   "SignatureKey"
            BoundTable      =   "timWarehouseExt_RKL"
            IsForeignKey    =   -1  'True
            sSQLReturnCols  =   "SignatureID,,;SignatureKey,,;"
         End
         Begin NEWSOTALib.SOTANumber nbrBoilingPoint 
            Height          =   285
            Left            =   5400
            TabIndex        =   33
            Top             =   3000
            WhatsThisHelpID =   69530
            Width           =   1695
            _Version        =   65536
            _ExtentX        =   2990
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<ILH>##<ILp0>#"
            text            =   "  0"
            dMaxValue       =   999.99
            sIntegralPlaces =   3
            locale          =   1033
            sDecimalPlaces  =   0
         End
         Begin VB.Label lblBoilingPoint 
            Caption         =   "Boiling Point"
            Height          =   255
            Left            =   4320
            TabIndex        =   32
            Top             =   3000
            Width           =   975
         End
         Begin VB.Label lblCOASig 
            Caption         =   "COA Report Signature"
            Height          =   255
            Left            =   4320
            TabIndex        =   30
            Top             =   2400
            Width           =   2055
         End
      End
   End
   Begin NEWSOTALib.SOTANumber numMaxQulLeadTimePctHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   138
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69512
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILp0>#<IRp0>|.####"
      text            =   "0.0000"
      sIntegralPlaces =   1
      locale          =   1033
      sDecimalPlaces  =   4
   End
   Begin NEWSOTALib.SOTANumber numMinQulLeadTimePctHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   139
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69511
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>##<ILp0>#<IRp0>|.####"
      text            =   "  0.0000"
      sIntegralPlaces =   3
      locale          =   1033
      sDecimalPlaces  =   4
   End
   Begin NEWSOTALib.SOTANumber numCostOfCarryingPctHidden 
      Height          =   285
      Left            =   120
      TabIndex        =   140
      TabStop         =   0   'False
      Top             =   60
      WhatsThisHelpID =   69510
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILp0>#<IRp0>|.####"
      text            =   "0.0000"
      dMaxValue       =   9.9999
      sIntegralPlaces =   1
      locale          =   1033
      sDecimalPlaces  =   4
   End
   Begin NEWSOTALib.SOTANumber numTrendPctHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   141
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69509
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILp0>#<IRp0>|.####"
      text            =   "0.0000"
      dMaxValue       =   1
      sIntegralPlaces =   1
      locale          =   1033
      sDecimalPlaces  =   4
   End
   Begin NEWSOTALib.SOTANumber numMaxDmdMultipleHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   142
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69508
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<IRp0>|.##"
      text            =   ".00"
      dMaxValue       =   0.99
      sIntegralPlaces =   0
      locale          =   1033
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber numMinDmdMultipleHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   143
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69507
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<IRp0>|.##"
      text            =   ".00"
      dMaxValue       =   0.99
      sIntegralPlaces =   0
      locale          =   1033
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber numMaxSenlDmdMultipleHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   144
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69506
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<IRp0>|.##"
      text            =   ".00"
      dMaxValue       =   0.99
      sIntegralPlaces =   0
      locale          =   1033
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber numRestockRateHidden 
      Height          =   285
      Left            =   0
      TabIndex        =   145
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   69505
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,##<ILp0>#<IRp0>|.######"
      text            =   "     0.000000"
      sIntegralPlaces =   6
      locale          =   1033
      sDecimalPlaces  =   6
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -3000
      TabIndex        =   149
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -3000
      TabIndex        =   152
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -3000
      TabIndex        =   153
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4425
      Top             =   495
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -3000
      TabIndex        =   151
      Top             =   0
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblDescription 
      AutoSize        =   -1  'True
      Caption         =   "&Description"
      Height          =   195
      Left            =   3360
      TabIndex        =   2
      Top             =   660
      Width           =   795
   End
   Begin VB.Label lblWarehouse 
      AutoSize        =   -1  'True
      Caption         =   "Warehouse"
      Height          =   195
      Left            =   180
      TabIndex        =   0
      Top             =   660
      Width           =   825
   End
End
Attribute VB_Name = "frmWareHouseMNT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*******************************************************
'   Name        :   frmWarehouseMNT
'   Desc        :   This form maintains Warehouses.
'   Original    :   03/26/98
'   Mods        :
'*******************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'-- Private variables of Form Properties
    Private moClass             As Object
    Private mlRunMode           As Long
    Private mbSaved             As Boolean
    Private mbCancelShutDown    As Boolean
    Private msCountryID         As String
    Private msPhoneMask         As String
    Private moOptions           As New clsModuleOptions
    Private mbIRIsActivated     As Boolean
    Private mbWMIsLicensed      As Boolean
    
'-- Public Form Variables
    Public moSotaObjects        As New Collection
    Public moMapSrch            As New Collection

'-- Private objects
    Private WithEvents moDmForm     As clsDmForm
Attribute moDmForm.VB_VarHelpID = -1
    Private WithEvents moDmMailAddr As clsDmForm
Attribute moDmMailAddr.VB_VarHelpID = -1
    Private WithEvents moDmShipAddr As clsDmForm
Attribute moDmShipAddr.VB_VarHelpID = -1
    
    Private moCIContact             As Object
    Private moIMDemandVar           As Object

'-- Public objects
    Public moContextMenu            As New clsContextMenu
    Public mbManualComboSelect      As Boolean
    Public moValMgr                 As Object
    'For Transfer Tab
    Public WithEvents moDmGrid      As clsDmGrid
Attribute moDmGrid.VB_VarHelpID = -1
    Public WithEvents moGM                     As clsGridMgr       ' Grid Manager
Attribute moGM.VB_VarHelpID = -1
    Public WithEvents moDmZoneGrid  As clsDmGrid
Attribute moDmZoneGrid.VB_VarHelpID = -1
    Public WithEvents moGMZoneGrid             As clsGridMgr       ' Grid Manager
Attribute moGMZoneGrid.VB_VarHelpID = -1
    
'-- Miscellaneous Variables
    Private miFilter            As Integer
    Private mbEnterAsTab        As Boolean
    Private msCompanyID         As String
    Private msUserID            As String
    Private mlLanguage          As Long
    Private mbInNavigator       As Boolean
    Private mbDmLoad            As Boolean
    Private msNavRestrict       As String
    Private miSecurityLevel     As Integer
    Private mlSysDisabledColor  As Long
    Private mlSysEnabledColor   As Long
    Private msCurrID            As String
    Private mbLoadTrackQtyAtBin As Boolean
    Private mbLoadUseBins       As Boolean
    Private bNewWarehouse       As Boolean
    Private mlPrimaryCntctKey   As Long
    Private mlCntctKey          As Long
    Private mlWhseRunMode       As Long
    Private bChkNavClick        As Boolean
    Private mlWhseOvrdSegKey    As Long
    Private msWarehouseMgr      As String
    Private bCheckInboundTransfer As Boolean
    Private mbDMStateChanged      As Boolean  'DMStateChange Flag
'-- Constants
    Private Const ktskIMWarehouseLST = 118095972
    Private Const ktskIMDemandVarMNT = 117506188
    Private Const ktskIMDemandVarCLS = "imzmk003.clsDemandVariance"
    Private Const ktskIMBinVarMNT = 117506268
    Private Const ktskIMBinVarCLS = "imzmn001.clsSetupBins"
    Private Const ksWarehouse = "Warehouse"
    Private Const kTabMain = 0
    Private Const kTabAddress = 1
    Private Const kTabReplenishment = 2
    Private Const kTabGLAccount = 3
    Private Const kTabTransfer = 4
    Private Const kTabPicking = 5
    Private Const kTabZone = 6
    Private Const kGetCostWhse = 1
    Private Const kGetPriceWhse = 2
    
    Private Const kMailAddress = 0
    Private Const kShipAddress = 1
    
    ' grdTransferRules COLUMN constants
    Private Const kMaxCols = 15
    Private Const kNumberOfVisibleColumns = 8
    Private Const kCol0 = 0
    Private Const kColReceivingWarehouse = 1
    Private Const kColShipMethod = 2
    Private Const kColSurchargePercent = 3
    Private Const kColSurchargeAmount = 4
    Private Const kColSurchargeAmountMethod = 5
    Private Const kColAutomaticApproval = 6
    Private Const kColTransitWarehouse = 7
    Private Const kColLeadTimeDays = 8
    Private Const kColShipWhseKey = 9                     ' hidden column
    Private Const kColReceivingWarehouseKey = 10          ' hidden column
    Private Const kColShipMethodKey = 11                  ' hidden column
    Private Const kColSurchargeAmountMethodValue = 12     ' hidden column
    Private Const kColAutomaticApprovalValue = 13         ' hidden column
    Private Const kColTransitWarehouseKey = 14            ' hidden column
    Private Const kColSurchargePercentValue = 15          ' hidden column
    
    ' grdTransferRules column WIDTH constants
    Private Const kColReceivingWarehouseWidth = 8
    Private Const KColShipMethodWidth = 7
    Private Const kColSurchargePercentWidth = 7
    Private Const kColSurchargeAmountWidth = 7
    Private Const kColSurchargeAmountMethodWidth = 10
    Private Const kColAutomaticApprovalWidth = 13
    Private Const kColTransitWarehouseWidth = 8
    Private Const kColLeadTimeDaysWidth = 7
    
    Private Const kMaxIntegerDigits = 5                   ' maximum digits for an integer cell in the grid
    Private Const koptFreightChargesCapitalize = 1        ' Freight Charges option control -- Capitalize
    Private Const koptFreightChargesExpense = 2           ' Freight Charges option control -- Expense
    Private Const koptSurchargesCapitalize = 1            ' Surcharges option control -- Capitalize
    Private Const koptSurchargesExpense = 2               ' Surcharges option control -- Expense
    Private Const kSurchargeAmountMethodDDNDefault = 1    ' Surcharge Amount Method grid dropdown default
    Private Const kAutomaticApprovalDDNDefault = 3        ' Automatic Approval grid dropdown default
    Private Const kValidationMode = 2                     ' Mode parameter to call the spimCostGetCostingWhse
    Private Const kCostWhseCircRef = 12                   ' Error Number for Circular Reference of costing warehouse
    Private Const kPriceWhseCircRef = 18                  ' Error Number for Circular Reference of pricing warehouse
    Private Const kModuleIR = 16                          ' Inventory Replenishment Module
    Private Const kStatusUnapproved = 0                   ' Transfer Order Status
    Private Const kStatusOpen = 1                         ' Transfer Order Status
    
    
    ' grdTransferRules drop down column object variables
    Private moStatLstSurchargeAmountMethod      As New clsStaticList
    Private moStatLstAutomaticApproval          As New clsStaticList
    
    ' grdTransferRules lookup column (GridNav) object variables
    Private moGrdNavRcvgWhse                    As clsGridLookup
    Private moGrdNavShipMethod                  As clsGridLookup
    Private moGrdNavTransitWhse                 As clsGridLookup
    Private mbDoNotLeaveCell                    As Boolean
    Private mvValueHolder                       As Variant
    Private mbSurchargePercentIsNonZero         As Boolean
    Private mbSurchargeAmountIsNonZero          As Boolean
    Private mbFreightAndOrSurchargesExpensed    As Boolean
    
    ' grdWhseZone
    Private Const kMaxZoneCols = 4
    Private Const kColWhseZoneKey = 1
    Private Const kColWhseZoneID = 2
    Private Const kColDescription = 3
    Private Const kColSortOrder = 4
    Private mbDontLeaveCell      As Boolean  ' Used to reset focus back to a cell with invalid data.
    Private mbUnloadFlag         As Boolean   'This flag will be used in Form QueryUnload
    Private mlNNBins             As Long
    Private mbDeleteZoneIntWhs   As Boolean
    Private mbDeleteZoneReference As Boolean
    Private mbDeleteZones        As Boolean
    
    
    Private mlImmedPickRptSettingKey As Long
    Private mlWillCallPickRptSettingKey As Long
    Private mlImmedInvcRptSettingKey As Long
    'Public moSettings         As clsSettings
    Private Const kmsInvoiceProgramName = "ARZTC001"
    Private Const kmsPrintProgramName = "SOZDD101"
    Private Const kNoneSetting = "(None)"
    

    
Const VBRIG_MODULE_ID_STRING = "IMZMK001.FRM"

Private Function bIsCircWhseRef(lTgtWhseKey As Long, iCostOrPrice As Integer) As Boolean
' This function determines if the Costing Warehouse chosen for this warehouse,
' creates a circular reference either by itself or in context of this warehouse
'
' Circular Reference
'-------------------
'  If Warehosue W1 has W2 for Costing Whse and W2 points to W3 for costing and
' W3 points back to W1 for Costing warehouse, a circular reference occurs. The
' circular referene situation is not limited to the above example - there could be
' just 2 warehouses in the circle or more. To avoid this we check the Costing warehouse
' for circular reference at Setup warehouse. For this, we look at all the warehouses in the
'whse - Costing Whse chain of the Costing Whse chosen. If the Main Whse / Costing Whse appears
' in the chain, an error is raised.
Dim lSourceWhse As Long
Dim lCostWhse As Long
Dim lRetVal As Long

On Error GoTo ExpectedErrorRoutine

bIsCircWhseRef = False

'Check for empty  Costing Warehouse Keys
If lTgtWhseKey = 0 Then
    Exit Function
End If
'Check for Empty Source Whse
'If empty, check only for Circular reference of the
' Costing warehouse
lSourceWhse = glGetValidLong(lkuMain.KeyValue)

If lSourceWhse <> 0 And lSourceWhse = lTgtWhseKey Then
        bIsCircWhseRef = False
        Exit Function
    
End If


'Exeute SP in Validation mode
With moClass.moAppDB
    .SetInParam msCompanyID
    .SetInParam lSourceWhse
    .SetInParam lTgtWhseKey
    .SetInParam iCostOrPrice
    .SetOutParam lRetVal
    .ExecuteSP "spimCheckCircWhseReference"
    lRetVal = glGetValidLong(.GetOutParam(5))
    .ReleaseParams
End With

'Check Return Value
Select Case lRetVal
Case -1                 'Unexpected Error
    bIsCircWhseRef = False
Case kCostWhseCircRef   'Circular Reference Error
    bIsCircWhseRef = False
Case 1                  'Successfull execution
    bIsCircWhseRef = True
Case Else
    bIsCircWhseRef = False
End Select
Exit Function

ExpectedErrorRoutine:

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "bIsCircWhseRef", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Function
    End Select
'+++ VB/Rig End +++
End Function


Private Function bRetUseBinStatus(lWhseKy As Long, bIsUseBins As Boolean) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL    As String
    Dim sMsg    As String
    
    bRetUseBinStatus = False
    
    If bIsUseBins = True Then
        sMsg = gsStripChar(chkUseBins.Caption, "&")
    Else
        sMsg = gsStripChar(chkTrackQtyAtBin.Caption, "&")
    End If
    
    '-- Holding Bin Check
    sSQL = "timWhseBin.WhseKey = " & lWhseKy _
& " AND timWhseBin.Type = 3" _
& " AND timWhseBinInvt.QtyOnHand > 0" _
& " AND timWhseBinInvt.WhseBinKey = timWhseBin.WhseBinKey"
    
    If moClass.moAppDB.Lookup("COUNT(*)", "timWhseBin, timWhseBinInvt", sSQL) <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMChkHolding, sMsg
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    '-- Physical Tran Check
    sSQL = "timWarehouse.CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & " AND timWarehouse.WhseKey = timWhseBin.WhseKey"
    sSQL = sSQL & " AND timPhysCountTran.WhseBinKey = timWhseBin.WhseBinKey"
    sSQL = sSQL & " AND timWarehouse.WhseKey = " & lWhseKy

    If moClass.moAppDB.Lookup("COUNT(*)", "timPhysCountTran WITH (NOLOCK), timWarehouse WITH (NOLOCK) , timWhseBin WITH (NOLOCK)", sSQL) <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMChkPhysTran, sMsg
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    '-- Pending Tran Check
    sSQL = "timWarehouse.CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & " AND timPendInvtTran.WhseKey = timWarehouse.WhseKey"
    sSQL = sSQL & " AND timWarehouse.WhseKey = " & lWhseKy
    
    If moClass.moAppDB.Lookup("COUNT(*)", "timPendInvtTran , timWarehouse", sSQL) <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMChkPendingTran, sMsg
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    bRetUseBinStatus = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRetUseBinStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bDenyControlFocus() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bDenyControlFocus = False
    
    If (Len(Trim$(lkuMain)) = 0) Then
        If lkuMain.Enabled And lkuMain.Visible Then
            lkuMain.SetFocus
            bDenyControlFocus = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDenyControlFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "IMZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "IMZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************
' Description:
'    This routine will bind Right-Click context menus to
'    the form and controls.
'*******************************************************
    With moContextMenu
        Set .Form = frmWareHouseMNT
        .BindGrid moGM, grdTransferRules.hwnd
        .BindGrid moGMZoneGrid, grdWhseZones.hwnd
        .Bind "IMDA08", lkuMain.hwnd, kEntTypeIMWarehouse
        .Bind "IMDA08", lkuCostWhse.hwnd, kEntTypeIMWarehouse
        .Bind "IMDA08", lkuPriceWhse.hwnd, kEntTypeIMWarehouse
        
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine will bind fields on the form to fields in the database.
'************************************************************************
    '-- create a new data Manager Form class
    Set moDmForm = New clsDmForm
    
    '-- Bind Form
    With moDmForm
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        
        .CompanyId = msCompanyID
        .AppName = Me.Caption
        .Table = "timWarehouse"
        .UniqueKey = "WhseID"
        .SaveOrder = 3
        
        .Bind Nothing, "WhseKey", SQL_INTEGER
        .Bind lkuMain, "WhseID", SQL_CHAR
        .Bind txtDescription, "Description", SQL_CHAR
        
        .Bind chkIntransitWhs, "Transit", SQL_SMALLINT
        .Bind chkTrackQtyAtBin, "TrackQtyAtBin", SQL_SMALLINT
        
        .Bind numMaxQulLeadTimePctHidden, "MaxQualLeadTimePct", SQL_DECIMAL, kDmSetNull
        .Bind numMinQulLeadTimePctHidden, "MinQualLeadTimePct", SQL_DECIMAL, kDmSetNull
        
        .BindLookup lkuSTaxSchedule, kDmSetNull
        .BindLookup lkuWhsOvrdSegVal, kDmSetNull

        .Bind chkShipComplete, "ShipComplete", SQL_SMALLINT
        .Bind chkPickFromSO, "AllowImmedPickFromSO", SQL_SMALLINT
        .Bind chkShipFromPick, "AllowImmedShipFromPick", SQL_SMALLINT
        .Bind chkUseBins, "UseBins", SQL_SMALLINT
        .Bind chkUseZones, "UseZones", SQL_SMALLINT
        
        .BindLookup lkuCostWhse, kDmSetNull
        .BindLookup lkuPriceWhse, kDmSetNull
        
        .Bind Nothing, "WhseMgrCntctKey", SQL_INTEGER, kDmSetNull
        
        .Bind Nothing, "MailAddrKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "ShipAddrKey", SQL_INTEGER
        
        .BindComboBox ddnReorderMeth, "ReordMeth", SQL_SMALLINT, kDmUseItemData
        .Bind numCostOfReplenishment, "CostToReplenish", SQL_DECIMAL
        .Bind numCostOfCarryingPctHidden, "CostOfCarry", SQL_DECIMAL
        
        .Bind numTrendPct, "TrendPct", SQL_DECIMAL, kDmSetNull
        .Bind numMaxOrdCycle, "MaxOrderCycle", SQL_SMALLINT, kDmSetNull
        .Bind numMinOrdCycle, "MinOrderCycle", SQL_SMALLINT, kDmSetNull
        .Bind numMaxDmdMultiple, "MaxDemandMult", SQL_DECIMAL
        .Bind numMinDmdMultiple, "MinDemandMult", SQL_DECIMAL
        .Bind numMaxSenlDmdMultiple, "MaxSeasDemandMult", SQL_DECIMAL
        
        .BindComboBox ddnRestockMeth, "RestockMeth", SQL_SMALLINT, kDmUseItemData
        .Bind numRestockRateHidden, "RestockRate", SQL_DECIMAL
        
        .BindComboBox ddnDefPickMethod, "DfltPickMeth", SQL_SMALLINT, kDmUseItemData
        .BindComboBox ddnDefLotPickMethod, "DfltLotPickMethod", SQL_SMALLINT, kDmUseItemData
        
        .Bind Nothing, "ImmedPickRptSettingKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "WillCallPickRptSettingKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "ImmedInvcRptSettingKey", SQL_INTEGER, kDmSetNull
        .BindComboBox ddnImPickDefPrinter, "ImmedPickPrinterDest", SQL_VARCHAR, kDmSetNull
        .BindComboBox ddnCallPickDefPrinter, "WillCallPickPrinterDest", SQL_VARCHAR, kDmSetNull
        .BindComboBox ddnImInvoiceDefPrinter, "ImmedInvcPrinterDest", SQL_VARCHAR, kDmSetNull
        
        .Bind Nothing, "LastRplnsmntUpdate", SQL_DATE, kDmSetNull
        .Bind calLastRankDate, "LastRankDate", SQL_DATE, kDmSetNull
        .Bind Nothing, "LastRankInvtPerKey", SQL_INTEGER, kDmSetNull
        
        .BindLookup glaPurchases, kDmSetNull
        .BindLookup glaSales, kDmSetNull
        .BindLookup glaSalesOffset, kDmSetNull
        .BindLookup glaReturns, kDmSetNull
        .BindLookup glaInventory, kDmSetNull
        .BindLookup glaCostOfSales, kDmSetNull
        .BindLookup glaIssues, kDmSetNull
        .BindLookup glaMiscAdj, kDmSetNull
        .BindLookup glaCostTierAdj, kDmSetNull
        .BindLookup glaPhysicalCount, kDmSetNull
        
        ' Transfer Tab GL Account controls
        .Bind Nothing, "TrnsfrFrtChrgOpt", SQL_SMALLINT   ' stores optFreightCharges
        .Bind Nothing, "TrnsfrSrchrgOpt", SQL_SMALLINT    ' stores optSurcharges
        .BindLookup glaExpenseAcct, kDmSetNull
        .BindLookup glaFreightSurcharge, kDmSetNull
        
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        
        .LinkSource "timInvtPeriod", "InvtPerKey=<<LastRankInvtPerKey>>"
        .Link txtLastRankPeriod, "InvtPer"
        .Link txtLastRankYear, "InvtYear"
        
        .LinkSource "tsmReportSetting", "RptSettingKey=<<ImmedPickRptSettingKey>>"
        .Link ddnImPickPrintSetting, "RptSettingID", SQL_VARCHAR
        
        .LinkSource "tsmReportSetting", "RptSettingKey=<<WillCallPickRptSettingKey>>"
        .Link ddnCallPickPrintSetting, "RptSettingID", SQL_VARCHAR
        
        .LinkSource "tsmReportSetting", "RptSettingKey=<<ImmedInvcRptSettingKey>>"
        .Link ddnImInvPrintSetting, "RptSettingID", SQL_VARCHAR
      
       .Init
    End With
    
    '-- Bind Mail Address
    Set moDmMailAddr = New clsDmForm
    
    With moDmMailAddr
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        
        .AppName = Me.Caption
        .Table = "tciAddress"
        .UniqueKey = "AddrKey"
        
        Set .Parent = moDmForm
        .ParentLink "AddrKey", "MailAddrKey", SQL_INTEGER
        .SaveOrder = 1
        
        .Bind txtMAddrName, "AddrName", SQL_VARCHAR
        .Bind txtMAddress(0), "AddrLine1", SQL_VARCHAR
        .Bind txtMAddress(1), "AddrLine2", SQL_VARCHAR
        .Bind txtMAddress(2), "AddrLine3", SQL_VARCHAR
        .Bind txtMAddress(3), "AddrLine4", SQL_VARCHAR
        .Bind txtMAddress(4), "AddrLine5", SQL_VARCHAR
        .Bind txtMCity, "City", SQL_CHAR
        .BindLookup lkuMPostalCode, kDmSetNull
        .Bind txtMLatitude, "Latitude", SQL_DECIMAL, kDmSetNull
        .Bind txtMLongitude, "Longitude", SQL_DECIMAL, kDmSetNull
        .BindComboBox ddnMCountry, "CountryID", SQL_CHAR, kDmSetNull
        .BindComboBox ddnMState, "StateID", SQL_CHAR
        
        .Init
    End With

    '-- Bing Ship Address
    Set moDmShipAddr = New clsDmForm
    
    With moDmShipAddr
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        
        .AppName = Me.Caption
        .Table = "tciAddress"
        .UniqueKey = "AddrKey"
       
        Set .Parent = moDmForm
        .ParentLink "AddrKey", "ShipAddrKey", SQL_INTEGER
        .SaveOrder = 2
        
        .Bind txtSAddrName, "AddrName", SQL_VARCHAR
        .Bind txtSAddress(0), "AddrLine1", SQL_VARCHAR
        .Bind txtSAddress(1), "AddrLine2", SQL_VARCHAR
        .Bind txtSAddress(2), "AddrLine3", SQL_VARCHAR
        .Bind txtSAddress(3), "AddrLine4", SQL_VARCHAR
        .Bind txtSAddress(4), "AddrLine5", SQL_VARCHAR
        .Bind txtSCity, "City", SQL_CHAR
        .BindLookup lkuSPostalCode, kDmSetNull
        .Bind txtSLatitude, "Latitude", SQL_DECIMAL, kDmSetNull
        .Bind txtSLongitude, "Longitude", SQL_DECIMAL, kDmSetNull
        .BindComboBox ddnSCountry, "CountryID", SQL_CHAR
        .BindComboBox ddnSState, "StateID", SQL_CHAR
        
        .Init
    End With

    Set moDmGrid = New clsDmGrid
    
    With moDmGrid
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdTransferRules
            .Table = "timWhseTrnsfr"
            .UniqueKey = "ShipWhseKey,RcvgWhseKey"
        Set .Parent = moDmForm
        
        .ParentLink "ShipWhseKey", "WhseKey", SQL_INTEGER
        .SaveOrder = 4

        .BindColumn "ShipWhseKey", kColShipWhseKey, SQL_INTEGER               ' hidden column
        .BindColumn "RcvgWhseKey", kColReceivingWarehouseKey, SQL_INTEGER     ' hidden column
        .BindColumn "ShipMethKey", kColShipMethodKey, SQL_INTEGER             ' hidden column
        .BindColumn "LeadTime", kColLeadTimeDays, SQL_SMALLINT
        .BindColumn "SurchargeFixedAmt", kColSurchargeAmount, SQL_DECIMAL
        .BindColumn "SurchargeFixedMeth", kColSurchargeAmountMethodValue, SQL_SMALLINT ' hidden column
        .BindColumn "AutoApproveMeth", kColAutomaticApprovalValue, SQL_SMALLINT ' hidden column
        .BindColumn "SurchargePct", kColSurchargePercentValue, SQL_DECIMAL    ' hidden column
        .BindColumn "TransitWhseKey", kColTransitWarehouseKey, SQL_INTEGER    ' hidden column
        
        .Init
    End With
    
    '-- Bing Whse Zones
    Set moDmZoneGrid = New clsDmGrid
    
    With moDmZoneGrid
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdWhseZones
            .Table = "timWhseZone"
            .UniqueKey = "WhseKey,WhseZoneID"
        Set .Parent = moDmForm
        
        .ParentLink "WhseKey", "WhseKey", SQL_INTEGER
        .SaveOrder = 5
        .OrderBy = "SortOrder"
        
        .BindColumn "WhseZoneKey", kColWhseZoneKey, SQL_INTEGER
        .BindColumn "WhseZoneID", kColWhseZoneID, SQL_CHAR
        .BindColumn "Description", kColDescription, SQL_CHAR
        .BindColumn "SortOrder", kColSortOrder, SQL_INTEGER
        
        .SortByColumn kColSortOrder
        .Init
    End With


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim iActionCode As Integer
    Dim iConfirmUnload As Integer
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant
    Dim lRow As Long
    Dim lCol As Long
    Dim lReturnCode As Long
    
    iActionCode = kDmFailure
    
    mbDmLoad = True
    
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            DoFinish
        
        Case kTbSave
            DoSave
        
        Case kTbCancel, kTbCancelExit
            DoCancel
        
        Case kTbDelete
            DoDelete
        
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            If grdTransferRules.MaxRows - 1 <> 0 Then
                ' The following setfocus event is to prevent LeaveCellValidation from executing.
                tbrMain.SetFocus
                If Not bGridRowsAreValid() Then             ' Validate all rows in grdTransferRules
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If
            End If
            
            If chkIntransitWhs Then
                If glaInventoryIsBlank Then
                    glaInventory.SetFocus
                    Exit Sub
                End If
            End If
            
            'If either the surchargs or freight amounts are expensed, then the GL Expense Acct no
            'must be entered.
            If optFreightCharges(1) Or optSurcharges(1) Then
                If Len(Trim$(glaExpenseAcct.Text)) = 0 Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMInboundExpAcct
                    If glaExpenseAcct.Enabled Then glaExpenseAcct.SetFocus
                    Exit Sub
                End If
            End If

            'Process requested browse move
            If Not bUnloadConfirmed() Then
                Exit Sub
            End If
            
            'Execute requested move.
            lReturnCode = glLookupBrowse(lkuMain, sKey, miFilter, sNewKey)

            'Evaluate outcome of requested browse move.
            Select Case lReturnCode
                Case MS_SUCCESS
                    vParseRet = gvParseLookupReturn(sNewKey)
                    If IsNull(vParseRet) Then Exit Sub
                    If Trim(lkuMain.Text) <> Trim(vParseRet(1)) Then
                        lkuMain.Text = Trim(vParseRet(1))
                        bIsValidID
                    End If

                    txtDescription.SetFocus
                    
                    If chkIntransitWhs.Value = 0 Then
                        tabWarehouse.Tab = 0
                    End If
                    
                Case Else
                    gLookupBrowseError lReturnCode, Me, moClass
            End Select
            
        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'iActionCode = moDmForm.CopyFrom

        Case kTbRenameId
            moDmForm.RenameID
            
        Case kTbPrint
            DoPrint
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case kTbFilter
            miFilter = giToggleLookupFilter(miFilter)

        Case Else
            tbrMain.GenericHandler sKey, Me, moDmForm, moClass
    End Select

    Select Case iActionCode
        Case kDmSuccess
            moClass.lUIActive = kChildObjectInactive
            mbDmLoad = False
        
        Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            mbDmLoad = False
            Exit Sub
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseZones_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkUseZones, True
    #End If
'+++ End Customizer Code Push +++
    If Trim$(lkuMain.Text) = "" Then
        chkUseZones.Value = 0
        Exit Sub
    End If
    
    If chkUseZones.Value = 0 Then
        tabWarehouse.TabEnabled(kTabZone) = False
    Else
        tabWarehouse.TabEnabled(kTabZone) = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkUseZones_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdGetMailToMap_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine

    'Get mapquest map (using MapButton control)
    cmdGetMailToMap.Address1 = Trim(txtMAddress(0).Text)
    cmdGetMailToMap.Address2 = Trim(txtMAddress(1).Text) 'not used?, not sure why it's a parameter
    cmdGetMailToMap.City = Trim(txtMCity.Text)
    cmdGetMailToMap.State = Trim(ddnMState.Text)
    cmdGetMailToMap.PostalCode = Trim(lkuMPostalCode.Text)
    
    Set cmdGetMailToMap.AppDatabase = moClass.moAppDB

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdGetMailToMap_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdGetShipToMap_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine

    'Get mapquest map (using MapButton control)
    cmdGetShipToMap.Address1 = Trim(txtSAddress(0).Text)
    cmdGetShipToMap.Address2 = Trim(txtSAddress(1).Text) 'not used?, not sure why it's a parameter
    cmdGetShipToMap.City = Trim(txtSCity.Text)
    cmdGetShipToMap.State = Trim(ddnSState.Text)
    cmdGetShipToMap.PostalCode = Trim(lkuSPostalCode.Text)
    Set cmdGetShipToMap.AppDatabase = moClass.moAppDB
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdGetShipToMap_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickPrintSetting_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnCallPickPrintSetting, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If ddnCallPickPrintSetting.ListIndex <> glGetValidLong(ddnCallPickPrintSetting.Tag) Then
        mlWillCallPickRptSettingKey = glGetValidLong(ddnCallPickPrintSetting.ItemData(NewIndex))
        
        If mlWillCallPickRptSettingKey = 0 Then
            ddnCallPickPrintSetting.ListIndex = 0
            moDmForm.SetColumnValue "WillCallPickRptSettingKey", ""
        Else
            moDmForm.SetColumnValue "WillCallPickRptSettingKey", mlWillCallPickRptSettingKey
        End If
        
        ddnCallPickPrintSetting.Tag = ddnCallPickPrintSetting.ListIndex
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnCallPickPrintSetting_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvPrintSetting_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnImInvPrintSetting, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If ddnImInvPrintSetting.ListIndex <> glGetValidLong(ddnImInvPrintSetting.Tag) Then
        mlImmedInvcRptSettingKey = glGetValidLong(ddnImInvPrintSetting.ItemData(NewIndex))
        
        If mlImmedInvcRptSettingKey = 0 Then
            ddnImInvPrintSetting.ListIndex = 0
            moDmForm.SetColumnValue "ImmedInvcRptSettingKey", ""
        Else
            moDmForm.SetColumnValue "ImmedInvcRptSettingKey", mlImmedInvcRptSettingKey
        End If
        
        ddnImInvPrintSetting.Tag = ddnImInvPrintSetting.ListIndex
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnImInvPrintSetting_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickPrintSetting_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnImPickPrintSetting, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If ddnImPickPrintSetting.ListIndex <> glGetValidLong(ddnImPickPrintSetting.Tag) Then
        mlImmedPickRptSettingKey = glGetValidLong(ddnImPickPrintSetting.ItemData(NewIndex))
        
        If mlImmedPickRptSettingKey = 0 Then
            ddnImPickPrintSetting.ListIndex = 0
            moDmForm.SetColumnValue "ImmedPickRptSettingKey", ""
        Else
            moDmForm.SetColumnValue "ImmedPickRptSettingKey", mlImmedPickRptSettingKey
        End If
        
        ddnImPickPrintSetting.Tag = ddnImPickPrintSetting.ListIndex
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnImPickPrintSetting_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaInventory_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WarnTransitInvAcct

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "glaInventory_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMZoneGrid.Grid_Change Col, Row
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMZoneGrid.Grid_Click Col, Row

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmZoneGrid.SetRowDirty Row

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_EditChange", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moGMZoneGrid Is Nothing Then
        moGMZoneGrid.Grid_KeyDown KeyCode, Shift
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_KeyDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    With grdWhseZones
        If (.ActiveCol = kColSortOrder) And KeyAscii = 45 Then
            KeyAscii = 0
        End If
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
   
    If mbDontLeaveCell Then
        mbDontLeaveCell = False
        Cancel = True
    Else
        moGMZoneGrid.Grid_LeaveCell Col, Row, NewCol, NewRow
    End If
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_LeaveCell", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMZoneGrid.Grid_LeaveRow Row, NewRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_LeaveRow", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdWhseZones_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMZoneGrid.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_TopLeftChange", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



'RKL DEJ 2017-03-28 (Start)
Private Sub lkuCOASig_Validate(Cancel As Boolean)
    moDmForm.SetDirty True, True
End Sub
'RKL DEJ 2017-03-28 (Stop)

Private Sub moDmForm_DMBeforeDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
If Not bBinsAreUsed Then 'if bins not contain inventory with Quantities
    If grdWhseZones.MaxRows > 1 Then ' but zones exists in the timWhseZones
        bDeleteZones lkuMain.KeyValue
        bDeleteBins lkuMain.KeyValue
        bValid = True
    End If
Else
    giSotaMsgBox Me, moClass.moSysSession, 166233 'kImsgcannotDeleteWhse
    bValid = False
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeDelete", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGrid_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If grdTransferRules.MaxRows - 1 <> 0 Then
        If Not bGridRowsAreValid() Then                 ' Validate all rows in grdTransferRules
            bValid = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmGrid_DMGridValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub moDmZoneGrid_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 If (grdWhseZones.MaxRows - 1) <> 0 Then
    If Trim$(gsGridReadCellText(grdWhseZones, lRow, kColWhseZoneID)) <> "" Then
        moDmZoneGrid.SetColumnValue lRow, "WhseKey", lkuMain.KeyValue
        moDmZoneGrid.SetColumnValue lRow, "WhseZoneKey", lGetNextSurrogateKey("timWhseZone")
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmZoneGrid_DMGridBeforeInsert", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function lGetNextSurrogateKey(tblName As String) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lKeyVal As Long
    
    lKeyVal = 0
    With moClass.moAppDB
        .SetInParam tblName
        .SetOutParam lKeyVal
        .ExecuteSP ("spGetNextSurrogateKey")
        lKeyVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    lGetNextSurrogateKey = lKeyVal
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lGetNextSurrogateKey", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub moDmZoneGrid_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'check for presence of ZoneID
    If (grdWhseZones.MaxRows - 1) <> 0 Then
        If lRow <> grdWhseZones.MaxRows Then
            If Trim$(gsGridReadCellText(grdWhseZones, lRow, kColWhseZoneID)) = "" Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, "Zone" 'Zone can't be blank
                gGridSetActiveCell grdWhseZones, lRow, kColWhseZoneID
                bValid = False
                mbUnloadFlag = False
            End If
        End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    Else
        moDmZoneGrid.SetDirty False
    
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmZoneGrid_DMGridValidate", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMZoneGrid_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moDmZoneGrid.SetRowDirty (lRow)
    moGMZoneGrid.Grid_Change lCol, lRow

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMZoneGrid_CellChange", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMZoneGrid_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sWhseZoneID     As String
    Dim lWhseZoneKey    As Long
    Dim lWhseKey        As Long

    lWhseKey = glGetValidLong(moDmForm.GetColumnValue("WhseKey"))
    lWhseZoneKey = glGetValidLong(gsGridReadCellText(grdWhseZones, grdWhseZones.ActiveRow, _
                                    kColWhseZoneKey))
    sWhseZoneID = gsGetValidStr(gsGridReadCellText(grdWhseZones, grdWhseZones.ActiveRow, _
                                kColWhseZoneID))

    If bZoneExistWithBin(lWhseKey, lWhseZoneKey) Then
        ' Msgbox "This zone is used as the zone for (NN) bins. Delete this zone?(kImsgZoneInUse)
        If giSotaMsgBox(Me, moClass.moSysSession, kImsgZoneInUse, mlNNBins) = vbYes Then
            moDmZoneGrid.SetRowDirty grdWhseZones.ActiveRow
            mbDeleteZoneReference = True
        Else
            bContinue = False
            mbDeleteZoneReference = False
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMZoneGrid_GridBeforeDelete", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub navRcvgWhse_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    txtGrdNavRcvgWhse.Text = gsGridReadCellText(grdTransferRules, glGridGetActiveRow(grdTransferRules), _
kColReceivingWarehouse)
                             
    navRcvgWhse.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND WhseID <> " & _
gsQuoted(moDmForm.GetColumnValue("WhseID")) & _
" AND Transit = 0"
    gcLookupClick Me, navRcvgWhse, txtGrdNavRcvgWhse, "WhseID"
    moGM.LookupClicked

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navRcvgWhse_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkTrackQtyAtBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkTrackQtyAtBin, True
    #End If
'+++ End Customizer Code Push +++
    
    If Not mbDmLoad And mbWMIsLicensed = True Then
        If Not bNewWarehouse Then
            If chkUseBins.Value = 1 And chkTrackQtyAtBin.Value = 0 Then
            
                ' Check for bins that are not empty
                If mbLoadTrackQtyAtBin Then ' but only if checkbox was already checked when the form loaded
                    If bBinsAreUsed() Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMBinsUsed
                        chkTrackQtyAtBin.Value = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                
                    Rem Check for Holding Bin , Pending Transactions
                    If bRetUseBinStatus(lkuMain.KeyValue, False) = False Then
                        chkTrackQtyAtBin.Value = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                    If giSotaMsgBox(Me, moClass.moSysSession, kIMmsgTrackQtyAtBin) = kretYes Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    Else
                        chkTrackQtyAtBin.Value = 1
                    End If
                End If
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkTrackQtyAtBin_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ChkUseBins_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkUseBins, True
    #End If
'+++ End Customizer Code Push +++

If Trim$(lkuMain.Text) = "" Then
        chkUseBins.Value = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
End If
    If Not mbDmLoad Then
        If chkUseBins.Value = 0 Then
            If Not bNewWarehouse Then
            
                ' Check for bins that are not empty
                If mbLoadUseBins Then ' but only if checkbox was already checked when the form loaded
                    If bBinsAreUsed() Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMBinsUsed
                        chkUseBins.Value = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                End If
                
                'Checking for Transactions
                If moDmForm.GetColumnValue("UseBins") = 1 Then
                    If bRetUseBinStatus(lkuMain.KeyValue, True) = False Then
                        chkUseBins.Value = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                End If
                           
                   
                If chkTrackQtyAtBin.Value = 1 Then
                    If giSotaMsgBox(Me, moClass.moSysSession, kIMmsgUseBins) = kretYes Then
                    Else
                        chkTrackQtyAtBin.Value = 1
                        chkUseBins = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                End If
                
                'Checking for Zones
                If chkUseZones.Value = 1 Then
                    If Not bZonesCheck Then
                        chkUseZones.Value = 1
                        chkUseZones.Enabled = False
                        Exit Sub
                    End If
                End If
            
        End If
            chkTrackQtyAtBin.Value = 0
            chkTrackQtyAtBin.Enabled = False
            chkUseZones.Value = 0
            chkUseZones.Enabled = False
            tabWarehouse.TabEnabled(kTabZone) = False
        Else
            chkTrackQtyAtBin.Enabled = mbWMIsLicensed
            If chkUseZones.Value = 0 And chkUseZones.Enabled = False Then
                chkUseZones.Enabled = mbWMIsLicensed
            End If
        End If
   
           
        If chkUseBins.Value = 1 And mbWMIsLicensed Then
            chkTrackQtyAtBin.Enabled = True
            chkUseZones.Enabled = True

        Else
            chkTrackQtyAtBin.Value = 0
            chkTrackQtyAtBin.Enabled = False
            chkUseZones.Value = 0
            chkUseZones.Enabled = False
            tabWarehouse.TabEnabled(kTabZone) = False
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ChkUseBins_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdContact_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdContact, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim iSaveStatus  As Integer
    Dim iRetVal As Integer
    Dim lCntctKey As Long

    Const kFromAOF = False

    If lkuMain.EnabledText Then
        lkuMain.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    'If either the surchargs or freight amounts are expensed, then the GL Expense Acct no
    'must be entered.
    If optFreightCharges(1) Or optSurcharges(1) Then
        If Len(Trim$(glaExpenseAcct.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInboundExpAcct
            If glaExpenseAcct.Enabled Then glaExpenseAcct.SetFocus
            Exit Sub
        End If
    End If
    'Check for validation
    If Not moValMgr.IsValidDirtyCheck Then Exit Sub
    
    If moDmForm.IsDirty Or moDmMailAddr.IsDirty Or moDmShipAddr.IsDirty Then
        iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveChanges)
        If iRetVal = kretNo Or iRetVal = vbCancel Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        Else
           
            iSaveStatus = moDmForm.Save(True)
            If iSaveStatus <> kDmSuccess Then Exit Sub
        End If
        
    End If
    
    If moCIContact Is Nothing Then
        Set moCIContact = goGetSOTAChild(moClass.moFramework, moSotaObjects, kclsCIContactMNT, ktskCIContactMNT, kAOFRunFlags, kContextAOF)
        If moCIContact Is Nothing Then
            Exit Sub
        Else ' Only one warehouse manager can be defined, so hide the browse buttons on the common contacts screen.
            If moCIContact.LoadUI(kContextAOF) = False Then
                Exit Sub
            Else
                moCIContact.mfrmMain.CONTROLS("sbrMain").BrowseVisible = False
            End If
        End If
    End If
              
    mlPrimaryCntctKey = lkuMain.KeyValue
    moCIContact.EnterContacts mlPrimaryCntctKey, 0, lkuMain.Text & " " & msWarehouseMgr, kEntTypeIMWarehouse, msCountryID, msPhoneMask, 1, False, "", False
           
    If Not IsEmpty(lkuMain) Then
        If moClass.moAppDB.Lookup("COUNT(*)", "tciContact", "CntctOwnerKey = " & mlPrimaryCntctKey & " AND EntityType = 703") <> 0 Then
            lCntctKey = Val(moClass.moAppDB.Lookup("CntctKey", "tciContact", "CntctOwnerKey = " & mlPrimaryCntctKey & " AND EntityType = 703"))
            moClass.moAppDB.ExecuteSQL "UPDATE timWarehouse SET WhseMgrCntctKey = " & lCntctKey & " WHERE WhseKey = " & mlPrimaryCntctKey
        Else
            moClass.moAppDB.ExecuteSQL "UPDATE timWarehouse SET WhseMgrCntctKey = NULL WHERE WhseKey = " & mlPrimaryCntctKey
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdContact_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMCountry_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnMCountry, True
    #End If
'+++ End Customizer Code Push +++
        
    If ddnMCountry.ListIndex <> glGetValidLong(ddnMCountry.Tag) Then
        
        ddnMCountry.Tag = ddnMCountry.ListIndex
        
        txtMCity = Empty
        ddnMState.ListIndex = -1
        lkuMPostalCode = Empty
        ddnMState.Tag = ""
        txtMCity.Tag = ""
        lkuMPostalCode.Tag = ""

        RefreshStates ddnMState, ddnMCountry
        SetPostalCodeMask lkuMPostalCode, ddnMCountry
    
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnMCountry_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnRestockMeth_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnRestockMeth, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    
    If PrevIndex <> NewIndex Then
        numRestockRate.Value = 0
    End If
    If ddnRestockMeth.ItemData = 1 Then
        numRestockRate.Enabled = False
    Else
        numRestockRate.Enabled = True
        numRestockRate.ValueMax = 999999.999999
        numRestockRate.DecimalPlaces = 6
        numRestockRate.IntegralPlaces = 6
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnRestockMeth_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIntransitWhs_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkIntransitWhs, True
    #End If
'+++ End Customizer Code Push +++
    Dim oRs As Object
    Dim sSQL As String
    Dim lRow As Long

    If (Len(Trim$(lkuMain.Text)) = 0 _
    Or Not mbWMIsLicensed) Then
        chkIntransitWhs.Value = 0

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    If chkIntransitWhs.Value = 1 Then
        'Added By Amitabh on 03-09-99 To Fix Usability Bug # 11214
        'If Inventory exist for a current warehouse. User should not be allowed to change in to Transit Warehosue
        If moDmForm.State = kDmStateEdit Then
            sSQL = "SELECT timInventory.WhseKey FROM timInventory, timWarehouse WHERE timInventory.WhseKey = " & lkuMain.KeyValue & " And timInventory.WhseKey = timWarehouse.WhseKey And timWarehouse.Transit = 0 "
            Set oRs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, 0)
            
            If Not oRs.IsEOF Then
                giSotaMsgBox Me, moClass.moSysSession, kMsgTansitNotAllowed, lkuMain.Text
                chkIntransitWhs.Value = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
        End If
        
        If moDmForm.State = kDmStateAdd Then
            For lRow = 1 To grdWhseZones.MaxRows - 1
                If gsGetValidStr(gsGridReadCellText(grdWhseZones, lRow, _
                        kColWhseZoneID)) <> "" Then
                    moDmZoneGrid.Clear
                    mbDeleteZoneIntWhs = True
                    moDmZoneGrid.SetDirty False
                End If
            Next

            chkUseZones.Value = 0
            chkUseZones.Enabled = False
        End If
            
        mbDmLoad = True
        InitializeMainTab
        InitializeAddressTab
        InitializeReplenishTab
        mbDmLoad = False
        
        If moDmMailAddr.State <> kDmStateNone Then
            moDmMailAddr.Clear
        End If
        moDmMailAddr.SetDirty True
        
        If moDmShipAddr.State <> kDmStateNone Then
            moDmShipAddr.Clear
        End If
        moDmShipAddr.SetDirty True
        
        tabWarehouse.TabEnabled(kTabMain) = False
        tabWarehouse.TabEnabled(kTabAddress) = False
        tabWarehouse.TabEnabled(kTabReplenishment) = True
        DisableControlsOnReplTab
        tabWarehouse.TabEnabled(kTabGLAccount) = True
        tabWarehouse.TabEnabled(kTabTransfer) = False
        tabWarehouse.TabEnabled(kTabZone) = False
        
        tabWarehouse.Tab = kTabGLAccount
    Else
        tabWarehouse.TabEnabled(kTabMain) = True
        tabWarehouse.TabEnabled(kTabAddress) = True
        tabWarehouse.TabEnabled(kTabReplenishment) = True
        EnableControlsOnReplTab
        tabWarehouse.TabEnabled(kTabGLAccount) = True
        tabWarehouse.TabEnabled(kTabTransfer) = mbWMIsLicensed
        tabWarehouse.TabEnabled(kTabZone) = False
        tabWarehouse.Tab = kTabMain
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkIntransitWhs_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DisableControlsOnReplTab()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Disable all controls on the Replenishment tab.
    numCostOfCarryingPct.Enabled = False
    ddnReorderMeth.Enabled = False
    numCostOfReplenishment.Enabled = False
    numTrendPct.Enabled = False
    numMaxOrdCycle.Enabled = False
    numMinOrdCycle.Enabled = False
    numMaxDmdMultiple.Enabled = False
    numMinDmdMultiple.Enabled = False
    numMaxSenlDmdMultiple.Enabled = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisableControlsOnReplTab", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub EnableControlsOnReplTab()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Enable all controls on the Replenishment tab.
    numCostOfCarryingPct.Enabled = True
    ddnReorderMeth.Enabled = True
    numCostOfReplenishment.Enabled = True
    numTrendPct.Enabled = True
    numMaxOrdCycle.Enabled = True
    numMinOrdCycle.Enabled = True
    numMaxDmdMultiple.Enabled = True
    numMinDmdMultiple.Enabled = True
    numMaxSenlDmdMultiple.Enabled = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EnableControlsOnReplTab", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDemandVar_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDemandVar, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim iRetVal As Integer
    Dim iStatus As Integer

    If lkuMain.EnabledText Then
        lkuMain.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    If moDmForm.IsDirty Or moDmMailAddr.IsDirty Or moDmShipAddr.IsDirty Then
        iRetVal = giSotaMsgBox(Nothing, moClass.moSysSession, kmsgSaveChanges)

        If iRetVal = kretNo Or iRetVal = vbCancel Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        Else
            iStatus = moDmForm.Save(True)
            
            If iStatus <> kDmSuccess Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
        End If
    End If
    
    Set moIMDemandVar = goGetSOTAChild(moClass.moFramework, moSotaObjects, ktskIMDemandVarCLS, ktskIMDemandVarMNT, kAOFRunFlags, kContextAOF)
    
    If Not moIMDemandVar Is Nothing Then
        moIMDemandVar.bAddOnTheFly moDmForm.GetColumnValue("WhseKey")
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDemandVar_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSCountry_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnSCountry, True
    #End If
'+++ End Customizer Code Push +++
        
    If ddnSCountry.ListIndex <> glGetValidLong(ddnSCountry.Tag) Then
        
        ddnSCountry.Tag = ddnSCountry.ListIndex
        
        txtSCity = Empty
        ddnSState.ListIndex = -1
        lkuSPostalCode = Empty
        ddnSState.Tag = ""
        txtSCity.Tag = ""
        lkuSPostalCode.Tag = ""

        RefreshStates ddnSState, ddnSCountry
        SetPostalCodeMask lkuSPostalCode, ddnSCountry
    
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnSCountry_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Description:
'    Form_KeyDown traps the hot keys when the KeyPreview property
'    of the form is set to True.
'****************************************************************
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
            
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description:
'    Form_KeyPress traps the keys pressed when the KeyPreview property
'    of the form is set to True.
'*********************************************************************

    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                ' Had to put this IF statement; otherwise, hitting Return while the
                ' focus was on a grid cell makes it skip one cell since the
                ' gProcessSendKeys is sending an extra tab
                If (Me.ActiveControl.Name <> "grdTransferRules") Then
                    If (Me.ActiveControl.Name <> "grdWhseZones") Then
                        gProcessSendKeys "{TAB}"
                    End If
                End If
                
                KeyAscii = 0
                
            End If
        
        Case Else
    
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs   As Object
    Dim rsDesc   As Object
    'Set moSettings = New clsSettings

    '-- assign system object properties to module-level variables
    With moClass.moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msCurrID = .CurrencyID
        mbIRIsActivated = moClass.moSysSession.IsModuleActivated(kModuleIR)
        If CBool(.IsModuleActivated(kModuleWM)) = True Then
            mbWMIsLicensed = .IsModuleLicensed(kModuleWM)
        Else
            mbWMIsLicensed = False
        End If
     End With
    
    chkIntransitWhs.Enabled = mbWMIsLicensed
    chkUseZones.Enabled = mbWMIsLicensed
    tabWarehouse.TabEnabled(kTabZone) = mbWMIsLicensed
    chkTrackQtyAtBin.Enabled = mbWMIsLicensed
    
    mbDeleteZoneReference = False
    mbDeleteZones = False
    bNewWarehouse = True
    bChkNavClick = True

    'Enable inbound transfers check
    bCheckInboundTransfer = True

    '-- Setup navigator restrict clause
    msNavRestrict = "CompanyID = " & kSQuote & msCompanyID & kSQuote
    
    '-- Setup necessary properties for Options class
    With moOptions
        Set .oSysSession = moClass.moSysSession
        Set .oAppDB = moClass.moAppDB
        .sCompanyID = msCompanyID
    End With
    
    '-- get the country and phone mask
    msCountryID = moClass.moAppDB.Lookup("CountryID", "tsmCompany", msNavRestrict)
    msPhoneMask = moClass.moAppDB.Lookup("PhoneMask", "tsmCountry", "CountryID = " & gsQuoted(msCountryID))
    
    'Set form caption
    Me.Caption = gsBuildString(kSetupWarehouses, moClass.moAppDB, moClass.moSysSession)

    'Set the caption for the Warehouse Manager Contact command button
    msWarehouseMgr = gsBuildString(kIMWarehouseMgr, moClass.moAppDB, moClass.moSysSession)
    Me.cmdContact.Caption = msWarehouseMgr & "..."
    msWarehouseMgr = gsStripAmpersand(msWarehouseMgr)

    'Remove memo button from toolbar
    tbrMain.RemoveButton kTbMemo

    sbrMain.MessageVisible = False
    Set sbrMain.Framework = moClass.moFramework
    
    Set ddnMCountry.DBObject = moClass.moAppDB
    Set ddnSCountry.DBObject = moClass.moAppDB
    Set ddnMState.DBObject = moClass.moAppDB
    Set ddnSState.DBObject = moClass.moAppDB

    
    
    ddnReorderMeth.InitStaticList moClass.moAppDB, "timWarehouse", "ReordMeth", mlLanguage
    'Set ddnReorderMeth.DBObject = moClass.moAppDB
    'ddnReorderMeth.DefaultItemData = 0
    
    'Set ddnRestockMeth.DBObject = moClass.moAppDB
    'ddnRestockMeth.DefaultItemData = 0
    ddnRestockMeth.InitStaticList moClass.moAppDB, "timWarehouse", "RestockMeth", mlLanguage
    ddnDefPickMethod.InitStaticList moClass.moAppDB, "timWarehouse", "DfltPickMeth", mlLanguage
    ddnDefLotPickMethod.InitStaticList moClass.moAppDB, "timWarehouse", "DfltLotPickMethod", mlLanguage
    
      
    '-- bind form fields to database fields
    BindForm
    FormatGrid
    '-- setup and bind right click context menus to controls
    BindGM
    BindContextMenu
   
     'Initialize the report settings combo box.
        
    InitializeSettingsCombo ddnImPickPrintSetting
    InitializeSettingsCombo ddnCallPickPrintSetting
    InitializeSettingsCombo ddnImInvPrintSetting

    
        'populate list of print devices
    ddnImPickDefPrinter.Clear
    BuildPrintDeviceCombo Me, ddnImPickDefPrinter.Name
    ddnCallPickDefPrinter.Clear
    BuildPrintDeviceCombo Me, ddnCallPickDefPrinter.Name
    ddnImInvoiceDefPrinter.Clear
    BuildPrintDeviceCombo Me, ddnImInvoiceDefPrinter.Name
    
    
    '-- initialize security level
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm)
    
    If miSecurityLevel = kSecLevelDisplayOnly Then
        cmdContact.Enabled = False
        cmdDemandVar.Enabled = False
    End If
    
    BindVM
    
    '-- initialize browse filter tracking
    miFilter = RSID_UNFILTERED
    
    '-- used for determining if the Main navigator is open
    mbInNavigator = False
    
    '-- used for determining if a Click event occurred on a combo box
    ' because of the user or through code
    mbManualComboSelect = False
    
    '-- used for determining if text boxes are being changed by the user
    '-- or because data Manager is populating them
    mbDmLoad = False
    
    sbrMain.Status = SOTA_SB_START

    'Initialize all lookups
    
    With lkuMain
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        If mbWMIsLicensed Then
            .RestrictClause = msNavRestrict
        Else
            .RestrictClause = msNavRestrict & " AND Transit = 0"
        End If
        
    End With

    With lkuSTaxSchedule
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With

    With lkuCostWhse
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        .RestrictClause = msNavRestrict & " and Transit = 0"
    End With

    With lkuPriceWhse
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
        
        .RestrictClause = msNavRestrict & " and Transit = 0"
    End With

    With lkuMPostalCode
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With

    With lkuSPostalCode
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With
    
    'RKL DEJ 2017-03-21 (START)
    With lkuCOASig
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB
    End With
    'RKL DEJ 2017-03-21 (STOP)
    
    glaPurchases.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaSales.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaSalesOffset.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaReturns.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaInventory.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaCostOfSales.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaIssues.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaMiscAdj.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaCostTierAdj.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaPhysicalCount.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    ' Transfer Tab GL Account Controls
    glaExpenseAcct.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    glaFreightSurcharge.Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
    
    ' Status = 1 means GL accounts that are Active
    glaPurchases.RestrictClause = "Status = 1"
    glaSales.RestrictClause = "Status = 1"
    glaSalesOffset.RestrictClause = "Status = 1"
    glaReturns.RestrictClause = "Status = 1"
    glaInventory.RestrictClause = "Status = 1"
    glaCostOfSales.RestrictClause = "Status = 1"
    glaIssues.RestrictClause = "Status = 1"
    glaMiscAdj.RestrictClause = "Status = 1"
    glaCostTierAdj.RestrictClause = "Status = 1"
    glaPhysicalCount.RestrictClause = "Status = 1"
    
    ' Transfer Tab GL Account Controls
    glaExpenseAcct.RestrictClause = "Status = 1"
    glaFreightSurcharge.RestrictClause = "Status = 1"

    'Initialize Replenishment tab
    InitializeReplenishTab
    
    InitializeGLAccounts
    
    InitializeTransferTab
    
    InitializePickShipTab
    
    '-- Initialize warehouse override segment.
    mlWhseOvrdSegKey = moOptions.IM("WhseOvrdSegKey")

    lblWhsOvrdSegVal.Visible = (mlWhseOvrdSegKey <> 0)
    lkuWhsOvrdSegVal.Visible = (mlWhseOvrdSegKey <> 0)

    If mlWhseOvrdSegKey <> 0 Then
        lblWhsOvrdSegVal.Caption = moClass.moAppDB.Lookup("Description", "tglSegment", "SegmentKey = " & mlWhseOvrdSegKey)
    End If
    
    With lkuWhsOvrdSegVal
        Set .Framework = moClass.moFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moClass.moAppDB

        .RestrictClause = "SegmentKey = " & Format$(mlWhseOvrdSegKey)
    End With

    pnlTabWarehouse(0).Enabled = True
    pnlTabWarehouse(1).Enabled = False
    pnlTabWarehouse(2).Enabled = False
    pnlTabWarehouse(3).Enabled = False
    pnlTabWarehouse(4).Enabled = False
    pnlTabWarehouse(5).Enabled = False
    pnlTabWarehouse(6).Enabled = False

    moDmForm.SetDirty False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
   On Error GoTo ExpectedErrorRoutine

    Dim iConfirmUnload As Integer
   
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
    
   ' This tbrMain.SetFocus is a workaround. If removed, and moDmForm is dirty with an invalid
   ' input somewhere in the grdTransferRules, the Data Manager message box prompting to save
   ' changes will come up first, and the LeaveCellValidation routine somehow gets fired and
   ' comes up with its message box. Although one would think that Data Manager's Save Changes
   ' prompt is modal, it somehow allows program execution of the LeaveCellValidation.
   ' So please DO NOT remove this statement.
   On Error Resume Next
   tbrMain.SetFocus
   On Error GoTo ExpectedErrorRoutine
   
    If grdTransferRules.MaxRows - 1 <> 0 Then
        If Not bGridRowsAreValid() Then                 ' Validate all rows in grdTransferRules
            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If

    If chkIntransitWhs.Value = 1 Then
        If glaInventoryIsBlank Then
            glaInventory.SetFocus
            Cancel = True
            Exit Sub
        End If
    End If

    '-- reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False
    
    'If either the surchargs or freight amounts are expensed, then the GL Expense Acct no
    'must be entered.
    If Not moDmForm Is Nothing Then
        If moDmForm.State <> kDmStateNone Then
            If optFreightCharges(1) Or optSurcharges(1) Then
                If Len(Trim$(glaExpenseAcct.Text)) = 0 Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMInboundExpAcct
                    If glaExpenseAcct.Enabled Then glaExpenseAcct.SetFocus
                    GoTo CancelShutDown
                End If
            End If
        End If
    End If
    If moClass.mlError = 0 Then
        If Not moValMgr.IsValidDirtyCheck Then GoTo CancelShutDown
        '-- if the form is dirty, cancel the form close
        '-- if the form is dirty, prompt the user to save the record
        mbManualComboSelect = True
        
        'If form is dirty and TrackQty is checked, avoid displaying messages at unload time.
        mbDmLoad = True
        
        iConfirmUnload = moDmForm.ConfirmUnload()
        
        mbDmLoad = False

        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                
            Case Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
        End Select
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then
            GoTo CancelShutDown
        End If
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlRunMode
                    Case kWhseRunDD
                        Me.Hide
                        GoTo CancelShutDown

                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If

    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    
    If Not moCIContact Is Nothing Then
        Set moCIContact = Nothing
    End If
    
    If Not moIMDemandVar Is Nothing Then
        Set moIMDemandVar = Nothing
    End If
    
    If Not moDmMailAddr Is Nothing Then
        moDmMailAddr.UnloadSelf
        Set moDmMailAddr = Nothing
    End If
    
    If Not moDmShipAddr Is Nothing Then
        moDmShipAddr.UnloadSelf
        Set moDmShipAddr = Nothing
    End If
    
    If Not moDmForm Is Nothing Then
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If
    
    If Not moValMgr Is Nothing Then
        moValMgr.UnloadSelf
        Set moValMgr = Nothing
    End If
    
    ' Transfer Tab grid
    
    If (Not (moDmGrid Is Nothing)) Then
        moDmGrid.UnloadSelf
        Set moDmGrid = Nothing
    End If

    If Not moGM Is Nothing Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If
    
    
    
    If Not moStatLstSurchargeAmountMethod Is Nothing Then
        Set moStatLstSurchargeAmountMethod = Nothing
    End If
    
    If Not moStatLstAutomaticApproval Is Nothing Then
        Set moStatLstAutomaticApproval = Nothing
    End If
    
 
    
    If Not moDmZoneGrid Is Nothing Then
        moDmZoneGrid.UnloadSelf
        Set moDmZoneGrid = Nothing
    End If
    
    If Not moGMZoneGrid Is Nothing Then
        moGMZoneGrid.UnloadSelf
        Set moGMZoneGrid = Nothing
    End If
    

    lkuMain.Terminate
    lkuSTaxSchedule.Terminate
    lkuWhsOvrdSegVal.Terminate
    lkuCostWhse.Terminate
    lkuPriceWhse.Terminate
    lkuMPostalCode.Terminate
    lkuSPostalCode.Terminate
    
    glaPurchases.Terminate
    glaSales.Terminate
    glaSalesOffset.Terminate
    glaReturns.Terminate
    glaInventory.Terminate
    glaCostOfSales.Terminate
    glaIssues.Terminate
    glaMiscAdj.Terminate
    glaCostTierAdj.Terminate
    glaPhysicalCount.Terminate
    
    ' Transfer Tab GL Account controls
    glaExpenseAcct.Terminate
    glaFreightSurcharge.Terminate
    
    DoEvents    'invoke to process the Message Queue
    '-- If this form loads any other modal objects
    Set moOptions = Nothing
    Set moSotaObjects = Nothing
    Set moMapSrch = Nothing
    Set moClass = Nothing
    Set moContextMenu = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaCostOfSales_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaCostOfSales
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaCostOfSales_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaCostOfSales_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaCostOfSales_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaCostTierAdj_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaCostTierAdj
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaCostTierAdj_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaCostTierAdj_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaCostTierAdj_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaExpenseAcct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaExpenseAcct
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaExpenseAcct_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaExpenseAcct_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaExpenseAcct_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaFreightSurcharge_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaFreightSurcharge
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaFreightSurcharge_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaFreightSurcharge_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaFreightSurcharge_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Col = kColSurchargeAmountMethod Then
        gGridUpdateCell grdTransferRules, Row, kColSurchargeAmountMethodValue, moStatLstSurchargeAmountMethod.DBValue(gsGridReadCellText(grdTransferRules, Row, kColSurchargeAmountMethod))
    End If
    
    If Col = kColAutomaticApproval Then
        gGridUpdateCell grdTransferRules, Row, kColAutomaticApprovalValue, moStatLstAutomaticApproval.DBValue(gsGridReadCellText(grdTransferRules, Row, kColAutomaticApproval))
    End If
    
    moGM_CellChange Row, Col
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Row <> 0 Then
        Select Case Col
        
            Case kColReceivingWarehouse, kColTransitWarehouse, kColShipMethod
                moGM.Scroll
                mvValueHolder = gsGridReadCellText(grdTransferRules, Row, Col)
                

            Case kColSurchargeAmount, kColSurchargeAmountMethod
                If Val(gsGridReadCellText(grdTransferRules, Row, kColSurchargeAmount)) = 0 _
                And gsGridReadCellText(grdTransferRules, Row, kColSurchargeAmountMethod) = "" Then
                    ' Default Surcharge Amount to 0
                    gGridUpdateCell grdTransferRules, Row, kColSurchargeAmount, 0
                    ' Default Surcharge Amount Method to kSurchargeAmountMethodDDNDefault (= 1)
                    gGridUpdateCellText grdTransferRules, Row, kColSurchargeAmountMethod, moStatLstSurchargeAmountMethod.LocalText(kSurchargeAmountMethodDDNDefault)
                    gGridUpdateCellText grdTransferRules, Row, kColSurchargeAmountMethodValue, _
                                        kSurchargeAmountMethodDDNDefault
                    If Row = grdTransferRules.MaxRows Then
                        moDmGrid.AppendRow
                    End If
                End If
    
            Case kColAutomaticApproval
                If gsGridReadCellText(grdTransferRules, Row, Col) = "" Then
                    ' Default Automatic Approval dropdown to kAutomaticApprovalDDNDefault (= 3)
                    gGridUpdateCellText grdTransferRules, Row, kColAutomaticApproval, moStatLstAutomaticApproval.LocalText(kAutomaticApprovalDDNDefault)
                    gGridUpdateCellText grdTransferRules, Row, kColAutomaticApprovalValue, _
                                        kAutomaticApprovalDDNDefault
                    If Row = grdTransferRules.MaxRows Then
                        moDmGrid.AppendRow
                    End If
                End If
                    
            Case kColSurchargePercent, kColSurchargeAmount, kColLeadTimeDays
                If gsGridReadCellText(grdTransferRules, Row, Col) = "" Then
                    gGridUpdateCell grdTransferRules, Row, Col, giGetValidInt("0")
                    If Row = grdTransferRules.MaxRows Then
                        moDmGrid.AppendRow
                    End If
                End If
                mvValueHolder = gsGridReadCellText(grdTransferRules, Row, Col)
                
            Case Else
                mvValueHolder = ""
        End Select
        
        moGM.Grid_Click Col, Row
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGM.Grid_ColWidthChange Col1
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGM.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If mbDoNotLeaveCell Then
        Cancel = True
        mbDoNotLeaveCell = False
    Else
        moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
          If Me.ActiveControl.Name = "grdTransferRules" Then
            FillInGridRowDefaults NewRow, NewCol
        End If
        mvValueHolder = gsGridReadCellText(grdTransferRules, NewRow, NewCol)
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGM.Grid_LeaveRow Row, NewRow
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_LeaveRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdTransferRules_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdTransferRules_TopLeftChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuCostWhse, True
    #End If
'+++ End Customizer Code Push +++
    moValMgr.IsValidControl lkuCostWhse ' Validation manager internally call VMIsValidControl this function.
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCostWhse_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuCostWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuCostWhse_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaInventory_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaInventory
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaInventory_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaInventory_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        bCancel = bDenyControlFocus
        WarnTransitInvAcct
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaInventory_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaIssues_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaIssues
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaIssues_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaIssues_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaIssues_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaMiscAdj_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaMiscAdj
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaMiscAdj_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaMiscAdj_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaMiscAdj_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuMain, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Not gbGotFocus(Me, lkuMain) Then Exit Sub
    If Not bUnloadConfirmed() Then bCancel = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMain_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuMPostalCode, True
    #End If
'+++ End Customizer Code Push +++
    moValMgr.IsValidControl lkuMPostalCode ' Validation manager internally call VMIsValidControl this function.
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMPostalCode_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuMPostalCode, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim sCountry As String
    
    bCancel = bDenyControlFocus
    
    sCountry = Trim(ddnMCountry.Text)
    
    If Len(sCountry) > 0 Then
        lkuMPostalCode.RestrictClause = "CountryID =" & gsQuoted(ddnMCountry.Text)
    Else
        lkuMPostalCode.RestrictClause = ""
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMPostalCode_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaPhysicalCount_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaPhysicalCount
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaPhysicalCount_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaPhysicalCount_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaPhysicalCount_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceWhse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPriceWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPriceWhse_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaPurchases_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaPurchases
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaPurchases_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaPurchases_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaPurchases_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaReturns_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaReturns
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaReturns_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaReturns_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaReturns_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaSales_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaSales
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaSales_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaSales_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaSales_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaSalesOffset_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.IsValidControl glaSalesOffset
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaSalesOffset_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaSalesOffset_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaSalesOffset_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSPostalCode, True
    #End If
'+++ End Customizer Code Push +++
    moValMgr.IsValidControl lkuSPostalCode
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSPostalCode_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPricewhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPriceWhse, True
    #End If
'+++ End Customizer Code Push +++
    moValMgr.IsValidControl lkuPriceWhse
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPricewhse_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuSPostalCode, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim sCountry As String
    
    bCancel = bDenyControlFocus
    
    sCountry = Trim(ddnSCountry.Text)
    
    If Len(sCountry) > 0 Then
        lkuSPostalCode.RestrictClause = "CountryID =" & gsQuoted(ddnSCountry.Text)
    Else
        lkuSPostalCode.RestrictClause = ""
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSPostalCode_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSTaxSchedule, True
    #End If
'+++ End Customizer Code Push +++
    moValMgr.IsValidControl lkuSTaxSchedule
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSTaxSchedule_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuSTaxSchedule, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSTaxSchedule_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhsOvrdSegVal, True
    #End If
'+++ End Customizer Code Push +++
    moValMgr.IsValidControl lkuWhsOvrdSegVal

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhsOvrdSegVal, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmForm_DMAfterDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lCntctKey As Long
    
    bValid = False
    
    With moDmForm
        If (miSecurityLevel > kSecLevelDisplayOnly) Then
            'RKL DEJ 2017-03-29 (START)
            DeleteExtTable glGetValidLong(moDmForm.GetColumnValue("WhseKey"))
            'RKL DEJ 2017-03-29 (STOP)
                                
            '-- Delete the contact
            If Not bUpdateContact(0, glGetValidLong(.GetColumnValue("WhseKey"))) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            '-- Delete the mail address
            If Not bDeleteAddress(glGetValidLong(.GetColumnValue("MailAddrKey"))) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
        
            '-- Delete the ship address
            If Not bDeleteAddress(glGetValidLong(.GetColumnValue("ShipAddrKey"))) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
        End If
    End With
    
    bValid = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMAfterDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmForm_DMBeforeUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lCntctKey As Long
    
    bValid = False
    
    With moDmForm
        If (miSecurityLevel > kSecLevelDisplayOnly) Then
            If Not bUpdateContact(glGetValidLong(.GetColumnValue("WhseMgrCntctKey")), _
glGetValidLong(.GetColumnValue("WhseKey"))) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
        End If
    End With
    
    bValid = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmForm_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moValMgr.SetAllValid
    
    If oChild Is moDmForm Then
        If chkUseBins.Value = 1 Then
            chkTrackQtyAtBin.Enabled = mbWMIsLicensed
            If mbWMIsLicensed Then
                tabWarehouse.TabEnabled(6) = True
                mbLoadUseBins = True
                If bZoneExists(lkuMain.KeyValue) Then
                    chkUseZones.Value = 1
                    chkUseZones.Enabled = False
    
                Else
                    If glGetValidLong(moDmForm.GetColumnValue("UseZones")) = 1 Then
                        chkUseZones.Value = 1
                        chkUseZones.Enabled = True
                        tabWarehouse.TabEnabled(6) = True
                    Else
                        chkUseZones.Value = 0
                        chkUseZones.Enabled = True
                        tabWarehouse.TabEnabled(6) = False
                    End If
                End If
             moDmZoneGrid.Init
             moDmZoneGrid.OrderBy = "SortOrder"
            End If
        Else
            chkUseZones.Enabled = False
            tabWarehouse.TabEnabled(6) = False
            mbLoadUseBins = False
            chkTrackQtyAtBin.Enabled = False
            mbLoadTrackQtyAtBin = False
        End If
        
        If chkTrackQtyAtBin.Value = 1 Then
            chkTrackQtyAtBin.Enabled = True
            mbLoadTrackQtyAtBin = True
        End If
        
 
                
        'No need to check for inbound transfers exist against this warehouse
        'when loading up the existing record.
        bCheckInboundTransfer = False
        
        ' Making sure the option buttons on the Transfer tab reflect the data from the record
        If moDmForm.GetColumnValue("TrnsfrFrtChrgOpt") = koptFreightChargesCapitalize Then
            optFreightCharges(0).Value = True
        Else
            optFreightCharges(1).Value = True
        End If
        
        If moDmForm.GetColumnValue("TrnsfrSrchrgOpt") = koptSurchargesCapitalize Then
            optSurcharges(0).Value = True
        Else
            optSurcharges(1).Value = True
        End If
        
        'Enable inbound transfers check
        bCheckInboundTransfer = True
        
        ' Enable the glaExpenseAcct control if either option control set is set to Expense
        ' (as opposed to Capitalize)
        If optFreightCharges(1) Or optSurcharges(1) Then
            glaExpenseAcct.Enabled = True
        Else
            glaExpenseAcct.Enabled = False
            glaExpenseAcct.Text = ""
        End If
        
        numMaxQulLeadTimePct.Value = Val(numMaxQulLeadTimePctHidden.Value) * 100
        numMinQulLeadTimePct.Value = Val(numMinQulLeadTimePctHidden.Value) * 100
        numCostOfCarryingPct.Value = Val(numCostOfCarryingPctHidden.Value) * 100
        numRestockRate.Value = Val(numRestockRateHidden.Value)
    End If

    '-- Mail Address
    If oChild Is moDmMailAddr Then
        ddnMCountry.Tag = ddnMCountry.ListIndex
        SetPostalCodeMask lkuMPostalCode, ddnMCountry
    End If

    '-- Ship Address
    If oChild Is moDmShipAddr Then
        ddnSCountry.Tag = ddnSCountry.ListIndex
        SetPostalCodeMask lkuSPostalCode, ddnSCountry
    End If

    'Populate list of print devices.
    If ddnImPickDefPrinter.List = "" Then
        ddnImPickDefPrinter.Clear
        BuildPrintDeviceCombo Me, ddnImPickDefPrinter.Name
    End If
    If ddnCallPickDefPrinter.List = "" Then
        ddnCallPickDefPrinter.Clear
        BuildPrintDeviceCombo Me, ddnCallPickDefPrinter.Name
    End If
    If ddnImInvoiceDefPrinter.List = "" Then
        ddnImInvoiceDefPrinter.Clear
        BuildPrintDeviceCombo Me, ddnImInvoiceDefPrinter.Name
    End If
    
    mbDmLoad = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetSettingsCombo(oPrinter As Control, lKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sText As String
  
    sText = moClass.moAppDB.Lookup("RptSettingID", "tsmReportSetting", "RptSettingKey = " & glGetValidLong(lKey))
    oPrinter.List = sText
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPostalCodeMask", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub moDmForm_DMPostSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer

    'RKL DEJ 2017-03-29 (START)
    SaveToExtTable glGetValidLong(moDmForm.GetColumnValue("WhseKey")), glGetValidLong(lkuCOASig.KeyValue), glGetValidLong(nbrBoilingPoint.Value)
    'RKL DEJ 2017-03-29 (STOP)
    
    If bNewWarehouse Then
        mbLoadTrackQtyAtBin = False
        If mbDeleteZoneIntWhs Then
            '-- Delete the Zones
            bDeleteZones (glGetValidLong(moDmForm.GetColumnValue("WhseKey")))
        End If
    Else
        If (chkUseBins.Value = 0 And mbLoadUseBins = True) _
            Or (chkUseBins.Value = 1 And mbLoadUseBins = False) _
            Or (chkTrackQtyAtBin.Value = 0 And mbLoadTrackQtyAtBin = True) _
            Or (chkTrackQtyAtBin.Value = 1 And mbLoadTrackQtyAtBin = False) Then
            If chkUseBins.Value = 0 Then
                tabWarehouse.TabEnabled(6) = False
                mbLoadUseBins = False
            Else
                tabWarehouse.TabEnabled(6) = mbWMIsLicensed
                mbLoadUseBins = True
            End If

            If chkTrackQtyAtBin.Value = 0 Then
                mbLoadTrackQtyAtBin = False
            Else
                mbLoadTrackQtyAtBin = True
            End If
        Else
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    
    '-- Delete Zone Reference
    If mbDeleteZoneReference Then
       DeleteZoneReference lkuMain.KeyValue
    End If
   
    '-- Delete  Zones
    If mbDeleteZones Then
        bDeleteZones (glGetValidLong(moDmForm.GetColumnValue("WhseKey")))
    End If
          
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMPostSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmForm_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sState As String
    Dim lAddrKey As Long
    Dim sCountryID As String

    '-- Mail Address
    If oChild Is moDmMailAddr Then
        Select Case oChild.State
            Case kDmStateAdd
                '-- Init Mail Address
                ddnMCountry.ListIndex = lListIndexFromText(ddnMCountry, msCountryID)
                ddnMCountry.Tag = ddnMCountry.ListIndex
                RefreshStates ddnMState, ddnMCountry
                SetPostalCodeMask lkuMPostalCode, ddnMCountry

            Case kDmStateEdit
               '-- Refresh states before DM retrieves the address. (If it looks like a workaround, and smells like a workaround...)
                lAddrKey = gsGetValidStr(moDmForm.GetColumnValue("MailAddrKey"))
                If lAddrKey > 0 Then
                    sCountryID = gsGetValidStr(moClass.moAppDB.Lookup("CountryID", "tciAddress", "AddrKey = " & lAddrKey))
                    ddnMState.Clear
                    ddnMState.InitDynamicList moClass.moAppDB, "SELECT StateID FROM tsmState WHERE CountryID = " & gsQuoted(sCountryID)
                    ddnMState.Refresh
                End If
        End Select
    End If

    '-- Ship Address
    If oChild Is moDmShipAddr Then
        Select Case oChild.State
            Case kDmStateAdd
                '-- Init Ship Address
                ddnSCountry.ListIndex = lListIndexFromText(ddnSCountry, msCountryID)
                ddnSCountry.Tag = ddnSCountry.ListIndex
                RefreshStates ddnSState, ddnSCountry
                SetPostalCodeMask lkuSPostalCode, ddnSCountry

        Case kDmStateEdit
            '-- Refresh states before DM retrieves the address. (If it looks like a workaround, and smells like a workaround...)
            lAddrKey = moDmForm.GetColumnValue("ShipAddrKey")
            sCountryID = gsGetValidStr(moClass.moAppDB.Lookup("CountryID", "tciAddress", "AddrKey = " & lAddrKey))
            ddnSState.Clear
            ddnSState.InitDynamicList moClass.moAppDB, "SELECT StateID FROM tsmState WHERE CountryID = " & gsQuoted(sCountryID)
            ddnSState.Refresh
        End Select
    End If

    If oChild Is moDmForm Then
        Select Case oChild.State
            Case kDmStateAdd
                '-- ImmedPickRptSetting
                ddnImPickPrintSetting.ListIndex = 0
                ddnImPickPrintSetting.Tag = ddnImPickPrintSetting.ListIndex

                '-- WillCallPickRptSetting
                ddnCallPickPrintSetting.ListIndex = 0
                ddnCallPickPrintSetting.Tag = ddnCallPickPrintSetting.ListIndex

                '-- ImmedPickRptSetting
                ddnImInvPrintSetting.ListIndex = 0
                ddnImInvPrintSetting.Tag = ddnImInvPrintSetting.ListIndex

            Case kDmStateEdit
                '-- Get ImmedPickRpt Settings Key before DM retrieves.
                mlImmedPickRptSettingKey = glGetValidLong(moDmForm.GetColumnValue("ImmedPickRptSettingKey"))

                '-- Get WillCallPickRpt Setting Key before DM retrieves.
                mlWillCallPickRptSettingKey = glGetValidLong(moDmForm.GetColumnValue("WillCallPickRptSettingKey"))

                '-- Get ImmedInvcRpt Setting Key before DM retrieves.
                mlImmedInvcRptSettingKey = glGetValidLong(moDmForm.GetColumnValue("ImmedInvcRptSettingKey"))

                LoadPrintSettingsCtl ddnImPickPrintSetting, mlImmedPickRptSettingKey
                LoadPrintSettingsCtl ddnCallPickPrintSetting, mlWillCallPickRptSettingKey
                LoadPrintSettingsCtl ddnImInvPrintSetting, mlImmedInvcRptSettingKey
        End Select
        
        'RKL DEJ 2017-03-29 (START)
        GetExtTable glGetValidLong(moDmForm.GetColumnValue("WhseKey"))
        'RKL DEJ 2017-03-29 (STOP)
        
  End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmForm_DMValidate(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bValid = True

    If (numMaxOrdCycle.Value < numMinOrdCycle.Value) Then
        giSotaMsgBox Me, moClass.moSysSession, kMsgMaxMinOrdCycle
        tabWarehouse.Tab = kTabReplenishment
        numMaxOrdCycle.SetFocus
        bValid = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    If (numMaxDmdMultiple.Value < numMinDmdMultiple.Value) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgMaxMinDemandMultiple
        tabWarehouse.Tab = kTabReplenishment
        numMaxDmdMultiple.SetFocus
        bValid = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    'If replenishment is activated,
    'a cost of carry percentage is always required except for transit warehouse.
    'This is to avoid problems with inventory items not being included in calculation
    'of suggested orders in Replenishment.
    If (mbIRIsActivated = True And chkIntransitWhs.Value = 0 _
    And Val(numCostOfCarryingPct.Text) = 0) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCarryingCostReqd
        tabWarehouse.Tab = kTabReplenishment
        numCostOfCarryingPct.SetFocus
        bValid = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    'Validate Duplicate Zone entry.
    If (Not bValidateDuplicateZone) Then
        bValid = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    numMaxQulLeadTimePctHidden.Value = Val(numMaxQulLeadTimePct) / 100
    numMinQulLeadTimePctHidden.Value = Val(numMinQulLeadTimePct) / 100
    numCostOfCarryingPctHidden.Value = Val(numCostOfCarryingPct) / 100
    numRestockRateHidden.Value = Val(numRestockRate.Value)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuMain, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If colSQLReturnVal.Item(1) <> lkuMain.Text Then
        lkuMain.Text = colSQLReturnVal.Item(1)
        moDmForm.KeyChange

        If chkIntransitWhs.Value = 0 Then
            tabWarehouse.Tab = 0
        End If
    End If
    
    moDmForm.SetDirty False, kDmIncludeChildren

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMain_BeforeLookupReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuMain, True
    #End If
'+++ End Customizer Code Push +++
    
    Dim bValid As Boolean

    'Allow the user to click on the toolbar
    If Me.ActiveControl Is tbrMain Then Exit Sub
    
    If moDmForm.State <> kDmStateNone Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    'Trim the user input
    lkuMain.Text = Trim(lkuMain.Text)
         
    'Validate user input
    If Me.ActiveControl.Name <> lkuMain.Name Then
        bValid = bIsValidID()
    Else
        lkuMain.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    'Check if invalid due to a bad or non-existant key
    If (Not bValid) And moDmForm.State <> kDmStateAdd Then
        If lkuMain.Enabled Then
            lkuMain.SetFocus
        End If
    Else
        If chkIntransitWhs.Value = 0 Then
            tabWarehouse.Tab = 0
            gbSetFocus Me, txtDescription
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuMain_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvgWhseKey As Long
    Dim sRcvgWhseID As String
    Dim lShipMethKey As Long
    Dim sShipMethID As String
    Dim lTransitWhseKey As Long
    Dim sTransitWhseID As String
    
    ' Populating the Receiving Warehouse cell
    lRcvgWhseKey = glGetValidLong(gsGridReadCell(grdTransferRules, lRow, kColReceivingWarehouseKey))
    sRcvgWhseID = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "timWarehouse.WhseKey = " & _
                    gsGetValidStr(lRcvgWhseKey))
    gGridUpdateCell grdTransferRules, lRow, kColReceivingWarehouse, sRcvgWhseID
    gGridLockCell grdTransferRules, kColReceivingWarehouse, lRow
    
    ' Populating the Shipping Method cell
    lShipMethKey = glGetValidLong(gsGridReadCell(grdTransferRules, lRow, kColShipMethodKey))
    sShipMethID = moClass.moAppDB.Lookup("ShipMethID", "tciShipMethod", "tciShipMethod.ShipMethKey= " & _
                    gsGetValidStr(lShipMethKey))
    gGridUpdateCell grdTransferRules, lRow, kColShipMethod, sShipMethID
    
    ' Populating the Transit Warehouse cell
    lTransitWhseKey = glGetValidLong(gsGridReadCell(grdTransferRules, lRow, kColTransitWarehouseKey))
    sTransitWhseID = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "timWarehouse.WhseKey = " & _
                     gsGetValidStr(lTransitWhseKey))
    gGridUpdateCell grdTransferRules, lRow, kColTransitWarehouse, sTransitWhseID
    
    ' Populating the Surcharge Percent cell
    gGridUpdateCellText grdTransferRules, lRow, kColSurchargePercent, _
    gsGetValidStr(Val(gsGridReadCellText(grdTransferRules, lRow, kColSurchargePercentValue)) * 100)
   
    ' Populating the Surcharge Amount Method cell
    gGridUpdateCellText grdTransferRules, lRow, kColSurchargeAmountMethod, moStatLstSurchargeAmountMethod.LocalText(gsGridReadCell(grdTransferRules, lRow, kColSurchargeAmountMethodValue))

    ' Populating the Automatic Approval cell
    gGridUpdateCellText grdTransferRules, lRow, kColAutomaticApproval, moStatLstAutomaticApproval.LocalText(gsGridReadCell(grdTransferRules, lRow, kColAutomaticApprovalValue))
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmGrid_DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmMailAddr_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
     Dim lKey As Long
    '-------------------------------------------------------------
    'Adding the call to stored procedure for adding a surrogate key
    '--------------------------------------------------------------
    With moClass.moAppDB
        .SetInParamStr "tciAddress"
        .SetOutParam lKey
        .ExecuteSP "spGetNextSurrogateKey"
         lKey = .GetOutParam(2)
        .ReleaseParams
     End With
     
     moDmMailAddr.SetColumnValue "AddrKey", lKey
     moDmForm.SetColumnValue "MailAddrKey", lKey
     
     bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmMailAddr_DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmForm_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
     Dim lKey As Long

    '---------------------------------------------------------------
    'Adding the call to stored procedure for adding a surrogate key.
    '---------------------------------------------------------------
    'Check the License Count for Warehouses for this Company.
    'LicenseCount (EFE_LET_WAREHOUSE)

    'Only check the license count if we are NOT saving a warehouse that is a Transit Warehouse.
    If chkIntransitWhs.Value = 0 Then
        If Not moClass.moSysSession.CheckLicenseCount(msCompanyID, EFW_LET_WAREHOUSE) Then
            giSotaMsgBox Me, moClass.moSysSession, kMsgLicenseExceeded, moClass.moSysSession.LicenseCount(EFW_LET_WAREHOUSE), ksWarehouse
            bValid = False
            Exit Sub
        End If
    End If

    With moClass.moAppDB
        .SetInParamStr "timWarehouse"
        .SetOutParam lKey
        .ExecuteSP "spGetNextSurrogateKey"
         lKey = .GetOutParam(2)
        .ReleaseParams
    End With
     
    moDmForm.SetColumnValue "WhseKey", lKey

    'Making sure values from Transfer Tab option controls are set in Data Manager.
    If optFreightCharges(0).Value = True Then
        moDmForm.SetColumnValue "TrnsfrFrtChrgOpt", koptFreightChargesCapitalize
    Else
        moDmForm.SetColumnValue "TrnsfrFrtChrgOpt", koptFreightChargesExpense
    End If

    If optSurcharges(0).Value = True Then
        moDmForm.SetColumnValue "TrnsfrSrchrgOpt", koptSurchargesCapitalize
    Else
        moDmForm.SetColumnValue "TrnsfrSrchrgOpt", koptSurchargesExpense
    End If

    bValid = True
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDMForm_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************************
' Description:
'    This routine will be called by Data Manager when the record
'    state changes. Possible states are None, Add, Edit, Locked.
'***************************************************************
    Select Case iNewState
        Case Is = kDmStateNone
            tbrMain.SetState sotaTB_NONE
            sbrMain.Status = SOTA_SB_START
            bNewWarehouse = True
            chkIntransitWhs.Value = 0
            chkIntransitWhs.Enabled = mbWMIsLicensed
            mbLoadTrackQtyAtBin = False
            mbLoadUseBins = False
            chkTrackQtyAtBin.Enabled = False
            chkUseZones.Enabled = False
            tabWarehouse.TabEnabled(0) = True
            tabWarehouse.TabEnabled(1) = True
            tabWarehouse.TabEnabled(2) = True
            tabWarehouse.TabEnabled(3) = True
            tabWarehouse.TabEnabled(4) = mbWMIsLicensed
            tabWarehouse.TabEnabled(6) = False
            grdTransferRules.Enabled = False
            gGridSetActiveCell grdTransferRules, 1, 1
            tabWarehouse.Tab = 0
            
            'RKL DEJ 2017-03-29 (START)
            lkuCOASig.ClearData
            nbrBoilingPoint.ClearData
            'RKL DEJ 2017-03-29 (STOP)
        
        Case Is = kDmStateAdd
            tbrMain.SetState sotaTB_ADD
            sbrMain.Status = SOTA_SB_ADD
            bNewWarehouse = True
            chkTrackQtyAtBin.Enabled = False
            chkUseZones.Enabled = False
            chkUseZones.Value = 0
            chkIntransitWhs.Value = 0
            chkIntransitWhs.Enabled = mbWMIsLicensed
            grdTransferRules.Enabled = True
            gGridSetActiveCell grdTransferRules, 1, 1
        
        Case Is = kDmStateEdit
            tbrMain.SetState sotaTB_EDIT
            sbrMain.Status = SOTA_SB_EDIT
            bNewWarehouse = False
            
            'You cannot change this field once a Warehouse is saved (SCOPUS #19890).
            chkIntransitWhs.Enabled = False
            
            mbDmLoad = True
            grdTransferRules.Enabled = True
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDMForm_DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmShipAddr_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
     Dim lKey As Long
    
    '---------------------------------------------------------------
    'Adding the call to stored procedure for adding a surrogate key.
    '---------------------------------------------------------------
    With moClass.moAppDB
        .SetInParamStr "tciAddress"
        .SetOutParam lKey
        .ExecuteSP "spGetNextSurrogateKey"
         lKey = .GetOutParam(2)
        .ReleaseParams
     End With
     
     moDmShipAddr.SetColumnValue "AddrKey", lKey
     moDmForm.SetColumnValue "ShipAddrKey", lKey
     
     bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmShipAddr_DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navShipMethod_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    txtGrdNavShipMethod.Text = gsGridReadCellText(grdTransferRules, glGridGetActiveRow(grdTransferRules), kColShipMethod)
    navShipMethod.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    gvLookupClick Me, navShipMethod, txtGrdNavShipMethod
    moGM.LookupClicked

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navShipMethod_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navTransitWhse_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    txtGrdNavTransitWhse.Text = gsGridReadCellText(grdTransferRules, glGridGetActiveRow(grdTransferRules), kColTransitWarehouse)
    navTransitWhse.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND WhseID <> " & _
                                    gsQuoted(moDmForm.GetColumnValue("WhseID")) & " AND Transit = 1"
    gcLookupClick Me, navTransitWhse, txtGrdNavTransitWhse, "WhseID"
    moGM.LookupClicked

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navTransitWhse_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


'RKL DEJ 2018-06-27 (Start)
Private Sub nbrBoilingPoint_Validate(Cancel As Boolean)
    moDmForm.SetDirty True, True
End Sub
'RKL DEJ 2018-06-27 (End)

Private Sub numCostOfCarryingPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numCostOfCarryingPct, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numCostOfCarryingPct_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numCostOfCarryingPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numCostOfCarryingPct_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxQulLeadTimePct, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numMaxQulLeadTimePct_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxQulLeadTimePct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numMaxQulLeadTimePct_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinQulLeadTimePct, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numMinQulLeadTimePct_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinQulLeadTimePct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numMinQulLeadTimePct_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numRestockRate, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDmLoad Then
        moDmForm.SetDirty True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numRestockRate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFreightCharges_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optFreightCharges(Index), True
    #End If
'+++ End Customizer Code Push +++
    moDmForm.SetDirty True
    
    If moDmForm.State = kDmStateEdit And bCheckInboundTransfer Then
        If bChargeChangedDuring3StepsTransfer() Then
            'Prevent the recursive call to this block
            bCheckInboundTransfer = False
            
            If optFreightCharges(0).Value = True Then
                optFreightCharges(1).Value = True
            Else
                optFreightCharges(0).Value = True
            End If
            
            'Enable inbound transfers check
            bCheckInboundTransfer = True
        End If
    End If
    
    'Enable glaExpenseAcct control only if either one of the option control sets are
    'set to Expense (as opposed to Capitalize).
    If optFreightCharges(1) Or optSurcharges(1) Then
        glaExpenseAcct.Enabled = True
    Else
        'Clear the contents of the glaExpenseAcct text box, if both the Options are Capitalize
        glaExpenseAcct.Enabled = False
        glaExpenseAcct.Text = ""
    End If
    
    If optFreightCharges(0).Value = True Then
        moDmForm.SetColumnValue "TrnsfrFrtChrgOpt", koptFreightChargesCapitalize
    Else
        moDmForm.SetColumnValue "TrnsfrFrtChrgOpt", koptFreightChargesExpense
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optFreightCharges_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub optSurcharges_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optSurcharges(Index), True
    #End If
'+++ End Customizer Code Push +++
    moDmForm.SetDirty True

    If moDmForm.State = kDmStateEdit And bCheckInboundTransfer Then
        If bChargeChangedDuring3StepsTransfer() Then
            'Prevent the recursive call to this block
            bCheckInboundTransfer = False
            
            If optSurcharges(0).Value = True Then
                optSurcharges(1).Value = True
            Else
                optSurcharges(0).Value = True
            End If
            
            'Enable inbound transfers check
            bCheckInboundTransfer = True
        End If
    End If
    
    'Enable glaExpenseAcct control only if either one of the option control sets are
    'set to Expense (as opposed to Capitalize).
    If optFreightCharges(1) Or optSurcharges(1) Then
        glaExpenseAcct.Enabled = True
    Else
        'Clear the contents of the glaExpenseAcct text box, if both the Options are Capitalize
        glaExpenseAcct.Enabled = False
        glaExpenseAcct.Text = ""
    End If
    
    If optSurcharges(0).Value = True Then
        moDmForm.SetColumnValue "TrnsfrSrchrgOpt", koptSurchargesCapitalize
    Else
        moDmForm.SetColumnValue "TrnsfrSrchrgOpt", koptSurchargesExpense
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optSurcharges_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick sButton

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tabWarehouse_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iCnt As Integer

    For iCnt = 0 To 6
        pnlTabWarehouse(iCnt).Enabled = False
    Next iCnt
    
    pnlTabWarehouse(tabWarehouse.Tab).Enabled = True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabWarehouse_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    tbrMain.SetFocus
    HandleToolBarClick Button

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhseID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bIsValidID

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtWhseID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bIsValidID() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************
' Description:
'    This is the field validation routine. It loads the controls
'    if a valid ID is entered; otherwise it sets the form state to
'    Add.
'
' Returns:
'   True  if valid key is entered (new or existing record)
'   False if incomplete key or an error occurs
'**************************************************************************
    Dim bValid As Boolean
    Dim iKeyChangeCode As Integer
    
    bIsValidID = True
    mbDmLoad = True
        
    SetHourglass True
    
    '-- Clear the postal code masks.
    lkuMPostalCode.Mask = ""
    lkuSPostalCode.Mask = ""
    
    iKeyChangeCode = moDmForm.KeyChange()
    
    'Warehouse is transit, at loading time modmform becomes dirty
    'so in browsing it prompts to save the changes or not.
    moDmForm.SetDirty False
   
    Select Case iKeyChangeCode
        Case kDmKeyNotFound
            moClass.lUIActive = kChildObjectActive
            moDmMailAddr.SetDirty True
            moDmShipAddr.SetDirty True
            
        Case kDmKeyFound
            moClass.lUIActive = kChildObjectActive
           
        Case kDmKeyNotComplete
            '-- key not completely filled in
            bIsValidID = False
            
        Case kDmError
            '-- database error occurred trying to get row
            bIsValidID = False
         
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, iKeyChangeCode
            bIsValidID = False
    End Select
    
    SetHourglass False
    mbDmLoad = False

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                moFormCust.ApplyDataBindings moDmForm
                moFormCust.ApplyFormCust
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If


Private Sub cmdDemandVar_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDemandVar, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDemandVar_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDemandVar_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDemandVar, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDemandVar_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdContact_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdContact, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdContact_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdContact_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdContact, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdContact_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtDescription_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDescription, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavShipMethod_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtGrdNavShipMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavShipMethod_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavShipMethod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtGrdNavShipMethod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavShipMethod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavShipMethod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtGrdNavShipMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavShipMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavShipMethod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtGrdNavShipMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavShipMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavTransitWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtGrdNavTransitWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavTransitWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavTransitWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtGrdNavTransitWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavTransitWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavTransitWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtGrdNavTransitWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavTransitWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavTransitWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtGrdNavTransitWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavTransitWhse_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavRcvgWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtGrdNavRcvgWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavRcvgWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavRcvgWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtGrdNavRcvgWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavRcvgWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavRcvgWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtGrdNavRcvgWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavRcvgWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtGrdNavRcvgWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtGrdNavRcvgWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtGrdNavRcvgWhse_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankPeriod_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLastRankPeriod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankPeriod_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankPeriod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLastRankPeriod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankPeriod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankPeriod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLastRankPeriod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankPeriod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankPeriod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLastRankPeriod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankPeriod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankYear_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLastRankYear, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankYear_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankYear_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLastRankYear, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankYear_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankYear_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLastRankYear, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankYear_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastRankYear_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLastRankYear, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastRankYear_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddrName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSAddrName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddrName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddrName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSAddrName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddrName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddrName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSAddrName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddrName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddrName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSAddrName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddrName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSCity_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSCity_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSCity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSCity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSCity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSCity_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSCity_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSCity_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSCity_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddress_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSAddress(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddress_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddress_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSAddress(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddress_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddress_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSAddress(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddress_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSAddress_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSAddress(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSAddress_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddress_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMAddress(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddress_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddress_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMAddress(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddress_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddress_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMAddress(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddress_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddress_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMAddress(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddress_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMCity_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMCity_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMCity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMCity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMCity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMCity_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMCity_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMCity_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMCity_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddrName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMAddrName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddrName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddrName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMAddrName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddrName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddrName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMAddrName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddrName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMAddrName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMAddrName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMAddrName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuMain, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuMain, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuMain, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMain_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuMain, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMain_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuSPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSPostalCode_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuSPostalCode, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSPostalCode_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuSPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSPostalCode_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuSPostalCode, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSPostalCode_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSPostalCode_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuSPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSPostalCode_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuMPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMPostalCode_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuMPostalCode, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMPostalCode_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuMPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMPostalCode_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuMPostalCode, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMPostalCode_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMPostalCode_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuMPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMPostalCode_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuSTaxSchedule, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSTaxSchedule_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuSTaxSchedule, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSTaxSchedule_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuSTaxSchedule, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSTaxSchedule_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuSTaxSchedule, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSTaxSchedule_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSTaxSchedule_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuSTaxSchedule, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSTaxSchedule_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhsOvrdSegVal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhsOvrdSegVal, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhsOvrdSegVal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhsOvrdSegVal, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhsOvrdSegVal_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhsOvrdSegVal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhsOvrdSegVal_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuCostWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCostWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuCostWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCostWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuCostWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCostWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuCostWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCostWhse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuCostWhse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuCostWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuCostWhse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPriceWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPriceWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPriceWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceWhse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPriceWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceWhse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceWhse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPriceWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceWhse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub numCostOfReplenishment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numCostOfReplenishment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfReplenishment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfReplenishment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numCostOfReplenishment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfReplenishment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfReplenishment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numCostOfReplenishment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfReplenishment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfReplenishment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numCostOfReplenishment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfReplenishment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numCostOfCarryingPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfCarryingPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numCostOfCarryingPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfCarryingPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTrendPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTrendPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTrendPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTrendPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultiple_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxSenlDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultiple_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultiple_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxSenlDmdMultiple, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultiple_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultiple_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxSenlDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultiple_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultiple_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxSenlDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultiple_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultiple_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultiple_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultiple_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinDmdMultiple, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultiple_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultiple_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultiple_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultiple_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultiple_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultiple_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultiple_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultiple_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxDmdMultiple, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultiple_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultiple_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultiple_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultiple_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxDmdMultiple, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultiple_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinOrdCycle_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinOrdCycle_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinOrdCycle_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinOrdCycle, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinOrdCycle_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinOrdCycle_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinOrdCycle_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinOrdCycle_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinOrdCycle_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxOrdCycle_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxOrdCycle_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxOrdCycle_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxOrdCycle, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxOrdCycle_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxOrdCycle_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxOrdCycle_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxOrdCycle_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxOrdCycle_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numRestockRate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numRestockRate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numRestockRate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxQulLeadTimePct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxQulLeadTimePct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxQulLeadTimePct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxQulLeadTimePct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinQulLeadTimePct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinQulLeadTimePct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinQulLeadTimePct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinQulLeadTimePct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePctHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxQulLeadTimePctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxQulLeadTimePctHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePctHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxQulLeadTimePctHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxQulLeadTimePctHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePctHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxQulLeadTimePctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxQulLeadTimePctHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxQulLeadTimePctHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxQulLeadTimePctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxQulLeadTimePctHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePctHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinQulLeadTimePctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinQulLeadTimePctHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePctHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinQulLeadTimePctHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinQulLeadTimePctHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePctHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinQulLeadTimePctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinQulLeadTimePctHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinQulLeadTimePctHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinQulLeadTimePctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinQulLeadTimePctHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPctHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numCostOfCarryingPctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfCarryingPctHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPctHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numCostOfCarryingPctHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfCarryingPctHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPctHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numCostOfCarryingPctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfCarryingPctHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numCostOfCarryingPctHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numCostOfCarryingPctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numCostOfCarryingPctHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPctHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTrendPctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPctHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPctHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTrendPctHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPctHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPctHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTrendPctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPctHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTrendPctHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTrendPctHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTrendPctHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultipleHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultipleHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultipleHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxDmdMultipleHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultipleHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultipleHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultipleHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxDmdMultipleHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxDmdMultipleHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultipleHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMinDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultipleHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultipleHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMinDmdMultipleHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultipleHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultipleHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMinDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultipleHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMinDmdMultipleHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMinDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMinDmdMultipleHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultipleHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numMaxSenlDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultipleHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultipleHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numMaxSenlDmdMultipleHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultipleHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultipleHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numMaxSenlDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultipleHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numMaxSenlDmdMultipleHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numMaxSenlDmdMultipleHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numMaxSenlDmdMultipleHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRateHidden_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numRestockRateHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRateHidden_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRateHidden_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numRestockRateHidden, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRateHidden_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRateHidden_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numRestockRateHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRateHidden_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRestockRateHidden_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numRestockRateHidden, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRestockRateHidden_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIntransitWhs_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkIntransitWhs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIntransitWhs_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIntransitWhs_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkIntransitWhs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIntransitWhs_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickFromSO_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPickFromSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickFromSO_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickFromSO_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPickFromSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickFromSO_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickFromSO_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPickFromSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickFromSO_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipFromPick_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkShipFromPick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipFromPick_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipFromPick_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShipFromPick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipFromPick_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipFromPick_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShipFromPick, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipFromPick_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkTrackQtyAtBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkTrackQtyAtBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkTrackQtyAtBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkTrackQtyAtBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkTrackQtyAtBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkTrackQtyAtBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseZones_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkUseZones, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseZones_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseZones_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkUseZones, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseZones_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseBins_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkUseBins, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseBins_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseBins_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkUseBins, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseBins_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFreightCharges_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optFreightCharges(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optFreightCharges_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFreightCharges_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optFreightCharges(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optFreightCharges_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optFreightCharges_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optFreightCharges(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optFreightCharges_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optSurcharges_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optSurcharges(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optSurcharges_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optSurcharges_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optSurcharges(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optSurcharges_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optSurcharges_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optSurcharges(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optSurcharges_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefLotPickMethod_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnDefLotPickMethod, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefLotPickMethod_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefLotPickMethod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnDefLotPickMethod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefLotPickMethod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefLotPickMethod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnDefLotPickMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefLotPickMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefLotPickMethod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnDefLotPickMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefLotPickMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefPickMethod_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnDefPickMethod, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefPickMethod_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefPickMethod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnDefPickMethod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefPickMethod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefPickMethod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnDefPickMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefPickMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDefPickMethod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnDefPickMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDefPickMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvPrintSetting_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnImInvPrintSetting, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvPrintSetting_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvPrintSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnImInvPrintSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvPrintSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvPrintSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnImInvPrintSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvPrintSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvoiceDefPrinter_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnImInvoiceDefPrinter, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvoiceDefPrinter_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvoiceDefPrinter_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnImInvoiceDefPrinter, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvoiceDefPrinter_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvoiceDefPrinter_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnImInvoiceDefPrinter, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvoiceDefPrinter_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImInvoiceDefPrinter_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnImInvoiceDefPrinter, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImInvoiceDefPrinter_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickPrintSetting_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnCallPickPrintSetting, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickPrintSetting_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickPrintSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnCallPickPrintSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickPrintSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickPrintSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnCallPickPrintSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickPrintSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickDefPrinter_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnCallPickDefPrinter, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickDefPrinter_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickDefPrinter_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnCallPickDefPrinter, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickDefPrinter_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickDefPrinter_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnCallPickDefPrinter, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickDefPrinter_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCallPickDefPrinter_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnCallPickDefPrinter, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCallPickDefPrinter_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickPrintSetting_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnImPickPrintSetting, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickPrintSetting_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickPrintSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnImPickPrintSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickPrintSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickPrintSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnImPickPrintSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickPrintSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickDefPrinter_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnImPickDefPrinter, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickDefPrinter_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickDefPrinter_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnImPickDefPrinter, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickDefPrinter_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickDefPrinter_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnImPickDefPrinter, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickDefPrinter_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnImPickDefPrinter_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnImPickDefPrinter, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnImPickDefPrinter_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReorderMeth_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnReorderMeth, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReorderMeth_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReorderMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnReorderMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReorderMeth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReorderMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnReorderMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReorderMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReorderMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnReorderMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReorderMeth_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSState_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnSState, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSState_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSState_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnSState, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSState_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSState_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnSState, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSState_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSState_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnSState, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSState_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSCountry_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnSCountry, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSCountry_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSCountry_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnSCountry, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSCountry_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnSCountry_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnSCountry, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnSCountry_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMState_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnMState, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMState_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMState_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnMState, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMState_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMState_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnMState, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMState_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMState_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnMState, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMState_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMCountry_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnMCountry, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMCountry_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMCountry_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnMCountry, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMCountry_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnMCountry_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnMCountry, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnMCountry_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnRestockMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnRestockMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnRestockMeth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnRestockMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnRestockMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnRestockMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnRestockMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnRestockMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnRestockMeth_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastRankDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calLastRankDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastRankDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastRankDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calLastRankDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastRankDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastRankDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calLastRankDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastRankDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastRankDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calLastRankDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastRankDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub InitializeReplenishTab()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ddnReorderMeth.SetToDefault True
    numCostOfReplenishment.Value = 0
    numCostOfCarryingPct.Value = 0
    ddnRestockMeth.SetToDefault True
    numRestockRate.Value = 0
    numTrendPct.Value = 0
    numMaxOrdCycle.Value = 0
    numMinOrdCycle.Value = 0
    numMaxDmdMultiple.Value = 0
    numMinDmdMultiple.Value = 0
    numMaxSenlDmdMultiple.Value = 0
    
    'Set the Replenishment tab fields to disabled if IR is not active.
    If (Not mbIRIsActivated) Then
        calLastRankDate.Enabled = False
        txtLastRankPeriod.Enabled = False
        txtLastRankYear.Enabled = False
        cmdDemandVar.Enabled = False
        tabWarehouse.TabEnabled(kTabReplenishment) = False
        DisableControlsOnReplTab
    Else
        EnableControlsOnReplTab
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeReplenishTab", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitializeMainTab()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lkuCostWhse.Text = ""
    lkuPriceWhse.Text = ""
    lkuSTaxSchedule.Text = ""
    lkuWhsOvrdSegVal.Text = ""
    chkShipComplete = 0
    chkUseBins.Value = 0
    chkTrackQtyAtBin.Value = 0
    chkTrackQtyAtBin.Enabled = False
    numMaxQulLeadTimePct.Value = 0
    numMinQulLeadTimePct.Value = 0
    moDmForm.SetColumnValue ("WhseMgrCntctKey"), Empty

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeMainTab", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitializeAddressTab()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    txtMAddrName = ""

    txtMAddress(0) = ""
    txtMAddress(1) = ""
    txtMAddress(2) = ""
    txtMAddress(3) = ""
    txtMAddress(4) = ""
    
    txtMCity = ""
    ddnMState.ListIndex = -1
    ddnMCountry.ListIndex = -1
    ddnMCountry.Tag = ddnMCountry.ListIndex
    lkuMPostalCode.ClearData
    lkuMPostalCode.Tag = ""
    
    txtSAddrName = ""
    
    txtSAddress(0) = ""
    txtSAddress(1) = ""
    txtSAddress(2) = ""
    txtSAddress(3) = ""
    txtSAddress(4) = ""
    
    txtSCity = ""
    
    txtMLatitude = ""
    txtMLatitude = ""
    txtSLongitude = ""
    txtSLongitude = ""
    
    ddnSState.ListIndex = -1
    ddnSCountry.ListIndex = -1
    ddnSCountry.Tag = ddnSCountry.ListIndex
    lkuSPostalCode.ClearData
    lkuSPostalCode.Tag = ""

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeAddressTab", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindVM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moValMgr = New clsValidationManager
    
    With moValMgr
        Set .Form = frmWareHouseMNT
        Set .Class = moClass
        Set .TabControl = tabWarehouse
        
        .Bind lkuCostWhse, kTabMain
        .Bind lkuPriceWhse, kTabMain
        .Bind lkuSTaxSchedule, kTabMain
        .Bind lkuWhsOvrdSegVal, kTabMain
        
        .Bind lkuMPostalCode, kTabAddress
        .Bind lkuSPostalCode, kTabAddress
        
        .Bind glaPurchases, kTabGLAccount
        .Bind glaSales, kTabGLAccount
        .Bind glaSalesOffset, kTabGLAccount
        .Bind glaReturns, kTabGLAccount
        .Bind glaInventory, kTabGLAccount
        .Bind glaCostOfSales, kTabGLAccount
        .Bind glaIssues, kTabGLAccount
        .Bind glaMiscAdj, kTabGLAccount
        .Bind glaCostTierAdj, kTabGLAccount
        .Bind glaPhysicalCount, kTabGLAccount

        'Transfer Tab GL Account controls.
        .Bind glaExpenseAcct, kTabTransfer
        .Bind glaFreightSurcharge, kTabTransfer
                
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindVM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function VMIsValidControl(oCtl As Control) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    VMIsValidControl = False
    
    Select Case True
    Case oCtl Is lkuCostWhse
        VMIsValidControl = lkuCostWhse.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, _
            gsBuildString(kIMAssociateWarehouseCost, moClass.moAppDB, moClass.moSysSession)
            Exit Function
        End If
        
        If (glGetValidLong(lkuCostWhse.KeyValue) = 0 Or glGetValidLong(lkuMain.KeyValue) = 0) Then Exit Function
        
        VMIsValidControl = bIsCircWhseRef(lkuCostWhse.KeyValue, kGetCostWhse)
        
        If Not VMIsValidControl Then
            giSotaMsgBox Me, moClass.moSysSession, kMsgCostWhseCircRef, "Costing Warehouse", lkuCostWhse.Text
        End If
        
    Case oCtl Is lkuPriceWhse
        VMIsValidControl = lkuPriceWhse.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, _
            gsBuildString(kIMAssociateWarehousePrice, moClass.moAppDB, moClass.moSysSession)
        End If

        If (glGetValidLong(lkuPriceWhse.KeyValue) = 0 Or glGetValidLong(lkuMain.KeyValue) = 0) Then
            Exit Function
        End If
        
        VMIsValidControl = bIsCircWhseRef(lkuPriceWhse.KeyValue, kGetPriceWhse)
        
        If Not VMIsValidControl Then
            giSotaMsgBox Me, moClass.moSysSession, kMsgCostWhseCircRef, "Pricing Warehouse", lkuPriceWhse.Text
        End If
    
    Case oCtl Is lkuMPostalCode
        VMIsValidControl = bValidPostalCode(kMailAddress)
        VMIsValidControl = True
    
    Case oCtl Is lkuSPostalCode
        VMIsValidControl = bValidPostalCode(kShipAddress)
        VMIsValidControl = True
    
    Case oCtl Is lkuSTaxSchedule
        VMIsValidControl = lkuSTaxSchedule.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, _
            gsBuildString(kIMSalesTaxSchedule, moClass.moAppDB, moClass.moSysSession)
        End If
    
    Case oCtl Is lkuWhsOvrdSegVal
        VMIsValidControl = bValidOvrdSegVal
        
        If Not VMIsValidControl Then
            'Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblWhsOvrdSegVal.Caption, "&")
        End If
    
    Case oCtl Is glaPurchases
        VMIsValidControl = glaPurchases.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblPurchases.Caption, "&")
        End If
         
    Case oCtl Is glaSales
        VMIsValidControl = glaSales.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblSales.Caption, "&")
        End If
           
    Case oCtl Is glaSalesOffset
        VMIsValidControl = glaSalesOffset.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblSalesOffset.Caption, "&")
        End If
        
    Case oCtl Is glaReturns
        VMIsValidControl = glaReturns.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblReturns.Caption, "&")
        End If
        
    Case oCtl Is glaInventory
        VMIsValidControl = glaInventory.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblInventory.Caption, "&")
        End If

        If chkIntransitWhs.Value = 1 Then
            If glaInventoryIsBlank Then
                VMIsValidControl = False
            End If

            WarnIfPendingTrnsfrLinesExist
        End If
        
    Case oCtl Is glaCostOfSales
        VMIsValidControl = glaCostOfSales.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblCostOfSales.Caption, "&")
        End If
            
    Case oCtl Is glaIssues
        VMIsValidControl = glaIssues.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblIssues.Caption, "&")
        End If
        
    Case oCtl Is glaMiscAdj
        VMIsValidControl = glaMiscAdj.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblMiscAdj.Caption, "&")
        End If
    
    Case oCtl Is glaCostTierAdj
        VMIsValidControl = glaCostTierAdj.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblCostTierAdj.Caption, "&")
        End If
    
    Case oCtl Is glaPhysicalCount
        VMIsValidControl = glaPhysicalCount.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblPhysical.Caption, "&")
        End If
    
    Case oCtl Is glaExpenseAcct
        VMIsValidControl = glaExpenseAcct.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblExpenseAcct.Caption, "&")
        End If
    
    Case oCtl Is glaFreightSurcharge
        VMIsValidControl = glaFreightSurcharge.IsValid
        
        If Not VMIsValidControl Then
            ' ********Give message specific to your control.
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, gsStripChar(lblFreightSurcharge.Caption, "&")
        End If
        
    End Select
        
    moValMgr.AllowMessage = False

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bValidPostalCode(Index As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL    As String
    Dim oRs     As Object
    
    bValidPostalCode = False
    
    sSQL = "Select CountryID, StateID, City from tsmPostalCode "
    If Index = kMailAddress Then
        sSQL = sSQL & " WHERE PostalCode = '" & lkuMPostalCode.Text & kSQuote
        If ddnMCountry.ListIndex <> kItemNotSelected Then
            sSQL = sSQL & " AND CountryID = " & gsQuoted(ddnMCountry.Text)
        End If
    Else
        sSQL = sSQL & " WHERE PostalCode = '" & lkuSPostalCode.Text & kSQuote
        If ddnSCountry.ListIndex <> kItemNotSelected Then
            sSQL = sSQL & " AND CountryID = " & gsQuoted(ddnSCountry.Text)
        End If
    End If
    
    Set oRs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If oRs.RecordCount = 0 Then
        oRs.Close
        Set oRs = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    If Index = kMailAddress Then
        If Not IsNull(oRs.Field("CountryID")) Then
            ddnMCountry.Text = oRs.Field("CountryID")
            ddnMCountry.Tag = ddnMCountry.ListIndex
        End If
        
        RefreshStates ddnMState, ddnMCountry
        SetPostalCodeMask lkuMPostalCode, ddnMCountry
        
        If Not IsNull(oRs.Field("StateID")) Then ddnMState.Text = oRs.Field("StateID")
        If Not IsNull(oRs.Field("City")) Then txtMCity.Text = oRs.Field("City")
    End If
    
    If Index = kShipAddress Then
        If Not IsNull(oRs.Field("CountryID")) Then
            ddnSCountry.Text = oRs.Field("CountryID")
            ddnSCountry.Tag = ddnSCountry.ListIndex
        End If
        
        RefreshStates ddnSState, ddnSCountry
        SetPostalCodeMask lkuSPostalCode, ddnSCountry
        
        If Not IsNull(oRs.Field("StateID")) Then ddnSState.Text = oRs.Field("StateID")
        If Not IsNull(oRs.Field("City")) Then txtSCity.Text = oRs.Field("City")
    End If
    
    bValidPostalCode = True
    
    oRs.Close
    Set oRs = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidPostalCode", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub InitializeGLAccounts()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    glaPurchases.Text = ""
    glaSales.Text = ""
    glaSalesOffset.Text = ""
    glaReturns.Text = ""
    glaInventory.Text = ""
    glaCostOfSales.Text = ""
    glaIssues.Text = ""
    glaMiscAdj.Text = ""
    glaCostTierAdj.Text = ""
    glaPhysicalCount.Text = ""
            
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeGLAccounts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bValidOvrdSegVal() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL    As String
    Dim oRs     As Object
    
    bValidOvrdSegVal = False

    If Len(Trim$(lkuWhsOvrdSegVal.Text)) = 0 Then
        bValidOvrdSegVal = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    sSQL = "SELECT * FROM tglSegmentCode WITH (NOLOCK) WHERE SegmentKey = " & Format$(mlWhseOvrdSegKey)
    sSQL = sSQL & " AND AcctSegValue = " & gsQuoted(lkuWhsOvrdSegVal.Text)
    
    Set oRs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If oRs.RecordCount = 0 Then
        oRs.Close
        Set oRs = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    Else
        bValidOvrdSegVal = True
    End If
    
    oRs.Close
    Set oRs = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidOvrdSegVal", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Private Function bUnloadConfirmed() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iConfirmUnload      As Integer
    
    bUnloadConfirmed = False

    'check for dirty form here with message box
    iConfirmUnload = moDmForm.ConfirmUnload(mlRunMode <> kContextAOF) 'Pass True if Not AOF, to NOT clear the form after saving
    
    Select Case iConfirmUnload
        Case kDmSuccess
        Case kDmFailure
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Case kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Case Else
            Call giSotaMsgBox(Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, CVar(iConfirmUnload))
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
    End Select
    
    bUnloadConfirmed = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUnloadConfirmed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bDeleteAddress(lAddrKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String

    bDeleteAddress = False

    If lAddrKey > 0 Then
        
        sSQL = "DELETE tciAddress WHERE AddrKey = " & lAddrKey
        
        Debug.Print sSQL
        
        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            MsgBox Err.Description, vbOKOnly, gsBuildString(kSotaTitle, moClass.moAppDB, moClass.moSysSession)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
        On Error GoTo ExpectedErrorRoutine
    
    End If
    
    bDeleteAddress = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteAddress", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bZoneExists(lWhseKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim rs As Object
Dim sSQL As String
    sSQL = "SELECT 'Rows' = COUNT(*) FROM timWhseZone " & _
           "WHERE WhseKey = " & glGetValidLong(lWhseKey)
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.Field("Rows") > 0 Then
        bZoneExists = True
    Else
        bZoneExists = False
    End If
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bZoneExists", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Function

Private Function bDeleteZones(lWhseKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String

    bDeleteZones = False

    If lWhseKey > 0 Then
        
        sSQL = "DELETE timWhseZone WHERE WhseKey = " & lWhseKey
        
        Debug.Print sSQL
        
        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            MsgBox Err.Description, vbOKOnly, gsBuildString(kSotaTitle, moClass.moAppDB, moClass.moSysSession)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
        On Error GoTo ExpectedErrorRoutine
    
    End If
    
    bDeleteZones = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteZones", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bDeleteBins(lWhseKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String

    bDeleteBins = False

    If lWhseKey > 0 Then
        
        sSQL = "DELETE timWhseBin WHERE WhseKey = " & lWhseKey
        
        Debug.Print sSQL
        
        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            MsgBox Err.Description, vbOKOnly, gsBuildString(kSotaTitle, moClass.moAppDB, moClass.moSysSession)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
        On Error GoTo ExpectedErrorRoutine
    
    End If
    
    bDeleteBins = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteBins", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bUpdateContact(lCntctKey As Long, lWhseKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
   
    Dim sSQL As String

    bUpdateContact = False

    If lCntctKey = 0 Then
        
        sSQL = "DELETE tciContact WHERE CntctOwnerKey = " & lWhseKey & " AND EntityType = " & 703
        
        Debug.Print sSQL
        
        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        If Err.Number <> 0 Then
            MsgBox Err.Description, vbOKOnly, gsBuildString(kSotaTitle, moClass.moAppDB, moClass.moSysSession)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
        On Error GoTo ExpectedErrorRoutine
    End If
    
    bUpdateContact = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateContact", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Function lListIndexFromText(cboctl As Control, sTextID As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    For lListIndexFromText = 0 To cboctl.ListCount - 1
        If Trim(cboctl.List(lListIndexFromText)) = Trim(sTextID) Then Exit Function
    Next
    lListIndexFromText = -1
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lListIndexFromText", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub RefreshStates(oState As Control, oCountry As Control)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    oState.Clear
    oState.InitDynamicList moClass.moAppDB, "SELECT StateID FROM tsmState WHERE CountryID = " & gsQuoted(oCountry.Text)
    oState.Refresh
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RefreshStates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub RefreshPrinters(oPrinter As Control, sProgramName As String, lKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'    oPrinter.Clear
'    oPrinter.InitDynamicList moClass.moAppDB, "SELECT RptSettingID FROM tsmReportSetting WHERE CompanyID = " & gsQuoted(msCompanyID) _
'                                             & " AND RptProgram = " & gsQuoted(sProgramName) _
'                                             & " RptSettingKey = " & glGetValidLong(lKey)
'    oPrinter.Refresh
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RefreshPrinters", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub SetPostalCodeMask(oPostalCode As Control, oCountry As Control)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sPostalCodeMask As String
    Dim sPostalCode As String
    
    '-- save postal code
    sPostalCode = oPostalCode.Text
    
    '-- get postal code mask
    If Len(Trim(oCountry.Text)) > 0 Then
        sPostalCodeMask = moClass.moAppDB.Lookup("PostalCodeMask", "tsmCountry", "CountryID = " & gsQuoted(oCountry.Text))
    Else
        sPostalCodeMask = ""
    End If
    
    '-- set the postal code mask and restrict clause
    oPostalCode.Mask = sPostalCodeMask
    oPostalCode.RestrictClause = "CountryID =" & gsQuoted(oCountry.Text)

    '-- reset postal code
    oPostalCode.Text = sPostalCode

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPostalCodeMask", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'Public Function CMAppendContextMenu(oCtl As Control, lMenu As Long) As Boolean
''+++ VB/Rig Begin Push +++
'#If ERRORTRAPON Then
'On Error GoTo VBRigErrorRoutine
'#End If
''+++ VB/Rig End +++
'
'    CMAppendContextMenu = False
'
'    If (oCtl Is lkuMain) And moDmForm.State <> kDmStateNone And chkUseBins Then
'        AppendMenu lMenu, MF_ENABLED, 21000, _
'            gsBuildString(kIMSetupBins, moClass.moAppDB, moClass.moSysSession)
'        CMAppendContextMenu = True
'    End If
'
''+++ VB/Rig Begin Pop +++
'        Exit Function
'
'VBRigErrorRoutine:
'        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM
'        Select Case VBRIG_IS_NON_EVENT
'        Case VBRIG_IS_NON_EVENT
'                Err.Raise guSotaErr.Number
'        Case Else
'                Call giErrorHandler: Exit Function
'        End Select
''+++ VB/Rig End +++
'End Function

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyApp = App
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyApp_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyForms_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Function bBinsAreUsed() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

' Checks timWhseBinInvt if any bin in this warehouse (pulled up on this form)
' has a non-zero quantity on hand.
' Returns TRUE if there are non-zero qty-on-hand bins
' Returns FALSE if there are no non-zero qty-on-hand bins

Dim rs As Object
Dim sWarehouseKey As String
Dim sSQL As String

sWarehouseKey = gsGetValidStr(moDmForm.GetColumnValue("WhseKey"))

If Len(Trim$(sWarehouseKey)) > 0 Then
    sSQL = "SELECT 'CountOfBinWithQty' = COUNT(*) " & _
           "FROM timWhseBin wb " & _
            "INNER JOIN timWhseBinInvt wbi ON " & _
                "wb.WhseBinKey = wbi.WhseBinKey " & _
            "WHERE wb.WhseKey = " & sWarehouseKey & " AND wbi.QtyOnHand <> 0" & _
            " AND wb.WhseBinID <> 'Default'"
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If glGetValidLong(rs.Field("CountOfBinWithQty")) > 0 Then
        bBinsAreUsed = True
    Else
        'This warehouse's bins are all empty
        bBinsAreUsed = False
    End If
    
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
Else
    bBinsAreUsed = False
End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bBinsAreUsed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
' Must be called after binding DM

    Set moGM = New clsGridMgr
    With moGM
        Set .Grid = grdTransferRules
        Set .DM = moDmGrid
        Set .Form = Me
        .GridType = kGridDataSheet
         Set moGrdNavTransitWhse = .BindColumn(kColTransitWarehouse, navTransitWhse)
         Set moGrdNavTransitWhse.ReturnControl = txtGrdNavTransitWhse
         Set moGrdNavShipMethod = .BindColumn(kColShipMethod, navShipMethod)
         Set moGrdNavShipMethod.ReturnControl = txtGrdNavShipMethod
         Set moGrdNavRcvgWhse = .BindColumn(kColReceivingWarehouse, navRcvgWhse)
         Set moGrdNavRcvgWhse.ReturnControl = txtGrdNavRcvgWhse
         .Init
    End With

    Set moGMZoneGrid = New clsGridMgr
    With moGMZoneGrid
        Set .Grid = grdWhseZones
        Set .DM = moDmZoneGrid
        Set .Form = Me
        .GridType = kGridDataSheet
        .GridSortEnabled = True
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub InitializePickShipTab()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    chkShipComplete.Value = 0
    chkPickFromSO.Value = 0
    chkShipFromPick.Value = 0
    ddnDefPickMethod.SetToDefault True
    ddnDefLotPickMethod.SetToDefault True
    ddnImPickPrintSetting.ListIndex = 0
    ddnCallPickPrintSetting.ListIndex = 0
    ddnImInvPrintSetting.ListIndex = 0
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "InitializePickShipTab", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub InitializeTransferTab()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    optFreightCharges(0).Value = True
    optSurcharges(0).Value = True
    
    ' Transfer Tab GL Account controls
    With glaExpenseAcct
        .Text = ""
        .Enabled = False
    End With
    'Commented out the following lines to fix Scopus 19925
   ' With glaFreightSurcharge
   '     .Text = ""
   '     .Enabled = False
   ' End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeTransferTab", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub FormatGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iDefaultVal As Integer
    Dim iDecimalPlaces As Integer
    
       
    ' Setting the maximum number of columns for the grdTransferRules
    gGridSetProperties grdTransferRules, kMaxCols, kGridDataSheet

    ' Making both horizontal and vertical scrollbars appear
    grdTransferRules.ScrollBars = SS_SCROLLBAR_V_ONLY  ' when debugging, change this to
                                                       ' SS_SCROLLBAR_BOTH so that one can
                                                       ' scroll to the right and see the
                                                       ' hidden columns (which has to be made
                                                       ' visible first.)
    
    ' Setting the column widths of grdTransferRules
    grdTransferRules.RowHeight(0) = 20
    gGridSetColumnWidth grdTransferRules, kColReceivingWarehouse, kColReceivingWarehouseWidth
    gGridSetColumnWidth grdTransferRules, kColShipMethod, KColShipMethodWidth
    gGridSetColumnWidth grdTransferRules, kColSurchargePercent, kColSurchargePercentWidth
    gGridSetColumnWidth grdTransferRules, kColSurchargeAmount, kColSurchargeAmountWidth
    gGridSetColumnWidth grdTransferRules, kColSurchargeAmountMethod, kColSurchargeAmountMethodWidth
    gGridSetColumnWidth grdTransferRules, kColAutomaticApproval, kColAutomaticApprovalWidth
    gGridSetColumnWidth grdTransferRules, kColTransitWarehouse, kColTransitWarehouseWidth
    gGridSetColumnWidth grdTransferRules, kColLeadTimeDays, kColLeadTimeDaysWidth

    ' Setting the header cells
    gGridSetHeader grdTransferRules, kColReceivingWarehouse, _
gsBuildString(kIMReceivingWhse, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColShipMethod, _
gsBuildString(kIMShipMethod, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColSurchargePercent, _
gsBuildString(kIMSurchargePercent, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColSurchargeAmount, _
gsBuildString(kIMSurchargeAmount, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColSurchargeAmountMethod, _
gsBuildString(kIMSurchargeAmountMethod, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColAutomaticApproval, _
gsBuildString(kIMAutomaticApproval, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColTransitWarehouse, _
gsBuildString(kIMTransitWhse, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdTransferRules, kColLeadTimeDays, _
gsBuildString(kIMLeadTimeDays, moClass.moAppDB, moClass.moSysSession)

    ' The following headers are for hidden columns during DEBUGGING, please DON'T REMOVE!
'    gGridSetHeader grdTransferRules, kColShipWhseKey, "Ship Whse KEY"
'    gGridSetHeader grdTransferRules, kColReceivingWarehouseKey, "Rcvg Whse KEY"
'    gGridSetHeader grdTransferRules, kColShipMethodKey, "Ship Method KEY"
'    gGridSetHeader grdTransferRules, kColSurchargeAmountMethodValue, "Surch Amt Meth"
'    gGridSetHeader grdTransferRules, kColAutomaticApprovalValue, "Auto Approv Val"
'    gGridSetHeader grdTransferRules, kColTransitWarehouseKey, "Transit Whse KEY"
'    gGridSetHeader grdTransferRules, kColSurchargePercentValue, "Surcharge Pct"

    ' Setting the types of some columns
    iDecimalPlaces = iGettciOptionDecimalPlaces()   ' get decimal places for Surcharge Amount
                                                    ' and Surcharge Percent
    gGridSetColumnType grdTransferRules, kColSurchargePercent, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdTransferRules, kColSurchargeAmount, SS_CELL_TYPE_FLOAT, iDecimalPlaces
    gGridSetColumnType grdTransferRules, kColSurchargeAmountMethodValue, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdTransferRules, kColSurchargeAmountMethod, SS_CELL_TYPE_COMBOBOX
    gGridSetColumnType grdTransferRules, kColAutomaticApprovalValue, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdTransferRules, kColAutomaticApproval, SS_CELL_TYPE_COMBOBOX
    gGridSetColumnType grdTransferRules, kColLeadTimeDays, SS_CELL_TYPE_INTEGER, kMaxIntegerDigits

    'Hiding the columns that need to be hidden
    gGridHideColumn grdTransferRules, kColShipWhseKey
    gGridHideColumn grdTransferRules, kColReceivingWarehouseKey
    gGridHideColumn grdTransferRules, kColShipMethodKey
    gGridHideColumn grdTransferRules, kColSurchargeAmountMethodValue
    gGridHideColumn grdTransferRules, kColAutomaticApprovalValue
    gGridHideColumn grdTransferRules, kColTransitWarehouseKey
    gGridHideColumn grdTransferRules, kColSurchargePercentValue
    
    ' Read the drop down values from tsmListValidation
    moStatLstSurchargeAmountMethod.InitStaticList moClass.moAppDB, moClass.moSysSession.Language, "timWhseTrnsfr", "SurchargeFixedMeth"
    grdTransferRules.Row = -1
    grdTransferRules.Col = kColSurchargeAmountMethod
    grdTransferRules.TypeComboBoxList = moStatLstSurchargeAmountMethod.ListData
    moStatLstAutomaticApproval.InitStaticList moClass.moAppDB, moClass.moSysSession.Language, "timWhseTrnsfr", "AutoApproveMeth"
    grdTransferRules.Row = -1
    grdTransferRules.Col = kColAutomaticApproval
    grdTransferRules.TypeComboBoxList = moStatLstAutomaticApproval.ListData
    
    
    gbLookupInit navRcvgWhse, moClass, moClass.moAppDB, "Warehouse"
    gbLookupInit navShipMethod, moClass, moClass.moAppDB, "ShipMethod"
    gbLookupInit navTransitWhse, moClass, moClass.moAppDB, "Warehouse"
    
    
     ' Setting the maximum number of columns for the grdWhseZones
    gGridSetProperties grdWhseZones, kMaxZoneCols, kGridDataSheet
    
    ' Setting the column widths of grdWhseZones
    gGridSetColumnWidth grdWhseZones, kColWhseZoneID, 20
    gGridSetColumnWidth grdWhseZones, kColDescription, 25
    gGridSetColumnWidth grdWhseZones, kColSortOrder, 10
    
    'Hiding the columns that need to be hidden
    gGridHideColumn grdWhseZones, kColWhseZoneKey
    
    ' Setting the header cells
    gGridSetHeader grdWhseZones, kColWhseZoneID, "Zone"
    gGridSetHeader grdWhseZones, kColDescription, "Zone Description"
    gGridSetHeader grdWhseZones, kColSortOrder, "Sort Order"
    
     ' Setting the types
    gGridSetColumnType grdWhseZones, kColWhseZoneID, SS_CELL_TYPE_EDIT, 15
    gGridSetColumnType grdWhseZones, kColDescription, SS_CELL_TYPE_EDIT, 30
    gGridSetColumnType grdWhseZones, kColSortOrder, SS_CELL_TYPE_INTEGER, 4
    
    
   
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moGM_CellChange(ByVal Row As Long, ByVal Col As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    LeaveCellValidations Row, Col
    
    moDmGrid.SetRowDirty (Row)
    moGM.Grid_Change Col, Row

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGM_CellChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ValidateWarehouse(sWarehouseID As String, bExists As Boolean, bIsTransit As Boolean, _
lWareHouseKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' This routine checks two things:
    ' 1) whether or not sWarehouseID exists in the timWarehouse table;
    ' 2) whether or not sWarehouseID is a transit warehouse
    '
    ' bExists will be True if sWarehouseID exists, False if it doesn't
    ' bIsTransit will be True if sWarehouseID is a Transit warehouse, False if not

    Dim rs As Object
    Dim sSQL As String
    
    sWarehouseID = Trim(sWarehouseID)
    bExists = False
    bIsTransit = False
    
    sSQL = "SELECT WhseID,WhseKey,Transit FROM timWarehouse " & _
            "WHERE WhseID = " & gsQuoted(sWarehouseID) & " AND CompanyID = " & gsQuoted(msCompanyID)
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.RecordCount > 0 Then
        bExists = True
        If giGetValidInt(rs.Field("Transit")) = 1 Then
            bIsTransit = True
        Else
            bIsTransit = False
        End If
        lWareHouseKey = glGetValidLong(rs.Field("WhseKey"))
    End If
    
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValidateWarehouse", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bShipMethodIsValid(sShipMethod As String, lShipMethKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' This function does two things:
    ' 1) Checks if a shipping method (sShipMethod) exists in tciShipMethod table
    ' 2) If it exists, it retrieves the shipping method key (lShipMethKey)

    Dim rs As Object
    Dim sSQL As String
    
    sShipMethod = Trim(sShipMethod)
    
    sSQL = "SELECT ShipMethID,ShipMethKey FROM tciShipMethod " & _
            "WHERE ShipMethID = " & gsQuoted(sShipMethod) & " AND CompanyID = " & gsQuoted(msCompanyID)
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.RecordCount > 0 Then
        lShipMethKey = glGetValidLong(rs.Field("ShipMethKey"))
        bShipMethodIsValid = True
    Else
        bShipMethodIsValid = False
    End If
    
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bShipMethodIsValid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub DoFinish()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer
    
    'If either the surchargs or freight amounts are expensed, then the GL Expense Acct no
    'must be entered.
    If optFreightCharges(1) Or optSurcharges(1) Then
        If Len(Trim$(glaExpenseAcct.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInboundExpAcct
            If glaExpenseAcct.Enabled Then glaExpenseAcct.SetFocus
            Exit Sub
        End If
    End If

    If grdTransferRules.MaxRows - 1 <> 0 Then
        If Not bGridRowsAreValid() Then             ' Validate all rows in grdTransferRules
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If

    If chkIntransitWhs.Value = 1 Then
        If glaInventoryIsBlank Then
            glaInventory.SetFocus
            Exit Sub
        End If
    End If
    
    If Not moValMgr.IsValidDirtyCheck Then Exit Sub
    
    mbManualComboSelect = True
    iActionCode = moDmForm.Action(kDmFinish)
    
    If iActionCode <> kDmSuccess Then Exit Sub
    
    mbManualComboSelect = False
    moDmZoneGrid.Save
    
    If lkuMain.Enabled Then
        lkuMain.Text = ""
        lkuMain.SetFocus
        InitializeMainTab
        InitializeAddressTab
        InitializeReplenishTab
        InitializeGLAccounts
        InitializeTransferTab
        InitializePickShipTab
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoFinish", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DoCancel()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer
    
    mbDmLoad = True
    mbManualComboSelect = True
    
    iActionCode = moDmForm.Action(kDmCancel)
    
    If iActionCode <> kDmSuccess Then Exit Sub
    
    mbManualComboSelect = False

    moGM.Clear

    If lkuMain.Enabled Then
        lkuMain.Text = ""
        lkuMain.SetFocus
        InitializeMainTab
        InitializeAddressTab
        InitializeReplenishTab
        InitializeGLAccounts
        InitializeTransferTab
        InitializePickShipTab
        moValMgr.SetAllValid
    End If
    
    mbDMStateChanged = False
    txtMLatitude.Tag = ""
    txtMLongitude.Tag = ""
    txtSLatitude.Tag = ""
    txtSLongitude.Tag = ""

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoCancel", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DoDelete()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer
    
    iActionCode = moDmForm.Action(kDmDelete)
    
    If iActionCode <> kDmSuccess Then
        mbDmLoad = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    If lkuMain.Enabled Then
        lkuMain.Text = ""
        On Error Resume Next
        lkuMain.SetFocus
        InitializeMainTab
        InitializeAddressTab
        InitializeReplenishTab
        InitializeGLAccounts
        InitializeTransferTab
        InitializePickShipTab
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DoPrint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'If either the surchargs or freight amounts are expensed, then the GL Expense Acct no
    'must be entered.
    If optFreightCharges(1) Or optSurcharges(1) Then
        If Len(Trim$(glaExpenseAcct.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInboundExpAcct
            If glaExpenseAcct.Enabled Then glaExpenseAcct.SetFocus
            Exit Sub
        End If
    End If

    If Not moValMgr.IsValidDirtyCheck Then Exit Sub
    
    'the conserned task has to be launched from here
    If moDmForm.ConfirmUnload(True) = kDmSuccess Then                 'Have user save changes
        gPrintTask moClass.moFramework, ktskIMWarehouseLST
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoPrint", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DoSave()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iActionCode As Integer
    
    'If either the surchargs or freight amounts are expensed, then the GL Expense Acct no
    'must be entered.
    If optFreightCharges(1) Or optSurcharges(1) Then
        If Len(Trim$(glaExpenseAcct.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInboundExpAcct
            If glaExpenseAcct.Enabled Then glaExpenseAcct.SetFocus
            Exit Sub
        End If
    End If

    If grdTransferRules.MaxRows - 1 <> 0 Then
        If Not bGridRowsAreValid() Then                 ' Validate all rows in grdTransferRules
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If

    If chkIntransitWhs.Value = 1 Then
        If glaInventoryIsBlank Then
            glaInventory.SetFocus
            Exit Sub
        End If
    End If
    
    'Validate Duplicate Zone entry
    If Not bValidateDuplicateZone Then
        Exit Sub
    End If
    
    If Not moValMgr.IsValidDirtyCheck Then Exit Sub
    
    mbManualComboSelect = True
    
    iActionCode = moDmForm.Save(True)
    
    If iActionCode <> kDmSuccess Then Exit Sub
    
    mbManualComboSelect = False
   
    ' This is necessary to prevent users from further modifying the Receiving Warehouse.
    ' If the user wants to change it, they'd have to delete the whole line and reenter it.
    LockReceivingWarehouseColumn

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DoSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub LeaveCellValidations(lRow As Long, lCol As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sWarehouseID As String
    Dim bExists As Boolean
    Dim bIsTransit As Boolean
    Dim sInputtedValue As String
    Dim lLoop As Long
    Dim lWareHouseKey As Long
    Dim lShipMethKey As Long
    Dim sglTemp As Single

    If (Me.ActiveControl.Name = navRcvgWhse.Name _
    Or Me.ActiveControl.Name = navShipMethod.Name _
    Or Me.ActiveControl.Name = navTransitWhse.Name) Then
        Exit Sub
    End If

    
    sWarehouseID = gsGetValidStr(moDmForm.GetColumnValue("WhseID"))

    Select Case lCol
    
        Case kColReceivingWarehouse   ' Receiving Warehouse column in grdTransferRules grid
            
            sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColReceivingWarehouse))
            
            If Len(sInputtedValue) = 0 Then
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                gGridUpdateCell grdTransferRules, lRow, kColReceivingWarehouseKey, ""
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            ' Warehouse entered must not be the same as warehouse being maintained
            If sInputtedValue = sWarehouseID Then
                mbDoNotLeaveCell = True
                'MsgBox "Transfer Tab: You may not use the warehouse you are maintaining."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab01
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
    
            ' Warehouse must not already have been previously entered in the grid
            For lLoop = 1 To grdTransferRules.DataRowCnt
                If lLoop <> lRow Then
                    If gsGridReadCell(grdTransferRules, lLoop, kColReceivingWarehouse) = sInputtedValue Then
                        mbDoNotLeaveCell = True
                        'MsgBox "Transfer Tab: This warehouse has already been entered."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab02
                        gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                End If
            Next lLoop
            
            ValidateWarehouse sInputtedValue, bExists, bIsTransit, lWareHouseKey
    
            ' Warehouse cannot be a Transit warehouse
            If bIsTransit Then
                ' MsgBox "Transfer Tab: You may not use a Transit warehouse."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab03
                mbDoNotLeaveCell = True
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            ' Warehouse must be existent in timWarehouse
            If Not bExists Then
                mbDoNotLeaveCell = True
                ' MsgBox "Transfer Tab: Please enter a valid warehouse."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab04
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                gGridUpdateCell grdTransferRules, lRow, kColReceivingWarehouseKey, _
                        gsGetValidStr(lWareHouseKey)
            End If
            
        Case kColShipMethod

            sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColShipMethod))
            
            If Len(sInputtedValue) = 0 Then
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                gGridUpdateCell grdTransferRules, lRow, kColShipMethodKey, ""
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            ' Warehouse must be existent in timWarehouse
            If Not bShipMethodIsValid(sInputtedValue, lShipMethKey) Then
                mbDoNotLeaveCell = True
                ' MsgBox "Transfer Tab: Please enter a valid shipping method."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab05
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                gGridUpdateCell grdTransferRules, lRow, kColShipMethodKey, gsGetValidStr(lShipMethKey)
            End If
            
        Case kColSurchargePercent
        
            sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColSurchargePercent))
            
            If Val(sInputtedValue) < 0 Or Val(sInputtedValue) > 100 Then
                mbDoNotLeaveCell = True
                'MsgBox "Transfer Tab: Please enter a Surcharge Percent value greater than " & _
                '       "or equal to 0 and less than or equal to 100."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab06
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                sglTemp = gdGetValidDbl(gsGridReadCellText(grdTransferRules, lRow, kColSurchargePercent))
                sglTemp = sglTemp * 0.01

                gGridUpdateCell grdTransferRules, lRow, lCol, Val(gsGetValidStr(sInputtedValue))
                gGridUpdateCell grdTransferRules, lRow, kColSurchargePercentValue, gsGetValidStr(sglTemp)
            End If

        Case kColSurchargeAmount
        
            sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColSurchargeAmount))
            
            If Val(sInputtedValue) < 0 Then
                mbDoNotLeaveCell = True
                ' MsgBox "Transfer Tab: Please enter a Surcharge amount greater than 0."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab07
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                gGridUpdateCell grdTransferRules, lRow, lCol, Val(gsGetValidStr(sInputtedValue))
            End If

        Case kColTransitWarehouse
        
            sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColTransitWarehouse))
            
            If Len(sInputtedValue) = 0 Then
                gGridUpdateCell grdTransferRules, lRow, kColTransitWarehouseKey, ""
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            ' Warehouse must be existent in timWarehouse and a Transit warehouse as well
            ValidateWarehouse sInputtedValue, bExists, bIsTransit, lWareHouseKey
            If Not bExists Or Not bIsTransit Then
                mbDoNotLeaveCell = True
                'MsgBox "Transfer Tab: Please enter a valid Transit warehouse."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab08
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                gGridUpdateCell grdTransferRules, lRow, kColTransitWarehouseKey, _
                gsGetValidStr(lWareHouseKey)
            End If
        
        Case kColLeadTimeDays
            
            sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColLeadTimeDays))
            
            If Val(sInputtedValue) < 0 Or Val(sInputtedValue) > 9999 Then
                mbDoNotLeaveCell = True
                ' MsgBox "Transfer Tab: Please enter lead time days between 0 and 9999."
                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab09
                gGridUpdateCell grdTransferRules, lRow, lCol, giGetValidInt(mvValueHolder)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                gGridUpdateCell grdTransferRules, lRow, lCol, Val(gsGetValidStr(sInputtedValue))
            End If
        
    End Select
    
    mbDoNotLeaveCell = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LeaveCellValidations", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bGridRowsAreValid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sWarehouseID As String
    Dim bExists As Boolean
    Dim bIsTransit As Boolean
    Dim sInputtedValue As String
    Dim lLoop As Long
    Dim lWareHouseKey As Long
    Dim lShipMethKey As Long
    Dim lRow As Long
    Dim lCol As Long
    Dim rs As Object
    Dim sSQL As String
    Dim bVerifyRcvgWhseoptions As Boolean
    
    HideAllNavButtons
    bGridRowsAreValid = False
    sWarehouseID = gsGetValidStr(moDmForm.GetColumnValue("WhseID"))
    mvValueHolder = ""
    mbSurchargePercentIsNonZero = False
    mbSurchargeAmountIsNonZero = False
    
    If optFreightCharges(1).Value Or optSurcharges(1).Value Then
        mbFreightAndOrSurchargesExpensed = True
    Else
        mbFreightAndOrSurchargesExpensed = False
    End If
    
    For lRow = 1 To grdTransferRules.MaxRows - 1
        bVerifyRcvgWhseoptions = False
        For lCol = 1 To kNumberOfVisibleColumns
            Select Case lCol
                Case kColReceivingWarehouse   ' Receiving Warehouse column in grdTransferRules grid
                    sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColReceivingWarehouse))
                    
                    ' Receiving warehouse many not be blank
                    If Len(sInputtedValue) = 0 Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab04
                        gGridSetActiveCell grdTransferRules, lRow, kColReceivingWarehouse
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
                    ' Warehouse entered must not be the same as warehouse being maintained
                    If sInputtedValue = sWarehouseID Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: You may not use the warehouse you are maintaining."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab01
                        gGridSetActiveCell grdTransferRules, lRow, kColReceivingWarehouse
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
            
                    ' Warehouse must not already have been previously entered in the grid
                    For lLoop = 1 To grdTransferRules.DataRowCnt
                        If lLoop <> lRow Then
                            If gsGridReadCell(grdTransferRules, lLoop, kColReceivingWarehouse) = _
sInputtedValue Then
                                mbDoNotLeaveCell = True
                                tabWarehouse.Tab = kTabTransfer
                                ' MsgBox "Transfer Tab: This warehouse has already been entered."
                                giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab02
                                gGridSetActiveCell grdTransferRules, lRow, kColReceivingWarehouse
                                grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                                Exit Function
                            End If
                        End If
                    Next lLoop
                    
                    ValidateWarehouse sInputtedValue, bExists, bIsTransit, lWareHouseKey
            
                    ' Warehouse cannot be a Transit warehouse
                    If bIsTransit Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: You may not use a Transit warehouse."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab03
                        gGridSetActiveCell grdTransferRules, lRow, kColReceivingWarehouse
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
                    ' Warehouse must be existent in timWarehouse
                    If Not bExists Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: Please enter a valid warehouse."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab04
                        gGridSetActiveCell grdTransferRules, lRow, kColReceivingWarehouse
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Else
                        gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                        gGridUpdateCell grdTransferRules, lRow, kColReceivingWarehouseKey, _
                        gsGetValidStr(lWareHouseKey)
                    End If
                    
                Case kColShipMethod
        
                    sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColShipMethod))
                    
                    ' Shipping method may not be blank
                    If Len(sInputtedValue) = 0 Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: Please enter a valid shipping method."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab05
                        gGridSetActiveCell grdTransferRules, lRow, kColShipMethod
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
                    ' Shipping method must be an existing one in tciShipMethod
                    If Not bShipMethodIsValid(sInputtedValue, lShipMethKey) Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: Please enter a valid shipping method."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab05
                        gGridSetActiveCell grdTransferRules, lRow, kColShipMethod
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Else
                        gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                        gGridUpdateCell grdTransferRules, lRow, kColShipMethodKey, _
                        gsGetValidStr(lShipMethKey)
                    End If
                    
                Case kColSurchargePercent
                
                    sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColSurchargePercent))
                    
                    ' Surcharge Percent must >=0 and <=100
                    If Val(sInputtedValue) < 0 Or Val(sInputtedValue) > 100 Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        'MsgBox "Transfer Tab: Please enter a Surcharge Percent value greater than " & _
                        '       "or equal to 0 and less than or equal to 100."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab06
                        gGridSetActiveCell grdTransferRules, lRow, kColSurchargePercent
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Else
                        'if the SurchargeAmount/ SurchargePercentage <> 0 and the Freight and Surcharge options are
                        'Expense for that receiving warehouse, then the GL Expense Account must not be null for the
                        'receiving warehouse
                        If Val(sInputtedValue) > 0 Then
                            If Not bVerifyRcvgWhseoptions Then
                                bVerifyRcvgWhseoptions = True
                                If Not bRcvgWhseExpAcct(lWareHouseKey) Then
                                    gGridSetActiveCell grdTransferRules, lRow, kColSurchargePercent
                                    grdTransferRules.SetFocus
                                    Exit Function
                                End If
                            End If
                        End If
                        gGridUpdateCell grdTransferRules, lRow, lCol, gdGetValidDbl(sInputtedValue)
                        gGridUpdateCell grdTransferRules, lRow, kColSurchargePercentValue, _
gdGetValidDbl(Val(sInputtedValue) * 0.01)
                        
                        If Not mbSurchargePercentIsNonZero Then
                            If Val(sInputtedValue) > 0 Then
                                mbSurchargePercentIsNonZero = True
                            End If
                        End If
                        
                    End If
        
                Case kColSurchargeAmount
                
                    sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColSurchargeAmount))
                    
                    ' Surcharge amount must not be negative
                    If Val(sInputtedValue) < 0 Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: Please enter a Surcharge Amount greater than 0."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab07
                        gGridSetActiveCell grdTransferRules, lRow, kColSurchargeAmount
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Else
                        'if the SurchargeAmount/ SurchargePercentage <> 0 and the Freight and Surcharge options are
                        'Expense for that receiving warehouse, then the GL Expense Account must not be null for the
                        'receiving warehouse
                        If Val(sInputtedValue) > 0 Then
                            If Not bVerifyRcvgWhseoptions Then
                                bVerifyRcvgWhseoptions = True
                                If Not bRcvgWhseExpAcct(lWareHouseKey) Then
                                    gGridSetActiveCell grdTransferRules, lRow, kColSurchargeAmount
                                    grdTransferRules.SetFocus
                                    Exit Function
                                End If
                            End If
                        End If
                        gGridUpdateCell grdTransferRules, lRow, lCol, gdGetValidDbl(sInputtedValue)

                        If Not mbSurchargeAmountIsNonZero Then
                            If Val(sInputtedValue) > 0 Then
                                mbSurchargeAmountIsNonZero = True
                            End If
                        End If
                        
                    End If
        
                Case kColTransitWarehouse
                
                    sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColTransitWarehouse))
                    
                    ' Transit warehouse must not be blank
                    If Len(sInputtedValue) = 0 Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: Please enter a valid Transit warehouse."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab08
                        gGridSetActiveCell grdTransferRules, lRow, kColTransitWarehouse
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
                    ' Warehouse must be existent in timWarehouse and a Transit warehouse as well
                    ValidateWarehouse sInputtedValue, bExists, bIsTransit, lWareHouseKey
                    If Not bExists Or Not bIsTransit Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                       ' MsgBox "Transfer Tab: Please enter a valid Transit warehouse."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab08
                        gGridSetActiveCell grdTransferRules, lRow, kColTransitWarehouse
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Else
                        gGridUpdateCell grdTransferRules, lRow, lCol, gsGetValidStr(sInputtedValue)
                        gGridUpdateCell grdTransferRules, lRow, kColTransitWarehouseKey, _
                        gsGetValidStr(lWareHouseKey)
                    End If
                    
                Case kColLeadTimeDays
                    
                    sInputtedValue = Trim(gsGridReadCell(grdTransferRules, lRow, kColLeadTimeDays))
                    
                    ' Lead Time Days must not be negative and no greater than 9999
                    If Val(sInputtedValue) < 0 Or Val(sInputtedValue) > 9999 Then
                        mbDoNotLeaveCell = True
                        tabWarehouse.Tab = kTabTransfer
                        ' MsgBox "Transfer Tab: Please enter lead time days between 0 and 9999."
                        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab09
                        gGridSetActiveCell grdTransferRules, lRow, kColLeadTimeDays
                        grdTransferRules.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    Else
                        gGridUpdateCell grdTransferRules, lRow, lCol, giGetValidInt(sInputtedValue)
                    End If
                
            End Select
            
            mbDoNotLeaveCell = False
    
        Next lCol
    Next lRow

    ' If Surcharge Amount OR Surcharge Percent is non-zero, then the Outbound Freight & Surcharge
    ' GL account must NOT be empty
    If mbSurchargePercentIsNonZero Or mbSurchargeAmountIsNonZero Then
        If Val(Trim(glaFreightSurcharge.Text)) = 0 Then
            tabWarehouse.Tab = kTabTransfer
            ' MsgBox "'You entered a Surcharge Percent or Surcharge Amount. " & _
            '        "Please provide a Freight & Surcharge G/L Account."
            giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab11
            If glaFreightSurcharge.Enabled Then
                glaFreightSurcharge.SetFocus
            Else
                glaFreightSurcharge.Enabled = True
                glaFreightSurcharge.SetFocus
            End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    'The Following was commented out per Design Scopus 19513
    ' If Surcharge Amount OR Surcharge Percent is non-zero, AND, either one of the Inbound option
    ' controls are set to "Expense" (as opposed to "Capitalize"), then the Inbound GL Account
    ' must NOT be empty
    'If mbSurchargePercentIsNonZero Or mbSurchargeAmountIsNonZero Then
    '    If mbFreightAndOrSurchargesExpensed Then
     '       If Val(Trim(glaExpenseAcct.Text)) = 0 Then
      '          tabWarehouse.Tab = kTabTransfer
                ' MsgBox "You entered a Surcharge Percent or Surcharge Amount, and " & _
                '        "Freight & Surcharges will be expensed. Please provide a " & _
'        "Freight & Surcharge G/L Account."
       '         giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab12
        '        glaExpenseAcct.SetFocus
'+++ VB/Rig Begin Pop +++
'+++ VB/R'ig End +++
          '      Exit Function
           ' End If
        'End If
    'End If
    
    bGridRowsAreValid = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGridRowsAreValid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub FillInGridRowDefaults(Row As Long, Col As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Len(Trim(gsGridReadCellText(grdTransferRules, Row, kColReceivingWarehouse))) = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    ' The "ShipWhseKey" column for the grid in kColShipWhseKey (hidden grid column 9)
    gGridUpdateCell grdTransferRules, Row, kColShipWhseKey, moDmForm.GetColumnValue("WhseKey")
    
    ' Get the defaults for the grid dropdowns if not shown yet
    ' Surcharge Percent column
    If gsGridReadCellText(grdTransferRules, Row, kColSurchargePercent) = "" Then
        gGridUpdateCellText grdTransferRules, Row, kColSurchargePercent, 0
    End If
    
    ' Surcharge Amount column
    If gsGridReadCellText(grdTransferRules, Row, kColSurchargeAmount) = "" Then
        gGridUpdateCellText grdTransferRules, Row, kColSurchargeAmount, 0
    End If
    
    ' Surcharge Amount Method column (drop down)
    If gsGridReadCellText(grdTransferRules, Row, kColSurchargeAmountMethod) = "" Then
        gGridUpdateCellText grdTransferRules, Row, kColSurchargeAmountMethod, moStatLstSurchargeAmountMethod.LocalText(kSurchargeAmountMethodDDNDefault)
        gGridUpdateCellText grdTransferRules, Row, kColSurchargeAmountMethodValue, _
kSurchargeAmountMethodDDNDefault
    End If
    
    ' Automatic Approval column (drop down)
    If gsGridReadCellText(grdTransferRules, Row, kColAutomaticApproval) = "" Then
        gGridUpdateCellText grdTransferRules, Row, kColAutomaticApproval, moStatLstAutomaticApproval.LocalText(kAutomaticApprovalDDNDefault)
        gGridUpdateCellText grdTransferRules, Row, kColAutomaticApprovalValue, _
kAutomaticApprovalDDNDefault
    End If
    
    ' Lead Time Days column
    If gsGridReadCellText(grdTransferRules, Row, kColLeadTimeDays) = "" Then
        gGridUpdateCellText grdTransferRules, Row, kColLeadTimeDays, 0
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FillInGridRowDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub HideAllNavButtons()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' Hides the pesky magnifying glass buttons of the AFNavigator controls
    navRcvgWhse.Visible = False
    navShipMethod.Visible = False
    navTransitWhse.Visible = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideAllNavButtons", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function iGettciOptionDecimalPlaces() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' This function returns the number of decimal places defined for Unit Prices
    ' and is to be applied to the Surcharge Amount column in grdTransferRules

    Dim rs As Object
    Dim sSQL As String
    
    sSQL = "SELECT UnitPriceDecPlaces FROM tciOptions " & _
            "WHERE CompanyID = " & gsQuoted(msCompanyID)
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.RecordCount > 0 Then
        iGettciOptionDecimalPlaces = rs.Field("UnitPriceDecPlaces")
    Else
        iGettciOptionDecimalPlaces = 2    ' default to 2 decimal places if not available in tciOptions
    End If
    
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGettciOptionDecimalPlaces", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub moGM_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' This event is entered only when the user attempts to delete a line off the grid.
    
    Dim lShipWhseKey As Long
    Dim lRcvgWhseKey As Long
    Dim lWhseKey As Long
    
    lShipWhseKey = glGetValidLong(moDmForm.GetColumnValue("WhseKey"))
    lRcvgWhseKey = glGetValidLong(gsGridReadCellText(grdTransferRules, grdTransferRules.ActiveRow, _
                                    kColReceivingWarehouseKey))
               
    ' This checks if the Shipping Warehouse/Receiving Warehouse combination has any
    ' existing transfer orders in the timTrnsfrOrder table.
    ' The function is set to False if deletion is to be disallowed, True if allowed.
    If bThereAreTransferOrders(grdTransferRules.ActiveRow, lShipWhseKey, lRcvgWhseKey) Then
        ' Tells grid manager not to let the delete operation continue
        bContinue = False
        ' Msgbox "This line cannot be deleted because there are already transfer orders " & _
        '        "for this shipping and receiving warehouse."
        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab10
        bContinue = False
    Else
        ' Tells grid manager to let the delete operation continue
        bContinue = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moGM_GridBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bZoneExistWithBin(lWhseKey As Long, lWhseZoneKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim rs As Object
Dim sSQL As String
    sSQL = "SELECT 'Rows' = COUNT(*) FROM timWhseBin " & _
           "WHERE WhseKey = " & glGetValidLong(lWhseKey) & " AND WhseZoneKey = " & _
                            glGetValidLong(lWhseZoneKey)
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.Field("Rows") > 0 Then
        bZoneExistWithBin = True
    Else
        bZoneExistWithBin = False
    End If
    mlNNBins = gsGetValidStr(rs.Field("Rows"))
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bZoneExistWithBin", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub DeleteZoneReference(lWhseKey As Long, Optional lWhseZoneKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                    'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
Dim lWhseBinKey As Long
    sSQL = "SELECT WhseBinKey FROM timWhseBin " & _
            "WHERE WhseKey = " & glGetValidLong(lWhseKey) & " AND WhseZoneKey IS NOT NULL"
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    lWhseBinKey = glGetValidLong(rs.Field("WhseBinKey"))
    sSQL = "UPDATE timWhseBin " _
        & " SET WhseZoneKey = NULL " _
        & " WHERE WhseKey = " & glGetValidLong(lWhseKey) _
        & " AND WhseBinKey = " & _
        glGetValidLong(lWhseBinKey)
    moClass.moAppDB.ExecuteSQL (sSQL)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteZoneReference", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bThereAreTransferOrders(lRow As Long, lShipWhseKey As Long, lRcvgWhseKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' This function returns True if a Shipping Warehouse/Receiving Warehouse combination
    ' has any existing transfer orders in timTrnsfrOrder table, False if there are none.

    Dim rs As Object
    Dim sSQL As String
    
    sSQL = "SELECT 'Cnt' = COUNT(*) FROM timTrnsfrOrder " & _
           "WHERE ShipWhseKey = " & glGetValidLong(lShipWhseKey) & " AND RcvgWhseKey = " & _
            glGetValidLong(lRcvgWhseKey) & " AND CompanyID = " & gsQuoted(msCompanyID)
                 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.Field("Cnt") > 0 Then
        bThereAreTransferOrders = True
    Else
        bThereAreTransferOrders = False
    End If
    
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bThereAreTransferOrders", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function CMRightMouseDown(hwnd As Long, lPass As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' When the user clicks on a cell with a lookup, the button with the magnifying glass appears.
    ' Should the user RIGHT-click on a different ROW and picks Add, the cell focus moves to the
    ' last row, sometimes leaving that pesky magnifying glass button still visible.
    '
    ' Varying undesirable results occur should the user click on that button at that point.
    '
    ' This routine hides all Nav buttons upon a right-click, which solves that problem.
    
    If hwnd = grdTransferRules.hwnd Then
        HideAllNavButtons
    End If
    
    ' Tells context menu object to still process the context menu
    CMRightMouseDown = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMRightMouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub LockReceivingWarehouseColumn()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    
    For lRow = 1 To grdTransferRules.MaxRows - 1
        gGridLockCell grdTransferRules, kColReceivingWarehouse, lRow
    Next lRow
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LockReceivingWarehouseColumn", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function glaInventoryIsBlank() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

' All this function does is to check whether or not glaInventory. The check for the validity of
' the GL account number entered is already being checked by the validation manager.
' This was a mod requested by Kathryn Ratliff. 08/03/2000

    If Len(Trim(glaInventory.Text)) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMGLInvtBlank, gsStripChar(lblInventory.Caption, "&")
        glaInventoryIsBlank = True
        tabWarehouse.Tab = kTabGLAccount
        Exit Function
    Else
        glaInventoryIsBlank = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmWareHouseMNT", "glaInventoryIsBlank", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bRcvgWhseExpAcct(ByVal lWareHouseKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
Dim iSurchargeOption As Integer
Dim iFreightOption As Integer
Dim lExpenseAcctKey As Long
Const koptionExpense = 2
Dim sWhse As String

bRcvgWhseExpAcct = False
    sSQL = "Select WhseID, TrnsfrExpAcctKey, TrnsfrSrchrgOpt, TrnsfrFrtChrgOpt From timWarehouse WITH (NOLOCK) Where WhseKey = " & glGetValidLong(lWareHouseKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.RecordCount > 0 Then
        sWhse = gsGetValidStr(rs.Field("WhseID"))
        iSurchargeOption = giGetValidInt(rs.Field("TrnsfrSrchrgOpt"))
        iFreightOption = giGetValidInt(rs.Field("TrnsfrFrtChrgOpt"))
        lExpenseAcctKey = glGetValidLong(rs.Field("TrnsfrExpAcctKey"))
        
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If

        If (iSurchargeOption = koptionExpense Or iFreightOption = koptionExpense) And lExpenseAcctKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMRcvgWhseExpAcct, sWhse
            Exit Function
        End If
    Else
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
        giSotaMsgBox Me, moClass.moSysSession, kIMTransferTab06
        Exit Function
    End If
    bRcvgWhseExpAcct = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bRcvgWhseExpAcct", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bChargeChangedDuring3StepsTransfer() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rs As Object

    bChargeChangedDuring3StepsTransfer = False
    
    'When doing a 3 step transfer without any line items, It is still OK. Because
    'It will excluse from this scripts.
    sSQL = "SELECT COUNT(*) Rows FROM timTrnsfrOrder T WITH (NOLOCK) JOIN timTrnsfrOrderLine TL WITH (NOLOCK) ON "
    sSQL = sSQL & "T.TrnsfrOrderKey = TL.TrnsfrOrderKey "
    sSQL = sSQL & "WHERE (T.Status = " & kStatusOpen & " OR TL.Status IN (" & kStatusUnapproved & "," & kStatusOpen & ")) AND "
    sSQL = sSQL & "T.CompanyID = " & gsQuoted(msCompanyID) & " AND T.RcvgWhseKey = " & glGetValidLong(lkuMain.KeyValue)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If glGetValidLong(rs.Field("Rows")) > 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMFrghtSurchgChange
        bChargeChangedDuring3StepsTransfer = True
    End If
    
    rs.Close
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bChargeChangedDuring3StepsTransfer", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Function

Private Sub LoadNoneSetting(oSetting As Control)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
If oSetting.ListCount = 0 Then
    If oSetting.Text <> kNoneSetting Then
       oSetting.AddItem kNoneSetting, 0
    End If
End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadNoneSetting", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bZonesCheck() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRow As Long
Dim lWhseZoneKey As Long
Dim iCount As Integer
Dim bDelete As Boolean
Dim sZoneID As String
Dim bZoneInWhse As Boolean
    bZonesCheck = True
    bZoneInWhse = False
    mbDeleteZones = False
        iCount = 0
        If mbLoadUseBins Then ' but only if checkbox was already checked when the form loaded
            For lRow = 1 To grdWhseZones.MaxRows - 1
                sZoneID = Trim(gsGetValidStr(gsGridReadCellText(grdWhseZones, lRow, _
                            kColWhseZoneID)))
                lWhseZoneKey = glGetValidLong(gsGridReadCell(grdWhseZones, lRow, _
                            kColWhseZoneKey))
                If sZoneID <> "" Then
                    If bZoneExistWithBin(lkuMain.KeyValue, lWhseZoneKey) Then
                        tabWarehouse.Tab = kTabZone
                        moDmZoneGrid.HighlightRow lRow
                        ' Msgbox "This zone is used as the zone for (NN) bins. Delete this zone?(kImsgZoneInUse)
                        If giSotaMsgBox(Me, moClass.moSysSession, kImsgZoneInUse, mlNNBins) = vbYes Then
                            mbDeleteZoneReference = True
                            moDmZoneGrid.SetRowDirty lRow
                            moDmZoneGrid.DeleteBlock lRow
                            bDelete = True
                            tabWarehouse.Tab = kTabMain
                        Else
                            mbDeleteZoneReference = False
                            bDelete = False
                            tabWarehouse.Tab = kTabMain
                        End If
                        iCount = iCount + mlNNBins
                    Else
                        bZoneInWhse = True
                    End If
                End If
            Next
            If bZoneInWhse Then
                If giSotaMsgBox(Me, moClass.moSysSession, kImsgZoneInWhse) = vbYes Then
                    bDelete = True
                Else
                    bDelete = False
                End If
                    iCount = 1
            End If
            If iCount <> 0 And bDelete = False Then
                chkUseBins.Value = 1
                tabWarehouse.Tab = kTabMain
                bZonesCheck = False
                Exit Function
           
            Else
                '-- Delete  Zones
                mbDeleteZones = True
                tabWarehouse.TabEnabled(kTabZone) = False
                chkUseZones.Value = 0
                moDmZoneGrid.Clear
                tabWarehouse.Tab = kTabMain
            End If
        End If
   
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bZonesCheck", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


 Private Sub grdWhseZones_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMZoneGrid.Grid_ColWidthChange Col1
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdWhseZones_ColWidthChange", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bValidateDuplicateZone() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRowZone As Long
Dim lRowNext As Long
Dim sZoneID As String
Dim sZoneNext As String
bValidateDuplicateZone = True
    'Validate Duplicate Zone entry
    If grdWhseZones.MaxRows > 1 Then
        For lRowZone = 1 To grdWhseZones.MaxRows - 1
            sZoneID = gsGridReadCell(grdWhseZones, lRowZone, kColWhseZoneID)
            For lRowNext = 2 To grdWhseZones.MaxRows - 1
                If lRowZone <> lRowNext Then
                    sZoneNext = gsGridReadCellText(grdWhseZones, lRowNext, kColWhseZoneID)
                    If sZoneNext = sZoneID Then
                         Beep
                        giSotaMsgBox Me, moClass.moSysSession, kIMmsgDuplicateZone, sZoneID
                        tabWarehouse.Tab = kTabZone
                        gGridSetActiveCell grdWhseZones, lRowNext, kColWhseZoneID
                        bValidateDuplicateZone = False
                        Exit Function
                    End If
                End If
            Next lRowNext
        Next lRowZone
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidateDuplicateZone", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub WarnIfPendingTrnsfrLinesExist()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sSQL As String
    Dim rs As Object
    Dim lWhseKey As Long
    
    lWhseKey = glGetValidLong(lkuMain.KeyValue)
    If lWhseKey <> 0 Then
               sSQL = "SELECT t.TranID, tol.TrnsfrLineNo, tol.ItemKey, tol.TrnsfrOrderLineKey"
        sSQL = sSQL & " FROM (SELECT sl.TrnsfrOrderLineKey, SUM(sitd.DistQty) 'TotShipDistQty'"
        sSQL = sSQL & "      FROM timTrnsfrOrderLine tol1"
        sSQL = sSQL & "      JOIN tsoShipLine sl WITH (NOLOCK) ON tol1.TrnsfrOrderLineKey = sl.TrnsfrOrderLineKey"
        sSQL = sSQL & "      JOIN timInvtTranDist sitd WITH (NOLOCK) ON sl.InvtTranKey = sitd.InvtTranKey"
        sSQL = sSQL & "      JOIN tsoShipment s WITH (NOLOCK) ON sl.ShipKey = s.ShipKey"
        sSQL = sSQL & "      GROUP BY sl.TrnsfrOrderLineKey) PostShip"
        sSQL = sSQL & " LEFT OUTER JOIN (SELECT rl.TrnsfrOrderLineKey, SUM(ritd.DistQty) 'TotRcptDistQty'"
        sSQL = sSQL & "      FROM timTrnsfrOrderLine tol2"
        sSQL = sSQL & "      JOIN tpoRcvrLine rl WITH (NOLOCK) ON tol2.TrnsfrOrderLineKey = rl.TrnsfrOrderLineKey"
        sSQL = sSQL & "      JOIN timInvtTranDist ritd WITH (NOLOCK) ON rl.InvtTranKey = ritd.InvtTranKey"
        sSQL = sSQL & "      JOIN tpoReceiver r WITH (NOLOCK) ON rl.RcvrKey = r.RcvrKey"
        sSQL = sSQL & "      GROUP BY rl.TrnsfrOrderLineKey) PostRcpt"
        sSQL = sSQL & " ON PostShip.TrnsfrOrderLineKey = PostRcpt.TrnsfrOrderLineKey"
        sSQL = sSQL & " LEFT OUTER JOIN (SELECT it.TrnsfrOrderLineKey, SUM(aitd.DistQty) 'TotAdjDistQty'"
        sSQL = sSQL & "      FROM timTrnsfrOrderLine tol3"
        sSQL = sSQL & "      JOIN timInvtTran it WITH (NOLOCK) ON tol3.TrnsfrOrderLineKey = it.TrnsfrOrderLineKey"
        sSQL = sSQL & "      JOIN timInvtTranDist aitd WITH (NOLOCK) ON it.InvtTranKey = aitd.InvtTranKey"
        sSQL = sSQL & "      WHERE it.TrnsfrOrderLineKey IS NOT NULL AND it.TranType IN (705,706,710)"
        sSQL = sSQL & "      GROUP BY it.TrnsfrOrderLineKey) PostAdj"
        sSQL = sSQL & " ON PostShip.TrnsfrOrderLineKey = PostAdj.TrnsfrOrderLineKey"
        sSQL = sSQL & " JOIN timTrnsfrOrderLine tol "
        sSQL = sSQL & "    ON PostShip.TrnsfrOrderLineKey = tol.TrnsfrOrderLineKey "
        sSQL = sSQL & " JOIN timTrnsfrOrder t WITH (NOLOCK) ON tol.TrnsfrOrderKey = t.TrnsfrOrderKey"
        sSQL = sSQL & " WHERE ABS(PostShip.TotShipDistQty) > (ABS(COALESCE(PostRcpt.TotRcptDistQty, 0)) + ABS(COALESCE(PostAdj.TotAdjDistQty, 0)))"
        sSQL = sSQL & " AND t.TransitWhseKey = " & lWhseKey
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEOF Then
            'Raise warning to user stating Inventory currently exists in this transit warehouse.
            'Changing the Inventory GL Account Number now will require a journal entry be made
            'to clear the transit inventory account."
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMWarnTrnxInTrnsit
            rs.Close
        End If
    End If
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WarnIfPendingTrnsfrLinesExist", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub InitializeSettingsCombo(oControl As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sRptProgram As String
    Dim sSQL As String
    Dim rs As Object
    
   'set the reprort program name
    Select Case oControl.Name
        Case "ddnImPickPrintSetting"
            sRptProgram = kmsPrintProgramName
            
        Case "ddnCallPickPrintSetting"
            sRptProgram = kmsPrintProgramName

        Case "ddnImInvPrintSetting"
            sRptProgram = kmsInvoiceProgramName
        
    End Select
    
    'retrieve settings from the database for each combo box
    sSQL = "SELECT RptSettingKey,RptSettingID FROM tsmReportSetting WHERE CompanyID = " & _
        gsQuoted(msCompanyID) & " AND RptProgram = " & gsQuoted(sRptProgram)
        
    'load "(None)" into the list
    LoadNoneSetting oControl
    
    'open recordset from tsmReportSetting
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    'add report setting to combo  list
    Do While Not rs.IsEOF
        oControl.AddItem rs.Field("RptSettingID")
        oControl.ItemData(oControl.NewIndex) = rs.Field("RptSettingKey")
        rs.MoveNext
    Loop
    
    rs.Close
    Set rs = Nothing
    'set control to "(None)"
    oControl.ListIndex = 0
    oControl.Tag = "0"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "InitializeSettingsCombo", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Private Sub LoadPrintSettingsCtl(oControl As Object, SettingsKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
            
            'set the tag and the list index for the combo box
            If SettingsKey = 0 Then
                oControl.ListIndex = 0
                oControl.Tag = "0"
            Else
                oControl.ListIndex = oControl.GetIndexByItemData(SettingsKey)
                oControl.Tag = oControl.ListIndex
            End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadPrintSettingsCtl", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

End Sub

Private Sub WarnTransitInvAcct()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lWhseKey As Long

    If chkIntransitWhs.Value = 1 Then
        lWhseKey = glGetValidLong(lkuMain.KeyValue)
        If moClass.moAppDB.Lookup("COUNT(*)", "timCostTier WITH (NOLOCK)", "(OrigQty-QtyUsed) > 0 AND WhseKey = " & lWhseKey) > 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgIMWarnTrnxInTrnsit
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WarnTransitInvAcct", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub

Public Function DMReposition(oDm As clsDmForm)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

If (oDm Is moDmMailAddr Or oDm Is moDmShipAddr) And oDm.State = kDmStateEdit Then
    mbDMStateChanged = True
End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function
Public Function DMValidate(oDm As clsDmForm) As Boolean
    '+++ VB/Rig Begin Push +++
    #If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
    #End If
    '+++ VB/Rig End +++
    '********************************************************************
    '   Description:
    '       DMValidate is called from the DataManager before it attempts
    '       to save the record.  Here, entries will be checked for validity.
    '       If DMValidate returns true, the Data Manager will save the
    '       record.  Otherwise, it will stop processing, and the form
    '       will remain in the same state as if the use had not pressed
    '       the finish button.
    '
    '   Parameters:
    '       oDM <in>    - is the DataManager Object that caused the
    '                     DMValidate to be called.
    '   Return Values:
    '       True    - the entry is valid
    '       False   - the Entry is invalid because anyone of the components
    '                 tested for validity is invalid.
    '********************************************************************
    
        DMValidate = False
         
        If oDm Is moDmForm Then
            DMValidate = True
                
       ElseIf oDm Is moDmMailAddr Then
            If Len(Trim(txtMLatitude)) = 0 And Len(Trim(txtMLongitude)) <> 0 Then
                If Not bRequiredIsValid(oDm, txtMLatitude, lblMLatitude & " in Mail to Address", 1) Then  'Latitude is conditionally required
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                    Exit Function
                End If
            ElseIf Len(Trim(txtMLongitude)) = 0 And Len(Trim(txtMLatitude)) <> 0 Then
                If Not bRequiredIsValid(oDm, txtMLongitude, lblMLongitude & " in Mail to Address", 1) Then  'Longitude is conditionally required
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                    Exit Function
                End If
            Else
                DMValidate = True
            End If
       ElseIf oDm Is moDmShipAddr Then
            If Len(Trim(txtSLatitude)) = 0 And Len(Trim(txtSLongitude)) <> 0 Then
                If Not bRequiredIsValid(oDm, txtSLatitude, lblSLatitude & " in Ship to Address", 1) Then 'Latitude is conditionally required
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                    Exit Function
                End If
            ElseIf Len(Trim(txtSLongitude)) = 0 And Len(Trim(txtSLatitude)) <> 0 Then
                If Not bRequiredIsValid(oDm, txtSLongitude, lblSLongitude & " in Ship to Address", 1) Then  'Longitude is conditionally required
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                    Exit Function
                End If
            Else
                DMValidate = True
            End If
       ElseIf (moClass.moAppDB.Lookup("Count(CustomFormKey)", "tsmCustomForm WITH (NOLOCK)", _
                                        "UserId =" & gsQuoted(moClass.moSysSession.UserId) & _
                                        " AND TaskId = " & ktskIMWarehouseMaint & _
                                        " AND CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND LanguageID = " & moClass.moSysSession.Language) > 0) Then
                  DMValidate = True
       End If
    
    '+++ VB/Rig Begin Pop +++
            Exit Function
    
VBRigErrorRoutine:
            gSetSotaErr Err, sMyName, "DMValidate", VBRIG_IS_FORM
            Select Case VBRIG_IS_NON_EVENT
            Case VBRIG_IS_NON_EVENT
                    Err.Raise guSotaErr.Number
            Case Else
                    Call giErrorHandler: Exit Function
            End Select
    '+++ VB/Rig End +++
End Function

Private Function bRequiredIsValid(ByVal oDm As clsDmForm, ByVal ctlReqdControl As Control, ByVal sAssosLabel As String, Optional iPage As Variant) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Desc: Called from DMValidate.
'         bRequiredIsValid will return True if the ctlReqdControl
'         is not empty.
'         If Control is empty diplay message with assigned label and
'         set focus back to control.
'
'  Parms: ctlReqdControl - Required control to be verified
'         sAssosLabel   - Label associated to control to be displayed
'         iPage         - tabPage where control is
'***********************************************************************
    bRequiredIsValid = True

    'If ctl is invisible due to Options then it is not required
    If ctlReqdControl.Visible Then
        If Trim(ctlReqdControl) = Empty Then
            If Not oDm Is Nothing Then oDm.CancelAction 'Release table before giving ErrMsg
        
            '100078, kmsgEntryRequired, You must specify a value for {0}
            Call giSotaMsgBox(Me, moClass.moSysSession, kmsgEntryRequired, CVar(sAssosLabel))
            bRequiredIsValid = False
            If Not IsMissing(iPage) Then tabWarehouse.Tab = iPage    'Set correct tabPage before Setting focus to control
            If ctlReqdControl.Enabled Then ctlReqdControl.SetFocus
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRequiredIsValid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtMLatitude_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMLatitude, True
    #End If
'+++ End Customizer Code Push +++
    
    If mbDMStateChanged Then
        txtMLatitude.Text = Format(txtMLatitude, "##0.0#####")
    End If
    gLatLonChange txtMLatitude, True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMLatitude_Change", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLatitude_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMLatitude, True
    #End If
'+++ End Customizer Code Push +++
    txtMLatitude.Tag = txtMLatitude.Text
    mbDMStateChanged = False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMLatitude_GotFocus", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLatitude_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMLatitude, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = giLatLonKeyPress(txtMLatitude, KeyAscii, True)
    If KeyAscii = 0 Then
        SendKeys "{End}", True
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMLatitude_KeyPress", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLatitude_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMLatitude, True
    #End If
'+++ End Customizer Code Push +++
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtMLatitude_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLongitude_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMLongitude, True
    #End If
'+++ End Customizer Code Push +++
    If mbDMStateChanged Then
        txtMLongitude.Text = Format(txtMLongitude.Text, "###0.0#####")
    End If
    gLatLonChange txtMLongitude, False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMLongitude_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLongitude_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMLongitude, True
    #End If
'+++ End Customizer Code Push +++
    txtMLongitude.Tag = txtMLongitude.Text
    mbDMStateChanged = False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMLongitude_GotFocus", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLongitude_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMLongitude, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = giLatLonKeyPress(txtMLongitude, KeyAscii, False)
    If KeyAscii = 0 Then
        SendKeys "{End}", True
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMLongitude_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMLongitude_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMLongitude, True
    #End If
'+++ End Customizer Code Push +++
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtMLongitude_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLatitude_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSLatitude, True
    #End If
'+++ End Customizer Code Push +++
    
    If mbDMStateChanged Then
        txtSLatitude.Text = Format(txtSLatitude, "##0.0#####")
    End If
    gLatLonChange txtSLatitude, True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSLatitude_Change", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLatitude_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSLatitude, True
    #End If
'+++ End Customizer Code Push +++
    txtSLatitude.Tag = txtSLatitude.Text
    mbDMStateChanged = False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSLatitude_GotFocus", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLatitude_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSLatitude, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = giLatLonKeyPress(txtSLatitude, KeyAscii, True)
    If KeyAscii = 0 Then
        SendKeys "{End}", True
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSLatitude_KeyPress", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLatitude_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSLatitude, True
    #End If
'+++ End Customizer Code Push +++
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSLatitude_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLongitude_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSLongitude, True
    #End If
'+++ End Customizer Code Push +++
    If mbDMStateChanged Then
        txtSLongitude.Text = Format(txtSLongitude.Text, "###0.0#####")
        mbDMStateChanged = False
    End If
    gLatLonChange txtSLongitude, False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSLongitude_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLongitude_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSLongitude, True
    #End If
'+++ End Customizer Code Push +++
    txtSLongitude.Tag = txtSLongitude.Text
    mbDMStateChanged = False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSLongitude_GotFocus", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLongitude_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSLongitude, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = giLatLonKeyPress(txtSLongitude, KeyAscii, False)
    If KeyAscii = 0 Then
        SendKeys "{End}", True
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSLongitude_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSLongitude_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSLongitude, True
    #End If
'+++ End Customizer Code Push +++
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSLongitude_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'***********************************************************
'RKL DEJ 2017-03-29 (START)
'***********************************************************
Function SaveToExtTable(WhseKey As Long, SignatureKey As Long, Optional BoilingPoint As Long) As Boolean
    On Error GoTo Error
    Dim lcErrMsg As String
    Dim RtnErrMsg As String
    Dim RtnVal As Long
    
    'Default to Failure
    SaveToExtTable = False
    
'
    With moClass.moAppDB
        .SetInParam WhseKey
        .SetInParam SignatureKey
        .SetOutParam RtnErrMsg
        .SetOutParam RtnVal
        .SetInParam BoilingPoint
        
        .ExecuteSP "spimSaveWarehosueExt_RKL"
        
        RtnErrMsg = gsGetValidStr(.GetOutParam(3))
        RtnVal = glGetValidLong(.GetOutParam(4))
        
        .ReleaseParams
    End With
    
    'Check results
    If RtnVal = 1 Then
        'Success
        SaveToExtTable = True
    Else
        SaveToExtTable = False
        lcErrMsg = "Could not save the COA Report Signature." & vbCrLf
        lcErrMsg = lcErrMsg & "The following error message was returned:" & vbCrLf
        lcErrMsg = lcErrMsg & RtnErrMsg
        
        MsgBox lcErrMsg, vbExclamation, "SAGE 500"
    End If

Exit Function
Error:
    lcErrMsg = Me.Name & ".SaveToExtTable()" & vbCrLf
    lcErrMsg = lcErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lcErrMsg, vbExclamation, "SAGE 500"
    
    Err.Clear
End Function

Function DeleteExtTable(WhseKey As Long) As Boolean
    On Error GoTo Error
    Dim lcErrMsg As String
    Dim RtnErrMsg As String
    Dim RtnVal As Long
    
    'Default to Failure
    DeleteExtTable = False

    With moClass.moAppDB
        .SetInParam WhseKey
        .SetOutParam RtnErrMsg
        .SetOutParam RtnVal
        
        .ExecuteSP "spimDeleteWarehosueExt_RKL"
        
        RtnErrMsg = gsGetValidStr(.GetOutParam(2))
        RtnVal = glGetValidLong(.GetOutParam(3))
        
        .ReleaseParams
    End With
    
    'Check results
    If RtnVal = 1 Then
        'Success
        DeleteExtTable = True
    Else
        DeleteExtTable = False
        lcErrMsg = "Could not delete the Warehouse Extension table record." & vbCrLf
        lcErrMsg = lcErrMsg & "The following error message was returned:" & vbCrLf
        lcErrMsg = lcErrMsg & RtnErrMsg
        
        MsgBox lcErrMsg, vbExclamation, "SAGE 500"
    End If

Exit Function
Error:
    lcErrMsg = Me.Name & ".DeleteExtTable()" & vbCrLf
    lcErrMsg = lcErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lcErrMsg, vbExclamation, "SAGE 500"
    
    Err.Clear
    
End Function

Function GetExtTable(WhseKey As Long) As Boolean
    On Error GoTo Error
    Dim SignatureKey As Long
    Dim SignatureID As String
    Dim BoilingPoint As Long
    Dim lcErrMsg As String
    
    'Default to Failure
    GetExtTable = False

    lkuCOASig.ClearData
    nbrBoilingPoint.ClearData
    
    SignatureKey = glGetValidLong(moClass.moAppDB.Lookup("SignatureKey", "timWarehouseExt_RKL", "WhseKey = " & WhseKey))
    SignatureID = gsGetValidStr(moClass.moAppDB.Lookup("SignatureKey", "timWhseSignatures_RKL", "SignatureKey = " & SignatureKey))
    BoilingPoint = glGetValidLong(moClass.moAppDB.Lookup("BoilingPoint", "timWarehouseExt_RKL", "WhseKey = " & WhseKey))
    
    If Trim(SignatureID) <> Empty Then
        lkuCOASig.SetTextAndKeyValue SignatureID, SignatureKey
    End If
    
    nbrBoilingPoint.Value = BoilingPoint
    
    GetExtTable = True
Exit Function
Error:
    lcErrMsg = Me.Name & ".GetExtTable()" & vbCrLf
    lcErrMsg = lcErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lcErrMsg, vbExclamation, "SAGE 500"
    
    Err.Clear
    
End Function


'***********************************************************
'RKL DEJ 2017-03-29 (STOP)
'***********************************************************

