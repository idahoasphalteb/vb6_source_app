Attribute VB_Name = "basInventoryMNT"
'**********************************************************************
'     Name: basimzmt001
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 03-04-1998
'     Mods: mm/dd/yy XXX
'**********************************************************************
    Option Explicit

    'timItem | Status
    Public Const kvActive As Integer = 1                'Active                         Str=50028
    Public Const kvInactive As Integer = 2              'Inactive                       Str=50029
    Public Const kvDiscontinued As Integer = 3          'Discontinued                   Str=50387
    Public Const kvDeleted As Integer = 4               'Deleted                        Str=50033
    Public gbItemKeyPassed  As Boolean
    Public gbWhseKeyPassed  As Boolean

Const VBRIG_MODULE_ID_STRING = "IMZMT001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("imzmt001.clsInventoryMNT")
        StartAppInStandaloneMode oApp, Command$()
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
 '+++ VB/Rig Skip +++
    sMyName = "basInventoryMNT"
End Function
