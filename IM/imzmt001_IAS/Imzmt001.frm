VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Begin VB.Form frmInventoryMNT 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6960
   ClientLeft      =   1650
   ClientTop       =   1605
   ClientWidth     =   9765
   HelpContextID   =   60057
   Icon            =   "Imzmt001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6960
   ScaleWidth      =   9765
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   128
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   1000
      Left            =   -30000
      TabIndex        =   127
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin LookupViewControl.LookupView navMain 
      Height          =   285
      Left            =   -3000
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Navigator"
      Top             =   6270
      WhatsThisHelpID =   14
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupID        =   "IMItemWhse"
   End
   Begin VB.Frame Frame9 
      Height          =   480
      Left            =   0
      TabIndex        =   125
      Top             =   0
      Width           =   9375
      Begin SOTAToolbarControl.SOTAToolbar tbrMain 
         Height          =   495
         Left            =   0
         TabIndex        =   100
         TabStop         =   0   'False
         Top             =   -15
         WhatsThisHelpID =   63
         Width           =   9555
         _ExtentX        =   16854
         _ExtentY        =   873
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1000
      Left            =   100
      TabIndex        =   101
      Top             =   480
      Width           =   9525
      Begin VB.CommandButton cmdUpdRpcmntCst 
         Caption         =   "Upd Rplcmnt Cost"
         Height          =   255
         Left            =   7440
         TabIndex        =   204
         ToolTipText     =   "Update the Replacement Cost with the Standard Cost for every Item in the selected Warehouse."
         Top             =   200
         Width           =   1935
      End
      Begin EntryLookupControls.TextLookup lkuItem 
         Height          =   285
         Left            =   1080
         TabIndex        =   3
         Top             =   600
         WhatsThisHelpID =   60196
         Width           =   3400
         _ExtentX        =   6006
         _ExtentY        =   503
         ForeColor       =   -2147483640
         MaxLength       =   30
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Item"
         ParentIDColumn  =   "ItemID"
         ParentKeyColumn =   "ItemKey"
         ParentTable     =   "timItem"
         BoundColumn     =   "ItemKey"
         BoundTable      =   "timInventory"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "ItemID,lkuItem,;ShortDesc,lblItemDesc,;ItemKey,,;"
      End
      Begin EntryLookupControls.TextLookup lkuWhse 
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   240
         WhatsThisHelpID =   60195
         Width           =   1995
         _ExtentX        =   3519
         _ExtentY        =   503
         ForeColor       =   -2147483640
         MaxLength       =   6
         IsSurrogateKey  =   -1  'True
         LookupID        =   "Warehouse"
         ParentIDColumn  =   "WhseID"
         ParentKeyColumn =   "WhseKey"
         ParentTable     =   "timWarehouse"
         BoundColumn     =   "WhseKey"
         BoundTable      =   "timInventory"
         IsForeignKey    =   -1  'True
         sSQLReturnCols  =   "WhseID,lkuWhse,;Description,lblWhseDesc,;"
      End
      Begin SOTAVM.SOTAValidationMgr SOTAVM 
         Left            =   4560
         Top             =   120
         _ExtentX        =   741
         _ExtentY        =   661
         CtlsCount       =   0
      End
      Begin VB.Label lblItemDesc 
         Height          =   195
         Left            =   4665
         TabIndex        =   103
         Top             =   660
         UseMnemonic     =   0   'False
         Width           =   4305
      End
      Begin VB.Label lblWhseDesc 
         Height          =   195
         Left            =   3220
         TabIndex        =   104
         Top             =   300
         UseMnemonic     =   0   'False
         Width           =   5745
      End
      Begin VB.Label lblWhseKey 
         AutoSize        =   -1  'True
         Caption         =   "Warehouse"
         Height          =   195
         Left            =   120
         TabIndex        =   0
         Top             =   300
         Width           =   825
      End
      Begin VB.Label lblItemKey 
         AutoSize        =   -1  'True
         Caption         =   "Item"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   660
         Width           =   300
      End
   End
   Begin TabDlg.SSTab tabInventory 
      Height          =   4875
      Left            =   105
      TabIndex        =   4
      Top             =   1605
      WhatsThisHelpID =   60188
      Width           =   9570
      _ExtentX        =   16880
      _ExtentY        =   8599
      _Version        =   393216
      Tabs            =   6
      Tab             =   5
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "Imzmt001.frx":23D2
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "pnlTab(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Replenishment"
      TabPicture(1)   =   "Imzmt001.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "pnlTab(1)"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&GL Accounts"
      TabPicture(2)   =   "Imzmt001.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "pnlTab(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&Inventory Control"
      TabPicture(3)   =   "Imzmt001.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "pnlTab(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "CustomTab"
      TabPicture(4)   =   "Imzmt001.frx":2442
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "pnlTab(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "&Placard/Specs"
      TabPicture(5)   =   "Imzmt001.frx":245E
      Tab(5).ControlEnabled=   -1  'True
      Tab(5).Control(0)=   "pnlTab(5)"
      Tab(5).Control(0).Enabled=   0   'False
      Tab(5).ControlCount=   1
      Begin Threed.SSPanel pnlTab 
         Height          =   4385
         Index           =   4
         Left            =   -74880
         TabIndex        =   163
         Top             =   400
         Width           =   9360
         _Version        =   65536
         _ExtentX        =   16510
         _ExtentY        =   7735
         _StockProps     =   15
         BackColor       =   15790320
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   0
         BorderWidth     =   0
         BevelOuter      =   0
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   4275
         Index           =   3
         Left            =   -74880
         TabIndex        =   114
         Top             =   480
         Width           =   9360
         _Version        =   65536
         _ExtentX        =   16510
         _ExtentY        =   7541
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraPreferredBinDist 
            Caption         =   "Distribution Preferred Bins"
            Height          =   1845
            Left            =   4560
            TabIndex        =   159
            Top             =   0
            Width           =   4785
            Begin VB.TextBox txtPrefBinDist 
               Height          =   315
               Left            =   2895
               TabIndex        =   160
               TabStop         =   0   'False
               Top             =   405
               Visible         =   0   'False
               WhatsThisHelpID =   60186
               Width           =   1215
            End
            Begin FPSpreadADO.fpSpread grdPrefBinDist 
               Height          =   1440
               Left            =   75
               TabIndex        =   161
               Top             =   255
               WhatsThisHelpID =   60185
               Width           =   4620
               _Version        =   524288
               _ExtentX        =   8149
               _ExtentY        =   2540
               _StockProps     =   64
               ColHeaderDisplay=   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   -2147483633
               GridColor       =   -2147483633
               MaxCols         =   2
               MaxRows         =   1
               RestrictRows    =   -1  'True
               ScrollBarExtMode=   -1  'True
               SpreadDesigner  =   "Imzmt001.frx":247A
               AppearanceStyle =   0
            End
            Begin LookupViewControl.LookupView lkuPrefBinDist 
               Height          =   285
               Left            =   3180
               TabIndex        =   162
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   465
               Visible         =   0   'False
               WhatsThisHelpID =   60184
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
         End
         Begin VB.CheckBox chkUseDistBinsInMfg 
            Caption         =   "Use Distribution Preferred Bins as Manufacturing Preferred Bins"
            Height          =   255
            Left            =   4560
            TabIndex        =   158
            Top             =   2040
            Width           =   4815
         End
         Begin VB.Frame fraPreferredBinMfg 
            Caption         =   "Manufacturing  Preferred Bins"
            Height          =   1845
            Left            =   4560
            TabIndex        =   154
            Top             =   2400
            Width           =   4785
            Begin LookupViewControl.LookupView lkuPrefBinMfg 
               Height          =   285
               Left            =   3180
               TabIndex        =   157
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   465
               Visible         =   0   'False
               WhatsThisHelpID =   60184
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.TextBox txtPrefBinMfg 
               Height          =   315
               Left            =   2895
               TabIndex        =   155
               TabStop         =   0   'False
               Top             =   405
               Visible         =   0   'False
               WhatsThisHelpID =   60186
               Width           =   1215
            End
            Begin FPSpreadADO.fpSpread grdPrefBinMfg 
               Height          =   1440
               Left            =   75
               TabIndex        =   156
               Top             =   255
               WhatsThisHelpID =   60185
               Width           =   4620
               _Version        =   524288
               _ExtentX        =   8149
               _ExtentY        =   2540
               _StockProps     =   64
               ColHeaderDisplay=   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               GrayAreaBackColor=   -2147483633
               GridColor       =   -2147483633
               MaxCols         =   2
               MaxRows         =   1
               RestrictRows    =   -1  'True
               ScrollBarExtMode=   -1  'True
               SpreadDesigner  =   "Imzmt001.frx":28DB
               AppearanceStyle =   0
            End
         End
         Begin VB.Frame fraPickingShip 
            Caption         =   "&Picking and Shipping"
            Height          =   4215
            Left            =   45
            TabIndex        =   140
            Top             =   0
            Width           =   4425
            Begin VB.CheckBox chkPickPrefBinsFirst 
               Caption         =   "Pick From Preferred Bins First"
               Height          =   285
               Left            =   195
               TabIndex        =   144
               Top             =   860
               WhatsThisHelpID =   17777707
               Width           =   2655
            End
            Begin VB.CheckBox chkPickOneLotOnly 
               Caption         =   "Pick One Lot Only"
               Height          =   285
               Left            =   2715
               TabIndex        =   143
               Top             =   405
               WhatsThisHelpID =   17777708
               Width           =   1680
            End
            Begin VB.CheckBox chkSerialNoLinesOnPickList 
               Caption         =   "Print One Pick Line Per Serial Number"
               Height          =   210
               Left            =   195
               TabIndex        =   146
               Top             =   1720
               WhatsThisHelpID =   17777709
               Width           =   3135
            End
            Begin VB.CheckBox chkShowAvailLots 
               Caption         =   "Show Available Lots on Pick List"
               Height          =   270
               Left            =   195
               TabIndex        =   145
               Top             =   1290
               WhatsThisHelpID =   17777710
               Width           =   3015
            End
            Begin SOTADropDownControl.SOTADropDown ddnPickMethod 
               Height          =   315
               Left            =   1110
               TabIndex        =   142
               Top             =   390
               WhatsThisHelpID =   17777711
               Width           =   1500
               _ExtentX        =   2646
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnPickMethod"
            End
            Begin VB.CheckBox chkShipComplete 
               Caption         =   "Ship Complete"
               Height          =   255
               Left            =   195
               TabIndex        =   147
               Top             =   2150
               WhatsThisHelpID =   17771440
               Width           =   3030
            End
            Begin VB.CheckBox chkCloseSOLineFirst 
               Caption         =   "Close SO Line On First Shipment"
               Height          =   255
               Left            =   195
               TabIndex        =   149
               Top             =   2580
               WhatsThisHelpID =   17771439
               Width           =   3060
            End
            Begin VB.Label lblPickMethod 
               Caption         =   "Pick Method"
               Height          =   210
               Left            =   135
               TabIndex        =   141
               Top             =   450
               Width           =   1095
            End
         End
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   3795
         Index           =   2
         Left            =   -74880
         TabIndex        =   113
         Top             =   480
         Width           =   9135
         _Version        =   65536
         _ExtentX        =   16113
         _ExtentY        =   6694
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraGLAccount 
            Height          =   3750
            Left            =   0
            TabIndex        =   79
            Top             =   0
            Width           =   9090
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   0
               Left            =   1560
               TabIndex        =   81
               Top             =   240
               WhatsThisHelpID =   60181
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "PurchAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,glaGLAcct,0;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   1
               Left            =   1560
               TabIndex        =   83
               Top             =   570
               WhatsThisHelpID =   60180
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   2
               Left            =   1560
               TabIndex        =   85
               Top             =   900
               WhatsThisHelpID =   60179
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesOffsetAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   3
               Left            =   1560
               TabIndex        =   87
               Top             =   1230
               WhatsThisHelpID =   60178
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "SalesReturnAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   4
               Left            =   1560
               TabIndex        =   89
               Top             =   1560
               WhatsThisHelpID =   60177
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "InvtAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   5
               Left            =   1560
               TabIndex        =   91
               Top             =   1890
               WhatsThisHelpID =   60176
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "COSAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   6
               Left            =   1560
               TabIndex        =   93
               Top             =   2220
               WhatsThisHelpID =   60175
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "IssueAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   7
               Left            =   1560
               TabIndex        =   95
               Top             =   2550
               WhatsThisHelpID =   60174
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "MiscAdjAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   8
               Left            =   1560
               TabIndex        =   97
               Top             =   2880
               WhatsThisHelpID =   60173
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "CostTierAdjAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin EntryLookupControls.GLAcctLookup glaGLAcct 
               Height          =   285
               Index           =   9
               Left            =   1560
               TabIndex        =   99
               Top             =   3210
               WhatsThisHelpID =   60172
               Width           =   7260
               _ExtentX        =   12806
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "PhysCountAcctKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               WidthDesc       =   3900
               WidthAcct       =   2700
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,SOTAGLAccount1,;"
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "&Purchases"
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   80
               Top             =   300
               Width           =   1000
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "&Sales "
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   82
               Top             =   630
               Width           =   1000
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "Sales O&ffset"
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   84
               Top             =   960
               Width           =   1000
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "R&eturns"
               Height          =   195
               Index           =   3
               Left            =   120
               TabIndex        =   86
               Top             =   1290
               Width           =   1005
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "&Inventory"
               Height          =   195
               Index           =   4
               Left            =   120
               TabIndex        =   88
               Top             =   1620
               Width           =   1005
            End
            Begin VB.Label lblGLAcct 
               AutoSize        =   -1  'True
               Caption         =   "&Cost of Goods Sold"
               Height          =   195
               Index           =   5
               Left            =   120
               TabIndex        =   90
               Top             =   1950
               Width           =   1365
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "Iss&ues"
               Height          =   195
               Index           =   6
               Left            =   120
               TabIndex        =   92
               Top             =   2280
               Width           =   1000
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "Misc &Adj"
               Height          =   195
               Index           =   7
               Left            =   120
               TabIndex        =   94
               Top             =   2610
               Width           =   1000
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "C&ost Tier Adj"
               Height          =   195
               Index           =   8
               Left            =   120
               TabIndex        =   96
               Top             =   2940
               Width           =   1000
            End
            Begin VB.Label lblGLAcct 
               Caption         =   "P&hysical Count"
               Height          =   195
               Index           =   9
               Left            =   120
               TabIndex        =   98
               Top             =   3270
               Width           =   1095
            End
         End
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   4485
         Index           =   1
         Left            =   -74880
         TabIndex        =   107
         Top             =   330
         Width           =   9390
         _Version        =   65536
         _ExtentX        =   16563
         _ExtentY        =   7911
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame Frame7 
            Height          =   2295
            Left            =   0
            TabIndex        =   50
            Top             =   2040
            Width           =   5055
            Begin VB.VScrollBar vsLeadTime 
               Height          =   285
               Left            =   3045
               Max             =   -1
               Min             =   1
               TabIndex        =   63
               TabStop         =   0   'False
               Top             =   1890
               WhatsThisHelpID =   60159
               Width           =   255
            End
            Begin EntryLookupControls.TextLookup lkuDemand 
               Height          =   285
               Left            =   1860
               TabIndex        =   52
               Top             =   240
               WhatsThisHelpID =   60158
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMDEMAND"
               ParentIDColumn  =   "DemandCalcID"
               ParentKeyColumn =   "DemandCalcKey"
               ParentTable     =   "timDemandCalc"
               BoundColumn     =   "DemandCalcKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "DemandCalcID,lkuDemand,;"
            End
            Begin NEWSOTALib.SOTANumber nbrSafetyStockQty 
               Height          =   285
               Left            =   1860
               TabIndex        =   58
               Top             =   1230
               WhatsThisHelpID =   60157
               Width           =   1440
               _Version        =   65536
               _ExtentX        =   2540
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
               text            =   "       0.0000000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   7
            End
            Begin EntryLookupControls.TextLookup lkuLeadTime 
               Height          =   285
               Left            =   1860
               TabIndex        =   60
               Top             =   1560
               WhatsThisHelpID =   60156
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMLeadTmNav"
               ParentIDColumn  =   "LeadTimeCalcID"
               ParentKeyColumn =   "LeadTimeCalcKey"
               ParentTable     =   "timLeadTimeCalc"
               BoundColumn     =   "LeadTimeCalcKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "LeadTimeCalcID,lkuLeadTime,;"
            End
            Begin EntryLookupControls.TextLookup lkuSafetyStock 
               Height          =   285
               Left            =   1860
               TabIndex        =   56
               Top             =   900
               WhatsThisHelpID =   60155
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMSStockCalc"
               ParentIDColumn  =   "SafetyStockCalcID"
               ParentKeyColumn =   "SafetyStockCalcKey"
               ParentTable     =   "timSafetyStockCalc"
               BoundColumn     =   "SafetyStockCalcKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "SafetyStockCalcID,lkuSafetyStock,;"
            End
            Begin NEWSOTALib.SOTANumber nbrDemand 
               Height          =   285
               Left            =   1860
               TabIndex        =   54
               Top             =   570
               WhatsThisHelpID =   60154
               Width           =   1440
               _Version        =   65536
               _ExtentX        =   2540
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
               text            =   "       0.0000000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   7
            End
            Begin NEWSOTALib.SOTANumber txtLeadTime 
               Height          =   285
               Left            =   1860
               TabIndex        =   62
               Top             =   1890
               WhatsThisHelpID =   60153
               Width           =   1200
               _Version        =   65536
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#"
               text            =   "  0"
               dMaxValue       =   999
               sIntegralPlaces =   3
               sDecimalPlaces  =   0
            End
            Begin VB.Label lblDemandCalcKey 
               AutoSize        =   -1  'True
               Caption         =   "&Demand Formula"
               Height          =   195
               Left            =   120
               TabIndex        =   51
               Top             =   300
               Width           =   1200
            End
            Begin VB.Label lblLeadTime 
               AutoSize        =   -1  'True
               Caption         =   "Projected Lead &Time"
               Height          =   195
               Left            =   120
               TabIndex        =   61
               Top             =   1950
               Width           =   1470
            End
            Begin VB.Label lblLeadTimeCalcKey 
               AutoSize        =   -1  'True
               Caption         =   "&Lead Time Formula"
               Height          =   195
               Left            =   120
               TabIndex        =   59
               Top             =   1620
               Width           =   1350
            End
            Begin VB.Label lblSafetyStockCalcKey 
               AutoSize        =   -1  'True
               Caption         =   "Safety Stoc&k Formula"
               Height          =   195
               Left            =   120
               TabIndex        =   55
               Top             =   960
               Width           =   1515
            End
            Begin VB.Label lblSafetyStockQty 
               AutoSize        =   -1  'True
               Caption         =   "Projected Sa&fety Stock"
               Height          =   195
               Left            =   120
               TabIndex        =   57
               Top             =   1290
               Width           =   1635
            End
            Begin VB.Label lblLeadUOM 
               Caption         =   "Days"
               Height          =   195
               Left            =   3435
               TabIndex        =   102
               Top             =   1950
               Width           =   660
            End
            Begin VB.Label lblSafetyUOM 
               Height          =   195
               Left            =   3435
               TabIndex        =   112
               Top             =   1290
               Width           =   780
            End
            Begin VB.Label lblDemandUOM 
               Height          =   195
               Left            =   3435
               TabIndex        =   111
               Top             =   630
               Width           =   900
            End
            Begin VB.Label lblDemand 
               AutoSize        =   -1  'True
               Caption         =   "&Projected Demand"
               Height          =   195
               Left            =   120
               TabIndex        =   53
               Top             =   630
               Width           =   1320
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "&Source"
            Height          =   2010
            Left            =   0
            TabIndex        =   43
            Top             =   45
            Width           =   5040
            Begin VB.CheckBox chkUseWPPLPrimaryVendor 
               Caption         =   "Use Warehouse Replenishment Primary Vendor"
               Height          =   285
               Left            =   75
               TabIndex        =   153
               Top             =   1200
               WhatsThisHelpID =   17777707
               Width           =   3690
            End
            Begin EntryLookupControls.TextLookup lkuPrimaryVendor 
               Height          =   285
               Left            =   1275
               TabIndex        =   148
               Top             =   890
               WhatsThisHelpID =   17777712
               Width           =   1350
               _ExtentX        =   2381
               _ExtentY        =   503
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Vendor"
               ParentIDColumn  =   "VendID"
               ParentKeyColumn =   "VendKey"
               ParentTable     =   "tapVendor"
               BoundColumn     =   "PrimaryVendKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "VendID,lkuPrimaryVendor,;VendName,lblPrimaryVendorName,;"
            End
            Begin EntryLookupControls.TextLookup lkuWhseFrom 
               Height          =   285
               Left            =   1275
               TabIndex        =   46
               Top             =   540
               WhatsThisHelpID =   60142
               Width           =   1350
               _ExtentX        =   2381
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   6
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Warehouse"
               ParentIDColumn  =   "WhseID"
               ParentKeyColumn =   "WhseKey"
               ParentTable     =   "timWarehouse"
               BoundColumn     =   "RplnsmntWhseKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "WhseID,lkuWhseFrom,;Description,,;"
            End
            Begin EntryLookupControls.TextLookup lkuPPLine 
               Height          =   285
               Left            =   1275
               TabIndex        =   45
               Top             =   210
               WhatsThisHelpID =   60141
               Width           =   1845
               _ExtentX        =   3254
               _ExtentY        =   503
               ForeColor       =   -2147483640
               MaxLength       =   15
               IsSurrogateKey  =   -1  'True
               LookupID        =   "PurchProdLine"
               ParentIDColumn  =   "PurchProdLineID"
               ParentKeyColumn =   "PurchProdLineKey"
               ParentTable     =   "timPurchProdLine"
               BoundColumn     =   "PurchProdLineKey"
               BoundTable      =   "timInventory"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "PurchProdLineID,lkuPPLine,;Description,,;"
            End
            Begin SOTADropDownControl.SOTADropDown ddnBuyer 
               Height          =   315
               Left            =   1275
               TabIndex        =   49
               Top             =   1510
               WhatsThisHelpID =   60140
               Width           =   1845
               _ExtentX        =   3254
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnBuyer"
               SQLStatement    =   "Select timBuyer.BuyerID, timBuyer.BuyerKey From timBuyer"
               AllowClear      =   0   'False
            End
            Begin VB.Label lblPrimaryVendor 
               Caption         =   "Primary Vendor"
               Height          =   195
               Left            =   75
               TabIndex        =   152
               Top             =   920
               Width           =   1140
            End
            Begin VB.Label lblWhseDescFrom 
               Height          =   225
               Left            =   2760
               TabIndex        =   151
               Top             =   585
               Width           =   2115
            End
            Begin VB.Label lblPrimaryVendorName 
               Height          =   270
               Left            =   2700
               TabIndex        =   150
               Top             =   915
               Width           =   2265
            End
            Begin VB.Label lblPurchProdLineKey 
               AutoSize        =   -1  'True
               Caption         =   "Purch Prod Line"
               Height          =   195
               Left            =   75
               TabIndex        =   44
               Top             =   240
               Width           =   1140
            End
            Begin VB.Label lblRplnsmntWhseKey 
               AutoSize        =   -1  'True
               Caption         =   "Warehouse"
               Height          =   195
               Left            =   75
               TabIndex        =   47
               Top             =   570
               Width           =   1140
            End
            Begin VB.Label lblBuyerKey 
               AutoSize        =   -1  'True
               Caption         =   "Buyer"
               Height          =   195
               Left            =   75
               TabIndex        =   48
               Top             =   1570
               Width           =   1140
            End
         End
         Begin VB.Frame Frame8 
            Height          =   4290
            Left            =   5100
            TabIndex        =   64
            Top             =   45
            Width           =   4260
            Begin NEWSOTALib.SOTANumber nbrMaxStockQty 
               Height          =   285
               Left            =   1860
               TabIndex        =   72
               Top             =   1260
               WhatsThisHelpID =   60135
               Width           =   1680
               _Version        =   65536
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
               text            =   "       0.0000000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   7
            End
            Begin NEWSOTALib.SOTANumber nbrStdOrdQty 
               Height          =   285
               Left            =   1860
               TabIndex        =   74
               Top             =   2200
               WhatsThisHelpID =   60134
               Width           =   1680
               _Version        =   65536
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
               text            =   "       0.0000000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   7
            End
            Begin NEWSOTALib.SOTANumber nbrMinStockQty 
               Height          =   285
               Left            =   1860
               TabIndex        =   70
               Top             =   930
               WhatsThisHelpID =   60133
               Width           =   1680
               _Version        =   65536
               lFormat         =   2
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#######"
               text            =   "       0.0000000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   7
            End
            Begin SOTADropDownControl.SOTADropDown ddnReordMeth 
               Height          =   315
               Left            =   1860
               TabIndex        =   66
               Top             =   240
               WhatsThisHelpID =   60132
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnReordMeth"
               StaticListTableName=   "timInventory      "
               StaticListColumnName=   "ReordMeth         "
               AllowClear      =   0   'False
               Sorted          =   0   'False
            End
            Begin SOTACalendarControl.SOTACalendar calMinMax 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   1860
               TabIndex        =   68
               Top             =   600
               WhatsThisHelpID =   60131
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin NEWSOTALib.SOTAMaskedEdit nbrCostCarryPct 
               Height          =   285
               Left            =   3570
               TabIndex        =   129
               Top             =   2895
               Visible         =   0   'False
               WhatsThisHelpID =   60130
               Width           =   615
               _Version        =   65536
               _ExtentX        =   1085
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTAMaskedEdit nbrCostOfCarry 
               Height          =   285
               Left            =   1860
               TabIndex        =   78
               Top             =   2890
               WhatsThisHelpID =   60129
               Width           =   1680
               _Version        =   65536
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTAMaskedEdit crnCostToReplenish 
               Height          =   285
               Left            =   1860
               TabIndex        =   76
               Top             =   2530
               WhatsThisHelpID =   60128
               Width           =   1680
               _Version        =   65536
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTANumber nbrOrdPoint 
               Height          =   285
               Left            =   1860
               TabIndex        =   133
               Top             =   1560
               WhatsThisHelpID =   60127
               Width           =   1680
               _Version        =   65536
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "       0.000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrLinePoint 
               Height          =   285
               Left            =   1860
               TabIndex        =   134
               Top             =   1895
               WhatsThisHelpID =   60126
               Width           =   1680
               _Version        =   65536
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "       0.000"
               dMaxValue       =   99999999.9999999
               sIntegralPlaces =   8
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtOrdCycle 
               Height          =   285
               Left            =   1860
               TabIndex        =   138
               Top             =   3240
               WhatsThisHelpID =   60125
               Width           =   1680
               _Version        =   65536
               lAlignment      =   1
               _ExtentX        =   2963
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
            Begin VB.Label lblOrdCycleDays 
               Caption         =   "Days"
               Height          =   195
               Left            =   3615
               TabIndex        =   137
               Top             =   3285
               Width           =   480
            End
            Begin VB.Label lblLinePointUOM 
               Height          =   195
               Left            =   3600
               TabIndex        =   136
               Top             =   1920
               Width           =   570
            End
            Begin VB.Label lblOrdPointUOM 
               Height          =   195
               Left            =   3570
               TabIndex        =   135
               Top             =   1620
               Width           =   585
            End
            Begin VB.Label lblOrderCycle 
               AutoSize        =   -1  'True
               Caption         =   "Order Cycle"
               Height          =   195
               Left            =   120
               TabIndex        =   132
               Top             =   3270
               Width           =   1590
            End
            Begin VB.Label lblLinePoint 
               AutoSize        =   -1  'True
               Caption         =   "Line Point"
               Height          =   195
               Left            =   120
               TabIndex        =   131
               Top             =   1940
               Width           =   1590
            End
            Begin VB.Label lblOrderPoint 
               AutoSize        =   -1  'True
               Caption         =   "Order Point"
               Height          =   195
               Left            =   120
               TabIndex        =   130
               Top             =   1605
               Width           =   1590
            End
            Begin VB.Label lblRplnsmntParmFreeze 
               AutoSize        =   -1  'True
               Caption         =   "&Use Min/Max Until"
               Height          =   195
               Left            =   120
               TabIndex        =   67
               Top             =   660
               Width           =   1590
            End
            Begin VB.Label lblReordMeth 
               AutoSize        =   -1  'True
               Caption         =   "Re&order Method"
               Height          =   195
               Left            =   120
               TabIndex        =   65
               Top             =   300
               Width           =   1590
            End
            Begin VB.Label lblStdOrdQty 
               AutoSize        =   -1  'True
               Caption         =   "St&andard Order"
               Height          =   195
               Left            =   120
               TabIndex        =   73
               Top             =   2260
               Width           =   1590
            End
            Begin VB.Label lblMaxStockQty 
               AutoSize        =   -1  'True
               Caption         =   "Ma&ximum Stock"
               Height          =   195
               Left            =   120
               TabIndex        =   71
               Top             =   1320
               Width           =   1590
            End
            Begin VB.Label lblCostToReplenish 
               AutoSize        =   -1  'True
               Caption         =   "Cost of R&eplenishment"
               Height          =   195
               Left            =   120
               TabIndex        =   75
               Top             =   2590
               Width           =   1590
            End
            Begin VB.Label lblCostOfCarry 
               AutoSize        =   -1  'True
               Caption         =   "&Carrying Cost %"
               Height          =   195
               Left            =   120
               TabIndex        =   77
               Top             =   2920
               Width           =   1590
            End
            Begin VB.Label lblMinStockQty 
               AutoSize        =   -1  'True
               Caption         =   "Mi&nimum Stock"
               Height          =   195
               Left            =   120
               TabIndex        =   69
               Top             =   990
               Width           =   1590
            End
            Begin VB.Label lblMinStockUOM 
               Height          =   195
               Left            =   3570
               TabIndex        =   108
               Top             =   975
               Width           =   525
            End
            Begin VB.Label lblMaxStockUOM 
               Height          =   195
               Left            =   3585
               TabIndex        =   109
               Top             =   1290
               Width           =   600
            End
            Begin VB.Label lblStdOrdUOM 
               Height          =   195
               Left            =   3570
               TabIndex        =   110
               Top             =   2235
               Width           =   540
            End
         End
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   4275
         Index           =   0
         Left            =   -74880
         TabIndex        =   105
         Top             =   480
         Width           =   9360
         _Version        =   65536
         _ExtentX        =   16510
         _ExtentY        =   7541
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame Frame4 
            Height          =   1785
            Left            =   120
            TabIndex        =   6
            Top             =   0
            Width           =   4485
            Begin SOTACalendarControl.SOTACalendar calEstDate 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   2220
               TabIndex        =   10
               TabStop         =   0   'False
               Top             =   645
               WhatsThisHelpID =   60106
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   556
               BackColor       =   -2147483633
               DisplayOnly     =   -1  'True
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Protected       =   -1  'True
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin VB.TextBox txtItemType 
               BackColor       =   &H8000000F&
               Enabled         =   0   'False
               Height          =   285
               Left            =   2200
               Locked          =   -1  'True
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   1050
               WhatsThisHelpID =   60104
               Width           =   2000
            End
            Begin SOTADropDownControl.SOTADropDown ddnStatus 
               Height          =   315
               Left            =   2220
               TabIndex        =   8
               Top             =   240
               WhatsThisHelpID =   60103
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnStatus"
               StaticListTableName=   "timInventory      "
               StaticListColumnName=   "Status            "
               AllowClear      =   0   'False
               Sorted          =   0   'False
            End
            Begin VB.Label lblStatus 
               AutoSize        =   -1  'True
               Caption         =   "&Status"
               Height          =   195
               Left            =   120
               TabIndex        =   7
               Top             =   300
               Width           =   1950
            End
            Begin VB.Label lblDateEstablished 
               AutoSize        =   -1  'True
               Caption         =   "Date Established"
               Height          =   195
               Left            =   135
               TabIndex        =   9
               Top             =   705
               Width           =   1950
            End
            Begin VB.Label lblItemType 
               AutoSize        =   -1  'True
               Caption         =   "Item Type"
               Height          =   195
               Left            =   120
               TabIndex        =   11
               Top             =   1110
               Width           =   1950
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "&Unit Cost"
            Height          =   2475
            Left            =   120
            TabIndex        =   13
            Top             =   1815
            Width           =   4485
            Begin VB.CommandButton cmdStdCostBreakout 
               Caption         =   "..."
               Enabled         =   0   'False
               Height          =   285
               Left            =   3905
               TabIndex        =   139
               Tag             =   "True"
               ToolTipText     =   "Standard Unit Cost Breakout"
               Top             =   240
               WhatsThisHelpID =   17771441
               Width           =   300
            End
            Begin NEWSOTALib.SOTACurrency crnStdUnitCost 
               Height          =   285
               Left            =   2205
               TabIndex        =   15
               Top             =   240
               WhatsThisHelpID =   60098
               Width           =   1650
               _Version        =   65536
               _ExtentX        =   2910
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "         0.00"
               dMaxValue       =   9999999999.99999
               sIntegralPlaces =   10
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency crnRplcmntCost 
               Height          =   285
               Left            =   2205
               TabIndex        =   17
               Top             =   570
               WhatsThisHelpID =   60097
               Width           =   2000
               _Version        =   65536
               _ExtentX        =   3528
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "         0.00"
               dMaxValue       =   9999999999.99999
               sIntegralPlaces =   10
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency crnAvgUnitCost 
               Height          =   285
               Left            =   2205
               TabIndex        =   19
               TabStop         =   0   'False
               Top             =   900
               WhatsThisHelpID =   60096
               Width           =   2000
               _Version        =   65536
               _ExtentX        =   3528
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bLocked         =   -1  'True
               mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.#####"
               text            =   "         0.00000"
               dMaxValue       =   9999999999.99999
               sIntegralPlaces =   10
               sDecimalPlaces  =   5
            End
            Begin NEWSOTALib.SOTACurrency crnLandedCost 
               Height          =   285
               Left            =   2205
               TabIndex        =   21
               TabStop         =   0   'False
               Top             =   1230
               WhatsThisHelpID =   60095
               Width           =   2000
               _Version        =   65536
               _ExtentX        =   3528
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bLocked         =   -1  'True
               mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.#####"
               text            =   "         0.00000"
               dMaxValue       =   9999999999.99999
               sIntegralPlaces =   10
               sDecimalPlaces  =   5
            End
            Begin NEWSOTALib.SOTACurrency crnLastCost 
               Height          =   285
               Left            =   2205
               TabIndex        =   23
               TabStop         =   0   'False
               Top             =   1560
               WhatsThisHelpID =   60094
               Width           =   2000
               _Version        =   65536
               _ExtentX        =   3528
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   -2147483633
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bLocked         =   -1  'True
               mask            =   "<HL> <ILH>#|,###|,###|,##<ILp0>#<IRp0>|.#####"
               text            =   "         0.00000"
               dMaxValue       =   9999999999.99999
               sIntegralPlaces =   10
               sDecimalPlaces  =   5
            End
            Begin NEWSOTALib.SOTANumber nbrRollingWAC 
               Height          =   285
               Left            =   2205
               TabIndex        =   25
               Top             =   1920
               WhatsThisHelpID =   60133
               Width           =   2000
               _Version        =   65536
               lFormat         =   2
               _ExtentX        =   3528
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.#####"
               text            =   "       0.00000"
               dMaxValue       =   99999999.99999
               sIntegralPlaces =   8
               sDecimalPlaces  =   5
            End
            Begin VB.Label lblAvgAnnualWT 
               AutoSize        =   -1  'True
               Caption         =   "Rolling WAC"
               Height          =   195
               Left            =   120
               TabIndex        =   24
               Top             =   1980
               Width           =   900
            End
            Begin VB.Label lblRplcmntUnitCost 
               AutoSize        =   -1  'True
               Caption         =   "Annual WAC"
               Height          =   195
               Left            =   120
               TabIndex        =   16
               Top             =   630
               Width           =   915
            End
            Begin VB.Label lblAverage 
               AutoSize        =   -1  'True
               Caption         =   "Average"
               Height          =   195
               Left            =   120
               TabIndex        =   18
               Top             =   960
               Width           =   600
            End
            Begin VB.Label lblLandedCost 
               AutoSize        =   -1  'True
               Caption         =   "Landed"
               Height          =   195
               Left            =   120
               TabIndex        =   20
               Top             =   1290
               Width           =   540
            End
            Begin VB.Label lblStdUnitCost 
               AutoSize        =   -1  'True
               Caption         =   "Standard"
               Height          =   195
               Left            =   120
               TabIndex        =   14
               Top             =   300
               Width           =   645
            End
            Begin VB.Label lblLastCost 
               AutoSize        =   -1  'True
               Caption         =   "Last"
               Height          =   195
               Left            =   120
               TabIndex        =   22
               Top             =   1620
               Width           =   300
            End
         End
         Begin VB.Frame Frame10 
            Caption         =   "Ran&k"
            Height          =   2000
            Left            =   4860
            TabIndex        =   34
            Top             =   1815
            Width           =   4425
            Begin SOTADropDownControl.SOTADropDown ddnHitsRank 
               Height          =   315
               Left            =   1995
               TabIndex        =   42
               Top             =   1440
               WhatsThisHelpID =   60087
               Width           =   1200
               _ExtentX        =   2117
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnHitsRank"
               SQLStatement    =   "select timInvtRank.RankID from timInvtRank where HitsPct > 0"
               AllowClear      =   0   'False
            End
            Begin SOTADropDownControl.SOTADropDown ddnGrMgnRank 
               Height          =   315
               Left            =   1995
               TabIndex        =   38
               Top             =   640
               WhatsThisHelpID =   60086
               Width           =   1200
               _ExtentX        =   2117
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnGrMgnRank"
               SQLStatement    =   "select timInvtRank.RankID from timInvtRank where GrossMarginPct > 0"
               AllowClear      =   0   'False
            End
            Begin SOTADropDownControl.SOTADropDown ddnQtySoldRank 
               Height          =   315
               Left            =   1995
               TabIndex        =   40
               Top             =   1040
               WhatsThisHelpID =   60085
               Width           =   1200
               _ExtentX        =   2117
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnQtySoldRank"
               SQLStatement    =   "select timInvtRank.RankID from timInvtRank where QtySoldPct > 0"
               AllowClear      =   0   'False
            End
            Begin SOTADropDownControl.SOTADropDown ddnCOSRank 
               Height          =   315
               Left            =   1995
               TabIndex        =   36
               Top             =   240
               WhatsThisHelpID =   60084
               Width           =   1200
               _ExtentX        =   2117
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnCOSRank"
               AllowClear      =   0   'False
            End
            Begin VB.Label lblHitsRank 
               AutoSize        =   -1  'True
               Caption         =   "Hits"
               Height          =   195
               Left            =   120
               TabIndex        =   41
               Top             =   1500
               Width           =   1725
            End
            Begin VB.Label lblGrossMarginRank 
               AutoSize        =   -1  'True
               Caption         =   "Gross Margin %"
               Height          =   195
               Left            =   120
               TabIndex        =   37
               Top             =   700
               Width           =   1725
            End
            Begin VB.Label lblQtySoldRank 
               AutoSize        =   -1  'True
               Caption         =   "Quantity Sold"
               Height          =   195
               Left            =   120
               TabIndex        =   39
               Top             =   1100
               Width           =   1725
            End
            Begin VB.Label lblCOGSRank 
               AutoSize        =   -1  'True
               Caption         =   "Cost of Goods Sold"
               Height          =   195
               Left            =   120
               TabIndex        =   35
               Top             =   300
               Width           =   1725
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "&Physical Count"
            Height          =   1785
            Left            =   4845
            TabIndex        =   26
            Top             =   0
            Width           =   4425
            Begin VB.TextBox txtPct 
               Height          =   285
               Left            =   1680
               TabIndex        =   33
               TabStop         =   0   'False
               Top             =   1410
               Visible         =   0   'False
               WhatsThisHelpID =   60078
               Width           =   1575
            End
            Begin SOTACalendarControl.SOTACalendar calLastCountDate 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   1995
               TabIndex        =   30
               TabStop         =   0   'False
               Top             =   645
               WhatsThisHelpID =   60077
               Width           =   1995
               _ExtentX        =   3519
               _ExtentY        =   556
               BackColor       =   -2147483633
               DisplayOnly     =   -1  'True
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Protected       =   -1  'True
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin VB.VScrollBar vsTolPct 
               Height          =   285
               Left            =   3060
               Max             =   -1
               Min             =   1
               TabIndex        =   106
               TabStop         =   0   'False
               Top             =   1050
               Value           =   -1
               WhatsThisHelpID =   60076
               Width           =   255
            End
            Begin SOTADropDownControl.SOTADropDown ddnPhysCycle 
               Height          =   315
               Left            =   2000
               TabIndex        =   28
               Top             =   240
               WhatsThisHelpID =   60075
               Width           =   2000
               _ExtentX        =   3519
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnPhysCycle"
               SQLStatement    =   "select tciProcCycle.ProcCycleID, tciProcCycle.ProcCycleKey from tciProcCycle"
               Sorted          =   0   'False
            End
            Begin NEWSOTALib.SOTANumber txtCountTol 
               Height          =   285
               Left            =   1995
               TabIndex        =   32
               Top             =   1050
               WhatsThisHelpID =   60074
               Width           =   1080
               _Version        =   65536
               _ExtentX        =   1905
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#<IRp0>|.##"
               text            =   "    0.00"
               dMaxValue       =   999.99
               sIntegralPlaces =   5
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblPhysCountCycle 
               AutoSize        =   -1  'True
               Caption         =   "Cycle"
               Height          =   195
               Left            =   120
               TabIndex        =   27
               Top             =   300
               Width           =   1710
            End
            Begin VB.Label lblTolerance 
               AutoSize        =   -1  'True
               Caption         =   "Tolerance %"
               Height          =   195
               Left            =   120
               TabIndex        =   31
               Top             =   1110
               Width           =   1710
            End
            Begin VB.Label lblLastDate 
               AutoSize        =   -1  'True
               Caption         =   "Last Date"
               Height          =   195
               Left            =   120
               TabIndex        =   29
               Top             =   705
               Width           =   1710
            End
         End
      End
      Begin VB.Frame CustomFrame 
         Caption         =   "Frame6"
         Enabled         =   0   'False
         Height          =   255
         Index           =   0
         Left            =   -30000
         TabIndex        =   126
         Top             =   2160
         Visible         =   0   'False
         Width           =   735
      End
      Begin Threed.SSPanel pnlTab 
         Height          =   3795
         Index           =   5
         Left            =   120
         TabIndex        =   164
         Top             =   480
         Width           =   9360
         _Version        =   65536
         _ExtentX        =   16510
         _ExtentY        =   6694
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame Frame6 
            Caption         =   "Custom Settings"
            Height          =   1545
            Left            =   0
            TabIndex        =   165
            Top             =   0
            Width           =   9285
            Begin VB.ComboBox ddnPlaceCode 
               Height          =   315
               ItemData        =   "Imzmt001.frx":2D3C
               Left            =   1425
               List            =   "Imzmt001.frx":2D3E
               Style           =   2  'Dropdown List
               TabIndex        =   167
               Top             =   330
               Width           =   5280
            End
            Begin VB.ComboBox nbrPercentage 
               Enabled         =   0   'False
               Height          =   315
               Left            =   1425
               Style           =   2  'Dropdown List
               TabIndex        =   170
               Top             =   735
               Width           =   1290
            End
            Begin NEWSOTALib.SOTANumber nbrType 
               Height          =   300
               Left            =   6990
               TabIndex        =   168
               Top             =   345
               Visible         =   0   'False
               Width           =   1125
               _Version        =   65536
               _ExtentX        =   1984
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               dMaxValue       =   4
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber nbrPercentAsphalt 
               Height          =   300
               Left            =   1425
               TabIndex        =   172
               Top             =   1095
               Width           =   1290
               _Version        =   65536
               _ExtentX        =   2275
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "100.00"
               dMinValue       =   1
               dMaxValue       =   100
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
               dValue          =   100
            End
            Begin NEWSOTALib.SOTANumber nbrPercentResidue 
               Height          =   300
               Left            =   3000
               TabIndex        =   174
               Top             =   1080
               Visible         =   0   'False
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "         0.00"
               sIntegralPlaces =   10
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber nbrHeatCapacity 
               Height          =   300
               Left            =   4920
               TabIndex        =   176
               Top             =   1080
               Visible         =   0   'False
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "         0.00"
               sIntegralPlaces =   10
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber nbrNormalStorageTemp 
               Height          =   300
               Left            =   7080
               TabIndex        =   178
               Top             =   1080
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#"
               text            =   "         0"
               sIntegralPlaces =   10
               sDecimalPlaces  =   0
            End
            Begin VB.Label Label10 
               Caption         =   "Normal Storage Temp. ( F)"
               Height          =   225
               Left            =   7080
               TabIndex        =   177
               Top             =   795
               Width           =   1950
            End
            Begin VB.Label Label9 
               Caption         =   "Heat Capacity (BTU/Lb F)"
               Height          =   225
               Left            =   4920
               TabIndex        =   175
               Top             =   795
               Visible         =   0   'False
               Width           =   2190
            End
            Begin VB.Label Label8 
               Caption         =   "% Residue (%)"
               Height          =   225
               Left            =   3000
               TabIndex        =   173
               Top             =   795
               Visible         =   0   'False
               Width           =   1110
            End
            Begin VB.Label lblPlaceCode 
               Caption         =   "Place Code"
               Height          =   225
               Left            =   180
               TabIndex        =   166
               Top             =   345
               Width           =   1455
            End
            Begin VB.Label lblAdditive 
               Caption         =   "Additive %"
               Height          =   225
               Left            =   180
               TabIndex        =   169
               Top             =   795
               Width           =   1455
            End
            Begin VB.Label lblPercentAsphalt 
               Caption         =   "Percent Asphalt"
               Height          =   225
               Left            =   180
               TabIndex        =   171
               Top             =   1170
               Width           =   1455
            End
         End
         Begin VB.Frame Frame11 
            Caption         =   "Specifications"
            Height          =   1995
            Left            =   0
            TabIndex        =   179
            Top             =   1680
            Width           =   9285
            Begin NEWSOTALib.SOTAMaskedEdit txtRemarks 
               Height          =   300
               Left            =   1320
               TabIndex        =   193
               Top             =   1590
               Width           =   5175
               _Version        =   65536
               _ExtentX        =   9128
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   16
            End
            Begin NEWSOTALib.SOTANumber nbrViscosity 
               Height          =   300
               Left            =   1305
               TabIndex        =   187
               Top             =   720
               Width           =   1245
               _Version        =   65536
               _ExtentX        =   2196
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber nbrPenetration 
               Height          =   300
               Left            =   1305
               TabIndex        =   181
               Top             =   300
               Width           =   1245
               _Version        =   65536
               _ExtentX        =   2196
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber nbrPenTemp 
               Height          =   300
               Left            =   2775
               TabIndex        =   183
               Top             =   285
               Width           =   1125
               _Version        =   65536
               _ExtentX        =   1984
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber nbrViscTemp 
               Height          =   300
               Left            =   2775
               TabIndex        =   189
               Top             =   720
               Width           =   1125
               _Version        =   65536
               _ExtentX        =   1984
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtPenRemark 
               Height          =   300
               Left            =   4065
               TabIndex        =   185
               Top             =   285
               Width           =   2415
               _Version        =   65536
               _ExtentX        =   4260
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   16
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtViscRemark 
               Height          =   300
               Left            =   4080
               TabIndex        =   191
               Top             =   720
               Width           =   2415
               _Version        =   65536
               _ExtentX        =   4260
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   16
            End
            Begin NEWSOTALib.SOTANumber nbrFlashpoint 
               Height          =   300
               Left            =   7635
               TabIndex        =   195
               Top             =   285
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber nbrLbsGal 
               Height          =   300
               Left            =   7635
               TabIndex        =   198
               Top             =   720
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber nbrGravity 
               Height          =   300
               Left            =   7635
               TabIndex        =   200
               Top             =   1155
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin NEWSOTALib.SOTANumber nbrTemp 
               Height          =   300
               Left            =   7635
               TabIndex        =   202
               Top             =   1590
               Width           =   1275
               _Version        =   65536
               _ExtentX        =   2249
               _ExtentY        =   529
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#|,###|,###|,##<ILp0>#<IRp0>|.####"
               text            =   "         0.0000"
               sIntegralPlaces =   10
               sDecimalPlaces  =   4
            End
            Begin VB.Label lblPenetration 
               Caption         =   "Penetration"
               Height          =   225
               Left            =   180
               TabIndex        =   180
               Top             =   360
               Width           =   1455
            End
            Begin VB.Label lblViscosity 
               Caption         =   "Viscosity"
               Height          =   225
               Left            =   180
               TabIndex        =   186
               Top             =   735
               Width           =   1455
            End
            Begin VB.Label lblRemarks 
               Caption         =   "Remarks"
               Height          =   225
               Left            =   180
               TabIndex        =   192
               Top             =   1590
               Width           =   1455
            End
            Begin VB.Label Label3 
               Caption         =   "F"
               Height          =   285
               Left            =   3900
               TabIndex        =   184
               Top             =   330
               Width           =   165
            End
            Begin VB.Label Label4 
               Caption         =   "F"
               Height          =   285
               Left            =   3900
               TabIndex        =   190
               Top             =   780
               Width           =   165
            End
            Begin VB.Label lblFlashpoint 
               Alignment       =   1  'Right Justify
               Caption         =   "Flashpoint"
               Height          =   225
               Left            =   6615
               TabIndex        =   194
               Top             =   360
               Width           =   870
            End
            Begin VB.Label lblLbsGal 
               Alignment       =   1  'Right Justify
               Caption         =   "Lbs/gal"
               Height          =   225
               Left            =   6495
               TabIndex        =   197
               Top             =   765
               Width           =   990
            End
            Begin VB.Label lblGravity 
               Alignment       =   1  'Right Justify
               Caption         =   "Specific Gravity @ 60 F"
               Height          =   225
               Left            =   5715
               TabIndex        =   199
               Top             =   1185
               Width           =   1770
            End
            Begin VB.Label Label6 
               Caption         =   "F"
               Height          =   285
               Left            =   8940
               TabIndex        =   203
               Top             =   1620
               Width           =   180
            End
            Begin VB.Label Label7 
               Alignment       =   1  'Right Justify
               Caption         =   "Temperature"
               Height          =   240
               Left            =   6465
               TabIndex        =   201
               Top             =   1635
               Width           =   1020
            End
            Begin VB.Label Label1 
               Alignment       =   1  'Right Justify
               Caption         =   "@"
               Height          =   285
               Left            =   2250
               TabIndex        =   182
               Top             =   330
               Width           =   465
            End
            Begin VB.Label Label2 
               Alignment       =   1  'Right Justify
               Caption         =   "@"
               Height          =   285
               Left            =   2250
               TabIndex        =   188
               Top             =   810
               Width           =   465
            End
            Begin VB.Label Label5 
               Caption         =   "F"
               Height          =   285
               Left            =   8940
               TabIndex        =   196
               Top             =   300
               Width           =   225
            End
         End
      End
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   115
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   116
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   117
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   118
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   119
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   120
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   121
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   122
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   124
      TabStop         =   0   'False
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6570
      WhatsThisHelpID =   73
      Width           =   9765
      _ExtentX        =   17224
      _ExtentY        =   688
      NavigatorVisible=   -1  'True
   End
   Begin MSComctlLib.ImageList imlToolbar 
      Left            =   4650
      Top             =   -90
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -1
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   123
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmInventoryMNT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'*********************************************
'   Name        :   frmInventoryMNT
'   Desc        :   This form maintains an Inventory Item for a Warehouse.
'   Original    :   05/08/98
'   Mods        :
'*********************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    '=================================================================================================
    'SGS DEJ Start  ==================================================================================
    '=================================================================================================
    Private WithEvents moDMExt As clsDmForm
Attribute moDMExt.VB_VarHelpID = -1
    Dim mbSaveSGS As Boolean
    '=================================================================================================
    'SGS DEJ End    ==================================================================================
    '=================================================================================================

'-- Private Variables of Form Properties
    Private moClass              As Object
    Private mlRunMode            As Long
    Private mbSaved              As Boolean
    Private mbCancelShutDown     As Boolean
    Private mbIRIsActivated      As Boolean
    Private mbWMIsLicensed       As Boolean
    
'-- Public Form Variables
    Public moSotaObjects         As New Collection
    Public moMapSrch             As New Collection

'-- Private Form Variables
    Private miFilter             As Integer
    Private mbPPLManuallyEntered As Boolean

'-- Binding Object Variables
    Private WithEvents moDmForm             As clsDmForm       ' Data Manager Object
Attribute moDmForm.VB_VarHelpID = -1
    Private WithEvents moDmGridPrefBinDist  As clsDmGrid       ' Grid DM Object for distribution preferred bins
Attribute moDmGridPrefBinDist.VB_VarHelpID = -1
    Private WithEvents moDmGridPrefBinMfg   As clsDmGrid       ' Grid DM Object for manufacutring preferred bins
Attribute moDmGridPrefBinMfg.VB_VarHelpID = -1
    Private moGridNavPrefBinDist            As clsGridLookup   ' Grid Navigator
    Private moGridNavPrefBinMfg             As clsGridLookup   ' Grid Navigator
    Private WithEvents moGMPrefBinDist      As clsGridMgr      ' Grid Manager for distribution preferred bins
Attribute moGMPrefBinDist.VB_VarHelpID = -1
    Private WithEvents moGMPrefBinMfg       As clsGridMgr      ' Grid Manager for manufacturing preferred bins
Attribute moGMPrefBinMfg.VB_VarHelpID = -1
    Private mbF4Pressed                     As Boolean
    Private mbActiveCtrlWh                  As Boolean

'-- Object Variable for Context Menus
    Public moContextMenu        As New clsContextMenu   '-- Right Click Context Menu Class

'-- DrillDown Warehouse object
    Dim moDDRWhse   As Object

'-- DrillDown Item object
    Dim moDDRItem   As Object

    'Types of preferred bins.
    Enum PrefBinTypes
        Distribution = 1
        Manufacturing = 2
    End Enum

'-- Miscellaneous Variables
    Private mbEnterAsTab        As Boolean
    Private msCompanyID         As String
    Private msUserID            As String
    Private mlLanguage          As Long
    Private mbInNavigator       As Boolean
    Private mbDmLoad            As Boolean
    Private miSecurityLevel     As Integer
    Private mbIsBinValid        As Boolean
    Private msWarningForPPL     As String
        
    'Currency Info
    Private msHomeCurrID        As String           'Home Currency ID
    Private muHomeCurrInfo      As CurrencyInfo     'Currency Info for Home Currency

    '-- The following variable is only necessary if there are combo boxes
    '-- on the form. If there are none, you may remove all references of
    '-- this variable in the project.
    Private mbManualComboSelect  As Boolean     'Determines whether a combo box was "clicked" through code or by the user
    
    'Constants Defined for distribution preferred bin grid
    Private Const kColBinIDDist         As Integer = 1
    Private Const kColBinLocationDist   As Integer = 2
    Private Const kColWhseBinKeyDist    As Integer = 3
    Private Const kColOldWhseBinIDDist  As Integer = 4
    Private Const kColPrefBinTypeDist   As Integer = 5
    Private Const kMaxColsDist          As Integer = 5
    
    'Constants Defined for manufacturing preferred bin grid
    Private Const kColBinIDMfg          As Integer = 1
    Private Const kColBinLocationMfg    As Integer = 2
    Private Const kColWhseBinKeyMfg     As Integer = 3
    Private Const kColOldWhseBinIDMfg   As Integer = 4
    Private Const kcolPrefBinTypeMfg    As Integer = 5
    Private Const kMaxColsMfg           As Integer = 5
    
    'Variables for Drill-Around
    Private Const kDAMaintainItems = 1000
    Private Const kDAItemHistory = 2000
    Private Const kDAStockStatus = 3000
    Private msMaintainItems As String
    Private msItemHistory As String
    Private msStockStatus As String
    
    'Variable to check if warehouse tracks qty by bin or not.
    Private mbTrackQtyAtBin  As Boolean
    
    'Variable to check if warehouse uses bins or not.
    Private mbUseBins As Boolean
    
    Private mbIsAssembledKit As Boolean
    Private mbBTOKit         As Boolean
    Private mbValidBins     As Boolean
    
    'Inventory Item Types
    Private Const kFinishedGood = 5
    Private Const kRawMaterial = 6
    Private Const kBTOKit = 7
    Private Const kAssembledKit = 8
    Private Const kCatalog = 9
    
    'Item Status
    Private Const kItemStatus_Active        As Long = 1
    Private Const kItemStatus_Inactive      As Long = 2
    Private Const kItemStatus_Discontinued  As Long = 3
    Private Const kItemStatus_Deleted       As Long = 4
    
    'Warehouse Type
    Private Const kWhseType_Transit     As Long = 1
    
    'Random Bin Type
    Private Const kRandomBin = 2

    'Constants for Tab Control (Tab Numbers)
    Private Const kTabMain As Integer = 0
    Private Const kTabRplnmnt As Integer = 1
    Private Const kTabGLAcct As Integer = 2
    Private Const kTabPrefBin As Integer = 3
    Private Const kTabCustomizer As Integer = 4
    
    'Index of Control Array used for GL Accounts
    Private Const kGLPurchAcct = 0
    Private Const kGLSalesAcct = 1
    Private Const kGLSalesOffsetAcct = 2
    Private Const kGLSalesReturnAcct = 3
    Private Const kGLInvtAcct = 4
    Private Const kGLCOSAcct = 5
    Private Const kGLIssueAcct = 6
    Private Const kGLMiscAdjAcct = 7
    Private Const kGLCostTierAdjAcct = 8
    Private Const kGLPhysCountAcct = 9
    
'--Count for the Control Array
    Private Const kArrCount     As Integer = 9
    Private miCount             As Integer
    
    Private Const kPrimaryVendorInitialize   As Boolean = True
    Private Const kPrimaryVendorDoNotInitialize = False
    
'-- Private Varible to Implement Module Options Class
    Private moOptions           As New clsModuleOptions
    
'-- Variable for ItemType
    Private miItemType          As Integer
    Private miItemStatus        As Integer
    Private miStockUOMKey       As Long
    Private miAllowDecimalQty   As Boolean
    
    'Variables to hold key values for their corresponding ID's.
    Private mlItemKey           As Long     'Holds Item Key
    Private mlWhseKey           As Long     'Holds Warehouse Key
    Private mlPrevPPLKey        As Long     'Used to load default PPL values, if PPL changes in the lookup
    Private mlPPLKey            As Long     'Holds PPL Key
    Private mlUOMKey            As Long     'Holds UOM Key

    Private msBusinessDate      As String   'Holds Business Date
    
    Private mbAllowSave As Boolean          'Used to check whether BinID specified in BinID
                                            'column is valid or not, if user directly saves
                                            'record after entering BinID
    
    Private miQtyDecPlaces    As Integer    'Variable used to hold the decimal places for qty tciOptions
    Private miCostDecPlaces   As Integer    'Variable used to hold the decimal places for cost in tciOptions

    Private mbFrombIsValid As Boolean
    
    Const ktskMaintainItems = 117506178
    Const ktskItemHistoryInquiry = 117637190
    Const ktskStockStausInquiry = 117637200
    
    Private Const kWhseDrillDownTask = 117506168
    Private Const kWhseDrillDownClass = "imzmk001.clsWarehouse"
    Private Const kItemDrillDownTask = 117506178
    Private Const kItemDrillDownClass = "imzmr001.clsMaintainItem"
    Private Const kIMInvtList = 118095982   'Inventory Listing TaskID
    Private Const kModuleIR As Integer = 16
    
    Private mbMessageDisplayed      As Boolean
    
    'Item Valuation Method
    Private Const kItemValuationMeth_Std = 2
    
    'Constants for Security Events
    Private Const kInvSecEventRUCost = "CHGINVRCOS"
    Private Const kSecChangeInvtStat = "CHGIVTSTAT"
    
    Private iNoOfObj As Long
    
    'For Carrying Cost Percentage
    Private miCCPctIntPlaces As Integer
    Private miCCPctDecPlaces As Integer
    Private mbCCPctKeyPressed As Boolean
    
    'For Cost of Replenishment
    Private miCRepIntPlaces As Integer
    Private miCRepDecPlaces As Integer
    Private mbCRepKeyPressed As Boolean
    
    Private mbFirstTime As Boolean
    Private mbF5Down As Boolean
    Private miItemTrackMethod As Integer

    Private mbIMActivated As Boolean
    Private mbMFActivated          As Boolean          'Whether Manufacturing Module is Activated
    Private mbAMActivated As Boolean
    
   
    'Constants for PickMethod DropDown
    Private Const kMinBinPicks = 1
    Private Const kOldestRecDate = 2
    Private Const kEmptyBin = 3
    Private Const kMinimizeLot = 4
    Private Const kOldestLot = 5
    ' Constants for ItemTrackMethod
    Private Const kNone = 0
    Private Const kLotItem = 1
    Private Const kSerialItem = 2
    Private Const kLotSerialItem = 3
    
Const VBRIG_MODULE_ID_STRING = "IMZMT001.FRM"

Private Function bValidMinMaxStock() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValidMinMaxStock = True
    
    If nbrMaxStockQty.Value < nbrMinStockQty.Value Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, "Min Stock Qty", "Max Stock Qty"
        
        bValidMinMaxStock = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidMinMaxStock", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bValidSourceWarehouse() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rs As Object
    Dim bIsAssKitWhse As Boolean
    Dim iCanReplenish As Integer
    Dim lKey As Long
    Dim sRestrictClause As String

        bValidSourceWarehouse = False
    If Len(Trim$(lkuWhseFrom.Text)) > 0 Then
        'Source and destination should not be same for items other than Assembled Kit.
        If Trim$(lkuWhse.Text) = Trim$(lkuWhseFrom.Text) Then
            If Not (mbBTOKit Or mbIsAssembledKit) Then
                giSotaMsgBox Me, moClass.moSysSession, kIMSelfRplcmnt
                
                lkuWhseFrom.Text = lkuWhseFrom.Tag
                lkuWhseFrom.SetFocus
                
                Exit Function
            End If
   
        Else
            'When Both Warehouses are different.
            If mbIsAssembledKit Then
                sRestrictClause = lkuWhseFrom.RestrictClause

                sSQL = "SELECT DISTINCT WhseID, Description FROM timWarehouse WITH (NOLOCK)"
                sSQL = sSQL & " WHERE " & sRestrictClause
                Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
                
                bIsAssKitWhse = False

                If Not rs Is Nothing Then
                    Do While Not rs.IsEOF()
                        If Trim$(lkuWhseFrom.Text) = Trim$(rs.Field("WhseID")) Then
                            bIsAssKitWhse = True

                            'In some cases lookup was not able to get correct key value. Which causes a data corruption.
                            'So I am picking explictly here
                            lKey = gvCheckNull(moClass.moAppDB.Lookup("WhseKey", "timWarehouse", "CompanyID = " & gsQuoted(msCompanyID) & " AND WhseID = " & gsQuoted(lkuWhseFrom.Text)), 0)
                            If lKey <> 0 Then
                                lkuWhseFrom.KeyValue = lKey
                                lblWhseDescFrom = Trim$(rs.Field("Description"))
                            End If
                            
                            Exit Do
                        End If
                        rs.MoveNext
                    Loop
                    
                    rs.Close
                    Set rs = Nothing

                    If Not bIsAssKitWhse Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, "Warehouse"
                        
                        lkuWhseFrom.Text = lkuWhseFrom.Tag
                        lkuWhseFrom.SetFocus
                        lblWhseDescFrom.Caption = ""
                        Exit Function
                    End If
                End If
            End If
            
            sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
            If mbBTOKit Or mbIsAssembledKit Then
                sRestrictClause = sRestrictClause & " AND WhseKey = " & Format$(giGetValidInt(mlWhseKey)) & ")"
            Else
                sRestrictClause = sRestrictClause & " AND WhseKey IN (SELECT i.WhseKey FROM timInventory i WITH (NOLOCK) WHERE i.ItemKey = " & Format$(mlItemKey) & ")"
                
                If mbIsAssembledKit Then
                    sRestrictClause = sRestrictClause & " OR WhseKey = " & CStr(giGetValidInt(mlWhseKey))
                End If
            End If
            sRestrictClause = "(" & sRestrictClause & ") AND WhseID = " & gsQuoted(Trim$(lkuWhseFrom.Text))
           
            
            lKey = giGetValidInt(moClass.moAppDB.Lookup("WhseKey", "timWarehouse", sRestrictClause))
            If lKey <> 0 Then
                lkuWhseFrom.KeyValue = lKey
                lblWhseDescFrom.Caption = moClass.moAppDB.Lookup _
                                ("Description", "timWarehouse", "WhseKey = " & Format$(giGetValidInt(lkuWhseFrom.KeyValue)))
            Else
                lkuWhseFrom.Text = lkuWhseFrom.Tag
                lkuWhseFrom.SetFocus
                lblWhseDescFrom.Caption = ""
                Exit Function
            End If

        End If

        lkuPPLine.ClearData
        mlPrevPPLKey = 0
        lkuPPLine.Enabled = False
        lkuPPLine.EnabledLookup = False
        bValidSourceWarehouse = True

        With moClass.moAppDB
            .SetInParamLong giGetValidInt(mlWhseKey)
            .SetInParamLong giGetValidInt(mlItemKey)
            .SetInParamLong giGetValidInt(lkuWhseFrom.KeyValue)
            .SetOutParam iCanReplenish
            .ExecuteSP "spimWhseRepItem"
            iCanReplenish = .GetOutParam(4)
            .ReleaseParams
        End With
        
        If iCanReplenish = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kIMmsgCannotReplenish, lkuWhseFrom.Text, lkuWhse.Text
            
            lkuWhseFrom.Text = lkuWhseFrom.Tag
            lkuWhseFrom.SetFocus
        
        End If
    Else
        'When lkuWhseFrom is blank.
        bValidSourceWarehouse = True

        lkuPPLine.Enabled = True
        lkuPPLine.EnabledLookup = True
        
    End If
    
    'The source warehouse control interacts with the primary vendor field and the WPPL checkbox. Update these
    'controls according to the source warehouse changes.
    Call DisplayPrimaryVendor(kPrimaryVendorDoNotInitialize)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidSourceWarehouse", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function gvCheckNull(vField, Optional vDataType As Variant, Optional vSetEmpty As Variant) As Variant
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If IsMissing(vDataType) Then
        vDataType = SQL_CHAR
    End If
    
    If IsMissing(vSetEmpty) Then
        vSetEmpty = False
    End If
    
    If (IsEmpty(vField) _
Or Len(vField) = 0) Then
        If vSetEmpty Then
            gvCheckNull = Empty
        Else
            If vDataType = SQL_CHAR Then
                gvCheckNull = ""
            Else
                gvCheckNull = 0
            End If
       End If
    Else
        If IsNull(vField) Then
            If vSetEmpty Then
                gvCheckNull = Empty
            Else
                If vDataType = SQL_CHAR Then
                    gvCheckNull = ""
                Else
                    gvCheckNull = 0
                End If
            End If
        Else
            gvCheckNull = vField
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "gvCheckNull", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function bMoveRecords(tPrefBinType As PrefBinTypes) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lRowToDelete As Long
    Dim lRowToReplace As Long
    Dim lMaxRow As Long
    Dim oGrid As Object
    Dim oDM As Object
    Dim lColBinID As Long
    Dim lColBinLocation As Long
    Dim lColWhseBinKey As Long
    Dim lColOldWhseBinID As Long
    
    Select Case tPrefBinType
        Case PrefBinTypes.Distribution
            Set oGrid = grdPrefBinDist
            Set oDM = moDmGridPrefBinDist
            lColBinID = kColBinIDDist
            lColBinLocation = kColBinLocationDist
            lColWhseBinKey = kColWhseBinKeyDist
            lColOldWhseBinID = kColOldWhseBinIDDist
        Case PrefBinTypes.Manufacturing
            Set oGrid = grdPrefBinMfg
            Set oDM = moDmGridPrefBinMfg
            lColBinID = kColBinIDMfg
            lColBinLocation = kColBinLocationMfg
            lColWhseBinKey = kColWhseBinKeyMfg
            lColOldWhseBinID = kColOldWhseBinIDMfg

    End Select

    bMoveRecords = False
    
    lRowToDelete = oGrid.ActiveRow
    lMaxRow = glGridGetMaxRows(oGrid)
    
    oGrid.redraw = False
    
    'Move next row's data into the previous row, after the row marked for deletion
    'up to the last row.
    For lRowToReplace = lRowToDelete To lMaxRow - 2
        gGridUpdateCell oGrid, lRowToReplace, lColBinID, gsGridReadCell(oGrid, lRowToReplace + 1, lColBinID)
        gGridUpdateCell oGrid, lRowToReplace, lColBinLocation, gsGridReadCell(oGrid, lRowToReplace + 1, lColBinLocation)
        gGridUpdateCell oGrid, lRowToReplace, lColWhseBinKey, gsGridReadCell(oGrid, lRowToReplace + 1, lColWhseBinKey)
        gGridUpdateCell oGrid, lRowToReplace, lColOldWhseBinID, gsGridReadCell(oGrid, lRowToReplace + 1, lColOldWhseBinID)
        
        oDM.SetRowDirty lRowToReplace
    Next lRowToReplace

    With oGrid
        .Row = lMaxRow - 1
        .Row2 = lMaxRow - 1
        .Action = SS_ACTION_SELECT_BLOCK
        .redraw = True
    End With

    bMoveRecords = True
    Set oGrid = Nothing
    Set oDM = Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bMoveRecords", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function



Private Sub chkUseDistBinsInMfg_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkUseDistBinsInMfg, True
    #End If
'+++ End Customizer Code Push +++

    'If the checkbox is being checked by the user and manufacturing preferred bins
    'exist, prompt the user for verification as this action will delete the manufacturing
    'preferred  bins.
    If chkUseDistBinsInMfg.Value = vbChecked And mbDmLoad = False Then
        'Prompt user for verification and delete mfg bins if approved.  Restore to the
        'unchecked state if the user did not approve.
        If bDeleteMfgBinEntries = False Then
            chkUseDistBinsInMfg.Value = vbUnchecked
        End If
    End If

    'Since the checkbox may have changed, update the enabled state of the mfg
    'preferred bins frame.
    SetMfgPreferredBinsFrame
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkUseDistBinsInMfg_Click", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub chkUseWPPLPrimaryVendor_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkUseWPPLPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

    'Clear the existing vendor upon a click and then use the display routine to set the
    'appropriate primary vendor.
    lkuPrimaryVendor.ClearData
    lblPrimaryVendorName.Caption = ""
    
    If chkUseWPPLPrimaryVendor.Value = vbUnchecked And glGetValidLong(moDmForm.GetColumnValue("PrimaryVendKey")) > 0 Then
        lkuPrimaryVendor.KeyValue = moDmForm.GetColumnValue("PrimaryVendKey")
    End If
    
    Call DisplayPrimaryVendor(kPrimaryVendorDoNotInitialize)
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkUseWPPLPrimaryVendor_Click", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'SGS DEJ 11/13/09 Hal asked to add this functionality
Private Sub cmdUpdRpcmntCst_Click()
    On Error GoTo Error
    
    Dim lRetVal As Long
    Dim lWhseKey As Long
    Dim lWhseID As String
    Dim lItemKey As Long
    Dim lItemID As String
    
    lWhseKey = glGetValidLong(lkuWhse.KeyValue)
    lWhseID = gsGetValidStr(lkuWhse.Text)
    
    lItemKey = glGetValidLong(lkuItem.KeyValue)
    lItemID = gsGetValidStr(lkuItem.Text)
    
    
    'Validate the data
    If Trim(lkuWhse.Text) = Empty Then
        MsgBox "You must select a warehouse first.", vbInformation, "MAS 500"
        
        If lkuWhse.Enabled = True Then
            If Not Me.ActiveControl Is lkuWhse Then
                lkuWhse.SetFocus
            End If
        End If
    End If
    
    If mlWhseKey <> lkuWhse.KeyValue Then
        MsgBox "There is an issue with the warehouse.  Please contact your IT support.", vbInformation, "MAS 500"
        
        If lkuWhse.Enabled = True Then
            If Not Me.ActiveControl Is lkuWhse Then
                lkuWhse.SetFocus
            End If
        End If
    End If
    
    
    mbSaveSGS = False
    'Save the current record
    If MsgBox("This record must be cleared out before continuing.  Do you want to save this record first?", vbYesNo, "Save Record?") = vbYes Then
        HandleToolBarClick kTbSave
        
        If mbSaveSGS <> True Then
            MsgBox "The save was not successful.  Aborting the Update Replenishment Costs.", vbInformation, "MAS 500"
            Exit Sub
        End If
        
    End If
    
    
    'Close the Record
    HandleToolBarClick kTbCancel
    
    Me.MousePointer = vbHourglass
    
    'Begin Transaction
    moClass.moAppDB.BeginTrans
    
    
    With moClass.moAppDB
        .SetInParam glGetValidLong(lWhseKey)
        .SetOutParam lRetVal
        .ExecuteSP "spimUpdateReplacementCost_SGS"
        lRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    Select Case lRetVal
        Case -1 'SQL Error
            moClass.moAppDB.Rollback
            MsgBox "There was a SQL Error.  No records will be updated.", vbExclamation, "MAS 500"

        Case 1 'Success
            moClass.moAppDB.CommitTrans
            MsgBox "The Replacement Costs have been updated for the warehouse: " & lWhseID, vbExclamation, "MAS 500"
        
        Case 2  'No a valid WarehouseKey
            moClass.moAppDB.Rollback
            MsgBox "The warehouse could not be found.  No records will be updated.", vbExclamation, "MAS 500"
        
        Case Else   'Unknown Error
            moClass.moAppDB.Rollback
            MsgBox "Unknown SQL Execution Error.  No records will be updated.", vbExclamation, "MAS 500"
        
    End Select
        
CleanUP:
    Me.MousePointer = vbDefault
    
    'Reload the record
    lkuItem.SetTextAndKeyValue lItemID, lItemKey
    lkuWhse.SetTextAndKeyValue lWhseID, lWhseKey
    
    If Not Me.ActiveControl Is lkuWhse Then
        lkuWhse.SetFocus
    ElseIf Not Me.ActiveControl Is lkuItem Then
        lkuItem.SetFocus
    End If
    
    SendKeys "{TAB}"
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdUpdRpcmntCst_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500 Error"
    
    moClass.moAppDB.Rollback
    
    Err.Clear
    
    GoTo CleanUP
    
End Sub

'SGS DEJ
Private Sub ddnPlaceCode_Validate(Cancel As Boolean)
    
    Select Case ddnPlaceCode.ListIndex
        Case 0          'Not Valid
            If IsPaceReqrd() = True Then
            Cancel = True
            MsgBox "You must select a valid Placard Code.", vbExclamation, "MAS 500"
            End If
        Case 1, 2               'Elevated Temp.
            nbrType.Value = 1
        Case 3, 4, 5, 6, 7      'Hot Tars
            nbrType.Value = 2
        Case 8                  'Not Regulated
            nbrType.Value = 3
        Case 9                  'Other
            nbrType.Value = 4
    End Select
        
End Sub

Private Sub ddnStatus_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnStatus, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRetVal As Long
    
    If ddnStatus.Tag <> ddnStatus.ItemData Then
        If miItemStatus = kItemStatus_Inactive And ddnStatus.ItemData = kItemStatus_Active Then
            giSotaMsgBox Me, moClass.moSysSession, kmsIMInvStatToActiveItemInact
            Cancel = True
        End If
    
        If ddnStatus.ItemData = kItemStatus_Deleted Then ' User has chosen to mark the item for deletion
            With moClass.moAppDB
                .SetInParam 1                           ' Check Inventory
                .SetInParam glGetValidLong(mlItemKey)
                .SetInParam glGetValidLong(mlWhseKey)
                .SetOutParam lRetVal
                .ExecuteSP "spimCheckAllowDelete"
                lRetVal = .GetOutParam(4)
                .ReleaseParams
            End With
            
            If lRetVal <> 1 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMInvStatDelNoAllowed
                Cancel = True
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnStatus_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
End Sub



Private Sub grdPrefBinDist_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinDist.Grid_EditMode Col, Row, Mode, ChangeMade
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_EditMode", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'If the user leaves the grid, hide the lookup.
    If Me.ActiveControl.Name <> lkuPrefBinDist.Name Then
        lkuPrefBinDist.Visible = False
        grdPrefBinDist.ActiveCellHighlightStyle = ActiveCellHighlightStyleOff
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Indicating change to the Navigator
    If Col = kColBinIDMfg Then
        moGMPrefBinMfg_CellChange Row, Col
        grdPrefBinMfg.Tag = "0"
        Exit Sub
    End If
    
    If mbAllowSave Then
        moGMPrefBinMfg.Grid_Change Col, Row
    End If
    
    mbValidBins = True
    grdPrefBinMfg.Tag = "0"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_Change", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
            
    moGMPrefBinMfg.Grid_Click Col, Row
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_Click", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub grdPrefBinMfg_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Col1 = kColBinIDMfg Then
        moGMPrefBinMfg.Grid_ColWidthChange Col1
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_ColWidthChange", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    

    grdPrefBinMfg.ActiveCellHighlightStyle = ActiveCellHighlightStyleNormal
  
    If grdPrefBinMfg.ActiveCol = kColBinIDMfg And grdPrefBinMfg.ActiveRow <> 0 And lkuPrefBinMfg.Visible = False Then
        lkuPrefBinMfg.Visible = True
    End If
    
    moGMPrefBinMfg.Grid_Click 1, 1
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Give a call to moGridNavPrefBinDist's key down use full in f5 key for navigator
    If grdPrefBinMfg.ActiveCol = kColBinLocationMfg Then
        Exit Sub
    End If
    
    moGMPrefBinMfg.Grid_KeyDown KeyCode, Shift
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_KeyDown", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub grdPrefBinMfg_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iBinCount   As Long
    
    If Me.ActiveControl.Name = lkuPrefBinMfg.Name Then
        Exit Sub
    End If
    
    If Row > 0 Then
        If Col = kColBinIDMfg Then
            moGMPrefBinMfg.Grid_LeaveCell Col, Row, NewCol, NewRow
        Else
            If Not moDmGridPrefBinMfg Is Nothing Then
                moGMPrefBinMfg.Grid_LeaveCell Col, Row, NewCol, NewRow
            End If
        End If
        
        If Not mbValidBins Then
            Me.tabInventory.Tab = 3
            gGridSetActiveCell grdPrefBinMfg, Row, Col
            
            'if the user shuts down the form while in grid this will prevent error 5
            If grdPrefBinMfg.Enabled Then
                grdPrefBinMfg.SetFocus
            End If
            mbValidBins = True
        End If
    End If
                
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_LeaveCell", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinMfg.Grid_LeaveRow Row, NewRow
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_LeaveRow", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'If the user leaves the grid, hide the lookup.
    If Me.ActiveControl.Name <> lkuPrefBinMfg.Name Then
        lkuPrefBinMfg.Visible = False
        grdPrefBinMfg.ActiveCellHighlightStyle = ActiveCellHighlightStyleOff
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinMfg_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinMfg.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinMfg_TopLeftChange", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuWhseFrom_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhseFrom, True
    #End If
'+++ End Customizer Code Push +++

    'If the source warehouse is changed or set, clear the primary
    'vendor and update the display.
    If lkuWhseFrom.KeyValue <> lkuWhseFrom.Tag Then
        If lkuWhseFrom.Text <> "" Then
            lkuPrimaryVendor.ClearData
        End If
        Call DisplayPrimaryVendor(kPrimaryVendorDoNotInitialize)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseFrom_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'SGS DEJ New Method
Private Sub moDMExt_DMReposition(oChild As Object)
    '====================================================================================================
    'SGS DEJ Start  =====================================================================================
    '====================================================================================================
    If Trim(moDMExt.GetColumnValue("PlacardCode")) = "" Then
        moDmForm.SetDirty True, True
    End If
    '====================================================================================================
    'SGS DEJ End    =====================================================================================
    '====================================================================================================
End Sub

Private Sub moDmForm_DMBeforeUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Validate and set the primary vendor for save processing.
    bValid = bPrimaryVendorSaveProcessing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeUpdate", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub moGMPrefBinDist_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bContinue = False
    
    If tabInventory.Tab = kTabPrefBin Then
        If grdPrefBinDist.ActiveRow = grdPrefBinDist.MaxRows Then
            Exit Sub
        End If

        If Not bMoveRecords(PrefBinTypes.Distribution) Then
            Exit Sub
        End If
    End If

    bContinue = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMPrefBinDist_GridBeforeDelete", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'***************************************************************************************
'If Selected Item is BTO Kit and Its all components are in inventory then it returns true
'***************************************************************************************
Private Function bIsAllCompInInventory(lWhseKey As Long, lItemKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sWhere As String         'Contains where clause for inventory records
    Dim iInvtCount As Integer    'Count of all inventory records for components
    Dim iCompCount As Integer    'Count of all components for item
    
    sWhere = "timInventory.WhseKey = " & lWhseKey & " AND timInventory.ItemKey = timKitCompList.CompItemKey"
    sWhere = sWhere & " AND timKitCompList.KitItemKey  = " & lItemKey
    
    iInvtCount = moClass.moAppDB.Lookup("Count(*)", "timInventory,timKitCompList", sWhere)
    
    'Non-Inventory items are not considered to count the no. of components.
    iCompCount = moClass.moAppDB.Lookup("Count(*)", "timKitCompList, timItem", _
"KitItemKey = " & lItemKey & _
" AND timKitCompList.CompItemKey = timItem.ItemKey" & _
" AND timItem.ItemType >= " & kFinishedGood)
    
    If iInvtCount = iCompCount Then
        bIsAllCompInInventory = True
    Else
        bIsAllCompInInventory = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsAllCompInInventory", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bDenyControlFocus() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bDenyControlFocus = False

    If Len(Trim$(lkuWhse)) = 0 Then
        If (lkuWhse.Enabled _
        And lkuWhse.Visible) Then
            lkuWhse.SetFocus
            bDenyControlFocus = True
        End If
    End If
    
    If Len(Trim$(lkuItem)) = 0 Then
        If (lkuItem.Enabled _
        And lkuItem.Visible) Then
            lkuItem.SetFocus
            bDenyControlFocus = True
            Exit Function
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bDenyControlFocus", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub FormatGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Setup the grid for distribution preferred bins.
    'The third column is kept for WhseBinKey which will be hidden and will be used
    'while saving.  This will be bound in BinFor.
    gGridSetProperties grdPrefBinDist, kMaxColsDist, kGridDataSheet
    gGridSetMaxRows grdPrefBinDist, 1
    
    grdPrefBinDist.RowHeaderDisplay = DispNumbers
    grdPrefBinDist.UnitType = UnitTypeTwips
    grdPrefBinDist.RowHeight(-1) = 315
    
    'Setting the column headers
    gGridSetHeader grdPrefBinDist, kColBinIDDist, gsBuildString(kDDWhseBinIDShortCol, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdPrefBinDist, kColBinLocationDist, gsBuildString(kLocationCol, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdPrefBinDist, 0, gsBuildString(kIMPreference, moClass.moAppDB, moClass.moSysSession)
    
    'Setting cell types
    gGridSetColumnType grdPrefBinDist, kColBinIDDist, SS_CELL_TYPE_EDIT, 15
    gGridLockColumn grdPrefBinDist, kColBinLocationDist
    gGridSetColumnType grdPrefBinDist, kColOldWhseBinIDDist, SS_CELL_TYPE_STATIC_TEXT
    
    'Setting cell widths
    gGridSetColumnWidth grdPrefBinDist, 0, 1000
    gGridSetColumnWidth grdPrefBinDist, kColBinIDDist, 1800
    gGridSetColumnWidth grdPrefBinDist, kColBinLocationDist, 1500   'This leaves the data cells short of the grid width.
                                                                    'This was done on purpose to leave room for a vertical scroll
                                                                    'bar.  If the scroll bar is inserted a run-time, the grid has
                                                                    'display issues when a new row is added to cause the scroll bar
                                                                    'to appear.
    
    'Hiding the WhseBinKey column
    gGridHideColumn grdPrefBinDist, kColWhseBinKeyDist
    gGridHideColumn grdPrefBinDist, kColOldWhseBinIDDist
    gGridHideColumn grdPrefBinDist, kColPrefBinTypeDist
    
    'Setup the grid for manufacturing preferred bins.
    'The third column is kept for WhseBinKey which will be hidden and will be used
    'while saving.  This will be bound in BinFor.
    gGridSetProperties grdPrefBinMfg, kMaxColsMfg, kGridDataSheet
    gGridSetMaxRows grdPrefBinMfg, 1
    
    grdPrefBinMfg.RowHeaderDisplay = DispNumbers
    grdPrefBinMfg.UnitType = UnitTypeTwips
    grdPrefBinMfg.RowHeight(-1) = 315
    
    'Setting the column headers
    gGridSetHeader grdPrefBinMfg, kColBinIDDist, gsBuildString(kDDWhseBinIDShortCol, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdPrefBinMfg, kColBinLocationDist, gsBuildString(kLocationCol, moClass.moAppDB, moClass.moSysSession)
    gGridSetHeader grdPrefBinMfg, 0, gsBuildString(kIMPreference, moClass.moAppDB, moClass.moSysSession)
    
    'Setting cell types
    gGridSetColumnType grdPrefBinMfg, kColBinIDMfg, SS_CELL_TYPE_EDIT, 15
    gGridLockColumn grdPrefBinMfg, kColBinLocationMfg
    gGridSetColumnType grdPrefBinMfg, kColOldWhseBinIDMfg, SS_CELL_TYPE_STATIC_TEXT
    
    'Setting cell widths
    gGridSetColumnWidth grdPrefBinMfg, 0, 1000
    gGridSetColumnWidth grdPrefBinMfg, kColBinIDMfg, 1800
    gGridSetColumnWidth grdPrefBinMfg, kColBinLocationMfg, 1500
    
    'Hiding the WhseBinKey column
    gGridHideColumn grdPrefBinMfg, kColWhseBinKeyMfg
    gGridHideColumn grdPrefBinMfg, kColOldWhseBinIDMfg
    gGridHideColumn grdPrefBinMfg, kcolPrefBinTypeMfg
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "IMZ"         '       Place your prefix here
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "IMZ"         '       Place your prefix here
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lRunMode", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCancelShutDown", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Public Function CMMenuSelected(Ctl As Control, lTaskID As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************

    Dim oDrillDown  As Object
    Dim bRetCode    As Boolean
    Dim lWhseKey    As Long

    CMMenuSelected = False
    
    If (Ctl Is lkuItem) Then
        If (Trim(lkuItem.Text) = Empty) Then
            Exit Function
        End If
        Select Case lTaskID
            Case ktskMFMRPPlanningDD
                gIMNetReqInquiry lkuItem.KeyValue, lkuWhse.KeyValue, msCompanyID, moClass, Me
            
            Case ktskMFBillOFMat
                Set oDrillDown = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                                                kclsBillOfMatDD, ktskMFBillOFMat, _
                                                kDDRunFlags, kContextDD)
    
                If oDrillDown Is Nothing Then
                    Exit Function
                End If
    
                bRetCode = oDrillDown.bBillOfMatInv(lkuItem.KeyValue, lkuWhse.KeyValue)
                Set oDrillDown = Nothing
            
        End Select
    End If

    CMMenuSelected = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function CMAppendContextMenu(Ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    CMAppendContextMenu = False

    Dim bItemHasValue   As Boolean
    Dim bWhseHasValue   As Boolean
    
    ' Determine if controls have value
    bItemHasValue = (Trim(lkuItem.Text) <> "")
    bWhseHasValue = (Trim(lkuWhse.Text) <> "")

    If (Ctl Is lkuItem) Then
        
        ' Point control to different context menu
        If (bItemHasValue) And (bWhseHasValue) Then
            moContextMenu.AppendDrillMenu hmenu, "IMINVEN", _
                                          Ctl, Me, False, kEntTypeIMInventory
        Else
            moContextMenu.AppendDrillMenu hmenu, "IMITEM", _
                                          Ctl, Me, False, kEntTypeIMItem
        End If
        
        If Not (Trim(lkuItem.Text) = Empty) Then
            If mbMFActivated Then
                AppendMenu hmenu, MF_SEPARATOR, 0, ""
                AppendMenu hmenu, MF_ENABLED, ktskMFMRPPlanningDD, "&MRP Planning"
                AppendMenu hmenu, MF_ENABLED, ktskMFBillOFMat, "&Bill of Material"
            End If
        End If
    End If

    If (Ctl Is lkuWhse) Then
        
        ' Point control to different context menu
        If (bItemHasValue) And (bWhseHasValue) Then
            moContextMenu.AppendDrillMenu hmenu, "IMINVEN", _
                                          Ctl, Me, False, kEntTypeIMInventory
        Else
            moContextMenu.AppendDrillMenu hmenu, "IMWHSE", _
                                          Ctl, Me, False, kEntTypeIMWarehouse
        End If
    End If

    CMAppendContextMenu = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************
' Description:
'    This routine will bind Right-Click context menus to
'    the form and controls.
'*******************************************************
    With moContextMenu
        Set .Form = frmInventoryMNT
        
        .BindGrid moGMPrefBinDist, grdPrefBinDist.hwnd
        .BindGrid moGMPrefBinMfg, grdPrefBinMfg.hwnd

        'Entities will be bound in CMAppendContextMenu.
        .Bind "*DYNAMIC", lkuItem.hwnd
        .Bind "*DYNAMIC", lkuWhse.hwnd
        .Bind "APVEND", lkuPrimaryVendor.hwnd, kEntTypeAPVendor
        .Bind "IMBUYER", ddnBuyer.hwnd, kEntTypeIMBuyer
        .Bind "IMPURCH", lkuPPLine.hwnd, kEntTypeIMPurchProdLine
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function ETWhereClause(ByVal Ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    Dim bItemHasValue   As Boolean
    Dim bWhseHasValue   As Boolean
    
    ' Determine if controls have value
    bItemHasValue = (Trim(lkuItem.Text) <> "")
    bWhseHasValue = (Trim(lkuWhse.Text) <> "")
    
    Select Case True
        Case Ctl Is lkuWhse
            
            If (bItemHasValue) And (bWhseHasValue) Then
                ETWhereClause = "WhseKey = " & CStr(glGetValidLong(lkuWhse.KeyValue)) & " AND " & _
                                "ItemKey = " & CStr(glGetValidLong(lkuItem.KeyValue))
            Else
                ETWhereClause = "WhseKey = " & CStr(glGetValidLong(lkuWhse.KeyValue))
            End If

        Case Ctl Is lkuItem
            
            If (bItemHasValue) And (bWhseHasValue) Then
                ETWhereClause = "WhseKey = " & CStr(glGetValidLong(lkuWhse.KeyValue)) & " AND " & _
                                "ItemKey = " & CStr(glGetValidLong(lkuItem.KeyValue))
            Else
                ETWhereClause = "ItemKey = " & CStr(glGetValidLong(lkuItem.KeyValue))
            End If
        
        Case Ctl Is ddnBuyer
            If (ddnBuyer.ListIndex <> kItemNotSelected) Then
                ETWhereClause = "BuyerKey = " & CStr(glGetValidLong(ddnBuyer.ItemData(ddnBuyer.ListIndex)))
            Else
                ETWhereClause = "1 = 2"
            End If
        
        Case Else
            ETWhereClause = ""
    End Select
    
    Err.Clear
    
End Function

Private Sub BindForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
' Description:
'    This routine will bind fields on the form to fields in the database.
'************************************************************************
    '-- Create a new Data Manager Form class
    Set moDmForm = New clsDmForm
    
    With moDmForm
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
               
        .AppName = Me.Caption
        .Table = "timInventory"
        .UniqueKey = "ItemKey, WhseKey"
        
        'Setting the validation manager
        Set .ValidationMgr = SOTAVM
        
        .Bind chkShipComplete, "ShipComplete", SQL_SMALLINT
        .Bind chkCloseSOLineFirst, "CloseSoLineOnFirstShip", SQL_SMALLINT
        .Bind nbrCostCarryPct, "CostOfCarry", SQL_DECIMAL, kDmSetNull
        .Bind crnCostToReplenish, "CostToReplenish", SQL_DECIMAL, kDmSetNull
        .Bind nbrMinStockQty, "MinStockQty", SQL_DECIMAL
        .BindComboBox ddnPhysCycle, "PhysCountCycleKey", SQL_INTEGER, kDmSetNull Or kDmUseItemData
        .Bind txtPct, "PhysCountTolPct", SQL_DECIMAL
        .Bind nbrMaxStockQty, "MaxStockQty", SQL_DECIMAL
        .BindComboBox ddnReordMeth, "ReordMeth", SQL_SMALLINT, kDmUseItemData
        .Bind calEstDate, "DateEstab", SQL_DATE
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind Nothing, "CreateType", SQL_SMALLINT
        .Bind calMinMax, "RplnsmntParmFreeze", SQL_DATE
        .Bind calLastCountDate, "LastPhysCount", SQL_DATE
        
        .BindComboBox ddnCOSRank, "RankByCOS", SQL_CHAR
        .BindComboBox ddnGrMgnRank, "RankByGrMargin", SQL_CHAR
        .BindComboBox ddnHitsRank, "RankByHits", SQL_CHAR
        .BindComboBox ddnQtySoldRank, "RankByQtySold", SQL_CHAR
        
        .BindComboBox ddnPickMethod, "PickMethod", SQL_SMALLINT, kDmUseItemData
        .Bind chkShowAvailLots, "ShowAvailLots", SQL_SMALLINT
        .Bind chkSerialNoLinesOnPickList, "SerialNoLinesOnPickList", SQL_SMALLINT
        .Bind chkPickOneLotOnly, "PickOneLotOnly", SQL_SMALLINT
        .Bind chkPickPrefBinsFirst, "PickPrefBinsFirst", SQL_SMALLINT
        .Bind chkUseDistBinsInMfg, "UseDistPrefBinInMfg", SQL_SMALLINT
        
         'This is set to nothing because this will be set based on selected warehouse
        .Bind Nothing, "RplnsmntLevel", SQL_SMALLINT
        
        .Bind crnStdUnitCost, "StdUnitCost", SQL_DECIMAL
        .Bind crnRplcmntCost, "RplcmntUnitCost", SQL_DECIMAL
        .Bind crnLandedCost, "LandedUnitCost", SQL_DECIMAL
        .Bind crnLastCost, "LastUnitCost", SQL_DECIMAL
        .Bind crnAvgUnitCost, "AvgUnitCost", SQL_DECIMAL
        .Bind nbrDemand, "Demand", SQL_DECIMAL
        .Bind txtLeadTime, "LeadTime", SQL_SMALLINT
        .Bind nbrSafetyStockQty, "SafetyStockQty", SQL_DECIMAL
        .BindComboBox ddnStatus, "Status", SQL_SMALLINT, kDmUseItemData
        .Bind nbrStdOrdQty, "StdOrdQty", SQL_DECIMAL
        
        'New Controls Binding
        .Bind nbrOrdPoint, "OrderPoint", SQL_DECIMAL
        .Bind nbrLinePoint, "LinePoint", SQL_DECIMAL
        
        For miCount = 0 To kArrCount
            .BindLookup glaGLAcct(miCount), kDmSetNull
        Next miCount
                
        .BindComboBox ddnBuyer, "BuyerKey", SQL_INTEGER, kDmUseItemData
        .BindLookup lkuDemand, kDmSetNull
        .BindLookup lkuLeadTime, kDmSetNull
        .BindLookup lkuPPLine, kDmSetNull
        .BindLookup lkuWhseFrom, kDmSetNull
        .BindLookup lkuSafetyStock, kDmSetNull
        
        'The primary vendor lookup cannot be bound as it may display a vendor from another location
        'that is not saved to timInventory.  The lookup and is handled manually in code according to the
        'business rules.
        .Bind Nothing, "PrimaryVendKey", SQL_INTEGER, kDmSetNull
        
        .BindLookup lkuItem
        .BindLookup lkuWhse
        .SaveOrder = 1
                
        .Init
    End With
    
    Set moDmGridPrefBinDist = New clsDmGrid
    
    With moDmGridPrefBinDist
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdPrefBinDist
        
        .AppName = Me.Caption
        .Table = "timInvtBinList"
        .OrderBy = "PrefNo"
        .Where = "PrefBinType = " & PrefBinTypes.Distribution
        
        .UniqueKey = " WhseKey, ItemKey, PrefNo, PrefBinType"
        .BindColumn "PrefNo", Nothing, SQL_SMALLINT
        .BindColumn "WhseBinKey", kColWhseBinKeyDist, SQL_INTEGER
        .BindColumn "PrefBinType", kColPrefBinTypeDist, SQL_SMALLINT
                
        Set .Parent = moDmForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER
        .ParentLink "WhseKey", "WhseKey", SQL_INTEGER
        .SaveOrder = 2
        .LinkSource "timWhseBin", "WhseBinKey = <<WhseBinKey>>"
        .Link kColBinIDDist, "WhseBinID"
        .Link kColOldWhseBinIDDist, "WhseBinID"
        .Link kColBinLocationDist, "Location"
        
        .Init
    End With
    
    Set moDmGridPrefBinMfg = New clsDmGrid
    
    With moDmGridPrefBinMfg
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdPrefBinMfg
        
        .AppName = Me.Caption
        .Table = "timInvtBinList"
        .OrderBy = "PrefNo"
        .Where = "PrefBinType = " & PrefBinTypes.Manufacturing
        
        .UniqueKey = " WhseKey, ItemKey, PrefNo, PrefBinType"
        .BindColumn "PrefNo", Nothing, SQL_SMALLINT
        .BindColumn "WhseBinKey", kColWhseBinKeyMfg, SQL_INTEGER
        .BindColumn "PrefBinType", kcolPrefBinTypeMfg, SQL_SMALLINT
        
        Set .Parent = moDmForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER
        .ParentLink "WhseKey", "WhseKey", SQL_INTEGER
        .SaveOrder = 3
        .LinkSource "timWhseBin", "WhseBinKey = <<WhseBinKey>>"
        .Link kColBinIDMfg, "WhseBinID"
        .Link kColOldWhseBinIDMfg, "WhseBinID"
        .Link kColBinLocationMfg, "Location"
        
        .Init
    End With

    '================================================================================================
    'SGS DEJ START  =================================================================================
    '================================================================================================
    Set moDMExt = New clsDmForm
        
    With moDMExt
        Set .Form = Me
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
               
        .AppName = Me.Caption
        .Table = "timInventoryExt_SGS"
        .UniqueKey = "ItemKey, WhseKey"
        
        'Setting the validation manager
        Set .ValidationMgr = SOTAVM
        
        .BindComboBox ddnPlaceCode, "PlacardCode", SQL_VARCHAR, kDmSetNull
        .Bind nbrType, "PlacardType", SQL_SMALLINT, kDmSetNull
        
        .Bind Nothing, "Percentage", SQL_DECIMAL, kDmSetNull
        
        .Bind nbrPercentAsphalt, "PercentAsphalt", SQL_DECIMAL, kDmSetNull
        
        .Bind nbrPenetration, "Penetration", SQL_DECIMAL, kDmSetNull
        .Bind nbrPenTemp, "PenetrationTemperature", SQL_DECIMAL, kDmSetNull
        .Bind txtPenRemark, "PenetrationRemark", SQL_VARCHAR, kDmSetNull
        .Bind nbrFlashpoint, "Flash", SQL_DECIMAL, kDmSetNull
        
        .Bind nbrViscosity, "Viscosity", SQL_DECIMAL, kDmSetNull
        .Bind nbrViscTemp, "ViscosityTemperature", SQL_DECIMAL, kDmSetNull
        .Bind txtViscRemark, "ViscosityRemark", SQL_VARCHAR, kDmSetNull
        .Bind nbrLbsGal, "LbsPerGallon", SQL_DECIMAL, kDmSetNull
        
        .Bind nbrGravity, "SpecificGravity", SQL_DECIMAL, kDmSetNull
        
        .Bind txtRemarks, "SpecificationRemark", SQL_VARCHAR, kDmSetNull
        .Bind nbrTemp, "Temperature", SQL_DECIMAL, kDmSetNull
        
        .Bind nbrRollingWAC, "RollingWAC", SQL_DECIMAL, kDmSetNull
        
        .Bind nbrPercentResidue, "PercentResidue", SQL_DECIMAL, kDmSetNull   'RKL DEJ 2017-10-03
        .Bind nbrHeatCapacity, "HeatCapacity", SQL_DECIMAL, kDmSetNull   'RKL DEJ 2017-10-03
        .Bind nbrNormalStorageTemp, "NormalStorageTemp", SQL_DECIMAL, kDmSetNull   'RKL DEJ 2017-10-03
        
        
        Set .Parent = moDmForm
        .ParentLink "ItemKey", "ItemKey", SQL_INTEGER
        .ParentLink "WhseKey", "WhseKey", SQL_INTEGER
        .SaveOrder = 3
        
        .Init
    End With
    '================================================================================================
    'SGS DEJ End    =================================================================================
    '================================================================================================

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bIsBinValid(sWhseBinID As String, tPrefBinType As PrefBinTypes) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sValidBinStr As String
    Dim lWhseBinKey As Long
    Dim oGrid As fpSpread
    
    If tPrefBinType = Distribution Then
        Set oGrid = grdPrefBinDist
    Else
        Set oGrid = grdPrefBinMfg
    End If
        
    bIsBinValid = True

    If Len(Trim$(sWhseBinID)) = 0 Then
        bIsBinValid = False
        Exit Function
    End If

    sValidBinStr = "WhseBinID = " & gsQuoted(sWhseBinID)
    sValidBinStr = sValidBinStr & " AND WhseKey = " & Format$(mlWhseKey)
    sValidBinStr = sValidBinStr & " AND Status = " & kItemStatus_Active
    sValidBinStr = sValidBinStr & " AND Type <> " & kRandomBin
 
    lWhseBinKey = gvCheckNull(moClass.moAppDB.Lookup _
                ("WhseBinKey", "timWhseBin", sValidBinStr), SQL_INTEGER)
        
    If lWhseBinKey = 0 Then
        bIsBinValid = False
    Else
        If bBinAlreadyExists(oGrid.ActiveRow, tPrefBinType) Then
            bIsBinValid = False
        End If
    End If
    
    Set oGrid = Nothing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsBinValid", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Dim rs As Object
    Dim sSQL As String
    Dim iActionCode As Integer
    Dim iConfirmUnload As Integer
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

    iActionCode = kDmFailure
    
    If sKey = kTbFinish Or sKey = kTbFinishExit Or sKey = kTbSave Then
    
        If Me.ActiveControl.Name = grdPrefBinDist.Name Then
            grdPrefBinDist.Tag = "0"
            
            If glGridGetActiveRow(grdPrefBinDist) < glGridGetMaxRows(grdPrefBinDist) Then
                 grdPrefBinDist_Change glGridGetActiveCol(grdPrefBinDist), glGridGetActiveRow(grdPrefBinDist)
                
                If Not mbValidBins Then
                    Exit Sub
                End If
                
            End If

            grdPrefBinDist.Tag = "0"
        ElseIf Me.ActiveControl.Name = grdPrefBinMfg.Name Then
            grdPrefBinMfg.Tag = "0"
            
            If glGridGetActiveRow(grdPrefBinMfg) < glGridGetMaxRows(grdPrefBinMfg) Then
                 grdPrefBinMfg_Change glGridGetActiveCol(grdPrefBinMfg), glGridGetActiveRow(grdPrefBinMfg)
                
                If Not mbValidBins Then
                    Exit Sub
                End If
            End If

            grdPrefBinMfg.Tag = "0"
            
        End If

    End If
        
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            moDmForm.SetDirty True
            mbManualComboSelect = True
            
            If (Me.ActiveControl.Name = "grdPrefBinDist" _
                And Not bIsBinValid(gsGridReadCell(grdPrefBinDist, grdPrefBinDist.ActiveRow, kColBinIDDist), PrefBinTypes.Distribution)) Then
                mbAllowSave = False
                mbMessageDisplayed = False
            ElseIf (Me.ActiveControl.Name = "grdPrefBinMfg" _
                And Not bIsBinValid(gsGridReadCell(grdPrefBinMfg, grdPrefBinMfg.ActiveRow, kColBinIDMfg), PrefBinTypes.Manufacturing)) Then
                mbAllowSave = False
                mbMessageDisplayed = False
            Else
                mbAllowSave = True
            End If
            
            If Not bValidGLAccounts Then
                Exit Sub
            End If
            
            '********************************************************************************
            'SGS DEJ 9/7/12 Validation (START)
            '********************************************************************************
            If Not bIsTempValid Then
                Exit Sub
            End If
            '********************************************************************************
            'SGS DEJ 9/7/12 Validation (STOP)
            '********************************************************************************
            
            iActionCode = moDmForm.Action(kDmFinish)
            mbManualComboSelect = False
            
            If iActionCode = kDmSuccess Then
                lkuWhse.SetFocus
            End If
            
        Case kTbSave
            moDmForm.SetDirty True
            mbManualComboSelect = True
            
            If (Me.ActiveControl.Name = "grdPrefBinDist" _
                And Not bIsBinValid(gsGridReadCell(grdPrefBinDist, grdPrefBinDist.ActiveRow, kColBinIDDist), PrefBinTypes.Distribution)) Then
                mbAllowSave = False
                mbMessageDisplayed = False
            ElseIf (Me.ActiveControl.Name = "grdPrefBinMfg" _
                And Not bIsBinValid(gsGridReadCell(grdPrefBinMfg, grdPrefBinMfg.ActiveRow, kColBinIDMfg), PrefBinTypes.Manufacturing)) Then
                mbAllowSave = False
                mbMessageDisplayed = False
            Else
                mbAllowSave = True
            End If
            
            If Not bValidGLAccounts Then
                Exit Sub
            End If
            
            '********************************************************************************
            'SGS DEJ 9/7/12 Validation (START)
            '********************************************************************************
            If Not bIsTempValid Then
                Exit Sub
            End If
            '********************************************************************************
            'SGS DEJ 9/7/12 Validation (STOP)
            '********************************************************************************
            
            iActionCode = moDmForm.Save(True)
            mbManualComboSelect = False
        
        Case kTbCancel, kTbCancelExit
            moDmForm.SetDirty False
            
            mbManualComboSelect = True
            iActionCode = moDmForm.Action(kDmCancel)
            mbManualComboSelect = False
            
            If lkuWhse.Enabled Then
                lkuWhse.SetFocus
            End If
        
        Case kTbDelete
            'If delete is pressed without any selection then do not process any thing
            sSQL = "select count(*) from timInventory where ItemKey = " & Format$(mlItemKey) & " AND RplnsmntWhseKey = " & lkuWhse.KeyValue & " AND WhseKey <> " & lkuWhse.KeyValue
            Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
            If rs.Field(0) > 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMNOWhseDelete
            Else
                iActionCode = moDmForm.Action(kDmDelete)
            End If
                
            If lkuWhse.Enabled Then
                lkuWhse.SetFocus
            End If

        Case kTbRenameId
            moDmForm.RenameID
            
            If lkuWhse.Enabled Then
                lkuWhse.SetFocus
            End If
            
        Case kTbPrint
            '-- If this program has a related report/listing, here is
            '-- where you would launch it.
            
            'Since the control for nbrCostOfCarry is not bound so check if Carrying Cost % was changed
            'and form is not dirty then mark it dirty.
            If (Val(nbrCostCarryPct.Text) <> Val(nbrCostOfCarry.Text) / 100 _
                And Not moDmForm.IsDirty) Then
                moDmForm.SetDirty True
            End If

            mbManualComboSelect = True
            If moDmForm.ConfirmUnload(True) Then
                gPrintTask moClass.moFramework, kIMInvtList
            End If
           
           mbManualComboSelect = False
        
        Case kTbHelp
            gDisplayFormLevelHelp Me

        Case kTbFirst, kTbNext, kTbPrevious, kTbLast
            SetupNavMain
            lRet = glLookupBrowse(navMain, sKey, miFilter, sNewKey)
            Select Case lRet
                Case Is = MS_SUCCESS
                    If navMain.ReturnColumnValues.Count = 0 Then
                        Exit Sub
                    End If
                    
                    lkuItem.KeyValue = glGetValidLong(navMain.ReturnColumnValues("ItemKey"))
                    lkuWhse.KeyValue = glGetValidLong(navMain.ReturnColumnValues("WhseKey"))
                    
                    bIsValidID
                    
                    moDmForm.SetDirty False

                Case Else
                    gLookupBrowseError lRet, Me, moClass
            End Select
        
        Case kTbNavigate
            navMain_Click
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmForm, moClass
    End Select

    Select Case iActionCode
        Case kDmSuccess
            mbSaveSGS = True 'SGS DEJ 11/13/09

            moClass.lUIActive = kChildObjectInactive
            
            If sKey = kTbFinish Or sKey = kTbCancel Then
                mlItemKey = 0
                mlWhseKey = 0
            End If

        Case kDmFailure, kDmError
            Exit Sub
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CleanForm()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Settings on clean, delete and finish buttons.
    nbrCostOfCarry.Text = ""
    txtItemType.Text = ""
    lblDemandUOM.Caption = ""
    lblSafetyUOM.Caption = ""
    lblMinStockUOM.Caption = ""
    lblMaxStockUOM.Caption = ""
    lblStdOrdUOM.Caption = ""
    lblOrdPointUOM.Caption = ""
    lblLinePointUOM.Caption = ""
    txtOrdCycle.Text = ""
    calEstDate.SQLDate = msBusinessDate
    txtCountTol.Value = 0
    lblItemDesc.Caption = ""
    lblWhseDesc.Caption = ""
    miItemType = 0
    mbIsAssembledKit = False
    mbBTOKit = False
    lkuWhseFrom.Enabled = mbWMIsLicensed
    lkuWhseFrom.EnabledLookup = mbWMIsLicensed
    ddnBuyer.Enabled = True
    lkuPPLine.Enabled = True
    lkuPPLine.EnabledLookup = True
    nbrDemand.Value = 0
    nbrSafetyStockQty.Value = 0
    txtLeadTime.Value = 0
    mbPPLManuallyEntered = False
    lblPrimaryVendorName = ""
    lkuPrimaryVendor.KeyValue = 0
    lkuPrimaryVendor.Text = ""
    chkUseWPPLPrimaryVendor.Value = vbUnchecked
    If Not mbIRIsActivated Then
        ddnReordMeth.Text = "Manual"
    End If

    cmdStdCostBreakout.Enabled = True
    crnRplcmntCost.Enabled = True
    chkSerialNoLinesOnPickList.Enabled = True
    chkPickPrefBinsFirst.Enabled = True
    chkShowAvailLots.Enabled = True
    chkShipComplete.Enabled = True
    chkCloseSOLineFirst.Enabled = True
    chkPickOneLotOnly.Enabled = True
    lkuPrimaryVendor.Enabled = True
    chkUseWPPLPrimaryVendor.Enabled = False             'Since the PPL will be empty, it will always be disbled when the form is cleared.
    mbUseBins = False
    msWarningForPPL = ""
    
    SetDistPreferredBinsFrame
                    
    'SGS DEJ
    nbrPercentage.ListIndex = 0
                    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CleanForm", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub calMinMax_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calMinMax, True
    #End If
'+++ End Customizer Code Push +++
    calMinMax.Tag = calMinMax.Text
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "calMinMax_GotFocus", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub calMinMax_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calMinMax, True
    #End If
'+++ End Customizer Code Push +++
    If Not gbValidLocalDate(calMinMax.Text) Then
        Exit Sub
    End If

    If Len(Trim$(calMinMax.Text)) > 0 Then
        'The Use Min/Max should be greater than the business date
        If CDate(calMinMax.Text) <= moClass.moSysSession.BusinessDate Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInvalidMinMaxDt
            calMinMax.Text = calMinMax.Tag
            
            If Not calMinMax.IsValid Then
                calMinMax.Text = ""
            End If
            
            tabInventory.Tab = kTabRplnmnt
            calMinMax.SetFocus
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "calMinMax_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub chkShipComplete_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++
    If (Len(Trim$(lkuItem.Text)) = 0 _
Or Len(Trim$(lkuWhse.Text)) = 0 _
Or lkuItem.Enabled _
Or lkuWhse.Enabled) Then
        chkShipComplete.Value = 0
    End If
    
    If chkShipComplete.Value = 1 Then
    
        chkCloseSOLineFirst.Value = 0
        chkCloseSOLineFirst.Enabled = False
        
    Else
        chkCloseSOLineFirst.Enabled = True
        
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkShipComplete_Click", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



Private Sub cmdStdCostBreakout_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdStdCostBreakout, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
    'Display the standard cost breakout screen.
    'It will return the sum of the cost breakouts.
    crnStdUnitCost.Amount = frmInvtStdCostBreakout.dEditUnitCostBreakout(moClass, mlItemKey, mlWhseKey, crnStdUnitCost.Amount, miCostDecPlaces)
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdStdCostBreakout_Click", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub crnCostToReplenish_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange crnCostToReplenish, True
    #End If
'+++ End Customizer Code Push +++
    Dim iDecimalPlace As Integer

    iDecimalPlace = InStr(1, crnCostToReplenish.Text, ".")
    If iDecimalPlace > 0 Then
        If Len(Mid(crnCostToReplenish.Text, iDecimalPlace + 1)) > miCRepDecPlaces Then
            crnCostToReplenish.Text = Mid(crnCostToReplenish.Text, 1, iDecimalPlace) + Mid(crnCostToReplenish.Text, iDecimalPlace + 1, miCRepDecPlaces)
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "crnCostToReplenish_Change", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub crnCostToReplenish_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus crnCostToReplenish, True
    #End If
'+++ End Customizer Code Push +++
    crnCostToReplenish.Alignment = EALIGNMENTSTYLE_LEFT

    If Len(Trim$(crnCostToReplenish)) = 0 Then
        Select Case miCRepDecPlaces
            Case Is = 0
                crnCostToReplenish.Text = "0"
            
            Case Is = 1
                crnCostToReplenish.Text = "0.0"
            
            Case Is = 2
                crnCostToReplenish.Text = "0.00"
            
            Case Is = 3
                crnCostToReplenish.Text = "0.000"
        End Select
    End If

    crnCostToReplenish.SetSel 0, Len(crnCostToReplenish.Text)
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "crnCostToReplenish_GotFocus", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub crnCostToReplenish_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress crnCostToReplenish, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    Dim iIntPlaces As Integer

    If KeyAscii < 48 Or KeyAscii > 57 Then
        If KeyAscii = 46 Then
            If InStr(1, crnCostToReplenish.Text, ".") > 0 Then
                KeyAscii = 0
            End If
        Else
            KeyAscii = 0
        End If
    Else
        mbCRepKeyPressed = True
            
        iIntPlaces = InStr(1, crnCostToReplenish.Text, ".")
        If iIntPlaces = 0 Then
            If Len(crnCostToReplenish.Text) = miCRepIntPlaces Then
                KeyAscii = 0
                Beep
            End If
        Else
            If (Len(Left(crnCostToReplenish.Text, iIntPlaces - 1)) = miCRepIntPlaces _
And Len(Mid(crnCostToReplenish.Text, iIntPlaces + 1, miCRepDecPlaces)) = miCRepDecPlaces) Then
                KeyAscii = 0
                Beep
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "crnCostToReplenish_KeyPress", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub crnCostToReplenish_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus crnCostToReplenish, True
    #End If
'+++ End Customizer Code Push +++
    Dim iDecPos  As Integer
    
    crnCostToReplenish.Alignment = EALIGNMENTSTYLE_RIGHT
    
    If Len(crnCostToReplenish.Text) = 0 Then
        mbCRepKeyPressed = False
    End If
    
    If Not mbCRepKeyPressed Then
        crnCostToReplenish.Text = ""
    End If
    
    If crnCostToReplenish.Text <> "" Then
        iDecPos = InStr(1, crnCostToReplenish.Text, ".")
        If iDecPos = 0 Then
            If crnCostToReplenish.Text <> "" Then
                crnCostToReplenish.Text = crnCostToReplenish.Text & ".00"
            End If
        Else
            If Len(Mid(crnCostToReplenish.Text, iDecPos + 1, 2)) <> 2 Then
                 crnCostToReplenish.Text = crnCostToReplenish.Text & "0"
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "crnCostToReplenish_LostFocus", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnCOSRank_AfterRefresh(ByVal CurrentIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnCOSRank.AddItem "X"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnCOSRank_AfterRefresh", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnGrMgnRank_AfterRefresh(ByVal CurrentIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnGrMgnRank.AddItem "X"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnGrMgnRank_AfterRefresh", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnHitsRank_AfterRefresh(ByVal CurrentIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnHitsRank.AddItem "X"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnHitsRank_AfterRefresh", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnPickMethod_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnPickMethod, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If bDenyControlFocus Then
        Exit Sub
    End If
    
    If ddnPickMethod.Enabled = True Then
        SetPickControls
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnPickMethod_Click", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnQtySoldRank_AfterRefresh(ByVal CurrentIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnQtySoldRank.AddItem "X"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnQtySoldRank_AfterRefresh", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ddnReordMeth_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnReordMeth, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    nbrMaxStockQty.Enabled = True
    nbrMinStockQty.Enabled = True
    nbrStdOrdQty.Enabled = True

    If mbIRIsActivated Then
        crnCostToReplenish.Enabled = True
        nbrCostOfCarry.Enabled = True
    End If
    
    If NewIndex = 0 Then
        If PrevIndex <> 0 Then
            nbrStdOrdQty.Value = 0
        End If
        
        nbrStdOrdQty.Enabled = True
    Else
        If PrevIndex = 0 Then
            nbrStdOrdQty.Value = 0
        End If
        
        nbrStdOrdQty.Enabled = False
    End If
    
    If NewIndex = 3 Then
        nbrMinStockQty.Value = 0
        nbrMinStockQty.Enabled = False
    Else
        nbrMinStockQty.Enabled = True
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ddnReordMeth_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ExcludeStatus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnStatus.StaticListExcludeValues = "5"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ExcludeStatus", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbCancelShutDown = False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'****************************************************************
' Description:
'    Form_KeyDown traps the hot keys when the KeyPreview property
'    of the form is set to True.
'****************************************************************
    If Me.ActiveControl Is lkuWhse Then
        mbActiveCtrlWh = True
    Else
        mbActiveCtrlWh = False
    End If
    
    'Rem In case Invalid value entered in any main lookup its needed to change the focus
    If KeyCode = vbKeyF4 Then
        tbrMain.SetFocus
        DoEvents
    End If

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            If KeyCode = vbKeyF4 And Shift = 0 Then
                DoEvents
                
                If moDmForm.ConfirmUnload(True) <> kDmSuccess Then
                    Exit Sub
                End If
            End If
            
            'If F5 is pressed then do not perform validation on G_NavCellChange.
            'The validation will be performed when user selects a bin from search window.
            If (KeyCode = vbKeyF5 And Shift = 0) And _
                ((Me.ActiveControl.Name = grdPrefBinDist.Name And lkuPrefBinDist.Visible) _
                Or (Me.ActiveControl.Name = grdPrefBinMfg.Name And lkuPrefBinMfg.Visible)) Then
                
                mbF5Down = True
                
            End If
            
            gProcessFKeys Me, KeyCode, Shift
    End Select
    
    If KeyCode = vbKeyF3 Then
        LoadDropDowns
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
' Description:
'    Form_KeyPress traps the keys pressed when the KeyPreview property
'    of the form is set to True.
'*********************************************************************
    Select Case KeyAscii
        Case Is = vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
            'Do Nothing
    End Select
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    

    With moClass.moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        msHomeCurrID = .CurrencyID
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        mbIMActivated = .IsModuleActivated(kModuleIM)
        mbIRIsActivated = .IsModuleActivated(kModuleIR)
        mbMFActivated = .IsModuleActivated(kModuleMF)
        mbAMActivated = .IsModuleActivated(kModuleAM)

        If CBool(.IsModuleActivated(kModuleWM)) = True Then
            mbWMIsLicensed = .IsModuleLicensed(kModuleWM)
        Else
            mbWMIsLicensed = False
        End If
    End With

    msBusinessDate = moClass.moSysSession.BusinessDate
    calEstDate.SQLDate = msBusinessDate

    gbSetCurrCtls moClass, msHomeCurrID, muHomeCurrInfo, _
                  crnAvgUnitCost, crnLandedCost, crnLastCost, _
                  crnRplcmntCost, crnStdUnitCost

    'Setup the toolbar
    With tbrMain
        .RemoveButton kTbMemo
        .RemoveButton kTbRenameId
        .RemoveSeparator kTbPrint
    End With
    
    Me.Caption = gsBuildString(kIMMntInvt, moClass.moAppDB, moClass.moSysSession)

    SetLookups
    LoadDropDowns
    
    'Format the grid.
    FormatGrid

    'Bind form fields to database fields.
    BindForm
    BindGM

    'Setup and bind right click context menus to controls.
    BindContextMenu
        
    'Initialize the security level.
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm)
        
    'This part initiates lookups on the GL Tab.
    For miCount = 0 To kArrCount
        glaGLAcct(miCount).Init oClass.moFramework, oClass.moAppDB, oClass.moAppDB, moContextMenu, msCompanyID
    Next miCount

    moGMPrefBinDist.GridSortEnabled = False
    moGMPrefBinMfg.GridSortEnabled = False

    'Initialize browse filter tracking.
    miFilter = RSID_UNFILTERED

    'Used for determining if the Main navigator is open.
    mbInNavigator = False
    
    'Used for determining if a Click event occurred on a combo box
    'because of the user or through code.
    mbManualComboSelect = False
    
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.Status = SOTA_SB_START
    
    'Used for determining if text boxes are being changed by the user
    'or because data Manager is populating them.
    mbDmLoad = False
    
    With moOptions
        Set .oAppDB = moClass.moAppDB
        .sCompanyID = moClass.moSysSession.CompanyId
    End With

    miQtyDecPlaces = moOptions.CI("QtyDecPlaces")
    miCostDecPlaces = moOptions.CI("UnitCostDecPlaces")
    grdPrefBinDist.Tag = "0"
    grdPrefBinMfg.Tag = "0"
    
    'Set Form Fields
    If Not mbIRIsActivated Then
        lkuDemand.Enabled = False
        nbrDemand.Enabled = False
        lkuSafetyStock.Enabled = False
        nbrSafetyStockQty.Enabled = False
        lkuLeadTime.Enabled = False
        calMinMax.Enabled = False
        nbrOrdPoint.Enabled = False
        nbrLinePoint.Enabled = False
        crnCostToReplenish.Enabled = False
        txtOrdCycle.Enabled = False
    End If
    
    lkuWhseFrom.Enabled = mbWMIsLicensed
    
    'Define decimal places for price controls
    crnAvgUnitCost.DecimalPlaces = miCostDecPlaces
    crnLandedCost.DecimalPlaces = miCostDecPlaces
    crnLastCost.DecimalPlaces = miCostDecPlaces
    crnRplcmntCost.DecimalPlaces = miCostDecPlaces
    crnStdUnitCost.DecimalPlaces = miCostDecPlaces
    
    'Define decimal places for quantity controls
    nbrMaxStockQty.DecimalPlaces = miQtyDecPlaces
    nbrMinStockQty.DecimalPlaces = miQtyDecPlaces
    nbrSafetyStockQty.DecimalPlaces = miQtyDecPlaces
    nbrDemand.DecimalPlaces = miQtyDecPlaces
    nbrStdOrdQty.DecimalPlaces = miQtyDecPlaces

    pnlTab(kTabMain).Enabled = True
    pnlTab(kTabRplnmnt).Enabled = False
    pnlTab(kTabGLAcct).Enabled = False
    pnlTab(kTabPrefBin).Enabled = False
    pnlTab(kTabCustomizer).Enabled = False
    
    tabInventory.Tab = kTabMain
    
    ExcludeStatus
    
    miCCPctDecPlaces = 2
    miCCPctIntPlaces = 3
    
    If miCostDecPlaces <= 3 Then
        miCRepDecPlaces = miCostDecPlaces
    Else
        miCRepDecPlaces = 3
    End If

    miCRepIntPlaces = 12
    mbMessageDisplayed = False
    moDmForm.SetDirty False

    If chkShipComplete.Value = 1 Then
        chkCloseSOLineFirst.Value = 0
        chkCloseSOLineFirst.Enabled = False
    Else
        chkCloseSOLineFirst.Enabled = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub glaGLAcct_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    glaGLAcct(Index).Description = gvCheckNull(moClass.moAppDB.Lookup("Description", "tglAccount", "GLAcctNo = " & gsQuoted(glaGLAcct(Index).Text) & " AND CompanyID = " & gsQuoted(msCompanyID)), 1)
    
    If Not Me.ActiveControl Is sbrMain And Not Me.ActiveControl Is tbrMain Then
        bIsValidGLFinancialAcct (Index)
    End If


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "glaGLAcct_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub glaGLAcct_LookupClick(Index As Integer, bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "glaGLAcct_LookupClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    grdPrefBinDist.ActiveCellHighlightStyle = ActiveCellHighlightStyleNormal
  
    If grdPrefBinDist.ActiveCol = kColBinIDDist And grdPrefBinDist.ActiveRow <> 0 And lkuPrefBinDist.Visible = False Then
        lkuPrefBinDist.Visible = True
    End If
    
    moGMPrefBinDist.Grid_Click 1, 1

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuDemand_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuDemand, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuDemand.Text)) = 0 Then
        If mbIRIsActivated Then nbrDemand.Enabled = True
    Else
        nbrDemand.Value = 0
        nbrDemand.Enabled = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuDemand_Change", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuDemand_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuDemand, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuDemand.Text)) = 0 Then
        If mbIRIsActivated Then nbrDemand.Enabled = True
    Else
        nbrDemand.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuDemand_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuDemand_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuDemand, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuDemand_LookupClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuItem_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuItem, True
    #End If
'+++ End Customizer Code Push +++
    lkuItem.Tag = lkuItem.Text
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItem_GotFocus", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuLeadTime_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuLeadTime, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuLeadTime.Text)) = 0 Then
        txtLeadTime.Enabled = True
        vsLeadTime.Enabled = True
    Else
        txtLeadTime.Value = 0
        txtLeadTime.Enabled = False
        vsLeadTime.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuLeadTime_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuLeadTime_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuLeadTime, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuLeadTime.Text)) = 0 Then
        txtLeadTime.Enabled = True
        vsLeadTime.Enabled = True
    Else
        txtLeadTime.Enabled = False
        vsLeadTime.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuLeadTime_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuLeadTime_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuLeadTime, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuLeadTime_LookupClick", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPPLine_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPPLine, True
    #End If
'+++ End Customizer Code Push +++
    mbPPLManuallyEntered = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPPLine_GotFocus", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPPLine_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPPLine, True
    #End If
'+++ End Customizer Code Push +++
    
    
    If Len(Trim$(lkuPPLine.Text)) = 0 Then
        lkuPPLine.ClearData
        lkuWhseFrom.Enabled = True
        lkuWhseFrom.EnabledLookup = True
        If Len(Trim$(lkuWhseFrom.Text)) = 0 Then
            lkuWhseFrom.ClearData

        End If
        txtOrdCycle.Text = ""
        mlPrevPPLKey = 0
    Else
        lkuWhseFrom.ClearData
        lkuWhseFrom.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPPLine_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPPLine_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPPLine, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPPLine_LookupClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPPLine_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If (lkuPPLine.KeyValue = Empty _
And Len(Trim$(lkuPPLine.Text)) <> 0) Then
        Cancel = True
        lkuPPLine.KeyValue = mlPrevPPLKey
        Beep
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPPLine_Validate", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuSafetyStock_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuSafetyStock, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuSafetyStock.Text)) = 0 Then
        If mbIRIsActivated Then nbrSafetyStockQty.Enabled = True
    Else
         nbrSafetyStockQty.Value = 0
         nbrSafetyStockQty.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuSafetyStock_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuSafetyStock_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSafetyStock, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuSafetyStock.Text)) = 0 Then
        If mbIRIsActivated Then nbrSafetyStockQty.Enabled = True
    Else
        nbrSafetyStockQty.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuSafetyStock_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuSafetyStock_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuSafetyStock, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bCancel = bDenyControlFocus
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuSafetyStock_LookupClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhse_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhse, True
    #End If
'+++ End Customizer Code Push +++
    lkuWhse.Tag = lkuWhse.Text
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhse_GotFocus", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhseFrom_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuWhseFrom, True
    #End If
'+++ End Customizer Code Push +++
    lkuWhseFrom.Tag = lkuWhseFrom.Text
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhseFrom_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMAfterInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'If user went into the standard cost breakout form and saved the changes,
    'the the dirty flag would be true.  If true, call the save method so it
    'can update the breakout cost in timInvtStdUnitCost.
    'If false, the insert trigger of timInventory would have created the record
    'with the entire standard cost in the material bucket.
    If frmInvtStdCostBreakout.bIsDirty = True Then
        If Not frmInvtStdCostBreakout.bSaveUnitCostBreakout(moClass, glGetValidLong(mlItemKey), glGetValidLong(mlWhseKey), gdGetValidDbl(crnStdUnitCost.Amount)) Then
            bValid = False
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMAfterInsert", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMAfterUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Call the save method so it can update the breakout cost in timInvtStdUnitCost.
    If Not frmInvtStdCostBreakout.bSaveUnitCostBreakout(moClass, glGetValidLong(mlItemKey), glGetValidLong(mlWhseKey), gdGetValidDbl(crnStdUnitCost.Amount)) Then
        bValid = False
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMAfterUpdate", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmForm.SetColumnValue "CreateType", 1
    
    'Validate and set the primary vendor.
    bValid = bPrimaryVendorSaveProcessing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMBeforeInsert", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnStatus.Tag = ddnStatus.ItemData
    crnRplcmntCost.Tag = crnRplcmntCost.Amount
    ddnPickMethod.Tag = ddnPickMethod.List
    
    '====================================================================================================
    'SGS DEJ Start  =====================================================================================
    '====================================================================================================
    Select Case moDMExt.GetColumnValue("Percentage")
        Case 0.0025
            nbrPercentage.ListIndex = 5
        Case 0.003
            nbrPercentage.ListIndex = 4
        Case 0.005
            nbrPercentage.ListIndex = 3
        Case 0.0075
            nbrPercentage.ListIndex = 2
        Case 0.01
            nbrPercentage.ListIndex = 1
        Case Else
            nbrPercentage.ListIndex = 0
    End Select
    
    '====================================================================================================
    'SGS DEJ End    =====================================================================================
    '====================================================================================================
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMReposition", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMValidate(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = True
    bValid = bValidSourceWarehouse
    
    'Validate the Carrying Cost Percentage
    'The cost of carrying should be between 0 and 100
    If Val(nbrCostOfCarry.Text) < 0 Or Val(nbrCostOfCarry.Text) > 100 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMInvalidCaryCost
        
        nbrCostOfCarry.Text = nbrCostOfCarry.Tag
        tabInventory.Tab = kTabRplnmnt
        nbrCostOfCarry.SetFocus
        
        bValid = False
        
         Exit Sub
    End If
    
    If Not bValidMinMaxStock Then
        bValid = False
        tabInventory.Tab = kTabRplnmnt
        
        If Me.ActiveControl Is nbrMaxStockQty Then
            nbrMaxStockQty.SetFocus
        Else
            nbrMinStockQty.SetFocus
        End If

        Exit Sub
    End If
    
    If Len(Trim$(calMinMax.Text)) > 0 Then
        'The Use Min/Max should be greater than the business date
        If CDate(calMinMax.Text) <= moClass.moSysSession.BusinessDate Then
            giSotaMsgBox Me, moClass.moSysSession, kIMInvalidMinMaxDt
            
            bValid = False
            
            calMinMax.Text = calMinMax.Tag
                
            If Not calMinMax.IsValid Then
                 calMinMax.Text = ""
            End If
                
            tabInventory.Tab = kTabRplnmnt
            calMinMax.SetFocus
            
            Exit Sub
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMValidate", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navMain_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iActionCode As Integer
    Dim colRetVals As Collection


    SetupNavMain
    
    'Launch the Navigator.
    mbF4Pressed = True
    DoEvents
    mbF4Pressed = False

    Set colRetVals = gcLookupClick(Me, navMain, lkuItem, "ItemID")
    

    SetHourglass True
    
    If colRetVals.Count <> 0 Then
        lkuItem.KeyValue = colRetVals("ItemKey")
        lkuWhse.KeyValue = colRetVals("WhseKey")
        
        bIsValidID
    
        lkuItem.Enabled = False
        lkuWhse.Enabled = False
    
        tabInventory.SetFocus
    End If
    
    SetHourglass False


    moDmForm.SetDirty False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "navMain_Click", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub PopulateGLAccts()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As Object
    Dim rs1 As Object
    Dim sSQL As String
    
    sSQL = "SELECT timItemClass.PurchAcctKey, timItemClass.SalesAcctKey" _
            & ", timItemClass.SalesOffsetAcctKey, timItemClass.SalesReturnAcctKey" _
            & ", timItemClass.InvtAcctKey, timItemClass.COSAcctKey" _
            & ", timItemClass.IssueAcctKey, timItemClass.MiscAdjAcctKey" _
            & ", timItemClass.CostTierAdjAcctKey, timItemClass.PhysCountAcctKey" _
            & " FROM timItemClass, timItem WHERE timItemClass.ItemClassKey = timItem.ItemClassKey AND timItem.ItemKey = " & Format$(mlItemKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If Not rs Is Nothing Then
        If rs.RecordCount > 0 Then
            sSQL = "SELECT PurchAcctKey, SalesAcctKey" _
                    & ", SalesOffsetAcctKey, SalesReturnAcctKey" _
                    & ", InvtAcctKey, COSAcctKey" _
                    & ", IssueAcctKey, MiscAdjAcctKey" _
                    & ", CostTierAdjAcctKey, PhysCountAcctKey" _
                    & " FROM timWarehouse WHERE WhseKey = " & Format$(mlWhseKey)
            Set rs1 = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
        
            If rs.LongField("PurchAcctKey") = 0 Then
                If rs1.LongField("PurchAcctKey") > 0 Then
                    glaGLAcct(kGLPurchAcct).KeyValue = rs1.LongField("PurchAcctKey")
                End If
            Else
                glaGLAcct(kGLPurchAcct).KeyValue = rs.LongField("PurchAcctKey")
            End If
        
            If rs.LongField("SalesAcctKey") = 0 Then
                If rs1.LongField("SalesAcctKey") > 0 Then
                    glaGLAcct(kGLSalesAcct).KeyValue = rs1.LongField("SalesAcctKey")
                End If
            Else
                glaGLAcct(kGLSalesAcct).KeyValue = rs.LongField("SalesAcctKey")
            End If
    
            If rs.LongField("SalesOffsetAcctKey") = 0 Then
                If rs1.LongField("SalesOffsetAcctKey") > 0 Then
                    glaGLAcct(kGLSalesOffsetAcct).KeyValue = rs1.LongField("SalesOffsetAcctKey")
                End If
            Else
                glaGLAcct(kGLSalesOffsetAcct).KeyValue = rs.LongField("SalesOffsetAcctKey")
            End If
    
            If rs.LongField("SalesReturnAcctKey") = 0 Then
                If rs1.LongField("SalesReturnAcctKey") > 0 Then
                    glaGLAcct(kGLSalesReturnAcct).KeyValue = rs1.LongField("SalesReturnAcctKey")
                End If
            Else
                glaGLAcct(kGLSalesReturnAcct).KeyValue = rs.LongField("SalesReturnAcctKey")
            End If
        
            If rs.LongField("InvtAcctKey") = 0 Then
                If rs1.LongField("InvtAcctKey") > 0 Then
                    glaGLAcct(kGLInvtAcct).KeyValue = rs1.LongField("InvtAcctKey")
                End If
            Else
                glaGLAcct(kGLInvtAcct).KeyValue = rs.LongField("InvtAcctKey")
            End If
        
            If rs.LongField("COSAcctKey") = 0 Then
                If rs1.LongField("COSAcctKey") > 0 Then
                    glaGLAcct(kGLCOSAcct).KeyValue = rs1.LongField("COSAcctKey")
                End If
            Else
                glaGLAcct(kGLCOSAcct).KeyValue = rs.LongField("COSAcctKey")
            End If
        
            If rs.LongField("IssueAcctKey") = 0 Then
                If rs1.LongField("IssueAcctKey") > 0 Then
                    glaGLAcct(kGLIssueAcct).KeyValue = rs1.LongField("IssueAcctKey")
                End If
            Else
                glaGLAcct(kGLIssueAcct).KeyValue = rs.LongField("IssueAcctKey")
            End If
        
            If rs.LongField("MiscAdjAcctKey") = 0 Then
                If rs1.LongField("MiscAdjAcctKey") > 0 Then
                    glaGLAcct(kGLMiscAdjAcct).KeyValue = rs1.LongField("MiscAdjAcctKey")
                End If
            Else
                glaGLAcct(kGLMiscAdjAcct).KeyValue = rs.LongField("MiscAdjAcctKey")
            End If
        
            If rs.LongField("CostTierAdjAcctKey") = 0 Then
                If rs1.LongField("CostTierAdjAcctKey") > 0 Then
                    glaGLAcct(kGLCostTierAdjAcct).KeyValue = rs1.LongField("CostTierAdjAcctKey")
                End If
            Else
                glaGLAcct(kGLCostTierAdjAcct).KeyValue = rs.LongField("CostTierAdjAcctKey")
            End If
        
            If rs.LongField("PhysCountAcctKey") = 0 Then
                If rs1.LongField("PhysCountAcctKey") > 0 Then
                    glaGLAcct(kGLPhysCountAcct).KeyValue = rs1.LongField("PhysCountAcctKey")
                End If
            Else
                glaGLAcct(kGLPhysCountAcct).KeyValue = rs.LongField("PhysCountAcctKey")
            End If
        End If
        
        rs.Close
        Set rs = Nothing
    
        If Not rs1 Is Nothing Then
            rs1.Close
            Set rs1 = Nothing
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PopulateGLAccts", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub LoadDropDowns()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ddnStatus.InitStaticList moClass.moAppDB, "timInventory", "Status", mlLanguage
    ddnPhysCycle.InitDynamicList moClass.moAppDB, "select tciProcCycle.ProcCycleID, tciProcCycle.ProcCycleKey from tciProcCycle", msCompanyID
    ddnCOSRank.InitDynamicList moClass.moAppDB, "select timInvtRank.RankID from timInvtRank where timInvtRank.COSPct > 0", msCompanyID
    ddnGrMgnRank.InitDynamicList moClass.moAppDB, "select timInvtRank.RankID from timInvtRank where timInvtRank.GrossMarginPct > 0", msCompanyID
    ddnQtySoldRank.InitDynamicList moClass.moAppDB, "select timInvtRank.RankID from timInvtRank where timInvtRank.QtySoldPct > 0", msCompanyID
    ddnHitsRank.InitDynamicList moClass.moAppDB, "select timInvtRank.RankID from timInvtRank where timInvtRank.HitsPct > 0", msCompanyID
    ddnReordMeth.InitStaticList moClass.moAppDB, "timInventory", "ReordMeth", mlLanguage
    ddnPickMethod.InitStaticList moClass.moAppDB, "timInventory", "PickMethod", mlLanguage
    
    '======================================================================================
    'SGS DEJ Start  =======================================================================
    '======================================================================================
    'Place Codes
    ddnPlaceCode.AddItem "", 0
    ddnPlaceCode.AddItem "1 - Elevated Temperature Material, Liquid, N.O.S. 9. UN 3257. III", 1
    ddnPlaceCode.AddItem "2 - Elevated Temperature Material, Liquid, N.O.S. 9. NA 9259. III", 2
    ddnPlaceCode.AddItem "3 - Hot Asphalt, Combustible Liquid, NA 1999, III", 3
    ddnPlaceCode.AddItem "4 - COMBUSTIBLE LIQUID NA 1993 PGIII", 4
    ddnPlaceCode.AddItem "5 - Hot Asphalt, 3, NA 1999, III, Flammable", 5
    ddnPlaceCode.AddItem "6 - HOT TARS, LIQUID, 3 NA 1999, III, PLACARDED: FLAMMABLE", 6
    ddnPlaceCode.AddItem "7 - HOT TARS, LIQUID, 3 NA 1999, III", 7
    ddnPlaceCode.AddItem "8 - Not Requlated", 8
    ddnPlaceCode.AddItem "9 - Combustible Liquid, N.O.S., Combustible, NA 1993, III", 9
    ddnPlaceCode.AddItem "10 - Toxic Liquids, Organic, n.o.s., (Fatty Amines),6.1, UN2810, III", 10
    
    'Additive %
    nbrPercentage.AddItem "", 0
    nbrPercentage.AddItem "0.01", 1
    nbrPercentage.AddItem "0.0075", 2
    nbrPercentage.AddItem "0.005", 3
    nbrPercentage.AddItem "0.003", 4
    nbrPercentage.AddItem "0.0025", 5
    
    '======================================================================================
    'SGS DEJ End    =======================================================================
    '======================================================================================
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadDropDowns", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetLookups()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sNavRestrict As String

    'Setting up the Item look up.
    Set lkuItem.Framework = moClass.moFramework
    Set lkuItem.SysDB = moClass.moAppDB
    Set lkuItem.AppDatabase = moClass.moAppDB
    
    lkuItem.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & _
    " AND ItemType >= " & kFinishedGood & _
    " AND ItemType < " & kCatalog
    
    'Setting up the warehouse look up.
    Set lkuWhse.Framework = moClass.moFramework
    Set lkuWhse.SysDB = moClass.moAppDB
    Set lkuWhse.AppDatabase = moClass.moAppDB
    
    lkuWhse.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " AND Transit = 0"
        
    'Setting up the demand formula lookup
    Set lkuDemand.Framework = moClass.moFramework
    Set lkuDemand.SysDB = moClass.moAppDB
    Set lkuDemand.AppDatabase = moClass.moAppDB
    
    lkuDemand.RestrictClause = "timDemandCalc.CompanyID = " & gsQuoted(msCompanyID)
   
    'Setting up the Safety Stock formula look up
    Set lkuSafetyStock.Framework = moClass.moFramework
    Set lkuSafetyStock.SysDB = moClass.moAppDB
    Set lkuSafetyStock.AppDatabase = moClass.moAppDB
    
    lkuSafetyStock.RestrictClause = "timSafetyStockCalc.CompanyID = " & gsQuoted(msCompanyID)
   
    'Setting up the Lead Time formula look up
    Set lkuLeadTime.Framework = moClass.moFramework
    Set lkuLeadTime.SysDB = moClass.moAppDB
    Set lkuLeadTime.AppDatabase = moClass.moAppDB
    
    lkuLeadTime.RestrictClause = "timLeadTimeCalc.CompanyID = " & gsQuoted(msCompanyID)

    'Setting up the PPLine look up
    Set lkuPPLine.Framework = moClass.moFramework
    Set lkuPPLine.SysDB = moClass.moAppDB
    Set lkuPPLine.AppDatabase = moClass.moAppDB

    lkuPPLine.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    'Setting up the PrimaryVendor look up
    Set lkuPrimaryVendor.Framework = moClass.moFramework
    Set lkuPrimaryVendor.SysDB = moClass.moAppDB
    Set lkuPrimaryVendor.AppDatabase = moClass.moAppDB

    lkuPrimaryVendor.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)

    'Setting up the Source warehouse look up
    Set lkuWhseFrom.Framework = moClass.moFramework
    Set lkuWhseFrom.SysDB = moClass.moAppDB
    Set lkuWhseFrom.AppDatabase = moClass.moAppDB

    ddnBuyer.InitDynamicList moClass.moAppDB, ddnBuyer.SQLStatement, msCompanyID
    
    'Use bin lookups without preferred bin field in the lookup.  They have no context for
    'identifying the preferred bin itself.
    gbLookupInit lkuPrefBinDist, moClass, moClass.moAppDB, "Bin"
    gbLookupInit lkuPrefBinMfg, moClass, moClass.moAppDB, "Bin"
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetLookups", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iConfirmUnload As Integer

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
   
    'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
        'Since the control for nbrCostOfCarry is not bound so check if Carrying Cost % was changed
        'and form is not dirty then mark it dirty.
         If Val(nbrCostCarryPct.Text) <> Val(nbrCostOfCarry.Text) / 100 And Not moDmForm.IsDirty Then
             moDmForm.SetDirty True
         End If
        
        'If the form is dirty, cancel the form close.
        'If the form is dirty, prompt the user to save the record.
        mbManualComboSelect = True
        iConfirmUnload = moDmForm.ConfirmUnload()
        mbManualComboSelect = False
        
        Select Case iConfirmUnload
            Case Is = kDmSuccess
                'Do Nothing
                
            Case Is = kDmFailure
                GoTo CancelShutDown
                
            Case Is = kDmError
                GoTo CancelShutDown
                
            Case Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
        End Select
      
        'Check all other forms that may have been loaded from this main form.
        'If there are any Visible forms, then this means the form is Active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then
            GoTo CancelShutDown
        End If
    
        Select Case UnloadMode
            Case Is = vbFormCode
                'Do Nothing
            
            Case Else
                'Most likely the user has requested to shut down the form.
                'If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlRunMode
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case Is = kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not moGMPrefBinMfg Is Nothing Then
        moGMPrefBinMfg.UnloadSelf
        Set moGMPrefBinMfg = Nothing
    End If

    If Not moGMPrefBinDist Is Nothing Then
        moGMPrefBinDist.UnloadSelf
        Set moGMPrefBinDist = Nothing
    End If

    '==================================================================================================
    'SGS DEJ Start  ===================================================================================
    '==================================================================================================
    If Not moDMExt Is Nothing Then
        moDMExt.UnloadSelf
        Set moDMExt = Nothing
    End If
    '==================================================================================================
    'SGS DEJ End    ===================================================================================
    '==================================================================================================
    
    If Not moDmForm Is Nothing Then
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If
    
    TerminateControls Me

    If Not moDmGridPrefBinMfg Is Nothing Then
        moDmGridPrefBinMfg.UnloadSelf
        Set moDmGridPrefBinMfg = Nothing
    End If

    If Not moDmGridPrefBinDist Is Nothing Then
        moDmGridPrefBinDist.UnloadSelf
        Set moDmGridPrefBinDist = Nothing
    End If

    If Not moGMPrefBinDist Is Nothing Then
        moGMPrefBinDist.UnloadSelf
        Set moGMPrefBinDist = Nothing
    End If
    
    If Not moDDRItem Is Nothing Then
        Set moDDRItem = Nothing
    End If
    
    If Not moDDRWhse Is Nothing Then
        Set moDDRWhse = Nothing
    End If
        
    If Not moOptions Is Nothing Then
        Set moOptions = Nothing
    End If
    
    'If this form loads any other modal objects
    Set moSotaObjects = Nothing
    Set moMapSrch = Nothing
    Set moContextMenu = Nothing

    gUnloadChildForms Me
         
    'Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Indicating change to the Navigator
    If Col = kColBinIDDist Then
        moGMPrefBinDist_CellChange Row, Col
        grdPrefBinDist.Tag = "0"
        Exit Sub
    End If

    If mbAllowSave Then
        moGMPrefBinDist.Grid_Change Col, Row
    End If

    mbValidBins = True
    grdPrefBinDist.Tag = "0"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMPrefBinDist_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sValidBinStr As String
    Dim lWhseBinKey As Long
    Dim sWhseBinID As String
    Dim sOldWhseBinID As String
    Dim bInvalidBin As Boolean
    
    mbAllowSave = True

    If lCol <> kColBinIDDist Then
        Exit Sub
    End If

    If Me.ActiveControl.Name = lkuPrefBinDist.Name Then
        Exit Sub
    End If
    
    'We've got the WhseBinID, now lookup the WhseBinKey.
    sWhseBinID = gsGridReadCell(grdPrefBinDist, lRow, lCol)
    
    sValidBinStr = "WhseBinID = " & gsQuoted(sWhseBinID)
    sValidBinStr = sValidBinStr & " AND WhseKey = " & Format$(mlWhseKey)
    sValidBinStr = sValidBinStr & " AND Status = " & kItemStatus_Active
    sValidBinStr = sValidBinStr & " AND Type <> " & kRandomBin
    sValidBinStr = sValidBinStr & " AND TempBin <> " & 1
    
    lWhseBinKey = gvCheckNull(moClass.moAppDB.Lookup _
            ("WhseBinKey", "timWhseBin", sValidBinStr), SQL_INTEGER)
        
    If (lWhseBinKey = 0 _
        Or bBinAlreadyExists(lRow, PrefBinTypes.Distribution)) Then
        mbAllowSave = False
        moDmGridPrefBinDist.SetColumnValue lRow, "WhseBinKey", 0
        sOldWhseBinID = gsGridReadCell(grdPrefBinDist, lRow, kColOldWhseBinIDDist)
            
        If grdPrefBinDist.Tag <> "1" Then
            If Not mbMessageDisplayed Then
                grdPrefBinDist.Tag = "1"
                    
                If bBinAlreadyExists(grdPrefBinDist.ActiveRow, PrefBinTypes.Distribution) Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMBinAlreadySelected
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidWhseBin
                    bInvalidBin = True
                End If
            End If
        Else
            grdPrefBinDist.Tag = "0"
        End If
            
        mbMessageDisplayed = False
        If grdPrefBinDist.Enabled Then ' If focus is still on this tab stay in this cell, otherwise you can't.
            gGridSetActiveCell grdPrefBinDist, lRow, lCol
        End If
        mbValidBins = False
            
        If Len(Trim$(sOldWhseBinID)) = 0 Then
            If lRow = grdPrefBinDist.MaxRows - 1 Then
                moDmGridPrefBinDist.DeleteBlock lRow
            End If
        Else
            gGridUpdateCellText grdPrefBinDist, lRow, kColBinIDDist, sOldWhseBinID
            
            sValidBinStr = "WhseBinID = " & gsQuoted(sOldWhseBinID)
            sValidBinStr = sValidBinStr & " AND WhseKey = " & Format$(mlWhseKey)
            sValidBinStr = sValidBinStr & " AND Status = " & kItemStatus_Active
            sValidBinStr = sValidBinStr & " AND Type <> " & kRandomBin
                
            lWhseBinKey = gvCheckNull(moClass.moAppDB.Lookup _
                    ("WhseBinKey", "timWhseBin", sValidBinStr), SQL_INTEGER)
                
            If lWhseBinKey <> 0 Then
                moDmGridPrefBinDist.SetColumnValue lRow, "WhseBinKey", lWhseBinKey
                
                If chkPickPrefBinsFirst.Enabled = False Then
                    chkPickPrefBinsFirst.Enabled = True
                End If
            End If
        End If
    Else
        gGridUpdateCell grdPrefBinDist, lRow, kColOldWhseBinIDDist, sWhseBinID
        moDmGridPrefBinDist.SetColumnValue lRow, "WhseBinKey", lWhseBinKey
        sValidBinStr = gvCheckNull(moClass.moAppDB.Lookup("Location", "timWhseBin", "WhseBinKey = " & lWhseBinKey), 1)
        gGridUpdateCell grdPrefBinDist, lRow, kColBinLocationDist, sValidBinStr
        gGridUpdateCell grdPrefBinDist, lRow, kColPrefBinTypeDist, PrefBinTypes.Distribution
        mbValidBins = True
        
        If chkPickPrefBinsFirst.Enabled = False Then
            chkPickPrefBinsFirst.Enabled = True
        End If
    End If
        
    lkuPrefBinDist.Visible = False
        
    If bInvalidBin And grdPrefBinDist.Enabled Then
        gGridSetActiveCell grdPrefBinDist, lRow, lCol
        lkuPrefBinDist.Visible = True
        lkuPrefBinDist.SetFocus
        grdPrefBinDist.SetFocus
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMPrefBinDist_CellChange", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinDist.Grid_Click Col, Row

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinDist.Grid_ColWidthChange Col1

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_ColWidthChange", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Give a call to moGridNavPrefBinDist's key down use full in f5 key for navigator
    If grdPrefBinDist.ActiveCol = kColBinLocationDist Then
        Exit Sub
    End If
    
    moGMPrefBinDist.Grid_KeyDown KeyCode, Shift
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_KeyDown", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iBinCount   As Long
    
    If Me.ActiveControl.Name = lkuPrefBinDist.Name Then
        Exit Sub
    End If
    
    moGMPrefBinDist.Grid_LeaveCell Col, Row, NewCol, NewRow

        If Not mbValidBins Then
            Me.tabInventory.Tab = 3
            gGridSetActiveCell grdPrefBinDist, Row, Col

            'if the user shuts down the form while in grid this will prevent error 5
            If grdPrefBinDist.Enabled Then
                grdPrefBinDist.SetFocus
            End If
            mbValidBins = True
        End If
                
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_LeaveCell", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinDist.Grid_LeaveRow Row, NewRow
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_LeaveRow", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdPrefBinDist_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGMPrefBinDist.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdPrefBinDist_TopLeftChange", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub DispItemDefaults()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'This part will display the Item Type
    Dim rs As Object
    Dim rs1 As Object
    Dim rs2 As Object
    Dim sSQL As String
    Dim lPPLKey As Long
        
    sSQL = "SELECT Status, StdUnitCost, PurchProdLineKey, StockUnitMeasKey"
    sSQL = sSQL & " FROM timItem WHERE ItemKey = " & Format$(mlItemKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If Not rs Is Nothing Then
        If rs.RecordCount > 0 Then
            If rs.IntField("Status") > 0 Then
                ddnStatus.ListIndex = rs.IntField("Status") - 1
                ddnStatus.Tag = ddnStatus.ItemData
            End If
            
            crnStdUnitCost.Amount = rs.Field("StdUnitCost")
            
            mlPrevPPLKey = 0
            mlPPLKey = 0

            If rs.LongField("PurchProdLineKey") > 0 Then
                mlPPLKey = rs.LongField("PurchProdLineKey")

                sSQL = " SELECT PurchProdLineKey FROM timWhsePurchProdLn"
                sSQL = sSQL & " WHERE PurchProdLineKey = " & Format$(mlPPLKey)
                sSQL = sSQL & " AND WhseKey = " & Format$(mlWhseKey)
                Set rs2 = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
                
                If Not rs2 Is Nothing Then
                    If rs2.RecordCount > 0 Then
                        If rs2.LongField("PurchProdLineKey") > 0 Then
                            lkuPPLine.KeyValue = rs2.LongField("PurchProdLineKey")
                        End If
                    End If
                    
                    rs2.Close
                    Set rs2 = Nothing
                End If
                
                If lkuPPLine.IsValid Then
                    lkuWhseFrom.Enabled = mbWMIsLicensed 'Or mbIsAssembledKit Or mbBTOKit
                    
                    Call DisplayPPLDefaults(True)
                Else
                    lkuPPLine.ClearData
                End If
            End If
            
            If rs.LongField("StockUnitMeasKey") > 0 Then
                mlUOMKey = rs.LongField("StockUnitMeasKey")
            Else
                mlUOMKey = 0
            End If
        End If
        
        rs.Close
        Set rs = Nothing
    End If
    
    lblSafetyUOM.Caption = gvCheckNull(moClass.moAppDB.Lookup _
        ("UnitMeasID", "tciUnitMeasure", "UnitMeasKey = " & mlUOMKey), SQL_INTEGER)

    If Len(Trim$(lblSafetyUOM.Caption)) > 0 Then
        lblDemandUOM.Caption = lblSafetyUOM.Caption & "/Day"
        lblMinStockUOM.Caption = lblSafetyUOM.Caption
        lblMaxStockUOM.Caption = lblSafetyUOM.Caption
        lblStdOrdUOM.Caption = lblSafetyUOM.Caption
        lblOrdPointUOM.Caption = lblSafetyUOM.Caption
        lblLinePointUOM.Caption = lblSafetyUOM.Caption
    End If

    mlUOMKey = gvCheckNull(moClass.moAppDB.Lookup _
        ("StockUnitMeasKey", "timItem", "ItemKey = " & Format$(mlItemKey)), SQL_INTEGER)

    lblStdOrdUOM.Caption = gvCheckNull(moClass.moAppDB.Lookup _
        ("UnitMeasID", "tciUnitMeasure", "UnitMeasKey = " & mlUOMKey), SQL_INTEGER)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DispItemDefaults", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub DisplayPrimaryVendor(ByVal bInit As Boolean, Optional bRowLoading As Boolean = False)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    '************************************************************************************
    '* This routine displays the primary vendor information.  The primary vendor info
    '* can come from the inventory record, or the warehouse purchase product line (WPPL)
    '* record.  If the inventory primary vendor is not set, then it is assumed that the
    '* primary vendor is coming from the WPPL record, even if that record has a blank
    '* primary vendor or if the WPPL record does not exist.
    '*
    '* Please exercise caution when modifying this procedure as possibility of creating
    '* regression issues is high.
    '************************************************************************************
    
    Dim lPrimaryVendKey As Long
    Static bInRecursion As Boolean

    'If this routine checked the WPPL checkbox, that will cause this routine
    'to be called by itself.  In that case recursion will occur.  Disable the
    'recursion so unnecessary processing will not happen and so that warning for
    'PPL will not occur more than once.
    If bInRecursion Then
        bInRecursion = False
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
        Exit Sub
    End If

    'Get primary vendor if there is one specified and the WPPL checkbox is unchecked.
    If chkUseWPPLPrimaryVendor.Value = vbUnchecked Then
        lPrimaryVendKey = glGetValidLong(lkuPrimaryVendor.KeyValue)
    End If
        
    If lPrimaryVendKey = 0 Then
        
        If mbIRIsActivated Then
        
            'Check the checkbox if being called from data load.  This will default it to checked if the
            'inventory record has no primary vendor, but will not check it when the user unchecks the checkbox and
            'then rechecks it.
            If bInit Then
                bInRecursion = True
                chkUseWPPLPrimaryVendor.Value = vbChecked
            End If
            
            'Get the primary vendor from the WPPL record if the WPPL checkbox is checked and a PPL is specified.
            If chkUseWPPLPrimaryVendor.Value = vbChecked And glGetValidLong(lkuPPLine.KeyValue) > 0 Then
        
                'Get the primary vendor from the WPPL record.  If there is none, then the key will be set to zero.
                lPrimaryVendKey = glGetValidLong(moClass.moAppDB.Lookup("PrimaryVendKey", "timWhsePurchProdLn", _
                    "PurchProdLineKey= " & glGetValidLong(lkuPPLine.KeyValue) & " AND WhseKey = " & glGetValidLong(lkuWhse.KeyValue)))
                    
                If lPrimaryVendKey = 0 And bInit = False And msWarningForPPL <> lkuPPLine.Text And bRowLoading = False Then
                    If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "timWhsePurchProdLn", _
                        "PurchProdLineKey= " & glGetValidLong(lkuPPLine.KeyValue) & " AND WhseKey = " & glGetValidLong(lkuWhse.KeyValue))) = 1 Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgIMWPPLPrimaryVendorMissing
                        msWarningForPPL = lkuPPLine.Text
                    End If
                End If
            End If
            
        End If
    End If
    
    'If a primary vendor was found, display that vendor regardless of where it came from.
    If lPrimaryVendKey > 0 And glGetValidLong(lkuWhseFrom.KeyValue) = 0 Then
        lkuPrimaryVendor.KeyValue = lPrimaryVendKey
        lkuPrimaryVendor.Text = gsGetValidStr(moClass.moAppDB.Lookup("VendID", "tapVendor", " VendKey = " & glGetValidLong(lPrimaryVendKey)))
        lblPrimaryVendorName = gsGetValidStr(moClass.moAppDB.Lookup("VendName", "tapVendor", " VendKey = " & glGetValidLong(lPrimaryVendKey)))
    Else
        lkuPrimaryVendor.KeyValue = 0
        lkuPrimaryVendor.Text = ""
        lblPrimaryVendorName = ""
    End If
    
    'If there is no PPL, then the WPPL checkbox should be unchecked and disabled.
    If glGetValidLong(lkuPPLine.KeyValue) = 0 Then
        chkUseWPPLPrimaryVendor.Value = vbUnchecked
        chkUseWPPLPrimaryVendor.Enabled = False
    Else
        chkUseWPPLPrimaryVendor.Enabled = mbIRIsActivated
    End If
    
    'The primary vendor lookup is enabled when the WPPL checkbox is unchecked and there is no source warehouse.
    lkuPrimaryVendor.Enabled = (chkUseWPPLPrimaryVendor.Value = vbUnchecked) And _
        (glGetValidLong(lkuWhseFrom.KeyValue) = 0)
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisplayPrimaryVendor", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPrefBinDist_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lActiveRow As Long
    Dim lActiveCol As Long
   
    If bDenyControlFocus Then
        Exit Sub
    End If
   
    'Launch the navigator
    lActiveRow = glGridGetActiveRow(grdPrefBinDist)
    lActiveCol = glGridGetActiveCol(grdPrefBinDist)
    
    If lActiveRow > 0 And lActiveCol > 0 Then
        txtPrefBinDist.Text = gsGridReadCellText(grdPrefBinDist, lActiveRow, lActiveCol)
    End If
    
    Call gcLookupClick(Me, lkuPrefBinDist, txtPrefBinDist, "WhseBinID")

    moGMPrefBinDist.LookupClicked

    mbF5Down = False
   
    grdPrefBinDist.Tag = "0"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPrefBinDist_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub DispWhseDefaults()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Default the Reorder Method to that of the warehouse.
    Dim rs As Object
    Dim sSQL As String
    Dim lWhseKey As Long
   
    sSQL = "SELECT ShipComplete, ReordMeth, CostToReplenish, CostOfCarry, TrackQtyAtBin, UseBins FROM timWarehouse WHERE WhseKey = " & Format$(mlWhseKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If Not rs Is Nothing Then
        If rs.RecordCount > 0 Then
            chkShipComplete.Value = rs.IntField("ShipComplete")
            ddnReordMeth.ListIndex = rs.IntField("ReordMeth")
            crnCostToReplenish.Text = ""
            nbrCostOfCarry.Text = ""
            RestrictPrefBins
        End If
        
        rs.Close
        Set rs = Nothing
    End If
    
    'Check if warehouse is tracking by bin or not.
    mbTrackQtyAtBin = gbGetValidBoolean(gvCheckNull(moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse", "WhseKey = " & Format$(mlWhseKey)), SQL_INTEGER))

    If (Not mbTrackQtyAtBin) Then
        chkPickOneLotOnly.Enabled = False
    End If
    
    If lkuItem.KeyValue > 0 Then
        miItemTrackMethod = moClass.moAppDB.Lookup("TrackMeth", "timItem", "timItem.ItemKey = " & lkuItem.KeyValue)
        sSQL = "SELECT DfltPickMeth,DfltLotPickMethod FROM timWarehouse WHERE WhseKey = " & Format$(mlWhseKey)
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
        If Not rs Is Nothing Then
            If rs.RecordCount > 0 Then
                If (miItemTrackMethod = kLotItem) Or (miItemTrackMethod = kLotSerialItem) Then
                    ddnPickMethod.ItemData = rs.IntField("DfltLotPickMethod")
                Else
                    ddnPickMethod.ItemData = rs.IntField("DfltPickMeth")
                    ddnPickMethod.Enabled = True
                End If
            End If
        
            rs.Close
            Set rs = Nothing
        End If
       
        If (miItemTrackMethod = kSerialItem) _
        Or (miItemTrackMethod = kLotSerialItem) Then
            chkSerialNoLinesOnPickList.Enabled = True
        Else
            chkSerialNoLinesOnPickList.Enabled = False
        End If
    End If
    
    If mlPPLKey > 0 Then
        sSQL = "SELECT BuyerKey, PhysCountCycleKey FROM timWhsePurchProdLn WHERE WhseKey = " & Format$(mlWhseKey) & " AND PurchProdLineKey = " & Format$(mlPPLKey)
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
        
        If Not rs Is Nothing Then
            If rs.RecordCount > 0 Then
                If rs.LongField("BuyerKey") > 0 Then
                    ddnBuyer.ItemData = rs.LongField("BuyerKey")
                    Call DisplayDecimalPlaces
                End If
                
                If rs.LongField("PhysCountCycleKey") > 0 Then
                    ddnPhysCycle.ItemData(, False) = rs.LongField("PhysCountCycleKey")
                End If
                
                rs.Close
                Set rs = Nothing
            End If
        End If
    End If
   
    PopulateGLAccts
                
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DispWhseDefaults", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim rs As Object
    Dim iDecPos As Integer
    Dim iIndex As Integer
    Dim lKey As Long

    If mlItemKey > 0 And IsEmpty(lkuItem.KeyValue) Then
        lkuItem.SetTextAndKeyValue lkuItem.Text, mlItemKey
    End If
    
    If mbBTOKit Or mbIsAssembledKit Then
        If mbBTOKit Then
            lkuWhseFrom.KeyValue = mlWhseKey
            lkuWhseFrom.Enabled = False
        End If
        lkuPPLine.Enabled = False
        
    Else
        mlPrevPPLKey = 0
        If Not Len(Trim$(lkuPPLine.Text)) = 0 Then

            If lkuPPLine.IsValid Then
                lkuPPLine.Enabled = True
                lkuPPLine.EnabledLookup = True
                
                lkuWhseFrom.Enabled = False
                lkuWhseFrom.EnabledLookup = False
                lblWhseDescFrom.Caption = ""
                Call DisplayDecimalPlaces
            Else
                lkuPPLine.ClearData
            End If
        Else
            If mbWMIsLicensed Then
                lkuWhseFrom.Enabled = True
                lkuWhseFrom.EnabledLookup = True
                If Len(Trim$(lkuWhseFrom.Text)) > 0 Then
                    lblWhseDescFrom.Caption = moClass.moAppDB.Lookup _
                                ("Description", "timWarehouse", "WhseKey = " & Format$(lkuWhseFrom.KeyValue))
                    lkuPPLine.Enabled = False
                    
                End If
            
            Else
                lkuWhseFrom.Text = ""
                lkuWhseFrom.Enabled = False
                lkuWhseFrom.EnabledLookup = False
                lblWhseDescFrom.Caption = ""
                
            End If
        End If
    End If
    
    'Displaying the data from hidden controls.
    txtCountTol.Value = txtPct.Text * 100
    
    nbrCostOfCarry.Text = IIf(nbrCostCarryPct.Text <> "", Val(nbrCostCarryPct.Text) * 100, "")

    If nbrCostOfCarry.Text <> "" Then
        mbCCPctKeyPressed = True
        
        iDecPos = InStr(1, nbrCostOfCarry.Text, ".")
        If iDecPos = 0 Then
            If nbrCostOfCarry.Text <> "" Then
                nbrCostOfCarry.Text = nbrCostOfCarry.Text & ".00"
            End If
        Else
            If Len(Mid(nbrCostOfCarry.Text, iDecPos + 1, 2)) <> 2 Then
                 nbrCostOfCarry.Text = nbrCostOfCarry.Text & "0"
            End If
        End If
    Else
        mbCCPctKeyPressed = False
    End If
    
    If crnCostToReplenish <> "" Then
        mbCRepKeyPressed = True

        iDecPos = InStr(1, crnCostToReplenish.Text, ".")
        If iDecPos = 0 Then
            If crnCostToReplenish.Text <> "" Then
                crnCostToReplenish.Text = crnCostToReplenish.Text & ".00"
            End If
        Else
            If Len(Mid(crnCostToReplenish.Text, iDecPos + 1, 2)) <> 2 Then
                 crnCostToReplenish.Text = crnCostToReplenish.Text & "0"
            End If
        End If
    Else
        mbCRepKeyPressed = False
    End If

    If Not mbBTOKit Then
        RestrictPrefBins
        If mbUseBins Then
            RefreshPickMethodDropDown
        End If
    End If

    If Len(Trim$(lkuDemand.Text)) > 0 Then
        nbrDemand.Enabled = False
    Else
        If mbIRIsActivated Then
            nbrDemand.Enabled = True
        End If
    End If

    If Len(Trim$(lkuSafetyStock.Text)) > 0 Then
        nbrSafetyStockQty.Enabled = False
    Else
        If mbIRIsActivated Then
            nbrSafetyStockQty.Enabled = True
        End If
    End If

    If Len(Trim$(lkuLeadTime.Text)) > 0 Then
        txtLeadTime.Enabled = False
        vsLeadTime.Enabled = False
    Else
        txtLeadTime.Enabled = True
        vsLeadTime.Enabled = True
    End If

    '********* Get Order Cycle from (Warehouse Replenishment) timWhsePurchProdLn Table. *******
    If Not IsEmpty(lkuPPLine.KeyValue) Then
        mlPrevPPLKey = lkuPPLine.KeyValue

        sSQL = "SELECT OrderCycle FROM timWhsePurchProdLn WITH (NOLOCK) WHERE WhseKey = " & Format$(lkuWhse.KeyValue) & " AND PurchProdLineKey = " & Format$(lkuPPLine.KeyValue)
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)

        If Not rs Is Nothing Then
            If rs.RecordCount > 0 Then
                txtOrdCycle.Text = rs.Field("OrderCycle")
            Else
                txtOrdCycle.Text = ""
            End If
            
            rs.Close
            Set rs = Nothing
        End If

    End If
    
    'The inventory primary vendor is not bound, so it must be set manually.  It must be unbound because the control
    'may display a vendor from somewhere other than the inventory record.  If it is on the inventory record at load
    'time, set it here.
    chkUseWPPLPrimaryVendor.Value = vbUnchecked
    lKey = glGetValidLong(moDmForm.GetColumnValue("PrimaryVendKey"))
    If lKey > 0 Then
        lkuPrimaryVendor.KeyValue = moDmForm.GetColumnValue("PrimaryVendKey")
    Else
        lkuPrimaryVendor.ClearData
    End If
    Call DisplayPrimaryVendor(kPrimaryVendorInitialize, True)
    
    sSQL = "SELECT Demand, SafetyStockQty, LeadTime FROM timInventory WITH (NOLOCK) WHERE WhseKey = " & Format$(mlWhseKey) & " AND ItemKey = " & Format$(mlItemKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
        
    If Not rs Is Nothing Then
        If rs.RecordCount > 0 Then
            nbrDemand.Value = rs.Field("Demand")
            nbrSafetyStockQty.Value = rs.Field("SafetyStockQty")
            txtLeadTime.Value = rs.Field("LeadTime")

            rs.Close
            Set rs = Nothing
        End If
    End If
    
    'Look for existing distribution preferred bins.
    If mbUseBins Then
        If bPreferredBinsExist(PrefBinTypes.Distribution) Then
            chkPickPrefBinsFirst.Enabled = True
        Else
            chkPickPrefBinsFirst.Value = 0
            chkPickPrefBinsFirst.Enabled = False
        End If
    Else
       chkPickPrefBinsFirst.Value = 0
       chkPickPrefBinsFirst.Enabled = False
    End If
    
    crnRplcmntCost.Tag = crnRplcmntCost.Amount
    ddnStatus.Tag = ddnStatus.ItemData
    
    moGMPrefBinDist.Clear
    moGMPrefBinMfg.Clear
    mbValidBins = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMDataDisplayed", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMPreSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'If warehouse is specified then there must be a buyer.
    bValid = True

    If Len(Trim$(nbrCostOfCarry)) > 0 Then
        nbrCostCarryPct.Text = nbrCostOfCarry.Text / 100
    End If
    
'**************************************************************************************
'If Source warehouse is same as main warehouse then check for all components of that item
'All components should have records in inventory.
'**************************************************************************************
    If mbIsAssembledKit Then
        If Len(Trim$(lkuWhseFrom.Text)) > 0 Then
            'Component check, only in case source warehouse is same as main warehouse.
            If lkuWhseFrom.KeyValue = lkuWhse.KeyValue _
            And Not bIsAllCompInInventory(lkuWhse.KeyValue, lkuItem.KeyValue) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMComponentChk
                bValid = False
                tabInventory.Tab = kTabRplnmnt
                lkuPPLine.ClearData
                lkuPPLine.Enabled = True
                lkuWhseFrom.ClearData
                lblWhseDescFrom.Caption = ""
                lkuWhseFrom.Enabled = True
                lkuWhseFrom.SetFocus
            End If
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgWhseReq
            bValid = False
            tabInventory.Tab = kTabRplnmnt
            lkuWhseFrom.SetFocus
            Exit Sub
        End If
    End If
    
    If ddnBuyer.ListIndex = -1 Then
        giSotaMsgBox Me, moClass.moSysSession, kMsgBuyerNotBlank
        tabInventory.Tab = kTabRplnmnt
        bValid = False
        ddnBuyer.SetFocus
        Exit Sub
    End If

    Dim bFlag As Boolean

    For miCount = 0 To kArrCount
        bFlag = False

        If IsEmpty(glaGLAcct(miCount).KeyValue) Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, gsStripChar(Trim$(lblGLAcct(miCount)), "&")
            bValid = False
            tabInventory.Tab = kTabGLAcct
            glaGLAcct(miCount).SetFocus
            Exit Sub
         Else
            If Not bIsValidGLFinancialAcct(miCount) Then
              bValid = False
              Exit Sub
            End If
        End If
    Next miCount

    If mbBTOKit Then
        Exit Sub
    End If
    
    If Not mbUseBins Then
        moDmGridPrefBinDist.SetCellValue 1, kColWhseBinKeyDist, SQL_INTEGER, gvCheckNull(moClass.moAppDB.Lookup _
                ("WhseBinKey", "timWhseBin", "WhseKey = " & Format$(mlWhseKey) & " AND DfltBin = 1"), SQL_INTEGER)
        moDmGridPrefBinDist.SetRowDirty 1
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMPreSave", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmForm_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As Object
    Dim rs1 As Object
    Dim sSQL As String

    mbAllowSave = True

    SetHourglass True

    lkuWhseFrom.RestrictClause = "(CompanyID = " & gsQuoted(msCompanyID) & " AND Transit = 0"

    Select Case iNewState
        Case Is = kDmStateNone
            tbrMain.SetState sotaTB_NONE
            sbrMain.Status = SOTA_SB_START

            tabInventory.Tab = kTabMain

            CleanForm

            DisplayDefaultRanks

        Case Is = kDmStateAdd
            moDmForm.SetDirty True

            
            mlItemKey = lkuItem.KeyValue
            mlWhseKey = lkuWhse.KeyValue
            
            DisplayItemInfo mlItemKey
            If miItemStatus = kItemStatus_Deleted Or miItemStatus = kItemStatus_Discontinued Then
                GoTo ExitStateChange
            End If
            
            tbrMain.SetState sotaTB_ADD
            sbrMain.Status = SOTA_SB_ADD
            lkuItem.Enabled = False
            lkuWhse.Enabled = False
            
            DispItemDefaults
            DispWhseDefaults
            RefreshPickMethodDropDown
            
            'For a new record, set the default value to checked.
            chkUseDistBinsInMfg.Value = vbChecked

            If mbBTOKit Or mbIsAssembledKit Then
                lkuWhseFrom.RestrictClause = lkuWhseFrom.RestrictClause & " AND WhseKey = " & Format$(mlWhseKey) & ")"

                lkuPPLine.ClearData
                lkuPPLine.Enabled = False
                lkuPPLine.EnabledLookup = False

                lblWhseDescFrom.Caption = ""
                lkuWhseFrom.KeyValue = mlWhseKey
                lkuWhseFrom.Enabled = False
                lkuWhseFrom.EnabledLookup = False
            End If
            
            If mbBTOKit Then
                
                crnRplcmntCost.Enabled = False
                cmdStdCostBreakout.Enabled = False
                
            Else

                 
                 
                If Not mbIsAssembledKit Then
                    'lkuWhseFrom.Enabled = mbWMIsLicensed
                    'lkuWhseFrom.EnabledLookup = mbWMIsLicensed
                    'lkuWhseFrom.ClearData
                    lblWhseDescFrom.Caption = ""
                    lkuWhseFrom.RestrictClause = lkuWhseFrom.RestrictClause & " AND WhseKey IN (SELECT i.WhseKey FROM timInventory i WITH (NOLOCK) WHERE i.ItemKey = " & Format$(mlItemKey) & "))"
                    
                    If Len(Trim$(lkuWhseFrom.Text)) = 0 Then
                        lkuPPLine.Enabled = True
                        lkuPPLine.EnabledLookup = True
                    Else
                        lkuPPLine.ClearData
                        lkuPPLine.Enabled = False
                        lkuPPLine.EnabledLookup = False
                    End If
                    
                    If Len(Trim$(lkuPPLine.Text)) = 0 Then
                        lkuWhseFrom.Enabled = True
                        lkuWhseFrom.EnabledLookup = True
                    Else
                        lkuWhseFrom.ClearData
                        lkuWhseFrom.Enabled = False
                        lkuWhseFrom.EnabledLookup = False
                    End If
                    
                 End If
                
            End If
            Call DisplayPrimaryVendor(kPrimaryVendorInitialize)
       
       Case Is = kDmStateEdit
            tbrMain.SetState sotaTB_EDIT
            sbrMain.Status = SOTA_SB_EDIT
            
            mlItemKey = lkuItem.KeyValue
            mlWhseKey = lkuWhse.KeyValue
            
            DisplayItemInfo mlItemKey
            
            lkuItem.Enabled = False
            lkuWhse.Enabled = False
            
            If mbBTOKit Or mbIsAssembledKit Then
                If mbBTOKit Then
                    crnRplcmntCost.Enabled = False
                    cmdStdCostBreakout.Enabled = False
                End If
                
                lkuPPLine.Enabled = False
                lkuWhseFrom.Enabled = False
                lkuWhseFrom.RestrictClause = lkuWhseFrom.RestrictClause & " AND WhseKey = " & Format$(mlWhseKey) & ")"
            Else
                lkuWhseFrom.RestrictClause = lkuWhseFrom.RestrictClause & " AND WhseKey IN (SELECT i.WhseKey FROM timInventory i WITH (NOLOCK) WHERE i.ItemKey = " & Format$(mlItemKey) & "))"
            End If
            
            lblItemDesc.Caption = gvCheckNull(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription", "ItemKey = " & Format$(mlItemKey)))
            lblWhseDesc.Caption = gvCheckNull(moClass.moAppDB.Lookup("Description", "timWarehouse", "WhseKey = " & Format$(mlWhseKey)))
            lblSafetyUOM.Caption = gvCheckNull(moClass.moAppDB.Lookup("UnitMeasID", "tciUnitMeasure,timItem", "tciUnitMeasure.UnitMeasKey = timItem.StockUnitMeasKey AND timItem.ItemKey = " & Format$(mlItemKey)), SQL_INTEGER)
            
            If Len(Trim$(lblSafetyUOM.Caption)) > 0 Then
                lblDemandUOM.Caption = lblSafetyUOM.Caption & "/Day"
                lblMinStockUOM.Caption = lblSafetyUOM.Caption
                lblMaxStockUOM.Caption = lblSafetyUOM.Caption
                lblStdOrdUOM.Caption = lblSafetyUOM.Caption
                lblOrdPointUOM.Caption = lblSafetyUOM.Caption
                lblLinePointUOM.Caption = lblSafetyUOM.Caption
            End If
            
            RefreshPickMethodDropDown
    End Select
    
    If Len(Trim$(lkuDemand.Text)) > 0 Then
        nbrDemand.Enabled = False
    Else
        If mbIRIsActivated Then nbrDemand.Enabled = True
    End If
    
    If Len(Trim$(lkuSafetyStock.Text)) > 0 Then
        nbrSafetyStockQty.Enabled = False
    Else
        If mbIRIsActivated Then nbrSafetyStockQty.Enabled = True
    End If
    
    If Len(Trim$(lkuLeadTime.Text)) > 0 Then
        txtLeadTime.Enabled = False
        vsLeadTime.Enabled = False
    Else
        txtLeadTime.Enabled = True
        vsLeadTime.Enabled = True
    End If
    
    ddnStatus.Tag = ddnStatus.ItemData
    crnRplcmntCost.Tag = crnRplcmntCost.Amount
    
ExitStateChange:
    
    SetHourglass False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmForm_DMStateChange", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGridPrefBinDist_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If bBinAlreadyExists(lRow, PrefBinTypes.Distribution) Then
        mbMessageDisplayed = True
        giSotaMsgBox Me, moClass.moSysSession, kIMBinAlreadySelected
        bValid = False
        
        tabInventory.Tab = kTabPrefBin
        
        If grdPrefBinDist.Enabled Then
            grdPrefBinDist.SetFocus
        End If

        gGridSetSelectRow grdPrefBinDist, lRow
        gGridSetActiveCell grdPrefBinDist, lRow, kColBinIDDist
        
        Exit Sub
    End If
    
    moDmGridPrefBinDist.SetColumnValue lRow, "PrefNo", lRow
    moDmGridPrefBinDist.SetColumnValue lRow, "WhseKey", moDmForm.GetColumnValue("WhseKey")
    moDmGridPrefBinDist.SetColumnValue lRow, "ItemKey", moDmForm.GetColumnValue("ItemKey")
    moDmGridPrefBinDist.SetColumnValue lRow, "PrefBinType", PrefBinTypes.Distribution
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmGridPrefBinDist_DMGridBeforeInsert", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGridPrefBinDist_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bValid = True
    
    If (Not mbAllowSave _
        And grdPrefBinDist.ActiveRow <> grdPrefBinDist.MaxRows) Then
        If mbUseBins Then
            mbMessageDisplayed = True
            
            If bBinAlreadyExists(lRow, PrefBinTypes.Distribution) Then
                giSotaMsgBox Me, moClass.moSysSession, kIMBinAlreadySelected
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, "Bin"
            End If
            
            bValid = False
            
            If grdPrefBinDist.ActiveRow = grdPrefBinDist.MaxRows - 1 Then
                moDmGridPrefBinDist.DeleteBlock grdPrefBinDist.ActiveRow
            End If
        End If
        
        Exit Sub
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmGridPrefBinDist_DMGridValidate", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetUpDrillDownLabels(bEnable As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    gSetUpDrillDownLabels bEnable, lblItemKey, lblWhseKey
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetUpDrillDownLabels", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrCostOfCarry_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrCostOfCarry, True
    #End If
'+++ End Customizer Code Push +++
    Dim iDecimalPlace As Integer

    iDecimalPlace = InStr(1, nbrCostOfCarry.Text, ".")

    If iDecimalPlace > 0 Then
        If Len(Mid(nbrCostOfCarry.Text, iDecimalPlace + 1)) > miCCPctDecPlaces Then
            nbrCostOfCarry.Text = Mid(nbrCostOfCarry.Text, 1, iDecimalPlace) + Mid(nbrCostOfCarry.Text, iDecimalPlace + 1, miCCPctDecPlaces)
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrCostOfCarry_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrCostOfCarry_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrCostOfCarry, True
    #End If
'+++ End Customizer Code Push +++
    nbrCostOfCarry.Alignment = EALIGNMENTSTYLE_LEFT
    nbrCostOfCarry.Tag = nbrCostOfCarry.Text

    If Len(Trim$(nbrCostOfCarry)) = 0 Then
        nbrCostOfCarry.Text = "0.00"
    End If

    nbrCostOfCarry.SetSel 0, Len(nbrCostOfCarry.Text)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrCostOfCarry_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrCostOfCarry_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrCostOfCarry, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    Dim iIntPlaces As Integer

    If KeyAscii < 48 Or KeyAscii > 57 Then
        If KeyAscii = 46 Then
            If InStr(1, nbrCostOfCarry.Text, ".") > 0 Then
                KeyAscii = 0
            End If
        Else
            KeyAscii = 0
        End If
    Else
        mbCCPctKeyPressed = True
            
        iIntPlaces = InStr(1, nbrCostOfCarry.Text, ".")
        If iIntPlaces = 0 Then
            If Len(nbrCostOfCarry.Text) = miCCPctIntPlaces Then
                KeyAscii = 0
                Beep
            End If
        Else
            If Len(Left(nbrCostOfCarry.Text, iIntPlaces - 1)) = miCCPctIntPlaces And Len(Mid(nbrCostOfCarry.Text, iIntPlaces + 1, 2)) = miCCPctDecPlaces Then
                KeyAscii = 0
                Beep
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrCostOfCarry_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrCostOfCarry_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrCostOfCarry, True
    #End If
'+++ End Customizer Code Push +++
    Dim iDecPos  As Integer
    
    nbrCostOfCarry.Alignment = EALIGNMENTSTYLE_RIGHT
    
    If Len(nbrCostOfCarry.Text) = 0 Then
        mbCCPctKeyPressed = False
    End If
    
    If Not mbCCPctKeyPressed Then
        nbrCostOfCarry.Text = ""
    End If
    
    If nbrCostOfCarry.Text <> "" Then
        iDecPos = InStr(1, nbrCostOfCarry.Text, ".")
        If iDecPos = 0 Then
            If nbrCostOfCarry.Text <> "" Then
                nbrCostOfCarry.Text = nbrCostOfCarry.Text & ".00"
            End If
        Else
            If Len(Mid(nbrCostOfCarry.Text, iDecPos + 1, 2)) <> 2 Then
                 nbrCostOfCarry.Text = nbrCostOfCarry.Text & "0"
            End If
        End If
    End If
    
    'Validate the Carrying Cost Percentage
    'The cost of carrying should be between 0 and 100.
    If Val(nbrCostOfCarry.Text) < 0 Or Val(nbrCostOfCarry.Text) > 100 Then
        giSotaMsgBox Me, moClass.moSysSession, kIMInvalidCaryCost
        
        nbrCostOfCarry.Text = nbrCostOfCarry.Tag
        tabInventory.Tab = kTabRplnmnt
        nbrCostOfCarry.SetFocus
        
         Exit Sub
    End If
    
    'Update the hidden text box
    If Len(Trim$(nbrCostOfCarry.Text)) > 0 Then
        nbrCostCarryPct.Text = Val(nbrCostOfCarry.Text) / 100
    Else
        nbrCostCarryPct.Text = ""
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrCostOfCarry_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrMaxStockQty_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrMaxStockQty, True
    #End If
'+++ End Customizer Code Push +++
    nbrMaxStockQty.Tag = nbrMaxStockQty.Value
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrMaxStockQty_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrMinStockQty_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrMinStockQty, True
    #End If
'+++ End Customizer Code Push +++
    nbrMinStockQty.Tag = nbrMinStockQty.Value
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "nbrMinStockQty_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim iConfirmUnload As Integer
    Dim sNewKey As String
    Dim lRet As Long
    Dim vParseRet As Variant
        
    If sButton = kTbFilter Then
        If sbrMain.Filtered Then
            miFilter = RSID_FILTERED
        Else
            miFilter = RSID_UNFILTERED
        End If
        
        Exit Sub
    End If

    If moDmForm.IsDirty Then
        iConfirmUnload = moDmForm.ConfirmUnload()
        If iConfirmUnload = 0 Then
            Exit Sub
        End If
    End If
   
    Call HandleToolBarClick(sButton)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tabInventory_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    pnlTab(PreviousTab).Enabled = False
    
    pnlTab(tabInventory.Tab).Enabled = True
    
    If tabInventory.Tab <> kTabPrefBin Then
        lkuPrefBinDist.Visible = False
        lkuPrefBinMfg.Visible = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tabInventory_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandleToolBarClick Button

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SOTAVM_KeyChange()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Call the Key change of the main data manager from here
    moDmForm.KeyChange
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SOTAVM_KeyChange", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SOTAVM_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As Object
    Dim rs1 As Object
    Dim sSQL As String
    Dim iCanReplenish As Integer
    Dim bIsAssKitWhse As Boolean
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim ltmpPurchProdLineKey As Long
    Dim lUserKey As Long
    
    'Write validation code here for all controls.
    'Remember that you never require validations for look-ups.
    
    iReturn = SOTA_VALID
    
    Select Case oControl.Name
    
        Case lkuWhseFrom.Name
            If Len(Trim$(lkuWhseFrom.Text)) = 0 Then
                lkuWhseFrom.ClearData
                lblWhseDescFrom.Caption = ""
                lkuWhseFrom.Tag = ""
                If Not mbIsAssembledKit Then
                    lkuPPLine.Enabled = True
                    lkuPPLine.EnabledLookup = True
                End If
                
            Else
        
                If bValidSourceWarehouse = False Then
                    SOTAVM.Reset
                    SOTAVM.SetControlUIInvalid oControl
                    SOTAVM.SetControlFocus oControl
                End If
             
            End If
            lkuWhseFrom.Tag = lkuWhseFrom.Text
    
        Case crnRplcmntCost.Name
        
        If miSecurityLevel <> kSecLevelSupervisory Then
            If crnRplcmntCost.Tag <> crnRplcmntCost.Amount Then
                'Show security event form
                'Check the user id typed in can override (no cancel hit)
                sID = CStr(kInvSecEventRUCost)
                sUser = CStr(moClass.moSysSession.UserId)
                vPrompt = True
            
                If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
                    tabInventory.Tab = kTabMain
              
                    'No Override reset Value
                    crnRplcmntCost.Amount = crnRplcmntCost.Tag
                    crnRplcmntCost.SetFocus
                End If   'Security Event Overridden
            End If           'Click Event Called from Code
        End If
        
        Case ddnStatus.Name
            If miSecurityLevel <> kSecLevelSupervisory Then
                If ddnStatus.Tag <> ddnStatus.ItemData Then
                 
                    'Show security event form
                    'Check the user id typed in can override (no cancel hit)
                    sID = CStr(kSecChangeInvtStat)
                    sUser = CStr(moClass.moSysSession.UserId)
                    vPrompt = True
                    
                    If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
                        tabInventory.Tab = kTabMain
                    
                        'No Override reset Value
                        ddnStatus.ItemData = ddnStatus.Tag
                        ddnStatus.SetFocus
                    End If   'Security Event Overridden
                End If           'Click Event Called from Code
            End If

         Case ddnBuyer.Name
            If ddnBuyer.ListIndex = -1 Then
                giSotaMsgBox Me, moClass.moSysSession, kMsgBuyerNotBlank
                SOTAVM.SetControlFocus oControl
                iReturn = SOTA_INVALID
                sMessage = ""
                
                Exit Sub
            End If
        
        Case nbrDemand.Name
            'The projected demand should be greater yhan or equal to 0
            If nbrDemand.Value < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidProjDmd

                
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                iReturn = SOTA_INVALID
                
                Exit Sub
            End If
           
        Case nbrSafetyStockQty.Name
            'The projected safety stock should be greater yhan or equal to 0
            If nbrSafetyStockQty.Value < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidSStock
                
                SOTAVM.Reset
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                
                Exit Sub
            End If
           
        Case txtLeadTime.Name
            'The projected lead time should be greater yhan or equal to 0
            If Val(txtLeadTime.Text) < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidLeadTime
                
                SOTAVM.Reset
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                
                Exit Sub
            End If
               
        Case nbrMaxStockQty.Name
            'The Maximum stock should be greater than or equal to 0
            If nbrMaxStockQty.Value < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidMaxStk
                
                SOTAVM.Reset
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                
                Exit Sub
            End If
                        
        Case nbrMinStockQty.Name
            'The minimum stock should be greater than or equal to 0
            If nbrMinStockQty.Value < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidMinStk
                
                SOTAVM.Reset
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                
                Exit Sub
            End If
                        
        Case nbrStdOrdQty.Name
            'The standard order quantity should be greater than or equal to 0
            If nbrStdOrdQty.Value < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidStdOrd
                
                SOTAVM.Reset
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                
                Exit Sub
            End If

        Case crnCostToReplenish.Name
            'The cost of replenishing should be greater than 0 and
            If Val(crnCostToReplenish.Text) < 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kIMInvalidCostRplcmnt
                
                SOTAVM.Reset
                SOTAVM.SetControlUIInvalid oControl
                SOTAVM.SetControlFocus oControl
                
                Exit Sub
            End If

        Case lkuPPLine.Name ' Review the user's Purchase Product Line (PPL) entry.
            If bValidatePurchProdLinelku() = False Then
                iReturn = SOTA_INVALID
            End If
    
        '===========================================================================================
        'SGS DEJ Start  ============================================================================
        '===========================================================================================
        Case ddnPlaceCode.Name
                'Must exist
                Select Case ddnPlaceCode.ListIndex
                    Case 0          'Not Valid
                        If IsPaceReqrd() = True Then
                            iReturn = SOTA_INVALID
                            MsgBox "You must select a valid Placard Code.", vbExclamation, "MAS 500"
                            ddnPlaceCode.SetFocus
                            Exit Sub
                        End If
                    Case 1, 2               'Elevated Temp.
                        nbrType.Value = 1
                    Case 3, 4, 5, 6, 7      'Hot Tars
                        nbrType.Value = 2
                    Case 8                  'Not Regulated
                        nbrType.Value = 3
                    Case 9                  'Other
                        nbrType.Value = 4
                End Select
                
        
'        Case nbrPercentage.Name
'            If nbrPercentage.Tag <> nbrPercentage.ItemData Then
'                If 1 = 2 Then
'                    'No Override reset Value
'                    nbrPercentage.ItemData = nbrPercentage.Tag
'                    nbrPercentage.SetFocus
'                End If
'            End If
        
        Case nbrPercentAsphalt.Name
            If 1 >= nbrPercentAsphalt.Value <= 100 Then
            Else
                iReturn = SOTA_INVALID
                MsgBox "You must enter a valide number between 1 and 100 for the Percent Asphalt.", vbExclamation, "MAS 500"
                nbrPercentAsphalt.SetFocus
                Exit Sub
            End If
        
        '===========================================================================================
        'SGS DEJ End    ============================================================================
        '===========================================================================================
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SOTAVM_Validate", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function bValidatePurchProdLinelku() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As SOTADAS.DASRecordSet
    Dim sSQL As String
    Dim ltmpPurchProdLineKey As Long
    Dim lUserKey As Long
    
    'If the user typed in an invalid entry (a non-existent PPL), then the
    'keyvalue will be empty, but the control's text property won't be.
    'In this case, just return to the most recent, valid, selection.
    bValidatePurchProdLinelku = True
    
    If (lkuPPLine.KeyValue = Empty _
And Len(Trim$(lkuPPLine.Text)) <> 0) Then
        lkuPPLine.KeyValue = mlPrevPPLKey
        mlPPLKey = mlPrevPPLKey
        bValidatePurchProdLinelku = False
        Exit Function
    Else
        'If the PPL selection has changed from its previous value and it is not
        'blank, then ask the user if they want the PPL default values to replace the existing
        'values for the Buyer, Physical Cycle Count, Demand Formula, Safety Stock Formula,
        'Lead Time Formula and Reorder Method.  If they don't want to change the defaults, then
        'set the PPL selection back to the previous value.
        If (lkuPPLine.KeyValue <> mlPrevPPLKey _
            And lkuPPLine.KeyValue <> 0 _
            And (mlPrevPPLKey <> 0 Or mbPPLManuallyEntered)) Then
            lUserKey = giSotaMsgBox(Me, moClass.moSysSession, kMsgPPLDefaults)

            Select Case lUserKey
                Case kretOK
                    mlPrevPPLKey = lkuPPLine.KeyValue
                    mlPPLKey = mlPrevPPLKey
                    DisplayPPLDefaults (False)
                    
                    lkuWhseFrom.Enabled = False
                    lkuWhseFrom.EnabledLookup = False
    
                    Call DisplayDecimalPlaces
    
                    If ddnBuyer.ListIndex = -1 Then
                        'The reord method is picked up from timWhsePurchProdLine.
                        sSQL = "SELECT BuyerKey, ReordMeth FROM timWhsePurchProdLn WITH (NOLOCK) WHERE WhseKey = " & Format$(mlWhseKey) & " AND PurchProdLineKey = " & Format$(lkuPPLine.KeyValue)
                        Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
                        If Not rs Is Nothing Then
                            rs.MoveLast
                            rs.MoveFirst
    
                            If Not rs.IsEOF() Then
                                If Not IsNull(rs.Field("BuyerKey")) Then
                                    ddnBuyer.ItemData = rs.LongField("BuyerKey")
                                End If
                            End If
    
                            rs.Close
                            Set rs = Nothing
                        End If
                    End If
                    
                    msWarningForPPL = ""                                'allow warning message to be displayed for new PPL.
                    Call DisplayPrimaryVendor(kPrimaryVendorInitialize)
                
                Case Else
                    'Cancel and all other possibilities (only cancel is expected).
                    If mlPrevPPLKey <> 0 Then
                        lkuPPLine.KeyValue = mlPrevPPLKey
                    Else
                        lkuPPLine.Text = ""
                        lkuPPLine.ClearData
                        lkuWhseFrom.Enabled = mbWMIsLicensed
                        lkuWhseFrom.EnabledLookup = mbWMIsLicensed
                        txtOrdCycle.Text = ""
                    End If
            End Select
        ElseIf mlPPLKey = 0 Then
            lkuWhseFrom.Enabled = mbWMIsLicensed
            lkuWhseFrom.EnabledLookup = mbWMIsLicensed
            
        End If
    
        Call DisplayPrimaryVendor(kPrimaryVendorDoNotInitialize)
        
    End If

    mlPrevPPLKey = lkuPPLine.KeyValue
    mlPPLKey = mlPrevPPLKey

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidatePurchProdLinelku", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub txtCountTol_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCountTol, True
    #End If
'+++ End Customizer Code Push +++
    If txtCountTol.Value = 0 Then
        txtPct.Text = 0
    Else
        txtPct.Text = txtCountTol.Value / 100
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtCountTol_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub vsLeadTime_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If bDenyControlFocus Then
        Exit Sub
    End If
    
    'Logic for increment and Decrement
    If vsLeadTime.Value = 0 Then
        Exit Sub
    End If
        
    If vsLeadTime.Value > 0 Then
        If Val(txtLeadTime.Text) < 999 Then
            txtLeadTime.Value = Val(txtLeadTime.Text) + 1
        End If
    Else
        If Val(txtLeadTime.Text) > 0 Then
            txtLeadTime.Value = Val(txtLeadTime.Text) - 1
        End If
    End If
    
    vsLeadTime.Value = 0
    txtLeadTime.SetFocus

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "vsLeadTime_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub vsTolPct_Change()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If bDenyControlFocus Then
        Exit Sub
    End If
    
    'Logic for increment and Decrement
    If vsTolPct.Value = 0 Then
        Exit Sub
    End If
    
    If vsTolPct.Value > 0 Then
        If txtCountTol.Value = 999 Or txtCountTol.Value = 999.9 Or txtCountTol.Value = 999.99 Then
            Exit Sub
        End If

        If InStr(Trim$(Str(txtCountTol.Value)), ".") Then
            If Right(Trim$(txtCountTol.Text), 1) = "0" Then
                txtCountTol.Value = txtCountTol.Value + 0.1
            Else
                txtCountTol.Value = txtCountTol.Value + 0.01
            End If
        Else
            txtCountTol.Value = txtCountTol.Value + 1
        End If
        
        txtPct.Text = txtCountTol.Text / 100
    Else
        If txtCountTol.Value > 0 Then
            If InStr(Trim$(Str(txtCountTol.Value)), ".") Then
                If Right(Trim$(txtCountTol.Text), 1) = "0" Then
                    txtCountTol.Value = txtCountTol.Value - 0.1
                Else
                    txtCountTol.Value = txtCountTol.Value - 0.01
                End If
            Else
                txtCountTol.Value = txtCountTol.Value - 1
            End If
            
            txtPct.Text = txtCountTol.Text / 100
        End If
    End If
    
    vsTolPct.Value = 0
    txtCountTol.SetFocus
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "vsTolPct_Change", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuItem_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuItem, True
    #End If
'+++ End Customizer Code Push +++
    Dim sID As String
    Dim lItemKey As Long
    Dim iItemType As Integer
    Dim iItemStatus As Long
    Dim rs As Object
    Dim sSQL As String
    

    If mbF4Pressed Then
        Exit Sub
    End If
    
    If Trim$(lkuItem.Text) = "" Then
        lblItemDesc.Caption = ""
        Exit Sub
    End If
   
        sSQL = "Select ItemID, ItemKey, ItemType, Status FROM timItem WHERE " & _
                "ItemID = " & gsQuoted(Trim(lkuItem.Text)) & " AND CompanyID = " & gsQuoted(msCompanyID)
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        lItemKey = 0
        If Not rs.IsEOF Then
            sID = gsGetValidStr(rs.Field("ItemID"))
            lItemKey = glGetValidLong(rs.Field("ItemKey"))
            iItemType = giGetValidInt(rs.Field("ItemType"))
            miItemStatus = giGetValidInt(rs.Field("Status"))
        End If
        
        If Not lkuItem.IsValid Then

            
            
            If lItemKey = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, gsStripChar(lblItemKey, "&"), ":" & lkuItem.Text
            Else
                
                If iItemType >= kFinishedGood And iItemType <= kAssembledKit Then
                    giSotaMsgBox Me, moClass.moSysSession, kMsgItemNotActive, lkuItem.Text
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgIMSNonInvtItem, lkuItem.Text
                End If
                If mbUseBins Then
                   RefreshPickMethodDropDown
                End If
            End If
            
            lkuItem.Text = lkuItem.Tag
            
            If lkuItem.Enabled Then
                On Error Resume Next
                lkuItem.SetFocus
                Err.Clear
            End If
            
            Exit Sub
        Else
            'For Case insenstive database pick exact ID ( As defined by user)
            lkuItem.Text = sID
            
            bIsValidID
            If mbUseBins Then
                RefreshPickMethodDropDown
            End If
            If moDmForm.State = kDmStateEdit Then
                moDmForm.SetDirty False
            End If
            
            Exit Sub
        End If
                
    If Not lkuItem.IsValid Then
        lkuItem.Text = lkuItem.Tag
        Exit Sub
    End If
                
    bIsValidID

    Me.tabInventory.Tab = 0
    moDmForm.SetDirty False
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuItem_LostFocus", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuWhse_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuWhse, True
    #End If
'+++ End Customizer Code Push +++
    Dim sID As String

    If mbF4Pressed Then
        Exit Sub
    End If

    If Len(Trim$(lkuWhse.Text)) = 0 Then
        lblWhseDesc.Caption = ""
        Exit Sub
    End If
    
    If (Me.ActiveControl.Name = lkuItem.Name _
Or Me.ActiveControl.Name = navMain.Name) Then
        If Not lkuWhse.IsValid Then
            If gvCheckNull(moClass.moAppDB.Lookup("WhseKey", "timWarehouse", "timWarehouse.WhseID = " & gsQuoted(lkuWhse.Text) & " And CompanyID = " & gsQuoted(msCompanyID)), 0) <> 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kMsgTransitWarehouse, lkuWhse.Text
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidIncompatibleFields, gsStripChar(lblWhseKey, "&"), ":" & lkuWhse.Text
            End If
            
            lkuWhse.Text = lkuWhse.Tag
            
            If lkuWhse.Enabled Then
                lkuWhse.SetFocus
            End If

            Exit Sub
        Else
            'For Case insensitive database pick exact ID ( As defined by user)
            sID = gsGetValidStr(moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseID = " & gsQuoted(Trim$(lkuWhse.Text)) & " AND CompanyID = " & gsQuoted(msCompanyID)))
            lkuWhse.Text = sID
            bIsValidID
            
            Exit Sub
        End If
    End If
    
    If Not lkuWhse.IsValid Then
        lkuWhse.Text = lkuWhse.Tag
        Exit Sub
    End If

    If Not mbFrombIsValid Then
        bIsValidID
        
        If moDmForm.State = kDmStateEdit Then
            moDmForm.SetDirty False
        End If
    End If
    
    Me.tabInventory.Tab = 0
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuWhse_LostFocus", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bIsValidID() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**************************************************************************
' Description:
'    This is the field validation routine. It loads the controls
'    if a valid ID is entered; otherwise it sets the form state to
'    Add.
'
' Returns:
'   True  if valid key is entered (new or existing record)
'   False if incomplete key or an error occurs
'**************************************************************************
    Dim iActionCode  As Integer
    Dim bValid              As Boolean
    Dim iKeyChangeCode      As Integer
    Dim iItemType           As Integer
    Dim lCycleKey           As Long
    Dim lPPLKey             As Long
    Dim iReOrderMeth        As Integer
    
    mbFrombIsValid = True
    bIsValidID = True
    mbDmLoad = True
        
    iKeyChangeCode = moDmForm.KeyChange()
    
    Select Case iKeyChangeCode
        Case Is = kDmKeyNotFound
            If miItemStatus = kItemStatus_Deleted Or miItemStatus = kItemStatus_Discontinued Then
                giSotaMsgBox Me, moClass.moSysSession, kmsIMInvCreateDelDiscNotAllow
                HandleToolBarClick kTbCancel
                Exit Function
            End If
            
            
            iItemType = miItemType
            
            lPPLKey = IIf(IsEmpty(lkuPPLine.KeyValue), 0, lkuPPLine.KeyValue)
            mlPPLKey = lPPLKey
            
            If Not IsNull(moClass.moAppDB.Lookup("PhysCountCycleKey", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)) Then
                lCycleKey = glGetValidLong(moClass.moAppDB.Lookup("PhysCountCycleKey", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey))
                ddnPhysCycle.ItemData = lCycleKey
                txtPct.Text = gvCheckNull(moClass.moAppDB.Lookup("PhysCountTolPct", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey))
                txtCountTol.Value = Val(txtPct.Text) * 100
            End If
            
            If lPPLKey <> 0 Then
                If Not IsNull(moClass.moAppDB.Lookup("ReordMeth", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)) Then
                    iReOrderMeth = moClass.moAppDB.Lookup("ReordMeth", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)
                    ddnReordMeth.ItemData = iReOrderMeth
                End If
            End If
            
            'If BTOKit then check for all components.
            If iItemType = kBTOKit Or iItemType = kAssembledKit Then
                If bIsAllCompInInventory(lkuWhse.KeyValue, lkuItem.KeyValue) Then
                    moClass.lUIActive = kChildObjectActive
                Else
                    bIsValidID = False
                    
                    moDmForm.EnableKeyControls
                    
                    giSotaMsgBox Me, moClass.moSysSession, kmsgIMComponentChk
                    
                    iActionCode = moDmForm.Action(kDmCancel)
                    If iActionCode = kDmSuccess Then
                        moClass.lUIActive = kChildObjectInactive
                    End If
                    
                    lkuWhse.KeyValue = mlWhseKey
                    lkuPPLine.ClearData
                    lkuPPLine.Enabled = True
                    lkuWhseFrom.ClearData
                    lkuWhseFrom.Enabled = True
                    lblWhseDescFrom.Caption = ""
                    lkuItem.ClearData
                    lblItemDesc = ""
                    lkuItem.SetFocus
                    mlPPLKey = 0
                End If
            Else
                moClass.lUIActive = kChildObjectActive
            End If
            
            If bIsValidID Then
                SetDistPreferredBinsFrame
            End If
        
        Case Is = kDmKeyFound
            moClass.lUIActive = kChildObjectActive
            SetDistPreferredBinsFrame
            
        Case Is = kDmKeyNotComplete
            'Key not completely filled in
            bIsValidID = False
            
        Case Is = kDmNotAllowed
            lblItemDesc.Caption = ""
            lblWhseDesc.Caption = ""
            bIsValidID = False
            
        Case Is = kDmError
            'Database error occurred trying to get row
            bIsValidID = False
         
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, iKeyChangeCode
            bIsValidID = False
    End Select
    
    mbDmLoad = False
    mbFrombIsValid = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidID", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
#End If

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        
        If Not moFormCust Is Nothing Then
            tabInventory.TabVisible(kTabCustomizer) = False
            tabInventory.TabsPerRow = tabInventory.Tabs - 1
            moFormCust.Initialize Me, goClass, tabInventory.Name & ";" & kTabCustomizer
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyDataBindings moDmForm
            moFormCust.ApplyFormCust
        End If
    End If
#End If

    ' Validation Manager related code
    With SOTAVM
        Set .Framework = moClass.moFramework

        .Keys.Add lkuItem
        .Keys.Add lkuWhse
        .Keys.Add navMain

        .Init
    End With
    
    If gbItemKeyPassed Then
        lkuItem_LostFocus
        gbItemKeyPassed = False
    End If
    
    If gbWhseKeyPassed Then
        lkuWhse_LostFocus
        gbWhseKeyPassed = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub cmdStdCostBreakout_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdStdCostBreakout, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdStdCostBreakout_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdStdCostBreakout_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdStdCostBreakout, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdStdCostBreakout_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtPrefBinDist_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPrefBinDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinDist_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinDist_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPrefBinDist, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinDist_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinDist_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPrefBinDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinDist_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinDist_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPrefBinDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinDist_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinMfg_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPrefBinMfg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinMfg_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinMfg_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPrefBinMfg, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinMfg_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinMfg_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPrefBinMfg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinMfg_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPrefBinMfg_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPrefBinMfg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPrefBinMfg_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrCostCarryPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrCostCarryPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrCostCarryPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrCostCarryPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrCostCarryPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrCostCarryPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrCostCarryPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrCostCarryPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrCostCarryPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrCostCarryPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrCostCarryPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrCostCarryPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrdCycle_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrdCycle_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrdCycle_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtOrdCycle, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrdCycle_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrdCycle_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrdCycle_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtOrdCycle_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtOrdCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtOrdCycle_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemType_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemType_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuItem, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuItem, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuItem, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuItem_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuItem_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDemand_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuDemand, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDemand_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDemand_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuDemand, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDemand_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDemand_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuDemand, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDemand_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDemand_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuDemand, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDemand_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLeadTime_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuLeadTime, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLeadTime_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLeadTime_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuLeadTime, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLeadTime_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLeadTime_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuLeadTime, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLeadTime_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLeadTime_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuLeadTime, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLeadTime_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSafetyStock_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuSafetyStock, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSafetyStock_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSafetyStock_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuSafetyStock, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSafetyStock_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSafetyStock_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuSafetyStock, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSafetyStock_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSafetyStock_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuSafetyStock, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSafetyStock_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPrimaryVendor, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPrimaryVendor, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPrimaryVendor, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPrimaryVendor_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPrimaryVendor_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseFrom_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuWhseFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseFrom_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseFrom_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuWhseFrom, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseFrom_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseFrom_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuWhseFrom, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseFrom_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseFrom_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuWhseFrom, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseFrom_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuWhseFrom_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuWhseFrom, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuWhseFrom_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPPLine_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPPLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPPLine_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPPLine_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPPLine, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPPLine_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPPLine_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPPLine, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPPLine_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPPLine_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPPLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPPLine_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub nbrSafetyStockQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrSafetyStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrSafetyStockQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrSafetyStockQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrSafetyStockQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrSafetyStockQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrSafetyStockQty_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrSafetyStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrSafetyStockQty_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrSafetyStockQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrSafetyStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrSafetyStockQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrDemand_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrDemand, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrDemand_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrDemand_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrDemand, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrDemand_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrDemand_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrDemand, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrDemand_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrDemand_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrDemand, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrDemand_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLeadTime_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLeadTime, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLeadTime_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLeadTime_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLeadTime, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLeadTime_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLeadTime_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLeadTime, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLeadTime_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLeadTime_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLeadTime, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLeadTime_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxStockQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrMaxStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxStockQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxStockQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrMaxStockQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxStockQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxStockQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrMaxStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxStockQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrStdOrdQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrStdOrdQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrStdOrdQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrStdOrdQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrStdOrdQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrStdOrdQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrStdOrdQty_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrStdOrdQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrStdOrdQty_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrStdOrdQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrStdOrdQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrStdOrdQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMinStockQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrMinStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMinStockQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMinStockQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrMinStockQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMinStockQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMinStockQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrMinStockQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMinStockQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrOrdPoint_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrOrdPoint, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrOrdPoint_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrOrdPoint_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrOrdPoint, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrOrdPoint_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrOrdPoint_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrOrdPoint, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrOrdPoint_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrOrdPoint_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrOrdPoint, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrOrdPoint_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrLinePoint_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrLinePoint, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrLinePoint_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrLinePoint_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrLinePoint, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrLinePoint_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrLinePoint_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrLinePoint, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrLinePoint_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrLinePoint_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrLinePoint, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrLinePoint_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnStdUnitCost_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange crnStdUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnStdUnitCost_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnStdUnitCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress crnStdUnitCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnStdUnitCost_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnStdUnitCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus crnStdUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnStdUnitCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnStdUnitCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus crnStdUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnStdUnitCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnRplcmntCost_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange crnRplcmntCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnRplcmntCost_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnRplcmntCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress crnRplcmntCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnRplcmntCost_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnRplcmntCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus crnRplcmntCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnRplcmntCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnRplcmntCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus crnRplcmntCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnRplcmntCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnAvgUnitCost_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange crnAvgUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnAvgUnitCost_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnAvgUnitCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress crnAvgUnitCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnAvgUnitCost_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnAvgUnitCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus crnAvgUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnAvgUnitCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnAvgUnitCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus crnAvgUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnAvgUnitCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLandedCost_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange crnLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLandedCost_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLandedCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress crnLandedCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLandedCost_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLandedCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus crnLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLandedCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLandedCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus crnLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLandedCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLastCost_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange crnLastCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLastCost_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLastCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress crnLastCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLastCost_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLastCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus crnLastCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLastCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub crnLastCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus crnLastCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "crnLastCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCountTol_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCountTol, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCountTol_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCountTol_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCountTol, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCountTol_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCountTol_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCountTol, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCountTol_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseDistBinsInMfg_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkUseDistBinsInMfg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseDistBinsInMfg_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseDistBinsInMfg_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkUseDistBinsInMfg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseDistBinsInMfg_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickPrefBinsFirst_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkPickPrefBinsFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickPrefBinsFirst_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickPrefBinsFirst_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPickPrefBinsFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickPrefBinsFirst_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickPrefBinsFirst_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPickPrefBinsFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickPrefBinsFirst_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickOneLotOnly_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkPickOneLotOnly, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickOneLotOnly_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickOneLotOnly_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPickOneLotOnly, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickOneLotOnly_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPickOneLotOnly_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPickOneLotOnly, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPickOneLotOnly_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSerialNoLinesOnPickList_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkSerialNoLinesOnPickList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSerialNoLinesOnPickList_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSerialNoLinesOnPickList_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkSerialNoLinesOnPickList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSerialNoLinesOnPickList_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSerialNoLinesOnPickList_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkSerialNoLinesOnPickList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSerialNoLinesOnPickList_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShowAvailLots_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkShowAvailLots, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShowAvailLots_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShowAvailLots_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShowAvailLots, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShowAvailLots_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShowAvailLots_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShowAvailLots, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShowAvailLots_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOLineFirst_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkCloseSOLineFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOLineFirst_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOLineFirst_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCloseSOLineFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOLineFirst_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOLineFirst_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCloseSOLineFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOLineFirst_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseWPPLPrimaryVendor_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkUseWPPLPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseWPPLPrimaryVendor_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkUseWPPLPrimaryVendor_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkUseWPPLPrimaryVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkUseWPPLPrimaryVendor_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPickMethod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnPickMethod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPickMethod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPickMethod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnPickMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPickMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPickMethod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnPickMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPickMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnBuyer_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnBuyer, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnBuyer_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnBuyer_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnBuyer, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnBuyer_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnBuyer_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnBuyer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnBuyer_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnBuyer_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnBuyer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnBuyer_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReordMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnReordMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReordMeth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReordMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnReordMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReordMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReordMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnReordMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReordMeth_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnStatus, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnStatus_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnStatus_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnHitsRank_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnHitsRank, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnHitsRank_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnHitsRank_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnHitsRank, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnHitsRank_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnHitsRank_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnHitsRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnHitsRank_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnHitsRank_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnHitsRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnHitsRank_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnGrMgnRank_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnGrMgnRank, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnGrMgnRank_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnGrMgnRank_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnGrMgnRank, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnGrMgnRank_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnGrMgnRank_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnGrMgnRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnGrMgnRank_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnGrMgnRank_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnGrMgnRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnGrMgnRank_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnQtySoldRank_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnQtySoldRank, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnQtySoldRank_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnQtySoldRank_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnQtySoldRank, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnQtySoldRank_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnQtySoldRank_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnQtySoldRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnQtySoldRank_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnQtySoldRank_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnQtySoldRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnQtySoldRank_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCOSRank_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnCOSRank, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCOSRank_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCOSRank_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnCOSRank, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCOSRank_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCOSRank_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnCOSRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCOSRank_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCOSRank_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnCOSRank, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCOSRank_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPhysCycle_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnPhysCycle, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPhysCycle_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPhysCycle_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnPhysCycle, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPhysCycle_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPhysCycle_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnPhysCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPhysCycle_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPhysCycle_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnPhysCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPhysCycle_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calMinMax_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calMinMax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calMinMax_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calMinMax_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calMinMax, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calMinMax_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEstDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calEstDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEstDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEstDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calEstDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEstDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEstDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calEstDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEstDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calEstDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calEstDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calEstDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastCountDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calLastCountDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastCountDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastCountDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calLastCountDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastCountDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastCountDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calLastCountDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastCountDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calLastCountDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calLastCountDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calLastCountDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomButton(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomButton(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomButton(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomCheck(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCheck(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCheck(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCombo(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomCombo(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomCombo(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCombo(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCombo(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomCurrency(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomCurrency(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomCurrency(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomFrame(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomFrame(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomLabel(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomLabel(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomMask(Index)
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomMask(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomMask(Index), KeyAscii
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomMask(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomNumber(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomNumber(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomNumber(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomOption(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomOption(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomOption(Index)
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomOption(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinDown CustomSpin(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnSpinUp CustomSpin(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
#End If

' ********************************************************************************
'    Desc:  Binds the Grid Manager class to the Grid Manager object.
'   Parms:  none
' ********************************************************************************
Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moGMPrefBinDist = New clsGridMgr
 
    'Bind the Grid Manager for the distribution preferred bins.
    With moGMPrefBinDist
        Set .Grid = grdPrefBinDist
        Set .DM = moDmGridPrefBinDist
        Set .Form = Me
        
        If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        Set moGridNavPrefBinDist = .BindColumn(kColBinIDDist, lkuPrefBinDist)
        Set moGridNavPrefBinDist.ReturnControl = txtPrefBinDist
        .Init
    End With
    
    Set moGMPrefBinMfg = New clsGridMgr
 
    'Bind the Grid Manager for the manufacturing preferred bins.
    With moGMPrefBinMfg
        Set .Grid = grdPrefBinMfg
        Set .DM = moDmGridPrefBinMfg
        Set .Form = Me
        
        If miSecurityLevel = kSecLevelDisplayOnly Then  'Display Only set to read only grid
            .GridType = kGridDataSheetNoAppend
        Else
            .GridType = kGridDataSheet
        End If
        
        Set moGridNavPrefBinMfg = .BindColumn(kColBinIDMfg, lkuPrefBinMfg)
        Set moGridNavPrefBinMfg.ReturnControl = txtPrefBinMfg
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub DisplayItemInfo(ByVal iItemKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***************************************************************
' Description:
'    This routine displays ItemType for an Item whose Inventory
'    is to be created.
' Param:
'       ItemKey: Item for which ItemType is to be displayed.
'***************************************************************
    Dim rs As Object
    Dim rs1 As Object
    Dim sSQL As String
    Dim iValuationMeth As Integer
    
    sSQL = "SELECT ValuationMeth, ItemType, Status "
    sSQL = sSQL & " FROM timItem WHERE ItemKey = " & Format$(mlItemKey)
    On Error Resume Next
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    
    If rs Is Nothing Then Exit Sub
    
    If rs.IsEOF Then
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    
    miItemType = gvCheckNull(rs.Field("ItemType"), SQL_INTEGER)
    miItemStatus = giGetValidInt(rs.Field("Status"))
    iValuationMeth = giGetValidInt(rs.Field("ValuationMeth"))
    
    rs.Close
    Set rs = Nothing
    
    If miItemType = kAssembledKit Then
        mbIsAssembledKit = True
    Else
        mbIsAssembledKit = False
    End If
        
    If miItemType = kBTOKit Then
        mbBTOKit = True
    Else
        mbBTOKit = False
    End If

    If miItemType = kAssembledKit Or miItemType = kBTOKit Then
        lkuWhseFrom.Enabled = True
        lkuWhseFrom.EnabledLookup = True
    Else
        lkuWhseFrom.Enabled = (mbWMIsLicensed <> 0) And (glGetValidLong(lkuPPLine.KeyValue) = 0)
        lkuWhseFrom.EnabledLookup = (mbWMIsLicensed <> 0) And (glGetValidLong(lkuPPLine.KeyValue) = 0)
        lblWhseDescFrom.Caption = ""

        
    End If
    
    txtItemType.Text = gvCheckNull(moClass.moAppDB.Lookup _
                        ("LocalText", "tsmLocalString", _
                        "StringNo = (SELECT StringNo FROM tsmListValidation " & _
                        "WHERE TableName = 'timItem' AND ColumnName = 'ItemType'" & _
                        "AND DBValue = " & miItemType & ")"), SQL_CHAR)
    
    
    If iValuationMeth = kItemValuationMeth_Std Then
        'First check if Cost Tier Records exists.
        If moClass.moAppDB.Lookup("COUNT(*)", "timCostTier WITH (NOLOCK)", "ItemKey = " & Format$(mlItemKey) & " AND WhseKey = " & Format$(mlWhseKey)) > 0 Then
            sSQL = "SELECT CostTierKey FROM timCostTier WITH (NOLOCK)"
            sSQL = sSQL & " WHERE ItemKey = " & Format$(mlItemKey)
            sSQL = sSQL & " AND WhseKey = " & Format$(mlWhseKey)
            sSQL = sSQL & " AND (OrigQty - QtyUsed = 0)"

            Set rs1 = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
           
            If Not rs1 Is Nothing Then
                If rs1.RecordCount = 0 Then
                    'Cost Tier with pending activities or available qty still exists.
                    'So, do not allow user to update standard cost field.
                    cmdStdCostBreakout.Enabled = True
                Else
                    'A record was returned signifying the Cost Tier is all used up
                    'and there are no pending activities.
                    cmdStdCostBreakout.Enabled = True
                End If
            
                rs1.Close
                Set rs1 = Nothing
            End If
        Else
            'No Cost Tier records yet so enable the standard cost field.
            cmdStdCostBreakout.Enabled = True
        End If
    Else
        cmdStdCostBreakout.Enabled = True
    End If
    


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisplayItemInfo", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub DisplayDecimalPlaces()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************
' Description:
'    This routine restricts the decimal places in Qty fields.
'************************************************************
    Dim iAllowPartPack As Integer
    Dim iAllowDecimalQty As Integer
    
    nbrStdOrdQty.DecimalPlaces = miQtyDecPlaces
    nbrMaxStockQty.DecimalPlaces = miQtyDecPlaces
    nbrMinStockQty.DecimalPlaces = miQtyDecPlaces
    
    'New Controls Dec Places
    nbrOrdPoint.DecimalPlaces = miQtyDecPlaces
    nbrLinePoint.DecimalPlaces = miQtyDecPlaces
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisplayDecimalPlaces", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function sGetDescription(sSQL As String, sCol As String) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************
' Description:
'    This routine returns the value of the field sCol,
'    specified in the sSQL parameter.
'*******************************************************
    Dim rs As Object
    
    sGetDescription = ""
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If Not rs Is Nothing Then
        rs.MoveLast
        rs.MoveFirst
        
        If Not rs.IsEOF() Then
            If Not IsNull(rs.Field(sCol)) Then
                sGetDescription = rs.Field(sCol)
            End If
        End If
    
        rs.Close
        Set rs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sGetDescription", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub moGMPrefBinDist_GridAfterDelete()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moDmForm.SetDirty True
    lkuPrefBinDist.Visible = False
    
    If grdPrefBinDist.MaxRows = 1 Then
        If chkPickPrefBinsFirst.Value = 1 Then
            chkPickPrefBinsFirst.Value = 0
        End If
    
        chkPickPrefBinsFirst.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMPrefBinDist_GridAfterDelete", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub RestrictPrefBins()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**************************************************************
' Description:
'This routine checks if UseBins is true for a warehouse,
'If yes then check if Track Qty is set or not and then restrict
'the bins in the grid, otherwise disable the entire Preferred
'Bin Tab.
'**************************************************************
    Dim iUseBins As Integer
    Dim iTrackQtyAtBin As Integer

    'Check if warehosue is using bins or not.
    iUseBins = gvCheckNull(moClass.moAppDB.Lookup("UseBins", "timWarehouse", "WhseKey = " & Format$(mlWhseKey)), SQL_INTEGER)

    'Check if warehouse is tracking by bin or not.
    iTrackQtyAtBin = gvCheckNull(moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse", "WhseKey = " & Format$(mlWhseKey)), SQL_INTEGER)

    mbUseBins = False
    mbTrackQtyAtBin = False

    'If warehouse is not using bins, then disable the Preferred Bins tab.
    If iUseBins = 1 Then
        mbUseBins = True

        If bPreferredBinsExist(PrefBinTypes.Distribution) Then
            chkPickPrefBinsFirst.Enabled = True
        Else
            chkPickPrefBinsFirst.Value = 0
            chkPickPrefBinsFirst.Enabled = False
        End If
        
    Else
        chkPickPrefBinsFirst.Value = 0
        chkPickPrefBinsFirst.Enabled = False
        chkPickOneLotOnly.Enabled = False
        
        Exit Sub
    End If

    If iTrackQtyAtBin = 1 Then
        mbTrackQtyAtBin = True
    End If
    
    'If tracking quantity at bin, don't allow default bin to be selected in preferred bins
    'else allow Default Bin to be selected.
    If mbTrackQtyAtBin Then
        lkuPrefBinDist.RestrictClause = "WhseKey = " & Format$(mlWhseKey)
        lkuPrefBinMfg.RestrictClause = "WhseKey = " & Format$(mlWhseKey)
    Else
        lkuPrefBinDist.RestrictClause = "WhseKey = " & Format$(mlWhseKey) & " AND WhseBinKey IN (select wb.WhseBinKey from timWhseBin wb WITH (NOLOCK) where wb.DfltBin = 0)"
        lkuPrefBinMfg.RestrictClause = "WhseKey = " & Format$(mlWhseKey) & " AND WhseBinKey IN (select wb.WhseBinKey from timWhseBin wb WITH (NOLOCK) where wb.DfltBin = 0)"
    End If
    
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "RestrictPrefBins", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub DisplayDefaultRanks()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************
' Description:
'    This routine displays default Ranks, i.e. 'X' for
'    all Rank Types.
'*******************************************************
    ddnCOSRank.ListIndex = ddnCOSRank.ListCount - 1
    ddnGrMgnRank.ListIndex = ddnGrMgnRank.ListCount - 1
    ddnHitsRank.ListIndex = ddnHitsRank.ListCount - 1
    ddnQtySoldRank.ListIndex = ddnQtySoldRank.ListCount - 1
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisplayDefaultRanks", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub DisplayPPLDefaults(ByVal bNewRecord As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*******************************************************
' Description:
'    This routine displays DemandCalcID, LeadTimeCalcID,
'    SafetyStockCalcID for a Source PPL defined.
'*******************************************************
    Dim rs                  As Object
    Dim sSQL                As String
    Dim iItemType           As Integer
    Dim lPPLKey             As Long
    Dim lCycleKey           As Long
    Dim iReOrderMeth        As Integer
    Dim bBuyerSet           As Boolean

    sSQL = " Select DemandCalcKey, LeadTimeCalcKey, SafetyStockCalcKey FROM timPurchProdLine WITH (NOLOCK) where PurchProdLineKey = " & Format$(mlPPLKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If Not rs Is Nothing Then
        If rs.RecordCount > 0 Then
            If rs.LongField("DemandCalcKey") > 0 Then
                If mbIRIsActivated Then
                    lkuDemand.KeyValue = rs.LongField("DemandCalcKey")
                End If

                nbrDemand.Enabled = False
            Else
                If mbIRIsActivated Then
                    lkuDemand.KeyValue = Empty
                    nbrDemand.Enabled = True
                End If
            End If
            
            If rs.LongField("LeadTimeCalcKey") > 0 Then
                lkuLeadTime.KeyValue = rs.LongField("LeadTimeCalcKey")
                txtLeadTime.Enabled = False
                vsLeadTime.Enabled = False
            Else
                lkuLeadTime.KeyValue = Empty
                txtLeadTime.Enabled = True
                vsLeadTime.Enabled = True
            End If
            
            If rs.LongField("SafetyStockCalcKey") > 0 Then
                lkuSafetyStock.KeyValue = rs.LongField("SafetyStockCalcKey")
                nbrSafetyStockQty.Enabled = False
            Else
                lkuSafetyStock.KeyValue = Empty
                
                If mbIRIsActivated Then
                    nbrSafetyStockQty.Enabled = True
                End If
            End If
        End If
        
        rs.Close
        Set rs = Nothing
    End If

    sSQL = "SELECT BuyerKey,OrderCycle FROM timWhsePurchProdLn WHERE WhseKey = " & Format$(mlWhseKey) & " AND PurchProdLineKey = " & Format$(mlPPLKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    If Not rs Is Nothing Then
        If rs.RecordCount > 0 Then
            If rs.LongField("BuyerKey") > 0 Then
                ddnBuyer.ItemData = rs.LongField("BuyerKey")
                bBuyerSet = True
            End If
            
            txtOrdCycle.Text = rs.Field("OrderCycle")
        Else
            txtOrdCycle.Text = ""
            If mbIRIsActivated And bNewRecord = False Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMWPPLRequired
            End If
        End If

        rs.Close
        Set rs = Nothing
    End If

    'Get buyer from the PPL when PPL/Warehouse not setup and a PPL is specified.
    If (Len(Trim(lkuPPLine.Text)) > 0 _
And bBuyerSet = False) Then
        sSQL = "SELECT BuyerKey FROM timPurchProdLine WHERE PurchProdLineKey = " & Format$(mlPPLKey)
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)

        If rs.RecordCount > 0 Then
            If rs.LongField("BuyerKey") > 0 Then
                ddnBuyer.ItemData = rs.LongField("BuyerKey")
            End If
        End If
        
        rs.Close
        Set rs = Nothing
    End If

    iItemType = giGetValidInt(moClass.moAppDB.Lookup("ItemType", "timItem", "ItemID = " & gsQuoted(Trim$(lkuItem.Text)) & " AND CompanyID = " & gsQuoted(msCompanyID)))
    lPPLKey = IIf(IsEmpty(lkuPPLine.KeyValue), 0, lkuPPLine.KeyValue)
    
    If Not IsEmpty(lkuWhse.KeyValue) Then
        If Not IsNull(moClass.moAppDB.Lookup("PhysCountCycleKey", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)) Then
            lCycleKey = moClass.moAppDB.Lookup("PhysCountCycleKey", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)
            ddnPhysCycle.ItemData = lCycleKey
        Else
            ddnPhysCycle.ListIndex = -1
        End If

        If lPPLKey <> 0 Then
            If Not IsNull(moClass.moAppDB.Lookup("ReordMeth", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)) Then
                iReOrderMeth = moClass.moAppDB.Lookup("ReordMeth", "timWhsePurchProdLn", "WhseKey = " & lkuWhse.KeyValue & " AND PurchProdLineKey = " & lPPLKey)
                ddnReordMeth.ItemData = iReOrderMeth
            End If
        End If
    End If
    
    gbSetFocus Me, ddnBuyer

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DisplayPPLDefaults", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Function bValidGLAccounts() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim oRS As Object
    Dim bFlag As Boolean

    bValidGLAccounts = False
    
    For miCount = 0 To kArrCount
        bFlag = False

        If IsEmpty(glaGLAcct(miCount).KeyValue) Then
            If Len(Trim$(glaGLAcct(miCount).Text)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeBlank, gsStripChar(Trim$(lblGLAcct(miCount)), "&")
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgGLAcct
            End If

            tabInventory.Tab = kTabGLAcct
            glaGLAcct(miCount).SetFocus
            Exit Function
        Else
            sSQL = "SELECT Status FROM tglAccount WHERE GLAcctKey = " & glaGLAcct(miCount).KeyValue
            Set oRS = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, 0)

            If Not oRS.IsEOF Then
                If oRS.Field("Status") <> 1 Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgGLActStatus, glaGLAcct(miCount).MaskedText
                    tabInventory.Tab = kTabGLAcct
                    glaGLAcct(miCount).SetFocus
                    Exit Function
                Else
                    If Not bIsValidGLFinancialAcct(miCount) Then Exit Function
                End If
            End If
        End If
    Next miCount

    bValidGLAccounts = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidGLAccounts", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Function bBinAlreadyExists(lRow As Long, tPrefBinType As PrefBinTypes) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lCurrRow As Long
    Dim lCol As Long
    Dim oGrid As Object
    
    
    bBinAlreadyExists = False

    Select Case tPrefBinType
        Case PrefBinTypes.Distribution
            Set oGrid = grdPrefBinDist
            lCol = kColBinIDDist
        Case PrefBinTypes.Manufacturing
            Set oGrid = grdPrefBinMfg
            lCol = kColBinIDMfg
    End Select
    
    For lCurrRow = 1 To oGrid.MaxRows - 1
        If lCurrRow <> lRow Then
            If UCase(gsGridReadCell(oGrid, lRow, lCol)) = UCase(gsGridReadCell(oGrid, lCurrRow, lCol)) Then
                bBinAlreadyExists = True
                Set oGrid = Nothing
                Exit Function
            End If
        End If
    Next lCurrRow
    
    Set oGrid = Nothing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bBinAlreadyExists", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnClick CustomDate(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnDblClick CustomDate(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnGotFocus CustomDate(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnLostFocus CustomDate(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnKeyPress CustomDate(Index), KeyAscii
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not moFormCust Is Nothing Then
        moFormCust.OnChange CustomDate(Index)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
#End If

Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Sub RefreshPickMethodDropDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'
'   Initialize all of the picking controls, and remove the two lot related values from the
'   PickMethod combo box if this item doesn't track Lots.
'
    Dim iIndex As Integer
    
    SetPickControls
    
    ddnPickMethod.Refresh
    If (miItemTrackMethod <> kLotItem) And (miItemTrackMethod <> kLotSerialItem) Then
        iIndex = ddnPickMethod.GetIndexByItemData(kOldestLot)
        ddnPickMethod.RemoveItem (iIndex)
        iIndex = ddnPickMethod.GetIndexByItemData(kMinimizeLot)
        ddnPickMethod.RemoveItem (iIndex)
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "RefreshPickMethodDropDown", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetPickControls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'Repository Error Rig  {1.1.1.0.0}

    If glGetValidLong(lkuItem.KeyValue) = 0 Then Exit Sub

    miItemTrackMethod = giGetValidInt(moClass.moAppDB.Lookup("TrackMeth", "timItem", "timItem.ItemKey = " & glGetValidLong(lkuItem.KeyValue)))
    mbTrackQtyAtBin = gbGetValidBoolean(gvCheckNull(moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse", "WhseKey = " & Format$(mlWhseKey)), SQL_INTEGER))

    If (Not mbTrackQtyAtBin) Then
        chkPickOneLotOnly.Value = 0
        chkPickOneLotOnly.Enabled = False
        chkSerialNoLinesOnPickList.Enabled = False
    Else
        pnlTab(3).Enabled = True

        If ((miItemTrackMethod = kSerialItem) _
        Or (miItemTrackMethod = kLotSerialItem)) Then
            chkSerialNoLinesOnPickList.Enabled = True
        Else
            chkSerialNoLinesOnPickList.Enabled = False
        End If

        If ((miItemTrackMethod = kLotItem) _
        Or (miItemTrackMethod = kLotSerialItem)) Then
            If ((ddnPickMethod.ItemData = kMinimizeLot) _
            Or (ddnPickMethod.ItemData = kOldestLot)) Then
                chkPickOneLotOnly.Enabled = True
            Else
                chkPickOneLotOnly.Value = 0
                chkPickOneLotOnly.Enabled = False
            End If
        Else
            chkPickOneLotOnly.Value = 0
            chkPickOneLotOnly.Enabled = False
        End If
    End If

    If ((miItemTrackMethod = kLotItem) _
    Or (miItemTrackMethod = kLotSerialItem)) Then
        If ((ddnPickMethod.ItemData = kMinimizeLot) _
        Or (ddnPickMethod.ItemData = kOldestLot)) Then
            chkShowAvailLots.Value = 0
            chkShowAvailLots.Enabled = False
        Else
            chkShowAvailLots.Enabled = True
        End If
    Else
        chkShowAvailLots.Value = 0
        chkShowAvailLots.Enabled = False
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetPickControls", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupNavMain()
    
    Dim sNavRestrict As String
    
     If Len(Trim(navMain.Tag)) = 0 Then
        'Setting the bottom navigator
        sNavRestrict = "CompanyID = " & gsQuoted(msCompanyID) & _
                        " AND WhseKey IN (SELECT WhseKey from timWarehouse WITH (NOLOCK) WHERE Transit <> " & kWhseType_Transit & ")"
        Call gbLookupInit(navMain, moClass, moClass.moAppDB, "Inventory", sNavRestrict)
        navMain.Tag = navMain.LookupID
    End If

End Sub

Private Function bPrimaryVendorSaveProcessing() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

    '**************************************************************************
    '* This function validates that the screen state related to primary vendor
    '* is valid for a save.  It also moves the primary vendor lookup keyvalue
    '* to the form if the primary vendor is to be saved on the inventory
    '* record.  The primary vendor lookup is not bound to the form because
    '* it may come from more than one source, and may or may not be saved on
    '* the inventory record.
    '*************************************************************************


    'If IR is activated and a PPL is specified, then the user must either check the WPPL vendor checkbox or specify a vendor.
    'Note that this has the same data state as no primary vendor and the checkbox checked, but this is done to make the user
    'explicitly state this.  This helps avoid one data state having two screen states.
    If (mbIRIsActivated = True) And (chkUseWPPLPrimaryVendor.Value = vbUnchecked) And _
        (glGetValidLong(lkuPrimaryVendor.KeyValue) = 0) And (glGetValidLong(lkuPPLine.KeyValue) > 0) Then
    
        giSotaMsgBox Me, moClass.moSysSession, kmsgIMInventoryPrimaryVendor
                
'+++ VB/Rig Begin Pop +++
        Exit Function
    End If

    'Set the primary vendor if there is a primary vendor in the lookup and the WPPL checkbox is unchecked
    'and there is no source warehouse specified.  If this is NOT the case, then a primary vendor should
    'not be stored on the inventory record.
    If (chkUseWPPLPrimaryVendor.Value = vbUnchecked And glGetValidLong(lkuPrimaryVendor.KeyValue) > 0) And _
        glGetValidLong(lkuWhseFrom.KeyValue) = 0 Then
        moDmForm.SetColumnValue "PrimaryVendKey", lkuPrimaryVendor.KeyValue
    Else
        moDmForm.SetColumnValue "PrimaryVendKey", ""
    End If

    bPrimaryVendorSaveProcessing = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bPrimaryVendorSaveProcessing", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub moGMPrefBinMfg_CellChange(ByVal lRow As Long, ByVal lCol As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sValidBinStr As String
    Dim lWhseBinKey As Long
    Dim sWhseBinID As String
    Dim sOldWhseBinID As String
    Dim bInvalidBin As Boolean
    
    mbAllowSave = True

    If lCol <> kColBinIDMfg Then
        Exit Sub
    End If

    If Me.ActiveControl.Name = lkuPrefBinMfg.Name Then
        Exit Sub
    End If
    
    'We've got the WhseBinID, now lookup the WhseBinKey.
    sWhseBinID = gsGridReadCell(grdPrefBinMfg, lRow, lCol)
    
    sValidBinStr = "WhseBinID = " & gsQuoted(sWhseBinID)
    sValidBinStr = sValidBinStr & " AND WhseKey = " & Format$(mlWhseKey)
    sValidBinStr = sValidBinStr & " AND Status = " & kItemStatus_Active
    sValidBinStr = sValidBinStr & " AND Type <> " & kRandomBin
    sValidBinStr = sValidBinStr & " AND TempBin <> " & 1
    
    lWhseBinKey = gvCheckNull(moClass.moAppDB.Lookup _
            ("WhseBinKey", "timWhseBin", sValidBinStr), SQL_INTEGER)
        
    If (lWhseBinKey = 0 _
        Or bBinAlreadyExists(lRow, PrefBinTypes.Manufacturing)) Then
        mbAllowSave = False
        moDmGridPrefBinMfg.SetColumnValue lRow, "WhseBinKey", 0
        sOldWhseBinID = gsGridReadCell(grdPrefBinMfg, lRow, kColOldWhseBinIDMfg)
            
        If grdPrefBinMfg.Tag <> "1" Then
            If Not mbMessageDisplayed Then
                grdPrefBinMfg.Tag = "1"
                    
                If bBinAlreadyExists(grdPrefBinMfg.ActiveRow, PrefBinTypes.Manufacturing) Then
                    giSotaMsgBox Me, moClass.moSysSession, kIMBinAlreadySelected
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidWhseBin
                    bInvalidBin = True
                End If
            End If
        Else
            grdPrefBinMfg.Tag = "0"
        End If
            
        mbMessageDisplayed = False
        If grdPrefBinMfg.Enabled Then ' If focus is still on this tab stay in this cell, otherwise you can't.
            gGridSetActiveCell grdPrefBinMfg, lRow, lCol
        End If
        mbValidBins = False
            
        If Len(Trim$(sOldWhseBinID)) = 0 Then
            If lRow = grdPrefBinMfg.MaxRows - 1 Then
                moDmGridPrefBinMfg.DeleteBlock lRow
            End If
        Else
            gGridUpdateCellText grdPrefBinMfg, lRow, kColBinIDMfg, sOldWhseBinID
            
            sValidBinStr = "WhseBinID = " & gsQuoted(sOldWhseBinID)
            sValidBinStr = sValidBinStr & " AND WhseKey = " & Format$(mlWhseKey)
            sValidBinStr = sValidBinStr & " AND Status = " & kItemStatus_Active
            sValidBinStr = sValidBinStr & " AND Type <> " & kRandomBin
                
            lWhseBinKey = gvCheckNull(moClass.moAppDB.Lookup _
                    ("WhseBinKey", "timWhseBin", sValidBinStr), SQL_INTEGER)
                
            If lWhseBinKey <> 0 Then
                moDmGridPrefBinMfg.SetColumnValue lRow, "WhseBinKey", lWhseBinKey
            End If
        End If
    Else
        gGridUpdateCell grdPrefBinMfg, lRow, kColOldWhseBinIDMfg, sWhseBinID
        moDmGridPrefBinMfg.SetColumnValue lRow, "WhseBinKey", lWhseBinKey
        sValidBinStr = gvCheckNull(moClass.moAppDB.Lookup("Location", "timWhseBin", "WhseBinKey = " & lWhseBinKey), 1)
        gGridUpdateCell grdPrefBinMfg, lRow, kColBinLocationMfg, sValidBinStr
        moDmGridPrefBinMfg.SetColumnValue lRow, "PrefBinType", PrefBinTypes.Manufacturing
        mbValidBins = True
    End If
        
    lkuPrefBinMfg.Visible = False
        
    If bInvalidBin And grdPrefBinMfg.Enabled Then
        gGridSetActiveCell grdPrefBinMfg, lRow, lCol
        lkuPrefBinMfg.Visible = True
        lkuPrefBinMfg.SetFocus
        grdPrefBinMfg.SetFocus
    End If
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMPrefBinMfg_CellChange", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMPrefBinMfg_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bContinue = False
    
    If tabInventory.Tab = kTabPrefBin Then
        If grdPrefBinMfg.ActiveRow = grdPrefBinMfg.MaxRows Then
            Exit Sub
        End If

        If Not bMoveRecords(PrefBinTypes.Manufacturing) Then
            Exit Sub
        End If
    End If

    bContinue = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMPrefBinMfg_GridBeforeDelete", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moGMPrefBinMfg_GridAfterDelete()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moDmForm.SetDirty True
    lkuPrefBinMfg.Visible = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGMPrefBinMfg_GridAfterDelete", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuPrefBinMfg_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lActiveRow As Long
    Dim lActiveCol As Long
   
    If bDenyControlFocus Then
        Exit Sub
    End If
   
    'Launch the navigator
    lActiveRow = glGridGetActiveRow(grdPrefBinMfg)
    lActiveCol = glGridGetActiveCol(grdPrefBinMfg)
    
    If lActiveRow > 0 And lActiveCol > 0 Then
        txtPrefBinMfg.Text = gsGridReadCellText(grdPrefBinMfg, lActiveRow, lActiveCol)
    End If
    
    Call gcLookupClick(Me, lkuPrefBinMfg, txtPrefBinMfg, "WhseBinID")
   
    moGMPrefBinMfg.LookupClicked
    mbF5Down = False
   
    grdPrefBinMfg.Tag = "0"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuPrefBinMfg_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGridPrefBinMfg_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If bBinAlreadyExists(lRow, PrefBinTypes.Manufacturing) Then
        mbMessageDisplayed = True
        giSotaMsgBox Me, moClass.moSysSession, kIMBinAlreadySelected
        bValid = False
        
        tabInventory.Tab = kTabPrefBin
        
        If grdPrefBinMfg.Enabled Then
            grdPrefBinMfg.SetFocus
        End If

        gGridSetSelectRow grdPrefBinMfg, lRow
        gGridSetActiveCell grdPrefBinMfg, lRow, kColBinIDMfg
        
        Exit Sub
    End If
    
    moDmGridPrefBinMfg.SetColumnValue lRow, "PrefNo", lRow
    moDmGridPrefBinMfg.SetColumnValue lRow, "WhseKey", moDmForm.GetColumnValue("WhseKey")
    moDmGridPrefBinMfg.SetColumnValue lRow, "ItemKey", moDmForm.GetColumnValue("ItemKey")
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmGridPrefBinMfg_DMGridBeforeInsert", VBRIG_IS_FORM  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub moDmGridPrefBinMfg_DMGridValidate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bValid = True
    
    If (Not mbAllowSave _
        And grdPrefBinMfg.ActiveRow <> grdPrefBinMfg.MaxRows) Then
        If mbUseBins Then
            mbMessageDisplayed = True
            
            If bBinAlreadyExists(lRow, PrefBinTypes.Manufacturing) Then
                giSotaMsgBox Me, moClass.moSysSession, kIMBinAlreadySelected
            Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, "Bin"
            End If
            
            bValid = False
            
            If grdPrefBinMfg.ActiveRow = grdPrefBinMfg.MaxRows - 1 Then
                moDmGridPrefBinMfg.DeleteBlock grdPrefBinMfg.ActiveRow
            End If
        End If
        
        Exit Sub
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moDmGridPrefBinMfg_DMGridValidate", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bPreferredBinsExist(tPrefBinType As PrefBinTypes) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '*********************************************************************
    '* Determines if a type of preferred bin exists for an inventory item.
    '*********************************************************************

    bPreferredBinsExist = glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "timInvtBinList WITH (NOLOCK)", "WhseKey = " & mlWhseKey & _
        " AND ItemKey = " & mlItemKey & " AND PrefBinType = " & tPrefBinType))

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bPreferredBinsExist", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bDeleteMfgBinEntries() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '**************************************************************************
    '* This function is designed to delete any manufacturing preferred bins.
    '* If any exist, the user will be prompted to confirm.
    '*
    '* This function will return true if no manufacturing preferred bins exist
    '* and will also return true if they exist and the user confirms the
    '* deletion.
    '*
    '* This function will return false if manufacturing preferred bins exist
    '* and the user chooses not to delete them.
    '**************************************************************************
    
    Dim lRows As Long
    Dim iResponse As Integer
    
    lRows = grdPrefBinMfg.MaxRows - 1
    
    If lRows > 0 Then
    
        'Ask the user if they wish to continue because if they do all mfg pref bins will be deleted.
        iResponse = giSotaMsgBox(Me, moClass.moSysSession, kmsgIMDeleteMfgPrefBin)
    
        'If the user said yes, delete all mfg bins in the grid.
        If iResponse = kretYes Then
            moDmGridPrefBinMfg.DeleteBlock 1, lRows
            bDeleteMfgBinEntries = True
        End If
        
    Else
        bDeleteMfgBinEntries = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bDeleteMfgBinEntries", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetDistPreferredBinsFrame()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '**************************************************************************
    '* This procedure will set the proper enabled settings of the following
    '* controls:
    '*
    '*      Dist Preferred Bins Frame (and grid)
    '*      Use Dist Bins in Mfg checkbox
    '*      Mfg Preferred Bins Frame (and grid)
    '*
    '* This call needs to be made after all various attributes have been set,
    '* such as the warhouse using bins, item type, etc.
    '**************************************************************************

    Dim bPanelEnabled As Boolean
    
    bPanelEnabled = pnlTab(kTabPrefBin).Enabled
    
    'The panel is a parent control to the frames.  If it is not enabled, then the enabled logic for
    'the frames and checkbox will not work.  Enable the panel.
    If bPanelEnabled = False Then
        pnlTab(kTabPrefBin).Enabled = True
    End If
    
    'Enable distribution fram if the warehouse involved uses bins and the item involved is not a BTO kit.
    fraPreferredBinDist.Enabled = (mbUseBins = True And mbBTOKit = False)
    If fraPreferredBinDist.Enabled = False Then
        grdPrefBinDist.ActiveCellHighlightStyle = ActiveCellHighlightStyleOff
    End If
    
    'Enable the checkbox if the the warehouse involved uses bins and the proper modules are activated and the distribution frame is enabled.
    chkUseDistBinsInMfg.Enabled = (mbUseBins = True And (mbMFActivated = True Or mbAMActivated = True) And fraPreferredBinDist.Enabled = True)
    
    SetMfgPreferredBinsFrame
    
    'Return the panel to its original state.
    pnlTab(kTabPrefBin).Enabled = bPanelEnabled
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetDistPreferredBinsFrame", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetMfgPreferredBinsFrame()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    '**************************************************************************
    '* This procedure will set the proper enabled setting of the manufacutring
    '* preferred bins frame.  This frame will always be disabled when the
    '* distribution preferred bins frame is disabled, and my be enabled when
    '* the distribution preferred bins frame is enabled.
    '*
    '* This should be called whenever the item/warehouse changes, or when the
    '* Use Distribution Preferred Bins in Manufacturing checkbox changes.
    '**************************************************************************

    If fraPreferredBinDist.Enabled = True And chkUseDistBinsInMfg.Enabled = True Then
        If chkUseDistBinsInMfg.Value = vbChecked Then
            fraPreferredBinMfg.Enabled = False
            grdPrefBinMfg.ActiveCellHighlightStyle = ActiveCellHighlightStyleOff
        Else
            fraPreferredBinMfg.Enabled = True
        End If
    Else
        fraPreferredBinMfg.Enabled = False
        grdPrefBinMfg.ActiveCellHighlightStyle = ActiveCellHighlightStyleOff
    End If
    
    
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetMfgPreferredBinsFrame", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bIsValidGLFinancialAcct(Index As Integer) As Boolean
'*******************************************************************************************************
' Desc: Run Standard Validation against the database to validate the Selected Account is Financial tyoe
'*******************************************************************************************************

'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
bIsValidGLFinancialAcct = False

    If Not glaGLAcct(Index).IsValid Then
          giSotaMsgBox Me, moClass.moSysSession, kmsgGLValidationErrMsg, glaGLAcct(Index).ErrorMsg
          tabInventory.Tab = kTabGLAcct
          gbSetFocus Me, glaGLAcct(Index)
          Exit Function
    End If
    
    bIsValidGLFinancialAcct = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidGLFinancialAcct", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'SGS DEJ IA (START)
Function IsPaceReqrd() As Boolean
    On Error Resume Next
    Dim rs As DASRecordSet
    Dim SQL As String
    Dim i As Long
    
    'The Placard is required when:
    '1 - The item is a finished good ItemType = 5
    '2 - The Item Class is one of the following:
        'AC
        'CutBack
        'DiluteEmuls
        'Emulsion
        'PMA
    
    SQL = "Select * From timItem with(NoLock) Where ItemType = 5 And ItemClassKey In(Select ItemClassKey From timItemClass Where ItemClassID in('AC', 'Cutback', 'Dilute Emuls', 'Emulsion', 'PMA')) And ItemKey = " & lkuItem.KeyValue
    
    Set rs = moClass.moAppDB.OpenRecordset(SQL, kSnapshot, kOptionNone)
    
    Do While Not rs.IsEOF
        i = glGetValidLong(rs.Field("ItemKey"))
        rs.MoveNext
    Loop
    
    rs.Close
    Set rs = Nothing
    
    If i > 0 Then
        IsPaceReqrd = True
    Else
        IsPaceReqrd = False
    End If
    
'    If glGetValidLong(moClass.moAppDB.LookupSys("Count(*)", "timItem", "Where ItemType = 5 And ItemClassKey In(Select ItemClassKey From timItemClass Where ItemClassID in('AC', 'Cutback', 'Dilute Emuls', 'Emulsion', 'PMA')) And ItemKey = " & lkuItem.KeyValue)) > 0 Then
'        IsPaceReqrd = True
'    Else
'        IsPaceReqrd = False
'    End If
    
End Function
'SGS DEJ IA (STOP)

'**************************************************************************
'SGS DEJ 9/7/12 (Make sure that a temperature exists) (START)
'**************************************************************************
Private Function bIsTempValid() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'Defalut to true
    bIsTempValid = True
    
    'Does the item need a temerature?
    'vluIMPlacardItemsRequiringTemp_SGS
'    lKey = glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "vluIMPlacardItemsRequiringTemp_SGS", "CompanyID = " & gsQuoted(msCompanyID) & " AND ItemKey = " & mlItemKey))
    If 0 >= glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "vluIMPlacardItemsRequiringTemp_SGS", "CompanyID = " & gsQuoted(msCompanyID) & " AND ItemKey = " & glGetValidLong(lkuItem.KeyValue))) Then
        'Temperature is not required.
        Exit Function
    End If
    
    'Make sure that there is a temperature before saving:
    If nbrTemp.Value = 0 Then
        bIsTempValid = False
        MsgBox "This item requires a temperature for the Placard/Specs .", vbExclamation, "MAS 500"
        Exit Function
    End If
    
    'Make sure that there is a Lbs/Gal before saving.
    If nbrLbsGal.Value = 0 Then
        bIsTempValid = False
        MsgBox "This item requires a valid Lbs/Gal for the Placard/Specs .", vbExclamation, "MAS 500"
        Exit Function
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsTempValid", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
'**************************************************************************
'SGS DEJ 9/7/12 (Make sure that a temperature exists) (STOP)
'**************************************************************************



