Attribute VB_Name = "BAS1099CEL"
Option Explicit
Public mbAutomaticShow As Boolean

Const VBRIG_MODULE_ID_STRING = "1099CEL.BAS"

Public Function gbSetFocus(frm As Form, ctl As Control) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
'   Description:
'       gbsetfocus returns a boolean (True/False) value if the control
'       in question is the active control for the specified form.
'       sets focus to that control
'   Parameters:
'       frm <in> -  Form containing the control
'       ctl <in> -  control in question
'   Return Value:
'       True:   if ctl is the active control for frm.
'       False:  otherwise.
'
'   Original:   01/11/96 KMC
'   Modified:   mm/dd/yy XXX
'**************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    gbSetFocus = False
    
    If Not ctl Is Nothing Then
        ctl.SetFocus
    End If
    
    If frm.ActiveControl Is ctl Then
        gbSetFocus = True
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    If Err = 5 Then   'Invalid procedure call
        gbSetFocus = False
        gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
    	gSetSotaErr Err, sMyName, "gbSetFocus",  VBRIG_IS_MODULE
    	Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Function sGetV1099BoxDescription(oDB As Object, _
                                        lV1099Form As Long, _
                                        sV1099Box As String) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQLString As String
    Dim rsV1099Box As Object

    sGetV1099BoxDescription = Empty

    'Create SQL string and the 1099 Box recordset
    'SELECT * FROM tapV1099Box
    'WHERE V1099Form = %s AND V1099Box = %s
    sSQLString = "#apGetV1099BoxAllCols;"
    sSQLString = sSQLString & Format$(lV1099Form) & ";"
    sSQLString = sSQLString & gsQuoted(sV1099Box) & ";"
    Set rsV1099Box = oDB.OpenRecordset(sSQLString, kSnapshot, kOptionNone)

    With rsV1099Box
        'Check to see if we found the row we were looking for
        If Not .IsEmpty Then
            'Yes, we DID find the row we were looking for
            'Get the 1099 Box Desc column now
            sGetV1099BoxDescription = gsGetValidStr(.Field("BoxDesc"))
        Else
            'No, we did NOT find the row we were looking for
            sGetV1099BoxDescription = Empty
        End If
    
        'Close the 1099 Box recordset now
        .Close
        Set rsV1099Box = Nothing
    End With
    
    'Exit this function
'+++ VB/Rig Begin Pop +++
    	Exit Function

VBRigErrorRoutine:
    	gSetSotaErr Err, sMyName, "sGetV1099BoxDescription",  VBRIG_IS_MODULE
    	Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Function sBoxDescInVend1099Hist(oDB As Object, _
                                       lVendKey As Long, _
                                       iPostDateYear, _
                                       lV1099Form As Long, _
                                       sV1099Box As String) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQLString As String
    Dim rsVend1099Hist As Object

    sBoxDescInVend1099Hist = Empty
    
    'Create SQL string and the Vendor 1099 Hist recordset
    'SELECT * FROM tapVend1099Hist
    'WHERE VendKey = %s AND Year = %s
    'AND V1099Box = %s AND V1099Form = %s
    sSQLString = "#apGetVend1099HistAllCols;"
    sSQLString = sSQLString & Format$(lVendKey) & ";"
    sSQLString = sSQLString & Format$(iPostDateYear) & ";"
    sSQLString = sSQLString & gsQuoted(sV1099Box) & ";"
    sSQLString = sSQLString & Format$(lV1099Form) & ";"
    Set rsVend1099Hist = oDB.OpenRecordset(sSQLString, kSnapshot, kOptionNone)

    With rsVend1099Hist
        'Check to see if we found the row we were looking for
        If Not .IsEmpty Then
            'Yes, we DID find the row we were looking for
            'Get the Vendor 1099 Box Text column now
            sBoxDescInVend1099Hist = gsGetValidStr(.Field("V1099BoxText"))
        Else
            'No, we did NOT find the row we were looking for
            sBoxDescInVend1099Hist = Empty
        End If
    
        'Close the Vend 1099 Hist recordset now
        .Close
        Set rsVend1099Hist = Nothing
    End With
    
    'Exit this function
'+++ VB/Rig Begin Pop +++
    	Exit Function

VBRigErrorRoutine:
    	gSetSotaErr Err, sMyName, "sBoxDescInVend1099Hist",  VBRIG_IS_MODULE
    	Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function sMyName() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
sMyName = "BAS1099CEL"
'+++ VB/Rig Begin Pop +++
    	Exit Function

VBRigErrorRoutine:
    	gSetSotaErr Err, sMyName, "sMyName",  VBRIG_IS_MODULE
    	Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
