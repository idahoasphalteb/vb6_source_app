VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Begin VB.Form frmTaggedOrders 
   Caption         =   "PO Tagged Orders"
   ClientHeight    =   5610
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9285
   Icon            =   "frmTaggedOrders.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5610
   ScaleWidth      =   9285
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtItemDesc 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   720
      Width           =   2775
   End
   Begin VB.TextBox txtRemainQty 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6240
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   1200
      Width           =   1215
   End
   Begin VB.TextBox txtTaggedQty 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6240
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox txtUOM 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   7680
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   240
      Width           =   1215
   End
   Begin VB.TextBox txtPurchQty 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   6240
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   240
      Width           =   1215
   End
   Begin VB.TextBox txtItemID 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   720
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   720
      Width           =   975
   End
   Begin VB.TextBox txtWhse 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   720
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   240
      Width           =   2055
   End
   Begin VB.Frame fraSalesOrders 
      Caption         =   "Sales Orders"
      Height          =   3855
      Left            =   120
      TabIndex        =   0
      Top             =   1680
      Width           =   9015
      Begin FPSpreadADO.fpSpread grdSalesOrders 
         Height          =   3375
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   8535
         _Version        =   196608
         _ExtentX        =   15055
         _ExtentY        =   5953
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "frmTaggedOrders.frx":0442
      End
   End
   Begin VB.Label lblTagged 
      Caption         =   "Remaining Qty:"
      Height          =   255
      Index           =   4
      Left            =   4920
      TabIndex        =   6
      Top             =   1215
      Width           =   1215
   End
   Begin VB.Label lblTagged 
      Caption         =   "Tagged Qty:"
      Height          =   255
      Index           =   3
      Left            =   4920
      TabIndex        =   5
      Top             =   735
      Width           =   1215
   End
   Begin VB.Label lblTagged 
      Caption         =   "Purchase Qty:"
      Height          =   255
      Index           =   2
      Left            =   4920
      TabIndex        =   4
      Top             =   255
      Width           =   1215
   End
   Begin VB.Label lblTagged 
      Caption         =   "Item:"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   3
      Top             =   735
      Width           =   735
   End
   Begin VB.Label lblTagged 
      Caption         =   "Whse:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   2
      Top             =   240
      Width           =   735
   End
End
Attribute VB_Name = "frmTaggedOrders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Label2_Click()

End Sub

Private Sub lblTagged_Click()

End Sub

