Attribute VB_Name = "baspurchord"
'*********************************************
'* NOTE: This module has NOT been VB/Rigged! *
'*********************************************

Option Explicit
'**********************************************************************
'     Name: baspurchord
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 05-07-1997
'     Mods: mm/dd/yy XXX
'**********************************************************************

'-- Security Event Constants
    '-- if any security events are defined for this task, here is
    '-- where you would define the constant for the event
    'Public Const ksecChangeStatus As String = "CICHGSTAT"

'-- Object Task Constants
    '-- if this maintenance program has an associated report/listing,
    '-- here is where you would define the report/listing's Task ID so
    '-- that it may be launched when the Print button is pressed on the
    '-- toolbar
  
    'Public Const ktskPrint = 9998
Public mfrmMain                 As Form
Public mbEnterAsTab             As Boolean              ' use enter as tab?
Public msUserFld(3)             As String
Public mbDontRunGridClick       As Boolean

' tpoPurchOrder | ApprovalStatus
    Public Const kvApproveNone      As Integer = 0
    Public Const kvApproveSubmit    As Integer = 1
    Public Const kvApproveYes       As Integer = 2
    Public Const kvApproveNo        As Integer = 3

' tpoPurchOrder | CreateType
    Public Const kvCreateTypeOther  As Integer = 0
    Public Const kvCreateTypeStd    As Integer = 1
    Public Const kvCreateTypeImport As Integer = 2
    Public Const kvCreateTypeUpgrd  As Integer = 3

' tpoPurchOrder | RcvrTicketFormat
    Public Const kvRcvrTicketStd    As Integer = 0
    Public Const kvRcvrTicketBlind  As Integer = 1
    Public Const kvRcvrTicketMtrl   As Integer = 2

' tapVendor | Status
    Public Const kvActiveVend       As Integer = 1
    Public Const kvInactiveVend     As Integer = 2
    Public Const kvTemporaryVend    As Integer = 3
    Public Const kvDeletedvend      As Integer = 4

'-- Static List Constants
' timItem | Status
    Public Const kvActiveItem       As Integer = 1
    Public Const kvInactiveItem     As Integer = 2
    Public Const kvDiscontinuedItem As Integer = 3
    Public Const kvDeletedItem      As Integer = 4
    
    ' timItem | ItemType
    Public Const kvFinishedGoods    As Integer = 5
    Public Const kvRawMaterial      As Integer = 6
    Public Const kvCommentOnly      As Integer = 4
    Public Const kvExpense          As Integer = 3
    Public Const kvService          As Integer = 2
    Public Const kvMiscItem         As Integer = 1
    Public Const kvKit              As Integer = 7

    Public moUF             As Object
    Public moSTax           As Object

    Public mlUniqueKey      As Long
    
    Public Type VendDflts
        lBuyerKey           As Long
        dCreditLimit        As Double
        lVendKey            As Long
        lPurchFromAddrKey   As Long
        sPurchFromAddrID    As String
        sPurchFromAddrName  As String
        lDfltItemKey        As Long
        lDfltPurchAcctKey   As Long
        sAcctNo             As String
        sAcctDesc           As String
        lRemitToAddrKey     As Long
        sRemitToAddrID      As String
        sRemitToAddrName    As String
        iHold               As Integer
        lPrimaryAddrKey     As Long
        dRetntRate          As Double
        iStatus             As Integer
        lPmtTermsKey        As Long
        sPOFormID           As String
        lPOFormKey          As Long
        iBlankFormType      As Integer
        lCurrExchSchdKey    As Long
        sCurrID             As String
        sDfltCntctName      As String
        lDfltCntctKey       As Long
        sShipMethID         As String
        lShipMethKey        As Long
        sShipZoneID         As String
        lShipZoneKey        As Long
        lSTaxTranKey        As Long
        lSTaxSchdKey        As Long
        sFOBID              As String
        lFOBKey             As Long
        iTaxPoint           As Integer
        sVendClassID        As String
        lVendClassKey       As Long
        sV1099Box           As String
        sV1099Control       As String
        iV1099Form          As Integer
        iV1099Type          As Integer
        lClassOvrdSegKey    As Long
        sClassOvrdSegVal    As String
        MatchKey            As Long
        iReqIssue           As Integer
    End Type
    
    Public Type ItemDflts
        lPOLineKey          As Long
        iPOLineNo           As Integer
        dQtyRcvd            As Double
        dQtyInvcd           As Double
        dQtyRtrnCred        As Double
        dQtyRtrnRplc        As Double
        dAmtInvcd           As Double
        dOrigQtyOrd         As Double
        sOrigPromiseDate    As String
        lQuoteLineKey       As Long
        lSTaxTranKey        As Long
        lSTaxSchdKey        As Long
        lUpdateCounter      As Long
        lShipZoneKey        As Long
        sShipZoneID         As String
        iAllowCostOvrd      As Integer
        iReqRcvr            As Integer
        iCommentOnly        As Integer
        lPOLineDistKey      As Long
        sUserFld1           As String
        sUserFld2           As String
        dQtyBO              As Double
        lShipToAddrKey      As Long
        iTaxPoint           As Integer
        sAddrName           As String
        sAddr1              As String
        sAddr2              As String
        sAddr3              As String
        sAddr4              As String
        sAddr5              As String
        sCity               As String
        sSTate              As String
        sCountry            As String
        sZIP                As String
        dLatitude           As Double
        dLongitude          As Double
        iInsertAddr         As Integer
        iUpdateAddr         As Integer
        lAddrUC             As Long
        iAddrForLine        As Integer
        
        dQtyOrd             As Double
        dUnitCost           As Double
        dExtAmt             As Double
        lSOLineKey              As Long
    End Type
    
    Public Type ItemWasDflts
        iStatus             As Integer
        sTargetCompID       As String
        lPOLineKey          As Long
        iPOLineNo           As Integer
        lItemKey            As Long
        lGLAcctKey          As Long
        dQtyOpen            As Double
        dQtyBackOrd         As Double
        dQtyRcvd            As Double
        dQtyInvcd           As Double
        dQtyRtrnCred        As Double
        dQtyRtrnRplc        As Double
        dAmtInvcd           As Double
        dOrigQtyOrd         As Double
        sOrigPromiseDate    As String
        iDropShip           As Integer
        lShipToCustKey      As Long
        lQuoteLineKey       As Long
        lShipToCustAddrKey  As Long
        lShipToWhseKey      As Long
        sComment            As String
        lToleranceKey       As Long
        iCloseInvc          As Integer
        iCloseRcpt          As Integer
        sCloseDate          As String
        iReqRcvr            As Integer
        lSTaxClassKey       As Long
        sSTaxClassID        As String
        lSTaxTranKey        As Long
        lSTaxSchdKey        As Long
        lAddrKey            As Long
        lUpdateCounter      As Long
        lShipMethKey        As Long
        sShipMethID         As String
        lShipZoneKey        As Long
        sShipZoneID         As String
        iAllowCostOvrd      As Integer
        iCommentOnly        As Integer
        lPOLineDistKey      As Long
        sUserFld1           As String
        sUserFld2           As String
        dFrtAmt             As Double
        dQtyBO              As Double
        lShipToAddrKey      As Long
        sAddrName           As String
        sAddr1              As String
        sAddr2              As String
        sAddr3              As String
        sAddr4              As String
        sAddr5              As String
        sCity               As String
        sSTate              As String
        sCountry            As String
        sZIP                As String
        dLatitude           As Double
        dLongitude          As Double
        lAddrUC             As Long
        
        lPurchDeptKey       As Long
        sItemDesc           As String
        dQtyOrd             As Double
        lUOMKey             As Long
        dUnitCost           As Double
        dExtAmt             As Double
        sRequestDate        As String
        sPromiseDate        As String
        iExclLeadTime       As Integer
        lExclLTReasCodeKey  As Long
        lAcctRefKey         As Long
        iExpedite           As Integer
        lExpediteReasKey    As Long
        lFOBKey             As Long
        lMatchToleranceKey  As Long
        iExclLastCost       As Integer
        lSOLineKey          As Long
    End Type
    
    Public Type MFAutoIssue
        lPOKey              As Long
        lPOLineKey          As Long
        lWorkOrderStepKey   As Long
        sStepID             As String
        sOperationDesc      As String
        lItemKey            As Long
        lWhseKey            As Long
    End Type
        
    
    Public Const kColPOPOLineNo = 1
    Public Const kColPOPOLineKey = 2
    Public Const kColPOTargetCompanyID = 3
    Public Const kColPOItemKey = 4
    Public Const kColPOItemID = 5
    Public Const kColPODescription = 6
    Public Const kColPOQtyOrdered = 7
    Public Const kColPOUnitMeasKey = 8
    Public Const kColPOUnitMeasID = 9
    Public Const kColPOUnitCost = 10
    Public Const kColPOCmntOnly = 11
    Public Const kColPOExtAmt = 12
    Public Const kColPOExtCmnt = 13
    Public Const kColPOQuoteLineKey = 14
    Public Const kColPOUnmatch = 15
    Public Const kColPOStatus = 16
    Public Const kColPOGLAcctKey = 17
    Public Const kColPOOrigPromiseDate = 18
    Public Const kColPODropShip = 19
    Public Const kColPOShipToCustKey = 20
    Public Const kColPOCustID = 21
    Public Const kColPOShipToCustAddrKey = 22
    Public Const kColPOCustAddrID = 23
    Public Const kColPOShipToWhseKey = 24
    Public Const kColPOWhseID = 25
    Public Const kColPOAddrName = 26
    Public Const kColPOAddr1 = 27
    Public Const kColPOAddr2 = 28
    Public Const kColPOAddr3 = 29
    Public Const kColPOAddr4 = 30
    Public Const kColPOAddr5 = 31
    Public Const kColPOCity = 32
    Public Const kColPOSTate = 33
    Public Const kColPOCountry = 34
    Public Const kColPOZIP = 35
    Public Const kColPOLatitude = 107
    Public Const kColPOLongitude = 108
    Public Const kColPOInsertAddr = 36
    Public Const kColPOUpdateAddr = 37
    Public Const kColPOAddrForLine = 38
    Public Const kColPOAddrUC = 39
    Public Const kColPOUOMType = 40
    Public Const kColPOToleranceKey = 41
    Public Const kColPOToleranceID = 42
    Public Const kColPOQtyOpen = 43
    Public Const kColPOCloseRcpt = 44
    Public Const kColPOCloseDate = 45
    Public Const kColPOReqRcvr = 46
    Public Const kColPOSTaxTranKey = 47
    Public Const kColPOSTaxClassKey = 48
    Public Const kColPOSTaxClassID = 49
    Public Const kColPOPurchDeptKey = 50
    Public Const kColPOPurchDeptID = 51
    Public Const kColPOFreightAmt = 52
    Public Const kColPOSTaxAmt = 53
    Public Const kColPOCloseInvc = 54
    Public Const kColPOExclLeadTime = 55
    Public Const kColPOExclLTRsnKey = 56
    Public Const kColPOExclLTRsnID = 57
    Public Const kColPOSTaxSchdKey = 58
    Public Const kColPOFOBKey = 59
    Public Const kColPOFOBID = 60
    Public Const kColPOExpedite = 61
    Public Const kColPOExpRsnKey = 62
    Public Const kColPOExpRsnID = 63
    Public Const kColPOShipToAddrKey = 64
    Public Const kColPOAcctRefCode = 65
    Public Const kColPOExclLastCost = 66
    Public Const kColPOShipMethKey = 67
    Public Const kColPOShipMethID = 68
    Public Const kColPOShipZoneKey = 69
    Public Const kColPOShipZoneID = 70
    Public Const kColPOAllowCostOvrd = 71
    Public Const kColPOQtyRtrnRplc = 72
    Public Const kColPOUserFld1 = 73
    Public Const kColPOUserFld2 = 74
    Public Const kColPORequestDate = 75
    Public Const kColPOPromiseDate = 76
    Public Const kColPOPromiseDateDirty = 77
    Public Const kColPOOrigQtyOrd = 78
    Public Const kColPOQtyRcvd = 79
    Public Const kColPOQtyInvcd = 80
    Public Const kColPOAmtInvcd = 81
    Public Const kColPOQtyRtrnCred = 82
    Public Const kColPOQtyBackOrd = 83
    Public Const kColPOGLAcctID = 84
    Public Const kColPOGLAcctIDMsk = 85
    Public Const kColPOGLAcctDesc = 86
    Public Const kColPOPAChange = 87
    Public Const kColPOTaxPoint = 88
    Public Const kColPOStatusText = 89
    Public Const kColPOSOLineKey = 90
    Public Const kcolPOTagged = 91
    Public Const kColPOUnitCostExact = 106 'Stores Un-rounded Unit Cost
    
    Public Const kColProjectID = 92
    Public Const kColProjectKey = 93
    Public Const kColPhaseID = 94
    Public Const kColPhaseKey = 95
    Public Const kColTaskID = 96
    Public Const kColTaskKey = 97
    Public Const kColCostClassID = 98
    Public Const kColCostClassKey = 99
    Public Const kColVarianceStr = 100
    Public Const kColVariance = 101
    Public Const kColJobLineKey = 102
    Public Const kColPOFASAssetTemplate = 103 'FAS Integration
    Public Const kColBillMeth = 104 'not used just need for clsProject
    Public Const kColBillMethKey = 105 'not used just need for clsProject
    Public Const kMaxCols = 108

    
    Public Const kChildPOMaxCols = 46
    Public Const kColChildPOLineKey = 1
    Public Const kColChildPOLineDistKey = 2
    Public Const kColChildAmtInvcd = 3
    Public Const kColChildGLAcctKey = 4
    Public Const kColChildGLAcctNo = 5
    Public Const kColChildGLAcctDesc = 6
    Public Const kColChildOrigOrdered = 7
    Public Const kColChildOrigPromiseDate = 8
    Public Const kColChildPromiseDate = 9
    Public Const kColChildQtyInvcd = 10
    Public Const kColChildQtyOnBO = 11
    Public Const kColChildQtyOrd = 12
    Public Const kColChildQtyRcvd = 13
    Public Const kColChildQtyRtrnCred = 14
    Public Const kColChildRequestDate = 15
    Public Const kColChildShipMethKey = 16
    Public Const kColChildShipMethID = 17
    Public Const kColChildExtAmt = 18
    Public Const kColChildShipToCustAddrKey = 19
    Public Const kColChildShipToCustAddrID = 20
    Public Const kColChildShipToCustKey = 21
    Public Const kColChildShipToCustID = 22
    Public Const kColChildDropShip = 23
    Public Const kColChildShipToWhseKey = 24
    Public Const kColChildShipToWhseID = 25
    Public Const kColChildShipZoneKey = 26
    Public Const kColChildShipZoneID = 27
    Public Const kColChildQtyRtrnRplc = 28
    Public Const kColChildSTaxTranKey = 29
    Public Const kColChildFrtAmt = 30
    Public Const kColChildQtyOpen = 31
    Public Const kColChildCloseInvc = 32
    Public Const kColChildCloseRcpt = 33
    Public Const kColChildExclLeadTime = 34
    Public Const kColChildExclLTRsnKey = 35
    Public Const kColChildSTaxSchdKey = 36
    Public Const kColChildPurchDeptKey = 37
    Public Const kColChildFOBKey = 38
    Public Const kColChildExpedite = 39
    Public Const kColChildExpRsnKey = 40
    Public Const kColChildShipToAddrKey = 41
    Public Const kColChildStatus = 42
    Public Const kColChildAcctRefKey = 43
    Public Const kColChildAcctRefID = 44
    Public Const kColChildTaxPoint = 45
    Public Const kColChildFASAssetTemplate = 46 'FAS Integration
    
    'Auto Issues column Const
    Public Const kIssueMaxCols = 7
    Public Const kColIssuePOLineKey = 1
    Public Const kColIssuePOKey = 2
    Public Const kColIssueWorkOrderStepKey = 3
    Public Const kColIssueStepID = 4
    Public Const kColIssueOperation = 7
      
    Public msCurrSymbol         As String
    Public msHomeCurrSymbol     As String
    Public miDigAfterDec        As Integer
    Public miHomeDigAfterDec    As Integer
    Public miRoundPrec          As Integer
    Public miHomeRoundPrec      As Integer
    Public miRoundMeth          As Integer
    Public miHomeRoundMeth      As Integer
    Public mlCurrencyLocale     As Long
    Public mlHomeCurrencyLocale As Long

#If Prototype <> 1 Then
Const VBRIG_MODULE_ID_STRING = "PURCHORD.BAS"

Public Sub SetCountryMasks(frm As Form, sCountry As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'  Desc:  Sets up Masks for phone, fax and postal code based on the country
'  Parms: Country
'***********************************************************************

    Dim sSQL        As String   'SQL String
    Dim rs          As New DASRecordSet   'SOTADAS Recordset Object
    Dim sZipMask    As String   'Postal Code
    Dim sZIP        As String   'Postal Code
        
       sZIP = frm.txtPostalCode.Text
    'Default Country Use Default Masks
       If Len(Trim(sCountry)) = 0 Then
          frm.txtPostalCode.Mask = ""
      
      Else                                        'Get masks from the database
          
          sSQL = "SELECT * FROM tsmCountry WHERE CountryID = "
          sSQL = sSQL & gsQuoted(sCountry)
          
          Debug.Print sSQL
          
          Set rs = frmPurchOrd.moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
          
        'Masks found load variables
          If Not rs.IsEOF Then
              sZipMask = gsGetValidStr(rs.Field("PostalCodeMask"))
          End If
          
        'Load Masks
          frm.txtPostalCode.Mask = sZipMask
      
        'Clean Up Sage MAS 500 DAS Recordset Object
            If Not rs Is Nothing Then
                rs.Close
                Set rs = Nothing
            End If
      
      End If
       
       frm.txtPostalCode.Text = sZIP
       frm.txtPostalCode.Tag = frm.txtPostalCode.Text

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetCountryMasks", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Function lGetNextSurrogateKey(oDB As Object, tblName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lKeyVal As Long
    
    lKeyVal = 0
    With oDB
        .SetInParam tblName
        .SetOutParam lKeyVal
        .ExecuteSP ("spGetNextSurrogateKey")
        lKeyVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    lGetNextSurrogateKey = lKeyVal
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lGetNextSurrogateKey", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sText As String

    If lErr = guSotaErr.Number And Trim(guSotaErr.Description) <> Trim(sDesc) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If

    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyID
        sText = sText & "  AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyErrMsg", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

#End If
Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("pozda001.clspurchord")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "baspurchord"
End Function



