VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#143.0#0"; "SOTATbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#81.0#0"; "LookupView.ocx"
Begin VB.Form frmMFAutoIssue 
   Caption         =   "Auto Issue"
   ClientHeight    =   1950
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6180
   HelpContextID   =   38087
   Icon            =   "frmMFAutoIssue.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   1950
   ScaleWidth      =   6180
   StartUpPosition =   3  'Windows Default
   Begin NEWSOTALib.SOTAMaskedEdit txtOperationDesc 
      Height          =   315
      Left            =   3735
      TabIndex        =   4
      Top             =   1080
      Width           =   2280
      _Version        =   65536
      _ExtentX        =   4022
      _ExtentY        =   556
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   6180
      _ExtentX        =   10901
      _ExtentY        =   741
      Style           =   5
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtStepID 
      Height          =   315
      Left            =   1890
      TabIndex        =   2
      Top             =   1080
      WhatsThisHelpID =   17370618
      Width           =   1530
      _Version        =   65536
      _ExtentX        =   2699
      _ExtentY        =   556
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin LookupViewControl.LookupView navWorkOrder 
      Height          =   285
      Left            =   3435
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1095
      WhatsThisHelpID =   17370619
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupMode      =   1
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtWorkOrder 
      Height          =   315
      Left            =   150
      TabIndex        =   1
      Top             =   1080
      WhatsThisHelpID =   17370620
      Width           =   1725
      _Version        =   65536
      _ExtentX        =   3043
      _ExtentY        =   556
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblOperationDesc 
      Caption         =   "Operation Description"
      Height          =   210
      Left            =   4170
      TabIndex        =   7
      Top             =   735
      Width           =   1845
   End
   Begin VB.Label lblStepID 
      Caption         =   "Step "
      Height          =   270
      Left            =   2235
      TabIndex        =   6
      Top             =   705
      Width           =   600
   End
   Begin VB.Label lblWorkOrderNo 
      Caption         =   "Work Order Number"
      Height          =   195
      Left            =   315
      TabIndex        =   5
      Top             =   705
      Width           =   1635
   End
End
Attribute VB_Name = "frmMFAutoIssue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************************
'       Name: frmMFAutoIssue (frmMFAutoIssue.frm)
'       Desc: MF Auto Issue
'  Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
'  Original: SVB 11/14/03
'  Mods:
'****************************************************************************
    Option Explicit

    Private mbUserSelectedItem As Boolean

    'Private Variables of Form Properties
    Private moClass             As Object
    Private mbCancelShutDown    As Boolean
    Private miSecurityLevel     As Integer
    Private mbIsDirty           As Boolean
    Private msCompanyID         As String
    Private mbNewRow            As Boolean
    Private mlLanguage          As Long
    Private mlWorkOrderKey      As Long
    Private msNavRestrict       As String
    
    'Binding Object Variables
    Private muMFAutoIssue          As MFAutoIssue
    Private muOrigMFAutoIssue      As MFAutoIssue
    
   Friend Sub ProcessMFAutoIssue(uMFAutoIssue As MFAutoIssue, bNewRow As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim lRetVal As Long

    
    muMFAutoIssue = uMFAutoIssue
    muOrigMFAutoIssue = uMFAutoIssue
    
    mbNewRow = bNewRow
    
    BindToolbar

    IsDirty = False

    LoadData
    
   
    Me.Show vbModal
    
    If IsDirty Then
        ' updated with current values using the save and exit
        uMFAutoIssue = muMFAutoIssue
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ProcessMFAutoIssue", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub LoadData()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sStepID As String
'Populate the form with data
    mlWorkOrderKey = glGetValidLong(moClass.moAppDB.Lookup _
                                       ("WorkOrderKey", "tmfWorkOrdDetl_HAI", _
                                        "WorkOrderStepKey = " & Str(muMFAutoIssue.lWorkOrderStepKey) _
                                      & " AND CompanyID = " & gsQuoted(msCompanyID)))
   
    txtWorkOrder.Text = gsGetValidStr(moClass.moAppDB.Lookup _
                                     ("WorkOrderNo", "tmfWorkOrdHead_HAI", _
                                     "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                   & " AND CompanyID = " & gsQuoted(msCompanyID)))
     sStepID = gsGetValidStr(moClass.moAppDB.Lookup _
                                     ("StepId", "tmfWorkOrdDetl_HAI", _
                                     "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                   & " AND WorkOrderStepKey = " & Str(muMFAutoIssue.lWorkOrderStepKey) _
                                   & " AND MatItemKey = " & Str(muMFAutoIssue.lItemKey) _
                                   & " AND CompanyID = " & gsQuoted(msCompanyID)))
    If muMFAutoIssue.sStepID = "" Then
        txtStepID.Text = gsGetValidStr(moClass.moAppDB.Lookup _
                                     ("StepId", "tmfWorkOrdDetl_HAI", _
                                     "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                       & " AND StepId = " & gsQuoted(sStepID) _
                                       & " AND CompanyID = " & gsQuoted(msCompanyID)))
    Else
        txtStepID.Text = muMFAutoIssue.sStepID
    End If
    If muMFAutoIssue.sOperationDesc = "" Then
        txtOperationDesc.Text = gsGetValidStr(moClass.moAppDB.Lookup _
                                     ("Operationdesc1", "tmfWorkOrdDetl_HAI", _
                                     "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                   & " AND StepId = " & gsQuoted(sStepID) _
                                       & " AND CompanyID = " & gsQuoted(msCompanyID)))
    Else
        txtOperationDesc = muMFAutoIssue.sOperationDesc
    End If

        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadData", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SaveData()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlWorkOrderKey = glGetValidLong(moClass.moAppDB.Lookup _
                                       ("WorkOrderKey", "tmfWorkOrdHead_HAI", _
                                        "WorkOrderNo = " & gsQuoted(txtWorkOrder) _
                                      & " AND CompanyID = " & gsQuoted(msCompanyID)))
    muMFAutoIssue.lWorkOrderStepKey = glGetValidLong(moClass.moAppDB.Lookup _
                                       ("WorkOrderStepKey", "tmfWorkOrdDetl_HAI", _
                                        "WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
                                      & " AND StepId = " & gsQuoted(txtStepID) _
                                      & " AND CompanyID = " & gsQuoted(msCompanyID)))
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SaveData", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function MFAutoIssueChanged(uOrigMFAutoIssue As MFAutoIssue) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Determine if modifications were made on the form
    With muMFAutoIssue
       MFAutoIssueChanged = (.lWorkOrderStepKey <> uOrigMFAutoIssue.lWorkOrderStepKey)
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MFAutoIssueChanged", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'********************************************************************

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    HandleToolbarClick Button

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
  'Get Session defaults.
    msCompanyID = moClass.moSysSession.CompanyID    'Company ID
    mlLanguage = moClass.moSysSession.Language      'Language ID
    msNavRestrict = "OperationType IN ('M','O','X') " _
                  & " AND CompanyID = " & gsQuoted(msCompanyID) _
                  & " AND Complete <> " & gsQuoted(kWOComplete) _
                  & " AND MatItemKey = " & glGetValidLong(muMFAutoIssue.lItemKey)

                      
    gbLookupInit navWorkOrder, moClass, moClass.moAppDB, "WorkOrderStep", msNavRestrict

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig Begin Push +++


    'Validate all controls which have a Lost_Focus event
    If Not bIsFormValid Then
        Exit Sub
    End If

     
    Select Case sKey
        Case kTbFinishExit
          SaveData

          If MFAutoIssueChanged(muOrigMFAutoIssue) Then
              IsDirty = True
          End If
          
          Me.Hide
          
        Case kTbDelete
          muMFAutoIssue.lWorkOrderStepKey = 0
          muMFAutoIssue.sOperationDesc = ""
          muMFAutoIssue.sStepID = 0
          
          IsDirty = True
          Me.txtWorkOrder = ""
          Me.txtStepID = ""
          Me.txtOperationDesc = ""
        
        Case kTbCancelExit
          muMFAutoIssue = muOrigMFAutoIssue
          Me.Hide
        Case kTbHelp
            gDisplayFormLevelHelp Me
        Case Else
          giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         CVar(sKey)
            
    End Select

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "MFZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'MAS 500 Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'MAS 500 Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "MFZ"         '       Place your prefix here
'+++ VB/Rig Begin Pop +++                                                                 'MAS 500 Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'MAS 500 Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                'MAS 500 Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'MAS 500 Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'MAS 500 Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get IsDirty() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    IsDirty = mbIsDirty
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "IsDirty", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let IsDirty(bFormDirty As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mbIsDirty = bFormDirty
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "IsDirty", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
'************************************************************************
'   Description:
'       Create the toolbar.
'   Param:
'       <none>
'   Returns:
'************************************************************************
Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    
    'Set up the toolbar
    With tbrMain
      .SecurityLevel = moClass.moFramework.GetTaskPermission(moClass.moFramework.GetTaskID())
      .Build sotaTB_SINGLE_ROW
      .RemoveButton kTbPrint
      .AddButton kTbDelete, 4
      .LocaleID = mlLanguage
    End With

  
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navWorkOrder_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    navWorkOrder.RestrictClause = "OperationType IN ('M','O','X') " _
                  & " AND CompanyID = " & gsQuoted(msCompanyID) _
                  & " AND Complete <> " & gsQuoted(kWOComplete) _
                  & " AND MatItemKey = " & glGetValidLong(muMFAutoIssue.lItemKey) _
                  & " AND COALESCE(WhseKey, 0) = " & glGetValidLong(muMFAutoIssue.lWhseKey) _
                  & " AND MatItemType IN (1,3,5,6,8)"
                  
    gcLookupClick Me, navWorkOrder, txtStepID, "StepID"
    
    If navWorkOrder.ReturnColumnValues.Count <> 0 Then
        txtWorkOrder.Text = navWorkOrder.ReturnColumnValues("WorkOrderNo")
        txtStepID.Text = navWorkOrder.ReturnColumnValues("StepID")
        txtOperationDesc.Text = navWorkOrder.ReturnColumnValues("OperationDesc1")
        mlWorkOrderKey = glGetValidLong(moClass.moAppDB.Lookup _
                                       ("WorkOrderKey", "tmfWorkOrdHead_HAI", _
                                        "WorkOrderNo = " & gsQuoted(txtWorkOrder) _
                                      & " AND CompanyID = " & gsQuoted(msCompanyID)))
        muMFAutoIssue.lWorkOrderStepKey = glGetValidLong(moClass.moAppDB.Lookup _
                                       ("WorkOrderStepKey", "tmfWorkOrdDetl_HAI", _
                                        "WorkOrderKey = " & glGetValidLong(mlWorkOrderKey) _
                                      & " AND StepId = " & gsQuoted(txtStepID) _
                                      & " AND CompanyID = " & gsQuoted(msCompanyID)))
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "navWorkOrder_Click", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub txtStepID_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sStepID As String
    If Len(Trim(txtStepID)) <> 0 And Len(Trim(txtWorkOrder)) <> 0 Then
        sStepID = gsGetValidStr(moClass.moAppDB.Lookup _
                                ("StepId", "tmfWorkOrdDetl_HAI", _
                                  "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                  & " AND StepId = " & gsQuoted(txtStepID) _
                                  & " AND MatItemKey = " & Str(muMFAutoIssue.lItemKey) _
                                  & " AND COALESCE(WhseKey, 0) = " & Str(muMFAutoIssue.lWhseKey) _
                                  & " AND CompanyID = " & gsQuoted(msCompanyID)))
            
        muMFAutoIssue.lWorkOrderStepKey = glGetValidLong(moClass.moAppDB.Lookup _
                             ("WorkOrderStepKey", "tmfWorkOrdDetl_HAI wod WITH (NOLOCK) JOIN timItem i WITH (NOLOCK) ON wod.MatItemKey = i.ItemKey", _
                              "wod.WorkOrderKey = " & Str(mlWorkOrderKey) _
                              & " AND wod.StepId = " & gsQuoted(sStepID) _
                              & " AND COALESCE(wod.WhseKey, 0) = " & Str(muMFAutoIssue.lWhseKey) _
                              & " AND i.ItemType IN (1,3,5,6,8)" _
                              & " AND wod.CompanyID = " & gsQuoted(msCompanyID)))
            
        If muMFAutoIssue.lWorkOrderStepKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblStepID
            txtStepID.Text = ""
            gbSetFocus frmMFAutoIssue, txtStepID
            Exit Sub
        Else
            txtOperationDesc.Text = gsGetValidStr(moClass.moAppDB.Lookup _
                                         ("Operationdesc1", "tmfWorkOrdDetl_HAI", _
                                         "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                       & " AND StepId = " & gsQuoted(sStepID) _
                                           & " AND CompanyID = " & gsQuoted(msCompanyID)))
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtStepID_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub txtWorkOrder_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Len(Trim(txtWorkOrder)) <> 0 Then
        If IsNumeric(txtWorkOrder) Then
            txtWorkOrder = gsMFZeroFill(txtWorkOrder, 8)
        End If
        mlWorkOrderKey = glGetValidLong(moClass.moAppDB.Lookup _
                                               ("WorkOrderKey", "tmfWorkOrdHead_HAI", _
                                                "WorkOrderNo = " & gsQuoted(txtWorkOrder) _
                                              & " AND CompanyID = " & gsQuoted(msCompanyID)))
    
        If mlWorkOrderKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblWorkOrderNo
            txtWorkOrder.Text = ""
            gbSetFocus frmMFAutoIssue, txtWorkOrder
            Exit Sub
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtWorkOrder_LostFocus", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bIsFormValid() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iComplete As Integer
Dim sStepID As String
 
    bIsFormValid = False
    If Len(Trim(txtWorkOrder)) <> 0 And Len(Trim(txtStepID)) <> 0 Then
        If IsNumeric(txtWorkOrder) Then
            txtWorkOrder = gsMFZeroFill(txtWorkOrder, 8)
        End If
    
        mlWorkOrderKey = glGetValidLong(moClass.moAppDB.Lookup _
                                               ("WorkOrderKey", "tmfWorkOrdHead_HAI", _
                                                "WorkOrderNo = " & gsQuoted(txtWorkOrder) _
                                              & " AND CompanyID = " & gsQuoted(msCompanyID)))
    
        If mlWorkOrderKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblWorkOrderNo
            gbSetFocus frmMFAutoIssue, txtWorkOrder
            Exit Function
        End If
        iComplete = giGetValidInt(moClass.moAppDB.Lookup _
                                 ("Complete", "tmfWorkOrdHead_HAI", _
                                 "WorkOrderKey = " & Str(mlWorkOrderKey)))
        If iComplete = kWOComplete Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgMFWorkOrderCompleted
            gbSetFocus frmMFAutoIssue, txtWorkOrder
            Exit Function
        End If
        
        sStepID = gsGetValidStr(moClass.moAppDB.Lookup _
                                ("StepId", "tmfWorkOrdDetl_HAI", _
                                  "WorkOrderKey = " & Str(mlWorkOrderKey) _
                                  & " AND StepId = " & gsQuoted(txtStepID) _
                                  & " AND MatItemKey = " & Str(muMFAutoIssue.lItemKey) _
                                  & " AND COALESCE(WhseKey, 0) = " & Str(muMFAutoIssue.lWhseKey) _
                                  & " AND CompanyID = " & gsQuoted(msCompanyID)))
            
        muMFAutoIssue.lWorkOrderStepKey = glGetValidLong(moClass.moAppDB.Lookup _
                             ("WorkOrderStepKey", "tmfWorkOrdDetl_HAI wod WITH (NOLOCK) JOIN timItem i WITH (NOLOCK) ON wod.MatItemKey = i.ItemKey", _
                              "wod.WorkOrderKey = " & Str(mlWorkOrderKey) _
                              & " AND wod.StepId = " & gsQuoted(sStepID) _
                              & " AND COALESCE(wod.WhseKey, 0) = " & Str(muMFAutoIssue.lWhseKey) _
                              & " AND i.ItemType IN (1,3,5,6,8)" _
                              & " AND wod.CompanyID = " & gsQuoted(msCompanyID)))
            
        If muMFAutoIssue.lWorkOrderStepKey = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, lblStepID
            gbSetFocus frmMFAutoIssue, txtStepID
            Exit Function
        End If
    
    End If
    bIsFormValid = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsFormValid", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmMFAutoIssue"
End Function

