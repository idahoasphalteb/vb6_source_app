VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Begin VB.Form frmRecentPurch 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Recent Purchase Summary"
   ClientHeight    =   5385
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8190
   HelpContextID   =   83307
   Icon            =   "frmRecentPurch.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5385
   ScaleWidth      =   8190
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtItemDesc 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   3120
      Locked          =   -1  'True
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   600
      WhatsThisHelpID =   83314
      Width           =   5295
   End
   Begin VB.TextBox txtItemID 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   1065
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   600
      WhatsThisHelpID =   83313
      Width           =   1815
   End
   Begin VB.Frame fraInquiry 
      Caption         =   "Summary"
      Height          =   3855
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   7935
      Begin FPSpreadADO.fpSpread grdSummary 
         Height          =   3495
         Left            =   120
         TabIndex        =   2
         Top             =   240
         WhatsThisHelpID =   83311
         Width           =   7695
         _Version        =   524288
         _ExtentX        =   13573
         _ExtentY        =   6165
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "frmRecentPurch.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4995
      WhatsThisHelpID =   73
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   8190
      _ExtentX        =   14446
      _ExtentY        =   741
      Style           =   5
   End
   Begin VB.Label lblItemID 
      Caption         =   "Item:"
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   600
      Width           =   615
   End
End
Attribute VB_Name = "frmRecentPurch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmPOTagging
'     Desc: Template for Inquiry
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original:
'     Mods: Date    SE          Note
'
'************************************************************************************
Public moClass As Object
Private moContextMenu As clsContextMenu
Private WithEvents moGM As clsGridMgr
Attribute moGM.VB_VarHelpID = -1
Private moDmGrid As clsDmGrid
Private msCompanyID As String

Private mlItemKey As Long
Private msItemID As String
Private msItemDesc As String

Private Const kcolPOLineKey = 1
Private Const kcolVendID = 2
Private Const kcolWhseID = 3
Private Const kcolUnitCost = 4
Private Const kcolUOMID = 5
Private Const kcolQty = 6
Private Const kcolPODate = 7
Private Const kcolCompanyID = 8
Private Const kcolMaxCol = 8

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object

    Set MyForms = Forms
End Property
Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
'On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set moDmGrid = New clsDmGrid

    With moDmGrid
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Form = Me
        Set .Grid = grdSummary
        .Table = "#tpoRecentPurchWrk"
        .ReadOnly = True
        .UniqueKey = "POLineKey"
        .NoAppend = True
        .OrderBy = "PODate DESC, WhseID"
        
        .BindColumn "POLineKey", kcolPOLineKey, SQL_INTEGER
        .BindColumn "VendID", kcolVendID, SQL_VARCHAR
        .BindColumn "WhseID", kcolWhseID, SQL_CHAR
        .BindColumn "UnitCost", kcolUnitCost, SQL_DECIMAL
        .BindColumn "UOMID", kcolUOMID, SQL_CHAR
        .BindColumn "Qty", kcolQty, SQL_DECIMAL
        .BindColumn "PODate", kcolPODate, SQL_DATE
        .BindColumn "CompanyID", kcolCompanyID, SQL_CHAR
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "BindForm", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler
            Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moGM = New clsGridMgr

    With moGM
        Set .Grid = grdSummary
        Set .Form = Me
        .GridType = kGridInquiryDD
        .GridSortEnabled = True
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, "frmRecentPurch", "BindGM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class

   With moContextMenu
        .BindGrid moDmGrid, grdSummary.hWnd
        Set .Form = frmRecentPurch
        .Init                      'Init will set properties of Winhook control
    
    End With
  

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, "frmRecentPurch", "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
    FormHelpPrefix = "POZ"         '       Put your prefix here
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
    WhatHelpPrefix = "POZ"         '       Put your prefix here
End Property

Private Property Get sMyName() As String
    sMyName = "frmRecentPurch"
End Property

Private Sub FormatGrid()
'**************************************************************************************
'Customize: Format the grid.  usaually some general gid property, col width, col header
'           and special showing and hiding.
'**************************************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    grdSummary.MaxCols = kcolMaxCol

    gGridSetProperties grdSummary, grdSummary.MaxCols, kGridInquiryDD
    grdSummary.DisplayRowHeaders = False

    'Setup Column Widths
    gGridSetColumnWidth grdSummary, kcolVendID, 14
    gGridSetColumnWidth grdSummary, kcolWhseID, 6
    gGridSetColumnWidth grdSummary, kcolUnitCost, 8
    gGridSetColumnWidth grdSummary, kcolUOMID, 6
    gGridSetColumnWidth grdSummary, kcolQty, 8
    gGridSetColumnWidth grdSummary, kcolPODate, 9
    gGridSetColumnWidth grdSummary, kcolCompanyID, 8

    gGridSetHeader grdSummary, kcolVendID, "Vendor"
    gGridSetHeader grdSummary, kcolWhseID, "Whse"
    gGridSetHeader grdSummary, kcolUnitCost, "Unit Cost"
    gGridSetHeader grdSummary, kcolUOMID, "UOM"
    gGridSetHeader grdSummary, kcolQty, "Ord Qty"
    gGridSetHeader grdSummary, kcolPODate, "PO Date"
    gGridSetHeader grdSummary, kcolCompanyID, "Company"
    
    ' format date columns
    gGridSetColumnType grdSummary, kcolPODate, SS_CELL_TYPE_DATE
    
    ' format numeric columns
    gGridSetColumnType grdSummary, kcolUnitCost, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdSummary, kcolQty, SS_CELL_TYPE_FLOAT

    'Hide the surrogate key and any currently unused columns.
    gGridHideColumn grdSummary, kcolPOLineKey

    'Lock columns to force gray background and no data entry behavior
    gGridLockColumn grdSummary, kcolVendID
    gGridLockColumn grdSummary, kcolWhseID
    gGridLockColumn grdSummary, kcolUnitCost
    gGridLockColumn grdSummary, kcolUOMID
    gGridLockColumn grdSummary, kcolQty
    gGridLockColumn grdSummary, kcolPODate
    gGridLockColumn grdSummary, kcolCompanyID

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "FormatGrid", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Public Sub Display()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer
    
    CreateTempTable
    
    With moClass.moAppDB
    
        .SetInParam mlItemKey
        .SetInParam msCompanyID
        .SetOutParam iRetVal
        
        .ExecuteSP ("sppoRecentPurchInquiry")
        
        iRetVal = .GetOutParam(3)
         
        .ReleaseParams
        
    End With
        
    If iRetVal = 1 Then
        txtItemID.Text = msItemID
        txtItemDesc.Text = msItemDesc
        moDmGrid.Refresh
        Me.Show vbModal
    Else
        Err.Raise "12345", "Display", "An error occured in sppoRecentPurchInquiry"
    End If
    
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "Display", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

Private Sub CreateTempTable()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String

    'If table does not already exist, then drop
    sSQL = "IF object_id('tempdb..#tpoRecentPurchWrk') IS NOT NULL "
    sSQL = sSQL & vbCrLf & "DROP TABLE #tpoRecentPurchWrk "
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = sSQL & vbCrLf & "CREATE TABLE #tpoRecentPurchWrk ( "
    sSQL = sSQL & vbCrLf & "POLineKey INT NOT NULL , "
    sSQL = sSQL & vbCrLf & "VendID varchar (50) NULL , "
    sSQL = sSQL & vbCrLf & "CompanyID varchar (3) NULL , "
    sSQL = sSQL & vbCrLf & "WhseID varchar (6) NULL , "
    sSQL = sSQL & vbCrLf & "UnitCost decimal(15, 3) NULL , "
    sSQL = sSQL & vbCrLf & "UOMID varchar (6) NULL , "
    sSQL = sSQL & vbCrLf & "Qty decimal(15, 3) NULL , "
    sSQL = sSQL & vbCrLf & "PODate datetime NULL) "

    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "CreateTempTable", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

Private Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case sKey
        Case kTbClose
            Me.Hide
         
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmGrid, moClass
    
    End Select

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "HandleToolbarClick", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmPurchOrd", "oClass", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmPurchOrd", "oClass", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get ItemKey() As Long
    ItemKey = mlItemKey
End Property

Public Property Let ItemKey(ByVal lNewItemKey As Long)
    mlItemKey = glGetValidLong(lNewItemKey)
End Property

Public Property Get ItemID() As String
    ItemID = msItemID & ""
End Property

Public Property Let ItemID(ByVal sNewItemID As String)
    msItemID = sNewItemID & ""
End Property

Public Property Get ItemDesc() As String
    ItemDesc = msItemDesc & ""
End Property

Public Property Let ItemDesc(ByVal sNewItemDesc As String)
    msItemDesc = sNewItemDesc & ""
End Property

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'********************************************************************

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
'On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Form_Load initializes variables to be used throughout the
'       form's user interface.  It also sets up control
'       specific properties.  For example, here you would fill a
'       combo box with static/dynamic list information.
'
'********************************************************************
         
    'initial toolbar
    tbrMain.Init sotaTB_REGISTER, moClass.moSysSession.Language
    tbrMain.RemoveButton kTbProceed
        
    With moClass.moFramework
        tbrMain.SecurityLevel = _
.GetTaskPermission(.GetTaskID())
    End With
    
    'initial status bar
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.Status = SOTA_SB_START
    
    'CreateTempTable
   
    FormatGrid                          ' format SO Line Tagging Grid
    BindContextMenu                     ' bind CM object
    BindForm                            ' bind DM object
    BindGM                              ' bind Grid Mgr
         
    sbrMain.Status = SOTA_SB_INQUIRY

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "Form_Load", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Call the shutdown procedure
    PerformCleanShutDown

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "QueryUnload", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

Private Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Cleanup objects being used before exiting form.
    'There is no status or save activity required here because this is an inquiry form.
    
    If (Not (moDmGrid Is Nothing)) Then
        moDmGrid.UnloadSelf
        Set moDmGrid = Nothing
    End If
    
    If Not moGM Is Nothing Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, "frmRecentPurch", "PerformCleanShutdown", VBRIG_IS_FORM
    Select Case VBRIG_IS_NON_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Public Property Get CompanyID() As String
    CompanyID = msCompanyID
End Property

Public Property Let CompanyID(ByVal sNewID As String)
    msCompanyID = sNewID & ""
End Property

Private Sub tbrMain_ButtonClick(Button As String)
    HandleToolbarClick Button
End Sub


 Private Sub grdSummary_KeyDown(keycode As Integer, Shift As Integer)
    moGM.Grid_KeyDown keycode, Shift
End Sub
 Private Sub grdSummary_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
End Sub
 Private Sub grdSummary_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    moGM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
End Sub

