VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Begin VB.Form frmpozxxRailDist 
   Caption         =   "Rail Car Distribution"
   ClientHeight    =   7800
   ClientLeft      =   660
   ClientTop       =   1995
   ClientWidth     =   9330
   Icon            =   "pozxxRailDist.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7800
   ScaleWidth      =   9330
   Begin NEWSOTALib.SOTANumber SOTANbrQtyOrd 
      Height          =   285
      Left            =   6135
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   1200
      Width           =   2895
      _Version        =   65536
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483648
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin VB.TextBox txtItemID 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   6135
      Locked          =   -1  'True
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   855
      Width           =   2895
   End
   Begin VB.TextBox txtLandCostCode 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   6135
      Locked          =   -1  'True
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   510
      Width           =   2895
   End
   Begin VB.TextBox txtVendor 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   1605
      Locked          =   -1  'True
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1545
      Width           =   2895
   End
   Begin VB.TextBox txtBOL 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   1605
      Locked          =   -1  'True
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1200
      Width           =   2895
   End
   Begin VB.TextBox txtPOLineNbr 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   1605
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   855
      Width           =   2895
   End
   Begin VB.TextBox txtPONbr 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   1605
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   510
      Width           =   2895
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   45
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   52
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   51
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   53
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7410
      Width           =   9330
      _ExtentX        =   16457
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   0
      Width           =   9330
      _ExtentX        =   16457
      _ExtentY        =   741
   End
   Begin VB.PictureBox ofcOffice 
      Height          =   345
      Left            =   6480
      ScaleHeight     =   285
      ScaleWidth      =   195
      TabIndex        =   43
      Top             =   60
      Visible         =   0   'False
      Width           =   255
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin TabDlg.SSTab tabDataEntry 
      Height          =   4890
      Left            =   15
      TabIndex        =   19
      Top             =   2400
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   8625
      _Version        =   393216
      Tabs            =   1
      TabsPerRow      =   1
      TabHeight       =   520
      TabCaption(0)   =   "&Rail Car Distribution"
      TabPicture(0)   =   "pozxxRailDist.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "pnlTab(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      Begin Threed.SSPanel pnlTab 
         Height          =   4410
         Index           =   0
         Left            =   75
         TabIndex        =   47
         Top             =   390
         Width           =   9120
         _Version        =   65536
         _ExtentX        =   16087
         _ExtentY        =   7779
         _StockProps     =   15
         BackColor       =   13160660
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame frmLineEntry 
            Height          =   2355
            Left            =   45
            TabIndex        =   58
            Top             =   75
            Width           =   9000
            Begin VB.CommandButton cmdOK 
               Caption         =   "&OK"
               Height          =   360
               Left            =   7845
               TabIndex        =   40
               Top             =   1020
               WhatsThisHelpID =   35806
               Width           =   760
            End
            Begin VB.CommandButton cmdUndo 
               Caption         =   "&Undo"
               Height          =   360
               Left            =   7845
               TabIndex        =   41
               Top             =   1500
               WhatsThisHelpID =   35807
               Width           =   760
            End
            Begin SOTACalendarControl.SOTACalendar SOTACalShipped 
               Height          =   285
               Left            =   5640
               TabIndex        =   31
               Top             =   300
               Width           =   1770
               _ExtentX        =   3122
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtChildInvoiceLineKey 
               Height          =   315
               Left            =   -10000
               TabIndex        =   59
               TabStop         =   0   'False
               Top             =   600
               Visible         =   0   'False
               Width           =   1500
               _Version        =   65536
               _ExtentX        =   2646
               _ExtentY        =   556
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtManifestNbr 
               Height          =   285
               Left            =   1515
               TabIndex        =   21
               Top             =   360
               Width           =   1770
               _Version        =   65536
               _ExtentX        =   3122
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtChildInvoiceKey 
               Height          =   315
               Left            =   -10000
               TabIndex        =   60
               TabStop         =   0   'False
               Top             =   1430
               Visible         =   0   'False
               Width           =   1500
               _Version        =   65536
               _ExtentX        =   2646
               _ExtentY        =   556
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtChildSeqNo 
               Height          =   315
               Left            =   -10000
               TabIndex        =   61
               TabStop         =   0   'False
               Top             =   1015
               Visible         =   0   'False
               Width           =   1500
               _Version        =   65536
               _ExtentX        =   2646
               _ExtentY        =   556
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin SOTACalendarControl.SOTACalendar SOTACalArrived 
               Height          =   285
               Left            =   5640
               TabIndex        =   33
               Top             =   615
               Width           =   1770
               _ExtentX        =   3122
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
            End
            Begin SOTACalendarControl.SOTACalendar SOTACalSpotted 
               Height          =   285
               Left            =   5640
               TabIndex        =   35
               Top             =   930
               Width           =   1770
               _ExtentX        =   3122
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
            End
            Begin SOTACalendarControl.SOTACalendar SOTACalReleased 
               Height          =   285
               Left            =   5640
               TabIndex        =   37
               Top             =   1260
               Width           =   1770
               _ExtentX        =   3122
               _ExtentY        =   503
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
            End
            Begin NEWSOTALib.SOTANumber SOTANbrQtyRecvd 
               Height          =   285
               Left            =   5640
               TabIndex        =   39
               Top             =   1560
               Width           =   1770
               _Version        =   65536
               _ExtentX        =   3122
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtRailCarNbr 
               Height          =   285
               Left            =   1515
               TabIndex        =   23
               Top             =   690
               Width           =   1770
               _Version        =   65536
               _ExtentX        =   3122
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtRefineryNbr 
               Height          =   285
               Left            =   1515
               TabIndex        =   25
               Top             =   1035
               Width           =   1770
               _Version        =   65536
               _ExtentX        =   3122
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtCarrierNbr 
               Height          =   285
               Left            =   1515
               TabIndex        =   27
               Top             =   1365
               Width           =   1770
               _Version        =   65536
               _ExtentX        =   3122
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin SOTADropDownControl.SOTADropDown ddnSpot 
               Height          =   315
               Left            =   1515
               TabIndex        =   29
               Top             =   1680
               Width           =   2895
               _ExtentX        =   5106
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnSpot"
            End
            Begin VB.Label lblSpot 
               Caption         =   "Rail Car Spot"
               Height          =   240
               Left            =   90
               TabIndex        =   28
               Top             =   1710
               Width           =   1260
            End
            Begin VB.Label lblChildComment 
               Caption         =   "Manifest Number"
               Height          =   255
               Left            =   135
               TabIndex        =   20
               Top             =   390
               Width           =   1335
            End
            Begin VB.Label Label10 
               Caption         =   "Shipped"
               Height          =   255
               Left            =   4500
               TabIndex        =   30
               Top             =   300
               Width           =   1140
            End
            Begin VB.Label Label11 
               Caption         =   "Arrived"
               Height          =   255
               Left            =   4500
               TabIndex        =   32
               Top             =   615
               Width           =   1140
            End
            Begin VB.Label Label12 
               Caption         =   "Spotted"
               Height          =   255
               Left            =   4500
               TabIndex        =   34
               Top             =   930
               Width           =   1140
            End
            Begin VB.Label Label13 
               Caption         =   "Released"
               Height          =   255
               Left            =   4500
               TabIndex        =   36
               Top             =   1260
               Width           =   1140
            End
            Begin VB.Label Label14 
               Caption         =   "Qty Received"
               Height          =   255
               Left            =   4500
               TabIndex        =   38
               Top             =   1620
               Width           =   1140
            End
            Begin VB.Label Label15 
               Caption         =   "Rail Car Number"
               Height          =   255
               Left            =   120
               TabIndex        =   22
               Top             =   720
               Width           =   1335
            End
            Begin VB.Label Label16 
               Caption         =   "Refinery Invoice #"
               Height          =   255
               Left            =   105
               TabIndex        =   24
               Top             =   1065
               Width           =   1335
            End
            Begin VB.Label Label17 
               Caption         =   "Carrier Invoice #"
               Height          =   255
               Left            =   90
               TabIndex        =   26
               Top             =   1395
               Width           =   1335
            End
            Begin VB.Shape shpFocusRect 
               Height          =   285
               Left            =   105
               Top             =   120
               Visible         =   0   'False
               Width           =   585
            End
         End
         Begin FPSpreadADO.fpSpread grdMain 
            Height          =   1635
            Left            =   0
            TabIndex        =   57
            Top             =   2715
            Width           =   8970
            _Version        =   524288
            _ExtentX        =   15822
            _ExtentY        =   2884
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "pozxxRailDist.frx":23EE
            AppearanceStyle =   0
         End
      End
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   46
      Top             =   4320
      Visible         =   0   'False
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   55
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   615
      Index           =   0
      Left            =   -10000
      TabIndex        =   56
      TabStop         =   0   'False
      Top             =   720
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin NEWSOTALib.SOTANumber SOTANbrQtyRec 
      Height          =   285
      Left            =   6135
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1545
      Width           =   2895
      _Version        =   65536
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483648
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber SOTANbrQtyRem 
      Height          =   285
      Left            =   6135
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1875
      Width           =   2895
      _Version        =   65536
      _ExtentX        =   5106
      _ExtentY        =   503
      _StockProps     =   93
      BackColor       =   -2147483648
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin VB.TextBox txtRcvrLnKey 
      Height          =   285
      Left            =   3165
      TabIndex        =   0
      Top             =   2460
      Width           =   855
   End
   Begin VB.Label Label9 
      Caption         =   "Quantity Remaining"
      Height          =   240
      Left            =   4560
      TabIndex        =   17
      Top             =   1905
      Width           =   1500
   End
   Begin VB.Label Label8 
      Caption         =   "Quantity Received"
      Height          =   240
      Left            =   4560
      TabIndex        =   15
      Top             =   1560
      Width           =   1500
   End
   Begin VB.Label Label7 
      Caption         =   "Quantity Ordered"
      Height          =   240
      Left            =   4560
      TabIndex        =   13
      Top             =   1215
      Width           =   1500
   End
   Begin VB.Label Label6 
      Caption         =   "Item ID"
      Height          =   240
      Left            =   4560
      TabIndex        =   11
      Top             =   870
      Width           =   1500
   End
   Begin VB.Label Label5 
      Caption         =   "Landed Cost Code"
      Height          =   240
      Left            =   4560
      TabIndex        =   9
      Top             =   525
      Width           =   1500
   End
   Begin VB.Label Label4 
      Caption         =   "Vendor"
      Height          =   240
      Left            =   30
      TabIndex        =   7
      Top             =   1560
      Width           =   1500
   End
   Begin VB.Label Label3 
      Caption         =   "Bill Of Lading"
      Height          =   240
      Left            =   30
      TabIndex        =   5
      Top             =   1215
      Width           =   1500
   End
   Begin VB.Label Label2 
      Caption         =   "PO Line Number:"
      Height          =   240
      Left            =   30
      TabIndex        =   3
      Top             =   870
      Width           =   1500
   End
   Begin VB.Label Label1 
      Caption         =   "PO Number:"
      Height          =   240
      Left            =   30
      TabIndex        =   1
      Top             =   525
      Width           =   1500
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   54
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmpozxxRailDist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public moSotaObjects            As New Collection

Private moContextMenu           As clsContextMenu

Private moLE                    As clsLineEntry

Private moOptions               As New clsModuleOptions

Private mbEnterAsTab            As Boolean

Private msCompanyID             As String

Private msUserID                As String

Private miSecurityLevel         As Integer

Private miFilter                As Integer

Private mlLanguage              As Long

Private miMinFormHeight As Long

Private miMinFormWidth As Long

Private miOldFormHeight As Long

Private miOldFormWidth As Long

Public moClass                  As Object

Private moAppDB                 As Object

Private moSysSession            As Object

Private mbCancelShutDown        As Boolean

Private mbLoadSuccess           As Boolean

Private mbSaved                 As Boolean

Private mlRunMode               As Long

Public WithEvents moDmHeader    As clsDmForm
Attribute moDmHeader.VB_VarHelpID = -1

Private WithEvents moDmDetl     As clsDmGrid
Attribute moDmDetl.VB_VarHelpID = -1

Private Const kiHeaderTab = 1

Private Const kiDetailTab = 0

Private Const kiTotalsTab = 2

Const VBRIG_MODULE_ID_STRING = "frmpozxxRailDist.FRM"

Private mbInBrowse As Boolean

Private msNatCurrID As String

Private msHomeCurrID As String

Private muHomeCurrInfo As CurrencyInfo

Private muNatCurrInfo As CurrencyInfo

Private msLookupRestrict As String
    Private Const kColInvoiceNo = 1
    Private Const kColInvoiceKey = 2
    Private Const kColBillDate = 3
    Private Const kColCustKey = 4
    Private Const kColPmtTermsKey = 5
    Private Const kColPostDate = 6
    Private Const kColShipMethKey = 7
    Private Const kColTranAmt = 8
    Private Const kColUpdateCounter = 9
    
    Private Const kColSeqNo = 1
    Private Const kColRcvrLineExtKey = 2
    Private Const kColRcvrLineKey = 3
    Private Const kColDateShipped = 4
    Private Const kColDateArrived = 5
    Private Const kColDateSpotted = 6
    Private Const kColDateReleased = 7
    Private Const kColManifestNumber = 8
    Private Const kColRailCarNumber = 9
    Private Const kColQtyReceived = 10
    Private Const kColRefineryInvcNbr = 11
    Private Const kColCarrierInvcNbr = 12
    Private Const kColCompanyID = 13
    Private Const kColSpotID = 14    'RKL DEJ 2016-04-14
    Private Const kColSpotKey = 15    'RKL DEJ 2016-04-14
    
    
    Private Const kColChildNbrCols = 15
    
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If


Public Property Let ReceiptLineKey(lReceiptLineKey As Long)
    txtRcvrLnKey.Text = lReceiptLineKey
    
    'RKL DEJ 2016-04-27 (START)
    'Add filter by WhseKey
    Call SetupDropDowns
    'RKL DEJ 2016-04-27 (STOP)

End Property


Public Sub Init()
    
    moLE.GridEditUndo
    
    DoEvents
    
    valMgr.Reset
    
End Sub



Sub CalcQtyRem()
    On Error GoTo Error
    Dim i As Long
    Dim iCurrRow As Long
    Dim QtyRem As Double
    
'    iCurrRow = grdMain.ActiveRow
    
    For i = 0 To grdMain.MaxRows
        QtyRem = QtyRem + gdGetValidDbl(gsGridReadCell(grdMain, i, kColQtyReceived))
'        lLineKey = glGetValidLong(gsGridReadCell(grdMain, lRow, kColReceiptLineKey))
    Next
    
    QtyRem = SOTANbrQtyRec - QtyRem
    
'    grdMain.ActiveRow = iCurrRow
'    grdMain.Row = iCurrRow
    
    SOTANbrQtyRem.Value = QtyRem
    
    Exit Sub
Error:
    MsgBox "The following error occurred while calculating the Quantity Remaining: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
End Sub

Public Property Get FormHelpPrefix() As String
    FormHelpPrefix = "ARZ"
End Property

Function ManifestExists(ManifestNbr As String, RcvrLineExtKey As Long) As Boolean
    On Error GoTo Error
    Dim db As SOTADAS.DASDatabase
    Dim Cnt As Long
    Dim lManifestNbr As String
    Dim lRow As Long
    
    Set db = moClass.moAppDB
    
    'Default to fales
    ManifestExists = False
    
    For lRow = 1 To grdMain.MaxRows
        lManifestNbr = gsGetValidStr(moDmDetl.GetColumnValue(lRow, "ManifestNumber"))
        
        If UCase(lManifestNbr) = UCase(ManifestNbr) Then
            Cnt = Cnt + 1
        End If
        
    Next
    
    If Cnt > 1 Then
        GoTo Done
    End If
    
    Cnt = glGetValidLong(db.LookupSys("Count(*) cnt", "tpoRcvrLineExt_SGS", "RcvrLineExtKey <> " & RcvrLineExtKey & " And ManifestNumber = '" & Replace(ManifestNbr, "'", "''") & "' "))
    
Done:
    If Cnt > 0 Then
        ManifestExists = True
    Else
        ManifestExists = False
    End If
    
    Exit Function
Error:
    MsgBox "The following error occurred while validating the Manifest Number: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
End Function



Function ManifestRcvrUnique(ManifestNbr As String, RcvrLineExtKey As Long, RcvrLineKey As Long) As Boolean
    On Error GoTo Error
    Dim db As SOTADAS.DASDatabase
    Dim Cnt As Long
    Dim lManifestNbr As String
    Dim lRow As Long
    
    Set db = moClass.moAppDB
    
    'Default to false
    ManifestRcvrUnique = False
    
    For lRow = 1 To grdMain.MaxRows
        lManifestNbr = gsGetValidStr(moDmDetl.GetColumnValue(lRow, "ManifestNumber"))
        
        If UCase(lManifestNbr) = UCase(ManifestNbr) Then
            Cnt = Cnt + 1
        End If
        
    Next
    
    If Cnt > 1 Then
        GoTo Done
    End If
    
    Cnt = glGetValidLong(db.LookupSys("Count(*) cnt", "tpoRcvrLineExt_SGS", "RcvrLineExtKey <> " & RcvrLineExtKey & " And ManifestNumber = '" & Replace(ManifestNbr, "'", "''") & "' And RcvrLineKey = " & RcvrLineKey))
    
Done:
    If Cnt > 0 Then
        ManifestRcvrUnique = False
    Else
        ManifestRcvrUnique = True
    End If
    
    Exit Function
Error:
    MsgBox "The following error occurred while validating the Manifest Number: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
End Function


Public Property Get WhatHelpPrefix() As String
    WhatHelpPrefix = "ARZ"
End Property

Public Property Get oClass() As Object
    Set oClass = moClass
End Property

Public Property Set oClass(oNewClass As Object)
    Set moClass = oNewClass
End Property

Public Property Get lRunMode() As Long
    lRunMode = mlRunMode
End Property

Public Property Let lRunMode(lNewRunMode As Long)
    mlRunMode = lNewRunMode
End Property

Public Property Get bCancelShutDown()
    bCancelShutDown = mbCancelShutDown
End Property

Public Property Get bLoadSuccess() As Boolean
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Get bSaved() As Boolean
    bSaved = mbSaved
End Property

Public Property Let bSaved(bNewSaved As Boolean)
    mbSaved = bNewSaved
End Property

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub SetupDropDowns()

    '-- Set up drop down properties (see APZDA001 for reference)
    
    'RKL DEJ 2016-04-14 (START)
    Dim sSQL As String
    
    'Apply Warehouse filter to the SPOT DDN
    Dim lcRcvrLineKey As Long
    Dim lcWhseKey As Long
    
    lcRcvrLineKey = glGetValidLong(txtRcvrLnKey.Text)
    
    lcWhseKey = glGetValidLong(moClass.moAppDB.Lookup("WhseKey", "vpoPndRcvrDetailUpd_SGS", "RcvrLineKey = " & lcRcvrLineKey))
    
    With ddnSpot
        Set .DBObject = moClass.moAppDB
        .LocaleID = mlLanguage
        .Style = sotaDD_Styles.sotaDD_DYNAMIC_LIST
        
        sSQL = "Select SpotID, SpotKey From tpoRailCarSPOT_SGS where CompanyID = " & gsQuoted(msCompanyID) & " And IsActive = 1 And WhseKey = " & lcWhseKey
        
        .SQLStatement = sSQL
        
        .Refresh
    End With
    'RKL DEJ 2016-04-14 (STOP)


End Sub

Private Sub SetupLookups()
Dim oFramework As Object
    '-- Set up lookup properties (see APZDA001 for reference)
    Set oFramework = moClass.moFramework

    Set oFramework = Nothing
    
'    With lkuInvoiceNo
'        Set .Framework = moClass.moFramework
'        Set .AppDatabase = moClass.moAppDB
'        .RestrictClause = msLookupRestrict
'    End With
'
'    With lkuCustKey
'        Set .Framework = moClass.moFramework
'        Set .AppDatabase = moClass.moAppDB
'        .RestrictClause = msLookupRestrict
'    End With
'
'    With lkuShipMethKey
'        Set .Framework = moClass.moFramework
'        Set .AppDatabase = moClass.moAppDB
'        .RestrictClause = msLookupRestrict
'    End With
'
'    With lkuPmtTermsKey
'        Set .Framework = moClass.moFramework
'        Set .AppDatabase = moClass.moAppDB
'        .RestrictClause = msLookupRestrict
'    End With
'
'    With lkuChildItemKey
'        Set .Framework = moClass.moFramework
'        Set .AppDatabase = moClass.moAppDB
'        .RestrictClause = msLookupRestrict
'    End With
    
End Sub


Private Sub SetupBars()

    Set sbrMain.Framework = moClass.moFramework
    

    With tbrMain
        .Style = sotaTB_TRANSACTION
        
        .RemoveSeparator kTbRenameId
        .RemoveSeparator kTbDelete
        .RemoveSeparator kTbMemo
        .RemoveSeparator kTbNextNumber
        .RemoveSeparator kTbPrint
        .RemoveSeparator kTbHelp
        
        .RemoveButton kTbCopyFrom
        .RemoveButton kTbRenameId
        .RemoveButton kTbDelete
        .RemoveButton kTbMemo
        .RemoveButton kTbNextNumber
        .RemoveButton kTbPrint
        .RemoveButton kTbHelp
        
        
'        .AddButton kTbOffice, .GetIndex(kTbHelp)
        
        
'        .AddSeparator .GetIndex(kTbHelp)
        sbrMain.BrowseVisible = True

        .LocaleID = mlLanguage
    End With
End Sub

Private Sub SetupOffice()

    '-- Entity type/title must be changed to fit your application
    'moOffice.Init moClass, moDmHeader, Me, ofcOffice, _
    '    kEntTypeAPVoucher, "tarInvoice_DJP.InvoiceNo", _
    '    lkuVouchNo, kEntTitleAPVoucher


End Sub


Private Sub SetupModuleVars()
    '-- Currency variables
    msNatCurrID = msHomeCurrID
    
    '-- Assign the initial filter value
    miFilter = RSID_UNFILTERED

End Sub

Private Sub BindForm()
    BindHeader
    BindDetail

End Sub

Private Sub BindHeader()
    '-- Bind the parent DM object
    Set moDmHeader = New clsDmForm

    With moDmHeader
        Set .Form = frmpozxxRailDist
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Toolbar = tbrMain
        Set .SOTAStatusBar = sbrMain
        Set .ValidationMgr = valMgr
        .AppName = gsStripChar(Me.Caption, ".")
        
        .UniqueKey = "RcvrLineKey"
        .OrderBy = "RcvrLineKey"
        
        .AccessType = kDmBuildQueries
        .Table = "vpoPndRcvrDetail_SGS"

        .Bind txtRcvrLnKey, "RcvrLineKey", DT_SQLAppDataTypes.SQL_INTEGER

        .Bind Nothing, "CompanyID", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "POKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind txtPONbr, "POTranNo", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "polinekey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind txtPOLineNbr, "polineno", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "RcvrKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind Nothing, "RcvrTranNo", DT_SQLAppDataTypes.SQL_CHAR
        .Bind txtBOL, "BillOfLadingNo", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "VendKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind Nothing, "VendID", DT_SQLAppDataTypes.SQL_CHAR
        .Bind txtVendor, "VendName", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "LandCostKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind txtLandCostCode, "LandCostID", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "ItemKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind txtItemID, "ItemID", DT_SQLAppDataTypes.SQL_CHAR
        .Bind Nothing, "RcvrLineKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind SOTANbrQtyRec, "QtyRcvd", DT_SQLAppDataTypes.SQL_DECIMAL
        .Bind Nothing, "POLineDistKey", DT_SQLAppDataTypes.SQL_INTEGER
        .Bind SOTANbrQtyOrd, "QtyOrd", DT_SQLAppDataTypes.SQL_DECIMAL

        .Init
    End With
End Sub


Private Sub BindDetail()
    
    Set moDmDetl = New clsDmGrid

    With moDmDetl
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmpozxxRailDist
        Set .Parent = moDmHeader
        
        .UniqueKey = "RcvrLineExtKey"
        .OrderBy = "SeqNo"
'        .UniqueKey = "CompanyID, ManifestNumber"
'        .OrderBy = "CompanyID, ManifestNumber"
        
        '-- Bind the detail columns
        Set .Grid = grdMain
        .Table = "tpoRcvrLineExt_SGS"
        
        
        .BindColumn "RcvrLineExtKey", kColRcvrLineExtKey, DT_SQLAppDataTypes.SQL_INTEGER
        
        .BindColumn "CompanyID", kColCompanyID, DT_SQLAppDataTypes.SQL_CHAR
        
        .BindColumn "DateShipped", kColDateShipped, DT_SQLAppDataTypes.SQL_DATE, SOTACalShipped
        .BindColumn "DateArrived", kColDateArrived, DT_SQLAppDataTypes.SQL_DATE, SOTACalArrived
        .BindColumn "DateSpotted", kColDateSpotted, DT_SQLAppDataTypes.SQL_DATE, SOTACalSpotted
        .BindColumn "DateReleased", kColDateReleased, DT_SQLAppDataTypes.SQL_DATE, SOTACalReleased
        
        .BindColumn "ManifestNumber", kColManifestNumber, DT_SQLAppDataTypes.SQL_CHAR, txtManifestNbr
        .BindColumn "RailCarNumber", kColRailCarNumber, DT_SQLAppDataTypes.SQL_CHAR, txtRailCarNbr
        .BindColumn "RefineryInvcNbr", kColRefineryInvcNbr, DT_SQLAppDataTypes.SQL_CHAR, txtRefineryNbr
        .BindColumn "CarrierInvcNbr", kColCarrierInvcNbr, DT_SQLAppDataTypes.SQL_CHAR, txtCarrierNbr
        
        .BindColumn "QtyReceived", kColQtyReceived, DT_SQLAppDataTypes.SQL_DECIMAL, SOTANbrQtyRecvd
        
        .BindColumn "SeqNo", kColSeqNo, SQL_INTEGER
        
        'RKL DEJ 2016-04-14 (START)
        .BindColumn "SpotKey", kColSpotKey, SQL_INTEGER, Nothing, kDmSetNull
        
        .LinkSource "tpoRailCarSPOT_SGS", "tpoRcvrLineExt_SGS.SpotKey=tpoRailCarSPOT_SGS.SpotKey", kDmJoin, LeftOuter
        .Link kColSpotID, "SpotID", ddnSpot
        
        'RKL DEJ 2016-04-14 (STOP)
        
        .ParentLink "RcvrLineKey", "RcvrLineKey", SQL_INTEGER

        .Init
    End With
End Sub

Private Sub BindLE()

    Set moLE = New clsLineEntry
    
    With moLE
        Set .Grid = grdMain
        .GridType = kGridLineEntry
        Set .TabControl = tabDataEntry
        .TabDetailIndex = kiDetailTab
        Set .DM = moDmDetl
        Set .Ok = cmdOK
        Set .Undo = cmdUndo
        .SeqNoCol = kColSeqNo
        .AllowGotoLine = True
        Set .FocusRect = shpFocusRect
        Set .Form = frmpozxxRailDist
        .Init
    End With
End Sub

Private Sub BindContextMenu()
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        .BindGrid moLE, grdMain.hWnd
        Set .Form = frmpozxxRailDist
         .Init
    End With
End Sub

Public Sub HandleToolbarClick(sKey As String)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            If Not valMgr.ValidateForm Then Exit Sub
            If Not moLE.GridEditDone() = kLeSuccess Then Exit Sub
            
            If SOTANbrQtyRem.Value <> 0 Then
                MsgBox "The Quantity Remaining must be 0.", vbExclamation, "MAS 500"
                Exit Sub
            End If
            
            moDmHeader.Action (kDmFinish)

            'SGS DEJ START
            SOTANbrQtyRem.Value = 0
            Unload Me
            'SGS DEJ STOP

        Case kTbSave
            If Not valMgr.ValidateForm Then Exit Sub
            If Not moLE.GridEditDone() = kLeSuccess Then Exit Sub
            moDmHeader.Save (True)

        Case kTbCancel, kTbCancelExit
            'SGS DEJ
            If MsgBox("Canceling will delete all Rail Car Distribution records for this PO Line.  Are you sure you want to Cancel?", vbExclamation + vbYesNo, "MAS 500") <> vbYes Then
                Exit Sub
            End If
            
            ProcessCancel
            
            'SGS DEJ START
            SOTANbrQtyRem.Value = 0
            Unload Me
            'SGS DEJ STOP
        
        Case kTbDelete
'            moDmHeader.Action (kDmDelete)

        Case kTbNextNumber
            HandleNextNumberClick
        
        Case kTbMemo
            '-- The Memo button was pressed by the user
            'CMMemoSelected lkuVendID
        
        Case kTbOffice
            'Refer to documentation to determine the steps necessary
            'to correctly initialize Office
            'moOffice.Start

        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'moDmHeader.CopyFrom

        Case kTbRenameId
            moDmHeader.RenameID
            
        Case kTbFilter
            miFilter = giToggleLookupFilter(miFilter)
            
        '-- Trap the browse control buttons
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            HandleBrowseClick sKey
                        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmHeader, moClass
            
    End Select
End Sub

Private Sub HandleNextNumberClick()
    Dim sNextNo As String
    
    If Not bConfirmUnload(0) Then Exit Sub
    
    '-- Clear the form
    ProcessCancel
    
    '-- Retrieve the next Voucher Number now
    'sNextNo = sGetNextNo()
    MsgBox "Sage MAS 500 Repository" & vbNewLine & "The Next Number procedure needs to be coded manually, please refer to the MAS 500 documentation for advice."
    If (Len(Trim$(sNextNo)) > 0) Then
        '-- Assign the next No to the control
        'lkuKeyControl = sNextNo
        
        '-- Fire the KeyChange event now

        valMgr_KeyChange
    End If
End Sub

Public Sub HandleBrowseClick(sKey As String)

    Dim lRet                  As Long
    Dim sNewKey          As String
    Dim iConfirmUnload  As Integer

    Select Case sKey

        Case kTbFilter

            miFilter = giToggleLookupFilter(miFilter)

        Case kTbFirst, kTbPrevious, kTbLast, kTbNext

'            bConfirmUnload iConfirmUnload, True
'            If Not iConfirmUnload = kDmSuccess Then Exit Sub
'
'                lRet = glLookupBrowse(lkuInvoiceNo, sKey, miFilter, sNewKey)
'
'                Select Case lRet
'
'                    Case MS_SUCCESS
'
'                        If lkuInvoiceNo.ReturnColumnValues.Count = 0 Then Exit Sub
'
'                        '!!! TO DO: Replace ReturnColumnValues Key "CustID" with ColumnName to be placed into the control.
'                        If StrComp(Trim(lkuInvoiceNo.Text), Trim(lkuInvoiceNo.ReturnColumnValues("InvoiceNo")), vbTextCompare) <> 0 Then
'
'                            lkuInvoiceNo.Text = Trim(lkuInvoiceNo.ReturnColumnValues("InvoiceNo"))
'                            valMgr_KeyChange
'                        End If
'
'                    Case Else
'
'                        gLookupBrowseError lRet, Me, moClass
'
'                End Select

        End Select

End Sub

Private Sub ProcessCancel()
    On Error GoTo Error
    Dim db As SOTADAS.DASDatabase
    
'    moDmHeader.Action kDmCancel
    
    Set db = moClass.moAppDB
    
    db.ExecuteSQL "Delete tpoRcvrLineExt_SGS Where RcvrLineKey = " & moDmHeader.GetColumnValue("RcvrLineKey")
    
    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset
        
    Exit Sub
Error:
    MsgBox "The following error occurred while canceling: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
End Sub

Private Function bConfirmUnload(iConfirmUnload As Integer, Optional ByVal bNoClear As Boolean = False) As Boolean
    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    If Not valMgr.ValidateForm Then Exit Function
    
    iConfirmUnload = moDmHeader.ConfirmUnload(bNoClear)

    If (iConfirmUnload = kDmSuccess) Then
        bConfirmUnload = True
    End If
End Function

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    Dim sMenuText As String

    CMAppendContextMenu = False

'-- This will need to be specific to your application

End Function

Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim lVoucherKey As Long
    Dim iPos As Integer
    Dim oDDN As Object
    Dim sClassID As String
    Dim lRealTaskID As Long
    
    CMMenuSelected = False

'-- This will need to be specific to your application
   
    CMMenuSelected = True

    Exit Function

ExpectedErrorRoutine:
    SetHourglass False
    
    If oDDN Is Nothing Then
        iPos = InStr(sClassID, ".")
        giSotaMsgBox Nothing, moClass.moSysSession, kmsgDDNNotRegistered, _
            Left$(sClassID, iPos) & "dll"
    
        gClearSotaErr
        Exit Function
    End If
    
End Function


Public Sub CMMemoSelected(oCtl As Control)
'-- This will need to be specific to your application


End Sub

Private Sub cmdOK_Click()
    Dim lRow As Long
    Dim RcvrLineExtKey As Long
    
    'SGS DEJ START
    lRow = grdMain.ActiveRow
    RcvrLineExtKey = glGetValidLong(moDmDetl.GetColumnValue(lRow, "RcvrLineExtKey"))
    
'    If ManifestRcvrUnique(txtManifestNbr.Text, RcvrLineExtKey, moDmHeader.GetColumnValue("RcvrLineKey")) = False Then
'        MsgBox "This Manifest Number '" & txtManifestNbr.Text & "' must be unique by Receipt Of Goods Line.", vbExclamation, "MAS 500"
'        txtManifestNbr.SetFocus
'        Exit Sub
'    End If
    'SGS DEJ STOP
    
    moLE.GridEditOk
    
    'SGS DEJ START
    If ManifestExists(txtManifestNbr.Text, RcvrLineExtKey) = True Then
        MsgBox "This Manifest Number '" & txtManifestNbr.Text & "' already exists.", vbExclamation, "MAS 500"
    End If
    
    Call CalcQtyRem
    'SGS DEJ STOP
End Sub




Private Sub cmdUndo_Click()
    moLE.GridEditUndo
End Sub

Private Function bSetupNumerics() As Boolean
'*************************************************************************
' This routine will properly format all the currency controls on the form.
'*************************************************************************
    Dim bValid As Boolean
    Dim iIntegralPlaces As Integer
    
    bSetupNumerics = False

    '-- Set attributes for home currency controls
    If (Len(Trim$(msHomeCurrID)) > 0) Then
        SetHomeCurrCtrls msHomeCurrID
    Else
        '-- Apparently, the home currency hasn't been set
        giSotaMsgBox Nothing, moSysSession, kmsgSetCurrControlsError, _
            gsBuildString(kNone, moAppDB, moSysSession)
        Exit Function
    End If

    '-- Set attributes for natural currency controls
    If (Len(Trim$(msNatCurrID)) > 0) Then
        SetNatCurrCtrls msNatCurrID, False
    Else
        '-- Apparently, the natural currency hasn't been set
        giSotaMsgBox Nothing, moSysSession, kmsgSetCurrControlsError, _
            gsBuildString(kNone, moAppDB, moSysSession)
        Exit Function
    End If

    '-- Set properties for number fields
    'nbrQuantity.DecimalPlaces = moOptions.CI("QtyDecPlaces")
    
    '-- Setting the integral places at run-time sets it back to 12!! We only want 8.
    'iIntegralPlaces = IIf(kiQtyLen - moOptions.CI("QtyDecPlaces") > kiQtyIntegralPlaces, _
    '    kiQtyIntegralPlaces, kiQtyLen - moOptions.CI("QtyDecPlaces"))
    'nbrQuantity.IntegralPlaces = iIntegralPlaces
    
    bSetupNumerics = True
End Function


Private Sub SetHomeCurrCtrls(sCurrID As String)
    Dim bValid As Boolean
    Dim iIntegralPlaces As Integer

'-- The HC controls on the form need to be identified here
    

    '-- Set attributes for home currency controls
    'bValid = gbSetCurrCtls(moClass, sCurrID, muHomeCurrInfo, _
                'curInvTotalHC, curInvTotalHCS, curDetlAmtHC, curFormatHC, curBatchTotal)
    
    If bValid Then
        '-- Set integral places for Decimal(15,3) fields
    End If

    '-- Display the currency descriptions
End Sub


Private Sub SetNatCurrCtrls(sCurrID As String, Optional bRefresh As Boolean = True)
    Dim bValid As Boolean
    Dim iIntegralPlaces As Integer
    
'-- The NC controls on the form need to be identified here
    
    '-- Set attributes for natural currency controls
    
    If bValid Then
        '-- Set integral places for Decimal(15,3) fields

    End If
    
    '-- Display the currency descriptions
    
End Sub

Private Sub InitializeDetlGrid()
'*******************************************
' Desc: Initializes/formats the Detail grid.
'*******************************************
    Dim iGridType As Integer
    Dim sTitle As String

    '-- Set default grid properties
    gGridSetProperties grdMain, kColChildNbrCols, kGridLineEntry
    gGridSetColors grdMain
    gGridSetMaxCols grdMain, kColChildNbrCols
    
    
    'Hide Columns
    gGridHideColumn grdMain, kColSeqNo
    gGridHideColumn grdMain, kColRcvrLineExtKey
    gGridHideColumn grdMain, kColRcvrLineKey
'    gGridHideColumn grdMain, kColDateShipped
'    gGridHideColumn grdMain, kColDateArrived
'    gGridHideColumn grdMain, kColDateSpotted
'    gGridHideColumn grdMain, kColDateReleased
'    gGridHideColumn grdMain, kColManifestNumber
'    gGridHideColumn grdMain, kColRailCarNumber
'    gGridHideColumn grdMain, kColQtyReceived
'    gGridHideColumn grdMain, kColRefineryInvcNbr
'    gGridHideColumn grdMain, kColCarrierInvcNbr
    gGridHideColumn grdMain, kColCompanyID
    gGridHideColumn grdMain, kColSpotKey 'RKL DEJ 2016-04-14
'    gGridHideColumn grdMain, kColSpotID 'RKL DEJ 2016-04-14
    
    'Captions
    gGridSetHeader grdMain, kColSeqNo, "SeqNo"
    gGridSetHeader grdMain, kColRcvrLineExtKey, "RcvrLineExtKey"
    gGridSetHeader grdMain, kColRcvrLineKey, "RcvrLineKey"
    gGridSetHeader grdMain, kColDateShipped, "Shipped"
    gGridSetHeader grdMain, kColDateArrived, "Arrived"
    gGridSetHeader grdMain, kColDateSpotted, "Spotted"
    gGridSetHeader grdMain, kColDateReleased, "Released"
    gGridSetHeader grdMain, kColManifestNumber, "Manifest Number"
    gGridSetHeader grdMain, kColRailCarNumber, "Rail Car Number"
    gGridSetHeader grdMain, kColQtyReceived, "Qty Received"
    gGridSetHeader grdMain, kColRefineryInvcNbr, "Refinery Invoice Nbr"
    gGridSetHeader grdMain, kColCarrierInvcNbr, "Carrier Invoice Nbr"
    gGridSetHeader grdMain, kColCompanyID, "CompanyID"
    gGridSetHeader grdMain, kColSpotKey, "SpotKey"  'RKL DEJ 2016-04-14
    gGridSetHeader grdMain, kColSpotID, "SpotID"  'RKL DEJ 2016-04-14
    

    'Width
    gGridSetColumnWidth grdMain, kColSeqNo, 8
    gGridSetColumnWidth grdMain, kColRcvrLineExtKey, 8
    gGridSetColumnWidth grdMain, kColRcvrLineKey, 8
    gGridSetColumnWidth grdMain, kColDateShipped, 8
    gGridSetColumnWidth grdMain, kColDateArrived, 8
    gGridSetColumnWidth grdMain, kColDateSpotted, 8
    gGridSetColumnWidth grdMain, kColDateReleased, 8
    gGridSetColumnWidth grdMain, kColManifestNumber, 10
    gGridSetColumnWidth grdMain, kColRailCarNumber, 10
    gGridSetColumnWidth grdMain, kColQtyReceived, 8
    gGridSetColumnWidth grdMain, kColRefineryInvcNbr, 10
    gGridSetColumnWidth grdMain, kColCarrierInvcNbr, 10
    gGridSetColumnWidth grdMain, kColCompanyID, 8
    gGridSetColumnWidth grdMain, kColSpotKey, 8 'RKL DEJ 2016-04-14
    gGridSetColumnWidth grdMain, kColSpotID, 8 'RKL DEJ 2016-04-14
    
    'Type
    gGridSetColumnType grdMain, kColSeqNo, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColRcvrLineExtKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColRcvrLineKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdMain, kColDateShipped, SS_CELL_TYPE_DATE
    gGridSetColumnType grdMain, kColDateArrived, SS_CELL_TYPE_DATE
    gGridSetColumnType grdMain, kColDateSpotted, SS_CELL_TYPE_DATE
    gGridSetColumnType grdMain, kColDateReleased, SS_CELL_TYPE_DATE
    gGridSetColumnType grdMain, kColManifestNumber, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColRailCarNumber, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColQtyReceived, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColRefineryInvcNbr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColCarrierInvcNbr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColCompanyID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdMain, kColSpotKey, SS_CELL_TYPE_INTEGER   'RKL DEJ 2016-04-14
    gGridSetColumnType grdMain, kColSpotID, SS_CELL_TYPE_EDIT   'RKL DEJ 2016-04-14
    
'    'LockColumns
'    gGridLockColumn grdMain, kColSeqNo
'    gGridLockColumn grdMain, kColRcvrLineExtKey
'    gGridLockColumn grdMain, kColRcvrLineKey
'    gGridLockColumn grdMain, kColDateShipped
'    gGridLockColumn grdMain, kColDateArrived
'    gGridLockColumn grdMain, kColDateSpotted
'    gGridLockColumn grdMain, kColDateReleased
'    gGridLockColumn grdMain, kColManifestNumber
'    gGridLockColumn grdMain, kColRailCarNumber
'    gGridLockColumn grdMain, kColQtyReceived
'    gGridLockColumn grdMain, kColRefineryInvcNbr
'    gGridLockColumn grdMain, kColCarrierInvcNbr
'    gGridLockColumn grdMain, kColCompanyID
'    gGridLockColumn grdMain, kColSpotKey   'RKL DEJ 2016-04-14
    gGridLockColumn grdMain, kColSpotID   'RKL DEJ 2016-04-14
    
    
    '-- Setup the grid's Currency columns based on the Natural Currency ID
    bSetupGridNumerics
End Sub

Private Function bSetupGridNumerics() As Boolean
    bSetupGridNumerics = True
    
'-- The numeric columns in the grid need to be identified here
    

    '-- Setup the grid's Currency columns based on the Natural Currency ID

End Function

Private Sub ddnSpot_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
    moLE.GridEditChange ddnSpot
End Sub

Private Sub Form_Initialize()
    mbCancelShutDown = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
         
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
    
    End Select
End Sub

Private Sub Form_Load()
    mbLoadSuccess = False
    
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msHomeCurrID = .CurrencyID
    End With
    Set moAppDB = moClass.moAppDB
    msLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)

    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With

    Me.Caption = "Rail Car Distribution"
        
     With moOptions
        Set .oSysSession = moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    
     SetupModuleVars
    
    '-- Setup currency controls
    If Not bSetupNumerics() Then Exit Sub
    
     SetupBars
    
     SetupLookups
    
     SetupDropDowns
    
     BindForm
    
     miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmHeader, moDmDetl)
            
     BindLE
    
     InitializeDetlGrid
    
     BindContextMenu
    
     SetupOffice
        
     'SetFieldStates
    
     'SetTBButtonStates
    
    '-- Make sure the header tab is shown to the user first
'    tabVoucher.Tab = kiHeaderTab
'    tabVoucher.Tab = kiDetailTab
    
'    tabDataEntry.Tab = kiHeaderTab
    tabDataEntry.Tab = kiDetailTab

  '-- Disable controls on hidden tabs
    Dim i As Integer
'   pnlTab(kiHeaderTab).Enabled = True
   pnlTab(kiDetailTab).Enabled = True
'   For i = 0 To pnlTab.Count - 1
'        pnlTab(i).Enabled = False
'   Next i
 
    '-- Grid starts out with 500 rows.Remove the rows then add the header
    grdMain.MaxRows = 0
    grdMain.MaxRows = 1
    

    '-- We made it through Form_Load successfully
    mbLoadSuccess = True
End Sub

Private Sub Form_Paint()
    'SGS DEJ IA START
    If txtRcvrLnKey.Text <> "" And txtRcvrLnKey.Enabled = True Then
        SendKeys "{TAB}"
    End If

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
    Dim bValid As Boolean
    
    'SGS DEJ START
    If SOTANbrQtyRem.Value <> 0 Then
        Cancel = True
        MsgBox "The Quantity Remaining must be 0.", vbExclamation, "MAS 500"
        Exit Sub
    End If
    'SGS DEJ STOP
    
   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If (moClass.mlError = 0) Then
        '-- If the form is dirty, prompt the user to save the record
         bValid = bConfirmUnload(iConfirmUnload)
        
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
                
            Case kDmFailure, kDmError
                GoTo CancelShutDown
        
        End Select
              
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        'DEJ SGS Modified the code below (START)
        Select Case UnloadMode
            Case QueryUnloadConstants.vbFormCode
                'Do Nothing
                moClass.miShutDownRequester = kUnloadSelfShutDown
            
'            Case QueryUnloadConstants.vbAppTaskManager
'                'Do Nothing
'
'            Case QueryUnloadConstants.vbAppWindows
'                'Do Nothing
'
'            Case QueryUnloadConstants.vbFormControlMenu
'                'Do Nothing
'
'            Case QueryUnloadConstants.vbFormMDIForm
'                'Do Nothing
'
'            Case QueryUnloadConstants.vbFormOwner
'                'Do Nothing
            
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                moClass.miShutDownRequester = kUnloadSelfShutDown
        End Select
        'DEJ SGS Modified the code below (STOP)
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.


    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub
End Sub

Public Sub PerformCleanShutDown()

#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

'-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    '-- Clean up object references
    If Not moDmHeader Is Nothing Then
        moDmHeader.UnloadSelf
        Set moDmHeader = Nothing
    End If
    
    If Not moDmDetl Is Nothing Then
        moDmDetl.UnloadSelf
        Set moDmDetl = Nothing
    End If
    

    '-- Clean up Line Entry references
    If Not moLE Is Nothing Then
        moLE.UnloadSelf
        Set moLE = Nothing
    End If
    
    '-- Fire Terminate event for lookups
    TerminateControls Me

    '-- Clean up other objects
    Set moOptions = Nothing
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing
    Set moAppDB = Nothing
    Set moSysSession = Nothing

End Sub

Private Sub Form_Resize()
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
    '    '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
                        pnlTab(kiDetailTab), _
                        grdMain, tabDataEntry

        '-- Move the controls to the right
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
                        pnlTab(kiDetailTab), _
                        grdMain, tabDataEntry
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set moClass = Nothing
End Sub

Public Function LECheckRequiredData(oLE As clsLineEntry) As Boolean
'***********************************************************************
' Desc: Soft touch validation in case hard validation at the
'       source control was click-bypassed.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
    LECheckRequiredData = False

'-- Validate data from Line Entry here before pushing it into the grid



    LECheckRequiredData = True
End Function

Public Sub LEClearDetailControls(oLE As Object)
'***********************************************************************
' Description: Clear detail fields on the Line Entry object.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
'    ClearDetlFields True
    ClearDetlFields
    
    'SGS DEJ START
    Call CalcQtyRem
    'SGS DEJ STOP

    
End Sub

Public Sub LESetDetailControlDefaults(oLE As clsLineEntry)
'************************************************************************************
'   Description:
'           Detail edit controls loading point.
'   Parameter:
'           oLE - the line entry class object calling this procedure.
'************************************************************************************

'-- Set up default values for Line Entry here


End Sub

Public Sub LEDetailToGrid(oLE As clsLineEntry)
'***********************************************************************
' Desc: If any detail entry controls need to be manually linked to
'       the grid after entry, then do it here.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************

'-- Update any unbound columns in the grid here

    'RKL DEJ 2016-04-14 (START)
    Dim lRow As Long
    
    lRow = oLE.ActiveRow
    
    'Load SpotKey
    If ddnSpot.ListIndex <> kItemNotSelected Then
        gGridUpdateCell grdMain, lRow, kColSpotKey, ddnSpot.ItemData(ddnSpot.ListIndex)
    Else
        gGridUpdateCell grdMain, lRow, kColSpotKey, ""
    End If
    
   'Load SpotID Text
    gGridUpdateCell grdMain, lRow, kColSpotID, ddnSpot.Text
    
    'RKL DEJ 2016-04-14 (STOP)

End Sub

Public Sub LEGridToDetail(oLE As clsLineEntry)
'***********************************************************************
' Desc: If any grid columns need to be manually linked to the detail
'       entry controls, then do it here.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************

    '-- Set up Line Entry from the grid here
    
    'PSG
    Dim lRow As Long
    
    lRow = oLE.ActiveRow
'    lkuChildItemKey.Text = gsGridReadCell(grdMain, lRow, kColChildItemKey)
    
    
    'RKL DEJ 2016-04-14 (START)
    Dim lSpotKey As Long
    
    'Load Spot Combo Box.
    lSpotKey = val(gsGridReadCell(grdMain, lRow, kColSpotKey))
    
    If kColSpotKey = 0 Then
        ddnSpot.ListIndex = kItemNotSelected
    Else
        ddnSpot.ListIndex = ddnSpot.GetIndexByItemData(lSpotKey)
    End If

    ddnSpot.Tag = ddnSpot.ListIndex
    
    'RKL DEJ 2016-04-14 (STOP)
    
    
    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset
End Sub

Public Function LEGridBeforeDelete(oLE As clsLineEntry, lRow As Long) As Boolean
'************************************************************************
'   Description:
'       called from line entry class when a row has been successfully
'       deleted from the grid
'   Param:
'       oLE -   line entry class making the call
'       lRow -  grid row deleted
'************************************************************************
    
    LEGridBeforeDelete = False
    
'-- Perform processing before a line is deleted from the grid here

    
    LEGridBeforeDelete = True
End Function

Public Sub LEGridAfterDelete(oLE As clsLineEntry, lRow As Long)
'************************************************************************
'   Description:
'       called from line entry class when a row has been successfully
'       deleted from the grid
'   Param:
'       oLE -   line entry class making the call
'       lRow -  grid row deleted
'************************************************************************

'-- Perform processing after a line is deleted from the grid here
    
    Call CalcQtyRem

End Sub



Public Sub LESetDetailFocus(oLE As clsLineEntry, Optional Col As Variant)
'***********************************************************************
' Desc: Double-clicking on any grid column will drive focus to the
'       detail oControl representing the Selected grid column.
' Parameters: oLE - the Line Entry class object calling this procedure.
'             Col - The column to set the focus into.
'***********************************************************************
    Dim oControl As Object

'-- Link up columns to controls in Line Entry here
End Sub

Private Sub moDmDetl_DMGridAfterDelete(lRow As Long, bValid As Boolean)
    bValid = False

'-- Perform processing after a row is deleted from the detail table here


    bValid = True
End Sub

Private Sub moDmDetl_DMGridAfterInsert(lRow As Long, bValid As Boolean)
    bValid = False

'-- Perform processing after a row is inserted into the detail table here

    
    bValid = True
End Sub

Private Sub moDmDetl_DMGridAfterUpdate(lRow As Long, bValid As Boolean)
    bValid = False

'-- Perform processing after a row is updated in the detail table here

    
    bValid = True
End Sub

Private Sub moDmDetl_DMGridAppend(lRow As Long)
    '-- This adds a new row to the grandchild detail grid


'-- Perform processing after a row is appended to the detail grid here

End Sub

Private Sub moDmDetl_DMGridRowLoaded(lRow As Long)
'************************************************************************************
'   Description:
'      Fires when each row of data is loaded by the DM into the grid.
'      This is a manual movement of a copy of the data from the DM to the correct
'      grid cell on the same row.
'************************************************************************************
Dim DBValue As Integer

'-- Perform processing as each row is loaded into the grid
    Call CalcQtyRem
End Sub

Private Sub moDmHeader_DMAfterDelete(bValid As Boolean)
    bValid = False
    
'-- Perform processing after a record is deleted from the main table here

    
    bValid = True
End Sub

Private Sub moDmHeader_DMBeforeInsert(bValid As Boolean)
    Dim lKey As Long
    On Error GoTo CancelInsert
    bValid = True

    With moClass.moAppDB
        .SetInParam "tarInvoice_DJP"
        .SetOutParam lKey
        .ExecuteSP ("spGetNextSurrogateKey")
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With
    moDmHeader.SetColumnValue "InvoiceKey", lKey
   
    Exit Sub
CancelInsert:
    bValid = False
End Sub

Private Sub moDmHeader_DMBeforeUpdate(bValid As Boolean)
    bValid = False
    
'-- Perform processing before a record is updated in the main table here

    
    bValid = True
End Sub

Private Sub moDmHeader_DMAfterUpdate(bValid As Boolean)
    bValid = False

'-- Perform processing after a record is updated in the main table here

    
    bValid = True
End Sub

Private Sub moDmHeader_DMBeforeDelete(bValid As Boolean)
    bValid = False

'-- Perform processing before a record is deleted from the main table here


    bValid = True
End Sub

Private Sub moDmHeader_DMDataDisplayed(oChild As Object)
    If oChild Is moDmHeader Then

'-- Perform processing as a record is loaded onto the form after data is displayed here


    End If
End Sub

Private Sub moDmHeader_DMPostSave(bValid As Boolean)
    bValid = False

'-- Perform processing after a record is saved to the main table here

    
    bValid = True
End Sub

Private Sub moDmHeader_DMPreSave(bValid As Boolean)
  'PSG
  CalcTotals
End Sub

Private Sub moDmHeader_DMReposition(oChild As Object)
    If oChild Is moDmHeader Then

'-- Perform processing as a record is loaded onto the form before data is displayed here
    End If
    
End Sub

Private Sub moDmHeader_DMStateChange(iOldState As Integer, iNewState As Integer)
    '
    If iNewState = kDmStateNone Then
        If Not moLE Is Nothing Then
            If moLE.State <> kGridNone Then
                On Error Resume Next
                moLE.InitDataReset
            End If
        End If
    End If
End Sub

Private Sub moDmHeader_DMBeforeTransaction(bValid As Boolean)
'*******************************************************************
' Description:
'    This routine will be called by Data Manager before the record
'    is saved. This is where form-level validation should occur.
'*******************************************************************
    bValid = False

'-- Perform validations and other processes that need to occur before the DB transaction here

        
    bValid = True
End Sub

Private Sub lkuInvoiceNo_LookupClick(bCancel As Boolean)
    bCancel = Not bConfirmUnload(0, True)

End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
    HandleToolbarClick sButton
End Sub

Private Sub SOTACalArrived_Change()
On Error Resume Next
    moLE.GridEditChange SOTACalArrived
    Exit Sub

End Sub

Private Sub SOTACalReleased_Change()
On Error Resume Next
    moLE.GridEditChange SOTACalReleased
    Exit Sub

End Sub

Private Sub SOTACalShipped_Change()
On Error Resume Next
    moLE.GridEditChange SOTACalShipped
    Exit Sub

End Sub

Private Sub SOTACalSpotted_Change()
On Error Resume Next
    moLE.GridEditChange SOTACalSpotted
    Exit Sub

End Sub

Private Sub SOTANbrQtyRec_Change()
    'SGS DEJ START
    Call CalcQtyRem
    'SGS DEJ STOP

End Sub

Private Sub SOTANbrQtyRecvd_KeyPress(KeyAscii As Integer)
    moLE.GridEditChange SOTANbrQtyRecvd
    Exit Sub

End Sub

Private Sub txtCarrierNbr_Change()
On Error Resume Next
    moLE.GridEditChange txtCarrierNbr
    Exit Sub
End Sub

Private Sub txtManifestNbr_Change()
On Error Resume Next
    moLE.GridEditChange txtManifestNbr
    Exit Sub
End Sub

Private Sub txtRailCarNbr_Change()
On Error Resume Next
    moLE.GridEditChange txtRailCarNbr
    Exit Sub

End Sub

Private Sub txtRcvrLnKey_LostFocus()
'    txtRcvrLnKey.Visible = False
End Sub


Private Sub txtRefineryNbr_Change()
On Error Resume Next
    moLE.GridEditChange txtRefineryNbr
    Exit Sub

End Sub

Private Sub valMgr_KeyChange()
    Dim iKeyChangeCode  As Integer

    'moDmHeader.SetColumnValue "CompanyID", msCompanyID
    moDmHeader.SetColumnValue "CompanyID", msCompanyID
    iKeyChangeCode = moDmHeader.KeyChange
    
    'SGS DEJ START
    Call CalcQtyRem
    'SGS DEJ STOP
    
    Select Case iKeyChangeCode
        Case kDmKeyFound, kDmKeyNotFound
            moLE.InitDataLoaded
            moLE.Grid_Click 1, 1
        Case kDmKeyNotComplete
            MsgBox "valMgr_KeyChange - key  not complete"
    End Select
End Sub


Private Sub valMgr_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
    'mbFromCode = True
    
    'iReturn = SOTA_INVALID
    
'-- Call individual validation routines for controls here
End Sub

Private Sub ClearDetlFields()

'-- Clear out controls that are not bound to Line Entry here
    txtManifestNbr.Text = Empty
    txtRailCarNbr.Text = Empty
    txtRefineryNbr.Text = Empty
    txtCarrierNbr.Text = Empty

    SOTACalShipped.Value = Empty
    SOTACalArrived.Value = Null
    SOTACalSpotted.Value = Null
    SOTACalReleased.Value = Null
    SOTANbrQtyRecvd.Value = 0

    '-- Set the detail controls as valid so the old values are correct
    valMgr.Reset
End Sub

Private Sub tabSubDetl_Click(PreviousTab As Integer)
    '-- Disable controls on hidden tabs
    HandleSubTabClick
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
    HandleToolbarClick Button
End Sub

Private Sub tabDataEntry_Click(PreviousTab As Integer)
    Dim bValid As Boolean
    Static bInTabChange As Boolean
    
    If bInTabChange Then Exit Sub
    
    bInTabChange = True
    bValid = True
    
    If (PreviousTab = kiDetailTab) And (tabDataEntry.Tab <> PreviousTab) Then
        bValid = moLE.TabChange(tabDataEntry.Tab, PreviousTab)
    End If
    
    If bValid Then

'-- Handle specific tab change logic here

    End If
    
    'PSG - Calc totals
    If (tabDataEntry.Tab = kiTotalsTab) And (PreviousTab <> tabDataEntry.Tab) Then
      CalcTotals
    End If
    
    
    '-- Disable controls on hidden tabs
    pnlTab(PreviousTab).Enabled = False
    pnlTab(tabDataEntry.Tab).Enabled = True
        
    bInTabChange = False
End Sub

Private Sub HandleTabClick()
    Dim i As Integer
    
    '-- Disable all panels
End Sub

Private Sub HandleSubTabClick()
    Dim i As Integer
    
    '-- Disable all panels
'    '-- Enable the visible panel
End Sub

Private Sub Form_Activate()
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmHeader
                moFormCust.ApplyFormCust
        End If
    End If
#End If

    Dim i As Integer

    '-- Setup the form (validation) manager control
    With valMgr
        Set .Framework = moClass.moFramework
        '.Keys.Add lkuVouchNo
'        .Keys.Add lkuInvoiceNo
        .Keys.Add txtRcvrLnKey
        .Init
    End With

End Sub


Public Sub grdMain_Click(ByVal Col As Long, ByVal Row As Long)
    If (moLE.GridEditDone(Col, Row) <> kLeSuccess) Then
        Exit Sub
    End If

    '-- Select the row that the user Selected
    moLE.Grid_Click Col, Row
End Sub

Private Sub grdMain_DblClick(ByVal Col As Long, ByVal Row As Long)
    moLE.Grid_DblClick Col, Row
End Sub

Private Sub grdMain_DragDrop(Source As Control, x As Single, y As Single)
    moLE.Grid_DragDrop x, y
End Sub

Private Sub grdMain_DragOver(Source As Control, x As Single, y As Single, State As Integer)
    moLE.Grid_DragOver x, y, State
End Sub

Private Sub grdMain_GotFocus()
    moLE.Grid_GotFocus
End Sub

Public Sub grdMain_KeyDown(KeyCode As Integer, Shift As Integer)
    moLE.Grid_KeyDown KeyCode, Shift
End Sub

Private Sub grdMain_LostFocus()
    moLE.Grid_LostFocus
End Sub

Private Sub grdMain_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    moLE.Grid_MouseDown Button, Shift, x, y
End Sub

Private Sub grdMain_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    moLE.Grid_MouseMove Button, Shift, x, y
End Sub

Private Sub moDmDetl_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
    
    bValid = False
    Dim lKey As Long
    
    With moClass.moAppDB
        .SetInParamStr "tpoRcvrLineExt_SGS"
        .SetOutParam lKey
        .ExecuteSP "spGetNextSurrogateKey"
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With
    
    moDmDetl.SetColumnValue lRow, "RcvrLineExtKey", lKey
    moDmDetl.SetColumnValue lRow, "RcvrLineKey", moDmHeader.GetColumnValue("RcvrLineKey")
    moDmDetl.SetColumnValue lRow, "CompanyID", msCompanyID
    
    bValid = True
End Sub

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal TaskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    ETWhereClause = ""
    
    ' Specific checks go here
    
    Err.Clear
    
End Function



Private Sub CustomButton_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
#End If
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
#End If
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
#End If
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
#End If
End Sub

Private Sub CustomOption_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
#End If
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
#End If
End Sub

Private Sub picdrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseDown Index, Button, Shift, x, y
    End If
#End If
End Sub

Private Sub picdrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseMove Index, Button, Shift, x, y
    End If
#End If
End Sub

Private Sub picdrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_MouseUp Index, Button, Shift, x, y
    End If
#End If
End Sub

Private Sub picdrag_Paint(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.picdrag_Paint Index
    End If
#End If
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
#End If
End Sub

Private Sub CustomCheck_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
#End If
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
#End If
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
#End If
End Sub

Private Sub CustomCombo_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
#End If
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
#End If
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
#End If
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
#End If
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
#End If
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
#End If
End Sub

Private Sub CustomDate_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
#End If
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
#End If
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
#End If
End Sub

Private Sub CustomFrame_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
#End If
End Sub

Private Sub CustomLabel_Click(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
#End If
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
#End If
End Sub

Private Sub CustomMask_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
#End If
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
#End If
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
#End If
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
#End If
End Sub

Private Sub CustomNumber_Change(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
#End If
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
#End If
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
#End If
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
#End If
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
#End If
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
#End If
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
#End If
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
#End If
End Sub

Private Sub txtChildInvoiceLineKey_Change()    'LE Control Change Event
    moLE.GridEditChange txtChildInvoiceLineKey
    Exit Sub
End Sub

Private Sub txtChildInvoiceKey_Change()    'LE Control Change Event
    moLE.GridEditChange txtChildInvoiceKey
    Exit Sub
End Sub

Private Sub txtChildSeqNo_Change()    'LE Control Change Event
    moLE.GridEditChange txtChildSeqNo
    Exit Sub
End Sub

'End of Sage MAS 500 Generated Code

Private Sub CalcTotals()
  'PSG
  
  Dim dTotalAmt As Double
  Dim dAmt As Double
  Dim lRow As Long
  
'  dTotalAmt = 0
'  For lRow = 1 To glGetValidLong(glGridGetDataRowCnt(grdMain))
'    dAmt = gdGetValidDbl(gsGridReadCell(grdMain, lRow, _
'                         kColChildQtyShipped)) * _
'           gdGetValidDbl(gsGridReadCell(grdMain, lRow, _
'                         kColChildUnitPrice))
'    dTotalAmt = dTotalAmt + dAmt
'  Next lRow
'
'  curTranAmt.Amount = dTotalAmt
  
End Sub



































