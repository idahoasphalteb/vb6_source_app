VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Begin VB.Form frmViewRcptOfGoods 
   Caption         =   "View Receivers"
   ClientHeight    =   6795
   ClientLeft      =   405
   ClientTop       =   1380
   ClientWidth     =   10815
   HelpContextID   =   17772076
   Icon            =   "pozdg001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6795
   ScaleMode       =   0  'User
   ScaleWidth      =   10815
   Begin FPSpreadADO.fpSpread grdJobLine 
      Height          =   735
      Left            =   -75000
      TabIndex        =   86
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   17772077
      Width           =   1455
      _Version        =   524288
      _ExtentX        =   2566
      _ExtentY        =   1296
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "pozdg001.frx":23D2
      AppearanceStyle =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtPurchCompany 
      Height          =   285
      Left            =   6480
      TabIndex        =   49
      Top             =   600
      WhatsThisHelpID =   17772078
      Width           =   1155
      _Version        =   65536
      _ExtentX        =   2034
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtVendID 
      Height          =   285
      Left            =   900
      TabIndex        =   46
      Top             =   945
      WhatsThisHelpID =   17772079
      Width           =   1635
      _Version        =   65536
      _ExtentX        =   2884
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtRelNo 
      Height          =   285
      Left            =   10140
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   945
      WhatsThisHelpID =   17772080
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      lMaxLength      =   15
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtPONum 
      Height          =   285
      Left            =   8910
      TabIndex        =   3
      Top             =   945
      WhatsThisHelpID =   17772081
      Width           =   1125
      _Version        =   65536
      _ExtentX        =   1984
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      lMaxLength      =   10
   End
   Begin SOTACalendarControl.SOTACalendar calRcptDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   8940
      TabIndex        =   8
      Top             =   600
      WhatsThisHelpID =   17772082
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      BackColor       =   -2147483633
      DisplayButton   =   0   'False
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin TabDlg.SSTab tabReceipt 
      Height          =   5025
      Left            =   0
      TabIndex        =   10
      Top             =   1350
      WhatsThisHelpID =   17772083
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   8864
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "&Header"
      TabPicture(0)   =   "pozdg001.frx":2810
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lblBOL"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblComment1"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "grdRcvrLineDist"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtTranComment"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtBOL"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmdAdditionalNotes1"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "cmdVendPerf"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmdPOLandedCost"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "&Lines"
      TabPicture(1)   =   "pozdg001.frx":282C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "shpFocusRect"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "grdRcptDetl"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      Begin VB.CommandButton cmdPOLandedCost 
         Caption         =   "&Landed Cost..."
         Height          =   375
         Left            =   -67110
         TabIndex        =   88
         Top             =   1590
         WhatsThisHelpID =   17770915
         Width           =   1785
      End
      Begin VB.CommandButton cmdVendPerf 
         Caption         =   "V&endor Performance..."
         Height          =   375
         Left            =   -67110
         TabIndex        =   87
         Top             =   1080
         WhatsThisHelpID =   17772084
         Width           =   1785
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   2355
         Left            =   90
         TabIndex        =   36
         Top             =   420
         Width           =   10575
         Begin VB.CommandButton cmdVendPerfDtl 
            Caption         =   "Vendor Performance ..."
            Height          =   315
            Left            =   8280
            TabIndex        =   89
            Top             =   720
            WhatsThisHelpID =   17772085
            Width           =   1815
         End
         Begin VB.CommandButton cmdOK 
            Caption         =   "O&K"
            Height          =   315
            Left            =   8790
            TabIndex        =   52
            Top             =   45
            Visible         =   0   'False
            WhatsThisHelpID =   100208
            Width           =   1305
         End
         Begin VB.CommandButton cmdUndo 
            Caption         =   "U&ndo"
            Height          =   315
            Left            =   8790
            TabIndex        =   51
            Top             =   390
            Visible         =   0   'False
            WhatsThisHelpID =   100207
            Width           =   1305
         End
         Begin NEWSOTALib.SOTANumber nbrQtyRcvd 
            Height          =   285
            Left            =   120
            TabIndex        =   15
            Top             =   750
            WhatsThisHelpID =   17772088
            Width           =   1710
            _Version        =   65536
            _ExtentX        =   3016
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
            text            =   "           0.000"
            sDecimalPlaces  =   3
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtItemDescription 
            Height          =   285
            Left            =   4245
            TabIndex        =   37
            Top             =   240
            WhatsThisHelpID =   17772089
            Width           =   2880
            _Version        =   65536
            _ExtentX        =   5080
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
            sBorder         =   0
            text            =   "Item Description ============="
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtPOLine 
            Height          =   285
            Left            =   135
            TabIndex        =   38
            Top             =   225
            WhatsThisHelpID =   17772090
            Width           =   1005
            _Version        =   65536
            bPadLeft        =   -1  'True
            _ExtentX        =   1773
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin TabDlg.SSTab tabSubDetl 
            Height          =   1215
            Left            =   60
            TabIndex        =   17
            Top             =   1140
            WhatsThisHelpID =   17772091
            Width           =   10425
            _ExtentX        =   18389
            _ExtentY        =   2143
            _Version        =   393216
            TabOrientation  =   1
            Style           =   1
            Tabs            =   5
            TabsPerRow      =   8
            TabHeight       =   520
            TabCaption(0)   =   "&Project"
            TabPicture(0)   =   "pozdg001.frx":2848
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblTask"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblPhase"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblProject"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lkuProject"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lkuPhase"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lkuTask"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).ControlCount=   6
            TabCaption(1)   =   "&Original Order"
            TabPicture(1)   =   "pozdg001.frx":2864
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblQtyShip"
            Tab(1).Control(0).Enabled=   0   'False
            Tab(1).Control(1)=   "lblUOM(1)"
            Tab(1).Control(1).Enabled=   0   'False
            Tab(1).Control(2)=   "Label5"
            Tab(1).Control(2).Enabled=   0   'False
            Tab(1).Control(3)=   "Label6"
            Tab(1).Control(3).Enabled=   0   'False
            Tab(1).Control(4)=   "Label8"
            Tab(1).Control(4).Enabled=   0   'False
            Tab(1).Control(5)=   "lblQtyReturn"
            Tab(1).Control(5).Enabled=   0   'False
            Tab(1).Control(6)=   "nbrQtyShip"
            Tab(1).Control(6).Enabled=   0   'False
            Tab(1).Control(7)=   "nbrQtyOpen"
            Tab(1).Control(7).Enabled=   0   'False
            Tab(1).Control(8)=   "nbrQtyReturn"
            Tab(1).Control(8).Enabled=   0   'False
            Tab(1).Control(9)=   "nbrTotRcvd"
            Tab(1).Control(9).Enabled=   0   'False
            Tab(1).Control(10)=   "nbrQtyOrd"
            Tab(1).Control(10).Enabled=   0   'False
            Tab(1).Control(11)=   "txtUOM"
            Tab(1).Control(11).Enabled=   0   'False
            Tab(1).ControlCount=   12
            TabCaption(2)   =   "Comm&ents"
            TabPicture(2)   =   "pozdg001.frx":2880
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLnComment"
            Tab(2).Control(0).Enabled=   0   'False
            Tab(2).Control(1)=   "txtLnComment"
            Tab(2).Control(1).Enabled=   0   'False
            Tab(2).Control(2)=   "cmdCommentDtl"
            Tab(2).Control(2).Enabled=   0   'False
            Tab(2).ControlCount=   3
            TabCaption(3)   =   "&Misc"
            TabPicture(3)   =   "pozdg001.frx":289C
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "chkCloseDistr"
            Tab(3).Control(0).Enabled=   0   'False
            Tab(3).Control(1)=   "chkMatch"
            Tab(3).Control(1).Enabled=   0   'False
            Tab(3).ControlCount=   2
            TabCaption(4)   =   "Rail Car"
            TabPicture(4)   =   "pozdg001.frx":28B8
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "SSPanel2"
            Tab(4).Control(0).Enabled=   0   'False
            Tab(4).ControlCount=   1
            Begin EntryLookupControls.TextLookup lkuTask 
               Height          =   285
               Left            =   3720
               TabIndex        =   82
               TabStop         =   0   'False
               Top             =   120
               WhatsThisHelpID =   17772092
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   503
               ForeColor       =   -2147483640
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               VisibleLookup   =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "TaskNum_FAN "
               ParentIDColumn  =   "chrTaskNumber"
               ParentKeyColumn =   "intTaskKey"
               ParentTable     =   "tPA00005"
               BoundColumn     =   "intTaskKey"
               BoundTable      =   "papoLine"
               IsForeignKey    =   -1  'True
               Datatype        =   0
               sSQLReturnCols  =   "chrTaskNumber,,;chrTaskName,,;"
            End
            Begin EntryLookupControls.TextLookup lkuPhase 
               Height          =   285
               Left            =   795
               TabIndex        =   81
               TabStop         =   0   'False
               Top             =   450
               WhatsThisHelpID =   17772093
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   503
               ForeColor       =   -2147483640
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               VisibleLookup   =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "PhaseNum_FAN"
               ParentIDColumn  =   "chrPhaseNumber"
               ParentKeyColumn =   "intPhaseKey"
               ParentTable     =   "tPA00002"
               BoundColumn     =   "intPhaseKey"
               BoundTable      =   "papoLine"
               IsForeignKey    =   -1  'True
               Datatype        =   0
               sSQLReturnCols  =   "chrPhaseNumber,,;chrPhaseName,,;"
            End
            Begin EntryLookupControls.TextLookup lkuProject 
               Height          =   285
               Left            =   795
               TabIndex        =   80
               TabStop         =   0   'False
               Top             =   120
               WhatsThisHelpID =   17772094
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   503
               ForeColor       =   -2147483640
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               VisibleLookup   =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Project"
               ParentIDColumn  =   "chrJobNumber"
               ParentKeyColumn =   "intJobKey"
               ParentTable     =   "tPA00175"
               BoundColumn     =   "intJobKey"
               BoundTable      =   "papoLine"
               IsForeignKey    =   -1  'True
               Datatype        =   0
               sSQLReturnCols  =   "chrJobNumber,lkuProject,;chrJobName,,;"
            End
            Begin VB.CommandButton cmdCommentDtl 
               Caption         =   "Addit&ional Notes..."
               Height          =   315
               Left            =   -66270
               TabIndex        =   65
               Top             =   120
               WhatsThisHelpID =   17772095
               Width           =   1605
            End
            Begin VB.CheckBox chkMatch 
               Caption         =   "Match"
               Enabled         =   0   'False
               Height          =   195
               Left            =   -74760
               TabIndex        =   64
               Top             =   120
               Value           =   1  'Checked
               WhatsThisHelpID =   17772096
               Width           =   945
            End
            Begin VB.CheckBox chkCloseDistr 
               Caption         =   "Close PO Line for Receiving"
               Enabled         =   0   'False
               Height          =   195
               Left            =   -74760
               TabIndex        =   63
               Top             =   420
               WhatsThisHelpID =   17772097
               Width           =   3225
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtLnComment 
               Height          =   615
               Left            =   -74100
               TabIndex        =   66
               Top             =   150
               WhatsThisHelpID =   17772098
               Width           =   4755
               _Version        =   65536
               _ExtentX        =   8387
               _ExtentY        =   1085
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtUOM 
               Height          =   285
               Left            =   -73110
               TabIndex        =   68
               Top             =   330
               WhatsThisHelpID =   17772099
               Width           =   765
               _Version        =   65536
               _ExtentX        =   1349
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               lMaxLength      =   15
               text            =   "XXXXXX"
            End
            Begin NEWSOTALib.SOTANumber nbrQtyOrd 
               Height          =   285
               Left            =   -74880
               TabIndex        =   69
               Top             =   330
               WhatsThisHelpID =   17772100
               Width           =   1710
               _Version        =   65536
               _ExtentX        =   3016
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrTotRcvd 
               Height          =   285
               Left            =   -72270
               TabIndex        =   70
               Top             =   330
               WhatsThisHelpID =   17772101
               Width           =   1710
               _Version        =   65536
               _ExtentX        =   3016
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrQtyReturn 
               Height          =   285
               Left            =   -70500
               TabIndex        =   71
               Top             =   330
               WhatsThisHelpID =   17772102
               Width           =   1530
               _Version        =   65536
               _ExtentX        =   2699
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrQtyOpen 
               Height          =   285
               Left            =   -68880
               TabIndex        =   72
               Top             =   330
               WhatsThisHelpID =   17772103
               Width           =   1530
               _Version        =   65536
               _ExtentX        =   2699
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrQtyShip 
               Height          =   285
               Left            =   -70500
               TabIndex        =   78
               Top             =   330
               Visible         =   0   'False
               WhatsThisHelpID =   17772104
               Width           =   1530
               _Version        =   65536
               _ExtentX        =   2699
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin Threed.SSPanel SSPanel2 
               Height          =   675
               Left            =   -74880
               TabIndex        =   90
               Top             =   120
               Width           =   10155
               _Version        =   65536
               _ExtentX        =   17912
               _ExtentY        =   1191
               _StockProps     =   15
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BevelOuter      =   0
               Begin VB.CommandButton cmd_RailDist 
                  Caption         =   "Rail Car Distribution"
                  Height          =   420
                  Left            =   3840
                  TabIndex        =   91
                  Top             =   0
                  Width           =   1815
               End
            End
            Begin VB.Label lblProject 
               AutoSize        =   -1  'True
               Caption         =   "Project"
               Height          =   195
               Left            =   120
               TabIndex        =   85
               Top             =   120
               Width           =   495
            End
            Begin VB.Label lblPhase 
               AutoSize        =   -1  'True
               Caption         =   "Phase"
               Height          =   195
               Left            =   120
               TabIndex        =   84
               Top             =   450
               Width           =   450
            End
            Begin VB.Label lblTask 
               AutoSize        =   -1  'True
               Caption         =   "Task"
               Height          =   195
               Left            =   3150
               TabIndex        =   83
               Top             =   120
               Width           =   360
            End
            Begin VB.Label lblQtyReturn 
               AutoSize        =   -1  'True
               Caption         =   "Qty Returned"
               Height          =   195
               Left            =   -70500
               TabIndex        =   79
               Top             =   120
               Width           =   945
            End
            Begin VB.Label Label8 
               AutoSize        =   -1  'True
               Caption         =   "Qty Open"
               Height          =   195
               Left            =   -68910
               TabIndex        =   77
               Top             =   120
               Width           =   675
            End
            Begin VB.Label Label6 
               AutoSize        =   -1  'True
               Caption         =   "Prior Received"
               Height          =   195
               Left            =   -72300
               TabIndex        =   76
               Top             =   120
               Width           =   1050
            End
            Begin VB.Label Label5 
               AutoSize        =   -1  'True
               Caption         =   "Qty Ordered"
               Height          =   195
               Left            =   -74880
               TabIndex        =   75
               Top             =   120
               Width           =   855
            End
            Begin VB.Label lblUOM 
               AutoSize        =   -1  'True
               Caption         =   "UOM"
               Height          =   195
               Index           =   1
               Left            =   -73110
               TabIndex        =   74
               Top             =   120
               Width           =   405
            End
            Begin VB.Label lblQtyShip 
               AutoSize        =   -1  'True
               Caption         =   "Total Shipped"
               Height          =   195
               Left            =   -70500
               TabIndex        =   73
               Top             =   120
               Visible         =   0   'False
               Width           =   990
            End
            Begin VB.Label lblLnComment 
               AutoSize        =   -1  'True
               Caption         =   "Comment"
               Height          =   195
               Left            =   -74880
               TabIndex        =   67
               Top             =   150
               Width           =   660
            End
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtDept 
            Height          =   285
            Left            =   4650
            TabIndex        =   39
            Top             =   750
            WhatsThisHelpID =   17772105
            Width           =   1710
            _Version        =   65536
            _ExtentX        =   3016
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtWhse 
            Height          =   285
            Left            =   2820
            TabIndex        =   40
            Top             =   750
            WhatsThisHelpID =   17772106
            Width           =   1710
            _Version        =   65536
            _ExtentX        =   3016
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtItemOrdID 
            Height          =   285
            Left            =   1620
            TabIndex        =   45
            Top             =   210
            WhatsThisHelpID =   17772107
            Width           =   2535
            _Version        =   65536
            _ExtentX        =   4471
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtRcvUOM 
            Height          =   285
            Left            =   1950
            TabIndex        =   50
            Top             =   750
            WhatsThisHelpID =   17772108
            Width           =   765
            _Version        =   65536
            _ExtentX        =   1349
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin VB.Label lblDept 
            Caption         =   "Department"
            Height          =   255
            Left            =   4650
            TabIndex        =   44
            Top             =   510
            Width           =   1245
         End
         Begin VB.Label lblWhse 
            Caption         =   "Warehouse"
            Height          =   255
            Left            =   2850
            TabIndex        =   43
            Top             =   510
            Width           =   1245
         End
         Begin VB.Label lblQtyRcvd 
            AutoSize        =   -1  'True
            Caption         =   "Qty &Received"
            Height          =   195
            Left            =   120
            TabIndex        =   14
            Top             =   540
            Width           =   975
         End
         Begin VB.Label lblUOM 
            AutoSize        =   -1  'True
            Caption         =   "&UOM"
            Height          =   195
            Index           =   0
            Left            =   1950
            TabIndex        =   16
            Top             =   510
            Width           =   375
         End
         Begin VB.Label lblItemID 
            AutoSize        =   -1  'True
            Caption         =   "Item"
            Height          =   195
            Left            =   1620
            TabIndex        =   42
            Top             =   0
            Width           =   300
         End
         Begin VB.Label lblPOLine 
            AutoSize        =   -1  'True
            Caption         =   "PO Line"
            Height          =   195
            Left            =   135
            TabIndex        =   41
            Top             =   15
            Width           =   570
         End
      End
      Begin VB.CommandButton cmdAdditionalNotes1 
         Caption         =   "&Additional Notes..."
         Height          =   375
         Left            =   -67110
         TabIndex        =   13
         Top             =   570
         WhatsThisHelpID =   17772109
         Width           =   1785
      End
      Begin FPSpreadADO.fpSpread grdRcptDetl 
         Height          =   2025
         Left            =   150
         TabIndex        =   34
         Top             =   2820
         WhatsThisHelpID =   17772110
         Width           =   10410
         _Version        =   524288
         _ExtentX        =   18362
         _ExtentY        =   3572
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "pozdg001.frx":28D4
         AppearanceStyle =   0
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtBOL 
         Height          =   285
         Left            =   -73470
         TabIndex        =   47
         Top             =   720
         WhatsThisHelpID =   17772111
         Width           =   1875
         _Version        =   65536
         _ExtentX        =   3307
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtTranComment 
         Height          =   285
         Left            =   -73470
         TabIndex        =   48
         Top             =   1110
         WhatsThisHelpID =   17772112
         Width           =   5295
         _Version        =   65536
         _ExtentX        =   9340
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
      End
      Begin FPSpreadADO.fpSpread grdRcvrLineDist 
         Height          =   615
         Left            =   -74400
         TabIndex        =   62
         Top             =   2640
         Visible         =   0   'False
         WhatsThisHelpID =   17772113
         Width           =   1695
         _Version        =   524288
         _ExtentX        =   2990
         _ExtentY        =   1085
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "pozdg001.frx":2D39
         AppearanceStyle =   0
      End
      Begin VB.Label lblComment1 
         Caption         =   "&Comment"
         Height          =   225
         Left            =   -74790
         TabIndex        =   12
         Top             =   1140
         Width           =   1245
      End
      Begin VB.Label lblBOL 
         Caption         =   "B&ill of Lading"
         Height          =   225
         Left            =   -74790
         TabIndex        =   11
         Top             =   750
         Width           =   1245
      End
      Begin VB.Shape shpFocusRect 
         Height          =   315
         Left            =   120
         Top             =   960
         Visible         =   0   'False
         Width           =   585
      End
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.TextBox txtClassOvrdSegValue 
      Height          =   285
      Left            =   -10000
      MaxLength       =   50
      MultiLine       =   -1  'True
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   17772115
      Width           =   1800
   End
   Begin SOTAVM.SOTAValidationMgr SOTAValidationMgr1 
      Left            =   5910
      Top             =   30
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6405
      WhatsThisHelpID =   73
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   688
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   23
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   29
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   741
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtVendName 
      Height          =   285
      Left            =   2880
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   960
      WhatsThisHelpID =   17772127
      Width           =   3240
      _Version        =   65536
      _ExtentX        =   5715
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bAutoSelect     =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTACurrency curInvAmtS 
      Height          =   285
      Left            =   -10000
      TabIndex        =   31
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   17772128
      Width           =   1500
      _Version        =   65536
      _ExtentX        =   2646
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency curPurchAmtS 
      Height          =   285
      Left            =   -10000
      TabIndex        =   32
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   17772129
      Width           =   1800
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   3175
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "            0.00"
      sIntegralPlaces =   13
      sDecimalPlaces  =   2
   End
   Begin EntryLookupControls.TextLookup lkuReceiptNo 
      Height          =   285
      Left            =   3540
      TabIndex        =   1
      Top             =   600
      WhatsThisHelpID =   17772130
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   10
      LookupID        =   "Receiver"
      BoundColumn     =   "VouchNo"
      BoundTable      =   "tapPendVoucher"
      sSQLReturnCols  =   "TranNo,lkuReceiptNo,;CompanyID,,;"
   End
   Begin SOTADropDownControl.SOTADropDown ddnType 
      Height          =   315
      Left            =   900
      TabIndex        =   53
      Top             =   570
      WhatsThisHelpID =   17772131
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Text            =   "ddnType"
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtShipWhse 
      Height          =   285
      Left            =   900
      TabIndex        =   56
      Top             =   945
      Visible         =   0   'False
      WhatsThisHelpID =   17772132
      Width           =   1635
      _Version        =   65536
      _ExtentX        =   2884
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtShipWhseDesc 
      Height          =   285
      Left            =   2880
      TabIndex        =   57
      TabStop         =   0   'False
      Top             =   960
      Visible         =   0   'False
      WhatsThisHelpID =   17772133
      Width           =   3240
      _Version        =   65536
      _ExtentX        =   5715
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bAutoSelect     =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtTransit 
      Height          =   285
      Left            =   6480
      TabIndex        =   59
      Top             =   600
      Visible         =   0   'False
      WhatsThisHelpID =   17772134
      Width           =   1155
      _Version        =   65536
      _ExtentX        =   2037
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtTransfer 
      Height          =   285
      Left            =   8910
      TabIndex        =   61
      Top             =   945
      Visible         =   0   'False
      WhatsThisHelpID =   17772135
      Width           =   1635
      _Version        =   65536
      _ExtentX        =   2884
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      lMaxLength      =   10
   End
   Begin VB.Label lblTransfer 
      AutoSize        =   -1  'True
      Caption         =   "Transfer"
      Height          =   195
      Left            =   7920
      TabIndex        =   60
      Top             =   1005
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.Label lblTransit 
      AutoSize        =   -1  'True
      Caption         =   "Transit"
      Height          =   195
      Left            =   5520
      TabIndex        =   58
      Top             =   660
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblShipWhse 
      AutoSize        =   -1  'True
      Caption         =   "Ship Whse"
      Height          =   195
      Left            =   60
      TabIndex        =   55
      Top             =   1005
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      Caption         =   "Type"
      Height          =   195
      Left            =   60
      TabIndex        =   54
      Top             =   660
      Width           =   360
   End
   Begin VB.Label lblDash 
      Caption         =   "-"
      Height          =   285
      Left            =   10050
      TabIndex        =   4
      Top             =   990
      Width           =   75
   End
   Begin VB.Label lblRcptNo 
      AutoSize        =   -1  'True
      Caption         =   "Receipt"
      Height          =   195
      Left            =   2880
      TabIndex        =   35
      Top             =   660
      Width           =   555
   End
   Begin VB.Label lblPONo 
      AutoSize        =   -1  'True
      Caption         =   "&PO"
      Height          =   195
      Left            =   7920
      TabIndex        =   2
      Top             =   1005
      Width           =   255
   End
   Begin VB.Label lblRcptDate 
      AutoSize        =   -1  'True
      Caption         =   "Receipt &Date"
      Height          =   195
      Left            =   7920
      TabIndex        =   7
      Top             =   660
      Width           =   975
   End
   Begin VB.Label lblVendID 
      AutoSize        =   -1  'True
      Caption         =   "&Vendor"
      Height          =   195
      Left            =   60
      TabIndex        =   9
      Top             =   1005
      Width           =   510
   End
   Begin VB.Label lblPurchCompany 
      AutoSize        =   -1  'True
      Caption         =   "Purch Comp"
      Height          =   195
      Left            =   5520
      TabIndex        =   5
      Top             =   660
      Width           =   870
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   28
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmViewRcptOfGoods"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If
' constants
Const kEntTypePOReceiver = 1109
Const kEntTypePORcvrLine = 1110

'Receipt Type (matches ddnType index values)
Private Enum enumReceiptType
    kReceiptTypePurchase = 0
    kReceiptTypeTransfer = 1
End Enum

'-- Public Form Variables
    Public moSotaObjects            As New Collection

'-- Context Menu Object
    Private moContextMenu           As clsContextMenu

'-- Data Manager Object Variables
    Public WithEvents moDmHeader    As clsDmForm    '-- Public for CTI
Attribute moDmHeader.VB_VarHelpID = -1
    Private WithEvents moDmDetl     As clsDmGrid
Attribute moDmDetl.VB_VarHelpID = -1
    Private WithEvents moDmLineDist As clsDmGrid
Attribute moDmLineDist.VB_VarHelpID = -1

'-- Binding Object Variables
    Private moLE                    As New clsLineEntry
    Private moOptions               As New clsModuleOptions
    Private moIMSClass              As New clsIMS
    Private moItem                  As clsIMSItem
    Private moPOEnterLandedCost     As Object
    

'-- Miscellaneous Variables
    Private mbEnterAsTab            As Boolean
    Private msCompanyID             As String
    Private msPurchCompanyID        As String
    Private msUserID                As String
    Private msHomeCurrID            As String
    Private miSecurityLevel         As Integer
    Private miFilter                As Integer
    Private mlLanguage              As Long
    Private mlTranType              As Long
    Private msTranType              As String

'-- Form resize variables
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long

'-- Private variables of Form Properties
    Public moClass                  As Object    '-- Public for CTI
    Private moAppDB                 As Object
    Private moSysSession            As Object
    Private mbCancelShutDown        As Boolean
    Private mbLoadSuccess           As Boolean
    Private mbAllowInterCo          As Boolean
    Private mbShowMsg               As Boolean
    Private mbOverflow              As Boolean
    Private mbInBrowse              As Boolean
    Private mbSaved                 As Boolean
    Private mbFromCode              As Boolean
    Private mbValidKeyChange        As Boolean
    Private mbFromTargetCo          As Boolean
    Private mbFromNav               As Boolean
    Private mbLoadingRec            As Boolean
    Private mbAllowKeyChange        As Boolean
    Private mbLineJoinCreated       As Boolean
    Private mbLoadingPORow          As Boolean
    Private mbPOCleared             As Boolean
    Private mbIntegrateWithIM       As Boolean
    Private mbCloseSrcLine           As Boolean
    Private miCaptureWght           As Integer
    Private miCaptureVol            As Integer
    Private miRcptDaysEarly         As Integer
    Private mbFilterByDate          As Boolean
    Private mbLoadingForm           As Boolean  'Controls whether certain controls will be disabled.
    Private mbTrackByBin            As Boolean  'Does the warehouse track by bin??
    Private mlPurchUOMKey           As Long     'The Purchase UOM for the current line.
    Private mdQtyInPOUOM            As Double   'The qty received in PO UOM
    Private mlIMTempTranID          As Long     'The tran id for unsaved IM Distribuitons
    Private miIMDistDirty           As Integer  'Flag to indicate that the UOM or Qty has changed for
                                                '  a line that is associated with an IM Distribution.
    Private mlPOLineKey             As Long     'The PO Line Key for the current Rcvr Line.
    ' Variables for retaining data
    Private mlItemKey               As Long
    Private mlUOMKey                As Long
    Private mlInvtKey               As Long
    Private msVolUOM                As String
    Private msWghtUOM               As String
    Private mdItemVol               As Double
    Private mdItemWght              As Double
    Private mdRcvrVol               As Double
    Private mdRcvrWght              As Double
    Private mdFreightAmt            As Double
    Private mdSTaxAmt               As Double
    Private mdUnitCost              As Double
    
    Private miPostDateYear          As Integer
    Private miSignFactor            As Integer
    Private mlRunMode               As Long
    Private mlVRunMode              As lVRunModes
    Private mlBatchkey              As Long
    Private mlWhseKey               As Long
    Private mlOrigPOKey             As Long
    Private mlCurrExchSchdKey       As Long
    Private mlCurrExchSchdKeyOld    As Long
    Private mlRcvrLineKey           As Long
    Private mlInvtTranKey           As Long
    Private msPostDate              As String
    Private msFormCaption           As String
    Private msApplyFromTranID       As String
    Private msRcvrNoLbl             As String
    Private msCurrID                As String
    Private mcTranTypes             As New Collection
    Private mcICGLAcctMask          As New Collection
    Private muNatCurrInfo           As CurrencyInfo
    Private muHomeCurrInfo          As CurrencyInfo
    Private miReceiptType           As Integer
    
    Private Type uRcvrLineType
        lRcvrLineKey            As Long
        dItemWght               As Double
        dItemVol                As Double
        lDistTranKey            As Long
    End Type
    Private muRcvrLine           As uRcvrLineType
    
    'Vendor performance detail fields
    Private muVendPerfDtl       As uVendPerfDtl

        
'-- Constants
    '-- ReceiverLog TranStatus constants
    Private Const kvIncompleteReceiverLog = 1  'Incomplete
    Private Const kvPendingReceiverLog = 2     'Pending
    Private Const kvPostedReceiverLog = 3      'Posted
    Private Const kvPurgedReceiverLog = 4      'Purged
    Private Const kvVoidReceiverLog = 5        'Void
        
    '-- Sign Factor constants
    Private Const kiPosSignFactor = 1
    Private Const kiNegSignFactor = -1
    
    '-- Tab page constants
    Private Const kiHeaderTab = 0
    Private Const kiDetailTab = 1
    
    '-- Detail Tab page constants
    Private Const kiProjectTab = 0
    Private Const kTabOrigOrder = 1
    Private Const kTabComments = 2
    Private Const kTabMisc = 3
    
    '-- Constants for Insert or Update Mode
    Private Const kiInsertMode = 0
    Private Const kiUpdateMode = 1
    
    '-- Arbitrary value for extra secret super special flag
    Private Const kiExtraSpecial = -999
    
    '-- Drill-Down task ID constants
    Private Const ktskPOPurchOrdDA = 100
    Private Const ktskPORcptDA = 200
        
    '-- Static List constants
    'Receipt Status constants
    Private Const kvReceiptOpen = 1             'Open        Val=1
    
    'Vendor Status constants
    Private Const kvVendorActive = 1            'Active      Val=1 *Default*
    Private Const kvVendorTemporary = 3         'Temporary   Val=3
    
    'Item Status constants
    Private Const kvItemActive = 1              'Active           Val=1
    
    'Item Type constants
    Private Const kvItemMiscItem = 1            'Misc Item        Val=1
    Private Const kvItemExpense = 3             'Expense          Val=3
    Private Const kvItemCommentOnly = 4         'Comment Only     Val=4
        
    'Meas Type constants
    Private Const kvMeasTypeNone = 0
    
    'GL Account Status constants
    Private Const kvGLAcctStatusActive = 1      'Active      *Default*
    
    'GL Account Category ID constants
    Private Const kvGLAcctCatIDNonFinancial = 9 'Non-Financial
        
    'GL Account Currency Restriction constants
    Private Const kiGLAcctCurrRestrictSpecific = 1
        
    'PO Status constants
    Private Const kvPOOpen = 1                  'Open        Val=1
    
    'PO Closed for Rcving status
    Private Const kvOpenForRcving = 0           'NO
    
    'Receipt Tran Size
    Private Const kvRcptTranSize = 10
    
    'Rcpt Tran types
    Private Const kvTranRcpt = 1110
    Private Const kvtranRtrn = 1106
    
    'Weight & Vol capture types
    Private Const kvCaptureNone = 0
    Private Const kvCaptureOptional = 1
    Private Const kvCaptureRequired = 2

'-- Remove this ENUM, clsDmForm and frmGetID after 3.0!!
'    Public Enum StatusType    '   Sage MAS 500 StatusBar Status Images
'        Sage MAS 500_DMSB_NONE = 0
'        Sage MAS 500_DMSB_START = 1
'        Sage MAS 500_DMSB_ADD = 2
'        Sage MAS 500_DMSB_EDIT = 3
'        Sage MAS 500_DMSB_SAVE = 4
'        Sage MAS 500_DMSB_LOCKED = 5
'        Sage MAS 500_DMSB_INQUIRY = 6
'        Sage MAS 500_DMSB_BUSY = 7
'    End Enum
    
    ' *** RMM 5/2/01, Context menu project
    Private mbIMActivated As Boolean
    
    Private moProject               As clsProject

Const VBRIG_MODULE_ID_STRING = "POZDF001.FRM"

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bLoadSuccess = mbLoadSuccess
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadSuccess_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let sFormCaption(sNewFormCaption As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msFormCaption = sNewFormCaption
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sFormCaption_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lVRunMode(lNewVRunMode As lVRunModes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlVRunMode = lNewVRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lVRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim oFramework As cFramework
    
    Set oFramework = moClass.moFramework
    
'-- Main fields
    '-- Receipt
    With lkuReceiptNo
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
        
        If (mlVRunMode = kViewEditReceiptDDN) Or (mlVRunMode = kViewEditReceiptDTE) Then
            .BoundTable = "tpoReceiver"
            .LookupID = "Receiver"
        End If
        
    End With
        
    '-- Release the internal variables
    Set oFramework = Nothing
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sGetReceiptNoRestrict() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    sGetReceiptNoRestrict = "CompanyID = " & gsQuoted(msCompanyID)
    sGetReceiptNoRestrict = sGetReceiptNoRestrict & " AND TranType = " & mlTranType
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetReceiptNoRestrict", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bSetupReceiver() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs As Object

    bSetupReceiver = False
    
    mbIntegrateWithIM = CBool(moOptions.PO("IntegrateWithIM")) And (moClass.mlWhseKey <> 0) ' Integrate with IM
    
    lkuReceiptNo.RestrictClause = sGetReceiptNoRestrict()
    
    ' Set focus to the header tab
    tabReceipt.Tab = 0
  
    If Not mbDefaultQty Then
        tabSubDetl.Tab = kTabComments ' Set it to the Comments tab
    Else
        'tabSubDetl.Tab = kiProjectTab
    End If
    
    bSetupReceiver = True
    
    mbLoadingForm = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupReceiver", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub SetupBars()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
    '-- Setup the status bar
    Set sbrMain.Framework = moClass.moFramework
    
    '-- Setup the toolbar
    With tbrMain
            '-- Standard Data Entry mode
            .Style = sotaTB_TRANSACTION
            .RemoveButton kTbCopyFrom
            .RemoveButton kTbRenameId
            .AddButton kTbClose, .GetIndex(kTbFinish)
            .RemoveButton kTbFinish
            .RemoveButton kTbSave
            .RemoveButton kTbDelete
            .RemoveButton kTbNextNumber
            .RemoveButton kTbPrint
            .RemoveSeparator kTbHelp
        Select Case mlVRunMode
            Case kViewEditReceiptDDN
                '-- Drill Down mode or AOF mode
                sbrMain.BrowseVisible = False
            Case Else
                sbrMain.BrowseVisible = True
        End Select
        .LocaleID = mlLanguage
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupModuleVars()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Currency variables
    
    '-- Most signs will be positive, Credit Memo's are stored as negative numbers
    miSignFactor = kiPosSignFactor
    
    '-- Assign the initial filter value
    miFilter = RSID_UNFILTERED

    '-- Set misc. flags
    mbLoadingRec = False
    mbFromNav = False
    mbManualClick = False
    mbAllowKeyChange = True
    mbLoadingPORow = False
    mbPOCleared = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupModuleVars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetFieldStates()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iIntegralPlaces As Integer
    Dim iButtonTop As Integer
    
    '-- Initialize toolbar button states
    SetNextNumberState
    
    ' If not defaulting quantity, then hide the Original Order tab
    If Not mbDefaultQty Then
        tabSubDetl.TabVisible(kTabOrigOrder) = False
    End If
    
    '-- Determine if PA is active
    mbIntPAActive = gbIsModuleActive(moClass, kModulePA)

    If mbIntPAActive Then
        tabSubDetl.TabVisible(kiProjectTab) = True
    Else
        tabSubDetl.TabVisible(kiProjectTab) = False
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFieldStates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine will bind fields on the form to fields in the database.
'************************************************************************
    BindHeader
    BindDetail
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindHeader()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Bind the parent DM object
    Set moDmHeader = New clsDmForm
    
    With moDmHeader
        Set .Form = frmViewRcptOfGoods
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Toolbar = tbrMain
        Set .SOTAStatusBar = sbrMain
        Set .ValidationMgr = SOTAValidationMgr1
        .AppName = gsStripChar(Me.Caption, ".")
        .UniqueKey = "CompanyID, TranNo, TranType"
        .OrderBy = "TranNo"
'        .AccessType = kDmBuildQueries
        .SaveOrder = 3
        
        '-- Bind columns common to both pending and posted receiver tables
        .Bind Nothing, "BatchKey", SQL_INTEGER
        .Bind txtBOL, "BillOfLadingNo", SQL_CHAR
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind Nothing, "CreateType", SQL_SMALLINT
        .Bind txtPurchCompany, "PurchCompanyID", SQL_CHAR
        .Bind txtTranComment, "TranCmnt", SQL_VARCHAR, kDmSetNull
        .Bind calRcptDate, "TranDate", SQL_DATE
        .Bind lkuReceiptNo, "TranNo", SQL_CHAR, kDmDisplayColumn
        .Bind Nothing, "TranType", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind Nothing, "VendKey", SQL_INTEGER
        .Bind Nothing, "POKey", SQL_INTEGER
        .Bind Nothing, "TrnsfrOrderKey", SQL_INTEGER
        .Bind Nothing, "RcvrKey", SQL_INTEGER
        .Bind Nothing, "VendPerfExceptional", SQL_SMALLINT
        .Bind Nothing, "VendPerfUpgradeSubstItem", SQL_SMALLINT
        .Bind Nothing, "VendPerfUnauthSubstItem", SQL_SMALLINT
        .Bind Nothing, "VendPerfPoorCustService", SQL_SMALLINT
        .Bind Nothing, "VendPerfOtherProblem", SQL_SMALLINT
        .Bind Nothing, "VendPerfDeliveryWrongTime", SQL_SMALLINT
        .Bind Nothing, "VendPerfDeliveryWrongDest", SQL_SMALLINT
        .Bind Nothing, "VendPerfDeliveryWrongDate", SQL_SMALLINT
        .Bind Nothing, "VendPerfComment", SQL_VARCHAR, kDmDisplayColumn
        
        .Table = "tpoReceiver"
                
        '-- Bind columns specifically for posted receivers
        .Bind Nothing, "PostDate", SQL_DATE
                            
        '-- Setup surrogate key and description field links
        .LinkSource "tapVendor", "VendKey=<<VendKey>>"
        .Link txtVendName, "VendName"
        .Link txtVendID, "VendID"
        
        .LinkSource "tpoPurchOrder", "POKey=<<POKey>>"
        .Link txtPONum, "TranNo"
        .Link txtRelNo, "TranNoRel"

        .LinkSource "timTrnsfrOrder", "TrnsfrOrderKey=<<TrnsfrOrderKey>>"
        .Link txtTransfer, "TranNo"
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindHeader", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindDetail()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Bind the receiver detail DM object
    Set moDmDetl = New clsDmGrid

    With moDmDetl
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = Me
        Set .Grid = grdRcptDetl
        Set .Parent = moDmHeader
        .Table = "tpoRcvrLine"
        .UniqueKey = "RcvrLineKey"
        .OrderBy = "SeqNo"
        .SaveOrder = 1
        
        .NoAppend = True
        
        '-- Bind the detail columns
        .BindColumn "RcvrLineKey", kColReceiptLineKey, SQL_INTEGER
        .BindColumn "TranCmnt", kColTranComment, SQL_VARCHAR, txtLnComment, kDmSetNull
        .BindColumn "SeqNo", kColSeqNo, SQL_INTEGER
        .BindColumn "UnitCost", kColUnitCost, SQL_DECIMAL, , kDmSetNull
        .BindColumn "ItemRcvdKey", kColItemKey, SQL_INTEGER
        .BindColumn "ItemVol", kColItemVol, SQL_INTEGER
        .BindColumn "ItemWght", kColItemWght, SQL_INTEGER
        .BindColumn "InvtTranKey", kColInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "UnitMeasKey", kColUnitMeasRcvdKey, SQL_INTEGER
        .BindColumn "RtrnType", kColReturnType, SQL_SMALLINT
        .BindColumn "UpdateCounter", Nothing, SQL_INTEGER
        .BindColumn "POLineKey", kColPOLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TrnsfrOrderLineKey", kColTrnsfrLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "MatchStatus", kColMatchStatus, SQL_SMALLINT
        .BindColumn "CloseSrcLine", kColCloseSrcLine, SQL_SMALLINT
        .BindColumn "TaxAmt", kColSTaxAmt, SQL_DECIMAL
        .BindColumn "VendPerfDefectiveGoods", kColDefective, SQL_SMALLINT
        .BindColumn "VendPerfDamagedGoods", kColDamaged, SQL_SMALLINT
        .BindColumn "VendPerfCarrierDamage", kColCarrierDamage, SQL_SMALLINT
        .BindColumn "VendPerfImproperLabeling", kColImpLabel, SQL_SMALLINT
        .BindColumn "VendPerfMissingDoc", kColDocMissing, SQL_SMALLINT
        .BindColumn "VendPerfImproperPackaging", kColImpPackage, SQL_SMALLINT
        .BindColumn "VendPerfNotToSpec", kColNotToSpec, SQL_SMALLINT
        .BindColumn "VendPerfOtherProblem", kColOtherIssue, SQL_SMALLINT
        .BindColumn "VendPerfComment", kColPerfComments, SQL_VARCHAR
        
        .LinkSource "#tpoPOJoin", "(tpoRcvrLine.POLineKey=#tpoPOJoin.POLineKey OR tpoRcvrLine.TrnsfrOrderLineKey=#tpoPOJoin.TrnsfrOrderLineKey)", kDmJoin
        .Link kColPOLineNo, "POLineNo"
        .Link kColItemID, "POItemID"
        .Link kColItemDesc, "POItemDesc"
        .Link kColWhseID, "POWhseID"
        .Link kColDeptID, "PODeptID"
        .Link kColQtyOrdered, "POQtyOrd"
        .Link kColUnitMeasOrdID, "POUOMID"
        .Link kColTotalRcvd, "POQtyRcvd"
        .Link kColQtyRtrned, "POQtyRtrned"
        .Link kColQtyOpen, "POQtyOpen"
        .Link kColUnitMeasOrdKey, "POUOMKey"
        .Link kColMeasType, "POUOMType"
        .Link kColQtyShip, "TRQtyShip"

        .LinkSource "tpoRcvrLineDist", "tpoRcvrLine.RcvrLineKey=tpoRcvrLineDist.RcvrLineKey", kDmJoin, LeftOuter
        .Link kColQtyRcvd, "QtyRcvd"
        .Link kColQtyInPOUOM, "QtyRcvdInSrcUOM"
        .Link kColFreightAmt, "FreightAmt"
        
        .LinkSource "tciUnitMeasure", "tpoRcvrLine.UnitMeasKey=tciUnitMeasure.UnitMeasKey", kDmJoin, LeftOuter
        .Link kColUnitMeasRcvdID, "UnitMeasID"
        
        .ParentLink "RcvrKey", "RcvrKey", SQL_INTEGER

        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindDetail", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindLE()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Desc:   Binds grids to standard Line Entry object manager.
'
'  Parms:   None.
'************************************************************************************
    '-- Initiate a new Line Entry object for the Receiver Detail data
    Set moLE = New clsLineEntry
    
    With moLE
        Set .Grid = grdRcptDetl
        .GridType = iGetGridType()
        Set .TabControl = tabReceipt
        .TabDetailIndex = kiDetailTab
        Set .DM = moDmDetl
        Set .Ok = cmdOK
        Set .Undo = cmdUndo
        ' **PRESTO ** Set .Hook = WinHook1
        .SeqNoCol = kColSeqNo
        .AllowGotoLine = True
        Set .FocusRect = shpFocusRect
        Set .Form = frmViewRcptOfGoods
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLE", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        .BindGrid moLE, grdRcptDetl.hwnd
        .Bind "PORCPTLINE", grdRcptDetl.hwnd, kEntTypePORcptLine

        .Bind "PORECEIPT", lkuReceiptNo.hwnd, kEntTypePOReceipt
        .Bind "IMITEM", txtItemOrdID.hwnd, kEntTypeIMItem
        .Bind "PAPROJECT", lkuProject.hwnd, kEntTypePAProject
        .Bind "PAPHASE", lkuPhase.hwnd, kEntTypePAPhase
        .Bind "PATASK", lkuTask.hwnd, kEntTypePATask
        .Bind "POPURCHORD", txtPONum.hwnd, kEntTypePOPurchOrder
        .Bind "IMTRANSFER", txtTransfer.hwnd, kEntTypeIMTransfer
        
        .Bind "APDA03", txtVendID.hwnd, kEntTypeAPVendor

        .Bind "IMDA08", txtWhse.hwnd, kEntTypeIMWarehouse
        

        Set .Form = frmViewRcptOfGoods
    
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    Select Case True
        Case ctl Is lkuReceiptNo
            ETWhereClause = "RcvrKey = " & CStr(glGetValidLong(moDmHeader.GetColumnValue("RcvrKey")))
        Case ctl Is txtPONum
            ETWhereClause = "POKey = " & CStr(glGetValidLong(moDmHeader.GetColumnValue("POKey")))
        Case ctl Is grdRcptDetl
            ETWhereClause = "RcvrLineKey = " & CStr(glGetValidLong(moDmDetl.GetColumnValue(grdRcptDetl.ActiveRow, "RcvrLineKey")))
        Case ctl Is txtTransfer
            ETWhereClause = "TrnsfrOrderKey = " & CStr(glGetValidLong(moDmHeader.GetColumnValue("TrnsfrOrderKey")))
        Case Else
            ETWhereClause = ""
    End Select
    
    Err.Clear
    
End Function

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    Select Case sKey
        Case kTbFinish, kTbFinishExit
            If Not bProcessBeforeSave() Then Exit Sub
            
            If (moDmHeader.Action(kDmFinish) = kDmSuccess) Then
                ClearFields
                moProject.ResetJobGrid
            End If
            
        Case kTbCancel, kTbCancelExit, kTbClose
            ProcessCancel
            moProject.ResetJobGrid

        Case kTbDelete
            If (moDmDetl.Action(kDmDelete) = kDmSuccess) Then
                If (moDmHeader.Action(kDmDelete) = kDmSuccess) Then
                    ClearFields
                    moProject.ResetJobGrid
                End If
            End If

        Case kTbNextNumber
            HandleNextNumberClick
            moProject.ResetJobGrid
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
            
        Case kTbMemo
            CMMemoSelected
        
        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'moDmHeader.CopyFrom

        Case kTbRenameId
            moDmHeader.RenameID
            
        Case kTbFilter
            miFilter = giToggleLookupFilter(miFilter)
            
        '-- Trap the browse control buttons
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            HandleBrowseClick sKey
                        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmHeader, moClass
            
    End Select

    If (sKey = kTbClose) Then
        Select Case mlVRunMode
            Case kViewEditReceiptDDN
                moClass.moFramework.SavePosSelf
                moClass.miShutDownRequester = kFrameworkShutDown
                mbCancelShutDown = True
                Me.Hide
            Case Else
                moClass.miShutDownRequester = kUnloadSelfShutDown
                Unload Me
        End Select
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
            
Private Function bProcessBeforeSave() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bProcessBeforeSave = False
    
    '-- Make sure all controls on the form are validated
    If Not SOTAValidationMgr1.ValidateForm Then Exit Function
    
    '-- To remedy a strange bug (ICS #13848) where line entry does not handle
    '-- the last row in the grid if it is blank
' MVB - I don't know if I need this
'    UndoNewBlankRow
            
    '-- If the grid is in edit/add state, force a save at this point
    If (moLE.GridEditDone() <> kLeSuccess) Then Exit Function

    bProcessBeforeSave = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessBeforeSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub HandleNextNumberClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sNextRcptNo As String
    If Not bConfirmUnload(0) Then Exit Sub
    
    '-- Clear the form
    ProcessCancel
    
    '-- Retrieve the next Receipt Number now
    sNextRcptNo = sGetNextRcptNo()
    
    If (Len(Trim$(sNextRcptNo)) > 0) Then
        '-- Assign the next Receipt No to the control
        lkuReceiptNo = sNextRcptNo
        
        '-- Fire the KeyChange event now
        mbAllowKeyChange = True
        SOTAValidationMgr1_KeyChange
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleNextNumberClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleBrowseClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant

    If Not mbInBrowse Then
        If Not bConfirmUnload(0) Then Exit Sub
    End If
    
    '-- Execute the requested move
    lRet = glLookupBrowse(lkuReceiptNo, sKey, miFilter, sNewKey)
    
    '-- Evaluate outcome of requested browse move
    Select Case lRet
        Case MS_SUCCESS
        
            If lkuReceiptNo.ReturnColumnValues.Count = 0 Then
                Exit Sub
            End If

            If StrComp(Trim(lkuReceiptNo.Text), Trim(lkuReceiptNo.ReturnColumnValues("TranNo")), vbTextCompare) <> 0 Then
                    '-- Assign the returned number
               lkuReceiptNo.Text = Trim(lkuReceiptNo.ReturnColumnValues("TranNo"))
            End If

            '-- Fire the KeyChange event now
            mbAllowKeyChange = True
            SOTAValidationMgr1_KeyChange

        Case Else
            '-- Error processing
            mbInBrowse = True
            gLookupBrowseError lRet, Me, moClass
            mbInBrowse = False
    
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleBrowseClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lrcptkey As Long
    Dim bValid As Boolean
    
    
    With moDmHeader
        '-- If the current record is a new receiver, save the
        '-- Rcvr so we can delete the unnecessary receiver log record
        If (.State = kDmStateAdd) Then
            lrcptkey = glGetValidLong(.GetColumnValue("RcvrKey"))
        End If

        .Action kDmCancel

    End With
    
    '-- Clear out controls not cleared by Data Manager
    ClearFields

    SetCtrlFocus lkuReceiptNo
    ' -- Prevent an extra line from displaying in the grid
'    grdRcptDetl.MaxRows = grdRcptDetl.MaxRows - 1
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessCancel", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ClearFields()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************
' Desc: Clear out fields and variables and start afresh!
'*********************************************************
    miSignFactor = kiPosSignFactor
    mbLoadingRec = False
    mbFromNav = False
    mbAllowKeyChange = True
    mbLoadingPORow = False
    mbPOCleared = False
    msApplyFromTranID = ""
    txtPurchCompany = ""
    txtPONum = ""
    txtRelNo = ""
    
    '-- Clear Vendor Defaults structure
    With muVendDflts
        .lVendKey = 0
        .lPrimaryAddrKey = 0
        .lDfltPurchAcctKey = 0
        .lDfltItemKey = 0
        .bLoadedDfltItem = False
    End With
        
    '-- Clear PO-specific fields
    If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Or _
       (mlVRunMode = kViewEditReceiptDTE) Or (mlVRunMode = kViewEditReceiptDDN) Then
        mlOrigPOKey = 0
        ClearPODefaultsStruct
    End If
    
    '-- Clear Transfer-specific fields
    txtShipWhse = ""
    txtShipWhseDesc = ""
    txtTransit = ""
    nbrQtyShip = 0#
    Me.txtItemDescription = ""
    
    ClearDetlFields True
    
    '-- Initialize the data in the Line Entry object
    moLE.InitDataReset
    
    '-- Set the detail controls as valid so the old values are correct
    SOTAValidationMgr1.Reset
        
    lkuReceiptNo.Protected = False
    
    ddnType.Enabled = True
    SetCtrlFocus lkuReceiptNo
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearFields", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bConfirmUnload(iConfirmUnload As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    iConfirmUnload = moDmHeader.ConfirmUnload()

    If (iConfirmUnload = kDmSuccess) Or (iConfirmUnload = kDmNotAllowed) Then
        If (moDmHeader.State = kDmStateNone) Then
            ProcessCancel
        End If
        
        bConfirmUnload = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bConfirmUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sGetNextRcptNo() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
'   Description:
'           Given the Company ID, get and assign the next Receipt Number.
'   Parameters:
'           oDB <in>:  The database variable to use.
'           sCompanyID <in>:  The Company ID to read tciTranTypeCompany with.
'           lNumCharsInMask <in>:  The Number of characters in the
'                                  Receiver mask.
'           lTranType <in>:  The Transaction Type to get the next Number for.
'           sNextRcptNo <out>:  The next Receipt Number to use.
'   Returns:
'           True if sp was successful, false if the stored procedure failed.
'*****************************************************************************
    Dim lRet As Long
    Dim sRcptNo As String

    sGetNextRcptNo = ""
    
    With moAppDB
        .SetInParam msCompanyID
        .SetInParam 10
        .SetInParam mlTranType
        .SetOutParam sRcptNo
        .SetOutParam lRet
        .ExecuteSP "sppoGetNextRcptNo"
        
        If (Err.Number <> 0) Then
            giSotaMsgBox Me, moSysSession, kmsgGetVouchNoError, msRcvrNoLbl, msRcvrNoLbl
        Else
            sGetNextRcptNo = glGetValidLong(.GetOutParam(4))
            lRet = glGetValidLong(.GetOutParam(5))
        End If
        
        .ReleaseParams
    End With
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetNextRcptNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'***********************************************************************************
'RKL DEJ Custom Methods (START)
'***********************************************************************************
Private Sub cmd_RailDist_Click() 'SGS DEJ IA
    'Launch the Screen
    Dim oDrillDown      As Object
    Dim lContractKey    As Long
    Dim lTask       As Long
    Dim sClass      As String
    Dim iTranType   As Integer
    Dim lLineKey    As Long
    Dim lRow As Long
    
    'Require saving before entering Rail Car Distribution.
    If moDmHeader.IsDirty = True Then
        If MsgBox("Do you want to save your changes?  You must save before entering the Rail Car Distribution.", vbQuestion + vbYesNo, "MAS 500 Save?") = vbYes Then
            HandleToolbarClick kTbSave
            
            If moDmHeader.IsDirty = True Then Exit Sub
        Else
            Exit Sub
        End If
    End If
    
    On Error GoTo ExpectedErrorRoutine
    
    lRow = grdRcptDetl.ActiveRow
    lLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey))
    
    
    'Make sure a line record is selected
    If lLineKey > 0 Then
        
        'Require the Landed Cost be entred.
        If 0 >= glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "tpoRcvrLineDistExt_SGS", "RcvrLineKey = " & lLineKey & " And LandCostKey Is Not Null ")) Then
            MsgBox "You must enter the landed cost first.", vbExclamation, "MAS 500"
            Exit Sub
        End If
        
        
        lTask = 1010089004  'RKL DEJ 1/28/14 Change to Correct TaskID
        sClass = "pozxxRailDist.clspozxxRailDist"
        Set oDrillDown = goGetSOTAChild(moClass.moFramework, moSotaObjects, sClass, lTask, kAOFRunFlags, kContextAOF)
    End If
    If Not oDrillDown Is Nothing Then
        Me.Enabled = False
        oDrillDown.DrillDown lLineKey
        Set oDrillDown = Nothing
        Me.Enabled = True
        If Me.Visible Then Me.SetFocus
    End If
    
    Exit Sub

ExpectedErrorRoutine:
    MsgBox Me.Name & ".cmd_RailDist_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Me.Enabled = True
    Me.SetFocus
    If Not (oDrillDown Is Nothing) Then
        Set oDrillDown = Nothing
    End If
    gClearSotaErr
    Exit Sub
End Sub
'***********************************************************************************
'RKL DEJ Custom Methods (STOP)
'***********************************************************************************

Private Sub cmdAdditionalNotes1_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdAdditionalNotes1, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
On Error GoTo ExpectedErrorRoutine
    
'    If bDontClick Then
''+++ VB/Rig Begin Pop +++
''+++ VB/Rig End +++
'        Exit Sub
'    End If
    
    Dim oNotes As Object
    Set oNotes = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                        kClsSelEditCommentsAOF, ktskSelEditCommentsAOF, kAOFRunFlags, kContextAOF)
    If Not oNotes Is Nothing Then
        oNotes.EditComments glGetValidLong(moDmHeader.GetColumnValue("RcvrKey")), kEntTypePOReceiver
    End If

Exit Sub

ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "cmdNotes_Click"
gClearSotaErr
Exit Sub

End Sub

Private Sub cmdCommentDtl_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdCommentDtl, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
On Error GoTo ExpectedErrorRoutine
    
'    If bDontClick Then
'        Exit Sub
'    End If
    
    Dim oNotes As Object
    Set oNotes = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kClsSelEditCommentsAOF, ktskSelEditCommentsAOF, kAOFRunFlags, kContextAOF)
    If Not oNotes Is Nothing Then
        oNotes.EditComments muRcvrLine.lRcvrLineKey, kEntTypePORcvrLine
    End If

Exit Sub

ExpectedErrorRoutine:
MyErrMsg frmViewRcptOfGoods.moClass, Err.Description, Err, sMyName, "cmdCommentDtl_Click"
gClearSotaErr
Exit Sub


End Sub


Private Sub InitializeDetlGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************
' Desc: Initializes/formats the Detail grid.
'*******************************************
    Dim iGridType As Integer
    Dim sTitle As String

    '-- Set default grid properties
    gGridSetProperties grdRcptDetl, kGridMaxCols, iGetGridType()

    gGridSetProperties grdJobLine, kMaxColsJ, iGetGridType()

    '-- Set column headers
    sTitle = gsBuildString(ksLine, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColPOLineNo, sTitle
    
    sTitle = gsBuildString(kItem, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColMaskedItemID, sTitle
    
    sTitle = gsBuildString(kDescription, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColItemDesc, sTitle
    
    sTitle = gsBuildString(kAnUOM, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColUnitMeasRcvdID, sTitle
    
    sTitle = gsBuildString(ksQtyOrdered, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColQtyOrdered, sTitle
    
    sTitle = gsBuildString(ksQtyReceived, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColQtyRcvd, sTitle
    
    '-- Set column widths
    gGridSetColumnWidth grdRcptDetl, kColPOLineNo, 10
    gGridSetColumnWidth grdRcptDetl, kColMaskedItemID, 20
    gGridSetColumnWidth grdRcptDetl, kColItemDesc, 26
    gGridSetColumnWidth grdRcptDetl, kColUnitMeasRcvdID, 12
    gGridSetColumnWidth grdRcptDetl, kColQtyOrdered, 14
    gGridSetColumnWidth grdRcptDetl, kColQtyRcvd, 14
    
    '-- Hide the appropriate columns
    gGridHideColumn grdRcptDetl, kColPOLineKey
    gGridHideColumn grdRcptDetl, kColTrnsfrLineKey
    gGridHideColumn grdRcptDetl, kColItemKey
    gGridHideColumn grdRcptDetl, kColItemID
    gGridHideColumn grdRcptDetl, kColUnitMeasRcvdKey
    gGridHideColumn grdRcptDetl, kColQtyRcvdS
    gGridHideColumn grdRcptDetl, kColUnitCost
    gGridHideColumn grdRcptDetl, kColMatchStatus
    gGridHideColumn grdRcptDetl, kColDispMatchStatus
    gGridHideColumn grdRcptDetl, kColGLAcctKey
    gGridHideColumn grdRcptDetl, kColAcctRefKey
    gGridHideColumn grdRcptDetl, kColReturnType
    gGridHideColumn grdRcptDetl, kColDispReturnType
    gGridHideColumn grdRcptDetl, kColTranComment
    gGridHideColumn grdRcptDetl, kColSeqNo
    gGridHideColumn grdRcptDetl, kColReceiptLineKey
    gGridHideColumn grdRcptDetl, kColWhseID
    gGridHideColumn grdRcptDetl, kColDeptID
    gGridHideColumn grdRcptDetl, kColUnitMeasOrdKey
    gGridHideColumn grdRcptDetl, kColUnitMeasOrdID
    gGridHideColumn grdRcptDetl, kColTotalRcvd
    gGridHideColumn grdRcptDetl, kColQtyRtrned
    gGridHideColumn grdRcptDetl, kColQtyOpen
    gGridHideColumn grdRcptDetl, kColCloseSrcLine
    gGridHideColumn grdRcptDetl, kColItemVol
    gGridHideColumn grdRcptDetl, kColItemWght
    gGridHideColumn grdRcptDetl, kColInvtTranKey
    gGridHideColumn grdRcptDetl, kColDiscAmt
    gGridHideColumn grdRcptDetl, kColOtherAmt
    gGridHideColumn grdRcptDetl, kColSTaxAmt
    gGridHideColumn grdRcptDetl, kColMeasType
    gGridHideColumn grdRcptDetl, kColQtyInPOUOM
    gGridHideColumn grdRcptDetl, kColInvtTempTranKey
    gGridHideColumn grdRcptDetl, kColInvtTranDirty
    gGridHideColumn grdRcptDetl, kColFreightAmt
    gGridHideColumn grdRcptDetl, kColQtyShip
    gGridHideColumn grdRcptDetl, kColDefective
    gGridHideColumn grdRcptDetl, kColDamaged
    gGridHideColumn grdRcptDetl, kColCarrierDamage
    gGridHideColumn grdRcptDetl, kColImpLabel
    gGridHideColumn grdRcptDetl, kColDocMissing
    gGridHideColumn grdRcptDetl, kColImpPackage
    gGridHideColumn grdRcptDetl, kColNotToSpec
    gGridHideColumn grdRcptDetl, kColOtherIssue
    gGridHideColumn grdRcptDetl, kColPerfComments
    
    
    'If Not mbDefaultQty Then
        gGridHideColumn grdRcptDetl, kColQtyOrdered
    'End If
    
    
    '-- Set the column types
    gGridSetColumnType grdRcptDetl, kColItemDesc, SS_CELL_TYPE_EDIT, 40
    gGridSetColumnType grdRcptDetl, kColTranComment, SS_CELL_TYPE_EDIT, 255
    gGridSetColumnType grdRcptDetl, kColQtyOrdered, SS_CELL_TYPE_FLOAT, 3
    gGridSetColumnType grdRcptDetl, kColQtyRcvd, SS_CELL_TYPE_FLOAT, 3
    gGridSetColumnType grdRcptDetl, kColPerfComments, SS_CELL_TYPE_EDIT, 255

    
    '-- Don't allow the user to resize the line number column
    grdRcptDetl.Col = 0
    grdRcptDetl.UserResizeCol = SS_USER_RESIZE_OFF
    
    moProject.InitializeJobGrid
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeDetlGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function iGetGridType() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Desc: Return the grid type based on the run mode and security
'       level: Line Entry or No Append.
'****************************************************************
    iGetGridType = kGridLENoAppend
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetGridType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bGetOptions(sCompanyID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iBlindRcpt As Integer
    Dim sSQL As String
    Dim rs As Object
    Dim lWghtUOMKey As Long
    Dim lVolUOMKey As Long
    
    
    bGetOptions = False
    
    mbAllowInterCo = True
    
    iBlindRcpt = CInt(moOptions.PO("BlindRcpt"))
    mbDefaultQty = (iBlindRcpt < 1) 'ReceiptQtyDflt
    mbFilterByDate = (iBlindRcpt < 2) 'Filter the receipts by date

    mbAutoSelectLines = CBool(moOptions.PO("DfltRcptComplete")) 'Receive Complete
    
    miCaptureWght = CInt(moOptions.PO("RcvrItemWghtUpdate")) 'Capture Item Weight
    
    miCaptureVol = CInt(moOptions.PO("RcvrItemVolUpdate")) ' Capture Item Volume
    
    mbCloseSrcLine = CBool(moOptions.PO("AllowPOBackOrder")) 'Close PO Line on first receipt
    
    miRcptDaysEarly = CInt(moOptions.PO("DfltRcptEarlyDays")) ' Days early that a line can be accepted
    
    If CBool(moOptions.PO("IntegrateWithIM")) Then
        lVolUOMKey = CLng(moOptions.IM("VolumeUnitMeasKey")) ' Volume UOM Key
        lWghtUOMKey = CLng(moOptions.IM("WeightUnitMeasKey")) ' Weight UOM Key
    End If
    
    If lVolUOMKey = 0 Then
        msVolUOM = "None"
    Else
        sSQL = "SELECT UnitMeasID FROM tciUnitMeasure WHERE UnitMeasKey = " & lVolUOMKey
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            msVolUOM = rs.Field("UnitMeasID")
        Else
            msVolUOM = "None"
        End If
        If Not rs Is Nothing Then
            rs.Close: Set rs = Nothing
        End If
    End If
    
    If lWghtUOMKey = 0 Then
        msWghtUOM = "None"
    Else
        sSQL = "SELECT UnitMeasID FROM tciUnitMeasure WHERE UnitMeasKey = " & lWghtUOMKey
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            msWghtUOM = rs.Field("UnitMeasID")
        Else
            msWghtUOM = "None"
        End If
        If Not rs Is Nothing Then
            rs.Close: Set rs = Nothing
        End If
    End If
    
    '-- Default misc. module-level variables
    SetupModuleVars
    
    bGetOptions = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetOptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub cmdVendPerf_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdVendPerf, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
     
    'Invoke Vendor Performance for header in View Only mode
    frmVendPerfHdr.bEditVendPerfHdr moDmHeader, moClass, lkuReceiptNo.Text, txtPONum.Text, _
        txtVendID.Text, txtVendName.Text, True
         
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdVendPerf_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerfDtl_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdVendPerfDtl, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim sWhseOrDept As String

    sWhseOrDept = gsGetValidStr(txtWhse.Text)
    If Len(sWhseOrDept) = 0 Then
        sWhseOrDept = gsGetValidStr(txtDept.Text)
    End If
     
    'Invoke Vendor Performance for lines in view only mode
    frmVendPerfDtl.bEditVendPerfDtl muVendPerfDtl, moClass, sWhseOrDept, lkuReceiptNo.Text, _
        txtPONum.Text, txtPOLine.Text, txtItemOrdID.Text, txtItemDescription.Text, True
                    
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdVendPerfDtl_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Description:
'    Form_KeyDown traps the hot keys when the KeyPreview property
'    of the form is set to True.
'****************************************************************
    Select Case KeyCode
        Case 189 'vbDash
            If Shift = 2 Then    'vbKeyControl
                If Me.ActiveControl Is txtPONum Then
                    KeyCode = 0
                    If Len(Trim(txtPONum.Text)) > 0 Then
                        txtPONum.Text = sZeroFillNumber(txtPONum.Text, txtPONum.MaxLength)
                        SetCtrlFocus txtRelNo
                    End If
                End If
            End If
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
         
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetTranType()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine sets the transaction type variables.
'************************************************************************
    

Dim sSQL As String

'Set the tran type - MVB this will need to handle both
    mlTranType = kvTranRcpt
    sSQL = "CompanyID = " & gsQuoted(msCompanyID) & " AND TranType = " & mlTranType
        
    msTranType = Trim(gsGetValidStr(moClass.moAppDB.Lookup("TranTypeID", _
                                    "tciTranTypeCompany", sSQL)))

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description:
'    Form_KeyPress traps the keys pressed when the KeyPreview property
'    of the form is set to True.
'*********************************************************************
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
    
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupIMInterfaces()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Setup the IMS Class
    moIMSClass.Init moClass.moAppDB, moClass.moAppDB, msCompanyID
    Set moItem = moIMSClass.Items.Item
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupIMInterfaces", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbLoadSuccess = False
    
    '-- assign system object properties to module-level variables
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        ' *** RMM 5/2/01, Context menu project
        mbIMActivated = .IsModuleActivated(kModuleIM)
    End With
    Set moAppDB = moClass.moAppDB
    
    msPurchCompanyID = msCompanyID
    
    '-- Set up the form resizing variables
    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With
    
    SetTranType
    
    '-- Get the Receipt No. label, minus any accelerator
    msRcvrNoLbl = gsStripChar(lblRcptNo, kAmpersand)
    
    '-- Setup necessary properties for Options class
    With moOptions
        Set .oSysSession = moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    
    '-- Retrieve necessary options and set module-level variables
    If Not bGetOptions(msCompanyID) Then Exit Sub
    
    SetupIMInterfaces
    
    '-- Setup the toolbar and status bar
    SetupBars
    
    '-- Setup the necessary lookup properties
    SetupLookups
    
    lkuProject.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    
    '-- Bind form fields to database fields
    BindForm
    
    '-- Set the security level for this user
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmHeader, moDmDetl, moDmLineDist)
            
    Set moProject = New clsProject
    moProject.Init JOB_PORCPT, moClass, Me, moSotaObjects, moDmDetl, grdRcptDetl, _
                   moLE, grdJobLine, lkuProject, lkuPhase, lkuTask, , _
                     , , , , , kiProjectTab
    
    '-- Bind the Line Entry class
    BindLE
    
    '-- Initialize the detail grid
    InitializeDetlGrid
    
    '-- Bind the Context Menu
    BindContextMenu
    
    'MVB could use this for toggling between EROG and Returns
    '-- Setup PO-related fields, options, etc.
'    If Not bInitForPO(Me, mlVRunMode, moOptions) Then Exit Sub
    
    '-- Set the initial states/values of fields based on run mode
    SetFieldStates
    
    '-- Set the initial toolbar button states
    SetTBButtonStates
    
    '-- Make sure the header tab is shown to the user first
    tabReceipt.Tab = kiHeaderTab
    
    tabReceipt_Click kiHeaderTab
    
    '-- Grid starts out with weird stuff in it; fix that problem right now!
    grdRcptDetl.MaxRows = 0
    grdRcptDetl.MaxRows = 1
    
'   Change the caption for the form based on the string.
    Me.Caption = msFormCaption
    
'    nbrQtyOrd.Border = BORDERSTYLE_3D

    If Not bSetupReceiver Then Exit Sub
    
    '-- Initialize the Type Combo
    With ddnType
        .AddItem "Purchase"
        .AddItem "Transfer"
        .Tag = .List(0)
        .ListIndex = 0
    End With
        
    '-- We made it through Form_Load successfully
    mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
    Dim bValid As Boolean
   
    '-- reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
        '-- if the form is dirty, cancel the form close
        '-- if the form is dirty, prompt the user to save the record
        bValid = bConfirmUnload(iConfirmUnload)

        
        Select Case iConfirmUnload
            Case kDmSuccess
                'Do Nothing
            Case kDmFailure
                GoTo CancelShutDown
                
            Case kDmError
                GoTo CancelShutDown
                
            Case Else
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
                
        End Select
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlVRunMode
                    Case kViewEditReceiptDDN
                        '-- Hide the main form and cancel the shutdown
                        moClass.moFramework.SavePosSelf
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
End Sub


Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
        '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
                        grdRcptDetl, tabReceipt
'                        pnlTab(kiHeaderTab), pnlTab(kiDetailTab), _

        '-- Move the controls to the right
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
                        grdRcptDetl, tabReceipt
'                        pnlTab(kiHeaderTab), pnlTab(kiDetailTab), _

    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    lkuProject.Terminate
    lkuPhase.Terminate
    lkuTask.Terminate
    
    If Not moProject Is Nothing Then
        moProject.UnloadSelf
        Set moProject = Nothing
    End If

    '-- Fire Terminate event for lookups
    lkuReceiptNo.Terminate
    moIMSClass.Terminate
    Set moIMSClass = Nothing
    
    Set moItem = Nothing
    
    '-- Clean up object references
    If Not moDmHeader Is Nothing Then
        moDmHeader.UnloadSelf
        Set moDmHeader = Nothing
    End If
    
    If Not moDmDetl Is Nothing Then
        moDmDetl.UnloadSelf
        Set moDmDetl = Nothing
    End If
    
    If Not moDmLineDist Is Nothing Then
        moDmLineDist.UnloadSelf
        Set moDmLineDist = Nothing
    End If
    
    
    '-- Clean up Line Entry references
    If Not moLE Is Nothing Then
        moLE.UnloadSelf
        Set moLE = Nothing
    End If
    
    If Not moPOEnterLandedCost Is Nothing Then
        Set moPOEnterLandedCost = Nothing
    End If
    
    '-- Clean up other objects
    Set moOptions = Nothing
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing
    Set moAppDB = Nothing
    Set moSysSession = Nothing
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
        
Private Sub UndoNewBlankRow()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Implicitly undo a new blank row in the grid on certain user actions
    '-- (tab change, save...)
    If (moLE.ActiveRow > grdRcptDetl.DataRowCnt) And _
       (Len(Trim$(txtItemOrdID)) = 0) Then
        moLE.GridEditUndo
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UndoNewBlankRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub grdRcptDetl_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'           Select the row that the user Selected.
'   Parameters:
'           Row, Col - The cell coordinates that were clicked.
'***********************************************************************
    UndoNewBlankRow
    
    If (moLE.GridEditDone(Col, Row) <> kLeSuccess) Then
        Exit Sub
    End If

    '-- Select the row that the user Selected
    moLE.Grid_Click Col, Row

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_DblClick(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Initiate edit mode where the row contents can be changed.
'           Assumes a Click has preceeded this event.
'   Parameters:
'           Row, Col - The cell coordinates that were clicked.
'************************************************************************
    moLE.Grid_DblClick Col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           A drag-and-drop terminated here, check if the drop should
'           result in an actual movement of a dropped row.
'   Parameters:
'           mlSourceRow - the row from where the drag was initiated.
'           source, x - not used.
'           y - the drop Position.  See VB Manual.
'************************************************************************
    moLE.Grid_DragDrop x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_DragOver(Source As Control, x As Single, y As Single, State As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Indicate whether drop is allowed or disallowed at this point.
'   Parameters:
'           source, etc - See VB Manual.
'************************************************************************
    moLE.Grid_DragOver x, y, State
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_DragOver", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Fires when the Grid gets focus.
'************************************************************************
    moLE.Grid_GotFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub grdRcptDetl_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Provide keyboard interface.
'   Parameters:
'           KeyCode, etc - See VB Manual.
'************************************************************************
    moLE.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Determine if focus is lost to a edit control.  If so,
'           begin the edit or add mode process.
'************************************************************************
    moLE.Grid_LostFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Mark the x,y Position in preparation for drag-and-drop.
'   Parameters:
'           Button, etc - See VB Manual.
'************************************************************************
    moLE.Grid_MouseDown Button, Shift, x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Check if mouse movement qualifies to be a drap-and-drop.
'   Parameters:
'           Button, etc - See VB Manual.
'************************************************************************
    moLE.Grid_MouseMove Button, Shift, x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LEClearDetailControls(oLE As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Description: Clear detail fields on the Line Entry object.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
    ClearDetlFields True
    
    moProject.LEClearDetailControls
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEClearDetailControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LEGridToDetail(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: If any grid columns need to be manually linked to the detail
'       entry controls, then do it here.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
    Dim lRow As Long
    Dim lChildRow As Long
    Dim dUnitCost As Double
    Dim lKey As Long
    Dim sCode As String
    Dim iMatchStatus As Integer

    lRow = oLE.ActiveRow
    lChildRow = 1
    '-- Set the PO Line Num
        '-- Set up the Item Info structure
    With muItemInfo
        .iMeasType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColMeasType))
        .lReceiverLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey))
    End With

    With muRcvrLine
        .lRcvrLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey))
        .lDistTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
        
        '-- Set the PO Line Num
        txtPOLine = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColPOLineNo))

        '-- Set the Item ID
        txtItemOrdID = gsGetValidStr(gsGridReadCellText(grdRcptDetl, lRow, kColMaskedItemID))
        
        '-- Set the Item Description
        txtItemDescription = gsGetValidStr(gsGridReadCellText(grdRcptDetl, lRow, kColItemDesc))
        
        '-- Set the Quantity Received
        mbManualClick = True
        nbrQtyRcvd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd))
        mbManualClick = False
        nbrQtyRcvd.Tag = nbrQtyRcvd.Value
        mdQtyInPOUOM = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyInPOUOM))

        mlItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
        
        '-- Set the Receipt UOM
        txtRcvUOM = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdID))

        '-- Set the Warehouse
        txtWhse = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColWhseID))
        
        '-- Set the Department
        txtDept = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColDeptID))
        
        '-- Original Order tab
        nbrQtyOrd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyOrdered))

        txtUOM = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasOrdID))
        mlPurchUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasOrdKey))

        nbrTotRcvd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColTotalRcvd))
        nbrQtyReturn = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRtrned))
        nbrQtyOpen = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyOpen))
        nbrQtyShip = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyShip))

        '--Comments tab
        txtLnComment = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColTranComment))
    
        '-- Misc tab
        mbManualClick = True
        iMatchStatus = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColMatchStatus))
        If iMatchStatus < 4 Then
            chkMatch = 1
        Else
            chkMatch = 0
        End If
        chkCloseDistr = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColCloseSrcLine))
        mbManualClick = False
    End With
    
    With muVendPerfDtl
        .iDamaged = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColDamaged))
        .iDefective = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColDefective))
        .iCarrierDamage = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColCarrierDamage))
        .iImproperLabel = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColImpLabel))
        .iDocMissing = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColDocMissing))
        .iImproperPackage = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColImpPackage))
        .iNotToSpec = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColNotToSpec))
        .iOtherIssue = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColOtherIssue))
        .sPerfComments = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPerfComments))
    End With
    
    Dim sIDJ As String
    Dim lKeyJ As Long
    Dim lPOLineKey As Long
    
    lPOLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey))
    
    moProject.SetJobGridRow lPOLineKey
    
    sIDJ = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColProjectID))
    lKeyJ = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColProjectKey))
    lkuProject.SetTextAndKeyValue sIDJ, lKeyJ
    
    sIDJ = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPhaseID))
    lKeyJ = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColPhaseKey))
    lkuPhase.SetTextAndKeyValue sIDJ, lKeyJ
    
    sIDJ = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColTaskID))
    lKeyJ = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTaskKey))
    lkuTask.SetTextAndKeyValue sIDJ, lKeyJ
    
    lkuTask.RestrictClause = moProject.sGetTaskRestrict()
    
    '-- Set the detail controls as valid so the old values are correct
    SOTAValidationMgr1.Reset

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEGridToDetail", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function LEGridBeforeDelete(oLE As clsLineEntry, lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'       called from line entry class when a row has been successfully
'       deleted from the grid
'   Param:
'       oLE -   line entry class making the call
'       lRow -  grid row deleted
'************************************************************************
    
    LEGridBeforeDelete = False
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEGridBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub moDmDetl_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Description:
'      Fires when each row of data is loaded by the DM into the grid.
'      This is a manual movement of a copy of the data from the DM to the correct
'      grid cell on the same row.
'************************************************************************************
    Dim sCompanyID As String
    Dim sGLAcctNo As String
    Dim sItemID As String
    Dim dAmt As Double
    Dim iReturnType As Integer
    Dim iMatchStatus As Integer
    
    '-- Set the masked Item ID column
    sItemID = gsGridReadCell(grdRcptDetl, lRow, kColItemID)
    gGridUpdateCell grdRcptDetl, lRow, kColMaskedItemID, sItemID
    
    '-- Load the associated PA link record into the link grid
    moProject.GridRowLoaded lRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvrKey As Long
    Dim lKey As Long
    Dim bNegativeDoc As Boolean
    Dim sSQL As String
    Dim rs As Object
    
    If oChild Is moDmHeader Then
    
        lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        
        '-- Create the temporary join to display PO info in the grid
        CreatePONoLinkForGrid moAppDB, msCompanyID, lRcvrKey, mlVRunMode
        
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lKeyValue As Long
    
    If oChild Is moDmHeader Then
        If (moDmHeader.State = kDmStateAdd) Then
            ProcessKeyNotFound
        Else
            ProcessKeyFound
        End If
    
        lKeyValue = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        '-- Set the Memo toolbar to the correct state
        gSetMemoToolBarState moClass.moAppDB, lKeyValue, kEntTypePOReceipt, msCompanyID, tbrMain
        
        moProject.ResetJobGrid

    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    '-- Set toolbar button states
    SetTBButtonStates
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick sButton
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SOTAValidationMgr1_KeyChange()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
        
    mbFromCode = True
    
    '-- Only allow KeyChange once per record;
    '-- weird behavior happening in compiled version vis a vis Next Number logic.
    '-- mbAllowKeyChange tells us if the KeyChange has already occurred
    If mbAllowKeyChange Then
        bValid = bIsValidRcptNo()
        If bValid Then mbAllowKeyChange = False
    End If

    mbFromCode = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SOTAValidationMgr1_KeyChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Function bIsValidRcptNo() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRet As Integer
    
            bIsValidRcptNo = False
    mbValidKeyChange = True
    
    mbLoadingRec = True
    
    '-- Set the Company ID in DM
    moDmHeader.SetColumnValue "CompanyID", msCompanyID
    '-- Set the Tran Type in DM
    moDmHeader.SetColumnValue "TranType", mlTranType

    '-- Zero-fill the Receipt Number if appropriate
    lkuReceiptNo = sZeroFillNumber(lkuReceiptNo.MaskedText, kvRcptTranSize)
    
    '-- Perform some checks before KeyChange
    If bProcessPreKeyChange() Then
        iRet = moDmHeader.KeyChange()
        
        If (iRet = kDmNotAllowed) Then
            ProcessKeyNotAllowed
        End If
    End If

    If mbValidKeyChange Then
        ProcessKeyFoundDetl
        txtRelNo.Text = Mid(txtRelNo.Text, 12, 4)

        ' -- Prevent an extra line from being added on the end.
'        grdRcptDetl.MaxRows = grdRcptDetl.MaxRows - 1
        '-- Determine whether to enable the Lines... button for PO
        '-- Set focus to the correct field
        SetKeyChangeFocus iRet
                
        If (moDmHeader.State = kDmStateEdit) Then
            '-- Set the data manager object and all children to clean
            '-- (the header object was becoming dirty somewhere in the display process)
            moDmHeader.SetDirty False, True
        End If
        
        lkuReceiptNo.Protected = True
        
        bIsValidRcptNo = True
        ddnType.Enabled = False
    Else
        '-- Clear the form
        ProcessCancel
    End If
    
    mbLoadingRec = False
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidRcptNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function SetKeyChangeFocus(iKCState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If (iKCState = kDmKeyNotFound) Then
        '-- Set focus to the next appropriate control for a new record
        If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
            SetCtrlFocus txtPONum
        Else
            SetCtrlFocus txtPONum
        End If
    ElseIf (iKCState = kDmKeyFound) Then
        '-- Set focus to the next appropriate control for an existing record
        If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
            SetCtrlFocus txtPONum
        Else
            SetCtrlFocus txtBOL
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetKeyChangeFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bProcessPreKeyChange() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Perform processing before a DM KeyChange is fired; verify
'       that the Recepit number entered is in the correct batch.
'******************************************************************
    Dim lReceiptBatchKey As Long
    Dim lRcvrKey As Long
    Dim sSQL As String
    Dim sBatchNo As String
    Dim sTranID As String
    Dim bValid As Boolean
    Dim rs As DASRecordSet
    
    bProcessPreKeyChange = False
    
    bValid = True
        
    sSQL = "SELECT RcvrKey, TranID FROM tpoReceiver "
    sSQL = sSQL & "WHERE CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & " AND TranNo = " & gsQuoted(lkuReceiptNo)
    sSQL = sSQL & " AND TranType = " & mlTranType
    
        
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
    If Not rs.IsEOF Then
        lRcvrKey = glGetValidLong(rs.Field("RcvrKey"))
        sTranID = gsGetValidStr(rs.Field("TranID"))
            
'            If (lRcvrKey <> 0) And (sTranID <> "") Then
'                '-- Verify that the receipt hasn't been purged (ICS #6327)
'                If Not bCheckReceiverLogStatus(lkuReceiptNo, lRcvrKey, sTranID) Then
'                    bValid = False
'                End If
'            End If
    Else
            '-- Can't enter a new receipt in this task; show error message
    
        giSotaMsgBox Me, moSysSession, kmsgCannotAddNewVoucher, _
            msRcvrNoLbl, msRcvrNoLbl
            
        bValid = False
    End If
        
    rs.Close: Set rs = Nothing
            
    If Not bValid Then
        mbValidKeyChange = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
 
    
    bProcessPreKeyChange = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessPreKeyChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub ProcessKeyFound()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Processing for when an existing receipt number is entered.
'******************************************************************
    Dim lRcvrKey As Long
    Dim lPurchAddrKey As Long
    Dim sTranID As String
    Dim bValid As Boolean
    Dim rs As Object
    Dim sSQL As String
    
    With moDmHeader
        '-- Check the Receiver Log Status to make sure it is OK
        lRcvrKey = glGetValidLong(.GetColumnValue("RcvrKey"))
        sTranID = gsGetValidStr(.GetColumnValue("TranID"))
        
'        If Not bCheckReceiverLogStatus(lkuReceiptNo, lRcvrKey, sTranID) Then
'            mbValidKeyChange = False
''+++ VB/Rig Begin Pop +++
''+++ VB/Rig End +++
'            Exit Sub
'        End If
        
        ClearFields
        
        '-- Setup the Vendor defaults structure
        muVendDflts.lVendKey = glGetValidLong(.GetColumnValue("VendKey"))
        
        If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
            '-- Set the module-level key variable for the PO Number
            mlOrigPOKey = glGetValidLong(.GetColumnValue("POKey"))
        End If
    End With
    
    If mlTranType = kTranTypePOTR Then
        Dim lTrnsfrOrderKey As Long
        Dim lShipWhseKey As Long
        Dim lTransitWhseKey As Long
        lTrnsfrOrderKey = moDmHeader.GetColumnValue("TrnsfrOrderKey")
        lShipWhseKey = glGetValidLong(moClass.moAppDB.Lookup("ShipWhseKey", "timTrnsfrOrder", "TrnsfrOrderKey = " & lTrnsfrOrderKey))
        txtShipWhse = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & lShipWhseKey)
        txtShipWhseDesc = moClass.moAppDB.Lookup("Description", "timWarehouse", "WhseKey = " & lShipWhseKey)
        lTransitWhseKey = glGetValidLong(moClass.moAppDB.Lookup("TransitWhseKey", "timTrnsfrOrder", "TrnsfrOrderKey = " & lTrnsfrOrderKey))
        txtTransit = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & lTransitWhseKey)
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyFound", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessKeyFoundDetl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Processing for when existing lines are loaded.
'******************************************************************
    '-- The Line Entry object must be updated after a key change
    moLE.InitDataLoaded
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyFoundDetl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessKeyNotFound()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Processing for when an receipt number that does not exist
'       in the database is entered.
'******************************************************************
        '-- Set the Batch Key now
        moDmHeader.SetColumnValue "BatchKey", mlBatchkey
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyNotFound", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessKeyNotAllowed()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Data Manager operations are not allowed due to security constraints
    lkuReceiptNo = ""
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyNotAllowed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetTBButtonStates()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    SetNextNumberState
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetTBButtonStates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetNextNumberState()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Set the enabled state of the Next Number toolbar button
    If (mlVRunMode = kEnterReceiptDTE) Or _
       (mlVRunMode = kErogDTE) Then
        If (miSecurityLevel > kSecLevelDisplayOnly) Then
            tbrMain.ButtonEnabled(kTbNextNumber) = True
        Else
            tbrMain.ButtonEnabled(kTbNextNumber) = False
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetNextNumberState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bCheckReceiverLogStatus(sRcptNo As String, lRcvrKey As Long, _
                                        sVouchTranID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sStatus As String
    Dim sTranStatus As String
    Dim iTranStatus As Integer
    Dim sInv As String
    
    bCheckReceiverLogStatus = False

    '-- Get the receiver Log Status
    iTranStatus = giGetValidInt(moAppDB.Lookup("TranStatus", "tpoReceiverLog", _
                      "CompanyID = " & gsQuoted(msCompanyID) & _
                      " AND TranNo = " & gsQuoted(sRcptNo)) & _
                      " AND TranType = " & mlTranType)
    
    If (iTranStatus = 0) Then
        '-- Error accessing the Receipt Log record
        giSotaMsgBox Me, moSysSession, kmsgVoucherLogAccessError, _
            msRcvrNoLbl, msRcvrNoLbl
        
        '-- Clear the form
        ProcessCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If (iTranStatus = kvIncompleteReceiverLog) Then
        '-- Update the Log record for the current receipt to Pending
        If Not bUpdateReceiverLog(lRcvrKey, sVouchTranID, kvPendingReceiverLog) Then
            '-- Clear the form
            ProcessCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    sTranStatus = ""
    
    If (iTranStatus = kvPostedReceiverLog) Then
        If (mlVRunMode <> kViewEditReceiptDTE) And (mlVRunMode <> kViewEditReceiptDDN) Then
            sTranStatus = gsBuildString(kTranPosted, moAppDB, moSysSession)
        End If
    ElseIf (iTranStatus = kvPurgedReceiverLog) Then
        sTranStatus = gsBuildString(kTranPurged, moAppDB, moSysSession)
    ElseIf (iTranStatus = kvVoidReceiverLog) Then
        sTranStatus = gsBuildString(kTranVoid, moAppDB, moSysSession)
    End If
    
    If (Len(Trim$(sTranStatus)) > 0) Then
        '-- Log record found in an error-causing state
        sStatus = gsBuildString(kStatus, moAppDB, moSysSession)
                    
        sInv = gsBuildString(kReceiptNo, moAppDB, moSysSession)

        giSotaMsgBox Me, moSysSession, kmsgVoucherLogStatusInvalid, _
            msRcvrNoLbl, msRcvrNoLbl, sRcptNo, sInv, sRcptNo, _
            sStatus, sTranStatus
        
        '-- Clear the form
        ProcessCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    bCheckReceiverLogStatus = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCheckReceiverLogStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bRcvrUsed(sRcptNo As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
'   Description:
'           Determine if receipt number has been used before (in tpoReceiverLog).
'   Parameters:
'           sRcptNo <in>:  The Receipt No. we are working with.
'   Returns:
'           True if Receipt No. is found in tpoReceiverLog, else False.
'*******************************************************************************
    Dim sSQL As String
    Dim rs As Object
    Dim sTranNo As String
    Dim iTranStatus As Integer
    Dim sStatus As String
    Dim sTranStatus As String
    Dim sInv As String

    bRcvrUsed = False

    '-- Check tpoReceiverLog for Receiver No.
    sSQL = "SELECT TranNo, TranStatus FROM tpoReceiverLog"
    sSQL = sSQL & " WHERE CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & " AND TranNo = " & gsQuoted(sRcptNo)
    sSQL = sSQL & " AND TranType = " & mlTranType
    
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        '-- Get info about the receiver for display in error message
        sTranNo = gsGetValidStr(rs.Field("TranNo"))
        iTranStatus = giGetValidInt(rs.Field("TranStatus"))
        
        bRcvrUsed = True
    End If

    rs.Close: Set rs = Nothing

    '-- Display an appropriate error message
    If bRcvrUsed Then
        sStatus = gsBuildString(kStatus, moAppDB, moSysSession)

        Select Case iTranStatus
            Case kvIncompleteReceiverLog
                sTranStatus = gsBuildString(kTranIncomplete, moAppDB, moSysSession)

            Case kvPendingReceiverLog
                sTranStatus = gsBuildString(kTranPending, moAppDB, moSysSession)

            Case kvPostedReceiverLog
                sTranStatus = gsBuildString(kTranPosted, moAppDB, moSysSession)

            Case kvPurgedReceiverLog
                sTranStatus = gsBuildString(kTranPurged, moAppDB, moSysSession)

            Case Else
                sTranStatus = gsBuildString(kUnknown, moAppDB, moSysSession)
        
        End Select

        sInv = gsBuildString(kReceiptNo, moAppDB, moSysSession)
        
        giSotaMsgBox Me, moSysSession, kmsgVoucherNoUsedBefore, _
            msRcvrNoLbl, msRcvrNoLbl, lkuReceiptNo, sInv, _
            sTranNo, sStatus, sTranStatus, msRcvrNoLbl
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRcvrUsed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bUpdateReceiverLog(lRcvrKey As Long, iRcptTranID As String, _
                                   Optional iSetToStatus As Integer = 0) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Desc: Update the Receiver Log table with info about the new receipt.
'*********************************************************************
    Dim sSQL As String
    Dim rs As Object
    Dim sRcptDate As String
    Dim iTranStatus As Integer
    Dim iMode As Integer
    Dim iRcptStatus As Integer
    Dim bRetCode As Boolean

    bUpdateReceiverLog = False
    
    '-- Verify that we have a Receiver Key
    If (lRcvrKey > 0) Then
        '-- If there is an Receipt Date, convert it to SQL format
        If (Len(Trim$(calRcptDate)) > 0) Then
            sRcptDate = calRcptDate.SQLDate
        Else
            sRcptDate = ""
        End If
            
        iTranStatus = giGetValidInt(moAppDB.Lookup("TranStatus", "tpoReceiverLog", _
            "RcvrKey = " & lRcvrKey))
                
        If (iTranStatus > 0) Then
            '-- An existing record was found
            iMode = kiUpdateMode
        Else
            iMode = kiInsertMode
        End If
    
        '-- Check to make sure we have all the necessary components
        If Len(Trim$(lkuReceiptNo)) > 0 Then
            '-- Set the Receiver Log status appropriately
            If (iSetToStatus <> 0) Then
                iRcptStatus = iSetToStatus
            ElseIf (iMode = kiInsertMode) Then
                iRcptStatus = kvIncompleteReceiverLog
            ElseIf (iMode = kiUpdateMode) Then
                iRcptStatus = iTranStatus
            End If
    
    
            If bRetCode Then
                '-- Receiver Log insert/update was successful
                bUpdateReceiverLog = True
            End If
        End If
    End If
    
    If Not bUpdateReceiverLog Then
        '-- Display error message
        moDmHeader.CancelAction
        giSotaMsgBox Me, moSysSession, kmsgVoucherLogError, msRcvrNoLbl
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateReceiverLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SOTAValidationMgr1_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbFromCode = True
    
    iReturn = SOTA_INVALID
    Debug.Print "VM - Validating Data"
    
    iReturn = SOTA_VALID

    mbFromCode = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SOTAValidationMgr1_Validate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ClearDetlFields(Optional bClearAll As Boolean = False, _
                            Optional bDetlPop As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Clear out detail field values based on Item
    mbManualClick = True
    nbrQtyRcvd = 0
    mbManualClick = False
    txtRcvUOM = ""
    txtLnComment = ""
    mbManualClick = True
    chkMatch = 1
    chkCloseDistr = 0
    nbrQtyOrd.Value = 0
    txtUOM.Text = ""
    nbrTotRcvd.Value = 0
    nbrQtyReturn.Value = 0
    nbrQtyOpen.Value = 0
    txtItemOrdID.Text = ""
    txtPOLine.Text = ""
    txtWhse.Text = ""
    txtDept.Text = ""
    mbManualClick = False
    '-- Set the detail controls as valid so the old values are correct
    SOTAValidationMgr1.Reset
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearDetlFields", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tabReceipt_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
    Static bInTabChange As Boolean
    
    If bInTabChange Then Exit Sub
    
    bInTabChange = True
    
    '-- Do nothing if the tab is the same as last time
    If (tabReceipt.Tab = PreviousTab) Then
        bInTabChange = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
        
    bValid = True
    
    If (PreviousTab = kiDetailTab) And (tabReceipt.Tab <> PreviousTab) Then
        '-- Undo this line if the user hasn't done "anything" to it
        UndoNewBlankRow
    
        bValid = moLE.TabChange(tabReceipt.Tab, PreviousTab)
    End If
    
      
    bInTabChange = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabReceipt_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuReceiptNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuReceiptNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Not bCancel Then
        ProcessCancel
        mbFromNav = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_BeforeLookupReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++
    If mbFromNav Then
        '-- Fire the KeyChange event now
        mbAllowKeyChange = True
        SOTAValidationMgr1_KeyChange
    End If

    mbFromNav = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseDistr_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkCloseDistr, True
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long
    Dim sPOLineKey As String
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    
    '-- Security event is needed to change the Close PO Lines flag
    If Not mbManualClick Then
        sID = "RCVCLSDIST"
        sUser = CStr(moSysSession.UserId)
        vPrompt = True
        If (moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0) Then
            mbManualClick = True
            If chkCloseDistr = 1 Then
                chkCloseDistr = 0
            Else
                chkCloseDistr = 1
            End If
            mbManualClick = False
            Exit Sub
        End If
    End If
'    '-- Enable Item and Description fields if not matching this item
'    lRow = moLE.ActiveRow
'    sPOLineKey = Trim$(gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey)))
    If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
        moLE.GridEditChange chkMatch
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkCloseDistr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub chkMatch_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkMatch, True
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long
    Dim sPOLineKey As String
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    
    '-- Security event is needed to override matching
    If (chkMatch = vbUnchecked) And Not mbManualClick Then
        sID = "MTCHOVRDRC"
        sUser = CStr(moSysSession.UserId)
        vPrompt = True
        If (moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0) Then
            mbManualClick = True
            chkMatch = vbChecked
            mbManualClick = False
            Exit Sub
        End If
    End If

    '-- Enable Item and Description fields if not matching this item
'    lRow = moLE.ActiveRow
'    sPOLineKey = Trim$(gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey)))
    If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
        moLE.GridEditChange chkMatch
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkMatch_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkMatch_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Turn on the detail command buttons
    moLE.GridEditChange chkMatch
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkMatch_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmHeader
                moFormCust.ApplyFormCust
        End If
    End If
#End If

    Dim i As Integer
    
    '-- Setup the Sage MAS 500 form (validation) manager control
    With SOTAValidationMgr1
        Set .Framework = moClass.moFramework
        .Keys.Add ddnType
        .Keys.Add lkuReceiptNo
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub cmdPOLandedCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPOLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPOLandedCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPOLandedCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPOLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPOLandedCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerf_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdVendPerf, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerf_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerf_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdVendPerf, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerf_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerfDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdVendPerfDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerfDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerfDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdVendPerfDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerfDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOK_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdOK, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOK_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOK_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdOK, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOK_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOK_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdOK, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOK_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdUndo_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdUndo, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdUndo_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdUndo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdUndo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdUndo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdUndo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdUndo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdUndo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCommentDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCommentDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCommentDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCommentDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdAdditionalNotes1_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdAdditionalNotes1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdAdditionalNotes1_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdAdditionalNotes1_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdAdditionalNotes1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdAdditionalNotes1_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtPurchCompany_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPurchCompany, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPurchCompany_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPurchCompany_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPurchCompany, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPurchCompany_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPurchCompany_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPurchCompany, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPurchCompany_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPurchCompany_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPurchCompany, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPurchCompany_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtVendID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtVendID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtVendID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtVendID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRelNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRelNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRelNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRelNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPONum, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemDescription, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPOLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPOLine, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPOLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPOLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLnComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLnComment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLnComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLnComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDept, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDept, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDept, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDept, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemOrdID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemOrdID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemOrdID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemOrdID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRcvUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRcvUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRcvUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRcvUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRcvUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRcvUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRcvUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRcvUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRcvUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRcvUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRcvUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRcvUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBOL, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBOL, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBOL, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBOL, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtTranComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtTranComment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtTranComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtTranComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtClassOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtClassOvrdSegValue, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtClassOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtClassOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtVendName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtVendName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtVendName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtVendName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhse_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipWhseDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtTransit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtTransit, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtTransit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtTransit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransfer_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtTransfer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransfer_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransfer_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtTransfer, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransfer_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransfer_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtTransfer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransfer_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransfer_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtTransfer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransfer_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuTask, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuTask, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuTask, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPhase, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPhase, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPhase, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuProject, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuReceiptNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub nbrQtyRcvd_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyRcvd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyRcvd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyRcvd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyRcvd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyOrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyOrd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyOrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyOrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrTotRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrTotRcvd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrTotRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrTotRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyReturn, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyOpen, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyOpen, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyOpen, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyOpen, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyShip, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curInvAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curInvAmtS, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curInvAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curInvAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curPurchAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curPurchAmtS, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curPurchAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curPurchAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkMatch_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkMatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkMatch_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkMatch_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkMatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkMatch_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseDistr_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCloseDistr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseDistr_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseDistr_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCloseDistr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseDistr_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calRcptDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calRcptDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calRcptDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calRcptDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Private Function bLoseFocus() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    bLoseFocus = True
    
    If Me.ActiveControl Is sbrMain Or Me.ActiveControl Is tbrMain Then
        bLoseFocus = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoseFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function










Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property









'*****************************************************************************************
'   New Stuff added for Transfer Receipts
'*****************************************************************************************

Private Function ReceiptType() As enumReceiptType
'+++ VB/Rig Skip +++
    ReceiptType = ddnType.ListIndex
End Function

Private Sub SetUpReceiptTypeFields()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim bIsPurchaseReceipt As Boolean
    
    If ReceiptType = kReceiptTypePurchase Then
        Debug.Print "Purchase Receipt"
        mlTranType = TransactionTypes.kTranTypePORG
        bIsPurchaseReceipt = True
        chkCloseDistr.Caption = "Close PO Line for Receiving"
        lblPOLine.Caption = "PO Line"
        lkuReceiptNo.LookupID = "Receiver"
        SetRcptTransferFieldsVisible False
        SetRcptPurchaseFieldsVisible True
        txtShipWhse = ""
        txtShipWhseDesc = ""
        txtTransfer = ""
        txtTransit = ""
        txtShipWhse.Tag = ""
        txtShipWhseDesc.Tag = ""
        txtTransfer.Tag = ""
        txtTransit.Tag = ""
        cmdVendPerf.Enabled = True
        cmdVendPerfDtl.Enabled = True
    Else
        Debug.Print "Transfer Receipt"
        mlTranType = TransactionTypes.kTranTypePOTR
        bIsPurchaseReceipt = False
        chkCloseDistr.Caption = "Close Transfer Line for Receiving"
        lblPOLine.Caption = "Transfer Line"
        lkuReceiptNo.LookupID = "ReceiverTransferOrder"
        SetRcptPurchaseFieldsVisible False
        SetRcptTransferFieldsVisible True
        txtVendID = ""
        txtVendName = ""
        txtPONum = ""
        txtRelNo = ""
        txtVendID.Tag = ""
        txtVendName.Tag = ""
        txtPONum.Tag = ""
        txtRelNo.Tag = ""
        cmdVendPerf.Enabled = False
        cmdVendPerfDtl.Enabled = False
    End If
            
    'Get the Tran Type ID from common information
    sSQL = "CompanyID = " & gsQuoted(msCompanyID) & " AND TranType = " & mlTranType
    msTranType = Trim(gsGetValidStr(moClass.moAppDB.Lookup("TranTypeID", _
                                    "tciTranTypeCompany", sSQL)))
    
    'Reinitialize the ReceiptNo Restrict Clause using the new value of mlTranType
    Me.lkuReceiptNo.RestrictClause = sGetReceiptNoRestrict()
    Debug.Print Me.lkuReceiptNo.RestrictClause
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "SetUpReceiptTypeFields", VBRIG_IS_FORM   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub SetRcptTransferFieldsVisible(bVisible As Boolean)
'+++ VB/Rig Skip
    Me.lblShipWhse.Visible = bVisible
    Me.txtShipWhse.Visible = bVisible
    Me.txtShipWhseDesc.Visible = bVisible
    Me.lblTransfer.Visible = bVisible
    Me.txtTransfer.Visible = bVisible
    Me.lblTransit.Visible = bVisible
    Me.txtTransit.Visible = bVisible
    Me.lblQtyShip.Visible = bVisible
    Me.nbrQtyShip.Visible = bVisible
End Sub

Public Sub SetRcptPurchaseFieldsVisible(bVisible As Boolean)
'+++ VB/Rig Skip
    Me.lblVendID.Visible = bVisible
    Me.txtVendID.Visible = bVisible
    Me.txtVendName.Visible = bVisible
    Me.lblPurchCompany.Visible = bVisible
    Me.txtPurchCompany.Visible = bVisible
    Me.lblPONo.Visible = bVisible
    Me.txtPONum.Visible = bVisible
    Me.lblDash.Visible = bVisible
    Me.txtRelNo.Visible = bVisible
    Me.lblWhse.Visible = bVisible
    Me.txtWhse.Visible = bVisible
    Me.lblDept.Visible = bVisible
    Me.txtDept.Visible = bVisible
    Me.chkMatch.Visible = bVisible
    Me.lblQtyReturn.Visible = bVisible
    Me.nbrQtyReturn.Visible = bVisible
End Sub


Private Sub ddnType_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnType, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    With ddnType
        If .Text <> .Tag Then
            SetUpReceiptTypeFields
        End If
        .Tag = .Text
    End With
    SetCtrlFocus Me.lkuReceiptNo
End Sub

Private Sub Form_Paint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
' This will only get triggered when the form first comes up.
    If mbLoadingForm Then
'   Disable the controls for line entry and clear out various fields.
        nbrQtyRcvd.Value = 0
        chkMatch.Enabled = False
        chkCloseDistr.Enabled = False
        txtItemDescription = ""
        txtUOM = ""
        SetCtrlFocus lkuReceiptNo
        
        mbLoadingForm = False
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub CMMemoSelected()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lKeyValue As Long
    
    lKeyValue = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    
    Me.Enabled = False
    
    '-- Launch the Memo object
    gLaunchMemo Me, moClass.moFramework, moSotaObjects, kEntTypePOReceipt, lKeyValue, _
                "Receipt:", lkuReceiptNo.Text, moClass.moSysSession.BusinessDate

    '-- Set the Memo toolbar to the correct state
    gSetMemoToolBarState moClass.moAppDB, lKeyValue, kEntTypePOReceipt, msCompanyID, tbrMain
    
    Me.Enabled = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMemoSelected", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub OfficeInitialization()
'+++ VB/Rig Skip +++
On Error Resume Next
'*************************************************************************
'      Desc:  The Office class has been initialized elsewhere and has
'             already received this form�s reference. Therefore,
'             this method is additive if the client task wishes to add
'             clsDMForm and/or clsDMGrid references.
'     Parms:  N/A
'   Returns:  N/A
'************************************************************************
    With tbrMain.Office
        .AddFormObject moDmHeader, "Receipt"
        .AddGridObject moDmDetl, "ReceiptLines"
        .AddGridObject moDmLineDist, "LinesDist"
    End With
    Err.Clear
End Sub

Private Sub cmdPOLandedCost_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdPOLandedCost, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
On Error GoTo ExpectedErrorRoutine
    
    Dim lRcvrKey As Long
    Dim iRet As Integer
    
    If moPOEnterLandedCost Is Nothing Then
        Set moPOEnterLandedCost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kclsPOEnterLandedCost, ktskPOEnterLandedCost, kAOFRunFlags, kContextAOF)
                            
        If Not moPOEnterLandedCost Is Nothing Then
            If Not moPOEnterLandedCost.InitEnterLandedCost(moClass) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Error initializing Landed Cost object."
                Exit Sub
            End If
        End If
    End If
    
    'Display the Enter Landed Cost UI.
    If Not moPOEnterLandedCost Is Nothing Then
        lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        If lRcvrKey > 0 Then
            moPOEnterLandedCost.bEnterLandedCostForViewRcvr lRcvrKey
        End If
    End If
    
Exit Sub

ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "cmdPOLandedCost_Click"
gClearSotaErr
Exit Sub

End Sub
