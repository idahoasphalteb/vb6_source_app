Attribute VB_Name = "basEnterRcptOfGoods"
'***********************************************************************
'   Name:
'       basProcRcptOfGoods
'
'   Description:
'       This module contains code specific to the Process Receipt of
'       Invoice task.
'
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 05/07/97 JSP
'     Mods:
'***********************************************************************
Option Explicit

    '-- Public PO-specific variables
    Public mbAutoSelectLines    As Boolean
    Public mbDefaultQty         As Boolean
    Public miMatchLevel         As Integer
    Public mbInternalLinesClick As Boolean
    Public mvPOKey              As Variant
    Public mlOrigPOKey          As Long
    Public msOpenString         As String
    Public msClosedString       As String
    Public msSoftOvrdString     As String
    Public msHardOvrdString     As String
    Public msCreditString       As String
    Public msReplacementString  As String
    Public mbLineDeleted        As Boolean
    Public mbValidatingPO       As Boolean
    Public mbManualClick        As Boolean
    
    '-- Private PO-specific variables
    Private mbInVendID          As Boolean
    Private mbInPurchCompany    As Boolean
    
    '-- Public PO-specific constants
    Public Const kPONoLen = 10
    Public Const kPONoMaxLen = 15
    Public Const kMatchLevelNone = 0
    Public Const kMatchLevel2Way = 2
    Public Const kMatchLevel3Way = 3
    Public Const kMatchStatusOpen = 1
    Public Const kMatchStatusClosed = 2
    Public Const kMatchStatusSoftOvrd = 3
    Public Const kMatchStatusHardOvrd = 4
    Public Const kReturnNone = 0
    Public Const kReturnForCredit = 1
    Public Const kReturnForReplace = 2
    
       
    '-- Select PO Lines Grid column constants
    Public Const kcolPOSelect = 1
    Public Const kcolPOPOLineNo = 2
    Public Const kcolPOItemID = 3
    Public Const kcolPOQtyView = 4
    Public Const kcolPOUOM = 5
    Public Const kcolPOUnitCost = 6
    Public Const kcolPOItemDesc = 7
    Public Const kcolPOPONo = 8
    Public Const kcolPOPOKey = 9
    Public Const kcolPOPOLineKey = 10
    Public Const kcolPOPOLineDistKey = 11
    Public Const kcolPOGLAcctKey = 12
    Public Const kcolPOAcctRefKey = 13
    Public Const kcolPOItemKey = 14
    Public Const kcolPOUnitMeasKey = 15
    Public Const kcolPOUnitMeasType = 16
    Public Const kcolPOLineClosed = 17
    Public Const kcolPOTargetCoID = 18
    Public Const kcolPORcptReq = 19
    Public Const kcolPOQtyOrd = 20
    Public Const kcolPOQtyRcvd = 21
    Public Const kcolPOQtyRtrnForReplace = 22
    Public Const kcolPOQtyOpen = 23
    Public Const kcolPOQty = 24
    Public Const kcolPOWhseID = 25
    Public Const kcolPODeptID = 26
    Public Const kcolPOPromiseDate = 27
    Public Const kPOGridMaxCols = 27
    
    
    '-- PO Number validation return values
    Public Enum iPOValidVals
        kPOValid = -1
        kErrPONotExist = 1
        kErrPONotOpen = 2
        kErrPONotStandard = 3
        kErrPOAllLinesClosed = 4
        kErrPOBadVendor = 5
        kErrPOOnHold = 6
        kErrPOBlankVendor = 7
    End Enum
    
    Type uPODfltsType
        sPONo                   As String
        lPOKey                  As Long
        bPONumberChanged        As Boolean
        sVendID                 As String
        lVendKey                As Long
        sCurrID                 As String
        dCurrExchRate           As Double
        bReturnedVendor         As Boolean
        bSetCurrIDFromPO        As Boolean
    End Type
    Public muPODflts            As uPODfltsType
    
    Type uPOLineOldVals
        lPOLineKey              As Long
        lUnitMeasKey            As Long
        dQty                    As Double
        dUnitCost               As Double
    End Type
    Public muPOLineOldVals      As uPOLineOldVals
    
    Public Type uVendPerfDtl
        iDefective              As Integer
        iDamaged                As Integer
        iCarrierDamage          As Integer
        iImproperLabel          As Integer
        iDocMissing             As Integer
        iImproperPackage        As Integer
        iNotToSpec              As Integer
        iOtherIssue             As Integer
        sPerfComments           As String
    End Type
    
    
Public Const ktskProcRcptOfGoods = 184943042              'PO Enter Receipts Data Entry
Public Const kclsProcRcptOfGoods = "pozdfdl1.clsEnterRcptOfGoodsDTE"



Const VBRIG_MODULE_ID_STRING = "POZDB001.BAS"
'
    
Public Function iGetPostDateYearPO(oClass As Object, lBatchKey As Long, sPostDate As String) As Integer
'******************************************************************
'    Desc: This function will retrieve and return the posting
'          date's year for the current PO receipt of invoice batch.
' Returns: The PostDate Year.
'******************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim rs As Object
    Dim iPostDateYear As Integer

    iGetPostDateYearPO = -1
    
    '-- Get the Post Date for this Receiver batch
    sSQL = "SELECT PostDate FROM tpoBatch "
    sSQL = sSQL & "WHERE BatchKey = " & lBatchKey
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not rs.IsEmpty Then
        sPostDate = Trim$(gsGetValidStr(rs.Field("PostDate")))
        
        '-- Check to see if the Post Date has a value
        If Len(sPostDate) = 0 Then
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateNull
            GoTo ExpectedErrorRoutine
        Else
            iPostDateYear = Year(sPostDate)
            If (iPostDateYear <= 0) Then
                giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateError
                GoTo ExpectedErrorRoutine
            End If
        End If
    Else
        '-- The corresponding batch row was not found
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchNotFoundError, Format$(lBatchKey)
        GoTo ExpectedErrorRoutine
    End If

    rs.Close: Set rs = Nothing

    '-- Return the value
    iGetPostDateYearPO = iPostDateYear
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    rs.Close: Set rs = Nothing
    sPostDate = ""
    iGetPostDateYearPO = -1
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetPostDateYearPO", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub SetGridRedraw(grd As Object, bFlag As Boolean)
'*********************************************************
' Desc: This routine will use the Windows API and the grid
'       methods to turn redraw on/off. The Windows API
'       is used because it WORKS!, whereas the grid method
'       is flaky!
'*********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With grd
        SendMessage .hwnd, WM_SETREDRAW, bFlag, 0
        .redraw = bFlag
        If bFlag Then .Refresh
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridRedraw", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub CreatePONoLinkForGrid(oAppDB As Object, sCompanyID As String, _
                                 lRcvrKey As Long, lVRunMode As Long)
'*******************************************************
' Desc: This routine will create a temp table containing
'       the PO number so that data manager can join with
'       the temp table to display the full PO number in
'       the receiver detail grid.
'*******************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL    As String
    Static bDid As Boolean

    If (Not bDid) Then
        On Error Resume Next
        sSQL = "DROP TABLE #tpoPOJoin"
        oAppDB.ExecuteSQL sSQL
        On Error GoTo VBRigErrorRoutine
        
        sSQL = "CREATE TABLE #tpoPOJoin "
        sSQL = sSQL & "(POLineKey INT      NULL, "
        sSQL = sSQL & "POLineNo   SMALLINT NULL, "
        sSQL = sSQL & "POItemKey  INT      NULL, "
        sSQL = sSQL & "POItemID   varCHAR(30)      NULL, "
        sSQL = sSQL & "POItemDesc VARCHAR(40) NULL, "
        sSQL = sSQL & "POQtyOrd   DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POUOMID    varCHAR(6) NULL, "
        sSQL = sSQL & "POQtyRcvd  DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POQtyRtrned  DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POQtyOpen  DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POWhseID    varCHAR(6) NULL, "
        sSQL = sSQL & "PODeptID    varCHAR(15) NULL, "
        sSQL = sSQL & "POUOMType   INT NULL, "
        sSQL = sSQL & "POUOMKey    INT NULL,"
        sSQL = sSQL & "TrnsfrOrderLineKey INT      NULL, "
        sSQL = sSQL & "TRQtyShip  DECIMAL (16,8) NULL)"
        oAppDB.ExecuteSQL sSQL

        bDid = True
    End If
    
    '-- Populate the temp table
    sSQL = "{call sppoGetRcvrDetl (" & gsQuoted(sCompanyID) & ", " & lRcvrKey & ")}"
    oAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CreatePONoLinkForGrid", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ClearPODefaultsStruct()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With muPODflts
        .sPONo = ""
        .lPOKey = 0
        .bPONumberChanged = False
        .sVendID = ""
        .lVendKey = 0
        .sCurrID = ""
        .dCurrExchRate = 0
        .bReturnedVendor = False
        .bSetCurrIDFromPO = False
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearPODefaultsStruct", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basEnterRcptOfGoods"
End Function
