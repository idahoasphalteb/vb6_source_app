Attribute VB_Name = "basReceipt"
'**********************************************************************
'     Name: basReceipt
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 01-07-1998
'     Mods: mm/dd/yy XXX
'**********************************************************************
Option Explicit
    
    '-- Security Event Constants
    
    '-- Object Task Constants
    
    '-- Static List Constants

    '-- Run modes for this project, since it has so many versatile uses
    Public Enum lVRunModes
        kEnterReceiptDTE = 1
        kEnterReceiptAOF = 2
        kViewEditReceiptDTE = 3
        kViewEditReceiptDDN = 4
        kErogDTE = 5
        kErogDDN = 6
    End Enum

    '-- Needed for the currency form
    Public mbEnterAsTab As Boolean

    '-- Currency field constants
    Public Const kiAmtLen = 24
    Public Const kiAmtIntegralPlaces = 21
    Public Const kiUCLen = 15
    Public Const kiUCIntegralPlaces = 10
    Public Const kiQtyLen = 16
    Public Const kiQtyIntegralPlaces = 8
    
    '-- Amount Overflow constants
    Public Const kMaxAmtLen = "999999999999.999"
    Public Const kMaxCostLen = "9999999999.99999"
        
    '-- Detail tab page constants
    Public Const kiAccountTab = 0
    Public Const kiSalesTaxTab = 1
    Public Const kiFreightTab = 2
    Public Const kiCommentTab = 3
    Public Const kiPOTab = 4
    
    '-- Detail grid column constants
    Public Const kColPOLineNo = 1          '-- displays PO Line Number
    Public Const kColPOLineKey = 2         '-- hidden column; stores POLineKey
    Public Const kColItemKey = 3           '-- hidden column; stores the item key
    Public Const kColItemID = 4            '-- hidden column; stores unmasked ItemID
    Public Const kColMaskedItemID = 5      '-- displays masked ItemID
    Public Const kColItemDesc = 6          '-- displays item's Description from PO
    Public Const kColUnitMeasRcvdID = 7    '-- displays UnitMeasID
    Public Const kColUnitMeasRcvdKey = 8   '-- hidden column; stores UnitMeasKey
    Public Const kColQtyOrdered = 9        '-- displays the quantity ordered on the PO line
    Public Const kColQtyRcvd = 10          '-- displays unsigned Quantity
    Public Const kColQtyRcvdS = 11         '-- hidden column; stores signed Quantity
    Public Const kColUnitCost = 12         '-- displays Unit Cost
    Public Const kColMatchStatus = 13      '-- hidden column; stores MatchStatus
    Public Const kColDispMatchStatus = 14  '-- displays Match Status text
    Public Const kColGLAcctKey = 15        '-- hidden column; stores GLAcctKey
    Public Const kColAcctRefKey = 16       '-- hidden column; stores AcctRefKey
    Public Const kColReturnType = 17       '-- hidden column; stores ReturnType
    Public Const kColDispReturnType = 18   '-- displays Return Type text
    Public Const kColTranComment = 19      '-- displays item's ExtCmnt
    Public Const kColSeqNo = 20            '-- hidden column; stores line's SeqNo
    Public Const kColReceiptLineKey = 21   '-- hidden column; stores ReceiptLineKey
    Public Const kColWhseID = 22           '-- displays the Warehouse ID
    Public Const kColDeptID = 23           '-- displays the Department ID
    Public Const kColUnitMeasOrdKey = 24   '-- hidden column;
    Public Const kColUnitMeasOrdID = 25    '-- displays the UOM from the PO Line
    Public Const kColTotalRcvd = 26        '-- displays the qty rcvd from the PO line dist
    Public Const kColQtyRtrned = 27        '-- displays the qty returned for replacement
    Public Const kColQtyOpen = 28          '-- displays the qty currently open for receiving
    Public Const kColCloseSrcLine = 29      '-- displays the Close PO line flag
    Public Const kColItemVol = 30          '-- hidden column;
    Public Const kColItemWght = 31         '-- hidden column;
    Public Const kColInvtTranKey = 32      '-- hidden column;
    Public Const kColDiscAmt = 33          '-- hidden column;
    Public Const kColOtherAmt = 34         '-- hidden column;
    Public Const kColSTaxAmt = 35          '-- hidden column;
    Public Const kColMeasType = 36         '-- hidden column;
    Public Const kColQtyInPOUOM = 37       '-- hidden column;
    Public Const kColInvtTempTranKey = 38  '-- hidden column;
    Public Const kColInvtTranDirty = 39    '-- hidden column;
    Public Const kColFreightAmt = 40    '-- hidden column;
    Public Const kColTrnsfrLineKey = 41     '-- hidden column; stores TrnsfrOrderLineKey
    Public Const kColQtyShip = 42           '-- hidden; the Transfer order qty shipped
    Public Const kColProjectID = 43
    Public Const kColProjectKey = 44
    Public Const kColPhaseID = 45
    Public Const kColPhaseKey = 46
    Public Const kColTaskID = 47
    Public Const kColTaskKey = 48
    Public Const kColCostClassID = 49
    Public Const kColCostClassKey = 50
    Public Const kColVarianceStr = 51
    Public Const kColVariance = 52
    Public Const kColJobLineKey = 53
    Public Const kColBillMeth = 63 'not used just need for Project class
    Public Const kColBillMethKey = 64 'nou used just need for project class
    
     'Vendor performance
    Public Const kColDefective = 54
    Public Const kColDamaged = 55
    Public Const kColCarrierDamage = 56
    Public Const kColImpLabel = 57
    Public Const kColDocMissing = 58
    Public Const kColImpPackage = 59
    Public Const kColNotToSpec = 60
    Public Const kColOtherIssue = 61
    Public Const kColPerfComments = 62
    Public Const kGridMaxCols = 62
    
        
    '-- Misc. Constants
    Public Const kDisplayMsg = "1"
    Public Const kVendPmtNoRowsFound = 10
    Public Const kiOn = 1
    Public Const kiOff = 0
    
    '-- Vendor Defaults structure
    Public Type uVendDfltsType
        lVendKey            As Long
        lPrimaryAddrKey     As Long
        lDfltPurchAcctKey   As Long
        lDfltItemKey        As Long
        bLoadedDfltItem     As Boolean
        sDfltCurrID         As String
    End Type
    Public muVendDflts      As uVendDfltsType
    
    '-- Item Info structure
    Public Type uItemInfoType
        iMeasType           As Integer
        lReceiverLineKey     As Long
        lReceiverLineDistKey As Long
        iCmntOnly           As Integer
    End Type
    Public muItemInfo       As uItemInfoType
    
    
    
    
Const VBRIG_MODULE_ID_STRING = "APZDG001.BAS"

Public Function iGetPostDateYear(oClass As Object, lBatchKey As Long, sPostDate As String) As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'******************************************************************
'    Desc: This function will retrieve and return the posting
'          date's year for the current AP Receiver batch.
' Returns: The PostDate Year.
'******************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim rs As Object
    Dim iPostDateYear As Integer

    iGetPostDateYear = -1
    
    '-- Get the Post Date for this Receiver batch
    sSQL = "SELECT PostDate FROM tapBatch "
    sSQL = sSQL & "WHERE BatchKey = " & lBatchKey
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not rs.IsEmpty Then
        sPostDate = Trim$(gsGetValidStr(rs.Field("PostDate")))
        
        '-- Check to see if the Post Date has a value
        If Len(sPostDate) = 0 Then
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateNull
            GoTo ExpectedErrorRoutine
        Else
            iPostDateYear = Year(sPostDate)
            If (iPostDateYear <= 0) Then
                giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateError
                GoTo ExpectedErrorRoutine
            End If
        End If
    Else
        '-- The corresponding batch row was not found
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchNotFoundError, Format$(lBatchKey)
        GoTo ExpectedErrorRoutine
    End If

    rs.Close: Set rs = Nothing

    '-- Return the value
    iGetPostDateYear = iPostDateYear
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    rs.Close: Set rs = Nothing
    sPostDate = ""
    iGetPostDateYear = -1
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetPostDateYear", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function lGetNextRcptSeqNo(oDB As Object, lBatchKey As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Description: Given the Batch Key, get and assign the next sequence
'              Number.
' Parameters: oDB <in>:  The database variable to use.
'             lBatchKey <in>:  The Batch Key to read tapBatchLogSysDetl
'                              with.
'*************************************************************************
    lGetNextRcptSeqNo = 0

    With oDB
        .SetInParam lBatchKey
        .SetOutParam lGetNextRcptSeqNo
        .ExecuteSP "spciGetBatchNextSeqNo"
        If (Err.Number = 0) Then
            lGetNextRcptSeqNo = .GetOutParam(2)
        End If
    
        .ReleaseParams
    End With
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lGetNextRcptSeqNo", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub SetGridNumericAttr(grd As Control, lCol As Long, iDecPlaces As Integer, sMaxFloat As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Set the column type based on the currency attributes passed in
    gGridSetColumnType grd, lCol, SS_CELL_TYPE_FLOAT
    With grd
        .Col = lCol
        .Col2 = lCol
        .Row = -1
        .Row2 = -1
        .BlockMode = True
        .TypeFloatMax = sMaxFloat
        .TypeFloatDecimalPlaces = iDecPlaces
        '.TypeFloatMoney = True
        .TypeFloatSeparator = True
        .BlockMode = False
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridNumericAttr", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function sZeroFillNumber(sNum As String, iLen As Integer) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sTempNum As String

    sTempNum = Trim$(sNum)
    
    '-- Make sure it's numeric before zero filling
    If IsNumeric(sTempNum) Then
        '-- Use the mask to zero-fill the Receiver Number
        sTempNum = Format$(sTempNum, String$(iLen, "0"))
    End If

    sZeroFillNumber = sTempNum
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sZeroFillNumber", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sText As String

    If (lErr = guSotaErr.Number) And (Trim$(guSotaErr.Description) <> Trim$(sDesc)) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If

    If Not oClass Is Nothing Then
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyId
        sText = sText & "  AppName:  " & App.Title
    End If
    
    sText = sText & " AppName:  " & App.Title
    sText = sText & "    Error < " & Format(lErr)
    sText = sText & " >   occurred at < " & sSub
    sText = sText & " >  in Procedure < " & sProc
    sText = sText & " > " & sDesc
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MyErrMsg", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("pozdg001.clsViewRcptOfGoods")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basReceipt"
End Function



