VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#102.0#0"; "SOTATbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Begin VB.Form frmWghtVol 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Weight/Volume"
   ClientHeight    =   1980
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   HelpContextID   =   69137
   Icon            =   "pozdf003.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1980
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin NEWSOTALib.SOTANumber nbrWeight 
      Height          =   285
      Left            =   1410
      TabIndex        =   7
      Top             =   1080
      WhatsThisHelpID =   69148
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtItemID 
      Height          =   285
      Left            =   780
      TabIndex        =   1
      Top             =   630
      WhatsThisHelpID =   69147
      Width           =   1905
      _Version        =   65536
      _ExtentX        =   3360
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   4680
      _ExtentX        =   8255
      _ExtentY        =   741
      Style           =   6
   End
   Begin NEWSOTALib.SOTANumber nbrVolume 
      Height          =   285
      Left            =   1410
      TabIndex        =   8
      Top             =   1440
      WhatsThisHelpID =   69145
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin VB.Label lblVolUOM 
      Caption         =   "VOL UOM"
      Height          =   255
      Left            =   2820
      TabIndex        =   10
      Top             =   1470
      Width           =   975
   End
   Begin VB.Label lblWghtUOM 
      Caption         =   "WGHT UOM"
      Height          =   255
      Left            =   2820
      TabIndex        =   9
      Top             =   1110
      Width           =   975
   End
   Begin VB.Label lblUOM2 
      Caption         =   "UOM ID"
      Height          =   285
      Left            =   360
      TabIndex        =   6
      Top             =   1470
      Width           =   645
   End
   Begin VB.Label lblUOM1 
      Caption         =   "UOM ID"
      Height          =   285
      Left            =   360
      TabIndex        =   5
      Top             =   1110
      Width           =   645
   End
   Begin VB.Label Label3 
      Caption         =   "1"
      Height          =   285
      Left            =   90
      TabIndex        =   4
      Top             =   1470
      Width           =   225
   End
   Begin VB.Label Label2 
      Caption         =   "1"
      Height          =   285
      Left            =   90
      TabIndex        =   3
      Top             =   1110
      Width           =   225
   End
   Begin VB.Label Label1 
      Caption         =   "Item"
      Height          =   225
      Left            =   120
      TabIndex        =   2
      Top             =   660
      Width           =   435
   End
End
Attribute VB_Name = "frmWghtVol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

    Private mdWeight As Double
    Private mdVolume As Double
    '-- Context Menu Object
    Private moContextMenu       As clsContextMenu



Public Sub InitializeFormInfo(sItem As String, _
                                sUOMID As String, _
                                sWghtUOMID As String, _
                                sVolUOMID As String, _
                                dWeight As Double, _
                                dVolume As Double, _
                                bEditWght As Boolean, _
                                bEditVol As Boolean)

    'Initialize the form with the values passed in.

    txtItemID.Text = sItem
    lblUOM1.Caption = sUOMID
    lblUOM2.Caption = sUOMID
    nbrWeight.Value = dWeight
    nbrVolume.Value = dVolume
    lblWghtUOM.Caption = sWghtUOMID
    lblVolUOM.Caption = sVolUOMID
    nbrWeight.Enabled = bEditWght
    nbrVolume.Enabled = bEditVol

End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        BindContextMenu
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case sKey
        Case kTbFinishExit
            frmEnterRcptOfGoods.dVolAmt = CDbl(nbrVolume.Value)
            frmEnterRcptOfGoods.dWghtAmt = CDbl(nbrWeight.Value)
            Me.Hide
        Case kTbCancelExit
            Me.Hide
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Sets all object and collection variables to nothing.
' Parameters: N/A
' Returns: N/A
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next


'    Set moClass = Nothing
'    Set moMainForm = Nothing
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Instantiate Context Menu Class
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        'Add any Special .Bind or .BindGrid commands here for Drill Around/Drill Down
        'or for Grids - Here

        'Assign the Winhook control to the Context Menu Class
        ' **PRESTO ** Set .Hook = Me.WinHook1
        Set .Form = frmWghtVol
    
        'Init will set properties of Winhook to intercept WM_RBUTTONDOWN
        .Init
    End With
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property






Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property





