VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#47.0#0"; "sotacalendar.ocx"
Begin VB.Form frmSelectPOLines 
   Caption         =   "Select Purchase Order Lines"
   ClientHeight    =   5475
   ClientLeft      =   60
   ClientTop       =   1440
   ClientWidth     =   9450
   HelpContextID   =   69104
   Icon            =   "pozdf002.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5475
   ScaleWidth      =   9450
   ShowInTaskbar   =   0   'False
   Begin FPSpreadADO.fpSpread grdSelectPOLines 
      Height          =   3975
      Left            =   90
      TabIndex        =   9
      Top             =   960
      WhatsThisHelpID =   69136
      Width           =   9255
      _Version        =   524288
      _ExtentX        =   16325
      _ExtentY        =   7011
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   1
      MaxRows         =   1
      SpreadDesigner  =   "pozdf002.frx":23D2
      AppearanceStyle =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   29
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   741
      Style           =   4
   End
   Begin VB.CommandButton cmdSelectClearAll 
      Caption         =   "&Select All"
      Height          =   360
      Index           =   0
      Left            =   90
      TabIndex        =   27
      Top             =   5040
      WhatsThisHelpID =   69133
      Width           =   915
   End
   Begin VB.CommandButton cmdSelectClearAll 
      Caption         =   "&Clear All"
      Height          =   360
      Index           =   1
      Left            =   1110
      TabIndex        =   26
      Top             =   5040
      WhatsThisHelpID =   69132
      Width           =   915
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   19
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin VB.CheckBox chkViewClosedLines 
      Caption         =   "&View Closed Lines"
      Height          =   285
      Left            =   4980
      TabIndex        =   8
      Top             =   1065
      Visible         =   0   'False
      WhatsThisHelpID =   69121
      Width           =   1695
   End
   Begin VB.Frame fraRcptOption 
      Height          =   585
      Left            =   90
      TabIndex        =   5
      Top             =   3870
      Visible         =   0   'False
      Width           =   3675
      Begin VB.OptionButton optQtyMethod 
         Caption         =   "Receive &Manually"
         Height          =   285
         Index           =   1
         Left            =   1830
         TabIndex        =   7
         Top             =   210
         WhatsThisHelpID =   69119
         Width           =   1785
      End
      Begin VB.OptionButton optQtyMethod 
         Caption         =   "Receive in &Full"
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   210
         Value           =   -1  'True
         WhatsThisHelpID =   69118
         Width           =   1635
      End
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   14
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5085
      WhatsThisHelpID =   73
      Width           =   9450
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin VB.Label txtRcvrNo 
      Height          =   195
      Left            =   1350
      TabIndex        =   13
      Top             =   2600
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.Label lblRcvrNo 
      Caption         =   "Receiver:"
      Height          =   195
      Left            =   90
      TabIndex        =   25
      Top             =   2600
      Visible         =   0   'False
      Width           =   765
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblLegend 
      AutoSize        =   -1  'True
      Caption         =   "("
      Height          =   195
      Index           =   2
      Left            =   6720
      TabIndex        =   12
      Top             =   1095
      Visible         =   0   'False
      Width           =   45
   End
   Begin VB.Label lblLegend 
      AutoSize        =   -1  'True
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   6780
      TabIndex        =   11
      Top             =   1125
      Visible         =   0   'False
      Width           =   75
   End
   Begin VB.Label lblLegend 
      AutoSize        =   -1  'True
      Caption         =   "indicates a Closed line)"
      Height          =   195
      Index           =   0
      Left            =   6900
      TabIndex        =   10
      Top             =   1095
      Visible         =   0   'False
      Width           =   1620
   End
   Begin VB.Label txtVendName 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   4950
      TabIndex        =   4
      Top             =   600
      UseMnemonic     =   0   'False
      Width           =   2535
   End
   Begin VB.Label txtVendID 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   3660
      TabIndex        =   3
      Top             =   600
      UseMnemonic     =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblVendor 
      AutoSize        =   -1  'True
      Caption         =   "Vendor:"
      Height          =   195
      Left            =   3000
      TabIndex        =   2
      Top             =   600
      Width           =   555
   End
   Begin VB.Label txtPONo 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   1350
      TabIndex        =   1
      Top             =   600
      UseMnemonic     =   0   'False
      Width           =   1545
   End
   Begin VB.Label lblPONo 
      AutoSize        =   -1  'True
      Caption         =   "Purchase Order:"
      Height          =   195
      Left            =   90
      TabIndex        =   0
      Top             =   600
      Width           =   1155
   End
End
Attribute VB_Name = "frmSelectPOLines"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'   Name:
'       frmSelectPOLines
'
'   Description:
'       This form handles the user interface for selecting purchase
'       order lines onto a receiver.
'
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 05/08/97 JSP
'     Mods:
'***********************************************************************
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    '-- Private variables of form properties
    Private moClass             As Object
    Private moMainForm          As Form
    Private mbShowUI            As Boolean
    Private mbDefaultQty        As Boolean
    Private miUnitCostDecPlaces As Integer
    Private miQtyDecPlaces      As Integer
    Private mlReceiverKey        As Long
    Private msCompanyID         As String
    Private msPurchCompanyID    As String
    Private mlPOWhseKey         As Long
    Private msRcptDate          As String
    Private miDaysEarly         As String
    Private mdQtyRcvd           As Double
    Private mlTranType          As Long
    
    '-- Context Menu Object
    Private moContextMenu       As clsContextMenu
    
    '-- Form resize variables
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    
    Private Const kClipSize = 1
    Private Const kiLineClosed = 2
    
    Private mbProcessOnExit As Boolean
    
    '-- Button constants
    Private Const kBtnSelectAll = 0
    Private Const kBtnClearAll = 1

Const VBRIG_MODULE_ID_STRING = "POZDB002.FRM"
Public Property Get bProcessExit() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bProcessExit = mbProcessOnExit
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessExit_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oMainForm(oNewMainForm As Form)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moMainForm = oNewMainForm
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oMainForm_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bShowUI(bNewShowUI As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbShowUI = bNewShowUI
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bShowUI_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bShowUI() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bShowUI = mbShowUI
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bShowUI_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bDefaultQty(bNewDefaultQty As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbDefaultQty = bNewDefaultQty
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDefaultQty_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let sRcptDate(sNewRcptDate As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msRcptDate = sNewRcptDate
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sRcptDate_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let iDaysEarly(iNewDaysEarly As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    miDaysEarly = iNewDaysEarly
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iDaysEarly_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let iUnitCostDecPlaces(iNewUnitCostDecPlaces As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    miUnitCostDecPlaces = iNewUnitCostDecPlaces
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iUnitCostDecPlaces_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let iQtyDecPlaces(iNewQtyDecPlaces As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    miQtyDecPlaces = iNewQtyDecPlaces
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iQtyDecPlaces_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRcvrKey(lNewReceiverKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlReceiverKey = lNewReceiverKey
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lReceiverKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let lTranType(lNewTranType As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlTranType = lNewTranType
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lTranType_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let sCompanyID(sNewCompanyID As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msCompanyID = sNewCompanyID
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sCompanyID_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let sPurchCompanyID(sNewCompanyID As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msPurchCompanyID = sNewCompanyID
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sPurchCompanyID_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lWhseKey(lNewWhseKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlPOWhseKey = lNewWhseKey
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lWhseKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Private Sub chkViewClosedLines_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkViewClosedLines, True
    #End If
'+++ End Customizer Code Push +++

    ViewClosedLines chkViewClosedLines.Value

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkViewClosedLines_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectClearAll_Click(Index As Integer)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelectClearAll(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'***************************************************************
' Desc: This routine will turn the Select checkbox on or off for
'       all lines, depending on the button pressed.
'***************************************************************
    Dim lRow As Long
    Dim sValue As String
    
    If (Index = kBtnSelectAll) Then
        sValue = CStr(vbChecked)
    Else
        sValue = CStr(vbUnchecked)
    End If
    
    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdSelectPOLines, False
    
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        gGridUpdateCell grdSelectPOLines, lRow, kcolPOSelect, sValue
    Next lRow
        
    '-- Turn redraw of the grid back on
    SetGridRedraw grdSelectPOLines, True
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Form_KeyDown traps the hot keys when the KeyPreview
'       property of the form is set to True.
' Parameters: Standard ones.
' Returns: N/A
'***********************************************************************
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
      
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'       of the form is set to True.
'********************************************************************
    Select Case KeyAscii
        Case vbKeyReturn
            If moClass.moSysSession.EnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

  Set sbrMain.Framework = moClass.moFramework
  sbrMain.BrowseVisible = False

    
    '-- Setup form resizing variables
    miMinFormHeight = Me.Height
    miMinFormWidth = Me.Width
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    
        '-- Populate PO and Vendor fields
    If mlTranType = kTranTypePORG Then
        txtPONo = moMainForm.lkuPONo
    Else
        SetupFormForReturns
        txtPONo = moMainForm.txtPONum
        If Trim(moMainForm.txtRelNo) <> "" Then
            txtPONo = txtPONo & "-" & moMainForm.txtRelNo
        End If
        txtRcvrNo = moMainForm.lkuReceiver
    End If
    txtVendID = moMainForm.lkuVendID
    txtVendName = moMainForm.txtVendName
    
    '-- Decide which quantity method to default to
    If mbDefaultQty Then
        optQtyMethod(0) = True
    Else
        optQtyMethod(1) = True
    End If
    
    '-- Setup the display of the grid
    InitializeGrid
    
    '-- Load the grid with all valid PO lines
    LoadPOLinesGrid
    
    '-- Hide all closed lines initially
'    ViewClosedLines False
    
    If mbShowUI Then
'        ShowClosedLinesIndicator
        BindContextMenu
        SetupBars
        mbProcessOnExit = True
    Else
        '-- Automatically select all lines
        cmdSelectClearAll_Click kBtnSelectAll
        mbProcessOnExit = True
    End If
    
    
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    '-- How are we being unloaded?
    Select Case UnloadMode
        Case vbFormCode
            'Do Nothing
        
        Case Else
            '-- Make sure quantities in grid are up-to-date
            UpdateQtyAndAmt
            
            '-- This form will be unloaded from the Receiver form,
            '-- after the lines have been loaded in the detail grid
            If mbShowUI Then
                Me.Hide
            End If
            GoTo CancelShutDown
    
    End Select
  
    '-- Perform a clean shutdown now
    PerformCleanShutDown
  
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
  
CancelShutDown:
    Cancel = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Clean up the project when shutting down.
' Parameters: N/A
' Returns: N/A
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    'Unload all forms loaded from this form
    'gUnloadChildForms Me
    
    Set moContextMenu = Nothing
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        '-- Resize the grid and associated controls
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdSelectPOLines
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, grdSelectPOLines
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Sets all object and collection variables to nothing.
' Parameters: N/A
' Returns: N/A
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    Set moClass = Nothing
    Set moMainForm = Nothing
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitializeGrid()
'*************************************************************
' Desc: This routine will set up the Select PO Lines grid with
'       all necessary properties.
'*************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sTitle As String
    Dim sItem As String
    Dim oSysSession As Object
    Dim oAppDB As Object
    Dim lCol As Long
    
    Set oSysSession = moClass.moSysSession
    Set oAppDB = moClass.moAppDB
    
    '-- Set default grid properties
    gGridSetProperties grdSelectPOLines, kPOGridMaxCols, kGridDataSheet
    grdSelectPOLines.ScrollBars = SS_SCROLLBAR_BOTH

    '-- Set column headers
    '-- Select
    sTitle = gsBuildString(kARSelect, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kcolPOSelect, sTitle
    
    '-- PO Line
    sTitle = gsBuildString(ksPOLineNum, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kcolPOPOLineNo, sTitle

    '-- Item
    sItem = gsBuildString(kItem, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kcolPOItemID, sItem
    
    '-- Qty Received or returned
    If mlTranType = kTranTypePORG Then
        sTitle = gsBuildString(ksQtyReceived, oAppDB, oSysSession)
    Else
'MVB - make this into a string.
        sTitle = "Qty Return"
    End If
    gGridSetHeader grdSelectPOLines, kcolPOQtyView, sTitle
    
    '-- UOM
    sTitle = gsBuildString(kAnUOM, oAppDB, oSysSession)
    If mlTranType = kTranTypePORG Then
        gGridSetHeader grdSelectPOLines, kcolPOUOM, sTitle
    Else
        gGridSetHeader grdSelectPOLines, kcolPORcptUOMID, sTitle
    End If
    
    '-- Item Description
    sTitle = sItem & " " & gsBuildString(kDescription, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kcolPOItemDesc, sTitle
    
    '-- Set grid column widths
    gGridSetColumnWidth grdSelectPOLines, kcolPOSelect, 6
    gGridSetColumnWidth grdSelectPOLines, kcolPOPOLineNo, 6
    gGridSetColumnWidth grdSelectPOLines, kcolPOItemID, 14
    gGridSetColumnWidth grdSelectPOLines, kcolPOQtyView, 11
    gGridSetColumnWidth grdSelectPOLines, kcolPOUOM, 7
    gGridSetColumnWidth grdSelectPOLines, kcolPORcptUOMID, 7
    gGridSetColumnWidth grdSelectPOLines, kcolPOItemDesc, 30
    
    gGridSetHeader grdSelectPOLines, kColPOProjectID, "Project"
    gGridSetHeader grdSelectPOLines, kColPOPhaseID, "Phase"
    gGridSetHeader grdSelectPOLines, kColPOTaskID, "Task"
    
    
    '-- Set the column types
    gGridSetColumnType grdSelectPOLines, kcolPOSelect, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdSelectPOLines, kcolPOPOLineNo, SS_CELL_TYPE_EDIT
    grdSelectPOLines.Col = kcolPOPOLineNo: grdSelectPOLines.Row = -1
    grdSelectPOLines.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
    gGridSetColumnType grdSelectPOLines, kcolPOUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kcolPORcptUOMID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kcolPOItemID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kcolPOItemDesc, SS_CELL_TYPE_EDIT, 40
    
    gGridSetColumnType grdSelectPOLines, kColPOProjectID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColPOPhaseID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColPOTaskID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kcolPOUnitCostExact, SS_CELL_TYPE_FLOAT, 13, 12
    
    '-- Lock the appropriate columns
    For lCol = 2 To kPOGridMaxCols
        gGridLockColumn grdSelectPOLines, lCol
    Next lCol
    
    gGridUnlockColumn grdSelectPOLines, kcolPOQtyView
    
    SetGridNumericAttr grdSelectPOLines, kcolPOQtyView, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kcolPOQty, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kcolPOQtyOrd, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kcolPOQtyRcvd, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kcolPOQtyRtrnForCredit, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kcolPOQtyOpen, miQtyDecPlaces, kMaxAmtLen
    '-- Setup currency columns for locale settings
    SetGridNumericAttr grdSelectPOLines, kcolPOUnitCost, miUnitCostDecPlaces, kMaxCostLen
        
    '-- Hide the appropriate columns
    gGridHideColumn grdSelectPOLines, kcolPOPONo
    gGridHideColumn grdSelectPOLines, kcolPOPOKey
    gGridHideColumn grdSelectPOLines, kcolPOPOLineKey
    gGridHideColumn grdSelectPOLines, kcolPOPOLineDistKey
    gGridHideColumn grdSelectPOLines, kcolPOGLAcctKey
    gGridHideColumn grdSelectPOLines, kcolPOAcctRefKey
    gGridHideColumn grdSelectPOLines, kcolPOItemKey
    gGridHideColumn grdSelectPOLines, kcolPOUnitMeasKey
    gGridHideColumn grdSelectPOLines, kcolPOUnitMeasType
    gGridHideColumn grdSelectPOLines, kcolPOLineClosed
    gGridHideColumn grdSelectPOLines, kcolPOTargetCoID
    gGridHideColumn grdSelectPOLines, kcolPORcptReq
    gGridHideColumn grdSelectPOLines, kcolPOUnitCost
    gGridHideColumn grdSelectPOLines, kcolPOQtyOrd
    gGridHideColumn grdSelectPOLines, kcolPOQtyRcvd
    gGridHideColumn grdSelectPOLines, kcolPOQtyRtrnForCredit
    gGridHideColumn grdSelectPOLines, kcolPOQtyOpen
    gGridHideColumn grdSelectPOLines, kcolPOQty
    gGridHideColumn grdSelectPOLines, kcolPOWhseID
    gGridHideColumn grdSelectPOLines, kcolPODeptID
    gGridHideColumn grdSelectPOLines, kcolPOPromiseDate
    gGridHideColumn grdSelectPOLines, kcolPORcptKey
    gGridHideColumn grdSelectPOLines, kcolPORcptLineKey
    gGridHideColumn grdSelectPOLines, kcolPORcptLineDistKey
    gGridHideColumn grdSelectPOLines, kcolPORcptUOMKey
    gGridHideColumn grdSelectPOLines, kcolPOItemAllowDec
    gGridHideColumn grdSelectPOLines, kcolPOLineComment
    gGridHideColumn grdSelectPOLines, kcolPORcptOrigQty
    gGridHideColumn grdSelectPOLines, kcolPOUnitCostExact
    If mlTranType = kTranTypePORG Then
        gGridHideColumn grdSelectPOLines, kcolPORcptUOMID
    Else
        gGridHideColumn grdSelectPOLines, kcolPOUOM
    End If

    gGridHideColumn grdSelectPOLines, kColPOProjectKey
    gGridHideColumn grdSelectPOLines, kColPOPhaseKey
    gGridHideColumn grdSelectPOLines, kColPOTaskKey
    gGridHideColumn grdSelectPOLines, kColPOJobLineKey
    gGridHideColumn grdSelectPOLines, kColPOJobType
    gGridHideColumn grdSelectPOLines, kColPOTaskType

    '-- Now freeze some columns for looks
    gGridFreezeCols grdSelectPOLines, kcolPOSelect
    gGridFreezeCols grdSelectPOLines, kcolPOPOLineNo
    gGridFreezeCols grdSelectPOLines, kcolPOItemID
        
    '-- Allow the user to resize the columns
    grdSelectPOLines.Col = -1
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_ON
       
    '-- Don't allow the user to resize hidden columns (bug in FarPoint?)
    grdSelectPOLines.Col = kcolPOPONo
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOPOKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOPOLineKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOPOLineDistKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOGLAcctKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOAcctRefKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOItemKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOUnitMeasKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOUnitMeasType
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOLineClosed
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOTargetCoID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPORcptReq
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOQtyOrd
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOQtyRcvd
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOQtyRtrnForCredit
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOQtyOpen
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOQty
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOWhseID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPODeptID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOPromiseDate
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPORcptKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPORcptLineKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPORcptLineDistKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPORcptUOMKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPORcptUOMID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOItemAllowDec
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kcolPOLineComment
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOProjectKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOPhaseKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOTaskKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOJobLineKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOJobType
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOTaskType
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    
    '-- Dont allow negative numbers
    grdSelectPOLines.Row = -1
    grdSelectPOLines.Col = kcolPOQtyView
    grdSelectPOLines.TypeFloatMin = 0
    grdSelectPOLines.TypeFloatMax = 99999999.999
    


    '-- Set the font size on the row header column to be bigger,
    '-- so the "*" is more visible
    grdSelectPOLines.Row = -1: grdSelectPOLines.Col = 0
    grdSelectPOLines.Font.Size = 10
    
    Set oSysSession = Nothing
    Set oAppDB = Nothing
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub LoadPOLinesGrid()
'***************************************************************
' Desc: This routine will load the Select PO Lines grid with all
'       valid purchase order lines.
'***************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rs As Object
    Dim lNumRows As Long
    Dim iDefaultQty As Integer
    Dim sClip As String
    Dim sClipData As String
    Dim lCurrRow As Long
    
    
    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdSelectPOLines, False
    
    If mbDefaultQty Then
        iDefaultQty = 1
    Else
        iDefaultQty = 0
    End If
    
    lNumRows = 0
    grdSelectPOLines.MaxRows = 0
    
    If mlTranType = kTranTypePORG Then
        sSQL = "{call sppoSelectPOLinesRcvr ("
        sSQL = sSQL & gsQuoted(msCompanyID)
        sSQL = sSQL & ", " & mlReceiverKey
        sSQL = sSQL & ", " & gsQuoted(txtPONo)
        sSQL = sSQL & ", " & iDefaultQty
        sSQL = sSQL & ", " & gsQuoted(msPurchCompanyID)
        sSQL = sSQL & ", " & mlPOWhseKey
        sSQL = sSQL & ", " & gsQuoted(msRcptDate)
        sSQL = sSQL & ", " & miDaysEarly
        sSQL = sSQL & ", " & muPODflts.dCurrExchRate
        sSQL = sSQL & ")}"
    Else
        sSQL = "{call sppoSelectRcvrLinesRtrn ("
        sSQL = sSQL & mlReceiverKey
        sSQL = sSQL & ", " & gsQuoted(msCompanyID)
        sSQL = sSQL & ", " & gsQuoted(txtRcvrNo)
        sSQL = sSQL & ", " & iDefaultQty
        sSQL = sSQL & ")}"
    End If
    
    Set rs = moMainForm.oClass.moAppDB.OpenRowset(sSQL, kClipSize, kOptionNone)
    
    sClip = vbCrLf
    
    With rs
        sClipData = .Clip
    
        While sClipData <> ""
            sClip = sClip & vbTab & sClipData
            lNumRows = lNumRows + kClipSize
            .MoveNext
            sClipData = .Clip
        Wend
    End With
    
    gGridSetMaxRows grdSelectPOLines, lNumRows

    grdSelectPOLines.Row = -1
    grdSelectPOLines.Col = -1
    grdSelectPOLines.ClipValue = sClip
    
    For lCurrRow = 1 To lNumRows
        FormatDecimalPlaces (lCurrRow)
    Next
    '-- Turn redraw of the grid back on
    SetGridRedraw grdSelectPOLines, True
    
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadPOLinesGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub UpdateQtyAndAmt()
'*************************************************************
' Desc: This routine will reload the quantity invoiced for all
'       selected lines in the grid. This is done to maintain
'       concurrency.
'*************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sSQL As String
    Dim rs As Object
    Dim dQtyRemain As Double
    Dim dQtyOnReceiver As Double
    Dim dQtyInvc As Double
    Dim dUnitCost As Double
    Dim dNewExtAmt As Double
    Dim lPOLineKey As Long
    Dim dUnitCostExact As Double
    
    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdSelectPOLines, False
    
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        If Val(gsGridReadCell(grdSelectPOLines, lRow, kcolPOSelect)) = vbChecked Then
            If optQtyMethod(0).Value Then
                '-- Receive in Full
                lPOLineKey = gsGridReadCell(grdSelectPOLines, lRow, kcolPOPOLineKey)
                
                sSQL = "SELECT QtyRemain = CASE pol.RcptReq " & _
                            " WHEN 0 THEN ISNULL(pold.QtyOrd,0) - ISNULL(pold.QtyInvcd,0) " & _
                            " WHEN 1 THEN ISNULL(pold.QtyOpenToRcv,0) END, " & _
                            " pol.UnitCost UnitCost ," & _
                            " pol.UnitCostExact UnitCostExact " & _
                        " FROM tpoPurchOrder po WITH (NOLOCK) " & _
                            " JOIN tpoPOLine pol WITH (NOLOCK) ON pol.POKey = po.POKey" & _
                            " JOIN  tpoPOLineDist pold WITH (NOLOCK) ON pold.POLineKey = pol.POLineKey" & _
                        " WHERE po.CompanyID = " & gsQuoted(msCompanyID) & _
                            " AND po.TranNoRel = " & gsQuoted(txtPONo) & _
                            " AND pol.POLineKey = " & lPOLineKey
            
                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
                If Not rs.IsEOF Then
                    dQtyRemain = IIf(gdGetValidDbl(rs.Field("QtyRemain")) > 0, gdGetValidDbl(rs.Field("QtyRemain")), 0)
                    dUnitCost = gdGetValidDbl(rs.Field("UnitCost"))
                    dUnitCostExact = gdGetValidDbl(rs.Field("UnitCostExact"))
                    rs.Close
                End If
                
                sSQL = "SELECT ROUND(SUM(COALESCE(dbo.fnIMItemUOMConv(vd.ItemRcvdKey, vld.QtyRcvd, vd.UnitMeasKey, pol.UnitMeasKey), 1)), " & miQtyDecPlaces & ") Quantity " & _
                        " FROM tpoPendReceiver pv WITH (NOLOCK) " & _
                            " JOIN  tpoRcvrLine vd WITH (NOLOCK) ON vd.RcvrKey = pv.RcvrKey" & _
                            " JOIN  tpoRcvrLineDist vld WITH (NOLOCK) ON  vld.RcvrLineKey = vd.RcvrLineKey" & _
                            " JOIN  tpoPurchOrder po WITH (NOLOCK) ON pv.POKey = po.POKey" & _
                            " JOIN  tpoPOLine pol WITH (NOLOCK) ON pol.POKey = po.POKey" & _
                                                              " AND vd.POLineKey = pol.POLineKey" & _
                         " WHERE pv.CompanyID = " & gsQuoted(msCompanyID) & _
                         " AND pv.RcvrKey <> " & mlReceiverKey & _
                         " AND po.TranNoRel = " & gsQuoted(txtPONo) & _
                         " AND pol.POLineKey = " & lPOLineKey & _
                         " GROUP BY vd.POLineKey"
                
                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
                If Not rs.IsEOF Then
                    dQtyOnReceiver = gdGetValidDbl(rs.Field("Quantity"))
                    rs.Close
                Else
                    dQtyOnReceiver = 0
                End If
                
                dQtyInvc = dQtyRemain - dQtyOnReceiver
                If (dQtyInvc < 0) Then dQtyInvc = 0
                dNewExtAmt = dQtyInvc * dUnitCostExact
            Else
                '-- Invoice Manually
                dQtyInvc = 0
                dNewExtAmt = 0
            End If
            
'            gGridUpdateCell grdSelectPOLines, lRow, kcolPOQtyRcvd, CStr(dQtyInvc)
        End If
    Next lRow
    
    '-- Turn redraw of the grid back on
    SetGridRedraw grdSelectPOLines, True
    
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UpdateQtyAndAmt", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Private Function bQtyValid(lCurRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim dMaxQtyToRcv As Double
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    
    bQtyValid = False
    dMaxQtyToRcv = gdGetValidDbl(gsGridReadCell(grdSelectPOLines, lCurRow, kcolPOQty))
    If dMaxQtyToRcv < gdGetValidDbl(gsGridReadCell(grdSelectPOLines, lCurRow, kcolPOQtyView)) Then
        If mlTranType = kTranTypePORG Then
            sID = "RCVQTYOVER"
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            bQtyValid = frmEnterRcptOfGoods.bRemoteSecurEvent(sID, sUser, vPrompt)
        Else
' MVB           giSotaMsgBox Nothing, moClass.moSysSession, kmsgCannotRtrnMoreThanRcvd
            giSotaMsgBox Me, moClass.moSysSession, 220101
        End If
    Else
        bQtyValid = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bQtyValid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++


End Function
Private Sub grdSelectPOLines_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case Col
        Case kcolPOQtyView
            If bQtyValid(Row) Then
                mdQtyRcvd = gdGetValidDbl(gsGridReadCell(grdSelectPOLines, Row, kcolPOQtyView))
            Else
                gGridUpdateCell grdSelectPOLines, Row, kcolPOQtyView, CStr(mdQtyRcvd)
            End If
        
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSelectPOLines_EditMode", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
        
End Sub

Private Sub FormatDecimalPlaces(Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
Dim lPassAmt As Double
'+++ VB/Rig End +++
'    if the item does not allow decimal places, set the column type to integer.

    If gsGridReadCell(grdSelectPOLines, Row, kcolPOItemAllowDec) = "0" Then
        lPassAmt = glGetValidLong(gsGridReadCell(grdSelectPOLines, Row, kcolPOQtyView))
        gGridSetCellType grdSelectPOLines, Row, kcolPOQtyView, SS_CELL_TYPE_INTEGER
        gGridUpdateCell grdSelectPOLines, Row, kcolPOQtyView, CStr(lPassAmt)
        grdSelectPOLines.Row = Row
        grdSelectPOLines.Col = kcolPOQtyView
        grdSelectPOLines.TypeIntegerMin = 0
        grdSelectPOLines.TypeIntegerMax = 99999999
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatDecimalPlaces", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub grdSelectPOLines_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
' Get a snapshot of the value in the column prior to changing so that is can be
' set back to its original value if there is an error
    If Mode = 1 Then
        Select Case Col
            Case kcolPOQtyView
                mdQtyRcvd = gdGetValidDbl(gsGridReadCell(grdSelectPOLines, Row, kcolPOQtyView))
        End Select
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSelectPOLines_EditMode", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub grdSelectPOLines_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'    If NewCol = kcolPOQtyView Then
'        FormatDecimalPlaces (NewRow)
'    End If
End Sub

Private Sub optQtyMethod_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick optQtyMethod(Index), True
    #End If
'+++ End Customizer Code Push +++
    
    If optQtyMethod(0) = True Then
        mbDefaultQty = True
    Else
        mbDefaultQty = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optQtyMethod_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ViewClosedLines(bFlag As Boolean)
'****************************************************************
' Desc: This routine will show/hide the closed lines in the grid.
'****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        If (Val(gsGridReadCell(grdSelectPOLines, lRow, kcolPOLineClosed)) = kiLineClosed) Then
            '-- The line is Closed, so show or hide based on flag
            grdSelectPOLines.Row = lRow
            grdSelectPOLines.RowHidden = Not bFlag
        End If
    Next lRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ViewClosedLines", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ShowClosedLinesIndicator()
'***************************************************************
' Desc: This routine will display the closed lines indicator for
'       each line that is closed.
'***************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        If (Val(gsGridReadCell(grdSelectPOLines, lRow, kcolPOLineClosed)) = kiLineClosed) Then
            '-- The line is Closed, so show "*" in the row header
            gGridUpdateCellText grdSelectPOLines, lRow, 0, "*"
        Else
            '-- The line is Open, so blank out the row header
            gGridUpdateCellText grdSelectPOLines, lRow, 0, " "
        End If
    Next lRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ToggleClosedLines", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Instantiate Context Menu Class
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        'Add any Special .Bind or .BindGrid commands here for Drill Around/Drill Down
        'or for Grids - Here

        'Assign the Winhook control to the Context Menu Class
        ' **PRESTO ** Set .Hook = Me.WinHook1
        Set .Form = frmSelectPOLines
    
        'Init will set properties of Winhook to intercept WM_RBUTTONDOWN
        .Init
    End With
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupBars()
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Setup the toolbar
    With tbrMain
        .Style = sotaTB_PROCESS
        .RemoveButton kTbProceed
        .LocaleID = moClass.moSysSession.Language
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub SetupFormForReturns()
'****************************************************************
' Description:
'    This routine will setup the form to show the fields appropriate
'     for returns.
'****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    txtRcvrNo.Visible = True
    txtRcvrNo.Top = 600
    lblRcvrNo.Visible = True
    lblRcvrNo.Top = 600
    lblPONo.Top = 900
    txtPONo.Top = 900
    grdSelectPOLines.Height = 3795
    grdSelectPOLines.Top = 1140
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupFormForReturns", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
    
    ' VB 5 does not automatically fire LostFocus event when pressing toolbar
    Me.SetFocus
    
    DoEvents
    
    Select Case sKey
        Case kTbClose
            '-- Make sure quantities in grid are up-to-date
            UpdateQtyAndAmt
            mbProcessOnExit = True
            '-- This form will be unloaded from the Receiver form,
            '-- after the lines have been loaded in the detail grid
            If mbShowUI Then
                Me.Hide
            End If
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = Nothing
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub cmdSelectClearAll_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelectClearAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectClearAll_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelectClearAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkViewClosedLines_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkViewClosedLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkViewClosedLines_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkViewClosedLines_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkViewClosedLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkViewClosedLines_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optQtyMethod_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optQtyMethod(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optQtyMethod_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optQtyMethod_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optQtyMethod(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optQtyMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optQtyMethod_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optQtyMethod(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optQtyMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If













Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property







