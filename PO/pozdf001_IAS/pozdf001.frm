VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Begin VB.Form frmEnterRcptOfGoods 
   Caption         =   "Enter Receipt of Goods"
   ClientHeight    =   6795
   ClientLeft      =   570
   ClientTop       =   1035
   ClientWidth     =   10785
   HelpContextID   =   95661
   Icon            =   "pozdf001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6795
   ScaleWidth      =   10785
   Begin FPSpreadADO.fpSpread grdJobLine 
      Height          =   735
      Left            =   -75000
      TabIndex        =   107
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   95776
      Width           =   1455
      _Version        =   524288
      _ExtentX        =   2566
      _ExtentY        =   1296
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "pozdf001.frx":23D2
      AppearanceStyle =   0
   End
   Begin SOTADropDownControl.SOTADropDown ddnType 
      Height          =   315
      Left            =   900
      TabIndex        =   1
      Top             =   600
      WhatsThisHelpID =   95774
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Text            =   "ddnType"
   End
   Begin LookupViewControl.LookupView navPO 
      Height          =   285
      Left            =   10110
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   945
      WhatsThisHelpID =   95773
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupMode      =   1
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtRelNo 
      Height          =   285
      Left            =   9510
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   930
      WhatsThisHelpID =   95772
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   4
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtPONum 
      Height          =   285
      Left            =   8310
      TabIndex        =   10
      Top             =   945
      WhatsThisHelpID =   95771
      Width           =   1125
      _Version        =   65536
      _ExtentX        =   1984
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   10
   End
   Begin SOTACalendarControl.SOTACalendar calRcptDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   8280
      TabIndex        =   7
      Top             =   570
      WhatsThisHelpID =   95770
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTADropDownControl.SOTADropDown ddnPurchCompany 
      Height          =   315
      Left            =   6210
      TabIndex        =   6
      Top             =   570
      WhatsThisHelpID =   95769
      Width           =   885
      _ExtentX        =   1561
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Style           =   2
      Text            =   "ddnPurchCompany"
   End
   Begin TabDlg.SSTab tabReceipt 
      Height          =   5025
      Left            =   0
      TabIndex        =   38
      Top             =   1320
      WhatsThisHelpID =   95768
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   8864
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "&Header"
      TabPicture(0)   =   "pozdf001.frx":2810
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdAdditionalNotes1"
      Tab(0).Control(1)=   "SSPanel1"
      Tab(0).Control(2)=   "cmdVendPerf"
      Tab(0).Control(3)=   "cmdPOLandedCost"
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "&Lines"
      TabPicture(1)   =   "pozdf001.frx":282C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "shpFocusRect"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "grdRcptDetl"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Frame1"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "grdRcvrLineDist"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.CommandButton cmdPOLandedCost 
         Caption         =   "&Landed Cost..."
         Height          =   375
         Left            =   -66720
         TabIndex        =   22
         Top             =   1590
         WhatsThisHelpID =   17770915
         Width           =   1905
      End
      Begin VB.CommandButton cmdVendPerf 
         Caption         =   "V&endor Performance..."
         Height          =   375
         Left            =   -66690
         TabIndex        =   21
         Top             =   1080
         WhatsThisHelpID =   17770915
         Width           =   1905
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   3765
         Left            =   -74820
         TabIndex        =   104
         Top             =   645
         Width           =   7995
         _Version        =   65536
         _ExtentX        =   14102
         _ExtentY        =   6641
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraPutAwayAllGoodsTo 
            Caption         =   "Put Away All Goods To"
            Height          =   855
            Left            =   120
            TabIndex        =   116
            Top             =   1320
            Width           =   3855
            Begin EntryLookupControls.TextLookup lkuReceiveToBin 
               Height          =   285
               Left            =   120
               TabIndex        =   103
               Top             =   360
               WhatsThisHelpID =   17772774
               Width           =   1935
               _ExtentX        =   3413
               _ExtentY        =   503
               ForeColor       =   -2147483640
               Enabled         =   0   'False
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Bin"
               ParentIDColumn  =   "WhseBinID"
               ParentKeyColumn =   "WhseBinKey"
               ParentTable     =   "timWhseBin"
               BoundColumn     =   "WhseBinKey"
               BoundTable      =   "timWhseBin"
               sSQLReturnCols  =   "WhseBinID,,;WhseBinKey,,;"
            End
            Begin VB.CheckBox chkPreferredBin 
               Caption         =   "&Preferred Bins"
               Height          =   255
               Left            =   2280
               TabIndex        =   117
               Top             =   360
               Value           =   1  'Checked
               WhatsThisHelpID =   17772775
               Width           =   1455
            End
         End
         Begin VB.TextBox txtBOL 
            Height          =   285
            Left            =   1485
            MaxLength       =   15
            TabIndex        =   15
            Top             =   105
            WhatsThisHelpID =   95766
            Width           =   1875
         End
         Begin VB.TextBox txtTranComment 
            Height          =   285
            Left            =   1485
            MaxLength       =   50
            TabIndex        =   17
            Top             =   495
            WhatsThisHelpID =   95765
            Width           =   5295
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtRMA 
            Height          =   285
            Left            =   1485
            TabIndex        =   19
            Top             =   885
            Visible         =   0   'False
            WhatsThisHelpID =   95764
            Width           =   1875
            _Version        =   65536
            _ExtentX        =   3307
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            lMaxLength      =   20
         End
         Begin VB.Label lblBOL 
            Caption         =   "Bill o&f Lading"
            Height          =   225
            Left            =   165
            TabIndex        =   14
            Top             =   135
            Width           =   1245
         End
         Begin VB.Label lblComment1 
            Caption         =   "&Comment"
            Height          =   225
            Left            =   165
            TabIndex        =   16
            Top             =   525
            Width           =   1245
         End
         Begin VB.Label lblRMA 
            Caption         =   "RM&A"
            Height          =   225
            Left            =   165
            TabIndex        =   18
            Top             =   915
            Visible         =   0   'False
            Width           =   555
         End
      End
      Begin FPSpreadADO.fpSpread grdRcvrLineDist 
         Height          =   2805
         Left            =   240
         TabIndex        =   93
         Top             =   5040
         Visible         =   0   'False
         WhatsThisHelpID =   95760
         Width           =   10440
         _Version        =   524288
         _ExtentX        =   18415
         _ExtentY        =   4948
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "pozdf001.frx":2848
         AppearanceStyle =   0
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   2355
         Left            =   60
         TabIndex        =   84
         Top             =   420
         Width           =   10635
         Begin VB.CommandButton cmdDist 
            Caption         =   "&Dist..."
            Height          =   315
            Left            =   9100
            TabIndex        =   31
            Top             =   735
            WhatsThisHelpID =   95758
            Width           =   1305
         End
         Begin VB.CommandButton cmdUndo 
            Caption         =   "U&ndo"
            Height          =   315
            Left            =   9100
            TabIndex        =   30
            Top             =   375
            WhatsThisHelpID =   100207
            Width           =   1305
         End
         Begin VB.CommandButton cmdOK 
            Caption         =   "O&K"
            Height          =   315
            Left            =   9100
            TabIndex        =   29
            Top             =   15
            WhatsThisHelpID =   100208
            Width           =   1305
         End
         Begin NEWSOTALib.SOTANumber nbrQtyRcvd 
            Height          =   285
            Left            =   120
            TabIndex        =   25
            Top             =   750
            WhatsThisHelpID =   95755
            Width           =   1710
            _Version        =   65536
            _ExtentX        =   3016
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            mask            =   "<ILH>##|,###|,##<ILp0>#<IRp0>|.###"
            text            =   "56789012.123"
            sIntegralPlaces =   8
            sDecimalPlaces  =   3
            dValue          =   56789012.123
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtItemDescription 
            Height          =   285
            Left            =   3480
            TabIndex        =   85
            Top             =   240
            WhatsThisHelpID =   95754
            Width           =   3120
            _Version        =   65536
            _ExtentX        =   5503
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
            sBorder         =   0
            text            =   "Item Description ============="
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtPOLine 
            Height          =   285
            Left            =   135
            TabIndex        =   86
            Top             =   225
            WhatsThisHelpID =   95753
            Width           =   1005
            _Version        =   65536
            bPadLeft        =   -1  'True
            _ExtentX        =   1773
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin TabDlg.SSTab tabSubDetl 
            Height          =   1155
            Left            =   60
            TabIndex        =   54
            Top             =   1200
            WhatsThisHelpID =   95752
            Width           =   10500
            _ExtentX        =   18521
            _ExtentY        =   2037
            _Version        =   393216
            TabOrientation  =   1
            Style           =   1
            Tabs            =   6
            TabsPerRow      =   8
            TabHeight       =   520
            TabCaption(0)   =   "&Project"
            TabPicture(0)   =   "pozdf001.frx":2CAD
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "lblProject"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).Control(1)=   "lblPhase"
            Tab(0).Control(1).Enabled=   0   'False
            Tab(0).Control(2)=   "lblTask"
            Tab(0).Control(2).Enabled=   0   'False
            Tab(0).Control(3)=   "lkuProject"
            Tab(0).Control(3).Enabled=   0   'False
            Tab(0).Control(4)=   "lkuPhase"
            Tab(0).Control(4).Enabled=   0   'False
            Tab(0).Control(5)=   "lkuTask"
            Tab(0).Control(5).Enabled=   0   'False
            Tab(0).ControlCount=   6
            TabCaption(1)   =   "&Original Order"
            TabPicture(1)   =   "pozdf001.frx":2CC9
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "lblQtyShip"
            Tab(1).Control(1)=   "lblUOM(1)"
            Tab(1).Control(2)=   "lblTotRcvd"
            Tab(1).Control(3)=   "lblQtyReturn"
            Tab(1).Control(4)=   "lblQtyOpen"
            Tab(1).Control(5)=   "lblUOMRcpt"
            Tab(1).Control(6)=   "lblPOUOM"
            Tab(1).Control(7)=   "lblTotalReturn"
            Tab(1).Control(8)=   "lblQtyOrd"
            Tab(1).Control(9)=   "txtUOMRcpt"
            Tab(1).Control(10)=   "nbrQtyShip"
            Tab(1).Control(11)=   "nbrQtyReturn"
            Tab(1).Control(12)=   "nbrQtyOpen"
            Tab(1).Control(13)=   "nbrTotRcvd"
            Tab(1).Control(14)=   "nbrQtyOrd"
            Tab(1).Control(15)=   "txtUOM"
            Tab(1).ControlCount=   16
            TabCaption(2)   =   "Commen&ts"
            TabPicture(2)   =   "pozdf001.frx":2CE5
            Tab(2).ControlEnabled=   0   'False
            Tab(2).Control(0)=   "lblLnComment"
            Tab(2).Control(1)=   "txtLnComment"
            Tab(2).Control(2)=   "cmdCommentDtl"
            Tab(2).ControlCount=   3
            TabCaption(3)   =   "&Misc"
            TabPicture(3)   =   "pozdf001.frx":2D01
            Tab(3).ControlEnabled=   0   'False
            Tab(3).Control(0)=   "chkCloseDistr"
            Tab(3).Control(1)=   "chkMatch"
            Tab(3).Control(2)=   "cmdWghtVol"
            Tab(3).Control(3)=   "optRetCredit"
            Tab(3).Control(4)=   "optRetReplace"
            Tab(3).ControlCount=   5
            TabCaption(4)   =   "&Bin Dist"
            TabPicture(4)   =   "pozdf001.frx":2D1D
            Tab(4).ControlEnabled=   0   'False
            Tab(4).Control(0)=   "fraPutAwayAllGoodsToDetl"
            Tab(4).ControlCount=   1
            TabCaption(5)   =   "Rail Car"
            TabPicture(5)   =   "pozdf001.frx":2D39
            Tab(5).ControlEnabled=   0   'False
            Tab(5).Control(0)=   "SSPanel2"
            Tab(5).Control(0).Enabled=   0   'False
            Tab(5).ControlCount=   1
            Begin VB.Frame fraPutAwayAllGoodsToDetl 
               BorderStyle     =   0  'None
               Caption         =   "Bin Distribution"
               Height          =   615
               Left            =   -74880
               TabIndex        =   114
               Top             =   120
               Width           =   10215
               Begin VB.CheckBox chkPreferredBinDetl 
                  Caption         =   "&Preferred Bins"
                  Height          =   255
                  Left            =   2520
                  TabIndex        =   115
                  Top             =   180
                  WhatsThisHelpID =   17772779
                  Width           =   1575
               End
               Begin EntryLookupControls.TextLookup lkuAutoDistBin 
                  Height          =   285
                  Left            =   500
                  TabIndex        =   119
                  Top             =   180
                  WhatsThisHelpID =   17777171
                  Width           =   1785
                  _ExtentX        =   3149
                  _ExtentY        =   503
                  ForeColor       =   -2147483640
                  IsSurrogateKey  =   -1  'True
                  LookupID        =   "DistBin"
                  ParentIDColumn  =   "WhseBinID"
                  ParentKeyColumn =   "WhseBinKey"
                  ParentTable     =   "timWhseBin"
                  BoundColumn     =   "WhseBinKey"
                  BoundTable      =   "timWhseBin"
                  sSQLReturnCols  =   "Bin,lkuAutoDistBin,;WhseBinKey,,;OnHand,,;Available,,;UOMKey,,;"
               End
               Begin VB.TextBox txtMultipleBin 
                  Height          =   285
                  Left            =   500
                  Locked          =   -1  'True
                  TabIndex        =   118
                  Text            =   "Multiple Bin"
                  Top             =   180
                  WhatsThisHelpID =   17777172
                  Width           =   1450
               End
               Begin VB.Label lblAutDistBin 
                  Caption         =   "Bin"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   120
                  Top             =   180
                  Width           =   300
               End
            End
            Begin VB.OptionButton optRetReplace 
               Caption         =   "Return for Replacement"
               Height          =   285
               Left            =   -74970
               TabIndex        =   113
               Top             =   2075
               WhatsThisHelpID =   95751
               Width           =   2025
            End
            Begin VB.OptionButton optRetCredit 
               Caption         =   "Return for Credit"
               Height          =   285
               Left            =   -74970
               TabIndex        =   112
               Top             =   2180
               Value           =   -1  'True
               WhatsThisHelpID =   95750
               Width           =   2025
            End
            Begin EntryLookupControls.TextLookup lkuTask 
               Height          =   285
               Left            =   3720
               TabIndex        =   37
               TabStop         =   0   'False
               Top             =   120
               WhatsThisHelpID =   95749
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   503
               ForeColor       =   -2147483640
               EnabledLookup   =   0   'False
               EnabledText     =   0   'False
               VisibleLookup   =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "TaskNum_FAN "
               ParentIDColumn  =   "chrTaskNumber"
               ParentKeyColumn =   "intTaskKey"
               ParentTable     =   "tPA00005"
               BoundColumn     =   "intTaskKey"
               BoundTable      =   "papoLine"
               IsForeignKey    =   -1  'True
               Datatype        =   0
               sSQLReturnCols  =   "chrTaskNumber,,;chrTaskName,,;"
            End
            Begin EntryLookupControls.TextLookup lkuPhase 
               Height          =   285
               Left            =   795
               TabIndex        =   35
               Top             =   480
               WhatsThisHelpID =   95748
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   503
               ForeColor       =   -2147483640
               EnabledText     =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "PhaseNum_FAN"
               ParentIDColumn  =   "chrPhaseNumber"
               ParentKeyColumn =   "intPhaseKey"
               ParentTable     =   "tPA00002"
               BoundColumn     =   "intPhaseKey"
               BoundTable      =   "papoLine"
               IsForeignKey    =   -1  'True
               Datatype        =   0
               sSQLReturnCols  =   "chrPhaseNumber,,;chrPhaseName,,;"
            End
            Begin EntryLookupControls.TextLookup lkuProject 
               Height          =   285
               Left            =   795
               TabIndex        =   33
               Top             =   120
               WhatsThisHelpID =   95747
               Width           =   2115
               _ExtentX        =   3731
               _ExtentY        =   503
               ForeColor       =   -2147483640
               EnabledText     =   0   'False
               Protected       =   -1  'True
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Project"
               ParentIDColumn  =   "chrJobNumber"
               ParentKeyColumn =   "intJobKey"
               ParentTable     =   "tPA00175"
               BoundColumn     =   "intJobKey"
               BoundTable      =   "papoLine"
               IsForeignKey    =   -1  'True
               Datatype        =   0
               sSQLReturnCols  =   "chrJobNumber,lkuProject,;chrJobName,,;"
            End
            Begin VB.CommandButton cmdCommentDtl 
               Caption         =   "Addit&ional Notes..."
               Height          =   315
               Left            =   -66225
               TabIndex        =   52
               Top             =   120
               WhatsThisHelpID =   95746
               Width           =   1605
            End
            Begin VB.TextBox txtLnComment 
               Height          =   495
               Left            =   -74100
               MaxLength       =   50
               ScrollBars      =   2  'Vertical
               TabIndex        =   51
               Top             =   150
               WhatsThisHelpID =   95745
               Width           =   4755
            End
            Begin VB.CommandButton cmdWghtVol 
               Caption         =   "&Wght / Vol..."
               Height          =   315
               Left            =   -65965
               TabIndex        =   57
               Top             =   120
               WhatsThisHelpID =   95744
               Width           =   1305
            End
            Begin VB.CheckBox chkMatch 
               Caption         =   "Match"
               Height          =   195
               Left            =   -74760
               TabIndex        =   55
               Top             =   180
               Value           =   1  'Checked
               WhatsThisHelpID =   95743
               Width           =   945
            End
            Begin VB.CheckBox chkCloseDistr 
               Caption         =   "Close PO Line for Receiving"
               Height          =   195
               Left            =   -74760
               TabIndex        =   56
               Top             =   480
               WhatsThisHelpID =   95742
               Width           =   2805
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtUOM 
               Height          =   285
               Left            =   -73080
               TabIndex        =   42
               Top             =   330
               WhatsThisHelpID =   95741
               Width           =   765
               _Version        =   65536
               _ExtentX        =   1349
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               lMaxLength      =   15
               text            =   "XXXXXX"
            End
            Begin NEWSOTALib.SOTANumber nbrQtyOrd 
               Height          =   285
               Left            =   -74880
               TabIndex        =   40
               Top             =   330
               WhatsThisHelpID =   95740
               Width           =   1710
               _Version        =   65536
               _ExtentX        =   3016
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrTotRcvd 
               Height          =   285
               Left            =   -72270
               TabIndex        =   44
               Top             =   330
               WhatsThisHelpID =   95739
               Width           =   1710
               _Version        =   65536
               _ExtentX        =   3016
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrQtyOpen 
               Height          =   285
               Left            =   -68880
               TabIndex        =   49
               Top             =   330
               WhatsThisHelpID =   95738
               Width           =   1530
               _Version        =   65536
               _ExtentX        =   2699
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrQtyReturn 
               Height          =   285
               Left            =   -70500
               TabIndex        =   47
               Top             =   330
               WhatsThisHelpID =   95737
               Width           =   1530
               _Version        =   65536
               _ExtentX        =   2699
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTANumber nbrQtyShip 
               Height          =   285
               Left            =   -70500
               TabIndex        =   106
               Top             =   330
               Visible         =   0   'False
               WhatsThisHelpID =   95736
               Width           =   1530
               _Version        =   65536
               _ExtentX        =   2699
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.###"
               text            =   "           0.000"
               dMaxValue       =   999999999999999
               sDecimalPlaces  =   3
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtUOMRcpt 
               Height          =   285
               Left            =   -73020
               TabIndex        =   108
               Top             =   2285
               Visible         =   0   'False
               WhatsThisHelpID =   95735
               Width           =   1005
               _Version        =   65536
               _ExtentX        =   1773
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
            End
            Begin Threed.SSPanel SSPanel2 
               Height          =   675
               Left            =   -74880
               TabIndex        =   121
               Top             =   120
               Width           =   10275
               _Version        =   65536
               _ExtentX        =   18124
               _ExtentY        =   1191
               _StockProps     =   15
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BevelOuter      =   0
               Begin VB.CommandButton cmd_RailDist 
                  Caption         =   "Rail Car Distribution"
                  Height          =   420
                  Left            =   3840
                  TabIndex        =   122
                  Top             =   0
                  Width           =   1815
               End
            End
            Begin VB.Label lblQtyOrd 
               AutoSize        =   -1  'True
               Caption         =   "Qty Ordered"
               Height          =   195
               Left            =   -74880
               TabIndex        =   39
               Top             =   120
               Width           =   855
            End
            Begin VB.Label lblTotalReturn 
               Caption         =   "Total Returned"
               Height          =   195
               Left            =   -71940
               TabIndex        =   111
               Top             =   2075
               Width           =   1125
            End
            Begin VB.Label lblPOUOM 
               Caption         =   "PO UOM"
               Height          =   195
               Left            =   -68370
               TabIndex        =   110
               Top             =   2075
               Width           =   705
            End
            Begin VB.Label lblUOMRcpt 
               Caption         =   "Receipt UOM"
               Height          =   195
               Left            =   -73020
               TabIndex        =   109
               Top             =   2075
               Visible         =   0   'False
               Width           =   1005
            End
            Begin VB.Label lblTask 
               AutoSize        =   -1  'True
               Caption         =   "Task"
               Height          =   195
               Left            =   3150
               TabIndex        =   36
               Top             =   120
               Width           =   360
            End
            Begin VB.Label lblPhase 
               AutoSize        =   -1  'True
               Caption         =   "Phase"
               Height          =   195
               Left            =   120
               TabIndex        =   34
               Top             =   480
               Width           =   450
            End
            Begin VB.Label lblProject 
               AutoSize        =   -1  'True
               Caption         =   "Project"
               Height          =   195
               Left            =   120
               TabIndex        =   32
               Top             =   120
               Width           =   495
            End
            Begin VB.Label lblQtyOpen 
               AutoSize        =   -1  'True
               Caption         =   "Qty Open"
               Height          =   195
               Left            =   -68910
               TabIndex        =   48
               Top             =   120
               Width           =   675
            End
            Begin VB.Label lblQtyReturn 
               AutoSize        =   -1  'True
               Caption         =   "Qty Returned"
               Height          =   195
               Left            =   -70500
               TabIndex        =   45
               Top             =   120
               Width           =   945
            End
            Begin VB.Label lblTotRcvd 
               AutoSize        =   -1  'True
               Caption         =   "Total Received"
               Height          =   195
               Left            =   -72300
               TabIndex        =   43
               Top             =   120
               Width           =   1095
            End
            Begin VB.Label lblUOM 
               AutoSize        =   -1  'True
               Caption         =   "UOM"
               Height          =   195
               Index           =   1
               Left            =   -73110
               TabIndex        =   41
               Top             =   120
               Width           =   405
            End
            Begin VB.Label lblLnComment 
               AutoSize        =   -1  'True
               Caption         =   "Comment"
               Height          =   195
               Left            =   -74880
               TabIndex        =   105
               Top             =   150
               Width           =   660
            End
            Begin VB.Label lblQtyShip 
               AutoSize        =   -1  'True
               Caption         =   "Total Shipped"
               Height          =   195
               Left            =   -70500
               TabIndex        =   46
               Top             =   120
               Visible         =   0   'False
               Width           =   990
            End
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtDept 
            Height          =   285
            Left            =   4920
            TabIndex        =   87
            Top             =   750
            WhatsThisHelpID =   95721
            Width           =   1710
            _Version        =   65536
            _ExtentX        =   3016
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtWhse 
            Height          =   285
            Left            =   3090
            TabIndex        =   88
            Top             =   750
            WhatsThisHelpID =   95720
            Width           =   1710
            _Version        =   65536
            _ExtentX        =   3016
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin SOTADropDownControl.SOTADropDown ddnUOM 
            Height          =   315
            Left            =   1920
            TabIndex        =   27
            Top             =   750
            WhatsThisHelpID =   95719
            Width           =   1125
            _ExtentX        =   1984
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Style           =   2
            Text            =   "ddnUOM"
            AllowClear      =   0   'False
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtItemOrdID 
            Height          =   285
            Left            =   1380
            TabIndex        =   94
            Top             =   210
            WhatsThisHelpID =   95718
            Width           =   2055
            _Version        =   65536
            _ExtentX        =   3625
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin SOTADropDownControl.SOTADropDown ddnReason 
            Height          =   315
            Left            =   6760
            TabIndex        =   66
            Top             =   2210
            WhatsThisHelpID =   95717
            Width           =   1785
            _ExtentX        =   3149
            _ExtentY        =   556
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Style           =   2
            Text            =   "ddnReason"
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtRtrnUOM 
            Height          =   285
            Left            =   3750
            TabIndex        =   98
            Top             =   2750
            Visible         =   0   'False
            WhatsThisHelpID =   95716
            Width           =   1065
            _Version        =   65536
            _ExtentX        =   1879
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
         End
         Begin VB.CommandButton cmdVendPerfDtl 
            Caption         =   "Vendor Pe&rformance..."
            Height          =   315
            Left            =   6960
            TabIndex        =   28
            Top             =   735
            WhatsThisHelpID =   17770916
            Width           =   1935
         End
         Begin VB.Label lblQuantityReturned 
            AutoSize        =   -1  'True
            Caption         =   "&Qty Returned"
            Height          =   195
            Left            =   120
            TabIndex        =   65
            Top             =   2540
            Visible         =   0   'False
            Width           =   945
         End
         Begin VB.Label lblReason 
            Caption         =   "Reason"
            Height          =   225
            Left            =   6760
            TabIndex        =   97
            Top             =   2510
            Visible         =   0   'False
            Width           =   915
         End
         Begin VB.Label lblDept 
            Caption         =   "Department"
            Height          =   255
            Left            =   4920
            TabIndex        =   92
            Top             =   510
            Width           =   1245
         End
         Begin VB.Label lblWhse 
            Caption         =   "Warehouse"
            Height          =   255
            Left            =   3120
            TabIndex        =   91
            Top             =   510
            Width           =   1245
         End
         Begin VB.Label lblQtyRcvd 
            AutoSize        =   -1  'True
            Caption         =   "&Qty Received"
            Height          =   195
            Left            =   120
            TabIndex        =   24
            Top             =   540
            Width           =   975
         End
         Begin VB.Label lblUOM 
            AutoSize        =   -1  'True
            Caption         =   "&UOM"
            Height          =   195
            Index           =   0
            Left            =   1890
            TabIndex        =   26
            Top             =   510
            Width           =   375
         End
         Begin VB.Label lblItemID 
            AutoSize        =   -1  'True
            Caption         =   "Item"
            Height          =   195
            Left            =   1380
            TabIndex        =   90
            Top             =   0
            Width           =   300
         End
         Begin VB.Label lblPOLine 
            AutoSize        =   -1  'True
            Caption         =   "PO Line"
            Height          =   195
            Left            =   135
            TabIndex        =   89
            Top             =   15
            Width           =   570
         End
      End
      Begin VB.CommandButton cmdAdditionalNotes1 
         Caption         =   "Additi&onal Notes..."
         Height          =   375
         Left            =   -66690
         TabIndex        =   20
         Top             =   570
         WhatsThisHelpID =   95707
         Width           =   1905
      End
      Begin FPSpreadADO.fpSpread grdRcptDetl 
         Height          =   2025
         Left            =   180
         TabIndex        =   83
         Top             =   2880
         WhatsThisHelpID =   95706
         Width           =   10470
         _Version        =   524288
         _ExtentX        =   18468
         _ExtentY        =   3572
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "pozdf001.frx":2D55
         AppearanceStyle =   0
      End
      Begin VB.Shape shpFocusRect 
         Height          =   315
         Left            =   120
         Top             =   960
         Visible         =   0   'False
         Width           =   585
      End
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   82
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.TextBox txtClassOvrdSegValue 
      Height          =   285
      Left            =   -10000
      MaxLength       =   50
      MultiLine       =   -1  'True
      TabIndex        =   79
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   95703
      Width           =   1800
   End
   Begin SOTAVM.SOTAValidationMgr SOTAValidationMgr1 
      Left            =   5910
      Top             =   30
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   173
      Ctl1            =   "txt1099Type;-1;CustomLabel;0"
      Ctl2            =   "txtClassOvrdSegValue;-1;CustomLabel;0"
      Ctl3            =   "SOTAValidationMgr1;-1;;-1"
      Ctl4            =   "cmdLines;-1;;-1"
      Ctl5            =   "SOTAOffice1;-1;;-1"
      Ctl6            =   "pnlTab;0;;-1"
      Ctl7            =   "fraVendorInfo;-1;;-1"
      Ctl8            =   "cmdRemitToAddr;-1;;-1"
      Ctl9            =   "cmdPurchFromAddr;-1;;-1"
      Ctl10           =   "cmd1099;-1;;-1"
      Ctl11           =   "txtRemitToAddrName;-1;lblRemitToAddrID;-1"
      Ctl12           =   "lkuRemitToAddrID;-1;lblRemitToAddrID;-1"
      Ctl13           =   "txtPurchFromAddrName;-1;lblPurchFromAddrID;-1"
      Ctl14           =   "lkuPurchFromAddrID;-1;lblPurchFromAddrID;-1"
      Ctl15           =   "lkuContactName;-1;lblContactName;-1"
      Ctl16           =   "lkuVendClassID;-1;lblVendClassID;-1"
      Ctl17           =   "txt1099FormDesc;-1;lblVendClassID;-1"
      Ctl18           =   "lblVendClassID;-1;;-1"
      Ctl19           =   "lblContactName;-1;;-1"
      Ctl20           =   "lblPurchFromAddrID;-1;;-1"
      Ctl21           =   "lblRemitToAddrID;-1;;-1"
      Ctl22           =   "fraTermsInfo;-1;;-1"
      Ctl23           =   "ddnPmtTermsID;-1;lblPmtTermsID;-1"
      Ctl24           =   "calDueDate;-1;lblDueDate;-1"
      Ctl25           =   "calDiscDate;-1;lblDiscDate;-1"
      Ctl26           =   "lblPmtTermsID;-1;;-1"
      Ctl27           =   "lblDiscDate;-1;;-1"
      Ctl28           =   "lblDueDate;-1;;-1"
      Ctl29           =   "fraShippingInfo;-1;;-1"
      Ctl30           =   "lkuShipMethID;-1;lblShipMethID;-1"
      Ctl31           =   "lkuFOBID;-1;lblFOBID;-1"
      Ctl32           =   "lblFOBID;-1;;-1"
      Ctl33           =   "lblShipMethID;-1;;-1"
      Ctl34           =   "fraInvoiceInfo;-1;;-1"
      Ctl35           =   "txtTranCmnt;-1;lblTranCmnt;-1"
      Ctl36           =   "chkSeparateChk;-1;;-1"
      Ctl37           =   "chkHoldPmt;-1;;-1"
      Ctl38           =   "cmdCurrency;-1;;-1"
      Ctl39           =   "txtCurrID;-1;lblCashAcctID;-1"
      Ctl40           =   "ddnReasonCodeID;-1;lblReasonCodeID;-1"
      Ctl41           =   "ddnCashAcctID;-1;lblCashAcctID;-1"
      Ctl42           =   "calInvcRcptDate;-1;lblInvcRcptDate;-1"
      Ctl43           =   "calTranDate;-1;lblTranDate;-1"
      Ctl44           =   "lblTranCmnt;-1;;-1"
      Ctl45           =   "lblReasonCodeID;-1;;-1"
      Ctl46           =   "lblTranDate;-1;;-1"
      Ctl47           =   "lblInvcRcptDate;-1;;-1"
      Ctl48           =   "lblCashAcctID;-1;;-1"
      Ctl49           =   "pnlTab;1;;-1"
      Ctl50           =   "fraDetail;-1;;-1"
      Ctl51           =   "spnQuantity;-1;;-1"
      Ctl52           =   "cmdUndo;-1;;-1"
      Ctl53           =   "cmdOK;-1;;-1"
      Ctl54           =   "ddnTargetCompany;-1;lblTargetCompany;-1"
      Ctl55           =   "nbrQuantity;-1;lblQuantity;-1"
      Ctl56           =   "curExtAmt;-1;lblExtAmt;-1"
      Ctl57           =   "curUnitCost;-1;lblUnitCost;-1"
      Ctl58           =   "lkuItemID;-1;lblItemID;-1"
      Ctl59           =   "ddnUnitMeasID;-1;lblUnitMeasID;-1"
      Ctl60           =   "txtShortDesc;-1;lblItemID;-1"
      Ctl61           =   "pnlSubTab;3;;-1"
      Ctl62           =   "txtExtComment;-1;lblExtComment;-1"
      Ctl63           =   "lblExtComment;-1;;-1"
      Ctl64           =   "pnlSubTab;4;;-1"
      Ctl65           =   "chkReturnType;-1;;-1"
      Ctl66           =   "chkDoNotMatch;-1;;-1"
      Ctl67           =   "pnlSubTab;0;;-1"
      Ctl68           =   "glaGLAcctNo;-1;lblGLAcctNo;-1"
      Ctl69           =   "lkuAcctRefCode;-1;lblAcctRefCode;-1"
      Ctl70           =   "txtAcctRefDesc;-1;lblAcctRefCode;-1"
      Ctl71           =   "lblAcctRefCode;-1;;-1"
      Ctl72           =   "lblGLAcctNo;-1;;-1"
      Ctl73           =   "pnlSubTab;2;;-1"
      Ctl74           =   "curFreightAmtDetl;-1;lblFreightAmtDetl;-1"
      Ctl75           =   "lkuShipMethIDDetl;-1;lblShipMethIDDetl;-1"
      Ctl76           =   "lkuFOBIDDetl;-1;lblFOBIDDetl;-1"
      Ctl77           =   "lblShipMethIDDetl;-1;;-1"
      Ctl78           =   "lblFOBIDDetl;-1;;-1"
      Ctl79           =   "lblFreightAmtDetl;-1;;-1"
      Ctl80           =   "pnlSubTab;1;;-1"
      Ctl81           =   "cmdSTaxLineDetl;-1;;-1"
      Ctl82           =   "ddnSTaxClassID;-1;lblSTaxClassID;-1"
      Ctl83           =   "curSTaxAmtDetl;-1;lblSTaxAmtDetl;-1"
      Ctl84           =   "lblSTaxAmtDetl;-1;;-1"
      Ctl85           =   "lblSTaxClassID;-1;;-1"
      Ctl86           =   "lblExtAmt;-1;;-1"
      Ctl87           =   "lblQuantity;-1;;-1"
      Ctl88           =   "lblUnitCost;-1;;-1"
      Ctl89           =   "lblUnitMeasID;-1;;-1"
      Ctl90           =   "lblItemID;-1;;-1"
      Ctl91           =   "lblTargetCompany;-1;;-1"
      Ctl92           =   "shpFocusRect;-1;;-1"
      Ctl93           =   "pnlTab;2;;-1"
      Ctl94           =   "fraTotalsInfo;0;;-1"
      Ctl95           =   "cmdFreightDetl;-1;;-1"
      Ctl96           =   "cmdSTaxDetl;-1;;-1"
      Ctl97           =   "fraTotalsLine1;-1;;-1"
      Ctl98           =   "txtHomeCurrID;-1;;-1"
      Ctl99           =   "txtNatCurrID;-1;lblInvTotal;-1"
      Ctl100          =   "curInvTotalHC;-1;;-1"
      Ctl101          =   "curInvTotalNC;0;lblInvTotal;-1"
      Ctl102          =   "curSTaxAmt;-1;lblSTaxAmt;-1"
      Ctl103          =   "curFreightAmt;-1;lblFreightAmt;-1"
      Ctl104          =   "curPurchAmt;-1;lblPurchAmt;-1"
      Ctl105          =   "curDetlAmtHC;-1;lblPurchAmt;-1"
      Ctl106          =   "curInvTotalHCS;-1;;-1"
      Ctl107          =   "curFreightAmtS;-1;;-1"
      Ctl108          =   "curSTaxAmtS;-1;;-1"
      Ctl109          =   "lblInvTotal;-1;;-1"
      Ctl110          =   "lblSTaxAmt;-1;;-1"
      Ctl111          =   "lblFreightAmt;-1;;-1"
      Ctl112          =   "lblPurchAmt;-1;;-1"
      Ctl113          =   "fraTotalsInfo;3;;-1"
      Ctl114          =   "fraTotalsLine3;-1;;-1"
      Ctl115          =   "curUndistBalAmt;-1;lblUndistBalAmt;-1"
      Ctl116          =   "curInvTotalNC;2;lblInvTotalUndist;-1"
      Ctl117          =   "curInvAmt;1;lblInvAmtUndist;-1"
      Ctl118          =   "lblUndistBalAmt;-1;;-1"
      Ctl119          =   "lblInvAmtUndist;-1;;-1"
      Ctl120          =   "lblInvTotalUndist;-1;;-1"
      Ctl121          =   "fraTotalsInfo;2;;-1"
      Ctl122          =   "fraTotalsLine2;-1;;-1"
      Ctl123          =   "cmdPmtDetl;-1;;-1"
      Ctl124          =   "curBalDueAmt;-1;lblBalDueAmt;-1"
      Ctl125          =   "curPmtAmt;-1;lblPmtAmt;-1"
      Ctl126          =   "curInvTotalNC;1;lblInvTotalBalDue;-1"
      Ctl127          =   "curDiscAmt;-1;lblDiscAmt;-1"
      Ctl128          =   "lkuApplyToTranID;-1;lblPmtAmt;-1"
      Ctl129          =   "curFormatHC;-1;lblDiscAmt;-1"
      Ctl130          =   "curFormatNC;-1;;-1"
      Ctl131          =   "curFormatOverflow;-1;lblBalDueAmt;-1"
      Ctl132          =   "curDiscAmtS;-1;;-1"
      Ctl133          =   "lblDiscAmt;-1;;-1"
      Ctl134          =   "lblInvTotalBalDue;-1;;-1"
      Ctl135          =   "lblPmtAmt;-1;;-1"
      Ctl136          =   "lblBalDueAmt;-1;;-1"
      Ctl137          =   "fraTotalsInfo;1;;-1"
      Ctl138          =   "txtBatchTotalCurrID;-1;lblBatchTotal;-1"
      Ctl139          =   "txtBatchID;-1;lblBatchID;-1"
      Ctl140          =   "curBatchTotal;-1;lblBatchTotal;-1"
      Ctl141          =   "calBatchPostDate;-1;lblBatchPostDate;-1"
      Ctl142          =   "lblBatchID;-1;;-1"
      Ctl143          =   "lblBatchPostDate;-1;;-1"
      Ctl144          =   "lblBatchTotal;-1;;-1"
      Ctl145          =   "curInvAmt;0;lblVouchAmt;-1"
      Ctl146          =   "lkuVouchNo;-1;lblVouchNo;-1"
      Ctl147          =   "ddnTranType;-1;lblTranType;-1"
      Ctl148          =   "sbrMain;-1;;-1"
      Ctl149          =   "txtTranNo;-1;lblTranNo;-1"
      Ctl150          =   "CustomCombo;0;;-1"
      Ctl151          =   "CustomOption;0;;-1"
      Ctl152          =   "CustomCheck;0;;-1"
      Ctl153          =   "CustomButton;0;;-1"
      Ctl154          =   "CustomFrame;0;;-1"
      Ctl155          =   "CustomSpin;0;;-1"
      Ctl156          =   "CustomCurrency;0;;-1"
      Ctl157          =   "CustomNumber;0;;-1"
      Ctl158          =   "CustomMask;0;CustomLabel;0"
      Ctl159          =   "picDrag;0;;-1"
      Ctl160          =   "tbrMain;-1;;0"
      Ctl161          =   "lkuVendID;-1;lblVendID;-1"
      Ctl162          =   "txtVendName;-1;lblVendID;-1"
      Ctl163          =   "lkuPONo;-1;lblPONo;-1"
      Ctl164          =   "ddn1099Form;-1;CustomLabel;0"
      Ctl165          =   "WinHook1;-1;;-1"
      Ctl166          =   "curInvAmtS;-1;CustomLabel;0"
      Ctl167          =   "lblPONo;-1;;-1"
      Ctl168          =   "lblVouchAmt;-1;;-1"
      Ctl169          =   "lblVendID;-1;;-1"
      Ctl170          =   "lblVouchNo;-1;;-1"
      Ctl171          =   "lblTranNo;-1;;-1"
      Ctl172          =   "lblTranType;-1;;-1"
      Ctl173          =   "CustomLabel;0;;-1"
   End
   Begin VB.CommandButton cmdLines 
      Caption         =   "..."
      Height          =   285
      Left            =   10470
      TabIndex        =   13
      ToolTipText     =   "Select Purchase Order Lines"
      Top             =   945
      WhatsThisHelpID =   95701
      Width           =   285
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6405
      WhatsThisHelpID =   73
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   688
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   68
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   69
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   70
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   71
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   72
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   73
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   74
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   75
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   76
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   78
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   741
   End
   Begin EntryLookupControls.TextLookup lkuVendID 
      Height          =   285
      Left            =   900
      TabIndex        =   64
      Top             =   930
      WhatsThisHelpID =   95687
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   503
      ForeColor       =   -2147483640
      MaxLength       =   12
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Vendor"
      ParentIDColumn  =   "VendID"
      ParentKeyColumn =   "VendKey"
      ParentTable     =   "tapVendor"
      BoundColumn     =   "VendKey"
      BoundTable      =   "tapPendReceiver"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "VendID,lkuVendID,;VendName,,;"
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtVendName 
      Height          =   285
      Left            =   2700
      TabIndex        =   67
      TabStop         =   0   'False
      Top             =   960
      WhatsThisHelpID =   95686
      Width           =   3240
      _Version        =   65536
      _ExtentX        =   5715
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bAutoSelect     =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTACurrency curInvAmtS 
      Height          =   285
      Left            =   -10000
      TabIndex        =   80
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   95685
      Width           =   1500
      _Version        =   65536
      _ExtentX        =   2646
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency curPurchAmtS 
      Height          =   285
      Left            =   -10000
      TabIndex        =   81
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   95684
      Width           =   1800
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   3175
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "            0.00"
      sIntegralPlaces =   13
      sDecimalPlaces  =   2
   End
   Begin EntryLookupControls.TextLookup lkuReceiptNo 
      Height          =   285
      Left            =   3360
      TabIndex        =   4
      Top             =   600
      WhatsThisHelpID =   95683
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   10
      LookupID        =   "PendReceiver"
      BoundColumn     =   "VouchNo"
      BoundTable      =   "tapPendVoucher"
      sSQLReturnCols  =   "TranNo,lkuReceiptNo,;"
   End
   Begin EntryLookupControls.TextLookup lkuPONo 
      Height          =   285
      Left            =   8310
      TabIndex        =   53
      Top             =   930
      Visible         =   0   'False
      WhatsThisHelpID =   95682
      Width           =   2115
      _ExtentX        =   3731
      _ExtentY        =   503
      ForeColor       =   -2147483640
      MaxLength       =   15
      IsSurrogateKey  =   -1  'True
      LookupID        =   "PurchaseOrderWithVendor"
      ParentIDColumn  =   "TranNoRel"
      ParentKeyColumn =   "POKey"
      ParentTable     =   "tpoPurchOrder"
      BoundColumn     =   "POKey"
      BoundTable      =   "tapPendVoucher"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "TranNo,,;TranNoRel,lkuPONo,;TranNoRelChngOrd,,;"
   End
   Begin EntryLookupControls.TextLookup lkuReceiver 
      Height          =   285
      Left            =   6870
      TabIndex        =   61
      Top             =   2600
      Visible         =   0   'False
      WhatsThisHelpID =   95681
      Width           =   2115
      _ExtentX        =   3731
      _ExtentY        =   503
      ForeColor       =   -2147483640
      MaxLength       =   10
      IsSurrogateKey  =   -1  'True
      LookupID        =   "Receiver  "
      ParentIDColumn  =   "TranNo"
      ParentKeyColumn =   "RcvrKey"
      ParentTable     =   "tpoReceiver"
      BoundColumn     =   "RtrnRcvrKey"
      BoundTable      =   "tpoReceiver"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "TranNo,lkuReceiver,;CompanyID,,;"
   End
   Begin EntryLookupControls.TextLookup lkuReturn 
      Height          =   285
      Left            =   720
      TabIndex        =   95
      Top             =   2600
      Visible         =   0   'False
      WhatsThisHelpID =   95680
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupMode      =   0
      MaxLength       =   10
      LookupID        =   "PendReceiver"
      BoundColumn     =   "VouchNo"
      BoundTable      =   "tapPendVoucher"
      sSQLReturnCols  =   "TranNo,lkuReturn,;"
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtShipWhseDesc 
      Height          =   285
      Left            =   2700
      TabIndex        =   100
      TabStop         =   0   'False
      Top             =   960
      Visible         =   0   'False
      WhatsThisHelpID =   95679
      Width           =   3240
      _Version        =   65536
      _ExtentX        =   5715
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bAutoSelect     =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin EntryLookupControls.TextLookup lkuShipWhse 
      Height          =   285
      Left            =   900
      TabIndex        =   63
      Top             =   930
      Visible         =   0   'False
      WhatsThisHelpID =   95678
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   503
      ForeColor       =   -2147483640
      Enabled         =   0   'False
      MaxLength       =   12
      EnabledLookup   =   0   'False
      EnabledText     =   0   'False
      VisibleLookup   =   0   'False
      Protected       =   -1  'True
      LookupID        =   "Warehouse"
      ParentIDColumn  =   "WhseID"
      ParentKeyColumn =   "WhseKey"
      ParentTable     =   "timWarehouse"
      BoundColumn     =   "WhseID"
      BoundTable      =   "timWarehouse"
      sSQLReturnCols  =   "WhseID,lkuShipWhse,;Description,txtShipWhseDesc,;"
   End
   Begin EntryLookupControls.TextLookup lkuTransfer 
      Height          =   285
      Left            =   8310
      TabIndex        =   50
      Top             =   930
      Visible         =   0   'False
      WhatsThisHelpID =   95677
      Width           =   1635
      _ExtentX        =   2884
      _ExtentY        =   503
      ForeColor       =   -2147483640
      Enabled         =   0   'False
      MaxLength       =   12
      EnabledLookup   =   0   'False
      EnabledText     =   0   'False
      VisibleLookup   =   0   'False
      Protected       =   -1  'True
      IsSurrogateKey  =   -1  'True
      LookupID        =   "TransferOrder"
      ParentIDColumn  =   "TranNo"
      ParentKeyColumn =   "TrnsfrOrderKey"
      ParentTable     =   "vimTrnsfrOrder"
      BoundColumn     =   "TrnsfrOrderKey"
      BoundTable      =   "tpoPendReceiver"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "TranNo,lkuTransfer,;"
   End
   Begin VB.TextBox txtTransit 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      Height          =   315
      Left            =   5790
      TabIndex        =   101
      Top             =   570
      Visible         =   0   'False
      WhatsThisHelpID =   95676
      Width           =   1305
   End
   Begin VB.Label lblPurchCompany 
      AutoSize        =   -1  'True
      Caption         =   "Purch Comp"
      Height          =   195
      Left            =   5220
      TabIndex        =   5
      Top             =   630
      Width           =   870
   End
   Begin VB.Label lblTransit 
      AutoSize        =   -1  'True
      Caption         =   "Transit"
      Height          =   195
      Left            =   5220
      TabIndex        =   102
      Top             =   630
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblRcptNo 
      AutoSize        =   -1  'True
      Caption         =   "Receipt"
      Height          =   195
      Left            =   2700
      TabIndex        =   2
      Top             =   630
      Width           =   555
   End
   Begin VB.Label lblType 
      AutoSize        =   -1  'True
      Caption         =   "Type"
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   630
      Width           =   360
   End
   Begin VB.Label lblRtrnDate 
      AutoSize        =   -1  'True
      Caption         =   "Return &Date"
      Height          =   195
      Left            =   2700
      TabIndex        =   3
      Top             =   630
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.Label lblReturn 
      Caption         =   "Return"
      Height          =   225
      Left            =   60
      TabIndex        =   96
      Top             =   2660
      Visible         =   0   'False
      Width           =   585
   End
   Begin VB.Label lblReceiver 
      Caption         =   "R&eceiver"
      Height          =   225
      Left            =   5880
      TabIndex        =   60
      Top             =   2660
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.Label lblDash 
      Caption         =   "-"
      Height          =   285
      Left            =   9425
      TabIndex        =   58
      Top             =   960
      Width           =   80
   End
   Begin VB.Label lblPONo 
      AutoSize        =   -1  'True
      Caption         =   "&PO"
      Height          =   195
      Left            =   7290
      TabIndex        =   9
      Top             =   1005
      Width           =   255
   End
   Begin VB.Label lblRcptDate 
      AutoSize        =   -1  'True
      Caption         =   "Receipt &Date"
      Height          =   195
      Left            =   7290
      TabIndex        =   59
      Top             =   630
      Width           =   975
   End
   Begin VB.Label lblVendID 
      AutoSize        =   -1  'True
      Caption         =   "&Vendor"
      Height          =   195
      Left            =   60
      TabIndex        =   62
      Top             =   1005
      Width           =   510
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   77
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblShipWhse 
      AutoSize        =   -1  'True
      Caption         =   "&Ship Whse"
      Height          =   195
      Left            =   60
      TabIndex        =   99
      Top             =   1005
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.Label lblTransfer 
      AutoSize        =   -1  'True
      Caption         =   "Transfer"
      Height          =   195
      Left            =   7290
      TabIndex        =   8
      Top             =   1005
      Visible         =   0   'False
      Width           =   585
   End
End
Attribute VB_Name = "frmEnterRcptOfGoods"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Constants
Const kEntTypePOReceiver = 1109
Const kEntTypePORcvrLine = 1110
Const kEntTypePOReturn = 1111
Const kEntTypePORtrnLine = 1112

Const kArrBinRetVal_BinID = 0
Const kArrBinRetVal_AvailQty = 1
Const kArrBinRetVal_UOMKey = 2
Const kArrBinRetVal_LotNo = 3
Const kArrBinRetVal_LotExpDate = 4

'Receipt Type (matches ddnType index values)
Private Enum enumReceiptType
    kReceiptTypePurchase = 0
    kReceiptTypeTransfer = 1
End Enum

'-- Public Form Variables
    Public moSotaObjects            As New Collection
    Public moMapSrch                As New Collection
    Public mbCancelTransferBeforeNavReturn As Boolean
    Public m_MenuInfo               As New Collection       ' Collection for toolbar button menus

'-- Context Menu Object
    Private moContextMenu           As clsContextMenu

'-- Data Manager Object Variables
    Public WithEvents moDmHeader    As clsDmForm    '-- Public for CTI
Attribute moDmHeader.VB_VarHelpID = -1
    Private WithEvents moDmDetl     As clsDmGrid
Attribute moDmDetl.VB_VarHelpID = -1
    Private WithEvents moDmLineDist As clsDmGrid
Attribute moDmLineDist.VB_VarHelpID = -1

'-- Binding Object Variables
    Private moLE                    As New clsLineEntry
    Private moOptions               As New clsModuleOptions
    Private moIMSClass              As New clsIMS
    Private moItem                  As clsIMSItem
    Private moIMDistribution        As Object
    Private moPOEnterLandedCost     As Object
    

'-- Miscellaneous Variables
    Private mbEnterAsTab            As Boolean
    Private msCompanyID             As String
    Private msPurchCompanyID        As String
    Private msUserID                As String
    Private msHomeCurrID            As String
    Private miSecurityLevel         As Integer
    Private miFilter                As Integer
    Private mlLanguage              As Long
    Private mlTranType              As Long
    Private msTranType              As String
    Private miEntType               As Integer
    Private miEntLineType           As Integer
    Private mlInvalidRow            As Long
    Private miReceiptType           As Integer
    Private msOldlkuPONoText        As String
    Private msOldlkuTransferText    As String

'-- Form resize variables
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long

'-- Private variables of Form Properties
    Public moClass                  As Object    '-- Public for CTI
    Private moAppDB                 As Object
    Private moSysSession            As Object
    Private mbCancelShutDown        As Boolean
    Private mbLoadSuccess           As Boolean
    Private mbAllowInterCo          As Boolean
    Private mbShowMsg               As Boolean
    Private mbOverflow              As Boolean
    Private mbInBrowse              As Boolean
    Private mbSaved                 As Boolean
    Private mbFromCode              As Boolean
    Private mbValidKeyChange        As Boolean
    Private mbFromTargetCo          As Boolean
    Private mbFromNav               As Boolean
    Private mbLoadingRec            As Boolean
    Private mbAllowKeyChange        As Boolean
    Private mbLineJoinCreated       As Boolean
    Private mbLoadingPORow          As Boolean
    Private mbPOCleared             As Boolean
    Private mbIntegrateWithIM       As Boolean
    Private miClosePOonFirstRcpt    As Integer
    Private mbCloseSrcLine          As Boolean
    Private mbMatching              As Boolean
    Private miCaptureWght           As Integer
    Private miCaptureVol            As Integer
    Private miRcptDaysEarly         As Integer
    Private mbFilterByDate          As Boolean
    Private mbLoadingForm           As Boolean  'Controls whether certain controls will be disabled.
    Private mbTrackByBin            As Boolean  'Does the warehouse track by bin??
    Private mlSourceUOMKey          As Long     'The Purchase or Transfer UOM for the current line.
    Private mdQtyInSrcUOM            As Double   'The qty received in Source UOM
    Private mlIMTempTranID          As Long     'The tran id for unsaved IM Distribuitons
    Private miIMDistDirty           As Integer  'Flag to indicate that the UOM or Qty has changed for
                                                '  a line that is associated with an IM Distribution.
    Private mlPOLineKey             As Long     'The PO Line Key for the current Rcvr Line.
    Private mlTrnsfrOrderLineKey    As Long     'The Transfer Order Line Key for the current Rcvr Line.
    ' Variables for retaining data
    Private mlItemKey               As Long
    Private mlUOMKey                As Long
    Private mlReasonKey             As Long
    Private mlInvtKey               As Long
    Private msVolUOM                As String
    Private msWghtUOM               As String
    Private mdItemVol               As Double
    Private mdItemWght              As Double
    Private mdRcvrVol               As Double
    Private mdRcvrWght              As Double
    Private mdFreightAmt            As Double
    Private mdSTaxAmt               As Double
    Private mdUnitCost              As Double
    Private mdUnitCostExact         As Double   ' The UnitCostExact value
'   Private msArrBinRetValues()     As String
    
    Private miQtyDecPlaces          As Integer
    Private miPostDateYear          As Integer
    Private miSignFactor            As Integer
    Private mlRunMode               As Long
    Private mlVRunMode              As lVRunModes
    Private mlBatchkey              As Long
    Private mlWhseKey               As Long
    Private mlOrigPOKey             As Long
    Private mlOrigTransferKey       As Long
    Private mlOrigRcvrKey           As Long
    Private mlCurrExchSchdKey       As Long
    Private mlCurrExchSchdKeyOld    As Long
    Private mlRcvrKey               As Long
    Private mlRcvrLineKey           As Long
    Private mlInvtTranKey           As Long
    Private msPostDate              As String
    Private msFormCaption           As String
    Private msApplyFromTranID       As String
    Private msRcvrNoLbl             As String
    Private mcTranTypes             As New Collection
    Private mcICGLAcctMask          As New Collection
    Private muNatCurrInfo           As CurrencyInfo
    Private muHomeCurrInfo          As CurrencyInfo
    Private mbInRcptNoValidation    As Boolean
    Private mbValidAutoDistBin      As Boolean
    Private mbAutoDistBinClicked    As Boolean
    Private mbGridClicked           As Boolean
    
    Private mbSaveSuccessful        As Boolean
    Private mbUserClickedLandedCost As Boolean
    Public mbAdjToLandedCostAmtsRequired  As Boolean
    Private mbDisplayMsgPOLandCostAutoAdjAmtOnEdit As Boolean
    Public mbAllRcvrLinesWereOnceDeleted As Boolean
    
    Private Type uRcvrLineType
        lRcvrLineKey            As Long
        dItemWght               As Double
        dItemVol                As Double
        lDistTranKey            As Long
        lTrnsfrOrderLineKey     As Long
    End Type
    Private muRcvrLine           As uRcvrLineType
    
    'Vendor performance detail fields
    Private muVendPerfDtl       As uVendPerfDtl
    
'-- Constants
    '-- ReceiverLog TranStatus constants
    Private Const kvIncompleteReceiverLog = 1  'Incomplete
    Private Const kvPendingReceiverLog = 2     'Pending
    Private Const kvPostedReceiverLog = 3      'Posted
    Private Const kvPurgedReceiverLog = 4      'Purged
    Private Const kvVoidReceiverLog = 5        'Void
        
    '-- Sign Factor constants
    Private Const kiPosSignFactor = 1
    Private Const kiNegSignFactor = -1
    
    '-- Tab page constants
    Private Const kiHeaderTab = 0
    Private Const kiDetailTab = 1
    
    '-- Detail Tab page constants
    Private Const kiProjectTab = 0
    Private Const kTabOrigOrder = 1
    Private Const kTabComments = 2
    Private Const kTabMisc = 3
    Private Const kTabPutAway = 4
    
    '-- Constants for Insert or Update Mode
    Private Const kiInsertMode = 0
    Private Const kiUpdateMode = 1
    
    '-- Arbitrary value for extra secret super special flag
    Private Const kiExtraSpecial = -999
    
    '-- Static List constants
    'Receipt Status constants
    Private Const kvReceiptOpen = 1             'Open        Val=1
    
    'Vendor Status constants
    Private Const kvVendorActive = 1            'Active      Val=1 *Default*
    Private Const kvVendorTemporary = 3         'Temporary   Val=3
    
    'Item Status constants
    Private Const kvItemActive = 1              'Active           Val=1
    
    'Item Type constants
    Private Const kvItemMiscItem = 1            'Misc Item        Val=1
    Private Const kvItemExpense = 3             'Expense          Val=3
    Private Const kvItemCommentOnly = 4         'Comment Only     Val=4
        
    'Meas Type constants
    Private Const kvMeasTypeNone = 0
    
    'GL Account Status constants
    Private Const kvGLAcctStatusActive = 1      'Active      *Default*
    
    'GL Account Category ID constants
    Private Const kvGLAcctCatIDNonFinancial = 9 'Non-Financial
        
    'GL Account Currency Restriction constants
    Private Const kiGLAcctCurrRestrictSpecific = 1
        
    'PO Status constants
    Private Const kvPOOpen = 1                  'Open        Val=1
    
    'PO Closed for Rcving status
    Private Const kvOpenForRcving = 0           'NO
    
    'Receipt Tran Size
    Private Const kvRcptTranSize = 10
    
    Private Const kvTranOrderTranSize = 10
    'Weight & Vol capture types
    Private Const kvCaptureNone = 0
    Private Const kvCaptureOptional = 1
    Private Const kvCaptureRequired = 2

    'Transfer Order Status
    Private Const kvTransferOpen = 1

'-- Remove this ENUM, clsDmForm and frmGetID after 3.0!!
'    Public Enum StatusType    '   Sage MAS 500 StatusBar Status Images
'        Sage MAS 500_DMSB_NONE = 0
'        Sage MAS 500_DMSB_START = 1
'        Sage MAS 500_DMSB_ADD = 2
'        Sage MAS 500_DMSB_EDIT = 3
'        Sage MAS 500_DMSB_SAVE = 4
'        Sage MAS 500_DMSB_LOCKED = 5
'        Sage MAS 500_DMSB_INQUIRY = 6
'        Sage MAS 500_DMSB_BUSY = 7
'    End Enum
    
    ' *** RMM 5/2/01, Context menu project
    Private mbIMActivated As Boolean
    Private mbWMIsLicensed As Boolean
    
    Private moProject               As clsProject

Const VBRIG_MODULE_ID_STRING = "POZDF001.FRM"

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Private Sub MapControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    giCollectionAdd moMapSrch, navPO, CStr(txtPONum.hwnd)
    giCollectionAdd moMapSrch, navPO, CStr(txtRelNo.hwnd)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "POZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get bInRcptNoValidation() As Boolean
'+++ VB/Rig Skip +++
    bInRcptNoValidation = mbInRcptNoValidation
End Property
Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bLoadSuccess = mbLoadSuccess
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadSuccess_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let sPurchCompanyID(lNewPurchCompanyID As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msPurchCompanyID = lNewPurchCompanyID
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbSaved = bNewSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let dVolAmt(dNewVolAmt As Double)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mdRcvrVol = dNewVolAmt
    If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
        moLE.GridEditChange chkMatch
    End If
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "dVolAmt_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let dWghtAmt(dNewWghtAmt As Double)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mdRcvrWght = dNewWghtAmt
    If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
        moLE.GridEditChange chkMatch
    End If
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "dWghtAmt_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let lBatchKey(lNewBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlBatchkey = lNewBatchKey
    
    '-- Refresh the main navigator with the new Batch Key
    If mlTranType <> kTranTypePORTrn Then
        lkuReceiptNo.RestrictClause = sGetReceiptNoRestrict()
    Else
        lkuReturn.RestrictClause = sGetReceiptNoRestrict()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lBatchKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lWhseKey(lNewWhseKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlWhseKey = lNewWhseKey
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lWhseKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Public Property Let sPostDate(sNewPostDate As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msPostDate = sNewPostDate
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sPostDate_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let iPostDateYear(iNewPostDateYear As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    miPostDateYear = iNewPostDateYear
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iPostDateYear_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let sFormCaption(sNewFormCaption As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    msFormCaption = sNewFormCaption
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sFormCaption_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lVRunMode(lNewVRunMode As lVRunModes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlVRunMode = lNewVRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lVRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function
Public Function bRemoteSecurEvent(sID As String, sUser As String, vPrompt As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bRemoteSecurEvent = False
    If (moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) <> 0) Then
        bRemoteSecurEvent = True
    End If
'    frmSelectPOLines.Show vbModal
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRemoteSecurEvent", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
    
End Function
Private Sub SetupDropDowns()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    
'-- Main fields
    '-- Purchasing company Type
    With ddnPurchCompany
        Set .DBObject = moAppDB
        .LocaleID = mlLanguage
        
        '-- Get Source companies related to the home company
        sSQL = "SELECT c.CompanyID " & _
                "FROM tsmCompany c " & _
                " JOIN tglTargetComp t ON c.CompanyID = t.SourceCompanyID" & _
                " WHERE t.TargetCompanyID = " & gsQuoted(msCompanyID) & _
                " ORDER BY c.CompanyID"
        .SQLStatement = sSQL
        .Refresh
        
    End With
    With ddnReason
        Set .DBObject = moAppDB
        .LocaleID = mlLanguage
        
        '-- Get Source companies related to the home company
        sSQL = "SELECT ReasonCodeID, ReasonCodeKey " & _
                "FROM tciReasonCode " & _
                "WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                " ORDER BY ReasonCodeID"
        .SQLStatement = sSQL
        .Refresh
        
    End With

    With ddnUOM
        Set .DBObject = moAppDB
        .LocaleID = mlLanguage
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupDropDowns", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupLookups()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim oFramework As cFramework
    
    Set oFramework = moClass.moFramework
    
'-- Main fields
    '-- Receipt
    With lkuReceiptNo
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
    End With
        
     With lkuReturn
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
    End With
       
    '-- Vendor
    With lkuVendID
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
        
        If (mlVRunMode = kViewEditReceiptDDN) Or (mlVRunMode = kViewEditReceiptDTE) Then
            .BoundTable = "tpoReceiver"
        End If
        
        .RestrictClause = sGetVendorRestrict()
    End With
    
    '-- PO
    With lkuPONo
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
        .RestrictClause = sGetPORestrict()
    End With
    
    '-- Receiver
    With lkuReceiver
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
        .RestrictClause = sGetReceiverRestrict()
    End With
    
    '-- Ship Whse
    With lkuShipWhse
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
        .RestrictClause = sGetShipWhseRestrict()
    End With
    
    '-- Transfer
    With lkuTransfer
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
        .RestrictClause = sGetTransferRestrict()
    End With
    
    'Set Up the Header Warehouse Bin navigator.
    bSetupLookupNav lkuReceiveToBin, "Bin", sHeaderWhseBinRestrict(moClass.mlWhseKey)

    'Set Up the Detail Auto-Dist Bin navigator.
    With lkuAutoDistBin
        Set .Framework = oFramework
        Set .SysDB = moClass.moAppDB
        Set .AppDatabase = moAppDB
    End With

    '-- Release the internal variables
    Set oFramework = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupLookups", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sGetReceiptNoRestrict() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    sGetReceiptNoRestrict = "CompanyID = " & gsQuoted(msCompanyID) & _
        " AND BatchKey = " & mlBatchkey & _
        " AND TranType = " & mlTranType
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetReceiptNoRestrict", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bSetupReceiver() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs As Object

    bSetupReceiver = False
    
    mbIntegrateWithIM = CBool(moOptions.PO("IntegrateWithIM")) And (moClass.mlWhseKey <> 0) ' Integrate with IM
    miClosePOonFirstRcpt = moOptions.PO("ClosePOonFirstRcpt")
    
    If mbIntegrateWithIM Then
        If moClass.mlWhseKey > 0 Then
            sSQL = "SELECT TrackQtyAtBin FROM timWarehouse WHERE WhseKey = " & moClass.mlWhseKey
            Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            If Not rs.IsEmpty Then
                mbTrackByBin = gbGetValidBoolean(rs.Field("TrackQtyAtBin"))
            Else
                mbTrackByBin = False
            End If
            If Not rs Is Nothing Then
                rs.Close: Set rs = Nothing
            End If
        End If
        
        cmdDist.Visible = True
        cmdDist.Enabled = True
        
        'Enable Put Away controls.
        Select Case mlTranType
            Case kTranTypePORG, kTranTypePOTR
                'Put Away controls only supported in PORG and POTR.
                'Move up the put away controls to hide the RMA control.
                fraPutAwayAllGoodsTo.Top = txtRMA.Top '885
                
                'If the warehouse does not tracking quantity at bin, everything goes to a default bin.
                'So, disable the put away controls.
                If mbTrackByBin = False Then
                    lkuReceiveToBin.Enabled = False
                    chkPreferredBin.Enabled = False
                Else
                    'Enable the put away controls.
                    chkPreferredBin.Enabled = True
                    If chkPreferredBin.Value = 1 Then
                        'Checked, possibly from previous save.
                        lkuReceiveToBin.Enabled = False
                    Else
                        lkuReceiveToBin.Enabled = True
                    End If
                End If
                
                'Disable detail put away controls.
                lkuAutoDistBin.Enabled = False
                chkPreferredBinDetl.Enabled = False
            
            Case Else
                'Disable and hide put away controls.
                HidePutAwayCtrls
        End Select

        If moIMDistribution Is Nothing Then
            Set moIMDistribution = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                                    "imzde001.clsimzde001", 117833828, _
                                    kAOFRunFlags, kContextAOF)
            If moIMDistribution Is Nothing Then
                Exit Function
            ElseIf Not moIMDistribution.InitDistribution(moClass.moAppDB, moClass.moAppDB, moClass.mlWhseKey) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgErrorInitIMDist
                Exit Function
            End If
        Else
            moIMDistribution.WarehouseKey = moClass.mlWhseKey
        End If

    Else
        mbTrackByBin = False
        cmdDist.Visible = False
        cmdDist.Enabled = False
        HidePutAwayCtrls
    End If

    If mlTranType <> kTranTypePORTrn Then
        lkuReceiptNo.RestrictClause = sGetReceiptNoRestrict()
    Else
        lkuReturn.RestrictClause = sGetReceiptNoRestrict()
    End If

    ' Set focus to the header tab
    'tabReceipt.Tab = 0
    ' If not defaulting quantity, then the Original Order tab is hidden and the Comments tab should be first
    If Not mbDefaultQty Then
        tabSubDetl.Tab = kTabComments ' Set it to the Comments tab
    Else
        'tabSubDetl.Tab = kiProjectTab ' Set it to the Original Order tab
    End If
    
    If mlTranType = kTranTypePORTrn Then
        lkuReceiver.RestrictClause = sGetReceiverRestrict
    End If

    
    bSetupReceiver = True
    
    mbLoadingForm = True
    
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupReceiver", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sGetVendorRestrict() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sRestrict As String
    
    sRestrict = "CompanyID = " & gsQuoted(msPurchCompanyID)

    sRestrict = sRestrict & " AND (Status = " & kvVendorActive
    sRestrict = sRestrict & " OR Status = " & kvVendorTemporary & ")"

    sGetVendorRestrict = sRestrict
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetVendorRestrict", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Function sGetReceiverRestrict() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    sGetReceiverRestrict = "CompanyID = " & gsQuoted(msCompanyID) & _
        " AND TranType = " & kTranTypePORG
    If (Len(Trim$(lkuVendID)) <> 0) Then
        sGetReceiverRestrict = sGetReceiverRestrict & " AND VendKey = " & muVendDflts.lVendKey
    End If
    If moClass.mlWhseKey > 0 Then
        sGetReceiverRestrict = sGetReceiverRestrict & " AND WhseKey = " & moClass.mlWhseKey
    Else
        sGetReceiverRestrict = sGetReceiverRestrict & " AND WhseKey IS NULL"
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetReceiverRestrict", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function
Private Function sGetPORestrict() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sRestrict As String

    sRestrict = "CompanyID = " & gsQuoted(msPurchCompanyID)
    sRestrict = sRestrict & " AND Status = " & kvPOOpen  '-- Open
    sRestrict = sRestrict & " AND TranType = " & kTranTypePOPO  '-- Standard PO
    sRestrict = sRestrict & " AND ClosedForRcvg = " & kvOpenForRcving
    If (Len(Trim$(lkuVendID)) <> 0) Then
        sRestrict = sRestrict & " AND VendKey = " & muVendDflts.lVendKey
    End If

    sGetPORestrict = sRestrict
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetPORestrict", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sGetShipWhseRestrict() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sRestrict As String
    
    sRestrict = "CompanyID = " & gsQuoted(msPurchCompanyID) & _
                " AND WhseKey <> " & moClass.mlWhseKey & _
                " AND Transit = 0"

    sGetShipWhseRestrict = sRestrict
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "sGetShipWhseRestrict", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function sGetTransferRestrict() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sRestrict As String

    sRestrict = "CompanyID = " & gsQuoted(msCompanyID)

    sRestrict = sRestrict & " AND Status = " & Format$(kvTransferOpen)
    sRestrict = sRestrict & " AND RcvgWhseKey = " & Format$(moClass.mlWhseKey)

    If (Me.lkuShipWhse.KeyValue <> 0) Then
        sRestrict = sRestrict & " AND ShipWhseKey = " & Format$(lkuShipWhse.KeyValue)
    End If
    
    sGetTransferRestrict = sRestrict

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "sGetTransferRestrict", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub RefreshVendDepends()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************
' Desc: Refresh the restrict clause for all lookups that
'       depend on the VendKey.
'*************************************************************
    If mlTranType = kTranTypePORG Then
        lkuPONo.RestrictClause = sGetPORestrict()
    Else
        lkuReceiver.RestrictClause = sGetReceiverRestrict()
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RefreshVendDepends", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'*************************************************************
' Desc: Refresh the restrict clause for all lookups that
'       depend on the ShipWhseKey.
'*************************************************************
Private Sub RefreshShipWhseDepends()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    lkuTransfer.RestrictClause = sGetTransferRestrict
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "RefreshShipWhseDepends", VBRIG_IS_FORM   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupBars()
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
    '-- Setup the status bar
    Set sbrMain.Framework = moClass.moFramework
    
    '-- Setup the toolbar
    With tbrMain
        Select Case mlVRunMode
            Case kEnterReceiptDTE, kViewEditReceiptDTE, kErogDTE
                '-- Standard Data Entry mode
                .Style = sotaTB_TRANSACTION
                If (mlVRunMode = kViewEditReceiptDTE) Then
                    .RemoveButton kTbDelete
                    .RemoveButton kTbNextNumber
                End If
                .RemoveButton kTbCopyFrom
                .RemoveButton kTbRenameId
                .RemoveButton kTbPrint
                sbrMain.BrowseVisible = True
                ConfigureMemos
                .ChangeButtonStyle kTbMemo, tbrDropdown, m_MenuInfo
                
            Case kViewEditReceiptDDN, kErogDDN, kEnterReceiptAOF
                '-- Drill Down mode or AOF mode
                .Style = sotaTB_AOF
                If (mlVRunMode = kEnterReceiptAOF) Then
                    .AddButton kTbDelete, .GetIndex(kTbCancelExit)
                End If
                .AddSeparator
                .AddButton kTbMemo, .GetIndex(kTbHelp)
                .AddSeparator .GetIndex(kTbHelp)
                sbrMain.BrowseVisible = False
                ' Configure memo menus for toolbar button
                ConfigureMemos
                .ChangeButtonStyle kTbMemo, tbrDropdown, m_MenuInfo
        
        End Select
        
        .LocaleID = mlLanguage
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupModuleVars()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Assign the initial filter value
    miFilter = RSID_UNFILTERED

    '-- Set misc. flags
    mbLoadingRec = False
    mbFromNav = False
    mbManualClick = False
    mbAllowKeyChange = True
    mbLoadingPORow = False
    mbAdjToLandedCostAmtsRequired = False
    mbUserClickedLandedCost = False
    mbDisplayMsgPOLandCostAutoAdjAmtOnEdit = False
    mbAllRcvrLinesWereOnceDeleted = False
    mbSaveSuccessful = False
    
    mbPOCleared = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupModuleVars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetFieldStates()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iIntegralPlaces As Integer
    Dim iButtonTop As Integer
    
    '-- Initialize toolbar button states
    SetNextNumberState
    
    
    If miCaptureWght = kvCaptureNone And miCaptureVol = kvCaptureNone Then
        cmdWghtVol.Visible = False
    End If
    
    ' If not defaulting quantity, then hide the Original Order tab
    If Not mbDefaultQty Then
        tabSubDetl.TabVisible(kTabOrigOrder) = False
    End If

    '-- Determine if PA is active
    mbIntPAActive = gbIsModuleActive(moClass, kModulePA)

    If mbIntPAActive Then
        tabSubDetl.TabVisible(kiProjectTab) = True
    Else
        tabSubDetl.TabVisible(kiProjectTab) = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetFieldStates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine will bind fields on the form to fields in the database.
'************************************************************************
    BindHeader
    BindDetail
    BindLineDist
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindHeader()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Bind the parent DM object
    Set moDmHeader = New clsDmForm
    
    With moDmHeader
        Set .Form = Me
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Toolbar = tbrMain
        Set .SOTAStatusBar = sbrMain
        Set .ValidationMgr = SOTAValidationMgr1
        .AppName = gsStripChar(Me.Caption, ".")
        .UniqueKey = "CompanyID, TranNo, TranType"
        .OrderBy = "TranNo"
        .AccessType = kDmBuildQueries
        .SaveOrder = 3
        
        '-- Bind columns common to both pending and posted receiver tables
        .BindLookup lkuVendID, kDmSetNull
        .Bind Nothing, "BatchKey", SQL_INTEGER
        .Bind txtBOL, "BillOfLadingNo", SQL_CHAR
        .Bind txtRMA, "RMA", SQL_CHAR
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind Nothing, "CreateType", SQL_SMALLINT
        .Bind txtTranComment, "TranCmnt", SQL_VARCHAR, kDmSetNull
        .Bind calRcptDate, "TranDate", SQL_DATE
        .Bind Nothing, "TranID", SQL_CHAR
        If mlTranType <> kTranTypePORTrn Then
            .Bind lkuReceiptNo, "TranNo", SQL_CHAR, kDmDisplayColumn
            .BindComboBox ddnPurchCompany, "PurchCompanyID", SQL_CHAR, kDmUseListData
        Else
            .Bind lkuReturn, "TranNo", SQL_CHAR, kDmDisplayColumn
            .Bind Nothing, "PurchCompanyID", SQL_CHAR
        End If
        .Bind Nothing, "TranType", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind Nothing, "RcvrKey", SQL_INTEGER
        .Bind Nothing, "WhseKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CurrID", SQL_CHAR
        .Bind Nothing, "CurrExchRate", SQL_FLOAT
        .Bind Nothing, "VendPerfExceptional", SQL_SMALLINT
        .Bind Nothing, "VendPerfUpgradeSubStItem", SQL_SMALLINT
        .Bind Nothing, "VendPerfUnauthSubstItem", SQL_SMALLINT
        .Bind Nothing, "VendPerfPoorCustService", SQL_SMALLINT
        .Bind Nothing, "VendPerfOtherProblem", SQL_SMALLINT
        .Bind Nothing, "VendPerfDeliveryWrongDest", SQL_SMALLINT
        .Bind Nothing, "VendPerfDeliveryWrongDate", SQL_SMALLINT
        .Bind Nothing, "VendPerfDeliveryWrongTime", SQL_SMALLINT
        .Bind Nothing, "VendPerfComment", SQL_VARCHAR, kDmSetNull
                
        .Table = "tpoPendReceiver"
                
        '-- Bind columns specifically for pending receivers
        .Bind Nothing, "SeqNo", SQL_INTEGER
                
        If mlTranType <> kTranTypePORTrn Then
            .BindLookup lkuPONo, kDmSetNull
            .BindLookup lkuTransfer, kDmSetNull
        Else
            .BindLookup lkuReceiver, kDmSetNull
            .Bind Nothing, "POKey", SQL_INTEGER, kDmSetNull
            .Bind Nothing, "TrnsfrOrderKey", SQL_INTEGER, kDmSetNull
            
            .LinkSource "tpoPurchOrder", "POKey=<<POKey>>"
            .Link txtPONum, "TranNo"
            .Link lkuPONo, "TranNoRel"
        End If

        '-- Setup surrogate key and description field links
        .LinkSource "tapVendor", "VendKey=<<VendKey>>"
        .Link txtVendName, "VendName"
                        
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindHeader", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindDetail()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Bind the receiver detail DM object
    Set moDmDetl = New clsDmGrid

    With moDmDetl
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = Me
        Set .Grid = grdRcptDetl
        Set .Parent = moDmHeader
        .Table = "tpoRcvrLine"
        .UniqueKey = "RcvrLineKey"
        .OrderBy = "SeqNo"
        .SaveOrder = 1
        
' MVB - Should we always be at NoAppend = true??
        If (mlVRunMode = kViewEditReceiptDTE) Or (mlVRunMode = kViewEditReceiptDDN) Then
            .NoAppend = True
        Else
            .NoAppend = False
        End If
        
        '-- Bind the detail columns
        .BindColumn "RcvrLineKey", kColReceiptLineKey, SQL_INTEGER
        .BindColumn "TranCmnt", kColTranComment, SQL_VARCHAR, txtLnComment, kDmSetNull
        .BindColumn "SeqNo", kColSeqNo, SQL_INTEGER
        .BindColumn "UnitCost", kColUnitCost, SQL_DECIMAL, , kDmSetNull
        .BindColumn "ItemRcvdKey", kColItemKey, SQL_INTEGER
        .BindColumn "ItemVol", kColItemVol, SQL_INTEGER
        .BindColumn "ItemWght", kColItemWght, SQL_INTEGER
        .BindColumn "InvtTranKey", kColInvtTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "RtrnType", kColReturnType, SQL_SMALLINT
        .BindColumn "UpdateCounter", Nothing, SQL_INTEGER
        .BindColumn "POLineKey", kColPOLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TrnsfrOrderLineKey", kColTrnsfrLineKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "MatchStatus", kColMatchStatus, SQL_SMALLINT
        .BindColumn "CloseSrcLine", kColCloseSrcLine, SQL_SMALLINT
        .BindColumn "TaxAmt", kColSTaxAmt, SQL_DECIMAL
        .BindColumn "UnitCostExact", kColUnitCostExact, SQL_DECIMAL
        If mlTranType = kTranTypePORTrn Then
            .BindColumn "RtrnRcvrLineKey", kColRtrnRcvrLineKey, SQL_INTEGER, , kDmSetNull
        End If
        .BindColumn "UnitMeasKey", kColUnitMeasRcvdKey, SQL_INTEGER, ddnUOM, kDmUseItemData + kDmSetNull
        .BindColumn "RtrnReasonCodeKey", kColRtrnReasonKey, SQL_INTEGER, ddnReason, kDmUseItemData + kDmSetNull
        'Vendor Performance
        .BindColumn "VendPerfDefectiveGoods", kColDefective, SQL_SMALLINT
        .BindColumn "VendPerfDamagedGoods", kColDamaged, SQL_SMALLINT
        .BindColumn "VendPerfCarrierDamage", kColCarrierDamage, SQL_SMALLINT
        .BindColumn "VendPerfImproperLabeling", kColImpLabel, SQL_SMALLINT
        .BindColumn "VendPerfMissingDoc", kColDocMissing, SQL_SMALLINT
        .BindColumn "VendPerfImproperPackaging", kColImpPackage, SQL_SMALLINT
        .BindColumn "VendPerfNotToSpec", kColNotToSpec, SQL_SMALLINT
        .BindColumn "VendPerfOtherProblem", kColOtherIssue, SQL_SMALLINT
        .BindColumn "VendPerfComment", kColPerfComments, SQL_VARCHAR, , kDmSetNull
        
        
        .LinkSource "#tpoPOJoin", "(tpoRcvrLine.POLineKey=#tpoPOJoin.POLineKey OR tpoRcvrLine.TrnsfrOrderLineKey=#tpoPOJoin.TrnsfrOrderLineKey)", kDmJoin
        .Link kColPOLineNo, "POLineNo"
        .Link kColItemID, "POItemID"
        .Link kColItemDesc, "POItemDesc"
        .Link kColWhseID, "POWhseID"
        .Link kColDeptID, "PODeptID"
        .Link kColQtyOrdered, "POQtyOrd"
        .Link kColUnitMeasOrdID, "POUOMID"
        .Link kColTotalRcvd, "POQtyRcvd"
        .Link kColQtyRtrned, "POQtyRtrned"
        .Link kColQtyOpen, "POQtyOpen"
        .Link kColUnitMeasOrdKey, "POUOMKey"
        .Link kColMeasType, "POUOMType"
        .Link kColQtyShip, "TRQtyShip"
        
        If mlTranType = kTranTypePORTrn Then
            .LinkSource "#tpoRtrnJoin", "tpoRcvrLine.RtrnRcvrLineKey=#tpoRtrnJoin.RcvdLineKey", kDmJoin, LeftOuter
            .Link kColOrigQtyRcvd, "RcvdQty"
            .Link kColOrigRcvrUOMKey, "RtrnRcvrUnitMeasKey"
        End If

        .LinkSource "tpoRcvrLineDist", "tpoRcvrLine.RcvrLineKey=tpoRcvrLineDist.RcvrLineKey", kDmJoin, LeftOuter
        .Link kColQtyRcvdS, "QtyRcvd"
        .Link kColQtyInPOUOM, "QtyRcvdInSrcUOM"
        .Link kColFreightAmt, "FreightAmt"
                       
        .LinkSource "tciUnitMeasure", "tpoRcvrLine.UnitMeasKey=tciUnitMeasure.UnitMeasKey", kDmJoin, LeftOuter
        .Link kColUnitMeasRcvdID, "UnitMeasID"
        
        .ParentLink "RcvrKey", "RcvrKey", SQL_INTEGER

        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindDetail", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub BindLineDist()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Bind the receipt detail DM object
    Set moDmLineDist = New clsDmGrid

    With moDmLineDist
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = Me
        Set .Grid = grdRcvrLineDist
        Set .Parent = moDmDetl
        .Table = "tpoRcvrLineDist"
        .UniqueKey = "RcvrLineDistKey"
        .SaveOrder = 2
        
        If (mlVRunMode = kViewEditReceiptDTE) Or _
           (mlVRunMode = kViewEditReceiptDDN) Then
            .NoAppend = True
        Else
            .NoAppend = False
        End If
        
        '-- Bind the detail columns
        .BindColumn "RcvrLineDistKey", kColRcvrLineDistKeyD, SQL_INTEGER
        .BindColumn "AcctRefKey", kColAcctRefKeyD, SQL_INTEGER, , kDmSetNull
        .BindColumn "FreightAmt", kColFreightAmtD, SQL_DECIMAL
        .BindColumn "GLAcctKey", kColGLAcctKeyD, SQL_INTEGER, , kDmSetNull
        .BindColumn "POLineDistKey", kColPOLineDistKeyD, SQL_INTEGER, , kDmSetNull
If mlTranType = kTranTypePORTrn Then
        .BindColumn "RtrnRcvrLineDstKey", kcolRtrnRcvrLineDistKeyD, SQL_INTEGER
End If
        .BindColumn "QtyRcvd", kColQtyRcvdD, SQL_DECIMAL ', , kDmSetNull
        .BindColumn "QtyRcvdInSrcUOM", kcolQtyRcvdInPOUOMD, SQL_DECIMAL ', , kDmSetNull
        
                 
        .ParentLink "RcvrLineKey", "RcvrLineKey", SQL_INTEGER
        
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLineDist", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindLE()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Desc:   Binds grids to standard Line Entry object manager.
'
'  Parms:   None.
'************************************************************************************
    '-- Initiate a new Line Entry object for the Receiver Detail data
    Set moLE = New clsLineEntry
    
    With moLE
        Set .Grid = grdRcptDetl
        .GridType = iGetGridType()
        Set .TabControl = tabReceipt
        .TabDetailIndex = kiDetailTab
        Set .DM = moDmDetl
        Set .Ok = cmdOK
        Set .Undo = cmdUndo
        ' **PRESTO ** Set .Hook = WinHook1
        .SeqNoCol = kColSeqNo
        .AllowGotoLine = True
        Set .FocusRect = shpFocusRect
        Set .Form = frmEnterRcptOfGoods
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLE", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        .BindGrid moLE, grdRcptDetl.hwnd
        .Bind "PORCPTLINE", grdRcptDetl.hwnd    ' , kEntTypePORcptLine
        
        .Bind "APDA03", lkuVendID.hwnd, kEntTypeAPVendor
        .Bind "IMWHSE", lkuShipWhse.hwnd, kEntTypeIMWarehouse
        .Bind "PAPROJECT", lkuProject.hwnd, kEntTypePAProject
        .Bind "PAPHASE", lkuPhase.hwnd, kEntTypePAPhase
        .Bind "PATASK", lkuTask.hwnd, kEntTypePATask
        .Bind "POPURCHORD", txtPONum.hwnd, kEntTypePOPurchOrder
        .Bind "IMTRANSFER", lkuTransfer.hwnd, kEntTypeIMTransfer
        
        .Bind "IMDA08", txtWhse.hwnd, kEntTypeIMWarehouse
        .Bind "IMITEM", txtItemOrdID.hwnd, kEntTypeIMItem
        .Bind "PORECEIPT", lkuReceiver.hwnd, kEntTypePOReceipt
        .Bind "POLINE", txtPOLine.hwnd, kEntTypePOPOLine
        
        Set .Form = frmEnterRcptOfGoods
    
        .Init
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    Select Case True
        Case ctl Is lkuReceiver
            ETWhereClause = "RcvrKey = " & CStr(glGetValidLong(moDmHeader.GetColumnValue("RcvrKey")))
        Case ctl Is txtPONum
            ETWhereClause = "POKey = " & CStr(glGetValidLong(moDmHeader.GetColumnValue("POKey")))
'        Case ctl Is grdRcptDetl
'            ETWhereClause = "RcvrLineKey = " & CStr(glGetValidLong(moDmDetl.GetColumnValue(grdRcptDetl.ActiveRow, "RcvrLineKey")))
        Case ctl Is txtPOLine
            ETWhereClause = "POLineKey = " & CStr(glGetValidLong(moDmDetl.GetColumnValue(grdRcptDetl.ActiveRow, "POLineKey")))
        Case ctl Is lkuTransfer
            ETWhereClause = "TrnsfrOrderKey = " & CStr(glGetValidLong(moDmHeader.GetColumnValue("TrnsfrOrderKey")))
        Case Else
            ETWhereClause = ""
    End Select
    
    Err.Clear
    
End Function

Private Sub SaveSuccessful()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine cleans up the grid after a successful save.
'************************************************************************
Dim lRowCtr As Long

For lRowCtr = 1 To grdRcptDetl.MaxRows
    gGridUpdateCell grdRcptDetl, lRowCtr, kColSysGenTranID, "0"
    gGridUpdateCell grdRcptDetl, lRowCtr, kColInvtTranID, CStr(0)
    gGridUpdateCell grdRcptDetl, lRowCtr, kColOrigTranKey, gsGridReadCell(grdRcptDetl, lRowCtr, kColInvtTranKey)
    gGridUpdateCell grdRcptDetl, lRowCtr, kColInvtTranDirty, "0"
    gGridUpdateCell grdRcptDetl, lRowCtr, kColOrigRtrnType, giGetValidInt(gsGridReadCell(grdRcptDetl, lRowCtr, kColReturnType))
Next
    
SetRcptEntryFieldsEnabled False

    mbSaveSuccessful = True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SaveSuccessful", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub ResetToInvalidRow(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine positions to the invalid row
'************************************************************************
Dim oControl As Object
    grdRcptDetl_Click 1, lRow
    
    Set oControl = cmdDist
    SetCtrlFocus oControl
    Set oControl = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetToInvalidRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub SaveFailed()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine resets the grid after an unsuccessful save.
'************************************************************************
Dim lRowCtr As Long

    For lRowCtr = 1 To grdRcptDetl.MaxRows
        If gsGridReadCell(grdRcptDetl, lRowCtr, kColSysGenTranID) = "1" Then
            gGridUpdateCell grdRcptDetl, lRowCtr, kColSysGenTranID, "0"
            gGridUpdateCell grdRcptDetl, lRowCtr, kColInvtTranID, CStr(0)
        End If
        gGridUpdateCell grdRcptDetl, lRowCtr, kColInvtTranKey, gsGridReadCell(grdRcptDetl, lRowCtr, kColOrigTranKey)
    Next

    'set back the mlInvtTranKey.  fixed #13907
    mlInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, grdRcptDetl.ActiveRow, kColOrigTranKey))


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SaveFailed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************


    Select Case sKey
        Case kTbFinish, kTbFinishExit
            If Not bProcessBeforeSave() Then Exit Sub
            'Will be set to true in SaveSuccessful routine (requires DM finish to return success).
            mbSaveSuccessful = False
            If (moDmHeader.Action(kDmFinish) = kDmSuccess) Then
                SaveSuccessful
                ClearFields
                moProject.ResetJobGrid
            Else
            ' if the save is not successful, clean up and prevent the exit from happening
                SaveFailed
                
                If mlInvalidRow > 0 Then
                    ResetToInvalidRow mlInvalidRow
                    mlInvalidRow = 0
                End If
                
                sKey = kTbFinish
            End If
            
        Case kTbSave
            If Not bProcessBeforeSave() Then Exit Sub
            'Will be set to true in SaveSuccessful routine (requires DM.Save to return success).
            mbSaveSuccessful = False
            If (moDmHeader.Save(True) = kDmSuccess) Then
                SaveSuccessful
                '-- Because of DM interaction w/ new controls, weird stuff
                '-- happens on Save; set form clean as it should be
                moDmHeader.SetDirty False, True
                
                moLE.InitDataLoaded
            Else
                SaveFailed
                
                If mlInvalidRow > 0 Then
                    ResetToInvalidRow mlInvalidRow
                    mlInvalidRow = 0
                End If
            End If

        Case kTbCancel, kTbCancelExit
            '-- Even though we are not saving, the LE needs to be pushed into the grid since
            '-- there may be IM TranIDs that need to be cleaned up on the cancel.
            '-- First if the PO Lookup is not valid, clear the control
            Me.SOTAValidationMgr1.StopValidation
            If Not lkuPONo.IsValid Then lkuPONo.Text = ""
            If Not lkuReceiver.IsValid Then lkuReceiver.Text = ""
            bProcessBeforeSave
            '-- Clear the form
            ProcessCancel
            moProject.ResetJobGrid
            Me.SOTAValidationMgr1.StartValidation
        
        Case kTbDelete
            If (moDmDetl.Action(kDmDelete) = kDmSuccess) Then
                If (moDmHeader.Action(kDmDelete) = kDmSuccess) Then
                    ClearFields
                    moProject.ResetJobGrid
                End If
            End If

        Case kTbNextNumber
            HandleNextNumberClick
            moProject.ResetJobGrid
        
        Case kTbMemo
            '-- The Memo button was pressed by the user
            CMMemoSelected lkuReceiptNo
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case kTbCopyFrom
            '-- this is a future enhancement to Data Manager
            'moDmHeader.CopyFrom

        Case kTbRenameId
            moDmHeader.RenameID
            
        Case kTbFilter
            miFilter = giToggleLookupFilter(miFilter)
            
        '-- Trap the browse control buttons
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            HandleBrowseClick sKey
                        
        Case Else
            tbrMain.GenericHandler sKey, Me, moDmHeader, moClass
            
    End Select

    If (sKey = kTbFinishExit) Or (sKey = kTbCancelExit) Then
        '-- Unload myself of things that have often bothered me
        '-- Hide the main form and cancel the shutdown
        moClass.moFramework.SavePosSelf
        moClass.miShutDownRequester = kFrameworkShutDown
        mbCancelShutDown = True
        Me.Hide
    Else
        If Not bSetupReceiver Then Exit Sub
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
            
Private Function bProcessBeforeSave() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bProcessBeforeSave = False
    
    '-- Make sure all controls on the form are validated
    If Not SOTAValidationMgr1.ValidateForm Then Exit Function
    
    '-- To remedy a strange bug (ICS #13848) where line entry does not handle
    '-- the last row in the grid if it is blank
' MVB - I don't know if I need this
'    UndoNewBlankRow
            
    '-- If the grid is in edit/add state, force a save at this point
    If (moLE.GridEditDone() <> kLeSuccess) Then Exit Function

    bProcessBeforeSave = True
    

                       
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessBeforeSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub HandleNextNumberClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sNextRcptNo As String
    If Not bConfirmUnload(0) Then Exit Sub
    
    '-- Clear the form
    ProcessCancel
    
    '-- Retrieve the next Receipt Number now
    sNextRcptNo = sGetNextRcptNo()
    
    If (Len(Trim$(sNextRcptNo)) > 0) Then
        '-- Assign the next Receipt No to the control
        If mlTranType <> kTranTypePORTrn Then
            lkuReceiptNo = sNextRcptNo
        Else
            lkuReturn = sNextRcptNo
        End If
        
        '-- Fire the KeyChange event now
        mbAllowKeyChange = True
        SOTAValidationMgr1_KeyChange
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleNextNumberClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleBrowseClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRet As Long
    Dim sNewKey As String
    Dim vParseRet As Variant
    Dim sTranNo As String
    Dim lkuCtrl As TextLookup

    If Not mbInBrowse Then
        If Not bConfirmUnload(0) Then Exit Sub
    End If
    
    If mlTranType <> kTranTypePORTrn Then
        Set lkuCtrl = lkuReceiptNo
    Else
        Set lkuCtrl = lkuReturn
    End If
    
    lRet = glLookupBrowse(lkuCtrl, sKey, miFilter, sNewKey)
    sTranNo = lkuCtrl.Text
    
    '-- Evaluate outcome of requested browse move
    Select Case lRet
    
        Case MS_SUCCESS
        
            If lkuCtrl.ReturnColumnValues.Count = 0 Then
                Exit Sub
            End If

            If StrComp(Trim(sTranNo), Trim(lkuCtrl.ReturnColumnValues("TranNo")), vbTextCompare) <> 0 Then
                '-- Assign the returned number
                lkuCtrl.Text = Trim(lkuCtrl.ReturnColumnValues("TranNo"))
            End If
            
            '-- Fire the KeyChange event now
            mbAllowKeyChange = True
            SOTAValidationMgr1_KeyChange

        Case Else
            '-- Error processing
            mbInBrowse = True
            gLookupBrowseError lRet, Me, moClass
            mbInBrowse = False
    
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleBrowseClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub CancelIMDists()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lLoop As Long
    Dim lTranID As Long
    Dim lInvtTranKey As Long
    Dim arrInvtTranKey() As Long
    Dim lIndex As Long
    
    If Not (moIMDistribution Is Nothing) Then
        '-- For every row in the grid. Check if the row has a Invt tran ID.  If so, cancel that tran id
        lIndex = -1
        For lLoop = 1 To grdRcptDetl.MaxRows - 1
            lTranID = glGetValidLong(gsGridReadCell(grdRcptDetl, lLoop, kColInvtTranID))
            If lTranID > 0 Then
                moIMDistribution.Cancel lTranID
                If moIMDistribution.oError.Number <> 0 Then
                    giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                    moIMDistribution.oError.Clear
                End If
                'An InvtTranKey may have been reserved without the receipt line being saved.  Build a list of
                'InvtTranKeys on the receiver so that the DeleteDistReceipt procedure can be called.
                lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lLoop, kColInvtTranKey))
                If lInvtTranKey <> 0 Then
                    'Add the InvtTranKey to the list only if there are no distributions for it.  If there are any distributions, the leave
                    'the InvtTranKey alone.  A reserved InvtTranKey that was not saved should have no distributions.
                    If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "timInvtTranDist WITH (NOLOCK)", "InvtTranKey=" & lInvtTranKey)) = 0 Then
                        lIndex = lIndex + 1
                        ReDim Preserve arrInvtTranKey(lIndex)
                        arrInvtTranKey(lIndex) = lInvtTranKey
                    End If
                End If
            End If
        Next
        
        'If any InvtTranKeys were found, call the DeleteDistReceipt procedure to ensure that the
        'TranStatus for the InvtTranKeys is set to void.  Since the arrInvtTranKey array was built from
        'InvtTranKeys without distributions, the DeleteDistReceipt call should only end up updating
        'timInvtTranLog.TranStatus to void, and shoudl have no reciepts to process.
        If lIndex >= 0 Then
            moIMDistribution.voidreservedinventoryTransactions (arrInvtTranKey())
            If moIMDistribution.oError.Number <> 0 Then
                giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                moIMDistribution.oError.Clear
            End If
        End If
        
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CancelIMDists", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub ProcessCancel()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lrcptkey As Long
    Dim bValid As Boolean
    
    
    '-- First I need to cancel any IM Dist transactions not saved.
    CancelIMDists
    
    With moDmHeader
        '-- If the current record is a new receiver, save the
        '-- Rcvr so we can delete the unnecessary receiver log record
        If (.State = kDmStateAdd) Then
            lrcptkey = glGetValidLong(.GetColumnValue("RcvrKey"))
        End If

        .Action kDmCancel

        If (mlVRunMode = kEnterReceiptDTE) Or (mlVRunMode = kEnterReceiptAOF) Or _
           (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
            '-- Delete the Receiver Log row if still in 'Incomplete' status
            If .State = kDmStateNone Then 'balzer
                If mlRcvrKey <> lrcptkey Then lrcptkey = mlRcvrKey
            End If
            bValid = bDeleteReceiverLog(lrcptkey, kvIncompleteReceiverLog)
            mlRcvrKey = 0
        End If
    End With
    
    '-- Clear out controls not cleared by Data Manager
    ClearFields

    '-- Refresh lookup restrict clauses that depend on VendKey
    RefreshVendDepends
    RefreshShipWhseDepends
    
    If mlTranType <> kTranTypePORTrn Then
        SetCtrlFocus lkuReceiptNo
    Else
        SetCtrlFocus lkuReturn
    End If
    ' -- Prevent an extra line from displaying in the grid
'    grdRcptDetl.MaxRows = grdRcptDetl.MaxRows - 1
'+++ VB/Rig Begin Pop +++
    
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "ProcessCancel", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub ClearFields()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************
' Desc: Clear out fields and variables and start afresh!
'*********************************************************
    mbLoadingRec = False
    mbFromNav = False
    mbAllowKeyChange = True
    mbLoadingPORow = False
    mbAdjToLandedCostAmtsRequired = False
    mbUserClickedLandedCost = False
    mbDisplayMsgPOLandCostAutoAdjAmtOnEdit = False
    mbAllRcvrLinesWereOnceDeleted = False
    mbSaveSuccessful = False
    mbPOCleared = False
    msApplyFromTranID = ""
    lkuVendID.Tag = ""
    ddnPurchCompany.SetToDefault
    lkuShipWhse.Tag = ""
    
    '-- Clear Vendor Defaults structure
    With muVendDflts
        .lVendKey = 0
        .lPrimaryAddrKey = 0
        .lDfltPurchAcctKey = 0
        .lDfltItemKey = 0
        .bLoadedDfltItem = False
    End With
        
    '-- Disable the PO Lines button
    ToggleCtrlState cmdLines, False
    
    '-- Clear PO-specific fields
    If mlTranType = kTranTypePORG Then
        mlOrigPOKey = 0
        ClearPODefaultsStruct
    ElseIf mlTranType = kTranTypePOTR Then
        mlOrigTransferKey = 0
        ClearTransferDefaultsStruct
        txtShipWhseDesc = ""
        txtTransit = ""
        msOldlkuTransferText = ""
        lkuShipWhse.Text = ""
        lkuShipWhse.KeyValue = 0
        RefreshShipWhseDepends
    Else 'kTranTypePORTrn
        mlOrigRcvrKey = 0
        ClearRcvrDefaultsStruct
    End If
    
    ClearDetlFields True
    
    '-- Initialize the data in the Line Entry object
    moLE.InitDataReset
    
    '-- Set the detail controls as valid so the old values are correct
    SOTAValidationMgr1.Reset
        
    If mlTranType <> kTranTypePORTrn And mbWMIsLicensed Then
        lkuReceiptNo.Protected = False
        ddnType.Enabled = True
        SetRcptEntryFieldsEnabled False
        SetCtrlFocus lkuReceiptNo
    Else
        lkuReturn.Protected = False
        SetCtrlFocus lkuReturn
    End If
    
    lkuAutoDistBin.ClearData
    If Not moIMDistribution Is Nothing Then
        'If the batch does not have a warehouse, the distribution object will not be instanciated.
        moAppDB.ExecuteSQL "TRUNCATE TABLE #timInvtDistWrk"
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "ClearFields", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bConfirmUnload(iConfirmUnload As Integer) As Boolean
    Dim bValid As Boolean
    
    bConfirmUnload = False
    
    iConfirmUnload = moDmHeader.ConfirmUnload()

    If (iConfirmUnload = kDmSuccess) Or (iConfirmUnload = kDmNotAllowed) Then
        If (moDmHeader.State = kDmStateNone) Then
            ProcessCancel
        End If
        
        bConfirmUnload = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bConfirmUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sGetNextRcptNo() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
'   Description:
'           Given the Company ID, get and assign the next Receipt Number.
'   Parameters:
'           oDB <in>:  The database variable to use.
'           sCompanyID <in>:  The Company ID to read tciTranTypeCompany with.
'           lNumCharsInMask <in>:  The Number of characters in the
'                                  Receiver mask.
'           lTranType <in>:  The Transaction Type to get the next Number for.
'           sNextRcptNo <out>:  The next Receipt Number to use.
'   Returns:
'           True if sp was successful, false if the stored procedure failed.
'*****************************************************************************
    Dim lRet As Long
    Dim sRcptNo As String

    sGetNextRcptNo = ""
    
    With moAppDB
        .SetInParam msCompanyID
        .SetInParam 10
        .SetInParam mlTranType
        .SetOutParam sRcptNo
        .SetOutParam lRet
        .ExecuteSP "sppoGetNextRcptNo"
        
        If (Err.Number <> 0) Then
            giSotaMsgBox Me, moSysSession, kmsgGetVouchNoError, msRcvrNoLbl, msRcvrNoLbl
        Else
            sGetNextRcptNo = glGetValidLong(.GetOutParam(4))
            lRet = glGetValidLong(.GetOutParam(5))
        End If
        
        .ReleaseParams
    End With
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetNextRcptNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub CMMemoSelected(oCtl As Control)
'Called from  HandleToolBarClick
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iReply As Integer
    Const vNoClear = True
    Dim lVendKey As Long
    Dim lRcvrKey As Long
    Dim oToolbarMenu As clsToolbarMenuInfo
    
    lVendKey = gvCheckNull(moDmHeader.GetColumnValue("VendKey"), SQL_INTEGER)
    lRcvrKey = gvCheckNull(moDmHeader.GetColumnValue("RcvrKey"), SQL_INTEGER)

    If oCtl Is lkuReceiptNo Then
        gLaunchMemo Me, moClass.moFramework, moSotaObjects, kEntTypePOReceipt, lRcvrKey, _
                    "Receipt:", lkuReceiptNo.Text, moClass.moSysSession.BusinessDate
        
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypePOReceipt)
        oToolbarMenu.lKey2 = lRcvrKey
        
        gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain    ' Set in case change in memos
    End If

    If oCtl Is lkuVendID Then
        'Abort if empty
        If Len(Trim(lkuVendID)) = 0 Then Exit Sub
        gLaunchMemo Me, moClass.moFramework, moSotaObjects, kEntTypeAPVendor, lVendKey, _
                    "Vendor: ", lkuVendID.Text, moClass.moSysSession.BusinessDate
        
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeAPVendor)
        oToolbarMenu.lKey2 = lVendKey
        
        gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain   ' Set in case change in memos
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMemoSelected", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkPreferredBin_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPreferredBin, True
    #End If
'+++ End Customizer Code Push +++

    If chkPreferredBin.Value = vbChecked Then
        'Disable the header bin lookup.
        lkuReceiveToBin.Enabled = False
        lkuReceiveToBin.SetTextAndKeyValue "", 0
    Else
        lkuReceiveToBin.Enabled = True
        
        'If the bin is blank, set the bin to the previous value (if any).
        If glGetValidLong(lkuReceiveToBin.KeyValue) = 0 Then
            lkuReceiveToBin.SetTextAndKeyValue msPrevReceiveToBinID, mlPrevReceiveToBinKey
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkPreferredBin_Click", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chkPreferredBinDetl_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPreferredBinDetl, True
    #End If
'+++ End Customizer Code Push +++
    
        If Not moLE.RowAction Then
            'Retrieve the preferred bin info and set the ID and Key values of the put away bin control.
            SetDetlPreferredBin CBool(chkPreferredBinDetl.Value), mlItemKey, moClass.mlWhseKey, grdRcptDetl.ActiveRow, True
        
        '-- Turn on the detail command buttons
            moLE.GridEditChange chkPreferredBinDetl
        End If

End Sub

'SGS DEJ Custom Event Method (START)
Private Sub cmd_RailDist_Click() 'SGS DEJ IA
    'Launch the Screen
    Dim oDrillDown      As Object
    Dim lContractKey    As Long
    Dim lTask       As Long
    Dim sClass      As String
    Dim iTranType   As Integer
    Dim lLineKey    As Long
    Dim lRow As Long
    
    'Require saving before entering Rail Car Distribution.
    If moDmHeader.IsDirty = True Then
        If MsgBox("Do you want to save your changes?  You must save before entering the Rail Car Distribution.", vbQuestion + vbYesNo, "MAS 500 Save?") = vbYes Then
            HandleToolbarClick kTbSave
            
            If moDmHeader.IsDirty = True Then Exit Sub
        Else
            Exit Sub
        End If
    End If
    
    On Error GoTo ExpectedErrorRoutine
    
    lRow = grdRcptDetl.ActiveRow
    lLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey))
    
    
    'Make sure a line record is selected
    If lLineKey > 0 Then
        
        'Require the Landed Cost be entred.
        If 0 >= glGetValidLong(moClass.moAppDB.Lookup("Count(*)", "tpoRcvrLineDistExt_SGS", "RcvrLineKey = " & lLineKey & " And LandCostKey Is Not Null ")) Then
            MsgBox "You must enter the landed cost first.", vbExclamation, "MAS 500"
            Exit Sub
        End If
        
        
        lTask = 1010089004  'RKL DEJ 1/28/15 Change to 1010089004
        sClass = "pozxxRailDist.clspozxxRailDist"
        Set oDrillDown = goGetSOTAChild(moClass.moFramework, moSotaObjects, sClass, lTask, kAOFRunFlags, kContextAOF)
    End If
    If Not oDrillDown Is Nothing Then
        Me.Enabled = False
        oDrillDown.DrillDown lLineKey
        Set oDrillDown = Nothing
        Me.Enabled = True
        If Me.Visible Then Me.SetFocus
    End If
    
    Exit Sub

ExpectedErrorRoutine:
    MsgBox Me.Name & ".cmd_RailDist_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Me.Enabled = True
    Me.SetFocus
    If Not (oDrillDown Is Nothing) Then
        Set oDrillDown = Nothing
    End If
    gClearSotaErr
    Exit Sub
End Sub
'SGS DEJ Custom Event Method (STOP)

Private Sub cmdPOLandedCost_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdPOLandedCost, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
On Error GoTo ExpectedErrorRoutine
    
    Dim lRcvrKey As Long
    Dim iRet As Integer
    
    'Flag use to tell if the DM save is caused by user clicking the Landed Cost button.  If true, it will not try to auto adjust
    'the landed cost amounts (if edits have been detected) since the user is about to view the landed cost UI and the amounts will be
    're-calculated anyways.
    mbUserClickedLandedCost = True

    If moDmDetl.IsDirty Then
        iRet = giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveBeforeAdd, gsStripChar(gsStripChar(msFormCaption, "."), "&"))
        If iRet = vbYes Then
            'Force a save of the receipt before showing the Enter Landed Cost UI because receipt lines must first exist
            'and also up to date (Edit Mode) so the landed cost amounts are properly calculated.
            HandleToolbarClick kTbSave
            If Not mbSaveSuccessful Then
                mbUserClickedLandedCost = False
                Exit Sub
            End If
        Else
            mbUserClickedLandedCost = False
            Exit Sub
        End If
    End If
    
    If moPOEnterLandedCost Is Nothing Then
        Set moPOEnterLandedCost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kclsPOEnterLandedCost, ktskPOEnterLandedCost, kAOFRunFlags, kContextAOF)
                            
        If Not moPOEnterLandedCost Is Nothing Then
            If Not moPOEnterLandedCost.InitEnterLandedCost(moClass) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgFatalError
                mbUserClickedLandedCost = False
                Exit Sub
            End If
        End If
    End If
    
    'Display the Enter Landed Cost UI.
    If Not moPOEnterLandedCost Is Nothing Then
        lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        If lRcvrKey > 0 Then
            'Since we always require the receipt to first be saved, the DM state is edit.
            'Only time the DM State passed in is "new" is if the user creates a new receipt and
            'saves the transaction without going into Enter Landed Cost, building the LC trans silently.
            'This info is used to tell the routine that gets the data to only pickup the LC trans
            'from the permanent LC tran tables instead of loading the default LCs defined in Setup Landed Cost.
            moPOEnterLandedCost.bEnterLandedCostForRcvr lRcvrKey
        End If
    End If
    
    mbUserClickedLandedCost = False
Exit Sub

ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "cmdPOLandedCost_Click"
gClearSotaErr
Exit Sub

End Sub

Private Sub cmdVendPerf_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdVendPerf, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
     
    'Process Vendor Performance for header in edit mode
    frmVendPerfHdr.bEditVendPerfHdr moDmHeader, moClass, lkuReceiptNo.Text, lkuPONo.Text, _
        lkuVendID.Text, txtVendName.Text, False
         
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdVendPerf_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerfDtl_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdVendPerfDtl, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim sWhseOrDept As String

    sWhseOrDept = gsGetValidStr(txtWhse.Text)
    If Len(sWhseOrDept) = 0 Then
        sWhseOrDept = gsGetValidStr(txtDept.Text)
    End If
     
    'Process Vendor Performance for lines in edit mode.
    If frmVendPerfDtl.bEditVendPerfDtl(muVendPerfDtl, moClass, sWhseOrDept, lkuReceiptNo.Text, _
        txtPONum.Text, txtPOLine.Text, txtItemOrdID.Text, txtItemDescription.Text, False) Then
        
        If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
            moLE.GridEditChange cmdVendPerfDtl
        End If
    End If
    
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdVendPerfDtl_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnType, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    With ddnType
        If .Text <> .Tag Then
            SetUpReceiptTypeFields
        End If
        .Tag = .Text
    End With
    SetCtrlFocus Me.lkuReceiptNo
End Sub


Private Sub cmdAdditionalNotes1_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdAdditionalNotes1, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
On Error GoTo ExpectedErrorRoutine
    
'    If bDontClick Then
''+++ VB/Rig Begin Pop +++
''+++ VB/Rig End +++
'        Exit Sub
'    End If
    
    Dim oNotes As Object
    Set oNotes = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                        kClsSelEditCommentsAOF, ktskSelEditCommentsAOF, kAOFRunFlags, kContextAOF)
    If Not oNotes Is Nothing Then
        oNotes.EditComments glGetValidLong(moDmHeader.GetColumnValue("RcvrKey")), kEntTypePOReceiver
    End If

Exit Sub

ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "cmdNotes_Click"
gClearSotaErr
Exit Sub

End Sub

Private Sub cmdCommentDtl_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdCommentDtl, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
On Error GoTo ExpectedErrorRoutine
    
'    If bDontClick Then
'        Exit Sub
'    End If
    
    Dim oNotes As Object
    Set oNotes = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kClsSelEditCommentsAOF, ktskSelEditCommentsAOF, kAOFRunFlags, kContextAOF)
    If Not oNotes Is Nothing Then
        oNotes.EditComments muRcvrLine.lRcvrLineKey, kEntTypePORcvrLine
    End If

Exit Sub

ExpectedErrorRoutine:
MyErrMsg frmEnterRcptOfGoods.moClass, Err.Description, Err, sMyName, "cmdCommentDtl_Click"
gClearSotaErr
Exit Sub


End Sub

Private Sub cmdOK_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdOK, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'           cmdOK is Enabled only if the detail data contained in the
'           detail edit controls have been altered.
'************************************************************************
    Dim lOldRow As Long
    
    lOldRow = moLE.ActiveRow
    
    If bDenyControlFocus() Then Exit Sub
    
    moLE.GridEditOk

    '-- Make sure we are adding a new Job Line row;
    '-- also make sure that LECheckRequiredData did not fail
    If (moLE.State = kGridAdd) And (lOldRow <> moLE.ActiveRow) Then
        moProject.SetJobGridRow 0
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdOK_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOK_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdOK, True
    #End If
'+++ End Customizer Code Push +++
    If bDenyControlFocus() Then Exit Sub
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdOK_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDist, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim bProcTemp As Boolean
    Dim bValid As Boolean
    Dim dRetTranQyt As Double
    Dim lRcvrKey    As Long
    Dim lInvtTranKey As Long
    Dim lRow As Long
    Dim recordExist As Long
    Dim sSQL As String
    Dim rs As Object
    Dim lDfltWhseBinKey As Long
    Dim bUsePreferredBin As Boolean
    Dim bMultipleBin As Boolean
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim lTranIdentifier As Long
            
    bValid = True
    bProcTemp = (mlInvtTranKey = 0)
    
    SetHourglass True
    lRow = grdRcptDetl.ActiveRow
    
    lInvtTranKey = mlInvtTranKey
    If lInvtTranKey = 0 Then
        lInvtTranKey = lGetInvtTranKey(lRow)
        If lInvtTranKey = 0 Then
            bValid = False
            Exit Sub
        End If
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(lInvtTranKey)
        moDmDetl.SetColumnValue lRow, "InvtTranKey", lInvtTranKey
        mlInvtTranKey = lInvtTranKey
    End If
    
    'User previously cancelled or 'x' out of the distributions.  Set it back to zero.
    '*See notes below.
    If mlIMTempTranID = -1 Then
        mlIMTempTranID = 0
    End If
    
    If mlTranType = kTranTypePOTR Then
        lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        lDfltWhseBinKey = glGetValidLong(lkuAutoDistBin.KeyValue)
        bUsePreferredBin = gbGetValidBoolean(chkPreferredBinDetl.Value)

        lTranIdentifier = moIMDistribution.ReceiveTransferDist(mlTranType, _
                                            mlWhseKey, _
                                            mlTrnsfrOrderLineKey, _
                                            lRcvrKey, _
                                            gdGetValidDbl(nbrQtyRcvd), _
                                            mlUOMKey, _
                                            dRetTranQyt, _
                                            mlIMTempTranID, _
                                            mlInvtTranKey, _
                                            bUsePreferredBin, _
                                            lDfltWhseBinKey, _
                                            True) 'Show UI
                                            
    Else
        'If the user selected a bin to auto-distribute then clicked the Dist button,
        'do the auto-distribution now so they can see it when the dialog appears.
        bAutoDistFromBinLku lRow, True
        
        mlIMTempTranID = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
        lTranIdentifier = moIMDistribution.EditDistribution(mlTranType, mlItemKey, mlUOMKey, gdGetValidDbl(nbrQtyRcvd), dRetTranQyt, mlIMTempTranID, mlInvtTranKey, , , , , , , , glGetValidLong(lkuAutoDistBin.KeyValue))
        gGridUpdateCell grdRcptDetl, lRow, kColOrigTranKey, CStr(mlInvtTranKey)
    End If
    
    If moIMDistribution.CancelClicked <> True Then
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranID, CStr(lTranIdentifier)
        mlIMTempTranID = lTranIdentifier
        
        'Set the value of the auto-dist bin lookup based on what is saved during EditDistribution.
        GetAutoDistSavedInfo grdRcptDetl.ActiveRow
    End If
    
    SetHourglass False
    
    If moIMDistribution.oError.Number <> 0 Then
        giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
        moIMDistribution.oError.Clear
        bValid = False
    End If
    
    '*User did not perform any distributions.  They either cancelled or hit the 'x' to close the form.
    'By doing so, the mlIMTempTranID was set to zero.  However, we still need the app to think the
    'row is dirty.  It was not being set to dirty if the mlIMTempTranID returned a zero. Couldn't figure
    'out why so decided to just set it to a non-zero number.
    If mlIMTempTranID = 0 Then
        mlIMTempTranID = -1
    End If
    
    If (mlIMTempTranID <> 0) And bValid Then
        If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
            moLE.GridEditChange chkMatch
        End If
    End If
    
    If mlIMTempTranID <> 0 Then
        miIMDistDirty = 0     '  that the IM stuff is dirty
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDist_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdUndo_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdUndo, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'           cmdUndo is Enabled only if the detail data contained in the
'           detail edit controls have been altered.
'************************************************************************
    If bDenyControlFocus() Then Exit Sub
    
    moLE.GridEditUndo
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdUndo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
        
Private Sub UndoNewBlankRow()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Implicitly undo a new blank row in the grid on certain user actions
    '-- (tab change, save...)
    If (moLE.ActiveRow > grdRcptDetl.DataRowCnt) And _
       (Len(Trim$(txtItemOrdID)) = 0) Then
        moLE.GridEditUndo
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UndoNewBlankRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdUndo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdUndo, True
    #End If
'+++ End Customizer Code Push +++
    If bDenyControlFocus() Then Exit Sub
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdUndo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub InitializeDetlGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************
' Desc: Initializes/formats the Detail grid.
'*******************************************
    Dim iGridType As Integer
    Dim sTitle As String

    '-- Set default grid properties
    gGridSetProperties grdRcptDetl, kGridMaxCols, iGetGridType()
    gGridSetProperties grdRcvrLineDist, kGridDistMaxCols, iGetGridType()

    gGridSetProperties grdJobLine, kMaxColsJ, iGetGridType()

    '-- Set column headers
    sTitle = gsBuildString(ksLine, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColPOLineNo, sTitle
    
    sTitle = gsBuildString(kItem, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColMaskedItemID, sTitle
    
    sTitle = gsBuildString(kDescription, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColItemDesc, sTitle
    
    sTitle = gsBuildString(kAnUOM, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColUnitMeasRcvdID, sTitle
    
    If mlTranType <> kTranTypePORTrn Then
        sTitle = gsBuildString(ksQtyOrdered, moAppDB, moSysSession)
        gGridSetHeader grdRcptDetl, kColOrigQtyCurrUOM, sTitle
    
        sTitle = gsBuildString(ksQtyReceived, moAppDB, moSysSession)
    Else
        sTitle = gsBuildString(ksQtyReceived, moAppDB, moSysSession)
        gGridSetHeader grdRcptDetl, kColOrigQtyCurrUOM, sTitle
        sTitle = gsBuildString(ksQtyReturned, moAppDB, moSysSession)
    End If
    gGridSetHeader grdRcptDetl, kColQtyRcvd, sTitle
    
    
    sTitle = gsBuildString(kSOPutAwayBin, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColAutoDistBin, sTitle
    
    sTitle = gsBuildString(kSOUsePrefBin, moAppDB, moSysSession)
    gGridSetHeader grdRcptDetl, kColUsePreferred, sTitle
    
    'RKL DEJ 1/27/14 (START)
    gGridSetHeader grdRcptDetl, kColUnitCost, "UnitCost"
    gGridSetColumnWidth grdRcptDetl, kColUnitCost, 15
    gGridSetColumnType grdRcptDetl, kColUnitCost, SS_CELL_TYPE_FLOAT, giGetValidInt(moClass.moAppDB.Lookup("UnitCostDecPlaces", "tciOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    'RKL DEJ 1/27/14 (STOP)
    
    '-- Set column widths
    gGridSetColumnWidth grdRcptDetl, kColPOLineNo, 5
    gGridSetColumnWidth grdRcptDetl, kColMaskedItemID, 20
    gGridSetColumnWidth grdRcptDetl, kColItemDesc, 26
    gGridSetColumnWidth grdRcptDetl, kColUnitMeasRcvdID, 8
    gGridSetColumnWidth grdRcptDetl, kColQtyOrdered, 12
    gGridSetColumnWidth grdRcptDetl, kColQtyRcvd, 12
    gGridSetColumnWidth grdRcptDetl, kColOrigQtyCurrUOM, 12
    gGridSetColumnWidth grdRcptDetl, kColOrigQtyRcvd, 12
    gGridSetColumnWidth grdRcptDetl, kColAutoDistBin, 15
    
    '-- Hide the appropriate columns
    gGridHideColumn grdRcptDetl, kColPOLineKey
    gGridHideColumn grdRcptDetl, kColTrnsfrLineKey
    gGridHideColumn grdRcptDetl, kColItemKey
    gGridHideColumn grdRcptDetl, kColItemID
    gGridHideColumn grdRcptDetl, kColUnitMeasRcvdKey
    gGridHideColumn grdRcptDetl, kColQtyRcvdS
'    gGridHideColumn grdRcptDetl, kColUnitCost   'RKL DEJ Unhide column / Comment out this line
    gGridHideColumn grdRcptDetl, kColUnitCostExact
    gGridHideColumn grdRcptDetl, kColMatchStatus
    gGridHideColumn grdRcptDetl, kColDispMatchStatus
    gGridHideColumn grdRcptDetl, kColGLAcctKey
    gGridHideColumn grdRcptDetl, kColAcctRefKey
    gGridHideColumn grdRcptDetl, kColReturnType
    gGridHideColumn grdRcptDetl, kColDispReturnType
    gGridHideColumn grdRcptDetl, kColTranComment
    gGridHideColumn grdRcptDetl, kColSeqNo
    gGridHideColumn grdRcptDetl, kColReceiptLineKey
    gGridHideColumn grdRcptDetl, kColWhseID
    gGridHideColumn grdRcptDetl, kColDeptID
    gGridHideColumn grdRcptDetl, kColUnitMeasOrdKey
    gGridHideColumn grdRcptDetl, kColUnitMeasOrdID
    gGridHideColumn grdRcptDetl, kColTotalRcvd
    gGridHideColumn grdRcptDetl, kColQtyRtrned
    gGridHideColumn grdRcptDetl, kColQtyOpen
    gGridHideColumn grdRcptDetl, kColCloseSrcLine
    gGridHideColumn grdRcptDetl, kColItemVol
    gGridHideColumn grdRcptDetl, kColItemWght
    gGridHideColumn grdRcptDetl, kColInvtTranKey
    gGridHideColumn grdRcptDetl, kColDiscAmt
    gGridHideColumn grdRcptDetl, kColOtherAmt
    gGridHideColumn grdRcptDetl, kColSTaxAmt
    gGridHideColumn grdRcptDetl, kColMeasType
    gGridHideColumn grdRcptDetl, kColQtyInPOUOM
    gGridHideColumn grdRcptDetl, kColInvtTranID
    gGridHideColumn grdRcptDetl, kColInvtTranDirty
    gGridHideColumn grdRcptDetl, kColFreightAmt
    gGridHideColumn grdRcptDetl, kColRtrnRcvrLineKey
    gGridHideColumn grdRcptDetl, kColRtrnReasonKey
    gGridHideColumn grdRcptDetl, kColOrigTranKey
    gGridHideColumn grdRcptDetl, kColSysGenTranID
    gGridHideColumn grdRcptDetl, kColOrigRtrnType
    gGridHideColumn grdRcptDetl, kColQtyShip
    gGridHideColumn grdRcptDetl, kColDefective
    gGridHideColumn grdRcptDetl, kColDamaged
    gGridHideColumn grdRcptDetl, kColCarrierDamage
    gGridHideColumn grdRcptDetl, kColImpLabel
    gGridHideColumn grdRcptDetl, kColDocMissing
    gGridHideColumn grdRcptDetl, kColImpPackage
    gGridHideColumn grdRcptDetl, kColNotToSpec
    gGridHideColumn grdRcptDetl, kColOtherIssue
    gGridHideColumn grdRcptDetl, kColPerfComments
    gGridHideColumn grdRcptDetl, kColAutoDistBin
    gGridHideColumn grdRcptDetl, kColAutoDistBinKey
    gGridHideColumn grdRcptDetl, kColAutoDistInvtLotKey
    gGridHideColumn grdRcptDetl, kColAutoDistLotNo
    gGridHideColumn grdRcptDetl, kColAutoDistLotExpDate
    gGridHideColumn grdRcptDetl, kColAutoDistBinUOMKey
    gGridHideColumn grdRcptDetl, kColDoAutoDist
    gGridHideColumn grdRcptDetl, kColPrefBinKey
    gGridHideColumn grdRcptDetl, kColBinQtyAvail
    
    
    
    gGridHideColumn grdRcptDetl, kColQtyOrdered 'in PO UOM
    gGridHideColumn grdRcptDetl, kColOrigQtyRcvd 'in orig receiver UOM
    gGridHideColumn grdRcptDetl, kColOrigRcvrUOMKey 'UOM of return's original receipt
    
    If Not mbDefaultQty Then
        gGridHideColumn grdRcptDetl, kColOrigQtyCurrUOM
    End If
    
    
    '-- Set the column types
    gGridSetColumnType grdRcptDetl, kColItemDesc, SS_CELL_TYPE_EDIT, 40
    gGridSetColumnType grdRcptDetl, kColTranComment, SS_CELL_TYPE_EDIT, 255
    gGridSetColumnType grdRcptDetl, kColPerfComments, SS_CELL_TYPE_EDIT, 255
    gGridSetColumnType grdRcptDetl, kColQtyOrdered, SS_CELL_TYPE_FLOAT, miQtyDecPlaces
    gGridSetColumnType grdRcptDetl, kColQtyRcvd, SS_CELL_TYPE_FLOAT, miQtyDecPlaces
    gGridSetColumnType grdRcptDetl, kColOrigQtyCurrUOM, SS_CELL_TYPE_FLOAT, miQtyDecPlaces
    gGridSetColumnType grdRcptDetl, kColOrigQtyRcvd, SS_CELL_TYPE_FLOAT, miQtyDecPlaces
    gGridSetColumnType grdRcptDetl, kColUsePreferred, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdRcptDetl, kColAutoDistBin, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdRcptDetl, kColAutoDistLotExpDate, SS_CELL_TYPE_DATE
        
    '-- Don't allow the user to resize the line number column
    grdRcptDetl.Col = 0
    grdRcptDetl.UserResizeCol = SS_USER_RESIZE_OFF
    
    moProject.InitializeJobGrid

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeDetlGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function iGetGridType() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Desc: Return the grid type based on the run mode and security
'       level: Line Entry or No Append.
'****************************************************************
    iGetGridType = kGridLENoAppend
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetGridType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bGetOptions(sCompanyID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iBlindRcpt As Integer
    Dim sSQL As String
    Dim rs As Object
    Dim lWghtUOMKey As Long
    Dim lVolUOMKey As Long
    
    
    bGetOptions = False
    
    mbAllowInterCo = True
    
    iBlindRcpt = CInt(moOptions.PO("BlindRcpt"))
    If mlTranType <> kTranTypePORTrn Then
        mbDefaultQty = (iBlindRcpt < 1) 'ReceiptQtyDflt
        mbFilterByDate = (iBlindRcpt < 2) 'Filter the receipts by date

        mbAutoSelectLines = CBool(moOptions.PO("DfltRcptComplete")) 'Receive Complete
        miCaptureWght = CInt(moOptions.PO("RcvrItemWghtUpdate")) 'Capture Item Weight
    
        miCaptureVol = CInt(moOptions.PO("RcvrItemVolUpdate")) ' Capture Item Volume
    
        mbCloseSrcLine = CBool(moOptions.PO("AllowPOBackOrder")) 'Close PO Line on first receipt
        If Not CBool(moOptions.PO("MatchRcvrToPO")) Then
            chkMatch.Visible = False
        End If
    Else
        mbDefaultQty = True
        mbFilterByDate = False
        mbAutoSelectLines = False
        miCaptureWght = 0
        miCaptureVol = 0
        mbCloseSrcLine = False
        mbMatching = False
    End If
    
    
    miRcptDaysEarly = CInt(moOptions.PO("DfltRcptEarlyDays")) ' Days early that a line can be accepted
    
    miQtyDecPlaces = moOptions.CI("QtyDecPlaces")

    If CBool(moOptions.PO("IntegrateWithIM")) Then
        lVolUOMKey = CLng(moOptions.IM("VolumeUnitMeasKey")) ' Volume UOM Key
        lWghtUOMKey = CLng(moOptions.IM("WeightUnitMeasKey")) ' Weight UOM Key
    End If
    
    If lVolUOMKey = 0 Then
        msVolUOM = gsBuildString(kBudgetOptNone, moClass.moAppDB, moSysSession)
    Else
        sSQL = "SELECT UnitMeasID FROM tciUnitMeasure WHERE UnitMeasKey = " & lVolUOMKey
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            msVolUOM = rs.Field("UnitMeasID")
        Else
            msVolUOM = gsBuildString(kBudgetOptNone, moClass.moAppDB, moSysSession)
        End If
        If Not rs Is Nothing Then
            rs.Close: Set rs = Nothing
        End If
    End If
    
    If lWghtUOMKey = 0 Then
        msWghtUOM = gsBuildString(kBudgetOptNone, moClass.moAppDB, moSysSession)
    Else
        sSQL = "SELECT UnitMeasID FROM tciUnitMeasure WHERE UnitMeasKey = " & lWghtUOMKey
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            msWghtUOM = rs.Field("UnitMeasID")
        Else
            msWghtUOM = gsBuildString(kBudgetOptNone, moClass.moAppDB, moSysSession)
        End If
        If Not rs Is Nothing Then
            rs.Close: Set rs = Nothing
        End If
    End If
    
    '-- Default misc. module-level variables
    SetupModuleVars
    
    bGetOptions = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetOptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub cmdWghtVol_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdWghtVol, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim dVol As Double
    Dim dWght As Double
    Dim bEditVol As Boolean
    Dim bEditWght As Boolean

    If (mdItemVol > 0) Or miCaptureVol = kvCaptureNone Then
        dVol = mdItemVol
        bEditVol = False
    Else
        dVol = mdRcvrVol
        bEditVol = True
    End If
    If mdItemWght > 0 Or miCaptureWght = kvCaptureNone Then
        dWght = mdItemWght
        bEditWght = False
    Else
        dWght = mdRcvrWght
        bEditWght = True
    End If
    
    Load frmWghtVol
    frmWghtVol.InitializeFormInfo txtItemOrdID, ddnUOM.GetListByItemData(ddnUOM.ItemData), _
        msWghtUOM, msVolUOM, dWght, dVol, bEditWght, bEditVol
    frmWghtVol.Show vbModal
    
'    Unload (frmWghtVol)
End Sub

Private Sub ddnPurchCompany_AfterRefresh(ByVal CurrentIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        ddnPurchCompany.AddItem msCompanyID, , kiExtraSpecial
        ddnPurchCompany.DefaultItemData = kiExtraSpecial
        ddnPurchCompany.SetToDefault
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnPurchCompany_AfterRefresh", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPurchCompany_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnPurchCompany, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If NewIndex <> PrevIndex Then
        msPurchCompanyID = ddnPurchCompany.List(NewIndex)
        ddnPurchCompany.Tag = msPurchCompanyID
        lkuVendID.RestrictClause = sGetVendorRestrict
        lkuPONo.RestrictClause = sGetPORestrict
        msHomeCurrID = gsGetValidStr(moClass.moAppDB.Lookup("CurrID", "tsmCompany", "CompanyID = " & gsQuoted(msPurchCompanyID)))
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnPurchCompany_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReason_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnReason, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
         If PrevIndex <> NewIndex Then
            moLE.GridEditChange ddnReason
            SetCtrlFocus ddnReason
        End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnReason_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReason_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnReason, True
    #End If
'+++ End Customizer Code Push +++

'This is here to fix a bug that occurs when the control loses focus.  It was turning grey,
' even though the control was still enabled

    If ddnReason.Enabled Then
        ddnReason.BackColor = vbWindowBackground
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnReason_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub ddnUOM_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnUOM, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long

    'Sometimes, LE/DM will call the SotaDropDown control's click event when switching
    'between rows, usually when the value of the ddn changes from row to row.
    'Exit if this is the case.
    If NewIndex = -1 Or mbGridClicked Then Exit Sub
    
    lRow = grdRcptDetl.ActiveRow
    
    If PrevIndex <> NewIndex Then
        moLE.GridEditChange ddnUOM
        SetCtrlFocus ddnUOM
    
        If bIsValidAutoDistBin(lRow) Then
            If mbValidAutoDistBin Then
                gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "1"
            Else
                gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "0"
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnUOM_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Description:
'    Form_KeyDown traps the hot keys when the KeyPreview property
'    of the form is set to True.
'****************************************************************
    Select Case KeyCode
        Case 189 'vbDash
            If Shift = 2 Then    'vbKeyControl
                If Me.ActiveControl Is txtPONum Then
                    KeyCode = 0
                    If Len(Trim(txtPONum.Text)) > 0 Then
                        txtPONum.Text = sZeroFillNumber(txtPONum.Text, txtPONum.MaxLength)
                        SetCtrlFocus txtRelNo
                    End If
                End If
            End If
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
         
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetTranType()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine sets the transaction type variables.
'************************************************************************

Dim sSQL As String

    Select Case moClass.lTranType
        Case kTypeReceipt
            mlTranType = kTranTypePORG
            miEntType = kEntTypePOReceiver
            miEntLineType = kEntTypePORcvrLine
            miSignFactor = kiPosSignFactor
        
        Case kTypeReturn
            mlTranType = kTranTypePORTrn
            miEntType = kEntTypePOReturn
            miEntLineType = kEntTypePORtrnLine
            miSignFactor = kiNegSignFactor
            
            On Error Resume Next
            sSQL = "DROP TABLE #tpoRtrnJoin"
            moAppDB.ExecuteSQL sSQL
        
            sSQL = "CREATE TABLE #tpoRtrnJoin "
            sSQL = sSQL & "(RcvdLineKey INT NULL, "
            sSQL = sSQL & "RcvdQty DECIMAL(16,8) NULL,"
            sSQL = sSQL & "RtrnRcvrUnitMeasKey INT NULL)"
            moAppDB.ExecuteSQL sSQL
            On Error GoTo VBRigErrorRoutine
            
    End Select
    sSQL = "CompanyID = " & gsQuoted(msCompanyID) & " AND TranType = " & mlTranType
        
    msTranType = Trim(gsGetValidStr(moClass.moAppDB.Lookup("TranTypeID", _
                                    "tciTranTypeCompany", sSQL)))

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetTranType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DeleteIMRow(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    Delete the IM Distribution entries if necessary
'************************************************************************
    

    Dim lInvtTranKey As Long
    Dim lItemKey As Long
    Dim iOrigRtrnType As Integer
    Dim lTranID As Long
    Dim lTrnsfrOrderLineKey As Long
    Dim sSQL As String
    Dim arrInvtTranKeys() As Long
    
    If (moIMDistribution Is Nothing) Then Exit Sub

    lTranID = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
    lItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
    iOrigRtrnType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColOrigRtrnType))
    lTrnsfrOrderLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTrnsfrLineKey))
    
   
    If lTranID > 0 Then
        moIMDistribution.Cancel lTranID
    End If
    
    If mlTranType = kTranTypePORTrn Then
        If lInvtTranKey > 0 Then
            moIMDistribution.Delete lInvtTranKey, lItemKey, , , iOrigRtrnType, lItemKey, , lTrnsfrOrderLineKey
        End If
    Else
        If lInvtTranKey <> 0 Then
            ReDim arrInvtTranKeys(0)
            arrInvtTranKeys(0) = lInvtTranKey
            
            If bIsReceiptDistDeleted(arrInvtTranKeys) = False Then
                giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                moIMDistribution.oError.Clear
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteIMRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub SetupForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description:
'    This routine sets the form based on the transaction type.
'************************************************************************

    If mlTranType = kTranTypePORG Then
        cmdPOLandedCost.Visible = True
        cmdPOLandedCost.Enabled = True
    End If
    
    If mlTranType = kTranTypePORTrn Then
        lblPurchCompany.Visible = False
        ddnPurchCompany.Visible = False
        lblRcptDate.Visible = False
        lblRtrnDate.Visible = True
        calRcptDate.Left = 3800
        lblComment1.Top = 870
        txtTranComment.Top = 840
        txtPONum.Enabled = False
        txtRelNo.Enabled = False
        navPO.Visible = False
        cmdLines.Top = 600
        lblRcptNo.Visible = False
        lkuReceiptNo.Visible = False
        lblQtyRcvd.Visible = False
        lblQuantityReturned.Top = 540
        lblQuantityReturned.Visible = True
        lblType.Visible = False
        ddnType.Visible = False
        cmdPOLandedCost.Visible = False
        cmdPOLandedCost.Enabled = False
        
' update the positions of items on the detail tabs
        tabSubDetl.Tab = kTabOrigOrder
        
        lblQtyOpen.Visible = False
        nbrQtyOpen.Visible = False
        
        lblTotRcvd.Left = 180
        nbrTotRcvd.Left = 180
        lblQtyOrd.Left = 4800
        nbrQtyOrd.Left = 4800
        txtUOM.Left = 6600
        
        lblUOM(1).Visible = False
        lblQtyReturn.Visible = False
        nbrQtyReturn.Left = 3030
        nbrQtyReturn.Width = 1710
        txtUOMRcpt.Top = 330
        lblUOMRcpt.Top = 120
        lblPOUOM.Top = 120
        lblTotalReturn.Top = 120
        
        tabSubDetl.Tab = kTabMisc
        cmdWghtVol.Visible = False
        chkMatch.Visible = False
        chkCloseDistr.Visible = False
        optRetCredit.Visible = True
        optRetCredit.Top = 150
        optRetReplace.Visible = True
        optRetReplace.Top = 450
  

        tabSubDetl.Tab = kTabOrigOrder
        
        ' For localization, this should be read from the string table.
        tabSubDetl.TabCaption(kTabOrigOrder) = gsBuildString(220602, moClass.moAppDB, moSysSession)
              
        lblReason.Visible = True
        ddnReason.Visible = True
        ddnReason.Enabled = False
                
        lblReason.Top = 505
        ddnReason.Top = 750

        lkuAutoDistBin.Visible = True
        txtMultipleBin.Visible = True
        
        txtRMA.Top = 450
        lblRMA.Top = 480
        txtRMA.Visible = True
        lblRMA.Visible = True
        
        lblReceiver.Top = Me.lblRcptDate.Top
        lblReceiver.Left = Me.lblRcptDate.Left
        lkuReceiver.Top = Me.lkuReceiptNo.Top
        lkuReceiver.Left = Me.txtPONum.Left
        lblReceiver.Visible = True
        lkuReceiver.Visible = True
        lblReturn.Top = Me.lblType.Top
        lblReturn.Left = Me.lblType.Left
        lkuReturn.Top = Me.ddnType.Top
        lkuReturn.Left = Me.ddnType.Left
        lblReturn.Visible = True
        lkuReturn.Visible = True
        cmdVendPerf.Visible = False
        cmdVendPerfDtl.Visible = False
    End If

    lkuProject.Enabled = False
    lkuProject.VisibleLookup = False
    
    lkuPhase.Enabled = False
    lkuPhase.VisibleLookup = False
    
    lkuTask.Enabled = False
    lkuTask.VisibleLookup = False

 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Description:
'    Form_KeyPress traps the keys pressed when the KeyPreview property
'    of the form is set to True.
'*********************************************************************
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
    
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupIMInterfaces()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Setup the IMS Class
    moIMSClass.Init moClass.moAppDB, moClass.moAppDB, msCompanyID
    Set moItem = moIMSClass.Items.Item
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupIMInterfaces", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    mbLoadSuccess = False
    

    '-- assign system object properties to module-level variables
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        mbIMActivated = .IsModuleActivated(kModuleIM)
        If CBool(.IsModuleActivated(kModuleWM)) = True Then
            mbWMIsLicensed = True
        Else
            mbWMIsLicensed = False
        End If
    End With
    Set moAppDB = moClass.moAppDB
    
    msPurchCompanyID = msCompanyID
    msHomeCurrID = gsGetValidStr(moClass.moAppDB.Lookup("CurrID", "tsmCompany", "CompanyID = " & gsQuoted(msPurchCompanyID)))
    
    '-- Set up the form resizing variables
    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With
    
    SetTranType
    SetupForm
    
    '-- Set the form caption
    Me.Caption = gsStripChar(msFormCaption, kAmpersand)
    
    '-- Get the Receipt No. label, minus any accelerator
    msRcvrNoLbl = gsStripChar(lblRcptNo, kAmpersand)
    
    '-- Setup necessary properties for Options class
    With moOptions
        Set .oSysSession = moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    
    '-- Retrieve necessary options and set module-level variables
    If Not bGetOptions(msCompanyID) Then Exit Sub
    
    SetupIMInterfaces
    
    '-- Setup the toolbar and status bar
    SetupBars
    
    '-- Setup the necessary lookup properties
    SetupLookups
    MapControls

    lkuProject.RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)

    '-- Setup the necessary combo box properties
    SetupDropDowns
    
    '-- Bind form fields to database fields
    BindForm
    
    '-- Set the security level for this user
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmHeader, moDmDetl, _
                          moDmLineDist)
            
    Set moProject = New clsProject
    moProject.Init JOB_PORCPT, moClass, Me, moSotaObjects, moDmDetl, grdRcptDetl, _
                   moLE, grdJobLine, lkuProject, lkuPhase, lkuTask, , _
                     , , , , , kiProjectTab
            
    '-- Bind the Line Entry class
    BindLE
    
    '-- Initialize the detail grid
    InitializeDetlGrid
    
    '-- Bind the Context Menu
    BindContextMenu
    
    'MVB could use this for toggling between EROG and Returns
    '-- Setup PO-related fields, options, etc.
'    If Not bInitForPO(Me, mlVRunMode, moOptions) Then Exit Sub
    
    '-- Set the initial states/values of fields based on run mode
    SetFieldStates
    
    '-- Set the initial toolbar button states
    SetTBButtonStates
    
    '-- Make sure the header tab is shown to the user first
    tabReceipt.Tab = kiHeaderTab
    
    tabReceipt_Click kiHeaderTab
    
    '-- Grid starts out with weird stuff in it; fix that problem right now!
    grdRcptDetl.MaxRows = 0
    grdRcptDetl.MaxRows = 1
    
'    nbrQtyOrd.Border = BORDERSTYLE_3D

    If Not bSetupReceiver Then Exit Sub
    
    '-- Initialize the Type Combo
    With ddnType
        .AddItem "Purchase"
        .AddItem "Transfer"
        .Tag = .List(0)
        .ListIndex = 0
    End With
    
    If Not mbWMIsLicensed Then
        ddnType.Enabled = False
        
    End If
    
    '-- Disable fields until a new receipt no is entered
    SetRcptEntryFieldsEnabled False
    
    '-- We made it through Form_Load successfully
    mbLoadSuccess = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iConfirmUnload As Integer
    Dim bValid As Boolean
    
    On Error GoTo ExpectedErrorRoutine
   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If (moClass.mlError = 0) Then
        If (mlVRunMode = kEnterReceiptDTE) Then
            If Not moClass.moCTITask Is Nothing Then
                If moClass.moCTITask.CTIQueryUnload(ktskProcRcptOfGoods) Then
                    Cancel = True
                    Exit Sub
                End If
            End If
        End If
        
        If moDmHeader.State = kDmStateEdit Or moDmHeader.State = kDmStateAdd Then
          '-- If the form is dirty, prompt the user to save the record
          '-- (Push the data down into the grid)
          If Not bProcessBeforeSave Then
            SaveFailed
            GoTo CancelShutDown
          End If
          
          bValid = bConfirmUnload(iConfirmUnload)
          
          Select Case iConfirmUnload
              Case kDmSuccess
                  'Do Nothing
                  
              Case kDmFailure, kDmError
                  SaveFailed
                  GoTo CancelShutDown
          
          End Select
        
          '-- Clear the form
          ProcessCancel
        End If
        
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                Select Case mlVRunMode
                    Case kViewEditReceiptDTE
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                    
                    Case Else
                        '-- Hide the main form and cancel the shutdown
                        moClass.moFramework.SavePosSelf
                        ' -- Clear out the fields so that the Receipt No is able to be the initial field
                        If mlTranType <> kTranTypePORTrn Then
                            SetCtrlFocus lkuReceiptNo
                        Else
                            SetCtrlFocus lkuReturn
                        End If
                        
                        '-- Ensure that this temp table is dropped -- might still exist if there were problems
                        '-- during the save operation.  Failing to drop this could cause conflicts with
                        '-- spimPostMiscCleanup during posting.
                        On Error Resume Next
                        moAppDB.ExecuteSQL "DROP TABLE #timInventory "
                        
                        On Error GoTo ExpectedErrorRoutine
                        Me.Hide
                        
                        GoTo CancelShutDown
                    
                End Select
        End Select
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else
            'Do Nothing
            
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub
ExpectedErrorRoutine:
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "Form_QueryUnload", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
        '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
                        grdRcptDetl, tabReceipt
'                        pnlTab(kiHeaderTab), pnlTab(kiDetailTab), _

        '-- Move the controls to the right
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
                        grdRcptDetl, tabReceipt
'                        pnlTab(kiHeaderTab), pnlTab(kiDetailTab), _

    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    
    lkuProject.Terminate
    lkuPhase.Terminate
    lkuTask.Terminate
    
    If Not moProject Is Nothing Then
        moProject.UnloadSelf
        Set moProject = Nothing
    End If
    
    '-- Fire Terminate event for lookups
    lkuReceiptNo.Terminate
    lkuReturn.Terminate
    lkuReceiver.Terminate
    lkuVendID.Terminate
    lkuPONo.Terminate
    moIMSClass.Terminate
    lkuShipWhse.Terminate
    lkuTransfer.Terminate
    Set moIMSClass = Nothing
    
    Set moItem = Nothing
    
    '-- Clean up object references
    If Not moDmHeader Is Nothing Then
        moDmHeader.UnloadSelf
        Set moDmHeader = Nothing
    End If
    
    If Not moDmDetl Is Nothing Then
        moDmDetl.UnloadSelf
        Set moDmDetl = Nothing
    End If
    
    If Not moDmLineDist Is Nothing Then
        moDmLineDist.UnloadSelf
        Set moDmLineDist = Nothing
    End If

    If Not m_MenuInfo Is Nothing Then
        Set m_MenuInfo = Nothing
    End If
    
    
    '-- Clean up Line Entry references
    If Not moLE Is Nothing Then
        moLE.UnloadSelf
        Set moLE = Nothing
    End If
    
    If Not moIMDistribution Is Nothing Then
        Set moIMDistribution = Nothing
    End If
    
    If Not moPOEnterLandedCost Is Nothing Then
        Set moPOEnterLandedCost = Nothing
    End If
    
    '-- Ensure that this temp table is dropped -- might still exist if there were problems
    '-- during the save operation.  Failing to drop this could cause conflicts with
    '-- spimPostMiscCleanup during posting.
    On Error Resume Next
    moAppDB.ExecuteSQL "DROP TABLE #timInventory "
    
    '-- Clean up other objects
    Set moOptions = Nothing
    Set moSotaObjects = Nothing
    Set moContextMenu = Nothing
    Set moAppDB = Nothing
    Set moSysSession = Nothing
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub grdRcptDetl_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'           Select the row that the user Selected.
'   Parameters:
'           Row, Col - The cell coordinates that were clicked.
'***********************************************************************
    UndoNewBlankRow
    
    If (moLE.GridEditDone(Col, Row) <> kLeSuccess) Then
        gGridSetActiveCell grdRcptDetl, moLE.ActiveRow, Col
        Exit Sub
    End If
    
    mbGridClicked = True
    '-- Select the row that the user Selected
    moLE.Grid_Click Col, Row

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_DblClick(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Initiate edit mode where the row contents can be changed.
'           Assumes a Click has preceeded this event.
'   Parameters:
'           Row, Col - The cell coordinates that were clicked.
'************************************************************************
    moLE.Grid_DblClick Col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           A drag-and-drop terminated here, check if the drop should
'           result in an actual movement of a dropped row.
'   Parameters:
'           mlSourceRow - the row from where the drag was initiated.
'           source, x - not used.
'           y - the drop Position.  See VB Manual.
'************************************************************************
    moLE.Grid_DragDrop x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_DragOver(Source As Control, x As Single, y As Single, State As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Indicate whether drop is allowed or disallowed at this point.
'   Parameters:
'           source, etc - See VB Manual.
'************************************************************************
    moLE.Grid_DragOver x, y, State
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_DragOver", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Fires when the Grid gets focus.
'************************************************************************
    moLE.Grid_GotFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub grdRcptDetl_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Provide keyboard interface.
'   Parameters:
'           KeyCode, etc - See VB Manual.
'************************************************************************
    moLE.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Determine if focus is lost to a edit control.  If so,
'           begin the edit or add mode process.
'************************************************************************
    moLE.Grid_LostFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Mark the x,y Position in preparation for drag-and-drop.
'   Parameters:
'           Button, etc - See VB Manual.
'************************************************************************
    moLE.Grid_MouseDown Button, Shift, x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdRcptDetl_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Check if mouse movement qualifies to be a drap-and-drop.
'   Parameters:
'           Button, etc - See VB Manual.
'************************************************************************
    moLE.Grid_MouseMove Button, Shift, x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdRcptDetl_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LEClearDetailControls(oLE As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Description: Clear detail fields on the Line Entry object.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
    ClearDetlFields True
    moProject.LEClearDetailControls
    SetDetailCtrlState True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEClearDetailControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LESetDetailControlDefaults(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Description:
'           Detail edit controls loading point.
'   Parameter:
'           oLE - the line entry class object calling this procedure.
'************************************************************************************
    Dim bValid As Boolean
    
    ' - MVB - set for returns
    '-- Default the Return Type column if it's a Debit Memo
'    If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
'        If (glGetValidLong(ddnTranType.ItemData) = kTranTypeAPDM) Then
'            ToggleCtrlState chkReturnType, False
'        End If
'    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LESetDetailControlDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       called from line entry class when the users pressed 'OK'.  Used
'       to validate the data entered in the detail controls
'
'   Param:
'       oLE -   line entry class making the call
'
'   Returns:
'       True -  data is valid and can be saved
'       False - data is invalid.  do not save the line (stay in edit mode)
'
'************************************************************************

Public Function LECheckRequiredData(oLE As clsLineEntry) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim lOrigUOMKey As Long
    Dim lCurrUOMKey As Long
    Dim dConvFactor As Double
    Dim dConvertedQtyRcvd As Double
    Dim sUnitMeasID  As String
    
    LECheckRequiredData = True
    gbSetFocus Me, tbrMain
    DoEvents
    'Validate dirty controls in case validation manager didn't get fired
    'on the last control.  Do this before checking weight and volume since
    'changing UOM will 0 these out.
    If Not SOTAValidationMgr1.ValidateForm() Then
        LECheckRequiredData = False
        Exit Function
    End If
    
'-- Check that the Weight is being properly cpatured.
    If Len(txtWhse) > 0 And miCaptureWght = kvCaptureRequired Then
        If mdItemWght = 0 And mdRcvrWght = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgWeightRequired
            cmdWghtVol.SetFocus
            LECheckRequiredData = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
'-- Check that the Volume is being properly cpatured.
    If Len(txtWhse) > 0 And miCaptureVol = kvCaptureRequired Then
        If mdItemVol = 0 And mdRcvrVol = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgVolumeRequired
            cmdWghtVol.SetFocus
            LECheckRequiredData = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If

'-- Check that the IM Distribution stuff is being captured properly
    If cmdDist.Enabled Then
        If mlIMTempTranID = 0 And mlInvtTranKey = 0 And nbrQtyRcvd > 0 Then
            If bCanDistAgainstPrefBin(mlItemKey, mlWhseKey, 1) = False And glGetValidLong(lkuAutoDistBin.KeyValue) = 0 And Len(lkuAutoDistBin.Text) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgIMDistInfoRequired
                LECheckRequiredData = False
                tabSubDetl.Tab = kTabPutAway
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
        If miIMDistDirty > 0 And (mlIMTempTranID > 0 Or mlInvtTranKey > 0) And cmdDist.Enabled Then
            If bCanDistAgainstPrefBin(mlItemKey, mlWhseKey, 1) = False And glGetValidLong(lkuAutoDistBin.KeyValue) And Len(lkuAutoDistBin.Text) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUpdateIMDistInfo
                LECheckRequiredData = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
    End If
    
    'Item does not support Decimals,hence check if the Qty Received in Source UOM will result in Decimal number as a result of
    'this receiver line UOM conversion, in which case do not allow the transaction to save
    If Not moItem.AllowDecimalQty Then
        lOrigUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, moLE.ActiveRow, IIf(moClass.lTranType = kTypeReceipt, kColUnitMeasOrdKey, kColOrigRcvrUOMKey)))

        lCurrUOMKey = ddnUOM.ItemData(ddnUOM.ListIndex)

        dConvertedQtyRcvd = nbrQtyRcvd
        moItem.UOMConversion lCurrUOMKey, lOrigUOMKey, dConvertedQtyRcvd, dConvFactor

        'If converted qty is decimal and if AllowDecimalQuantity is not set then stop user to change the UOM
        If Int(dConvertedQtyRcvd) <> dConvertedQtyRcvd Then
            sUnitMeasID = gsGetValidStr(moClass.moAppDB.Lookup("UnitMeasID", "tciUnitMeasure", "UnitMeasKey = " & lOrigUOMKey & " AND CompanyID = " & gsQuoted(msCompanyID)))
            giSotaMsgBox Me, moClass.moSysSession, kmsgPONoDecimalQty, CStr(nbrQtyRcvd), RTrim(ddnUOM.Text), FormatNumber(dConvertedQtyRcvd, miQtyDecPlaces, False, False, False), sUnitMeasID
            nbrQtyRcvd.SetFocus
            LECheckRequiredData = False
            Exit Function
        End If
    End If
         
    'To warn the user when project is closed and to revert the changes on edit.
    If moProject.LECheckRequiredData() = False Then
        cmdUndo_Click
    End If
                
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

ExpectedErrorRoutine:
 LECheckRequiredData = False
MyErrMsg moClass, Err.Description, Err, sMyName, "LECheckrequiredData"
gClearSotaErr

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LECheckRequiredData", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Sub LEDetailToGrid(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: If any detail entry controls need to be manually linked to
'       the grid after entry, then do it here.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
    Dim lRow As Long
    Dim lChildRow As Long
    Dim dExtAmtS As Double
    Dim dFreightAmtS As Double
    Dim dQtyS As Double
    Dim iOldMatch As Integer
    Dim dQtyRcvd As Double
    Dim dQtyRcvdinPOUOM As Double
    Dim lReasonKey As Long
    Dim lInvtTranKey As Long
    Dim sWhseBinID As String
    Dim dBinQtyAvail As Double
    Dim sLotNo As String
    Dim sLotExpDate As String
    Dim lTranIdentifier As Long
    Dim ocolBinRetColVals As Collection
    Dim sDoAutoDist As String
    Dim dRetTranQyt As Double
    Dim lDfltWhseBinKey As Long
    Dim bUsePreferredBin As Boolean
    Dim lRcvrKey As Long
        
    lRow = moLE.ActiveRow
    lChildRow = 1
    
    'Need to detect if the receipt line's volume, weight, quantity, or UOM changed.
    'So prior to updating the grid (so we can compare the values), check if the fields have changed.
    If mbAdjToLandedCostAmtsRequired = False And _
      (gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemVol)) <> mdRcvrVol Or _
       gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemWght)) <> mdRcvrWght Or _
       gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd)) <> nbrQtyRcvd Or _
       giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey)) <> ddnUOM.ItemData) Then
       
       mbAdjToLandedCostAmtsRequired = True
   End If
    
    dQtyRcvd = nbrQtyRcvd * miSignFactor
    dQtyRcvdinPOUOM = mdQtyInSrcUOM * miSignFactor
    gGridUpdateCell grdRcptDetl, lRow, kColQtyRcvd, CStr(nbrQtyRcvd)
    gGridUpdateCell grdRcptDetl, lRow, kColQtyRcvdS, CStr(dQtyRcvd)
    gGridUpdateCell grdRcvrLineDist, lChildRow, kColQtyRcvdD, CStr(dQtyRcvd)
    
    'Convert the total quantity ordered (receipts) or received (returned) to the current
    'unit of measure for display in the grid
    DisplayOriginalQty lRow
    
            
    gGridUpdateCell grdRcptDetl, lRow, kColQtyInPOUOM, CStr(dQtyRcvdinPOUOM)
    gGridUpdateCell grdRcvrLineDist, lChildRow, kcolQtyRcvdInPOUOMD, CStr(dQtyRcvdinPOUOM)
    If mlTranType <> kTranTypePORTrn Then
        iOldMatch = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColMatchStatus))
        If chkMatch.Value = 1 And iOldMatch = 4 Then
            gGridUpdateCell grdRcptDetl, lRow, kColMatchStatus, 1
        ElseIf chkMatch.Value = 0 And iOldMatch <> 4 Then
            gGridUpdateCell grdRcptDetl, lRow, kColMatchStatus, 4
        End If
        gGridUpdateCell grdRcptDetl, lRow, kColReturnType, 0
        gGridUpdateCell grdRcptDetl, lRow, kColOrigRtrnType, 0
    Else
        If optRetReplace Then
            gGridUpdateCell grdRcptDetl, lRow, kColReturnType, 2
        Else
            gGridUpdateCell grdRcptDetl, lRow, kColReturnType, 1
        End If
    End If
    gGridUpdateCell grdRcptDetl, lRow, kColSTaxAmt, CStr(mdSTaxAmt)
    gGridUpdateCell grdRcvrLineDist, lChildRow, kColFreightAmtD, CStr(mdFreightAmt)
    gGridUpdateCell grdRcptDetl, lRow, kColFreightAmt, CStr(mdFreightAmt)
    
    gGridUpdateCell grdRcptDetl, lRow, kColCloseSrcLine, chkCloseDistr
    gGridUpdateCell grdRcptDetl, lRow, kColItemVol, CStr(mdRcvrVol)
    gGridUpdateCell grdRcptDetl, lRow, kColItemWght, CStr(mdRcvrWght)
    If mlInvtTranKey = 0 Then
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, ""
    Else
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(mlInvtTranKey)
    End If
    gGridUpdateCell grdRcptDetl, lRow, kColInvtTranID, CStr(mlIMTempTranID)
    gGridUpdateCell grdRcptDetl, lRow, kColInvtTranDirty, CStr(miIMDistDirty)
    gGridUpdateCell grdRcptDetl, lRow, kColUnitCost, CStr(mdUnitCost)
    gGridUpdateCell grdRcptDetl, lRow, kColUnitCostExact, CStr(mdUnitCostExact) ' Updating the UnitCostExact value to the grid

    gGridUpdateCell grdRcptDetl, lRow, kColUnitMeasRcvdKey, CStr(ddnUOM.ItemData)
    gGridUpdateCell grdRcptDetl, lRow, kColUnitMeasRcvdID, ddnUOM.Text
    gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBin, lkuAutoDistBin.Text
    
    If mlTranType = kTranTypePORTrn Then
        lReasonKey = CStr(ddnReason.ItemData)
        If lReasonKey = 0 Then
            gGridUpdateCell grdRcptDetl, lRow, kColRtrnReasonKey, ""
        Else
            gGridUpdateCell grdRcptDetl, lRow, kColRtrnReasonKey, CStr(ddnReason.ItemData)
        End If
    End If
    
    With muVendPerfDtl
        gGridUpdateCell grdRcptDetl, lRow, kColDamaged, gsGetValidStr(.iDamaged)
        gGridUpdateCell grdRcptDetl, lRow, kColDefective, gsGetValidStr(.iDefective)
        gGridUpdateCell grdRcptDetl, lRow, kColCarrierDamage, gsGetValidStr(.iCarrierDamage)
        gGridUpdateCell grdRcptDetl, lRow, kColImpLabel, gsGetValidStr(.iImproperLabel)
        gGridUpdateCell grdRcptDetl, lRow, kColDocMissing, gsGetValidStr(.iDocMissing)
        gGridUpdateCell grdRcptDetl, lRow, kColImpPackage, gsGetValidStr(.iImproperPackage)
        gGridUpdateCell grdRcptDetl, lRow, kColNotToSpec, gsGetValidStr(.iNotToSpec)
        gGridUpdateCell grdRcptDetl, lRow, kColOtherIssue, gsGetValidStr(.iOtherIssue)
        gGridUpdateCellText grdRcptDetl, lRow, kColPerfComments, gsGetValidStr(.sPerfComments)
    End With

    If Len(Trim$(lkuProject.Text)) > 0 Then
        '-- Update the voucher detail grid columns
        gGridUpdateCell grdRcptDetl, lRow, kColProjectID, lkuProject.Text
        gGridUpdateCell grdRcptDetl, lRow, kColProjectKey, CStr(lkuProject.KeyValue)
        gGridUpdateCell grdRcptDetl, lRow, kColPhaseID, lkuPhase.Text
        gGridUpdateCell grdRcptDetl, lRow, kColPhaseKey, CStr(lkuPhase.KeyValue)
        gGridUpdateCell grdRcptDetl, lRow, kColTaskID, lkuTask.Text
        gGridUpdateCell grdRcptDetl, lRow, kColTaskKey, CStr(lkuTask.KeyValue)
        
        '-- Update the job grid columns
        JobLEDetailToGrid lRow
    End If

    gGridUpdateCell grdRcptDetl, lRow, kColUsePreferred, chkPreferredBinDetl.Value
    If Not (moIMDistribution Is Nothing) Then
        If mlTranType = kTranTypePOTR Then
            lDfltWhseBinKey = glGetValidLong(lkuAutoDistBin.KeyValue)
            bUsePreferredBin = gbGetValidBoolean(chkPreferredBinDetl.Value)
            If lDfltWhseBinKey <> 0 Or bUsePreferredBin = True Then
                lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
                mlTrnsfrOrderLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTrnsfrLineKey))
                mlIMTempTranID = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
                
                lInvtTranKey = mlInvtTranKey
                If lInvtTranKey = 0 Then
                    lInvtTranKey = lGetInvtTranKey(lRow)
                    If lInvtTranKey = 0 Then
                        Exit Sub
                    End If
                    gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(lInvtTranKey)
                    moDmDetl.SetColumnValue lRow, "InvtTranKey", lInvtTranKey
                    mlInvtTranKey = lInvtTranKey
                End If
                
                'User previously cancelled or 'x' out of the distributions.  Set it back to zero.
                '*See notes below.
                If mlIMTempTranID = -1 Then
                    mlIMTempTranID = 0
                End If
                
                lTranIdentifier = moIMDistribution.ReceiveTransferDist(mlTranType, _
                                                    mlWhseKey, _
                                                    mlTrnsfrOrderLineKey, _
                                                    lRcvrKey, _
                                                    gdGetValidDbl(nbrQtyRcvd), _
                                                    mlUOMKey, _
                                                    dRetTranQyt, _
                                                    mlIMTempTranID, _
                                                    mlInvtTranKey, _
                                                    bUsePreferredBin, _
                                                    lDfltWhseBinKey, _
                                                    False) 'Silent Distribution (when possible)

                'Update the grid with the TranIdentifier
                gGridUpdateCell grdRcptDetl, lRow, kColInvtTranID, CStr(lTranIdentifier)
                mlIMTempTranID = lTranIdentifier

                '*User did not perform any distributions.  They either cancelled or hit the 'x' to close the form.
                'By doing so, the mlIMTempTranID was set to zero.  However, we still need the app to think the
                'row is dirty.  It was not being set to dirty if the mlIMTempTranID returned a zero. Couldn't figure
                'out why so decided to just set it to a non-zero number.
                If mlIMTempTranID = 0 Then
                    mlIMTempTranID = -1
                End If
    
            End If
        Else
            bAutoDistFromBinLku lRow
        End If
    End If
            
    '-- Handle the sub grid appropriately
    moDmLineDist.SetRowDirty lChildRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEDetailToGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub DisplayOriginalQty(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lCurrUOMKey As Long
Dim lOrigUOMKey As Long
Dim dTotalQty As Double
Dim dConvFactor As Double

    If mlTranType = kTranTypePORTrn Then
        lOrigUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColOrigRcvrUOMKey))
        dTotalQty = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColOrigQtyRcvd))
    Else
        lOrigUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasOrdKey))
        dTotalQty = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyOrdered))
    End If
    lCurrUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey))
    If lCurrUOMKey <> 0 And lOrigUOMKey <> 0 Then
        moItem.UOMConversion lOrigUOMKey, lCurrUOMKey, dTotalQty, dConvFactor
    End If
    gGridUpdateCell grdRcptDetl, lRow, kColOrigQtyCurrUOM, gsGetValidStr(dTotalQty)
    '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisplayOriginalQty", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub RefreshUOMSQL(sCompanyID As String, iMeasType As Integer, bRefresh As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    
    ddnUOM.CompanyId = sCompanyID
    
    sSQL = "SELECT UnitMeasID, UnitMeasKey FROM tciUnitMeasure"
    sSQL = sSQL & " WHERE MeasType = " & iMeasType
    
    ddnUOM.SQLStatement = sSQL
    
    If bRefresh Then ddnUOM.Refresh
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RefreshUOMSQL", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LEGridToDetail(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: If any grid columns need to be manually linked to the detail
'       entry controls, then do it here.
' Parameters: oLE - the Line Entry class object calling this procedure.
'***********************************************************************
    Dim lRow As Long
    Dim lChildRow As Long
    Dim dUnitCost As Double
    Dim lKey As Long
    Dim sCode As String
    Dim iMatchStatus As Integer
    Dim iReturnType As Integer
    Dim lDfltWhseBinKey As Long
    Dim sDfltWhseBinID As String
    Dim sWhseBinID As String
    
    Dim bIsEnabled As Boolean
    Dim sLookupID As String
    Dim sBinText As String
    Dim bMultipleBin As Boolean

    lRow = oLE.ActiveRow
    lChildRow = 1
    mbGridClicked = False
    
    '-- Set the PO Line Num
        '-- Set up the Item Info structure
    With muItemInfo
        .iMeasType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColMeasType))
        .lReceiverLineDistKey = glGetValidLong(gsGridReadCell(grdRcvrLineDist, lChildRow, kColRcvrLineDistKeyD))
        .lReceiverLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey))
    End With

    With muRcvrLine
        .lRcvrLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey))
        .lDistTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
        .lTrnsfrOrderLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTrnsfrLineKey))
                
        '-- Set the PO Line Num
        txtPOLine = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColPOLineNo))

        '-- Set the Item ID
        txtItemOrdID = gsGetValidStr(gsGridReadCellText(grdRcptDetl, lRow, kColMaskedItemID))
        
        '-- Set the Item Description
        txtItemDescription = gsGetValidStr(gsGridReadCellText(grdRcptDetl, lRow, kColItemDesc))
        
        mlItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
        Set moItem = moIMSClass.Items(mlItemKey)
        If moItem.AllowDecimalQty Then
            nbrQtyRcvd.DecimalPlaces = miQtyDecPlaces
            nbrQtyOrd.DecimalPlaces = miQtyDecPlaces
            nbrTotRcvd.DecimalPlaces = miQtyDecPlaces
            nbrQtyReturn.DecimalPlaces = miQtyDecPlaces
            nbrQtyOpen.DecimalPlaces = miQtyDecPlaces
        Else
            nbrQtyRcvd.DecimalPlaces = 0
            nbrQtyOrd.DecimalPlaces = 0
            nbrTotRcvd.DecimalPlaces = 0
            nbrQtyReturn.DecimalPlaces = 0
            nbrQtyOpen.DecimalPlaces = 0
        End If
        '-- Set the Quantity Received
        mbManualClick = True
        nbrQtyRcvd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd))
        mbManualClick = False
        nbrQtyRcvd.Tag = nbrQtyRcvd.Text
        mdQtyInSrcUOM = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyInPOUOM)) * miSignFactor
        mlUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey))
                        
        moItem.GetValidUOMs ddnUOM
        If (mlUOMKey > 0) Then
            ddnUOM.ListIndex(False) = ddnUOM.GetIndexByItemData(mlUOMKey)
            ddnUOM.Tag = ddnUOM.Text
        End If
        
        If mlTranType <> kTranTypePORTrn Then
            mdItemVol = moItem.Volume(mlUOMKey)
            mdItemWght = moItem.Weight(mlUOMKey)
            mdRcvrVol = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemVol))
            mdRcvrWght = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemWght))
        
        Else
            txtUOMRcpt.Text = gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdID)
            mlReasonKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColRtrnReasonKey))
            If (mlReasonKey > 0) Then
                ddnReason.ListIndex(False) = ddnReason.GetIndexByItemData(mlReasonKey)
            End If
        End If
        
        With muVendPerfDtl
            .iDamaged = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColDamaged))
            .iDefective = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColDefective))
            .iCarrierDamage = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColCarrierDamage))
            .iImproperLabel = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColImpLabel))
            .iDocMissing = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColDocMissing))
            .iImproperPackage = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColImpPackage))
            .iNotToSpec = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColNotToSpec))
            .iOtherIssue = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColOtherIssue))
            .sPerfComments = gsGetValidStr(gsGridReadCellText(grdRcptDetl, lRow, kColPerfComments))
        End With
        
        '-- Set the Warehouse
        txtWhse = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColWhseID))
        
'        If the warehouse is there, determine if the Serial/Lot/Bin button should be enabled.
'        Also, if there is a warehouse and wght and/or volume is being captured, enable the button.
        If Len(txtWhse) > 0 And mlItemKey > 0 Then
            If mbTrackByBin Then
                cmdDist.Enabled = True
            ElseIf moItem.TrackMethod = IMS_TRACK_METHD_NONE Then
                cmdDist.Enabled = False
            Else
                cmdDist.Enabled = True
            End If
            
            If mbTrackByBin Then
                '-- Auto-dist bin lookup.
                'Get the Lookup ID to use for the current row and decide if the bin
                'lookup control can be enabled.
                With lkuAutoDistBin
                    .LookupID = moIMDistribution.sGetBinLkuCtrlLookupID(mlItemKey, giGetValidInt(mlTranType))
                    .Enabled = moIMDistribution.bEnableBinLkuCtrl(mlWhseKey, mlItemKey)
                End With

                'Bring the bin lookup to the front.
                lkuAutoDistBin.ZOrder 0

                If glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID)) <= 0 And glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey)) = 0 Then
                    '--There are no saved distributions.
                    '--If the use preferred bin is checked, make sure the associated preferred bin
                    '--is being displayed on the control.
                    
                    'PO Vendor Return does not have a preferred bin checkbox no does it support default putaway
                    'bins defined in the header.  Skip following code if TranType is PO Vendor Return.
                    If mlTranType <> kTranTypePORTrn Then
                        
                        chkPreferredBinDetl = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUsePreferred))
                            
                        If chkPreferredBinDetl.Value = vbChecked Then
                            SetDetlPreferredBin True, mlItemKey, mlWhseKey, lRow, True
                        Else
                            'Get any previously stored bin info from the grid.
                            lkuAutoDistBin.Enabled = True
                            lDfltWhseBinKey = lkuReceiveToBin.KeyValue
                            sDfltWhseBinID = lkuReceiveToBin.Text
                            
                            bIsValidAutoDistBin lRow
    
                            lkuAutoDistBin.SetTextAndKeyValue sDfltWhseBinID, lDfltWhseBinKey
                            'Now update the grid.  Make sure to update it after calling bIsValidAutoDistBin
                            'or else the kColDoAutoDist column will not be updated if it can be auto-distributed.
                            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBin, gsGetValidStr(sDfltWhseBinID)
                            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBinKey, CStr(lDfltWhseBinKey)
    
                        End If
                        
                        'Disable the use preferred bin if a preferred bin does not exist for the inventory item.
                        If bCanDistAgainstPrefBin(mlItemKey, mlWhseKey, 1, True) = False Then
                            chkPreferredBinDetl.Value = 0
                            chkPreferredBinDetl.Enabled = False
                        Else
                            chkPreferredBinDetl.Enabled = True
                        End If
                        lkuAutoDistBin.Tag = lkuAutoDistBin.Text
                    Else
                        'PO Vendor Return having no saved distributions.  Just clear the lookup.
                        lkuAutoDistBin.SetTextAndKeyValue "", 0
                        lkuAutoDistBin.Tag = ""
                    End If

                Else
                    'Current saved distribution or user has distributed the line.
                    GetAutoDistSavedInfo lRow
                End If
            End If
            
            If cmdWghtVol.Visible Then
                cmdWghtVol.Enabled = True
            End If
        Else
            cmdDist.Enabled = False
            
            'For non-inventory items, clear and disable put away detail controls.
            chkPreferredBinDetl.Value = 0
            lkuAutoDistBin.SetTextAndKeyValue "", 0
            lkuAutoDistBin.Tag = ""
            lkuAutoDistBin.Enabled = False
            chkPreferredBinDetl.Enabled = False
            
            If cmdWghtVol.Visible Then
                cmdWghtVol.Enabled = False
            End If
        End If
        
        '-- Get the Unit Cost
        mdUnitCost = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColUnitCost))
        
        '--Get the Unit Cost Exact
        
        mdUnitCostExact = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColUnitCostExact))
        '-- Set the Department
        txtDept = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColDeptID))
        
        '-- Set the Inventory transaction key
        mlInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
        
        '-- Set the temporary transaction key
        mlIMTempTranID = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))

        '-- Set the flag to indicate the IM Dist Tran is dirty
        miIMDistDirty = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranDirty))
        
        
        '-- Original Order tab
        nbrQtyOrd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyOrdered))

        txtUOM = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasOrdID))
        mlSourceUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasOrdKey))

        nbrTotRcvd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColTotalRcvd))
        nbrQtyReturn = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRtrned))
        nbrQtyShip = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyShip))
        nbrQtyOpen = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyOpen))

        '--Comments tab
        txtLnComment = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColTranComment))
        
        '-- Misc tab
        If mlTranType <> kTranTypePORTrn Then
            mbManualClick = True
            iMatchStatus = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColMatchStatus))
            If iMatchStatus < 3 Then
                chkMatch = 1
            Else
                chkMatch = 0
            End If
            chkCloseDistr = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColCloseSrcLine))
            mbManualClick = False
        
            .dItemVol = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemVol))
            .dItemWght = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemWght))
        Else
            mbManualClick = True
            iReturnType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColReturnType))
            If iReturnType = 1 Then
                optRetCredit = True
                optRetReplace = False
            ElseIf iReturnType = 2 Then
                optRetCredit = False
                optRetReplace = True
            Else
                optRetCredit = True
                optRetReplace = False
            End If
            mbManualClick = False
        End If
        '-- Get Freight Amt
        mdFreightAmt = gdGetValidDbl(gsGridReadCell(grdRcvrLineDist, 1, kColFreightAmtD))
        '-- Get STax Amt
        mdSTaxAmt = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColSTaxAmt))

    End With
    ' If the POLineKey is 0, then the Line is not a valid line and the Controls should be disabled.
    ' Otherwise, enable the controls.
    mlPOLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey))
    mlTrnsfrOrderLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTrnsfrLineKey))
    If (mlPOLineKey = 0) And (mlTrnsfrOrderLineKey = 0) Then
        nbrQtyRcvd.Enabled = False
        ddnUOM.Enabled = False
        ddnReason.Enabled = False
        txtLnComment.Enabled = False
        chkMatch.Enabled = False
        chkCloseDistr.Enabled = False
        optRetCredit.Enabled = False
        optRetReplace.Enabled = False
        moProject.LEClearDetailControls
        moProject.SetJobGridRow 0
    Else
        nbrQtyRcvd.Enabled = True
        ddnUOM.Enabled = True
        If mlTranType = kTranTypePORTrn Then
            ddnReason.Enabled = True
            ddnReason.BackColor = vbWindowBackground
        End If
        txtLnComment.Enabled = True
        chkMatch.Enabled = True
        chkCloseDistr.Enabled = True
        optRetCredit.Enabled = True
        optRetReplace.Enabled = True
        Dim sIDJ As String
        Dim lKeyJ As Long
        Dim lPOLineKey As Long
        
        lPOLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey))
        
        moProject.SetJobGridRow lPOLineKey
        
        sIDJ = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColProjectID))
        lKeyJ = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColProjectKey))
        lkuProject.SetTextAndKeyValue sIDJ, lKeyJ
        
        sIDJ = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPhaseID))
        lKeyJ = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColPhaseKey))
        lkuPhase.SetTextAndKeyValue sIDJ, lKeyJ
        
        sIDJ = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColTaskID))
        lKeyJ = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTaskKey))
        lkuTask.SetTextAndKeyValue sIDJ, lKeyJ
        
        lkuTask.RestrictClause = moProject.sGetTaskRestrict()
    End If
    
    '-- Set the detail controls as valid so the old values are correct
    SOTAValidationMgr1.Reset

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEGridToDetail", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function LEGridBeforeDelete(oLE As clsLineEntry, lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'       called from line entry class when a row has been successfully
'       deleted from the grid
'   Param:
'       oLE -   line entry class making the call
'       lrow -  grid row deleted
'************************************************************************
    Dim dTotalTaxAmt As Double
    
    LEGridBeforeDelete = False
    
    LEGridBeforeDelete = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEGridBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub LEGridAfterDelete(oLE As clsLineEntry, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'       called from line entry class when a row has been successfully
'       deleted from the grid
'   Param:
'       oLE -   line entry class making the call
'       lrow -  grid row deleted
'************************************************************************

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEGridAfterDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LESetDetailFocus(oLE As clsLineEntry, Optional Col As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Double-clicking on any grid column will drive focus to the
'       detail oControl representing the Selected grid column.
' Parameters: oLE - the Line Entry class object calling this procedure.
'             Col - The column to set the focus into.
'***********************************************************************
    Dim oControl As Object

    If IsMissing(Col) Then
        Col = kColMaskedItemID
    End If

    Select Case Col
        Case kColQtyRcvd
            Set oControl = nbrQtyRcvd

        Case kColUnitMeasRcvdID
            Set oControl = ddnUOM

       Case Else
            Set oControl = nbrQtyRcvd
            
    End Select

    If (Not oControl Is Nothing) Then
        SetCtrlFocus oControl
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LESetDetailFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Validate the lookup control myself through local routine.
    iReturn = EL_CTRL_VALID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_BeforeValidate", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuAutoDistBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

    moLE.GridEditChange lkuAutoDistBin

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_KeyPress", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuReceiver_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuReceiver, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

    Dim iInteger As Integer
    Dim sMessage As String
'MsgBox "BeforeNavReturn"
    
    If Trim(gsGetValidStr(colSQLReturnVal(1))) <> Trim(lkuReceiver.Text) Then
        lkuReceiver.Text = gsGetValidStr(colSQLReturnVal(1))
        If lkuReceiver.IsValid Then
            If Not bIsValidReceiver(Me, moDmDetl, msCompanyID, moLE, , sMessage) Then
                giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblReceiver, kAmpersand)
                lkuReceiver.Text = lkuReceiver.Tag
            End If
        Else
            giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblReceiver, kAmpersand)
            lkuReceiver.Text = lkuReceiver.Tag
        End If
        Debug.Print "Vaidatation Returned " & iInteger
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiver_BeforeLookupReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub


Private Sub lkuPONo_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPONo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

    Dim iInteger As Integer
    
    If Trim(gsGetValidStr(colSQLReturnVal("TranNoRel"))) <> Trim(lkuPONo.Text) Then
        lkuPONo.Text = gsGetValidStr(colSQLReturnVal("TranNoRel"))
        If lkuPONo.IsValid Then
            'Lookup value (restict clause) is valid, so do more extensive checking with detailed messages
            Dim sMessage As String
            sMessage = kDisplayMsg
            If Not bIsValidPONo(Me, moDmDetl, moLE, msHomeCurrID, , sMessage, , miClosePOonFirstRcpt) Then
                bCancel = True
                'Put Focus back...
                SetCtrlFocus lkuPONo
                'Put back the old values...
                Me.SOTAValidationMgr1.StopValidation
                lkuPONo.Text = msOldlkuPONoText
                lkuPONo.Tag = msOldlkuPONoText
                Me.SOTAValidationMgr1.StartValidation
            Else
                lkuPONo.Tag = lkuPONo.Text
                If mlTranType = kTranTypePORG Then
                    moDmHeader.SetColumnValue "POKey", glGetValidLong(lkuPONo.KeyValue)
                End If
            End If

        Else
            giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblPONo, kAmpersand)
            lkuPONo.Text = ""
        End If
        Debug.Print "Vaidatation Returned " & iInteger
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPONo_BeforeLookupReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lkuPONo_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'    lkuPONo = sZeroFillNumber(lkuPONo.MaskedText, lkuPONo.MaxLength)
    Debug.Print "lkuPONo_BeforeValidate"
    msOldlkuPONoText = lkuPONo.Tag
    iReturn = EL_CTRL_CONTINUE

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPONo_BeforeValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub lkuReceiver_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lkuReceiver = sZeroFillNumber(lkuReceiver.MaskedText, lkuReceiver.MaxLength)
    Debug.Print "lkuReceiver_BeforeValidate"
    iReturn = EL_CTRL_CONTINUE
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiver_BeforeValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPONo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPONo, True
    #End If
'+++ End Customizer Code Push +++
    txtPONum.Text = Mid(lkuPONo.Text, 1, 10)
    txtPONum.Tag = txtPONum.Text
    txtRelNo.Text = Mid(lkuPONo.Text, 12, 4)
    txtRelNo.Tag = txtRelNo.Text
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPONo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub lkuReceiver_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuReceiver, True
    #End If
'+++ End Customizer Code Push +++
'MsgBox "LostFocus"
    Dim sMessage As String
    If Not lkuReceiver.IsValid Then
        giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblReceiver, kAmpersand)
        SetCtrlFocus lkuReceiver
        lkuReceiver.Text = lkuReceiver.Tag
    Else
        If Not bIsValidReceiver(Me, moDmDetl, msCompanyID, moLE, , sMessage) Then
            giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblReceiver, kAmpersand)
            lkuReceiver.Text = lkuReceiver.Tag
        End If
    End If

    If (moDmHeader.State <> kDmStateNone) Then
        If bDenyControlFocus() Then Exit Sub
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiver_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPONo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPONo, True
    #End If
'+++ End Customizer Code Push +++
    If (moDmHeader.State <> kDmStateNone) Then
        If bDenyControlFocus() Then Exit Sub
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPONo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub lkuReceiver_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuReceiver, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'MsgBox "NavClick"

    If Len(Trim$(lkuReturn)) = 0 Then
        bCancel = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiver_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPONo_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPONo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuReceiptNo)) = 0 Then
        bCancel = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPONo_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub lkuReceiveToBin_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuReceiveToBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    bSetupLookupNav lkuReceiveToBin, "Bin", sHeaderWhseBinRestrict(mlWhseKey)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuReceiveToBin_LookupClick", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub lkuReceiveToBin_Validate(Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    If Not lkuReceiveToBin.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidWhseBin
        Cancel = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuReceiveToBin_Validate", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuShipWhse_GotFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuShipWhse, True
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
    lkuShipWhse.Tag = lkuShipWhse
End Sub

Private Sub lkuShipWhse_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuShipWhse, True
    #End If
'+++ End Customizer Code Push +++
    Dim sMessage As String
    
    If Not lkuShipWhse.IsValid Then
        giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblShipWhse, kAmpersand)
        SetCtrlFocus lkuShipWhse
        lkuShipWhse.Text = lkuShipWhse.Tag
        Exit Sub
    Else
        sMessage = kDisplayMsg
        If Not bIsValidShipWhse(sMessage) Then Exit Sub
    End If

    If (moDmHeader.State <> kDmStateNone) Then
        If bDenyControlFocus() Then Exit Sub
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "lkuShipWhse_LostFocus", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuShipWhse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuShipWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuReceiptNo)) = 0 Then
        bCancel = True
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "lkuShipWhse_LookupClick", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub lkuTransfer_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuTransfer, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lWhseKey As Long
    Dim sWhseID As String
    
    If Trim(gsGetValidStr(colSQLReturnVal(1))) <> Trim(lkuTransfer.Text) Then
        lkuTransfer.Text = gsGetValidStr(colSQLReturnVal(1))

        If lkuTransfer.IsValid Then
            'Lookup value (restict clause) is valid, so do more extensive checking with detailed messages
            Dim sMessage As String
            sMessage = kDisplayMsg
            If Not bIsValidTransfer(Me, moDmDetl, moLE, msHomeCurrID, , sMessage) Or mbCancelTransferBeforeNavReturn Then
                bCancel = True

                'Put Focus back...
                SetCtrlFocus lkuTransfer

                'Put back the old values...
                Me.SOTAValidationMgr1.StopValidation
                lkuTransfer.Text = msOldlkuTransferText
                lkuTransfer.Tag = msOldlkuTransferText
                Me.SOTAValidationMgr1.StartValidation
            Else
                lkuTransfer.Tag = lkuTransfer.Text
            End If
        Else
            giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblTransfer, kAmpersand)
            lkuTransfer.Text = lkuTransfer.Tag
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "lkuTransfer_BeforeLookupReturn", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuTransfer_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
    If IsNumeric(Trim(lkuTransfer.Text)) And Len(Trim(lkuTransfer.Text)) > 0 And Len(Trim(lkuTransfer.Text)) < kvTranOrderTranSize Then
        lkuTransfer.Text = gsZeroFill(lkuTransfer.Text, kvTranOrderTranSize)
        Text = lkuTransfer.Text
    End If

    msOldlkuTransferText = lkuTransfer.Tag

    iReturn = EL_CTRL_CONTINUE

End Sub

Private Sub lkuTransfer_GotFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuTransfer, True
    #End If
'+++ End Customizer Code Push +++
'+++ VB/Rig Skip +++
    lkuTransfer.Tag = lkuTransfer

End Sub

Private Sub lkuTransfer_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuTransfer, True
    #End If
'+++ End Customizer Code Push +++
    
    Dim sMessage As String
    If Not lkuTransfer.IsValid Then
        giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblTransfer, kAmpersand)
        SetCtrlFocus lkuTransfer
        lkuTransfer.Text = lkuTransfer.Tag
        Exit Sub
    Else
        sMessage = kDisplayMsg
        If Not bIsValidTransfer(Me, moDmDetl, moLE, msHomeCurrID, , sMessage) Or mbCancelTransferBeforeNavReturn Then
            SetCtrlFocus lkuTransfer
            lkuTransfer.Text = msOldlkuTransferText
            lkuTransfer.Tag = msOldlkuTransferText
        Else
            lkuTransfer.Tag = lkuTransfer.Text
        End If
    End If
    
    If (moDmHeader.State <> kDmStateNone) Then
        If bDenyControlFocus() Then
            Exit Sub
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "lkuTransfer_LostFocus", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuTransfer_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuTransfer, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuReceiptNo)) = 0 Then
        bCancel = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "lkuTransfer_LookupClick", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuVendID_GotFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuVendID, True
    #End If
'+++ End Customizer Code Push +++
    lkuVendID.Tag = lkuVendID
End Sub

Private Sub lkuVendID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuVendID, True
    #End If
'+++ End Customizer Code Push +++
    Dim sMessage As String
    If Not lkuVendID.IsValid Then
        giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblVendID, kAmpersand)
        SetCtrlFocus lkuVendID
        lkuVendID.Text = lkuVendID.Tag
        Exit Sub
    Else
        If lkuVendID.Tag <> lkuVendID.Text Then
            sMessage = kDisplayMsg
            If Not bIsValidVendID(sMessage) Then Exit Sub
        End If
    End If

    If (moDmHeader.State <> kDmStateNone) Then
        If bDenyControlFocus() Then Exit Sub
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuVendID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVendID_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuVendID, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim$(lkuReceiptNo)) = 0 And Len(Trim$(lkuReturn)) = 0 Then
        bCancel = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuVendID_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmDetl_DMGridBeforeDelete(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvrLineKey As Long
    Dim sSQL As String
    
    bValid = False

    '-- Save the receiver line key for later deletion
    If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
        lRcvrLineKey = glGetValidLong(moDmDetl.Columns("RcvrLineKey").Value)
        mlRcvrLineKey = glGetValidLong(moAppDB.Lookup("RcvrLineKey", "tpoRcvrLine", "RcvrLineKey=" & lRcvrLineKey))
    End If
    
        '-- Delete Receiver Line Dists First - RI should handle this after version 3.99.050
    sSQL = "DELETE tpoRcvrLineDist "
    sSQL = sSQL & "FROM tpoRcvrLineDist rld, tpoRcvrLine rl WITH (NOLOCK)"
    sSQL = sSQL & "WHERE rld.RcvrLineKey = " & lRcvrLineKey
    moAppDB.ExecuteSQL sSQL

    
    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub moDmDetl_DMGridBeforeInsert(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bValid = True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridAfterInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub moDmDetl_DMGridBeforeUpdate(lRow As Long, bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvrLineKey As Long
    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim dUnitCost As Double
    Dim dQtyRcvd As Double
    Dim lOldTranUOMKey As Long
    Dim lNewTranUOMKey As Long
    Dim lLandedCostLinesCnt As Long
    Dim sSQL As String
    
    
    bValid = False

    '-- Save the receiver line key for later deletion
    If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
        lRcvrLineKey = glGetValidLong(moDmDetl.Columns("RcvrLineKey").Value)
        mlRcvrLineKey = glGetValidLong(moAppDB.Lookup("RcvrLineKey", "tpoRcvrLine", "RcvrLineKey=" & lRcvrLineKey))
        
        'Check if there a saved landed cost transaction line.
        lLandedCostLinesCnt = glGetValidLong(moAppDB.Lookup("COUNT(*)", "tpoLandedCostLine", "RcvrLineKey=" & lRcvrLineKey))
        If lLandedCostLinesCnt > 0 Then
            'Check if the receiver line's UOM has been changed.  If so, we will need to sync the landed cost quantity
            'and unit cost fields since the quantity and the unit cost are based on the receiver line's UOM.
            lOldTranUOMKey = glGetValidLong(moAppDB.Lookup("UnitMeasKey", "tpoRcvrLine", "RcvrLineKey=" & lRcvrLineKey))
            lNewTranUOMKey = glGetValidLong(moDmDetl.GetUpdatedColumnValue(lRow, "UnitMeasKey"))
            If lOldTranUOMKey <> lNewTranUOMKey And lOldTranUOMKey <> 0 And lNewTranUOMKey <> 0 Then
                'Convert the landed cost quantity and unit cost fields to match the new unit of measure.
                sSQL = "UPDATE lcl"
                sSQL = sSQL & " SET LandedCostQty = ROUND(lcl.LandedCostQty * dbo.fnIMItemUOMConvFactor(rl.ItemRcvdKey, " & lOldTranUOMKey & ", " & lNewTranUOMKey & "), " & miQtyDecPlaces & ")"
                sSQL = sSQL & " ,UnitLandedCost = ROUND(lcl.UnitLandedCost / dbo.fnIMItemUOMConvFactor(rl.ItemRcvdKey, " & lOldTranUOMKey & ", " & lNewTranUOMKey & "), " & moOptions.CI("UnitCostDecPlaces") & ")"
                sSQL = sSQL & " ,OrigUnitLandedCost = ROUND(lcl.UnitLandedCost / dbo.fnIMItemUOMConvFactor(rl.ItemRcvdKey, " & lOldTranUOMKey & ", " & lNewTranUOMKey & "), " & moOptions.CI("UnitCostDecPlaces") & ")"
                sSQL = sSQL & " FROM tpoLandedCostLine lcl"
                sSQL = sSQL & " JOIN tpoRcvrLine rl ON lcl.RcvrLineKey = rl.RcvrLineKey"
                sSQL = sSQL & " WHERE rl.RcvrLineKey = " & lRcvrLineKey
                moAppDB.ExecuteSQL sSQL
            End If
        End If
    End If
    If mlTranType <> kTranTypePORTrn Then
        bValid = bValidateIMDist(lRow)
    Else
        bValid = True
    End If
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridBeforeUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub moDmDetl_DMGridAppend(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If (muItemInfo.iCmntOnly = kiOff) Then
        moDmLineDist.AppendRow
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridAppend", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmDetl_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Description:
'      Fires when each row of data is loaded by the DM into the grid.
'      This is a manual movement of a copy of the data from the DM to the correct
'      grid cell on the same row.
'************************************************************************************
    Dim sCompanyID As String
    Dim sGLAcctNo As String
    Dim sItemID As String
    Dim dAmt As Double
    Dim iReturnType As Integer
    Dim iMatchStatus As Integer
    
    '-- Set the masked Item ID column
    sItemID = gsGridReadCell(grdRcptDetl, lRow, kColItemID)
    gGridUpdateCell grdRcptDetl, lRow, kColMaskedItemID, sItemID
    
    dAmt = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvdS)) * miSignFactor
    gGridUpdateCell grdRcptDetl, lRow, kColQtyRcvd, CStr(dAmt)
    
    '-- Set the original tran key to the tran key just loaded.
    gGridUpdateCell grdRcptDetl, lRow, kColOrigTranKey, gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey)
    
    '-- Set the original return type.
    gGridUpdateCell grdRcptDetl, lRow, kColOrigRtrnType, giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColReturnType))
    
    '-- Set the original quantity ordered (receipts) or received (returns) for display in grid
    DisplayOriginalQty lRow

    '-- Load the associated PA link record into the link grid
    moProject.GridRowLoaded lRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmDetl_DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmDetl_DMGridValidate(lRow As Long, bValid As Boolean)

Dim lItemKey As Long
Dim lUOMKey As Long
Dim dItemVol As Double
Dim dRcvrVol As Double
Dim dItemWght As Double
Dim dRcvrWght As Double
Dim dQtyRcvd As Double
Dim dRetTranQty As Double
Dim lIMTempTranID As Long
Dim lInvtTranKey As Long

    bValid = True
    If Len(gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColWhseID))) > 0 Then
        lItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
        lUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey))
        Set moItem = moIMSClass.Items(lItemKey)
        If miCaptureWght = kvCaptureRequired And bValid Then
            dItemWght = moItem.Weight(lUOMKey)
            dRcvrWght = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemWght))
            If dItemWght = 0 And dRcvrWght = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgWeightRequired
                grdRcptDetl_Click 1, lRow
                bValid = False
            End If
        End If
        If miCaptureVol = kvCaptureRequired And bValid Then
            dItemVol = moItem.Volume(lUOMKey)
            dRcvrVol = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColItemVol))
            If dItemVol = 0 And dRcvrVol = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgVolumeRequired
                grdRcptDetl_Click 1, lRow
                bValid = False
            End If
        
        End If
        
    End If

    If mlTranType = kEntTypePOReturn Then
        If bValid = True Then
            bValid = bValidateIMDist(lRow)
        End If
    End If

End Sub



Private Sub moDmHeader_DMAfterDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim sErr As String
    Dim lBatchKey As Long
    Dim lRcvrKey As Long
    Dim lTranType As Long
    Dim iDocType As Integer
    Dim iRet As Integer

    bValid = False
    
    lBatchKey = glGetValidLong(moDmHeader.GetColumnValue("BatchKey"))
    lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    lTranType = glGetValidLong(moDmHeader.GetColumnValue("TranType"))

    '-- Delete the Receipt Log row
    If Not bDeleteReceiverLog(lRcvrKey) Then
        moDmHeader.CancelAction
        giSotaMsgBox Me, moSysSession, kmsgVoucherLogError, msRcvrNoLbl
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    With moAppDB
        '-- Update/delete Batch Tran records; Update Batch Total records
        .SetInParam lBatchKey
        .SetInParam lTranType
        .SetInParam CStr(0)  ' Old Amount
        .SetOutParam iRet                                                        ' Output parameter
        .ExecuteSP "spDeleteBatchTran"
        .ReleaseParams
    End With
    
    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMAfterDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMAfterInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bValid = False

    Dim lRcvrKey As Long
    
    lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    
    'Create and save landed cost transactions automatically for newly created receipts.
    'Note: Code is here because the moDMHeader is the last record to be saved based on its SaveOrder.
    If (mlVRunMode <> kViewEditReceiptDTE) And (mlVRunMode <> kViewEditReceiptDDN) Then
        If mlTranType = kTranTypePORG Then
            If Not bGetSaveLandedCostForNewRcpt(lRcvrKey) Then
'+++ VB/Rig Begin Pop +++
                Exit Sub
            End If
        End If
    End If

    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMAfterInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMAfterSaveCommitTrans()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'We are now outside the transaction, go ahead and display the informational message
    'that the landed cost amounts were automatically adjusted.
    If mbDisplayMsgPOLandCostAutoAdjAmtOnEdit = True Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgPOLandCostAutoAdjAmtOnEdit
        mbDisplayMsgPOLandCostAutoAdjAmtOnEdit = False
    End If
                
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMAfterSaveCommitTrans", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMAfterUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim bIsValid As Boolean
    Dim lRcvrKey As Long
    
    bIsValid = True

    'If a receipt line's quantity/uom, volume, or weight is changed the mbAdjToLandedCostAmtsRequired will be true.
    'Adjust any saved landed cost transactions automatically for existing receipts.
    'Note: Code is here because the moDMHeader is the last record to be saved based on its SaveOrder.
    If (mlVRunMode <> kViewEditReceiptDTE) And (mlVRunMode <> kViewEditReceiptDDN) Then
        If mlTranType = kTranTypePORG Then
            lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
            If Not bAdjustSaveLandedCostForExistingRcpt(lRcvrKey) Then
'+++ VB/Rig Begin Pop +++
                Exit Sub
            End If
        End If
    End If

    bValid = bIsValid
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMAfterUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub moDmHeader_DMBeforeInsert(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bValid = False
    
    With moDmHeader
        If (miSecurityLevel > kSecLevelDisplayOnly) Then
            '-- Update the receiver log totals
            If Not bUpdateReceiverLog(glGetValidLong(.GetColumnValue("RcvrKey")), _
                       gsGetValidStr(.GetColumnValue("TranID")), kvPendingReceiverLog) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            .SetColumnValue "TranType", mlTranType
            If mlWhseKey > 0 Then
                .SetColumnValue "WhseKey", mlWhseKey
            End If
        End If
    End With
    
    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMBeforeUpdate(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bValid = False
    
    With moDmHeader
        If (miSecurityLevel > kSecLevelDisplayOnly) Then
            '-- Update the receiver log totals
            If Not bUpdateReceiverLog(glGetValidLong(.GetColumnValue("RcvrKey")), _
                       gsGetValidStr(.GetColumnValue("TranID"))) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            If mlWhseKey > 0 Then
                .SetColumnValue "WhseKey", mlWhseKey
            End If
        End If
    End With
    
    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bDeleteIMDists() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRow As Long
    Dim lInvtTranKey As Long
    Dim lItemKey As Long
    Dim iOrigRtrnType As Integer
    Dim lTrnsfrOrderLineKey As Long
    Dim lRet As Integer
    Dim sSQL As String
    Dim lRcvrKey As Long
    Dim arrInvtTranKeys() As Long
    Dim lArrIndex As Long
    Dim lKeyCnt As Long
    Dim rs As Object
    
    bDeleteIMDists = False
    
    lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    
    'If this is return we handle with line-oriented IM routines, otherwise
    'use the newer set-based distribution routines.
    If mlTranType = kTranTypePORTrn Then
         For lRow = 1 To grdRcptDetl.MaxRows - 1
            lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
            lItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
            iOrigRtrnType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColOrigRtrnType))
            lTrnsfrOrderLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTrnsfrLineKey))
            If lInvtTranKey > 0 Then
                moIMDistribution.Delete lInvtTranKey, lItemKey, , , iOrigRtrnType, lItemKey, , lTrnsfrOrderLineKey
                If moIMDistribution.oError.Number <> 0 Then
                    giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                    moIMDistribution.oError.Clear
                    Exit Function
                End If
            End If
        Next
    Else 'Process as Receipt of Goods (from either PO or Transfer)
        
        'Delete the item distribution records for the shipment
        'Using the ShipKey build an array of InvtTranKeys to delete.
        
        lKeyCnt = glGetValidLong(moAppDB.Lookup("COUNT(1)", "tpoRcvrLine WITH (NOLOCK)", "tpoRcvrLine.InvtTranKey IN " & _
                "(SELECT itd.InvtTranKey FROM timInvtTranDist itd WITH (NOLOCK)) AND tpoRcvrLine.RcvrKey = " & lRcvrKey))
        
        If lKeyCnt > 0 Then
            ReDim arrInvtTranKeys(lKeyCnt - 1)
            
            sSQL = "SELECT DISTINCT tpoRcvrLine.InvtTranKey FROM tpoRcvrLine WITH (NOLOCK) " & _
                    " WHERE tpoRcvrLine.InvtTranKey IN (SELECT itd.InvtTranKey FROM timInvtTranDist itd WITH (NOLOCK)) " & _
                    " AND tpoRcvrLine.RcvrKey = " & lRcvrKey
            
            Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
            'Load the transit InvtTranKey array.
            lArrIndex = 0
            Do While Not rs.IsEOF
                arrInvtTranKeys(lArrIndex) = glGetValidLong(rs.Field("InvtTranKey"))
                lArrIndex = lArrIndex + 1
                rs.MoveNext
            Loop
            
            If lArrIndex <> 0 Then
                If bIsReceiptDistDeleted(arrInvtTranKeys) = False Then
                    giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                    moIMDistribution.oError.Clear
                    Exit Function
                End If
            End If
        End If
    End If

    bDeleteIMDists = True
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteIMDists", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

Private Function bIsReceiptDistDeleted(arrInvtTranKeys() As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        
    bIsReceiptDistDeleted = False

    'Delete the transit warehouse distributions first (if any).
    If bIsReceiptTrnsfrDistDeleted(arrInvtTranKeys) = False Then
        Exit Function
    End If

    If moIMDistribution.DeleteDistReceipt(arrInvtTranKeys) = False Then
        Exit Function
    End If
    
    bIsReceiptDistDeleted = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsReceiptDistDeleted", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsReceiptTrnsfrDistDeleted(arrReceiptInvtTranKeys() As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim arrTransitInvtTranKeys() As Long
    Dim rs As Object
    Dim sSQL As String
    Dim sKeyList As String
    Dim lArrIndex As Long
    
    bIsReceiptTrnsfrDistDeleted = False
        
    'Using the arrReceiptInvtTranKeys, get the matching TransitInvtTranKeys and load
    'it in arrTransitInvtTranKeys
    
    ReDim arrTransitInvtTranKeys(UBound(arrReceiptInvtTranKeys))
    
    sKeyList = sGetInvtTranKeyList(arrReceiptInvtTranKeys)
    
    sSQL = "SELECT DISTINCT TransitInvtTranKey FROM tpoRcvrLine WITH (NOLOCK) " & _
           "WHERE TransitInvtTranKey IS NOT NULL AND InvtTranKey IN (" & sKeyList & ")"
    
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    'Load the transit InvtTranKey array.
    lArrIndex = 0
    Do While Not rs.IsEOF
        arrTransitInvtTranKeys(lArrIndex) = glGetValidLong(rs.Field("TransitInvtTranKey"))
        lArrIndex = lArrIndex + 1
        rs.MoveNext
    Loop

    If lArrIndex <> 0 Then
        If moIMDistribution.DeleteDistTrnsfrInOut(arrTransitInvtTranKeys, kTranTypePOTR) = False Then
            Exit Function
        End If
    End If
    
    bIsReceiptTrnsfrDistDeleted = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsReceiptTrnsfrDistDeleted", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function sGetInvtTranKeyList(vInvtTranKeyArr As Variant) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'===========================================================================
'DESCRIPTION: This procedure builds a string contain a list of InvtTranKeys.
'===========================================================================

    Dim i                 As Integer
    Dim iArgs             As Integer
    Dim sInvtTranKeyList  As String

    sGetInvtTranKeyList = ""
    
    If IsArray(vInvtTranKeyArr) = True Then
        On Error GoTo ArrayErr
        iArgs = UBound(vInvtTranKeyArr)
        On Error GoTo VBRigErrorRoutine
        If iArgs >= 0 Then
            For i = 0 To iArgs
                If Len(CStr(vInvtTranKeyArr(i))) <> 0 Then
                    sInvtTranKeyList = sInvtTranKeyList & CStr(vInvtTranKeyArr(i)) & ","
                End If
            Next
        End If
    Else
        sInvtTranKeyList = gsGetValidStr(vInvtTranKeyArr)
    End If
    
    'Remove the trailing "," from the end of the sInvtTranKeyList.
    If Right(sInvtTranKeyList, 1) = "," Then
        sInvtTranKeyList = Mid(sInvtTranKeyList, 1, Len(sInvtTranKeyList) - 1)
    End If
    
    sGetInvtTranKeyList = sInvtTranKeyList

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}

ArrayErr:
    'If "Subscript out of Range Error" (likely due to an empty array),
    'just exit function.
    If Err.Number = 9 Then
        Exit Function
    End If

VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sGetInvtTranKeyList", VBRIG_IS_CLASS                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub moDmHeader_DMBeforeDelete(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim sErr As String
    Dim lBatchKey As Long
    Dim lRcvrKey As Long
    Dim lTranType As Long
    
    bValid = False
    
    lBatchKey = glGetValidLong(moDmHeader.GetColumnValue("BatchKey"))
    lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    lTranType = glGetValidLong(moDmHeader.GetColumnValue("TranType"))
    
   If Not (IsNull(mlWhseKey)) And Not (IsEmpty(mlWhseKey)) And (mlWhseKey <> 0) Then
        If bDeleteIMDists = False Then
            Exit Sub
        End If
    End If
    
' Defect 34087 - Move after so a) is available and b) doesn't disappear if delete fails
    '-- Delete Receiver Line Dists First
    sSQL = "DELETE tpoRcvrLineDist "
    sSQL = sSQL & "FROM tpoRcvrLineDist rld, tpoRcvrLine rl WITH (NOLOCK)"
    sSQL = sSQL & "WHERE rld.RcvrLineKey = rl.RcvrLineKey AND rl.RcvrKey = " & lRcvrKey
    moAppDB.ExecuteSQL sSQL
    
    
    '-- Delete receiver lines
    sSQL = "DELETE tpoRcvrLine WHERE RcvrKey = " & lRcvrKey
    
    moAppDB.ExecuteSQL sSQL
    
    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMDataDisplayed(oChild As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvrKey As Long
    Dim lKey As Long
    Dim bNegativeDoc As Boolean
    Dim sSQL As String
    Dim rs As Object
    
    If oChild Is moDmHeader Then
    
        lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        
        '-- Create the temporary join to display PO info in the grid
        If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Or _
           (mlVRunMode = kViewEditReceiptDTE) Or (mlVRunMode = kViewEditReceiptDDN) Then
            Me.SOTAValidationMgr1.StopValidation
            CreatePONoLinkForGrid moAppDB, msCompanyID, lRcvrKey, mlVRunMode, mlTranType
            Me.SOTAValidationMgr1.StartValidation
        End If
        
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMPostSave(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer
    Dim sSQL As String
    Dim rs As Object
    Dim tableExist As Integer
    Dim sLoginName  As String
    Dim lRet As Integer
    Dim lRcvrLineKey As Integer
    Dim lRow As Long
            
    bValid = False
    
    'Create Temp Table #tsoVoidedInvtLogEntries to store the existing inventory transactions used by the current transaction
    sSQL = "IF OBJECT_ID ('tempdb..#tsoVoidedInvtLogEntries') IS NULL "
    sSQL = sSQL & " BEGIN"
    sSQL = sSQL & " CREATE TABLE #tsoVoidedInvtLogEntries "
    sSQL = sSQL & "(InvtTranKey  INTEGER NOT NULL) "
    sSQL = sSQL & " END"
    sSQL = sSQL & " ELSE"
    sSQL = sSQL & " BEGIN"
    sSQL = sSQL & " TRUNCATE TABLE #tsoVoidedInvtLogEntries "
    sSQL = sSQL & " END"
    moClass.moAppDB.ExecuteSQL sSQL
    

    'If this is a receipt of goods (from PO or Transfer), then call the
    'newer set-orietned distribution routine.
    If mlTranType <> kTranTypePORTrn Then
        If Not (moIMDistribution Is Nothing) Then  'will be nothing when batch has no warehouse.
            If Not moIMDistribution.SaveDistPOReceipt(glGetValidLong(moDmHeader.GetColumnValue("RcvrKey")), gsFormatDateToDB(calRcptDate.SQLDate)) Then
                moDmHeader.CancelAction
                giSotaMsgBox Me, moSysSession, kmsgTranQtyDistQtyDiffer
                moIMDistribution.oError.Clear
                mlInvalidRow = 1
                bValid = False
                
                Exit Sub
            End If
        End If
    Else
        'Returns are processed using older style line-oriented IM routines -
        If Not moIMDistribution Is Nothing Then
           
            'Save the existing InvtTranKey used for this transaction which are currently stored in #timInvtDistWrk
            'The keys need to be saved before calling moIMDistribution.Save sine at the end of moIMDistribution.Save
            'the records in #timInvtDistWrk will be deleted
            sSQL = "INSERT #tsoVoidedInvtLogEntries (InvtTranKey) "
            sSQL = sSQL & " SELECT DISTINCT wrk.TranKey from #timInvtDistWrk wrk WITH (NOLOCK)"
            moClass.moAppDB.ExecuteSQL sSQL
      
            bValid = moIMDistribution.Save("", 0, 0, 0, 0, 0, 0)
            If moIMDistribution.oError.Number <> 0 Or Not bValid Then
                '-- Display error message
                moDmHeader.CancelAction
                giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                moIMDistribution.oError.Clear
                mlInvalidRow = lRow 'balzer
                bValid = False
                
                Exit Sub
            End If
        End If
    End If
    
    'Check whether any of existing Invt Transaction has been deleted, if they are, the related timInvtTranDist record will be deleted.
    'Delete the InvtTranKey that are still tied to a timInvtTranDist record.
    sSQL = "DELETE #tsoVoidedInvtLogEntries "
    sSQL = sSQL & " FROM #tsoVoidedInvtLogEntries "
    sSQL = sSQL & " JOIN timInvtTranDist td WITH (NOLOCK) ON td.invttrankey = #tsoVoidedInvtLogEntries.InvtTranKey"
    moClass.moAppDB.ExecuteSQL sSQL
    
    'Update the status in timInvtTranLog to void for the deleted InvtTranKey
    sSQL = "UPDATE timInvtTranLog "
    sSQL = sSQL & "SET TranDate = " & gsQuoted(calRcptDate.SQLDate) & ", "
    sSQL = sSQL & "TranStatus = " & Format$(5) & Space$(1)     'Void
    sSQL = sSQL & " FROM timInvtTranLog tl "
    sSQL = sSQL & " JOIN #tsoVoidedInvtLogEntries td WITH (NOLOCK) ON td.InvtTranKey = tl.InvtTranKey"
    moClass.moAppDB.ExecuteSQL sSQL

    'Set the InvtTranKey to NULL in the related receiver line where its distribution has been deleted.
    sSQL = "UPDATE tpoRcvrLine "
    sSQL = sSQL & "SET InvtTranKey = NULL"
    sSQL = sSQL & " FROM tpoRcvrLine tl"
    sSQL = sSQL & " JOIN #tsoVoidedInvtLogEntries td WITH (NOLOCK) ON td.InvtTranKey = tl.InvtTranKey"
    moClass.moAppDB.ExecuteSQL sSQL
    
    If mlTranType = kTranTypePOTR Then
        'For Receipt Transfers, update tpoRcvrLine.TransitInvtTranKey
        moAppDB.ExecuteSQL "UPDATE tpoRcvrLine SET TransitInvtTranKey = wrk.TransitInvtTranKey " & _
                            " FROM tpoRcvrLine WITH (NOLOCK) " & _
                            " JOIN #timInvtDistTrnsfrWrk wrk ON tpoRcvrLine.InvtTranKey = wrk.SourceInvtTranKey " & _
                            " WHERE tpoRcvrLine.TrnsfrOrderLineKey IS NOT NULL"
                         
    End If
    
    If (moDmHeader.State <> kDmStateEdit) Then
        With moAppDB
            '-- Update Batch Transaction and Totals
            .SetInParam glGetValidLong(moDmHeader.GetColumnValue("BatchKey"))
            .SetInParam glGetValidLong(moDmHeader.GetColumnValue("TranType"))
    ' Nothing to do with amounts - pass in dummy values
            .SetInParam "0"
            .SetInParam "0"
            .SetInParam 1   '-- Insert a Tran record
            .SetOutParam iRetVal
            .ExecuteSP "spUpdateBatchTran"
            iRetVal = giGetValidInt(.GetOutParam(6))
            .ReleaseParams
        
        End With
        
    End If
    
    
        sSQL = "SELECT tableExist = COUNT(OBJECT_ID('tempdb..#timInventory')) + COUNT(OBJECT_ID('timInventory'))"
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            tableExist = glGetValidLong(rs.Field("tableExist"))
        Else
            tableExist = 0
        End If
        
        If (tableExist = 2) Then
        
            With moAppDB
                .SetOutParam sLoginName
                .ExecuteSP "spGetLoginName"
                sLoginName = .GetOutParam(1)
                .ReleaseParams
            End With

            sSQL = "Update timInventory " & _
                    "SET QtyOnPO = i.QtyOnPO + i1.tmpQtyOnPO, " & _
                    "    QtyOnSO = i.QtyOnSO + i1.tmpQtyOnSO, " & _
                    "    UpdateDate = getdate(), UpdateUserID = " & gsQuoted(sLoginName) & _
                     " FROM #timInventory i1 " & _
                     " JOIN timInventory i WITH (NOLOCK) ON i.ItemKey = i1.ItemKey AND i.WhseKey = i1.WhseKey "

            moAppDB.ExecuteSQL sSQL
            moAppDB.ExecuteSQL "DROP TABLE #timInventory "
            
       End If
                            
    UpdateTranLogRecs
    
    bValid = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMPostSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub moDmHeader_DMReposition(oChild As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lVendKey As Long
    Dim lRcvrKey As Long
    Dim oToolbarMenu As clsToolbarMenuInfo
    


    If oChild Is moDmHeader Then
        If (moDmHeader.State = kDmStateAdd) Then
            ProcessKeyNotFound
        Else
            ProcessKeyFound
        End If
        
        moProject.ResetJobGrid
        
        'Set Memo button state (for drop-down multiple-memo implementation)
        lVendKey = gvCheckNull(moDmHeader.GetColumnValue("VendKey"), SQL_INTEGER)
        lRcvrKey = gvCheckNull(moDmHeader.GetColumnValue("RcvrKey"), SQL_INTEGER)
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypePOReceipt)
        oToolbarMenu.lKey2 = lRcvrKey
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeAPVendor)
        oToolbarMenu.lKey2 = lVendKey
        gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain

    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMStateChange(iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    '-- Set toolbar button states
    SetTBButtonStates
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moDmHeader_DMBeforeTransaction(bValid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************
' Description:
'    This routine will be called by Data Manager before the record
'    is saved. This is where form-level validation should occur.
'*******************************************************************
    Dim lKey As Long
    Dim dExchangeRate As Double
    Dim iRet As Integer
    Dim sTranID As String
    Dim lRow As Long
    
    bValid = False

    mbOverflow = False

    If mlTranType <> kTranTypePOTR Then
        '-- Vendor ID cannot be blank
        If (Len(Trim$(lkuVendID)) = 0) Then
            giSotaMsgBox Me, moSysSession, kmsgCannotBeBlank, gsStripChar(lblVendID, kAmpersand)
            SetCtrlFocus lkuVendID
            Exit Sub
        End If
    End If
    
    If mlTranType = kTranTypePORG Then
        '-- PO cannot be blank
        If (Len(Trim$(lkuPONo)) = 0) Then
            giSotaMsgBox Me, moSysSession, kmsgCannotBeBlank, gsStripChar(lblPONo, kAmpersand)
            SetCtrlFocus lkuPONo
            Exit Sub
        End If
    ElseIf mlTranType = kTranTypePOTR Then
        '-- Transfer cannot be blank
        If (Len(Trim$(lkuTransfer)) = 0) Then
            giSotaMsgBox Me, moSysSession, kmsgCannotBeBlank, gsStripChar(lblTransfer, kAmpersand)
            SetCtrlFocus lkuTransfer
            Exit Sub
        End If
        '-- Ship Whse cannot be blank
        If (Len(Trim$(lkuShipWhse)) = 0) Then
            giSotaMsgBox Me, moSysSession, kmsgCannotBeBlank, gsStripChar(lblShipWhse, kAmpersand)
            SetCtrlFocus lkuShipWhse
            Exit Sub
        End If
    Else
        '-- Receiver cannot be blank
        If (Len(Trim$(lkuReceiver)) = 0) Then
            giSotaMsgBox Me, moSysSession, kmsgCannotBeBlank, gsStripChar(lblReceiver, kAmpersand)
            SetCtrlFocus lkuReceiver
            Exit Sub
        End If
    End If

   '-- Tran Date cannot be blank
    If (Len(Trim$(calRcptDate)) = 0) Then
        giSotaMsgBox Me, moSysSession, kmsgCannotBeBlank, _
            gsStripChar(lblRcptDate, kAmpersand)
        
        '-- Change to Header tab so user can rectify the situation
        tabReceipt.Tab = kiHeaderTab
        SetCtrlFocus calRcptDate
        Exit Sub
    End If

'-- From DMValidate...
    If (mlVRunMode = kEnterReceiptDTE) Or (mlVRunMode = kEnterReceiptAOF) Or _
       (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
        '-- Make sure all DM columns are set before the save
        With moDmHeader
            '-- Delete any 0 quantity lines associated with a PO
            If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
                DeleteZeroQtyPOLinesFromGrid Me, moDmDetl, moLE
            End If
                
            '-- Format the TranID based on the TranNo
            If mlTranType <> kTranTypePORTrn Then
                sTranID = Trim$(lkuReceiptNo) & "-" & msTranType
            Else
                sTranID = Trim(lkuReturn) & "-" & msTranType
            End If
            .SetColumnValue "TranID", sTranID
        End With
    
    End If
    
    With moDmHeader
        '-- Set other DM column values
        .SetColumnValue "CreateType", kCreateTypeStandard
        
        
        If mlTranType = kTranTypePORG Then
        '-- POKey is not being populated. Why, you might ask?
        '-- Because it's Data Mangler!
            If (glGetValidLong(lkuPONo.KeyValue) = 0) Then
                .SetColumnValue "POKey", Empty
            Else
                .SetColumnValue "POKey", glGetValidLong(lkuPONo.KeyValue)
            End If
        ElseIf mlTranType = kTranTypePOTR Then
        '-- Transfer is not being populated. Why, you might ask?
        '-- Because it's Data Mangler!
            If (glGetValidLong(lkuTransfer.KeyValue) = 0) Then
                .SetColumnValue "TrnsfrOrderKey", Empty
            Else
                .SetColumnValue "TrnsfrOrderKey", glGetValidLong(lkuTransfer.KeyValue)
            End If
        Else
        '-- ReceiptKey is not being populated. Why, you might ask?
        '-- Because it's Data Mangler!
            If (glGetValidLong(lkuReceiver.KeyValue) = 0) Then
                .SetColumnValue "RtrnRcvrKey", Empty
            Else
                .SetColumnValue "RtrnRcvrKey", glGetValidLong(lkuReceiver.KeyValue)
            End If
        End If
        
        .SetDirty True, False
    End With
    
    On Error Resume Next
    moAppDB.ExecuteSQL "DROP TABLE #timInventory "
    
    On Error GoTo VBRigErrorRoutine
    moAppDB.ExecuteSQL "CREATE TABLE #timInventory " & _
                       "(WhseKey int NOT NULL, " & _
                       "ItemKey int NOT NULL, " & _
                       "QtyOnSO decimal (25, 13) DEFAULT 0 NULL, " & _
                       "QtyOnPO decimal (25, 13) DEFAULT 0 NULL, " & _
                       "QtyReqForTrnsfr decimal (25, 13) DEFAULT 0 NULL, " & _
                       "QtyOnTrnsfr decimal (25, 13) DEFAULT 0 NULL, " & _
                       "bDelete bit DEFAULT 0, " & _
                       "bInsert bit DEFAULT 0, " & _
                       "tmpQtyOnSO decimal (25, 13) DEFAULT 0 NULL, " & _
                       "tmpQtyOnPO decimal (25, 13) DEFAULT 0 NULL, " & _
                       "tmpQtyReqForTrnsfr decimal (25, 13) DEFAULT 0 NULL, " & _
                       "tmpQtyOnTrnsfr decimal (25, 13) DEFAULT 0 NULL)"
    
    moAppDB.ExecuteSQL "CREATE CLUSTERED INDEX tempTimInv_cls_ind ON #timInventory(WhseKey, ItemKey) "
           
    If mlTranType = kTranTypePORG Or mlTranType = kTranTypePOTR Then
        
        For lRow = 1 To grdRcptDetl.DataRowCnt
            'Check if distributions are complete.
            bValid = bValidateIMDist(lRow)
            
            'If the OrigTranKey is blank (i.e. this transaction has not been saved), then assign
            'the InvtTranKey as the OrigTranKey.  This will allow the distributions created during
            'save to be available if the user decides to cancel the save and view the distributions.
            If Len(gsGridReadCell(grdRcptDetl, lRow, kColOrigTranKey)) = 0 And gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey) <> "" Then
                gGridUpdateCell grdRcptDetl, lRow, kColOrigTranKey, gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey)
            End If
    
            If bValid = False Then
                Exit Sub
            End If
   
        Next lRow
    End If

    bValid = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDmHeader_DMBeforeTransaction", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bQtyValid(sMessage As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvrKey As Long
    Dim lPOLineDistKey As Long
    Dim lRtrnLineDistKey As Long
    Dim lRetCode As Long
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim dConvertedQtyRcvd As Double
    Dim dConvFactor As Double
    Dim dUnitCost As Double
    Dim lUOMRcvdKey As Long
    Dim rs As Object
    Dim sSQL As String
    Dim dQtyAvail As Double
    Dim dUnitCostExact As Double
        
    bQtyValid = False
    If mlItemKey = 0 Then
        bQtyValid = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
        
    lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    lPOLineDistKey = glGetValidLong(gsGridReadCell(grdRcvrLineDist, 1, kColPOLineDistKeyD))
    dConvertedQtyRcvd = gdGetValidDbl(nbrQtyRcvd)
    If dConvertedQtyRcvd < 0 Then
        If mlTranType <> kTranTypePORTrn Then
            sMessage = gsBuildString(ksCannotBeNegative, moClass.moAppDB, moSysSession, gsStripChar(lblQtyRcvd.Caption, "&"))
        Else
            sMessage = gsBuildString(ksCannotBeNegative, moClass.moAppDB, moSysSession, gsStripChar(lblQtyReturn.Caption, "&"))
        End If
        nbrQtyRcvd.Value = nbrQtyRcvd.Tag
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    lUOMRcvdKey = ddnUOM.ItemData
    
' First, convert the Quantity recv to PO/Transfer UOM
' If the UOM key for PO/Transfer is not filled in, get the key from the PO/Transfer Line table
    If mlSourceUOMKey = 0 Then
        If mlTranType = kTranTypePOTR Then
            sSQL = "SELECT UnitMeasKey FROM timTrnsfrOrderLine WHERE TrnsfrOrderLineKey = " & mlTrnsfrOrderLineKey
        Else
            sSQL = "SELECT UnitMeasKey FROM tpoPOLine WITH (NOLOCK) WHERE POLineKey = " & mlPOLineKey
        End If
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            mlSourceUOMKey = glGetValidLong(rs.Field("UnitMeasKey"))
        Else
            mlSourceUOMKey = 0
        End If
        If Not rs Is Nothing Then
            rs.Close: Set rs = Nothing
        End If
    End If
    moItem.UOMConversion lUOMRcvdKey, mlSourceUOMKey, dConvertedQtyRcvd, dConvFactor
    dConvertedQtyRcvd = IMUtils.gdRoundItemQty(moClass.moAppDB, dConvertedQtyRcvd, mlItemKey, mlSourceUOMKey)
    dUnitCost = mdUnitCost
    dUnitCostExact = mdUnitCostExact

    ' Now, get the conversion factor between the old and new UOM to convert the unit cost.
    moItem.UOMConversion mlUOMKey, lUOMRcvdKey, 0, dConvFactor
    If dConvFactor = 0 Then
        dUnitCost = 0
    Else
        dUnitCost = dUnitCost / dConvFactor
        dUnitCostExact = dUnitCostExact / dConvFactor
    End If
    
    
    'Validate for transfer order receipts:
    'Only allow non-tracked items to receive more than the qty available in the transit warehouse.
    If mlTranType = kTranTypePOTR Then
       dQtyAvail = gdGetValidDbl(gsGridReadCell(grdRcptDetl, grdRcptDetl.ActiveRow, kColOrigQtyRcvd))
       
       If dQtyAvail = 0 Then
            dQtyAvail = dGetAvailTrnsfrRcvgQty(glGetValidLong(gsGridReadCell(grdRcptDetl, grdRcptDetl.ActiveRow, kColTrnsfrLineKey)))
       End If
       
       If (dConvertedQtyRcvd > dQtyAvail) And (moItem.TrackMethod <> IMS_TRACK_METHD_NONE) Then
            sMessage = gsBuildString(ksCannotRcvMoreThanAvail, moClass.moAppDB, moSysSession)
            nbrQtyRcvd.Value = nbrQtyRcvd.Tag
        Else
            'Qty Received is valid
            bQtyValid = True
            mdQtyInSrcUOM = dConvertedQtyRcvd
            mdUnitCost = dUnitCost
            mdUnitCostExact = dUnitCostExact
            miIMDistDirty = 1
            nbrQtyRcvd.Tag = nbrQtyRcvd.Text
       End If
       Exit Function
    End If
    
    If mlTranType <> kTranTypePORTrn Then
        With moAppDB
            .SetInParam lRcvrKey
            .SetInParam lPOLineDistKey
            .SetInParam dConvertedQtyRcvd
            .SetOutParam lRetCode
            .ExecuteSP "sppoValQtyToReceive"
            lRetCode = .GetOutParam(4)
            .ReleaseParams
        End With
    Else
        lRtrnLineDistKey = glGetValidLong(gsGridReadCell(grdRcvrLineDist, 1, kcolRtrnRcvrLineDistKeyD))
        With moAppDB
            .SetInParam lRcvrKey
            .SetInParam lRtrnLineDistKey
            .SetInParam dConvertedQtyRcvd
            .SetOutParam lRetCode
            .ExecuteSP "sppoValQtyToReturn"
            lRetCode = .GetOutParam(4)
            .ReleaseParams
        End With
    End If
    
    If (lRetCode = 0) Then
        'Qty Received is greater than Qty Available...
        If mlTranType <> kTranTypePORTrn Then
            sID = "RCVQTYOVER"
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            If (moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) <> 0) Then
                bQtyValid = True
                mdQtyInSrcUOM = dConvertedQtyRcvd
                mdUnitCost = dUnitCost
                mdUnitCostExact = dUnitCostExact
                GetFreightAndSTax lPOLineDistKey, mdQtyInSrcUOM
                miIMDistDirty = 1
                nbrQtyRcvd.Tag = nbrQtyRcvd.Text
            End If
        Else
            sMessage = gsBuildString(ksCannotRtrnMoreThanRcvd, moClass.moAppDB, moSysSession)
        End If
    Else
        'Qty Received is less than or equal to Qty Available...
        bQtyValid = True
        mdQtyInSrcUOM = dConvertedQtyRcvd
        mdUnitCost = dUnitCost
        mdUnitCostExact = dUnitCostExact
        If mlTranType = kTranTypePORG Then
            GetFreightAndSTax lPOLineDistKey, mdQtyInSrcUOM
        End If
        'NOTE: For Transfers, STax=0, Freight is computed during posting
        miIMDistDirty = 1
        nbrQtyRcvd.Tag = nbrQtyRcvd.Text
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bQtyValid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function


Private Sub GetFreightAndSTax(lPOLineDistKey As Long, dQtyRcvd As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    
    With moAppDB
        .SetInParam lPOLineDistKey
        .SetInParam dQtyRcvd
        .SetOutParam mdFreightAmt
        .SetOutParam mdSTaxAmt
        .ExecuteSP "sppoGetRcvrFreightAndSTax"
        
        If (Err.Number <> 0) Then
            giSotaMsgBox Me, moSysSession, kmsgGetVouchNoError, msRcvrNoLbl, msRcvrNoLbl
        Else
            mdFreightAmt = gdGetValidDbl(.GetOutParam(3))
            mdSTaxAmt = gdGetValidDbl(.GetOutParam(4))
        End If
        
        .ReleaseParams
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "GetFreightAndSTax", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




Private Sub NavPO_Click()
    If Trim(lkuReceiptNo.Text) = "" Then Exit Sub
    'Trigger the lookup navigator click
    lkuPONo.DoLookupClick
End Sub

Private Sub nbrQtyRcvd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyRcvd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    '-- Turn on the detail command buttons
    moLE.GridEditChange nbrQtyRcvd
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "nbrQtyRcvd_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetCredit_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optRetCredit, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbManualClick Then
        moLE.GridEditChange optRetCredit
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optRetCredit_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub optRetReplace_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optRetReplace, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbManualClick Then
        moLE.GridEditChange optRetReplace
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optRetReplace_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub sbrMain_ButtonClick(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick sButton
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SOTAValidationMgr1_KeyChange()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
        
    mbFromCode = True
    
    '-- Only allow KeyChange once per record;
    '-- weird behavior happening in compiled version vis a vis Next Number logic.
    '-- mbAllowKeyChange tells us if the KeyChange has already occurred
    If mbAllowKeyChange Then
        mbAllowKeyChange = False
        bValid = bIsValidRcptNo()
        mbAllowKeyChange = Not bValid
    End If

    mbFromCode = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SOTAValidationMgr1_KeyChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Function bIsValidRcptNo() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRet As Integer
    
    bIsValidRcptNo = False
    mbValidKeyChange = True
    mbInRcptNoValidation = True
    mbLoadingRec = True
    
    '-- Set the Company ID in DM
    moDmHeader.SetColumnValue "CompanyID", msCompanyID
    moDmHeader.SetColumnValue "TranType", mlTranType

    '-- Zero-fill the Number if appropriate
    If mlTranType <> kTranTypePORTrn Then
        lkuReceiptNo = sZeroFillNumber(lkuReceiptNo.MaskedText, kvRcptTranSize)
    Else
        lkuReturn = sZeroFillNumber(lkuReturn.MaskedText, kvRcptTranSize)
    End If
    
    '-- Perform some checks before KeyChange
    If bProcessPreKeyChange() Then
        iRet = moDmHeader.KeyChange()
        
        If (iRet = kDmNotAllowed) Then
            ProcessKeyNotAllowed
        End If
    End If

    If mbValidKeyChange Then
        Debug.Print 'bIsValidRcptNo: Valid Key Change'
        ProcessKeyFoundDetl
        ' -- Prevent an extra line from being added on the end.
'        grdRcptDetl.MaxRows = grdRcptDetl.MaxRows - 1
        '-- Determine whether to enable the Lines... button for PO
        If mlTranType = kTranTypePORG Then
            If (Len(Trim$(lkuPONo)) > 0) Then ToggleCtrlState cmdLines, True
            'When the PO Number is loaded, make sure to run through the logic to set up the
            'PO defaults
            If bIsValidPONo(Me, moDmDetl, moLE, msHomeCurrID) Then
                lkuPONo.Tag = lkuPONo.Text
            End If
        ElseIf mlTranType = kTranTypePORTrn Then
            If (Len(Trim$(lkuReceiver)) > 0) Then ToggleCtrlState cmdLines, True
            txtRelNo.Text = Mid(lkuPONo.Text, 12, 4)
            lkuReceiver.Tag = lkuReceiver.Text
        End If
        
        '-- Set focus to the correct field
        SetKeyChangeFocus iRet
                
        If (moDmHeader.State = kDmStateEdit) Then
            '-- Set the data manager object and all children to clean
            '-- (the header object was becoming dirty somewhere in the display process)
            moDmHeader.SetDirty False, True
        End If
        
        If mlTranType <> kTranTypePORTrn Then
            ddnType.Enabled = False
            lkuReceiptNo.Protected = True
        Else
            lkuReturn.Protected = True
        End If
        
        bIsValidRcptNo = True
    Else
        Debug.Print 'bIsValidRcptNo: Not Valid Key Change'
        '-- Clear the form
        ProcessCancel
    End If
    
    mbLoadingRec = False
    mbInRcptNoValidation = False
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidRcptNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function SetKeyChangeFocus(iKCState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If (iKCState = kDmKeyNotFound) Then
        '-- Set focus to the next appropriate control for a new record
        If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
            SetCtrlFocus IIf(txtPONum.Visible, txtPONum, lkuTransfer)
        Else
            SetCtrlFocus IIf(txtPONum.Visible, txtPONum, lkuTransfer)
        End If
    ElseIf (iKCState = kDmKeyFound) Then
        '-- Set focus to the next appropriate control for an existing record
        If (mlVRunMode = kEnterReceiptDTE) Or (mlVRunMode = kEnterReceiptAOF) Then
            SetCtrlFocus IIf(txtPONum.Visible, txtPONum, lkuTransfer)
        ElseIf (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
            SetCtrlFocus IIf(txtPONum.Visible, txtPONum, lkuTransfer)
        Else
            SetCtrlFocus txtBOL
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetKeyChangeFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bProcessPreKeyChange() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Perform processing before a DM KeyChange is fired; verify
'       that the Recepit number entered is in the correct batch.
'******************************************************************
    Dim lReceiptBatchKey As Long
    Dim lRcvrKey As Long
    Dim sSQL As String
    Dim sBatchNo As String
    Dim sTranID As String
    Dim bValid As Boolean
    Dim rs As DASRecordSet
    Dim lTranType As Long
    
    bProcessPreKeyChange = False
    
    '-- Verify that this receipt is in the correct batch
    If mlTranType <> kTranTypePORTrn Then
        lReceiptBatchKey = glGetValidLong(moAppDB.Lookup("BatchKey", "tpoPendReceiver", _
                         "CompanyID = " & gsQuoted(msCompanyID) & _
                         " AND TranNo = " & gsQuoted(lkuReceiptNo)))
    Else
        lReceiptBatchKey = glGetValidLong(moAppDB.Lookup("BatchKey", "tpoPendReceiver", _
                         "CompanyID = " & gsQuoted(msCompanyID) & _
                         " AND TranNo = " & gsQuoted(lkuReturn) & _
                         " AND TranType = " & mlTranType))
    End If
    
    If (lReceiptBatchKey <> 0) And (lReceiptBatchKey <> mlBatchkey) Then
        '-- The receipt is not in the correct batch; error
        sBatchNo = gsGetValidStr(moAppDB.Lookup("BatchNo", "tciBatchLog", _
                       "BatchKey = " & lReceiptBatchKey))
                       
        giSotaMsgBox Me, moSysSession, kmsgRcptNotInThisBatch, _
            msRcvrNoLbl, msRcvrNoLbl, sBatchNo
            
        mbValidKeyChange = False
        Exit Function
    End If
    
    'Change the form to match the TranType, if necessary...
    If mlTranType <> kTranTypePORTrn Then
        lTranType = glGetValidLong(moAppDB.Lookup("TranType", "tpoPendReceiver", _
                         "CompanyID = " & gsQuoted(msCompanyID) & _
                         " AND TranNo = " & gsQuoted(lkuReceiptNo)))
        If (mlTranType <> lTranType) And (lTranType <> 0) Then
            mlTranType = lTranType
            moDmHeader.SetColumnValue "TranType", mlTranType
            If lTranType = TransactionTypes.kTranTypePORG Then
                ddnType.ListIndex = 0
            Else
                ddnType.ListIndex = 1
            End If
        End If
    End If
    
    
    bProcessPreKeyChange = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bProcessPreKeyChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub ProcessKeyFound()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Processing for when an existing receipt number is entered.
'******************************************************************
    Dim lRcvrKey As Long
    Dim lPurchAddrKey As Long
    Dim sTranID As String
    Dim bValid As Boolean
    Dim rs As Object
    Dim sSQL As String
    
    With moDmHeader
        '-- Check the Receiver Log Status to make sure it is OK
        lRcvrKey = glGetValidLong(.GetColumnValue("RcvrKey"))
        sTranID = gsGetValidStr(.GetColumnValue("TranID"))
        If mlTranType <> kTranTypePORTrn Then
            If Not bCheckReceiverLogStatus(lkuReceiptNo, lRcvrKey, sTranID) Then
                mbValidKeyChange = False
                Exit Sub
            End If
        
            ClearFields
        
            '-- Setup the Vendor defaults structure
            muVendDflts.lVendKey = glGetValidLong(.GetColumnValue("VendKey"))
        
            '-- Refresh lookup restrict clauses that depend on VendKey
            RefreshVendDepends
    
            If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
                '-- Set the module-level key variable for the PO Number
                mlOrigPOKey = glGetValidLong(.GetColumnValue("POKey"))
            End If
        Else
            If Not bCheckReceiverLogStatus(lkuReturn, lRcvrKey, sTranID) Then
                mbValidKeyChange = False
                Exit Sub
            End If
        
            ClearFields
        
            '-- Setup the Vendor defaults structure
            muVendDflts.lVendKey = glGetValidLong(.GetColumnValue("VendKey"))
        
            '-- Refresh lookup restrict clauses that depend on VendKey
            RefreshVendDepends
    
            If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
                '-- Set the module-level key variable for the PO Number
                mlOrigRcvrKey = glGetValidLong(.GetColumnValue("RtrnRcvrKey"))
            End If
        End If
    End With
    
    If mlTranType = kTranTypePOTR Then
        Dim lTrnsfrOrderKey As Long
        Dim lShipWhseKey As Long
        Dim lTransitWhseKey As Long
        lTrnsfrOrderKey = moDmHeader.GetColumnValue("TrnsfrOrderKey")
        lShipWhseKey = glGetValidLong(moClass.moAppDB.Lookup("ShipWhseKey", "timTrnsfrOrder", "TrnsfrOrderKey = " & lTrnsfrOrderKey))
        lkuShipWhse = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & lShipWhseKey)
        txtShipWhseDesc = moClass.moAppDB.Lookup("Description", "timWarehouse", "WhseKey = " & lShipWhseKey)
        lTransitWhseKey = glGetValidLong(moClass.moAppDB.Lookup("TransitWhseKey", "timTrnsfrOrder", "TrnsfrOrderKey = " & lTrnsfrOrderKey))
        txtTransit = moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & lTransitWhseKey)
        mlOrigTransferKey = glGetValidLong(moDmHeader.GetColumnValue("TrnsfrOrderKey"))
        RefreshShipWhseDepends
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyFound", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessKeyFoundDetl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Processing for when existing lines are loaded.
'******************************************************************
    '-- The Line Entry object must be updated after a key change
    moLE.InitDataLoaded
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyFoundDetl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessKeyNotFound()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************
' Desc: Processing for when an receipt number that does not exist
'       in the database is entered.
'******************************************************************
    '-- Check that we're running in a mode where we can enter new receipts
    If (mlVRunMode = kEnterReceiptDTE) Or (mlVRunMode = kEnterReceiptAOF) Or _
       (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
        If Not bSetupNewReceipt() Then
            mbValidKeyChange = False
            Exit Sub
        End If

        '-- Set the Batch Key now
        moDmHeader.SetColumnValue "BatchKey", mlBatchkey
            
        SetRcptEntryFieldsEnabled True
                
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyNotFound", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessKeyNotAllowed()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Data Manager operations are not allowed due to security constraints
    If mlTranType <> kTranTypePORTrn Then
        lkuReceiptNo = ""
    Else
        lkuReturn = ""
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessKeyNotAllowed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetTBButtonStates()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    SetNextNumberState
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetTBButtonStates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetNextNumberState()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Set the enabled state of the Next Number toolbar button
    If (mlVRunMode = kEnterReceiptDTE) Or _
       (mlVRunMode = kErogDTE) Then
        If (miSecurityLevel > kSecLevelDisplayOnly) Then
            tbrMain.ButtonEnabled(kTbNextNumber) = True
        Else
            tbrMain.ButtonEnabled(kTbNextNumber) = False
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetNextNumberState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bCheckReceiverLogStatus(sRcptNo As String, lRcvrKey As Long, _
                                        sVouchTranID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sStatus As String
    Dim sTranStatus As String
    Dim iTranStatus As Integer
    Dim sInv As String
    
    bCheckReceiverLogStatus = False

    '-- Get the receiver Log Status
    iTranStatus = giGetValidInt(moAppDB.Lookup("TranStatus", "tpoReceiverLog", _
                      "CompanyID = " & gsQuoted(msCompanyID) & _
                      " AND TranNo = " & gsQuoted(sRcptNo) & _
                      " AND TranType = " & mlTranType))
    
    If (iTranStatus = 0) Then
        '-- Error accessing the Receipt Log record
        giSotaMsgBox Me, moSysSession, kmsgVoucherLogAccessError, _
            msRcvrNoLbl, msRcvrNoLbl
        
        '-- Clear the form
        ProcessCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If (iTranStatus = kvIncompleteReceiverLog) Then
        '-- Update the Log record for the current receipt to Pending
        If Not bUpdateReceiverLog(lRcvrKey, sVouchTranID, kvPendingReceiverLog) Then
            '-- Clear the form
            ProcessCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    sTranStatus = ""
    
    If (iTranStatus = kvPostedReceiverLog) Then
        If (mlVRunMode <> kViewEditReceiptDTE) And (mlVRunMode <> kViewEditReceiptDDN) Then
            sTranStatus = gsBuildString(kTranPosted, moAppDB, moSysSession)
        End If
    ElseIf (iTranStatus = kvPurgedReceiverLog) Then
        sTranStatus = gsBuildString(kTranPurged, moAppDB, moSysSession)
    ElseIf (iTranStatus = kvVoidReceiverLog) Then
        sTranStatus = gsBuildString(kTranVoid, moAppDB, moSysSession)
    End If
    
    If (Len(Trim$(sTranStatus)) > 0) Then
        '-- Log record found in an error-causing state
        sStatus = gsBuildString(kStatus, moAppDB, moSysSession)
                    
        sInv = gsBuildString(kReceiptNo, moAppDB, moSysSession)

        giSotaMsgBox Me, moSysSession, kmsgVoucherLogStatusInvalid, _
            msRcvrNoLbl, msRcvrNoLbl, sRcptNo, sInv, sRcptNo, _
            sStatus, sTranStatus
        
        '-- Clear the form
        ProcessCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    bCheckReceiverLogStatus = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCheckReceiverLogStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bSetupNewReceipt() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRcvrKey As Long
    Dim lRcptSeqNo As Long
    Dim sRcvrKey As String
    Dim sTranID As String
    Dim bValid As Boolean
    Dim sTranNo As String

    bSetupNewReceipt = False

    If mlTranType <> kTranTypePORTrn Then
        sTranNo = lkuReceiptNo
    Else
        sTranNo = lkuReturn
    End If
    '-- Check the tpoReceiverLog table to see if this Receipt No. has been used before
    lRcvrKey = lRcvrUsed(sTranNo)
    If lRcvrKey = 0 Then
        GoTo CancelNew
    End If

    If lRcvrKey = -1 Then
        '-- Receiver No. hasn't been used before; get the next Receiver Key
        lRcvrKey = glGetNextSurrogateKey(moAppDB, "tpoReceiverLog")
        If (lRcvrKey > 0) Then
            moDmHeader.SetColumnValue "RcvrKey", lRcvrKey 'balzer
            mlRcvrKey = lRcvrKey
        Else
    
            '-- An error occurred retrieving the next surrogate key
            sRcvrKey = gsBuildString(kReceiverKey, moAppDB, moSysSession)
            
            giSotaMsgBox Me, moSysSession, kmsgGetSurrogateKeyFailure, sRcvrKey
        
            GoTo CancelNew
        End If
    End If

    '-- Get the next Receipt Sequence No. for this batch
    lRcptSeqNo = lGetNextRcptSeqNo(moAppDB, mlBatchkey)
  
    If (lRcptSeqNo > 0) Then
        moDmHeader.SetColumnValue "SeqNo", lRcptSeqNo
    Else
        '-- An error occurred retrieving the next sequence number
        giSotaMsgBox Me, moSysSession, kmsgGetVoucherSeqNoErr, _
            msRcvrNoLbl, msRcvrNoLbl, mlBatchkey
        
        GoTo CancelNew
    End If

    '-- Default the Invoice & Receipt Dates
    calRcptDate = Format$(moSysSession.BusinessDate, gsGetLocalVBDateMask())
    bValid = bIsValidTranDate()

    '-- Insert a Receiver Log row in 'Incomplete' Status
    sTranID = ""
    
    '-- Refresh lookup restrict clauses that depend on VendKey
    RefreshVendDepends
    
    If Not bUpdateReceiverLog(lRcvrKey, sTranID, kvIncompleteReceiverLog) Then
        GoTo CancelNew
    End If

    bSetupNewReceipt = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

CancelNew:
    '-- Clear the form
    ProcessCancel

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupNewReceipt", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function lRcvrUsed(sRcptNo As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
'   Description:
'           Determine if receipt number has been used before (in tpoReceiverLog).
'   Parameters:
'           sRcptNo <in>:  The Receipt No. we are working with.
'   Returns:
'           -1 if not found, 0 if Receipt No. is found and can't be used, else returns the key.
'*******************************************************************************
    Dim sSQL As String
    Dim rs As Object
    Dim sTranNo As String
    Dim iTranStatus As Integer
    Dim sStatus As String
    Dim sTranStatus As String
    Dim sInv As String
    Dim lRcvrKey As Long

    lRcvrUsed = -1

    '-- Check tpoReceiverLog for Receiver No.
    sSQL = "SELECT RcvrKey, TranNo, TranStatus " & _
            " FROM tpoReceiverLog WITH (NOLOCK) " & _
            " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
            " AND TranNo = " & gsQuoted(sRcptNo) & _
            " AND TranType = " & mlTranType
    
    Debug.Print sSQL
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        '-- Get info about the receiver for display in error message
        lRcvrKey = giGetValidInt(rs.Field("RcvrKey"))
        sTranNo = gsGetValidStr(rs.Field("TranNo"))
        iTranStatus = giGetValidInt(rs.Field("TranStatus"))
        
        lRcvrUsed = lRcvrKey
    End If

    rs.Close: Set rs = Nothing

    '-- Display an appropriate error message
    If lRcvrUsed > -1 Then
        sStatus = gsBuildString(kStatus, moAppDB, moSysSession)

        Select Case iTranStatus
            Case kvIncompleteReceiverLog
                'sTranStatus = gsBuildString(kTranIncomplete, moAppDB, moSysSession)
                If giSotaMsgBox(Nothing, moClass.moSysSession, kmsgRGIncomplete) = kretYes Then
                    Exit Function
                End If
            Case kvPendingReceiverLog
                sTranStatus = gsBuildString(kTranPending, moAppDB, moSysSession)

            Case kvPostedReceiverLog
                sTranStatus = gsBuildString(kTranPosted, moAppDB, moSysSession)

            Case kvPurgedReceiverLog
                sTranStatus = gsBuildString(kTranPurged, moAppDB, moSysSession)

            Case Else
                sTranStatus = gsBuildString(kUnknown, moAppDB, moSysSession)
        
        End Select

        sInv = gsBuildString(kReceiptNo, moAppDB, moSysSession)
        
        giSotaMsgBox Me, moSysSession, kmsgVoucherNoUsedBefore, _
            msRcvrNoLbl, msRcvrNoLbl, sTranNo, sInv, _
            sTranNo, sStatus, sTranStatus, msRcvrNoLbl
        lRcvrUsed = 0
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRcvrUsed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bValidateIMDist(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    ' This routine is called during save for each row.  It checks to see if the distributions
    ' have been completely performed for each item.  It will try to auto distribute any items
    ' that it can.
    
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lInvtTranKey As Long
    Dim lTranIdentifier As Long
    Dim dQtyRcvd As Double
    Dim dRetTranQty As Double
    Dim dRetStockQty As Double
    Dim lTrnsfrOrderLineKey As Long
    Dim lRcvrKey As Long
    Dim lDfltWhseBinKey As Long
    
    Dim dPendingDistStockQty As Double
    Dim dStockQtyToBeRcvd As Double
    Dim dConvFactor As Double
    Dim bDistributionComplete As Boolean
    Dim bUsePreferredBin As Boolean
    Dim bCanBeAutoDistributed As Boolean
    Dim bShowUI As Boolean
    Dim iRet As Integer

    bCanBeAutoDistributed = False

    lTranIdentifier = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
    dQtyRcvd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd))
    dStockQtyToBeRcvd = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd))
    lUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey))
    lItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
    mlItemKey = lItemKey
    
    'If this is a non-inventory item.  Distributions are not required.
    If Len(gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColWhseID))) = 0 Then
        'This is a non-inventory item.  Distributions are not required.
        bValidateIMDist = True
        Exit Function
    End If
    
    'If the row is set to zero and has had no activity, do nothing.
    If lInvtTranKey = 0 And lTranIdentifier = 0 And dQtyRcvd = 0 Then
        bValidateIMDist = True
        Exit Function
    End If
    
    'Reserve a new InvtTranKey if needed.
    If lInvtTranKey = 0 Then
        lInvtTranKey = lGetInvtTranKey(lRow)
        
        If lInvtTranKey = 0 Then
            gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, ""
            bValidateIMDist = False
            Exit Function
        Else
            gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(lInvtTranKey)
            moDmDetl.SetColumnValue lRow, "InvtTranKey", lInvtTranKey
        End If
    End If
    
    'Try to auto distribute.
    bAutoDistFromBinLku lRow
    
    'Get the keys again in case bAutoDistFromBinLku set their values.
    lTranIdentifier = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
    
    'Make sure that the TranKey is set in the Distribution work table for the lTranIdentifier
    If lTranIdentifier <> 0 And lInvtTranKey <> 0 Then
        moAppDB.ExecuteSQL "UPDATE #timInvtDistWrk SET TranKey = " & lInvtTranKey & " WHERE COALESCE(TranKey, 0) = 0 AND TranIdentifier = " & lTranIdentifier
    End If
    
    bDistributionComplete = moIMDistribution.IsDistComplete(dQtyRcvd, lUOMKey, lInvtTranKey, lTranIdentifier)
    
    Set moItem = moIMSClass.Items(lItemKey)
    moItem.UOMConversion lUOMKey, moItem.StockUnitMeasKey, dStockQtyToBeRcvd, dConvFactor
    
    'PO Vendor Return Distribution Save.
    If mlTranType = kTranTypePORTrn And bDistributionComplete = True Then
        'PO Return still uses old distribution save routine.
        bValidateIMDist = bSaveIMDistPORtrn(lRow)
        Exit Function
    End If

    'Determine if the distribution can be automatically performed.
    If mbTrackByBin = False Then
        lDfltWhseBinKey = glGetValidLong(moClass.moAppDB.Lookup("WhseBinKey", "timWhseBin wb WITH (NOLOCK)", "WhseKey = " & glGetValidLong(mlWhseKey) & " AND DfltBin = 1"))
        If moItem.TrackMethod = IMS_TRACK_METHD_NONE Then
            bShowUI = False
        End If
    Else
        lDfltWhseBinKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColAutoDistBinKey))
    End If
    
    If bDistributionComplete = False And mbTrackByBin = True Then
        If mlTranType = kTranTypePORG Then
            bShowUI = True
        Else
            'If dealing with a Transfer, we might be able to auto-distribute the lines.
            If mlTranType = kTranTypePOTR Then
                'If a preferred bin or a default bin exist or warehouse does not track by bin,
                'The item can be automatically distributed.
                If ((bUsePreferredBin = True And bCanDistAgainstPrefBin(lItemKey, mlWhseKey, 1) = True) Or (glGetValidLong(lDfltWhseBinKey) <> 0)) Or mbTrackByBin = False Then
                    bCanBeAutoDistributed = True
                    bShowUI = False
                Else
                    bCanBeAutoDistributed = False
                    bShowUI = True
                End If
            Else
                '-- Display error message
                moDmHeader.CancelAction
                giSotaMsgBox Me, moClass.moSysSession, kmsgTranQtyDistQtyDiffer
                mlInvalidRow = lRow
                ResetToInvalidRow lRow
                bValidateIMDist = False
                tabSubDetl.Tab = kTabPutAway
                Exit Function
            End If
        End If
    End If

    If bDistributionComplete = False Or bCanBeAutoDistributed = True Then
    
        lTrnsfrOrderLineKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColTrnsfrLineKey))
    
        'Perform the auto distribution.
        Select Case mlTranType
            Case kTranTypePORG, kEntTypePOReturn
                
                'Have distribution use the WhseBinKey we are passing in with no UI
                lTranIdentifier = moIMDistribution.EditDistribution(mlTranType, lItemKey, _
                    lUOMKey, gdGetValidDbl(dQtyRcvd), dRetTranQty, lTranIdentifier, _
                    lInvtTranKey, , , , , bShowUI, , bUsePreferredBin, lDfltWhseBinKey)
            
                'Check for distribution error.
                If moIMDistribution.oError.Number <> 0 Or lTranIdentifier = 0 Then
                    '-- Display error message
                    moDmHeader.CancelAction
                    giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                    moIMDistribution.oError.Clear
                    mlInvalidRow = lRow
                    bValidateIMDist = False
                    Exit Function
                End If
                
                'If distributions are incomplete, raise message until it is or
                'until user decides to cancel save.
                Do While gdGetValidDbl(dQtyRcvd) <> gdGetValidDbl(dRetTranQty)
                    bShowUI = True
                    iRet = giSotaMsgBox(Me, moSysSession, kmsgDistIncompleteContinue, "")
                    
                    If iRet = vbYes Then
                        'Have distribution use the WhseBinKey we are passing in with SHOW UI
                        SetHourglass True
                        lTranIdentifier = moIMDistribution.EditDistribution(mlTranType, lItemKey, _
                            lUOMKey, gdGetValidDbl(dQtyRcvd), dRetTranQty, lTranIdentifier, _
                            lInvtTranKey, , , , , bShowUI, , bUsePreferredBin, lDfltWhseBinKey)
                        SetHourglass False
                        'Check for distribution error.
                        If moIMDistribution.oError.Number <> 0 Or lTranIdentifier = 0 Then
                            '-- Display error message
                            moDmHeader.CancelAction
                            giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                            moIMDistribution.oError.Clear
                            mlInvalidRow = lRow
                            bValidateIMDist = False
                            Exit Function
                        End If
                    Else
                        moDmHeader.CancelAction
                        mlInvalidRow = lRow
                        ResetToInvalidRow lRow
                        bValidateIMDist = False
                        tabSubDetl.Tab = kTabPutAway
                        Exit Function
                    End If
                Loop
                
            Case kTranTypePOTR
                lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    
                lTranIdentifier = moIMDistribution.ReceiveTransferDist(mlTranType, _
                    mlWhseKey, lTrnsfrOrderLineKey, lRcvrKey, gdGetValidDbl(dQtyRcvd), _
                    lUOMKey, dRetTranQty, lTranIdentifier, lInvtTranKey, bUsePreferredBin, lDfltWhseBinKey, bShowUI)
    
                'Check for distribution error.
                If moIMDistribution.oError.Number <> 0 Or lTranIdentifier = 0 Then
                    '-- Display error message
                    moDmHeader.CancelAction
                    giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
                    moIMDistribution.oError.Clear
                    mlInvalidRow = lRow
                    bValidateIMDist = False
                    Exit Function
                End If
        End Select

        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranID, CStr(lTranIdentifier)
        'gGridUpdateCell grdRcptDetl, lRow, kColSysGenTranID, "1"

    End If
    
    If mlTranType = kTranTypePORTrn Then
        'PO Return still uses old distribution save routine.
        bValidateIMDist = bSaveIMDistPORtrn(lRow)
    Else
        bValidateIMDist = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidateIMDist", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function
Public Function bDeleteReceiverLog(lRcvrKey As Long, _
                                  Optional iRcptLogStatus As Integer = 0) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rs As Object
    Dim iTranStatus As Integer
    Dim lRetCode As Long

    bDeleteReceiverLog = True

    '-- Verify that we have a receiver Key
    If (lRcvrKey > 0) Then
        '-- See if a log record exists for this receipt
        sSQL = "SELECT TranStatus " & _
                " FROM tpoReceiverLog WITH (NOLOCK) " & _
                " WHERE RcvrKey = " & lRcvrKey
        
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
        If Not rs.IsEOF Then
            '-- Check to see if a specific Status to delete was passed-in
            If (iRcptLogStatus <> 0) Then
                iTranStatus = giGetValidInt(rs.Field("TranStatus"))
                If (iTranStatus <> iRcptLogStatus) Then
                    '-- The Receiver Log row does not have the correct Status
                    GoTo FunctionExit
                End If
            End If
                    
            '-- Execute a stored procedure to delete the
            '-- Receiver Log row in 'Incomplete' Status
            With moAppDB
                .SetInParam lRcvrKey
                .SetInParam kSPAutoCommit
                .SetOutParam lRetCode
                .ExecuteSP "sptpoReceiverLogDelete"
                lRetCode = .GetOutParam(3)
                .ReleaseParams
            End With
            
            If (lRetCode <> kdasSuccess) Then
                bDeleteReceiverLog = False
                GoTo FunctionExit
            End If
        End If
    End If

FunctionExit:
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteReceiverLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bUpdateReceiverLog(lRcvrKey As Long, iRcptTranID As String, _
                                   Optional iSetToStatus As Integer = 0) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Desc: Update the Receiver Log table with info about the new receipt.
'*********************************************************************
    Dim sSQL As String
    Dim rs As Object
    Dim sRcptDate As String
    Dim iTranStatus As Integer
    Dim iMode As Integer
    Dim iRcptStatus As Integer
    Dim bRetCode As Boolean
    Dim sTranNo As String

    bUpdateReceiverLog = False
    
    '-- Verify that we have a Receiver Key
    If (lRcvrKey > 0) Then
        '-- If there is an Receipt Date, convert it to SQL format
        If (Len(Trim$(calRcptDate)) > 0) Then
            sRcptDate = calRcptDate.SQLDate
        Else
            sRcptDate = ""
        End If
            
        iTranStatus = giGetValidInt(moAppDB.Lookup("TranStatus", "tpoReceiverLog", _
            "RcvrKey = " & lRcvrKey))
                
        If (iTranStatus > 0) Then
            '-- An existing record was found
            iMode = kiUpdateMode
        Else
            iMode = kiInsertMode
        End If
    
        If mlTranType <> kTranTypePORTrn Then
            sTranNo = lkuReceiptNo
        Else
            sTranNo = lkuReturn
        End If
        
        '-- Check to make sure we have all the necessary components
        If Len(Trim$(sTranNo)) > 0 Then
            '-- Set the Receiver Log status appropriately
            If (iSetToStatus <> 0) Then
                iRcptStatus = iSetToStatus
            ElseIf (iMode = kiInsertMode) Then
                iRcptStatus = kvIncompleteReceiverLog
            ElseIf (iMode = kiUpdateMode) Then
                iRcptStatus = iTranStatus
            End If
    
            '-- Verify that we have rights to update the Receiver Log
            If (miSecurityLevel > kSecLevelDisplayOnly) Then
                bRetCode = bExecReceiverLogSP(iMode, lRcvrKey, _
                               sRcptDate, iRcptTranID, sTranNo, iRcptStatus, _
                               mlTranType)
            Else
                bRetCode = True
            End If
    
            If bRetCode Then
                '-- Receiver Log insert/update was successful
                bUpdateReceiverLog = True
            End If
        End If
    End If
    
    If Not bUpdateReceiverLog Then
        '-- Display error message
        moDmHeader.CancelAction
        giSotaMsgBox Me, moSysSession, kmsgVoucherLogError, msRcvrNoLbl
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUpdateReceiverLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bExecReceiverLogSP(iMode As Integer, lRcvrKey As Long, _
                                   sRcptDate As String, sTranID As String, sRcptNo As String, _
                                   iVouchStatus As Integer, lTranType As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
' Desc: Execute a stored procedure to insert/update a Receiver Log row.
'**********************************************************************
    Dim lRetCode As Long

    bExecReceiverLogSP = False

    With moAppDB
        .SetInParam lRcvrKey
        .SetInParam msCompanyID
        .SetInParam 0
        If (Len(calRcptDate) = 0) Then
            .SetInParamNull SQL_DATE
        Else
            .SetInParam calRcptDate.SQLDate
        End If
        .SetInParam sRcptNo
        .SetInParam iVouchStatus
        .SetInParam lTranType
        .SetInParam kSPAutoCommit
        .SetOutParam lRetCode
        If (iMode = kiInsertMode) Then
            .ExecuteSP "sptpoReceiverLogInsert"
        Else
            .ExecuteSP "sptpoReceiverLogUpdate"
        End If
        lRetCode = glGetValidLong(.GetOutParam(9))
        .ReleaseParams
    End With

    If (lRetCode = kdasSuccess) Then
        bExecReceiverLogSP = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExecReceiverLogSP", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SOTAValidationMgr1_Validate(oControl As Object, iReturn As SOTAVM.SOTA_VALID_RETURN_TYPES, sMessage As String, ByVal iLevel As SOTAVM.SOTA_VALIDATION_LEVELS)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbFromCode = True
    
    iReturn = SOTA_INVALID
    Debug.Print "VM - Validating Data: " & oControl.Name
    
    Select Case oControl.Name
        '-- Main fields
        Case lkuVendID.Name
            If Not bIsValidVendID(sMessage) Then Exit Sub
            
        Case lkuPONo.Name
            If Not bIsValidPONo(Me, moDmDetl, moLE, msHomeCurrID, , _
                 sMessage, , miClosePOonFirstRcpt) Then Exit Sub
            
        Case lkuReceiver.Name
        
            If Not bIsValidReceiver(Me, moDmDetl, msCompanyID, moLE, , _
                 sMessage) Then Exit Sub
            
        Case calRcptDate.Name
            If Not bIsValidTranDate(sMessage) Then Exit Sub
        
        Case ddnPurchCompany.Name
            If Not bIsValidPurchCompany(sMessage) Then Exit Sub

        Case nbrQtyRcvd.Name
            If Not bIsValidQtyRcvd(sMessage) Then Exit Sub
            
        Case ddnUOM.Name
            If Not bIsValidUOM(sMessage) Then Exit Sub
            
        Case lkuShipWhse.Name
            If Not bIsValidShipWhse(sMessage) Then Exit Sub
            
        Case lkuTransfer.Name
            If Not bIsValidTransfer(Me, moDmDetl, moLE, msHomeCurrID, , _
                 sMessage) Then Exit Sub
                 
        Case lkuReceiveToBin.Name
            If Not bIsValidReceiveToBin(Me) Then Exit Sub
            
    End Select

    iReturn = SOTA_VALID
    Debug.Print "SOTAValidationMgr1_Validate: " & oControl.Name & " is " & IIf(iReturn = SOTA_VALID, "valid.", "not valid.")
    mbFromCode = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SOTAValidationMgr1_Validate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bDenyControlFocus() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bDenyControlFocus = False
    
    If mlTranType = kTranTypePORG Then
        If (Len(Trim$(lkuReceiptNo)) = 0) Or (Len(Trim$(lkuVendID)) = 0) Or (Len(Trim$(lkuPONo)) = 0) Then
            '-- Allow free movement between VendID and PONo
            If (Screen.ActiveControl.Name <> lkuVendID.Name) _
                And (Screen.ActiveControl.Name <> ddnPurchCompany.Name) _
                And (Screen.ActiveControl.Name <> calRcptDate.Name) Then
                SetCtrlFocus txtPONum
            End If
            bDenyControlFocus = True
        End If
    ElseIf mlTranType = kTranTypePOTR Then
        If (Len(Trim$(lkuReceiptNo)) = 0) Or (Len(Trim$(lkuShipWhse)) = 0) Or (Len(Trim$(lkuTransfer)) = 0) Then
            '-- Allow free movement between Ship Whse and Transfer
            If (Screen.ActiveControl.Name <> lkuShipWhse.Name) _
                And (Screen.ActiveControl.Name <> txtTransit.Name) _
                And (Screen.ActiveControl.Name <> calRcptDate.Name) Then
                SetCtrlFocus txtPONum
            End If
            bDenyControlFocus = True
        End If
    Else
        If (Len(Trim$(lkuReturn)) = 0) Or (Len(Trim$(lkuVendID)) = 0) Or (Len(Trim$(lkuReceiver)) = 0) Then
            '-- Allow free movement between VendID and PONo
            If (Screen.ActiveControl.Name <> lkuVendID.Name) _
                And (Screen.ActiveControl.Name <> calRcptDate.Name) Then
                SetCtrlFocus lkuReceiver
            End If
            bDenyControlFocus = True
        End If
    
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDenyControlFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bIsValidVendID(Optional sMessage As String = "", _
                               Optional bCompanyChanged As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sVendID As String
    Dim sSQL As String
    Dim rs As Object
    Dim iStatus As Integer
    Dim lNewVendKey As Long
    Dim lKey As Long
    Dim bVendChanged As Boolean
    Dim bValid As Boolean
    Dim iReply As Integer

    bIsValidVendID = False
    
    sVendID = Trim$(lkuVendID)
    
    If (Len(sVendID) > 0) Then
    
        sSQL = "SELECT * FROM tapVendor" & _
                " WHERE CompanyID = " & gsQuoted(msPurchCompanyID) & _
                " AND VendID = " & gsQuoted(sVendID)
    
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
        If Not rs.IsEOF Then
            iStatus = giGetValidInt(rs.Field("Status"))
        
            '-- Only vendors in Active or Temporary Status are valid
            If (iStatus <> CInt(kvVendorActive)) And _
            (iStatus <> CInt(kvVendorTemporary)) Then
                '-- Build an error message to return to VM
                sMessage = gsBuildMessage(kmsgVendorActiveOrTemp, mlLanguage, moAppDB)
                giSotaMsgBox Me, moClass.moSysSession, kmsgVendorActiveOrTemp

                GoTo BadVendID
            End If
            lNewVendKey = glGetValidLong(rs.Field("VendKey"))
        Else
            GoTo BadVendID
        End If
    Else
        lNewVendKey = 0
    End If
    '-- Determine if the vendor has changed, and if so, does the
    bVendChanged = False
        
    '-- Compare the new vendor with the old one
    If (muVendDflts.lVendKey <> lNewVendKey) And (lNewVendKey <> 0) And (muVendDflts.lVendKey <> 0) Then
        bVendChanged = True
    End If

    '-- Set the current VendKey tracking variable
    muVendDflts.lVendKey = lNewVendKey
    lkuVendID.SetTextAndKeyValue sVendID, lNewVendKey
    moDmHeader.SetColumnValue "VendKey", lNewVendKey
    lkuVendID.Tag = lkuVendID.Text
'    lkuVendID.KeyValue = lNewVendKey

    '-- Refresh lookup restrict clauses that depend on VendKey
    RefreshVendDepends
    

    '-- Handle PO defaults here
    If (mlVRunMode = kErogDTE) Or (mlVRunMode = kErogDDN) Then
        If Not mbValidatingPO And Not mbValidatingRcvr Then
            If mlTranType = kTranTypePORG Then
                '-- Validate the PO Number now
                If (Len(Trim$(lkuPONo)) > 0) Or bCompanyChanged Then
                    bValid = bIsValidPONo(Me, moDmDetl, moLE, msHomeCurrID, True, kDisplayMsg, bCompanyChanged, miClosePOonFirstRcpt)
                End If
            ElseIf mlTranType = kTranTypePOTR Then
                '-- Validate the Transfer now
                If (Len(Trim$(lkuTransfer)) > 0) Or bCompanyChanged Then
                    bValid = bIsValidTransfer(Me, moDmDetl, moLE, msHomeCurrID, True, kDisplayMsg, bCompanyChanged)
                End If
            Else
                '-- Validate the Receiver Number now
                ' MVB change the validation.
                If (Len(Trim$(lkuReceiver)) > 0) Then
                    bValid = bIsValidReceiver(Me, moDmDetl, msCompanyID, moLE, True, kDisplayMsg, False)
                End If
            End If
        End If
    End If
    
    '-- Retrieve & display all linked fields now
    moDmHeader.DoLinks
    
    ' This was added because when creating a Return for an intercompany PO, the
    ' DoLinks statement was setting the KeyValue for the lkuVendID to Null.
    lkuVendID.SetTextAndKeyValue sVendID, lNewVendKey

    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If

    '-- Everything just loaded is valid
    SOTAValidationMgr1.Reset
    
    bIsValidVendID = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

BadVendID:
    '-- Close the recordset
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
    lkuVendID.Tag = ""
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidVendID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bIsValidShipWhse(Optional sMessage As String = "", _
                               Optional bCompanyChanged As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sShipWhseID As String
    Dim sSQL As String
    Dim rs As Object
    Dim iStatus As Integer
    Dim lNewWhseKey As Long
    Dim lKey As Long
    Dim bWhseChanged As Boolean
    Dim bValid As Boolean
    Dim iReply As Integer
    Dim sWhseDesc As String


    'Skip Validation if we are currently validating the Transfer Order
    If mbValidatingTrnsfr Then
        bIsValidShipWhse = True
        Exit Function
    End If
    
    
    bIsValidShipWhse = False
    
    sShipWhseID = Trim$(lkuShipWhse)
    
    If (Len(sShipWhseID) > 0) Then
    
        sSQL = "SELECT * FROM timWarehouse" & _
                " WHERE CompanyID = " & gsQuoted(msPurchCompanyID) & _
                " AND WhseID = " & gsQuoted(sShipWhseID)
    
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
        If Not rs.IsEOF Then
            lNewWhseKey = glGetValidLong(rs.Field("WhseKey"))
            sWhseDesc = gsGetValidStr(rs.Field("Description"))
        Else
            GoTo BadWhseID
        End If
    Else
        lNewWhseKey = 0
    End If
    '-- Determine if the vendor has changed, and if so, does the
    bWhseChanged = False
        
    '-- Compare the new vendor with the old one
    If (CStr(lkuShipWhse.Tag) <> sShipWhseID) Then
        bWhseChanged = True
    End If

    If bWhseChanged Then
        '-- Set the current VendKey tracking variable
        lkuShipWhse.SetTextAndKeyValue sShipWhseID, lNewWhseKey
        lkuShipWhse.Tag = lkuShipWhse.Text
        txtShipWhseDesc = sWhseDesc
    
        '-- Refresh lookup restrict clauses that depend on ShipWhseKey
        RefreshShipWhseDepends
        
        '-- Retrieve & display all linked fields now
        moDmHeader.DoLinks
        
        ' This was added because when creating a Return for an intercompany PO, the
        ' DoLinks statement was setting the KeyValue for the lkuVendID to Null.
        lkuShipWhse.SetTextAndKeyValue sShipWhseID, lNewWhseKey
    End If

    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If

    '-- Everything just loaded is valid
    SOTAValidationMgr1.Reset
    
    bIsValidShipWhse = True
    
    Exit Function

BadWhseID:
    '-- Close the recordset
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
    Exit Function
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "bIsValidShipWhse", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function bIsValidPurchCompany(Optional sMessage As String = "") As Boolean

    If Len(Trim(lkuVendID)) > 0 Then
        If Len(Trim(lkuPONo)) > 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgExistingPOAndVendNotValid, msPurchCompanyID
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgExistingVendNotValid, msPurchCompanyID
        End If
        lkuVendID.ClearData
        txtVendName.Text = ""
        lkuPONo.ClearData
        bIsValidVendID "", True
    End If
    
'    lkuPONo.ClearData
'    cmdLines.Enabled = False
    
    bIsValidPurchCompany = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidPurchCompany", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidTranDate(Optional sMessage As String = "") As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim dExchangeRate As Double
    Dim bValid As Boolean

    bIsValidTranDate = False
    
    If msHomeCurrID <> muPODflts.sCurrID Then
    
        dExchangeRate = gdGetExchangeRate(moAppDB, msHomeCurrID, msCompanyID, _
                                muPODflts.sCurrID, msHomeCurrID, calRcptDate.Text, _
                                kExchangeRateBuy, muPODflts.lCurrExchSchdKey)
        
        If dExchangeRate > 0 And muRcvrDflts.dCurrExchRate <> dExchangeRate Then
            Me.SOTAValidationMgr1.StopValidation
            giSotaMsgBox Me, moSysSession, kmsgARNoRecalcOnExchRate
            Me.SOTAValidationMgr1.StartValidation
        End If
    End If
    

    
    bIsValidTranDate = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidTranDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SetDetailCtrlState(bFlag As Boolean, Optional bNewItem As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iMatchStatus As Integer
    Dim lPOLineKey As Long
    Dim bDetlPop As Boolean
    
    '-- Set fields to proper state
    If (mlVRunMode <> kViewEditReceiptDTE) And (mlVRunMode <> kViewEditReceiptDDN) Then
        
        ToggleCtrlState nbrQtyRcvd, bFlag
        If mlTranType <> kTranTypePORTrn Then
            ToggleCtrlState chkMatch, bFlag
            ToggleCtrlState chkCloseDistr, bFlag
        Else
            ToggleCtrlState ddnReason, bFlag
            ToggleCtrlState optRetCredit, bFlag
            ToggleCtrlState optRetReplace, bFlag
        End If
        ToggleCtrlState txtLnComment, bFlag
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetDetailCtrlState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ClearDetlFields(Optional bClearAll As Boolean = False, _
                            Optional bDetlPop As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Clear out detail field values based on Item
    mbManualClick = True
    nbrQtyRcvd = 0
    mbManualClick = False
    ddnUOM.ListIndex(False) = kItemNotSelected
    ddnReason.ListIndex(False) = kItemNotSelected
    txtLnComment = ""
    mbManualClick = True
    chkMatch = 1
    chkCloseDistr = 0
    nbrQtyOrd.Value = 0
    txtUOM.Text = ""
    nbrTotRcvd.Value = 0
    nbrQtyReturn.Value = 0
    nbrQtyShip.Value = 0
    nbrQtyOpen.Value = 0
    txtItemOrdID.Text = ""
    txtPOLine.Text = ""
    txtWhse.Text = ""
    txtDept.Text = ""
    mbManualClick = False
    txtItemDescription.Text = ""
    optRetCredit = True
    optRetReplace = False
    txtRtrnUOM = ""
    txtUOMRcpt = ""
    lkuPONo.Tag = ""
    lkuReceiver.Tag = ""
    '-- Set the detail controls as valid so the old values are correct
    SOTAValidationMgr1.Reset
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearDetlFields", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bIsValidQtyRcvd(Optional sMessage As String = "") As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lRow As Long
    
    bIsValidQtyRcvd = bQtyValid(sMessage)
       
    If bIsValidQtyRcvd Then
        lRow = grdRcptDetl.ActiveRow
        If bIsValidAutoDistBin(lRow) Then
            If mbValidAutoDistBin Then
                gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "1"
            Else
                gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "0"
            End If
        End If
    End If
       
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidQtyRcvd", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidUOM(Optional sMessage As String = "") As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bIsValidUOM = False
    
    If bQtyValid(sMessage) Then
        bIsValidUOM = True
        mdRcvrWght = 0
        mdRcvrVol = 0
        mlUOMKey = ddnUOM.ItemData
        mdItemVol = moItem.Volume(mlUOMKey)
        mdItemWght = moItem.Weight(mlUOMKey)
        moLE.GridEditChange ddnUOM
    End If
         
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidUOM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tabReceipt_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
    Static bInTabChange As Boolean
    
    If bInTabChange Then Exit Sub
    
    bInTabChange = True
    
    '-- Do nothing if the tab is the same as last time
    If (tabReceipt.Tab = PreviousTab) Then
        bInTabChange = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
        
    bValid = True
    
    If (PreviousTab = kiDetailTab) And (tabReceipt.Tab <> PreviousTab) Then
        '-- Undo this line if the user hasn't done "anything" to it
        UndoNewBlankRow
    
        bValid = moLE.TabChange(tabReceipt.Tab, PreviousTab)
    End If
    
      
    bInTabChange = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabReceipt_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub ShowBatchIDInCaption(sBatchID As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sTempCaption As String
    Dim sBatch As String
    
    '-- Put the Batch ID in the form caption
    If (Len(Trim$(sBatchID)) > 0) Then
        '-- Get string for "Batch"
        sBatch = gsBuildString(kBatch, moAppDB, moSysSession)
        
        sTempCaption = gsStripChar(msFormCaption, ".")
        Me.Caption = gsStripChar(sTempCaption & " [" & sBatch & ": " & sBatchID & "]", kAmpersand)
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowBatchIDInCaption", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuReceiptNo_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- To work around a problem with the lookup control button keeping
    '-- focus if the next control in the tab order is disabled
    If Not lkuReceiptNo.EnabledText Then
        'lkuReceiptNo_LostFocus needs to fire to ensure key change -- so set focus
        'to lkuReceiptNo first just in case.
        SetCtrlFocus lkuReceiptNo
        SetCtrlFocus lkuVendID
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_BeforeValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReturn_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- To work around a problem with the lookup control button keeping
    '-- focus if the next control in the tab order is disabled
    If Not lkuReturn.EnabledText Then
        SetCtrlFocus lkuVendID
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReturn_BeforeValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuReceiptNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Not bConfirmUnload(0) Then
        bCancel = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReturn_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuReturn, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    If Not bConfirmUnload(0) Then
        bCancel = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReturn_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuReceiptNo_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuReceiptNo, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    mbFromNav = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_BeforeLookupReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuReturn_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuReturn, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    mbFromNav = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReturn_BeforeLookupReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuReceiptNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++
    If mbFromNav Then
        '-- Fire the KeyChange event now
        mbAllowKeyChange = True
        SOTAValidationMgr1_KeyChange
    End If

    mbFromNav = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiptNo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuReturn_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuReturn, True
    #End If
'+++ End Customizer Code Push +++
    If mbFromNav Then
        '-- Fire the KeyChange event now
        mbAllowKeyChange = True
        SOTAValidationMgr1_KeyChange
    End If

    mbFromNav = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReturn_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonMenuClick(Button As String, ButtonMenu As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarMenuClick Button, ButtonMenu
'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonMenuClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLnComment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    '-- Turn on the detail command buttons
    moLE.GridEditChange txtLnComment
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtLnComment_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sConvertMask(sSOTAMask As String, sMaskDelim As String, _
                              bLeftJust As Boolean) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: This routine converts a valid Sage MAS 500 mask to a VB mask that
'       would then be used in a Format$() command.
' Parms: sSOTAMask <in>:  The Sage MAS 500 mask we want to convert.
'        sMaskDelim <in>: The delimiter used for segments in
'                          the mask.
'        bLeftJust <in>:  Indicates whether (true) or not
'                          (false) we want VB to force left
'                          justification in the Format$() command.
' Returns: A string containing the VB mask to be used by Format$,
'          or an empty string if not possible.
'***********************************************************************
    Dim i As Integer
    Dim sVBMask As String

    sConvertMask = ""

    '-- Loop through the passed-in Sage MAS 500 mask looking for the passed-in mask delimiter
    For i = 1 To Len(sSOTAMask)
        If (Mid$(sSOTAMask, i, 1) <> sMaskDelim) Then
            '-- Replace the character with an '@' sign
            sVBMask = sVBMask & "@"
        Else
            '-- Use the Sage MAS 500 mask delimiter character in the VB mask
            sVBMask = sVBMask & Mid$(sSOTAMask, i, 1)
        End If
    Next i
  
    '-- See if forced left justification is desired
    If bLeftJust And (Len(sVBMask) > 0) Then
        sVBMask = "!" & sVBMask
    End If
    
    sConvertMask = sVBMask
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sConvertMask", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function



Public Sub cmdLines_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdLines, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim iHookType As Integer
    Static bProcessing As Boolean
    
    '-- Need to turn off the WinHook for the main form while the Lines
    '-- form is loaded; WinHooks step on each other otherwise
    ' **PRESTO ** iHookType = WinHook1.KeyboardHook
    ' **PRESTO ** WinHook1.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHook1.HookEnabled = False
    If bProcessing Then Exit Sub
    
    bProcessing = True
    
    mbAllRcvrLinesWereOnceDeleted = bRcvrLinesAllHaveZeroQty()
    
    With frmSelectPOLines
        '-- Set initialization properties
        Set .oClass = moClass
        Set .oMainForm = Me
        .lRcvrKey = glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
        .sCompanyID = msCompanyID
        .sPurchCompanyID = msPurchCompanyID
        .lWhseKey = mlWhseKey
        
        .iUnitCostDecPlaces = moOptions.CI("UnitCostDecPlaces")
        .iQtyDecPlaces = miQtyDecPlaces
        .bDefaultQty = mbDefaultQty
        .lTranType = mlTranType
        If mbAutoSelectLines And mbInternalLinesClick Then
            .bShowUI = False
            If mbFilterByDate Then
                .sRcptDate = calRcptDate.SQLDate
                .iDaysEarly = miRcptDaysEarly
            Else
                .sRcptDate = ""
                .iDaysEarly = -1 ' Don't filter on date
            End If
        Else
            .bShowUI = True
            .sRcptDate = ""
            .iDaysEarly = -1 ' Don't filter on date
        
        End If
    
        Load frmSelectPOLines
        
        If .bShowUI Then .Show vbModal
    End With
    
    '-- Load receiver detail grid here
    If frmSelectPOLines.bProcessExit Then
        LoadPOLinesInDetlGrid
    End If
    
    Unload frmSelectPOLines
    
    mbInternalLinesClick = False
    bProcessing = False

    
    '-- Need to turn the WinHook for the main form back on
    ' **PRESTO ** WinHook1.KeyboardHook = iHookType
    ' **PRESTO ** WinHook1.HookEnabled = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdLines_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DeletePOLinesFromGrid()
'**********************************************************
' Desc: This routine will delete all lines from the receiver
'       detail grid associated with the old PO number.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim lOrigMaxRows As Long
    Dim lRowsChecked As Long
    Dim sGridPO As String
        
    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdRcptDetl, False
        
    lOrigMaxRows = grdRcptDetl.DataRowCnt
    lRow = lOrigMaxRows
    lRowsChecked = 0
        
    Do While (lRowsChecked <= lOrigMaxRows) And (lRow > 0)
        mbManualClick = True
               
        '-- Set the row to be deleted to the current row
        'moLE.Grid_Click 1, lRow
            
        DeleteIMRow (lRow)
        '-- Delete the row
        GridDeleteRow Me, moDmDetl, moLE, lRow
       
        mbManualClick = False
        lRow = lRow - 1
        lRowsChecked = lRowsChecked + 1
    
        'Since a line has been deleted, set flag to allow the auto-adjustment of the landed cost transactions.
        mbAdjToLandedCostAmtsRequired = True
    Loop
        
    '-- Turn redraw of the grid back on
    SetGridRedraw grdRcptDetl, True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteOldPOLinesFromGrid", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub LoadPOLinesInDetlGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************************
'     Desc: This routine will load all selected lines from the
'           Select PO Lines dialog into the receiver detail grid.
'***************************************************************
    Dim lPORow As Long
    Dim lVDRow As Long
    Dim lNewRow As Long
    Dim lLDRow As Long
    Dim lOldRow As Long
    Dim lRowsInserted As Long
    Dim l As Long
    Dim sPONum As String
    Dim sPOLineNum As String
    Dim sPOKeyStr As String
    Dim oPOGrid As Object
    Dim dNewQty As Double
    Dim dNewUC As Double
    Dim dNewUCE As Double
    Dim dConvFactor As Double
    Dim lNewUOMKey As Long
    Dim lAcctRefKey As Long
    Dim iAllowRtrns As Integer
    Dim sCloseSrcLine As String
    Dim sTempCloseSrcLine As String
    Dim sSQL As String
    Dim rs As Object
    Dim iAllowBO As Integer
    Dim sTranCmnt As String
    Dim dTotalQtyOrdered As Double
    Dim dTotalQtyRcvd As Double
    Dim lItemKey As Long
    
    ' First remove existing lines, if any
    If grdRcptDetl.DataRowCnt > 0 Then
        DeletePOLinesFromGrid
    End If
    
    If mbCloseSrcLine Then
        sCloseSrcLine = "1"
    Else
        sCloseSrcLine = "0"
    End If
    
    Set oPOGrid = frmSelectPOLines.grdSelectPOLines

    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdRcptDetl, False

    lRowsInserted = 0

    '-- Save off the current grid row
    lOldRow = grdRcptDetl.ActiveRow
    
    '-- Click on the last line in the grid; fixes a grids-as-parents bug
    '-- where line distribution records weren't being saved
    moLE.Grid_Click kColPOLineNo, grdRcptDetl.MaxRows
    
    '-- Reset the grid row to it's old state
    moLE.Grid_Click kColPOLineNo, lOldRow

    mbLoadingPORow = True

    For lPORow = 1 To oPOGrid.DataRowCnt

        sTempCloseSrcLine = sCloseSrcLine
        If (giGetValidInt(gsGridReadCell(oPOGrid, lPORow, kcolPOSelect)) = vbChecked) Then
                '-- The PO line has been selected
                sPONum = Trim$(gsGridReadCellText(oPOGrid, lPORow, kcolPOPONo))
                sPOLineNum = Trim$(gsGridReadCell(oPOGrid, lPORow, kcolPOPOLineNo))
                sPOKeyStr = Left$(sPONum & String$(kPONoMaxLen, " "), kPONoMaxLen) & sPOLineNum
    
                '-- Get values of columns that can cause a line to be unmatched
                lVDRow = grdRcptDetl.ActiveRow
                dNewQty = gdGetValidDbl(gsGridReadCell(oPOGrid, lPORow, kcolPOQtyView))
                lNewUOMKey = glGetValidLong(gsGridReadCell(oPOGrid, lPORow, kcolPOUnitMeasKey))
    
                '-- Insert a new line in the receiver detail grid
                lNewRow = grdRcptDetl.MaxRows
                lRowsInserted = lRowsInserted + 1

                '-- Append a new row to the receiver detail grid
                moDmDetl.AppendRow
  
                '-- Assign all column values
                '-- SeqNo
                moLE.AssignSeqNo lNewRow
                
                '-- Get a new surrogate key for tpoRcvrLine
                gGridUpdateCell grdRcptDetl, lNewRow, kColReceiptLineKey, glGetNextSurrogateKey(moAppDB, "tpoRcvrLine")
    
                '-- Check if the line needs to be unmatched
                '-- MatchStatus
                gGridUpdateCell grdRcptDetl, lNewRow, kColMatchStatus, CStr(kMatchStatusOpen)
    
                '-- DispMatchStatus
                gGridUpdateCell grdRcptDetl, lNewRow, kColDispMatchStatus, _
                    gsBuildString(ksMatchOpen, moAppDB, moSysSession)
                    
                dTotalQtyRcvd = gdGetValidDbl(gsGridReadCell(oPOGrid, lPORow, kcolPORcptOrigQty))
                dTotalQtyOrdered = gdGetValidDbl(gsGridReadCell(oPOGrid, lPORow, kcolPOQtyOrd))
                lItemKey = glGetValidLong(gsGridReadCell(oPOGrid, lPORow, kcolPOItemKey))
                mlItemKey = lItemKey
                    
                '-- Rcpt total qty Rcvd
                gGridUpdateCell grdRcptDetl, lNewRow, kColOrigQtyRcvd, gsGetValidStr(dTotalQtyRcvd)
                                
                '-- PO Total Qty Ordered
                gGridUpdateCell grdRcptDetl, lNewRow, kColQtyOrdered, gsGetValidStr(dTotalQtyOrdered)
                
                'kColOrigQtyCurrUOM column contains the total quantity ordered (receipts) or received (returns)
                'in the current unit of measure.  Starts out in the unit of measure of the original document.
                If mlTranType = kTranTypePORTrn Then
                    gGridUpdateCell grdRcptDetl, lNewRow, kColOrigQtyCurrUOM, gsGetValidStr(dTotalQtyRcvd)
                Else
                    gGridUpdateCell grdRcptDetl, lNewRow, kColOrigQtyCurrUOM, gsGetValidStr(dTotalQtyOrdered)
                End If
                
                
                '-- Item Key
                moDmDetl.SetColumnValue lNewRow, "ItemRcvdKey", gsGetValidStr(lItemKey)
                
                 '-- UnitMeasID for both ordered and received
                gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasOrdID, gsGridReadCellText(oPOGrid, lPORow, kcolPOUOM)
                If mlTranType <> kTranTypePORTrn Then
                    gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdID, gsGridReadCellText(oPOGrid, lPORow, kcolPOUOM)
                '-- UnitMeasKey
                    If (lNewUOMKey <> 0) Then
                        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdKey, gsGetValidStr(lNewUOMKey)
                        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasOrdKey, gsGetValidStr(lNewUOMKey)
                    Else
                        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdKey, ""
                        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasOrdKey, gsGetValidStr(lNewUOMKey)
                    End If
                    'save the unit of measure from the PO in case they change it
                Else
                    gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdID, gsGridReadCellText(oPOGrid, lPORow, kcolPORcptUOMID)
                    gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdKey, gsGridReadCellText(oPOGrid, lPORow, kcolPORcptUOMKey)
                    'save the unit of measure from the receiver in case they change it.
                    gGridUpdateCell grdRcptDetl, lNewRow, kColOrigRcvrUOMKey, gsGridReadCell(oPOGrid, lPORow, kcolPORcptUOMKey)
                End If
    
                                            
                '-- POLineNo
                gGridUpdateCell grdRcptDetl, lNewRow, kColPOLineNo, gsGridReadCell(oPOGrid, lPORow, kcolPOPOLineNo)
    
                '-- POLineKey
                gGridUpdateCell grdRcptDetl, lNewRow, kColPOLineKey, gsGridReadCell(oPOGrid, lPORow, kcolPOPOLineKey)
                    
                '-- RtrnRcvrLineKey
                gGridUpdateCell grdRcptDetl, lNewRow, kColRtrnRcvrLineKey, gsGridReadCell(oPOGrid, lPORow, kcolPORcptLineKey)
    
                
                '-- ItemID
                gGridUpdateCell grdRcptDetl, lNewRow, kColItemID, gsGridReadCellText(oPOGrid, lPORow, kcolPOItemID)
    
                '-- MaskedItemID
                gGridUpdateCell grdRcptDetl, lNewRow, kColMaskedItemID, _
                    gsGridReadCellText(oPOGrid, lPORow, kcolPOItemID)
    
                '-- ItemDesc
                gGridUpdateCell grdRcptDetl, lNewRow, kColItemDesc, gsGridReadCellText(oPOGrid, lPORow, kcolPOItemDesc)
    
                '-- Warehouse ID
                gGridUpdateCell grdRcptDetl, lNewRow, kColWhseID, gsGridReadCellText(oPOGrid, lPORow, kcolPOWhseID)
    
                '-- Department ID
                gGridUpdateCell grdRcptDetl, lNewRow, kColDeptID, gsGridReadCellText(oPOGrid, lPORow, kcolPODeptID)
                
                '-- ReturnType
                If mlTranType <> kTranTypePORTrn Then
                    gGridUpdateCell grdRcptDetl, lNewRow, kColReturnType, 0
                    gGridUpdateCell grdRcptDetl, lNewRow, kColOrigRtrnType, 0
                Else
                    gGridUpdateCell grdRcptDetl, lNewRow, kColReturnType, 1
                    gGridUpdateCell grdRcptDetl, lNewRow, kColOrigRtrnType, 1
                End If
                
                '-- Quantity Being Received/Returned
                gGridUpdateCell grdRcptDetl, lNewRow, kColQtyRcvd, CStr(dNewQty)
                gGridUpdateCell grdRcptDetl, lNewRow, kColQtyRcvdS, CStr(dNewQty * miSignFactor)
               
                           
    
                '-- MeasType
                gGridUpdateCell grdRcptDetl, lNewRow, kColMeasType, gsGridReadCell(oPOGrid, lPORow, kcolPOUnitMeasType)
                
                '-- UnitCost
                dNewUC = gdGetValidDbl(gsGridReadCell(oPOGrid, lPORow, kcolPOUnitCost))
                gGridUpdateCell grdRcptDetl, lNewRow, kColUnitCost, CStr(dNewUC)
                '-- UnitCost Exact
                 dNewUCE = gdGetValidDbl(gsGridReadCell(oPOGrid, lPORow, kcolPOUnitCostExact))
                gGridUpdateCell grdRcptDetl, lNewRow, kColUnitCostExact, CStr(dNewUCE)
    
                '-- GLAcctKey
                gGridUpdateCell grdRcptDetl, lNewRow, kColGLAcctKey, gsGridReadCell(oPOGrid, lPORow, kcolPOGLAcctKey)
    
                
                '-- Qty Received
                gGridUpdateCell grdRcptDetl, lNewRow, kColTotalRcvd, gsGridReadCell(oPOGrid, lPORow, kcolPOQtyRcvd)

                '-- Qty Returned for credit
                gGridUpdateCell grdRcptDetl, lNewRow, kColQtyRtrned, gsGridReadCell(oPOGrid, lPORow, kcolPOQtyRtrnForCredit)

                '-- Qty Open to receive
                gGridUpdateCell grdRcptDetl, lNewRow, kColQtyOpen, gsGridReadCell(oPOGrid, lPORow, kcolPOQtyOpen)

                '-- Close PO Line
                '-- If the PO option isn't set to close the PO line, check the Vendor/Item table.
                
                If sTempCloseSrcLine = "0" Then
                    sSQL = "SELECT AllowVendBackOrd FROM timVendItem WHERE ItemKey = " & gsGridReadCell(oPOGrid, lPORow, kcolPOItemKey)
                    sSQL = sSQL & " AND VendKey = " & lkuVendID.KeyValue
                    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                    If Not rs.IsEmpty Then
                        iAllowBO = giGetValidInt(rs.Field("AllowVendBackOrd"))
                    Else
                        iAllowBO = 1
                    End If
                    If Not rs Is Nothing Then
                        rs.Close: Set rs = Nothing
                    End If
                    If iAllowBO = 0 Then
                        sTempCloseSrcLine = "1"
                    End If
                End If
                gGridUpdateCell grdRcptDetl, lNewRow, kColCloseSrcLine, sTempCloseSrcLine

                '-- AcctRefKey
                lAcctRefKey = glGetValidLong(gsGridReadCell(oPOGrid, lPORow, kcolPOAcctRefKey))
                If (lAcctRefKey <> 0) Then
                    gGridUpdateCell grdRcptDetl, lNewRow, kColAcctRefKey, CStr(lAcctRefKey)
                Else
                    gGridUpdateCell grdRcptDetl, lNewRow, kColAcctRefKey, ""
                End If
                
                '-- TranCmnt
                sTranCmnt = Left(gsGridReadCell(oPOGrid, lPORow, kcolPOLineComment), 50)
                gGridUpdateCell grdRcptDetl, lNewRow, kColTranComment, sTranCmnt
                
                
                '-- Populate the Project-related columns in the voucher grid here
                gGridUpdateCell grdRcptDetl, lNewRow, kColProjectID, gsGridReadCell(oPOGrid, lPORow, kColPOProjectID)
                gGridUpdateCell grdRcptDetl, lNewRow, kColProjectKey, gsGridReadCell(oPOGrid, lPORow, kColPOProjectKey)
                gGridUpdateCell grdRcptDetl, lNewRow, kColPhaseID, gsGridReadCell(oPOGrid, lPORow, kColPOPhaseID)
                gGridUpdateCell grdRcptDetl, lNewRow, kColPhaseKey, gsGridReadCell(oPOGrid, lPORow, kColPOPhaseKey)
                gGridUpdateCell grdRcptDetl, lNewRow, kColTaskID, gsGridReadCell(oPOGrid, lPORow, kColPOTaskID)
                gGridUpdateCell grdRcptDetl, lNewRow, kColTaskKey, gsGridReadCell(oPOGrid, lPORow, kColPOTaskKey)
                gGridUpdateCell grdRcptDetl, lNewRow, kColJobLineKey, gsGridReadCell(oPOGrid, lPORow, kColPOJobLineKey)
                
                '-- Populate the Put Away related columns from the header settings.
                LoadGridPutAwayDetlInfo glGetValidLong(gsGridReadCell(oPOGrid, lPORow, kcolPOItemKey)), _
                                        mlWhseKey, mbTrackByBin, lNewRow
                
                '-- Create a new Line Dist row too, if necessary
                lLDRow = 1
                
                '-- Set the current row active in the detail grid
                moLE.Grid_Click 1, lNewRow
                    
                '-- Get a new surrogate key for tpoRcvrLineDist
                gGridUpdateCell grdRcvrLineDist, lNewRow, kColRcvrLineDistKeyD, _
                    glGetNextSurrogateKey(moAppDB, "tpoRcvrLineDist")
                    
                '-- AcctRefKey
                gGridUpdateCell grdRcvrLineDist, lLDRow, kColAcctRefKeyD, gsGridReadCell(oPOGrid, lPORow, kcolPOAcctRefKey)
                    
                '-- GLAcctKey
                gGridUpdateCell grdRcvrLineDist, lLDRow, kColGLAcctKeyD, gsGridReadCell(oPOGrid, lPORow, kcolPOGLAcctKey)
                    
                '-- POLineDistKey
                gGridUpdateCell grdRcvrLineDist, lLDRow, kColPOLineDistKeyD, gsGridReadCell(oPOGrid, lPORow, kcolPOPOLineDistKey)
                    
                If mlTranType = kTranTypePORTrn Then
                '-- RtrnRcvrLineDistKey
                    gGridUpdateCell grdRcvrLineDist, lLDRow, kcolRtrnRcvrLineDistKeyD, gsGridReadCell(oPOGrid, lPORow, kcolPORcptLineDistKey)
                End If
 
                '-- Update Freight and Sales Tax
                GetFreightAndSTax glGetValidLong(gsGridReadCell(oPOGrid, lPORow, kcolPOPOLineDistKey)), dNewQty
                gGridUpdateCell grdRcvrLineDist, lLDRow, kColFreightAmtD, CStr(mdFreightAmt)
                gGridUpdateCell grdRcptDetl, lNewRow, kColSTaxAmt, CStr(mdSTaxAmt)
                    
                '-- Quantity
                gGridUpdateCell grdRcvrLineDist, lLDRow, kColQtyRcvdD, CStr(dNewQty * miSignFactor)
                
                '-- Qty Rcvd in PO UOM
                If mlTranType = kTranTypePORTrn Then
                    If lNewUOMKey = 0 Then
                        sSQL = "SELECT UnitMeasKey FROM tpoPOLine WITH (NOLOCK) WHERE POLineKey = " & mlPOLineKey
                        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                        If Not rs.IsEmpty Then
                            lNewUOMKey = glGetValidLong(rs.Field("UnitMeasKey"))
                        Else
                            lNewUOMKey = 0
                        End If
                        If Not rs Is Nothing Then
                            rs.Close: Set rs = Nothing
                        End If
                    End If
                    moItem.UOMConversion glGetValidLong(gsGridReadCellText(oPOGrid, lPORow, kcolPORcptUOMKey)), lNewUOMKey, dNewQty, dConvFactor

                End If
                gGridUpdateCell grdRcvrLineDist, lLDRow, kcolQtyRcvdInPOUOMD, CStr(dNewQty * miSignFactor)
                '-- Qty Rcvd in PO UOM
                gGridUpdateCell grdRcptDetl, lNewRow, kColQtyInPOUOM, CStr(dNewQty * miSignFactor)
    
                '-- Item weight and volumne
                gGridUpdateCell grdRcptDetl, lNewRow, kColItemVol, moItem.Volume(lNewUOMKey)
                gGridUpdateCell grdRcptDetl, lNewRow, kColItemWght, moItem.Weight(lNewUOMKey)
    
                '-- Append a new row in the line dist grid
                moDmLineDist.AppendRow
                
                moDmLineDist.SetRowDirty lLDRow
                
                '-- Need to inform DM of new or changed row
                moDmDetl.SetRowDirty lNewRow
    
            '-- Populate the Project-related columns in the Job grid here
            If Len(Trim$(lkuProject.Text)) > 0 Then
                JobLEDetailToGrid lNewRow
            End If
        End If
    Next lPORow
    
    '-- Set the currently selected row in the detail grid to the first row
    If (lRowsInserted > 0) Then
        moLE.Grid_Click kColPOLineNo, 1
    Else
        moLE.Grid_Click kColPOLineNo, moLE.ActiveRow
        'Update flag to indicate all receiver lines were deleted and to try to re-create the default landed cost transactions
        'from LCs linked to the vendor/item in Setup Landed Cost.
        mbAllRcvrLinesWereOnceDeleted = True
    End If

    '-- Turn redraw of the grid back on
    SetGridRedraw grdRcptDetl, True
    
'    grdRcptDetl.MaxRows = grdRcptDetl.MaxRows - 1
'    ReDim msArrBinRetValues(1 To grdRcptDetl.DataRowCnt, 4)

    mbLoadingPORow = False
    Set oPOGrid = Nothing
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadPOLinesInDetlGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseDistr_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkCloseDistr, True
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long
    Dim sPOLineKey As String
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    
    '-- Security event is needed to change the Close PO Lines flag
    If Not mbManualClick Then
        sID = "RCVCLSDIST"
        sUser = CStr(moSysSession.UserId)
        vPrompt = True
        If (moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0) Then
            mbManualClick = True
            If chkCloseDistr = 1 Then
                chkCloseDistr = 0
            Else
                chkCloseDistr = 1
            End If
            mbManualClick = False
            Exit Sub
        End If
    End If
'    '-- Enable Item and Description fields if not matching this item
'    lrow = moLE.ActiveRow
'    sPOLineKey = Trim$(gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey)))
    If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
        moLE.GridEditChange chkMatch
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkCloseDistr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub chkMatch_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkMatch, True
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long
    Dim sPOLineKey As String
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    
    '-- Security event is needed to override matching
    If (chkMatch = vbUnchecked) And Not mbManualClick Then
        sID = "MTCHOVRDRC"
        sUser = CStr(moSysSession.UserId)
        vPrompt = True
        If (moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0) Then
            mbManualClick = True
            chkMatch = vbChecked
            mbManualClick = False
            Exit Sub
        End If
    End If

    '-- Enable Item and Description fields if not matching this item
'    lrow = moLE.ActiveRow
'    sPOLineKey = Trim$(gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColPOLineKey)))
    If Not moLE.RowAction Then
        '-- Turn on the detail command buttons
        moLE.GridEditChange chkMatch
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkMatch_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkMatch_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Turn on the detail command buttons
    moLE.GridEditChange chkMatch
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkMatch_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Sub lkuReceiver_PostLostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- If the PO number has a value, it is valid, so enable the Lines... button
    If Trim$(lkuReceiver) <> "" Then
        ToggleCtrlState cmdLines, True
    Else
        ToggleCtrlState cmdLines, False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuReceiver_PostLostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Sub lkuPONo_PostLostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- If the PO number has a value, it is valid, so enable the Lines... button
    If Trim$(lkuPONo) <> "" Then
        ToggleCtrlState cmdLines, True
    Else
        ToggleCtrlState cmdLines, False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPONo_PostLostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmHeader
                moFormCust.ApplyFormCust
        End If
    End If
#End If

    Dim i As Integer
    
    '-- Setup the Sage MAS 500 form (validation) manager control
    With SOTAValidationMgr1
        Set .Framework = moClass.moFramework
        If mlTranType <> kTranTypePORTrn Then
            .Keys.Add ddnType
            .Keys.Add lkuReceiptNo
        Else
            .Keys.Add lkuReturn
        End If
        .Init
    End With
    
    ' This will only get triggered when the form first comes up.
    If mbLoadingForm Then
        If Trim(lkuReceiptNo) = "" And Trim(lkuReturn) = "" Then
'   Disable the controls for line entry and clear out various fields.
            nbrQtyRcvd.Enabled = False
            nbrQtyRcvd.Value = 0
            ddnUOM.Enabled = False
            ddnReason.Enabled = False
            txtLnComment.Enabled = False
            chkMatch.Enabled = False
            chkCloseDistr.Enabled = False
            optRetCredit.Enabled = False
            optRetReplace.Enabled = False
            txtItemDescription = ""
            txtUOM = ""
            If mlTranType <> kTranTypePORTrn Then
                SetCtrlFocus lkuReceiptNo
            Else
                SetCtrlFocus lkuReturn
            End If
        End If
        mbLoadingForm = False
    End If

   
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub cmdPOLandedCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPOLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPOLandedCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPOLandedCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPOLandedCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPOLandedCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerf_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdVendPerf, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerf_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerf_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdVendPerf, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerf_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDist_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDist, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDist_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdUndo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdUndo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdUndo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdOK_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdOK, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdOK_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCommentDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCommentDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCommentDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCommentDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdWghtVol_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdWghtVol, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdWghtVol_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdWghtVol_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdWghtVol, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdWghtVol_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerfDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdVendPerfDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerfDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdVendPerfDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdVendPerfDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdVendPerfDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdAdditionalNotes1_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdAdditionalNotes1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdAdditionalNotes1_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdAdditionalNotes1_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdAdditionalNotes1, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdAdditionalNotes1_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLines_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLines_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLines_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLines_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtRelNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRelNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRelNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRelNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRelNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPONum, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBOL, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBOL, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBOL, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBOL_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBOL, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBOL_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtTranComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtTranComment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtTranComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTranComment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtTranComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTranComment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRMA_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRMA, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRMA_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRMA_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRMA, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRMA_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRMA_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRMA, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRMA_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRMA_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRMA, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRMA_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemDescription, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDescription_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDescription_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPOLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPOLine, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPOLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPOLine_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPOLine, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPOLine_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMultipleBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMultipleBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMultipleBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMultipleBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLnComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLnComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLnComment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLnComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLnComment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOMRcpt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtUOMRcpt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOMRcpt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOMRcpt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtUOMRcpt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOMRcpt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOMRcpt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtUOMRcpt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOMRcpt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOMRcpt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtUOMRcpt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOMRcpt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDept, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDept, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDept, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDept_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDept, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDept_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtWhse_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemOrdID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemOrdID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemOrdID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemOrdID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemOrdID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemOrdID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRtrnUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRtrnUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRtrnUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRtrnUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRtrnUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRtrnUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRtrnUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRtrnUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRtrnUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRtrnUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRtrnUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRtrnUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtClassOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtClassOvrdSegValue, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtClassOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtClassOvrdSegValue_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtClassOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtClassOvrdSegValue_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtVendName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtVendName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtVendName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtVendName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipWhseDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipWhseDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipWhseDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipWhseDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtTransit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtTransit, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtTransit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtTransit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtTransit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtTransit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiveToBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuReceiveToBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiveToBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiveToBin_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuReceiveToBin, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiveToBin_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiveToBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuReceiveToBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiveToBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiveToBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuReceiveToBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiveToBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiveToBin_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuReceiveToBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiveToBin_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiveToBin_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuReceiveToBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiveToBin_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAutoDistBin_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAutoDistBin_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuTask, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuTask, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuTask, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPhase, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPhase, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPhase, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuProject, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVendID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuVendID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVendID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVendID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuVendID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVendID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVendID_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuVendID, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVendID_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuVendID_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuVendID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuVendID_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuReceiptNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiptNo_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuReceiptNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiptNo_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPONo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPONo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPONo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPONo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPONo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPONo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPONo_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPONo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPONo_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiver_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuReceiver, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiver_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiver_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuReceiver, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiver_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiver_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuReceiver, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiver_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReceiver_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuReceiver, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReceiver_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReturn_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReturn_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReturn_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuReturn, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReturn_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReturn_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReturn_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuReturn_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuReturn_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuShipWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuShipWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipWhse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuShipWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipWhse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipWhse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuShipWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipWhse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTransfer_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuTransfer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTransfer_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTransfer_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuTransfer, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTransfer_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTransfer_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuTransfer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTransfer_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub nbrQtyRcvd_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyRcvd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyRcvd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyRcvd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyOrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyOrd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyOrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOrd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyOrd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOrd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrTotRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrTotRcvd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrTotRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrTotRcvd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrTotRcvd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrTotRcvd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyOpen, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyOpen, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyOpen, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyOpen_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyOpen, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyOpen_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyReturn, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyReturn_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyReturn, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyReturn_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrQtyShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrQtyShip, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrQtyShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrQtyShip_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrQtyShip, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrQtyShip_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curInvAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curInvAmtS, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curInvAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvAmtS_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curInvAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvAmtS_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curPurchAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curPurchAmtS, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curPurchAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curPurchAmtS_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curPurchAmtS, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curPurchAmtS_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPreferredBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPreferredBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPreferredBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPreferredBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPreferredBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPreferredBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPreferredBinDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPreferredBinDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPreferredBinDetl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPreferredBinDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPreferredBinDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPreferredBinDetl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkMatch_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkMatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkMatch_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkMatch_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkMatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkMatch_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseDistr_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCloseDistr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseDistr_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseDistr_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCloseDistr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseDistr_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetReplace_DblClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optRetReplace, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRetReplace_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetReplace_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optRetReplace, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRetReplace_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetReplace_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optRetReplace, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRetReplace_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetCredit_DblClick()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optRetCredit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRetCredit_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetCredit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optRetCredit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRetCredit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRetCredit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optRetCredit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRetCredit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPurchCompany_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnPurchCompany, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPurchCompany_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPurchCompany_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnPurchCompany, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPurchCompany_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPurchCompany_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnPurchCompany, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPurchCompany_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReason_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnReason, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReason_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnReason_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnReason_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange calRcptDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress calRcptDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus calRcptDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub calRcptDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus calRcptDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "calRcptDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Private Function bLoseFocus() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    bLoseFocus = True
    
    If Me.ActiveControl Is sbrMain Or Me.ActiveControl Is tbrMain Then
        bLoseFocus = False
'+++ VB/Rig Begin Pop +++
        Exit Function
'+++ VB/Rig End +++
        
    End If
    
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoseFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bFocusBack() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bFocusBack = False
    
    '-- Make sure we are not still in key section
    Select Case True
        Case Me.ActiveControl Is txtPONum
            Exit Function
        Case Me.ActiveControl Is lkuPONo
            Exit Function
        Case Me.ActiveControl Is lkuTransfer
            Exit Function
        Case Me.ActiveControl Is lkuReceiver
            Exit Function
        Case Me.ActiveControl Is navPO
            Exit Function
        Case Me.ActiveControl Is txtRelNo
            Exit Function
        Case Me.ActiveControl Is lkuVendID
            If mlTranType = kTranTypePORG Then
                If Len(Trim(lkuPONo.Text)) = 0 Then
                    Exit Function
                End If
            Else
                If Len(Trim(lkuReceiver.Text)) = 0 Then
                    Exit Function
                End If
            End If
        Case Me.ActiveControl Is lkuShipWhse
            If mlTranType = kTranTypePOTR Then
                If Len(Trim(lkuTransfer.Text)) = 0 Then
                    Exit Function
                End If
            Else
                If Len(Trim(lkuReceiver.Text)) = 0 Then
                    Exit Function
                End If
            End If
    End Select
    
    '-- If tran no is blank and type is not standard
    '-- do not lose focus
    If mlTranType <> kTranTypePOTR Then
        If Len(Trim(txtPONum)) = 0 Or Len(Trim(lkuVendID.Text)) = 0 Then
            bFocusBack = True
        End If
    Else
        If Len(Trim(lkuTransfer.Text)) = 0 Or Len(Trim(lkuShipWhse.Text)) = 0 Then
            bFocusBack = True
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bFocusBack", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtPONum_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPONum, True
    #End If
'+++ End Customizer Code Push +++
    Dim sOldlkuPoNoText As String
    If bLoseFocus() Then
        
        If Len(Trim(txtPONum.Text)) > 0 Then
            txtPONum.Text = sZeroFillNumber(txtPONum.Text, txtPONum.MaxLength)
        End If
        
        If Not (Me.ActiveControl Is txtRelNo Or Me.ActiveControl Is navPO) Then
            sOldlkuPoNoText = lkuPONo.Text
            If Len(Trim(txtPONum.Text)) > 0 Then
                If Len(Trim(Trim(txtRelNo.Text))) > 0 Then
                    lkuPONo.Text = Mid(txtPONum.Text & "          ", 1, 10) & "-" & Trim(txtRelNo.Text)
                Else
                    lkuPONo.Text = txtPONum.Text
                End If
            Else
                lkuPONo.Text = ""
            End If
            If lkuPONo.IsValid Then
                'Lookup value (restict clause) is valid, so do more extensive checking with detailed messages
                Dim sMessage As String
                sMessage = kDisplayMsg
                If Not bIsValidPONo(Me, moDmDetl, moLE, msHomeCurrID, , sMessage, , miClosePOonFirstRcpt) Then
                    'Put Focus back...
                    SetCtrlFocus txtPONum
                    'Put back the old values...
                    Me.SOTAValidationMgr1.StopValidation
                    Me.txtPONum.Text = Me.txtPONum.Tag
                    lkuPONo.Text = sOldlkuPoNoText
                    lkuPONo.Tag = sOldlkuPoNoText
                    Me.SOTAValidationMgr1.StartValidation
                Else
                    Me.txtPONum.Tag = Me.txtPONum.Text
                    If mlTranType = kTranTypePORG Then
                        moDmHeader.SetColumnValue "POKey", glGetValidLong(lkuPONo.KeyValue)
                    End If
                End If
            Else
                giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblPONo, kAmpersand)
                SetCtrlFocus txtPONum
                lkuPONo.Text = lkuPONo.Tag
            End If

        Else
            
            If bFocusBack Then
                SetCtrlFocus txtPONum
            End If
        
        End If
    
    Else
        
        SetCtrlFocus txtPONum
    
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtPONum_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRelNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRelNo, True
    #End If
'+++ End Customizer Code Push +++
    Dim sOldlkuPoNoText As String
    If bLoseFocus() Then
        
        If Not (Me.ActiveControl Is txtPONum Or Me.ActiveControl Is navPO) Then
            sOldlkuPoNoText = lkuPONo.Text
            
            If Len(Trim(txtPONum.Text)) > 0 Then
                If Len(Trim(Trim(txtRelNo.Text))) > 0 Then
                    lkuPONo.Text = Mid(txtPONum.Text & "          ", 1, 10) & "-" & Trim(txtRelNo.Text)
                Else
                    lkuPONo.Text = Mid(txtPONum.Text & "          ", 1, 10)
                End If
            End If
            If lkuPONo.IsValid Then
                'Lookup value (restict clause) is valid, so do more extensive checking with detailed messages
                Dim sMessage As String
                sMessage = kDisplayMsg
                If Not bIsValidPONo(Me, moDmDetl, moLE, msHomeCurrID, , sMessage, , miClosePOonFirstRcpt) Then
                    'Put Focus back...
                    SetCtrlFocus txtRelNo
                    'Put back the old values...
                    Me.SOTAValidationMgr1.StopValidation
                    Me.txtRelNo.Text = Me.txtRelNo.Tag
                    lkuPONo.Text = sOldlkuPoNoText
                    lkuPONo.Tag = sOldlkuPoNoText
                    Me.SOTAValidationMgr1.StartValidation
                Else
                    Me.txtRelNo.Tag = Me.txtRelNo.Text
                    If mlTranType = kTranTypePORG Then
                        moDmHeader.SetColumnValue "POKey", glGetValidLong(lkuPONo.KeyValue)
                    End If
                End If
            
            Else
                giSotaMsgBox Me, moSysSession, kmsgInvalidField, gsStripChar(lblPONo, kAmpersand)
                SetCtrlFocus txtRelNo
                lkuPONo.Text = lkuPONo.Tag
            End If
        
        Else
            
            If bFocusBack Then
                SetCtrlFocus txtRelNo
            End If
        
        End If
    
    Else
        
        SetCtrlFocus txtRelNo
    
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtRelNo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Property Get MyApp() As Object
'+++ VB/Rig Skip +++
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Skip +++
    Set MyForms = Forms
End Property

'*****************************************************************************************
'   New Stuff added for Transfer Receipts
'*****************************************************************************************

Private Function ReceiptType() As enumReceiptType
'+++ VB/Rig Skip +++
    ReceiptType = ddnType.ListIndex
End Function

Private Sub SetUpReceiptTypeFields()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL As String
    Dim bIsPurchaseReceipt As Boolean
    
    If ReceiptType = kReceiptTypePurchase Then
        Debug.Print "Purchase Receipt"
        mlTranType = TransactionTypes.kTranTypePORG
        bIsPurchaseReceipt = True
        chkCloseDistr.Caption = "Close PO Line for Receiving"
        lblPOLine.Caption = "PO Line"
        lkuReceiptNo.LookupID = "PendReceiver"
        SetRcptTransferFieldsVisible False
        SetRcptPurchaseFieldsVisible True
        lkuShipWhse = ""
        txtShipWhseDesc = ""
        lkuTransfer = ""
        txtTransit = ""
        lkuShipWhse.Tag = ""
        txtShipWhseDesc.Tag = ""
        lkuTransfer.Tag = ""
        txtTransit.Tag = ""
        cmdVendPerf.Enabled = True
        cmdVendPerfDtl.Enabled = True
        cmdPOLandedCost.Visible = True
        cmdPOLandedCost.Enabled = True
    Else
        Debug.Print "Transfer Receipt"
        mlTranType = TransactionTypes.kTranTypePOTR
        bIsPurchaseReceipt = False
        chkCloseDistr.Caption = "Close Transfer Line for Receiving"
        lblPOLine.Caption = "Transfer Line"
        lkuReceiptNo.LookupID = "PendReceiverTransferOrder" '"POPendRcvrTr"
        SetRcptPurchaseFieldsVisible False
        SetRcptTransferFieldsVisible True
        lkuVendID = ""
        txtVendName = ""
        txtPONum = ""
        txtRelNo = ""
        lkuVendID.Tag = ""
        txtVendName.Tag = ""
        txtPONum.Tag = ""
        txtRelNo.Tag = ""
        cmdVendPerf.Enabled = False
        cmdVendPerfDtl.Enabled = False
        cmdPOLandedCost.Visible = False
        cmdPOLandedCost.Enabled = False
    End If
            
    'Get the Tran Type ID from common information
    sSQL = "CompanyID = " & gsQuoted(msCompanyID) & " AND TranType = " & mlTranType
    msTranType = Trim(gsGetValidStr(moClass.moAppDB.Lookup("TranTypeID", _
                                    "tciTranTypeCompany", sSQL)))
    
    'Reinitialize the ReceiptNo Restrict Clause using the new value of mlTranType
    lkuReceiptNo.RestrictClause = sGetReceiptNoRestrict()
    Debug.Print lkuReceiptNo.RestrictClause
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "SetUpReceiptTypeFields", VBRIG_IS_FORM   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub SetRcptTransferFieldsVisible(bVisible As Boolean)
'+++ VB/Rig Skip
    Me.lblShipWhse.Visible = bVisible
    Me.lkuShipWhse.Visible = bVisible
    Me.lkuShipWhse.VisibleLookup = bVisible
    Me.txtShipWhseDesc.Visible = bVisible
    Me.lblTransfer.Visible = bVisible
    Me.lkuTransfer.Visible = bVisible
    Me.lkuTransfer.VisibleLookup = bVisible
    Me.lblTransit.Visible = bVisible
    Me.txtTransit.Visible = bVisible
    Me.lblQtyShip.Visible = bVisible
    Me.nbrQtyShip.Visible = bVisible

End Sub

Public Sub SetRcptPurchaseFieldsVisible(bVisible As Boolean)
'+++ VB/Rig Skip
    Me.lblVendID.Visible = bVisible
    Me.lkuVendID.Visible = bVisible
    Me.txtVendName.Visible = bVisible
    Me.lblPurchCompany.Visible = bVisible
    Me.ddnPurchCompany.Visible = bVisible
    Me.lblPONo.Visible = bVisible
    Me.txtPONum.Visible = bVisible
    Me.lblDash.Visible = bVisible
    Me.txtRelNo.Visible = bVisible
    Me.navPO.Visible = bVisible
    Me.cmdLines.Visible = bVisible
    Me.lblWhse.Visible = bVisible
    Me.txtWhse.Visible = bVisible
    Me.lblDept.Visible = bVisible
    Me.txtDept.Visible = bVisible
    Me.chkMatch.Visible = bVisible
    Me.lblQtyReturn.Visible = bVisible
    Me.nbrQtyReturn.Visible = bVisible
End Sub

Public Sub SetRcptEntryFieldsEnabled(bEnabled As Boolean)
    lkuShipWhse.Enabled = bEnabled
    lkuTransfer.Enabled = bEnabled
    
    lkuVendID.Enabled = bEnabled
    
    If mlTranType <> kTranTypePORTrn Then
        txtPONum.Enabled = bEnabled
        txtRelNo.Enabled = bEnabled
    End If
    
    navPO.Enabled = bEnabled
    ddnPurchCompany.Enabled = bEnabled

End Sub

Public Sub LoadTransferLinesInDetlGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim rs As SOTADAS.DASRecordSet
    Dim sSQL As String
    Dim lReceiptNo As Long
    
    Dim sCloseSrcLine As String
    Dim lOldRow As Long
    Dim lRowsInserted As Long
    Dim lNewRow As Long
    Dim lAcctRefKey As Long
    Dim lLDRow As Long
    
    '--Get the Receiver Lines for the Transfer from the database
    If IsEmpty(lkuReceiptNo.KeyValue) Or IsNull(lkuReceiptNo.KeyValue) Then
        lReceiptNo = 0
    Else
        lReceiptNo = lkuReceiptNo.KeyValue
    End If
    sSQL = "{call sppoSelectTransferLinesRcvr (" & _
            gsQuoted(msCompanyID) & _
            ", " & lReceiptNo & _
            ", " & gsQuoted(lkuTransfer) & _
            ")}"
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    '--Initialize the Grid
    'Remove existing lines, if any
    If grdRcptDetl.DataRowCnt > 0 Then DeletePOLinesFromGrid
    
    If rs.IsEmpty Then
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
        
    sCloseSrcLine = IIf(mbCloseSrcLine, "1", "0")
    
    'Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdRcptDetl, False

    lRowsInserted = 0

    'Save off the current grid row
    lOldRow = grdRcptDetl.ActiveRow
    
    'Click on the last line in the grid; fixes a grids-as-parents bug
    'where line distribution records weren't being saved
    moLE.Grid_Click kColPOLineNo, grdRcptDetl.MaxRows
    
    'Reset the grid row to it's old state
    moLE.Grid_Click kColPOLineNo, lOldRow
    
    mbLoadingPORow = True
    
    '--Load recordset data into the grid
    Do While Not rs.IsEOF
        Debug.Print rs.Field("ItemID")
        
        '-- Insert a new line in the receiver detail grid
        lNewRow = grdRcptDetl.MaxRows
        lRowsInserted = lRowsInserted + 1
        moDmDetl.AppendRow

        '-- Assign all column values
        '-- SeqNo
        moLE.AssignSeqNo lNewRow
        
        '-- Get a new surrogate key for tpoRcvrLine
        gGridUpdateCell grdRcptDetl, lNewRow, kColReceiptLineKey, glGetNextSurrogateKey(moAppDB, "tpoRcvrLine")
        
        gGridUpdateCell grdRcptDetl, lNewRow, kColMatchStatus, CStr(kMatchStatusHardOvrd)
        gGridUpdateCell grdRcptDetl, lNewRow, kColDispMatchStatus, gsBuildString(kMatchStatusHardOvrd, moAppDB, moSysSession)
        'NOTE: Put the transfer line no in the "Line" column
        gGridUpdateCell grdRcptDetl, lNewRow, kColPOLineNo, rs.Field("TrnsfrLineNo")
        gGridUpdateCell grdRcptDetl, lNewRow, kColOrigQtyRcvd, rs.Field("QtyRcvd")
        'gGridUpdateCell grdRcptDetl, lNewRow, kColPOLineKey, 0
        gGridUpdateCell grdRcptDetl, lNewRow, kColTrnsfrLineKey, rs.Field("TrnsfrOrderLineKey")
        gGridUpdateCell grdRcptDetl, lNewRow, kColRtrnRcvrLineKey, 0
        moDmDetl.SetColumnValue lNewRow, "ItemRcvdKey", rs.Field("ItemKey")
        gGridUpdateCell grdRcptDetl, lNewRow, kColItemID, rs.Field("ItemID")
        gGridUpdateCell grdRcptDetl, lNewRow, kColMaskedItemID, rs.Field("ItemID")
        gGridUpdateCell grdRcptDetl, lNewRow, kColItemDesc, rs.Field("ItemDesc")
        gGridUpdateCell grdRcptDetl, lNewRow, kColWhseID, rs.Field("WhseID")
        gGridUpdateCell grdRcptDetl, lNewRow, kColDeptID, rs.Field("PurchDeptID")
        gGridUpdateCell grdRcptDetl, lNewRow, kColReturnType, 0
        gGridUpdateCell grdRcptDetl, lNewRow, kColOrigRtrnType, 0
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyRcvd, rs.Field("QtyRcvd")
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyRcvdS, CStr(rs.Field("QtyRcvd") * miSignFactor)
        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasOrdID, rs.Field("UnitMeasID")
        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasOrdKey, rs.Field("UnitMeasKey")
        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdID, rs.Field("RcvrUOMID")
        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitMeasRcvdKey, rs.Field("RcvrUOMKey")
        gGridUpdateCell grdRcptDetl, lNewRow, kColMeasType, rs.Field("MeasType")
        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitCost, CStr(gdGetValidDbl(rs.Field("UnitCost")))
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyOrdered, rs.Field("QtyOrd")
        gGridUpdateCell grdRcptDetl, lNewRow, kColTotalRcvd, rs.Field("TotalQtyRcvd")
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyRtrned, rs.Field("QtyRtrnCredit")
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyShip, rs.Field("QtyShip")
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyOpen, rs.Field("QtyOpen")
        gGridUpdateCell grdRcptDetl, lNewRow, kColCloseSrcLine, 0 'Default to No
        gGridUpdateCell grdRcptDetl, lNewRow, kColTranComment, "" 'No default comment
        gGridUpdateCell grdRcptDetl, lNewRow, kColUnitCostExact, CStr(gdGetValidDbl(rs.Field("UnitCost"))) 'UnitCost value to UnitCostExact field
                                
        '-- Populate the Put Away related columns from the header settings.
        LoadGridPutAwayDetlInfo glGetValidLong(rs.Field("ItemKey")), _
                                mlWhseKey, mbTrackByBin, lNewRow
                                
        '-- Insert a new line in the receiver detail grid
        lLDRow = 1
        
        '-- Set the current row active in the detail grid
        moLE.Grid_Click 1, lNewRow
        
        
        '-- Get a new surrogate key for tpoRcvrLineDist
        gGridUpdateCell grdRcvrLineDist, lLDRow, kColRcvrLineDistKeyD, _
            glGetNextSurrogateKey(moAppDB, "tpoRcvrLineDist")

        'gGridUpdateCell grdRcvrLineDist, lLDRow, kColAcctRefKeyD, rs.Field("AcctRefKey")
        'gGridUpdateCell grdRcvrLineDist, lLDRow, kColGLAcctKeyD, rs.Field("GLAcctKey")
        'gGridUpdateCell grdRcvrLineDist, lLDRow, kColPOLineDistKeyD, rs.Field("POLineDistKey")
        gGridUpdateCell grdRcvrLineDist, lLDRow, kColQtyRcvdD, CStr(rs.Field("QtyRcvd") * miSignFactor)
        gGridUpdateCell grdRcvrLineDist, lLDRow, kcolQtyRcvdInPOUOMD, CStr(rs.Field("QtyRcvd") * miSignFactor)
        '-- Qty Rcvd in PO UOM
        gGridUpdateCell grdRcptDetl, lNewRow, kColQtyInPOUOM, CStr(rs.Field("QtyRcvd") * miSignFactor)
        
        'kColOrigQtyCurrUOM column contains the total quantity ordered in the current unit of measure.
        'Starts out in the unit of measure of the original document.
        gGridUpdateCell grdRcptDetl, lNewRow, kColOrigQtyCurrUOM, rs.Field("QtyOrd")
        
                   
        '-- Append a new row in the line dist grid
        moDmLineDist.AppendRow

        '-- Need to inform DM of new or changed rows
        moDmLineDist.SetRowDirty lLDRow
        moDmDetl.SetRowDirty lNewRow

        rs.MoveNext
    Loop
    
    '-- Set the currently selected row in the detail grid to the first row
    If (lRowsInserted > 0) Then
        moLE.Grid_Click kColPOLineNo, 1
    Else
        moLE.Grid_Click kColPOLineNo, moLE.ActiveRow
    End If

    '-- Turn redraw of the grid back on
    SetGridRedraw grdRcptDetl, True

    mbLoadingPORow = False

    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
  

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmEnterRcptOfGoods", "LoadTransferLinesInDetlGrid", VBRIG_IS_FORM    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function lGetInvtTranKey(lRow As Long) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sTranDate As String
    Dim sTranNo As String
    Dim lTranType As Long
    Dim lInvtTranKey As Long
    Dim lRetVal As Long
    
' The following code was inserted to fix Scopus 19253
' The TranNo field in timInvtTranLog is the same as the
' Receipt No. By generating the InvtTranKey at this point
' the generation of a new Invt TranNo for the corresponding Invt Tran record is
' prevented (at spimDistSaveDist). After posting this number is updated in
' timInvtTran.
    
    sTranDate = calRcptDate.SQLDate
    
    If mlTranType <> kTranTypePORTrn Then
        sTranNo = gsGetValidStr(lkuReceiptNo.Text)
    Else
        sTranNo = gsGetValidStr(lkuReturn.Text)
    End If
    
    lTranType = glGetValidLong(moDmHeader.GetColumnValue("TranType"))
    With moAppDB
        '-- Get InvtTranKey For this receipt InvtTran
        .SetInParam msCompanyID
        .SetInParam sTranDate
        .SetInParam sTranNo
        .SetInParam lTranType
        .SetOutParam lInvtTranKey                                                  ' Output parameter
        .SetOutParam lRetVal                                                       ' Output parameter
        .ExecuteSP "spimGetInvtTran"
        lRetVal = .GetOutParam(6)
        lInvtTranKey = glGetValidLong(.GetOutParam(5))
        .ReleaseParams
    End With
    If lRetVal <> 0 Then
        giSotaMsgBox Me, moSysSession, kmsgFatalError
        lGetInvtTranKey = 0
        Exit Function
    End If
    gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, glGetValidLong(lInvtTranKey)
    lGetInvtTranKey = lInvtTranKey
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lGetInvtTranKey", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub JobLEDetailToGrid(lRow As Long)
    '-- Update the job grid columns
    With moProject
        gGridUpdateCell grdJobLine, .lJobGridRow, kColProjectIDJ, lkuProject.Text
        gGridUpdateCell grdJobLine, .lJobGridRow, kColProjectKeyJ, CStr(lkuProject.KeyValue)
        gGridUpdateCell grdJobLine, .lJobGridRow, kColPhaseIDJ, lkuPhase.Text
        gGridUpdateCell grdJobLine, .lJobGridRow, kColPhaseKeyJ, CStr(lkuPhase.KeyValue)
        gGridUpdateCell grdJobLine, .lJobGridRow, kColTaskIDJ, lkuTask.Text
        gGridUpdateCell grdJobLine, .lJobGridRow, kColTaskKeyJ, CStr(lkuTask.KeyValue)
        gGridUpdateCell grdJobLine, .lJobGridRow, kColJobLineKeyJ, _
            CStr(glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColJobLineKey)))
        gGridUpdateCell grdJobLine, .lJobGridRow, kColDocLineKeyJ, _
            CStr(glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColReceiptLineKey)))
        gGridUpdateCell grdJobLine, .lJobGridRow, kColAddUpdDelJ, CStr(LINE_UPDATE)
        
        If (moProject.iJobGridState = kGridAdd) Then
            moProject.AppendJobGridRow
        End If
    End With
End Sub

Private Function bCanDistAgainstPrefBin(lItemKey As Long, lWhseKey As Long, iPrefBinNo As Integer, Optional bIgnoreTrackMeth As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim bPrefBinExists As Boolean

    bCanDistAgainstPrefBin = False

    If bIgnoreTrackMeth = True Then
        'Just check if a preferred bin exist for the inventory item.
        bPrefBinExists = CBool(moAppDB.Lookup("COUNT(*)", _
                              "timInvtBinList ibl WITH (NOLOCK)", _
                              "ibl.WhseKey = " & lWhseKey & " AND ibl.ItemKey = " & lItemKey & " AND ibl.PrefNo = " & giGetValidInt(iPrefBinNo) & _
                              " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(" & lWhseKey & "," & lItemKey & "," & mlTranType & ")"))
    Else
        'If the inventory item has a preferred bin defined and the item is "None" tracked,
        'then there is enough information to perform the distribution.
        bPrefBinExists = CBool(moAppDB.Lookup("COUNT(*)", _
                              "timInvtBinList ibl WITH (NOLOCK) JOIN timItem i WITH (NOLOCK) ON ibl.ItemKey = i.ItemKey", _
                              "i.TrackMeth = " & IMS_TRACK_METHD_NONE & _
                              " AND ibl.WhseKey = " & lWhseKey & " AND ibl.ItemKey = " & lItemKey & " AND ibl.PrefNo = " & giGetValidInt(iPrefBinNo) & _
                              " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(" & lWhseKey & "," & lItemKey & "," & mlTranType & ")"))
    End If
    
    If bPrefBinExists = True Then
        bCanDistAgainstPrefBin = True
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bCanDistAgainstPrefBin", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub bSetupLookupNav(lkuCtl As Control, _
                            ByVal sNavID As String, _
                            ByVal sWhereClause As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set lkuCtl.Framework = moClass.moFramework
    Set lkuCtl.SysDB = moClass.moAppDB
    Set lkuCtl.AppDatabase = moClass.moAppDB
    
    lkuCtl.LookupID = sNavID

    If Len(Trim$(sWhereClause)) > 0 Then
        lkuCtl.RestrictClause = sWhereClause
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetupLookupNav", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub LoadGridPutAwayDetlInfo(ByVal lItemKey As String, ByVal lWhseKey As Long, ByVal bTrackByBin As Boolean, ByVal lNewRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'This routine will look to the header-level put away controls to update the grid with
'default values for each item loaded.  It will also consider the item's type and if
'the warehouse tracks quantity at bin.

Dim sSQL As String
Dim rs As Object
Dim iItemType As Integer
Dim bIsInventoryItem As Boolean

    'Determine if the item is an inventory item.
    iItemType = moClass.moAppDB.Lookup("ItemType", "timItem WITH (NOLOCK)", "ItemKey = " & lItemKey)
    If iItemType = IMS_FINISHED_GOOD Or _
       iItemType = IMS_RAW_MATERIAL Then
        bIsInventoryItem = True
    Else
        bIsInventoryItem = False
    End If

    If bIsInventoryItem = True Then
        If bTrackByBin = True Then
            'Setup the preferred bin control first.
             SetDetlPreferredBin CBool(chkPreferredBin.Value), lItemKey, lWhseKey, lNewRow, True
            
            'Check if the use defined a header-level put away bin.
            If glGetValidLong(lkuReceiveToBin.KeyValue) <> 0 Then
                'Assign the header bin to all detail rows as they are loaded.
                lkuAutoDistBin.SetTextAndKeyValue lkuReceiveToBin.Text, lkuReceiveToBin.KeyValue
                
                bIsValidAutoDistBin lNewRow
                
                'Now update the grid.  Make sure to update it after calling bIsValidAutoDistBin
                'or else the kColDoAutoDist column will not be updated if it can be auto-distributed.
                gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistBin, CStr(lkuReceiveToBin.Text)
                gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistBinKey, CStr(lkuReceiveToBin.KeyValue)
                
                'Set the preferred bin value to false.
                gGridUpdateCell grdRcptDetl, lNewRow, kColUsePreferred, 0
            End If

        Else
            'Warehouse does not track qty at bin.  Get and use the default bin.
            sSQL = "SELECT wb.WhseBinKey, wb.WhseBinID FROM timWhseBin wb WITH (NOLOCK)" & _
                    " WHERE wb.WhseKey = " & glGetValidLong(lWhseKey) & _
                    " AND wb.DfltBin = 1"
            
            Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
            If Not rs.IsEmpty Then
                gGridUpdateCell grdRcptDetl, lNewRow, kColUsePreferred, 0
                gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistBinKey, glGetValidLong(rs.Field("WhseBinKey"))
                gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistBin, ""
            End If
            If Not rs Is Nothing Then
                rs.Close: Set rs = Nothing
            End If
        End If

    Else
        gGridUpdateCell grdRcptDetl, lNewRow, kColUsePreferred, 0
        gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistBinKey, 0
        gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistBin, ""
        gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistInvtLotKey, 0
        gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistLotNo, ""
        gGridUpdateCell grdRcptDetl, lNewRow, kColAutoDistLotExpDate, ""
        gGridUpdateCell grdRcptDetl, lNewRow, kColDoAutoDist, 0
    End If
    
    'After the detail lines have been loaded, disable the header bin controls.
    'as they can only be used one when the lines are loaded.
    lkuReceiveToBin.Enabled = False
    chkPreferredBin.Enabled = False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadGridPutAwayDetlInfo", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub HidePutAwayCtrls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

        chkPreferredBin.Value = 0
        lkuReceiveToBin.SetTextAndKeyValue "", 0
        
        fraPutAwayAllGoodsTo.Enabled = False
        chkPreferredBin.Enabled = False
        lkuReceiveToBin.Enabled = False
        
        fraPutAwayAllGoodsTo.Visible = False
        chkPreferredBin.Visible = False
        lkuReceiveToBin.Visible = False

        chkPreferredBinDetl.Value = 0
        lkuAutoDistBin.SetTextAndKeyValue "", 0
        lkuAutoDistBin.Tag = ""
        
        fraPutAwayAllGoodsToDetl.Enabled = True
        chkPreferredBinDetl.Enabled = False
        lkuAutoDistBin.Enabled = False
        
        fraPutAwayAllGoodsToDetl.Visible = True
        chkPreferredBinDetl.Visible = False
        lkuAutoDistBin.Visible = True

        tabSubDetl.TabVisible(kTabPutAway) = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HidePutAwayCtrls", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function bSaveIMDistPORtrn(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim bValid As Boolean
    Dim lItemKey As Long
    Dim lInvtTranKey As Long
    Dim lTranIdentifier As Long
    Dim dQtyRcvd As Double
    Dim dRetStockQty As Double
    Dim iRetType As Integer
    Dim iOldRtrnType As Integer
    Dim iTranDirty As Integer
    Dim sSQL As String
    
    bSaveIMDistPORtrn = False
    
    If mlTranType <> kTranTypePORTrn Then
        Exit Function
    Else
        lTranIdentifier = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
        lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
        iTranDirty = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranDirty))
        lItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
        iRetType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColReturnType))

        ' If nothing has changed, and this is a previously saved distribution.
        ' However, ay still be necessary to deal with changed return type (adding/subtracting from timInventory.QtyOnPO)
        If lInvtTranKey > 0 And lTranIdentifier = 0 And iTranDirty = 0 Then
            'Check for changed return type.
            iOldRtrnType = giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColOrigRtrnType))
            If iOldRtrnType <> iRetType Then
                'timInvtTranDist has quantity in stock UOM, which is what we need.
                dRetStockQty = glGetValidLong(moClass.moAppDB.Lookup("SUM(DistQty)", "timInvtTranDist", "InvtTranKey = " & lInvtTranKey))
                '(Updates will be rolled back by DM if save fails.)
                If iOldRtrnType = 1 And iRetType = 2 Then
                    'need to increase qty on PO
                    sSQL = "UPDATE timInventory SET QtyOnPO = QtyOnPO + " & dRetStockQty & " WHERE ItemKey = " & lItemKey & " AND WhseKey = " & mlWhseKey
                ElseIf iOldRtrnType = 2 And iRetType = 1 Then
                    'need to decrease qty on PO
                    sSQL = "UPDATE timInventory SET QtyOnPO = QtyOnPO - " & dRetStockQty & " WHERE ItemKey = " & lItemKey & " AND WhseKey = " & mlWhseKey
                End If
                moClass.moAppDB.ExecuteSQL sSQL
            End If
            
            bSaveIMDistPORtrn = True
            Exit Function
        End If
    
'        If lTranIdentifier > 0 Then
'            'Get a new InvtTranKey using the TranNo of the Receipt for new receipt lines.
'            If lInvtTranKey = 0 Then
'                lInvtTranKey = lGetInvtTranKey(lRow)
'                If lInvtTranKey = 0 Then
'                    bSaveIMDistPORtrn = False
'                    Exit Function
'                End If
'            End If
'
'            'Returns are processed using older style line-oriented IM routines -
'            bValid = moIMDistribution.Save(calRcptDate.Text, lTranIdentifier, lInvtTranKey, _
'            lItemKey, dQtyRcvd, iRetType, lItemKey, , lTrnsfrOrderLineKey)
'
'            If moIMDistribution.oError.Number <> 0 Or Not bValid Then
'                '-- Display error message
'                moDmHeader.CancelAction
'                giSotaMsgBox Me, moSysSession, moIMDistribution.oError.MessageConstant
'                moIMDistribution.oError.Clear
'                mlInvalidRow = lrow 'balzer
'                bValid = False
'            End If
'
'            If lInvtTranKey = 0 And bValid Then
'                gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, ""
'            ElseIf bValid Then
'                gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(lInvtTranKey)
'                moDmDetl.SetColumnValue lRow, "InvtTranKey", lInvtTranKey
'            End If
'
'            bSaveIMDistPORtrn = bValid
'            Exit Function
'        End If
    
    End If

    bSaveIMDistPORtrn = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaveIMDistPORtrn", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetDetlPreferredBin(ByVal bUsePrefBin As Boolean, ByVal lItemKey As Long, ByVal lWhseKey As Long, Optional ByVal lRow As Long = 0, Optional ByVal bUpdateGrid As Boolean = False)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sSQL As String
    Dim rs As Object

    If bUsePrefBin = True Then
        sSQL = "SELECT ibl.WhseBinKey, wb.WhseBinID FROM timInvtBinList ibl WITH (NOLOCK)" & _
                " JOIN timInventory i WITH (NOLOCK) on ibl.ItemKey = i.ItemKey AND ibl.WhseKey = i.WhseKey" & _
                " JOIN timWhseBin wb WITH (NOLOCK) on ibl.WhseBinKey = wb.WhseBinKey" & _
                " WHERE ibl.PrefNo = 1" & _
                " AND ibl.ItemKey = " & lItemKey & _
                " AND ibl.WhseKey = " & glGetValidLong(lWhseKey) & _
                " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(" & lWhseKey & "," & lItemKey & "," & mlTranType & ")"
        
        Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEmpty Then
            'Bring the bin lookup to the front.
            lkuAutoDistBin.ZOrder 0

            'Use the preferred bin info to set the detail put away bin control's info
            lkuAutoDistBin.SetTextAndKeyValue gsGetValidStr(rs.Field("WhseBinID")), glGetValidLong(rs.Field("WhseBinKey"))
            lkuAutoDistBin.Enabled = False
            
            If bUpdateGrid = True And lRow <> 0 Then
                gGridUpdateCell grdRcptDetl, lRow, kColUsePreferred, 1
                gGridUpdateCell grdRcptDetl, lRow, kColPrefBinKey, CStr(glGetValidLong(rs.Field("WhseBinKey")))
            End If
            
            'A preferred bin was found.  See if it can be used to auto distribute.
            bIsValidAutoDistBin lRow
        
            'Now update the grid.  Make sure to update it after calling bIsValidAutoDistBin
            'or else the kColDoAutoDist column will not be updated if it can be auto-distributed.
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBin, gsGetValidStr(rs.Field("WhseBinID"))
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBinKey, CStr(glGetValidLong(rs.Field("WhseBinKey")))
        
        Else
            'Preferred bin does not exist.  Clear and enable the detail put away bin control.
            lkuAutoDistBin.SetTextAndKeyValue "", 0
            lkuAutoDistBin.Tag = ""
            lkuAutoDistBin.Enabled = True
            
            If bUpdateGrid = True And lRow <> 0 Then
                gGridUpdateCell grdRcptDetl, lRow, kColUsePreferred, 0
                gGridUpdateCell grdRcptDetl, lRow, kColPrefBinKey, 0
            End If
        End If
    
        If Not rs Is Nothing Then
            rs.Close: Set rs = Nothing
        End If
    Else
        'Preferred bin is not checked.  Clear and enable the detail put away bin control.
        lkuAutoDistBin.SetTextAndKeyValue "", 0
        lkuAutoDistBin.Tag = ""
        lkuAutoDistBin.Enabled = True
        
        If bUpdateGrid = True And lRow <> 0 Then
            gGridUpdateCell grdRcptDetl, lRow, kColUsePreferred, 0
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBinKey, 0
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBin, ""
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetDetlPreferredBin", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sHeaderWhseBinRestrict(lWhseKey As Long) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
        
    If mlRunMode = kContextAOF Then
        'Enter Customer Returns
        'Get valid bins - Active status, not LockedForPutaway.
        sHeaderWhseBinRestrict = "WhseBinKey IN " & "(SELECT WB.WhseBinKey " & _
            "FROM timWhseBin WB WITH (NOLOCK) " & _
            "WHERE WB.Status = 1 " & _
            "AND WB.LockedForPutaway <> 1 " & _
            "AND WB.WhseKey = " & Format$(lWhseKey) & " " & _
            "AND WB.Type <> 2)"
    Else
        'View Customer Returns
        sHeaderWhseBinRestrict = "WhseBinKey = 0"     'Will cause no items.
    End If
     
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sHeaderWhseBinRestrict", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function sDetlWhseBinRestrict(ByVal lWhseKey As Long, ByVal lItemKey As Long) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sRestrictClause As String

    sDetlWhseBinRestrict = ""
    
    If (lWhseKey > 0 And lItemKey > 0) Then
        'Get valid bins - Active status, not LockedForPutaway.  If random bin,
        'the bin must be empty or only have this item.
        sRestrictClause = "WhseBinKey IN " & "(SELECT WB.WhseBinKey " & _
            "FROM timWhseBin WB WITH (NOLOCK) " & _
            "LEFT OUTER JOIN timWhseBinInvt WBI WITH (NOLOCK) ON " & _
            "WB.WhseBinKey = WBI.WhseBinKey " & _
            "WHERE WB.Status = 1 " & _
            "AND WB.LockedForPutaway <> 1 " & _
            "AND WB.WhseKey = " & lWhseKey & " " & _
            "AND (WB.Type <> 2 " & _
            "OR (WB.Type = 2 " & _
            "AND (COALESCE(WBI.ItemKey, 0) IN (0, " & lItemKey & ") " & _
            "OR (WBI.ItemKey <> " & lItemKey & " " & _
            "AND WBI.QtyOnHand + WBI.PendQtyIncrease - WBI.PendQtyDecrease = 0))))) "
        
    Else
        sRestrictClause = "WhseBinKey = 0" 'will cause no items
    End If
    
    If lItemKey > 0 Then
        sRestrictClause = sRestrictClause & " AND ItemKey = " & lItemKey
    End If
    
    sDetlWhseBinRestrict = sRestrictClause
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupWhseBinRestrict", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function dGetAvailTrnsfrRcvgQty(lTrnsfrOrderLineKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim dAvailQty As Double
    Dim sSQL As String
    Dim rs As Object
    
    dGetAvailTrnsfrRcvgQty = 0
    
    sSQL = "{call sppoSelectTransferLinesRcvr ("
    sSQL = sSQL & gsQuoted(msCompanyID)
    sSQL = sSQL & ", " & glGetValidLong(moDmHeader.GetColumnValue("RcvrKey"))
    sSQL = sSQL & ", " & gsQuoted(lkuTransfer)
    sSQL = sSQL & ")}"
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    '--Load recordset data into the grid
    Do While Not rs.IsEOF
        If lTrnsfrOrderLineKey = glGetValidLong(rs.Field("TrnsfrOrderLineKey")) Then
            dAvailQty = gdGetValidDbl(rs.Field("QtyRcvd"))
            Exit Do
        End If
        rs.MoveNext
    Loop
    
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
    dGetAvailTrnsfrRcvgQty = dAvailQty
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dGetAvailTrnsfrRcvgQty", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub SetupLookupFunctionParams(ByRef lkuCtrl As Control, lWhseKey As Long, lItemKey As Long, lTranUOMKey As Long, lInvtTranKey As Long, lTranType As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    lkuCtrl.Parameters.Clear
    Call lkuCtrl.Parameters.Add(SQL_INTEGER, lWhseKey)
    Call lkuCtrl.Parameters.Add(SQL_INTEGER, lItemKey)
    Call lkuCtrl.Parameters.Add(SQL_INTEGER, lTranUOMKey)
    Call lkuCtrl.Parameters.Add(SQL_INTEGER, lTranType)
    Call lkuCtrl.Parameters.Add(SQL_INTEGER, lInvtTranKey)

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "SetupLookupFunctionParams", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetAutoDistSavedInfo(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lItemKey As Long
    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim bMultipleBin As Boolean
    Dim sLotNo As String
    Dim lInvtLotKey As Long
    Dim sLotExpDate As String
    Dim lPrefWhseBinKey As Long

    If mbTrackByBin = True Then
    
    'Lookup control does not allow any value except those that are available
    'for selection.  If we are dealing with multiple bins, bring the multiple
    'bin text box to the front and set the lookup value to an empty string.

    lItemKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColItemKey))
    lTranIdentifier = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
    
    Call moIMDistribution.sGetBinLkuCtrlSavedDistValues(lItemKey, lTranIdentifier, lInvtTranKey, bMultipleBin, sWhseBinID, lWhseBinKey, sLotNo, lInvtLotKey, sLotExpDate)
    
    If bMultipleBin = True Or lWhseBinKey <> 0 Then
        If bMultipleBin = True Then
            'Bring the multiple bin text box to the front.
            txtMultipleBin.ZOrder 0
            lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
            lkuAutoDistBin.Tag = ""
        Else
            'Bring the bin lookup to the front.
            lkuAutoDistBin.ZOrder 0
            'Set the values back to the bin lookup control and the grid.
            lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
            lkuAutoDistBin.Enabled = True
            lkuAutoDistBin.Tag = sWhseBinID
        End If
        
        lPrefWhseBinKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColPrefBinKey))
        
        If lPrefWhseBinKey = 0 Then
            lPrefWhseBinKey = glGetValidLong(moAppDB.Lookup("WhseBinKey", _
                "timInvtBinList WITH (NOLOCK)", _
                "PrefNo = 1 AND WhseKey = " & mlWhseKey & " AND ItemKey = " & mlItemKey & _
                " AND PrefBinType = dbo.fnIMGetInvtPrefBinType(" & mlWhseKey & "," & mlItemKey & "," & mlTranType & ")"))
        End If
        
        'Disable the use preferred bin if a preferred bin does not exist for the inventory item.
        If lPrefWhseBinKey = 0 Then
            chkPreferredBinDetl.Value = 0
            chkPreferredBinDetl.Enabled = False
        Else
            chkPreferredBinDetl.Enabled = True
        End If

        If bMultipleBin = True Then
            'Uncheck the preferred bin checkbox
            chkPreferredBinDetl.Value = 0
        ElseIf lWhseBinKey = lPrefWhseBinKey Then
            'Bin distributed matches preferred bin.
            If mlTranType <> kTranTypePORTrn Then
                chkPreferredBinDetl.Enabled = True
                chkPreferredBinDetl.Value = 1
                lkuAutoDistBin.Enabled = False
            End If
        Else
            If mlTranType <> kTranTypePORTrn Then
                lkuAutoDistBin.Enabled = True
                chkPreferredBinDetl.Value = 0
            End If
        End If
        
        'Update the grid with the distribution related values.
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranID, gsGetValidStr(lTranIdentifier)
        gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBin, sWhseBinID
        gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBinKey, gsGetValidStr(lWhseBinKey)
        gGridUpdateCell grdRcptDetl, lRow, kColAutoDistInvtLotKey, gsGetValidStr(lInvtLotKey)
        gGridUpdateCell grdRcptDetl, lRow, kColAutoDistLotNo, sLotNo
        gGridUpdateCell grdRcptDetl, lRow, kColAutoDistLotExpDate, sLotExpDate
        gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "0"
        lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
    
    Else
        lkuAutoDistBin.SetTextAndKeyValue sWhseBinID, lWhseBinKey
        chkPreferredBinDetl.Value = 0
    End If
    
        lkuAutoDistBin.Tag = lkuAutoDistBin.Text
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetAutoDistSavedInfo", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bAutoDistFromBinLku(lRow As Long, Optional bCmdDistClicked As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim lWhseKey As Long
    Dim lItemKey As Long
    Dim lTranType As Long
    Dim lUOMKey As Long
    Dim lBinUOMKey As Long
    Dim dBinQtyAvail As Double
    Dim dDistQty As Double
    Dim ocolBinRetColVals As Collection
    Dim sDoAutoDist As String
    Dim sWhseBinID As String
    Dim sLotNo As String
    Dim sLotExpDate As String

    bAutoDistFromBinLku = False
    
    'If this is a Transfer Receipt, it cannot be auto-distributed because
    'transfers have their own distinct lots/serial to receive.  Transfer
    'has it's own way of handling auto distributions.  See .ReceiveTransferDist
    If mlTranType = kTranTypePOTR Then
        bAutoDistFromBinLku = True
        Exit Function
    End If
    
    'Determine if we should do an auto-distribution.
    sDoAutoDist = gsGridReadCell(grdRcptDetl, lRow, kColDoAutoDist)
    If sDoAutoDist = "1" Then
        lTranIdentifier = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranID))
        lInvtTranKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey))
        
        'Reserve an InvtTranKey if needed.
        If lInvtTranKey = 0 Then
            lInvtTranKey = lGetInvtTranKey(lRow)
            If lInvtTranKey = 0 Then
                Exit Function
            End If
            gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(lInvtTranKey)
            moDmDetl.SetColumnValue lRow, "InvtTranKey", lInvtTranKey
            mlInvtTranKey = lInvtTranKey
        End If
                
        'Get values for auto dist method.
        lTranType = mlTranType
        lWhseKey = mlWhseKey
        lItemKey = mlItemKey
        
        'If the user modifies the quantity or uom and the clicks on the dist button,
        'the values won't be in the grid yet.  So go against the controls.
        If bCmdDistClicked Then
            lUOMKey = glGetValidLong(ddnUOM.ItemData)
            dDistQty = gdGetValidDbl(nbrQtyRcvd)
            sWhseBinID = gsGetValidStr(lkuAutoDistBin.Text)
        Else
            lUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey))
            dDistQty = gdGetValidDbl(gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd)))
            sWhseBinID = gsGridReadCell(grdRcptDetl, lRow, kColAutoDistBin)
        End If
        
        lBinUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColAutoDistBinUOMKey))
        If lBinUOMKey = 0 Then
            lBinUOMKey = lUOMKey
        End If
        dBinQtyAvail = gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColBinQtyAvail))
        sLotNo = gsGridReadCell(grdRcptDetl, lRow, kColAutoDistLotNo)
        sLotExpDate = gsGridReadCellText(grdRcptDetl, lRow, kColAutoDistLotExpDate)
        
        'Attempt to auto distribute the bin selection.
        'If successful, this routine will assign a TranIdentifier.
        
        If moIMDistribution.bPerformSilentDistribution(lTranIdentifier, lWhseKey, lItemKey, _
                                                       lTranType, lUOMKey, lBinUOMKey, _
                                                       dDistQty, dBinQtyAvail, sWhseBinID, _
                                                       sLotNo, sLotExpDate, "", lInvtTranKey) = True Then
            If lTranIdentifier > 0 Then
                'Store the values in the grid.
                gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "0"
                '.ClearData will clear the data plus the return columns collection.  Must clear the
                'collection in case user decides to type a value after using the lookup.  If it is not
                'cleared, it will use the collection and distribute against the wrong set of data.
                lkuAutoDistBin.ClearData
                lkuAutoDistBin.Text = lkuAutoDistBin.Tag
                mbAutoDistBinClicked = False
                
                'Mark DM dirty.
                moDmDetl.SetRowDirty lRow
            Else
                Exit Function
            End If
        
        End If
        
        '-- Set the Item Distribution fields
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranID, CStr(lTranIdentifier)
        gGridUpdateCell grdRcptDetl, lRow, kColInvtTranKey, CStr(lInvtTranKey)
    End If

    bAutoDistFromBinLku = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bAutoDistFromBinLku", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bIsValidAutoDistBin(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim colAutoDistBinRetCol As Collection
    Dim sAutoDistBinID As String
    Dim lAutoDistBinKey As Long
    Dim dAutoDistBinQtyAvail As Double
    Dim lAutoDistBinUOMKey As Long
    Dim sAutoDistLotNo As String
    Dim sAutoDistLotExpDate As String
    Dim bCanAutoDist As Boolean
    Dim bQuiet As Boolean
    Dim lUOMKey As Long
    Dim dDistQty As Double

    bIsValidAutoDistBin = False
    
    If lRow = grdRcptDetl.MaxRows Then
        Exit Function
    End If
    
    'This validation routine is designed to tell if the bin can be used
    'to auto-distribute the transaction.  Based on the tracking method,
    'it will try to see if there is enough information to do the auto-
    'distribution.  For example: Lot-tracked items require a lot number.
    
    'We will try to use the return column collection if we detect the user
    'selected a value by clicking the lookup, otherwise we will use the bin
    'itself to get the distribution info.  If the info is lacking, we will
    'raise an error stating the bin is invalid.
    mbValidAutoDistBin = False
    
    lUOMKey = glGetValidLong(gsGridReadCell(grdRcptDetl, lRow, kColUnitMeasRcvdKey))
    dDistQty = gdGetValidDbl(gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd)))
        
    'Check if the bin's value changed OR if the user changed the quantity / UOM
    '(signified by the Me.ActiveControl.Name not being equal the bin lookup control).
    If mlItemKey <> 0 And _
       (Len(lkuAutoDistBin.Text) <> 0 And _
        ((Me.ActiveControl.Name <> "lkuAutoDistBin") Or _
         (LCase(Trim(lkuAutoDistBin.Text)) <> LCase(Trim(gsGridReadCell(grdRcptDetl, lRow, kColAutoDistBin)))))) Then
       
        If mbAutoDistBinClicked = True Then
            If lkuAutoDistBin.ReturnColumnValues.Count <> 0 Then
                bCanAutoDist = True
                
                Set colAutoDistBinRetCol = lkuAutoDistBin.ReturnColumnValues
                moIMDistribution.GetAutoDistInfoFromRetCol mlItemKey, colAutoDistBinRetCol, sAutoDistBinID, _
                                                    dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                                    sAutoDistLotNo, sAutoDistLotExpDate, , lAutoDistBinKey
                                                    
                'Get the number of rows that match the BinID typed by the user.
                bCanAutoDist = moIMDistribution.bIsValidBinForAutoDist(mlWhseKey, mlItemKey, lkuAutoDistBin.Text, _
                                                 mlTranType, dDistQty, _
                                                 lUOMKey, moClass.moSysSession.BusinessDate, _
                                                 mlInvtTranKey, dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                                 sAutoDistLotNo, sAutoDistLotExpDate, , lAutoDistBinKey)
            End If
        Else
            'User likely typed in the bin.
            sAutoDistBinID = Trim(lkuAutoDistBin.Text)
            sAutoDistLotNo = gsGetValidStr(gsGridReadCell(grdRcptDetl, lRow, kColAutoDistLotNo))
            bQuiet = IIf(mbLoadingPORow = True, True, False)
            
            'Check if we can auto-distribute against the bin.
            bCanAutoDist = moIMDistribution.bIsValidBinForAutoDist(mlWhseKey, mlItemKey, lkuAutoDistBin.Text, _
                                             mlTranType, dDistQty, _
                                             lUOMKey, moClass.moSysSession.BusinessDate, _
                                             mlInvtTranKey, dAutoDistBinQtyAvail, lAutoDistBinUOMKey, _
                                             sAutoDistLotNo, sAutoDistLotExpDate, , lAutoDistBinKey, bQuiet)
        End If
        
        'Check if we can do an auto-distribution.
        If bCanAutoDist = False Then
            mbValidAutoDistBin = False
            If mbLoadingPORow <> True Then
                'OK to clear the control since user must have been to one to enter the data.
                'But, if the user clicked the preferred bin checkbox, then don't clear it.
                If giGetValidInt(gsGridReadCell(grdRcptDetl, lRow, kColUsePreferred)) <> 1 Then
                    lkuAutoDistBin.ClearData
                    lkuAutoDistBin.Tag = ""
                End If
            End If
                
            If lkuAutoDistBin.Enabled = True Then
                lkuAutoDistBin.SetFocus
            End If
            bIsValidAutoDistBin = False
        Else
            mbValidAutoDistBin = True
            bIsValidAutoDistBin = True
            UpdateDoAutoDistCol lRow
            lkuAutoDistBin.Tag = sAutoDistBinID
            lkuAutoDistBin.SetTextAndKeyValue sAutoDistBinID, lAutoDistBinKey
            
            'Update the grid column values.
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBin, CStr(sAutoDistBinID)
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistLotNo, CStr(sAutoDistLotNo)
            gGridUpdateCellText grdRcptDetl, lRow, kColAutoDistLotExpDate, CStr(sAutoDistLotExpDate)
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBinUOMKey, CStr(lAutoDistBinUOMKey)
            gGridUpdateCell grdRcptDetl, lRow, kColAutoDistBinKey, CStr(lAutoDistBinKey)
            gGridUpdateCell grdRcptDetl, lRow, kColBinQtyAvail, CStr(dAutoDistBinQtyAvail)
            
        End If
    
        'Must reset to false in case user decides to type a value after using the lookup.  If it is not reset,
        'it will use the lku's return columns collection which will have the wrong values.
        mbAutoDistBinClicked = False
    
    Else
        bIsValidAutoDistBin = True
    End If
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidAutoDistBin", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub lkuAutoDistBin_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuAutoDistBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lInvtTranKey As Long
    Dim dTranQty As Double
    
    lItemKey = mlItemKey
    lUOMKey = ddnUOM.ItemData
    lInvtTranKey = mlInvtTranKey
    dTranQty = nbrQtyRcvd

    moIMDistribution.bAutoDistBinLkuSetupClick lkuAutoDistBin, mlWhseKey, lItemKey, mlTranType, lUOMKey, dTranQty, lInvtTranKey

    SetupLookupFunctionParams lkuAutoDistBin, mlWhseKey, lItemKey, lUOMKey, lInvtTranKey, mlTranType
    
    moLE.GridEditChange lkuAutoDistBin
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_LookupClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuAutoDistBin, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    'Set flag use to determine if user has selected a value from the lookup section grid.
    'Flag is reset to False after auto-distribution is created.
    If colSQLReturnVal.Count <> 0 Then
        mbAutoDistBinClicked = True
        'If the item is lot tracked and the user selected the same bin but a different lot
        'from the lookup, set the tag to blank so the new lot/bin can be validated.
        If moItem.TrackMethod = IMS_TRACK_METHD_LOT And gsGridReadCell(Me.grdRcptDetl, grdRcptDetl.ActiveRow, kColAutoDistBin) = colSQLReturnVal(1) Then
            If gsGridReadCell(Me.grdRcptDetl, grdRcptDetl.ActiveRow, kColAutoDistLotNo) <> colSQLReturnVal(3) Then
                lkuAutoDistBin.Tag = ""
            End If
        End If
    Else
        mbAutoDistBinClicked = False
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_BeforeLookupReturn", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub lkuAutoDistBin_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuAutoDistBin, True
    #End If
'+++ End Customizer Code Push +++

    If LCase(Trim(lkuAutoDistBin.Tag)) <> LCase(Trim(lkuAutoDistBin.Text)) Then
        bIsValidAutoDistBin grdRcptDetl.ActiveRow
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lkuAutoDistBin_LostFocus", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub UpdateDoAutoDistCol(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'Bring the bin lookup to the front.
    lkuAutoDistBin.ZOrder 0
    
    If mbValidAutoDistBin = True And lkuAutoDistBin.IsValid = True Then
        If lkuAutoDistBin.Text <> gsGridReadCell(grdRcptDetl, lRow, kColAutoDistBin) Then
            gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "1"
        Else
            'Bin on the grid is not different from the bin selected.  So nothing has changed.
            If moItem.TrackMethod = IMS_TRACK_METHD_LOT Then
                'Since it is possible user selected the same bin but,
                'a different lot, we will assume it is a different selection
                'and set the auto-dist to true.
                gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "1"
            End If
        End If
    Else
        gGridUpdateCell grdRcptDetl, lRow, kColDoAutoDist, "0"
    End If

    If Not moLE.RowAction Then
        'Set the LE row dirty.
        moLE.GridEditChange cmdDist
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UpdateDoAutoDistCol", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub HandleToolbarMenuClick(ButtonConst As String, menu As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
'On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim oToolbarMenu   As clsToolbarMenuInfo
    
    Me.SetFocus
    
  ' See which key you were called from
    Select Case ButtonConst
        Case kTbMemo
            ' See which menu was selected
            Select Case menu
                Case "K" & kEntTypeAPVendor
                
                    ' Default Memo
                      Me.Enabled = False
                      CMMemoSelected lkuVendID
                      Me.Enabled = True
                      Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeAPVendor)
                      oToolbarMenu.lKey2 = gvCheckNull(moDmHeader.GetColumnValue("VendKey"), SQL_INTEGER)
                      gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain
                
                Case "K" & kEntTypePOReceipt
            
                    ' Additional Memo
                      Me.Enabled = False
                      CMMemoSelected lkuReceiptNo
                      Me.Enabled = True
                      Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypePOReceipt)
                      oToolbarMenu.lKey2 = gvCheckNull(moDmHeader.GetColumnValue("RcvrKey"), SQL_INTEGER)
                      gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain
            
            End Select
            
        Case Else   'Unknown button
            'New Command Button not programmed for
            tbrMain.GenericHandler ButtonConst, Me, moDmHeader, moClass
    End Select
   

'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarMenuClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ConfigureMemos()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim oToolbarMenu    As clsToolbarMenuInfo
    
    ' Vendor
    Set oToolbarMenu = New clsToolbarMenuInfo
    oToolbarMenu.CompanyId = msCompanyID
    oToolbarMenu.lKey1 = kEntTypePOReceipt
    oToolbarMenu.lKey2 = 0
    oToolbarMenu.ToolbarButtonKey = kTbMemo
    oToolbarMenu.ToolbarMenuKey = "K" & kEntTypePOReceipt
    oToolbarMenu.MenuEnabled = True
    oToolbarMenu.MenuVisible = True
    oToolbarMenu.MenuText = "Stock Receipt"
    m_MenuInfo.Add oToolbarMenu, oToolbarMenu.ToolbarButtonKey & oToolbarMenu.ToolbarMenuKey

    ' Customer
    Set oToolbarMenu = New clsToolbarMenuInfo
    oToolbarMenu.CompanyId = msCompanyID
    oToolbarMenu.lKey1 = kEntTypeAPVendor
    oToolbarMenu.lKey2 = 0
    oToolbarMenu.ToolbarButtonKey = kTbMemo
    oToolbarMenu.ToolbarMenuKey = "K" & kEntTypeAPVendor
    oToolbarMenu.MenuEnabled = True
    oToolbarMenu.MenuVisible = True
    oToolbarMenu.MenuText = "Vendor"
    m_MenuInfo.Add oToolbarMenu, oToolbarMenu.ToolbarButtonKey & oToolbarMenu.ToolbarMenuKey
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ConfigureMemos", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub OfficeInitialization()
'+++ VB/Rig Skip +++
On Error Resume Next
'*************************************************************************
'      Desc:  The Office class has been initialized elsewhere and has
'             already received this form�s reference. Therefore,
'             this method is additive if the client task wishes to add
'             clsDMForm and/or clsDMGrid references.
'     Parms:  N/A
'   Returns:  N/A
'************************************************************************
    With tbrMain.Office
        .AddFormObject moDmHeader, "Receipt"
        .AddGridObject moDmDetl, "ReceiptLines"
        .AddGridObject moDmLineDist, "LinesDist"
    End With
    Err.Clear
End Sub


Private Sub UpdateTranLogRecs()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

'***************************************************************************
'* Update the Pending timInvtTranLog record with TranAmt and TranQty
'* The timInvttranLog Record should have already been created.
'*
'*'*************************************************************************

Dim lRow As Long
Dim dQty As Double
Dim dUnitCost As Double
Dim lInvtTranKey  As Long


    If mlTranType = kTranTypePORG Or mlTranType = kTranTypePORTrn Then
       For lRow = 1 To grdRcptDetl.DataRowCnt
                If gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey) <> "" Then
                    
                    lInvtTranKey = gsGridReadCell(grdRcptDetl, lRow, kColInvtTranKey)
                    
                    If gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvdS) <> "" Then
                        dQty = gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvdS)
                    Else
                        dQty = 0
                    End If
                    
                    If moDmDetl.GetColumnValue(lRow, "UnitCost") <> "" Then
                        dUnitCost = moDmDetl.GetColumnValue(lRow, "UnitCost")
                    Else
                        dUnitCost = 0
                    End If
                    
                    moAppDB.ExecuteSQL "Update timInvtTranLog Set TranQty = " & dQty & " , TranAmt= " & dQty * dUnitCost & " WHERE  InvtTranKey = " & lInvtTranKey & " AND TranStatus=2"
                
                End If
        Next lRow
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UpdateTranLogRecs", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
End Sub

Private Function bGetSaveLandedCostForNewRcpt(lRcvrKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bGetSaveLandedCostForNewRcpt = False

    'Try to get and save landed cost for the receiver lines automatically if the receipt
    'is not yet represented in tpoLandedCostTran.
    If moPOEnterLandedCost Is Nothing Then
        Set moPOEnterLandedCost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                            kclsPOEnterLandedCost, ktskPOEnterLandedCost, kAOFRunFlags, kContextAOF)
                            
        If Not moPOEnterLandedCost Is Nothing Then
            If Not moPOEnterLandedCost.InitEnterLandedCost(moClass) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Landed Cost init failed."
                Exit Function
            End If
        End If
        
    End If

    If Not moPOEnterLandedCost Is Nothing Then
        If moPOEnterLandedCost.bLandedCostTranExistForRcvr(lRcvrKey) = False Then
            If moPOEnterLandedCost.bCreateLandedCostTrnxsForRcvr(lRcvrKey) = False Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Landed Cost save failed."
                Exit Function
            End If
        End If
    End If

    bGetSaveLandedCostForNewRcpt = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bGetSaveLandedCostForNewRcpt", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bAdjustSaveLandedCostForExistingRcpt(lRcvrKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bAdjustSaveLandedCostForExistingRcpt = False
    
    'Display the informational message outside the DM's sql transaction.  This flag tells us if we need to display it.
    mbDisplayMsgPOLandCostAutoAdjAmtOnEdit = False

    'Try to adjust existing landed cost for the receiver lines automatically if the receipt
    'line's quantity/uom, volume, weight is changed or a line is deleted.
    If mbAdjToLandedCostAmtsRequired Then
    
        If moPOEnterLandedCost Is Nothing Then
            Set moPOEnterLandedCost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                                kclsPOEnterLandedCost, ktskPOEnterLandedCost, kAOFRunFlags, kContextAOF)
                                
            If Not moPOEnterLandedCost Is Nothing Then
                If Not moPOEnterLandedCost.InitEnterLandedCost(moClass) Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Landed Cost init failed."
                    Exit Function
                End If
            End If
        End If
    
        If Not moPOEnterLandedCost Is Nothing Then
            'Check if we have a saved landed cost transaction for this receiver.
            If moPOEnterLandedCost.bLandedCostTranExistForRcvr(lRcvrKey) = True Then
                'Do not display message if a saved receipt went from having records to no records (by using the "..." next to the PO text box and
                'not selecting any lines).  They are basically saving a receipt with no lines however, we still need to call bEditLandedCostTrnxsForRcvr
                'so the landed cost lines are deleted.
                If grdRcptDetl.DataRowCnt > 0 Then
                    'On occasion, there is an empty blank line (counted as a DataRow) on the grid but really there no lines tied to the receipt.
                    If grdRcptDetl.DataRowCnt = 1 And gdGetValidDbl(gsGridReadCell(grdRcptDetl, 1, kColQtyRcvd)) = 0 Then
                        'Do nothing.
                    Else
                        'Don't display the message since user is about to view the Enter Landed Cost UI.
                        If Not mbUserClickedLandedCost Then
                            mbDisplayMsgPOLandCostAutoAdjAmtOnEdit = True
                        End If
                    End If
                End If
                'Since we saved landed cost transaction for this receiver, call the Edit routine so that it will re-apply the LC only to
                'those lines in the LC tran tables.
                If moPOEnterLandedCost.bEditLandedCostTrnxsForRcvr(lRcvrKey) = False Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Landed Cost update failed."
                    Exit Function
                End If
            Else
                'There are no LC transactions for this receipt.
                If mbAllRcvrLinesWereOnceDeleted = True Then
                    'This means the receipt lines were deleted which caused the LC tran lines to be deleted as well due to the cascade trigger.
                    'Rebuild the LC trans from default.  Call the Create method.
                    If moPOEnterLandedCost.bCreateLandedCostTrnxsForRcvr(lRcvrKey) = False Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Landed Cost save failed."
                        mbAllRcvrLinesWereOnceDeleted = False
                        Exit Function
                    End If
                    mbAllRcvrLinesWereOnceDeleted = False
                Else
                    'We never got rid of all of the rows. So call the Edit routine so that it will re-apply the LC only to
                    'those lines in the LC tran tables that currently exist in the receipt lines grid.  On occasion, there
                    'is an empty blank line (counted as a DataRow) on the grid but really there no lines tied to the receipt.
                    If grdRcptDetl.DataRowCnt >= 1 And gdGetValidDbl(gsGridReadCell(grdRcptDetl, 1, kColQtyRcvd)) <> 0 Then
                        'There truly is at least one receipt line.
                        If moPOEnterLandedCost.bEditLandedCostTrnxsForRcvr(lRcvrKey) = False Then
                            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, "Landed Cost save failed."
                            Exit Function
                        End If
                    End If
                End If
            End If
        End If
    End If

    bAdjustSaveLandedCostForExistingRcpt = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bAdjustSaveLandedCostForExistingRcpt", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bRcvrLinesAllHaveZeroQty() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lRow As Long
    Dim bNonZeroLineExist As Boolean
    
    'Loop through the receipt lines and check if the quantity for each line is zero.
    'If all are zero, then return true or if there are no records in the grid, return true.
    For lRow = 1 To grdRcptDetl.DataRowCnt
        If gdGetValidDbl(gsGridReadCell(grdRcptDetl, lRow, kColQtyRcvd)) <> 0 Then
            bNonZeroLineExist = True
            Exit For
        End If
    Next

    If bNonZeroLineExist Then
        bRcvrLinesAllHaveZeroQty = False
    Else
        bRcvrLinesAllHaveZeroQty = True
    End If



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bRcvrLinesAllHaveZeroQty", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

