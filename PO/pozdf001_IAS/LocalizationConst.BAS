Attribute VB_Name = "basLocalizationConst"
Option Explicit

'
'*********************************************************************************
' This file contains the String and Message constant definitions for this project
' based upon the LocalString and LocalMessage tables.
'
' Created On: 1/7/2004 2:00:36 PM
'*********************************************************************************
'
' Strings
'
Public Const kAnUOM = 140103                                ' UOM
Public Const kARSelect = 151182                             ' Select
Public Const kBatch = 6413                                  ' Batch
Public Const kBudgetOptNone = 1975                          ' None
Public Const kDescription = 6001                            ' Description
Public Const kIMTrnsfrOrdLineSum = 166242                   ' Transfer Order Line Summary
Public Const kItem = 6472                                   ' Item
Public Const kMessage = 240173                              ' Message
Public Const kPAInvoiceDescription = 263334                 ' Invoice Description
Public Const kPAPhase = 262538                              ' Phase
Public Const kPAProject = 262609                            ' Project
Public Const kPATask = 262539                               ' Task
Public Const kPurchOrdKeyCol = 16295                        ' Purchase Order
Public Const kReceiptNo = 220533                            ' Receipt No
Public Const kReceiverKey = 220554                          ' Receiver Key
Public Const ksCannotBeNegative = 220609                    ' {0} cannot be a negative value.
Public Const ksCannotRcvMoreThanAvail = 220643              ' You cannot receive more than the quantity available in the transit warehouse.
Public Const ksCannotRtrnMoreThanRcvd = 220603              ' You cannot return more than the quantity available on the receipt.
Public Const ksEnterPO = 220153                             ' Enter Purchase &Orders
Public Const ksLine = 220063                                ' Line
Public Const ksMatchOpen = 220014                           ' Open
Public Const kSOPutAwayBin = 250689                         ' Put Away Bin
Public Const kSOUsePrefBin = 250688                         ' Use Pref Bin
Public Const ksPOBatchTypeRGCmd = 220117                    ' &Enter Receipt Of Goods...
Public Const ksPOBatchTypeRTCmd = 220116                    ' &Enter Return Of Goods...
Public Const ksPOLineNum = 220044                           ' PO Line
Public Const ksQtyOrdered = 220071                          ' Qty Ordered
Public Const ksQtyReceived = 220072                         ' Qty Received
Public Const ksQtyReturned = 220074                         ' Qty Returned
Public Const kStatus = 2200                                 ' Status
Public Const kTranIncomplete = 604                      ' Incomplete
Public Const kTranPending = 601                         ' Pending
Public Const kTranPosted = 602                          ' Posted
Public Const kTranPurged = 603                          ' Purged
Public Const kTranVoid = 600                            ' Void
Public Const kUnknown = 610                             ' Unknown
Public Const kViewRcvrs = 220592                            ' View &Receivers
'
' Messages
'
Public Const kmsgSotaErr = 1002                             '             Module: {0}  Company:  {1}  AppName:  {2}Error < {3} >  occurred at < {4} >  in Procedure < {5} >{6}                                    This application will now close.
Public Const kmsgInvalidField = 100013                      ' Invalid {0}.
Public Const kmsgUnexpectedOperation = 100015               ' Unexpected Operation Value: {0}
Public Const kmsgGetSurrogateKeyFailure = 100031            ' Program failed to establish a {0} key.This record will not be added to the table.
Public Const kmsgDDNNotRegistered = 220056                  ' The drill down "{0}" is not registered on your machine. Please contact your system administrator.
Public Const kmsgPOErrCreatingPOVchLink = 220064            ' An error occurred while creating the PO-Voucher link record for the current voucher. Please call Product Support for technical assistance.
Public Const kmsgExistingPOAndVendNotValid = 220075         ' The specified vendor and purchase order are not valid for the current purchasing company ({0}).
Public Const kmsgExistingVendNotValid = 220076              ' The specified vendor is not valid for the current purchasing company ({0}).
Public Const kmsgWeightRequired = 220084                    ' The weight for the receiving unit of measure is required.
Public Const kmsgVolumeRequired = 220085                    ' The volume for the receiving unit of measure is required.
Public Const kmsgRcptNotInThisBatch = 220092                ' The {0} you entered is not in this batch so it may not be edited or viewed.    Batch Number for {1} you entered:  {2}    Please enter another Receipt Number.
Public Const kmsgErrorInitIMDist = 220093                   ' Unable to initialize for IM Distributions
Public Const kmsgIMDistInfoRequired = 220094                ' Lot/Serial/Bin information must be entered.
Public Const kmsgUpdateIMDistInfo = 220095                  ' The Lot/Serial/Bin information must be updated because the Quantity Received or the Unit of Measure has been changed.
Public Const kmsgPONotOpen = 220000                         ' The selected purchase order ({0}) does not have a Status of "Open".
Public Const kmsgPONotStandard = 220001                     ' The selected purchase order ({0}) is not a "Standard PO" type.
Public Const kmsgPONotValidForVendor = 220002               ' The selected {0} ({1}) is not valid for the current vendor ({2}) .
Public Const kmsgPOAllLinesClosed = 220003                  ' All lines for the selected purchase order ({0}) have been closed for matching. This purchase order cannot be used.
Public Const kmsgPOOnHold = 220004                          ' The selected purchase order ({0}) is on hold. Do you wish to continue?
Public Const kmsgPOLinesExist = 220006                      ' You have changed the purchase order number. All detail lines associated with the old purchase order ({0}) will be removed from the voucher. Do you really want to do this?
Public Const kmsgPOMatchAmtErr = 220007                     ' The amount invoiced for this item ({0}) is greater than the extended amount on the purchase order.
Public Const kmsgPOMatchQtyErr = 220008                     ' The quantity invoiced for this item ({0}) is greater than the quantity ordered on the purchase order.
Public Const kmsgPOErrCreatingRcpt = 220009                 ' An error occurred while creating receivers for the current purchase order. Please call Product Support for technical assistance.
Public Const kmsgPOErrDeletingRcpt = 220010                 ' An error occurred while deleting receivers for the current purchase order. Please call Product Support for technical assistance.
Public Const kmsgExistingPONotValid = 220011                ' The existing {0} ({1}) is not valid for the newly selected vendor ({2}). The {3} field will be cleared.
Public Const kmsgARBadField = 150003                        ' You have entered an invalid value in {0}.  It will be changed back.
Public Const kmsgPOTrnsfrNoStockToReceive = 220168          ' The selected transfer order ({0}) has no stock to receive. Do you wish to cancel this receipt?
Public Const kmsgFatalError = 165173                        ' Unexpected Error.
Public Const kmsgInvalidWhseBin = 165186                    ' Invalid Bin.
Public Const kmsgTranQtyDistQtyDiffer = 160144              ' An inventory distribution error has occured on the indicated line: Check that the quantity distributed matches the quantity received, the lot and serial numbers are correct, and the serial number is not a duplicate.
Public Const kmsgCannotRtrnMoreThanRcvd = 220101            ' You cannot return more than the quantity available on the receipt.
Public Const kmsgCannotBeBlank = 130082                     ' {0} cannot be blank.
Public Const kmsgBatchNotFoundError = 140033                ' The Accounts Payable batch you are attempting to access cannot be found at this time:Batch Key: {0}
Public Const kmsgBatchPostDateNull = 140035                 ' The batch posting date is empty.Please call Product Support for technical assistance.
Public Const kmsgVoucherLogStatusInvalid = 140040           ' The {0} shown below has a status which is inconsistent with its current state in the database:{1}:  {2}{3}:  {4}{5}:  {6}Please call Product Support for technical assistance.
Public Const kmsgVoucherLogError = 140012                   ' An error occurred while attempting to update the {0} log.Please call Product Support for technical assistance.
Public Const kmsgVendorActiveOrTemp = 140006                ' Only vendors in Active or Temporary status may be selected.
Public Const kmsgBatchPostDateError = 140034                ' The batch posting date is invalid:Posting Date: {0}
Public Const kmsgVoucherLogAccessError = 140039             ' The {0} Log information for this {1} cannot be accessed at this time.Please call Product Support for technical assistance.
Public Const kmsgGetVouchNoError = 140025                   ' A serious error occurred while attempting to retrieve the next {0} number for this {1}.Please call Product Support for technical assistance.
Public Const kmsgVoucherNoUsedBefore = 140027               ' The {0} number shown below has been used previously so it may not be used again.{1}:  {2}{3}:  {4}{5}:  {6}Please select another {7} number.
Public Const kmsgGetVoucherSeqNoErr = 140028                ' An error occurred while retrieving the next {0} Seq. No. to be used for this {1} batch:Batch Number:  {2}
Public Const kmsgPOTransferNotOpen = 220163                 ' The selected transfer order ({0}) does not have a Status of "Open".
Public Const kmsgPONoOpenTransferLines = 220164             ' The selected transfer order ({0}) has no lines open for receiving.
Public Const kmsgPOBadTransferRcvgWhse = 220165             ' The transfer order entered does not have the correct receiving warehouse.
Public Const kmsgPOTransfrNotValidForShipWh = 220166        ' The selected transfer ({0}) is not valid for the current shipping warehouse ({1}).
Public Const kmsgTransferLinesExist = 220167                ' You have changed the transfer order number. All detail lines associated with the old transfer order ({0}) will be removed from the voucher. Do you really want to do this?
Public Const kmsgPOClosePOOnFirstReceiptErr = 220169        ' The PO selected ({0}) is invalid because the PO Option "Close Entire PO On First Receipt" has been selected and a receipt already exists for this PO.
Public Const kmsgRGIncomplete = 220174                      ' This receipt exists in the log with a status of Incomplete another user may have created it.  Do you want to use it?
Public Const kMissingPhase = 260022                         ' Please select a phase.
Public Const kMissingProject = 260023                       ' Please select a project.
Public Const kMissingCostClass = 260036                     ' Please select a cost classification.
Public Const kmsgPALoseProject = 260083                     ' Changing the {0} will cause any lines associated with a Project to be disassociated from the Project.""You will then need to manually reassociate the lines or set them as variances.""Do you still wish to change the {0}?
Public Const kmsgPAInvDescProject = 260084                  ' Warning: The selected project is an Invoice Description project, and one""or more of the invoice lines does not have an invoice description.
Public Const kmsgPAReallyChangeProject = 260085             ' Changing the Project will cause all lines related to this project to be removed from the invoice.""Do you still wish to change the Project?
Public Const kmsgPAInvProjectInUse = 260086                 ' Another user is currently invoicing against this project.""Please select a different project.
Public Const kmsgPAProjectLockFailed = 260087               ' Unable to lock this project. Please try again later or contact your system administrator.
Public Const kmsgPAItemNotAllowed = 260102                  ' Only non-inventory items are allowed when project information is specified.""Please choose a new item.
Public Const kmsgPAInvoicesExist = 260103                   ' At least one unposted invoice exists for this project.""Are you sure you want to continue?
Public Const kmsgPANeedTaskFPP = 260104                     ' A task is required on fixed price+ projects without posted estimates.""Please enter a task.
Public Const kmsgDistIncompleteContinue = 166227            ' The transaction quantity does not match the quantity distributed. Do you wish to continue {0}?
Public Const KmsgPAProjClosedPO = 260245                    ' Line cannot be changed because project {0} is closed. Replace {0} with the Project control on the Project tab.
Public Const kmsgARNoRecalcOnExchRate = 153406              ' You have changed your currency or exchange rate. No amounts will automatically recalculate.
Public Const kmsgSaveBeforeAdd = 100246                     ' {0} must be saved before continuing.  Do you wish to save now?
Public Const kmsgPOLandCostAutoAdjAmtOnEdit = 220197        'Landed cost amounts will be automatically adjusted based on the changes made to the quantity, weight, and volume to the receipt lines. Landed cost amounts will not be adjusted if the cost is ov
Public Const kmsgPONoDecimalQty = 220199                    ' Item does not allow decimal quantities. The quantity {0} in UOM {1} will result in {2} {3}, the UOM of the source transaction.
