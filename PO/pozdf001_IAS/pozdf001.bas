Attribute VB_Name = "basEnterRcptOfGoods"
'***********************************************************************
'   Name:
'       basProcRcptOfGoods
'
'   Description:
'       This module contains code specific to the Process Receipt of
'       Invoice task.
'
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 05/07/97 JSP
'     Mods:
'***********************************************************************
Option Explicit

    '-- Public PO-specific variables
    Public mbAutoSelectLines    As Boolean
    Public mbDefaultQty         As Boolean
    Public miMatchLevel         As Integer
    Public mbInternalLinesClick As Boolean
    Public mvPOKey              As Variant
    Public mvTransferKey        As Variant
    Public mlOrigPOKey          As Long
    Public msOpenString         As String
    Public msClosedString       As String
    Public msSoftOvrdString     As String
    Public msHardOvrdString     As String
    Public msCreditString       As String
    Public msReplacementString  As String
    Public mbLineDeleted        As Boolean
    Public mbValidatingPO       As Boolean
    Public mbValidatingRcvr     As Boolean
    Public mbValidatingTrnsfr   As Boolean
    Public mbManualClick        As Boolean
    
    '-- Put Away Bin variables
    Public msPrevReceiveToBinID      As String
    Public mlPrevReceiveToBinKey   As Long
    
    '-- Private PO-specific variables
    Private mbInVendID          As Boolean
    Private mbInPurchCompany    As Boolean
    Private mbInShipWhseID      As Boolean
    
    '-- Public PO-specific constants
    Public Const kPONoLen = 10
    Public Const kPONoMaxLen = 15
    Public Const kMatchLevelNone = 0
    Public Const kMatchLevel2Way = 2
    Public Const kMatchLevel3Way = 3
    Public Const kMatchStatusOpen = 1
    Public Const kMatchStatusClosed = 2
    Public Const kMatchStatusSoftOvrd = 3
    Public Const kMatchStatusHardOvrd = 4
    Public Const kReturnNone = 0
    Public Const kReturnForCredit = 1
    Public Const kReturnForReplace = 2
    
    
    '-- Select PO Lines Grid column constants
    Public Const kcolPOSelect = 1
    Public Const kcolPOPOLineNo = 2
    Public Const kcolPOItemID = 3
    Public Const kcolPOQtyView = 4
    Public Const kcolPOUOM = 5
    Public Const kcolPORcptUOMID = 6
    Public Const kcolPOUnitCost = 7
    Public Const kcolPOItemDesc = 8
    Public Const kcolPOPONo = 9
    Public Const kcolPOPOKey = 10
    Public Const kcolPOPOLineKey = 11
    Public Const kcolPOPOLineDistKey = 12
    Public Const kcolPOGLAcctKey = 13
    Public Const kcolPOAcctRefKey = 14
    Public Const kcolPOItemKey = 15
    Public Const kcolPOUnitMeasKey = 16
    Public Const kcolPOUnitMeasType = 17
    Public Const kcolPOLineClosed = 18
    Public Const kcolPOTargetCoID = 19
    Public Const kcolPORcptReq = 20
    Public Const kcolPOQtyOrd = 21
    Public Const kcolPOQtyRcvd = 22
    Public Const kcolPOQtyRtrnForCredit = 23
    Public Const kcolPOQtyOpen = 24
    Public Const kcolPOQty = 25
    Public Const kcolPOWhseID = 26
    Public Const kcolPODeptID = 27
    Public Const kcolPOPromiseDate = 28
    Public Const kcolPORcptKey = 29
    Public Const kcolPORcptLineKey = 30
    Public Const kcolPORcptLineDistKey = 31
    Public Const kcolPORcptUOMKey = 32
    Public Const kcolPORcptOrigQty = 33
    Public Const kcolPOItemAllowDec = 34
    Public Const kcolPOLineComment = 35
    Public Const kColPOProjectID = 36
    Public Const kColPOProjectKey = 37
    Public Const kColPOPhaseID = 38
    Public Const kColPOPhaseKey = 39
    Public Const kColPOTaskID = 40
    Public Const kColPOTaskKey = 41
    Public Const kColPOJobLineKey = 42
    Public Const kColPOJobType = 43
    Public Const kColPOTaskType = 44
    Public Const kcolPOUnitCostExact = 45
    Public Const kPOGridMaxCols = 45
    
    '-- PO Number validation return values
    Public Enum iPOValidVals
        kPOValid = -1
        kErrPONotExist = 1
        kErrPONotOpen = 2
        kErrPONotStandard = 3
        kErrPOAllLinesClosed = 4
        kErrPOBadVendor = 5
        kErrPOOnHold = 6
        kErrPOBlankVendor = 7
        kErrClosePOonFirstRcpt = 8
    End Enum
    
    Type uPODfltsType
        sPONo                   As String
        lPOKey                  As Long
        bPONumberChanged        As Boolean
        sVendID                 As String
        lVendKey                As Long
        sCurrID                 As String
        dCurrExchRate           As Double
        lCurrExchSchdKey        As Long
        iFixedRate              As Integer
        bReturnedVendor         As Boolean
    End Type
    Public muPODflts            As uPODfltsType
    
    '-- Transfer Number validation return values
    Public Enum iTransferValidVals
        kTransferValid = -1
        kErrTransferNotExist = 1
        kErrTransferNotOpen = 2
        kErrTransferNoLinesOpen = 3
        kErrTransferBadRcvgWhse = 4
        kErrTransferBadShipWhse = 5
        kErrTransferBlankShipWhse = 6
    End Enum
    
    Type uTransferDfltsType
        sTranNo                 As String
        lTrnsfrOrderKey         As Long
        bTransferNumberChanged  As Boolean
        sShipWhseID             As String
        lShipWhseKey            As Long
        sTransitWhseID          As String
        bReturnedShipWhse       As Boolean
    End Type
    Public muTransferDflts        As uTransferDfltsType
    
    Type uRcvrDfltsType
        sRcvrNo                   As String
        lRcvrKey                  As Long
        bRcvrNumberChanged        As Boolean
        sVendID                 As String
        lVendKey                As Long
        bReturnedVendor         As Boolean
        lPOKey                  As Long
        sPOTranNo               As String
        sPOTranNoRel            As String
        sCurrID                 As String
        dCurrExchRate           As Double
    End Type
    Public muRcvrDflts            As uRcvrDfltsType
        
    Type uPOLineOldVals
        lPOLineKey              As Long
        lUnitMeasKey            As Long
        dQty                    As Double
        dUnitCost               As Double
    End Type
    Public muPOLineOldVals      As uPOLineOldVals
    
    
Public Const ktskProcRcptOfGoods = 184943042              'PO Enter Receipts Data Entry
Public Const kclsProcRcptOfGoods = "pozdfdl1.clsEnterRcptOfGoodsDTE"

Public Const kTypeReceipt = 1
Public Const kTypeReturn = 2

Const VBRIG_MODULE_ID_STRING = "POZDB001.BAS"
'
    
Public Function iGetPostDateYearPO(oClass As Object, lBatchKey As Long, sPostDate As String) As Integer
'******************************************************************
'    Desc: This function will retrieve and return the posting
'          date's year for the current PO receipt of invoice batch.
' Returns: The PostDate Year.
'******************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim rs As Object
    Dim iPostDateYear As Integer

    iGetPostDateYearPO = -1
    
    '-- Get the Post Date for this Receiver batch
    sSQL = "SELECT PostDate FROM tpoBatch WITH (NOLOCK) "
    sSQL = sSQL & "WHERE BatchKey = " & lBatchKey
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not rs.IsEmpty Then
        sPostDate = Trim$(gsGetValidStr(rs.Field("PostDate")))
        
        '-- Check to see if the Post Date has a value
        If Len(sPostDate) = 0 Then
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateNull
            GoTo ExpectedErrorRoutine
        Else
            iPostDateYear = Year(sPostDate)
            If (iPostDateYear <= 0) Then
                giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateError
                GoTo ExpectedErrorRoutine
            End If
        End If
    Else
        '-- The corresponding batch row was not found
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchNotFoundError, Format$(lBatchKey)
        GoTo ExpectedErrorRoutine
    End If

    rs.Close: Set rs = Nothing

    '-- Return the value
    iGetPostDateYearPO = iPostDateYear
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
    rs.Close: Set rs = Nothing
    sPostDate = ""
    iGetPostDateYearPO = -1
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetPostDateYearPO", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bIsValidReceiver(frm As Form, oDMGrid As clsDmGrid, _
                             sCompanyID As String, _
                             oLEGrid As clsLineEntry, _
                             Optional bFromVendID As Boolean = False, _
                             Optional sMessage As String = "", _
                             Optional bCompanyChanged As Boolean = False) As Boolean
'************************************************************
'    Desc: This function will validate the entered Receipt number.
' Returns: True if the Receipt number is valid, else false.
'************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
    Dim sRcvrNo As String
    Dim sOldRcvrNo As String
    Dim iRetVal As iPOValidVals
    Dim iTempRetVal As iPOValidVals
    Dim iReply As Integer
    Dim bLinesExist As Boolean
    Dim bRetMsg As Boolean
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim oClass As Object
    Dim sString As String
    Dim sPurchCompanyID As String

    mbValidatingRcvr = True
    
    Set oClass = frm.oClass
        
    If (sMessage = kDisplayMsg) Then
        bRetMsg = False
    Else
        bRetMsg = True
    End If
    
    bValid = False
    
    '-- Right justify, zero fill PO number if all numeric
    sOldRcvrNo = Trim$(frm.lkuReceiver.Tag)
    sRcvrNo = frm.lkuReceiver
    
    If (((sRcvrNo <> "") And (sRcvrNo <> sOldRcvrNo)) Or (bFromVendID)) And Not bCompanyChanged Then
        With oClass.moAppDB
            .SetInParam sCompanyID
            .SetInParam sRcvrNo
            .SetInParam frm.lkuVendID.Text
            
            .SetOutParam muRcvrDflts.lRcvrKey
            .SetOutParam muRcvrDflts.sVendID
            .SetOutParam muRcvrDflts.lVendKey
            .SetOutParam muRcvrDflts.lPOKey
            .SetOutParam muRcvrDflts.sPOTranNo
            .SetOutParam muRcvrDflts.sPOTranNoRel
            .SetOutParam sPurchCompanyID
            .SetOutParam muRcvrDflts.sCurrID
            .SetOutParam muRcvrDflts.dCurrExchRate
           .SetOutParam iRetVal
            
            .ExecuteSP "sppoValRcvrForReturn"
         
            muRcvrDflts.lRcvrKey = glGetValidLong(.GetOutParam(4))
            muRcvrDflts.sVendID = gsGetValidStr(.GetOutParam(5))
            muRcvrDflts.lVendKey = glGetValidLong(.GetOutParam(6))
            muRcvrDflts.lPOKey = glGetValidLong(.GetOutParam(7))
            muRcvrDflts.sPOTranNo = gsGetValidStr(.GetOutParam(8))
            muRcvrDflts.sPOTranNoRel = gsGetValidStr(.GetOutParam(9))
            sPurchCompanyID = gsGetValidStr(.GetOutParam(10))
            muRcvrDflts.sCurrID = gsGetValidStr(.GetOutParam(11))
            muRcvrDflts.dCurrExchRate = gdGetValidDbl(.GetOutParam(12))
            iRetVal = .GetOutParam(13)
            .ReleaseParams
        End With
        If (bFromVendID And (frm.lkuVendID = "")) Or bCompanyChanged Then
            iRetVal = kErrPOBadVendor
        End If
        With oClass
            Select Case iRetVal
                Case kErrPONotExist
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgInvalidField, .moSysSession.Language, .moAppDB, frm.lblReceiver)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgInvalidField, frm.lblReceiver
                    End If

                Case kErrPONotOpen
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONotOpen, .moSysSession.Language, .moAppDB, sRcvrNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONotOpen, sRcvrNo
                    End If

                Case kErrPONotStandard
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONotStandard, .moSysSession.Language, .moAppDB, sRcvrNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONotStandard, sRcvrNo
                    End If

                Case kErrPOAllLinesClosed
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPOAllLinesClosed, .moSysSession.Language, .moAppDB, sRcvrNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOAllLinesClosed, sRcvrNo
                    End If

                Case kErrPOBadVendor
                    If bFromVendID Then
                        '-- There was an existing PO number and the vendor was changed,
                        '-- making the PO invalid. Therefore, blank out the PO number.
                        sString = .moAppDB.DASBuildLocalString(kPurchOrdKeyCol, .moSysSession.Language, "")
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgExistingPONotValid, .moSysSession.Language, .moAppDB, sString, sRcvrNo, frm.lkuVendID)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgExistingPONotValid, sString, sRcvrNo, frm.lkuVendID
                        End If
                        frm.lkuReceiver.Tag = frm.lkuReceiver
                        frm.lkuReceiver = ""
                        mbInVendID = True
                        bIsValidReceiver frm, oDMGrid, sCompanyID, oLEGrid
                        mbInVendID = False
                        frm.lkuReceiver_PostLostFocus
                        frm.lkuReceiver.Tag = ""
                    Else
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgPONotValidForVendor, .moSysSession.Language, .moAppDB, sRcvrNo, frm.lkuVendID)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgPONotValidForVendor, sRcvrNo, frm.lkuVendID
                        End If
                    End If

                Case kErrPOBlankVendor
                    iRetVal = kPOValid

            End Select
        End With

        '-- The PO is valid
        If (iRetVal = kPOValid) Then
            frm.sPurchCompanyID = sPurchCompanyID
            frmEnterRcptOfGoods.moDmHeader.SetColumnValue "PurchCompanyID", sPurchCompanyID
            frm.lkuReceiver.Tag = frm.lkuReceiver
            '-- Check if the PO number has changed and there are existing
            '-- lines for the PO in the Receiver detail grid. If so, warn the user.
            If bChangedPOOK(frm, sRcvrNo, sOldRcvrNo, bLinesExist) Then
                bValid = True

                frm.lkuReceiver = sRcvrNo
                mvPOKey = muRcvrDflts.lRcvrKey

                muRcvrDflts.bRcvrNumberChanged = True

                If (Trim$(frm.lkuVendID.Text) = "") And (Trim$(muRcvrDflts.sVendID) <> "") Then
                    muRcvrDflts.bReturnedVendor = True
                End If

                If muRcvrDflts.bReturnedVendor Then
                    '-- The Vendor was returned from the PO,
                    '-- so set the Vendor info on the form
                    frm.lkuVendID = muRcvrDflts.sVendID
                    frm.bIsValidVendID ""
                End If

                '-- Since the PO Number has changed, and the user has already
                '-- agreed to have all associated detail lines removed from
                '-- the Receiver detail grid, do that here
                If bLinesExist Then
                    DeleteOldPOLinesFromGrid frm, oDMGrid, oLEGrid, sOldRcvrNo
                End If

                '-- Automatically show the Select PO Lines dialog, or
                '-- automatically load the receiver detail grid for the
                '-- PO number, depending on options
                frm.cmdLines.Enabled = True
                If mbAutoSelectLines Then
                    mbInternalLinesClick = True
                Else
                    mbInternalLinesClick = False
                End If
                ' set the PO value here so it can show up on the selection form
                frmEnterRcptOfGoods.moDmHeader.SetColumnValue "POKey", muRcvrDflts.lPOKey
                frmEnterRcptOfGoods.txtPONum.Text = muRcvrDflts.sPOTranNo
                frmEnterRcptOfGoods.txtRelNo.Text = Mid(muRcvrDflts.sPOTranNoRel, 12, 4)
                '-- Automatically show the Select PO Lines dialog
                frm.cmdLines_Click
            End If
        End If
    ElseIf (sRcvrNo = "") And (sOldRcvrNo <> "") Then
        '-- PO number was blanked out; need to remove any associated lines
        '-- from the receiver detail grid
        If bChangedPOOK(frm, sRcvrNo, sOldRcvrNo, bLinesExist) Then
            bValid = True
            mvPOKey = ""
            '-- Since the PO Number has changed, and the user has already
            '-- agreed to have all associated detail lines removed from
            '-- the receiver detail grid, do that here
            If bLinesExist Then
                DeleteOldPOLinesFromGrid frm, oDMGrid, oLEGrid, sOldRcvrNo
            End If
            
            'Clear information about the PO so it does not get displayed.
            With muRcvrDflts
                .lPOKey = 0
                .sPOTranNo = ""
                .sPOTranNoRel = ""
            End With
        End If
        frm.lkuReceiver.Tag = frm.lkuReceiver
    ElseIf (sRcvrNo = "") Then
        mvPOKey = ""
        bValid = True
    Else
        bValid = True
    End If

    muRcvrDflts.sRcvrNo = sRcvrNo

    If bValid Then
        frmEnterRcptOfGoods.moDmHeader.SetColumnValue "POKey", muRcvrDflts.lPOKey
        frmEnterRcptOfGoods.txtPONum.Text = muRcvrDflts.sPOTranNo
        frmEnterRcptOfGoods.txtRelNo.Text = Mid(muRcvrDflts.sPOTranNoRel, 12, 4)
        frmEnterRcptOfGoods.moDmHeader.SetColumnValue "CurrID", muRcvrDflts.sCurrID
        frmEnterRcptOfGoods.moDmHeader.SetColumnValue "CurrExchRate", muRcvrDflts.dCurrExchRate
    End If

    bIsValidReceiver = bValid

    Set oClass = Nothing

    mbValidatingRcvr = False
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidReceiver", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bIsValidPONo(frm As Form, oDMGrid As clsDmGrid, _
                             oLEGrid As clsLineEntry, _
                             sHomeCurrID As String, _
                             Optional bFromVendID As Boolean = False, _
                             Optional sMessage As String = "", _
                             Optional bCompanyChanged As Boolean = False, _
                             Optional iClosePOonFirstRcpt As Integer = 0) As Boolean
'************************************************************
'    Desc: This function will validate the entered PO number.
' Returns: True if the PO number is valid, else false.
'************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bValid As Boolean
    Dim sCompanyID As String
    Dim sPONo As String
    Dim sOldPONo As String
    Dim iRetVal As iPOValidVals
    Dim bLinesExist As Boolean
    Dim bRetMsg As Boolean
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim oClass As Object
    Dim sString As String
    
    mbValidatingPO = True
    
    Set oClass = frm.oClass
        
    If (sMessage = kDisplayMsg) Then
        bRetMsg = False
    Else
        bRetMsg = True
    End If
    
    bValid = False
    
   
    '-- Right justify, zero fill PO number if all numeric
    sOldPONo = Trim$(frm.lkuPONo.Tag)
'    sPONo = sZeroFillNumber(frm.lkuPONo, kPONoLen)
    sPONo = frm.lkuPONo
    sCompanyID = frm.ddnPurchCompany.Tag
    
    If (((sPONo <> "") And (sPONo <> sOldPONo)) Or (bFromVendID)) And Not bCompanyChanged Then
    
        With muPODflts
            .lVendKey = -1
            .bReturnedVendor = False
            .sCurrID = ""
            .dCurrExchRate = 0
            .lCurrExchSchdKey = 0
            .bPONumberChanged = False
        End With
        
        With oClass.moAppDB
            .SetInParam sCompanyID
            .SetInParam sPONo
            .SetInParam frm.lkuVendID.Text
            .SetInParam iClosePOonFirstRcpt
            
            .SetOutParam muPODflts.lPOKey
            .SetOutParam muPODflts.sVendID
            .SetOutParam muPODflts.lVendKey
            .SetOutParam muPODflts.sCurrID
            .SetOutParam muPODflts.dCurrExchRate
            .SetOutParam muPODflts.lCurrExchSchdKey
            .SetOutParam muPODflts.iFixedRate
            .SetOutParam iRetVal
            
            .ExecuteSP "sppoValPOforReceiver"
         
            muPODflts.lPOKey = glGetValidLong(.GetOutParam(5))
            muPODflts.sVendID = gsGetValidStr(.GetOutParam(6))
            muPODflts.lVendKey = glGetValidLong(.GetOutParam(7))
            muPODflts.sCurrID = gsGetValidStr(.GetOutParam(8))
            muPODflts.dCurrExchRate = gdGetValidDbl(.GetOutParam(9))
            muPODflts.lCurrExchSchdKey = glGetValidLong(.GetOutParam(10))
            muPODflts.iFixedRate = giGetValidInt(.GetOutParam(11))
            iRetVal = .GetOutParam(12)
            .ReleaseParams
        End With
        If (bFromVendID And (frm.lkuVendID = "")) Or bCompanyChanged Then
            iRetVal = kErrPOBadVendor
        End If
        With oClass
            Select Case iRetVal
                Case kErrPONotExist
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgInvalidField, .moSysSession.Language, .moAppDB, frm.lblPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgInvalidField, frm.lblPONo
                    End If

                Case kErrPONotOpen
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONotOpen, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONotOpen, sPONo
                    End If

                Case kErrPONotStandard
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONotStandard, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONotStandard, sPONo
                    End If

                Case kErrPOAllLinesClosed
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPOAllLinesClosed, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOAllLinesClosed, sPONo
                    End If

                Case kErrPOBadVendor
                    If bFromVendID Then
                        '-- There was an existing PO number and the vendor was changed,
                        '-- making the PO invalid. Therefore, blank out the PO number.
                        sString = .moAppDB.DASBuildLocalString(kPurchOrdKeyCol, .moSysSession.Language, "")
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgExistingPONotValid, .moSysSession.Language, .moAppDB, sString, sPONo, frm.lkuVendID)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgExistingPONotValid, sString, sPONo, frm.lkuVendID
                        End If
                        frm.lkuPONo.Tag = frm.lkuPONo
                        frm.lkuPONo = ""
                        mbInVendID = True
                        bIsValidPONo frm, oDMGrid, oLEGrid, sHomeCurrID
                        mbInVendID = False
                        frm.lkuPONo_PostLostFocus
                        frm.lkuPONo.Tag = ""
                    Else
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgPONotValidForVendor, .moSysSession.Language, .moAppDB, sPONo, frm.lkuVendID)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgPONotValidForVendor, sPONo, frm.lkuVendID
                        End If
                    End If

                Case kErrPOOnHold

                    If giSotaMsgBox(Nothing, .moSysSession, kmsgPOOnHold, sPONo) = kretYes Then
                        '-- Security event is needed to receive an on-hold PO
                        sID = "RCVHOLDPO"
                        sUser = CStr(oClass.moSysSession.UserId)
                        vPrompt = True
                        If (.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) <> 0) Then
                            iRetVal = kPOValid
                        End If
                    End If

                Case kErrPOBlankVendor
                    iRetVal = kPOValid

                Case kErrClosePOonFirstRcpt
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPOClosePOOnFirstReceiptErr, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOClosePOOnFirstReceiptErr, sPONo
                    End If

            End Select
        End With
            
        '-- The PO is valid
        If (iRetVal = kPOValid) Then
                
            frm.lkuVendID.Enabled = False
            frm.ddnPurchCompany.Enabled = False
                
            frm.lkuPONo.Tag = frm.lkuPONo.Text
            
           
            '-- Check if the PO number has changed and there are existing
            '-- lines for the PO in the Receiver detail grid. If so, warn the user.
            If bChangedPOOK(frm, sPONo, sOldPONo, bLinesExist) Then
                bValid = True
                
                frm.lkuPONo = sPONo
                mvPOKey = muPODflts.lPOKey
                
                If Not frm.bInRcptNoValidation Then
                    muPODflts.bPONumberChanged = True
                End If
                    
                If (Trim$(frm.lkuVendID.Text) = "") And (Trim$(muPODflts.sVendID) <> "") Then
                    muPODflts.bReturnedVendor = True
                End If
                
                If muPODflts.bReturnedVendor Then
                    '-- The Vendor was returned from the PO,
                    '-- so set the Vendor info on the form
                    frm.lkuVendID = muPODflts.sVendID
                    If Not frm.bIsValidVendID Then
                        frm.lkuVendID.SetTextAndKeyValue "", 0
                        bIsValidPONo = bValid
                        Set oClass = Nothing
                        mbValidatingPO = False
                        Exit Function
                    End If

                End If
                    
                '-- Since the PO Number has changed, and the user has already
                '-- agreed to have all associated detail lines removed from
                '-- the Receiver detail grid, do that here
                If Not frm.bInRcptNoValidation Then
                    If bLinesExist Then
                        DeleteOldPOLinesFromGrid frm, oDMGrid, oLEGrid, sOldPONo
                    End If
                
                
                    '-- Automatically show the Select PO Lines dialog, or
                    '-- automatically load the receiver detail grid for the
                    '-- PO number, depending on options
                    frm.cmdLines.Enabled = True
                    If mbAutoSelectLines Then
                        mbInternalLinesClick = True
                    Else
                        mbInternalLinesClick = False
                    End If
                End If
                
                'If the exchange rate on the PO isn't fixed, get the current exchange rate
                If muPODflts.iFixedRate <> 1 Then
                    muPODflts.dCurrExchRate = gdGetExchangeRate(oClass.moAppDB, sHomeCurrID, sCompanyID, _
                        muPODflts.sCurrID, sHomeCurrID, frm.calRcptDate.Text, kExchangeRateBuy, _
                        muPODflts.lCurrExchSchdKey)
                End If
                
                If Not frm.bInRcptNoValidation Then
                    '-- Automatically show the Select PO Lines dialog
                    frm.cmdLines_Click
                End If
               
                                
                frm.moDmHeader.SetColumnValue "CurrID", muPODflts.sCurrID
                frm.moDmHeader.SetColumnValue "CurrExchRate", muPODflts.dCurrExchRate
            End If
        End If
    ElseIf (sPONo = "") And (sOldPONo <> "") Then
        frm.lkuPONo.Tag = ""

        '-- PO number was blanked out; need to remove any associated lines
        '-- from the receiver detail grid
        If bChangedPOOK(frm, sPONo, sOldPONo, bLinesExist) Then
            bValid = True
            mvPOKey = ""
            
            '-- Since the PO Number has changed, and the user has already
            '-- agreed to have all associated detail lines removed from
            '-- the receiver detail grid, do that here
            If bLinesExist Then
                DeleteOldPOLinesFromGrid frm, oDMGrid, oLEGrid, sOldPONo
            End If
        End If
    ElseIf (sPONo = "") Then
        mvPOKey = ""
        bValid = True
    Else
        bValid = True
    End If
    
    '-- If PONo is now blank, reset the fields...
    If sPONo = "" Then
        frm.lkuVendID.Enabled = True
        frm.ddnPurchCompany.Enabled = True
    End If
    
    muPODflts.sPONo = sPONo
    
    bIsValidPONo = bValid
    
    Set oClass = Nothing
    
    mbValidatingPO = False
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidPONo", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Function bIsValidReceiveToBin(frm As Form) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lRow As Long
    
    bIsValidReceiveToBin = False
    
    If frm.lkuReceiveToBin.IsValid = True Then
        'Set the current id and key value to the previous variables.
        mlPrevReceiveToBinKey = glGetValidLong(frm.lkuReceiveToBin.KeyValue)
        msPrevReceiveToBinID = gsGetValidStr(frm.lkuReceiveToBin.Text)
        'Uncheck the header preferred bin checkbox.
        frm.chkPreferredBin.Value = vbUnchecked
    Else
        mlPrevReceiveToBinKey = 0
        msPrevReceiveToBinID = ""
    End If
    
    bIsValidReceiveToBin = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidReceiveToBin", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function bIsValidTransfer(frm As Form, oDMGrid As clsDmGrid, _
                             oLEGrid As clsLineEntry, _
                             sHomeCurrID As String, _
                             Optional bFromShipWhseID As Boolean = False, _
                             Optional sMessage As String = "", _
                             Optional bCompanyChanged As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************
'    Desc: This function will validate the entered Transfer number.
' Returns: True if the Transfer number is valid, else false.
'************************************************************
    Dim bValid As Boolean
    Dim sCompanyID As String
    Dim sTranNo As String
    Dim sOldTranNo As String
    Dim iRetVal As Long
    Dim bLinesExist As Boolean
    Dim bRetMsg As Boolean
    Dim oClass As Object
    Dim sString As String
    Dim lRcvgWhseKey As Long
    Dim sShipWhseDesc As String

    mbValidatingTrnsfr = True
    frm.mbCancelTransferBeforeNavReturn = False
    
    Set oClass = frm.oClass
        
    If (sMessage = kDisplayMsg) Then
        bRetMsg = False
    Else
        bRetMsg = True
    End If
    
    bValid = False
    
    With muTransferDflts
        .lShipWhseKey = -1
        .bReturnedShipWhse = False
        .bTransferNumberChanged = False
    End With
    
    sOldTranNo = Trim$(frm.lkuTransfer.Tag)
    sTranNo = frm.lkuTransfer
    sCompanyID = oClass.moSysSession.CompanyId
    lRcvgWhseKey = oClass.mlWhseKey
    
    If (((sTranNo <> "") And (sTranNo <> sOldTranNo)) Or (bFromShipWhseID)) And Not bCompanyChanged Then
        With oClass.moAppDB
            .SetInParam sCompanyID
            .SetInParam sTranNo
            .SetInParam lRcvgWhseKey
            .SetInParam frm.lkuShipWhse.Text
            
            .SetOutParam muTransferDflts.lTrnsfrOrderKey
            .SetOutParam muTransferDflts.sShipWhseID
            .SetOutParam muTransferDflts.lShipWhseKey
            .SetOutParam sShipWhseDesc
            .SetOutParam muTransferDflts.sTransitWhseID
            .SetOutParam iRetVal
            
            .ExecuteSP "sppoValTransferForReceiver"
         
            muTransferDflts.lTrnsfrOrderKey = glGetValidLong(.GetOutParam(5))
            muTransferDflts.sShipWhseID = gsGetValidStr(.GetOutParam(6))
            muTransferDflts.lShipWhseKey = glGetValidLong(.GetOutParam(7))
            sShipWhseDesc = gsGetValidStr(.GetOutParam(8))
            muTransferDflts.sTransitWhseID = gsGetValidStr(.GetOutParam(9))
            iRetVal = .GetOutParam(10)
            .ReleaseParams
        End With
        
        If (bFromShipWhseID And (frm.lkuShipWhse = "")) Or bCompanyChanged Then
            iRetVal = kErrPOBadVendor
        End If
        With oClass
            Select Case iRetVal
                Case iTransferValidVals.kErrTransferNotExist
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgInvalidField, .moSysSession.Language, .moAppDB, frm.Transfer)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgInvalidField, frm.lblTransfer
                    End If
                    
                Case iTransferValidVals.kErrTransferNotOpen
                    If bRetMsg Then
                        'The selected transfer order ({0}) does not have a Status of "Open".
                        sMessage = gsBuildMessage(kmsgPOTransferNotOpen, .moSysSession.Language, .moAppDB, sTranNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOTransferNotOpen, sTranNo
                    End If
                    
                Case iTransferValidVals.kErrTransferNoLinesOpen
                    If bRetMsg Then
                        'The selected transfer order ({0}) has no lines open for receiving.
                        sMessage = gsBuildMessage(kmsgPONoOpenTransferLines, .moSysSession.Language, .moAppDB, sTranNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONoOpenTransferLines, sTranNo
                    End If
                    
                Case iTransferValidVals.kErrTransferBadRcvgWhse
                    If bRetMsg Then
                        'The transfer order entered does not have the correct receiving warehouse.
                        sMessage = gsBuildMessage(kmsgPOBadTransferRcvgWhse, .moSysSession.Language, .moAppDB, sTranNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOBadTransferRcvgWhse, sTranNo
                    End If
                                    
                Case iTransferValidVals.kErrTransferBadShipWhse
                    If bFromShipWhseID Then
                        '-- There was an existing Transfer number and the Ship Whse was changed,
                        '-- making the Transfer invalid. Therefore, blank out the Transfer number.
                        sString = "Transfer"
                        If bRetMsg Then
                            'TO DO: new local message
                            sMessage = gsBuildMessage(kmsgExistingPONotValid, .moSysSession.Language, .moAppDB, sString, sTranNo, frm.lkuShipWhse)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgExistingPONotValid, sString, sTranNo, frm.lkuShipWhse
                        End If
                        frm.lkuTransfer.Tag = frm.lkuTransfer
                        frm.lkuTransfer = ""
                        mbInShipWhseID = True
                        bIsValidTransfer frm, oDMGrid, oLEGrid, sHomeCurrID
                        mbInShipWhseID = False
                        'frm.lkuTransfer_PostLostFocus
                        frm.lkuTransfer.Tag = ""
                    Else
                        If bRetMsg Then
                            'The selected transfer ({0}) is not valid for the current shipping warehouse ({1}).
                            sMessage = gsBuildMessage(kmsgPOTransfrNotValidForShipWh, .moSysSession.Language, .moAppDB, sTranNo, frm.lkuShipWhse)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgPOTransfrNotValidForShipWh, sTranNo, frm.lkuShipWhse
                        End If
                    End If
                                
            End Select
        End With
            
        '-- The Transfer is valid
        If (iRetVal = kTransferValid) Then
        
            frm.lkuShipWhse.Enabled = False
            
            frm.lkuTransfer.Tag = frm.lkuTransfer.Text
            '-- Check if the PO number has changed and there are existing
            '-- lines for the PO in the Receiver detail grid. If so, warn the user.
            If bChangedTransferOK(frm, sTranNo, sOldTranNo, bLinesExist) Then
                bValid = True
                
                frm.lkuTransfer = sTranNo
                mvTransferKey = muTransferDflts.lTrnsfrOrderKey
                
                If Not frm.bInRcptNoValidation Then
                    muTransferDflts.bTransferNumberChanged = True
                End If
                    
                If (Trim$(frm.lkuShipWhse.Text) = "") And (Trim$(muTransferDflts.sShipWhseID) <> "") Then
                    muTransferDflts.bReturnedShipWhse = True
                End If
                
                If muTransferDflts.bReturnedShipWhse Then
                    '-- The ShipWhse was returned from the Transfer,
                    '-- so set the ShipWhse info on the form
                    frm.lkuShipWhse = muTransferDflts.sShipWhseID
                    frm.txtShipWhseDesc = sShipWhseDesc
                    frm.bIsValidShipWhse ""
                End If
                frm.txtTransit = muTransferDflts.sTransitWhseID
                    
                '-- Since the Transfer Number has changed, and the user has already
                '-- agreed to have all associated detail lines removed from
                '-- the Receiver detail grid, do that here
                
                If Not frm.bInRcptNoValidation Then
                    If bLinesExist Then
                        DeleteOldPOLinesFromGrid frm, oDMGrid, oLEGrid, sOldTranNo
                    End If
                    frm.LoadTransferLinesInDetlGrid
                
                    '--If there are no lines created for this new receipt because there is no additional
                    '--stock to receive, ask the user if they wish to cancel this receipt.
                    If glGridGetDataRowCnt(frm.grdRcptDetl) = 0 Then
                        Dim iReply As Integer
                        iReply = giSotaMsgBox(Nothing, frm.moClass.moSysSession, kmsgPOTrnsfrNoStockToReceive, frm.lkuTransfer.Text)
                        If iReply = kretYes Then
                            frm.HandleToolbarClick kTbCancel
                            frm.mbCancelTransferBeforeNavReturn = True
                        End If
                    End If
                
                
                End If
            Else
                'Restore the Old TranNo
                bValid = True
                frm.SOTAValidationMgr1.StopValidation
                frm.lkuTransfer.Text = sOldTranNo
                frm.lkuTransfer.Tag = sOldTranNo
                frm.SOTAValidationMgr1.StartValidation
                frm.mbCancelTransferBeforeNavReturn = True
            End If
        End If
    ElseIf (sTranNo = "") And (sOldTranNo <> "") Then
        frm.lkuTransfer.Tag = ""

        '-- Transfer number was blanked out; need to remove any associated lines
        '-- from the receiver detail grid
        If bChangedTransferOK(frm, sTranNo, sOldTranNo, bLinesExist) Then
            bValid = True
            mvTransferKey = ""
            
            '-- Since the Transfer Number has changed, and the user has already
            '-- agreed to have all associated detail lines removed from
            '-- the receiver detail grid, do that here
            If bLinesExist Then
                DeleteOldPOLinesFromGrid frm, oDMGrid, oLEGrid, sOldTranNo
            End If
        End If
    ElseIf (sTranNo = "") Then
        mvTransferKey = ""
        bValid = True
    Else
        bValid = True
    End If
    
    'Currency ID and exchange rate aren't used for transfers, but need
    'to populate required fields
    If bValid Then
        frm.moDmHeader.SetColumnValue "CurrID", sHomeCurrID
        frm.moDmHeader.SetColumnValue "CurrExchRate", 1
    End If
    
    '-- If Transfer is now blank, reset the fields
    If sTranNo = "" Then
        frm.lkuShipWhse.Enabled = True
        frm.txtTransit = ""
    End If
    
    muTransferDflts.sTranNo = sTranNo
    
    bIsValidTransfer = bValid
    
    If ((Not bValid) And bRetMsg) Then
        frm.lkuTransfer.Text = frm.lkuTransfer.Tag
    End If
    
    Set oClass = Nothing
    
    mbValidatingTrnsfr = False
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "basEnterRcptOfGoods", "bIsValidTransfer", VBRIG_IS_MODULE       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Function bChangedPOOK(frm As Form, sPONo As String, _
                              sOldPONo As String, bLinesExist As Boolean) As Boolean
'************************************************************
'    Desc: This function will determine if the PO Number has
'          changed. If it has, the receiver detail grid will
'          be checked to see if any lines are associated with
'          the old PO number. If there are lines associated,
'          warn the user and allow them to continue or not.
' Returns: True if the PO number is hasn't changed or the
'          user wants to remove associated lines, else false.
'************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iReply As Integer
    Dim lRow As Long
    Dim sGridPO As String
    
    bChangedPOOK = True
    bLinesExist = False
    
        '-- Check the Receipt detail grid to see if any lines are
        '-- associated with the old PO number
    If (sPONo <> sOldPONo) And (sOldPONo <> "") And frm.grdRcptDetl.DataRowCnt > 0 Then
        If (Not mbInVendID) And (Not mbInPurchCompany) Then
            If (sPONo <> "") Then
                iReply = giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgPOLinesExist, sOldPONo)
                If iReply = kretNo Then bChangedPOOK = False
            End If
        End If
        bLinesExist = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bChangedPOOK", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bChangedTransferOK(frm As Form, sTranNo As String, _
                              sOldTranNo As String, bLinesExist As Boolean) As Boolean
'************************************************************
'    Desc: This function will determine if the Transfer Number has
'          changed. If it has, the receiver detail grid will
'          be checked to see if any lines are associated with
'          the old Transfer number. If there are lines associated,
'          warn the user and allow them to continue or not.
' Returns: True if the Transfer number is hasn't changed or the
'          user wants to remove associated lines, else false.
'************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iReply As Integer
    Dim lRow As Long
    Dim sGridPO As String
    
    bChangedTransferOK = True
    bLinesExist = False
    
        '-- Check the Receipt detail grid to see if any lines are
        '-- associated with the old Transfer number
    If (sTranNo <> sOldTranNo) And (sOldTranNo <> "") And frm.grdRcptDetl.DataRowCnt > 0 Then
        If (Not mbInShipWhseID) Then
            If (sTranNo <> "") Then
                'You have changed the transfer order number. All detail lines associated with the old transfer order ({0}) will be removed from the voucher. Do you really want to do this?
                iReply = giSotaMsgBox(Nothing, frm.oClass.moSysSession, kmsgTransferLinesExist, sOldTranNo)
                If iReply = kretNo Then bChangedTransferOK = False
            End If
        End If
        bLinesExist = True
    End If
    

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bChangedTransferOK", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Private Function bNewPOChosen(frm As Form, sPONo As String) As Boolean
'*****************************************************************
'    Desc: This function will determine if lines from a different
'          PO than the one chosen exist on the receiver.
'*****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sGridPO As String
    
    bNewPOChosen = False
    
    If (sPONo <> "") And frm.grdRcvrDetl.DataRowCnt > 0 Then
        bNewPOChosen = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bNewPOChosen", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub DeleteOldPOLinesFromGrid(frm As Form, oDMGrid As clsDmGrid, _
                                     oLEGrid As clsLineEntry, sOldPONo As String)
'**********************************************************
' Desc: This routine will delete all lines from the receiver
'       detail grid associated with the old PO number.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim lOrigMaxRows As Long
    Dim lRowsChecked As Long
    Dim sGridPO As String
        
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdRcptDetl, False
        
        lOrigMaxRows = .grdRcptDetl.DataRowCnt
        lRow = lOrigMaxRows
        lRowsChecked = 0
        
        Do While (lRowsChecked <= lOrigMaxRows) And (lRow > 0)
            mbManualClick = True
                
            '-- Set the row to be deleted to the current row
            oLEGrid.Grid_Click 1, lRow
            
            '-- Delete the row
            GridDeleteRow frm, oDMGrid, oLEGrid, lRow
         
            mbManualClick = False
            lRow = lRow - 1
            lRowsChecked = lRowsChecked + 1
        Loop
        
        '-- Turn redraw of the grid back on
        SetGridRedraw .grdRcptDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteOldPOLinesFromGrid", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub DeleteZeroQtyPOLinesFromGrid(frm As Form, oDMGrid As clsDmGrid, _
                                        oLEGrid As clsLineEntry)
'**********************************************************
' Desc: This routine will delete all lines from the receiver
'       detail grid associated with a PO number that have a
'       quantity of 0 and are not Comment Only lines.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim lOrigMaxRows As Long
    Dim lRowsChecked As Long
    Dim sPOLineKey As String
    Dim sTransferLineKey As String
    Dim dQty As Double
    Dim bCmntOnly As Boolean
    Dim lTranID As Long
    Dim lTranKey As Long
    
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdRcptDetl, False
    
        lOrigMaxRows = .grdRcptDetl.DataRowCnt
        lRow = lOrigMaxRows
        lRowsChecked = 0
        
        Do While (lRowsChecked <= lOrigMaxRows) And (lRow > 0)
            sPOLineKey = Trim$(gsGridReadCell(.grdRcptDetl, lRow, kColPOLineKey))
            sTransferLineKey = Trim$(gsGridReadCell(.grdRcptDetl, lRow, kColTrnsfrLineKey))
            dQty = gdGetValidDbl(gsGridReadCell(.grdRcptDetl, lRow, kColQtyRcvd))
            
            If ((sPOLineKey <> "") Or (sTransferLineKey <> "")) And (dQty = 0) Then
                mbManualClick = True
                
                '-- Set the row to be deleted to the current row
                oLEGrid.Grid_Click 1, lRow
                        
                frmEnterRcptOfGoods.DeleteIMRow (lRow)

                '-- Delete the row
                GridDeleteRow frm, oDMGrid, oLEGrid, lRow
            
                mbManualClick = False
            End If
            
            lRow = lRow - 1
            lRowsChecked = lRowsChecked + 1
        
            'Since a line has been deleted, set flag to allow the auto-adjustment of the landed cost transactions.
            frm.mbAdjToLandedCostAmtsRequired = True
        Loop

        '-- Turn redraw of the grid back on
        SetGridRedraw .grdRcptDetl, True
    End With
    
    If frm.grdRcptDetl.DataRowCnt >= 1 And gdGetValidDbl(gsGridReadCell(frm.grdRcptDetl, 1, kColQtyRcvd)) <> 0 Then
        'Do nothing
    Else
        'All lines were deleted because they were all change to a zero qty.
        frm.mbAllRcvrLinesWereOnceDeleted = True
    End If
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteZeroQtyPOLinesFromGrid", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub GridDeleteRow(frm As Form, oDMGrid As clsDmGrid, _
                         oLEGrid As clsLineEntry, lRow As Long)
'**********************************************************
' Desc: This routine will delete all lines from the receiver
'       detail grid associated with the old PO number. This
'       is a bastardized version of the GridDelete sub in
'       the line entry class.
'**********************************************************
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
    
    Dim oGrid As Object
    Dim bResponse As Boolean
    Dim lError As Long
    
    Set oGrid = frm.grdRcptDetl
    
    bResponse = True 'Assume it is OK to continue
    
    On Error Resume Next
    bResponse = frm.LEGridBeforeDelete(oLEGrid, lRow)
    lError = Err.Number
    On Error GoTo ExpectedErrorRoutine
    If lError <> 438 And lError <> 0 Then Err.Raise lError
    If bResponse = False Then Exit Sub
    
    If oDMGrid.DeleteBlock(lRow) = kDmSuccess Then
        On Error Resume Next
        frm.LEGridAfterDelete oLEGrid, lRow
        On Error GoTo ExpectedErrorRoutine
        lError = Err.Number
        If lError <> 438 And lError <> 0 Then Err.Raise lError
        oLEGrid.Grid_Click 1, lRow, True
    
        '-- Set global flag indicating that lines were deleted from
        '-- the detail grid, so tax and terms need to be recalculated
        mbLineDeleted = True
    End If
    
    Set oGrid = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GridDeleteRow", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ReopenPOLines(frm As Form, oDMGrid As clsDmGrid)
'**********************************************************
' Desc: This routine will set the MatchStatus to "Open" for
'       all receiver lines associated with a purchase order.
'       This will only be called if the currency exchange
'       rate is changed.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim iMatchStatus As Integer
    Dim sPOLineKey As String
        
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdRcptDetl, False
        
        For lRow = 1 To .grdRcptDetl.DataRowCnt
            sPOLineKey = Trim$(gsGridReadCell(.grdRcptDetl, lRow, kColPOLineKey))
            iMatchStatus = Val(gsGridReadCell(.grdRcptDetl, lRow, kColMatchStatus))
            
            If (sPOLineKey <> "") And _
               ((iMatchStatus = kMatchStatusClosed) Or (iMatchStatus = kMatchStatusSoftOvrd)) Then
                '-- Update the line's MatchStatus to "Open"
                gGridUpdateCell .grdRcptDetl, lRow, kColMatchStatus, kMatchStatusOpen
                gGridUpdateCellText .grdRcptDetl, lRow, kColDispMatchStatus, msOpenString
            
                '-- Need to inform DM of changed row
                oDMGrid.SetRowDirty lRow
            End If
        Next lRow

        '-- Turn redraw of the grid back on
        SetGridRedraw .grdRcptDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ReopenPOLines", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ClearReturnTypeLines(frm As Form, oDMGrid As clsDmGrid)
'**********************************************************
' Desc: This routine will clear the Return Type column for
'       all receiver lines associated with a purchase order.
'       This is called when the receiver Type is changed to
'       something other than "Credit Memo".
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sPOLineKey As String
        
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdRcptDetl, False
        
        For lRow = 1 To .grdRcptDetl.DataRowCnt
            sPOLineKey = Trim$(gsGridReadCell(.grdRcptDetl, lRow, kColPOLineKey))
            
            If (sPOLineKey <> "") Then
                '-- Update the line's ReturnType to "None"
                gGridUpdateCell .grdRcptDetl, lRow, kColReturnType, kReturnNone
                gGridUpdateCellText .grdRcptDetl, lRow, kColDispReturnType, ""
            
                '-- Need to inform DM of changed row
                oDMGrid.SetRowDirty lRow
            End If
        Next lRow

        '-- Turn redraw of the grid back on
        SetGridRedraw .grdRcptDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearReturnTypeLines", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub SetGridRedraw(grd As Object, bFlag As Boolean)
'*********************************************************
' Desc: This routine will use the Windows API and the grid
'       methods to turn redraw on/off. The Windows API
'       is used because it WORKS!, whereas the grid method
'       is flaky!
'*********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With grd
        SendMessage .hWnd, WM_SETREDRAW, bFlag, 0
        .redraw = bFlag
        If bFlag Then .Refresh
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridRedraw", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub CreatePONoLinkForGrid(oAppDB As Object, sCompanyID As String, _
                                 lRcvrKey As Long, lVRunMode As Long, _
                                 lTranType As Long)
'*******************************************************
' Desc: This routine will create a temp table containing
'       the PO number so that data manager can join with
'       the temp table to display the full PO number in
'       the receiver detail grid.
'*******************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL    As String
    Static bDid As Boolean

    If (Not bDid) Then
        On Error Resume Next
        sSQL = "DROP TABLE #tpoPOJoin"
        oAppDB.ExecuteSQL sSQL
        On Error GoTo VBRigErrorRoutine
        
        sSQL = "CREATE TABLE #tpoPOJoin "
        sSQL = sSQL & "(POLineKey INT      NULL, "
        sSQL = sSQL & "POLineNo   SMALLINT NULL, "
        sSQL = sSQL & "POItemKey  INT      NULL, "
        sSQL = sSQL & "POItemID   varCHAR(30)      NULL, "
        sSQL = sSQL & "POItemDesc VARCHAR(40) NULL, "
        sSQL = sSQL & "POQtyOrd   DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POUOMID    varCHAR(6) NULL, "
        sSQL = sSQL & "POQtyRcvd  DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POQtyRtrned  DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POQtyOpen  DECIMAL (16,8) NULL, "
        sSQL = sSQL & "POWhseID    varCHAR(6) NULL, "
        sSQL = sSQL & "PODeptID    varCHAR(15) NULL, "
        sSQL = sSQL & "POUOMType   INT NULL, "
        sSQL = sSQL & "POUOMKey    INT NULL,"
        sSQL = sSQL & "TrnsfrOrderLineKey INT      NULL, "
        sSQL = sSQL & "TRQtyShip  DECIMAL (16,8) NULL)"
        oAppDB.ExecuteSQL sSQL
        
        If lTranType = kTranTypePORTrn Then
            On Error Resume Next
            sSQL = "DROP TABLE #tpoRtrnJoin"
            oAppDB.ExecuteSQL sSQL
            On Error GoTo VBRigErrorRoutine
        
            sSQL = "CREATE TABLE #tpoRtrnJoin "
            sSQL = sSQL & "(RcvdLineKey INT      NULL, "
            sSQL = sSQL & "RcvdQty   DECIMAL (16,8) NULL,"
            sSQL = sSQL & "RtrnRcvrUnitMeasKey  INT      NULL)"
            oAppDB.ExecuteSQL sSQL
        End If

        bDid = True
    End If
    
    '-- Populate the temp table
    sSQL = "{call sppoGetRcvrDetl (" & gsQuoted(sCompanyID) & ", " & lRcvrKey & ")}"
    oAppDB.ExecuteSQL sSQL
    
    If lTranType = kTranTypePORTrn Then
        '-- Populate the Return temp table
        sSQL = "{call sppoGetRtrnDetl (" & gsQuoted(sCompanyID) & ", " & lRcvrKey & ")}"
        oAppDB.ExecuteSQL sSQL
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CreatePONoLinkForGrid", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Public Function bCreateReceiver(oClass As Object, oAppDB As Object, oDm As Object, _
                                lReceiverKey As Long) As Boolean
'*******************************************************
' Desc: This routine will create/update receiver records
'       for the just-saved receiver.
'*******************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer

    bCreateReceiver = False
    
    With oAppDB
        .SetInParam lReceiverKey
        .SetOutParam iRetVal
        
        .ExecuteSP "sppoCreateReceiver"
        
        iRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    If (iRetVal = 0) Then
        oDm.CancelAction
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOErrCreatingRcpt
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    bCreateReceiver = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCreateReceiver", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bDeleteReceiver(oClass As Object, oAppDB As Object, oDm As Object, _
                                lBatchKey As Long, lReceiverKey As Long, lTranType As Long) As Boolean
'********************************************************
' Desc: This routine will delete receiver records for the
'       just-deleted receiver.
'********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer

    bDeleteReceiver = False
    
    With oAppDB
        .SetInParam lBatchKey
        .SetInParam lReceiverKey
        .SetInParam lTranType
        .SetOutParam iRetVal
        
        .ExecuteSP "sppoDeleteReceiver"
        
        iRetVal = .GetOutParam(4)
        .ReleaseParams
    End With
    
    If (iRetVal = 0) Then
        oDm.CancelAction
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOErrDeletingRcpt
        Exit Function
    End If

    bDeleteReceiver = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteReceiver", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bDeleteRcvrLine(oClass As Object, oAppDB As Object, oDm As Object, _
                                lRcvrLineKey As Long) As Boolean
'********************************************************
' Desc: This routine will delete receiver line records
'       for the just-deleted receiver line.
'********************************************************
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    
    bDeleteRcvrLine = False
    
    sSQL = "DELETE tpoRcvrLineDist WHERE RcvrLineKey = " & lRcvrLineKey
    oAppDB.ExecuteSQL sSQL
    
    sSQL = "DELETE tpoRcvrLine WHERE RcvrLineKey = " & lRcvrLineKey
    oAppDB.ExecuteSQL sSQL
    
    bDeleteRcvrLine = True

'+++ VB/Rig Begin Pop +++
        Exit Function

ExpectedErrorRoutine:
    oDm.CancelAction
    giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOErrDeletingRcpt
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteRcvrLine", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bCreatePOVouchLink(oClass As Object, oAppDB As Object, oDm As Object, _
                                   lReceiverKey As Long) As Boolean
'*******************************************************
' Desc: This routine will create/update PO-Receiver link
'       records for the just-saved receiver.
'*******************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRetVal As Integer

    bCreatePOVouchLink = False
    
    With oAppDB
        .SetInParam lReceiverKey
        .SetOutParam iRetVal
        
        .ExecuteSP "sppoCreatePOVchLnk"
        
        iRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    If (iRetVal = 0) Then
        oDm.CancelAction
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOErrCreatingPOVchLink
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    bCreatePOVouchLink = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCreatePOVouchLink", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bIsPOActive(oSysDB As Object, sCompanyID As String) As Boolean
'********************************************************
' Desc: This routine will determine if the PO module has
'       been activated.
'********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rs As Object
    
    bIsPOActive = False
    
    sSQL = "SELECT Active FROM tsmCompanyModule "
    sSQL = sSQL & "WHERE CompanyID = " & gsQuoted(sCompanyID)
    sSQL = sSQL & " AND ModuleNo = " & kModulePO

    Set rs = oSysDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        bIsPOActive = gbGetValidBoolean(rs.Field("Active"))
    End If
    
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsPOActive", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub ClearRcvrDefaultsStruct()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With muRcvrDflts
        .sRcvrNo = ""
        .lRcvrKey = 0
        .bRcvrNumberChanged = False
        .sVendID = ""
        .lVendKey = 0
        .bReturnedVendor = False
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearRcvrDefaultsStruct", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ClearPODefaultsStruct()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With muPODflts
        .sPONo = ""
        .lPOKey = 0
        .bPONumberChanged = False
        .sVendID = ""
        .lVendKey = 0
        .sCurrID = ""
        .dCurrExchRate = 0
        .bReturnedVendor = False
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearPODefaultsStruct", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ClearTransferDefaultsStruct()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    With muTransferDflts
        .sTranNo = ""
        .lTrnsfrOrderKey = 0
        .bTransferNumberChanged = False
        .sShipWhseID = ""
        .lShipWhseKey = 0
        .sTransitWhseID = ""
        .bReturnedShipWhse = False
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "basEnterRcptOfGoods", "ClearTransferDefaultsStruct", VBRIG_IS_MODULE    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basEnterRcptOfGoods"
End Function

