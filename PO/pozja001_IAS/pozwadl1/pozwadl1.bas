Attribute VB_Name = "basRegister"
#Const NEWGL = True
'***********************************************************************
'     Name: bas{Name} (source: Template)
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: {date&Inits}
'     Mods: {date&Inits};
'***********************************************************************
Option Explicit
Global Const RPT_MODULE = "PO" 'enter the two character module Name here
Public Declare Sub Sleep Lib "kernel32" _
 (ByVal dwMilliseconds As Long)

Public gbSkipPrintDialog As Boolean      '   whether to show Print Dialog window. HBS.
Public gbCancelPrint As Boolean     '   whether user selected cancel on print dialog. HBS.
    
Public gbPrintToFile As Boolean     'whether to print to file
Public gbSkipMessage As Boolean     'whethr to skip message box

Public gsPathDefPrint As String     'path for deffered printing

' global constants for PO  batch types so we can manipulate them
Global Const BATCH_TYPE_RECV = 1101
Global Const BATCH_TYPE_RTRN = 1103

'tglOptions | AcctRefUsage
Public Const kvAcctRefUsageNotUsed = 0
Public Const kvAcctRefUsageValidated = 1
Public Const kvAcctRefUsageNonValidated = 2

Public gsStatus As String
Public gsTranType As String

Public Type uLocalizedStrings
    NotPrinted      As String
    Printed         As String
    FatalErrors     As String
    NotBalanced     As String
    Balanced        As String
    posting         As String
    posted          As String
    PostingFailed   As String
    JournalDesc     As String
    SOJournalDesc   As String
    ProcessingRegister As String
    PrintingRegister As String
    RegisterPrinted As String
    PostingComplete As String
    PostingModule As String
    ModulePosted As String
    Validating As String
    PrintError As String
    PrintErrorCompleted As String
    ProcessingGLReg As String
    CheckingBatch As String
    PostingModule2 As String
    ModulePosted2 As String
    GLPosting As String
    GLPosted As String
End Type

Public Type RegStatus
    iBatchStatus As Long
    iRecordsExist As Long
    iPrintStatus As Long
    iErrorStatus As Long
End Type
    
Const VBRIG_MODULE_ID_STRING = "pozwadl1.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Main is required as the standard 'Startup Form'.
'           Main can be empty.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub GetRegisterStatus(DAS As Object, lBatchKey As Long, uRegStat As RegStatus)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

        'Used to get the current status from the tciBatchlog table
        DAS.SetInParam lBatchKey
        DAS.SetOutParam uRegStat.iPrintStatus
        DAS.SetOutParam uRegStat.iErrorStatus
        DAS.SetOutParam uRegStat.iBatchStatus
        DAS.SetOutParam uRegStat.iRecordsExist
        DAS.ExecuteSP ("spCheckRgstrStatus")
                                                                                                                               
        Sleep 0&
                
        uRegStat.iPrintStatus = DAS.GetOutParam(2)
        uRegStat.iErrorStatus = DAS.GetOutParam(3)
        uRegStat.iBatchStatus = DAS.GetOutParam(4)
        uRegStat.iRecordsExist = DAS.GetOutParam(5)
        
        DAS.ReleaseParams

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetRegisterStatus", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


    
Public Sub BuildLocalizedStrings(SysDB As Object, SysSession As Object, guLocalizedStrings As uLocalizedStrings)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With guLocalizedStrings
        .NotPrinted = gsBuildString(kJrnlNotPrinted, SysDB, SysSession)
        .Printed = gsBuildString(kJrnlPrinted, SysDB, SysSession)
        .FatalErrors = gsBuildString(kJrnlFatalErrors, SysDB, SysSession)
        .NotBalanced = gsBuildString(kJrnlNotBalanced, SysDB, SysSession)
        .Balanced = gsBuildString(kJrnlBalanced, SysDB, SysSession)
        .posting = gsBuildString(kJrnlPosting, SysDB, SysSession)
        .posted = gsBuildString(kJrnlPosted, SysDB, SysSession)
        .PostingFailed = gsBuildString(kJrnlPostingFailed, SysDB, SysSession)
        .SOJournalDesc = gsBuildString(kARZJA001Invoice, SysDB, SysSession)
        .ProcessingRegister = gsBuildString(kARZJA001ProcessRegister, SysDB, SysSession)
        .PrintingRegister = gsBuildString(kARZJA001PrintingRegister, SysDB, SysSession)
        .RegisterPrinted = gsBuildString(kARZJA001RegisterPrinted, SysDB, SysSession)
        .PostingComplete = gsBuildString(kARZJA001PostingComplete, SysDB, SysSession)
        .PostingModule = gsBuildString(kVSDelCustKey, SysDB, SysSession)
        .ModulePosted = gsBuildString(kVCustIactive, SysDB, SysSession)
        .Validating = gsBuildString(kARZJA001Validating, SysDB, SysSession)
        .PrintError = gsBuildString(kARZJA001PrintError, SysDB, SysSession)
        .PrintErrorCompleted = gsBuildString(kARZJA001PrintErrorCompleted, SysDB, SysSession)
        .ProcessingGLReg = gsBuildString(kARZJA001ProcessingGLReg, SysDB, SysSession)
        .CheckingBatch = gsBuildString(kARZJA001CheckingBatch, SysDB, SysSession)
        .PostingModule2 = gsBuildString(kPostingModule, SysDB, SysSession)
        .ModulePosted2 = gsBuildString(kModulePosted, SysDB, SysSession)
        .GLPosting = gsBuildString(kGLPosting, SysDB, SysSession)
        .GLPosted = gsBuildString(kGLPostingComplete, SysDB, SysSession)
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BuildLocalizedStrings", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basRegister"
End Function

Public Sub StartErrorReport(sButton As String, frm As Form)

Dim sTitle1 As String

    ShowStatusBusy frm
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    frm.moReport.ReportFileName() = "pozjadl3.rpt"
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
    
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    'DO NOT HARDCODE ANY TEXT
    Select Case frm.miBatchType
    Case BATCH_TYPE_RECV
        sTitle1 = gsBuildString(kReceiptReg, frm.oClass.moAppDB, frm.oClass.moSysSession)
    Case BATCH_TYPE_RTRN
        sTitle1 = gsBuildString(ksReturnsRegister, frm.oClass.moAppDB, frm.oClass.moSysSession)
    End Select
    
    
    frm.moReport.ReportTitle1() = sTitle1
    frm.moReport.ReportTitle2() = gsBuildString(kSOSRRptErrorTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
    
    'automated means of labeling subtotal fields.
    'this is not mandatory
    frm.moReport.UseSubTotalCaptions() = 0
    frm.moReport.UseHeaderCaptions() = 0
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If

    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BY with new one. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    'This sub will set any labels for the table fields on the RPT file if they exist.
    'Labels must match the column name from the parent table as well as prefixed by "lbl".
    'For example, to label customer ID from tarCustomer table, a formula on the report
    'must exist with the name, lblCustID. At design time fill this formula with empty quotes.
    'This is not a mandatory function. Often the captions from the DD will not be exactly
    'what you want to appear on the report.  If this is the case, there is a function
    'available in the Crystal design environment called FreeFormText that takes a string
    'table constant as a parameter to place text on the report.
    frm.moReport.SetReportCaptions
        
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    If (frm.moReport.lRestrictBy("{tciErrorLog.SessionID} = " & frm.mlBatchKey) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If
    
    
    If gbPrintToFile Then
        frm.moReport.ExportRptFile gsPathDefPrint & "\POPost" & Replace(Replace(Replace(Now, "/", "_"), ":", "_"), " ", "_") & ".rpt", "DCD PO Posting"
    Else
        frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog
    End If
    frm.sbrMain.Status = SOTA_SB_START
    
    Exit Sub

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartErrorReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Public Sub StartShipRegSum(sButton As String, frm As Form)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim sTitle1 As String
Dim sRestrictBy As String
Dim iQtyDecimals As Integer
Dim iGroupsToSkip As Integer

    ShowStatusBusy frm
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    With frm
        Select Case .miBatchType
            Case BATCH_TYPE_RECV
              If frmRegister.chkProjectDetails = vbChecked Then
                 .moReport.ReportFileName() = "POZJADL2_IPA.RPT"    'Rcpt
              Else
                 .moReport.ReportFileName() = "pozjadl2.RPT"    'Rcpt
              End If
                 iGroupsToSkip = 1
            Case BATCH_TYPE_RTRN
              If frmRegister.chkProjectDetails = vbChecked Then
                 .moReport.ReportFileName() = "POZJB002_IPA.RPT"    'Retrn
              Else
                 .moReport.ReportFileName() = "pozjb002.RPT"    'Retrn
              End If
                 iGroupsToSkip = 0
        End Select
    End With
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
                                   
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    'DO NOT HARDCODE ANY TEXT
    Select Case frm.miBatchType
    Case BATCH_TYPE_RECV
        sTitle1 = gsBuildString(kReceiptReg, frm.oClass.moAppDB, frm.oClass.moSysSession) 'Rcpt
    Case BATCH_TYPE_RTRN
        sTitle1 = gsBuildString(ksReturnsRegister, frm.oClass.moAppDB, frm.oClass.moSysSession) 'Retrn
    End Select
    
    With frm
        .moReport.ReportTitle1() = sTitle1
        .moReport.ReportTitle2() = ""
        .moReport.UseSubTotalCaptions() = 1
        .moReport.UseHeaderCaptions() = 1

        'set standard formulas, business date, run time, company name etc.
        'as defined in the template
        If (.moReport.lSetStandardFormulas(frm) = kFailure) Then
            MsgBox "SetStandardFormulas failed"
            GoTo badexit
        End If
    
        iQtyDecimals = giGetValidInt(.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", "CompanyID = " & gsQuoted(.msCompanyID)))
        
        If .moReport.bSetReportFormula("QtyDecimals", CStr(iQtyDecimals)) = 0 Then
            GoTo badexit
        End If
        
        'translate sort selections into Crystal commands
        If (.moReport.lSetSortCriteria(.moSort, iGroupsToSkip) = kFailure) Then
            MsgBox "SetSortCriteria failed"
            GoTo badexit
        End If
        
    End With
   
 
    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BYs with new ones. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    frm.moReport.SetSQL

    frm.moReport.SetReportCaptions
    
    'used in the Summary section on the report: use kLenPortrait or kLenLandscape
'    frm.moReport.SelectString = frm.moSelect.sGetUserReadableWhereClause(kLenPortrait)
    
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    If frm.cboFormat.ListIndex <> frm.miDetail Then
        If frm.moReport.bSetReportFormula("RptFormatIsSumm", CStr(1)) = 0 Then
            GoTo badexit
        End If
    End If
    
    
    'if using work tables, it's necessary to restrict the query to the data produced by your
    '"run".  The engine used to do this behind the scenes but as the engine is used in more
    'different ways now, it's better to make the call explicitly yourself.  This permits
    'restriction by any column and doesn't force the inclusing of a column, SessionID in the
    'work tables.
    'only for the report that not use sort grid
    'frm.moSort.SortsUsed()
    Select Case frm.miBatchType
    Case BATCH_TYPE_RECV
        Select Case frm.cboFormat.ListIndex
            Case -1, frm.miSummary
                sRestrictBy = "{tpoRcptsRegSummWrk.SessionID} = "
            Case frm.miDetail
                sRestrictBy = "{tpoRcptsRegDtlWrk.SessionID} = "
        End Select
    Case BATCH_TYPE_RTRN
        Select Case frm.cboFormat.ListIndex
            Case -1, frm.miSummary
                sRestrictBy = "{tpoRtrnsRegSummWrk.SessionID} = "
            Case frm.miDetail
                sRestrictBy = "{tpoRtrnsRegDtlWrk.SessionID} = "
        End Select
    End Select
    
    If (frm.moReport.lRestrictBy(sRestrictBy & frm.moReport.SessionID) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If
    
    If gbPrintToFile Then
        frm.moReport.ExportRptFile gsPathDefPrint & "\POPost" & Replace(Replace(Replace(Now, "/", "_"), ":", "_"), " ", "_") & ".rpt", "DCD PO Posting"
    Else
        frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog
    End If
    frm.sbrMain.Status = SOTA_SB_START
    
    Exit Sub

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartShipRegSum", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub

Public Sub StartGLPostReg(sButton As String, frm As Form)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim iJob As Integer
Dim iRetVal As Integer
Dim sTitle1 As String, sTitle2 As String

Dim oClass          As Object
Dim sCompanyID      As String
Dim lAcctRefTitle   As Long
Dim sAcctRefTitle   As String
Dim lStrHandle      As Long
Dim iLen            As Integer
'Dim iQtyDecimals    As Integer

    ShowStatusBusy frm
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
#If NEWGL Then
    If frm.miUseMultCurr = 1 Then
        frm.moReport.ReportFileName() = "glzje006.rpt"
    Else
        frm.moReport.ReportFileName() = "glzje005.rpt"
    End If
            
    If frm.miGLPostRgstrFormat = 2 Then
    ' summary
        frm.moReport.ReportTitle2() = gsBuildString(kSOShipRegGLSum, frm.oClass.moAppDB, frm.oClass.moSysSession)
    Else
        frm.moReport.ReportTitle2() = gsBuildString(kSOShipRegGLDtl, frm.oClass.moAppDB, frm.oClass.moSysSession)
    End If

#Else
    If frm.miGLPostRgstrFormat = 2 Then
        If frm.miUseMultCurr = 1 Then
            frm.moReport.ReportFileName() = "glzje502.rpt"  'summary MC
        Else
            frm.moReport.ReportFileName() = "glzje501.rpt"  'summary
        End If
    
    Else
        If frm.miUseMultCurr = 1 Then
            frm.moReport.ReportFileName() = "glzje504.rpt"  'detail MC
        Else
            frm.moReport.ReportFileName() = "glzje503.rpt"  'detail
        End If
    
        frm.moReport.ReportTitle2() = sBuildString(kSOShipRegGLDtl, frm.oClass.moAppDB, frm.oClass.moSysSession)
    End If
#End If
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORPortrait
    
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    'DO NOT HARDCODE ANY TEXT
    Select Case frm.miBatchType
        Case BATCH_TYPE_RECV
                sTitle1 = gsBuildString(kReceiptReg, frm.oClass.moAppDB, frm.oClass.moSysSession) 'Rcpt
        Case BATCH_TYPE_RTRN
                sTitle1 = gsBuildString(ksReturnsRegister, frm.oClass.moAppDB, frm.oClass.moSysSession) 'Retrn
    End Select
    
    frm.moReport.ReportTitle1() = sTitle1
    
    'automated means of labeling subtotal fields.
    'this is not manditory if you choose not to use this feature
    frm.moReport.UseSubTotalCaptions() = 0
    frm.moReport.UseHeaderCaptions() = 0
        
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If
    

#If NEWGL Then
    Set oClass = frm.oClass
    sCompanyID = oClass.moSysSession.CompanyId
    If kvAcctRefUsageNotUsed <> giGetValidInt(oClass.moAppDB.Lookup("AcctRefUsage", "tglOptions", "CompanyID = " & gsQuoted(sCompanyID))) Then
        If Not frm.moReport.bGetFormula("AcctRefTitle", sAcctRefTitle) Then
            'fatal error
            GoTo badexit
        End If
        
        'Find the Localized Title from GLOptions
        lAcctRefTitle = oClass.moAppDB.Lookup("AcctRefTitleStrNo", "tglOptions", "CompanyID =" & gsQuoted(sCompanyID))
        
        'QQ, sGetLocalString will generate 'Invalid use of Null' error if lAcctRefTitle is not in tsmLocalString
        On Error Resume Next
        sAcctRefTitle = sGetLocalString(lAcctRefTitle, oClass.moAppDB, oClass.moSysSession)
        If Err <> 0 Then
            Err.Clear
            sAcctRefTitle = "Reference"
        End If
        On Error GoTo badexit
        
        If Not frm.moReport.bSetReportFormula("AcctRefTitle", CStr(QUOTECON & sAcctRefTitle & QUOTECON)) Then
            'fatal error
            GoTo badexit
        End If

    End If

    If frm.miGLPostRgstrFormat = 2 Then
        ' summary
        If Not frm.moReport.bSetReportFormula("ReportFormat", "0") Then
            'fatal error
            GoTo badexit
        End If
    Else
        ' detail
        If Not frm.moReport.bSetReportFormula("ReportFormat", "1") Then
            'fatal error
            GoTo badexit
        End If
    End If
#End If

    
    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BYs with new ones. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    'this sub will set any labels for the table fields on the RPT file if they exist.
    'Labels must match the column name from the parent table as well as prefixed by "lbl".
    'For example, to label customer ID from tarCustomer table, a formula on the report
    'must exist with the name, lblCustID. At design time fill this formula with empty quotes.
    'this is not a manitory function. Often the captions from the DD will not be exactly
    'what you want to appear on the report.  If this is the case, there is a function
    'available in the Crystal design environment called FreeFormText that takes a string
    'table constant as a parameter to place text on the report.
    frm.moReport.SetReportCaptions
        
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    'if using work tables, it's necessary to restrict the query to the data produced by your
    '"run".  The engine used to do this behind the scenes but as the engine is used in more
    'different ways now, it's better to make the call explicitly yourself.  This permits
    'restriction by any column and doesn't force the inclusing of a column, SessionID in the
    'work tables.
    'only for the report that not use sort grid
    'frm.moSort.SortsUsed()
    If (frm.moReport.lRestrictBy("{tglPosting.BatchKey} = " & frm.mlBatchKey) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If
    
    If gbPrintToFile Then
        frm.moReport.ExportRptFile gsPathDefPrint & "\POPost" & Replace(Replace(Replace(Now, "/", "_"), ":", "_"), " ", "_") & ".rpt", "DCD PO Posting"
    Else
        frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog
    End If
    frm.sbrMain.Status = SOTA_SB_START
    
    Exit Sub
Resume
badexit:

    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartGLPostReg", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub

