Attribute VB_Name = "basProcRcptOfInvc"
'***********************************************************************
'   Name:
'       basProcRcptOfInvc
'
'   Description:
'       This module contains code specific to the Process Receipt of
'       Invoice task.
'
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 05/07/97 JSP
'     Mods:
'***********************************************************************
Option Explicit

    '-- Public PO-specific variables
    Public mbAutoSelectLines    As Boolean
    Public mbDefaultQty         As Boolean
    Public miMatchLevel         As Integer
    Public mbInternalLinesClick As Boolean
    Public msOpenString         As String
    Public msClosedString       As String
    Public msSoftOvrdString     As String
    Public msHardOvrdString     As String
    Public msCreditString       As String
    Public msReplacementString  As String
    Public mbLineDeleted        As Boolean
    Public mbValidatingPO       As Boolean
    Public mbManualClick        As Boolean
    
    '-- Private PO-specific variables
    Private mbInVendID          As Boolean
    
    '-- Public PO-specific constants
    Public Const kPONoLen = 10
    Public Const kPONoMaxLen = 15
    Public Const kMatchLevelNone = 0
    Public Const kMatchLevel2Way = 2
    Public Const kMatchLevel3Way = 3
    Public Const kMatchStatusOpen = 1
    Public Const kMatchStatusClosed = 2
    Public Const kMatchStatusSoftOvrd = 3
    Public Const kMatchStatusHardOvrd = 4
    Public Const kReturnNone = 0
    Public Const kReturnForCredit = 1
    Public Const kReturnForReplace = 2
    Public Const kPO = 0
    Public Const kRcvr = 1
    
       
    '-- Select PO Lines Grid column constants
    Public Const kColPOSelect = 1
    Public Const kColPOPOLineNo = 2
    Public Const kColPOItemID = 3
    Public Const kColPOQty = 4
    Public Const kColPOUOM = 5
    Public Const kColPOUnitCost = 6
    Public Const kColPOAmount = 7
    Public Const kColPOItemDesc = 8
    Public Const kColPOPONo = 9
    Public Const kColPOPOKey = 10
    Public Const kColPOPOLineKey = 11
    Public Const kColPOPOLineDistKey = 12
    Public Const kColPOGLAcctKey = 13
    Public Const kColPOGLAcctNo = 14
    Public Const kColPOGLAcctDesc = 15
    Public Const kColPOAcctRefKey = 16
    Public Const kColPOAcctRefCode = 17
    Public Const kColPOAcctRefDesc = 18
    Public Const kColPOItemKey = 19
    Public Const kColPOExtCmnt = 20
    Public Const kColPOSTaxClassID = 21
    Public Const kColPOSTaxClassKey = 22
    Public Const kColPOSTaxTranKey = 23
    Public Const kColPOUnitMeasKey = 24
    Public Const kColPOUnitMeasType = 25
    Public Const kColPOCmntOnly = 26
    Public Const kColPOQtyInvc = 27
    Public Const kColPOAmtInvc = 28
    Public Const kColPOLineClosed = 29
    Public Const kColPOTargetCoID = 30
    Public Const kColPOShipMethKey = 31
    Public Const kColPOShipMethID = 32
    Public Const kColPOShipZoneKey = 33
    Public Const kColPOShipZoneID = 34
    Public Const kColPOFOBKey = 35
    Public Const kColPOFOBID = 36
    Public Const kColPOFreightAmt = 37
    Public Const kColPORcptReq = 38
    Public Const kColPOAllowCostOvrd = 39
    Public Const kColPORcvrKey = 40
    Public Const kColPORcvrLineKey = 41
    Public Const kColPORtrnType = 42
    Public Const kColPOQtyRcvd = 43
    Public Const kColPORcvrUOM = 44
    Public Const kColPOWhseKey = 45
    Public Const kColPOFixedExchRate = 61
    Public Const kColUnBilledQty = 60
    Public Const kColPOUnitCostExact = 62 'Constant declarartion for UnitCostExact
'Intellisol start
    Public Const kColPOProjectID = 46
    Public Const kColPOProjectKey = 47
    Public Const kColPOPhaseID = 48
    Public Const kColPOPhaseKey = 49
    Public Const kColPOTaskID = 50
    Public Const kColPOTaskKey = 51
    Public Const kColPOCostClassID = 52
    Public Const kColPOCostClassKey = 53
    Public Const kColPOVarianceStr = 54
    Public Const kColPOVariance = 55
    Public Const kColPOJobLineKey = 56
    Public Const kColPOJobType = 57
    Public Const kColPOTaskType = 58
'Intellisol end
    Public Const kColPOFASAssetTemplate = 59 'FAS Integration
    Public Const kPOGridMaxCols = 62
    
    '-- PO Number validation return values
    Public Enum iPOValidVals
        kPOBadRet = 0
        kPOValid = -1
        kErrPONotExist = 1
        kErrPONotOpen = 2
        kErrPONotStandard = 3
        kErrPOAllLinesClosed = 4
        kErrPOBadVendor = 5
        kErrPOOnHold = 6
        kErrPOBlankVendor = 7
        kErrPODiffCurr = 8
        kErrPONoROG = 9
        kErrPODiffExchRate = 10
        kErrPOInvalidExchRateSchd = 11
        kErrPOMultExchRates = 12
        kErrPOExchRateZero = 13
    End Enum

    '-- Select Lines Grid mode types
    Public Enum iSelLinesMode
        kPOLines = 0
        kRcvrLines = 1
    End Enum
    
    Type uPODfltsType
        sPONo                   As String
        lPOKey                  As Long
        bPONoChanged            As Boolean
        sRcvrNo                 As String
        lRcvrKey                As Long
        bRcvrNoChanged          As Boolean
        sVendID                 As String
        lVendKey                As Long
        lRemitToAddrKey         As Long
        lRemitToVendAddrKey     As Long
        lPurchFromAddrKey       As Long
        lPurchFromVendAddrKey   As Long
        lPmtTermsKey            As Long
        sCurrID                 As String
        lCurrExchSchdKey        As Long
        dCurrExchRate           As Double
        i1099Type               As Integer
        i1099Form               As Integer
        s1099Box                As String
        s1099BoxText            As String
        bReturnedVendor         As Boolean
        bSetCurrIDFromPO        As Boolean
        lShipMethKey            As Long
        sShipMethID             As String
        lFOBKey                 As Long
        sFOBID                  As String
        sTranCmnt               As String
        dFreightAmt             As Double
        dSTaxAmt                As Double
        lSTaxTranKey            As Long
        bFixedExchRate          As Boolean
    End Type
    Public muPODflts            As uPODfltsType
    
    Type uPOLineOldVals
        lPOLineKey              As Long
        lUnitMeasKey            As Long
        dQty                    As Double
        dUnitCost               As Double
    End Type
    Public muPOLineOldVals      As uPOLineOldVals
    
Const VBRIG_MODULE_ID_STRING = "POZDB001.BAS"

Public Function bInitForPO(frm As Form, lVRunMode As Long, oOptions As clsModuleOptions) As Boolean
'***********************************************************
'    Desc: This function will display and enable/disable the
'          PO-related fields on the Enter Vouchers form.
' Returns: True if successful, else false.
'***********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim oClass As Object
    Dim sCompanyID As String
        
    bInitForPO = False
    
    Set oClass = frm.oClass
    sCompanyID = oClass.moSysSession.CompanyID
    
    mbLineDeleted = False
    mbValidatingPO = False
    ClearPODefaultsStruct
    
    If (lVRunMode = kProiDTE) Or (lVRunMode = kProiDDN) Then
        '-- Display PO-related fields and grid columns
        With frm
            .lblPONo.Visible = True
            .txtPONo.Visible = True
            .lblDash.Visible = True
            .txtRelNo.Visible = True
            ToggleCtrlState .cmdLines(kPO), False
            .cmdLines(kPO).Visible = True
            .lblRcvrNo.Visible = True
            .lkuRcvrNo.Visible = True
            ToggleCtrlState .cmdLines(kRcvr), False
            .cmdLines(kRcvr).Visible = True
            
            '-- Set up PO Options variables
            mbAutoSelectLines = CBool(oOptions.PO("DfltInvcComplete")) 'VouchAutoLineSel
            mbDefaultQty = Not CBool(oOptions.PO("BlindInvc")) 'VouchQtyDflt
            miMatchLevel = oOptions.PO("MatchInvcToPO")
            
            If (miMatchLevel <> kMatchLevelNone) Then
                .chkDoNotMatch.Visible = True
            End If
        
            .lblExtAmt.Caption = "In&vc Amt"
                        
            '-- Make sure the PO sub detail tab is visible
            .tabSubDetl.TabVisible(kiPOTab) = True
            
            If (lVRunMode = kProiDDN) Then
                .lkuVouchNo.VisibleLookup = False
            End If
        End With
            
        '-- Setup PO-specific columns in the detail grid
        InitializeDetlGridPO frm, oClass.moSysSession, oClass.moAppDB
    
        mbInternalLinesClick = False
    Else
        '-- Hide PO-related fields and grid columns
        With frm
            '-- Default the PO sub detail tab to hidden
            .tabSubDetl.TabVisible(kiPOTab) = False
            
            gGridHideColumn .grdVouchDetl, kColPONo
            gGridHideColumn .grdVouchDetl, kColPOLineNo
            
            If (lVRunMode = kViewEditVouchDTE) Or (lVRunMode = kViewEditVouchDDN) Then
                '-- Check that PO is activated before displaying PO field
                If bIsPOActive(oClass.moAppDB, sCompanyID) Then
                    .lblPONo.Visible = True
                    .txtPONo.Visible = True
                    .lblDash.Visible = True
                    .txtRelNo.Visible = True
                    ToggleCtrlState .lkuPONo, False
                    ToggleCtrlState .txtPONo, False
                    ToggleCtrlState .txtRelNo, False
                    .navPO.Visible = False
                    .lblRcvrNo.Visible = True
                    .lkuRcvrNo.Visible = True
                    ToggleCtrlState .lkuRcvrNo, False
                    .lkuRcvrNo.VisibleLookup = False
                    
                    '-- Setup PO-specific columns in the detail grid
                    InitializeDetlGridPO frm, oClass.moSysSession, oClass.moAppDB
                
                    gGridShowColumn .grdVouchDetl, kColPONo
                    gGridShowColumn .grdVouchDetl, kColPOLineNo
                Else
                    .lblPONo.Visible = False
                    .txtPONo.Visible = False
                    .lblDash.Visible = False
                    .txtRelNo.Visible = False
                    .navPO.Visible = False
                    .lblRcvrNo.Visible = False
                    .lkuRcvrNo.Visible = False
                End If
            Else
                .lblPONo.Visible = False
                .txtPONo.Visible = False
                .lblDash.Visible = False
                .txtRelNo.Visible = False
                .navPO.Visible = False
                .lblRcvrNo.Visible = False
                .lkuRcvrNo.Visible = False
            End If
            .cmdLines(kPO).Visible = False
            .cmdLines(kRcvr).Visible = False
            .chkDoNotMatch.Visible = False
        
            gGridHideColumn .grdVouchDetl, kColPOLineKey
            gGridHideColumn .grdVouchDetl, kColMatchStatus
            gGridHideColumn .grdVouchDetl, kColDispMatchStatus
            gGridHideColumn .grdVouchDetl, kColReturnType
            gGridHideColumn .grdVouchDetl, kColDispReturnType
        End With
    End If
        
    '-- Now freeze some columns for looks;
    '-- this freezes everything to the left of the given column
    gGridFreezeCols frm.grdVouchDetl, kColMaskedItemID

    Set oClass = Nothing
    
    bInitForPO = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

FuncExit:
    Set oClass = Nothing
    Exit Function
    
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bInitForPO", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub InitializeDetlGridPO(frm As Form, oSysSession As Object, oAppDB As Object)
'*******************************************************
' Desc: This routine will format the voucher detail grid
'       with the PO-specific columns.
'*******************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sCaption As String

    With frm
        '-- Set column headers
        '-- PO
        sCaption = gsBuildString(ksPO, oAppDB, oSysSession)
        gGridSetHeader .grdVouchDetl, kColPONo, sCaption
    
        '-- PO Line
        sCaption = gsBuildString(ksPOLineNum, oAppDB, oSysSession)
        gGridSetHeader .grdVouchDetl, kColPOLineNo, sCaption
    
        '-- Match Status
        sCaption = gsBuildString(ksMatchStatus, oAppDB, oSysSession)
        gGridSetHeader .grdVouchDetl, kColDispMatchStatus, sCaption
                
        '-- Return Type
        sCaption = gsBuildString(ksReturnType, oAppDB, oSysSession)
        gGridSetHeader .grdVouchDetl, kColDispReturnType, sCaption
                
        '-- Set grid Column Widths
        gGridSetColumnWidth .grdVouchDetl, kColPONo, 10
        gGridSetColumnWidth .grdVouchDetl, kColPOLineNo, 6
        gGridSetColumnWidth .grdVouchDetl, kColDispMatchStatus, 12
        gGridSetColumnWidth .grdVouchDetl, kColDispReturnType, 12
    
        '-- Hide the appropriate columns
         '-- PO: The Item Description column should be moved to the end of the grid,
         '-- but worry about this later...
        gGridHideColumn .grdVouchDetl, kColItemDesc
        gGridHideColumn .grdVouchDetl, kColPOLineKey
        gGridHideColumn .grdVouchDetl, kColMatchStatus
        gGridHideColumn .grdVouchDetl, kColReturnType
        gGridHideColumn .grdVouchDetl, kColPOKey
        gGridHideColumn .grdVouchDetl, kColRcvrKey
        gGridHideColumn .grdVouchDetl, kColRcvrLineKey
        gGridHideColumn .grdVouchDetl, kColFixedExchRate
        
        If miMatchLevel = kMatchLevelNone Then
            gGridHideColumn .grdVouchDetl, kColDispMatchStatus
        End If
        
        '-- Set the column types
        gGridSetColumnType .grdVouchDetl, kColPONo, SS_CELL_TYPE_EDIT
        gGridSetColumnType .grdVouchDetl, kColPOLineNo, SS_CELL_TYPE_EDIT
        .grdVouchDetl.Col = kColPOLineNo: .grdVouchDetl.Row = -1
        .grdVouchDetl.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
        gGridSetColumnType .grdVouchDetl, kColDispMatchStatus, SS_CELL_TYPE_EDIT
        gGridSetColumnType .grdVouchDetl, kColDispReturnType, SS_CELL_TYPE_EDIT
    
        '-- Allow the user to resize the new PO columns
        .grdVouchDetl.Col = kColPONo
        .grdVouchDetl.UserResizeCol = SS_USER_RESIZE_ON
        .grdVouchDetl.Col = kColPOLineNo
        .grdVouchDetl.UserResizeCol = SS_USER_RESIZE_ON
        .grdVouchDetl.Col = kColDispMatchStatus
        .grdVouchDetl.UserResizeCol = SS_USER_RESIZE_ON
        .grdVouchDetl.Col = kColDispReturnType
        .grdVouchDetl.UserResizeCol = SS_USER_RESIZE_ON
    End With
       
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeDetlGridPO", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
    
Public Function iGetPostDateYearPO(oClass As Object, lBatchKey As Long, sPostDate As String) As Integer
'******************************************************************
'    Desc: This function will retrieve and return the posting
'          date's year for the current PO receipt of invoice batch.
' Returns: The PostDate Year.
'******************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    Dim rs As Object
    Dim iPostDateYear As Integer

    iGetPostDateYearPO = -1
    
    '-- Get the Post Date for this Voucher batch
    sSQL = "SELECT PostDate FROM tpoBatch "
    sSQL = sSQL & "WHERE BatchKey = " & lBatchKey
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If Not rs.IsEmpty Then
        sPostDate = Trim$(gsGetValidStr(rs.Field("PostDate")))
        
        '-- Check to see if the Post Date has a value
        If Len(sPostDate) = 0 Then
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateNull
            GoTo ExpectedErrorRoutine
        Else
            iPostDateYear = Year(sPostDate)
            If (iPostDateYear <= 0) Then
                giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchPostDateError
                GoTo ExpectedErrorRoutine
            End If
        End If
    Else
        '-- The corresponding batch row was not found
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgBatchNotFoundError, Format$(lBatchKey)
        GoTo ExpectedErrorRoutine
    End If

    rs.Close: Set rs = Nothing

    '-- Return the value
    iGetPostDateYearPO = iPostDateYear
    
    Exit Function

ExpectedErrorRoutine:
    rs.Close: Set rs = Nothing
    sPostDate = ""
    iGetPostDateYearPO = -1
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetPostDateYearPO", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function bIsValidPONo(frm As Form, oDmGrid As clsDmGrid, _
                             oLEGrid As clsLineEntry, iUseMultCurr As Integer, _
                             dDocCurrExchRate As Double, sHomeCurrID As String, sTranDate As String, Optional bFromVendID As Boolean = False, _
                             Optional sMessage As String = "") As Boolean
'+++ VB/Rig Skip +++
'************************************************************
'    Desc: This function will validate the entered PO number.
' Returns: True if the PO number is valid, else false.
'************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim bValid As Boolean
    Dim sCompanyID As String
    Dim sPONo As String
    Dim iRetVal As iPOValidVals
    Dim iTempRetVal As iPOValidVals
    Dim iReply As Integer
    Dim bRetMsg As Boolean
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim oClass As Object
    Dim sPOStr As String
    Dim lTranType As Long
    Dim lVendClassKey As Long
    Dim sVendClassID As String
    Dim lOrigVendClassKey As Long
    Dim sOrigVendClassID As String
    Dim iGetExchMode As Integer
    Dim iHold As Integer
    Dim iDiffCurrID As Integer
    Dim iDiffExchRate As Integer
    Dim iWarnMultExchRates As Integer
    
    If frm.lkuPONo.Tag = frm.lkuPONo.Text Then
        bIsValidPONo = True
        Exit Function
    End If
        
    mbValidatingPO = True
    
    Set oClass = frm.oClass
        
    If (sMessage = kDisplayMsg) Then
        bRetMsg = False
    Else
        bRetMsg = True
    End If
    
    bValid = False
    
    With muPODflts
        .lVendKey = -1
        .bReturnedVendor = False
        .sCurrID = ""
        .bSetCurrIDFromPO = False
        .bPONoChanged = False
        .bRcvrNoChanged = False
    End With
    
    sCompanyID = oClass.moSysSession.CompanyID
    
    sPOStr = gsBuildString(15416, oClass.moAppDB, oClass.moSysSession)
    
    sPONo = frm.lkuPONo
    
    lTranType = frm.ddnTranType.ItemData
    
    If (Len(Trim$(sPONo)) > 0) Or bFromVendID Then
        With oClass.moAppDB
            .SetInParam sCompanyID
            .SetInParam sPONo
            .SetInParam frm.lkuVendID.Text
            .SetInParam frm.txtCurrID.Text
            .SetInParam dDocCurrExchRate
            .SetInParam lTranType
            .SetInParam sHomeCurrID
            .SetInParam gsFormatDateToDB(sTranDate)
                        
            .SetOutParam muPODflts.lPOKey
            .SetOutParam muPODflts.sVendID
            .SetOutParam muPODflts.lVendKey
            .SetOutParam muPODflts.lRemitToAddrKey
            .SetOutParam muPODflts.lRemitToVendAddrKey
            .SetOutParam muPODflts.lPurchFromAddrKey
            .SetOutParam muPODflts.lPurchFromVendAddrKey
            .SetOutParam muPODflts.lPmtTermsKey
            .SetOutParam muPODflts.sCurrID
            .SetOutParam muPODflts.lCurrExchSchdKey
            .SetOutParam muPODflts.dCurrExchRate
            .SetOutParam iGetExchMode
            .SetOutParam muPODflts.i1099Type
            .SetOutParam muPODflts.i1099Form
            .SetOutParam muPODflts.s1099Box
            .SetOutParam muPODflts.s1099BoxText
            .SetOutParam muPODflts.lShipMethKey
            .SetOutParam muPODflts.sShipMethID
            .SetOutParam muPODflts.lFOBKey
            .SetOutParam muPODflts.sFOBID
            .SetOutParam muPODflts.sTranCmnt
            .SetOutParam muPODflts.dFreightAmt
            .SetOutParam muPODflts.dSTaxAmt
            .SetOutParam muPODflts.lSTaxTranKey
            .SetOutParam lVendClassKey
            .SetOutParam sVendClassID
            .SetOutParam iHold
            .SetOutParam iDiffCurrID
            .SetOutParam iDiffExchRate
            .SetOutParam iWarnMultExchRates
            .SetOutParam iRetVal
            
            .ExecuteSP "sppoValPOforRcpt"
            
            muPODflts.lPOKey = glGetValidLong(.GetOutParam(9))
            muPODflts.sVendID = gsGetValidStr(.GetOutParam(10))
            muPODflts.lVendKey = glGetValidLong(.GetOutParam(11))
            muPODflts.lRemitToAddrKey = glGetValidLong(.GetOutParam(12))
            muPODflts.lRemitToVendAddrKey = glGetValidLong(.GetOutParam(13))
            muPODflts.lPurchFromAddrKey = glGetValidLong(.GetOutParam(14))
            muPODflts.lPurchFromVendAddrKey = glGetValidLong(.GetOutParam(15))
            muPODflts.lPmtTermsKey = glGetValidLong(.GetOutParam(16))
            muPODflts.sCurrID = gsGetValidStr(.GetOutParam(17))
            muPODflts.lCurrExchSchdKey = glGetValidLong(.GetOutParam(18))
            muPODflts.dCurrExchRate = gdGetValidDbl(.GetOutParam(19))
            iGetExchMode = giGetValidInt(.GetOutParam(20))
            muPODflts.i1099Type = giGetValidInt(.GetOutParam(21))
            muPODflts.i1099Form = giGetValidInt(.GetOutParam(22))
            muPODflts.s1099Box = gsGetValidStr(.GetOutParam(23))
            muPODflts.s1099BoxText = gsGetValidStr(.GetOutParam(24))
            muPODflts.lShipMethKey = glGetValidLong(.GetOutParam(25))
            muPODflts.sShipMethID = gsGetValidStr(.GetOutParam(26))
            muPODflts.lFOBKey = glGetValidLong(.GetOutParam(27))
            muPODflts.sFOBID = gsGetValidStr(.GetOutParam(28))
            muPODflts.sTranCmnt = gsGetValidStr(.GetOutParam(29))
            muPODflts.dFreightAmt = gdGetValidDbl(.GetOutParam(30))
            muPODflts.dSTaxAmt = gdGetValidDbl(.GetOutParam(31))
            muPODflts.lSTaxTranKey = glGetValidLong(.GetOutParam(32))
            lVendClassKey = glGetValidLong(.GetOutParam(33))
            sVendClassID = gsGetValidStr(.GetOutParam(34))
            iHold = giGetValidInt(.GetOutParam(35))
            iDiffCurrID = giGetValidInt(.GetOutParam(36))
            iDiffExchRate = giGetValidInt(.GetOutParam(37))
            iWarnMultExchRates = giGetValidInt(.GetOutParam(38))
            iRetVal = .GetOutParam(39)
            .ReleaseParams
        End With
        
        With oClass
            Select Case iRetVal
                Case kErrPONotExist
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgInvalidField, .moSysSession.Language, .moAppDB, frm.lblPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgInvalidField, frm.lblPONo
                    End If

                Case kErrPONotOpen
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONotOpen, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONotOpen, sPONo
                    End If

                Case kErrPONotStandard
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONotStandard, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONotStandard, sPONo
                    End If

                Case kErrPOAllLinesClosed
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPOAllLinesClosed, .moSysSession.Language, .moAppDB, sPONo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOAllLinesClosed, sPONo
                    End If

                Case kErrPOBadVendor
                    If bFromVendID Then
                        '-- There was an existing PO number and the vendor was changed,
                        '-- making the PO invalid. Therefore, blank out the PO number.
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgExistingPONotValid, .moSysSession.Language, .moAppDB, sPOStr, sPONo, frm.lkuVendID, sPOStr)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgExistingPONotValid, sPOStr, sPONo, frm.lkuVendID, sPOStr
                        End If
                        frm.lkuPONo.SetTextAndKeyValue Empty, Empty
                        mbInVendID = True
                        DeletePOLinesForVend frm, oDmGrid, oLEGrid
                        bIsValidPONo frm, oDmGrid, oLEGrid, iUseMultCurr, dDocCurrExchRate, sHomeCurrID, sTranDate
                        mbInVendID = False
                        frm.lkuPONo_PostLostFocus
                    Else
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgPONotValidForVendor, .moSysSession.Language, .moAppDB, sPOStr, sPONo, frm.lkuVendID)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgPONotValidForVendor, sPOStr, sPONo, frm.lkuVendID
                        End If
                    End If

                'No lines available for invoicing
                Case kErrPONoROG
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPONoROG, .moSysSession.Language, .moAppDB)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPONoROG
                    End If

                'No lines are available for invoicing.  There are receivers with multiple receivers
                '(and no lines where receipt is not required).
                Case kErrPOMultExchRates
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPOMultExchRates, .moSysSession.Language, .moAppDB)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOMultExchRates
                    End If

                'Unable to obtain the exchange rate
                Case kErrPOExchRateZero
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgPOExchRateZero, .moSysSession.Language, .moAppDB)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgPOExchRateZero
                    End If
                    
                                  
                Case kErrPOBlankVendor
                     iRetVal = kPOValid
                
                

            End Select
            
            If iRetVal = kPOValid Then

                If iHold <> 0 Then
                    iReply = giSotaMsgBox(Nothing, .moSysSession, kmsgPOOnHold, sPONo)
                    If iReply = kretYes Then
                        '-- Security event is needed to invoice an on-hold PO
                        sID = "INVCHOLDPO"
                        sUser = CStr(oClass.moSysSession.UserId)
                        vPrompt = True
                        If (.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) <> 0) Then
                            iRetVal = kPOValid
                        End If
                    End If
                End If

                If iRetVal = kPOValid And iDiffCurrID = 1 Then
                    If (iUseMultCurr = kiOn) Then
                        iReply = iCheckCurrency(oClass, frm, sPONo, sPOStr, sPONo, bRetMsg, sMessage)
                    End If

                    If (iReply = kretYes) Or (iUseMultCurr = kiOff) Then
                        iRetVal = kPOValid
                        muPODflts.bSetCurrIDFromPO = True
                        frm.RefreshCashAcctSQL muPODflts.sCurrID, True
                    Else
                        'Set to invalid PO
                        iRetVal = kErrPOMultExchRates
                    End If
                End If

                'Don't message if different currency - in this case also load the
                'associated rate.
                

                If iRetVal = kPOValid And iDiffExchRate = 1 And iDiffCurrID = 0 And iGetExchMode <> 1 Then
                    If (iUseMultCurr = kiOn) Then
                        iReply = iCheckExchRate(oClass, frm, sPONo, sPOStr, sPONo, dDocCurrExchRate, bRetMsg, sMessage)
                    End If

                    If (iReply = kretYes) Or (iUseMultCurr = kiOff) Then
                        iRetVal = kPOValid
                        muPODflts.bSetCurrIDFromPO = True
                    ElseIf (iReply = kretNo) And (iUseMultCurr = kiOn) Then
                        iRetVal = kPOValid
                    Else
                        'Set to invalid PO
                        iRetVal = kErrPOMultExchRates
                    End If
                End If

                'Warn user if not all lines will be available due to differing
                'exchange rates.  These must be invoiced separately.
                If iRetVal = kPOValid And iWarnMultExchRates Then
                    giSotaMsgBox Nothing, .moSysSession, kmsgPOWarnMultExchRates
                End If
                  
            End If
        End With

        '-- The PO is valid
        If (iRetVal = kPOValid) Then
            bValid = True

            muPODflts.bPONoChanged = True

            '-- Save the original vendor class information
            lOrigVendClassKey = frm.lkuVendClassID.KeyValue
            sOrigVendClassID = frm.lkuVendClassID

            If iGetExchMode = 1 Then
                muPODflts.bFixedExchRate = True
                muPODflts.bSetCurrIDFromPO = True
            End If

            If (Trim$(frm.lkuVendID.Text) = "") And (Trim$(muPODflts.sVendID) <> "") Then
                muPODflts.bReturnedVendor = True
            End If

            If muPODflts.bReturnedVendor Then
                '-- The Vendor was returned from the PO,
                '-- so set the Vendor info on the form
                frm.lkuVendID.SetTextAndKeyValue muPODflts.sVendID, muPODflts.lVendKey
                frm.bIsValidVendID ""
            End If

            '-- Set Vendor Class info from the PO
            If (frm.grdVouchDetl.DataRowCnt = 0) Then
                'Use the Vendor class from this PO
                frm.lkuVendClassID.SetTextAndKeyValue sVendClassID, lVendClassKey
            Else
                'If the Vendor Class is different on this PO from the last, warn the user
                If (lVendClassKey <> lOrigVendClassKey) Then
                    giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOHasDiffVendClass, sVendClassID, sOrigVendClassID
                End If

                'Restore the original Vendor Class, since changing the vendor will also update the vendor class
                frm.lkuVendClassID.SetTextAndKeyValue sOrigVendClassID, lOrigVendClassKey
            End If

            '-- Automatically show the Select PO Lines dialog, or
            '-- automatically load the voucher detail grid for the
            '-- PO number, depending on options
            ToggleCtrlState frm.cmdLines(kPO), True
            If mbAutoSelectLines Then
                mbInternalLinesClick = True
            Else
                mbInternalLinesClick = False
            End If

            If muPODflts.bSetCurrIDFromPO Then
                frm.bSetNewVoucherCurr muPODflts.sCurrID
                frm.txtCurrID = muPODflts.sCurrID
            End If

            '-- Automatically show the Select PO Lines dialog
            frm.cmdLines_Click kPO
        End If
    Else
        ToggleCtrlState frm.cmdLines(kPO), False
        bValid = True
    End If

    muPODflts.sPONo = sPONo

    bIsValidPONo = bValid
    
    If bIsValidPONo = True Then
        frm.lkuPONo.Tag = frm.lkuPONo.Text
    End If
    

    Set oClass = Nothing

    mbValidatingPO = False
    
    Exit Function

ExpectedErrorRoutine:
    MyErrMsg oClass, Err.Description, Err, sMyName, "bIsValidPONo"
    gClearSotaErr
    Exit Function
End Function

Private Function iCheckCurrency(oClass As Object, frm As Form, sPONo As String, sDocStr, sDocNo As String, bRetMsg As Boolean, sMessage As String) As Integer
Dim iReply As Integer

    If bNewPOChosen(frm, sPONo) Then
        If bRetMsg Then
            sMessage = gsBuildMessage(kmsgPODiffCurrNotAllowed, oClass.moSysSession.Language, oClass.moAppDB, sDocStr, sDocStr)
        Else
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgPODiffCurrNotAllowed, "PO", sDocStr
        End If
        iReply = kretNo
    Else
        iReply = giSotaMsgBox(Nothing, oClass.moSysSession, kmsgPODiffCurr, _
            sDocStr, muPODflts.sCurrID, frm.txtCurrID.Text, sDocStr)
            
        If (iReply = kretNo) Then
            sMessage = gsBuildMessage(kmsgPODocCurrDiffThanHC, oClass.moSysSession.Language, oClass.moAppDB, _
                           sDocStr, sDocNo)
        End If
    End If
    iCheckCurrency = iReply
End Function
Private Function iCheckExchRate(oClass, frm As Form, sPONo As String, sDocStr, sDocNo As String, _
    dDocCurrExchRate, bRetMsg As Boolean, sMessage As String) As Integer
Dim iReply As Integer
    
    If bNewPOChosen(frm, sPONo) Then
        If bRetMsg Then
            sMessage = gsBuildMessage(kmsgPODiffExchRateNotAllowed, oClass.moSysSession.Language, oClass.moAppDB, sDocStr)
        Else
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgPODiffExchRateNotAllowed, sDocStr
        End If
        iReply = kretNo
    Else
        iReply = giSotaMsgBox(Nothing, oClass.moSysSession, kmsgPODiffExchRate, _
            sDocStr, muPODflts.dCurrExchRate, dDocCurrExchRate, sDocStr)

        If (iReply = kretNo) Then
            sMessage = gsBuildMessage(kmsgPODocDiffExchRate, oClass.moSysSession.Language, oClass.moAppDB, _
                           sDocStr, sDocNo)
        End If
    End If
    iCheckExchRate = iReply
End Function

Public Function bIsValidRcvrNo(frm As Form, oDmGrid As clsDmGrid, _
                               oLEGrid As clsLineEntry, iUseMultCurr As Integer, _
                               dDocCurrExchRate As Double, Optional bFromVendID As Boolean = False, _
                               Optional sMessage As String = "") As Boolean
'+++ VB/Rig Skip +++
'******************************************************************
'    Desc: This function will validate the entered receiver number.
' Returns: True if the PO number is valid, else false.
'******************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim bValid As Boolean
    Dim sCompanyID As String
    Dim sPONo As String
    Dim sPORelNo As String
    Dim sRcvrNo As String
    Dim iRetVal As iPOValidVals
    Dim iTempRetVal As iPOValidVals
    Dim iReply As Integer
    Dim bRetMsg As Boolean
    Dim sID As String
    Dim sUser As String
    Dim vPrompt As Variant
    Dim oClass As Object
    Dim sRcvrStr As String
    Dim lTranType As Long
    Dim lVendClassKey As Long
    Dim sVendClassID As String
    Dim lOrigVendClassKey As Long
    Dim sOrigVendClassID As String
    Dim iGetExchMode As Integer
    Dim rs As Object
    Dim sSQL As String
    Dim sDocCurrExchRate As String

    mbValidatingPO = True
    
    Set oClass = frm.oClass
        
    If (sMessage = kDisplayMsg) Then
        bRetMsg = False
    Else
        bRetMsg = True
    End If
    
    bValid = False
    
    With muPODflts
        .lVendKey = -1
        .bReturnedVendor = False
        .sCurrID = ""
        .bSetCurrIDFromPO = False
        .bPONoChanged = False
        .bRcvrNoChanged = False
    End With
    
    sCompanyID = oClass.moSysSession.CompanyID
    
    sRcvrStr = gsBuildString(kReceiver, oClass.moAppDB, oClass.moSysSession)
    
    sRcvrNo = frm.lkuRcvrNo
    
    sDocCurrExchRate = CStr(dDocCurrExchRate)       'Passing a VB double to a SP that expects a float
                                                    'may cause the value to change.  This is a suspected DAS
                                                    'issue.  Passing it as a string is a work-around.
    
    If (frm.ddnTranType.ItemData = kTranTypeAPDM) Then
        lTranType = kTranTypePORTrn
    Else
        lTranType = kTranTypePORG
    End If
    
    If (Len(Trim$(sRcvrNo)) > 0) Or bFromVendID Then
        With oClass.moAppDB
            .SetInParam sCompanyID
            .SetInParam frm.txtRcvrCompanyID.Text
            .SetInParam sRcvrNo
            .SetInParam frm.lkuVendID.Text
            .SetInParam frm.txtCurrID.Text
            .SetInParam sDocCurrExchRate
            .SetInParam lTranType
            
            .SetOutParam muPODflts.lPOKey
            .SetOutParam muPODflts.sVendID
            .SetOutParam muPODflts.lVendKey
            .SetOutParam muPODflts.lRemitToAddrKey
            .SetOutParam muPODflts.lRemitToVendAddrKey
            .SetOutParam muPODflts.lPurchFromAddrKey
            .SetOutParam muPODflts.lPurchFromVendAddrKey
            .SetOutParam muPODflts.lPmtTermsKey
            .SetOutParam muPODflts.sCurrID 'Receiver currency ID
            .SetOutParam muPODflts.lCurrExchSchdKey 'always 0 for Receiver
            .SetOutParam muPODflts.dCurrExchRate 'Receiver exchange rate
            .SetOutParam iGetExchMode
            .SetOutParam muPODflts.i1099Type
            .SetOutParam muPODflts.i1099Form
            .SetOutParam muPODflts.s1099Box
            .SetOutParam muPODflts.s1099BoxText
            .SetOutParam muPODflts.lShipMethKey
            .SetOutParam muPODflts.sShipMethID
            .SetOutParam muPODflts.lFOBKey
            .SetOutParam muPODflts.sFOBID
            .SetOutParam muPODflts.sTranCmnt
            .SetOutParam muPODflts.dFreightAmt
            .SetOutParam muPODflts.dSTaxAmt
            .SetOutParam muPODflts.lSTaxTranKey
            .SetOutParam muPODflts.lRcvrKey
            .SetOutParam sPONo
            .SetOutParam sPORelNo
            .SetOutParam lVendClassKey
            .SetOutParam sVendClassID
            .SetOutParam iRetVal
            
            .ExecuteSP "sppoValRcvrForInvc"
            
            muPODflts.lPOKey = glGetValidLong(.GetOutParam(8))
            muPODflts.sVendID = gsGetValidStr(.GetOutParam(9))
            muPODflts.lVendKey = glGetValidLong(.GetOutParam(10))
            muPODflts.lRemitToAddrKey = glGetValidLong(.GetOutParam(11))
            muPODflts.lRemitToVendAddrKey = glGetValidLong(.GetOutParam(12))
            muPODflts.lPurchFromAddrKey = glGetValidLong(.GetOutParam(13))
            muPODflts.lPurchFromVendAddrKey = glGetValidLong(.GetOutParam(14))
            muPODflts.lPmtTermsKey = glGetValidLong(.GetOutParam(15))
            muPODflts.sCurrID = gsGetValidStr(.GetOutParam(16))
            muPODflts.lCurrExchSchdKey = glGetValidLong(.GetOutParam(17))
            muPODflts.dCurrExchRate = gdGetValidDbl(.GetOutParam(18))
            iGetExchMode = giGetValidInt(.GetOutParam(19))
            muPODflts.i1099Type = giGetValidInt(.GetOutParam(20))
            muPODflts.i1099Form = giGetValidInt(.GetOutParam(21))
            muPODflts.s1099Box = gsGetValidStr(.GetOutParam(22))
            muPODflts.s1099BoxText = gsGetValidStr(.GetOutParam(23))
            muPODflts.lShipMethKey = glGetValidLong(.GetOutParam(24))
            muPODflts.sShipMethID = gsGetValidStr(.GetOutParam(25))
            muPODflts.lFOBKey = glGetValidLong(.GetOutParam(26))
            muPODflts.sFOBID = gsGetValidStr(.GetOutParam(27))
            muPODflts.sTranCmnt = gsGetValidStr(.GetOutParam(28))
            muPODflts.dFreightAmt = gdGetValidDbl(.GetOutParam(29))
            muPODflts.dSTaxAmt = gdGetValidDbl(.GetOutParam(30))
            muPODflts.lSTaxTranKey = glGetValidLong(.GetOutParam(31))
            muPODflts.lRcvrKey = glGetValidLong(.GetOutParam(32))
            sPONo = gsGetValidStr(.GetOutParam(33))
            sPORelNo = gsGetValidStr(.GetOutParam(34))
            lVendClassKey = glGetValidLong(.GetOutParam(35))
            sVendClassID = gsGetValidStr(.GetOutParam(36))
            iRetVal = .GetOutParam(37)
            .ReleaseParams
        End With
               
        With oClass
            Select Case iRetVal
                Case kErrPONotExist
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgInvalidField, .moSysSession.Language, .moAppDB, frm.lblRcvrNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgInvalidField, frm.lblRcvrNo
                    End If
                    
                Case kErrPONotStandard
                    If bRetMsg Then
                        sMessage = gsBuildMessage(kmsgRcvrNotStandard, .moSysSession.Language, .moAppDB, sRcvrNo)
                    Else
                        giSotaMsgBox Nothing, .moSysSession, kmsgRcvrNotStandard, sRcvrNo
                    End If
                        
                Case kErrPOBadVendor
                    If bFromVendID Then
                        '-- There was an existing Rcvr number and the vendor was changed,
                        '-- making the Rcvr invalid. Therefore, blank out the Rcvr number.
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgExistingPONotValid, .moSysSession.Language, .moAppDB, sRcvrStr, sRcvrNo, frm.lkuVendID, sRcvrStr)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgExistingPONotValid, sRcvrStr, sRcvrNo, frm.lkuVendID, sRcvrStr
                        End If
                        frm.lkuRcvrNo.SetTextAndKeyValue Empty, Empty
                        mbInVendID = True
                        DeletePOLinesForVend frm, oDmGrid, oLEGrid
                        bIsValidRcvrNo frm, oDmGrid, oLEGrid, iUseMultCurr, dDocCurrExchRate
                        mbInVendID = False
                        frm.lkuRcvrNo_PostLostFocus
                    Else
                        If bRetMsg Then
                            sMessage = gsBuildMessage(kmsgPONotValidForVendor, .moSysSession.Language, .moAppDB, sRcvrStr, sRcvrNo, frm.lkuVendID)
                        Else
                            giSotaMsgBox Nothing, .moSysSession, kmsgPONotValidForVendor, sRcvrStr, sRcvrNo, frm.lkuVendID
                        End If
                    End If
                    
                Case kErrPODiffCurr
                    If (iUseMultCurr = kiOn) Then
                        iReply = iCheckCurrency(oClass, frm, sPONo, sRcvrStr, sRcvrNo, bRetMsg, sMessage)
                    End If
                    
                    If (iReply = kretYes) Or (iUseMultCurr = kiOff) Then
                        iRetVal = kPOValid
                        muPODflts.bSetCurrIDFromPO = True
                        frm.RefreshCashAcctSQL muPODflts.sCurrID, True
                    End If
                
                Case kErrPODiffExchRate
                    If (iUseMultCurr = kiOn) And iGetExchMode <> 1 Then
                        iReply = iCheckExchRate(oClass, frm, sPONo, sRcvrStr, sRcvrNo, dDocCurrExchRate, bRetMsg, sMessage)
                    End If
                    
                    If (iReply = kretYes) Or (iUseMultCurr = kiOff) Then
                        iRetVal = kPOValid
                        muPODflts.bSetCurrIDFromPO = True
                    ElseIf ((iReply = kretNo) And (iUseMultCurr = kiOn)) Or iGetExchMode = 1 Then
                        iRetVal = kPOValid
                    End If
                
                Case kErrPOBlankVendor
                    iRetVal = kPOValid
            
            End Select
        End With
            
        '-- The Rcvr is valid
        If (iRetVal = kPOValid) Then
            bValid = True
            
            muPODflts.bRcvrNoChanged = True
            
                     
            '-- Save the original PO and vendor class information
            lOrigVendClassKey = frm.lkuVendClassID.KeyValue
            sOrigVendClassID = frm.lkuVendClassID
            
            If iGetExchMode = 1 Then
                muPODflts.bFixedExchRate = True
                muPODflts.bSetCurrIDFromPO = True
            End If
            
            'Exchange rate is always fixed for receivers.
            'muPODflts.bFixedExchRate = True
                                    
            frm.lkuRcvrNo.SetTextAndKeyValue sRcvrNo, muPODflts.lRcvrKey
            
            If (Trim$(frm.lkuVendID.Text) = "") And (Trim$(muPODflts.sVendID) <> "") Then
                muPODflts.bReturnedVendor = True
            End If
            
            If muPODflts.bReturnedVendor Then
                '-- The Vendor was returned from the PO,
                '-- so set the Vendor info on the form
                frm.lkuVendID.SetTextAndKeyValue muPODflts.sVendID, muPODflts.lVendKey
                frm.bIsValidVendID ""
            End If
                
            '-- Default the PO No. if it's blank
'            frm.lkuPONo.SetTextAndKeyValue sPORelNo, muPODflts.lPOKey
'            frm.lkuPONo_PostLostFocus
                
            '-- Set Vendor Class info from the PO
            If (frm.grdVouchDetl.DataRowCnt = 0) Then
                'Use the Vendor class from this PO
                frm.lkuVendClassID.SetTextAndKeyValue sVendClassID, lVendClassKey
            Else
                'If the Vendor Class is different on this PO from the last, warn the user
                If (lVendClassKey <> lOrigVendClassKey) Then
                    giSotaMsgBox Nothing, oClass.moSysSession, kmsgRcvrPOHasDiffVendClass, sVendClassID, sOrigVendClassID
                End If
                
                'Restore the original Vendor Class, since changing the vendor will also update the vendor class
                frm.lkuVendClassID.SetTextAndKeyValue sOrigVendClassID, lOrigVendClassKey
            End If
                
            '-- Automatically show the Select Receiver Lines dialog, or
            '-- automatically load the voucher detail grid for the
            '-- Rcvr number, depending on options
            ToggleCtrlState frm.cmdLines(kRcvr), True
            If mbAutoSelectLines Then
                mbInternalLinesClick = True
            Else
                mbInternalLinesClick = False
            End If
            
            
            If muPODflts.bSetCurrIDFromPO Then
                frm.bSetNewVoucherCurr muPODflts.sCurrID
                frm.txtCurrID = muPODflts.sCurrID
                
            End If
            
            '-- Automatically show the Select Receiver Lines dialog
            frm.cmdLines_Click kRcvr
        End If
    Else
        ToggleCtrlState frm.cmdLines(kRcvr), False
        bValid = True
    End If
    
    muPODflts.sRcvrNo = sRcvrNo
    
    bIsValidRcvrNo = bValid
    
    'Clear the PO controls and disable the PO lines command.  Selection of
    'a receipt could invalidate any unselected PO lines if the exchange rate differs.
    If bIsValidRcvrNo = True Then
        frm.txtPONo.Text = Empty
        frm.txtRelNo.Text = Empty
        frm.lkuPONo.SetTextAndKeyValue Empty, Empty
        frm.lkuPONo_PostLostFocus 'disable lines command
    End If
    
    Set oClass = Nothing
    
    mbValidatingPO = False
    
    Exit Function

ExpectedErrorRoutine:
    MyErrMsg oClass, Err.Description, Err, sMyName, "bIsValidRcvrNo"
    gClearSotaErr
    Exit Function
End Function

Private Function bNewPOChosen(frm As Form, sPONo As String) As Boolean
'*****************************************************************
'    Desc: This function will determine if lines from a different
'          PO than the one chosen exist on the voucher.
'*****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sGridPO As String
    
    bNewPOChosen = False
    
    If (sPONo <> "") Then
        '-- Check the voucher detail grid to see if any lines are
        '-- associated with another
        With frm
            For lRow = 1 To .grdVouchDetl.DataRowCnt
                sGridPO = gsGetValidStr(gsGridReadCellText(.grdVouchDetl, lRow, kColPONo))
                If (sGridPO <> "") And (sGridPO <> sPONo) Then
                    bNewPOChosen = True
                    Exit For
                End If
            Next lRow
        End With
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bNewPOChosen", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub DeletePOLinesForVend(frm As Form, oDmGrid As clsDmGrid, oLEGrid As clsLineEntry)
'**********************************************************
' Desc: This routine will delete all lines from the voucher
'       detail grid associated with the old PO number since
'       the vendor was changed.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim lOrigMaxRows As Long
    Dim lRowsChecked As Long
    Dim lPOLineKey As Long
        
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdVouchDetl, False
        
        lOrigMaxRows = .grdVouchDetl.DataRowCnt
        lRow = lOrigMaxRows
        lRowsChecked = 0
        
        Do While (lRowsChecked <= lOrigMaxRows) And (lRow > 0)
            lPOLineKey = glGetValidLong(gsGridReadCell(.grdVouchDetl, lRow, kColPOLineKey))
            
            If (lPOLineKey > 0) Then
                mbManualClick = True
                
                '-- Set the row to be deleted to the current row
                oLEGrid.Grid_Click 1, lRow
                
                '-- Delete the row
                GridDeleteRow frm, oDmGrid, oLEGrid, lRow
            
                mbManualClick = False
            End If
            
            lRow = lRow - 1
            lRowsChecked = lRowsChecked + 1
        Loop
        
        '-- Turn redraw of the grid back on
        SetGridRedraw .grdVouchDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeletePOLinesForVend", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub DeleteZeroQtyPOLinesFromGrid(frm As Form, oDmGrid As clsDmGrid, oLEGrid As clsLineEntry)
'**********************************************************
' Desc: This routine will delete all lines from the voucher
'       detail grid associated with a PO number that have a
'       quantity of 0 and are not Comment Only lines.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim lOrigMaxRows As Long
    Dim lRowsChecked As Long
    Dim sPOLineKey As String
    Dim dQty As Double
    Dim bCmntOnly As Boolean
    
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdVouchDetl, False
    
        lOrigMaxRows = .grdVouchDetl.DataRowCnt
        lRow = lOrigMaxRows
        lRowsChecked = 0
        
        Do While (lRowsChecked <= lOrigMaxRows) And (lRow > 0)
            sPOLineKey = Trim$(gsGridReadCell(.grdVouchDetl, lRow, kColPOLineKey))
            dQty = CDbl(gsGridReadCell(.grdVouchDetl, lRow, kColQuantity))
            bCmntOnly = CBool(gsGridReadCell(.grdVouchDetl, lRow, kColCmntOnly))
            
            If (sPOLineKey <> "") And (dQty = 0) And (Not bCmntOnly) Then
                mbManualClick = True
                
                '-- Set the row to be deleted to the current row
                oLEGrid.Grid_Click 1, lRow
                
                '-- Delete the row
                GridDeleteRow frm, oDmGrid, oLEGrid, lRow
            
                mbManualClick = False
            End If
            
            lRow = lRow - 1
            lRowsChecked = lRowsChecked + 1
        Loop

        '-- Turn redraw of the grid back on
        SetGridRedraw .grdVouchDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DeleteZeroQtyPOLinesFromGrid", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub GridDeleteRow(frm As Form, oDmGrid As clsDmGrid, oLEGrid As clsLineEntry, lRow As Long)
'**********************************************************
' Desc: This routine will delete all lines from the voucher
'       detail grid associated with the old PO number. This
'       is a bastardized version of the GridDelete sub in
'       the line entry class.
'**********************************************************
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
    
    Dim oGrid As Object
    Dim bResponse As Boolean
    Dim lError As Long
    
    Set oGrid = frm.grdVouchDetl
    
    bResponse = True 'Assume it is OK to continue
    
    On Error Resume Next
    bResponse = frm.LEGridBeforeDelete(oLEGrid, lRow)
    lError = Err.Number
    On Error GoTo ExpectedErrorRoutine
    If lError <> 438 And lError <> 0 Then Err.Raise lError
    If bResponse = False Then Exit Sub
    
    If oDmGrid.DeleteBlock(lRow) = kDmSuccess Then
        On Error Resume Next
        frm.LEGridAfterDelete oLEGrid, lRow
        On Error GoTo ExpectedErrorRoutine
        lError = Err.Number
        If lError <> 438 And lError <> 0 Then Err.Raise lError
        oLEGrid.Grid_Click 1, lRow, True
    
        '-- Set global flag indicating that lines were deleted from
        '-- the detail grid, so tax and terms need to be recalculated
        mbLineDeleted = True
    End If
    
    Set oGrid = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GridDeleteRow", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ReopenPOLines(frm As Form, oDmGrid As clsDmGrid)
'**********************************************************
' Desc: This routine will set the MatchStatus to "Open" for
'       all voucher lines associated with a purchase order.
'       This will only be called if the currency exchange
'       rate is changed.
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim iMatchStatus As Integer
    Dim sPOLineKey As String
    Dim bCmntOnly As Boolean
        
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdVouchDetl, False
        
        For lRow = 1 To .grdVouchDetl.DataRowCnt
            sPOLineKey = Trim$(gsGridReadCell(.grdVouchDetl, lRow, kColPOLineKey))
            iMatchStatus = Val(gsGridReadCell(.grdVouchDetl, lRow, kColMatchStatus))
            bCmntOnly = CBool(gsGridReadCell(.grdVouchDetl, lRow, kColCmntOnly))
            
            If (sPOLineKey <> "") And (Not bCmntOnly) And _
               ((iMatchStatus = kMatchStatusClosed) Or (iMatchStatus = kMatchStatusSoftOvrd)) Then
                '-- Update the line's MatchStatus to "Open"
                gGridUpdateCell .grdVouchDetl, lRow, kColMatchStatus, kMatchStatusOpen
                gGridUpdateCellText .grdVouchDetl, lRow, kColDispMatchStatus, msOpenString
            
                '-- Need to inform DM of changed row
                oDmGrid.SetRowDirty lRow
            End If
        Next lRow

        '-- Turn redraw of the grid back on
        SetGridRedraw .grdVouchDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ReopenPOLines", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub ClearReturnTypeLines(frm As Form, oDmGrid As clsDmGrid)
'**********************************************************
' Desc: This routine will clear the Return Type column for
'       all voucher lines associated with a purchase order.
'       This is called when the Voucher Type is changed to
'       something other than "Credit Memo".
'**********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sPOLineKey As String
        
    With frm
        '-- Turn redraw of the grid off so no flashing occurs
        SetGridRedraw .grdVouchDetl, False
        
        For lRow = 1 To .grdVouchDetl.DataRowCnt
            sPOLineKey = Trim$(gsGridReadCell(.grdVouchDetl, lRow, kColPOLineKey))
            
            If (sPOLineKey <> "") Then
                '-- Update the line's ReturnType to "None"
                gGridUpdateCell .grdVouchDetl, lRow, kColReturnType, kReturnNone
                gGridUpdateCellText .grdVouchDetl, lRow, kColDispReturnType, ""
            
                '-- Need to inform DM of changed row
                oDmGrid.SetRowDirty lRow
            End If
        Next lRow

        '-- Turn redraw of the grid back on
        SetGridRedraw .grdVouchDetl, True
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearReturnTypeLines", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub SetGridRedraw(grd As Object, bFlag As Boolean)
'*********************************************************
' Desc: This routine will use the Windows API and the grid
'       methods to turn redraw on/off. The Windows API
'       is used because it WORKS!, whereas the grid method
'       is flaky!
'*********************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With grd
        SendMessage .hwnd, WM_SETREDRAW, bFlag, 0
        .redraw = bFlag
        If bFlag Then .Refresh
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridRedraw", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub CreatePONoLinkForGrid(oAppDB As Object, sCompanyID As String, _
                                 lVouchKey As Long, lVRunMode As Long)
'+++ VB/Rig Skip +++
'*******************************************************
' Desc: This routine will create a temp table containing
'       the PO number so that data manager can join with
'       the temp table to display the full PO number in
'       the voucher detail grid.
'*******************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim sSQL As String
    Static bDid As Boolean

    If (Not bDid) Then
        On Error Resume Next
        '-- Batch these 2 queries
        sSQL = "DROP TABLE #tpoPOJoin" & vbCrLf
        
        sSQL = sSQL & "CREATE TABLE #tpoPOJoin "
        sSQL = sSQL & "(POLineKey INT      NULL, "
        sSQL = sSQL & "POLineNo   SMALLINT NULL, "
        sSQL = sSQL & "PONo       varCHAR(15) NULL, "
        sSQL = sSQL & "FixedCurrExchRate SMALLINT)"
        oAppDB.ExecuteSQL sSQL

        On Error GoTo ExpectedErrorRoutine
        
        bDid = True
    End If
    
    '-- Populate the temp table
    sSQL = "{call sppoGetVouchDetl (" & gsQuoted(sCompanyID) & ", " & lVouchKey & ")}"
    oAppDB.ExecuteSQL sSQL
    
    Exit Sub

ExpectedErrorRoutine:
    MyErrMsg goClass, Err.Description, Err, sMyName, "CreatePONoLinkForGrid"
    gClearSotaErr
    Exit Sub
End Sub

Public Function bUpdatePOVouchLink(oClass As Object, oAppDB As Object, oDm As Object, _
                                   lVoucherKey As Long) As Boolean
'+++ VB/Rig Skip +++
'*******************************************************
' Desc: This routine will create/update PO-Voucher link
'       records for the just-saved voucher.
'*******************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim iRetVal As Integer

    bUpdatePOVouchLink = False
    
    With oAppDB
        .SetInParam lVoucherKey
        .SetOutParam iRetVal
        
        .ExecuteSP "sppoUpdatePOVchLnk"
        
        iRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    bUpdatePOVouchLink = True
    
    Exit Function

ExpectedErrorRoutine:
    oDm.CancelAction
    MyErrMsg oClass, Err.Description, Err, sMyName, "bUpdatePOVouchLink"
    gClearSotaErr
    Exit Function
End Function

Public Function bDeleteRcvrVouchLink(oClass As Object, oAppDB As Object, oDm As Object, _
                                     lVoucherKey As Long) As Boolean
'+++ VB/Rig Skip +++
'********************************************************
' Desc: This routine will delete Rcvr-Voucher link
'       records for the just-deleted voucher.
'********************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim iRetVal As Integer

    bDeleteRcvrVouchLink = False
    
    With oAppDB
        .SetInParam lVoucherKey
        .SetOutParam iRetVal
        
        .ExecuteSP "sppoDeleteRcvVcLnk"
        
        iRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    bDeleteRcvrVouchLink = True
    
    Exit Function

ExpectedErrorRoutine:
    oDm.CancelAction
    MyErrMsg oClass, Err.Description, Err, sMyName, "bDeleteRcvrVouchLink"
    gClearSotaErr
    Exit Function
End Function

Public Function bUpdateRcvrVouchLink(frm As Form, oClass As Object, oAppDB As Object, _
                                     oDm As Object, lVoucherKey As Long) As Boolean
'+++ VB/Rig Skip +++
'********************************************************
' Desc: This routine will create/update Rcvr-Voucher link
'       records for the just-saved voucher.
'********************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim lRcvrLineKey As Long
    Dim lVoucherLineKey As Long
    Dim iRetVal As Integer
    Dim lRow As Long
    Dim sSQL As String
    Dim oGrd As Object

    bUpdateRcvrVouchLink = False
    
    If Not bDeleteRcvrVouchLink(oClass, oAppDB, oDm, lVoucherKey) Then
        Exit Function
    End If
    
    '-- Now re-create the Rcvr/Voucher link
    Set oGrd = frm.grdVouchDetl
    
    For lRow = 1 To oGrd.DataRowCnt
        lRcvrLineKey = glGetValidLong(gsGridReadCell(oGrd, lRow, kColRcvrLineKey))
        lVoucherLineKey = glGetValidLong(gsGridReadCell(oGrd, lRow, kColVoucherLineKey))
        
        If (lRcvrLineKey > 0) Then
            With oAppDB
                .SetInParam lVoucherKey
                .SetInParam lVoucherLineKey
                .SetInParam lRcvrLineKey
                .SetOutParam iRetVal
                
                .ExecuteSP "sppoCreateRcvVcLnk"
                
                iRetVal = .GetOutParam(4)
                .ReleaseParams
            End With
        End If
    Next lRow
    
    Set oGrd = Nothing
    
    bUpdateRcvrVouchLink = True
    
    Exit Function

ExpectedErrorRoutine:
    Set oGrd = Nothing
    oDm.CancelAction
    MyErrMsg oClass, Err.Description, Err, sMyName, "bUpdateRcvrVouchLink"
    gClearSotaErr
    Exit Function
End Function

Private Function bIsPOActive(oSysDB As Object, sCompanyID As String) As Boolean
'+++ VB/Rig Skip +++
'********************************************************
' Desc: This routine will determine if the PO module has
'       been activated.
'********************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim sSQL As String
    Dim rs As Object
    
    bIsPOActive = False
    
    sSQL = "SELECT Active FROM tsmCompanyModule "
    sSQL = sSQL & "WHERE CompanyID = " & gsQuoted(sCompanyID)
    sSQL = sSQL & " AND ModuleNo = " & kModulePO

    Set rs = oSysDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        bIsPOActive = gbGetValidBoolean(rs.Field("Active"))
    End If
    
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
    Exit Function

ExpectedErrorRoutine:
    MyErrMsg goClass, Err.Description, Err, sMyName, "bIsPOActive"
    gClearSotaErr
    Exit Function
End Function

Public Sub PerformPreMatching(frm As Form, oClass As Object, lPOLineKey As Long, lItemKey As Long, _
                              sItemID As String, dInvQty As Double, dInvExtAmt As Double, _
                              lInvUOMKey As Long, iRcptReq As Integer)
'+++ VB/Rig Skip +++
'******************************************************************
' Desc: This routine will perform pre-matching on the current line.
'       This involves checking quantity and extended amount on the
'       voucher (invoice) against the purchase order.
'******************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim dPOQty As Double
    Dim dPOUnitCost As Double
    Dim dPOExtAmt As Double
    Dim lPOUOMKey As Long
    Dim dConvFactor As Double
    Dim lRetVal As Long
    Dim bMatchErr As Boolean
    Dim sSQL As String
    Dim rs As Object
    Dim dOldExtAmt As Double
    
    bMatchErr = False
    
    '-- Get the PO unit of measure
    sSQL = "SELECT UnitMeasKey FROM tpoPOLine "
    sSQL = sSQL & "WHERE POLineKey = " & lPOLineKey
    
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    lPOUOMKey = 0
    If Not rs.IsEOF Then
        lPOUOMKey = glGetValidLong(rs.Field("UnitMeasKey"))
    End If
    
    '-- Get the UOM conversion, if necessary
    If (lInvUOMKey <> lPOUOMKey) Then
        dConvFactor = 1
        With oClass.moAppDB
            .SetInParam oClass.moSysSession.CompanyID
            .SetInParam lInvUOMKey
            .SetInParam lPOUOMKey
            .SetInParam lItemKey
            
            .SetOutParam dInvQty      'Quantity
            .SetOutParam dConvFactor  'Conversion Factor
            .SetOutParam lRetVal
        
            .ExecuteSP "spimIMSUnitOfMeasConv"
            
            ' Do the error handling before loading the properties.
            ' SO if error has occured then it will not load properties
            lRetVal = .GetOutParam(7)
            If lRetVal = 0 Then
                dInvQty = gdGetValidDbl(.GetOutParam(5))
            End If
            .ReleaseParams
        End With
    End If
    
    '-- Check the quantities
    If (iRcptReq = kiOff) Then
        sSQL = "SELECT ISNULL(QtyOrd,0) - ISNULL(QtyInvcd,0) QtyRemain "
    Else
        sSQL = "SELECT ISNULL(QtyOpenToRcv,0) QtyRemain "
    End If
    sSQL = sSQL & "FROM tpoPOLineDist WHERE POLineKey = " & lPOLineKey
    
    Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    dPOQty = 0
    If Not rs.IsEOF Then
        dPOQty = gdGetValidDbl(rs.Field("QtyRemain"))
    End If

    'Compare PO Qty to the UOM converted InvQty
    If dInvQty > dPOQty Then
        bMatchErr = True
        giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOMatchQtyErr, Trim$(sItemID)
    End If
    
    '-- Only display one matching error
    If Not bMatchErr Then
        rs.Close
        
        '-- Check the unit cost/extended amount
        sSQL = "SELECT UnitCost FROM tpoPOLine "
        sSQL = sSQL & "WHERE POLineKey = " & lPOLineKey
        
        Set rs = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        dPOUnitCost = 0
        If Not rs.IsEOF Then
            dPOUnitCost = gdGetValidDbl(rs.Field("UnitCost"))
        End If
    
        '-- Compare the correct number of decimal places
        dPOExtAmt = dPOQty * dPOUnitCost
        dOldExtAmt = frm.curExtAmt
        frm.curExtAmt = dPOExtAmt
        dPOExtAmt = frm.curExtAmt
        If (dInvExtAmt > dPOExtAmt) Then
            giSotaMsgBox Nothing, oClass.moSysSession, kmsgPOMatchAmtErr, Trim$(sItemID)
        End If
        frm.curExtAmt = dOldExtAmt
    End If
        
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
    Exit Sub

ExpectedErrorRoutine:
    MyErrMsg oClass, Err.Description, Err, sMyName, "PerformPreMatching"
    gClearSotaErr
    Exit Sub
End Sub

Public Sub ClearPODefaultsStruct()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With muPODflts
        .sPONo = ""
        .lPOKey = 0
        .bPONoChanged = False
        .sRcvrNo = ""
        .lRcvrKey = 0
        .bRcvrNoChanged = False
        .sVendID = ""
        .lVendKey = 0
        .lRemitToAddrKey = 0
        .lRemitToVendAddrKey = 0
        .lPurchFromAddrKey = 0
        .lPurchFromVendAddrKey = 0
        .lPmtTermsKey = 0
        .sCurrID = ""
        .lCurrExchSchdKey = 0
        .i1099Type = 0
        .i1099Form = 0
        .s1099Box = ""
        .s1099BoxText = ""
        .bReturnedVendor = False
        .bSetCurrIDFromPO = False
        .lShipMethKey = 0
        .sShipMethID = ""
        .lFOBKey = 0
        .sFOBID = ""
        .sTranCmnt = ""
        .dFreightAmt = 0
        .dSTaxAmt = 0
        .lSTaxTranKey = 0
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearPODefaultsStruct", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basProcRcptOfInvc"
End Function

