VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmSelectPOLines 
   Caption         =   "Select Purchase Order Lines"
   ClientHeight    =   5475
   ClientLeft      =   60
   ClientTop       =   1440
   ClientWidth     =   9450
   HelpContextID   =   68962
   Icon            =   "pozdb002.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5475
   ScaleWidth      =   9450
   ShowInTaskbar   =   0   'False
   Begin FPSpreadADO.fpSpread grdSelectPOLines 
      Height          =   3375
      Left            =   90
      TabIndex        =   9
      Top             =   1560
      WhatsThisHelpID =   68992
      Width           =   9255
      _Version        =   524288
      _ExtentX        =   16325
      _ExtentY        =   5953
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaxCols         =   1
      MaxRows         =   1
      SpreadDesigner  =   "pozdb002.frx":23D2
      AppearanceStyle =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   13
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   0
      Width           =   9450
      _ExtentX        =   16669
      _ExtentY        =   741
      Style           =   4
   End
   Begin VB.CommandButton cmdSelectClearAll 
      Caption         =   "&Select All"
      Height          =   360
      Index           =   0
      Left            =   90
      TabIndex        =   27
      Top             =   5040
      WhatsThisHelpID =   68989
      Width           =   915
   End
   Begin VB.CommandButton cmdSelectClearAll 
      Caption         =   "&Clear All"
      Height          =   360
      Index           =   1
      Left            =   1110
      TabIndex        =   26
      Top             =   5040
      WhatsThisHelpID =   68988
      Width           =   915
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   19
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin VB.CheckBox chkViewClosedLines 
      Caption         =   "&View Closed Lines"
      Height          =   285
      Left            =   4950
      TabIndex        =   8
      Top             =   1065
      WhatsThisHelpID =   68977
      Width           =   1695
   End
   Begin VB.Frame fraRcptOption 
      Height          =   585
      Left            =   90
      TabIndex        =   5
      Top             =   870
      Width           =   3675
      Begin VB.OptionButton optQtyMethod 
         Caption         =   "Invoice &Manually"
         Height          =   285
         Index           =   1
         Left            =   1830
         TabIndex        =   7
         Top             =   210
         WhatsThisHelpID =   68975
         Width           =   1785
      End
      Begin VB.OptionButton optQtyMethod 
         Caption         =   "Invoice in &Full"
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   210
         Value           =   -1  'True
         WhatsThisHelpID =   68974
         Width           =   1635
      End
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   14
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5085
      WhatsThisHelpID =   73
      Width           =   9450
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblLegend 
      AutoSize        =   -1  'True
      Caption         =   "("
      Height          =   195
      Index           =   2
      Left            =   6720
      TabIndex        =   12
      Top             =   1095
      Width           =   45
   End
   Begin VB.Label lblLegend 
      AutoSize        =   -1  'True
      Caption         =   "*"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   6780
      TabIndex        =   11
      Top             =   1125
      Width           =   75
   End
   Begin VB.Label lblLegend 
      AutoSize        =   -1  'True
      Caption         =   "indicates a Closed line)"
      Height          =   195
      Index           =   0
      Left            =   6900
      TabIndex        =   10
      Top             =   1095
      Width           =   1620
   End
   Begin VB.Label txtVendName 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   4950
      TabIndex        =   4
      Top             =   600
      UseMnemonic     =   0   'False
      Width           =   2535
   End
   Begin VB.Label txtVendID 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   3660
      TabIndex        =   3
      Top             =   600
      UseMnemonic     =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblVendor 
      AutoSize        =   -1  'True
      Caption         =   "Vendor:"
      Height          =   195
      Left            =   3000
      TabIndex        =   2
      Top             =   600
      Width           =   555
   End
   Begin VB.Label txtDocNo 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   1350
      TabIndex        =   1
      Top             =   600
      UseMnemonic     =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblDocNo 
      AutoSize        =   -1  'True
      Caption         =   "Purchase Order:"
      Height          =   195
      Left            =   90
      TabIndex        =   0
      Top             =   600
      Width           =   1155
   End
End
Attribute VB_Name = "frmSelectPOLines"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'   Name:
'       frmSelectPOLines
'
'   Description:
'       This form handles the user interface for selecting purchase
'       order lines onto a voucher.
'
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 05/08/97 JSP
'     Mods:
'***********************************************************************
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

    '-- Private variables of form properties
    Private moClass             As Object
    Private moMainForm          As Form
    Private mbShowUI            As Boolean
    Private mbDefaultQty        As Boolean
    Private msNatCurrID         As String
    Private miNatCurrDecPlaces  As Integer
    Private mdCurrExchRate      As Double
    Private miUnitCostDecPlaces As Integer
    Private miQtyDecPlaces      As Integer
    Private mlVoucherKey        As Long
    Private mlTranType          As Long
    Private msCompanyID         As String
    Private muMode              As iSelLinesMode
    
    '-- Context Menu Object
    Private moContextMenu       As clsContextMenu
    
    '-- Form resize variables
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    
    Private Const kClipSize = 1
    Private Const kiLineClosed = 2
    
    '-- Button constants
    Private Const kBtnSelectAll = 0
    Private Const kBtnClearAll = 1

Const VBRIG_MODULE_ID_STRING = "POZDB002.FRM"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = "POZ"
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = "POZ"
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Public Property Set oMainForm(oNewMainForm As Form)
'+++ VB/Rig Skip +++
    Set moMainForm = oNewMainForm
End Property

Public Property Let bShowUI(bNewShowUI As Boolean)
'+++ VB/Rig Skip +++
    mbShowUI = bNewShowUI
End Property

Public Property Get bShowUI() As Boolean
'+++ VB/Rig Skip +++
    bShowUI = mbShowUI
End Property

Public Property Let bDefaultQty(bNewDefaultQty As Boolean)
'+++ VB/Rig Skip +++
    mbDefaultQty = bNewDefaultQty
End Property

Public Property Let sNatCurrID(sNewNatCurrID As String)
'+++ VB/Rig Skip +++
    msNatCurrID = sNewNatCurrID
End Property

Public Property Let iNatCurrDecPlaces(iNewNatCurrDecPlaces As Integer)
'+++ VB/Rig Skip +++
    miNatCurrDecPlaces = iNewNatCurrDecPlaces
End Property

Public Property Let dCurrExchRate(dNewCurrExchRate As Double)
'+++ VB/Rig Skip +++
    mdCurrExchRate = dNewCurrExchRate
End Property

Public Property Let iUnitCostDecPlaces(iNewUnitCostDecPlaces As Integer)
'+++ VB/Rig Skip +++
    miUnitCostDecPlaces = iNewUnitCostDecPlaces
End Property

Public Property Let iQtyDecPlaces(iNewQtyDecPlaces As Integer)
'+++ VB/Rig Skip +++
    miQtyDecPlaces = iNewQtyDecPlaces
End Property

Public Property Let lVoucherKey(lNewVoucherKey As Long)
'+++ VB/Rig Skip +++
    mlVoucherKey = lNewVoucherKey
End Property

Public Property Let lTranType(lNewTranType As Long)
'+++ VB/Rig Skip +++
    mlTranType = lNewTranType
End Property

Public Property Let sCompanyID(sNewCompanyID As String)
'+++ VB/Rig Skip +++
    msCompanyID = sNewCompanyID
End Property

Public Property Let Mode(NewMode As iSelLinesMode)
'+++ VB/Rig Skip +++
    muMode = NewMode
End Property

Private Sub chkViewClosedLines_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkViewClosedLines, True
    #End If
'+++ End Customizer Code Push +++

    ViewClosedLines chkViewClosedLines.Value

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkViewClosedLines_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectClearAll_Click(Index As Integer)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelectClearAll(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'***************************************************************
' Desc: This routine will turn the Select checkbox on or off for
'       all lines, depending on the button pressed.
'***************************************************************
    Dim lRow As Long
    Dim sValue As String
    
    If (Index = kBtnSelectAll) Then
        sValue = CStr(vbChecked)
    Else
        sValue = CStr(vbUnchecked)
    End If
    
    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdSelectPOLines, False
    
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        '-- If the line is closed, and we're not showing
        '-- closed lines, don't select it
        If Val(gsGridReadCell(grdSelectPOLines, lRow, kColPOLineClosed)) <> kiLineClosed Then
            gGridUpdateCell grdSelectPOLines, lRow, kColPOSelect, sValue
        ElseIf (Val(gsGridReadCell(grdSelectPOLines, lRow, kColPOLineClosed)) = kiLineClosed) And _
                chkViewClosedLines.Value Then
            gGridUpdateCell grdSelectPOLines, lRow, kColPOSelect, sValue
        End If
    Next lRow
        
    '-- Turn redraw of the grid back on
    SetGridRedraw grdSelectPOLines, True
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Form_KeyDown traps the hot keys when the KeyPreview
'       property of the form is set to True.
' Parameters: Standard ones.
' Returns: N/A
'***********************************************************************
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
      
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'       of the form is set to True.
'********************************************************************
    Select Case KeyAscii
        Case vbKeyReturn
            If moClass.moSysSession.EnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

  Set sbrMain.Framework = moClass.moFramework
  sbrMain.BrowseVisible = False

    
    '-- Setup form resizing variables
    miMinFormHeight = Me.Height
    miMinFormWidth = Me.Width
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    
    '-- Setup form display
    SetupFormDisplay
    
    '-- Decide which quantity method to default to
    If mbDefaultQty Then
        optQtyMethod(0) = True
    Else
        optQtyMethod(1) = True
    End If
    
    '-- Setup the display of the grid
    InitializeGrid
    
    '-- Load the grid with all valid lines
    LoadLinesInGrid
    
    '-- Hide all closed lines initially
    ViewClosedLines False
    
    If mbShowUI Then
        ShowClosedLinesIndicator
        BindContextMenu
        SetupBars
    Else
        '-- Automatically select all lines
        cmdSelectClearAll_Click kBtnSelectAll
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

    '-- How are we being unloaded?
    Select Case UnloadMode
        Case vbFormCode
            'Do Nothing
        
        Case Else
            '-- Make sure quantities in grid are up-to-date
            LoadLinesInGrid
            
            '-- This form will be unloaded from the Voucher form,
            '-- after the lines have been loaded in the detail grid
            If mbShowUI Then
                Me.Hide
            End If
            GoTo CancelShutDown
    
    End Select
  
    '-- Perform a clean shutdown now
    PerformCleanShutDown
  
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
  
CancelShutDown:
    Cancel = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Clean up the project when shutting down.
' Parameters: N/A
' Returns: N/A
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    'Unload all forms loaded from this form
    'gUnloadChildForms Me
    
    Set moContextMenu = Nothing
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
        '-- Resize the grid and associated controls
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdSelectPOLines
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, grdSelectPOLines
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Sets all object and collection variables to nothing.
' Parameters: N/A
' Returns: N/A
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    Set moClass = Nothing
    Set moMainForm = Nothing
    
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupFormDisplay()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If (muMode = kPOLines) Then
        Me.Caption = "Select Purchase Order Lines"
        lblDocNo = "Purchase Order:"
        txtDocNo = moMainForm.lkuPONo
        
        chkViewClosedLines.Visible = True
        lblLegend(0).Visible = True
        lblLegend(1).Visible = True
        lblLegend(2).Visible = True
    Else
        If (mlTranType = kTranTypeAPDM) Then
            Me.Caption = "Select Return Lines"
            lblDocNo = "Return:"
        Else
            Me.Caption = "Select Receiver Lines"
            lblDocNo = "Receiver:"
        End If
        
        txtDocNo = moMainForm.lkuRcvrNo
        
        chkViewClosedLines.Visible = False
        lblLegend(0).Visible = False
        lblLegend(1).Visible = False
        lblLegend(2).Visible = False
    End If
    
    txtVendID = moMainForm.lkuVendID
    txtVendName = moMainForm.txtVendName
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupFormDisplay", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitializeGrid()
'*************************************************************
' Desc: This routine will set up the Select PO Lines grid with
'       all necessary properties.
'*************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sTitle As String
    Dim sItem As String
    Dim oSysSession As Object
    Dim oAppDB As Object
    Dim lCol As Long
    
    Set oSysSession = moClass.moSysSession
    Set oAppDB = moClass.moAppDB
    
    '-- Set default grid properties
    gGridSetProperties grdSelectPOLines, kPOGridMaxCols + 9, kGridDataSheet 'EB-JMM 1/10/2018
    grdSelectPOLines.ScrollBars = SS_SCROLLBAR_BOTH

    '-- Set column headers
    '-- Select
    sTitle = gsBuildString(kARSelect, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOSelect, sTitle
    
    '-- PO Line
    sTitle = gsBuildString(ksPOLineNum, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOPOLineNo, sTitle

    '-- Item
    sItem = gsBuildString(kItem, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOItemID, sItem
    
    '-- Quantity
    sTitle = gsBuildString(kQuantity, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOQty, sTitle
    
    '-- UOM
    sTitle = gsBuildString(kAnUOM, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOUOM, sTitle
    
    '-- Unit Cost
    sTitle = gsBuildString(kUnitCost, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOUnitCost, sTitle
    
    '-- Amount
    sTitle = gsBuildString(kAmount, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOAmount, sTitle
    
    '-- Item Description
    sTitle = sItem & " " & gsBuildString(kDescription, oAppDB, oSysSession)
    gGridSetHeader grdSelectPOLines, kColPOItemDesc, sTitle
            
    '-- Set grid column widths
    gGridSetColumnWidth grdSelectPOLines, kColPOSelect, 6
    gGridSetColumnWidth grdSelectPOLines, kColPOPOLineNo, 6
    gGridSetColumnWidth grdSelectPOLines, kColPOItemID, 14
    gGridSetColumnWidth grdSelectPOLines, kColPOQty, 7
    gGridSetColumnWidth grdSelectPOLines, kColPOUOM, 7
    gGridSetColumnWidth grdSelectPOLines, kColPOUnitCost, 9
    gGridSetColumnWidth grdSelectPOLines, kColPOAmount, 14
    gGridSetColumnWidth grdSelectPOLines, kColPOItemDesc, 18
    
    '-- Set the column types
    gGridSetColumnType grdSelectPOLines, kColPOSelect, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdSelectPOLines, kColPOPOLineNo, SS_CELL_TYPE_EDIT
    grdSelectPOLines.Col = kColPOPOLineNo: grdSelectPOLines.Row = -1
    grdSelectPOLines.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
    gGridSetColumnType grdSelectPOLines, kColPOUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColPOItemID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColPOItemDesc, SS_CELL_TYPE_EDIT, 40
    gGridSetColumnType grdSelectPOLines, kColPOExtCmnt, SS_CELL_TYPE_EDIT, 255
    gGridSetColumnType grdSelectPOLines, kColPOUnitCostExact, SS_CELL_TYPE_FLOAT, 13, 12
    '-- Lock the appropriate columns
    For lCol = 2 To kPOGridMaxCols
        gGridLockColumn grdSelectPOLines, lCol
    Next lCol
    
    '-- Setup currency columns for locale settings
    SetGridNumericAttr grdSelectPOLines, kColPOQty, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kColPOQtyInvc, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kColPOUnitCost, miUnitCostDecPlaces, kMaxCostLen
    SetGridNumericAttr grdSelectPOLines, kColPOAmount, miNatCurrDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kColPOFreightAmt, miNatCurrDecPlaces, kMaxAmtLen
        
    '-- Hide the appropriate columns
    gGridHideColumn grdSelectPOLines, kColPOPONo
    gGridHideColumn grdSelectPOLines, kColPOPOKey
    gGridHideColumn grdSelectPOLines, kColPOPOLineKey
    gGridHideColumn grdSelectPOLines, kColPOPOLineDistKey
    gGridHideColumn grdSelectPOLines, kColPOGLAcctKey
    gGridHideColumn grdSelectPOLines, kColPOGLAcctNo
    gGridHideColumn grdSelectPOLines, kColPOGLAcctDesc
    gGridHideColumn grdSelectPOLines, kColPOAcctRefKey
    gGridHideColumn grdSelectPOLines, kColPOAcctRefCode
    gGridHideColumn grdSelectPOLines, kColPOAcctRefDesc
    gGridHideColumn grdSelectPOLines, kColPOItemKey
    gGridHideColumn grdSelectPOLines, kColPOExtCmnt
    gGridHideColumn grdSelectPOLines, kColPOSTaxClassID
    gGridHideColumn grdSelectPOLines, kColPOSTaxClassKey
    gGridHideColumn grdSelectPOLines, kColPOSTaxTranKey
    gGridHideColumn grdSelectPOLines, kColPOUnitMeasKey
    gGridHideColumn grdSelectPOLines, kColPOUnitMeasType
    gGridHideColumn grdSelectPOLines, kColPOCmntOnly
    gGridHideColumn grdSelectPOLines, kColPOQtyInvc
    gGridHideColumn grdSelectPOLines, kColPOAmtInvc
    gGridHideColumn grdSelectPOLines, kColPOLineClosed
    gGridHideColumn grdSelectPOLines, kColPOTargetCoID
    gGridHideColumn grdSelectPOLines, kColPOShipMethKey
    gGridHideColumn grdSelectPOLines, kColPOShipMethID
    gGridHideColumn grdSelectPOLines, kColPOShipZoneKey
    gGridHideColumn grdSelectPOLines, kColPOShipZoneID
    gGridHideColumn grdSelectPOLines, kColPOFOBKey
    gGridHideColumn grdSelectPOLines, kColPOFOBID
    gGridHideColumn grdSelectPOLines, kColPOFreightAmt
    gGridHideColumn grdSelectPOLines, kColPORcptReq
    gGridHideColumn grdSelectPOLines, kColPOAllowCostOvrd
    gGridHideColumn grdSelectPOLines, kColPORcvrKey
    gGridHideColumn grdSelectPOLines, kColPORcvrLineKey
    gGridHideColumn grdSelectPOLines, kColPORtrnType
    gGridHideColumn grdSelectPOLines, kColPOQtyRcvd
    gGridHideColumn grdSelectPOLines, kColPORcvrUOM
    gGridHideColumn grdSelectPOLines, kColPOWhseKey
    gGridHideColumn grdSelectPOLines, kColPOFixedExchRate
    gGridHideColumn grdSelectPOLines, kColPOProjectID
    gGridHideColumn grdSelectPOLines, kColPOProjectKey
    gGridHideColumn grdSelectPOLines, kColPOPhaseID
    gGridHideColumn grdSelectPOLines, kColPOPhaseKey
    gGridHideColumn grdSelectPOLines, kColPOTaskID
    gGridHideColumn grdSelectPOLines, kColPOTaskKey
    gGridHideColumn grdSelectPOLines, kColPOCostClassID
    gGridHideColumn grdSelectPOLines, kColPOCostClassKey
    gGridHideColumn grdSelectPOLines, kColPOVarianceStr
    gGridHideColumn grdSelectPOLines, kColPOVariance
    gGridHideColumn grdSelectPOLines, kColPOJobLineKey
    gGridHideColumn grdSelectPOLines, kColPOJobType
    gGridHideColumn grdSelectPOLines, kColPOTaskType
    gGridHideColumn grdSelectPOLines, kColPOFASAssetTemplate
    gGridHideColumn grdSelectPOLines, kColUnBilledQty
    gGridHideColumn grdSelectPOLines, kColPOUnitCostExact

    '-- Now freeze some columns for looks
    gGridFreezeCols grdSelectPOLines, kColPOSelect
    gGridFreezeCols grdSelectPOLines, kColPOPOLineNo
    gGridFreezeCols grdSelectPOLines, kColPOItemID
        
    '-- Allow the user to resize the columns
    grdSelectPOLines.Col = -1
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_ON
       
    '-- Don't allow the user to resize hidden columns (bug in FarPoint?)
    grdSelectPOLines.Col = kColPOPONo
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOPOKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOPOLineKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOPOLineDistKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOGLAcctKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOGLAcctNo
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOGLAcctDesc
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOAcctRefKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOAcctRefCode
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOAcctRefDesc
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOItemKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOExtCmnt
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOSTaxClassID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOSTaxClassKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOSTaxTranKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOUnitMeasKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOUnitMeasType
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOCmntOnly
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOQtyInvc
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOAmtInvc
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOLineClosed
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOTargetCoID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOShipMethKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOShipMethID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOShipZoneKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOShipZoneID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOFOBKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOFOBID
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOFreightAmt
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPORcptReq
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOAllowCostOvrd
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOQtyRcvd
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPORcvrUOM
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColPOWhseKey
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    grdSelectPOLines.Col = kColUnBilledQty
    grdSelectPOLines.UserResizeCol = SS_USER_RESIZE_OFF
    
    'EB-JMM 1/9/2019
    Const kColPOLineQtyInvcd As Integer = 63
    Const kColPOLineQtyOnBO As Integer = 64
    Const kColPOLineQtyRcvd As Integer = 65
    Const kColRcvrTranNo As Integer = 66
    Const kColRcvrBOL As Integer = 67
    Const kColRcvrTranDate As Integer = 68
    Const kColRcvrTranCmnt As Integer = 69
    Const kColRcvrLineTranCmnt As Integer = 70
    Const kColRcvrQtyRcvd As Integer = 71
    
    gGridSetHeader grdSelectPOLines, kColPOLineQtyInvcd, "Qty on Invc"
    gGridSetHeader grdSelectPOLines, kColPOLineQtyOnBO, "Qty on BO"
    gGridSetHeader grdSelectPOLines, kColPOLineQtyRcvd, "Qty Rcvd"
    gGridSetHeader grdSelectPOLines, kColRcvrTranNo, "Latest Rcvr"
    gGridSetHeader grdSelectPOLines, kColRcvrBOL, "Latest BOL"
    gGridSetHeader grdSelectPOLines, kColRcvrTranDate, "Latest Rcvr Date"
    gGridSetHeader grdSelectPOLines, kColRcvrTranCmnt, "Latest Rcvr Cmnt"
    gGridSetHeader grdSelectPOLines, kColRcvrLineTranCmnt, "Latest Rcvr Line Cmnt"
    gGridSetHeader grdSelectPOLines, kColRcvrQtyRcvd, "Latest Rcvr Line Qty"
    
    gGridSetColumnWidth grdSelectPOLines, kColPOLineQtyInvcd, 10
    gGridSetColumnWidth grdSelectPOLines, kColPOLineQtyOnBO, 10
    gGridSetColumnWidth grdSelectPOLines, kColPOLineQtyRcvd, 10
    gGridSetColumnWidth grdSelectPOLines, kColRcvrTranNo, 10
    gGridSetColumnWidth grdSelectPOLines, kColRcvrBOL, 10
    gGridSetColumnWidth grdSelectPOLines, kColRcvrTranDate, 15
    gGridSetColumnWidth grdSelectPOLines, kColRcvrTranCmnt, 30
    gGridSetColumnWidth grdSelectPOLines, kColRcvrLineTranCmnt, 30
    gGridSetColumnWidth grdSelectPOLines, kColRcvrQtyRcvd, 15
    
    gGridSetColumnType grdSelectPOLines, kColPOLineQtyInvcd, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdSelectPOLines, kColPOLineQtyOnBO, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdSelectPOLines, kColPOLineQtyRcvd, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdSelectPOLines, kColRcvrTranNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColRcvrBOL, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColRcvrTranDate, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColRcvrTranCmnt, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColRcvrLineTranCmnt, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectPOLines, kColRcvrQtyRcvd, SS_CELL_TYPE_FLOAT
    
    gGridLockColumn grdSelectPOLines, kColPOLineQtyInvcd
    gGridLockColumn grdSelectPOLines, kColPOLineQtyOnBO
    gGridLockColumn grdSelectPOLines, kColPOLineQtyRcvd
    gGridLockColumn grdSelectPOLines, kColRcvrTranNo
    gGridLockColumn grdSelectPOLines, kColRcvrBOL
    gGridLockColumn grdSelectPOLines, kColRcvrTranDate
    gGridLockColumn grdSelectPOLines, kColRcvrTranCmnt
    gGridLockColumn grdSelectPOLines, kColRcvrLineTranCmnt
    gGridLockColumn grdSelectPOLines, kColRcvrQtyRcvd
    
    SetGridNumericAttr grdSelectPOLines, kColPOLineQtyInvcd, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kColPOLineQtyOnBO, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kColPOLineQtyRcvd, miQtyDecPlaces, kMaxAmtLen
    SetGridNumericAttr grdSelectPOLines, kColRcvrQtyRcvd, miQtyDecPlaces, kMaxAmtLen
    'EB-JMM 1/9/2019
    
    
    '-- Set the font size on the row header column to be bigger,
    '-- so the "*" is more visible
    grdSelectPOLines.Row = -1: grdSelectPOLines.Col = 0
    grdSelectPOLines.Font.Size = 10
    
    Set oSysSession = Nothing
    Set oAppDB = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub LoadLinesInGrid()
'***************************************************************
' Desc: This routine will load the Select Lines grid with all
'       valid purchase order or receiver lines.
'***************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim rs As Object
    Dim lNumRows As Long
    Dim iDefaultQty As Integer
    Dim lRcvrTranType As Long
    Dim sClip As String
    Dim sClipData As String
    Dim cSelLines As New Collection
    Dim lRow As Long
    Dim vRow As Variant
    
    '-- Turn redraw of the grid off so no flashing occurs
    SetGridRedraw grdSelectPOLines, False
    
    '-- First store off the Selected state of lines
    '-- (in case we are refreshing the grid right before form close)
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        If (giGetValidInt(gsGridReadCell(grdSelectPOLines, lRow, kColPOSelect)) = kiOn) Then
            cSelLines.Add lRow, CStr(lRow)
        End If
    Next lRow
    
    If mbDefaultQty Then
        iDefaultQty = 1
    Else
        iDefaultQty = 0
    End If
    
    lNumRows = 0
    grdSelectPOLines.MaxRows = 0

    '-- According to Microsoft Knowledge Base Article ID: Q154295,
    '-- the Sleep function may be a workaround to the intermittent "Protocol error in TDS stream."
    Sleep 1250
    
    If (muMode = kPOLines) Then
        '-- Get PO Lines
        sSQL = "{call sppoSelectPOLines ("
        sSQL = sSQL & gsQuoted(msCompanyID)
        sSQL = sSQL & ", " & mlVoucherKey
        sSQL = sSQL & ", " & mlTranType
        sSQL = sSQL & ", " & gsQuoted(txtDocNo)
        sSQL = sSQL & ", " & gsQuoted(msNatCurrID)
        sSQL = sSQL & ", " & mdCurrExchRate
        sSQL = sSQL & ", " & miNatCurrDecPlaces
        sSQL = sSQL & ", " & iDefaultQty & ")}"
        Set rs = moMainForm.oClass.moAppDB.OpenRowset(sSQL, kClipSize, kOptionNone)
    Else
        If (mlTranType = kTranTypeAPDM) Then
            lRcvrTranType = kTranTypePORTrn
            chkViewClosedLines = vbChecked
        Else
            lRcvrTranType = kTranTypePORG
            chkViewClosedLines = vbUnchecked
        End If
        
        '-- Get Receiver Lines
        sSQL = "{call sppoSelectRcvLines ("
        sSQL = sSQL & gsQuoted(msCompanyID)
        sSQL = sSQL & ", " & gsQuoted(moMainForm.txtRcvrCompanyID)
        sSQL = sSQL & ", " & mlVoucherKey
        sSQL = sSQL & ", " & lRcvrTranType
        sSQL = sSQL & ", " & gsQuoted(txtDocNo)
        sSQL = sSQL & ", " & miNatCurrDecPlaces
        sSQL = sSQL & ", " & iDefaultQty & ")}"
        Set rs = moMainForm.oClass.moAppDB.OpenRowset(sSQL, kClipSize, kOptionNone)
    End If
    
    sClip = vbCrLf
    
    With rs
        '-- Work around for IM SP that has debug select statements in it
        If (LCase(Left$(.Clip, 4)) <> "conv") Then
            sClipData = .Clip
        
            While sClipData <> ""
                sClip = sClip & vbTab & sClipData
                lNumRows = lNumRows + kClipSize
                .MoveNext
                sClipData = .Clip
            Wend
        End If
    End With
    
    gGridSetMaxRows grdSelectPOLines, lNumRows

    grdSelectPOLines.Row = -1
    grdSelectPOLines.Col = -1
    grdSelectPOLines.ClipValue = sClip
    
    '-- Now restore the Selected state of lines
    For Each vRow In cSelLines
        gGridUpdateCell grdSelectPOLines, CLng(vRow), kColPOSelect, CStr(kiOn)
    Next
    
    '-- Turn redraw of the grid back on
    SetGridRedraw grdSelectPOLines, True
    
    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadLinesInGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub optQtyMethod_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick optQtyMethod(Index), True
    #End If
'+++ End Customizer Code Push +++
    
    If optQtyMethod(0) = True Then
        mbDefaultQty = True
    Else
        mbDefaultQty = False
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optQtyMethod_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ViewClosedLines(bFlag As Boolean)
'****************************************************************
' Desc: This routine will show/hide the closed lines in the grid.
'****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    
    If (mlTranType <> kTranTypeAPDM) Then
    '-- Show all lines on a return
        For lRow = 1 To grdSelectPOLines.DataRowCnt
            If (Val(gsGridReadCell(grdSelectPOLines, lRow, kColPOLineClosed)) = kiLineClosed) Then
                '-- The line is Closed, so show or hide based on flag
                grdSelectPOLines.Row = lRow
                grdSelectPOLines.RowHidden = Not bFlag
            End If
        Next lRow
        
        '-- Focus set to first row
        If bFlag Then
            grdSelectPOLines.SetActiveCell 1, 1
        End If
        
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ViewClosedLines", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ShowClosedLinesIndicator()
'***************************************************************
' Desc: This routine will display the closed lines indicator for
'       each line that is closed.
'***************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    
    For lRow = 1 To grdSelectPOLines.DataRowCnt
        If (Val(gsGridReadCell(grdSelectPOLines, lRow, kColPOLineClosed)) = kiLineClosed) Then
            '-- The line is Closed, so show "*" in the row header
            gGridUpdateCellText grdSelectPOLines, lRow, 0, "*"
        Else
            '-- The line is Open, so blank out the row header
            gGridUpdateCellText grdSelectPOLines, lRow, 0, " "
        End If
    Next lRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ToggleClosedLines", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Instantiate Context Menu Class
    Set moContextMenu = New clsContextMenu

    With moContextMenu
        'Add any Special .Bind or .BindGrid commands here for Drill Around/Drill Down
        'or for Grids - Here

        'Assign the Winhook control to the Context Menu Class
        Set .Form = frmSelectPOLines
    
        'Init will set properties of Winhook to intercept WM_RBUTTONDOWN
        .Init
    End With
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupBars()
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Setup the toolbar
    With tbrMain
        .Style = sotaTB_PROCESS
        .RemoveButton kTbProceed
        .LocaleID = moClass.moSysSession.Language
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
    
    Select Case sKey
        Case kTbClose
            '-- Make sure quantities in grid are up-to-date
            LoadLinesInGrid
            
            '-- This form will be unloaded from the Voucher form,
            '-- after the lines have been loaded in the detail grid
            If mbShowUI Then
                Me.Hide
            End If
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = Nothing
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub cmdSelectClearAll_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelectClearAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectClearAll_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelectClearAll(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkViewClosedLines_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkViewClosedLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkViewClosedLines_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkViewClosedLines_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkViewClosedLines, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkViewClosedLines_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optQtyMethod_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optQtyMethod(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optQtyMethod_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optQtyMethod_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optQtyMethod(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optQtyMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optQtyMethod_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optQtyMethod(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optQtyMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If














Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property







