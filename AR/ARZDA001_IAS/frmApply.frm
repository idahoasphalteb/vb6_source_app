VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#47.0#0"; "sotacalendar.ocx"
Begin VB.Form frmApply 
   Caption         =   "Unapplied Memos/Payments"
   ClientHeight    =   5280
   ClientLeft      =   75
   ClientTop       =   1170
   ClientWidth     =   9225
   HelpContextID   =   86069
   Icon            =   "frmApply.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5280
   ScaleWidth      =   9225
   Begin VB.CommandButton cmdHelp 
      Height          =   405
      Left            =   8655
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   4680
      Width           =   405
   End
   Begin VB.Frame fraSOAppls 
      Caption         =   "Sales Order Applications and Payments"
      Height          =   2415
      Left            =   120
      TabIndex        =   15
      Top             =   2640
      Width           =   7225
      Begin FPSpreadADO.fpSpread grdSOApply 
         Height          =   1935
         Left            =   120
         TabIndex        =   16
         Top             =   240
         WhatsThisHelpID =   17778023
         Width           =   6975
         _Version        =   524288
         _ExtentX        =   12303
         _ExtentY        =   3413
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "frmApply.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame fraARAppls 
      Caption         =   "AR Memos and Payments"
      Height          =   2415
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   9000
      Begin FPSpreadADO.fpSpread grdApply 
         Height          =   1965
         Left            =   120
         TabIndex        =   14
         Top             =   240
         WhatsThisHelpID =   86082
         Width           =   8730
         _Version        =   524288
         _ExtentX        =   15399
         _ExtentY        =   3466
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "frmApply.frx":2810
         AppearanceStyle =   0
      End
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   12
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   5
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      Height          =   405
      Left            =   7560
      TabIndex        =   0
      Top             =   4680
      WhatsThisHelpID =   4
      Width           =   945
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   6
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   11
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   10
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmApply"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
   Private moContextMenuObj        As clsContextMenu          ' from **PRESTO**
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Dim moDmSOGrid As clsDmGrid

Const kColApply = 1
Const kColCustID = 2
Const kColPmtKey = 3
Const kColPmtID = 4
Const kColTranType = 5
Const kColPrepaid = 6
Const kColUnappliedBal = 7
Const kColExchRate = 8
Const kColTranDate = 9
Const kLastApplyCol = 9

Const kColSOApply = 1
Const kColSOCustID = 2
Const kColSOPmtKey = 3
Const kColSOPmtID = 4
Const kColSOTranType = 5
Const kColSOSOPmtKey = 6
Const kColSOAppliedAmt = 7
Const kColSOOrigAppliedAmt = 8
Const kColSOExchRate = 9
Const kColSOTranDate = 10
Const kColSODiscTakenAmt = 11
Const kColSOOrigDiscTakenAmt = 12
Const kMaxSOGridCols = 12

Public mlTranType   As Long
Public mlPmtKey     As Long
Public msPmtID      As String
Public msTranDate   As String
Public mbUserUnChecked  As Boolean    'Scopus#7961-Used to see if user has manually check grid off
Private mbLoadingGrid As Boolean 'don't display warnings when loading grid
Private mbValidating  As Boolean 'prevent validation from firing if setting checkbox in code
Private mbShowMsg     As Boolean

Public TotalSOApplsAmt       As Double
Public TotalSODiscTakenAmt   As Double


' Resize Form Variables
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    
    

Const VBRIG_MODULE_ID_STRING = "FRMAPPLY.FRM"
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "ARZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "ARZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Sub FormatARGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  'set default grid colors
    gGridSetColors grdApply

    grdApply.MaxCols = kLastApplyCol
     
  'set column headings
    gGridSetHeader grdApply, kColApply, "Apply"
    gGridSetHeader grdApply, kColPmtID, "Payment ID"
    gGridSetHeader grdApply, kColCustID, "Customer ID"
    gGridSetHeader grdApply, kColUnappliedBal, "Unapplied Balance"
    gGridSetHeader grdApply, kColPrepaid, "Prepaid Inv No"

    gGridSetColumnType grdApply, kColApply, SS_CELL_TYPE_CHECKBOX
    
  'set column widths
    gGridSetColumnWidth grdApply, kColApply, 6
    gGridSetColumnWidth grdApply, kColPmtID, 15
    gGridSetColumnWidth grdApply, kColCustID, 15
    gGridSetColumnWidth grdApply, kColUnappliedBal, 20
    gGridSetColumnWidth grdApply, kColPrepaid, 15

  'Lock and hide columns
    
    If Not (muARDflts.UseNatlAccts And muCustDflts.PmtByParent) Then
        gGridHideColumn grdApply, kColCustID
    Else
        gGridShowColumn grdApply, kColCustID
    End If
    gGridLockColumn grdApply, kColCustID
    gGridHideColumn grdApply, kColPmtKey
    gGridHideColumn grdApply, kColTranType
    gGridHideColumn grdApply, kColExchRate
    gGridHideColumn grdApply, kColTranDate
    gGridLockColumn grdApply, kColPmtID
    gGridLockColumn grdApply, kColUnappliedBal
    gGridLockColumn grdApply, kColPrepaid
    
  'a few more grid settings
    With grdApply
        ' only allow entire rows to be Selected
        .SelectBlockOptions = SS_SELBLOCKOPT_ROWS
        ' no row/column resizing
        .UserResize = SS_USER_RESIZE_DEFAULT
        ' allow only single row selection
        '.OperationMode = SS_OP_MODE_SINGLE_SELECT
        ' turn row header back off (they get turned on automatically
        ' in SetDefaultGridProperties in clsDmGrid)
        .DisplayRowHeaders = False
        ' make all numbers right aligned
        .Col = -1
        .Row = -1
        .TypeHAlign = SS_CELL_H_ALIGN_RIGHT
        .Col = kColCustID
        .TypeHAlign = TypeHAlignLeft
    End With

    grdApply.CursorType = SS_CURSOR_TYPE_DEFAULT    ' set cursor type
    grdApply.CursorStyle = SS_CURSOR_STYLE_ARROW    ' set default cursor to arrow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatARGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Sub FormatSOGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
 
  'set default grid colors
    gGridSetColors grdSOApply

    grdSOApply.MaxCols = kMaxSOGridCols
     
  'set column headings
    gGridSetHeader grdSOApply, kColSOApply, "Apply"
    gGridSetHeader grdSOApply, kColSOPmtID, "Payment ID"
    gGridSetHeader grdSOApply, kColSOCustID, "Customer ID"
    gGridSetHeader grdSOApply, kColSOAppliedAmt, "Applied Amt"
        
    gGridSetColumnType grdSOApply, kColSOApply, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdSOApply, kColSOAppliedAmt, SS_CELL_TYPE_FLOAT, miDigAfterDec
    gGridSetColumnType grdSOApply, kColSOOrigAppliedAmt, SS_CELL_TYPE_FLOAT, miDigAfterDec
    gGridSetColumnType grdSOApply, kColSODiscTakenAmt, SS_CELL_TYPE_FLOAT, miDigAfterDec
    gGridSetColumnType grdSOApply, kColSOOrigDiscTakenAmt, SS_CELL_TYPE_FLOAT, miDigAfterDec
  
    gGridHAlignColumn grdSOApply, kColPmtID, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_RIGHT
            
  'set column widths
    gGridSetColumnWidth grdSOApply, kColSOApply, 6
    gGridSetColumnWidth grdSOApply, kColSOPmtID, 15
    gGridSetColumnWidth grdSOApply, kColSOCustID, 15
    gGridSetColumnWidth grdSOApply, kColSOAppliedAmt, 20
        
  'Lock and hide columns
    gGridLockColumn grdSOApply, kColSOCustID
    gGridLockColumn grdSOApply, kColSOPmtID
    gGridLockColumn grdSOApply, kColSOAppliedAmt
        
        
    gGridHideColumn grdSOApply, kColSOPmtKey
    gGridHideColumn grdSOApply, kColSOTranType
    gGridHideColumn grdSOApply, kColSOExchRate
    gGridHideColumn grdSOApply, kColSOTranDate
    gGridHideColumn grdSOApply, kColSOSOPmtKey
    gGridHideColumn grdSOApply, kColSOOrigAppliedAmt
    gGridHideColumn grdSOApply, kColSODiscTakenAmt
    gGridHideColumn grdSOApply, kColSOOrigDiscTakenAmt

           
  'a few more grid settings
    With grdSOApply
        ' only allow entire rows to be Selected
        .SelectBlockOptions = SS_SELBLOCKOPT_ROWS
        ' no row/column resizing
        .UserResize = SS_USER_RESIZE_DEFAULT
        ' allow only single row selection
        '.OperationMode = SS_OP_MODE_SINGLE_SELECT
        ' turn row header back off (they get turned on automatically
        ' in SetDefaultGridProperties in clsDmGrid)
        .DisplayRowHeaders = False
        
    End With

    grdSOApply.CursorType = SS_CURSOR_TYPE_DEFAULT    ' set cursor type
    grdSOApply.CursorStyle = SS_CURSOR_STYLE_ARROW    ' set default cursor to arrow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatSOGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmApply"
End Function
Public Function LoadAppls(bOnlyPPD As Boolean, iApplyMeth As Integer)
'+++ VB/Rig Begin Push +++

'******************************************************************************
'   Description:
'       02/01-Scopus#7961-Added logic to match the invoice number against the
'       PrepaidCustInvcNo added during cash recipt processing.If there
'       is a match and user gets to this screen for the first time,
'       it will pop up a message box asking user if they want to click
'       on the matched invoice on the grid automatically. If user selects
'       "No", It doesn't force the clicking on the matched payment. This
'       message box pops up only once per new invoice and once user unchecks
'       the grid or clicks on another payment it will not pop up the message
'       box again.
'
'   Parameters: None.
'*******************************************************************************

#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

Dim sSQL                As String
Dim lTranType           As Long
Dim rs                  As Object
Static bTableCreated    As Boolean
Dim lRow                As Long
Dim dAmt                As Double
Dim lPPInvcMatch        As Long  'Flag indicating payment matches
Dim lPPUserClick        As Long  'Flag indicating user previous clicks
Dim lNatlAcctParentKey  As Long
Dim iReturnCode         As Integer

    grdApply.MaxRows = 0
    
    
    lPPInvcMatch = 0
    lPPUserClick = 0

    If Not bTableCreated Then
                
        FormatARGrid
               
        sSQL = "CREATE TABLE #tarLoadPmts"
        sSQL = sSQL & "(PmtID        varchar(13) NULL,"
        sSQL = sSQL & "CustID       varchar(13)  NULL,"
        sSQL = sSQL & "UnappliedBal float     NULL,"
        'sSQL = sSQL & "PendingBal   float     NULL,"
        sSQL = sSQL & "PpdInvcNo    varchar(10)  NULL,"
        sSQL = sSQL & "PmtKey       int       NULL,"
        sSQL = sSQL & "CurrExchRate float     NULL,"
        sSQL = sSQL & "TranType     int       NULL,"
        sSQL = sSQL & "TranDate     datetime  NULL)"
        
        frmInvcEntry.moClass.moAppDB.ExecuteSQL sSQL
    
        bTableCreated = True
    
    Else
        
        FormatARGrid
                
        sSQL = "DELETE #tarLoadPmts"
        frmInvcEntry.moClass.moAppDB.ExecuteSQL sSQL
    
    End If

    If frmInvcEntry.cboInvcType.ListIndex = kItemNotSelected Then
        lTranType = 0
    Else
        lTranType = frmInvcEntry.cboInvcType.ItemData(frmInvcEntry.cboInvcType.ListIndex)
    End If
    
    If muCustDflts.PmtByParent And muCustDflts.NatlAcctKey <> 0 And muARDflts.UseNatlAccts Then
        lNatlAcctParentKey = muCustDflts.NatlAcctParentKey
    Else
        lNatlAcctParentKey = 0
    End If
    
    If iApplyMeth = AM_DOWNPMT Then
        grdApply.Enabled = False
    Else
        grdApply.Enabled = True
    End If

    With frmInvcEntry.moClass.moAppDB
        .SetInParam CLng(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER))
        .SetInParam lNatlAcctParentKey
        .SetInParam CStr(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("CurrID")))
        .SetInParam CDbl(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("CurrExchRate"), SQL_INTEGER))
        .SetInParam CLng(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("InvcKey"), SQL_INTEGER))
        .SetInParam lTranType
        .SetInParam CLng(frmInvcEntry.oClass.lBatchKey)
        .ExecuteSP ("sparGetUnappliedPmts")
        .ReleaseParams
    End With
        
    
    'Load the AR applications grid
    sSQL = "SELECT * FROM #tarLoadPmts"
        
    If bOnlyPPD Then
        sSQL = sSQL & " WHERE PpdInvcNo = " & gsQuoted(frmInvcEntry.txtInvcID.Text)
    End If
    
    If gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("SourceModule"), SQL_SMALLINT) = 19 Then
        If InStr(sSQL, "WHERE") > 0 Then
            sSQL = sSQL & " AND TranType <> " & kTranTypeARCM
        Else
            sSQL = sSQL & " WHERE TranType <> " & kTranTypeARCM
        End If
    End If
    
    Set rs = frmInvcEntry.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    rs.MoveFirst
            
          'Loop through and load the grid
    While Not rs.IsEOF
        
        lRow = lRow + 1                     'Set Row
        
        grdApply.MaxRows = lRow
        
        grdApply.Row = lRow                 'set current row to last row
        
      'Load Salesperson ID
        gGridSetCellType grdApply, lRow, kColApply, SS_CELL_TYPE_CHECKBOX
        
      'Set a flag indicating if invoice number has a matching cash receipt payments with the same prepaid invoice number
        If Trim(frmInvcEntry.txtInvcID.Text) = Trim(gvCheckNull(rs.Field("PpdInvcNo"))) Then
            lPPInvcMatch = lRow
        End If
        
        grdApply.Col = kColPmtKey
        grdApply.Text = gvCheckNull(rs.Field("PmtKey"), SQL_INTEGER)
        
        grdApply.Col = kColPmtID
        grdApply.Text = gvCheckNull(rs.Field("PmtID"))
        
        grdApply.Col = kColCustID
        grdApply.Text = gvCheckNull(rs.Field("CustID"))
        
        If gvCheckNull(gsGridReadCell(grdApply, lRow, kColPmtKey), SQL_INTEGER) = mlPmtKey Then
            If gvCheckNull(gsGridReadCell(grdApply, lRow, kColPmtID)) = msPmtID Then
                lPPUserClick = lRow
            End If
        End If
        
        grdApply.Row = lRow                 'set current row to last row
        
        grdApply.Col = kColTranType
        grdApply.Text = gvCheckNull(rs.Field("TranType"), SQL_INTEGER)

        grdApply.Col = kColPrepaid
        grdApply.Text = gvCheckNull(rs.Field("PpdInvcNo"))
        
        grdApply.Col = kColExchRate
        grdApply.Text = gvCheckNull(rs.Field("CurrExchRate"))

        grdApply.Col = kColUnappliedBal
        dAmt = CDbl(gvCheckNull(rs.Field("UnappliedBal"), SQL_INTEGER))
        grdApply.Text = gfRound(dAmt, miRoundMeth, miDigAfterDec)
        
                    
        grdApply.Col = kColTranDate
        grdApply.Text = gvCheckNull(rs.Field("TranDate"))

        rs.MoveNext

    Wend
    
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
    
    mbLoadingGrid = True
    
    If iApplyMeth <> AM_DOWNPMT Then
        'Click on the payment previously selected by the user or show a message box (if there is a matching payment
        'for the given invoice) to select if they like the system to pick the matching payment.
        If lPPUserClick <> 0 Then
            lRow = lPPUserClick
            gGridUpdateCell grdApply, lRow, kColApply, "1"
        Else
            If lPPInvcMatch <> 0 Then
                If mbUserUnChecked = False Then
                    iReturnCode = giSotaMsgBox(Me, frmInvcEntry.moClass.moSysSession, kmsgARPaymentMatching)
                        Select Case iReturnCode
                            Case Is = kretYes
                                lRow = lPPInvcMatch
                                gGridUpdateCell grdApply, lRow, kColApply, "1"
                        End Select
                End If
            End If
            
        End If
    End If
    
    mbLoadingGrid = False
    
    Set rs = Nothing
                
Exit Function

ExpectedErrorRoutine:
MyErrMsg frmInvcEntry.moClass, Err.Description, Err, sMyName, "LoadAppls"
gClearSotaErr
Set rs = Nothing

Exit Function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadAppls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Sub LoadSOApplications()
Dim sSQL As String

    grdSOApply.MaxRows = 0
    TotalSOApplsAmt = 0
    TotalSODiscTakenAmt = 0
    
    'Load up the SO Applications
    With frmInvcEntry.moClass.moAppDB
        .SetInParam CDbl(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("CurrExchRate"), SQL_INTEGER))
        .SetInParam CLng(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("InvcKey"), SQL_INTEGER))
        .SetInParam CLng(frmInvcEntry.oClass.lBatchKey)
        .SetOutParam TotalSOApplsAmt
        .SetOutParam TotalSODiscTakenAmt
        .ExecuteSP ("sparGetSOAppls")
        TotalSOApplsAmt = gdGetValidDbl(gvCheckNull(.GetOutParam(4), SQL_INTEGER))
        TotalSODiscTakenAmt = gdGetValidDbl(gvCheckNull(.GetOutParam(5), SQL_INTEGER))
        .ReleaseParams
    End With
    
                                                                                                                                                                                                             
    'Refresh the SO applications grid
    moDmSOGrid.Refresh
        
    If Not (muARDflts.UseNatlAccts And muCustDflts.PmtByParent) Then
        gGridHideColumn grdSOApply, kColSOCustID
    Else
        gGridShowColumn grdSOApply, kColSOCustID
    End If
    
End Sub

Public Sub SaveSOGrid()
'Dim lRow As Long
'Dim iApply As Integer


'    For lRow = 1 To grdSOApply.DataRowCnt
'    'LLJ TODO:  Check discount as well?
'        iApply = giGetValidInt(gsGridReadCell(grdSOApply, lRow, kColSOApply))
'        If iApply = 0 Then
'            gGridUpdateCell grdSOApply, lRow, kColSOAppliedAmt, "0" 'will cause the application to be deleted
'            moDmSOGrid.SetRowDirty lRow
'        End If
'
'    Next lRow
    
    moDmSOGrid.Save
      
End Sub

Private Sub cmdClose_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdClose, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    
    'bApplicationMade
    Me.Hide

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdClose_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdHelp_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdHelp, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    gDisplayFormLevelHelp Me
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'   Original: 07/29/95 HHO
'   Modified:
'   Copyright (c) 1995-2005 Sage Software, Inc.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    Dim iReturnValue As Integer

'ZZDebug.Print "formkeydown"
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'********************************************************************
'ZZDebug.Print "formkeypress"
     Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub Form_Load()
   Set moContextMenuObj = New clsContextMenu      ' from **PRESTO**
   Set moContextMenuObj.Form = frmApply         ' from **PRESTO**

    
    ' **PRESTO ** With WinHook1
        ' **PRESTO ** .HookEnabled = False
        ' **PRESTO ** .HookType = shkGetMessage    'Get Message
        ' **PRESTO ** .Monitor = shkThisForm       'This Form
        ' **PRESTO ** .Notify = shkPosted      'When Hooked
        ' **PRESTO ** .Messages = WM_RBUTTONDOWN
        ' **PRESTO ** .HookEnabled = True
    ' **PRESTO ** End With


    miMinFormHeight = Me.Height
    miMinFormWidth = Me.Width
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    
    FormatSOGrid
    BindSOGrid
       
    cmdHelp.Picture = gLoadResPicture(Help)
   
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    If UnloadMode = vbFormControlMenu Then
        Cancel = True
        cmdClose_Click
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'ZZZ    Debug.Print "In Form resize:", GetTickCount()

'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Allow user to resize form except for Width of the form.
'   Parameters: None.
'************************************************************************
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
       
        
      'resize Height
       gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
                    grdApply
           
      'resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth
                     'grdApply
               
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
        

    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindSOGrid()
    
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Bind the payments grid
    Set moDmSOGrid = New clsDmGrid

    With moDmSOGrid
        Set .Form = frmApply
        Set .Session = frmInvcEntry.moClass.moSysSession
        Set .Grid = grdSOApply
        Set .Database = frmInvcEntry.moClass.moAppDB
        .Table = "#tarLoadSOAppls"
        .UniqueKey = "SalesOrderPmtKey"

        .OrderBy = "TranDate"
        .NoAppend = True
        
        .BindColumn "Apply", kColSOApply, SQL_SMALLINT
        .BindColumn "PmtID", kColSOPmtID, SQL_VARCHAR
        .BindColumn "PmtKey", kColSOPmtKey, SQL_INTEGER
        .BindColumn "TranType", kColSOTranType, SQL_INTEGER
        .BindColumn "SalesOrderPmtKey", kColSOSOPmtKey, SQL_INTEGER
        .BindColumn "CustID", kColSOCustID, SQL_VARCHAR
        .BindColumn "ApplAmt", kColSOAppliedAmt, SQL_DECIMAL
        .BindColumn "OrigApplAmt", kColSOOrigAppliedAmt, SQL_DECIMAL
        .BindColumn "CurrExchRate", kColSOExchRate, SQL_FLOAT
        .BindColumn "TranDate", kColSOTranDate, SQL_DATE
        .BindColumn "DiscTakenAmt", kColSODiscTakenAmt, SQL_DECIMAL
        .BindColumn "OrigDiscTakenAmt", kColSOOrigDiscTakenAmt, SQL_DECIMAL
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindGrid", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub




Private Sub grdApply_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRow As Long
Dim bIsValid As Boolean
Dim lCMKey As Long
Dim lPmtKey As Long
Dim lApplKey As Long

    bIsValid = False
    
    'gGridUpdateCell causes grdApply_ButtonClicked to fire again - don't want to do anything in this case
    If mbValidating Then
        Exit Sub
    End If
    
    lApplKey = glGetValidLong(gsGridReadCell(grdApply, Row, kColPmtKey))
    
    If glGetValidLong(gsGridReadCell(grdApply, Row, kColTranType)) = kTranTypeARCM Then
        lCMKey = lApplKey
        lPmtKey = 0
    Else
        lPmtKey = lApplKey
        lCMKey = 0
    End If
        
    If gsGridReadCell(grdApply, Row, Col) = "1" Then
        If bValidAppl(Row) Then
            frmInvcEntry.mlApplyMeth = AM_CUSTPMT
            bApplicationMade Row
            If frmInvcEntry.bCalcApplAmt(lPmtKey, lCMKey, True) Then
                bIsValid = True
                For lRow = 1 To Row - 1
                    mbValidating = True
                    gGridUpdateCell grdApply, lRow, kColApply, "0"
                    mbValidating = False
                Next lRow
        
                For lRow = Row + 1 To grdApply.MaxRows
                    mbValidating = True
                    gGridUpdateCell grdApply, lRow, kColApply, "0"
                    mbValidating = False
                Next lRow
            End If
        End If
        
        If Not bIsValid Then
            mbValidating = True
            gGridUpdateCell grdApply, Row, kColApply, "0"
            mbValidating = False
        End If
            
    Else
        'frmInvcEntry.mlApplyMeth = AM_NONE
        'frmInvcEntry.bCalcApplAmt lPmtKey, lCMKey, True
        bApplicationMade Row
        frmInvcEntry.moDmAppRec.SetColumnValue "PmtAmt", 0
        frmInvcEntry.moDmAppRec.SetColumnValue "PmtAmtIC", 0
        
        mbValidating = True
        gGridUpdateCell grdApply, Row, kColApply, "0"
        mbValidating = False
    End If
    
               
    '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdApply_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bValidAppl(lRow As Long) As Boolean
Dim sSQL As String
Dim rs As Object
Dim dUnappliedAmt As Double
Dim lApplKey As Long
Dim sWhere As String

    bValidAppl = False
    
    If mbLoadingGrid Then
        bValidAppl = True
        Exit Function
    End If
    
    'Get the total applied amount on the SO grid.  If the invoice is fully applied, disallow
    'the AR application, otherwise, the application will be created for the balance on the invoice.
    If frmInvcEntry.curInvcAmt.Amount - TotalSOApplsAmt - TotalSODiscTakenAmt <= 0 Then
        giSotaMsgBox Me, frmInvcEntry.moClass.moSysSession, kmsgInvcFullyApplied
        Exit Function
    End If
    
    
    'If ok to apply, warn if there are any SO payments against this application for SO's not on this
    'invoice that have not been fully applied.  Applications won't be in this grid if there are SO assignments for SO's on this invoice
    '- will be in the bottom grid.
    
    lApplKey = glGetValidLong(gsGridReadCell(grdApply, lRow, kColPmtKey))
    
    If glGetValidLong(gsGridReadCell(grdApply, lRow, kColTranType)) = kTranTypeARCM Then
        sWhere = "CreditMemoKey = " & lApplKey
    Else
        sWhere = "CustPmtKey = " & lApplKey
    End If
       
    sSQL = "SELECT tarSalesOrderPmt.SalesOrderPmtKey, tarSalesOrderPmt.AssignedAmt - tarSalesOrderPmt.AppliedAmt - COALESCE(SUM(tarPendCustPmtAppl.PmtAmt),0) UnapplAmt "
    sSQL = sSQL & "FROM tarSalesOrderPmt WITH (NOLOCK) "
    sSQL = sSQL & "LEFT OUTER JOIN tarpendCustpmtAppl WITH (NOLOCK) "
    sSQL = sSQL & "ON tarSalesOrderPmt.SalesOrderPmtKey = tarPendCustPmtAppl.SalesOrderPmtKey "
    sSQL = sSQL & "WHERE " & sWhere & " "
    sSQL = sSQL & "GROUP BY tarSalesOrderPmt.SalesOrderPmtKey, tarSalesOrderPmt.AssignedAmt, tarSalesOrderPmt.AppliedAmt"
         
    Set rs = frmInvcEntry.moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    dUnappliedAmt = 0
    Do While Not rs.IsEOF
        dUnappliedAmt = dUnappliedAmt + gdGetValidDbl(rs.Field("UnapplAmt"))
        rs.MoveNext
    Loop
  
    If Not rs Is Nothing Then
        Set rs = Nothing
    End If
    
    If dUnappliedAmt > 0 Then
        If giSotaMsgBox(Me, frmInvcEntry.moClass.moSysSession, kmsgOverrideSOPmntAssignment) = kretYes Then
            If frmInvcEntry.bOverrideSecEvent("ARUNAPPLYSOPMT") Then
                bValidAppl = True
            End If
        End If
    Else
        bValidAppl = True
    End If
                    
End Function

Private Sub grdSOApply_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    
    If mbValidating Then
        Exit Sub
    End If
   
    If ButtonDown = 1 Then
    'If gsGridReadCell(grdApply, Row, Col) = "1" Then
        If Not bValidSOAppl(Row, 1) Then
            mbValidating = True
            gGridUpdateCell grdSOApply, Row, kColSOApply, "0"
            mbValidating = False
        Else
            moDmSOGrid.SetRowDirty Row
            frmInvcEntry.moDmHeader.SetDirty True, False
        End If
    Else
        If Not bValidSOAppl(Row, 0) Then
            mbValidating = True
            gGridUpdateCell grdSOApply, Row, kColSOApply, "1"
            mbValidating = False
        Else
            moDmSOGrid.SetRowDirty Row
            frmInvcEntry.moDmHeader.SetDirty True, False
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdApply_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bValidSOAppl(lRow As Long, bChecked As Boolean) As Boolean
Dim dInvoiceBalance As Double

    bValidSOAppl = False
    
    If bChecked Then
        'calc invoice balance not including so payment application for this row
        dInvoiceBalance = frmInvcEntry.curInvcAmt.Amount - frmInvcEntry.curDownPmtAmt.Amount _
                            - TotalSOApplsAmt - TotalSODiscTakenAmt
        If dInvoiceBalance <= 0 Then
            giSotaMsgBox Me, frmInvcEntry.moClass.moSysSession, kmsgInvcFullyApplied
            Exit Function
        End If
    Else
        'Warn the user - application was assigned in SO and will be deleted.
        If mbShowMsg Then
            If giSotaMsgBox(Me, frmInvcEntry.moClass.moSysSession, kmsgAssignedInSO) = kretNo Then
                Exit Function

            End If
        End If
    End If
    
        moDmSOGrid.SetRowDirty lRow
        'frmInvcEntry.moDmHeader.SetDirty True, False
   
        SaveSOGrid
        
        RecalcSOAppls
        
        bValidSOAppl = True

   End Function

Private Function bApplicationMade(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Dim lRow As Long
    mbUserUnChecked = False
    mlTranType = 0
    mlPmtKey = 0
    msPmtID = ""
    msTranDate = ""
    'For lRow = 1 To grdApply.MaxRows
        
        If gsGridReadCell(grdApply, lRow, kColApply) = "1" Then
            mlTranType = Val(gsGridReadCell(grdApply, lRow, kColTranType))
            mlPmtKey = Val(gsGridReadCell(grdApply, lRow, kColPmtKey))
            msPmtID = gsGridReadCell(grdApply, lRow, kColPmtID)
            msTranDate = gsGridReadCell(grdApply, lRow, kColTranDate)
        End If
        
    'Next lRow
    
    'Flag used to indicate user has seen the screen once for each loaded invoice
    If msPmtID = "" Then
        mbUserUnChecked = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bApplicationMade", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub RecalcSOAppls()
    Dim dDiscAvailable As Double
    Dim lRetVal As Long

    TotalSOApplsAmt = 0
    TotalSODiscTakenAmt = 0
    'calc discount available to take = Terms Disc Available - Down Pmt Disc (if any)
    If (frmInvcEntry.curTermsDiscAmt.Amount - frmInvcEntry.mdDwnPmtDiscTakenAmt) > 0 Then
        dDiscAvailable = gdGetValidDbl(frmInvcEntry.curTermsDiscAmt.Amount - frmInvcEntry.mdDwnPmtDiscTakenAmt)

    Else
        dDiscAvailable = 0
    End If

    'Recalc SO Applications
    With frmInvcEntry.moClass.moAppDB
            .SetInParam gdGetValidDbl(frmInvcEntry.curInvcAmt.Amount - frmInvcEntry.curDownPmtAmt.Amount)
            .SetInParam dDiscAvailable
            .SetInParam gsFormatDateToDB(frmInvcEntry.txtDiscDate.SQLDate)
            .SetInParam CStr(gvCheckNull(frmInvcEntry.moDmHeader.GetColumnValue("CurrID")))
            .SetOutParam TotalSOApplsAmt
            .SetOutParam TotalSODiscTakenAmt
            .SetOutParam lRetVal
            .ExecuteSP ("sparRecalcSOPmntAppls")
            TotalSOApplsAmt = gdGetValidDbl(gvCheckNull(.GetOutParam(5), SQL_INTEGER))
            TotalSODiscTakenAmt = gdGetValidDbl(gvCheckNull(.GetOutParam(6), SQL_INTEGER))
            lRetVal = gvCheckNull(.GetOutParam(7), SQL_INTEGER)
            .ReleaseParams
    End With
    
    moDmSOGrid.Refresh
    moDmSOGrid.SetDirty True
    
    End Sub

Private Sub SysInfo1_SysColorsChanged()
    gGridSetColors grdApply

End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = Nothing
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Unload(Cancel As Integer)
   Set moContextMenuObj = Nothing     ' from **PRESTO**
   
   If Not moDmSOGrid Is Nothing Then
        moDmSOGrid.UnloadSelf
        Set moDmSOGrid = Nothing
    End If


'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub cmdHelp_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdHelp, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdHelp_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdHelp_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdHelp, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdHelp_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClose_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClose, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClose_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClose_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClose, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClose_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If









Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property







