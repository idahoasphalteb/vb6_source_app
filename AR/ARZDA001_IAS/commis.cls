VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ClsCommissions"
Attribute VB_Creatable = True
Attribute VB_Exposed = False
Option Explicit

Dim lCommPlan           As Long         'Commissions Plan
Dim dSalesAmt           As Double       'Total Sales Amount
Dim dCostOfSales        As Double       'Unit Cost per Item
Dim dCalcCommAmt        As Double       'Calculated Commission Amount
Dim dActCommAmt         As Double       'Actual Commission Amount
'*************************************************
' Commission Plan Surrogate key
'*************************************************
Const VBRIG_MODULE_ID_STRING = "COMMIS.CLS"

Public Property Get CommPlan() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    CommPlan = lCommPlan
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "CommPlan_Get",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
Public Property Let CommPlan(lNewValue As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    lCommPlan = lNewValue
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "CommPlan_Let",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
'*************************************************
' Total Sales Amount
'*************************************************
Public Property Get SalesAmt() As Double
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    SalesAmt = dSalesAmt
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "SalesAmt_Get",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
Public Property Let SalesAmt(dNewValue As Double)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    dSalesAmt = dNewValue
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "SalesAmt_Let",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
'*************************************************
' Total Cost Of Sales Amount
'*************************************************
Public Property Get CostOfSales() As Double
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    CostOfSales = dCostOfSales
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "CostOfSales_Get",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
Public Property Let CostOfSales(dNewValue As Double)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    dCostOfSales = dNewValue
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "CostOfSales_Let",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
'*************************************************
' Actual Commission Amount
'*************************************************
Public Property Get ActCommAmt() As Double
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ActCommAmt = dActCommAmt
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "ActCommAmt_Get",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
Public Property Let ActCommAmt(dNewValue As Double)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    dActCommAmt = dNewValue
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "ActCommAmt_Let",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
'*************************************************
' Calculated Commission Amount
'*************************************************
Public Property Get CalcCommAmt() As Double
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    CalcCommAmt = dCalcCommAmt
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "CalcCommAmt_Get",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property
Public Property Let CalcCommAmt(dNewValue As Double)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
ON Error Goto VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    dCalcCommAmt = dNewValue
'+++ VB/Rig Begin Pop +++
    	Exit Property

VBRigLogErrorRoutine:
    	gMainClassErr Err, sMyName, "CalcCommAmt_Let",  VBRIG_IS_CLASS
    	Resume Next
'+++ VB/Rig End +++
End Property

Private Function sMyName() As String
'+++ VB/Rig Skip +++
	sMyName =  "ClsCommissions"
End Function
