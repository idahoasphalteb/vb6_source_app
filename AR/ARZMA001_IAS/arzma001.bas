Attribute VB_Name = "basCustMnt"
'************************************************************************************
'     Name: basTemplate
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 07/23/95 KMC
'     Mods: mm/dd/yy XXX
'************************************************************************************
Option Explicit

'SGS DEJ - RKL DEJ 2017-02-15 (START)
Public glCompanyID As String
'SGS DEJ - RKL DEJ 2017-02-15 (STOP)

'Object Task Constants
    Public Const ktskPrint = 84479264
    
    Public Const ksecChangeCreditLimit = "CHANGECRED"
    Public Const ksecChangeCustHold = "CUSTHOLD"
    Public Const ksecChangeStatus = "ARCHGSTAT"
    Public Const ksecDeleteCustomer = "ARDELETECUST"

'Constants for billing Type values customer master table
' tarCustomer | BillingType
    Public Const kvOpenItem = 1
    Public Const kvBalanceForward = 2
    Public Const kvBoth = 3

'Constants for Status vales customer master table
' tarCustomer | Status
    Public Const kvTemporary = 3
    Public Const kvInactive = 2
    Public Const kvActive = 1
    Public Const kvDeleted = 4
    Public Const kvOpen = 1
    Public Const kvClosed = 2

' tarCustomer | CreditLimitAgeCat
    Public Const kvnone = 0                             '(none)                         Str=50011 *Default*
    Public Const kvFirstCategory = 1                    'First Category                 Str=50311
    Public Const kvSecondCategory = 2                   'Second Category                Str=50314
    Public Const kvThirdCategory = 3                    'Third Category                 Str=50316
    Public Const kvFourthCategory = 4                   'Fourth Category
    
    Public Type CustOptions
        CustClassKey         As String
        CustClass            As String
        UseMultiCurr         As Boolean
        AutoNum              As Boolean
        Mask                 As String
        TrackSalesTax        As Boolean
        PrintInvoices        As Boolean
        PrintStmts           As Boolean
        UseSper              As Boolean
        CheckCreditLimit     As Boolean
        CanUpdateCreditLimit As Boolean
        CanChangeCustHold    As Boolean
        BillingTypeOpt       As Integer
        AgeUnit              As String
        AgeCat(4)            As String
    End Type
    
    Public Type VarText
      Text                  As String
    End Type
    Public Const KentMCExchRateSchd = 2
Declare Function GetTickCount Lib "kernel32" () As Long
Const VBRIG_MODULE_ID_STRING = "ARZMA001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("arzma001.clsCustMnt")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basCustMnt"
End Function
Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
Dim sText As String

    If lErr = guSotaErr.Number And Trim(guSotaErr.Description) <> Trim(sDesc) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If

    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyID
        sText = sText & "  AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

End Sub



Public Sub gEnableControls(ParamArray Controls())

    Dim iControl


    For iControl = 0 To UBound(Controls)

        Select Case UCase(TypeName(Controls(iControl)))
            Case "SSPANEL"
                On Error Resume Next
                With Controls(iControl)
                    .Font3D = 0
                    .ForeColor = vbWindowText
                End With
                Err.Clear
            Case "COMBOBOX", "TEXTBOX"
                On Error Resume Next
                With Controls(iControl)
                    .Enabled = True
                    .BackColor = vbWindowBackground
                End With
                Err.Clear
            Case "SOTALOOKUP", "SOTAGLACCOUNT", "GLACCTLOOKUP", "TEXTLOOKUP"
                On Error Resume Next
                CallByName Controls(iControl), "Enabled", VbLet, True
                Err.Clear
            Case Else
                On Error Resume Next
                CallByName Controls(iControl), "Protected", VbLet, False
                If Err.Number <> 0 Then
                    CallByName Controls(iControl), "Enabled", VbLet, True
                End If

                Err.Clear
        End Select

    Next iControl

End Sub

Public Sub gDisableControls(ParamArray Controls())

    Dim iControl


    For iControl = 0 To UBound(Controls)

        Select Case UCase(TypeName(Controls(iControl)))
            Case "SSPANEL"
                On Error Resume Next
                With Controls(iControl)
                    .Font3D = 3
                    .ForeColor = vbInactiveCaptionText
                End With
                Err.Clear
            Case "COMBOBOX", "TEXTBOX"
                On Error Resume Next
                With Controls(iControl)
                    .Enabled = False
                    .BackColor = vbButtonFace
                End With
                Err.Clear
            Case "SOTALOOKUP", "SOTAGLACCOUNT", "GLACCTLOOKUP", "TEXTLOOKUP"
                On Error Resume Next
                CallByName Controls(iControl), "Enabled", VbLet, False
                Err.Clear


            Case Else
                On Error Resume Next
                CallByName Controls(iControl), "Protected", VbLet, True
                If Err.Number <> 0 Then
                    CallByName Controls(iControl), "Enabled", VbLet, False
                End If

                Err.Clear
        End Select

    Next iControl
End Sub


'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
'SGS DEJ 6/14/2010 - Insert SLX Account/Customer   (START)
'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
Sub ProcessSLXData(oClass As Object, CompanyID As String, CustKey As Long, CustID As String, CustName As String, AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String, AddrLine4 As String, City As String, State As String, Zip As String, Country As String)
'InsSLXAccount(oClass As Object, CompanyID As String, CustKey As Long, CustID As String, CustName As String, ByRef AccountID As String) As Boolean
    On Error GoTo Error
'This method is called from the following locations:
'   frmCustomerMaint.DMAfterInsert
    
    Dim AccountID As String
    Dim AddressID As String
'    Dim AddrKey As Long
'    Dim AddrName As String
'    Dim AddrLine1 As String
'    Dim AddrLine2 As String
'    Dim AddrLine3 As String
'    Dim AddrLine4 As String
'    Dim City As String
'    Dim State As String
'    Dim Zip As String
'    Dim Country As String
    
    If glGetValidLong(oClass.moAppDB.Lookup("Count(*)", "tciCompanySendToSLX_RKL", "CompanyID = " & gsQuoted(glCompanyID))) <= 0 Then
        'We are only passing for IDA and PEA (and WEI 2016-05-10)
        Exit Sub
    End If
    
'    If UCase(Trim(CompanyID)) <> "IDA" And UCase(Trim(CompanyID)) <> "PEA" And UCase(Trim(CompanyID)) <> "WEI" Then
'        'We are only passing for IDA and PEA (and WEI 2016-05-10)
'        Exit Sub
'    End If
    
    'Create the SLX Account
    If InsSLXAccount(oClass, CompanyID, CustKey, CustID, CustName, AccountID) = False Then
        'Did not create the Account
        Exit Sub
    End If
    
    'Create the SLX Address
    If InsSLXAddress(oClass, CompanyID, AccountID, AddressID, AddrKey, AddrName, AddrLine1, AddrLine2, AddrLine3, AddrLine4, City, State, Zip, Country) = False Then
        'Did not Create the Address
    End If
    
    
    Exit Sub
Error:
    MsgBox "basSalesOrder.ProcessSLXData()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not insert/update the Sales Logix Opportunity with the Blanket Sales Order/Contract data you will need to do this manually." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub


'SGS DEJ 5/14/2010 (Added Method)
Public Function InsSLXAccount(oClass As Object, CompanyID As String, CustKey As Long, CustID As String, CustName As String, ByRef AccountID As String) As Boolean
    On Error GoTo Error

    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim SQL As String
    
    Dim MASUserID As String
    Dim MASDate As Date
    
    Dim SECCODEID As String
    Dim AddressID As String
    Dim MASLink_SGSID As String
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    MASDate = Date + Time
        
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    'Check to see if the SLX Account Already Exists
    SQL = "Select ACCOUNTID, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, MASCUSTKEY, MASCOMPANYID, MASCUSTID From AccountMASLink_SGS Where MASCustID = " & gsQuoted(CustID)
    
    If rs.State = 1 Then rs.Close
    Set rs = Conn.Execute(SQL)
    
    If rs.RecordCount > 0 Then
        AccountID = gsGetValidStr(rs("AccountID").Value)
        
        rs.MoveFirst
        While Not rs.BOF And Not rs.EOF
            If CustKey = glGetValidLong(rs("MASCustKey").Value) Then
                'The records has already been created... do not create again
                InsSLXAccount = True
                GoTo CleanUP
            End If
            rs.MoveNext
        Wend
        
        GoTo InsertLink
    End If
    
    'Get Values for creation of new account.
    If rs.State = 1 Then rs.Close
    
    SQL = "slx_dbids('Account', 1)"

    Set rs = Conn.Execute(SQL)

    If rs.RecordCount > 0 Then
        AccountID = gsGetValidStr(rs("ID").Value)
    Else
        InsSLXAccount = False
        MsgBox "Could not get the next key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
        GoTo CleanUP
    End If

    If rs.State = 1 Then rs.Close
    
    SECCODEID = "SYST00000001"  'Hard Coded Per Seteve Estes 6/16/2010
    
'    SQL = "slx_dbids('SecCode', 1)"
'
'    Set rs = Conn.Execute(SQL)
'
'    If rs.RecordCount > 0 Then
'        SECCODEID = gsGetValidStr(rs("ID").Value)
'    Else
'        InsSLXAccount = False
'        MsgBox "Could not get the next SECCODE value to create the record in SalesLogix.", vbExclamation, "MAS 500"
'        GoTo CleanUP
'    End If
        

    SQL = "Insert Into Account (AccountID, Account, SecCodeID, AccountManagerID, CreateUser, CreateDate, ModifyUser, ModifyDate, Account_UC, Type " & _
    " ) "

    SQL = SQL & "Values(" & _
    gsQuoted(AccountID) & ", " & _
    gsQuoted(CustName) & ", " & _
    gsQuoted(SECCODEID) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(CustName) & ", " & _
    "'Customer' " & _
    " ) "


    Conn.Execute SQL
    
InsertLink:
    'Insert into the Link Table
    If rs.State = 1 Then rs.Close

'    SQL = "slx_dbids('AccountMASLink_SGS', 1)"
'
'    Set rs = Conn.Execute(SQL)
'
'    If rs.RecordCount > 0 Then
'        MASLink_SGSID = gsGetValidStr(rs("ID").Value)
'    Else
'        InsSLXAccount = False
'        MsgBox "Could not get the next SLX to MAS Link Key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
'        GoTo CleanUP
'    End If
'
'    SQL = "Insert into AccountMASLink_SGS (AccountID, AccountMASLink_SGSID, CreateUser, CreateDate, ModifyUser, ModifyDate, MASCompanyID, MASCustKey, MASCustID ) "
'
'    SQL = SQL & "Values(" & _
'    gsQuoted(AccountID) & ", " & _
'    gsQuoted(MASLink_SGSID) & ", " & _
'    gsQuoted(MASUserID) & ", " & _
'    gsQuoted(MASDate) & ", " & _
'    gsQuoted(MASUserID) & ", " & _
'    gsQuoted(MASDate) & ", " & _
'    gsQuoted(CompanyID) & ", " & _
'    CustKey & ", " & _
'    gsQuoted(CustID) & " " & _
'    " ) "
    
    SQL = "Insert into AccountMASLink_SGS (AccountID, CreateUser, CreateDate, ModifyUser, ModifyDate, MASCompanyID, MASCustKey, MASCustID ) "

    SQL = SQL & "Values(" & _
    gsQuoted(AccountID) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(CompanyID) & ", " & _
    CustKey & ", " & _
    gsQuoted(CustID) & " " & _
    " ) "
    
    Conn.Execute SQL
    
    InsSLXAccount = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If rs.State = 1 Then rs.Close
    Set rs = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox "basSalesOrder.InsSLXAccount()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function

'SGS DEJ 5/14/2010 (Added Method)
Public Function InsSLXAddress(oClass As Object, CompanyID, AccountID As String, ByRef AddressID As String, AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String, AddrLine4 As String, City As String, State As String, Zip As String, Country As String) As Boolean
' AccountID As String, AddressID As String, AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String, AddrLine4 As String, City As String, State As String, Zip As String, Country As String
    
    On Error GoTo Error

    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim rs As New ADODB.Recordset
    Dim SQL As String
    
    Dim MASUserID As String
    Dim MASDate As Date
    
    Dim MASLink_SGSID As String
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    MASDate = Date + Time
        
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    'Check to see if the SLX Address Already Exists
    SQL = "Select * From Address Where IsPrimary = 'Y' and EntityID = " & gsQuoted(AccountID)
    
    If rs.State = 1 Then rs.Close
    Set rs = Conn.Execute(SQL)
    
    If rs.RecordCount > 0 Then
        AddressID = gsGetValidStr(rs("AddressID").Value)
                
        If rs.State = 1 Then rs.Close
        
        SQL = "Select ADDRESSID, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, MASADDRKEY, MASCOMPANYID From AddressMASLink_SGS Where AddressID = " & gsQuoted(AddressID)
        
        Set rs = Conn.Execute(SQL)
        If rs.RecordCount > 0 Then
            rs.MoveFirst
            While Not rs.EOF And Not rs.BOF
                If AddrKey = glGetValidLong(rs("MASAddrKey").Value) Then
                    'Link already exists
                    GoTo CleanUP
                End If
                rs.MoveNext
            Wend
        
        End If
        
        GoTo InsertLink
    End If
    
    'Get Values for creation of new address.
    If rs.State = 1 Then rs.Close
    
    SQL = "slx_dbids('Address', 1)"

    Set rs = Conn.Execute(SQL)

    If rs.RecordCount > 0 Then
        AddressID = gsGetValidStr(rs("ID").Value)
    Else
        InsSLXAddress = False
        MsgBox "Could not get the next key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
        GoTo CleanUP
    End If

'    SQL = "Insert Into Address (EntityID, AddressID, Description, IsMailing, IsPrimary, Address1, Address2, Address3, Address4, City, State, PostalCode, Country, CreateUser, CreateDate, ModifyUser, ModifyDate "
    SQL = "Insert Into Address (EntityID, AddressID, Description, IsMailing, IsPrimary, Address1, Address2, Address3,  City, State, PostalCode, Country, CreateUser, CreateDate, ModifyUser, ModifyDate " & _
    " ) "

'    gsQuoted(AddrLine4) & ", " & _

    SQL = SQL & "Values(" & _
    gsQuoted(AccountID) & ", " & _
    gsQuoted(AddressID) & ", " & _
    gsQuoted(AddrName) & ", " & _
    "'Y', " & _
    "'Y', " & _
    gsQuoted(AddrLine1) & ", " & _
    gsQuoted(AddrLine2) & ", " & _
    gsQuoted(AddrLine3) & ", " & _
    gsQuoted(City) & ", " & _
    gsQuoted(State) & ", " & _
    gsQuoted(Zip) & ", " & _
    gsQuoted(Country) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & " " & _
    " ) "

    Conn.Execute SQL
    
    'Update Account Primary and Shipping Address
    SQL = "Update Account Set AddressID = " & gsQuoted(AddressID) & ", ShippingID = " & gsQuoted(AddressID) & " Where AccountID = " & gsQuoted(AccountID)
    
    Conn.Execute SQL
    
InsertLink:
    'Insert into the Link Table
    If rs.State = 1 Then rs.Close

'    SQL = "slx_dbids('AddressMASLink_SGS', 1)"
'
'    Set rs = Conn.Execute(SQL)
'
'    If rs.RecordCount > 0 Then
'        MASLink_SGSID = gsGetValidStr(rs("ID").Value)
'    Else
'        InsSLXAddress = False
'        MsgBox "Could not get the next SLX to MAS Link Key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
'        GoTo CleanUP
'    End If
'
'    SQL = "Insert into AddressMASLink_SGS (AddressID, AddressMASLink_SGSID, CreateUser, CreateDate, ModifyUser, ModifyDate, MASAddrKey, MASCompanyID ) "
'
'    SQL = SQL & "Values(" & _
'    gsQuoted(AddressID) & ", " & _
'    gsQuoted(MASLink_SGSID) & ", " & _
'    gsQuoted(MASUserID) & ", " & _
'    gsQuoted(MASDate) & ", " & _
'    gsQuoted(MASUserID) & ", " & _
'    gsQuoted(MASDate) & ", " & _
'    AddrKey & ", " & gsQuoted(CompanyID) & " " & _
'    " ) "
    
    SQL = "Insert into AddressMASLink_SGS (AddressID, CreateUser, CreateDate, ModifyUser, ModifyDate, MASAddrKey, MASCompanyID ) "

    SQL = SQL & "Values(" & _
    gsQuoted(AddressID) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    AddrKey & ", " & _
    gsQuoted(CompanyID) & " " & _
    " ) "
    
    Conn.Execute SQL
    
    InsSLXAddress = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If rs.State = 1 Then rs.Close
    Set rs = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox "basSalesOrder.InsSLXAddress()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


'SGS DEJ 5/14/2010 (Added Method)
Public Function GetSLXConnStr(oClass As Object) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    On Error Resume Next
    Dim ConnStr As String
    
    ConnStr = gsGetValidStr(oClass.moAppDB.Lookup("SLXConnStr", "tciSLXConnStr_SGS", Empty))
    
    ConnStr = Decrypt(ConnStr)
    
    GetSLXConnStr = ConnStr
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetSLXConnStr", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

'SGS DEJ 5/14/2010 (Added Method)
Public Function Decrypt(Val As String) As String
    On Error GoTo Error
    
    Dim i As Long
    Dim str As String
    Dim NewVal As Variant
    Dim DecryptedVal As String
    
    For i = 1 To Len(Val)
        str = Mid(Val, i, 4)
        
        If IsNumeric(str) Then
            str = str - 1000
        End If
        
        i = i + 3
        
        NewVal = Chr(CLng(str))
        DecryptedVal = DecryptedVal & NewVal
        
    Next
    
    Decrypt = DecryptedVal
    
    Exit Function
Error:
    MsgBox "basSalesOrder.Decrypt(Val As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

End Function


'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
'SGS DEJ 5/4/2010 - Insert SLX Account/Customer   (STOP)
'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************





