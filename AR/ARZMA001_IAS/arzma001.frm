VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{5CD3F513-DD26-4914-BBBF-95A33A72F67A}#40.0#0"; "ButtonControls.ocx"
Begin VB.Form frmCustomerMaint 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Maintain Customers "
   ClientHeight    =   7305
   ClientLeft      =   1215
   ClientTop       =   1275
   ClientWidth     =   9480
   HelpContextID   =   86263
   Icon            =   "arzma001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7305
   ScaleWidth      =   9480
   Begin VB.TextBox txtMustUseZone 
      Height          =   285
      Left            =   9120
      TabIndex        =   5
      Top             =   540
      Visible         =   0   'False
      WhatsThisHelpID =   86352
      Width           =   195
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   60
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   229
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   219
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   220
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   221
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   222
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   223
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox txtCustName 
      Height          =   285
      Left            =   3450
      MaxLength       =   40
      TabIndex        =   4
      Top             =   540
      WhatsThisHelpID =   86472
      Width           =   3000
   End
   Begin TabDlg.SSTab tabC 
      Height          =   5985
      Left            =   75
      TabIndex        =   61
      Top             =   960
      WhatsThisHelpID =   86471
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   10557
      _Version        =   393216
      Tabs            =   6
      TabsPerRow      =   6
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Main"
      TabPicture(0)   =   "arzma001.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraTab(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Defaults"
      TabPicture(1)   =   "arzma001.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTab(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Bill To/Ship To"
      TabPicture(2)   =   "arzma001.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTab(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&Sales Order"
      TabPicture(3)   =   "arzma001.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraTab(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "&National Acct"
      TabPicture(4)   =   "arzma001.frx":2442
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraTab(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "C&ustomTab"
      TabPicture(5)   =   "arzma001.frx":245E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraTab(5)"
      Tab(5).ControlCount=   1
      Begin VB.Frame fraTab 
         BorderStyle     =   0  'None
         Height          =   5295
         Index           =   5
         Left            =   -74880
         TabIndex        =   248
         Top             =   390
         Width           =   9105
      End
      Begin VB.Frame fraTab 
         Enabled         =   0   'False
         Height          =   5235
         Index           =   4
         Left            =   -74880
         TabIndex        =   196
         Top             =   360
         Width           =   9045
         Begin NEWSOTALib.SOTAMaskedEdit txtNatAcctDesc 
            Height          =   285
            HelpContextID   =   86459
            Left            =   4860
            TabIndex        =   199
            Top             =   210
            WhatsThisHelpID =   17774058
            Width           =   4065
            _Version        =   65536
            _ExtentX        =   7170
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
         End
         Begin VB.CheckBox chkNatHold 
            Alignment       =   1  'Right Justify
            Caption         =   "On Hold"
            Height          =   285
            Left            =   90
            TabIndex        =   203
            Top             =   1200
            WhatsThisHelpID =   86463
            Width           =   2910
         End
         Begin VB.CheckBox chkNatAcctCreditLimit 
            Alignment       =   1  'Right Justify
            Caption         =   "Apply National Account Credit Limit  "
            Height          =   285
            Left            =   105
            TabIndex        =   200
            Top             =   540
            WhatsThisHelpID =   86461
            Width           =   2910
         End
         Begin VB.Frame fraNatSubsidiaryOptions 
            Caption         =   "Subsidiary &Options"
            Height          =   1695
            Left            =   90
            TabIndex        =   210
            Top             =   2100
            Width           =   4785
            Begin VB.CheckBox chkNatConsolidatedStat 
               Alignment       =   1  'Right Justify
               Caption         =   "Consolidated Statement"
               Height          =   255
               Left            =   90
               TabIndex        =   214
               Top             =   1200
               WhatsThisHelpID =   86469
               Width           =   2835
            End
            Begin VB.CheckBox chkNatAllowParentPay 
               Alignment       =   1  'Right Justify
               Caption         =   "Allow Parent to Pay"
               Height          =   255
               Left            =   90
               TabIndex        =   213
               Top             =   780
               WhatsThisHelpID =   86468
               Width           =   2835
            End
            Begin VB.ComboBox cboNatDfltBillTO 
               Height          =   315
               Left            =   2730
               Style           =   2  'Dropdown List
               TabIndex        =   212
               Top             =   300
               WhatsThisHelpID =   86467
               Width           =   1890
            End
            Begin NEWSOTALib.SOTACurrency SOTACurrency2 
               Height          =   285
               Left            =   7500
               TabIndex        =   234
               Top             =   0
               WhatsThisHelpID =   86466
               Width           =   1815
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblNatDfltBillTo 
               AutoSize        =   -1  'True
               Caption         =   "Default Bill-To"
               Height          =   195
               Left            =   120
               TabIndex        =   211
               Top             =   390
               Width           =   990
            End
         End
         Begin EntryLookupControls.TextLookup lkuNatAcct 
            Height          =   285
            Left            =   2820
            TabIndex        =   198
            Top             =   210
            WhatsThisHelpID =   86460
            Width           =   1935
            _ExtentX        =   3413
            _ExtentY        =   503
            ForeColor       =   -2147483640
            MaxLength       =   15
            IsSurrogateKey  =   -1  'True
            LookupID        =   "NationalAccount"
            ParentIDColumn  =   "NationalAcctID"
            ParentKeyColumn =   "NationalAcctKey"
            ParentTable     =   "tarNationalAcct"
            BoundColumn     =   "SalesSourceKey"
            BoundTable      =   "tarNationalAcct"
            IsForeignKey    =   -1  'True
            sSQLReturnCols  =   "NationalAcctID,,;Description,,;"
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtNatParentCustID 
            Height          =   285
            Left            =   2820
            TabIndex        =   205
            TabStop         =   0   'False
            Top             =   1530
            WhatsThisHelpID =   86458
            Width           =   1920
            _Version        =   65536
            _ExtentX        =   3387
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
         End
         Begin NEWSOTALib.SOTACurrency txtCurNatCreditLimit 
            Height          =   285
            Left            =   2820
            TabIndex        =   202
            Top             =   840
            WhatsThisHelpID =   86456
            Width           =   1920
            _Version        =   65536
            bShowCurrency   =   0   'False
            _ExtentX        =   3387
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
            mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTAMaskedEdit txtNatParentCustDesc 
            Height          =   285
            HelpContextID   =   86459
            Left            =   4860
            TabIndex        =   209
            Top             =   1530
            WhatsThisHelpID =   86457
            Width           =   4065
            _Version        =   65536
            _ExtentX        =   7170
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Enabled         =   0   'False
            bProtected      =   -1  'True
         End
         Begin VB.Label lblNatHold 
            AutoSize        =   -1  'True
            Caption         =   "On Hold"
            Height          =   195
            Left            =   5520
            TabIndex        =   215
            Top             =   2640
            Visible         =   0   'False
            Width           =   585
         End
         Begin VB.Label lblNatAcctCreditLimit 
            AutoSize        =   -1  'True
            Caption         =   "Apply National Account Credit Limit"
            Height          =   195
            Left            =   5310
            TabIndex        =   216
            Top             =   3030
            Visible         =   0   'False
            Width           =   3135
         End
         Begin VB.Label lblNatParentCustomer 
            Caption         =   "Parent Customer"
            Height          =   255
            Left            =   120
            TabIndex        =   204
            Top             =   1590
            Width           =   1815
         End
         Begin VB.Label lblNatCreditLimit 
            AutoSize        =   -1  'True
            Caption         =   "Credit Limit Amount"
            Height          =   255
            Left            =   120
            TabIndex        =   201
            Top             =   900
            Width           =   1350
         End
         Begin VB.Label lblNationalAcct 
            Caption         =   "National Account"
            Height          =   255
            Left            =   120
            TabIndex        =   197
            Top             =   240
            Width           =   1815
         End
      End
      Begin VB.Frame fraTab 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   5295
         Index           =   3
         Left            =   -74880
         TabIndex        =   170
         Top             =   390
         Width           =   9105
         Begin VB.Frame fraReqShipDoc 
            Caption         =   "Required Shipping Documents"
            Height          =   2445
            Left            =   45
            TabIndex        =   239
            Top             =   2565
            Width           =   4455
            Begin VB.CheckBox chkInvoiceReqd 
               Caption         =   "Invoice"
               Height          =   285
               Left            =   165
               TabIndex        =   208
               Top             =   1875
               WhatsThisHelpID =   17774091
               Width           =   2100
            End
            Begin VB.CheckBox chkPackListContentsReqd 
               Caption         =   "Pack List with Contents"
               Height          =   345
               Left            =   165
               TabIndex        =   207
               Top             =   1440
               WhatsThisHelpID =   17774090
               Width           =   2280
            End
            Begin VB.CheckBox chkPackListReqd 
               Caption         =   "Pack List"
               Height          =   270
               Left            =   165
               TabIndex        =   206
               Top             =   1080
               WhatsThisHelpID =   17774089
               Width           =   2025
            End
            Begin VB.CheckBox chkShipLabelsReqd 
               Caption         =   "Shipping Labels"
               Height          =   270
               Left            =   165
               TabIndex        =   195
               Top             =   720
               WhatsThisHelpID =   17774088
               Width           =   2265
            End
            Begin VB.CheckBox chkBOLReqd 
               Caption         =   "Bill of Lading"
               Height          =   330
               Left            =   165
               TabIndex        =   194
               Top             =   300
               WhatsThisHelpID =   17774087
               Width           =   2250
            End
         End
         Begin VB.Frame fraForms 
            Caption         =   "&Forms"
            Height          =   1425
            Left            =   0
            TabIndex        =   174
            Top             =   1080
            Width           =   4485
            Begin EntryLookupControls.TextLookup lkuLabelForm 
               Height          =   315
               Left            =   2160
               TabIndex        =   178
               Top             =   600
               WhatsThisHelpID =   86447
               Width           =   2085
               _ExtentX        =   3678
               _ExtentY        =   556
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "BusinessForm"
               ParentIDColumn  =   "BusinessFormID"
               ParentKeyColumn =   "BusinessFormKey"
               ParentTable     =   "tciBusinessForm"
               BoundColumn     =   "ShipLabelFormKey"
               BoundTable      =   "tarCustAddr"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "BusinessFormID,lkuLabelForm,;"
            End
            Begin EntryLookupControls.TextLookup lkuPackListForm 
               Height          =   315
               Left            =   2160
               TabIndex        =   180
               Top             =   960
               WhatsThisHelpID =   86446
               Width           =   2085
               _ExtentX        =   3678
               _ExtentY        =   556
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "BusinessForm"
               ParentIDColumn  =   "BusinessFormID"
               ParentKeyColumn =   "BusinessFormKey"
               ParentTable     =   "tciBusinessForm"
               BoundColumn     =   "PackListFormKey"
               BoundTable      =   "tarCustAddr"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "BusinessFormID,lkuPackListForm,;"
            End
            Begin EntryLookupControls.TextLookup lkuAckForm 
               Height          =   315
               Left            =   2160
               TabIndex        =   176
               Top             =   240
               WhatsThisHelpID =   86445
               Width           =   2085
               _ExtentX        =   3678
               _ExtentY        =   556
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "BusinessForm"
               ParentIDColumn  =   "BusinessFormID"
               ParentKeyColumn =   "BusinessFormKey"
               ParentTable     =   "tciBusinessForm"
               BoundColumn     =   "SOAckFormKey"
               BoundTable      =   "tarCustAddr"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "BusinessFormID,lkuAckForm,;"
            End
            Begin VB.Label lblAckForm 
               Caption         =   "Acknowledgement Form"
               Height          =   255
               Left            =   120
               TabIndex        =   175
               Top             =   270
               Width           =   1815
            End
            Begin VB.Label lblPackListForm 
               Caption         =   "Pack List Form"
               Height          =   270
               Left            =   120
               TabIndex        =   179
               Top             =   1035
               Width           =   1485
            End
            Begin VB.Label lblLabelForm 
               Caption         =   "Shipping Label Form"
               Height          =   195
               Left            =   120
               TabIndex        =   177
               Top             =   660
               Width           =   1455
            End
         End
         Begin VB.Frame fraShipping 
            Caption         =   "S&hipping"
            Height          =   3345
            Left            =   4620
            TabIndex        =   181
            Top             =   0
            Width           =   4395
            Begin VB.CheckBox chkCloseSOLineOnFirst 
               Alignment       =   1  'Right Justify
               Caption         =   "Close SO Line On First Shipment"
               Height          =   240
               Left            =   105
               TabIndex        =   193
               Top             =   2910
               WhatsThisHelpID =   17771471
               Width           =   3255
            End
            Begin VB.CheckBox chkCloseSOOnFirst 
               Alignment       =   1  'Right Justify
               Caption         =   "Close SO On First Shipment"
               Height          =   240
               Left            =   105
               TabIndex        =   192
               Top             =   2520
               WhatsThisHelpID =   17771472
               Width           =   3255
            End
            Begin VB.CheckBox chkShipComplete 
               Alignment       =   1  'Right Justify
               Caption         =   "Ship Complete"
               Height          =   240
               Left            =   105
               TabIndex        =   191
               Top             =   2135
               WhatsThisHelpID =   17771777
               Width           =   3255
            End
            Begin VB.CheckBox chkInvtSubst 
               Alignment       =   1  'Right Justify
               Caption         =   "Allow Inventory Substitutions"
               Height          =   240
               Left            =   105
               TabIndex        =   190
               Top             =   1740
               WhatsThisHelpID =   86437
               Width           =   3255
            End
            Begin EntryLookupControls.TextLookup lkuSource 
               Height          =   315
               Left            =   2370
               TabIndex        =   183
               Top             =   240
               WhatsThisHelpID =   86439
               Width           =   1875
               _ExtentX        =   3307
               _ExtentY        =   556
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "SOSource    "
               ParentIDColumn  =   "SalesSourceID"
               ParentKeyColumn =   "SalesSourceKey"
               ParentTable     =   "tsoSalesSource"
               BoundColumn     =   "SalesSourceKey"
               BoundTable      =   "tarCustomer"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "SalesSourceID,lkuSource,;"
            End
            Begin EntryLookupControls.TextLookup lkuClosestWhse 
               Height          =   315
               Left            =   2370
               TabIndex        =   189
               Top             =   1350
               WhatsThisHelpID =   86438
               Width           =   1875
               _ExtentX        =   3307
               _ExtentY        =   556
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "Warehouse"
               ParentIDColumn  =   "WhseID"
               ParentKeyColumn =   "WhseKey"
               ParentTable     =   "timWarehouse"
               BoundColumn     =   "WhseKey"
               BoundTable      =   "tarCustAddr"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "WhseID,lkuClosestWhse,;Description,,;"
            End
            Begin SOTADropDownControl.SOTADropDown ddnShipPriority 
               Height          =   315
               Left            =   2370
               TabIndex        =   185
               Top             =   630
               WhatsThisHelpID =   86436
               Width           =   1905
               _ExtentX        =   3360
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnShipPriority"
            End
            Begin NEWSOTALib.SOTANumber numShipDays 
               Height          =   315
               Left            =   2370
               TabIndex        =   187
               Top             =   990
               WhatsThisHelpID =   86435
               Width           =   915
               _Version        =   65536
               _ExtentX        =   1614
               _ExtentY        =   556
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#<IRp0>|.##"
               text            =   "    0.00"
               dMaxValue       =   32767
               sIntegralPlaces =   5
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblShipDays 
               Caption         =   "Ship Days"
               Height          =   255
               Left            =   120
               TabIndex        =   186
               Top             =   1050
               Width           =   1095
            End
            Begin VB.Label lblShippingPriority 
               Caption         =   "Shipping Priority"
               Height          =   225
               Left            =   120
               TabIndex        =   184
               Top             =   675
               Width           =   1305
            End
            Begin VB.Label lblSource 
               Caption         =   "Source"
               Height          =   285
               Left            =   120
               TabIndex        =   182
               Top             =   330
               Width           =   1245
            End
            Begin VB.Label lblClosestWhse 
               Caption         =   "Closest Warehouse"
               Height          =   315
               Left            =   120
               TabIndex        =   188
               Top             =   1410
               Width           =   1455
            End
         End
         Begin VB.Frame fraShip 
            Caption         =   "&Acknowledgements"
            Height          =   1035
            Left            =   0
            TabIndex        =   171
            Top             =   0
            Width           =   4485
            Begin VB.CheckBox chkReqAck 
               Caption         =   "Require Acknowledgement"
               Height          =   255
               Left            =   120
               TabIndex        =   173
               Top             =   600
               WhatsThisHelpID =   86429
               Width           =   2490
            End
            Begin VB.CheckBox chkPrintOrdAck 
               Caption         =   "Print Order Acknowledgements"
               Height          =   285
               Left            =   120
               TabIndex        =   172
               Top             =   240
               WhatsThisHelpID =   86428
               Width           =   2490
            End
         End
      End
      Begin VB.Frame fraTab 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   5535
         Index           =   2
         Left            =   -74880
         TabIndex        =   169
         Top             =   390
         Width           =   9105
         Begin VB.Frame fraInvoiceMsg 
            Height          =   615
            Left            =   15
            TabIndex        =   56
            Top             =   4485
            Width           =   8760
            Begin VB.TextBox txtInvoiceMessage 
               Height          =   285
               Left            =   1980
               MaxLength       =   100
               TabIndex        =   58
               Top             =   210
               WhatsThisHelpID =   86407
               Width           =   6570
            End
            Begin VB.Label lblInvoiceMessage 
               AutoSize        =   -1  'True
               Caption         =   "&Invoice Message"
               Height          =   195
               Index           =   1
               Left            =   150
               TabIndex        =   57
               Top             =   210
               Width           =   1215
            End
         End
         Begin VB.Frame fraShipTo 
            Caption         =   "S&hipping"
            Height          =   3120
            Left            =   4350
            TabIndex        =   35
            Top             =   0
            Width           =   4455
            Begin VB.CheckBox chkResidential 
               Caption         =   "Residential"
               Height          =   210
               Left            =   165
               TabIndex        =   238
               Top             =   2520
               WhatsThisHelpID =   17774059
               Width           =   1665
            End
            Begin SOTADropDownControl.SOTADropDown ddnFreightMethod 
               Height          =   315
               Left            =   2460
               TabIndex        =   49
               Top             =   2145
               WhatsThisHelpID =   17774060
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnFreightMethod"
            End
            Begin SOTADropDownControl.SOTADropDown ddnDfltCarrierBillMeth 
               Height          =   315
               Left            =   2460
               TabIndex        =   48
               Top             =   1815
               WhatsThisHelpID =   17774061
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   "ddnDfltCarrierBillMeth"
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtDfltCarrierAcctNo 
               Height          =   315
               Left            =   2460
               TabIndex        =   47
               Top             =   1500
               WhatsThisHelpID =   17774062
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   556
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   20
            End
            Begin VB.TextBox txtShipTo 
               Height          =   285
               Left            =   2460
               MaxLength       =   15
               TabIndex        =   37
               Top             =   210
               WhatsThisHelpID =   86403
               Width           =   1530
            End
            Begin VB.ComboBox cboShipZone 
               Height          =   315
               Left            =   2460
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   46
               Top             =   1170
               Visible         =   0   'False
               WhatsThisHelpID =   86402
               Width           =   1815
            End
            Begin VB.TextBox txtFOB 
               Height          =   285
               Left            =   2460
               MaxLength       =   20
               TabIndex        =   43
               Top             =   855
               WhatsThisHelpID =   86401
               Width           =   1530
            End
            Begin VB.TextBox txtShipMeth 
               Height          =   285
               Left            =   2460
               MaxLength       =   15
               TabIndex        =   40
               Top             =   525
               WhatsThisHelpID =   86400
               Width           =   1530
            End
            Begin LookupViewControl.LookupView navFOB 
               Height          =   285
               Left            =   4005
               TabIndex        =   44
               TabStop         =   0   'False
               Top             =   855
               WhatsThisHelpID =   86404
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navShipTo 
               Height          =   285
               Left            =   3990
               TabIndex        =   38
               TabStop         =   0   'False
               Top             =   210
               WhatsThisHelpID =   86399
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navShipMeth 
               Height          =   285
               Left            =   3990
               TabIndex        =   41
               TabStop         =   0   'False
               Top             =   525
               WhatsThisHelpID =   86398
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.Label lblFreightMethod 
               Caption         =   "Freight Method"
               Height          =   210
               Left            =   150
               TabIndex        =   237
               Top             =   2220
               Width           =   1950
            End
            Begin VB.Label lblDfltCarrierBillMeth 
               Caption         =   "Default Carrier Bill Method"
               Height          =   240
               Left            =   150
               TabIndex        =   236
               Top             =   1870
               Width           =   1905
            End
            Begin VB.Label lblDfltCarrierAcctNo 
               Caption         =   "Default Carrier Account No. "
               Height          =   255
               Left            =   150
               TabIndex        =   235
               Top             =   1505
               Width           =   2100
            End
            Begin VB.Label lblShipTo 
               AutoSize        =   -1  'True
               Caption         =   "Default Ship-To Address"
               Height          =   195
               Left            =   150
               TabIndex        =   36
               Top             =   285
               Width           =   1725
            End
            Begin VB.Label lblFOB 
               AutoSize        =   -1  'True
               Caption         =   "F.O.B."
               Height          =   195
               Left            =   150
               TabIndex        =   42
               Top             =   895
               Width           =   450
            End
            Begin VB.Label lblShipZone 
               AutoSize        =   -1  'True
               Caption         =   "Shipping Zone"
               Height          =   195
               Left            =   150
               TabIndex        =   45
               Top             =   1200
               Visible         =   0   'False
               Width           =   1035
            End
            Begin VB.Label lblShipMeth 
               AutoSize        =   -1  'True
               Caption         =   "Ship Via"
               Height          =   195
               Left            =   150
               TabIndex        =   39
               Top             =   590
               Width           =   585
            End
         End
         Begin VB.Frame fraSales 
            Caption         =   "S&ales"
            Height          =   1335
            Left            =   -15
            TabIndex        =   26
            Top             =   3165
            Width           =   4155
            Begin VB.TextBox txtSperName 
               Height          =   285
               Left            =   3870
               TabIndex        =   30
               Top             =   240
               Visible         =   0   'False
               WhatsThisHelpID =   86392
               Width           =   135
            End
            Begin VB.ComboBox cboSalesTerritory 
               Height          =   315
               Left            =   2050
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   34
               Top             =   930
               WhatsThisHelpID =   86391
               Width           =   1830
            End
            Begin VB.TextBox txtSalesperson 
               Height          =   285
               Left            =   2050
               MaxLength       =   12
               TabIndex        =   28
               Top             =   240
               WhatsThisHelpID =   86390
               Width           =   1500
            End
            Begin VB.ComboBox cboCommPlan 
               Height          =   315
               Left            =   2050
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   32
               Top             =   570
               WhatsThisHelpID =   86389
               Width           =   1830
            End
            Begin LookupViewControl.LookupView navSalesperson 
               Height          =   285
               Left            =   3580
               TabIndex        =   29
               TabStop         =   0   'False
               Top             =   240
               WhatsThisHelpID =   86388
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.Label lblSalesperson 
               AutoSize        =   -1  'True
               Caption         =   "Salesperson"
               Height          =   195
               Left            =   150
               TabIndex        =   27
               Top             =   270
               Width           =   870
            End
            Begin VB.Label lblSalesTerritory 
               AutoSize        =   -1  'True
               Caption         =   "Territory"
               Height          =   195
               Left            =   150
               TabIndex        =   33
               Top             =   960
               Width           =   570
            End
            Begin VB.Label lblCommPlan 
               AutoSize        =   -1  'True
               Caption         =   "Commission Plan"
               Height          =   195
               Left            =   150
               TabIndex        =   31
               Top             =   630
               Width           =   1185
            End
         End
         Begin VB.Frame fraMultiCurrency 
            Caption         =   "&Currency"
            Height          =   1305
            Left            =   4305
            TabIndex        =   50
            Top             =   3195
            Width           =   4455
            Begin VB.TextBox txtCurrency 
               Height          =   285
               Left            =   2460
               MaxLength       =   3
               TabIndex        =   52
               Top             =   240
               WhatsThisHelpID =   86383
               Width           =   1500
            End
            Begin VB.ComboBox cboCurrExchSchd 
               Height          =   315
               Left            =   2460
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   55
               Top             =   600
               WhatsThisHelpID =   86382
               Width           =   1815
            End
            Begin LookupViewControl.LookupView navCurrency 
               Height          =   285
               Left            =   3990
               TabIndex        =   53
               TabStop         =   0   'False
               Top             =   240
               WhatsThisHelpID =   86381
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin VB.Label lblCurrID 
               AutoSize        =   -1  'True
               Caption         =   "Currency"
               Height          =   195
               Left            =   120
               TabIndex        =   51
               Top             =   300
               Width           =   630
            End
            Begin VB.Label lblCurrExchSchd 
               AutoSize        =   -1  'True
               Caption         =   "Exchange Schedule"
               Height          =   195
               Left            =   120
               TabIndex        =   54
               Top             =   660
               Width           =   1440
            End
         End
         Begin VB.Frame fraBillTo 
            Caption         =   "Bi&lling"
            Height          =   3120
            Left            =   0
            TabIndex        =   6
            Top             =   0
            Width           =   4155
            Begin EntryLookupControls.TextLookup lkuDefaultCreditCard 
               Height          =   285
               Left            =   2040
               TabIndex        =   25
               Top             =   2700
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   503
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "CreditCardName"
               ParentIDColumn  =   "CreditCardName"
               ParentKeyColumn =   "CreditCardKey"
               ParentTable     =   "tccCustCreditCard"
               BoundColumn     =   "CreditCardKey"
               BoundTable      =   "tarCustAddr"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               sSQLReturnCols  =   "CreditCardName,,;CreditCardKey,,;"
            End
            Begin VB.TextBox txtPmtTermsDesc 
               Height          =   315
               Left            =   3870
               TabIndex        =   15
               Top             =   900
               Visible         =   0   'False
               WhatsThisHelpID =   86376
               Width           =   135
            End
            Begin VB.ComboBox cboLanguage 
               Height          =   315
               Left            =   2050
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   17
               Top             =   1260
               WhatsThisHelpID =   86375
               Width           =   1830
            End
            Begin VB.TextBox txtBillTo 
               Height          =   285
               Left            =   2050
               MaxLength       =   15
               TabIndex        =   8
               Top             =   240
               WhatsThisHelpID =   86374
               Width           =   1500
            End
            Begin VB.ComboBox cboPmtTerms 
               Height          =   315
               Left            =   2050
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   14
               Top             =   900
               WhatsThisHelpID =   86373
               Width           =   1830
            End
            Begin VB.TextBox txtInvoiceForm 
               Height          =   285
               Left            =   2050
               MaxLength       =   15
               TabIndex        =   11
               Top             =   570
               WhatsThisHelpID =   86372
               Width           =   1500
            End
            Begin NEWSOTALib.SOTANumber nbrPriceAdjPct 
               Height          =   315
               Left            =   2040
               TabIndex        =   23
               Top             =   2340
               WhatsThisHelpID =   17375652
               Width           =   1845
               _Version        =   65536
               _ExtentX        =   3254
               _ExtentY        =   556
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin SOTADropDownControl.SOTADropDown ddnPriceBase 
               Height          =   315
               Left            =   2050
               TabIndex        =   21
               Top             =   1980
               WhatsThisHelpID =   86377
               Width           =   1845
               _ExtentX        =   3254
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   2
               Text            =   "ddnPriceBase"
            End
            Begin LookupViewControl.LookupView navBillTo 
               Height          =   285
               Left            =   3580
               TabIndex        =   9
               TabStop         =   0   'False
               Top             =   240
               WhatsThisHelpID =   86371
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navInvoiceForm 
               Height          =   285
               Left            =   3580
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   570
               WhatsThisHelpID =   86370
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin EntryLookupControls.TextLookup lkuPriceGroup 
               Height          =   315
               Left            =   2050
               TabIndex        =   19
               Top             =   1620
               WhatsThisHelpID =   86369
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   556
               ForeColor       =   -2147483640
               IsSurrogateKey  =   -1  'True
               LookupID        =   "IMCPGrpDesc "
               ParentIDColumn  =   "CustPriceGroupID"
               ParentKeyColumn =   "CustPriceGroupKey"
               ParentTable     =   "timCustPriceGroup"
               BoundColumn     =   "CustPriceGroupKey"
               BoundTable      =   "tarCustAddr"
               IsForeignKey    =   -1  'True
               sSQLReturnCols  =   "CustPriceGroupID,lkuPriceGroup,;Description,,;"
            End
            Begin VB.Label lblDefaultCreditCard 
               Caption         =   "Default Credit Card/EFT"
               Height          =   255
               Left            =   120
               TabIndex        =   24
               Top             =   2730
               Width           =   2055
            End
            Begin VB.Label lblPriceAdjPct 
               Caption         =   "Price Adjustment Percent"
               Height          =   255
               Left            =   120
               TabIndex        =   22
               Top             =   2370
               Width           =   1935
            End
            Begin VB.Label lblPriceBase 
               Caption         =   "Price Base"
               Height          =   375
               Left            =   120
               TabIndex        =   20
               Top             =   2010
               Width           =   1215
            End
            Begin VB.Label lblPriceGroup 
               Caption         =   "Price Group"
               Height          =   375
               Left            =   120
               TabIndex        =   18
               Top             =   1650
               Width           =   1155
            End
            Begin VB.Label lblLanguage 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Language"
               Height          =   195
               Left            =   120
               TabIndex        =   16
               Top             =   1290
               Width           =   720
            End
            Begin VB.Label lblBillTo 
               AutoSize        =   -1  'True
               Caption         =   "Default Bill-To Address"
               Height          =   195
               Left            =   120
               TabIndex        =   7
               Top             =   300
               Width           =   1605
            End
            Begin VB.Label lblPmtTerms 
               AutoSize        =   -1  'True
               Caption         =   "Payment Terms"
               Height          =   195
               Left            =   120
               TabIndex        =   13
               Top             =   960
               Width           =   1095
            End
            Begin VB.Label lblInvoiceForm 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Form"
               Height          =   195
               Left            =   120
               TabIndex        =   10
               Top             =   630
               Width           =   915
            End
         End
         Begin VB.CommandButton cmdSalesTax 
            Caption         =   "Sales &Tax..."
            Height          =   360
            Left            =   30
            TabIndex        =   59
            Top             =   5130
            WhatsThisHelpID =   86362
            Width           =   1605
         End
      End
      Begin VB.Frame fraTab 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   5535
         Index           =   1
         Left            =   -74880
         TabIndex        =   129
         Top             =   345
         Width           =   9105
         Begin VB.Frame fraCCUpcharges 
            Caption         =   "Credit Card and EFT &Upcharges"
            Height          =   1335
            Left            =   5160
            TabIndex        =   243
            Top             =   4080
            Width           =   3885
            Begin SOTADropDownControl.SOTADropDown ddnCCUpchargeType 
               Height          =   315
               Left            =   2160
               TabIndex        =   246
               Top             =   360
               Width           =   1500
               _ExtentX        =   2646
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Style           =   1
               Text            =   "ddnCCUpchargeType"
               DefaultItemData =   "0"
            End
            Begin NEWSOTALib.SOTACurrency curDfltMaxUpcharge 
               Height          =   285
               Left            =   2160
               TabIndex        =   247
               Top             =   720
               Width           =   1500
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   2646
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblUpchargeType 
               AutoSize        =   -1  'True
               Caption         =   "Type"
               Height          =   195
               Left            =   120
               TabIndex        =   245
               Top             =   380
               Width           =   360
            End
            Begin VB.Label lblAmount 
               AutoSize        =   -1  'True
               Caption         =   "Amount"
               Height          =   195
               Left            =   120
               TabIndex        =   244
               Top             =   735
               Width           =   540
            End
         End
         Begin VB.Frame fraSalesTax 
            Height          =   1185
            Left            =   660
            TabIndex        =   167
            Top             =   3990
            Visible         =   0   'False
            Width           =   5805
            Begin FPSpreadADO.fpSpread grdExmpt 
               Height          =   765
               Left            =   90
               TabIndex        =   168
               Top             =   240
               WhatsThisHelpID =   86426
               Width           =   5415
               _Version        =   524288
               _ExtentX        =   9551
               _ExtentY        =   1349
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaxCols         =   3
               MaxRows         =   1
               SpreadDesigner  =   "arzma001.frx":247A
               VisibleCols     =   500
               VisibleRows     =   500
               AppearanceStyle =   0
            End
         End
         Begin VB.Frame fraBillDflts 
            Caption         =   "Bi&lling Defaults"
            Height          =   4005
            Left            =   0
            TabIndex        =   130
            Top             =   0
            Width           =   5055
            Begin EntryLookupControls.GLAcctLookup glaReturns 
               Height          =   285
               Left            =   2280
               TabIndex        =   144
               Top             =   2100
               WhatsThisHelpID =   17774063
               Width           =   2685
               _ExtentX        =   4736
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "DfltSalesReturnAcctKey"
               BoundTable      =   "tarCustomer"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               VisibleDesc     =   0   'False
               WidthDesc       =   2085
               WidthAcct       =   2025
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,,;"
            End
            Begin VB.ComboBox cboStmtCycle 
               Height          =   315
               Left            =   2295
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   135
               Top             =   605
               WhatsThisHelpID =   86332
               Width           =   1830
            End
            Begin VB.ComboBox cboPriceList 
               Height          =   315
               Left            =   2280
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   152
               Top             =   3585
               Visible         =   0   'False
               WhatsThisHelpID =   86331
               Width           =   1830
            End
            Begin VB.TextBox txtStmtForm 
               Height          =   285
               Left            =   2295
               MaxLength       =   15
               TabIndex        =   132
               Top             =   240
               WhatsThisHelpID =   86330
               Width           =   1500
            End
            Begin VB.ComboBox cboBillingType 
               Height          =   315
               Left            =   2295
               Style           =   2  'Dropdown List
               TabIndex        =   137
               Top             =   1000
               WhatsThisHelpID =   86329
               Width           =   1830
            End
            Begin NEWSOTALib.SOTACurrency curLateChgAmt 
               Height          =   285
               Left            =   2280
               TabIndex        =   146
               Top             =   2471
               WhatsThisHelpID =   86328
               Width           =   1845
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3254
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin LookupViewControl.LookupView navStmtForm 
               Height          =   285
               Left            =   3810
               TabIndex        =   133
               TabStop         =   0   'False
               Top             =   240
               WhatsThisHelpID =   86327
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtDfltItem 
               Height          =   285
               Left            =   2295
               TabIndex        =   139
               Top             =   1395
               WhatsThisHelpID =   86326
               Width           =   2325
               _Version        =   65536
               _ExtentX        =   4101
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               bAutoSelect     =   0   'False
               lMaxLength      =   30
            End
            Begin LookupViewControl.LookupView navDfltItem 
               Height          =   285
               Left            =   4650
               TabIndex        =   140
               TabStop         =   0   'False
               Top             =   1395
               WhatsThisHelpID =   86325
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTANumber numFinChgPct 
               Height          =   285
               Left            =   2280
               TabIndex        =   148
               Top             =   2842
               WhatsThisHelpID =   86324
               Width           =   1005
               _Version        =   65536
               _ExtentX        =   1773
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#<ILp0>#<IRp0>|.##"
               text            =   " 0.00"
               dMaxValue       =   99.99
               sIntegralPlaces =   2
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numTradeDiscPct 
               Height          =   285
               Left            =   2280
               TabIndex        =   150
               Top             =   3213
               WhatsThisHelpID =   86323
               Width           =   1005
               _Version        =   65536
               _ExtentX        =   1773
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>#<ILp0>#<IRp0>|.##"
               text            =   " 0.00"
               dMaxValue       =   99.99
               sIntegralPlaces =   2
               sDecimalPlaces  =   2
            End
            Begin EntryLookupControls.GLAcctLookup glaAccount 
               Height          =   285
               Left            =   2295
               TabIndex        =   142
               Top             =   1740
               WhatsThisHelpID =   86321
               Width           =   2685
               _ExtentX        =   4736
               _ExtentY        =   503
               ForeColor       =   -2147483640
               LookupID        =   "Account"
               FillOnJump      =   0   'False
               IsSurrogateKey  =   -1  'True
               ParentIDColumn  =   "GLAcctNo"
               ParentKeyColumn =   "GLAcctKey"
               ParentTable     =   "tglAccount"
               BoundColumn     =   "DfltSalesAcctKey"
               BoundTable      =   "tarCustomer"
               IsForeignKey    =   -1  'True
               Datatype        =   12
               VisibleDesc     =   0   'False
               WidthDesc       =   2325
               WidthAcct       =   2025
               FinancialType   =   2
               CurrencyID      =   "   "
               sSQLReturnCols  =   "GLAcctNo,,;"
            End
            Begin VB.Label lblDfltReturnsAcc 
               AutoSize        =   -1  'True
               Caption         =   "Default Returns Account"
               Height          =   195
               Left            =   120
               TabIndex        =   143
               Top             =   2145
               Width           =   1755
            End
            Begin VB.Label lblStmtForm 
               AutoSize        =   -1  'True
               Caption         =   "Statement Form"
               Height          =   195
               Left            =   120
               TabIndex        =   131
               Top             =   300
               Width           =   1110
            End
            Begin VB.Label lblStmtCycle 
               AutoSize        =   -1  'True
               Caption         =   "Statement Cycle"
               Height          =   195
               Left            =   120
               TabIndex        =   134
               Top             =   680
               Width           =   1155
            End
            Begin VB.Label lblDfltItem 
               AutoSize        =   -1  'True
               Caption         =   "Default Item"
               Height          =   195
               Left            =   120
               TabIndex        =   138
               Top             =   1440
               Width           =   855
            End
            Begin VB.Label lblGLAccount 
               AutoSize        =   -1  'True
               Caption         =   "Default Sales Account"
               Height          =   195
               Left            =   120
               TabIndex        =   141
               Top             =   1800
               Width           =   1590
            End
            Begin VB.Label lblTradeDiscPct 
               AutoSize        =   -1  'True
               Caption         =   "Trade Discount Percent"
               Height          =   195
               Left            =   120
               TabIndex        =   149
               Top             =   3258
               Width           =   1695
            End
            Begin VB.Label lblPriceList 
               AutoSize        =   -1  'True
               Caption         =   "Price List"
               Height          =   195
               Left            =   120
               TabIndex        =   151
               Top             =   3645
               Visible         =   0   'False
               Width           =   645
            End
            Begin VB.Label lblLateChgAmt 
               AutoSize        =   -1  'True
               Caption         =   "Finance Charge Flat Amount"
               Height          =   195
               Left            =   120
               TabIndex        =   145
               Top             =   2516
               Width           =   2010
            End
            Begin VB.Label lblFinChgPct 
               AutoSize        =   -1  'True
               Caption         =   "Finance Charge Percent"
               Height          =   195
               Left            =   120
               TabIndex        =   147
               Top             =   2887
               Width           =   1725
            End
            Begin VB.Label lblBillingType 
               AutoSize        =   -1  'True
               Caption         =   "Billing Type"
               Height          =   195
               Left            =   120
               TabIndex        =   136
               Top             =   1060
               Width           =   810
            End
         End
         Begin VB.Frame fraOptions 
            Caption         =   "&Options"
            Height          =   1965
            Left            =   5160
            TabIndex        =   162
            Top             =   2040
            Width           =   3885
            Begin VB.CheckBox chkPOReq 
               Alignment       =   1  'Right Justify
               Caption         =   "Purchase Order Required"
               Height          =   195
               Left            =   120
               TabIndex        =   166
               Top             =   1530
               WhatsThisHelpID =   86309
               Width           =   2325
            End
            Begin VB.CheckBox chkAllowrefunds 
               Alignment       =   1  'Right Justify
               Caption         =   "Allow Refunds"
               Height          =   210
               Left            =   120
               TabIndex        =   163
               Top             =   360
               WhatsThisHelpID =   86308
               Width           =   2325
            End
            Begin VB.CheckBox chkAllowWriteOffs 
               Alignment       =   1  'Right Justify
               Caption         =   "Allow Write Offs"
               Height          =   210
               Left            =   120
               TabIndex        =   164
               Top             =   750
               WhatsThisHelpID =   86307
               Width           =   2325
            End
            Begin VB.CheckBox chkDunningMsg 
               Alignment       =   1  'Right Justify
               Caption         =   "Print Dunning Message"
               Height          =   210
               Left            =   120
               TabIndex        =   165
               Top             =   1140
               WhatsThisHelpID =   86306
               Width           =   2325
            End
         End
         Begin VB.Frame fraUserField 
            Caption         =   "&Custom Fields"
            Height          =   1755
            Left            =   5160
            TabIndex        =   153
            Top             =   0
            Width           =   3885
            Begin VB.ComboBox cboUserFld 
               Height          =   315
               Index           =   0
               Left            =   2250
               Style           =   2  'Dropdown List
               TabIndex        =   159
               Top             =   960
               WhatsThisHelpID =   86284
               Width           =   1500
            End
            Begin VB.TextBox txtUserFld 
               Height          =   285
               Index           =   0
               Left            =   2250
               MaxLength       =   15
               TabIndex        =   155
               Top             =   240
               WhatsThisHelpID =   86283
               Width           =   1500
            End
            Begin SOTACalendarControl.SOTACalendar dteUserFld 
               CausesValidation=   0   'False
               Height          =   315
               Index           =   0
               Left            =   2250
               TabIndex        =   161
               Top             =   1320
               WhatsThisHelpID =   86282
               Width           =   1500
               _ExtentX        =   2646
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin NEWSOTALib.SOTANumber numUserFld 
               Height          =   285
               Index           =   0
               Left            =   2250
               TabIndex        =   157
               Top             =   600
               WhatsThisHelpID =   86281
               Width           =   1500
               _Version        =   65536
               _ExtentX        =   2646
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblUserFld 
               AutoSize        =   -1  'True
               Caption         =   "User field 1"
               Height          =   195
               Index           =   0
               Left            =   120
               TabIndex        =   154
               Top             =   300
               Width           =   840
            End
            Begin VB.Label lblUserFld 
               AutoSize        =   -1  'True
               Caption         =   "User field 2"
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   156
               Top             =   660
               Width           =   840
            End
            Begin VB.Label lblUserFld 
               AutoSize        =   -1  'True
               Caption         =   "User field 3"
               Height          =   195
               Index           =   2
               Left            =   120
               TabIndex        =   158
               Top             =   1020
               Width           =   840
            End
            Begin VB.Label lblUserFld 
               AutoSize        =   -1  'True
               Caption         =   "User field 4"
               Height          =   195
               Index           =   3
               Left            =   120
               TabIndex        =   160
               Top             =   1410
               Width           =   840
            End
         End
      End
      Begin VB.Frame fraTab 
         BorderStyle     =   0  'None
         Height          =   5400
         Index           =   0
         Left            =   120
         TabIndex        =   232
         Top             =   390
         Width           =   9105
         Begin VB.CommandButton cmdCreditCards 
            Caption         =   "Credit Cards/EFT..."
            Height          =   285
            Left            =   3300
            TabIndex        =   241
            Top             =   5100
            Width           =   1845
         End
         Begin VB.CommandButton cmdExemptions 
            Caption         =   "Sales Tax &Exemptions..."
            Height          =   285
            Left            =   7080
            TabIndex        =   240
            Top             =   5100
            WhatsThisHelpID =   17779627
            Width           =   1965
         End
         Begin VB.CommandButton cmdDocTrans 
            Caption         =   "Doc Transm&ittal..."
            Height          =   285
            Left            =   5190
            TabIndex        =   128
            Top             =   5100
            WhatsThisHelpID =   86449
            Width           =   1845
         End
         Begin VB.Frame fraContact 
            Caption         =   "Primary C&ontact"
            Height          =   1905
            Left            =   3780
            TabIndex        =   111
            Top             =   3150
            Width           =   5265
            Begin VB.CommandButton cmdLaunchEMail 
               Caption         =   "E-Mail..."
               Height          =   255
               Left            =   105
               TabIndex        =   125
               Top             =   1515
               WhatsThisHelpID =   86424
               Width           =   795
            End
            Begin VB.TextBox txtPhoneExt 
               Height          =   285
               Left            =   4110
               MaxLength       =   4
               TabIndex        =   120
               Top             =   840
               WhatsThisHelpID =   41
               Width           =   975
            End
            Begin VB.TextBox txtFaxExt 
               Height          =   285
               Left            =   4110
               MaxLength       =   4
               TabIndex        =   124
               Top             =   1170
               WhatsThisHelpID =   44
               Width           =   975
            End
            Begin VB.TextBox txtContName 
               Height          =   285
               Left            =   1410
               MaxLength       =   40
               TabIndex        =   113
               Top             =   180
               WhatsThisHelpID =   38
               Width           =   3315
            End
            Begin VB.CommandButton cmdContAddr 
               Caption         =   "Ot&her Contacts"
               Height          =   285
               Index           =   0
               Left            =   3450
               TabIndex        =   127
               Top             =   1530
               WhatsThisHelpID =   86420
               Width           =   1665
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtContTitle 
               Height          =   285
               Left            =   1410
               TabIndex        =   116
               Top             =   510
               WhatsThisHelpID =   39
               Width           =   3660
               _Version        =   65536
               _ExtentX        =   6456
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               bAutoSelect     =   0   'False
               lMaxLength      =   40
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtEmail 
               Height          =   285
               Left            =   1410
               TabIndex        =   126
               Top             =   1500
               WhatsThisHelpID =   42
               Width           =   1905
               _Version        =   65536
               _ExtentX        =   3360
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               bAutoSelect     =   0   'False
               lMaxLength      =   256
            End
            Begin LookupViewControl.LookupView navContact 
               Height          =   285
               Left            =   4770
               TabIndex        =   114
               TabStop         =   0   'False
               Top             =   180
               WhatsThisHelpID =   86417
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtPhone 
               Height          =   285
               Left            =   1410
               TabIndex        =   118
               Top             =   840
               WhatsThisHelpID =   40
               Width           =   1905
               _Version        =   65536
               _ExtentX        =   3360
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               bAutoSelect     =   0   'False
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtFax 
               Height          =   285
               Left            =   1410
               TabIndex        =   122
               Top             =   1170
               WhatsThisHelpID =   43
               Width           =   1905
               _Version        =   65536
               _ExtentX        =   3360
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               bAutoSelect     =   0   'False
            End
            Begin VB.Label lblTitle 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Title"
               Height          =   195
               Left            =   90
               TabIndex        =   115
               Top             =   570
               Width           =   300
            End
            Begin VB.Label lblPhone 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Telephone"
               Height          =   195
               Left            =   90
               TabIndex        =   117
               Top             =   900
               Width           =   765
            End
            Begin VB.Label lblFax 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Fax"
               Height          =   195
               Left            =   90
               TabIndex        =   121
               Top             =   1230
               Width           =   255
            End
            Begin VB.Label lblContact 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Name"
               Height          =   195
               Left            =   90
               TabIndex        =   112
               Top             =   240
               Width           =   420
            End
            Begin VB.Label lblPhoneExt 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Ext."
               Height          =   195
               Left            =   3510
               TabIndex        =   119
               Top             =   900
               Width           =   270
            End
            Begin VB.Label lblFaxExt 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Ext."
               Height          =   195
               Left            =   3510
               TabIndex        =   123
               Top             =   1230
               Width           =   270
            End
         End
         Begin VB.Frame fraCredit 
            Caption         =   "Credi&t "
            Height          =   1905
            Left            =   0
            TabIndex        =   82
            Top             =   3150
            Width           =   3645
            Begin VB.CheckBox chkCreditLimitUsed 
               Alignment       =   1  'Right Justify
               Caption         =   "Apply Credit Limit"
               Height          =   300
               Left            =   120
               TabIndex        =   83
               Top             =   240
               WhatsThisHelpID =   86360
               Width           =   1755
            End
            Begin VB.ComboBox cboCredLimitAgeCat 
               Height          =   315
               Left            =   1680
               Style           =   2  'Dropdown List
               TabIndex        =   87
               Top             =   1020
               WhatsThisHelpID =   86359
               Width           =   1830
            End
            Begin VB.CheckBox chkOnHold 
               Alignment       =   1  'Right Justify
               Caption         =   "On Hold"
               Height          =   300
               Left            =   90
               TabIndex        =   88
               Top             =   1410
               WhatsThisHelpID =   86358
               Width           =   1785
            End
            Begin NEWSOTALib.SOTACurrency curCreditLimit 
               Height          =   285
               Left            =   1680
               TabIndex        =   85
               Top             =   600
               WhatsThisHelpID =   86357
               Width           =   1815
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curOrigCreditLimit 
               Height          =   285
               Left            =   7500
               TabIndex        =   233
               Top             =   0
               WhatsThisHelpID =   86356
               Width           =   1815
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblCreditLimit 
               AutoSize        =   -1  'True
               Caption         =   "Credit Limit"
               Height          =   195
               Left            =   120
               TabIndex        =   84
               Top             =   645
               Width           =   1450
            End
            Begin VB.Label lblCredLimitAgeCat 
               AutoSize        =   -1  'True
               Caption         =   "Aging Category"
               Height          =   195
               Left            =   120
               TabIndex        =   86
               Top             =   1050
               Width           =   1450
            End
         End
         Begin VB.Frame fraAddress 
            Caption         =   "P&rimary Address"
            Height          =   3100
            Left            =   3780
            TabIndex        =   89
            Top             =   0
            Width           =   5265
            Begin VB.TextBox txtLatitude 
               Height          =   285
               Left            =   1440
               MaxLength       =   25
               TabIndex        =   107
               Top             =   2385
               Width           =   1000
            End
            Begin VB.TextBox txtLongitude 
               Height          =   285
               Left            =   1440
               MaxLength       =   25
               TabIndex        =   109
               Top             =   2730
               Width           =   1000
            End
            Begin ButtonControls.MapButton cmdGetMap 
               Height          =   285
               Left            =   1080
               TabIndex        =   230
               TabStop         =   0   'False
               Top             =   255
               WhatsThisHelpID =   17778336
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
            End
            Begin VB.CommandButton cmdContAddr 
               Caption         =   "Other &Addresses"
               Height          =   285
               Index           =   1
               Left            =   3450
               TabIndex        =   110
               Top             =   2550
               WhatsThisHelpID =   86350
               Width           =   1665
            End
            Begin VB.ComboBox cboState 
               Height          =   315
               Left            =   4140
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   103
               Top             =   1710
               WhatsThisHelpID =   12
               Width           =   975
            End
            Begin VB.ComboBox cboCountry 
               Height          =   315
               Left            =   4140
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   105
               Top             =   2070
               WhatsThisHelpID =   33
               Width           =   975
            End
            Begin VB.TextBox txtCity 
               Height          =   285
               Left            =   1440
               MaxLength       =   20
               TabIndex        =   98
               Top             =   1710
               WhatsThisHelpID =   36
               Width           =   1905
            End
            Begin Threed.SSPanel pnlAddress 
               Height          =   1410
               Left            =   1440
               TabIndex        =   91
               Top             =   240
               Width           =   3660
               _Version        =   65536
               _ExtentX        =   6456
               _ExtentY        =   2487
               _StockProps     =   15
               Caption         =   "SSPanel1"
               ForeColor       =   0
               BackColor       =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               BevelWidth      =   2
               BorderWidth     =   0
               BevelOuter      =   1
               Begin NEWSOTALib.SOTAMaskedEdit txtAddressLine 
                  Height          =   285
                  Index           =   4
                  Left            =   30
                  TabIndex        =   95
                  Top             =   825
                  WhatsThisHelpID =   37
                  Width           =   3600
                  _Version        =   65536
                  _ExtentX        =   6350
                  _ExtentY        =   503
                  _StockProps     =   93
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  bAutoSelect     =   0   'False
                  sBorder         =   1
                  lMaxLength      =   40
               End
               Begin NEWSOTALib.SOTAMaskedEdit txtAddressLine 
                  Height          =   285
                  Index           =   3
                  Left            =   30
                  TabIndex        =   94
                  Top             =   555
                  WhatsThisHelpID =   37
                  Width           =   3600
                  _Version        =   65536
                  _ExtentX        =   6350
                  _ExtentY        =   503
                  _StockProps     =   93
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  bAutoSelect     =   0   'False
                  sBorder         =   1
                  lMaxLength      =   40
               End
               Begin NEWSOTALib.SOTAMaskedEdit txtAddressLine 
                  Height          =   285
                  Index           =   2
                  Left            =   30
                  TabIndex        =   93
                  Top             =   285
                  WhatsThisHelpID =   37
                  Width           =   3600
                  _Version        =   65536
                  _ExtentX        =   6350
                  _ExtentY        =   503
                  _StockProps     =   93
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  bAutoSelect     =   0   'False
                  sBorder         =   1
                  lMaxLength      =   40
               End
               Begin NEWSOTALib.SOTAMaskedEdit txtAddressLine 
                  Height          =   285
                  Index           =   1
                  Left            =   30
                  TabIndex        =   92
                  Top             =   30
                  WhatsThisHelpID =   37
                  Width           =   3600
                  _Version        =   65536
                  _ExtentX        =   6350
                  _ExtentY        =   503
                  _StockProps     =   93
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  bAutoSelect     =   0   'False
                  sBorder         =   1
                  lMaxLength      =   40
               End
               Begin NEWSOTALib.SOTAMaskedEdit txtAddressLine 
                  Height          =   285
                  Index           =   5
                  Left            =   30
                  TabIndex        =   96
                  Top             =   1095
                  WhatsThisHelpID =   37
                  Width           =   3595
                  _Version        =   65536
                  _ExtentX        =   6350
                  _ExtentY        =   503
                  _StockProps     =   93
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  bAutoSelect     =   0   'False
                  sBorder         =   1
                  lMaxLength      =   40
               End
            End
            Begin LookupViewControl.LookupView navZip 
               Height          =   285
               Left            =   2505
               TabIndex        =   101
               TabStop         =   0   'False
               Top             =   2040
               WhatsThisHelpID =   86340
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtPostalCode 
               Height          =   285
               Left            =   1440
               TabIndex        =   100
               Top             =   2040
               WhatsThisHelpID =   35
               Width           =   1035
               _Version        =   65536
               _ExtentX        =   1826
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               bAutoSelect     =   0   'False
            End
            Begin Threed.SSCommand cmdAvaAddr 
               Height          =   285
               Left            =   1080
               TabIndex        =   242
               TabStop         =   0   'False
               ToolTipText     =   "Address Validation"
               Top             =   550
               Visible         =   0   'False
               Width           =   285
               _Version        =   65536
               _ExtentX        =   508
               _ExtentY        =   508
               _StockProps     =   78
               Enabled         =   0   'False
            End
            Begin VB.Label lblLongitude 
               Caption         =   "Longitude"
               Height          =   195
               Left            =   120
               TabIndex        =   108
               Top             =   2730
               Width           =   750
            End
            Begin VB.Label lblLatitude 
               Caption         =   "Latitude"
               Height          =   195
               Left            =   120
               TabIndex        =   106
               Top             =   2400
               Width           =   855
            End
            Begin VB.Label lblAddress 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Street Addr"
               Height          =   195
               Left            =   120
               TabIndex        =   90
               Top             =   300
               Width           =   795
            End
            Begin VB.Label lblCity 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "City"
               Height          =   195
               Left            =   120
               TabIndex        =   97
               Top             =   1740
               Width           =   1200
            End
            Begin VB.Label lblState 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "State"
               Height          =   195
               Left            =   3540
               TabIndex        =   102
               Top             =   1770
               Width           =   540
            End
            Begin VB.Label lblZip 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Postal Code"
               Height          =   195
               Left            =   120
               TabIndex        =   99
               Top             =   2070
               Width           =   1200
            End
            Begin VB.Label lblCountry 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Country"
               Height          =   195
               Left            =   3540
               TabIndex        =   104
               Top             =   2115
               Width           =   540
            End
         End
         Begin VB.Frame fraCustomer 
            Caption         =   "C&ustomer"
            Height          =   3100
            Left            =   0
            TabIndex        =   62
            Top             =   0
            Width           =   3645
            Begin VB.TextBox txtSic 
               Height          =   285
               Left            =   1650
               MaxLength       =   7
               TabIndex        =   69
               Top             =   930
               WhatsThisHelpID =   86303
               Width           =   1500
            End
            Begin VB.TextBox txtVendor 
               Height          =   285
               Left            =   1650
               MaxLength       =   12
               TabIndex        =   72
               Top             =   1260
               WhatsThisHelpID =   86302
               Width           =   1500
            End
            Begin VB.TextBox txtCustReference 
               Height          =   285
               Left            =   1650
               MaxLength       =   15
               TabIndex        =   75
               Top             =   1590
               WhatsThisHelpID =   86301
               Width           =   1500
            End
            Begin VB.ComboBox cboStatus 
               Height          =   315
               Left            =   1650
               Style           =   2  'Dropdown List
               TabIndex        =   67
               Top             =   570
               WhatsThisHelpID =   86300
               Width           =   1500
            End
            Begin VB.TextBox txtCustClass 
               Height          =   285
               Left            =   1650
               MaxLength       =   12
               TabIndex        =   64
               Top             =   240
               WhatsThisHelpID =   86299
               Width           =   1500
            End
            Begin VB.TextBox txtABANo 
               Height          =   285
               Left            =   1650
               MaxLength       =   10
               TabIndex        =   77
               Top             =   1920
               WhatsThisHelpID =   86298
               Width           =   1500
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtCRMCustID 
               Height          =   285
               Left            =   1650
               TabIndex        =   81
               Top             =   2605
               WhatsThisHelpID =   86304
               Width           =   1515
               _Version        =   65536
               _ExtentX        =   2672
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin LookupViewControl.LookupView navSic 
               Height          =   285
               Left            =   3180
               TabIndex        =   70
               TabStop         =   0   'False
               Top             =   930
               WhatsThisHelpID =   86297
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navVendor 
               Height          =   285
               Left            =   3180
               TabIndex        =   73
               TabStop         =   0   'False
               Top             =   1260
               WhatsThisHelpID =   86296
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin LookupViewControl.LookupView navCustClass 
               Height          =   285
               Left            =   3180
               TabIndex        =   65
               TabStop         =   0   'False
               Top             =   240
               WhatsThisHelpID =   86295
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin SOTACalendarControl.SOTACalendar txtDateEstablished 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   1650
               TabIndex        =   79
               Top             =   2250
               WhatsThisHelpID =   86294
               Width           =   1500
               _ExtentX        =   2646
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin VB.Label lblCRMID 
               AutoSize        =   -1  'True
               Caption         =   "CRM Account"
               Height          =   195
               Left            =   120
               TabIndex        =   80
               Top             =   2635
               Width           =   1455
            End
            Begin VB.Label lblSic 
               AutoSize        =   -1  'True
               Caption         =   "Std Industry Code"
               Height          =   195
               Left            =   120
               TabIndex        =   68
               Top             =   960
               Width           =   1455
            End
            Begin VB.Label lblVendor 
               AutoSize        =   -1  'True
               Caption         =   "Vendor"
               Height          =   195
               Left            =   120
               TabIndex        =   71
               Top             =   1290
               Width           =   1455
            End
            Begin VB.Label lblCustReference 
               AutoSize        =   -1  'True
               Caption         =   "Customer Reference"
               Height          =   195
               Left            =   120
               TabIndex        =   74
               Top             =   1620
               Width           =   1455
            End
            Begin VB.Label lblStatus 
               AutoSize        =   -1  'True
               Caption         =   "Status"
               Height          =   195
               Left            =   120
               TabIndex        =   66
               Top             =   630
               Width           =   1455
            End
            Begin VB.Label lblCustClass 
               AutoSize        =   -1  'True
               Caption         =   "Customer Class"
               Height          =   195
               Left            =   120
               TabIndex        =   63
               Top             =   270
               Width           =   1455
            End
            Begin VB.Label lblDateEstablished 
               AutoSize        =   -1  'True
               Caption         =   "Date Established"
               Height          =   195
               Left            =   120
               TabIndex        =   78
               Top             =   2280
               Width           =   1455
            End
            Begin VB.Label lblABANo 
               AutoSize        =   -1  'True
               Caption         =   "Bank ABA Number"
               Height          =   195
               Left            =   120
               TabIndex        =   76
               Top             =   1950
               Width           =   1455
            End
         End
      End
   End
   Begin VB.TextBox txtSaveCustID 
      Height          =   285
      Left            =   4890
      MaxLength       =   12
      TabIndex        =   217
      Top             =   540
      Visible         =   0   'False
      WhatsThisHelpID =   86276
      Width           =   1545
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   224
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   225
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   226
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   227
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   218
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCustomerID 
      Height          =   285
      Left            =   840
      TabIndex        =   1
      Top             =   540
      WhatsThisHelpID =   86269
      Width           =   1365
      _Version        =   65536
      _ExtentX        =   2408
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bAutoSelect     =   0   'False
   End
   Begin LookupViewControl.LookupView navMain 
      Height          =   285
      Left            =   2250
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   540
      WhatsThisHelpID =   14
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6915
      WhatsThisHelpID =   73
      Width           =   9480
      _ExtentX        =   16722
      _ExtentY        =   688
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   4200
      TabIndex        =   231
      Top             =   3360
      Width           =   1215
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   228
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblCustomerID 
      AutoSize        =   -1  'True
      Caption         =   "&Customer"
      Height          =   195
      Left            =   90
      TabIndex        =   0
      Top             =   600
      Width           =   660
   End
   Begin VB.Label lblCustName 
      AutoSize        =   -1  'True
      Caption         =   "Name"
      Height          =   195
      Left            =   2910
      TabIndex        =   3
      Top             =   600
      Width           =   420
   End
End
Attribute VB_Name = "frmCustomerMaint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmCustomerMaint
'     Desc: Customer Maintenance Form
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 08/16/95 AEW
'     Mods: 07/97 KMKD - Project Accounting integration
'           08/14/01 VB  Added the National Account logic by adding an additionl tab
'               - The NationalAcct Lookup is not bound to the form since the acutal key stored in
'                 tarCustomer is in a tarNationalAcctLevel table and not in tarNationalAcct.
'                 we don't maintain NationalAcctKey in tarCustomer therefore we need to maintain
'                 this column manually and in case of an insert get the surrogate key
'                 in order to update tarCustomer.NationalAcctLevelKey in tarCustomer.
'                 Database was designed this way to support multiple National Acct levels
'                 in the future. We only support two levels right now in this design
'               - Assumption is that a National Acct Parent and a child row always exists
'                 in tarNationalAcctLevel table.
'************************************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If


'Private variables of Form Properties
    Private moClass             As Object   'Parent Class Object
    Private mlRunMode           As Long     'Mode form was brought Up in
    Private mbSaved             As Boolean  'Saved successful (AOF)
    Private mbUnload            As Boolean  'Unload form if error occurs
    Private mbCancelShutDown    As Boolean  'Cancel Shutdown Flag
    Private mbFormLoading       As Boolean
    Private mbContAddrClick     As Boolean
    Private miExtShipSystem     As Integer
    Private Const kNone = 0
    Private Const kStarShip = 1
    Private Const kManual = 2
    
'Public Form Variables
    Public moSotaObjects        As New Collection   'Collection of child objects
    Public miFilter             As Integer
    Public miSecurityLevel      As Integer

'Validation, Active Modules Class
    Public moValidate           As New clsValidate      'Validation Class
    Private moLostFocus         As New clsValidationManager  'Lost Focus Validation
    
    
'Map Controls to buttons on form
    Public moMapSrch            As New Collection   'Collection of controls mapped to navigators
    Public moMapMemo            As New Collection   'Collection of controls mapped to Memo
    Private mbIntegratewAP      As Boolean
    Private miIntegratewCRM     As Integer
    Private miSLActivated       As Integer
    Private miCCActivated       As Integer
    Private bLicense            As Boolean
    Private mbCRMConnected      As Boolean
    Private mbAPActive          As Boolean
    Private mbIMActive          As Boolean
    Private mbPOActive          As Boolean
    Private mbSOActive          As Boolean
    Private mbPAActive          As Boolean
    Private mlAckFormKey        As Long
    Private mlLblFormKey        As Long
    Private miAllowInvt         As Integer


'Add on the fly variables
    Private mbAllowUserFieldValueAOF  As Boolean    'User field Add On the Fly Allowed
    Private mbAllowGLAccountAOF       As Boolean    'G/L Account Add On the Fly Allowed
    Private mbAllowPmtTermsAOF        As Boolean    'Payment Terms Add On the Fly Allowed
    Private mbAllowCustAddrAOF        As Boolean    'Customer Address Add On the Fly Allowed
    Private mbAllowCurrencyAOF        As Boolean    'Currency Add On the Fly Allowed
    Private mbAllowItemAOF            As Boolean    'Item Add On the Fly Allowed
    Private mbAllowCurrExchSchdAOF    As Boolean    'Currency exchange Schedule Add On the Fly Allowed
    Private mbAllowCustClassAOF       As Boolean    'Customer Class Add On the Fly Allowed
    Private mbAllowBusFormAOF         As Boolean    'Business Form Add On the Fly Allowed
    Private mbAllowStateAOF           As Boolean    'State Code Add On the Fly Allowed
    Private mbAllowPostalAOF          As Boolean    'Postal Code Add On the Fly Allowed
    Private mbAllowCountryAOF         As Boolean    'Country Code Add On the fly allowed
    Private mbAllowVendorAOF          As Boolean    'Vendor Add On the fly allowed
    Private mbAllowSalesPersonAOF     As Boolean    'Salesperson Add On the Fly Allowed
    Private mbAllowSalesterritoryAOF  As Boolean    'Sales Territory Add On the Fly Allowed
    Private mbAllowCommPlanAOF        As Boolean    'Commission Plan Add On the Fly Allowed
    Private mbAllowProcCycleAOF       As Boolean    'Process Cycle Add On the Fly Allowed
    Public mbAllowSTaxSchdAOF         As Boolean    'Sales Tax Schedule Add On the Fly Allowed
    Private mbAllowShipMethAOF        As Boolean    'Shipping Method Add On the Fly Allowed
    Private mbAllowShipZoneAOF        As Boolean    'Shipping Zone Add On the Fly Allowed
    Private mbAllowContactAOF         As Boolean    'Contact Add On the Fly Allowed
    Private mbAllowSicAOF             As Boolean    'Standard Industry Code Add On the Fly Allowed
    Private mbAllowLanguageAOF        As Boolean    'Language Code Add On the Fly Allowed
    Private mbAllowFOBAOF             As Boolean    'FOB Add On the Fly Allowed--v3
    
 'Object Variables user fields & Context menus
    Public moUserFlds           As New clsUserFields    'User Fields Class
    Public moContextMenu        As New clsContextMenu   'Right Click Context Menu Class
    
'Binding Object Variables
    Public moDmForm             As New clsDmForm        'Customer table data manager object
    Public moDmFormArAddr       As New clsDmForm        'AR Address table data manager object
    Public moDmFormCiAddr       As New clsDmForm        'Generic Address table data manager object
    Public moDmContact          As New clsDmForm        'Contacts table data manager object
    Public moDmSTax             As New clsDmGrid        'Sales Tax exemption data manager object
    Public WithEvents m_CopyMgr As clsCopyMgr
Attribute m_CopyMgr.VB_VarHelpID = -1
        
'Miscellaneous Variables
    Public mbEnterAsTab         As Boolean              'tab Key pressed when Enter Key pressed
    Public mbDontChkClick       As Boolean              'Dont run Click events
    Private msCompanyID         As String               'Company ID
    Private mbRowLoading        As Boolean              'Row is currently loading
    Private moLastControl       As Control
    Private msCustStatus        As String               'Hold customer status
    Private miCustKey           As Long                 'deleted cust key
    Private msCreditMgrEmail    As String
    Private msCreditMgrMailBox  As String
    Private mbSkipSecurityEventCheck As Boolean
    Private mbSetupSTax         As Boolean
    Private mbReloadSTaxExmpt   As Boolean
    Private miAllowUpcharge     As Integer
 
    ' Copy feature storage variables
    Private m_SourceKey             As Long
    Private m_SourceAddrKey         As Long
    Private m_SourceCntcKey         As Long
    Private m_AddressQuestion       As Boolean
 
'Customer defaults from ar options and security events
    Private muCustDflts          As CustOptions         'Customer Options Structure
 
 
'Company Defaults
    Private msDfltCurrency      As String               'Default Currency for company
    Private msDfltCountry       As String               'Default Country for company
    Private mlDfltLanguage      As Long                 'Default Language for company

    
'Default Country Masks
    Private msDfltPhoneMask     As String               'Phone mask for default country
    Private msDfltZipMask       As String               'Postal code mask for default country
    Private msCompanyWhere      As String               'Company Code where clause

    Private mbAutoNum         As Boolean
'Currency Rounding variables
    Private miRoundMeth       As Integer   'Rounding Method (Standard, Round Up, Round Down)
    Private miDecPlaces       As Integer   'Number of Decimal Places
    Private miPrecision       As Integer   'Rounding Precision(Round to the nearest)


'RESIZE FORM Variables
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    
    
'Default Static List List Index Variables
    Private miDfltBillType      As Integer              'Billing Type
    Private miDfltStatus        As Integer              'Status Code
    Private miDfltAgeCat        As Integer              'Credit Limit Aging Categeory
    
'National Account variables
    Private mbUseNationalAcct   As Boolean              'AR option to Enable/Disable National Acct Tab
    Private mlNatAcctKey        As Long                 'Unbound control used to determine when to save
    Private miNatUserLevel      As Integer              'Customer National Account Level
    
    ' *** RMM 5/2/01, Context menu project
    Private mbIMActivated As Boolean
    Private mbStoreCreditCardInfo As Boolean
    Private mbDMStateChanged          As Boolean     'DMStateChange Flag

'Tab Constants
    Const kTabMain              As Integer = 0          'Main Tab
    Const kTabDflt              As Integer = 1          'Defaults Tab
    Const kTabBTST              As Integer = 2          'Bill To Ship To Tab
    Const kTabSO                As Integer = 3          'Sales Order Tab
    Const kTabNatAcct           As Integer = 4          'National Account Tab
    Const kTabCustomizer        As Integer = 5          'Customizer tab
    Const kMin                  As Integer = 0          'min Ship Days
    Const kMax                  As Integer = 32767      'max Ship days

'National Acct defaults
    Const kiNatNone             As Integer = 0           'Not a National accoun Member
    Const kiNatParent           As Integer = 1           'Default to National acct Parent BillTo
    Const kiNatSub              As Integer = 2           'Default to National acct child BillTo
    
'Sales Tax Exemption Grid Constants
    Const kColAddrKey           As Integer = 1          'Address key Column
    Const kColExmpt             As Integer = 2          'Exemption Column
    Const kColTaxCode           As Integer = 3          'Tax Code Key Column
    
    Const kvOpen                As Integer = 1          'Customer Status Open
    Const ksNatParent           As String = "Parent"
    Const ksNatSub              As String = "Subsidiary"
    
    Const kvGLAcctCatIDNonFinancial = 9                 'To restrict GL Accounts to financial only


    
'Const kmsgARNatAcctNoSelected = 153611


'tciUserField | DataType
    Const kvValidated = 1
    Const kvAlphanumeric = 4
    Const kvNumeric = 3
    Const kvDate = 2

'API Declaration for SHELL32
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

' AvaTax integration - General Declarations
    Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
    Public mbAvaAddrEnabled         As Boolean ' Defines if the Avalara Address Validation option is enabled for this company
' AvaTax end

Const VBRIG_MODULE_ID_STRING = "ARZMA001.FRM"
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = "ARZ"
End Property

Private Sub GetDefaults()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'******************************************************************************
' Get Defaults and Load/Disable/Etc Controls
'******************************************************************************
On Error GoTo ExpectedErrorRoutine2

    Dim sCurrSymbol     As String
    Dim lCurrLocale     As Long
    Dim iRetval         As Integer
    Dim sGLAcctMask     As String
    Dim uCurrInfo       As CurrencyInfo
    Dim lLocale         As Long

  'Run Stored procedures
    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB
        .SetInParam msCompanyID
        .SetInParam msDfltCurrency
        .SetInParam mlDfltLanguage
        .SetOutParam CLng(Val(muCustDflts.CustClassKey))
        .SetOutParam CInt(muCustDflts.UseMultiCurr)
        .SetOutParam muCustDflts.BillingTypeOpt
        .SetOutParam CInt(muCustDflts.TrackSalesTax)
        .SetOutParam CInt(muCustDflts.PrintInvoices)
        .SetOutParam CInt(muCustDflts.AutoNum)
        .SetOutParam CInt(muCustDflts.PrintStmts)
        .SetOutParam CInt(muCustDflts.UseSper)
        .SetOutParam CInt(muCustDflts.CheckCreditLimit)
        .SetOutParam CInt(Val(muCustDflts.AgeCat(1)))
        .SetOutParam CInt(Val(muCustDflts.AgeCat(2)))
        .SetOutParam CInt(Val(muCustDflts.AgeCat(3)))
        .SetOutParam CInt(Val(muCustDflts.AgeCat(4)))
        .SetOutParam muCustDflts.AgeUnit
        .SetOutParam sCurrSymbol
        .SetOutParam miDecPlaces
        .SetOutParam miPrecision
        .SetOutParam miRoundMeth
        .SetOutParam msDfltCountry
        .SetOutParam CInt(mbIntegratewAP)
        .SetOutParam sGLAcctMask
        .SetOutParam CInt(mbAPActive)
        .SetOutParam CInt(mbIMActive)
        .SetOutParam CInt(mbPOActive)
        .SetOutParam CInt(mbSOActive)
        .SetOutParam CInt(mbPAActive)
        .SetOutParam miIntegratewCRM 'CInt(mbIntegratewCRM)
        .SetOutParam msCreditMgrEmail
        .SetOutParam msCreditMgrMailBox
        .SetOutParam CInt(mbUseNationalAcct)
        .SetOutParam iRetval
        .ExecuteSP "sparGetCustomerOptions"
        muCustDflts.CustClassKey = gvCheckNull(.GetOutParam(4), SQL_INTEGER)
        muCustDflts.UseMultiCurr = CBool(gvCheckNull(.GetOutParam(5), SQL_INTEGER))
        muCustDflts.BillingTypeOpt = gvCheckNull(.GetOutParam(6), SQL_INTEGER)
        muCustDflts.TrackSalesTax = CBool(gvCheckNull(.GetOutParam(7), SQL_INTEGER))
        muCustDflts.PrintInvoices = CBool(gvCheckNull(.GetOutParam(8), SQL_INTEGER))
        muCustDflts.AutoNum = CBool(gvCheckNull(.GetOutParam(9), SQL_INTEGER))
        muCustDflts.PrintStmts = CBool(gvCheckNull(.GetOutParam(10), SQL_INTEGER))
        muCustDflts.UseSper = CBool(gvCheckNull(.GetOutParam(11), SQL_INTEGER))
        muCustDflts.CheckCreditLimit = CBool(gvCheckNull(.GetOutParam(12), SQL_INTEGER))
        muCustDflts.AgeCat(1) = gvCheckNull(.GetOutParam(13))
        muCustDflts.AgeCat(2) = gvCheckNull(.GetOutParam(14))
        muCustDflts.AgeCat(3) = gvCheckNull(.GetOutParam(15))
        muCustDflts.AgeCat(4) = gvCheckNull(.GetOutParam(16))
        muCustDflts.AgeUnit = gvCheckNull(.GetOutParam(17))
        sCurrSymbol = gvCheckNull(.GetOutParam(18))
        miDecPlaces = gvCheckNull(.GetOutParam(19), SQL_INTEGER)
        miPrecision = gvCheckNull(.GetOutParam(20), SQL_INTEGER)
        miRoundMeth = gvCheckNull(.GetOutParam(21), SQL_INTEGER)
        msDfltCountry = gvCheckNull(.GetOutParam(22))
        mbIntegratewAP = CBool(gvCheckNull(.GetOutParam(23), SQL_INTEGER))
        sGLAcctMask = gvCheckNull(.GetOutParam(24))
        mbAPActive = CBool(gvCheckNull(.GetOutParam(25), SQL_INTEGER))
        mbIMActive = CBool(gvCheckNull(.GetOutParam(26), SQL_INTEGER))
        mbPOActive = CBool(gvCheckNull(.GetOutParam(27), SQL_INTEGER))
        mbSOActive = CBool(gvCheckNull(.GetOutParam(28), SQL_INTEGER))
        mbPAActive = CBool(gvCheckNull(.GetOutParam(29), SQL_INTEGER))
        'mbIntegratewCRM = CBool(gvCheckNull(.GetOutParam(31), SQL_INTEGER))
        miIntegratewCRM = .GetOutParam(30)
        msCreditMgrEmail = gsGetValidStr(.GetOutParam(31))
        msCreditMgrMailBox = gsGetValidStr(.GetOutParam(32))
        mbUseNationalAcct = CBool(gvCheckNull(.GetOutParam(33), SQL_INTEGER))
        iRetval = gvCheckNull(.GetOutParam(34), SQL_INTEGER)
        .ReleaseParams
    End With
    On Error GoTo ExpectedErrorRoutine2

    lLocale = moClass.moSysSession.CountryLocale
    
    numFinChgPct.Locale = lLocale
    numTradeDiscPct.Locale = lLocale
    
    numFinChgPct.DecimalPlaces = 2
    numTradeDiscPct.DecimalPlaces = 2

   'No record found
    If iRetval = 1000 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgNeedAROptionRec  'Send user a message
        mbUnload = True                                             'Set Unload Flag
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    ElseIf iRetval = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
            "spARGetCustomerOptions: " & Format(iRetval)
        mbUnload = True                                             'Set Unload Flag
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    Else
        'Set mask on customer ID according to whether Sage MAS 500 Enterprise Projects is activated for any company.
        If Len(txtCustomerID.Mask) = 0 Then
'--01/19/01, remove PA components
'            If moClass.moAppDB.Lookup("Count(*) RecCount", "tsmCompanyModule", "ModuleNo = " & kModulePA & " AND Active = 1") > 0 Then
'                txtCustomerID.Mask = "XXXXXXXX"
'            Else
                txtCustomerID.Mask = "XXXXXXXXXXXX"
'            End If
        End If
        
      'If the customer option to track sales tax is false disable sales tax combo
    cmdSalesTax.Enabled = muCustDflts.TrackSalesTax
    cmdExemptions.Enabled = muCustDflts.TrackSalesTax

      'Integrate with AP off disable vendor textbox and navigator
        If mbIntegratewAP = False Or mbAPActive = False Then
            txtVendor.Enabled = False       'Vendor Number Text Box
            txtVendor.BackColor = vbButtonFace
            navVendor.Enabled = False       'Vendor Number navigator
        End If
        
        
      'If the customer options to print invoices is false disable print invoices controls
        If muCustDflts.PrintInvoices = False Then
            txtInvoiceForm.Enabled = False      'Disable Invoice Form textbox
            txtInvoiceForm.BackColor = vbButtonFace
            navInvoiceForm.Enabled = False      'Disable Invoice Form navigator
        End If
        
      
      'Enable next Number button if auto Number customers is true
        If muCustDflts.AutoNum Then
            If mlRunMode <> kContextAOF Then
              'No Auto numbering disable next New button
                mbAutoNum = True
            End If
        End If
        
      'If sure the customer option to print statements is true
        If muCustDflts.PrintStmts = False Then
            txtStmtForm.Enabled = False         'Disable Statement form textbox
            txtStmtForm.BackColor = vbButtonFace
            navStmtForm.Enabled = False         'disable statement form navigator
            cboStmtCycle.Enabled = False
            cboStmtCycle.BackColor = vbButtonFace
        End If
                
        'Make sure the customer option to use salespeople is true
        If muCustDflts.UseSper = False Then
            txtSalesperson.Enabled = False      'Disable salesperson text box
            txtSalesperson.BackColor = vbButtonFace
            navSalesperson.Enabled = False      'Disable salesperson navigator
            cboCommPlan.Enabled = False         'Disable commission Plan Combo Box
            cboCommPlan.BackColor = vbButtonFace
        End If
        
        
      'if require credit limit checking then disable aging category
        If muCustDflts.CheckCreditLimit = False Then
            chkCreditLimitUsed.Enabled = False
            curCreditLimit.Enabled = False
            cboCredLimitAgeCat.Enabled = False      'Disable Credit Limit Aging category
            cboCredLimitAgeCat.BackColor = vbButtonFace
        Else
            chkCreditLimitUsed.Enabled = True
            curCreditLimit.Enabled = True
        End If
        SetupAgeCatCombo
    End If
       
  'Not using MultiCurrency hide multiCurrency fields
    If Not muCustDflts.UseMultiCurr Then
        fraMultiCurrency.Visible = False
    End If
  
  'Setup G/L Account Mask
    'txtGLAccount.Mask = sGLAcctMask
    glaAccount.MaskedText = sGLAcctMask
    glaReturns.MaskedText = sGLAcctMask
  'Setup Currency Controls
    gbSetCurrCtls moClass, msDfltCurrency, uCurrInfo, curCreditLimit, curLateChgAmt

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
        "spARGetCustomerOptions: " & Error$(Err)
    mbUnload = True                                             'Set Unload Flag
    gClearSotaErr
    Exit Sub

ExpectedErrorRoutine2:
    MyErrMsg moClass, Err.Description, Err, sMyName, "GetDefaults"
    mbUnload = True                                             'Set Unload Flag
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = "ARZ"
End Property
Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
    Set oClass = moClass
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property
'************************************************************************
'   lRunMode contains the user-defined run mode context in which the
'   class object was initialized. The run mode is extracted from the
'   context passed in through InitializeObject.
'************************************************************************
Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
    lRunMode = mlRunMode
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property
Public Property Get bSaved() As Boolean
'+++ VB/Rig Skip +++
    bSaved = mbSaved
End Property
Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Skip +++
    mbSaved = bNewSaved
End Property
'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Skip +++
    bCancelShutDown = mbCancelShutDown
End Property
'************************************************************************
'   bUnload unloads the form if an error occurs in form load
'************************************************************************
Public Property Get bUnload() As Boolean
'+++ VB/Rig Skip +++
    bUnload = mbUnload
End Property
Public Property Let bUnload(bUnload As Boolean)
'+++ VB/Rig Skip +++
    mbUnload = bUnload
End Property
Public Function DMAfterInsert(oDm As Object)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************
' Desc:     data Manager after Insert runs inside the transaction after
'           A Row has been inserted into the database
'           creates a Customer Status record
' Parms:    oDm - data manager object passed in
' Returns:  True if successful, False if Not successful
'************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim sSQL As String      'SQL String
    
  'Insert a record into the customer Status table
    If oDm.Table = moDmForm.Table Then
      'Create and have das build SQL String needed
        sSQL = "#arInsertCustStatus;" & moDmForm.GetColumnValue("CustKey") & ";"
        sSQL = moClass.moAppDB.BuildSQLString(sSQL)
        
        #If DMDEBUG Then
            Debug.Print sSQL
        #End If
        
      'Run SQL to insert Customer Status record
        moClass.moAppDB.ExecuteSQL sSQL
        
            
        ' Determine if Addresses and Contacts need to be copied from
        ' source vendor to current/new vendor if copy dialog was accessed
        If (m_AddressQuestion) Then
            DMAfterInsert = CopyOtherData()
        End If
        
        ' Init storage variables
        m_SourceKey = moDmForm.GetColumnValue("CustKey")
        m_SourceAddrKey = moDmForm.GetColumnValue("PrimaryAddrKey")
        m_SourceCntcKey = moDmForm.GetColumnValue("PrimaryCntctKey")
        m_AddressQuestion = False

'        '******************************************************************************
'        '******************************************************************************
'        'SGS DEJ 6/14/2010 Insert new customers into SLX    (START)
'        'RkL DEJ 2017-02-15 Moved this to the DMPostSave method so a customer can be sent
'        'after a init insert and on a save.
'        '******************************************************************************
'        '******************************************************************************
'        Dim CustKey As Long
'        Dim CustID As String
'        Dim CustName As String
'
'        Dim AddrKey As Long
'        Dim AddrName As String
'        Dim AddrLine1 As String
'        Dim AddrLine2 As String
'        Dim AddrLine3 As String
'        Dim AddrLine4 As String
'        Dim City As String
'        Dim State As String
'        Dim Zip As String
'        Dim Country As String
'
'        CustKey = glGetValidLong(moDmForm.GetColumnValue("CustKey"))
'        CustID = Trim(txtCustomerID.Text)
'        CustName = Trim(txtCustName.Text)
'
'        AddrKey = glGetValidLong(moDmFormCiAddr.GetColumnValue("AddrKey"))
'
'        AddrName = Trim(txtCustName.Text)
'        AddrLine1 = Trim(txtAddressLine(1).Text)
'        AddrLine2 = Trim(txtAddressLine(2).Text)
'        AddrLine3 = Trim(txtAddressLine(3).Text)
'        AddrLine4 = Trim(txtAddressLine(4).Text)
'        City = Trim(txtCity.Text)
'        State = Trim(cboState.Text)
'        Zip = Trim(txtPostalCode.Text)
'        Country = Trim(cboCountry.Text)
'
'        ProcessSLXData oClass, msCompanyID, CustKey, CustID, CustName, AddrKey, AddrName, AddrLine1, AddrLine2, AddrLine3, AddrLine4, City, State, Zip, Country
'
'        '******************************************************************************
'        '******************************************************************************
'        'SGS DEJ 6/14/2010 Insert new customers into SLX    (STOP)
'        '******************************************************************************
'        '******************************************************************************
    
    End If

    DMAfterInsert = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
   MyErrMsg moClass, Err.Description, Err, sMyName, "DMAfterInsert"
   oDm.CancelAction
   gClearSotaErr

Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMAfterInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Sub AreModulesActive()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc: Controls are hidden or Disabled based on Active modules
'************************************************************************
  
    Dim iCRMConnected   As Integer
    
  'If Accounts Payable Not Active Disable the Vendor
    If Not mbAPActive Then
        txtVendor.Enabled = False       'Vendor Number Text Box
        txtVendor.BackColor = vbButtonFace
        navVendor.Enabled = False       'Vendor Number navigator
    End If
     
  'If Sales Order not Active disable sales order related Fields
    If Not mbSOActive Then
        chkShipComplete.Enabled = False         'Ship Complete
        chkCloseSOOnFirst.Enabled = False       'CloseSOOnFirstShipment
        chkCloseSOLineOnFirst.Enabled = False   'CloseSOLineOnFirstShipment
        lkuSource.Enabled = False               'Sales Source
        ddnShipPriority.Enabled = False         'Ship Priority
        numShipDays.Enabled = False             'Ship Days
        chkPrintOrdAck.Enabled = False          'Print Order Acknowledgements
        lkuAckForm.Enabled = False              'Acknowledgement Form
        lkuLabelForm.Enabled = False            'Label Form
        lkuPackListForm.Enabled = False         'Packing List Form
        lkuClosestWhse.Enabled = False          'Closest Warehouse
        chkInvtSubst.Enabled = False            'Allow Inventory Substitutions
        chkReqAck.Enabled = False               'Require Acknowledgement
        tabC.TabVisible(3) = False              'Sales Order tab
        ddnFreightMethod.Enabled = False
        chkBOLReqd.Enabled = False
        chkShipLabelsReqd.Enabled = False
        chkPackListReqd.Enabled = False
        chkPackListContentsReqd.Enabled = False
        chkInvoiceReqd.Enabled = False
        fraReqShipDoc.Enabled = False
        
        If Not (mbPOActive And 1 = 2) Then      'Do not enable ship zones
            cboShipZone.Enabled = False         'Ship Zone Combo Box
            cboShipZone.BackColor = vbButtonFace
        End If
    Else
        If Not mbIMActive Then
            lblClosestWhse.Visible = False
            lkuClosestWhse.Visible = False
            lkuClosestWhse.Enabled = False
            chkInvtSubst.Visible = False
            chkInvtSubst.Enabled = False
        End If
    End If
            
    If Not mbIMActive Then
        lkuPriceGroup.Enabled = False
        ddnPriceBase.Enabled = False
        nbrPriceAdjPct.Enabled = False
    End If
    
  'Check if SalesLogix is activated
    miSLActivated = giGetValidInt(moClass.moSysSession.IsModuleActivated(kModuleSL))
        
  'Check for Liscense agreement before enableing the CRM fields
    bLicense = moClass.moSysSession.IsModuleLicensed(kModuleSL)
    
  'If integrate with CRM is off or SL module is not active or SL is not licensed then hide the CRMCustID
    If miIntegratewCRM = 0 Or (Not miSLActivated = 1) Or (Not bLicense) Then
       txtCRMCustID.Visible = False          'CRMCustID Text Box
       lblCRMID.Visible = False              'CRMcustID Label
    Else    'test the connection to CRM db
        mbCRMConnected = True
        On Error Resume Next
        iCRMConnected = giGetValidInt(moClass.moAppDB.Lookup("1", "USERSECURITY", ""))
        If Err <> 0 Then
            mbCRMConnected = False
            txtCRMCustID.Text = gsBuildString(kARDBNotAvailable, moClass.moAppDB, moClass.moSysSession)
            txtCRMCustID.Enabled = False
        End If
    End If
    
    On Error GoTo VBRigErrorRoutine
    
   
    
    'Check if Credit Card is activated
    miCCActivated = giGetValidInt(moClass.moSysSession.IsModuleActivated(kModuleCC))
    
    If miCCActivated Then
         If moClass.moAppDB.Lookup("StoreCCInfo", "tccOptions", "CompanyID = " & gsQuoted(msCompanyID)) = 1 Then
             mbStoreCreditCardInfo = True
         Else
             mbStoreCreditCardInfo = False
         End If
    End If
    
    If miCCActivated <> 1 Or mbStoreCreditCardInfo = False Then
        lkuDefaultCreditCard.Enabled = False
        lblDefaultCreditCard.Enabled = False
        cmdCreditCards.Enabled = False
    End If
    
       
    If miCCActivated = 1 Then
        miAllowUpcharge = giGetValidInt(moClass.moAppDB.Lookup("AllowUpCharge", "tccOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    End If
    
    If miCCActivated = 1 Then
        fraCCUpcharges.Visible = True
        
        If miAllowUpcharge = 1 Then
            ddnCCUpchargeType.Enabled = True
            curDfltMaxUpcharge.Enabled = True
        Else
            ddnCCUpchargeType.Enabled = False
            curDfltMaxUpcharge.Enabled = False
        End If
    Else
        fraCCUpcharges.Visible = False
    End If
    
'v3  'If Inventory Management Not Active disable the Price List Combo Box
'    If Not mbIMActive Then
'        cboPriceList.Enabled = False        'Price List Combo Box
'        cboPriceList.BackColor = GetSysColor(COLOR_BTNFACE)
'    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "AreModulesActive", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************
' Desc:     Cleans Up child forms and child/AOF Objects
'************************************************************************
On Error Resume Next
   
    gUnloadChildForms Me
    

    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me

    ' AvaTax Integration - PerformCleanShutDown
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    ' AvaTax end

    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc:  Bind RightClick Context Menus to the form/Controls
'************************************************************************

    With moContextMenu
        .Bind "GLDA01", glaAccount.hwnd, kEntTypeGLAccount
        .Bind "GLDA01", glaReturns.hwnd, kEntTypeGLAccount
        .Bind "ARDA04", txtCustomerID.hwnd, kEntTypeARCustomer
        .Bind "ARDA04", txtNatParentCustID.hwnd, kEntTypeARCustomer
        .Bind "ARDA02", txtCustClass.hwnd, kEntTypeARCustClass
        .Bind "ARDA03", txtSalesperson.hwnd, kEntTypeARSalesperson
        .Bind "APDA03", txtVendor.hwnd, kEntTypeAPVendor
        .Bind "IMDA06", txtDfltItem.hwnd, kEntTypeIMItem
        .Bind "IMDA08", lkuClosestWhse.hwnd, kEntTypeIMWarehouse
        Set .Form = frmCustomerMaint
        .Init                      'Init will set properties of Winhook control
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindCopyMgr()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc:  Bind the Copy Manager to the appropriate data manager
'        and establish questions, if any
'************************************************************************

    Set m_CopyMgr = New clsCopyMgr
    
    With m_CopyMgr
        ' Add question(s)
        .AddQuestion "Copy Addresses and Contacts", vbUnchecked, True
        If (mbUseNationalAcct) Then
            .AddQuestion "Include in National Account", vbUnchecked, True
        End If
        ' Init the Copy Manager
        .Init moDmForm, "Copy Customer", "New Customer", mbAutoNum, True
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindCopyMgr", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub GetCustomerClassDefaults()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc:  Load customer Class defaults into the textboxes
' Parms: rs SOTADAS recordset object
'************************************************************************
    Dim rs      As New DASRecordSet       'SOTADAS recordset Object
    Dim sSQL    As String       'SQL String
    Dim bOldmb  As Boolean
    Dim sCurrID As String
    Dim lAcctKey As Long
    Dim bAcctKey As Boolean     'to check for dflt Sales Acct key
   
    
    SetHourglass True               'Run Hourglass
    bOldmb = mbDontChkClick
    mbDontChkClick = True           'Dont Run Click event for combo/check boxes
    
  
  'Get all fields from customer class table
    sSQL = "#arGetCustClass;"
    sSQL = sSQL & gsQuoted(msCompanyID) & ";"
    sSQL = sSQL & moDmForm.GetColumnValue("CustClassKey") & ";"
    
    #If DMDEBUG Then
        Debug.Print sSQL
    #End If
    
   'Run query to get defaults
    Set rs = moDmForm.Database.OpenRecordset(sSQL, kSnapshot, kOptionNone)
       
    
  'There is customer class information
    If Not rs.IsEOF Then
        'mlAckFormKey = gvCheckNull(rs.Field("SOAckFormKey"), SQL_SMALLINT, True)
        'mlLblFormKey = gvCheckNull(rs.Field("ShipLabelFormKey"), SQL_SMALLINT, True)
        'miAllowInvt = rs.Field("AllowInvtSubst")
       'Load Customer Class (From Default)
        
        txtCustClass.Text = gvCheckNull(rs.Field("CustClassID"))
      
      'Load Billing Type Static List
        cboBillingType.ListIndex = giListIndexFromItemData(cboBillingType, _
                gvCheckNull(rs.Field("BillingType")))
        
      'Load Dynamic Lists (Payment Terms, Statement cycle, Language)
        cboPmtTerms.ListIndex = giListIndexFromItemData(cboPmtTerms, _
                gvCheckNull(rs.Field("PmtTermsKey"), SQL_INTEGER))
        cboLanguage.ListIndex = giListIndexFromItemData(cboLanguage, _
                rs.Field("LanguageID"))
            
'v3      'Load Bound textBoxes (FOB and Invoice Message)
        txtInvoiceMessage.Text = gvCheckNull(rs.Field("InvcMsg"))
        
      'Load CheckBoxes (Print Dunning Message, Allow Refunds, Allow Write Offs)
        chkDunningMsg.Value = gvCheckNull(rs.Field("PrintDunnMsg"), SQL_SMALLINT)
        chkAllowrefunds.Value = gvCheckNull(rs.Field("AllowCustRefund"), SQL_SMALLINT)
        chkAllowWriteOffs.Value = gvCheckNull(rs.Field("AllowWriteOff"), SQL_SMALLINT)
        chkPOReq.Value = gvCheckNull(rs.Field("ReqPO"), SQL_SMALLINT) 'v3
        
      'Sales Order is Active Load Sales Order relate checkboxes
      ' (Ship Complete, Allow Inventory Substitutions)
         If mbSOActive Then
                chkShipComplete.Value = gvCheckNull(rs.Field("ShipComplete"), SQL_SMALLINT)
                lkuAckForm.KeyValue = gvCheckNull(rs.Field("SOAckFormKey"), SQL_INTEGER, True)
                lkuLabelForm.KeyValue = gvCheckNull(rs.Field("ShipLabelFormKey"), SQL_INTEGER, True)
                chkInvtSubst.Value = rs.Field("AllowInvtSubst")
                chkReqAck.Value = rs.Field("RequireSOAck")
                                             
         End If
        

        
      'Require Credit Limit Checking is on (load Credit Limit Anyway,
      '   Load Aging category Combo only if on)
      
        curCreditLimit = gvCheckNull(rs.Field("CreditLimit"), SQL_FLOAT)
        
        If muCustDflts.CheckCreditLimit Then
            If chkCreditLimitUsed.Value = vbChecked Then
                gEnableControls curCreditLimit
            Else
                gDisableControls curCreditLimit
            End If
            cboCredLimitAgeCat.ListIndex = giListIndexFromItemData(cboCredLimitAgeCat, _
                        gvCheckNull(rs.Field("CreditLimitAgeCat"), SQL_SMALLINT))
        End If
        
      'Load Sage MAS 500 Currency Control (Finance Charge Flat Amount)
        curLateChgAmt.Amount = gvCheckNull(rs.Field("FinChgFlatAmt"), SQL_FLOAT)
        
      'Load Columns then Sage MAS 500 Number Controls * 100
      ' For Percent Fields (Trade Discount, Finance Charge, Retention)
        moDmForm.SetColumnValue "TradeDiscPct", gvCheckNull(rs.Field("TradeDiscPct"), SQL_FLOAT)
        moDmForm.SetColumnValue "FinChgPct", gvCheckNull(rs.Field("FinChgPct"), SQL_FLOAT)
        numTradeDiscPct.Value = CDbl(moDmForm.GetColumnValue("TradeDiscPct")) * 100
        numFinChgPct.Value = CDbl(moDmForm.GetColumnValue("FinChgPct")) * 100
        
      'Load Surrogate Key fields (SHIP METHOD)
        moDmFormArAddr.SetColumnValue "ShipMethKey", gvCheckNull(rs.Field("ShipMethKey"), _
                            SQL_INTEGER, True)
          
      'Load Surrogate Key fields (FOB)--v3
        moDmFormArAddr.SetColumnValue "FOBKey", gvCheckNull(rs.Field("FOBKey"), _
                            SQL_INTEGER, True)
          
      'Load Default Sales Account Key field
        'moDmForm.SetColumnValue "DfltSalesAcctKey", _
                    gvCheckNull(rs.Field("DfltSalesAcctKey"), SQL_INTEGER, True)
        If gvCheckNull(rs.Field("DfltSalesAcctKey"), SQL_INTEGER, True) = rs.Field("DfltSalesAcctKey") Then
            lAcctKey = rs.Field("DfltSalesAcctKey")
            bAcctKey = True
        Else
            bAcctKey = False
        End If
             
      'Print Invoices Option is On in AR Options load Invoice Form Surrogate Key
        If muCustDflts.PrintInvoices Then
            moDmFormArAddr.SetColumnValue "InvcFormKey", gvCheckNull(rs.Field("InvcFormKey"), _
                            SQL_INTEGER, True)
        End If
        
      'Print Statements Option is On in AR Options load Statement Form Surrogate Key
        If muCustDflts.PrintStmts Then
            moDmForm.SetColumnValue "StmtFormKey", gvCheckNull(rs.Field("StmtFormKey"), _
                            SQL_INTEGER, True)
            cboStmtCycle.ListIndex = giListIndexFromItemData(cboStmtCycle, _
                    gvCheckNull(rs.Field("StmtCycleKey"), SQL_INTEGER))
        End If
        
'v3      'Inventory Management is Active Load Price List Combo Box
'        If mbIMActive Then
'            cboPriceList.ListIndex = giListIndexFromItemData(cboPriceList, _
'                    gvCheckNull(rs.Field("PriceListKey"), SQL_INTEGER))
'        End If
        
      'Use salespersons and commissions is on in AR Options Load Salesperson Surrogate Key
        If muCustDflts.UseSper Then
            moDmFormArAddr.SetColumnValue "SperKey", gvCheckNull(rs.Field("SperKey"), _
                            SQL_INTEGER, True)
        End If
        
      'Track Sales Taxes is on in AR Options Load Sales Tax Schedule Combo
        If muCustDflts.TrackSalesTax Then
        
            If IsNull(rs.Field("STaxSchdKey")) Then
                frmSalesTax.lSTaxSchdKey = -1
            Else
                frmSalesTax.lSTaxSchdKey = CLng(rs.Field("STaxSchdKey"))
            End If
            
            moDmFormArAddr.SetColumnValue "STaxSchdKey", frmSalesTax.lSTaxSchdKey
            
        End If
      
      'Load Default Currency from cust class
        If muCustDflts.UseMultiCurr Then
            sCurrID = gvCheckNull(rs.Field("CurrID"))
            If Len(sCurrID) > 0 Then
                txtCurrency.Text = sCurrID
            End If
        End If
        
      'Load User Fields
        SetUserFlds gvCheckNull(rs.Field("UserFld1")), _
                    gvCheckNull(rs.Field("UserFld2")), _
                    gvCheckNull(rs.Field("UserFld3")), _
                    gvCheckNull(rs.Field("UserFld4"))
    
    
    End If
        
        
  'Clean up SOTADAS Recordset Object
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    'Default Sales Account key
    If bAcctKey Then
        glaAccount.KeyValue = lAcctKey
    Else
        glaAccount.Text = ""
        
    End If
    
       
    If IsEmpty(moDmForm.GetColumnValue("StmtFormKey")) Then
        txtStmtForm.Text = ""
    Else
        moDmForm.DoLink txtStmtForm             'Statement Form
    End If
    
    If IsEmpty(moDmFormArAddr.GetColumnValue("InvcFormKey")) Then
        txtInvoiceForm.Text = ""    'Invoice Form
    Else
        moDmFormArAddr.DoLink txtInvoiceForm    'Invoice Form
    End If
    
    If cboPmtTerms.ListIndex = kItemNotSelected Then
        txtPmtTermsDesc.Text = ""                'Invoice Form
    Else
        moDmFormArAddr.DoLink txtPmtTermsDesc    'Invoice Form
    End If
    
    If IsEmpty(moDmFormArAddr.GetColumnValue("SperKey")) Then
        txtSalesperson.Text = ""                'Salesperson
        txtSperName.Text = ""
    Else
        moDmFormArAddr.DoLink txtSalesperson    'Salesperson
        moDmFormArAddr.DoLink txtSperName
    End If
    
    If IsEmpty(moDmFormArAddr.GetColumnValue("ShipMethKey")) Then
        txtShipMeth.Text = ""                  'Shipping Method
        txtMustUseZone.Text = "0"              'Shipping Zones Used
    Else
        moDmFormArAddr.DoLink txtShipMeth      'Shipping Method
        moDmFormArAddr.DoLink txtMustUseZone   'Shipping Zones Used
    End If
    
    If IsEmpty(moDmFormArAddr.GetColumnValue("FOBKey")) Then 'v3
        txtFOB.Text = ""                  'FOB
    Else
        moDmFormArAddr.DoLink txtFOB      'FOB
    End If
    
  'Clear Out Ship Zone Combo Box
    SetupShipZone
    moDmFormArAddr.SetColumnValue "ShipZoneKey", Empty
    
    
  'Load Tags for validation
    txtCustClass.Tag = txtCustClass.Text                'Customer Class
    txtSalesperson.Tag = txtSalesperson.Text            'Salesperson
'v3    cboPriceList.Tag = cboPriceList.ListIndex           'Price List
    glaAccount.Tag = glaAccount.Text                'Default Sales Account
    glaReturns.Tag = glaReturns.Text                'Default Returns Sales Account
    txtInvoiceForm.Tag = txtInvoiceForm.Text            'Invoice Form
    txtStmtForm.Tag = txtStmtForm.Text                  'Statement Form
    cboStmtCycle.Tag = cboStmtCycle.ListIndex           'Statement Cycle
    txtCurrency.Tag = txtCurrency.Text                  'Currency
    cboPmtTerms.Tag = cboPmtTerms.ListIndex             'Payment Terms
    cboLanguage.Tag = cboLanguage.ListIndex             'Language
    txtShipMeth.Tag = txtShipMeth.Text                  'Shipping Method
    numTradeDiscPct.Tag = numTradeDiscPct.Value         'Trade Discount Percent
    numFinChgPct.Tag = numFinChgPct.Value               'Finance Charge Percent
    curLateChgAmt.Tag = curLateChgAmt.Amount            'Finance Charge Flat Amount
    curCreditLimit.Tag = curCreditLimit.Amount          'Credit Limit Amount
    
    
    txtFOB.Tag = txtFOB.Text                            'FOB--v3
    
    If frmSalesTax.lSTaxSchdKey = -1 Then
         frmSalesTax.lkuSTaxSchd.Tag = ""
    Else
        frmSalesTax.lkuSTaxSchd.Tag = CStr(frmSalesTax.lSTaxSchdKey)
    End If
    
    mbDontChkClick = bOldmb                 'reset dont run click event flag
    SetHourglass False                      'reset Mouse pointer

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetCustomerClassDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub SetUserFlds(ParamArray UserFldVals() As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc: Sets Up data passed in into the user fields
' Parms User field values - ParamArray of Values to load
'************************************************************************
 
    Dim Index     As Integer   'Looping Index
    Dim iCtlIndex As Integer   'Control Index
    Dim sVal      As String    'Stringed Value
    Dim bOldmb    As Boolean
    
  'Loop through for each Value passed in
    For Index = 0 To UBound(UserFldVals)
        With moDmForm
            If IsNull(UserFldVals(Index)) Then UserFldVals(Index) = ""
            If moUserFlds.Usage(Index) <> kvNotUsed Then
              'get control index from user fields class property
                iCtlIndex = moUserFlds.CtlIndex(Index)
            
              'Load user field control Based on data Type
                Select Case moUserFlds.DataType(Index)
                    Case kvAlphanumeric   'Alphanumeric move straight in
                        txtUserFld(iCtlIndex) = UserFldVals(Index)
                      'Set Old Value for Validation
                         moUserFlds.LetOldValue Index, txtUserFld(iCtlIndex).Text
                         
                    Case kvNumeric        'Numeric Convert and move straight in
                         numUserFld(iCtlIndex).Value = Val(UserFldVals(Index))
                      
                      'Set Old Value for Validation
                         moUserFlds.LetOldValue Index, numUserFld(iCtlIndex).Value
                    
                    Case kvDate          'Format date for control
                         dteUserFld(iCtlIndex).Text = _
                                gsFormatDateFromDB(UserFldVals(Index))
                      
                      'Set Old Value for Validation
                         moUserFlds.LetOldValue Index, dteUserFld(iCtlIndex).Text
                    
                    Case kvValidated     'Validated get Position
                        bOldmb = mbDontChkClick
                        mbDontChkClick = True   'Dont run click event
                        
                        sVal = UserFldVals(Index)       'Set Value
                        
                        If Len(sVal) = 0 Then           'No Value passed in
                            cboUserFld(iCtlIndex).ListIndex = kItemNotSelected 'Blank out
                        Else
                          'Get list Index from text Value passed in
                            cboUserFld(iCtlIndex).ListIndex = _
                                giListIndexFromText(cboUserFld(iCtlIndex), sVal)
                        End If
                        
                      'Set Old Value for Validation
                        moUserFlds.LetOldValue Index, cboUserFld(iCtlIndex).ListIndex
                        
                        mbDontChkClick = bOldmb   'reset dont run click event flag
                    
                End Select
            
            End If
        
        End With

    Next Index

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUserFlds", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboBillingType_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboBillingType, True
    #End If
'+++ End Customizer Code Push +++

'Validate Billing type against Consolidated Statement flag since it can't be checked if Billing type is Balance forward
'If caller is a parent we need to check all subsidary customers of that national acct to make sure non of them have
'the consolidated flag turned on

If Not mbDontChkClick Then
    If mbUseNationalAcct Then
        If cboBillingType.ListIndex <> kItemNotSelected Then
            If cboBillingType.ItemData(cboBillingType.ListIndex) = kvBalanceForward Then
                If miNatUserLevel = kiNatParent Then
                    If moClass.moAppDB.Lookup("Count(*) RecCount", _
                        "tarCustomer WITH (NOLOCK)", "ConsolidatedStatement = " & vbChecked & _
                        " AND NationalAcctLevelKey IN " & _
                                    "(SELECT NationalAcctLevelKey FROM tarNationalAcctLevel WITH (NOLOCK) " & _
                                    " WHERE NationalAcctKey = " & mlNatAcctKey & _
                                    " AND NationalAcctLevel = " & kiNatSub & ")") > 0 Then
                        tabC.Tab = kTabDflt                    'move tab back
                        gbSetFocus Me, cboBillingType          'Set focus on control
                        giSotaMsgBox Me, moClass.moSysSession, kmsgBadNatlParentBilling   'Send user msg
                        SetupBillingTypeCombo                  'reset up combo box
                    End If
                ElseIf chkNatConsolidatedStat.Value = vbChecked Then
                    tabC.Tab = kTabDflt                    'move tab back
                    gbSetFocus Me, cboBillingType          'Set focus on control
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBadNatlSubBilling   'Send user msg
                    SetupBillingTypeCombo                  'reset up combo box
                    cboBillingType.ListIndex = Val(cboBillingType.Tag)
                End If
            End If
        End If
    End If
End If
End Sub

Private Sub cboCommPlan_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboCommPlan, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
        Set moValidate.oDmForm = moDmFormArAddr
        moValidate.iTab = kTabBTST
        moValidate.ARCommPlan cboCommPlan, lblCommPlan, "CommPlanKey", _
                mbAllowCommPlanAOF, True, False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCommPlan_Click"
    cboCommPlan.ListIndex = cboCommPlan.Tag
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCommPlan_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboCommPlan_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    mbDontChkClick = True

    If Shift = 0 And KeyCode = vbKeyF3 Then
      'Refresh Combo Box
        gRefreshARCommPlan moClass.moAppDB, moClass.moSysSession, cboCommPlan, _
                                 mbAllowCommPlanAOF, True, msCompanyID
        cboCommPlan.Tag = cboCommPlan.ListIndex
    End If
    
    mbDontChkClick = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCommPlan_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCommPlan_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCountry_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboCountry, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    Dim bValid As Boolean      'Valid flag
    
    If cboCountry.ListIndex = Val(cboCountry.Tag) Then
        Exit Sub
    End If
    
  'Setup the state combo box if currently loading
    If mbRowLoading Then
        SetupStateCombo
        SetCountryMasks (cboCountry)
    End If
    
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
      'Validate country
        Set moValidate.oDB = moClass.moAppDB
        bValid = moValidate.CICountry(cboCountry, lblCountry, "CountryID", _
                    mbAllowCountryAOF, False, False)
        Set moValidate.oDB = moClass.moAppDB

      'Clear out postal code and state if a valid change was made
        If bValid And moValidate.bWasChanged Then
            SetupStateCombo
            txtPostalCode.Text = ""
            txtCity.Text = ""
            txtPostalCode.Tag = txtPostalCode.Text
            SetCountryMasks cboCountry.Text
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCountry_Click"
    cboCountry.ListIndex = cboCountry.Tag
    Set moValidate.oDB = moClass.moAppDB
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCountry_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboCountry_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim OldCountry As String

    mbDontChkClick = True
    
    If Shift = 0 And KeyCode = vbKeyF3 Then
      OldCountry = cboCountry.Text
      'Country
        gRefreshCICountry moClass.moAppDB, moClass.moSysSession, cboCountry, _
                            mbAllowCountryAOF, True
        cboCountry.Tag = cboCountry.ListIndex
        If OldCountry <> cboCountry.Text Then
            SetupStateCombo
        End If
    End If
    
    mbDontChkClick = False


'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCountry_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCountry_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboCredLimitAgeCat_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboCredLimitAgeCat, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
' Desc: Validate The Credit Limit Aging category
'       Check for a person with valid security to change this
'       Value if current user doesn't (if can't change back)
'************************************************************************
On Error GoTo ExpectedErrorRoutine
    Dim iHooktype   As Integer
    Dim bOldmb      As Boolean
    Dim vPrompt     As Variant
    Dim sUser       As String
    Dim sID         As String

    bOldmb = mbDontChkClick
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click true
        If cboCredLimitAgeCat.ListIndex <> cboCredLimitAgeCat.Tag Then
          'Show security form
    
          'Check the user id typed in can override (no cancel hit)
            sID = CStr(ksecChangeCreditLimit)
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            If moClass.moFramework.GetSecurityEventPerm(sID, _
                 sUser, vPrompt) = 0 Then
              
              'Set Flag not to run click event and reset combo Value
                mbDontChkClick = True
                cboCredLimitAgeCat.ListIndex = Val(cboCredLimitAgeCat.Tag)
                mbDontChkClick = bOldmb
            End If
        End If   'No change was made
    End If   'Dont check Click flag is on

    cboCredLimitAgeCat.Tag = cboCredLimitAgeCat.ListIndex

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
ExpectedErrorRoutine:
    'Set Flag not to run click event and reset combo Value
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCredLimitAgeCat_Click"
    mbDontChkClick = True
    cboCredLimitAgeCat.ListIndex = Val(cboCredLimitAgeCat.Tag)
    mbDontChkClick = bOldmb
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCredLimitAgeCat_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboCurrExchSchd_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboCurrExchSchd, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
     
        Set moValidate.oDmForm = moDmFormArAddr
        moValidate.iTab = kTabBTST
        
        moValidate.MCCurrExchSchd cboCurrExchSchd, lblCurrExchSchd, "CurrExchSchdKey", _
                mbAllowCurrExchSchdAOF, True, False
    
        'update currency exchange schedule key on the customer record
        If cboCurrExchSchd.ListIndex = kItemNotSelected Then
            moDmForm.SetColumnValue "CurrExchSchdKey", Empty
            moDmFormArAddr.SetColumnValue "CurrExchSchdKey", Empty
        Else
            moDmForm.SetColumnValue "CurrExchSchdKey", cboCurrExchSchd.ItemData(cboCurrExchSchd.ListIndex)
            moDmFormArAddr.SetColumnValue "CurrExchSchdKey", cboCurrExchSchd.ItemData(cboCurrExchSchd.ListIndex)
        End If
    
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCurrExchSchd_Click"
    cboCurrExchSchd.ListIndex = cboCurrExchSchd.Tag
    gClearSotaErr
    Exit Sub
  
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCurrExchSchd_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboCurrExchSchd_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim sSQL As String
    
    mbDontChkClick = True
    
    If Shift = 0 And KeyCode = vbKeyF3 Then
        SetupCurrExchSchdCbo
    End If
    
    mbDontChkClick = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboCurrExchSchd_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCurrExchSchd_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboLanguage_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboLanguage, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
        Set moValidate.oDmForm = moDmFormArAddr
        moValidate.iTab = kTabBTST
        moValidate.CILanguage cboLanguage, lblLanguage, "LanguageID", _
                mbAllowLanguageAOF, False, True
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboLanguage_Click"
    cboLanguage.ListIndex = cboLanguage.Tag
    gClearSotaErr
    Exit Sub
  
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboLanguage_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboLanguage_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine

    mbDontChkClick = True
    
    If Shift = 0 And KeyCode = vbKeyF3 Then
      'Refresh Combo Box
        gRefreshCILanguage moClass.moAppDB, moClass.moSysSession, cboLanguage, True
        cboLanguage.Tag = cboLanguage.ListIndex
    End If
    
    mbDontChkClick = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboLanguage_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub


VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboLanguage_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboNatDfltBillTO_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboNatDfltBillTO, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDontChkClick Then
        If glGetValidLong(lkuNatAcct.KeyValue) = 0 And cboNatDfltBillTO.ListIndex <> 2 Then
            cboNatDfltBillTO.ListIndex = 2
            giSotaMsgBox Me, moClass.moSysSession, kmsgARNatAcctNoSelected
        ElseIf glGetValidLong(lkuNatAcct.KeyValue) <> 0 And cboNatDfltBillTO.ListIndex <> 2 Then
            moDmForm.SetDirty True
        End If
    End If
End Sub

Private Sub cboPmtTerms_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboPmtTerms, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
        Set moValidate.oDmForm = moDmFormArAddr
        moValidate.iTab = kTabBTST
        moValidate.CIPaymentTerms cboPmtTerms, lblPmtTerms, "PmtTermsKey", _
                mbAllowPmtTermsAOF, True, False
    
        If cboPmtTerms.ListIndex = kItemNotSelected Then
            txtPmtTermsDesc.Text = ""
        Else
            moDmFormArAddr.DoLink txtPmtTermsDesc
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboPmtTerms_Click"
    cboPmtTerms.ListIndex = cboPmtTerms.Tag
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboPmtTerms_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboPmtTerms_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    mbDontChkClick = True

    If Shift = 0 And KeyCode = vbKeyF3 Then
      'Refresh Combo Box
        gRefreshCIPaymentTerms moClass.moAppDB, moClass.moSysSession, cboPmtTerms, _
                             mbAllowPmtTermsAOF, False, msCompanyID
        cboPmtTerms.Tag = cboPmtTerms.ListIndex
    End If
    
    mbDontChkClick = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboPmtTerms_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboPmtTerms_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSalesTerritory_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboSalesTerritory, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
        Set moValidate.oDmForm = moDmFormArAddr
        moValidate.iTab = kTabBTST
        moValidate.ARSalesTerritory cboSalesTerritory, lblSalesTerritory, "SalesTerritoryKey", _
                  mbAllowSalesterritoryAOF, True, False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboSalesTerritory_Click"
    cboSalesTerritory.ListIndex = cboSalesTerritory.Tag
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboSalesTerritory_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboSalesTerritory_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    mbDontChkClick = True

    If Shift = 0 And KeyCode = vbKeyF3 Then
      'Refresh Combo Box
        gRefreshARSalesTerritory moClass.moAppDB, moClass.moSysSession, cboSalesTerritory, _
              mbAllowSalesterritoryAOF, False, msCompanyID
        cboSalesTerritory.Tag = cboSalesTerritory.ListIndex
    End If
    
    mbDontChkClick = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboSalesTerritory_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboSalesTerritory_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboShipZone_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboShipZone, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
        Set moValidate.oDmForm = moDmFormArAddr
        moValidate.iTab = kTabBTST
        moValidate.CIShipZone cboShipZone, lblShipZone, "ShipZoneKey", _
              mbAllowShipZoneAOF, True, (CBool(txtMustUseZone.Text) And _
              ((mbPOActive And 1 = 2) Or mbSOActive))
        If cboShipZone.ListIndex = kItemNotSelected Then
            If Not IsEmpty(moDmFormArAddr.GetColumnValue("ShipZoneKey")) Then
                moDmFormArAddr.SetColumnValue "ShipZoneKey", ""  'Clear out ship zone
            End If
        Else
            moDmFormArAddr.SetColumnValue "ShipZoneKey", cboShipZone.ItemData(cboShipZone.ListIndex)
        End If
    End If
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboShipZone_Click"
    cboShipZone.ListIndex = cboShipZone.Tag
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboShipZone_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboShipZone_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    mbDontChkClick = True

    If Len(txtShipMeth) > 0 Then  'Only Run if Valid Sales Tax Schedule
        If Shift = 0 And KeyCode = vbKeyF3 Then
            If mbSOActive Or (mbPOActive And 1 = 2) Then       'Do not enable ship zones
              'Refresh Combo Box
                gRefreshCIShipZone moClass.moAppDB, moClass.moSysSession, cboShipZone, _
                      mbAllowShipZoneAOF, False, moDmFormArAddr.GetColumnValue("ShipMethKey")
            End If
            cboShipZone.Tag = cboShipZone.ListIndex
        End If
    End If

    mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboShipZone_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboShipZone_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboState_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboState, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
      'Run Standard database validation
        Set moValidate.oDB = moClass.moAppDB
        moValidate.CIState cboState, lblState, "StateID", _
                    mbAllowStateAOF, False, False, (cboCountry.Text)
        Set moValidate.oDB = moClass.moAppDB
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboState_Click"
    cboState.ListIndex = cboState.Tag
    Set moValidate.oDB = moClass.moAppDB
    gClearSotaErr
    Exit Sub


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboState_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboState_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    mbDontChkClick = True
    If Shift = 0 And KeyCode = vbKeyF3 Then
        SetupStateCombo
    End If
    mbDontChkClick = False
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboState_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboState_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboStatus_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboStatus, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'  Desc:  if Customer Status Is Deleted Disable all controls
'         otherwise keep controls Enabled
'**********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim iHooktype   As Integer
    Dim sUser       As String
    Dim sID         As String
    Dim vPrompt     As Variant
    Dim bOldmb      As Boolean

    ' save current 'check click' flag
    bOldmb = mbDontChkClick

    
    If Not mbDontChkClick Then
        ' no changes -- exit now
        If cboStatus.ListIndex = cboStatus.Tag Then Exit Sub
    
    
        'Show security form
       
        'Check the user id typed in can override (no cancel hit)
        sID = CStr(ksecChangeStatus)
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True
        If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
            mbDontChkClick = True
            cboStatus.ListIndex = Val(cboStatus.Tag)
            mbDontChkClick = bOldmb
            Exit Sub
        End If
        
        If cboStatus.ListIndex <> kItemNotSelected Then
            If cboStatus.ItemData(cboStatus.ListIndex) <> kvActive Then
                'if parent national account, give message and set status back
                    If miNatUserLevel = 1 Then
                        'Parent
                        giSotaMsgBox Me, moClass.moSysSession, kmsgARNatlAcctPCustStatusInv
                        mbDontChkClick = True
                        cboStatus.ListIndex = Val(cboStatus.Tag)
                        mbDontChkClick = bOldmb
                    End If
                Exit Sub
            End If
        End If
        
    End If

    'Nothing Selected enable all controls
    If cboStatus.ListIndex = kItemNotSelected Then
          If msCustStatus = "Disabled" Then      'controls are Disabled
              EnableControls True                 '  enable them
          End If
          
          msCustStatus = ""                      'set tag to not Disabled
          
          If moDmForm.State <> kDmStateNone And _
             mlRunMode <> kContextAOF And _
             miSecurityLevel <> sotaTB_DISPLAYONLY Then
             tbrMain.Buttons(kTbRenameId).Enabled = True
          End If
    Else
        'Status is Deleted
          If cboStatus.ItemData(cboStatus.ListIndex) = kvDeleted Then
              If msCustStatus <> "Disabled" Then    'controls are not Disabled
                  EnableControls False               '  disable them
              End If
              
              msCustStatus = "Disabled"             'set tag to Disabled
              
              If mlRunMode <> kContextAOF Then
                  tbrMain.Buttons(kTbRenameId).Enabled = False
              End If
          Else                                   'Anything but Deleted
              If msCustStatus = "Disabled" Then    'Controls are disables
                  EnableControls True               '  Enable controls
              End If
              msCustStatus = ""                    'set tag to not diabled
              
              If moDmForm.State <> kDmStateNone And _
                 mlRunMode <> kContextAOF And _
                 miSecurityLevel <> sotaTB_DISPLAYONLY Then  'And
                 tbrMain.Buttons(kTbRenameId).Enabled = True
              End If
          End If
    End If

    cboStatus.Tag = cboStatus.ListIndex
    Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboStatus_Click"
    mbDontChkClick = True
    cboStatus.ListIndex = Val(cboStatus.Tag)
    mbDontChkClick = bOldmb
    gClearSotaErr

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboStatus_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboStmtCycle_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboStmtCycle, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkClick Then  'Don't Run if global Don't check click
        Set moValidate.oDmForm = moDmForm
        moValidate.iTab = kTabDflt
        moValidate.CIProcCycle cboStmtCycle, lblStmtCycle, "StmtCycleKey", _
                  mbAllowProcCycleAOF, True, False
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboStmtCycle_Click"
    cboStmtCycle.ListIndex = cboStmtCycle.Tag
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboStmtCycle_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboStmtCycle_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    mbDontChkClick = True

    If Shift = 0 And KeyCode = vbKeyF3 Then
      'Refresh Combo Box
        gRefreshCIProcCycle moClass.moAppDB, moClass.moSysSession, cboStmtCycle, _
                             mbAllowProcCycleAOF, False, msCompanyID
        cboStmtCycle.Tag = cboStmtCycle.ListIndex
    End If
    
    mbDontChkClick = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "cboStmtCycle_KeyDown"
    gClearSotaErr
    mbDontChkClick = False
    Exit Sub
    
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboStmtCycle_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
''Private Sub cboUserFld_Click(Index As Integer)
'''+++ VB/Rig Begin Push +++
''#If ERRORTRAPON Then
''On Error GoTo VBRigErrorRoutine
''#End If
'''+++ VB/Rig End +++
'''****************************************************************************
''' Description: cboUserFld_Click validates the entry if not a selection
'''              from the list.  If not valid then AddOnTheFly object is called.
''' Parms:       index - Control Array Index for combo box
'''****************************************************************************
''
''    Dim iUserFldIndex   As Integer   'User field Index
''    Dim rs              As New DASRecordSet    'record set object
''    Dim bValid          As Boolean   'Boolean valid flag
''    Dim sUserFldValue   As String    'User field Value
''
''    iUserFldIndex = CInt(cboUserFld(Index).Tag)  'get User field Index from Control Tag
''
''  'If field is required see if you Have mad a  Valid Combo Selection
''  'if you have then reset the old Value and exit the subroutine
''    If moUserFlds.Usage(iUserFldIndex) = kvRequired Then
''        bValid = gbValidComboSelect(cboUserFld(Index), moUserFlds.GetOldValue(iUserFldIndex))
''    Else
''        bValid = gbValidComboSelect(cboUserFld(Index))
''    End If
''
''  'Selection was from list exit subroutine
''    If bValid Then
''        moUserFlds.LetOldValue CInt(cboUserFld(Index).Tag), cboUserFld(Index).ListIndex
'''+++ VB/Rig Begin Pop +++
'''+++ VB/Rig End +++
''        Exit Sub
''    End If
''
''  'Set User field Value to blank Item (to create new Item in add on the fly)
''    sUserFldValue = ""
''
'' 'If Called From code Assume it is set to a good Value
'' 'Otherwise Call User field validation routine
''    If mbDontChkClick Then
''        bValid = True
''    Else
''      'Validate User field
''        bValid = gbValidCIUserFldValue(moClass.moFramework, moSotaObjects, moClass.moAppDB, rs, _
''                                    mbAllowUserFieldValueAOF, _
''                                   sUserFldValue, moUserFlds.Title(iUserFldIndex), _
''                                   moUserFlds.UserFldKey(iUserFldIndex), kEntTypeARCustomer)
''
''      'If Valid Add New field to combo box(if not already there (AOF))
''      'Set combo box to new field and reset old Value
''      'otherwise reset combo back to what is was
''        If bValid Then
''            gComboAddItem cboUserFld(Index), rs.Field("UserFldValue")
''            cboUserFld(Index).ListIndex = cboUserFld(Index).NewIndex
''            moUserFlds.LetOldValue CInt(cboUserFld(Index).Tag), cboUserFld(Index).ListIndex
''        Else
''            cboUserFld(Index).ListIndex = moUserFlds.GetOldValue(iUserFldIndex)
''            gbSetFocus Me, cboUserFld(Index)
''        End If
''
''      'Clear out old recordset Object
''        If Not rs Is Nothing Then
''            rs.Close
''            Set rs = Nothing
''        End If
''    End If
''
'''+++ VB/Rig Begin Pop +++
'''+++ VB/Rig End +++
''Exit Sub
''
'''Error occurred reset Value back to what it was
''ExpectedErrorRoutine:
''    MyErrMsg moClass, Err.Description, Err, sMyName, "cboUserFld_Click"
''    cboUserFld(Index).ListIndex = moUserFlds.GetOldValue(iUserFldIndex)
''    gClearSotaErr
''    Exit Sub
''
'''+++ VB/Rig Begin Pop +++
''        Exit Sub
''
''VBRigErrorRoutine:
''        gSetSotaErr Err, sMyName, "cboUserFld_Click", VBRIG_IS_FORM
''        Select Case VBRIG_IS_CONTROL_EVENT
''        Case VBRIG_IS_NON_EVENT
''                Err.Raise guSotaErr.Number
''        Case Else
''                Call giErrorHandler: Exit Sub
''        End Select
'''+++ VB/Rig End +++
''End Sub
Private Sub cboUserFld_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************************
' Description: Refreshes the User field Combo Box if F3 Key is pressed
' Parms:       index - Control Array Index for combo box
'              KeyCode - Ansi Code of Key pressed
'              Shift - Shift, Ctrl or alt Key Pressed
'****************************************************************************
    
    Dim iUserFldIndex   As Integer
    
    iUserFldIndex = CInt(cboUserFld(Index).Tag)
    
  'F3 key was pressed (without Shift, Ctrl or alt being pressed)
  'Refresh Combo Box List of User field Values
    If KeyCode = vbKeyF3 And Shift = 0 Then
        gRefreshCIUserFieldValue moClass.moAppDB, moClass.moSysSession, cboUserFld(Index), _
                                 mbAllowUserFieldValueAOF, (moUserFlds.Usage(iUserFldIndex) = kvRequired), _
                                 moUserFlds.UserFldKey(iUserFldIndex)
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboUserFld_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub







Private Sub chkCloseSOLineOnFirst_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkCloseSOLineOnFirst, True
    #End If
'+++ End Customizer Code Push +++
     If chkCloseSOLineOnFirst.Value = 1 Then
        chkCloseSOOnFirst.Value = 0
        chkCloseSOOnFirst.Enabled = False
    Else
        chkCloseSOOnFirst.Enabled = True
    End If
    
     
End Sub

Private Sub chkCloseSOOnFirst_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkCloseSOOnFirst, True
    #End If
'+++ End Customizer Code Push +++
     If chkCloseSOOnFirst.Value = 1 Then
        chkCloseSOLineOnFirst.Value = 0
        chkCloseSOLineOnFirst.Enabled = False
    Else
        chkCloseSOLineOnFirst.Enabled = True
    End If
End Sub

Private Sub chkCreditLimitUsed_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkCreditLimitUsed, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
' Desc: Validate The Credit Limit Used checkbox
'       Check for a person with valid security to change this
'       Value if current user doesn't (if can't change back)
'************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim iHooktype As Integer
    Dim vPrompt     As Variant
    Dim sUser       As String
    Dim sID         As String
    Static bMyClick     'use this flag so this event event does not fire itself

    If Not mbDontChkClick Then  'Don't Run if global Don't check click true
        If bMyClick Then        'This event was called from itself
            bMyClick = False    'reset click called from this form code flag
        Else
          'Show security form
    
          'Check the user id typed in can override (no cancel hit)
            sID = CStr(ksecChangeCustHold)
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            
            If Not mbSkipSecurityEventCheck Then
                If moClass.moFramework.GetSecurityEventPerm(sID, _
                     sUser, vPrompt) = 0 Then
                    bMyClick = True         'Set flag click event called from here
                    If Len(Trim$(chkCreditLimitUsed.Tag)) > 0 Then
                        chkCreditLimitUsed.Value = chkCreditLimitUsed.Tag   'Reset Value Back
                    End If
                    bMyClick = False    'Set Click flag to false
                End If          'User Can override
            End If
        End If  'Called from itself
    End If   'Dont check Click flag is on
    
    'Credit limit box is enabled only when the CreditLimitUsed check box is checked
    If muCustDflts.CheckCreditLimit And chkCreditLimitUsed.Value = vbChecked Then
        gEnableControls curCreditLimit
    Else
        gDisableControls curCreditLimit
    End If
    
    chkCreditLimitUsed.Tag = chkCreditLimitUsed.Value   'Reset Value Back
              
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
    
ExpectedErrorRoutine:
'Set Flag not to run click event and reset combo Value
    MyErrMsg moClass, Err.Description, Err, sMyName, "chkCreditLimitUsed_Click"
    bMyClick = True                     'Set flag click event called from here
    chkCreditLimitUsed.Value = chkCreditLimitUsed.Tag     'Set Old Value
    bMyClick = False                    'Set Click flag to false
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkCreditLimitUsed_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub chkInvoiceReqd_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInvoiceReqd, True
    #End If
'+++ End Customizer Code Push +++
If Not mbDontChkClick Then
    If Len(Trim(txtInvoiceForm)) = 0 Then
        If chkInvoiceReqd.Value = vbChecked Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgSelectInvoiceForm
            tabC.Tab = kTabBTST
            gbSetFocus Me, txtInvoiceForm
        End If
    End If
End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkInvoiceReqd_Click", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub chkNatAllowParentPay_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkNatAllowParentPay, True
    #End If
'+++ End Customizer Code Push +++
    If Not mbDontChkClick Then
        If chkNatAllowParentPay.Value = vbChecked And glGetValidLong(lkuNatAcct.KeyValue) = 0 Then
    
            chkNatAllowParentPay.Value = vbUnchecked
            giSotaMsgBox Me, moClass.moSysSession, kmsgARNatAcctNoSelected
        End If
    End If
    
End Sub

Private Sub chkNatConsolidatedStat_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkNatConsolidatedStat, True
    #End If
'+++ End Customizer Code Push +++

Dim iParentBillType As Integer

 If Not mbDontChkClick Then
 
    If chkNatConsolidatedStat.Value = vbChecked And glGetValidLong(lkuNatAcct.KeyValue) = 0 Then
    
        chkNatConsolidatedStat.Value = vbUnchecked
        giSotaMsgBox Me, moClass.moSysSession, kmsgARNatAcctNoSelected
    Else
    'Validate Consolidated Statement since it can't be checked if customer's Billing type is Balance forward
     If chkNatConsolidatedStat.Value = vbChecked Then
         If cboBillingType.ItemData(cboBillingType.ListIndex) = kvBalanceForward Then
             tabC.Tab = kTabNatAcct                    'move tab back
             gbSetFocus Me, chkNatConsolidatedStat  'Set focus on control
             giSotaMsgBox Me, moClass.moSysSession, kmsgBadNatlBillingType   'Send user msg
             chkNatConsolidatedStat.Value = chkNatConsolidatedStat.Tag
         Else
            'Also check the parent to make sure is not of balance forward billing type
             iParentBillType = giGetValidInt(moClass.moAppDB.Lookup("BillingType", "tarCustomer WITH (NOLOCK)", "CustID = " & gsQuoted(txtNatParentCustID) & " AND CompanyID = " & gsQuoted(msCompanyID)))
             If iParentBillType = kvBalanceForward Then
                 tabC.Tab = kTabNatAcct                    'move tab back
                 gbSetFocus Me, chkNatConsolidatedStat  'Set focus on control
                 giSotaMsgBox Me, moClass.moSysSession, kmsgBadNatlBillingType   'Send user msg
                 chkNatConsolidatedStat.Value = chkNatConsolidatedStat.Tag
             End If
         End If
         
         
     End If
     
     
    End If
End If

End Sub

Private Sub chkOnHold_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkOnHold, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
' Desc: Validate The Credit Limit Aging category
'       Check for a person with valid security to change this
'       Value if current user doesn't (if can't change back)
'************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim iHooktype As Integer
    Dim vPrompt     As Variant
    Dim sUser       As String
    Dim sID         As String
    Static bMyClick     'use this flag so this event event does not fire itself
    

    If Not mbDontChkClick Then  'Don't Run if global Don't check click true
        If bMyClick Then      'This event was called from itself
            bMyClick = False     'reset click called from this form code flag
        Else
          'Show security form
    
          'Check the user id typed in can override (no cancel hit)
            sID = CStr(ksecChangeCustHold)
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            If moClass.moFramework.GetSecurityEventPerm(sID, _
                 sUser, vPrompt) = 0 Then
                bMyClick = True         'Set flag click event called from here
                chkOnHold.Value = chkOnHold.Tag   'Reset Value Back
                bMyClick = False    'Set Click flag to false
            End If          'User Can override
        End If  'Called from itself
    End If   'Dont check Click flag is on

    chkOnHold.Tag = chkOnHold.Value   'Reset Value Back
              
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
    
ExpectedErrorRoutine:
'Set Flag not to run click event and reset combo Value
    MyErrMsg moClass, Err.Description, Err, sMyName, "chkOnHold_Click"
    bMyClick = True                     'Set flag click event called from here
    chkOnHold.Value = chkOnHold.Tag     'Set Old Value
    bMyClick = False                    'Set Click flag to false
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkOnHold_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function CheckBeforeSave() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc:     Make Sure all required fields are there
' Returns:  kdmSuccess - All required Fields are there
'           kdmfailure - All required fields are not there
'*****************************************************************************
    
    Dim iActionCode As Integer   'Setup return code to pass along
    Dim bOldmb      As Boolean
    Dim iRetval     As Integer

    iActionCode = kDmSuccess    'Initialize return code
    
  'Main tab Customer  (Customer Class, Status Code
    iActionCode = gbCheckBeforeSave(Me, lblCustClass, txtCustClass, iActionCode, tabC, kTabMain, moDmForm)
    iActionCode = gbCheckBeforeSave(Me, lblStatus, cboStatus, iActionCode, tabC, kTabMain, moDmForm)
    iActionCode = gbCheckDate(Me, lblDateEstablished, txtDateEstablished, iActionCode, tabC, kTabMain, moDmForm)
    
    If ddnShipPriority.ListIndex = -1 Then
          giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, lblShippingPriority
          iActionCode = kDmFailure
    End If
    
    If ddnPriceBase.ListIndex = -1 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, lblPriceBase
        iActionCode = kDmFailure
    End If
  'Main tab Addresses   (Country field)
    'iActionCode = gbCheckBeforeSave(Me, lblCountry, cboCountry, iActionCode, tabC, kTabMain, moDmForm)
    
  'Defaults tab Billing Defaults (Billing Type)
    iActionCode = gbCheckBeforeSave(Me, lblBillingType, cboBillingType, iActionCode, tabC, kTabDflt, moDmForm)
  
  'Defaults tab User Defined Fields
  'Warn User they have not put in all required user fields
    If iActionCode = kDmSuccess Then
        If Not moUserFlds.IsValidUserFlds(Me) Then   'Check required user fields are there
            If Not moUserFlds.BadUserFld Is Nothing Then
                moDmForm.CancelAction
                giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, _
                    moUserFlds.Title(Val(moUserFlds.BadUserFld.Tag))
                tabC.Tab = kTabDflt                         'Reset Tab
                gbSetFocus Me, moUserFlds.BadUserFld        'Set To UserField
                iActionCode = kDmFailure                    'Set Return Code
            End If
        End If
    End If
    
  'Bill To/Ship To tab Language
    iActionCode = gbCheckBeforeSave(Me, lblLanguage, cboLanguage, iActionCode, tabC, kTabBTST, moDmForm)
    
  'Run Check On Sales Tax Schedule on the sales Tax Schedule Form
  'If Track Sales Taxes is on in AR Options
    If muCustDflts.TrackSalesTax Then
        bOldmb = mbDontChkClick
        mbDontChkClick = False
        iActionCode = frmSalesTax.CheckBeforeSave(iActionCode, moDmForm)
        mbDontChkClick = bOldmb
    End If
    
  'Bill To/Ship To tab Bill To Ship To Address
    iActionCode = gbCheckBeforeSave(Me, lblBillTo, txtBillTo, iActionCode, tabC, kTabBTST, moDmForm)
    iActionCode = gbCheckBeforeSave(Me, lblShipTo, txtShipTo, iActionCode, tabC, kTabBTST, moDmForm)
    
  'Bill To/Ship To tab Shipping Defaults
  'If must use Zone option is on For current ship method
    If Val(txtMustUseZone) <> 0 And _
        ((mbPOActive And 1 = 2) Or mbSOActive) Then
        iActionCode = gbCheckBeforeSave(Me, lblShipZone, cboShipZone, iActionCode, tabC, kTabBTST, moDmForm)
    End If


  'Bill To/Ship TO tab
  'If use multiCurrency is on in AR options force them to put in a Currency field
    If muCustDflts.UseMultiCurr Then
        iActionCode = gbCheckBeforeSave(Me, lblCurrID, txtCurrency, iActionCode, tabC, kTabBTST, moDmForm)
    Else
        If Len(gvCheckNull(moDmFormArAddr.GetColumnValue("CurrID"))) = 0 Then
            moDmFormArAddr.SetColumnValue "CurrID", msDfltCurrency
        End If
    End If
    
  'Validate billing Type in list from ar options
    If iActionCode = kDmSuccess And _
       moDmForm.Columns("BillingType").IsDirty Then
      'Get Billing Type allowed from AR Options
        muCustDflts.BillingTypeOpt = gvCheckNull(moClass.moAppDB.Lookup("BillingTypeOpt", "tarOptions", msCompanyWhere), SQL_INTEGER)
        Select Case muCustDflts.BillingTypeOpt   'Check option Vs. what is allowed in AR Options
            
            Case kvOpenItem  'Open Item
                If cboBillingType.ItemData(cboBillingType.ListIndex) <> kvOpenItem Then
                    tabC.Tab = kTabDflt                  'move tab back
                    gbSetFocus Me, cboBillingType        'Set focus on control
                    moDmForm.CancelAction
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBadBillingType   'Send user msg
                    SetupBillingTypeCombo                 'reset up combo box
                    iActionCode = kDmFailure              'Set Flag
                End If
            
            Case kvBalanceForward  'Balance Forward
                If cboBillingType.ItemData(cboBillingType.ListIndex) <> kvBalanceForward Then
                    tabC.Tab = kTabDflt                  'move tab back
                    gbSetFocus Me, cboBillingType        'Set focus on control
                    moDmForm.CancelAction
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBadBillingType   'Send user msg
                    SetupBillingTypeCombo                 'reset up combo box
                    iActionCode = kDmFailure              'Set Flag
                End If
            
            Case Else
                iActionCode = gbCheckBeforeSave(Me, lblBillingType, cboBillingType, iActionCode, tabC, kTabDflt, moDmForm)
        End Select
       'Set consolidated statement flag to 0 to maintain integrity if
       '(tarOptions.UseNationalAccts = 0 and billing type = balance forward)
        If mbUseNationalAcct = False Then
                If cboBillingType.ListIndex <> kItemNotSelected Then
                    If cboBillingType.ItemData(cboBillingType.ListIndex) <> kvOpenItem Then
                        moDmForm.SetColumnValue "ConsolidatedStatement", Empty 'Set to Null
                    End If
                End If
        End If
    End If
   'Display an error if there is no National Acct Bill To picked. This
    If (iActionCode = kDmSuccess) Then
        If mbUseNationalAcct Then
           If Not IsEmpty(lkuNatAcct.KeyValue) Then
                If cboNatDfltBillTO.ListIndex = 2 And miNatUserLevel <> kiNatParent Then
                    tabC.Tab = kTabNatAcct                'move tab back
                    gbSetFocus Me, cboNatDfltBillTO       'Set focus on control
                    moDmForm.CancelAction
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBadNatlDfltBillTo  'Send user msg
                    iActionCode = kDmFailure              'Set Flag
                 End If
            Else
                cboNatDfltBillTO.ListIndex = 2
            End If
        End If
    End If
    If iActionCode = kDmSuccess And moDmForm.State = kDmStateEdit And _
                    cboBillingType.ListIndex <> kItemNotSelected Then
        If cboBillingType.ItemData(cboBillingType.ListIndex) = kvBalanceForward Then
            With moClass.moAppDB
                .SetInParam CLng(gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER))
                .SetInParam (txtCurrency.Text)
                .SetInParam CLng(gvCheckNull(moDmForm.GetColumnValue("PrimaryAddrKey"), SQL_INTEGER))
                .SetOutParam iRetval
                .ExecuteSP ("sparCanSaveBalFwd")
                iRetval = .GetOutParam(4)
                .ReleaseParams
            End With
            If iRetval = 1 Then
                moDmForm.CancelAction
                giSotaMsgBox Me, moClass.moSysSession, kmsgNoCurrChgInvExists   'Send user msg
                iActionCode = kDmFailure              'Set Flag
            ElseIf iRetval = 2 Then
                moDmForm.CancelAction
                giSotaMsgBox Me, moClass.moSysSession, kmsgNoCurrChgPInvExists   'Send user msg
                iActionCode = kDmFailure              'Set Flag
            ElseIf iRetval = 3 Then
                moDmForm.CancelAction
                giSotaMsgBox Me, moClass.moSysSession, kmsgNoCurrChgAddr   'Send user msg
                iActionCode = kDmFailure              'Set Flag
            End If
        End If
    End If
    
    CheckBeforeSave = iActionCode   'Set Return Code


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckBeforeSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub GetNextCustNo(Optional UI As Boolean = True, _
                          Optional CustID As String = "", _
                          Optional ErrNo As Long = 0, _
                          Optional ErrData As Variant = "")
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
' Desc: Gets Next Number from stored procedure if Automatic numbering is on
' Parm: UI      Optional for Copy feature which does not use the UI
'       CustID  Optional for Copy feature to return the Next Cust ID
'       ErrNo   Optional for Copy feature to return error number
'       ErrData Optional for Copy feature to return error data

'**************************************************************************
On Error GoTo ExpectedErrorRoutine2
    
    Dim iLoop       As Integer      'Looping Variable
    Dim iNumChars   As Integer      'Number of characters
    Dim iRetval     As Integer      'Return Code Value from Stored procedure
    Dim sCustID     As String       'Customer ID Stringed
    Dim bInTrans    As Boolean      'In Transaction Flag
    Dim lErr        As Long
    Dim sErrDesc    As String
    
    If (UI) Then SetHourglass True               'Set Mouse Pointer to hourglass
  
  'Get Number of edit characters in mask
    For iLoop = 1 To Len(txtCustomerID.Mask)    'Loop Through each character in mask
        If Mid$(txtCustomerID.Mask, iLoop, 1) = "X" Then    'Check for edit character
            iNumChars = iNumChars + 1                       'Increment Counter
        End If
    Next iLoop
  
  'No Edit characters found so assume no mask - Set To max Characters
    If iNumChars = 0 Then
        iNumChars = 12              'Cust id = varchar(12)
    End If
    
  'Run Next Number stored procedure
    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB                'Run from Application Database
      .SetInParam msCompanyID           'Company ID
      .SetInParam iNumChars             'Number of Characters
      .SetOutParam sCustID              'Next Customer Id to use
      .SetOutParam iRetval              'Return Code
      .BeginTrans                       'Begin Transaction
      bInTrans = True                   'Set In Transaction Flag
      .ExecuteSP ("spGetNextCustNo")   'Run Stored Procedure
      sCustID = .GetOutParam(3)         'Get Customer Id
      iRetval = .GetOutParam(4)         'Set Return Code
      .ReleaseParams                    'Release Stored procedure parameters
      .CommitTrans                      'Commit The transaction
      bInTrans = False                  'rest in transaction flag
    End With
    
    On Error GoTo ExpectedErrorRoutine2
    
  'Run return Value Checking
    Select Case iRetval
      
      'An unknown stored procedure error occurred
        Case 0
          'Send an error and reset focus on customer id
            If (UI) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                                        "0  spGetNextCustNo"
                gbSetFocus Me, txtCustomerID
            Else
                CustID = ""
                ErrNo = kmsgUnexpectedSPReturnValue
                ErrData = "0  spGetNextCustNo"
            End If
      'Next customer id found
        Case 1
          'Truncate Customer id (on Left) for ID
          'Due to the other applications (AR Invoice Entry, SO Entry etc.)do not automatic
          'pad the CustID yet, remove the leading zero for now.
            sCustID = Trim(CStr(Val(sCustID)))
            If (UI) Then
                txtCustomerID.Text = Right(sCustID, iNumChars)
            Else
                CustID = Right(sCustID, iNumChars)
            End If
        
          'Run Customer Validation Set Focus Back on Customer id if failed
            If IsValidCustomer(CustID, ErrNo, ErrData) = kDmFailure Then
                If (UI) Then
                    gbSetFocus Me, txtCustomerID
                Else
                    CustID = ""
                End If
            End If
    
      'All numbers have been used
        Case 2
          'Send an error and reset focus on customer id
            If (UI) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgAllCustNumbersUsed
                gbSetFocus Me, txtCustomerID
            Else
                CustID = ""
                ErrNo = kmsgAllCustNumbersUsed
            End If
      'unknown stored procedure error
        Case Else
          'Send an error and reset focus on customer id
            If (UI) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, iRetval
                gbSetFocus Me, txtCustomerID
            Else
                CustID = ""
                ErrNo = kmsgUnexpectedSPReturnValue
                ErrData = CVar(iRetval)
            End If
    End Select
    
    If (UI) Then SetHourglass False      'Reset Mouse Pointer

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:
    'rollback the transaction if in One
    lErr = Err
    sErrDesc = Err.Description
    On Error Resume Next
    If bInTrans Then
        moClass.moAppDB.Rollback
    End If
    'return focus back to customer id
    If (UI) Then
        gbSetFocus Me, txtCustomerID
        SetHourglass False      'Reset Mouse Pointer
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                "spGetNextCustNo: " & sErrDesc
    Else
        CustID = ""
        ErrNo = kmsgUnexpectedSPReturnValue
        ErrData = "spGetNextCustNo: " & sErrDesc
    End If
    gClearSotaErr
    Exit Sub

ExpectedErrorRoutine2:
    lErr = Err
    sErrDesc = Err.Description
    'return focus back to customer id
    If (UI) Then
        gbSetFocus Me, txtCustomerID
        SetHourglass False      'Reset Mouse Pointer
        MyErrMsg moClass, sErrDesc, lErr, sMyName, "GetNextCustNo"
    Else
        CustID = ""
    End If
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetNextCustNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

    SetChkCloseSOFirst
    

End Sub

Private Sub cmdAvaAddr_Click() 'AvaTax Integration - Address Validation
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'cmdAvaAddr will only be enabled if AvaTax is installed (mbAvaAddrEnabled = True)
On Error GoTo ExpectedErrorRoutine

    SetHourglass True

    If moAvaTax Is Nothing Then
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

On Error GoTo VBRigErrorRoutine

    moAvaTax.ValidateAddress Me

    SetHourglass False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'If the cmdAvaAddr button was enabled, then the tavConfiguration table was found,
'and that means that AvaTax is likely used for this Site.  But when creating the
'local client object, the object was not found, and so, likely not installed.
'This is not a good error, let the administrator know to check that AvaTax was
'not properly implemented.

    SetHourglass False
    
    ' kMsgAvaObjectError: "The AvaTax options are enabled, but the client object AVZDADL1 not found on this machine."
    giSotaMsgBox Me, moClass.moSysSession, kmsgAvaObjectError

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdAvaAddr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub cmdContAddr_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdContAddr(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'*********************************************************************************
' Pull up Address or contacts form (based on control index)
'*********************************************************************************
    Dim iHooktype   As Integer
    Dim bOldmb      As Boolean
    
    If Index Then mbReloadSTaxExmpt = True
    
'Dont run click event when already clicked when process is not completed
If Not mbContAddrClick Then
    mbContAddrClick = True
    If Not moLostFocus.IsValidDirtyCheck() Then
        mbContAddrClick = False
        Exit Sub
    End If

    bOldmb = mbDontChkClick
  
    Dim iConfirmUnload    As Integer  'Confirm Unload
    
    If Index Then   'Addresses
        'No record saved yet or exemptions changed.  Need to save if exemptions changed
        'otherwise possible user creates new address which will have a tarCustSTaxExmpt
        'record created for the unsaved exemption, and then exits from the customer
        'record (so the exemption was saved for the new address but none of the
        'other existing addresses).
        
        If moDmForm.State = kDmStateAdd Or frmExemptions.bExemptionsChanged Then
            If miSecurityLevel = sotaTB_DISPLAYONLY Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                mbContAddrClick = False
                Exit Sub
            End If
            If Not gbSetFocus(Me, cmdContAddr(1)) Then
                mbDontChkClick = bOldmb              'Dont run click event on form clear
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                mbContAddrClick = False
                Exit Sub
            End If
            iConfirmUnload = moDmForm.ConfirmUnload(True)
            If iConfirmUnload = kDmSuccess Then         'Save Was successful
                If moDmForm.State = kDmStateAdd Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARSaveBeforeAddress
                Else
                    EnterAddresses                      'Run Enter Addresses
                End If
            Else
                If iConfirmUnload = kDmError Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingAddress
                Else
                    If frmSalesTax.bBadSalesTax Then
                        cmdSalesTax_Click
                    End If
                End If
            End If
        ElseIf moDmForm.State = kDmStateEdit Then  'Form is in edit
            EnterAddresses                         'Run Enter Addresses
        End If
    Else            'Contacts
        If moDmForm.State = kDmStateAdd Then      'No record saved yet
            If miSecurityLevel = sotaTB_DISPLAYONLY Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                mbContAddrClick = False
                Exit Sub
            End If
            If Not gbSetFocus(Me, cmdContAddr(0)) Then
                mbDontChkClick = bOldmb       'Dont run click event on form clear
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                mbContAddrClick = False
                Exit Sub
            End If
            iConfirmUnload = moDmForm.ConfirmUnload(True)
            If iConfirmUnload = kDmSuccess Then 'Save Was successful
                If moDmForm.State = kDmStateAdd Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARSaveBeforeContact
                Else
                    EnterContacts                     'Run Enter Contacts
                End If
            Else
                If iConfirmUnload = kDmError Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingContacts
                Else
                    If frmSalesTax.bBadSalesTax Then
                        cmdSalesTax_Click
                    End If
                End If
            End If
        ElseIf moDmForm.State = kDmStateEdit Then  'Form is in edit
            EnterContacts                         'Run Enter Addresses
        End If
    End If

    mbContAddrClick = False
 End If
 
'+++ VB/Rig Begin Pop +++
 Exit Sub

VBRigErrorRoutine:
        mbContAddrClick = False
        gSetSotaErr Err, sMyName, "cmdContAddr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreditCards_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdCreditCards, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim oCCMaint As Object
    Dim lCustkey As Long
    Dim iConfirmUnload As Integer
    

    iConfirmUnload = moDmForm.ConfirmUnload(True)
    
    If iConfirmUnload = kDmSuccess Then
    
        If Not moDmForm.IsDirty Then
        
            Set oCCMaint = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsCCMaintainCreditCardsDLL, ktskCCMaintainCreditCardsDLL, _
                              kAOFRunFlags, kContextAOF)
        
            lCustkey = moDmForm.GetColumnValue("CustKey")
        
            If Not oCCMaint Is Nothing Then
                oCCMaint.Init lCustkey
            End If
            
            
        End If

    End If
    
    Set oCCMaint = Nothing

    
    
End Sub

Private Sub cmdDocTrans_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDocTrans, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'*********************************************************************************
' Pull up Address or contacts form (based on control index)
'*********************************************************************************
    Dim iHooktype   As Integer
    Dim bOldmb      As Boolean
    
    Dim oChild As Object              'Address Form Object
    Dim sCustID             As String
    Dim lCustkey            As Long
    Dim lContKey            As Long
    Dim sDfltCountry        As String
    Dim iStatus             As Integer
    Dim sPhoneMask          As String

    If Not moLostFocus.IsValidDirtyCheck() Then
        Exit Sub
    End If

    bOldmb = mbDontChkClick
  
    Dim iConfirmUnload    As Integer  'Confirm Unload
    
        If moDmForm.State = kDmStateAdd Then      'No record saved yet
            If miSecurityLevel = sotaTB_DISPLAYONLY Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            If Not gbSetFocus(Me, cmdDocTrans) Then
                mbDontChkClick = bOldmb       'Dont run click event on form clear
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            iConfirmUnload = moDmForm.ConfirmUnload(True)
            If iConfirmUnload = kDmSuccess Then 'Save Was successful
                If moDmForm.State = kDmStateAdd Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARSaveBeforeContact
                Else
                    CustVendEmailConfig                     'Run CustVendEmailConfiguration
                End If
            Else
                If iConfirmUnload = kDmError Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingContacts
                Else
                    If frmSalesTax.bBadSalesTax Then
                        cmdSalesTax_Click
                    End If
                End If
            End If
        ElseIf moDmForm.State = kDmStateEdit Then  'Form is in edit
            CustVendEmailConfig                         'Run CustVendEmailConfiguration
        End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdContAddr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub


Private Sub cmdExemptions_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdExemptions, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Static bDid As Boolean
'*******************************************************************************
' Desc: Show Sales Tax Schedule Form and allow user to update (if not Deleted)
'*******************************************************************************
    If moDmForm.State = kDmStateAdd Or moDmForm.State = kDmStateEdit Then
        If Not moLostFocus.IsValidDirtyCheck() Then
            Exit Sub
        End If
        If Not bDid Then
            frmExemptions.Top = frmCustomerMaint.Top + 300
            frmExemptions.Left = frmCustomerMaint.Left
            bDid = True
        End If
        
        frmExemptions.SecurityLevel = miSecurityLevel
        
        'Re-populate #TaxCodes temptable if cleared
        If mbReloadSTaxExmpt Then
            GetSTaxExemptions
        End If
        
      'tell pop up form if current customer is in a Deleted Status
        If cboStatus.ListIndex = kItemNotSelected Then
            frmExemptions.iStatus = 0
        Else
            frmExemptions.iStatus = cboStatus.ItemData(cboStatus.ListIndex)
        End If
        
       'Setup sales tax exemptions one time per customer.
        If Not mbSetupSTax Then
            mbDontChkClick = False
            frmExemptions.Setup
            
            mbSetupSTax = True
        End If
                       
        frmExemptions.Show vbModal
          
               
        If frmExemptions.bExemptionsChanged And Not (moDmForm.IsDirty) Then
            moDmForm.SetDirty True
        End If
    End If
        
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdExcemptions_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub cmdGetMap_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    'Get mapquest map
    cmdGetMap.Address1 = Trim(txtAddressLine(1).Text)
    cmdGetMap.Address2 = Trim(txtAddressLine(2).Text) 'not used?, not sure why it's a parameter
    cmdGetMap.City = Trim(txtCity.Text)
    cmdGetMap.State = Trim(cboState.Text)
    cmdGetMap.PostalCode = Trim(txtPostalCode.Text)
    Set cmdGetMap.AppDatabase = moClass.moAppDB
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdGetMap_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLaunchEMail_Click()
'Using the ShellExecute API call launch the client's default e-mail program
'using the e-mail address from txtEMail.

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdLaunchEMail, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

    Dim sMailTo As String

    If Trim$(txtEmail.Text) = "" Then Exit Sub 'don't call if there is no e-mail address
    
    sMailTo = "mailto:" & Trim$(txtEmail.Text)

    Call ShellExecute(0&, vbNullString, sMailTo, _
      vbNullString, vbNullString, vbNormalFocus)

'+++ VB/Rig Begin Pop +++
        Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdLaunchEMail_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCreditLimit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curCreditLimit, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl curCreditLimit
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curCreditLimit_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidCreditLimit() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
' Desc:  Credit Limit Lost Focus Event
'        If user does not have security to change this field Show the
'        Security Form and change Value back if security is not overridden
' Parms: Valid         - Whether to keep focus on control
'        cyAmount      - CurrencyControl.Amount
'        AmountDisplay - Displayed Amount
'        hwnd          - windows handle of the control you are going to
'**************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim bValid      As Boolean  'Boolean Valid Flag
    Dim iHooktype   As Integer
    Dim vPrompt     As Variant
    Dim sUser       As String
    Dim sID         As String
    
    IsValidCreditLimit = 1  'Assume true
                                
    If curCreditLimit.Tag <> curCreditLimit.Amount Then  'field Was Changed
        
      'Show security event form
      'Security Event was overridden Round then Validate Currency Control
      'Check the user id typed in can override (no cancel hit)
        sID = CStr(ksecChangeCreditLimit)
        sUser = CStr(moClass.moSysSession.UserId)
        vPrompt = True
        If moClass.moFramework.GetSecurityEventPerm(sID, _
                 sUser, vPrompt) <> 0 Then
          'Round Currency Control
            curCreditLimit.Amount = gfRound((curCreditLimit.Amount), miRoundMeth, _
                                                           miDecPlaces, miPrecision)
          'Validate Currency Control
            bValid = gbIsValidCurAmt(Me, curCreditLimit, lblCreditLimit, _
                , , , tabC, kTabMain)
            IsValidCreditLimit = Abs(CInt(bValid))
        Else
            tabC.Tab = kTabMain
          'No Override reset Value
            curCreditLimit.Amount = curCreditLimit.Tag
            gbSetFocus Me, curCreditLimit
            IsValidCreditLimit = 0
        End If   'Security Event Overridden
    End If           'Click Event Called from Code
    

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An Error Occurred reset Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidCreditLimit"
    curCreditLimit.Amount = curCreditLimit.Tag
    IsValidCreditLimit = 0
    gClearSotaErr
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidCreditLimit", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub curDfltMaxUpcharge_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curDfltMaxUpcharge, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl curDfltMaxUpcharge
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curDfltMaxUpcharge_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function IsValidDfltMaxUpcharge()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sID As String
    Dim sUser As String
    Dim vVariant As Variant
    
    If CStr(curDfltMaxUpcharge.Amount) <> curDfltMaxUpcharge.Tag Then
         
         sID = "CCEDITUPCHARGE"
         sUser = CStr(moClass.moSysSession.UserId)
         vVariant = True
        
         If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vVariant) = 0 Then
             Exit Function
         End If
         

        'Force the amount positive
        curDfltMaxUpcharge.Amount = Abs(curDfltMaxUpcharge.Amount)
        
        If ddnCCUpchargeType.ItemData = 1 Then 'Percent
            If curDfltMaxUpcharge.Amount > 100 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                "Percent", "100.00"
                Exit Function
            End If
        End If
        
    End If
    
    IsValidDfltMaxUpcharge = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidDfltMaxUpcharge", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function




Private Sub curLateChgAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curLateChgAmt, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl curLateChgAmt
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curLateChgAmt_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidLateChgAmt() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
' Desc:  Finance Charge Flat Amount Lost Focus Event
'        Validate the field is in the correct range
'        and change Value back if doesn't pass validation
' Parms: Valid         - Whether to keep focus on control
'        cyAmount      - CurrencyControl.Amount
'        AmountDisplay - Displayed Amount
'        hwnd          - windows handle of the control you are going to
'**************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    IsValidLateChgAmt = 1
    
  'Round Late Charge Amount
    curLateChgAmt.Amount = gfRound((curLateChgAmt.Amount), miRoundMeth, _
                                           miDecPlaces, miPrecision)
  'Validate Amount
    IsValidLateChgAmt = Abs(CInt(gbIsValidCurAmt(Me, curLateChgAmt, lblLateChgAmt, _
                                , , , tabC, kTabDflt)))
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An Error Occured Change Value Back
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidLateChgAmt"
    curLateChgAmt.Amount = curLateChgAmt.Tag
    IsValidLateChgAmt = 0
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidLateChgAmt", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub ddnCCUpchargeType_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnCCUpchargeType, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

    Dim sID As String
    Dim sUser As String
    Dim vVariant As Variant
    
    If mbDontChkClick Then
        Exit Sub
    End If
    
    If PrevIndex <> NewIndex Then
         
         sID = "CCEDITUPCHARGE"
         sUser = CStr(moClass.moSysSession.UserId)
         vVariant = True
        
         If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vVariant) = 0 Then
            Cancel = True
            Exit Sub
         End If
    End If
         
    If ddnCCUpchargeType.ItemData = 0 Then
        lblAmount.Caption = "Amount"
    Else
        lblAmount.Caption = "Percent"
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ddnCCUpchargeType_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteUserFld_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteUserFld(Index), True
    #End If
'+++ End Customizer Code Push +++
'***********************************************************************
' Desc: dteUserFld_IsValidHwnd User Fields Valid Event
' Parms: index - Control Array index for control
'        Valid - If set to 0 then control will not lose focus
'        szmask - date mask
'        szTextdata - Sage MAS 500 Masked Edit Control. text Property
'        szTextdata - Sage MAS 500 Masked Edit Control. Masked text Property
'        hwndnewfocus - windows handle of the control you are going to
'***********************************************************************
    If Me.ActiveControl Is tbrMain Or Me.ActiveControl Is sbrMain Then  'New control going to requires Validation
        gbSetFocus Me, dteUserFld(Index)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    Else
       '(VB-09/18)Scopus#17776,Bypass validating the date user field when new blank screen is loaded via cmdFinish
        If moDmForm.State <> 0 Then
            IsValidUserfldDate Index
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "dteUserFld_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidUserfldDate(Index) As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    IsValidUserfldDate = 1
    
  'date Control was not changed
    If moUserFlds.GetOldValue(CInt(dteUserFld(Index).Tag)) <> dteUserFld(Index) Then
        'Validate date entered by user If Invalid
        'Display error message, reset Value and set control back
        If moUserFlds.Usage(CInt(dteUserFld(Index).Tag)) <> kvRequired And _
           Len(Trim(dteUserFld(Index).Text)) = 0 Then
           Exit Function
        Else
            If Not gbValidLocalDate(dteUserFld(Index).Text) Then
                tabC.Tab = kTabDflt
                Call giSotaMsgBox(Nothing, moClass.moSysSession, kmsgInvalidDate)
                dteUserFld(Index) = moUserFlds.GetOldValue(CInt(dteUserFld(Index).Tag))
                gbSetFocus Me, dteUserFld(Index)
                IsValidUserfldDate = 0
            End If
        End If
    End If
        
    moUserFlds.LetOldValue CInt(dteUserFld(Index).Tag), dteUserFld(Index).Text
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset field back
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidUserfldDate"
    dteUserFld(Index) = moUserFlds.GetOldValue(CInt(dteUserFld(Index).Tag))
    IsValidUserfldDate = 0
    gClearSotaErr
    Exit Function
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidUserfldDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'  Desc:  Setup Cancel Shutdown flag
'************************************************************************

    mbCancelShutDown = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub ClearUnboundControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'  Desc:  SClear out unbound controls
'************************************************************************
    Dim bOldmb As Boolean
    
    bOldmb = mbDontChkClick
    mbDontChkClick = True               'do not run user field click event
    
    If moDmForm.State <> kDmStateAdd And moDmForm.State <> kDmStateEdit Then
       'Customer ID Key Control
        txtCustomerID.Text = ""             'Clear Out customer id
        txtCustomerID.Protected = False
    
    
      'Sage MAS 500 Number Controls (Percents)
        numTradeDiscPct.Value = 0      'Trade Discount Percent
        numFinChgPct.Value = 0         'Finance Charge Percent
        numTradeDiscPct.Tag = numTradeDiscPct.Value    'Trade Discount Percent
        numFinChgPct.Tag = numFinChgPct.Value          'Finance Charge Percent
        nbrPriceAdjPct.Value = 0
      'National Acct options
        If mbUseNationalAcct Then
            ClearNationalAcctTab
       End If
       
      'Ship Zone Combo Box
        SetupShipZone
        moDmFormArAddr.SetColumnValue "ShipZoneKey", Empty
    End If
    
    mbDontChkClick = bOldmb               'reset dont run click event flag

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearUnboundControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ClearNationalAcctTab()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

  'Clear all controls in case the customer is not a national account user
  
    lkuNatAcct.Text = ""
    txtNatAcctDesc.Text = ""
    chkNatAcctCreditLimit.Value = vbUnchecked
    txtCurNatCreditLimit.Amount = 0
    chkNatHold.Value = vbUnchecked
    txtNatParentCustID.Text = ""
    txtNatParentCustDesc.Text = ""
    chkNatAllowParentPay.Value = vbUnchecked
    chkNatConsolidatedStat.Value = vbUnchecked
    'show the subsidiary controls
    fraNatSubsidiaryOptions.Visible = True
    lblNatParentCustomer.Visible = True
    txtNatParentCustID.Visible = True
    txtNatParentCustDesc.Visible = True
    If cboNatDfltBillTO.Enabled = True Then
        cboNatDfltBillTO.ListIndex = 2
    End If
    miNatUserLevel = kiNatNone
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCustomerMaint", "ClearNationalAcctTab", VBRIG_IS_FORM        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
' Desc: Set up form class and bind controls to fields in database
'*******************************************************************************

  'Set up default table, key etc.., Bind fields to database
  'tarCustomer Table columns
    With moDmForm
        
        .Table = "tarCustomer"
        .UniqueKey = "CustID,CompanyID"
        .SaveOrder = 3
        Set .Form = frmCustomerMaint
        Set .Database = moClass.moAppDB
        Set .Session = moClass.moSysSession
        
      'Bind columns
        
        .Bind Nothing, "CustKey", SQL_INTEGER, kDmNoInsert
        .Bind txtABANo, "ABANo", SQL_CHAR
        .Bind Nothing, "CRMCustID", SQL_CHAR
        .Bind chkAllowrefunds, "AllowCustRefund", SQL_SMALLINT
        '.Bind chkAllowInvSubs, "AllowInvtSubst", SQL_SMALLINT
        .Bind chkAllowWriteOffs, "AllowWriteOff", SQL_SMALLINT
        .BindComboBox cboBillingType, "BillingType", SQL_INTEGER, kDmUseItemData
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind curCreditLimit, "CreditLimit", SQL_FLOAT
        .Bind Nothing, "CreateType", SQL_SMALLINT 'v3
        .BindComboBox cboCredLimitAgeCat, "CreditLimitAgeCat", SQL_INTEGER, kDmUseItemData Or kDmSetNull
        .Bind Nothing, "CurrExchSchdKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CustClassKey", SQL_INTEGER
        .Bind txtSaveCustID, "CustID", SQL_CHAR
        .Bind txtCustName, "CustName", SQL_CHAR
        .Bind txtCustReference, "CustRefNo", SQL_CHAR
        .Bind txtDateEstablished, "DateEstab", SQL_DATE
        .Bind Nothing, "DfltBillToAddrKey", SQL_INTEGER
        .Bind Nothing, "DfltItemKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "DfltShipToAddrKey", SQL_INTEGER
        .Bind Nothing, "FinChgPct", SQL_FLOAT
        .Bind curLateChgAmt, "FinChgFlatAmt", SQL_FLOAT
        .Bind chkOnHold, "Hold", SQL_SMALLINT
        .Bind chkCreditLimitUsed, "CreditLimitUsed", SQL_SMALLINT
        .Bind Nothing, "PrimaryAddrKey", SQL_INTEGER
        .Bind Nothing, "PrimaryCntctKey", SQL_INTEGER
        .Bind chkDunningMsg, "PrintDunnMsg", SQL_SMALLINT
        .Bind Nothing, "ImportLogKey", SQL_INTEGER, kDmSetNull 'v3
        .Bind chkPOReq, "ReqPO", SQL_SMALLINT
        .Bind Nothing, "RetntPct", SQL_FLOAT
        .BindComboBox cboStatus, "Status", SQL_INTEGER, kDmUseItemData
        .Bind txtSic, "StdIndusCodeID", SQL_CHAR, kDmSetNull
        .BindComboBox cboStmtCycle, "StmtCycleKey", SQL_INTEGER, kDmSetNull Or kDmUseItemData
        .Bind Nothing, "StmtFormKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "TradeDiscPct", SQL_FLOAT
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind Nothing, "NationalAcctLevelKey", SQL_INTEGER, kDmSetNull
        .BindComboBox cboNatDfltBillTO, "BillToNationalAcctParent", SQL_INTEGER, kDmUseItemData
        .Bind chkNatConsolidatedStat, "ConsolidatedStatement", SQL_SMALLINT
        .Bind chkNatAllowParentPay, "PmtByNationalAcctParent", SQL_SMALLINT
        moUserFlds.BindUserField Me, moDmForm, 0, "UserFld1"
        moUserFlds.BindUserField Me, moDmForm, 1, "UserFld2"
        moUserFlds.BindUserField Me, moDmForm, 2, "UserFld3"
        moUserFlds.BindUserField Me, moDmForm, 3, "UserFld4"
        .Bind Nothing, "VendKey", SQL_INTEGER, kDmSetNull
        .BindLookup lkuSource, kDmSetNull
        .BindComboBox ddnShipPriority, "ShipPriority", SQL_SMALLINT
        .BindLookup glaAccount, kDmSetNull
        .BindLookup glaReturns, kDmSetNull
        .BindComboBox ddnCCUpchargeType, "DfltMaxUpChargeType", SQL_SMALLINT, kDmUseItemData
        .Bind curDfltMaxUpcharge, "DfltMaxUpcharge", SQL_DECIMAL
    
    
    'Define Links
    
      'Get Customer Class ID from tarCustClass
        .LinkSource "tarCustClass", "CustClassKey=<<CustClassKey>>"
        .Link txtCustClass, "CustClassID"
        
      'Get Address id for Bill To Address from tarCustAddr
        .LinkSource "tarCustAddr", "AddrKey=<<DfltBillToAddrKey>>"
        .Link txtBillTo, "CustAddrID"
      
      'Get Address id for Ship To Address from tarCustAddr
        .LinkSource "tarCustAddr", "AddrKey=<<DfltShipToAddrKey>>"
        .Link txtShipTo, "CustAddrID"

      'Get Item ID for default Item id from timItem
        .LinkSource "timItem", "ItemKey=<<DfltItemKey>>"
        .Link txtDfltItem, "ItemID"
        
      'Get GL Account Number for Default Sales Account from tglAccount
        '.LinkSource "tglAccount", "GLAcctKey=<<DfltSalesAcctKey>>"
        '.Link txtGLAccount, "GLAcctNo"
        
      'Get Business Form ID For Statement form from tciBusinessForm
        .LinkSource "tciBusinessForm", "BusinessFormKey=<<StmtFormKey>> AND BusinessFormType = 3"
        .Link txtStmtForm, "BusinessFormID"
       
      'Get Vendor ID from tapVendor
        .LinkSource "tapVendor", "VendKey=<<VendKey>>"
        .Link txtVendor, "VendID"
        
        .Init

    End With


'Common Information Address Binds
    With moDmFormCiAddr
        .Table = "tciAddress"
        .UniqueKey = "AddrKey"
        .SaveOrder = 2              'Save 1st (parent of tarCustomer)
        Set .Parent = moDmForm
        Set .Form = frmCustomerMaint
        Set .Database = moClass.moAppDB
        Set .Session = moClass.moSysSession
        .Bind txtCustName, "AddrName", SQL_CHAR
        .Bind txtAddressLine(1), "AddrLine1", SQL_CHAR
        .Bind txtAddressLine(2), "AddrLine2", SQL_CHAR
        .Bind txtAddressLine(3), "AddrLine3", SQL_CHAR
        .Bind txtAddressLine(4), "AddrLine4", SQL_CHAR
        .Bind txtAddressLine(5), "AddrLine5", SQL_CHAR
        .Bind txtCity, "City", SQL_CHAR
        .Bind txtLatitude, "Latitude", SQL_DECIMAL, kDmSetNull
        .Bind txtLongitude, "Longitude", SQL_DECIMAL, kDmSetNull
        'State and zip must be bound after country
        .BindComboBox cboCountry, "CountryID", SQL_CHAR
        .Bind txtPostalCode, "PostalCode", SQL_CHAR, kDmSetNull
        .BindComboBox cboState, "StateID", SQL_CHAR, kDmSetNull
        .ParentLink "AddrKey", "PrimaryAddrKey", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind chkResidential, "Residential", SQL_SMALLINT
        .Init
    End With

'A.R. Information Address Binds
    With moDmFormArAddr
        .Table = "tarCustAddr"
        .UniqueKey = "AddrKey"
        .SaveOrder = 4               'Save 3rd parent is customer and customer Address
        
        Set .Parent = moDmForm
        Set .Form = frmCustomerMaint
        Set .Database = moClass.moAppDB
        Set .Session = moClass.moSysSession
        
        .BindComboBox cboCommPlan, "CommPlanKey", SQL_INTEGER, kDmSetNull Or kDmUseItemData
        .Bind txtCurrency, "CurrID", SQL_CHAR, kDmSetNull
        .Bind Nothing, "CustAddrID", SQL_CHAR
        .Bind Nothing, "DfltCntctKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "FOBKey", SQL_INTEGER, kDmSetNull 'v3
        .Bind Nothing, "InvcFormKey", SQL_INTEGER, kDmSetNull
        .Bind txtInvoiceMessage, "InvcMsg", SQL_CHAR
        .BindComboBox cboLanguage, "LanguageID", SQL_INTEGER, kDmUseItemData
        .BindComboBox cboPmtTerms, "PmtTermsKey", SQL_INTEGER, kDmSetNull Or kDmUseItemData
        .BindComboBox cboSalesTerritory, "SalesTerritoryKey", SQL_INTEGER, kDmSetNull Or kDmUseItemData
        .Bind Nothing, "CurrExchSchdKey", SQL_INTEGER, kDmSetNull
        .BindComboBox ddnPriceBase, "PriceBase", SQL_SMALLINT, kDmUseItemData
        .Bind Nothing, "PriceAdj", SQL_FLOAT
        .Bind numShipDays, "ShipDays", SQL_SMALLINT
        .Bind chkShipComplete, "ShipComplete", SQL_SMALLINT
        .Bind chkPrintOrdAck, "PrintOrderAck", SQL_SMALLINT
        .Bind chkInvtSubst, "AllowInvtSubst", SQL_SMALLINT
        .Bind chkReqAck, "RequireSOAck", SQL_SMALLINT
        .Bind Nothing, "ShipMethKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "ShipZoneKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "SperKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "STaxSchdKey", SQL_INTEGER, kDmSetNull 'v3
        .Bind Nothing, "UsePromoPrice", SQL_INTEGER 'v3
        .Bind chkCloseSOOnFirst, "CloseSOOnFirstShip", SQL_SMALLINT
        .Bind chkCloseSOLineOnFirst, "CloseSOLineOnFirstShip", SQL_SMALLINT
        .Bind txtDfltCarrierAcctNo, "CarrierAcctNo", SQL_CHAR
        .BindComboBox ddnDfltCarrierBillMeth, "CarrierBillMeth", SQL_SMALLINT, kDmUseItemData
        .BindComboBox ddnFreightMethod, "FreightMethod", SQL_SMALLINT, kDmUseItemData
        .Bind chkBOLReqd, "BOLReqd", SQL_SMALLINT
        .Bind chkShipLabelsReqd, "ShipLabelsReqd", SQL_SMALLINT
        .Bind chkPackListReqd, "PackListReqd", SQL_SMALLINT
        .Bind chkPackListContentsReqd, "PackListContentsReqd", SQL_SMALLINT
        .Bind chkInvoiceReqd, "InvoiceReqd", SQL_SMALLINT
        
        .ParentLink "CustKey", "CustKey", SQL_INTEGER
        .ParentLink "AddrKey", "PrimaryAddrKey", SQL_INTEGER
        
        .BindLookup lkuAckForm, kDmSetNull
        .BindLookup lkuLabelForm, kDmSetNull
        .BindLookup lkuPackListForm, kDmSetNull
        .BindLookup lkuClosestWhse, kDmSetNull
        .BindLookup lkuPriceGroup, kDmSetNull
        .BindLookup lkuDefaultCreditCard, kDmSetNull
        
    'Define Links
      
      'Get SalesPerson ID from tarSalesperson
        .LinkSource "tarSalesperson", "SperKey=<<SperKey>>"
        .Link txtSalesperson, "SperID"
        .Link txtSperName, "SperName"
       
        .LinkSource "tciPaymentTerms", "PmtTermsKey=<<PmtTermsKey>>"
        .Link txtPmtTermsDesc, "Description"
       
      'Get Ship Method and Zones Used from tciShipMethod
        .LinkSource "tciShipMethod", "ShipMethKey=<<ShipMethKey>>"
        .Link txtShipMeth, "ShipMethID"
        .Link txtMustUseZone, "ZonesUsed"

      'Get FOB Used from tciFOB--v3
        .LinkSource "tciFOB", "FOBKey=<<FOBKey>>"
        .Link txtFOB, "FOBID"
        
       'Get Business Form ID for Invoice Form ID from tciBusinessForm
        .LinkSource "tciBusinessForm", "BusinessFormKey=<<InvcFormKey>> AND BusinessFormType = 2"
        .Link txtInvoiceForm, "BusinessFormID"
        
        .Init
    End With
   
        
'Common Information Contact Binds
    With moDmContact
        .Table = "tciContact"
        .UniqueKey = "CntctKey"
        .SaveOrder = 1              'Save 2nd (parent of tarCustomer)
        Set .Parent = moDmForm
        Set .Form = frmCustomerMaint
        Set .Database = moClass.moAppDB
        Set .Session = moClass.moSysSession
        .ParentLink "CntctKey", "PrimaryCntctKey", SQL_INTEGER
        .ParentLink "CntctOwnerKey", "CustKey", SQL_INTEGER
        .Bind Nothing, "EntityType", SQL_INTEGER
        .Bind txtContName, "Name", SQL_CHAR
        .Bind txtContTitle, "Title", SQL_CHAR
        .Bind txtPhone, "Phone", SQL_CHAR
        .Bind txtPhoneExt, "PhoneExt", SQL_CHAR
        .Bind txtFax, "Fax", SQL_CHAR
        .Bind txtFaxExt, "FaxExt", SQL_CHAR
        .Bind txtEmail, "EMailAddr", SQL_CHAR
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Init
    End With
        
        
  'Setup Sales Tax Grid to Customer tax exemptions
    With moDmSTax
        .Table = "#tarCustSTaxExmpt"
        'New schema tied to Address record
        .UniqueKey = "AddrKey, STaxCodeKey"
        Set .Form = frmExemptions 'frmSalesTax
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdExmpt
        .BindColumn "AddrKey", kColAddrKey, SQL_INTEGER
        .BindColumn "STaxCodeKey", kColTaxCode, SQL_INTEGER
        .BindColumn "ExmptNo", kColExmpt, SQL_CHAR
        .SetDefaultGridProperties
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'*******************************************************************************
'
'    DESC:  This sub loads the Default Bill To
'           drop list with the correct values '
'
'   PARMS:  none
'
'*******************************************************************************
Private Sub LoadDfltNatBillTo()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    
    If mbUseNationalAcct Then
        gComboAddItem cboNatDfltBillTO, ksNatSub, 0
        gComboAddItem cboNatDfltBillTO, ksNatParent, 1
        gComboAddItem cboNatDfltBillTO, "", 0
        
        cboNatDfltBillTO.ListIndex = 2
   End If
   
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadDfltNatBillTo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub MapControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
  'Map Search buttons
    giCollectionAdd moMapSrch, navDfltItem, txtDfltItem.hwnd         'Default Item
    giCollectionAdd moMapSrch, navBillTo, txtBillTo.hwnd             'Bill To addressID
    giCollectionAdd moMapSrch, navContact, txtContName.hwnd           'Default Contact
    giCollectionAdd moMapSrch, navCustClass, txtCustClass.hwnd       'Customer Class
    giCollectionAdd moMapSrch, navInvoiceForm, txtInvoiceForm.hwnd   'Invoice/Business Form
    giCollectionAdd moMapSrch, navSalesperson, txtSalesperson.hwnd   'Salesperson
    giCollectionAdd moMapSrch, navSic, txtSic.hwnd                   'Standard Industry Code
    giCollectionAdd moMapSrch, navShipMeth, txtShipMeth.hwnd         'Ship Via/Ship Method
    giCollectionAdd moMapSrch, navShipTo, txtShipTo.hwnd             'Ship To Address ID
    giCollectionAdd moMapSrch, navStmtForm, txtStmtForm.hwnd         'Statement/Business Form
    giCollectionAdd moMapSrch, navVendor, txtVendor.hwnd             'Vendor
    giCollectionAdd moMapSrch, navZip, txtPostalCode.hwnd            'Postal Code
    giCollectionAdd moMapSrch, navCurrency, txtCurrency.hwnd         'Currency
    giCollectionAdd moMapSrch, navFOB, txtFOB.hwnd                   'FOB--v3
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'         run generic OSTA Function Key processing
'********************************************************************

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16              'user pressed a function key
            gProcessFKeys Me, KeyCode, Shift  'run Generic Sage MAS 500 Function Keys processing
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'         runs tab key whenever the user presses enter
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter/return
            If mbEnterAsTab Then    'enter as tab is true
                gProcessSendKeys "{tab}"    'send tab key to keyboard buffer
                KeyAscii = 0        'cancel processing of enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Form_Load initializes variables to be used throughout the
'       form's user interface.  It also sets up control
'       specific properties.  For example, here you would fill a
'       combo box with static/dynamic list information.
'********************************************************************
    Dim iLoop           As Integer       'Looping Variable
    Dim sSQL            As String        'SQL String
    Dim iCRMConnected   As Integer

    Dim iAvaAddrState As Integer 'AvaTax

    Set sbrMain.Framework = moClass.moFramework
    
    mbFormLoading = True
    
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm, moDmFormArAddr, _
                                                moDmFormCiAddr, moDmContact, moDmSTax)
    miFilter = RSID_UNFILTERED
    
    For iLoop = 0 To 3
        lblUserFld(iLoop).Caption = ""
    Next iLoop

    'Set Form Height and Width.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
    miFilter = RSID_UNFILTERED

    'Get initial form/Session data ansd company Where clause.
    msCompanyID = moClass.moSysSession.CompanyID
    glCompanyID = moClass.moSysSession.CompanyID    'SGS DEJ - RKL DEJ 2017-02-15 Added global CompanyID var
    mlDfltLanguage = moClass.moSysSession.Language
    msDfltCurrency = moClass.moSysSession.CurrencyID
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    msCompanyWhere = "CompanyID = " & gsQuoted(msCompanyID) 'Company Where Clause
    mbIMActivated = moClass.moSysSession.IsModuleActivated(kModuleIM)
    
   
    'Get AR Options record.
    GetDefaults
    If mbUnload Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    With tbrMain
        .LocaleID = mlDfltLanguage

        'Hide animated Status box, date, delete button and main nabvigator.
        'Also change OK caption if called from add on the fly.
        If mlRunMode = kContextAOF Then
            .Build sotaTB_AOF
            navMain.Visible = False                 'Hide Navigator
        Else
            .Build sotaTB_MULTI_ROW
            .AddButton kTbNextNumber, 8
        
            ' Copy From
            .AddButton kTbCopyFrom, tbrMain.GetIndex(kTbMemo) + 1
            
            If Not mbAutoNum Then
                .Buttons(kTbNextNumber).Enabled = False
            End If
    
            If miSecurityLevel = sotaTB_DISPLAYONLY Then
                .Buttons(kTbNextNumber).Enabled = False
                ' Copy From
                .Buttons(kTbCopyFrom).Enabled = False
            End If

            .AddSeparator tbrMain.GetIndex(kTbHelp)
        End If
    End With

    If giGetSecurityLevel(moClass.moFramework, ktskCustAddrAOF) < sotaTB_DISPLAYONLY Then
        cmdContAddr(1).Enabled = False
    End If
    
    If giGetSecurityLevel(moClass.moFramework, ktskCIContactMNT) < sotaTB_DISPLAYONLY Then
        cmdContAddr(0).Enabled = False
    End If

    'Setup User Fields.
    moUserFlds.AllowAOF = mbAllowUserFieldValueAOF
    moUserFlds.UserFieldDisplay Me, moClass, msCompanyID, kEntTypeARCustomer

    'Setup Lost Focus validation.
    With moLostFocus
        Set .Form = frmCustomerMaint
        Set .TabControl = tabC

        .SkipForAll tbrMain, sbrMain
        .BindKey txtCustomerID, , True
        
        .SkipControls navMain
        
        .Bind txtCustClass, kTabMain, True
        .Bind txtSic, kTabMain, False
        .Bind txtVendor, kTabMain, False
        .Bind curCreditLimit, kTabMain, True
        .Bind txtPostalCode, kTabMain, False
        .Bind txtContName, kTabMain, False
        .Bind txtStmtForm, kTabDflt, False
        .Bind txtDfltItem, kTabDflt, False
        .Bind glaAccount, kTabDflt, False
        .Bind glaReturns, kTabDflt, False
        .Bind curLateChgAmt, kTabDflt, True
        .Bind numFinChgPct, kTabDflt, True
        .Bind numTradeDiscPct, kTabDflt, True
        .Bind txtBillTo, kTabBTST, True
        .Bind txtCurrency, kTabBTST, True
        .Bind txtInvoiceForm, kTabBTST, False
        .Bind txtSalesperson, kTabBTST, False
        .Bind txtShipTo, kTabBTST, True
        .Bind txtShipMeth, kTabBTST, False
        .Bind txtDateEstablished, kTabMain, True
        .Bind txtFOB, kTabBTST, False 'v3
        .Bind lkuPriceGroup, kTabBTST, True
        .Bind nbrPriceAdjPct, kTabBTST, False
        .Bind lkuAckForm, kTabSO, True
        .Bind lkuLabelForm, kTabSO, True
        .Bind lkuPackListForm, kTabSO, True
        .Bind lkuClosestWhse, kTabSO, True
        .Bind numShipDays, kTabSO, True
        .Bind lkuSource, kTabSO, True
        .Bind lkuNatAcct, kTabNatAcct, True
        .Bind lkuDefaultCreditCard, kTabBTST, True
        .Bind curDfltMaxUpcharge, kTabDflt, True
        
        .Init
    End With

    
  
    Set frmSalesTax.oClass = moClass
    Set frmSalesTax.oDm = moDmSTax
    Set frmSalesTax.oParentForm = Me
    
    frmSalesTax.bAllowAOF = mbAllowSTaxSchdAOF
    frmSalesTax.lTaxCodeCol = kColTaxCode
    frmSalesTax.lExmptCol = kColExmpt
    
    'Put Sales Tax Form in memory.
    frmSalesTax.Hide
    
    
    Set frmExemptions.oClass = moClass
    Set frmExemptions.oDm = moDmSTax
    
    frmExemptions.CreateSTaxWorkTables
     
    frmExemptions.Hide
       
    If miSecurityLevel = sotaTB_DISPLAYONLY Then
        gGridLockColumn frmExemptions.grdExemptions, kColExmpt
    Else
        gGridUnlockColumn frmExemptions.grdExemptions, kColExmpt
    End If
    
    'Disable National Acct Tab based on tarOptions flag.
    If mbUseNationalAcct Then
        tabC.TabEnabled(kTabNatAcct) = True
        gDisableControls chkNatAcctCreditLimit, chkNatHold
    Else
        tabC.TabEnabled(kTabNatAcct) = False
    End If
    
    LoadDfltNatBillTo

    'Setup Validation Class with necessary pieces.
    With moValidate
        Set .oFramework = moClass.moFramework   'Sage MAS 500 framework
        Set .oDB = moClass.moAppDB              'Database
        Set .oForm = Me                         'Form (for message Box)
        Set .oSotaObjects = moSotaObjects       'Sage MAS 500 Objects Collection (AOF)
        Set .oDmForm = moDmForm                 'data Manager Object
        Set .oTabCtl = tabC                     'Customer Tab control

        .bSetFocusBack = True                   'SetFocus Back on error flag
        .bShowMessage = True                    'Show messages
        .bResetValue = True                     'Change Value back Flag
        .sCompanyID = msCompanyID               'CompanyID
    End With

    'Setup and bind righ click context menus to controls.
    BindContextMenu

    'Bind the controls to the database and map the controls to navigators.
    Dim lTick As Long
    lTick = GetTickCount()

    BindForm
    MapControls
    
    'Initialize GL Account control.
    With glaAccount
        .Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu, msCompanyID
        .RestrictClause = "AcctCatID <> " & Format$(kvGLAcctCatIDNonFinancial)
    End With
    
    With glaReturns
         .Init moClass.moFramework, moClass.moAppDB, moClass.moAppDB, moContextMenu
         .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
    End With
 
    'Initialize lookup controls.
    Set lkuAckForm.Framework = moClass.moFramework
    Set lkuAckForm.SysDB = moClass.moAppDB
    Set lkuAckForm.AppDatabase = moClass.moAppDB
    
    With lkuAckForm
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID) & " AND BusinessFormType = " & 8
        .Text = ""
        .Tag = ""
    End With
 
    Set lkuLabelForm.Framework = moClass.moFramework
    Set lkuLabelForm.SysDB = moClass.moAppDB
    Set lkuLabelForm.AppDatabase = moClass.moAppDB

    With lkuLabelForm
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID) & " AND BusinessFormType = " & 17
        .Text = ""
        .Tag = ""
    End With
 
    Set lkuPackListForm.Framework = moClass.moFramework
    Set lkuPackListForm.SysDB = moClass.moAppDB
    Set lkuPackListForm.AppDatabase = moClass.moAppDB
    
    With lkuPackListForm
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID) & " AND BusinessFormType = " & 15
        .Text = ""
        .Tag = ""
    End With
 
    Set lkuClosestWhse.Framework = moClass.moFramework
    Set lkuClosestWhse.SysDB = moClass.moAppDB
    Set lkuClosestWhse.AppDatabase = moClass.moAppDB
    
    With lkuClosestWhse
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID) & " AND Transit = " & 0
        .Text = ""
        .Tag = ""
    End With
 
    Set lkuPriceGroup.Framework = moClass.moFramework
    Set lkuPriceGroup.SysDB = moClass.moAppDB
    Set lkuPriceGroup.AppDatabase = moClass.moAppDB
    
    With lkuPriceGroup
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID)
        .Text = ""
        .Tag = ""
    End With
 
    Set lkuSource.Framework = moClass.moFramework
    Set lkuSource.SysDB = moClass.moAppDB
    Set lkuSource.AppDatabase = moClass.moAppDB
    
    With lkuSource
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID)
        .Text = ""
        .Tag = ""
    End With
 
    Set lkuNatAcct.Framework = moClass.moFramework
    Set lkuNatAcct.SysDB = moClass.moAppDB
    Set lkuNatAcct.AppDatabase = moClass.moAppDB
    
    With lkuNatAcct
        .RestrictClause = "CompanyID =" & gsQuoted(msCompanyID)
        .Text = ""
        .Tag = ""
    End With
    
    
    Set lkuDefaultCreditCard.Framework = moClass.moFramework
    Set lkuDefaultCreditCard.SysDB = moClass.moAppDB
    Set lkuDefaultCreditCard.AppDatabase = moClass.moAppDB
    
 
    With lkuDefaultCreditCard
        .RestrictClause = "CustKey =" & gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
        .Text = ""
        .Tag = ""
    End With

    'Credit Card Upcharge type
    ddnCCUpchargeType.InitStaticList moClass.moAppDB, "tarCustomer", "DfltMaxUpChargeType", moClass.moSysSession.Language
 
'    Initialize PriceBase Combobox.
    ddnPriceBase.InitStaticList moClass.moAppDB, "tarCustAddr", "PriceBase", moClass.moSysSession.Language
 
    numShipDays.DecimalPlaces = 0
 
    'Setup combo box for ship Zone - set uses zones to false.
    SetupShipZone
    txtMustUseZone.Text = 0
   
    'This will hide/disable those controls which need to be based on the above.
    SetupShipPriority
    AreModulesActive
    msCompanyWhere = "CompanyID = " & gsQuoted(msCompanyID)
                
    'Get defaults and Load Billing Type and Status Static Lists.
    SetupBillingTypeCombo
    miDfltStatus = giFillStaticList(moClass.moAppDB, moClass.moSysSession, cboStatus, "tarCustomer", "Status")
    
    'Load dynamic lists - Combo Boxes
    'Payment Terms
    gRefreshCIPaymentTerms moClass.moAppDB, moClass.moSysSession, cboPmtTerms, _
                             mbAllowPmtTermsAOF, False, msCompanyID
    
    'If use Salesperson and Commissions is Load Commission Class.
    If muCustDflts.UseSper Then
        'Commission Class
        gRefreshARCommPlan moClass.moAppDB, moClass.moSysSession, cboCommPlan, _
                               mbAllowSTaxSchdAOF, False, msCompanyID
    End If
    
    'If print Statements is on Load Statement Cycle.
    If muCustDflts.PrintStmts Then
        'Statement Cycle
        gRefreshCIProcCycle moClass.moAppDB, moClass.moSysSession, cboStmtCycle, _
                                   mbAllowProcCycleAOF, False, msCompanyID
    End If
    
    'Sales Territory
    gRefreshARSalesTerritory moClass.moAppDB, moClass.moSysSession, cboSalesTerritory, _
                             mbAllowSalesterritoryAOF, False, msCompanyID
    
    'Language
    gRefreshCILanguage moClass.moAppDB, moClass.moSysSession, cboLanguage, True
    
    'Country
    gRefreshCICountry moClass.moAppDB, moClass.moSysSession, cboCountry, _
                        mbAllowCountryAOF, False
                        
    'Set up chkCloseSOOnFirst, chkcloseSOLineOnFirst.
    SetChkCloseSOFirst
    
    ddnDfltCarrierBillMeth.InitStaticList moClass.moAppDB, "tarCustAddr", "CarrierBillMeth", mlDfltLanguage
    ddnFreightMethod.InitStaticList moClass.moAppDB, "tarCustAddr", "FreightMethod", mlDfltLanguage
    ddnDfltCarrierBillMeth.SetToDefault True
    ddnFreightMethod.SetToDefault True
    If Not bExtShipSystemON Then
        gDisableControls txtDfltCarrierAcctNo, ddnDfltCarrierBillMeth
    End If
    
    ' Bind the Copy Manager to the data manager and establish questions
    BindCopyMgr
    
    'AvaTax Integration - FormLoad
    'Show and Enable cmdAvaAddr button.
    'Show based on AvaTax intall (tavConfiguration table was found)
    'Enable based on the AvaTax Address Option (tavConfiguration.AvaAddrEnabled) for this company.

    'AvaTax button load picture in case we enable,  default to hidden and disabled
    cmdAvaAddr.Picture = gLoadResPicture(Avalara)
    cmdAvaAddr.Visible = False
    cmdAvaAddr.Enabled = False

    mbAvaAddrEnabled = False

    On Error Resume Next 'AvaTax not installed will result in SQL 4050 error

    iAvaAddrState = 0 'Default to 0 - No AvaTax installation
    '+ 1 - Adding 1 to lookup return to diferenciate lookup return below; if table missing we will not get a value back
    iAvaAddrState = giGetValidInt(moClass.moAppDB.Lookup("AvaAddrEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(msCompanyID)) + 1)

    On Error GoTo VBRigErrorRoutine

     Select Case iAvaAddrState
        Case 0
            'Lookup failed due to missing table tavConfiguration, means AvaTax not installed.
            'cmdAvaAddr should remain hidden

        Case 1
            'AvaTax is installed (lookup successful) and Avalera Address option is disabled - tavConfiguration.AvaAddrEnabled = False
            'Display cmdAvaAddr , but since option is off we leave disabled.
            cmdAvaAddr.Visible = True

        Case 2
            'AvaTax is installed (lookup successful) and Avalera Address option is enabled - tavConfiguration.AvaAddrEnabled = True
            'Display cmdAvaAddr, and enable for use if addr not read-only.
            cmdAvaAddr.Visible = True
            mbAvaAddrEnabled = True

    End Select
    'AvaTax end

    tabC.Tab = kTabMain
    mbFormLoading = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub glaAccount_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moLostFocus.IsValidControl glaAccount
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaAccount_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub glaAccount_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "glaAccount_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub




Private Sub glaReturns_LookupClick(bCancel As Boolean)
 'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

End Sub

Private Sub glaReturns_LostFocus()
    moLostFocus.IsValidControl glaAccount
End Sub





Private Sub lkuAckForm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuAckForm, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuAckForm
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuAckForm_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub




Private Sub lkuAckForm_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuAckForm, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuAckForm_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuClosestWhse, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuClosestWhse
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuClosestWhse_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuClosestWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuClosestWhse_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuDefaultCreditCard, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'******************************************************************************
' Desc: Setup Lookup and Show Search window
'******************************************************************************
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, lkuDefaultCreditCard) Then
            Exit Sub
        End If
    End If
    With lkuDefaultCreditCard
        .RestrictClause = "CustKey =" & gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
        '.Text = ""
        '.Tag = ""
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuDefaultCreditCard, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuDefaultCreditCard
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLabelForm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuLabelForm, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuLabelForm
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuLabelForm_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub lkuLabelForm_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuLabelForm, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuLabelForm_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub lkuNatAcct_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuNatAcct, True
    #End If
'+++ End Customizer Code Push +++

    moLostFocus.IsValidControl lkuNatAcct

End Sub
Private Sub lkuPackListForm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPackListForm, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuPackListForm
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPackListForm_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub lkuPackListForm_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPackListForm, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPackListForm_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceGroup_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPriceGroup, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuPriceGroup
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPriceGroup_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuPriceGroup_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPriceGroup, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuPriceGroup_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSource, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl lkuSource
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSource_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuSource, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
'*****************************************************************************
' Desc: Checks if a valid key has been entered and if not cancels the click event
'*****************************************************************************
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State <> kDmStateEdit _
    And moDmForm.State <> kDmStateAdd) Then
        bCancel = True
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lkuSource_LookupClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub m_CopyMgr_GetNextNumber(NextNumber As String, Cancel As Boolean, ErrNo As Long, ErrData As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: The Copy Manager has requested the next customer number. So, reuse
'       logic already established in this module to return the next number
'*****************************************************************************

    GetNextCustNo False, NextNumber, ErrNo, ErrData

    ' Set the cancel flag if the next number was not generated
    If (Len(Trim(NextNumber)) = 0) Then
        Cancel = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "m_CopyMgr_GetNextNumber", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub m_CopyMgr_ManualChanges(Cancel As Boolean, ErrNo As Long, ErrData As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: The Copy Manager has requested the module to make any changes necessary
'       for the copy to be successful. Use logic already established in this module
'*****************************************************************************

    Dim CustID              As String
    Dim NatAcctQuestion     As CheckBoxConstants
    Dim AddressQuestion     As CheckBoxConstants
    Dim SalesTaxRow         As Long
    
    ' Get the entered customer id from the Copy Manager
    CustID = m_CopyMgr.ID(0)
    
    ' Get the answers to the Copy questions
    AddressQuestion = m_CopyMgr.Value(1)
    If (mbUseNationalAcct) Then
        NatAcctQuestion = m_CopyMgr.Value(2)
    Else
        NatAcctQuestion = vbUnchecked
    End If

    ' Perform surrogate key logic here
    LoadCustDefaults True
    
    ' Perform exclusions per design
    txtCustName.Text = ""
    txtCustReference.Text = ""
    txtABANo.Text = ""
    txtVendor.Text = ""
    moDmForm.SetColumnValue "VendKey", Empty

    For SalesTaxRow = 1 To moDmSTax.Grid.DataRowCnt
        gGridUpdateCell frmExemptions.grdExemptions, SalesTaxRow, kColExmpt, ""
        moDmSTax.SetCellValue SalesTaxRow, kColExmpt, SQL_CHAR, ""
    Next SalesTaxRow

    
    ' Perform assignments per design
    txtDateEstablished.Text = Format(moClass.moSysSession.BusinessDate, _
                                                gsGetLocalVBDateMask())
    txtDateEstablished.Tag = txtDateEstablished.Text  'set Tag
    
    ' Perform exclusion questions
    If (AddressQuestion = vbUnchecked) Then
        ' Do not copy addresses or contacts, so blank them
        ' Primary
        txtAddressLine(1).Text = ""
        txtAddressLine(2).Text = ""
        txtAddressLine(3).Text = ""
        txtAddressLine(4).Text = ""
        txtAddressLine(5).Text = ""
        txtCity.Text = ""
        txtLatitude.Text = ""
        txtLongitude.Text = ""
        cboCountry.ListIndex = kItemNotSelected
        txtPostalCode.Text = ""
        cboState.ListIndex = kItemNotSelected
        chkResidential.Value = vbUnchecked
        
        ' Contacts
        txtContName.Text = ""
        txtContTitle.Text = ""
        txtPhone.Text = ""
        txtPhoneExt.Text = ""
        txtFax.Text = ""
        txtFaxExt.Text = ""
        txtEmail.Text = ""
    End If
    
    If (NatAcctQuestion = vbUnchecked) Then
        moDmForm.SetColumnValue "NationalAcctLevelKey", Empty
        ClearNationalAcctTab
    Else
        'Reinitialize the national account so that it differs from the text control and the customer will be processed
        'correctly as a new member in DMPreSave and DMPostSAve
        mlNatAcctKey = 0
    End If
    
    ' Perform misc here
    
    ' Bill/Ship to
    txtBillTo.Text = CustID
    txtShipTo.Text = CustID
    
    ' Since bound control is not display control...
    txtCustomerID.Text = txtSaveCustID.Text
    
    ' Update counters
    On Error Resume Next
    ClearDMRowLoaded moDmForm
    ClearDMRowLoaded moDmFormCiAddr
    ClearDMRowLoaded moDmFormArAddr
    ClearDMRowLoaded moDmContact
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
    moDmForm.SetColumnValue "UpdateCounter", 0
    moDmFormCiAddr.SetColumnValue "UpdateCounter", 0
    moDmContact.SetColumnValue "UpdateCounter", 0

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "m_CopyMgr_ManualChanges", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub m_CopyMgr_ValidateNewID(Cancel As Boolean, ErrNo As Long, ErrData As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: The Copy Manager has requested the customer to be validated. So, reuse
'       logic already established in this module
'*****************************************************************************

    Dim CustID  As String
    
    ' Get the entered customer id from the Copy Manager
    CustID = m_CopyMgr.ID(0)
    
    ' Validate the entered customer, must not be on file
    If (IsValidCustomer(CustID, ErrNo, ErrData) = kDmFailure) Then
        ' Set the cancel flag if the customer id was invalid
        Cancel = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "m_CopyMgr_ValidateNewID", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ClearDMRowLoaded(ByVal dm As clsDmForm)
    Dim oChild As Object
    For Each oChild In dm.Child
        If (TypeName(oChild) = "clsDmForm") Then
            oChild.RowLoaded = False
            If (oChild.Child.Count > 0) Then
                ClearDMRowLoaded oChild
            End If
        End If
    Next
End Sub

Private Sub navFOB_Click() 'v3
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
   'Only Run Code if a valid key has been entered
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navFOB) Then
            Exit Sub
        End If
      
        'Setup Navigator if not already setup.
        If Not bDid Then
            bDid = True
            gbLookupInit navFOB, moClass, moClass.moAppDB, "FOB", msCompanyWhere
            navFOB.ColumnMasks = ""
        End If

        gcLookupClick Me, navFOB, txtFOB, "FOBID"   'Show search window
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navFOB_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navBillTo_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************************
' Desc: Setup Navigator and Show Search window
'******************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
    Dim sWhere As String
    
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navBillTo) Then
            Exit Sub
        End If

        sWhere = "CustKey = " & moDmForm.GetColumnValue("CustKey")
      
        'Check if Navigator was already setup then set it up.
        If Not bDid Then
            bDid = True
            gbLookupInit navBillTo, moClass, moClass.moAppDB, "CustomerAddressBilling", ""
            navBillTo.ColumnMasks = ""
        End If
   
        'Create restriction for current customer.
        navBillTo.RestrictClause = sWhere
        gcLookupClick Me, navBillTo, txtBillTo, "CustAddrID"
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navBillTo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navContact_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
    Dim sCWhere As String       'Where clause
    Dim iConfirmUnload    As Integer
    
    
    If moDmForm.State = kDmStateAdd Then
        txtContName.Text = txtContName.Tag
        iConfirmUnload = moDmForm.ConfirmUnload(True)
        If iConfirmUnload = kDmSuccess Then
            If moDmForm.State = kDmStateAdd Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgARSaveBeforePrimCntChange
                Exit Sub
            End If
        Else
            If iConfirmUnload = kDmFailure Then Exit Sub
        End If
    End If
    
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navContact) Then
            Exit Sub
        End If

        sCWhere = "CntctOwnerKey = " & moDmForm.GetColumnValue("CustKey")
        sCWhere = sCWhere & "AND EntityType = " & kEntTypeARCustomer
     
        'Setup navigator if First Time Clicked.
        If Not bDid Then
            bDid = True
            gbLookupInit navContact, moClass, moClass.moAppDB, "Contact", sCWhere
            navContact.ColumnMasks = ""
        End If
        
        navContact.RestrictClause = sCWhere
        gcLookupClick Me, navContact, txtContName, "Name"    'Show Search Window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navContact_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navCurrency_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navCurrency) Then
            Exit Sub
        End If
    
        'Setup navigator if First Time Clicked.
        If Not bDid Then
            bDid = True
            gbLookupInit navCurrency, moClass, moClass.moAppDB, "Currency", "IsUsed = 1"
            navCurrency.ColumnMasks = ""
        End If
        
        gcLookupClick Me, navCurrency, txtCurrency, "CurrID"   'Show search window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navCurrency_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub navDfltItem_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Dim sWhere  As String

    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If moDmForm.State = kDmStateEdit Or moDmForm.State = kDmStateAdd Then
        If Not gbSetFocus(Me, navDfltItem) Then
            Exit Sub
        End If

        'Setup navigator if First Time Clicked (No Expense Items).
        If Not bDid Then
            bDid = True
            sWhere = "ItemType <> 3 AND " & msCompanyWhere
            gbLookupInit navDfltItem, moClass, moClass.moAppDB, "Item", sWhere
        End If

        gcLookupClick Me, navDfltItem, txtDfltItem, "ItemID"   'Show search window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navDfltItem_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navCustClass_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navCustClass) Then
            Exit Sub
        End If

        'Setup navigator if First Time Clicked.
        If Not bDid Then
            bDid = True
            gbLookupInit navCustClass, moClass, moClass.moAppDB, "ARCustClass", msCompanyWhere
            navCustClass.ColumnMasks = ""
        End If

        gcLookupClick Me, navCustClass, txtCustClass, "CustClassID"   'Show Cust Class search Window
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navCustClass_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrPriceAdjPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrPriceAdjPct, True
    #End If
'+++ End Customizer Code Push +++

    If Not mbFormLoading Then
        moDmFormArAddr.SetColumnValue "PriceAdj", gdGetValidDbl(nbrPriceAdjPct.Value) / 100
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "nbrPriceAdjPct_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrPriceAdjPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrPriceAdjPct, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl nbrPriceAdjPct
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "nbrPriceAdjPct_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShipDays_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numShipDays, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl numShipDays
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numShipDays_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtFOB_LostFocus() 'v3
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtFOB, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtFOB
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtFOB_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navInvoiceForm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navInvoiceForm) Then
            Exit Sub
        End If

        'Setup Navigator if not already setup.
        If Not bDid Then
            bDid = True
            gbLookupInit navInvoiceForm, moClass, moClass.moAppDB, "BusinessForm", msCompanyWhere & " AND BusinessFormType = 2"
            navInvoiceForm.ColumnMasks = ""
        End If

        gcLookupClick Me, navInvoiceForm, txtInvoiceForm, "BusinessFormID"    'Show search window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navInvoiceForm_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navSalesperson_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navSalesperson) Then
            Exit Sub
        End If

        'Setup Navigator if not already setup.
        If Not bDid Then
            bDid = True
            gbLookupInit navSalesperson, moClass, moClass.moAppDB, "Salesperson", msCompanyWhere & " AND Status = 1"
            navSalesperson.ColumnMasks = ""
        End If

        gcLookupClick Me, navSalesperson, txtSalesperson, "SperID"   'Show search window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navSalesperson_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navShipMeth_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navShipMeth) Then
            Exit Sub
        End If

        'Setup Navigator if not already setup.
        If Not bDid Then
            bDid = True
            gbLookupInit navShipMeth, moClass, moClass.moAppDB, "ShipMethod", msCompanyWhere
            navShipMeth.ColumnMasks = ""
        End If

        gcLookupClick Me, navShipMeth, txtShipMeth, "ShipMethID"   'Show search window
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navShipMeth_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navShipTo_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************************
' Desc: Setup Navigator and Show Search window
'******************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
    Dim sWhere As String
    
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navShipTo) Then
            Exit Sub
        End If

        sWhere = "CustKey = " & moDmForm.GetColumnValue("CustKey")
        
        'Check if Navigator was already setup then set it up.
        If Not bDid Then
            bDid = True
            gbLookupInit navShipTo, moClass, moClass.moAppDB, "CustomerAddress", ""
            navShipTo.ColumnMasks = ""
        End If
   
        'Create restriction for current customer.
        navShipTo.RestrictClause = sWhere
        gcLookupClick Me, navShipTo, txtShipTo, "CustAddrID"
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navShipTo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navSic_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************************
' Desc: Setup Navigator and Show Search window
'******************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
    
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navSic) Then
            Exit Sub
        End If
  
        'Check if Navigator was already setup then set it up.
        If Not bDid Then
            bDid = True
            gbLookupInit navSic, moClass, moClass.moAppDB, "StdIndustryCode", ""
            navSic.ColumnMasks = ""
        End If

        gcLookupClick Me, navSic, txtSic, "StdIndusCodeID"  'Show Search Window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navSic_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navStmtForm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup Navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navStmtForm) Then
            Exit Sub
        End If

        'Setup Navigator if not already setup.
        If Not bDid Then
            bDid = True
            gbLookupInit navStmtForm, moClass, moClass.moAppDB, "BusinessForm", msCompanyWhere & " AND BusinessFormType = 3"
            navStmtForm.ColumnMasks = ""
        End If
    
        gcLookupClick Me, navStmtForm, txtStmtForm, "BusinessFormID"    'Show search window
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navStmtForm_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navVendor_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid As Boolean      'Already Setup navigator Flag
   
    'Only Run Code if a valid key has been entered.
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        If Not gbSetFocus(Me, navVendor) Then
            Exit Sub
        End If

        'Setup navigator if not already setup.
        If Not bDid Then
            bDid = True
            gbLookupInit navVendor, moClass, moClass.moAppDB, "Vendor", msCompanyWhere
            navVendor.ColumnMasks = ""
        End If

        gcLookupClick Me, navVendor, txtVendor, "VendID"  'Show Search Window
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navVendor_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navZip_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc: Setup navigator if it has not been setup and show search window
'*****************************************************************************
    Static bDid     As Boolean      'Already Setup navigator Flag
    Dim sWhere      As String
   
    'Only Run Code if a valid key has been entered.
    If moDmForm.State = kDmStateEdit Or moDmForm.State = kDmStateAdd Then
        If Not gbSetFocus(Me, navZip) Then Exit Sub
        If cboCountry.ListIndex = kItemNotSelected Then
            sWhere = ""   'Clear out restrict clause
        Else
            sWhere = "CountryID = " & gsQuoted(cboCountry.Text)
        End If
  
        'Setup navigator if First Time Clicked.
        If Not bDid Then
            bDid = True
            gbLookupInit navZip, moClass, moClass.moAppDB, "Postal", sWhere
        End If
        
        navZip.RestrictClause = sWhere
  
        gvLookupClick Me, navZip, txtPostalCode  'Show Search Window
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navZip_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
'***********************************************************************
'Desc: If the data has changed then Prompt to save changes
'Parms: Cancel if you set this to true you will not unload the form
'       UnloadMode - WHere unload was called from (code, cancel box etc...)
'***********************************************************************
    
    Dim iConfirmUnload  As Integer
    Dim bOldmb          As Boolean
    
    frmSalesTax.bBadSalesTax = False
   
   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
      'Set Focus to force current Control Lost Focus Event
        If mlRunMode = kContextAOF Then
            If UnloadMode <> vbFormCode Then
                If moDmForm.State = kDmStateAdd Or moDmForm.State = kDmStateEdit Then
                    If moLostFocus.IsValidDirtyCheck = 0 Then
                        GoTo CancelShutDown
                    ElseIf IsValidUFCheck = 0 Then
                        GoTo CancelShutDown
                    End If
                End If
            End If
        Else
            If moDmForm.State = kDmStateAdd Or moDmForm.State = kDmStateEdit Then
                If moLostFocus.IsValidDirtyCheck = 0 Then
                    GoTo CancelShutDown
                ElseIf IsValidUFCheck = 0 Then
                    GoTo CancelShutDown
                End If
            End If
        End If
        
         
      'Unload the form save or not if dirty based on user response
       If mlRunMode = kContextAOF Then
            If UnloadMode = vbFormCode Then
               iConfirmUnload = kDmSuccess
            Else
                bOldmb = mbDontChkClick
                mbDontChkClick = True                       'Global dont check click event flag
                iConfirmUnload = moDmForm.ConfirmUnload()   'Confirm save/cancel if dirty
                ClearUnboundControls
                mbDontChkClick = bOldmb                      'Global dont check click event flag
            End If
        Else
            bOldmb = mbDontChkClick
            mbDontChkClick = True                         'Global dont check click event flag
            iConfirmUnload = moDmForm.ConfirmUnload(True) 'Confirm save/cancel if dirty
            mbDontChkClick = bOldmb                      'Global dont check click event flag
        End If
        
        Select Case iConfirmUnload
            Case kDmSuccess     'succesful unload (may not have been dirty)
               If mlRunMode = kContextAOF And _
                  UnloadMode <> vbFormCode Then
                    bOldmb = mbDontChkClick             'Global dont check click event flag
                    mbDontChkClick = True               'Click Event Called from form code
                    SetupShipZone                       'Clear Out Ship Zones
                    moDmForm.Clear True                 'Clear Out Form
                    SetCountryMasks msDfltCountry       'Clear Country masks
                    gbSetFocus Me, txtCustomerID        'Set Focus on the customer id
                    tabC.Tab = kTabMain                 'Show 1st Tab
                    mbDontChkClick = bOldmb              'Click Event Called from form code
                End If
            Case kDmFailure     'dm process failed
                If frmSalesTax.bBadSalesTax Then
                    cmdSalesTax_Click
                End If
                GoTo CancelShutDown
            
            Case kDmError       'An error occured
                GoTo CancelShutDown
                'If you need data Manager Error Value then you would
                'dimension a variable, such as lError as Long and assign
                'to the DataManager Error property.
                'lError = moDmForm.Error
            
            Case Else
                'Unexcpected return code from confirm unload
                 giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                            iConfirmUnload
        
        End Select
    
      'Check all other forms  that may have been loaded from this main form.
      'If there are any Visible forms, then this means the form is Active.
      'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
      'Where was the unload called from
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            Case Else
           'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If
    
    
  'If execution gets to this point, the form and class object of the form
  'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester   'perfor Sage MAS 500 Shutdown
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else 'kframeworkShutDown
            'Do nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

'Cancel the shutdown
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
'***********************************************************************
'Desc: If the data has changed then Prompt to save changes
'Parms: Cancel if you set this to true you will not unload the form
'***********************************************************************
On Error Resume Next
    
    Set moLastControl = Nothing
  
  'Clear up objects created in this form
    Set moSotaObjects = Nothing 'Sage MAS 500 Objects
    
  'Context Menus
    Set moContextMenu.Hook = Nothing
    Set moContextMenu = Nothing

  'Clear up customer table data manager object
    If (Not (IsEmpty(moDmForm))) Then
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If
    
  'Clear up copy manager object
    If (Not (IsEmpty(m_CopyMgr))) Then
        m_CopyMgr.UnloadSelf
        Set m_CopyMgr = Nothing
    End If
    
  'Clear up customer Address table data manager object
    If (Not (IsEmpty(moDmFormArAddr))) Then
        moDmFormArAddr.UnloadSelf
        Set moDmFormArAddr = Nothing
    End If
    
  'Clear up generic Address table data manager object
    If (Not (IsEmpty(moDmFormCiAddr))) Then
        moDmFormCiAddr.UnloadSelf
        Set moDmFormCiAddr = Nothing
    End If
    
    If (Not (IsEmpty(moDmContact))) Then
        moDmContact.UnloadSelf
        Set moDmContact = Nothing
    End If
  
  'Clear up Sales tax exemption table data manager object
    If (Not (IsEmpty(moDmSTax))) Then
        moDmSTax.UnloadSelf
        Set moDmSTax = Nothing
    End If
    'Debug.Print "DmSTAx"
    
    Set moUserFlds = Nothing    'Clean up user fields
    
    If Not (moValidate Is Nothing) Then
        Set moValidate.oDB = Nothing
        Set moValidate.oRS = Nothing
        Set moValidate.oFramework = Nothing
        Set moValidate.oForm = Nothing
        Set moValidate.oSotaObjects = Nothing       'Sage MAS 500 Objects Collection (AOF)
        Set moValidate.Hook = Nothing       'Sage MAS 500 Objects Collection (AOF)
    End If
    Set moValidate = Nothing    'Clean up Validation Class
    
    If Not (moLostFocus Is Nothing) Then
        moLostFocus.UnloadSelf
        Set moLostFocus = Nothing    'Clean up Active Modules Class
    End If
    
    
  'Unload Sales Tax form
    Set frmSalesTax.oClass = Nothing
    Set frmSalesTax.oDm = Nothing
    Set frmSalesTax.oParentForm = Nothing
    Unload frmSalesTax           'Unload Sales Tax Form
    Set frmSalesTax = Nothing    'Set Sales Tax Form to Nothing
    
    Set frmExemptions.oClass = Nothing
    Set frmExemptions.oDm = Nothing
  
 
    Set moMapSrch = Nothing
    Set moMapMemo = Nothing
 
    Set moClass = Nothing       'Clean up Parent Class
 
 Set frmCustomerMaint = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
 Exit Sub
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "Form_Unload"
'Debug.Print "Error"
gClearSotaErr
Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navMain_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Main navigator Have User Save Changes if changes were made
'       if User Made No Changes, Answered Yes and Save was Successful
'       or answered no then Pull Up the Customer navigator Window
'***********************************************************************
    Dim bOldmb As Boolean
    Dim iConfirmUnload  As Integer  'Confirm Unload Check Value
    Static bDid         As Boolean  'navigator was already Setup
    Dim vParseRet       As Variant
    Dim sOldCustID      As String
    
    bSetupNavMain
    
    If moDmForm.State <> kDmStateNone Then
        If moLostFocus.IsValidDirtyCheck = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        ElseIf IsValidUFCheck = 0 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    
    frmSalesTax.bBadSalesTax = False

  'Ask User if they want to save changes and run save if Answer = Yes
    bOldmb = mbDontChkClick
    mbDontChkClick = True
    iConfirmUnload = moDmForm.ConfirmUnload(True)
    mbDontChkClick = bOldmb
    
    Select Case iConfirmUnload   'Check Value of Confirm Unload
        Case kDmSuccess
           'User Made No Changes,
           'Answered Yes and Save was Successful or
           'Answered no
        Case kDmFailure
            'Customer Pressed cancel
            'Or something Failed in data Manager
            If frmSalesTax.bBadSalesTax Then
                cmdSalesTax_Click
            End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        Case kDmError
            'An Error OccCurred
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        Case Else
           'UnKnown return Value
           giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
    End Select

    If Not bDid Then
        bDid = True
    End If
    
    Dim bEnabled As Boolean
    
    txtSaveCustID.Enabled = False
    sOldCustID = txtSaveCustID.Text
    
    txtSaveCustID.Text = txtCustomerID.Text
    
    bEnabled = txtCustomerID.Enabled
    gcLookupClick Me, navMain, txtCustomerID, "CustID"
    txtSaveCustID.Text = txtCustomerID.Text
    If navMain.ReturnColumnValues.Count > 0 And (Not bEnabled) Then
        If sOldCustID <> Trim(navMain.ReturnColumnValues("CustID")) Then
            LoadMaskedCustNo                     'Load Unbound control
            IsValidCustomer                      'validate Customer Id
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navMain_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub numFinChgPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numFinChgPct, True
    #End If
'+++ End Customizer Code Push +++
'*********************************************************************************
' Desc: Load Value from Sage MAS 500 Number Control Into data Manager Column
'       Divided By 100 (FinChgPct)
'*********************************************************************************
    
        
  'Format as .0000
    If Not mbFormLoading Then
        moDmForm.SetColumnValue "FinChgPct", Format(numFinChgPct.Value / 100, ".0000")
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numFinChgPct_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidFinChgPct() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
' Desc:  Finance Charge Percent Lost Focus Event
'        Validate that field is in a Valid Range
' Parms: Valid - If set to 0 then control will not lose focus
'        szmask - mask
'        szTextdata - Sage MAS 500 Number Control. text Property
'        szTextdata - Sage MAS 500 Number Control. Masked text Property
'        hwndnewfocus - windows handle of the control you are going to
'***********************************************************************
On Error GoTo ExpectedErrorRoutine
    IsValidFinChgPct = 1
    
    If numFinChgPct.Value > 100 Then   'Number Too Large
        tabC.Tab = kTabDflt
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
            sRemoveAmpersand(lblFinChgPct), "100.00"   'Send Error Message
        numFinChgPct.Value = numFinChgPct.Tag     'Reset Value to old Value
        gbSetFocus Me, numFinChgPct                         'Keep Cursor in this control
        IsValidFinChgPct = 0
    
    ElseIf numFinChgPct.Value < 0 Then   'Negative Value
        tabC.Tab = kTabDflt
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
            sRemoveAmpersand(lblFinChgPct)              'Send Error message
        numFinChgPct.Value = numFinChgPct.Tag      'Reset Value To Old Value
        gbSetFocus Me, numFinChgPct                          'Keep Cursor in this control
        IsValidFinChgPct = 0
    
    Else
        numFinChgPct.Tag = numFinChgPct.Value  'Valid entry reset tag with New Value
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset validating Flag and Value back to old Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidFinChgPct"
    numFinChgPct.Value = numFinChgPct.Tag
    IsValidFinChgPct = 0
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidFinChgPct", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub numFinChgPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numFinChgPct, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl numFinChgPct
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numFinChgPct_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++
'*********************************************************************************
' Desc: Load Value from Sage MAS 500 Number Control Into data Manager Column
'       Divided by 100 (TradeDiscPct)
'*********************************************************************************
    'Format as .0000
    If Not mbFormLoading Then
        moDmForm.SetColumnValue "TradeDiscPct", Format(numTradeDiscPct.Value / 100, ".0000")
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numTradeDiscPct_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub numTradeDiscPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl numTradeDiscPct
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numTradeDiscPct_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub SetChkCloseSOFirst()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    ' setup the close SO checkboxes according to the value of
    ' chkShipComplete
    
    If chkShipComplete.Value = 0 Then
        ' enable if chkShipComplete = 0
        chkCloseSOOnFirst.Enabled = True
        chkCloseSOLineOnFirst.Enabled = True
    Else
        ' disable and set to zero if chkShipComplete = 1
        chkCloseSOOnFirst.Enabled = False
        chkCloseSOOnFirst.Value = 0
        chkCloseSOLineOnFirst.Enabled = False
        chkCloseSOLineOnFirst.Value = 0
        
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetChkCloseSOFirst", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub




Private Function IsValidTradeDiscPct() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
' Desc:  Trade Discount Lost Focus Event
'        Validate that field is in a Valid Range
' Parms: Valid - If set to 0 then control will not lose focus
'        szmask - mask
'        szTextdata - Sage MAS 500 Number Control. text Property
'        szTextdata - Sage MAS 500 Number Control. Masked text Property
'        hwndnewfocus - windows handle of the control you are going to
'***********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    IsValidTradeDiscPct = 1
    
    If numTradeDiscPct.Value > 100 Then   'Number Too Large
        tabC.Tab = kTabDflt
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
            sRemoveAmpersand(lblTradeDiscPct), "100.00"   'Send Error Message
        numTradeDiscPct.Value = numTradeDiscPct.Tag      'Reset Value to old Value
        gbSetFocus Me, numTradeDiscPct                         'Keep Cursor in this control
        IsValidTradeDiscPct = 0
    
    ElseIf numTradeDiscPct.Value < 0 Then  'Number Negative
        tabC.Tab = kTabDflt
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeNegative, _
            sRemoveAmpersand(lblTradeDiscPct)              'Send Error message
        numTradeDiscPct.Value = numTradeDiscPct.Tag       'Reset Value To Old Value
        gbSetFocus Me, numTradeDiscPct                          'Keep Cursor in this control
        IsValidTradeDiscPct = 0
    Else
        numTradeDiscPct.Tag = numTradeDiscPct.Value  'Valid entry reset tag with New Value
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset validating Flag and Value back to old Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidTradeDiscPct"
    numTradeDiscPct.Value = numTradeDiscPct.Tag
    IsValidTradeDiscPct = 0
    gClearSotaErr
    Exit Function
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidTradeDiscPct", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidPriceAdjPct() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'**************************************************************************
' Desc:  Price Adjustment Percent Lost Focus Event
'        Validate that field is in a Valid Range
' Parms: Valid - If set to 0 then control will not lose focus

'***********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    IsValidPriceAdjPct = 1
    
    If nbrPriceAdjPct.Value > 100 Then   'Number Too Large
        'tabC.Tab = kTabBTST
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
            sRemoveAmpersand(lblPriceAdjPct), "100.00"   'Send Error Message
        nbrPriceAdjPct.Value = nbrPriceAdjPct.Tag      'Reset Value to old Value
        gbSetFocus Me, nbrPriceAdjPct                    'Keep Cursor in this control
        IsValidPriceAdjPct = 0
    
    ElseIf nbrPriceAdjPct.Value < -100 Then  'Number Negative
        'tabC.Tab = kTabBTST
        giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeLessThan, _
            sRemoveAmpersand(lblPriceAdjPct), "-100.00"  'Send Error message
        nbrPriceAdjPct.Value = nbrPriceAdjPct.Tag       'Reset Value To Old Value
        gbSetFocus Me, nbrPriceAdjPct                    'Keep Cursor in this control
        IsValidPriceAdjPct = 0
    Else
        nbrPriceAdjPct.Tag = nbrPriceAdjPct.Value  'Valid entry reset tag with New Value
    End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset validating Flag and Value back to old Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidPriceAdjPct"
    nbrPriceAdjPct.Value = nbrPriceAdjPct.Tag
    IsValidPriceAdjPct = 0
    gClearSotaErr
    Exit Function
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidPriceAdjPct", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub numuserfld_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numUserFld(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Me.ActiveControl Is tbrMain Or Me.ActiveControl Is sbrMain Then  'New control going to requires Validation
        gbSetFocus Me, numUserFld(Index)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    Else
        IsValidUserfldNumber Index
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numuserfld_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboUserFld_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboUserFld(Index), True
    #End If
'+++ End Customizer Code Push +++
    
    If Me.ActiveControl Is tbrMain Or Me.ActiveControl Is sbrMain Then  'New control going to requires Validation
        gbSetFocus Me, cboUserFld(Index)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    Else
        IsValidUserFldValidated Index
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboUserFld_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function IsValidUserfldNumber(Index) As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine

    Dim slbl As String
    Dim i As Integer

    IsValidUserfldNumber = 1
  
  'date Control was not changed
    If moUserFlds.GetOldValue(CInt(numUserFld(Index).Tag)) <> numUserFld(Index).Value Then
        'Validate date entered by user If Invalid
        'Display error message, reset Value and set control back
        If numUserFld(Index).Value > 999999999999.99 Then
            tabC.Tab = kTabDflt
            For i = 0 To 3
                If Not moUserFlds.Control(i) Is Nothing Then
                    If moUserFlds.Control(i) Is numUserFld(Index) Then
                        slbl = sRemoveAmpersand(lblUserFld(i))
                    End If
                End If
            Next i
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                   slbl, "999,999,999,999.99"
            numUserFld(Index) = moUserFlds.GetOldValue((numUserFld(Index).Tag))
            gbSetFocus Me, numUserFld(Index)
            IsValidUserfldNumber = 0
        ElseIf numUserFld(Index).Value < -99999999999.99 Then
            tabC.Tab = kTabDflt
            For i = 0 To 3
                If Not moUserFlds.Control(i) Is Nothing Then
                    If moUserFlds.Control(i) Is numUserFld(Index) Then
                        slbl = sRemoveAmpersand(lblUserFld(i))
                    End If
                End If
            Next i
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeLessThan, _
                   slbl, "-99,999,999,999.99"
            numUserFld(Index) = moUserFlds.GetOldValue((numUserFld(Index).Tag))
            gbSetFocus Me, numUserFld(Index)
            IsValidUserfldNumber = 0
        End If
    
    End If
    
    moUserFlds.LetOldValue numUserFld(Index).Tag, numUserFld(Index)
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset field back
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidUserfldNumber"
    numUserFld(Index) = moUserFlds.GetOldValue(numUserFld(Index).Tag)
    IsValidUserfldNumber = 0
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidUserfldNumber", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function IsValidUserFldValidated(Index As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iUserFldIndex   As Integer
    Dim rs              As Object
    Dim bValid          As Boolean
    Dim sUserFldValue   As String
    
    IsValidUserFldValidated = True
    
    bValid = gbValidComboSelect(cboUserFld(Index))
    
    If bValid Then GoTo Valid
    
    sUserFldValue = ""
    
    iUserFldIndex = CInt(cboUserFld(Index).Tag)
            
    bValid = gbValidCIUserFldValue(moClass.moFramework, moSotaObjects, moClass.moAppDB, rs, _
                                    mbAllowUserFieldValueAOF, _
                                    sUserFldValue, moUserFlds.Title(iUserFldIndex), _
                                    moUserFlds.UserFldKey(iUserFldIndex), kEntTypeARCustomer)
             
    If Not bValid Then GoTo Invalid
    
    'Valid add on the fly
    cboUserFld(Index).ListIndex = giComboAddItem(cboUserFld(Index), _
                                  gsGetValidStr(rs.Field("UserFldValue")))
    
Valid:
    GoTo CloseRecordset
    
Invalid:
    IsValidUserFldValidated = False
   ' moVM.AllowMessage = Not mbAllowUserFieldValueAOF
    GoTo CloseRecordset

CloseRecordset:
    If Not rs Is Nothing Then rs.Close: Set rs = Nothing


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidUserFldValidated", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub tabC_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************************
' Desc:  Enable tab you are going To and disable all other tabs
'********************************************************************************
    Dim i As Integer
    
    fraTab(PreviousTab).Enabled = False
    fraTab(tabC.Tab).Enabled = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabC_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub EnterAddresses()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************
' Desc: Show Address form and allow user to update addresses
'***********************************************************
On Error GoTo ExpectedErrorRoutine


    Dim oCustDflts As New Collection  'Collection of Defaults for Addresses
    Dim oChild As Object              'Address Form Object
        
        
'Main Lookup
  'Customer ID, Name & used for Caption
    oCustDflts.Add (txtCustomerID.Text), "CustID"
    oCustDflts.Add (txtCustName.Text), "CustName"
    
  'Customer Key & Primary Address Key used in data Manager Where Clause
    oCustDflts.Add moDmForm.GetColumnValue("CustKey"), "CustKey"
    oCustDflts.Add moDmForm.GetColumnValue("PrimaryAddrKey"), "PrimaryAddrKey"
    
    
'Surrogate Key Text Boxes
  'Invoice Form ID & Key
    oCustDflts.Add (txtInvoiceForm.Text), "BusinessFormID"
    oCustDflts.Add moDmFormArAddr.GetColumnValue("InvcFormKey"), "InvoiceFormKey"
    
  'Salesperson ID & Key
    oCustDflts.Add (txtSalesperson.Text), "SperID"
    oCustDflts.Add moDmFormArAddr.GetColumnValue("SperKey"), "SperKey"
  
  'Ship Method ID, Key& Uses Ship Zones Flag
    oCustDflts.Add (txtShipMeth.Text), "ShipMethID"
    oCustDflts.Add moDmFormArAddr.GetColumnValue("ShipMethKey"), "ShipMethKey"
    oCustDflts.Add (txtMustUseZone.Text), "UsesZones"
    
  'FOB ID & Key--v3
    oCustDflts.Add (txtFOB.Text), "FOBID"
    oCustDflts.Add moDmFormArAddr.GetColumnValue("FOBKey"), "FOBKey"
  
'Non Surrogate key Non Validated fields
  'Invoice Message, FOB
    oCustDflts.Add (txtInvoiceMessage.Text), "InvoiceMsg"

'v3 'FOB Code
'    oCustDflts.Add (txtFOB.Text), "FOBCode"
    
'Non Surrogate key Validated fields
  'Currency & Country Code
    oCustDflts.Add (txtCurrency.Text), "CurrID"
    oCustDflts.Add cboCountry.Text, "DfltCountryID"
    
'Combo Boxes - Dynamic Lists - Surrogate Keys
  'Commission Plan
    If cboCommPlan.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "CommPlanKey"
    Else
        oCustDflts.Add cboCommPlan.ItemData(cboCommPlan.ListIndex), "CommPlanKey"
    End If
    
  'Payment Terms Key
    If cboPmtTerms.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "PmtTermsKey"
    Else
        oCustDflts.Add cboPmtTerms.ItemData(cboPmtTerms.ListIndex), "PmtTermsKey"
    End If
    
  'Sales Territory Key
    If cboSalesTerritory.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "SalesTerritoryKey"
    Else
        oCustDflts.Add cboSalesTerritory.ItemData(cboSalesTerritory.ListIndex), "SalesTerritoryKey"
    End If
    
  'Sales Tax Schedule Key
    If frmSalesTax.lSTaxSchdKey = -1 Then
        oCustDflts.Add 0, "STaxSchdKey"
    Else
        oCustDflts.Add frmSalesTax.lSTaxSchdKey, "STaxSchdKey"
    End If
    
  'Language
    If cboLanguage.ListIndex > kItemNotSelected Then
        oCustDflts.Add cboLanguage.ItemData(cboLanguage.ListIndex), "LanguageID"
    Else
        oCustDflts.Add 0, "LanguageID"
    End If
    
  'Shipping Zones
    If cboShipZone.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "ShipZoneKey"
    Else
        oCustDflts.Add cboShipZone.ItemData(cboShipZone.ListIndex), "ShipZoneKey"
    End If

  'Currency exchange Schedule ID & Key
    If cboCurrExchSchd.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "CurrExchSchdKey"
    Else
        oCustDflts.Add cboCurrExchSchd.ItemData(cboCurrExchSchd.ListIndex), "CurrExchSchdKey"
    End If

'Combo Boxes - StaticList
    If cboStatus.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "Status"
    Else
        oCustDflts.Add cboStatus.ItemData(cboStatus.ListIndex), "Status"
    End If
    
    If cboBillingType.ListIndex = kItemNotSelected Then
        oCustDflts.Add 0, "BillingType"
    Else
        oCustDflts.Add cboBillingType.ItemData(cboBillingType.ListIndex), "BillingType"
    End If

    
'Add Country Deafult Masks
  'Postal Code mask
    oCustDflts.Add txtPostalCode.Mask, "PostalCodeMask"
    
  'Phone & Fax mask
    oCustDflts.Add txtPhone.Mask, "PhoneMask"
    oCustDflts.Add mbPOActive, "POActive"
    oCustDflts.Add mbSOActive, "SOActive"
    oCustDflts.Add mbIMActive, "IMActive"
    oCustDflts.Add muCustDflts.UseMultiCurr, "UseMultiCurr"
    oCustDflts.Add muCustDflts.TrackSalesTax, "TrackSalesTax"
    oCustDflts.Add muCustDflts.PrintInvoices, "PrintInvoices"
    oCustDflts.Add muCustDflts.UseSper, "UseSper"
    'mlAckFormKey = gvCheckNull(moClass.moAppDB.Lookup("SOAckFormKey", "tarCustClass", "CustClassKey = " & moDmForm.GetColumnValue("CustClassKey")), SQL_INTEGER)
    'oCustDflts.Add mlAckFormKey, "AckFormKey"
    oCustDflts.Add lkuAckForm.KeyValue, "AckFormKey"
    'mlLblFormKey = gvCheckNull(moClass.moAppDB.Lookup("ShipLabelFormKey", "tarCustClass", "CustClassKey = " & moDmForm.GetColumnValue("CustClassKey")), SQL_INTEGER)
    'oCustDflts.Add mlLblFormKey, "LblFormKey"
    oCustDflts.Add lkuLabelForm.KeyValue, "LblFormKey"
    'miAllowInvt = moClass.moAppDB.Lookup("AllowInvtSubst", "tarCustClass", "CustClassKey = " & moDmForm.GetColumnValue("CustClassKey"))
    'oCustDflts.Add miAllowInvt, "AllowInvt"
    oCustDflts.Add moDmFormArAddr.GetColumnValue("AllowInvtSubst"), "AllowInvt"
    oCustDflts.Add chkShipComplete.Value, "ShipComplete"
    oCustDflts.Add chkReqAck.Value, "RequireAck"
    oCustDflts.Add lkuPackListForm.KeyValue, "PackFormKey"
    oCustDflts.Add chkPrintOrdAck.Value, "PrintAck"
    oCustDflts.Add numShipDays.Value, "ShipDays"
    oCustDflts.Add lkuClosestWhse.KeyValue, "WhseKey"
    oCustDflts.Add ddnPriceBase.ItemData, "PriceBase"
    oCustDflts.Add lkuPriceGroup.KeyValue, "PriceGroupKey"
    oCustDflts.Add nbrPriceAdjPct.Value, "PriceAdj"
    oCustDflts.Add lkuDefaultCreditCard.KeyValue, "DfltCreditCard"
        
    SetHourglass True   'Set Mouse Pointer
        
  'Load Address form if hasn't been loaded yet
    Set oChild = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
              kClsCustAddrAOF, ktskCustAddrAOF, kAOFRunFlags, kContextAOF)
        
    SetHourglass False  'Reset Mouse Pointer
  
    If oChild Is Nothing Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingAddress
    Else
      'Send startup parameters to Address form
        oChild.EnterAddresses oCustDflts
    End If
    
  'Renable the form and set focus back on the form
    Me.SetFocus
    Set oCustDflts = Nothing  'Clean up Customer Defaults collection
    Set oChild = Nothing      'Clean up Address Form

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "EnterAddresses"
    On Error Resume Next
    Me.SetFocus
    Set oCustDflts = Nothing  'Clean up Customer Defaults collection
    Set oChild = Nothing      'Clean up Address Form
    SetHourglass False  'Reset Mouse Pointer
    gClearSotaErr
Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EnterAddresses", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub EnterContacts()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************
' Desc: Show Address form and allow user to update addresses
'***********************************************************
On Error GoTo ExpectedErrorRoutine


    Dim oChild As Object              'Address Form Object
    Dim sCustID             As String
    Dim lCustkey            As Long
    Dim lContKey            As Long
    Dim sDfltCountry        As String
    Dim iStatus             As Integer
    Dim sPhoneMask          As String
  
'Main Lookup
  'Customer ID, Name & used for Caption
    sCustID = txtCustomerID.Text
    
  'Customer Key & Primary Address Key used in data Manager Where Clause
    lCustkey = moDmForm.GetColumnValue("CustKey")
    lContKey = gvCheckNull(moDmFormArAddr.GetColumnValue("DfltCntctKey"), SQL_INTEGER)
    sDfltCountry = cboCountry.Text
    
'Combo Boxes - StaticList
    If cboStatus.ListIndex = kItemNotSelected Then
        iStatus = 0
    Else
        iStatus = cboStatus.ItemData(cboStatus.ListIndex)
    End If
    
'Add Country Deafult Masks
  'Phone & Fax mask
    sPhoneMask = txtPhone.Mask
       
    SetHourglass True   'Set Mouse Pointer
        
  'Load Address form if hasn't been loaded yet
    Set oChild = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
       kclsCIContactMNT, ktskCIContactMNT, kAOFRunFlags, kContextAOF)

    SetHourglass False  'Reset Mouse Pointer
  
    If oChild Is Nothing Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingContacts
    Else
      'Send startup parameters to Address form
        oChild.EnterContacts lCustkey, lContKey, sCustID, kEntTypeARCustomer, sDfltCountry, _
                    sPhoneMask, iStatus
    End If
    
  'Renable the form and set focus back on the form
    Me.SetFocus
    Set oChild = Nothing      'Clean up Address Form

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "EnterContacts"
    On Error Resume Next
    Me.SetFocus
    Set oChild = Nothing      'Clean up Address Form
    giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingContacts
    SetHourglass False  'Reset Mouse Pointer
    gClearSotaErr
Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EnterContacts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub sbrMain_ButtonClick(button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick button
'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub HandleToolbarClick(ButtonConst As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************
'   Description:
'       cmdMaint is a control array for the user interface command buttons
'       that determine what to do with the current record being maintained
'       or viewed.
'   Parameters:
'       index <in> - which command button was pressed.  The choices are
'                    Finish/OK, Cancel, Delete.  Depending on the state
'                    of the form, some of these buttons may be Disabled
'                    or invisible.
'**************************************************************************
    Dim iOldIndex       As Integer  'Old List index to reset Status from Deleted
    Dim iConfirmUnload  As Integer
    Dim bOldmb          As Boolean
    Dim lCustkey        As Long
    Dim lRet            As Long
    Dim sNewKey         As String
    Dim vParseRet       As Variant
    Dim iMsgRet         As Integer
    Dim bWasNationalAcct As Boolean
    Dim bActivity       As Boolean
    Dim sUser       As String
    Dim sID         As String
    Dim vPrompt     As Variant
    
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(ButtonConst) Then
            Exit Sub
        End If
    End If
#End If
    
    Me.SetFocus
    
    frmSalesTax.bBadSalesTax = False
    bOldmb = mbDontChkClick
    mbDontChkClick = True       'Dont run click event on form clear
    
  'See which key you were called from
    Select Case ButtonConst
        Case kTbFirst, kTbLast, kTbPrevious, kTbNext
            If moDmForm.State <> kDmStateNone Then
                If moLostFocus.IsValidDirtyCheck = 0 Then
                    mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                ElseIf IsValidUFCheck = 0 Then
                    mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If
            End If
    
            'Ask User if they want to save changes and run save if Answer = Yes.
            iConfirmUnload = moDmForm.ConfirmUnload(True)
            
            bSetupNavMain
            If iConfirmUnload = kDmSuccess Then
                lRet = glLookupBrowse(navMain, ButtonConst, miFilter, sNewKey)
                Select Case lRet
                    Case MS_SUCCESS
                        vParseRet = gvParseLookupReturn(sNewKey)
                        
                        If IsNull(vParseRet) Then
                            Exit Sub
                        End If

                        If Trim(txtSaveCustID) <> Trim(vParseRet(1)) Then
                            txtSaveCustID.Text = Trim(vParseRet(1))
                            LoadMaskedCustNo
                            IsValidCustomer
                        End If

                    Case MS_BORS
                        HandleToolbarClick kTbFirst
                    
                    Case MS_EMPTY_RS
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavEmptyRecordSet
            
                    Case MS_EORS
                        HandleToolbarClick kTbLast
            
                    Case MS_ERROR
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavBrowseError
            
                    Case MS_NO_CURRENT_KEY
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavNoCurrentKey
            
                    Case MS_UNDEF_RS
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavUndefinedError
            
                    Case Else
                        gLookupBrowseError lRet, Me, moClass
                End Select
            End If
        
        Case kTbFilter
            miFilter = IIf(miFilter = RSID_UNFILTERED, RSID_FILTERED, RSID_UNFILTERED)
        
        Case kTbFinish, kTbFinishExit                    'Accept/Finish Was pressed

            lCustkey = gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
            gbSetFocus Me, tbrMain

            If moLostFocus.IsValidDirtyCheck = 0 Then
                mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            ElseIf IsValidUFCheck = 0 Then
                mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            If miNatUserLevel <> kiNatParent And miNatUserLevel <> kiNatNone Then
                If cboStatus.ListIndex <> kItemNotSelected Then
                    If cboStatus.ItemData(cboStatus.ListIndex) = kvDeleted Then
                    
                        iMsgRet = giSotaMsgBox(Me, moClass.moSysSession, kmsgARNASubCustStatToDel, lkuNatAcct.Text)
                        If iMsgRet = kretOK Then
                            moDmForm.SetColumnValue "NationalAcctLevelKey", Empty 'Set to Null
                            ClearNationalAcctTab
                            If mbUseNationalAcct Then
                                cboNatDfltBillTO.ListIndex = 2
                            End If
                        Else
                            Exit Sub
                        End If
                        
                    End If
                End If
            End If
            
                   
            If moDmForm.Action(kDmFinish) = kDmSuccess Then 'Save Was successful
                mbSaved = True                              'Set bSaved property on form
                CustEmailSave lCustkey
                ToolBarSuccess                              'Clean Up Form reset state
                tabC.Tab = kTabMain
                
            Else
                If frmSalesTax.bBadSalesTax Then
                    cmdSalesTax_Click
                End If
                #If DMDEBUG Then
                    Debug.Print "this blew up "
                #End If
            End If
            
        Case kTbSave                    'Save Was pressed
             
          'Force validation on last control if hot key pressed
            If moLostFocus.IsValidDirtyCheck = 0 Then
                mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            ElseIf IsValidUFCheck = 0 Then
                mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            If miNatUserLevel <> kiNatParent And miNatUserLevel <> kiNatNone Then
                If cboStatus.ListIndex <> kItemNotSelected Then
                    If cboStatus.ItemData(cboStatus.ListIndex) = kvDeleted Then
                    
                        iMsgRet = giSotaMsgBox(Me, moClass.moSysSession, kmsgARNASubCustStatToDel, lkuNatAcct.Text)
                        If iMsgRet = kretOK Then
                            moDmForm.SetColumnValue "NationalAcctLevelKey", Empty 'Set to Null
                            ClearNationalAcctTab
                            If mbUseNationalAcct Then
                                cboNatDfltBillTO.ListIndex = 2
                            End If
                        Else
                            Exit Sub
                        End If
                        
                    End If
                End If
            End If
           
            If moDmForm.Save(True) = kDmSuccess Then 'Save Was successful
                mbSaved = True                              'Set bSaved property on form
                CustEmailSave
            Else
                If frmSalesTax.bBadSalesTax Then
                    cmdSalesTax_Click
                End If
                #If DMDEBUG Then
                    Debug.Print "this blew up "
                #End If
            End If
            
        
        Case kTbCancel, kTbCancelExit                       'Cancel was pressed
            If moDmForm.Action(kDmCancel) = kDmSuccess Then 'Cancel Was successful
                ToolBarSuccess                              'Clean Up Form reset state
            Else
                #If DMDEBUG Then
                    Debug.Print "this blew up "
                #End If
            End If
            mbDMStateChanged = False
            txtLatitude.Tag = ""
            txtLongitude.Tag = ""
                        
        Case kTbDelete                                      'Delete Was pressed
            sID = CStr(ksecDeleteCustomer)
            sUser = CStr(moClass.moSysSession.UserId)
            vPrompt = True
            
            If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
                mbDontChkClick = bOldmb
                Exit Sub
            End If
        
          'Force validation on last control if hot key pressed
            If moLostFocus.IsValidDirtyCheck = 0 Then
                mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            ElseIf IsValidUFCheck = 0 Then
                mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If

            iMsgRet = giSotaMsgBox(Me, moClass.moSysSession, kmsgPADeleteRecord)
            If iMsgRet = kretNo Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            
                Exit Sub
            
            End If

            miCustKey = glGetValidLong(moDmForm.GetColumnValue("CustKey"))
            'miNatUserLevel set to none upon delete, so store here.
            bWasNationalAcct = (miNatUserLevel <> kiNatNone)
    
            'Attempt DB delete.  If DB delete fails, perform logical delete.
            If bDeleteCustomerRecord(miCustKey, bActivity) Then
                moDmForm.Action (kDmCancel)
                If bWasNationalAcct Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgPendTransNotAffectedNA
                End If
                ToolBarSuccess
            Else
                'If the customer was not deleted due to activity, then logically delete.
                If bActivity Then
              
                    'Check the user id typed in can change customer status
                    sID = CStr(ksecChangeStatus)
                    sUser = CStr(moClass.moSysSession.UserId)
                    vPrompt = True
                    If moClass.moFramework.GetSecurityEventPerm(sID, sUser, vPrompt) = 0 Then
                        bOldmb = mbDontChkClick
                        mbDontChkClick = True
                        cboStatus.ListIndex = Val(cboStatus.Tag)
                        mbDontChkClick = bOldmb
                        Exit Sub
                    End If

                    'Get old Status Index and set Status index to Deleted
                    iOldIndex = cboStatus.ListIndex
                    cboStatus.ListIndex = giListIndexFromItemData(cboStatus, kvDeleted)
                    
                    If moDmForm.Action(kDmFinish) = kDmSuccess Then 'Delete Was successful
                        mbSaved = True                              'Set bSaved property on form
                        giSotaMsgBox Me, moClass.moSysSession, kmsgARLogicalCustomerDelete
                        If bWasNationalAcct Then
                            giSotaMsgBox Me, moClass.moSysSession, kmsgPendTransNotAffectedNA
                        End If
    
                        ToolBarSuccess                              'Clean Up Form reset state
                    Else
                        If frmSalesTax.bBadSalesTax Then
                            cmdSalesTax_Click
                        End If
                        #If DMDEBUG Then
                        Debug.Print "this blew up "
                        #End If
                        cboStatus.ListIndex = iOldIndex             '  Reset Status back
                    End If
                End If
            End If
        
        Case kTbNextNumber                   'Next New Button Pressed
            If moDmForm.State <> kDmStateNone Then
                If moLostFocus.IsValidDirtyCheck = 0 Then
                    mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                ElseIf IsValidUFCheck = 0 Then
                    mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If
            End If
            iConfirmUnload = moDmForm.ConfirmUnload()
            
            If iConfirmUnload = kDmSuccess Then
                moDmForm.Clear True
                ClearUnboundControls
                GetNextCustNo               'Get Next Customer Number
            Else
                If frmSalesTax.bBadSalesTax Then
                    cmdSalesTax_Click
                End If
            End If
            gbSetFocus Me, txtCustName
            ' Reset memo button to none since this is a new customer.
            lCustkey = gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
            gSetMemoToolBarState moClass.moAppDB, lCustkey, kEntTypeARCustomer, msCompanyID, tbrMain  ' Set in case change in memos

            mbDontChkClick = bOldmb
            
        Case kTbRenameId                'rename ID was Selecetd
            moDmForm.RenameID           'Run data Manager rename
            If Len(txtSaveCustID.Text) > Len(txtCustomerID.Mask) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCustIDTooLong, Len(txtCustomerID.Mask)
                txtSaveCustID.Text = Left$(txtSaveCustID.Text, Len(txtCustomerID.Mask))
            End If
            LoadMaskedCustNo            'Load Unbound control
        
        Case kTbMemo
          'This is memo stuff which is not supported in beta, because tciEntity has no IM entries
            Me.Enabled = False
            CMMemoSelected txtCustomerID
            Me.Enabled = True
            lCustkey = gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
            gSetMemoToolBarState moClass.moAppDB, lCustkey, kEntTypeARCustomer, msCompanyID, tbrMain  ' Set in case change in memos
          
        Case kTbCopyFrom                'Copy From

            If moLostFocus.IsValidDirtyCheck = 0 Then
                mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            ElseIf IsValidUFCheck = 0 Then
                mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            
            mbDontChkClick = bOldmb
            iConfirmUnload = moDmForm.ConfirmUnload(True)
            If (iConfirmUnload) Then                 'Have user save changes
                                
                ' Proceed with Copy From logic
                
                ' Need to determine if National Account question should be enabled
                If (mbUseNationalAcct) Then
                    If (miNatUserLevel = kiNatParent) Then
                        ' Disable if parent
                        m_CopyMgr.EnableQuestion 2, False
                    Else
                        If (miNatUserLevel = kiNatNone) Then
                            ' Disable if not a member
                            m_CopyMgr.EnableQuestion 2, False
                        Else
                            ' Enable question
                            m_CopyMgr.EnableQuestion 2, True
                        End If
                    End If
                End If

                ' Proceed with Copy From Logic
                If (m_CopyMgr.Copy) Then
                    ' Store whether or not they want to copy addresses/contacts
                    ' To be used later in DMAfterInsert
                    m_AddressQuestion = (m_CopyMgr.Value(1) = vbChecked)
                End If
                                
                
            Else
                If frmSalesTax.bBadSalesTax Then
                    cmdSalesTax_Click
                End If
            End If
            mbDontChkClick = bOldmb
            
        
        Case kTbPrint                       'Print was Selected
            If moLostFocus.IsValidDirtyCheck = 0 Then
                mbDontChkClick = bOldmb
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            ElseIf IsValidUFCheck = 0 Then
                mbDontChkClick = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
            bSetupNavMain
            mbDontChkClick = bOldmb
            iConfirmUnload = moDmForm.ConfirmUnload(True)
            If iConfirmUnload Then                 'Have user save changes
                gPrintTask moClass.moFramework, ktskPrint
            Else
                If frmSalesTax.bBadSalesTax Then
                    cmdSalesTax_Click
                End If
            End If
            mbDontChkClick = bOldmb
            
        
        Case kTbHelp                                                'Help was Selected
            gDisplayFormLevelHelp Me                                'Show Form level Help

        Case Else   'Unknown button
            'New Command Button not programmed for
            tbrMain.GenericHandler ButtonConst, Me, moDmForm, moClass
    End Select
   
    mbDontChkClick = bOldmb     'Reset Global Click Event Checking flag

'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtBillTo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBillTo, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtBillTo
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBillTo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidBillTo() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
     
    IsValidBillTo = 1
    Set moValidate.oDmForm = moDmForm
    moValidate.iTab = kTabBTST
    
    IsValidBillTo = Abs(CInt(moValidate.ARCustAddr(txtBillTo, lblBillTo, "DfltBillToAddrKey", _
            mbAllowCustAddrAOF, True, True, _
                "= " & gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER))))
     
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidBillTo"
    txtBillTo.Text = txtBillTo.Tag
    IsValidBillTo = 0
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidBillTo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtContName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtContName, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtContName
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtContName_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidContact() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************

On Error GoTo ExpectedErrorRoutine
     
    Dim sSQL     As String    'SQL String
    Dim rs       As New DASRecordSet    'SOTADAS recordset object
    Dim lCKey    As Long      'Contact Key
    Dim vOldKey  As Variant   'reset old keys
    Dim vOldCntctKey As Variant
    Dim lErr     As Long
    Dim sErrDesc As String
    Dim iConfirmUnload As Integer
    
    IsValidContact = 1
    
    vOldKey = moDmFormArAddr.GetColumnValue("DfltCntctKey")
    vOldCntctKey = moDmForm.GetColumnValue("PrimaryCntctKey")
  'Contact has been changed (not blanked out)
  If Len(Trim(txtContName.Text)) = 0 Then
    txtContName.Tag = txtContName.Text
    Exit Function
  End If
  If Not m_AddressQuestion Then
    
    If Trim(txtContName.Text) <> Trim(txtContName.Tag) And _
        Len(Trim(txtContName.Text)) > 0 Then
                                    
      'Get contacts if exist from the data base
        sSQL = "SELECT CntctKey CKey FROM "
        sSQL = sSQL & "tciContact WHERE CntctOwnerKey = "
        sSQL = sSQL & gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
        sSQL = sSQL & " AND Name = " & sFormatValueToSQL(txtContName.Text, SQL_CHAR, False)
        sSQL = sSQL & " AND EntityType = " & kEntTypeARCustomer
    
      'run query and retrieve the key
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
      'A Contact exeist with this Name
        If Not rs.IsEOF Then
                       
          'Check to make sure not same as current key
            lCKey = gvCheckNull(rs.Field("CKey"), SQL_INTEGER)
                
          'Clear out recordset object
            If Not rs Is Nothing Then
                rs.Close
                Set rs = Nothing
            End If
                
          'Not current Key (or invalid key Value)
            If lCKey <> gvCheckNull(moDmFormArAddr.GetColumnValue("DfltCntctKey"), SQL_INTEGER) _
                    And lCKey <> 0 Then
                        
              'Ask user if they want to load defaults for new key
                If giSotaMsgBox(Nothing, moClass.moSysSession, _
                        kmsgContactExists) = kretYes Then
                        
                  'Load Contact Keys
                    moDmForm.SetColumnValue "PrimaryCntctKey", lCKey
                    moDmFormArAddr.SetColumnValue "DfltCntctKey", lCKey
                    moDmContact.SetColumnValue "CntctKey", lCKey
                        
                  'Run Key Change event for contacts portion of the form
                    If moDmContact.KeyChange() <> kDmSuccess Then
                              
                      'Error to user if unsuccessful key change
                        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, _
                                  sRemoveAmpersand(lblContact)
                        IsValidContact = 0
                      'reset values
                        moDmForm.SetColumnValue "PrimaryCntctKey", vOldKey
                        moDmFormArAddr.SetColumnValue "DfltCntctKey", vOldKey
                        moDmContact.SetColumnValue "CntctKey", vOldKey
                        txtContName.Text = txtContName.Tag
                    Else
                        m_SourceCntcKey = moDmForm.GetColumnValue("PrimaryCntctKey")
                    End If  'Bad key change
                Else
                    txtContName.Text = txtContName.Tag
                    IsValidContact = 0
                End If  ' user does not want to load defaults
            End If  'current or invalid Key
        Else
            moDmFormArAddr.SetColumnValue "DfltCntctKey", vOldCntctKey
            'GetContactKey
        End If  'record with that Name does not exist
    End If  'No change made
        
    txtContName.Tag = txtContName.Text
 Else
    If moDmForm.State = kDmStateAdd Then
        txtContName.Text = txtContName.Tag
        iConfirmUnload = moDmForm.ConfirmUnload(True)
        If iConfirmUnload = kDmSuccess Then
            If moDmForm.State = kDmStateAdd Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgARSaveBeforePrimCntChange
                Exit Function
            End If
        Else
            If iConfirmUnload = kDmFailure Then Exit Function
        End If
    End If
 End If
 
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    lErr = Err
    sErrDesc = Err.Description
    txtContName.Text = txtContName.Tag
    'Clear out recordset object
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    moDmForm.SetColumnValue "PrimaryCntctKey", vOldKey
    moDmFormArAddr.SetColumnValue "DfltCntctKey", vOldKey
    moDmContact.SetColumnValue "CntctKey", vOldKey
    IsValidContact = 0
    MyErrMsg moClass, sErrDesc, lErr, sMyName, "IsValidContact"
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidContact", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtCurrency_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCurrency, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtCurrency
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidCurrID() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    IsValidCurrID = 1
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabBTST
        
    IsValidCurrID = moValidate.MCCurrency(txtCurrency, lblCurrID, "CurrID", _
            mbAllowCurrencyAOF, False, muCustDflts.UseMultiCurr)
    
    Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    IsValidCurrID = 0
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidCurrID"
    txtCurrency.Text = txtCurrency.Tag
    gClearSotaErr
    Exit Function
  
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidCurrID", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtCustomerID_LostFocus()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustomerID, True
    #End If
'+++ End Customizer Code Push +++
'**************************************************************************
' Desc:  Customer ID Lost Focus Event
'        Loads the form if a valid customer ID is entered
'        Otherwise sets form state to new/add
'        until a valid key is enterd focus remains on this control
' Parms: Valid - If set to 0 then control will not lose focus
'        szmask - mask
'        szTextdata - Sage MAS 500 Masked Edit Control. text Property
'        szTextdata - Sage MAS 500 Masked Edit Control. Masked text Property
'        hwndnewfocus - windows handle of the control you are going to
'***********************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim bOldmb As Boolean

    bOldmb = mbDontChkClick

    'Added 'If' condition to prevent the LostFocus event from resetting
        'the 'mbExemptionsChanged' flag, when the user return back
        'from the �Sales Tax Exemption� form
    If txtCustomerID.Enabled Then
        moLostFocus.IsValidControl txtCustomerID
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "txtCustomerID_LostFocus"
    mbRowLoading = False
    mbDontChkClick = bOldmb
    gbSetFocus Me, txtCustomerID
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtCustomerID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function IsValidCustomer(Optional CustID As String = "", _
                                Optional ErrNo As Long = 0, _
                                Optional ErrData As Variant = "") As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************
' Desc:    Customer Validation
'          Loads the form if a valid customer class is entered
'          Otherwise sets form state to new/add
'          unless an invalid key is entered
' Param:   CustID  Optional for Copy feature to valid the entered custid, not
'                  the cust id in the custid control
'          ErrNo   Optional for Copy feature to return error number
'          ErrData Optional for Copy feature to return error data
' Returns: kdmFailure if Incomplete Key Or An error occurs
'          kDmSuccess If Valid Key is entered (New or existing record)
'***********************************************************************
    Dim iKeyChangeCode  As Integer      'Key Change Return Code
    Dim bValid          As Boolean      'Valid Flag
    Dim bOldmb          As Boolean
    Dim lCustkey        As Long
    Dim lCopyCustKey    As Long
    
    IsValidCustomer = kDmSuccess        'Set Initial Return Code
    bOldmb = mbDontChkClick
  
  'Load Company ID if it is empty
    If IsEmpty(moDmForm.GetColumnValue("CompanyId")) Or _
        Len(moDmForm.GetColumnValue("CompanyId")) = 0 Then
         moDmForm.SetColumnValue "CompanyID", msCompanyID
    End If
     
  'Load Customer ID into the field padded with zeros
  'Pad control with zeros as well (length of the mask)
    If (Len(Trim(txtCustomerID.Text)) > 0) Or (Len(Trim(CustID)) > 0) Then
        If (Len(Trim(txtCustomerID.Text)) > 0) Then
            txtSaveCustID.Text = txtCustomerID.Text     'Load Bound Control
        End If
        LoadMaskedCustNo CustID                                        'Load Unbound control
    End If
    
  'Dont run click event for Combo Boxes (will lock the application)
    mbDontChkClick = True
    mbRowLoading = True

'Intellisol start
'    'As long as Project Accounting is activated for at least one company, ensure uniqueness of customer ID across all companies.
'    If moClass.moAppDB.Lookup("Count(*) RecCount", "tsmCompanyModule", "ModuleNo = " & kModulePA & " AND Active = 1") > 0 Then
'        If moClass.moAppDB.Lookup("Count(*) RecCount", "tarCustomer", "CustID = " & gsQuoted(txtSaveCustID.Text) & "AND CompanyID <> " & gsQuoted(msCompanyID)) > 0 Then
'            giSotaMsgBox Me, moClass.moSysSession, kmsgCustIDAlreadyUsed
'            txtCustomerID.Tag = ""
'            txtCustomerID.Text = ""
'            txtSaveCustID.Tag = ""
'            txtSaveCustID.Text = ""
'            gbSetFocus Me, txtCustomerID
'            txtCustomerID.Enabled = True
'            IsValidCustomer = kDmFailure
'            Exit Function
'        End If
'    End If
'Intellisol end
   
    ' Determine if the CopyFrom ID entered is already a valid ID.
    If (Len(Trim(CustID)) > 0) Then
        lCopyCustKey = glGetValidLong(moClass.moAppDB.Lookup("CustKey", "tarCustomer", "CompanyID = " & gsQuoted(msCompanyID) & _
                                      "AND CustID = " & gsQuoted(CustID)))
        mbDontChkClick = bOldmb
        mbRowLoading = False
        If (lCopyCustKey = 0) Then
            IsValidCustomer = kDmSuccess
            Exit Function
        Else
            IsValidCustomer = kDmFailure
            ErrNo = kmsgDMNotUniqueGen
            Exit Function
        End If
    Else
   
        'Profile code
        iKeyChangeCode = moDmForm.KeyChange()   'Run data Manager Key Change Event
        
        If cboStatus.ListIndex <> kItemNotSelected Then
            If msCustStatus = "Disabled" Then
                If mlRunMode <> kContextAOF Then
                    tbrMain.Buttons(kTbRenameId).Enabled = False
                End If
            End If
        End If
    End If
    
    mbRowLoading = False
    
    Select Case iKeyChangeCode              'Run Return Code Processing
        Case kDmKeyNotFound
          'Load static list defaults Load Customer defaults
          'enable Address button
            moClass.lUIActive = kChildObjectActive          'Do UI Activate
            txtCustomerID.Protected = True                   'Disable The Customer ID
            
            '-------------------------------------------------------------------------------------------
            'KMS 01/23/02 - Think the following this should be put into DMReposition, but think it is
            '               too risky to make this change before this release.
            '               There are multiple DM objects affected, and we would need to define some
            '               module level variables to store some of the information, which is more work
            '               than desired.   This appears to be working fine where it is.
            cboBillingType.ListIndex = miDfltBillType       'Default Billing Type Combo
            
            'Default Credit Limit Aging category Combo
            cboCredLimitAgeCat.ListIndex = miDfltAgeCat
            
            cboStatus.ListIndex = miDfltStatus              'Default Status Combo
            cboCredLimitAgeCat.Tag = cboCredLimitAgeCat.ListIndex
            LoadCustDefaults 'Load Customer Defaults
            '-------------------------------------------------------------------------------------------
            
            tabC.Tab = kTabMain
            
            GetSTaxExemptions
            
           
        Case kDmKeyFound
          'enable Address button
            moClass.lUIActive = kChildObjectActive
            txtCustomerID.Protected = True                   'Disable The Customer ID
            tabC.Tab = kTabMain
            
            GetSTaxExemptions
            
            moDmForm.SetDirty False, True

        Case kDmNotAllowed
                txtCustomerID.Tag = ""
                txtCustomerID.Text = ""
                txtSaveCustID.Tag = ""
                txtSaveCustID.Text = ""
                gbSetFocus Me, txtCustomerID
                txtCustomerID.Protected = False
        
        Case kDmKeyNotComplete
          'key not completely filled in
            IsValidCustomer = kDmFailure
            gbSetFocus Me, txtCustomerID
         
        Case kDmError
          'database error occurred trying to get row
            IsValidCustomer = kDmFailure
            gbSetFocus Me, txtCustomerID
         
        Case Else
           'Unknown return from data manager
             giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, iKeyChangeCode
             IsValidCustomer = kDmFailure
             gbSetFocus Me, txtCustomerID
            
    End Select
   
    mbDontChkClick = bOldmb   'Reset Global don't run click event flag

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidCustomer", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub GetSTaxExemptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    'Populate the temp tables used for exemptions and sales tax codes
    With frmExemptions
        .lCustkey = glGetValidLong(moDmForm.GetColumnValue("CustKey"))
        .lAddrKey = glGetValidLong(moDmForm.GetColumnValue("PrimaryAddrKey"))
        .PopulateSTaxCodes
        .bExemptionsChanged = False
    End With
            
    mbReloadSTaxExmpt = False
            
    'Reset the flag to indicate the temp tables used for tax codes and exemptions need to be refresh
    'if the user visits the exemptions form.
    mbSetupSTax = False
        
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetSTaxExemptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtDateEstablished_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDateEstablished, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtDateEstablished
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDateEstablished_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidDateEstablished() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard date Validation
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    IsValidDateEstablished = 1

    Dim bValid As Boolean       'Validation Flag
     
    If Len(Trim(txtDateEstablished.Text)) <> 0 Then         'date Has Been Changed
      'Perform date validation
        bValid = gbCheckDate(Me, lblDateEstablished, txtDateEstablished, , tabC, kTabMain)
      'If Valid Set up Old Value otherwise reset Value back
        If bValid Then
            txtDateEstablished.Tag = txtDateEstablished.Text
        Else
            txtDateEstablished.Text = txtDateEstablished.Tag
            IsValidDateEstablished = 0
        End If
    Else
      'If Valid Set up Old Value
        txtDateEstablished.Tag = txtDateEstablished.Text
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidDateEstablished"
    txtDateEstablished.Text = txtDateEstablished.Tag
    IsValidDateEstablished = 0
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidDateEstablished", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtDfltItem_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDfltItem, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtDfltItem
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDfltItem_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidItem() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return Surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim lItem   As Long
    Dim sItemID As String
    
    sItemID = txtDfltItem.Tag
    lItem = gvCheckNull(moDmForm.GetColumnValue("DfltItemKey"), SQL_INTEGER)
    
    IsValidItem = 1

    Set moValidate.oDmForm = moDmForm
    moValidate.iTab = kTabDflt
    moValidate.bReturnRecordset = True
        
    IsValidItem = Abs(CInt(moValidate.IMItem(txtDfltItem, lblDfltItem, "DfltItemKey", _
            mbAllowItemAOF, True, False)))
    
    If moValidate.bValidChangedRs Then
        If Not moValidate.oRS Is Nothing Then
            If gvCheckNull(moValidate.oRS.Field("ItemType"), SQL_INTEGER) = 3 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgArBadItemType
                tabC.Tab = kTabDflt
                gbSetFocus Me, txtDfltItem
                txtDfltItem.Text = sItemID
                txtDfltItem.Tag = txtDfltItem.Text
                If lItem = 0 Then
                    moDmForm.SetColumnValue "DfltItemKey", Empty
                Else
                    moDmForm.SetColumnValue "DfltItemKey", lItem
                End If
            End If
        End If
    End If
    
    Set moValidate.oRS = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidItem"
    txtDfltItem.Text = sItemID
    If lItem = 0 Then
        moDmForm.SetColumnValue "DfltItemKey", Empty
    Else
        moDmForm.SetColumnValue "DfltItemKey", lItem
    End If
    Set moValidate.oRS = Nothing
    IsValidItem = 0
    gClearSotaErr
    Exit Function
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidItem", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtSalesperson_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSalesperson, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtSalesperson
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSalesperson_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidSalesperson() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return Surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    IsValidSalesperson = 1
    
    Dim sOldtag     As String
    Dim lErr        As Long
    Dim sErrDesc    As String

    sOldtag = txtSalesperson.Tag
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabBTST
    moValidate.bReturnRecordset = True
        
    IsValidSalesperson = Abs(CInt(moValidate.ARSalesperson(txtSalesperson, lblSalesperson, "SperKey", _
            mbAllowSalesPersonAOF, True, False)))
    
    If Len(Trim(txtSalesperson)) = 0 Then
        txtSperName.Text = ""
    Else
        If moValidate.bValidChangedRs Then
            txtSperName.Text = gvCheckNull(moValidate.oRS.Field("SperName"))
        End If
    End If
    
    Set moValidate.oRS = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    lErr = Err
    sErrDesc = Err.Description
    txtSalesperson.Text = txtSalesperson.Tag
    IsValidSalesperson = 0
    Set moValidate.oRS = Nothing
    MyErrMsg moClass, sErrDesc, lErr, sMyName, "IsValidSalesperson"
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidSalesperson", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtShipTo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipTo, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtShipTo
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtShipTo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidShipTo() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
    IsValidShipTo = 1
    
    Set moValidate.oDmForm = moDmForm
    moValidate.iTab = kTabBTST

    IsValidShipTo = Abs(CInt(moValidate.ARCustAddr(txtShipTo, lblShipTo, "DfltShipToAddrKey", _
            mbAllowCustAddrAOF, True, True, _
                "= " & gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER))))
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidShipTo"
    txtShipTo.Text = txtShipTo.Tag
    IsValidShipTo = 0
    gClearSotaErr
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidShipTo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidGLAccount() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
'   Description:
'       IsValidGLAccount will check if a valid GLAccount Number entry
'       exists. If nothing was entered or there was no change in the Number
'       entered then continue.  Otherwise, check the Account Number to determine
'       if there is an existing one. If not then create the GLAccount object,
'       if necessary, and run the AddOnTheFly.
'   Returns: 0 - Invalid Account
'            1 - Valid Account Or Empty
'*******************************************************************************

    Dim rs          As Object   'SOTADAS recordset Object
    Dim bValid      As Boolean  'Boolean Valid Flag
    Dim sGLAcctNo   As String   'String GL Account Number
    Dim sCurrID     As String
    
    IsValidGLAccount = 1        'Initialize the return Value
    
    
  'No changes were made no need to validate
    'If txtGLAccount = txtGLAccount.Tag Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        'Exit Function
    'End If
    
    
  'No acct Entered Clear out Values for key and reset tag
    If Len(Trim(glaAccount.Text)) = 0 Then
        'moDmForm.SetColumnValue "DfltSalesAcctKey", Empty
        glaAccount.Tag = glaAccount.Text
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If glaAccount.IsValid = False Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblGLAccount)
        glaAccount.Text = glaAccount.Tag
        gbSetFocus Me, glaAccount
        IsValidGLAccount = 0
    Else
        glaAccount.Tag = glaAccount.Text
    End If
    
        
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidGLAccount", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtShipMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipMeth, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtShipMeth
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtShipMeth_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidShipMeth() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database and clears out
'       (and reloads) The shipping Zone combo box if it is changed
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim bValid As Boolean
    Dim bOldmb As Boolean
    Dim lErr As Long
    Dim sErrDesc    As String

    IsValidShipMeth = 1
    
    bOldmb = mbDontChkClick
    mbDontChkClick = True       'Dont run click event ship method
    moValidate.bReturnRecordset = True  'Bring Back UsesZones
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabBTST
  
  'Run Standard Validation
    bValid = moValidate.CIShipMethod(txtShipMeth, lblShipMeth, "ShipMethKey", _
            mbAllowShipMethAOF, True, False)

    IsValidShipMeth = Abs(CInt(bValid))
  'Ship Method was cleared out clear out related controls & fields
    If Len(Trim(txtShipMeth)) = 0 Then
        txtMustUseZone.Text = "0"
        SetupShipZone
        moDmFormArAddr.SetColumnValue "ShipZoneKey", Empty
    Else
      'Load Uses Zones
        If moValidate.bValidChangedRs Then                      'A valid state was entered
            txtMustUseZone = moValidate.oRS.Field("ZonesUsed")  'Return Shipper Uses Zones
            Set moValidate.oRS = Nothing                        'Clean Up recordset
        End If
        If bValid And moValidate.bWasChanged Then       'A Valid Change was Made (not Blanked Out)
          'Ship Method was changed out clear out ship zones & fields
            SetupShipZone
            moDmFormArAddr.SetColumnValue "ShipZoneKey", Empty
        End If
    End If

    mbDontChkClick = bOldmb       'Dont run click event
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    lErr = Err
    sErrDesc = Err.Description
    txtShipMeth.Text = txtShipMeth.Tag
    mbDontChkClick = bOldmb                               'Dont run click event
    Set moValidate.oRS = Nothing                        'Clean Up recordset
    IsValidShipMeth = 0
    MyErrMsg moClass, sErrDesc, Err, sMyName, "IsValidShipMeth"
    gClearSotaErr
    Exit Function
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidShipMeth", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function IsValidFOB() As Integer 'v3
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim lErr As Long
    Dim sErrDesc As String

    IsValidFOB = 1
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabBTST
  
    IsValidFOB = Abs(CInt(moValidate.CIFOB(txtFOB, lblFOB, "FOBKey", mbAllowFOBAOF, True, False)))
  
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidFOB"
    txtFOB.Text = txtFOB.Tag
    IsValidFOB = 0
    gClearSotaErr
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidFOB", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtSIC_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSic, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtSic
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSIC_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidSIC() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    IsValidSIC = 1
    Set moValidate.oDmForm = moDmForm
    moValidate.iTab = kTabMain

    IsValidSIC = Abs(CInt(moValidate.ARSIC(txtSic, lblSic, "StdIndusCodeID", _
            mbAllowSicAOF, False, False)))
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidSIC"
    txtSic.Text = txtSic.Tag
    IsValidSIC = 0
    gClearSotaErr
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidSIC", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtStmtform_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtStmtForm, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtStmtForm
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtStmtform_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidStmtForm() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    IsValidStmtForm = 1
    
    Set moValidate.oDmForm = moDmForm
    moValidate.iTab = kTabDflt

    IsValidStmtForm = Abs(CInt(moValidate.CIBusinessForm(txtStmtForm, lblStmtForm, "StmtFormKey", _
        mbAllowBusFormAOF, True, 3, False)))
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidStmtForm"
    txtStmtForm.Text = txtStmtForm.Tag
    IsValidStmtForm = 0
    gClearSotaErr
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidStmtForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtVendor_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtVendor, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtVendor
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtVendor_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidVendor() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim bValid As Boolean
    Dim sSQL As String
    
    IsValidVendor = 1
    sSQL = "#apGetVendorAllCols;" & kSQuote
    sSQL = sSQL & msCompanyID & kSQuote & ";" & kSQuote
    sSQL = sSQL & txtVendor.Text & kSQuote & ";"
    
    IsValidVendor = Abs(CInt(gbIsFieldValid(Me, moClass.moFramework, moSotaObjects, moClass.moAppDB, _
            moDmForm, txtVendor, lblVendor, _
            "VendKey", sSQL, "VendKey", _
            mbAllowVendorAOF, ktskAPVendorsAOF, kclsAPVendorsAOF, True)))
            
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidVendor", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtPostalCode_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPostalCode, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtPostalCode
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtPostalCode_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidZip() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim sSTate      As String
    Dim sCountry    As String
    Dim sCity       As String
    Dim vZip        As Variant
    Dim sWhere      As String
    Dim sZip        As String
    IsValidZip = 1
  
  'Bring Back Values
    moValidate.bReturnRecordset = True
    Set moValidate.oDB = moClass.moAppDB
    moValidate.iTab = kTabMain
    
    sWhere = "PostalCode = " & gsQuoted(txtPostalCode)
    
    If cboCountry.ListIndex <> kItemNotSelected Then
        sWhere = sWhere & " AND CountryID = " & gsQuoted(cboCountry.Text)
    End If
    
    vZip = moClass.moAppDB.Lookup("PostalCode", "tsmPostalCode", sWhere)
    If vZip <> "" Then
        'Run standard database validation
        IsValidZip = Abs(CInt(moValidate.CIPostalCode(txtPostalCode, lblZip, "PostalCode", _
                mbAllowPostalAOF, False, False, (cboCountry.Text))))
    Else
        Set moValidate.oRS = Nothing                         'Clean Up recordset
        Set moValidate.oDB = moClass.moAppDB
        Exit Function
    End If
                        
  'Load New City State and country
    If moValidate.bValidChangedRs Then                       'A valid Postal Code was entered
        sZip = txtPostalCode.Text
        sCity = gvCheckNull(moValidate.oRS.Field("City"))           'Return City
        sCountry = gvCheckNull(moValidate.oRS.Field("CountryID"))   'return Country
        sSTate = gvCheckNull(moValidate.oRS.Field("StateID"))       'Return State
        Set moValidate.oRS = Nothing                         'Clean Up recordset
        mbRowLoading = True
        cboCountry.ListIndex = giListIndexFromText( _
              cboCountry, sCountry)                          'Load Country Combo
        mbRowLoading = False
        txtPostalCode.Text = sZip
        txtCity.Text = sCity
        cboState.ListIndex = giListIndexFromText( _
                 cboState, sSTate)                           'Load State Combo
        SetCountryMasks cboCountry.Text                      'Setup Country Masks
        cboState.Tag = cboState.ListIndex
        cboCountry.Tag = cboCountry.ListIndex
    End If

    Set moValidate.oRS = Nothing                         'Clean Up recordset
    Set moValidate.oDB = moClass.moAppDB
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
 Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidZip"
    txtPostalCode.Text = txtPostalCode.Tag
    mbRowLoading = False
    Set moValidate.oDB = moClass.moAppDB
    Set moValidate.oRS = Nothing                         'Clean Up recordset
    IsValidZip = 0
    gClearSotaErr
    Exit Function
    

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidZip", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtCustClass_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustClass, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtCustClass
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtCustClass_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidCustClass() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'       and load defaults
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    Dim bOldmb As Boolean
    Dim bValid  As Boolean      'Boolean Validation Flag
    Dim bwasNothing  As Boolean      'Boolean Validation Flag

    bOldmb = mbDontChkClick
    IsValidCustClass = 1
    'Debug.Print Me.ActiveControl.Name
    
  'See if customer class was blank
    bwasNothing = (Len(Trim(txtCustClass.Tag)) = 0)
    moValidate.bReturnRecordset = True          'Return Recordset
    Set moValidate.oDmForm = moDmForm
    moValidate.iTab = kTabMain
    bValid = moValidate.ARCustClass(txtCustClass, lblCustClass, "CustClassKey", _
            mbAllowCustClassAOF, True, True)
    IsValidCustClass = Abs(CInt(bValid))
        
  'Selection was Valid and was Changed
    If bValid And moValidate.bWasChanged Then
      'Load Customer Class Defaults if user responds that they wish to
      'Don't ask if no changes were made
        If bwasNothing Then
            GetCustomerClassDefaults
        Else
            If giSotaMsgBox(Nothing, moClass.moSysSession, _
                        kmsgLoadCustClassDefaults) = kretYes Then
                GetCustomerClassDefaults
            End If
        End If
    End If

    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidCustClass"
    txtCustClass.Text = txtCustClass.Tag
    IsValidCustClass = 0
    SetHourglass False                'Run Hourglass
    mbDontChkClick = bOldmb
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidCustClass", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtInvoiceForm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtInvoiceForm, True
    #End If
'+++ End Customizer Code Push +++
    
    moLostFocus.IsValidControl txtInvoiceForm
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtInvoiceForm_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidInvoiceForm() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    IsValidInvoiceForm = 1
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabBTST
    IsValidInvoiceForm = Abs(CInt(moValidate.CIBusinessForm(txtInvoiceForm, lblInvoiceForm, "InvcFormKey", _
                mbAllowBusFormAOF, True, 2, False)))
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidInvoiceForm"
    txtInvoiceForm.Text = txtInvoiceForm.Tag
    IsValidInvoiceForm = 0
    gClearSotaErr
    Exit Function


'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidInvoiceForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function IsValidAckForm() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidAckForm = 1
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabSO
        
    If lkuAckForm.IsValid = False Then
        lkuAckForm.Text = lkuAckForm.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblAckForm)
        gbSetFocus Me, lkuAckForm
        IsValidAckForm = 0
    Else
        lkuAckForm.Tag = lkuAckForm.Text
    End If
    
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidAckForm"
lkuAckForm.Text = lkuAckForm.Tag
IsValidAckForm = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidAckForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidLblForm() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidLblForm = 1
    
   ' Set moValidate.oDmForm = moDmFormArAddr
   ' moValidate.iTab = kTabSO
        
    If lkuLabelForm.IsValid = False Then
        lkuLabelForm.Text = lkuLabelForm.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblLabelForm)
        gbSetFocus Me, lkuLabelForm
        IsValidLblForm = 0
    Else
        lkuLabelForm.Tag = lkuLabelForm.Text
    End If
    
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidLabelForm"
lkuLabelForm.Text = lkuLabelForm.Tag
IsValidLblForm = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidLabelForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidWhse() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidWhse = 1
    
    Set moValidate.oDmForm = moDmFormArAddr
    moValidate.iTab = kTabSO
        
    If lkuClosestWhse.IsValid = False Then
        lkuClosestWhse.Text = lkuClosestWhse.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblClosestWhse)
        gbSetFocus Me, lkuClosestWhse
        IsValidWhse = 0
    Else
        lkuClosestWhse.Tag = lkuClosestWhse.Text
    End If
    
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidWhse"
lkuClosestWhse.Text = lkuClosestWhse.Tag
IsValidWhse = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidWhse", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function IsValidPackListForm() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidPackListForm = 1
    
    'Set moValidate.oDmForm = moDmFormArAddr
   ' moValidate.iTab = kTabSO
        
    If lkuPackListForm.IsValid = False Then
        lkuPackListForm.Text = lkuPackListForm.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblPackListForm)
        gbSetFocus Me, lkuPackListForm
        IsValidPackListForm = 0
    Else
        lkuPackListForm.Tag = lkuPackListForm.Text
    End If
    
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidPackListForm"
lkuPackListForm.Text = lkuPackListForm.Tag
IsValidPackListForm = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidPackListForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidShipdays() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidShipdays = 1
    
    'Set moValidate.oDmForm = moDmFormArAddr
    'moValidate.iTab = kTabSO
        
   If numShipDays.Value < kMin Or numShipDays.Value > kMax Then
       numShipDays.Value = numShipDays.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblShipDays)
        gbSetFocus Me, numShipDays
        IsValidShipdays = 0
   Else
       numShipDays.Tag = numShipDays.Value
   End If
   
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidShipdays"
numShipDays.Text = numShipDays.Tag
IsValidShipdays = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidShipdays", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidPriceGroup() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidPriceGroup = 1
    
    'Set moValidate.oDmForm = moDmFormArAddr
    'moValidate.iTab = kTabAR
        
    If lkuPriceGroup.IsValid = False Then
        lkuPriceGroup.Text = lkuPriceGroup.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblPriceGroup)
        gbSetFocus Me, lkuPriceGroup
        IsValidPriceGroup = 0
    Else
        lkuPriceGroup.Tag = lkuPriceGroup.Text
    End If
    
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidPriceGroup"
lkuPriceGroup.Text = lkuPriceGroup.Tag
IsValidPriceGroup = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidPriceGroup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function IsValidSource() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
IsValidSource = 1
    
    'Set moValidate.oDmForm = moDmFormArAddr
    'moValidate.iTab = kTabAR
        
    If lkuSource.IsValid = False Then
        lkuSource.Text = lkuSource.Tag
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblSource)
        gbSetFocus Me, lkuSource
        IsValidSource = 0
    Else
        lkuSource.Tag = lkuSource.Text
    End If
    
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidSource"
lkuSource.Text = lkuSource.Tag
IsValidSource = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidSource", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function IsValidNatAcct() As Integer

'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim lNatAcctKey As Long
    
  'Validate the control
    IsValidNatAcct = 1
    If Not mbUseNationalAcct Then Exit Function
    
    If lkuNatAcct.Text <> "" Then  'Validate National Account if not empty
        'lNatAcctKey = gvCheckNull(moClass.moAppDB.Lookup("NationalAcctKey", "tarNationalAcct", "NationalAcctID = " & gsQuoted(lkuNatAcct.Text)), SQL_INTEGER)
        'If lNatAcctKey = 0 Then
        If IsEmpty(lkuNatAcct.KeyValue) Then
            lkuNatAcct.Text = lkuNatAcct.Tag
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblNationalAcct)
            gbSetFocus Me, lkuNatAcct
            IsValidNatAcct = 0
            Exit Function
        End If
    End If
        
    'Check to see if value has changed for saving
    If mlNatAcctKey <> glGetValidLong(lkuNatAcct.KeyValue) Then
        moDmForm.SetDirty True
    End If
    
    'Get National Account data for the new National Acct ID
    GetNatAcctInfo glGetValidLong(lkuNatAcct.KeyValue)
     
Exit Function
Resume

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidNatAcct"
lkuNatAcct.Text = lkuNatAcct.Tag
IsValidNatAcct = 0
gClearSotaErr
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCustomerMaint", "IsValidNatAcct", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub SetCountryMasks(NewCountry As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'  Desc:  Sets up Masks for Phone, fax and postal code based on the country
'  Parms: OldCountry - What The Country Was
'         NewCountry - What The Country Will Be
'***********************************************************************
    Dim sSQL        As String   'SQL String
    Dim rs          As New DASRecordSet   'SOTADAS Recordset Object
    Dim sPhoneMask  As String   'Phone mask
    Dim sZipMask    As String   'Postal Code
    Dim sPhone      As String
    Dim sFax        As String
    Dim sZip        As String
    Static bDid     As Boolean

    sPhone = txtPhone.Text
    sFax = txtFax.Text
    sZip = txtPostalCode.Text
    
  'Default Country use Default Masks
    If NewCountry = msDfltCountry Or _
              cboCountry.ListIndex = kItemNotSelected Then
        If Not bDid Then
            bDid = True
            sSQL = "SELECT PhoneMask, PostalCodeMask FROM tsmCountry WHERE CountryID = "
            sSQL = sSQL & kSQuote & msDfltCountry & kSQuote & ";"
            'Debug.Print sSQL
            Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
          
        'Masks found load variables
            If Not rs.IsEOF Then
                msDfltPhoneMask = gvCheckNull(rs.Field("PhoneMask"))
                msDfltZipMask = gvCheckNull(rs.Field("PostalCodeMask"))
            End If
        End If
        
        txtPhone.Mask = msDfltPhoneMask
        txtFax.Mask = msDfltPhoneMask
        txtPostalCode.Mask = msDfltZipMask
    Else                                        'Get masks from the database
        sSQL = "SELECT PhoneMask, PostalCodeMask FROM tsmCountry WHERE CountryID = "
        sSQL = sSQL & kSQuote & cboCountry.Text & kSQuote & ";"
          
        'Debug.Print sSQL
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
          
      'Masks found load variables
        If Not rs.IsEOF Then
            sPhoneMask = gvCheckNull(rs.Field("PhoneMask"))
            sZipMask = gvCheckNull(rs.Field("PostalCodeMask"))
        End If
          
      'Load Masks
        txtPhone.Mask = sPhoneMask
        txtFax.Mask = sPhoneMask
        txtPostalCode.Mask = sZipMask
      
      'Clean Up Sage MAS 500 DAS Recordset Object
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
    End If

    txtPhone.Text = sPhone
    txtFax.Text = sFax
    txtPostalCode.Text = sZip

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetCountryMasks", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub GetContactKey()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************
'  Desc: Loads Contact Key into tciContact, tarCustAddr contact
'        And tarCustAddr dflt contact
'****************************************************************
  
    Dim lContKey As Long        'Contact Key
  
  'Get the Next Contact Key and Assign
    With moClass.moAppDB
      .SetInParam "tciContact"              'Table to get key for
      .SetOutParam lContKey                 'Output contact key
      .ExecuteSP ("spGetNextSurrogateKey") 'get next surrogate key
      lContKey = .GetOutParam(2)            'retrieve surrogate key
      .ReleaseParams                        'relaease sp parameters
    End With
    
  'Load Contact Keys into data Manager Objects
    moDmForm.SetColumnValue "PrimaryCntctKey", lContKey         'set into ar Address object
    moDmFormArAddr.SetColumnValue "DfltCntctKey", lContKey      'set into ar Address object
    moDmContact.SetColumnValue "CntctKey", lContKey             'set into Contact object
    
  'Load entity Type and Customer key into contacts
    moDmContact.SetColumnValue "EntityType", kEntTypeARCustomer 'set into Contact object
        
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetContactKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function LoadCustDefaults(Optional FromCopyMgr As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************
'  Desc: Loads Customer Default Fields and controls
'  Parm: FromCopyMgr Optional parameter to re-use code if customer
'        is begin copied from another customer
'****************************************************************
    Dim lAddrKey As Long        'Address Key
    Dim lCustkey As Long        'Customer Key
    
  'Get the Next Address Key and Assign
    With moClass.moAppDB
      .SetInParam "tciAddress"              'Table Name for Address key
      .SetOutParam lAddrKey                 'Output Address key
      .ExecuteSP ("spGetNextSurrogateKey") 'Get next surrogate Address key
      lAddrKey = .GetOutParam(2)            'Get surrogate key
      .ReleaseParams                        'Release sp parameters
    End With
    
  'Load Address key into dm objects
    moDmForm.SetColumnValue "PrimaryAddrKey", lAddrKey      'Load Address key into primary Address
    moDmForm.SetColumnValue "DfltShipToAddrKey", lAddrKey   'Load Address key into ship to Address
    moDmForm.SetColumnValue "DfltBillToAddrKey", lAddrKey   'Load Address key into bill to Address
    moDmFormArAddr.SetColumnValue "AddrKey", lAddrKey       'Load Address key into ci Address
    moDmFormCiAddr.SetColumnValue "AddrKey", lAddrKey       'Load Address key into ar Address
    
  'Get the Next Customer Key and Assign
    With moClass.moAppDB
      .SetInParam "tarCustomer"             'Table to get key for
      .SetOutParam lCustkey                 'Output customer key
      .ExecuteSP ("spGetNextSurrogateKey") 'get next surrogate key
      lCustkey = .GetOutParam(2)            'retrieve surrogate key
      .ReleaseParams                        'relaease sp parameters
    End With
    
  'Load Customer Keys into data Manager Objects
    moDmForm.SetColumnValue "CustKey", lCustkey             'Set into customer form
    moDmFormArAddr.SetColumnValue "CustKey", lCustkey       'set into ar Address object
    moDmContact.SetColumnValue "CntctOwnerKey", lCustkey    'set into Contact object
    
  'Load Contact Key
    GetContactKey

  'default the create type--v3
    moDmForm.SetColumnValue "CreateType", kCreateTypeStandard

  ' If this routine is called from the Copy Manager, we do not need to
  ' perform the remainder of the logic in this routine.
    If (FromCopyMgr) Then
      Exit Function
    End If
  
    ' Set the default create check value
    If muCustDflts.CheckCreditLimit Then
        mbSkipSecurityEventCheck = True
        chkCreditLimitUsed.Value = vbChecked
        mbSkipSecurityEventCheck = False
    Else
        mbSkipSecurityEventCheck = True
        chkCreditLimitUsed.Value = vbUnchecked
        mbSkipSecurityEventCheck = False
    End If
  
  'Get default customer class defaults if a
  '  Default Customer Class setup in AR Options
    If Len(Trim(muCustDflts.CustClassKey)) > 0 Then
      'Load Customer class defaults
        moDmForm.SetColumnValue "CustClassKey", muCustDflts.CustClassKey
        GetCustomerClassDefaults
    End If
    
  'Load default bill to and ship to Address id's = cust id
    txtBillTo.Text = txtSaveCustID.Text            'Default bill to Address
    txtShipTo.Text = txtSaveCustID.Text            'Default ship to Address
    txtBillTo.Tag = txtBillTo.Text                 'Set tags
    txtShipTo.Tag = txtShipTo.Text                 'Set up tags
 
  'Load default Currency, Country and masks
    txtCurrency.Text = msDfltCurrency
    cboCountry.ListIndex = giListIndexFromText(cboCountry, msDfltCountry)
    txtCurrency.Tag = txtCurrency.Text              'Set Tags
    cboCountry.Tag = cboCountry.ListIndex           'Set Tags
    ddnShipPriority.ListIndex = 2
    ddnShipPriority.Tag = ddnShipPriority.ListIndex
    SetupStateCombo                                 'Load State Combo Box
    SetCountryMasks (cboCountry.Text)               'Setup Country Masks
    numShipDays.Value = 3
    ddnPriceBase.ListIndex = ddnPriceBase.GetIndexByListItem("List Price")  'List Price
    
  'Set date Established to today
    txtDateEstablished.Text = Format(moClass.moSysSession.BusinessDate, _
                                                gsGetLocalVBDateMask())
    txtDateEstablished.Tag = txtDateEstablished.Text  'set Tag
    
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadCustDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub cmdSalesTax_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdSalesTax, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Static bDid As Boolean
'*******************************************************************************
' Desc: Show Sales Tax Schedule Form and allow user to update (if not Deleted)
'*******************************************************************************
    If moDmForm.State = kDmStateAdd Or moDmForm.State = kDmStateEdit Then
        If Not moLostFocus.IsValidDirtyCheck() Then
            Exit Sub
        End If
        If Not bDid Then
            frmSalesTax.Top = frmCustomerMaint.Top + 300
            frmSalesTax.Left = frmCustomerMaint.Left
            bDid = True
        End If
        
      'tell pop up form if current customer is in a Deleted Status
        If cboStatus.ListIndex = kItemNotSelected Then
            frmSalesTax.iStatus = 0
        Else
            frmSalesTax.iStatus = cboStatus.ItemData(cboStatus.ListIndex)
        End If
        mbDontChkClick = False
        frmSalesTax.Setup
       
        frmSalesTax.Show vbModal
    End If
        
    frmSalesTax.bBadSalesTax = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdSalesTax_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub SetTags()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************************
' Desc: Set Tags/Old Values for Validated fields
'****************************************************************************
    
    Dim iLoop       As Integer     'looping variable
    Dim iCtlIndex   As Integer     'User field control control array index
    
  'Static Lists (Combo Boxes)
    cboCredLimitAgeCat.Tag = cboCredLimitAgeCat.ListIndex  'Credit Limit Aging category
    
  'Check Boxes
    chkOnHold.Tag = chkOnHold.Value                 'On Hold
    
  'Sage MAS 500 Currency Controls
    curCreditLimit.Tag = curCreditLimit.Amount      'Credit Limit
    curLateChgAmt.Tag = curLateChgAmt.Amount        'Finance Charge Flat Amount
    
    'need a control for original credit limit in order to get the currency mask for error message,
    'no other way to easily do this.
    curOrigCreditLimit.Amount = curCreditLimit.Amount

    
  'Sage MAS 500 Number Controls (percents)
    numFinChgPct.Tag = numFinChgPct.Value          'Finance Charge Percent
    numTradeDiscPct.Tag = numTradeDiscPct.Value    'Trade Discount Percent
    numShipDays.Tag = numShipDays.Value            ' Ship Days
  
  'Sage MAS 500 Masked Edit date Control
    txtDateEstablished.Tag = txtDateEstablished.Text    'date Established
  
  'Set Up Surrogate Key Validated TextBoxes
    txtBillTo.Tag = txtBillTo.Text                  'Default Bill To Address ID
    txtContName.Tag = txtContName.Text                'Default Contact Name
    txtDfltItem.Tag = txtDfltItem.Text              'Default Item ID
    txtSalesperson.Tag = txtSalesperson.Text        'Salesperson ID
    txtShipTo.Tag = txtShipTo.Text                  'Default Ship To Address ID
    glaAccount.Tag = glaAccount.Text                'Default Sales Account
    glaReturns.Tag = glaReturns.Text                'DefaultReturnsAccount
    txtShipMeth.Tag = txtShipMeth.Text              'Shipping Method ID
    txtStmtForm.Tag = txtStmtForm.Text              'Statement Form ID
    txtVendor.Tag = txtVendor.Text                  'Vendor ID
    txtCustClass.Tag = txtCustClass.Text            'Customer Class ID
    txtInvoiceForm.Tag = txtInvoiceForm.Text        'Invoice Form ID
    txtFOB.Tag = txtFOB.Text                        'FOB--v3
    lkuPriceGroup.Tag = lkuPriceGroup.Text          'Price Group
    ddnPriceBase.Tag = ddnPriceBase.ListIndex       'Price Base
    nbrPriceAdjPct.Tag = nbrPriceAdjPct.Value       'Price Adjustment Percent
    ddnShipPriority.Tag = ddnShipPriority.ListIndex 'Ship Priority
    
    lkuAckForm.Tag = lkuAckForm.Text                'Acknowledgement Form
    lkuLabelForm.Tag = lkuLabelForm.Text            'Label Form
    lkuPackListForm.Tag = lkuPackListForm.Text      'Packing List Form
    lkuClosestWhse.Tag = lkuClosestWhse.Text        'Closest Warehouse
    lkuSource.Tag = lkuSource.Text                  'Sales Source
    
  'National Acct
    lkuNatAcct.Tag = lkuNatAcct.Text                'National Account ID
    cboNatDfltBillTO.Tag = cboNatDfltBillTO.Text    'National Account Bill To Option(Parent,Self)
    chkNatAllowParentPay.Tag = chkNatAllowParentPay.Value 'Allow parent of National Account to pay
    chkNatConsolidatedStat.Tag = chkNatConsolidatedStat.Value  'Produce Consolidated Statement
    
  'Set Up Validate Fields(No surrogate key)
    txtSic.Tag = txtSic.Text                        'Standard Industry Code ID
    txtPostalCode.Tag = txtPostalCode.Text          'Postal Code
    
  'Dynamic List Combo Boxes
    cboCommPlan.Tag = cboCommPlan.ListIndex             'Commission Plan ID
'v3    cboPriceList.Tag = cboPriceList.ListIndex        'Price List ID
    frmSalesTax.lkuSTaxSchd.Tag = _
                         CStr(frmSalesTax.lSTaxSchdKey) 'Sales Tax Schedule ID
    cboStmtCycle.Tag = cboStmtCycle.ListIndex           'Statement Cycle ID
    cboShipZone.Tag = cboShipZone.ListIndex             'Ship Zone ID
    cboSalesTerritory.Tag = cboSalesTerritory.ListIndex 'Sales Territory ID
    cboCurrExchSchd.Tag = cboCurrExchSchd.ListIndex     'Currency exchange Schedule ID
    txtCurrency.Tag = txtCurrency.Text                  'Currency ID
    cboState.Tag = cboState.ListIndex                   'State Code ID
    cboCountry.Tag = cboCountry.ListIndex               'Country Code ID
  
  
  'Set user field Old Values (tags are used for Hold control index values)
    For iLoop = 0 To 3
        iCtlIndex = moUserFlds.CtlIndex(iLoop)   'Get Control index from user field Class
        Select Case moUserFlds.DataType(iLoop)   'Get Control data Type from user field Class
            
            Case kvAlphanumeric  'Alpha Numeric data Type - Text Box Control
                moUserFlds.LetOldValue iLoop, txtUserFld(iCtlIndex)
            
            Case kvNumeric       'Numeric data Type - Sage MAS 500 Number Control
                moUserFlds.LetOldValue iLoop, numUserFld(iCtlIndex).Value
            
            Case kvDate          'date data Type - Sage MAS 500 Masked Edit Control
                moUserFlds.LetOldValue iLoop, dteUserFld(iCtlIndex)
            
            Case kvValidated     'Validated data Type - Combo Box Control
                moUserFlds.LetOldValue iLoop, cboUserFld(iCtlIndex).ListIndex
        End Select
    Next iLoop
    
    txtCustName.Tag = txtCustName.Text
    txtCRMCustID.Tag = txtCRMCustID.Text
    
    ddnDfltCarrierBillMeth.Tag = ddnDfltCarrierBillMeth.List
    ddnFreightMethod.Tag = ddnFreightMethod.List
    
    lkuDefaultCreditCard.Tag = lkuDefaultCreditCard.Text          'Credit Card
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetTags", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bSkipValidation(ctlLostFocus As Control) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Desc:    Tells Whether You want to skip Validation based on the
'          Control which lost focus and the control you are going to
'          Don't validate if you are going to Cancel, Delete, Help
'          Or a text control's associated navigator
' Parms:   ctlLostFocus - The Control which lost focus
' Returns: True  - Run validation
'          False - Do Not Run Validation
'***********************************************************************
    
    Dim ctlMap As Control  'Navigator that control may be mapped to
    
    bSkipValidation = False  'Initialize function return
    
  'Skip Validation on Cancel, Delete & Help buttons
    If Me.ActiveControl Is tbrMain Then
        Set moLastControl = ctlLostFocus
        bSkipValidation = True
    Else
      'If You are Going From Customer class to the main navigator
      'or back then skip validation
        Select Case True
            Case ctlLostFocus Is txtCustomerID         'Moving From Customer ID TextBox
                If Me.ActiveControl Is navMain Then    '  To Main Navigator
                    bSkipValidation = True             '  Skip Validation
                End If
          
          'Moving From Main Navigator To Customer ID or Next New TextBox
            Case ctlLostFocus Is navMain
                If Me.ActiveControl Is txtCustomerID Or _
                   Me.ActiveControl Is tbrMain.Buttons(kTbHelp) Then
                    bSkipValidation = True                 'Skip Validation
                End If
            
            Case Else                               'Check if moving to associated Navigator
              'Search Collection of Mapped Controls for an Associated Navigator
              'if associated Navigator = control which has focus Skip Validation
                On Error Resume Next
                Set ctlMap = moMapSrch(CStr(ctlLostFocus.hwnd))
                On Error GoTo ExpectedErrorRoutine
                If Me.ActiveControl Is ctlMap Then
                    bSkipValidation = True
                End If
        End Select
    End If
    
ExpectedErrorRoutine:
    gClearSotaErr
    Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSkipValidation", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub SetupAgeCatCombo()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc:    Loads the credit Limit combo box with the AR Options Units
'          and descriptions chosen in ar options
'***********************************************************************

Dim iLoop    As Integer    'Looping Variable

  'Set up Aging category static List
    miDfltAgeCat = giFillStaticList(moClass.moAppDB, moClass.moSysSession, _
                    cboCredLimitAgeCat, "tarCustClass", "CreditLimitAgeCat")

   'Replace the aging category list Item to
   'the Description set up in AR Options
    For iLoop = 1 To 4
        If Len(muCustDflts.AgeCat(iLoop)) > 0 Then
            cboCredLimitAgeCat.List(iLoop) = gsGetValidStr((muCustDflts.AgeCat(iLoop)) & " " & muCustDflts.AgeUnit)
        End If
    Next iLoop

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupAgeCatCombo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub SetupBillingTypeCombo()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Loads the Billing Type Combo box with the Billing Types
'       Allowed From AR Options:
'         Balance Forward - Only Balance Forward in Combo Box
'         Open Item       - Only Open Item in Combo Box
'         Both            - Balance Forward & Open Item in Combo Box
'***********************************************************************

    Dim sSQL     As String     'SQL String
    Dim rs       As New DASRecordSet     'Record Set Object
    Dim iLoop    As Integer    'Looping Variable

    cboBillingType.Clear
   
    If muCustDflts.BillingTypeOpt = kvBoth Then
      
      'Set up all Billing Type Options
        miDfltBillType = giFillStaticList(moClass.moAppDB, moClass.moSysSession, _
                cboBillingType, "tarCustClass", "BillingType")
    
    ElseIf muCustDflts.BillingTypeOpt = kvBalanceForward Or _
          muCustDflts.BillingTypeOpt = kvOpenItem Then

      'Setup query to get Aging Unit From The static List and local string tables
        sSQL = "#smGetStaticListLocalText;" & kSQuote
        sSQL = sSQL & "tarCustClass" & kSQuote & ";" & kSQuote
        sSQL = sSQL & "BillingType" & kSQuote & ";" & muCustDflts.BillingTypeOpt
        sSQL = sSQL & ";" & moClass.moSysSession.Language & ";"
    
      
      'Run Query to Get Billing Type Text
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
      'Place Value into the Combo Box for the one chosen
        If Not rs.IsEOF Then
            miDfltBillType = 0
            cboBillingType.AddItem gvCheckNull(rs.Field("LocalText"))
            cboBillingType.ItemData(0) = muCustDflts.BillingTypeOpt
        End If

      'Clear out recordset object
        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
   
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBillingTypeCombo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub SetupShipZone()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc:     Setup shipping zone combo box
'*****************************************************************************

Dim bOldClick As Boolean  'Old dont check click event flag Value

    bOldClick = mbDontChkClick   'Capture flag Value
    
    mbDontChkClick = True        'dont check click event
    
    cboShipZone.ListIndex = kItemNotSelected                        'Clear Value of Ship Zone Combo Box
      
  'reset ship Zone values
    cboShipZone.Clear
    
    If Len(txtShipMeth) > 0 Then
      'reset ship Zone values
        If mbSOActive Or (mbPOActive And 1 = 2) Then
            gRefreshCIShipZone moClass.moAppDB, moClass.moSysSession, cboShipZone, _
                 mbAllowShipZoneAOF, CBool(Val(txtMustUseZone.Text)), _
                 moDmFormArAddr.GetColumnValue("ShipMethKey")
        End If
    End If

    cboShipZone.Tag = cboShipZone.ListIndex      'Ship Zone old Value
    mbDontChkClick = bOldClick                   'reset dont check click event

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupShipZone", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupShipPriority()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc:     Setup Sales Source combo box
'*****************************************************************************

Dim bOldClick As Boolean  'Old dont check click event flag Value

    bOldClick = mbDontChkClick   'Capture flag Value
    
    mbDontChkClick = True        'dont check click event
    
    ddnShipPriority.ListIndex = kItemNotSelected                        'Clear Value of Ship Zone Combo Box
      
  'reset ship Zone values
    ddnShipPriority.Clear
    With ddnShipPriority
        
        .AddItem "1", 0
        .AddItem "2", 1
        .AddItem "3", 2
        .AddItem "4", 3
        .AddItem "5", 4
    End With
    
   
        

    ddnShipPriority.Tag = ddnShipPriority.ListIndex      'Ship Zone old Value
    mbDontChkClick = bOldClick                   'reset dont check click event

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupShipPriority", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub SetupStateCombo()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc:     Setup State Combo zone combo box
'*****************************************************************************
    
    If cboCountry.ListIndex = kItemNotSelected Then
        If Len(msDfltCountry) > 0 Then
          gRefreshCIState moClass.moAppDB, moClass.moSysSession, cboState, _
                    mbAllowStateAOF, False, msDfltCountry
        Else
            cboState.Clear
        End If
    Else
          gRefreshCIState moClass.moAppDB, moClass.moSysSession, cboState, _
                    mbAllowStateAOF, False, cboCountry.Text
    End If

    cboState.Tag = cboState.ListIndex

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupStateCombo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function DMPostSave(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If

'+++ VB/Rig End +++

   Dim sSQL As String
   Dim lParentKey As Long
   Dim lCustomerKey As Long
   
    mbSaved = True
    DMPostSave = True
    
  
    frmExemptions.SaveExemptions
    
    If mbUseNationalAcct Then
   
        lCustomerKey = glGetValidLong(moDmForm.GetColumnValue("CustKey"))
  
        'update tarNationalAcctMember table based on national acct modifications
        If mlNatAcctKey <> glGetValidLong(lkuNatAcct.KeyValue) Then
                
                If IsEmpty(lkuNatAcct.KeyValue) Then
                  'Removed national acct key --> Delete row
                    sSQL = "DELETE FROM tarNationalAcctMember WHERE ChildCustKey = " & lCustomerKey
                    On Error Resume Next
                    moClass.moAppDB.ExecuteSQL (sSQL)
                    'Debug.Print "error:" & Err & " descrip:" & Err.Description
                Else
                    lParentKey = glGetValidLong(moClass.moAppDB.Lookup("CustKey", "tarCustomer", "CompanyID = " & gsQuoted(msCompanyID) & "AND CustID = " & gsQuoted(txtNatParentCustID)))
                    If mlNatAcctKey = 0 Then
                        'Insert the new row back
                          sSQL = "INSERT INTO tarNationalAcctMember (ParentCustKey,ChildCustKey) "
                          sSQL = sSQL & "VALUES (" & lParentKey & ", " & lCustomerKey & ")"
                          On Error Resume Next
                          moClass.moAppDB.ExecuteSQL (sSQL)
                          'Debug.Print "error:" & Err & " descrip:" & Err.Description
                    Else
                        'Updated national acct key --> First Remove the old row before inserting the new row back in
                          sSQL = "DELETE FROM tarNationalAcctMember WHERE ChildCustKey = " & lCustomerKey
                          On Error Resume Next
                          moClass.moAppDB.ExecuteSQL (sSQL)
                          'Debug.Print "error:" & Err & " descrip:" & Err.Description
                        'Insert the new row back
                          sSQL = "INSERT INTO tarNationalAcctMember (ParentCustKey,ChildCustKey) "
                          sSQL = sSQL & "VALUES (" & lParentKey & ", " & lCustomerKey & ")"
                          On Error Resume Next
                          moClass.moAppDB.ExecuteSQL (sSQL)
                          'Debug.Print "error:" & Err & " descrip:" & Err.Description
                    End If
                End If
              'Set the National Account Module Level Variables since our save is committed
                mlNatAcctKey = glGetValidLong(lkuNatAcct.KeyValue)
                
           End If
           
           If glGetValidLong(lkuNatAcct.KeyValue) = 0 Then
              cboNatDfltBillTO.ListIndex = 2
          End If
           
           If mlNatAcctKey = 0 Then miNatUserLevel = 0
    End If
    
    '******************************************************************************
    '******************************************************************************
    'SGS DEJ 6/14/2010 Insert new customers into SLX    (START)
    '******************************************************************************
    '******************************************************************************
    Dim CustKey As Long
    Dim CustID As String
    Dim CustName As String
    
    Dim AddrKey As Long
    Dim AddrName As String
    Dim AddrLine1 As String
    Dim AddrLine2 As String
    Dim AddrLine3 As String
    Dim AddrLine4 As String
    Dim City As String
    Dim State As String
    Dim Zip As String
    Dim Country As String
    
    CustKey = glGetValidLong(moDmForm.GetColumnValue("CustKey"))
    CustID = Trim(txtCustomerID.Text)
    CustName = Trim(txtCustName.Text)
    
    AddrKey = glGetValidLong(moDmFormCiAddr.GetColumnValue("AddrKey"))
    
    AddrName = Trim(txtCustName.Text)
    AddrLine1 = Trim(txtAddressLine(1).Text)
    AddrLine2 = Trim(txtAddressLine(2).Text)
    AddrLine3 = Trim(txtAddressLine(3).Text)
    AddrLine4 = Trim(txtAddressLine(4).Text)
    City = Trim(txtCity.Text)
    State = Trim(cboState.Text)
    Zip = Trim(txtPostalCode.Text)
    Country = Trim(cboCountry.Text)
    
    ProcessSLXData oClass, msCompanyID, CustKey, CustID, CustName, AddrKey, AddrName, AddrLine1, AddrLine2, AddrLine3, AddrLine4, City, State, Zip, Country
            
    '******************************************************************************
    '******************************************************************************
    'SGS DEJ 6/14/2010 Insert new customers into SLX    (STOP)
    '******************************************************************************
    '******************************************************************************
    
    
    SendSalespersonEmail
   
   
'+++ VB/Rig Begin Pop +++
Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMPostSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function DMPreSave(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************************************
'   Description
'       DMPreSave is called by the data Manager anytime there is a Save or
'       insert about to happen it occurs inside the transaction so you must roll
'       the transaction back before displaying an error message
'***************************************************************************

    DMPreSave = True
    Dim lRetVal         As Long
    Dim lPAInstalled    As Long
    Dim sErrMsg         As String
    Dim sHold           As String
    Dim sSQL            As String
    Dim rs              As Object
    Dim sCompanyID      As String
    Dim lNatlAcctLevelKey As Long
    
      
    If oDm Is moDmForm Then
        
        If CheckBeforeSave() = kDmSuccess Then

          'Put ID and Name into Address Name field
            If moDmForm.Columns("CustID").IsDirty Then
                moDmFormArAddr.SetColumnValue "CustAddrID", txtSaveCustID.Text
            End If
            
            If moDmForm.Columns("CustName").IsDirty Then
                moDmFormCiAddr.SetColumnValue "AddrName", txtCustName.Text
                moDmFormCiAddr.SetDirty True
            End If
            'Save National Acctount data  (saved 1st)
            If oDm Is moDmForm And mlNatAcctKey <> glGetValidLong(lkuNatAcct.KeyValue) Then
                If lkuNatAcct.Text = "" Then
                    moDmForm.SetColumnValue "NationalAcctLevelKey", Empty 'Set to Null
                Else
                  'Load NationalAcctLevel Key
                     lNatlAcctLevelKey = glGetValidLong(moClass.moAppDB.Lookup("NationalAcctLevelKey", "tarNationalAcctLevel WITH (NOLOCK)", "NationalAcctKey = " & lkuNatAcct.KeyValue & " AND NationalAcctLevel = " & kiNatSub))
                     moDmForm.SetColumnValue "NationalAcctLevelKey", lNatlAcctLevelKey 'set into ar NationalAcctLevelKey
                End If
            End If
            

          'No contacts data manager object exists(saved 2nd)- create one
            If moDmForm.State = kDmStateEdit Then
              'No current contact
                If gvCheckNull(moDmForm.GetColumnValue("PrimaryCntctKey"), SQL_INTEGER) = 0 Then
                    
                    If moDmContact.RowLoaded = False Then
                      'Load Contact Key
                        GetContactKey
                      'Load Customer keys
                        moDmContact.SetColumnValue "CntctOwnerKey", moDmForm.GetColumnValue("CustKey")
                        moDmContact.SetColumnValue "EntityType", kEntTypeARCustomer
'                        moDmContact.KeyChange
                    End If
                    
                End If
            
            End If
            
            
        If chkPrintOrdAck = vbChecked Then
            If lkuAckForm.Text = "" Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, lblAckForm
                DMPreSave = False
                tabC.Tab = kTabSO
                lkuAckForm.SetFocus
                Exit Function
            End If
        End If
        If chkInvoiceReqd.Value = vbChecked Then
            If Len(Trim(txtInvoiceForm)) = 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgSelectInvoiceForm
                tabC.Tab = kTabBTST
                gbSetFocus Me, txtInvoiceForm
            End If
        End If
        
        Else
            DMPreSave = False
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMPreSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
   
Public Function DMBeforeInsert(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMBeforeInsert is called by the data Manager from within the
'   Finish method - if a user attemts to insert a row to the contact database
'                   that is not ready it will create the necessary fields
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
    Dim sSQL        As String
    Dim sCompanyID  As String
    Dim rs          As Object
    
  'contacts data manager object (saved 2nd)
    If oDm Is moDmContact Then
      'No current contact
        If gvCheckNull(moDmContact.GetColumnValue("CntctKey"), SQL_INTEGER) = 0 Then
           'Load Contact Key
             GetContactKey
           'Load Customer keys
             moDmContact.SetColumnValue "CntctOwnerKey", moDmForm.GetColumnValue("CustKey")
             moDmContact.SetColumnValue "EntityType", kEntTypeARCustomer
        End If
    End If
    
    DMBeforeInsert = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMBeforeInsert", VBRIG_IS_FORM
        Set rs = Nothing
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Sub DMReposition(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMReposition is called by the data Manager from within the
'   KeyChange method, after attempting to select a recordset from
'   the database.  By the time this subroutine is called, the
'   DataManager's state has changed to either kDmStateAdd or
'   kDmStateEdit.  This routine is called whether or not the
'   record exists (in either add or edit state).
'
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
'====================================================================
'   **** Special NOTE ****
'   If you had any New Record Defaults in the SetFormControls for
'   and Add,AddAOF state, then you would move that code here.
'   If you had any variable/tag initializations, you would move that
'   code here from the SetForm controls.
'
'   The DMReposition subroutine is NOT REQUIRED by the data Manager.
'====================================================================
    Dim lCustkey As Long
    Dim sSTaxSchdKey
    Dim iIndex As Integer
        
    If oDm Is moDmForm Then
    
        Select Case oDm.State
        
        
            Case kDmStateEdit
            
        
                LoadMaskedCustNo                                'Load Unbound control
                
                'Setup The ship Zone combo box and load from Item data
                SetupShipZone
                cboShipZone.ListIndex = giListIndexFromItemData(cboShipZone, _
                                        gvCheckNull(moDmFormArAddr.GetColumnValue("ShipZoneKey"), SQL_INTEGER))
                cboShipZone.Tag = cboShipZone.ListIndex
               'Set up the currency exchange schedule combo box and load from Item data
                SetupCurrExchSchdCbo
                cboCurrExchSchd.ListIndex = giListIndexFromItemData(cboCurrExchSchd, _
                                        gvCheckNull(moDmForm.GetColumnValue("CurrExchSchdKey"), SQL_INTEGER))
                cboCurrExchSchd.Tag = cboCurrExchSchd.ListIndex
                 'Load percentage textboxes with 100 * Value
    
                numTradeDiscPct.Value = CDbl(gvCheckNull(moDmForm.GetColumnValue("TradeDiscPct"), _
                        SQL_FLOAT)) * 100
                numFinChgPct.Value = CDbl(gvCheckNull(moDmForm.GetColumnValue("FinChgPct"), _
                        SQL_FLOAT)) * 100
                numTradeDiscPct.Tag = numTradeDiscPct.Value
                numFinChgPct.Tag = numFinChgPct.Value
                SetCountryMasks (cboCountry.Text)           'Setup Masks for the country
                lCustkey = gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
                gSetMemoToolBarState moClass.moAppDB, lCustkey, _
                                 kEntTypeARCustomer, msCompanyID, tbrMain

                ' Copy feature storage variables
                m_SourceKey = lCustkey
                m_SourceAddrKey = moDmForm.GetColumnValue("PrimaryAddrKey")
                m_SourceCntcKey = glGetValidLong(moDmForm.GetColumnValue("PrimaryCntctKey"))
                m_AddressQuestion = False
                mbDMStateChanged = True
    
            Case kDmStateAdd
             
                ' Copy feature storage variables
                m_SourceKey = Empty
                m_SourceAddrKey = Empty
                m_SourceCntcKey = Empty
                m_AddressQuestion = False
             
        End Select
        
    End If
    
    If oDm Is moDmFormArAddr And oDm.State <> kDmStateNone Then
            SetupCurrExchSchdCbo
            If oDm.State = kDmStateEdit Then
                nbrPriceAdjPct.Value = gdGetValidDbl(moDmFormArAddr.GetColumnValue("PriceAdj")) * 100
                sSTaxSchdKey = moDmFormArAddr.GetColumnValue("STaxSchdKey")
                If Len(sSTaxSchdKey) = 0 Then
                    frmSalesTax.lSTaxSchdKey = -1
                Else
                    frmSalesTax.lSTaxSchdKey = CLng(sSTaxSchdKey)
                End If
                
            End If
    End If
       
    If oDm Is moDmFormArAddr Then
        'Set External Shiping System controls
        If Not bExtShipSystemON Then
            ddnDfltCarrierBillMeth.SetToDefault True
            txtDfltCarrierAcctNo.ClearData
            gDisableControls txtDfltCarrierAcctNo, ddnDfltCarrierBillMeth
'drop that restriction so that any person can use the �packaging� freight method even if they haven�t purchased StarShip
'       Else
'           If miExtShipSystem <> 1 Then
'               ddnFreightMethod.Refresh
'               iIndex = ddnFreightMethod.GetIndexByItemData(kStarShip)
'               ddnFreightMethod.RemoveItem (iIndex)
'           End If
        End If
        
        If oDm.State = kDmStateAdd Then
            ddnFreightMethod.SetToDefault True
            ddnDfltCarrierBillMeth.SetToDefault True
        End If
    End If
    
    SetTags
    moLostFocus.SetAllValid
'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
 Public Sub DMDataDisplayed(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMDataDisplayed is called by the data Manager from within the
'   KeyChange method, after displaying the contents of the recordset
'   from the database in the form's controls.
'   This routine is called ONLY if the record exists (edit state).
'
'   Parameters:
'       oDM <ini>   - is the DataManager Object that caused the
'                     DMDataDisplayed to be called.
'********************************************************************

On Error GoTo ExpectedErrorRoutine

  Dim vCRMAccount   As Variant
  Dim sCRMAccountID As String
  
  
'if customer uses national account, Obtain National Account data
  If (oDm Is moDmForm) And (oDm.State <> kDmStateNone) And (mbUseNationalAcct) Then
        SetNationalAcctdata
  End If
  
  If oDm Is moDmForm And mbCRMConnected Then
          
          sCRMAccountID = Trim(gsGetValidStr(moDmForm.GetColumnValue("CRMCustID")))
    
          If Not sCRMAccountID = "" Then
    
             On Error Resume Next
    
             vCRMAccount = moClass.moAppDB.Lookup("ISNULL(ACCOUNT,'')", "ACCOUNT", _
                  "ACCOUNTID = " & gsQuoted(Trim(sCRMAccountID)))
    
             If Err.Number <> 0 Then
                 txtCRMCustID.Protected = False
                 txtCRMCustID.Text = gsBuildString(kARDBNotAvailable, moClass.moAppDB, moClass.moSysSession)
                 txtCRMCustID.Enabled = False
             End If
    
             On Error GoTo ExpectedErrorRoutine
    
             If Not IsNull(vCRMAccount) Then
                 txtCRMCustID.Protected = False
                 txtCRMCustID.Text = vCRMAccount
                 txtCRMCustID.Protected = True
             Else
                 txtCRMCustID.Protected = False
                 txtCRMCustID.Text = gsBuildString(kARCRMCustIDDeleted, moClass.moAppDB, moClass.moSysSession)
                 txtCRMCustID.Protected = True
             End If
          Else
             txtCRMCustID.Protected = False
             txtCRMCustID.Text = ""
             txtCRMCustID.Protected = True
          End If
    End If
   
    If oDm Is moDmForm Then
        If ddnCCUpchargeType.ItemData = 0 Then
            lblAmount.Caption = "Amount"
        Else
            lblAmount.Caption = "Percent"
        End If
    End If
    
   SetTags
   moLostFocus.SetAllValid

'+++ VB/Rig Begin Pop +++
        Exit Sub

ExpectedErrorRoutine:
'

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DMStateChange(oDm As Object, iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************************************
'   Description
'       DMStateChange is called by the data Manager anytime there is a change
'       in the state of the record because of a KeyChange or some Type of action
'       (i.e. Save, Cancel, Delete)
'       This project is using the state change to set the class's UI Active
'       property.  Because some controls are not bound to the data manager,
'       they must be cleared when the state changes to none.
'***************************************************************************

'Enable/Disable form buttons based on form state
    Dim iDfltWhseKey    As Integer

     
    tbrMain.SetState (iNewState)
    
    'default the closest warehouse to the SO Option Default warehouse for new customer
    If iNewState = kDmStateNone And mbSOActive Then
        If lkuClosestWhse.Text = "" Then
            iDfltWhseKey = giGetValidInt(moClass.moAppDB.Lookup("DfltWhseKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID)))
            lkuClosestWhse.Text = gsGetValidStr(moClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & iDfltWhseKey))
        End If
    End If
    
    If (oDm Is moDmForm) And (oDm.State = kDmStateNone) And (mbUseNationalAcct) Then
      If gvCheckNull(moDmForm.GetColumnValue("NationalAcctLevelKey"), SQL_INTEGER) = 0 Then
        SetNationalAcctdata
      End If
    End If
    SetTags
    moLostFocus.SetAllValid
    
    If mlRunMode <> kContextAOF Then
        If iNewState = kDmStateNone Then 'Or mbPAActive Then
            tbrMain.Buttons(kTbRenameId).Enabled = False
            ' Copy From
            tbrMain.Buttons(kTbCopyFrom).Enabled = False
        
            ' Copy feature storage variables
            m_SourceKey = Empty
            m_SourceAddrKey = Empty
            m_SourceCntcKey = Empty
            m_AddressQuestion = False
        
        End If
        
        If Not mbAutoNum Then
            tbrMain.Buttons(kTbNextNumber).Enabled = False
        End If
        
        If miSecurityLevel = sotaTB_DISPLAYONLY Then
            tbrMain.Buttons(kTbNextNumber).Enabled = False
            ' Copy From
            tbrMain.Buttons(kTbCopyFrom).Enabled = False
        Else
            ' Copy From
            If (iNewState = kDmStateAdd) Or (iNewState = kDmStateEdit) Then
                tbrMain.Buttons(kTbCopyFrom).Enabled = True
            End If
        End If
        
    End If
    
    If oDm Is moDmForm And oDm.State <> kDmStateEdit Then
        txtCRMCustID.Protected = False
        txtCRMCustID.Text = ""
        txtCRMCustID.Protected = True
    End If
    
    If oDm Is moDmFormArAddr And oDm.State = kDmStateNone Then
        cboCurrExchSchd.ListIndex = kItemNotSelected
    End If

    ' AvaTax Integration - DMStateChange - enable/disable AvaAddr validation button
    If (oDm Is moDmForm Or oDm Is moDmFormArAddr) And mbAvaAddrEnabled Then
        cmdAvaAddr.Enabled = IIf(iNewState = kDmStateNone, False, True)
    End If
    ' AvaTax end

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function ToolBarSuccess()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Desc: Resets form State Back To Invalid
'********************************************************************
      
    moClass.lUIActive = kChildObjectInactive  'run class UI Activate
    txtCustomerID.Text = ""                   'Clear out Customer ID
    txtCustomerID.Tag = txtCustomerID.Text
    txtCustomerID.Protected = False              'Reenable Customer ID
    ClearUnboundControls                      'Clear Out Unbound controls
    SetupShipZone                             'Clear Out Ship Zone Combo
    SetCountryMasks msDfltCountry         'Reset up  Phone/Postal Code masks
    SetupStateCombo                           'Clear out State Combo
    gbSetFocus Me, txtCustomerID              'Set Focus Back On customer

    If mlRunMode = kContextAOF Then           'Hide form if called from AOF
        Me.Hide
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ToolBarSuccess", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub LoadMaskedCustNo(Optional CustID As String = "")
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************
' Desc: Gets Next Number from stored procedure if Automatic numbering is on
' Parm: CustID  Optional parameter to use this CustID instead of CustID in
'       the custid control
'**************************************************************************
    Dim iLoop       As Integer      'Looping Variable
    Dim iNumChars   As Integer      'Number of characters in mask
    Dim bNotNum     As Integer      'flag all characters are not numeric
    Dim sCustID     As String
    
  'Get Number of edit characters in mask
    For iLoop = 1 To Len(txtCustomerID.Mask)    'Loop Through each character in mask
        If Mid$(txtCustomerID.Mask, iLoop, 1) = "N" Or _
           Mid$(txtCustomerID.Mask, iLoop, 1) = "X" Then    'Check for edit character
            iNumChars = iNumChars + 1                       'Increment Counter
        End If
    Next iLoop
    
  'No Edit characters found so assume no mask - Set To max Characters
    If iNumChars = 0 Then
        If moClass.moAppDB.Lookup("Count(*) RecCount", "tsmCompanyModule", "ModuleNo = 19 AND Active = 1") > 0 Then
            iNumChars = 8              'Cust id = varchar(12)
        Else
            iNumChars = 12
        End If
    End If
    
  ' Since may use variable or control, assign to a generic variable to search
  If (Len(Trim(CustID)) > 0) Then
    sCustID = CustID
  Else
    sCustID = txtSaveCustID.Text
  End If
  
  'Check to see if there are any nonnumeric characters in this
    For iLoop = 1 To Len(sCustID)      'Loop through each character
        If InStr("0123456789", Mid(sCustID, iLoop, 1)) = 0 Then  'if a non-numeric character
            bNotNum = True                          'Set not numeric flag
        End If
    Next iLoop
    
    If bNotNum Then     'String was numeric
        If (Len(Trim(CustID)) > 0) Then
            ' No action
        Else
            txtCustomerID.Text = txtSaveCustID.Text
            txtCustomerID.Tag = txtCustomerID.Text
            txtSaveCustID.Tag = txtCustomerID.Text
        End If
    Else
        If (Len(Trim(CustID)) > 0) Then
            CustID = Right(CustID, iNumChars)
        Else
            'Truncate Customer id (on Left) for ID
             txtCustomerID.Text = Right(txtSaveCustID.Text, iNumChars)
             txtCustomerID.Tag = txtCustomerID.Text
             txtSaveCustID.Tag = txtCustomerID.Text
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadMaskedCustNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub EnableControls(bEnable As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iLoop       As Integer
    Dim iCtlIndex   As Integer

    If Not bEnable Then

      'Customer defaults
        gDisableControls txtCustClass, navCustClass, txtSic, navSic, _
                         txtVendor, navVendor, txtCustReference, _
                         txtABANo, txtDateEstablished
      
      'Credit defaults
        gDisableControls curCreditLimit, cboCredLimitAgeCat, chkOnHold, chkCreditLimitUsed

      'Address Block
        gDisableControls txtAddressLine(1), txtAddressLine(2), txtAddressLine(3), _
                         txtAddressLine(4), txtAddressLine(5), txtCity, _
                         cboState, txtPostalCode, navZip, cboCountry, txtLatitude, txtLongitude
        
      'Contacts Block
        gDisableControls txtContName, navContact, txtContTitle, txtPhone, _
                         txtPhoneExt, txtFax, txtFaxExt, txtEmail
    
      'Billing Defaults
         gDisableControls txtStmtForm, navStmtForm, cboStmtCycle, _
                          cboBillingType, txtDfltItem, _
                          navDfltItem, glaAccount, glaReturns, _
                          curLateChgAmt, numFinChgPct, numTradeDiscPct
                          'v3, cboPriceList,
             
      'User fields
        For iLoop = 0 To 3
            iCtlIndex = moUserFlds.CtlIndex(iLoop)   'Get Control index from user field Class
            Select Case moUserFlds.DataType(iLoop)   'Get Control data Type from user field Class
            
                Case kvAlphanumeric  'Alpha Numeric data Type - Text Box Control
                    gDisableControls txtUserFld(iCtlIndex)
            
                Case kvNumeric       'Numeric data Type - Sage MAS 500 Number Control
                    gDisableControls numUserFld(iCtlIndex)
            
                Case kvDate          'date data Type - Sage MAS 500 Masked Edit Control
                    gDisableControls dteUserFld(iCtlIndex)
            
                Case kvValidated     'Validated data Type - Combo Box Control
                    gDisableControls cboUserFld(iCtlIndex)
            End Select
        Next iLoop
        
        
      
      'Option Defaults
        gDisableControls chkAllowrefunds, chkAllowWriteOffs, chkDunningMsg, chkPOReq
      
      'Bill To Defaults
        gDisableControls txtBillTo, navBillTo, txtInvoiceForm, navInvoiceForm, _
                        cboLanguage, cboPmtTerms, ddnPriceBase, lkuPriceGroup, _
                        nbrPriceAdjPct
        
      'Ship To Defaults
        gDisableControls txtShipTo, navShipTo, txtShipMeth, navShipMeth, _
                        txtFOB, ddnDfltCarrierBillMeth, ddnFreightMethod, txtDfltCarrierAcctNo, _
                        chkResidential, cboShipZone
      
      'MultiCurrency Defaults
        gDisableControls txtCurrency, cboCurrExchSchd
    
      'Sales Defaults
        gDisableControls txtSalesperson, navSalesperson, cboCommPlan, cboSalesTerritory
          
      'Invoice Message Defaults
        gDisableControls txtInvoiceMessage
        
      'Sales Order tab Options
        gDisableControls chkShipComplete, chkReqAck, chkPrintOrdAck, lkuAckForm, lkuLabelForm, _
            lkuPackListForm, lkuSource, ddnShipPriority, numShipDays, lkuClosestWhse, chkInvtSubst, _
            chkCloseSOOnFirst, chkCloseSOLineOnFirst
            
      'National Acct Options
        gDisableControls lkuNatAcct, chkNatAcctCreditLimit, _
        chkNatHold, cboNatDfltBillTO, _
        chkNatAllowParentPay, chkNatConsolidatedStat
        
        
        
        

        
        gDisableControls lkuDefaultCreditCard
        
    Else
        gEnableControls txtCustName
        
      'Customer defaults
        gEnableControls txtCustClass, navCustClass, txtSic, navSic, _
                         txtCustReference, chkCreditLimitUsed, _
                         txtABANo, txtDateEstablished
        
        If mbAPActive Then
            gEnableControls txtVendor, navVendor
        End If
                
      'Credit defaults
        If chkCreditLimitUsed.Value = vbChecked Then
            gEnableControls curCreditLimit
        End If
      
        gEnableControls chkOnHold

        If muCustDflts.CheckCreditLimit Then
            gEnableControls cboCredLimitAgeCat
        End If

      'Address Block
        gEnableControls txtAddressLine(1), txtAddressLine(2), txtAddressLine(3), _
                         txtAddressLine(4), txtAddressLine(5), txtCity, _
                         cboState, txtPostalCode, navZip, cboCountry, txtLatitude, txtLongitude
        
      'Contacts Block
        gEnableControls txtContName, navContact, txtContTitle, txtPhone, _
                         txtPhoneExt, txtFax, txtFaxExt, txtEmail
    
      'Billing Defaults
         gEnableControls cboBillingType, txtDfltItem, _
                          navDfltItem, glaAccount, glaReturns, _
                          curLateChgAmt, numFinChgPct, numTradeDiscPct
        
        If muCustDflts.PrintStmts Then
             gEnableControls txtStmtForm, navStmtForm, cboStmtCycle
        End If
      
'v3        If mbIMActive Then
'            gEnableControls cboPriceList
'        End If
        
      'User fields
        For iLoop = 0 To 3
            iCtlIndex = moUserFlds.CtlIndex(iLoop)   'Get Control index from user field Class
        
            Select Case moUserFlds.DataType(iLoop)   'Get Control data Type from user field Class
                Case kvAlphanumeric  'Alpha Numeric data Type - Text Box Control
                    gEnableControls txtUserFld(iCtlIndex)
            
                Case kvNumeric       'Numeric data Type - Sage MAS 500 Number Control
                    gEnableControls numUserFld(iCtlIndex)
            
                Case kvDate          'date data Type - Sage MAS 500 Masked Edit Control
                    gEnableControls dteUserFld(iCtlIndex)
            
                Case kvValidated     'Validated data Type - Combo Box Control
                    gEnableControls cboUserFld(iCtlIndex)
            End Select
        Next iLoop
      
      'Option Defaults
        gEnableControls chkAllowrefunds, chkAllowWriteOffs, chkDunningMsg, chkPOReq
      
      'Bill To Defaults
        gEnableControls txtBillTo, navBillTo, cboLanguage, cboPmtTerms
        
        If mbIMActive Then
            gEnableControls lkuPriceGroup, ddnPriceBase, nbrPriceAdjPct
        End If
        
        If muCustDflts.PrintInvoices Then
            gEnableControls txtInvoiceForm, navInvoiceForm
        End If
        
      'Ship To Defaults
        gEnableControls txtShipTo, navShipTo, txtShipMeth, navShipMeth, txtFOB, chkResidential
                        
        If mbSOActive Or (mbPOActive And 1 = 2) Then
            gEnableControls cboShipZone
        End If
    
        If bExtShipSystemON Then
            gEnableControls txtDfltCarrierAcctNo, ddnDfltCarrierBillMeth
        End If

      'Sales Defaults
        If muCustDflts.UseSper Then
            gEnableControls txtSalesperson, navSalesperson, cboCommPlan
        End If
        
        gEnableControls cboSalesTerritory
      
      'MultiCurrency Defaults
        gEnableControls txtCurrency, cboCurrExchSchd
          
      'Invoice Message Defaults
        gEnableControls txtInvoiceMessage
        
      'Sales Order tab Options
        'If mbSOActive Then
            'tabC.TabVisible(kTabSO) = False
            'gEnableControls chkShipComplete
        'End If
        If mbSOActive Then
            gEnableControls chkShipComplete, chkReqAck, chkPrintOrdAck, lkuAckForm, lkuLabelForm, _
            lkuPackListForm, lkuSource, ddnShipPriority, numShipDays, _
            chkCloseSOOnFirst, chkCloseSOLineOnFirst, ddnFreightMethod, chkBOLReqd, _
            chkShipLabelsReqd, chkPackListReqd, chkPackListContentsReqd, chkInvoiceReqd, fraReqShipDoc
            
        End If
        
        If mbSOActive Then
            If mbIMActive Then
                gEnableControls lkuClosestWhse, chkInvtSubst
            End If
        End If
        
        If miCCActivated = 1 And mbStoreCreditCardInfo = True Then
            gEnableControls lkuDefaultCreditCard
        End If
        
        
        
      'Enable/disable the National account controls based on the user level
        EnableNatControls
   
        
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EnableControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function VMIsValidKey() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    VMIsValidKey = IsValidCustomer
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function VMIsValidControl(oCtl As Control) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim i As Integer
    
    
    
    VMIsValidControl = True
    
    
    For i = 0 To 3
        If oCtl Is moUserFlds.Control(i) Then
            If moUserFlds.DataType(i) = kvNumeric Then
                VMIsValidControl = IsValidUserfldNumber(moUserFlds.CtlIndex(i))
            ElseIf moUserFlds.DataType(i) = kvDate Then
                VMIsValidControl = IsValidUserfldDate(moUserFlds.CtlIndex(i))
            ElseIf moUserFlds.DataType(i) = kvValidated Then
                VMIsValidControl = IsValidUserFldValidated(moUserFlds.CtlIndex(i))
            End If
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    Next i
    
    
    Select Case True
        Case oCtl Is txtCustClass
            VMIsValidControl = IsValidCustClass
        Case oCtl Is txtSic
            VMIsValidControl = IsValidSIC
        Case oCtl Is txtVendor
            VMIsValidControl = IsValidVendor
        Case oCtl Is curCreditLimit
            VMIsValidControl = IsValidCreditLimit
        Case oCtl Is txtPostalCode
            VMIsValidControl = IsValidZip
        Case oCtl Is txtContName
            VMIsValidControl = IsValidContact
        Case oCtl Is txtStmtForm
            VMIsValidControl = IsValidStmtForm
        Case oCtl Is txtDfltItem
            VMIsValidControl = IsValidItem
        Case oCtl Is glaAccount
            VMIsValidControl = IsValidGLAccount
        Case oCtl Is glaReturns
            VMIsValidControl = IsValidRetAccount
        Case oCtl Is curLateChgAmt
            VMIsValidControl = IsValidLateChgAmt
        Case oCtl Is numFinChgPct
            VMIsValidControl = IsValidFinChgPct
        Case oCtl Is numTradeDiscPct
            VMIsValidControl = IsValidTradeDiscPct
        Case oCtl Is txtBillTo
            VMIsValidControl = IsValidBillTo
        Case oCtl Is txtInvoiceForm
            VMIsValidControl = IsValidInvoiceForm
        Case oCtl Is txtCurrency
            VMIsValidControl = IsValidCurrID
        Case oCtl Is txtSalesperson
            VMIsValidControl = IsValidSalesperson
        Case oCtl Is txtShipTo
            VMIsValidControl = IsValidShipTo
        Case oCtl Is txtShipMeth
            VMIsValidControl = IsValidShipMeth
        Case oCtl Is txtDateEstablished
            VMIsValidControl = IsValidDateEstablished
        Case oCtl Is txtFOB 'v3
            VMIsValidControl = IsValidFOB
        Case oCtl Is lkuPriceGroup
            VMIsValidControl = IsValidPriceGroup
        Case oCtl Is lkuAckForm
            VMIsValidControl = IsValidAckForm
        Case oCtl Is lkuLabelForm
            VMIsValidControl = IsValidLblForm
        Case oCtl Is lkuPackListForm
            VMIsValidControl = IsValidPackListForm
        Case oCtl Is lkuClosestWhse
            VMIsValidControl = IsValidWhse
        Case oCtl Is numShipDays
            VMIsValidControl = IsValidShipdays
        Case oCtl Is lkuSource
            VMIsValidControl = IsValidSource
        Case oCtl Is lkuNatAcct
            VMIsValidControl = IsValidNatAcct
        Case oCtl Is nbrPriceAdjPct
            VMIsValidControl = IsValidPriceAdjPct
        Case oCtl Is lkuDefaultCreditCard
            VMIsValidControl = IsValidCustDefaultCreditCard
        Case oCtl Is curDfltMaxUpcharge
            VMIsValidControl = IsValidDfltMaxUpcharge
        
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub SetupCurrExchSchdCbo()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lAddrKey    As Long
    Dim lCXSKey     As Long
    Dim lCustkey    As Long
    
    lAddrKey = gvCheckNull(moDmForm.GetColumnValue("PrimaryAddrKey"), SQL_INTEGER)
    lCustkey = gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)
    lCXSKey = gvCheckNull(moDmForm.GetColumnValue("CurrExchSchdKey"), SQL_INTEGER)

    gRefreshMCCurrExchSchd moClass.moAppDB, moClass.moSysSession, cboCurrExchSchd, _
                            mbAllowCurrExchSchdAOF, False, msCompanyID, 1, lCustkey, lAddrKey

    If lCXSKey <> 0 Then
        cboCurrExchSchd.ListIndex = giListIndexFromItemData(cboCurrExchSchd, lCXSKey)
    End If
        
    cboCurrExchSchd.Tag = cboCurrExchSchd.ListIndex

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupCurrExchSchdCbo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub SetNationalAcctdata()

#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If

    Dim lNatAcctLevelKey    As Long
    Dim lNatAcctKey         As Long
    Dim sNatAcctID          As String
  
    mlNatAcctKey = 0
    
    If glGetValidLong(moDmForm.GetColumnValue("NationalAcctLevelKey")) = 0 Then  'Not a national acct user
        miNatUserLevel = kiNatNone
        EnableNatControls
        Exit Sub
    End If
    
  'Obtain the NationalAcctKey and level
    lNatAcctLevelKey = glGetValidLong(moDmForm.GetColumnValue("NationalAcctLevelKey"))
    lNatAcctKey = glGetValidLong(moClass.moAppDB.Lookup("NationalAcctKey", "tarNationalAcctLevel WITH (NOLOCK) ", "NationalAcctLevelKey = " & lNatAcctLevelKey))
    miNatUserLevel = giGetValidInt(moClass.moAppDB.Lookup("NationalAcctLevel", "tarNationalAcctLevel WITH (NOLOCK) ", "NationalAcctLevelKey = " & lNatAcctLevelKey))
    
    mlNatAcctKey = lNatAcctKey                              'Used to determine when to save
    
  'Obtain National Account Info
    GetNatAcctInfo lNatAcctKey
    
    EnableNatControls                                       'Enable /disable control based on caller Account Level
    
    lkuNatAcct.Tag = lkuNatAcct.Text
    
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetNationalAcctdata", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select

End Sub
Private Sub EnableNatControls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++ 'Repository Error Rig  {1.1.1.0.0}

    If mbUseNationalAcct Then
        Select Case miNatUserLevel
            Case kiNatNone                                              'User is not a member of National Account
            
              'Show Parent data
                lblNatParentCustomer.Visible = False
                txtNatParentCustID.Visible = False
                txtNatParentCustDesc.Visible = False
              'Hide Subsidiary options
                fraNatSubsidiaryOptions.Visible = False
              'Enable the Lookup
                If cboStatus.ListIndex <> kItemNotSelected Then
                    If cboStatus.ItemData(cboStatus.ListIndex) <> kvDeleted Then
                        gEnableControls lkuNatAcct, cboNatDfltBillTO, chkNatAllowParentPay, chkNatConsolidatedStat
                    Else
                        gDisableControls lkuNatAcct
                    End If
                Else
                    gEnableControls lkuNatAcct, cboNatDfltBillTO, chkNatAllowParentPay, chkNatConsolidatedStat
                End If
                
              'Clear fields on form
                ClearNationalAcctTab
            
            Case kiNatSub                                               'User is a Subsidiary Account
            
             'Enable the Lookup if status is not deleted
                If cboStatus.ListIndex <> kItemNotSelected Then
                    If cboStatus.ItemData(cboStatus.ListIndex) <> kvDeleted Then
                        gEnableControls lkuNatAcct, cboNatDfltBillTO, chkNatAllowParentPay, chkNatConsolidatedStat
                    End If
                End If
              
              'Show Subsidiary options
                fraNatSubsidiaryOptions.Visible = True
                
            Case kiNatParent                                            'User is a parent National Account
                
              'Disable the Lookup for a parent caller
                gDisableControls lkuNatAcct
              'Hide Subsidiary options for a parent
                'fraNatSubsidiaryOptions.Visible = False
                fraNatSubsidiaryOptions.Visible = True
        End Select
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCustomerMaint", "EnableNatControls", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetNatAcctInfo(lNatAcctKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}


    Dim rs                 As Object
    Dim sSQL               As String
    Dim lNatAcctParentKey  As Long
    
    sSQL = "SELECT NationalAcctID,CreditLimit,CreditLimitUsed,Description,Hold "
    sSQL = sSQL & "From tarNationalAcct WITH (NOLOCK) WHERE   NationalAcctKey = " & lNatAcctKey & " AND CompanyID = " & gsQuoted(msCompanyID)
        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
   
    If rs.IsEOF Then
        Set rs = Nothing
        ClearNationalAcctTab
        Exit Sub
    End If
    
    lkuNatAcct.Text = CStr(Trim(rs.Field("NationalAcctID")))
    txtNatAcctDesc.Text = CStr(Trim(rs.Field("Description")))
    chkNatAcctCreditLimit.Value = rs.Field("CreditLimitUsed")
    txtCurNatCreditLimit.Amount = gcGetValidCur(rs.Field("CreditLimit"))
    chkNatHold.Value = rs.Field("Hold")

    If miNatUserLevel = kiNatNone Then
        miNatUserLevel = kiNatSub
    End If

    Set rs = Nothing
        
    lNatAcctParentKey = glGetValidLong(moClass.moAppDB.Lookup("NationalAcctLevelKey", "tarNationalAcctLevel", "NationalAcctKey = " & lNatAcctKey & " AND NationalAcctLevel = " & kiNatParent))
    txtNatParentCustID.Text = gsGetValidStr(moClass.moAppDB.Lookup("CustID", "tarCustomer", "NationalAcctLevelKey = " & CStr(lNatAcctParentKey)))
    txtNatParentCustDesc.Text = gsGetValidStr(moClass.moAppDB.Lookup("CustName", "tarCustomer", "NationalAcctLevelKey = " & CStr(lNatAcctParentKey)))
    
    If lkuNatAcct.Enabled Then 'And fraNatSubsidiaryOptions.Visible = False Then
        fraNatSubsidiaryOptions.Visible = True
        If glGetValidLong(lkuNatAcct.KeyValue) = 0 Then           'set default BillTo to empty to ensure user will enter the field not based on default
            cboNatDfltBillTO.ListIndex = 2
        End If
    End If
    lblNatParentCustomer.Visible = True
    txtNatParentCustID.Visible = True
    txtNatParentCustDesc.Visible = True
    
    'added for test
    If miNatUserLevel = kiNatParent Then
        cboNatDfltBillTO.ListIndex = 2
        'fraNatSubsidiaryOptions.Visible = True
        gDisableControls cboNatDfltBillTO, chkNatConsolidatedStat, chkNatAllowParentPay, lblNatDfltBillTo
    Else
        gEnableControls cboNatDfltBillTO, chkNatConsolidatedStat, chkNatAllowParentPay, lblNatDfltBillTo
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCustomerMaint", "GetNatAcctInfo", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Function IsValidUFCheck() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iLoop As Integer

    IsValidUFCheck = 1

    For iLoop = 0 To 3
        If moUserFlds.DataType(iLoop) = kvNumeric Then
            If moUserFlds.GetOldValue(iLoop) <> moUserFlds.Control(iLoop) Then
                If IsValidUserfldNumber(moUserFlds.CtlIndex(iLoop)) = 0 Then
                    IsValidUFCheck = 0
                    Exit Function
                End If
            End If
        ElseIf moUserFlds.DataType(iLoop) = kvDate Then
            If moUserFlds.GetOldValue(iLoop) <> moUserFlds.Control(iLoop) Then
                If IsValidUserfldDate(moUserFlds.CtlIndex(iLoop)) = 0 Then
                    IsValidUFCheck = 0
                    Exit Function
                End If
            End If
        ElseIf moUserFlds.DataType(iLoop) = kvValidated Then
            If moUserFlds.GetOldValue(iLoop) <> moUserFlds.Control(iLoop) Then
                If IsValidUserFldValidated(moUserFlds.CtlIndex(iLoop)) = 0 Then
                    IsValidUFCheck = 0
                    Exit Function
                End If
            End If
        End If
    Next iLoop

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidUFCheck", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bSetupNavMain() As Boolean

    bSetupNavMain = False
    
    If Len(Trim(navMain.Tag)) = 0 Then
        bSetupNavMain = gbLookupInit(navMain, moClass, moClass.moAppDB, "Customer", msCompanyWhere)
        navMain.ColumnMask("CustID") = txtCustomerID.Mask
        If bSetupNavMain Then
            navMain.Tag = 1
        End If
    Else
        bSetupNavMain = True
    End If
    
End Function
Public Function CheckNationalAcct(lCustkey As Long) As Boolean
'****************************************************************************************
'Validates Qty Amounts passed in
'****************************************************************************************
  

  Dim iRet     As Integer
  
  CheckNationalAcct = False

  iRet = giGetValidInt(moClass.moAppDB.Lookup("1", "tarOptions O, tarCustomer C ", _
        "O.CompanyID = C.CompanyID AND O.UseNationalAccts = 1 AND C.NationalAcctLevelKey <> NULL AND C.CustKey = " & lCustkey))
        
  If iRet = 1 Then
    CheckNationalAcct = True
  End If
  
End Function

Public Sub CMMemoSelected(oCtl As Control)
'Called from  HandleToolBarClick
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim ireply As Integer
    Const vNoClear = True
    Dim lCustkey As Long

    lCustkey = gvCheckNull(moDmForm.GetColumnValue("CustKey"), SQL_INTEGER)

    If oCtl Is txtCustomerID Then
        'Abort if empty
        If Len(Trim(txtCustomerID)) = 0 Then Exit Sub
        If moDmForm.State = kDmStateAdd Then
            ireply = giSotaMsgBox(Me, moClass.moSysSession, kmsgSaveBeforeAdd, CVar(lblCustomerID))
            If ireply = kretYes Then
                'check for dirty form here with message box
                If moDmForm.Save(vNoClear) = kDmSuccess Then
                    'see DMAfterUpdate, DMAfterInsert
                    'ok
                Else
                    If frmSalesTax.bBadSalesTax Then
                        cmdSalesTax_Click
                    End If
                    Exit Sub 'Validation or some DM method aborted save
                End If
            Else
                Exit Sub 'Replied "No" to "Save?"
            End If
        End If
        gLaunchMemo Me, moClass.moFramework, moSotaObjects, kEntTypeARCustomer, lCustkey, _
                    txtCustomerID, txtCustName, moClass.moSysSession.BusinessDate
        gSetMemoToolBarState moClass.moAppDB, lCustkey, _
                kEntTypeARCustomer, msCompanyID, tbrMain
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMMemoSelected", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmCustomerMaint"
End Function
#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            'Before we initialize, set the custom tab to invisible.
            'The initialize will make visible if needed.
            'Pass into the initialize the name of the tab control and the index of the custom tab
            tabC.TabVisible(kTabCustomizer) = False
            moFormCust.Initialize Me, goClass, tabC.Name & ";" & kTabCustomizer
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyDataBindings moDmForm
            moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub cmdSalesTax_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSalesTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSalesTax_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSalesTax_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSalesTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSalesTax_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreditCards_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCreditCards, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCreditCards_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreditCards_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCreditCards, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCreditCards_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdExemptions_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdExemptions, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdExemptions_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdExemptions_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdExemptions, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdExemptions_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDocTrans_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDocTrans, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDocTrans_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDocTrans_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDocTrans, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDocTrans_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLaunchEMail_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdLaunchEMail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLaunchEMail_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdLaunchEMail_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdLaunchEMail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdLaunchEMail_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdContAddr_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdContAddr(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdContAddr_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdContAddr_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdContAddr(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdContAddr_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtMustUseZone_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMustUseZone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMustUseZone_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMustUseZone_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMustUseZone, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMustUseZone_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMustUseZone_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMustUseZone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMustUseZone_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMustUseZone_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMustUseZone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMustUseZone_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatAcctDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNatAcctDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatAcctDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatAcctDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNatAcctDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatAcctDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatAcctDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNatAcctDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatAcctDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatAcctDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNatAcctDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatAcctDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNatParentCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNatParentCustID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNatParentCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNatParentCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNatParentCustDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNatParentCustDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNatParentCustDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNatParentCustDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNatParentCustDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNatParentCustDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceMessage_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtInvoiceMessage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceMessage_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceMessage_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtInvoiceMessage, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceMessage_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceMessage_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtInvoiceMessage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceMessage_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceMessage_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtInvoiceMessage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceMessage_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltCarrierAcctNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDfltCarrierAcctNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltCarrierAcctNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltCarrierAcctNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDfltCarrierAcctNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltCarrierAcctNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltCarrierAcctNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDfltCarrierAcctNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltCarrierAcctNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltCarrierAcctNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDfltCarrierAcctNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltCarrierAcctNo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipTo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipTo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipTo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipTo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipTo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipTo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipTo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOB_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtFOB, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOB_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOB_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtFOB, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOB_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOB_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtFOB, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOB_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMeth_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMeth_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMeth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSperName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSperName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSperName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSperName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSperName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSperName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSperName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSperName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSperName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSperName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSperName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSperName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesperson_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSalesperson, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesperson_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesperson_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSalesperson, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesperson_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesperson_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSalesperson, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesperson_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurrency_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCurrency, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurrency_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurrency_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCurrency, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurrency_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurrency_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCurrency, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurrency_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTermsDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPmtTermsDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTermsDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPmtTermsDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTermsDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPmtTermsDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTermsDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPmtTermsDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBillTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillTo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBillTo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillTo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBillTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillTo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceForm_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtInvoiceForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceForm_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceForm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtInvoiceForm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceForm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceForm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtInvoiceForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceForm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtStmtForm_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtStmtForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtStmtForm_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtStmtForm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtStmtForm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtStmtForm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtStmtForm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtStmtForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtStmtForm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltItem_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDfltItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltItem_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltItem_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDfltItem, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltItem_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDfltItem_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDfltItem, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDfltItem_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserFld_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUserFld_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserFld_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtUserFld(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUserFld_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserFld_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUserFld_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUserFld_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUserFld_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhoneExt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPhoneExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhoneExt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhoneExt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPhoneExt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhoneExt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhoneExt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPhoneExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhoneExt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhoneExt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPhoneExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhoneExt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFaxExt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtFaxExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFaxExt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFaxExt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtFaxExt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFaxExt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFaxExt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtFaxExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFaxExt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFaxExt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtFaxExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFaxExt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtContName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtContName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtContName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContTitle_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtContTitle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContTitle_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContTitle_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtContTitle, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContTitle_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContTitle_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtContTitle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContTitle_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContTitle_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtContTitle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContTitle_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmail_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtEmail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmail_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmail_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtEmail, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmail_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmail_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtEmail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmail_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtEmail_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtEmail, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtEmail_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhone_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPhone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhone_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhone_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPhone, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhone_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhone_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPhone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhone_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPhone_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPhone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPhone_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFax_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtFax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFax_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFax_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtFax, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFax_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFax_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtFax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFax_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFax_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtFax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFax_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCity_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCity_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCity_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCity, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCity_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCity_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCity_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCity_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCity, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCity_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAddressLine_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtAddressLine(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAddressLine_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAddressLine_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtAddressLine(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAddressLine_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAddressLine_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtAddressLine(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAddressLine_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAddressLine_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtAddressLine(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAddressLine_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostalCode_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostalCode_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostalCode_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPostalCode, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostalCode_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostalCode_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPostalCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostalCode_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSic_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSic, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSic_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSic_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSic, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSic_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSic_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSic, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSic_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendor_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendor_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendor_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtVendor, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendor_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtVendor_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtVendor, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtVendor_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustReference_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustReference, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustReference_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustReference_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustReference, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustReference_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustReference_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustReference, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustReference_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustReference_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustReference, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustReference_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustClass_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustClass_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustClass_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustClass, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustClass_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustClass_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustClass_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtABANo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtABANo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtABANo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtABANo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtABANo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtABANo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtABANo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtABANo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtABANo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtABANo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtABANo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtABANo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCRMCustID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCRMCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCRMCustID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCRMCustID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCRMCustID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCRMCustID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCRMCustID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCRMCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCRMCustID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCRMCustID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCRMCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCRMCustID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSaveCustID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSaveCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSaveCustID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSaveCustID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSaveCustID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSaveCustID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSaveCustID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSaveCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSaveCustID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSaveCustID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSaveCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSaveCustID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustomerID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustomerID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustomerID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustomerID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustomerID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustomerID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustomerID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustomerID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustomerID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuNatAcct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuNatAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuNatAcct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuNatAcct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuNatAcct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuNatAcct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuNatAcct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuNatAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuNatAcct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuNatAcct_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuNatAcct, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuNatAcct_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuNatAcct_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuNatAcct, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuNatAcct_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuNatAcct_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuNatAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuNatAcct_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLabelForm_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuLabelForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLabelForm_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLabelForm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuLabelForm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLabelForm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLabelForm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuLabelForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLabelForm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLabelForm_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuLabelForm, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLabelForm_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuLabelForm_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuLabelForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuLabelForm_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPackListForm_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPackListForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPackListForm_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPackListForm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPackListForm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPackListForm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPackListForm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPackListForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPackListForm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPackListForm_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPackListForm, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPackListForm_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPackListForm_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPackListForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPackListForm_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAckForm_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuAckForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAckForm_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAckForm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuAckForm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAckForm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAckForm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuAckForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAckForm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAckForm_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuAckForm, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAckForm_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuAckForm_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuAckForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuAckForm_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuSource, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSource_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuSource, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSource_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuSource, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSource_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuSource, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSource_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSource_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuSource, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSource_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuClosestWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuClosestWhse_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuClosestWhse, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuClosestWhse_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuClosestWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuClosestWhse_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuClosestWhse, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuClosestWhse_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuClosestWhse_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuClosestWhse, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuClosestWhse_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuDefaultCreditCard, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuDefaultCreditCard, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuDefaultCreditCard, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuDefaultCreditCard, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuDefaultCreditCard_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuDefaultCreditCard, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuDefaultCreditCard_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceGroup_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceGroup_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceGroup_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPriceGroup, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceGroup_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceGroup_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceGroup_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceGroup_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPriceGroup, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceGroup_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPriceGroup_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPriceGroup, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPriceGroup_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub SOTACurrency2_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange SOTACurrency2, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "SOTACurrency2_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub SOTACurrency2_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress SOTACurrency2, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "SOTACurrency2_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub SOTACurrency2_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus SOTACurrency2, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "SOTACurrency2_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub SOTACurrency2_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus SOTACurrency2, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "SOTACurrency2_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurNatCreditLimit_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCurNatCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurNatCreditLimit_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurNatCreditLimit_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCurNatCreditLimit, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurNatCreditLimit_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurNatCreditLimit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCurNatCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurNatCreditLimit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCurNatCreditLimit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCurNatCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCurNatCreditLimit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShipDays_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numShipDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numShipDays_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShipDays_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numShipDays, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numShipDays_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numShipDays_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numShipDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numShipDays_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrPriceAdjPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrPriceAdjPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrPriceAdjPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrPriceAdjPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrPriceAdjPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrPriceAdjPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curDfltMaxUpcharge_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curDfltMaxUpcharge, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curDfltMaxUpcharge_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curDfltMaxUpcharge_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curDfltMaxUpcharge, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curDfltMaxUpcharge_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curDfltMaxUpcharge_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curDfltMaxUpcharge, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curDfltMaxUpcharge_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curLateChgAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curLateChgAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLateChgAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curLateChgAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curLateChgAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLateChgAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curLateChgAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curLateChgAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLateChgAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numFinChgPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numFinChgPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numFinChgPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numFinChgPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numFinChgPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numFinChgPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTradeDiscPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTradeDiscPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTradeDiscPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numUserFld_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numUserFld_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numUserFld_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numUserFld(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numUserFld_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numUserFld_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numUserFld_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCreditLimit_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCreditLimit_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCreditLimit_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curCreditLimit, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCreditLimit_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCreditLimit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCreditLimit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCreditLimit_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curOrigCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCreditLimit_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCreditLimit_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curOrigCreditLimit, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCreditLimit_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCreditLimit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curOrigCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCreditLimit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCreditLimit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curOrigCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCreditLimit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatHold_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkNatHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatHold_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatHold_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkNatHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatHold_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatHold_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkNatHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatHold_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatAcctCreditLimit_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkNatAcctCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatAcctCreditLimit_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatAcctCreditLimit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkNatAcctCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatAcctCreditLimit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatAcctCreditLimit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkNatAcctCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatAcctCreditLimit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatConsolidatedStat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkNatConsolidatedStat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatConsolidatedStat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatConsolidatedStat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkNatConsolidatedStat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatConsolidatedStat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatAllowParentPay_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkNatAllowParentPay, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatAllowParentPay_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNatAllowParentPay_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkNatAllowParentPay, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNatAllowParentPay_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInvoiceReqd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInvoiceReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInvoiceReqd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInvoiceReqd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInvoiceReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInvoiceReqd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPackListContentsReqd_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPackListContentsReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPackListContentsReqd_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPackListContentsReqd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPackListContentsReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPackListContentsReqd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPackListContentsReqd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPackListContentsReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPackListContentsReqd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPackListReqd_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPackListReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPackListReqd_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPackListReqd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPackListReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPackListReqd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPackListReqd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPackListReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPackListReqd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipLabelsReqd_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkShipLabelsReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipLabelsReqd_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipLabelsReqd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShipLabelsReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipLabelsReqd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipLabelsReqd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShipLabelsReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipLabelsReqd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBOLReqd_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkBOLReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkBOLReqd_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBOLReqd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkBOLReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkBOLReqd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBOLReqd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkBOLReqd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkBOLReqd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOLineOnFirst_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCloseSOLineOnFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOLineOnFirst_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOLineOnFirst_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCloseSOLineOnFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOLineOnFirst_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOOnFirst_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCloseSOOnFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOOnFirst_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCloseSOOnFirst_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCloseSOOnFirst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCloseSOOnFirst_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkShipComplete_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkShipComplete, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkShipComplete_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInvtSubst_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInvtSubst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInvtSubst_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInvtSubst_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInvtSubst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInvtSubst_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInvtSubst_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInvtSubst, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInvtSubst_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReqAck_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkReqAck, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReqAck_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReqAck_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkReqAck, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReqAck_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkReqAck_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkReqAck, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkReqAck_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintOrdAck_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPrintOrdAck, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintOrdAck_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintOrdAck_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPrintOrdAck, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintOrdAck_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPrintOrdAck_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPrintOrdAck, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPrintOrdAck_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkResidential_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkResidential, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkResidential_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkResidential_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkResidential, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkResidential_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkResidential_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkResidential, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkResidential_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPOReq_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPOReq, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPOReq_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPOReq_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPOReq, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPOReq_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPOReq_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPOReq, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPOReq_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowrefunds_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowrefunds, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowrefunds_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowrefunds_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowrefunds, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowrefunds_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowrefunds_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowrefunds, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowrefunds_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowWriteOffs_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkAllowWriteOffs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowWriteOffs_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowWriteOffs_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkAllowWriteOffs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowWriteOffs_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkAllowWriteOffs_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkAllowWriteOffs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkAllowWriteOffs_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDunningMsg_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkDunningMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDunningMsg_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDunningMsg_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkDunningMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDunningMsg_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDunningMsg_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkDunningMsg, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDunningMsg_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCreditLimitUsed_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCreditLimitUsed, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCreditLimitUsed_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCreditLimitUsed_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCreditLimitUsed, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCreditLimitUsed_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkOnHold_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkOnHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOnHold_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkOnHold_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkOnHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOnHold_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboNatDfltBillTO_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboNatDfltBillTO, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboNatDfltBillTO_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboNatDfltBillTO_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboNatDfltBillTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboNatDfltBillTO_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboNatDfltBillTO_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboNatDfltBillTO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboNatDfltBillTO_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboShipZone_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboShipZone, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboShipZone_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboShipZone_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboShipZone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboShipZone_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboShipZone_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboShipZone, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboShipZone_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSalesTerritory_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSalesTerritory, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSalesTerritory_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSalesTerritory_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSalesTerritory, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSalesTerritory_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSalesTerritory_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSalesTerritory, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSalesTerritory_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCommPlan_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboCommPlan, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCommPlan_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCommPlan_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboCommPlan, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCommPlan_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCommPlan_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboCommPlan, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCommPlan_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCurrExchSchd_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboCurrExchSchd, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCurrExchSchd_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCurrExchSchd_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboCurrExchSchd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCurrExchSchd_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCurrExchSchd_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboCurrExchSchd, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCurrExchSchd_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboLanguage_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboLanguage, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboLanguage_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboLanguage_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboLanguage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboLanguage_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboLanguage_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboLanguage, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboLanguage_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPmtTerms_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboPmtTerms, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPmtTerms_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPmtTerms_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboPmtTerms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPmtTerms_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPmtTerms_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboPmtTerms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPmtTerms_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboStmtCycle_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboStmtCycle, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboStmtCycle_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboStmtCycle_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboStmtCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboStmtCycle_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboStmtCycle_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboStmtCycle, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboStmtCycle_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPriceList_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboPriceList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPriceList_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPriceList_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboPriceList, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPriceList_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPriceList_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboPriceList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPriceList_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboPriceList_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboPriceList, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboPriceList_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboBillingType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboBillingType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboBillingType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboBillingType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboBillingType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboBillingType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboBillingType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboBillingType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboBillingType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboUserFld_Click(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboUserFld_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboUserFld_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboUserFld(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboUserFld_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboUserFld_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboUserFld_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCredLimitAgeCat_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboCredLimitAgeCat, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCredLimitAgeCat_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCredLimitAgeCat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboCredLimitAgeCat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCredLimitAgeCat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCredLimitAgeCat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboCredLimitAgeCat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCredLimitAgeCat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboState_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboState, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboState_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboState_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboState, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboState_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboState_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboState, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboState_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCountry_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboCountry, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCountry_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCountry_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboCountry, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCountry_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCountry_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboCountry, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCountry_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboStatus_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboStatus, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboStatus_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboStatus_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboStatus_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboStatus_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboStatus_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnShipPriority_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnShipPriority, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnShipPriority_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnShipPriority_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnShipPriority, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnShipPriority_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnShipPriority_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnShipPriority, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnShipPriority_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnShipPriority_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnShipPriority, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnShipPriority_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFreightMethod_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnFreightMethod, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFreightMethod_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFreightMethod_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnFreightMethod, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFreightMethod_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFreightMethod_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnFreightMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFreightMethod_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFreightMethod_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnFreightMethod, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFreightMethod_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDfltCarrierBillMeth_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnDfltCarrierBillMeth, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDfltCarrierBillMeth_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDfltCarrierBillMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnDfltCarrierBillMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDfltCarrierBillMeth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDfltCarrierBillMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnDfltCarrierBillMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDfltCarrierBillMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnDfltCarrierBillMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnDfltCarrierBillMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnDfltCarrierBillMeth_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPriceBase_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnPriceBase, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPriceBase_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPriceBase_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnPriceBase, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPriceBase_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPriceBase_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnPriceBase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPriceBase_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnPriceBase_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnPriceBase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnPriceBase_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCCUpchargeType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnCCUpchargeType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCCUpchargeType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCCUpchargeType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnCCUpchargeType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCCUpchargeType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnCCUpchargeType_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnCCUpchargeType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnCCUpchargeType_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteUserFld_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteUserFld_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteUserFld_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteUserFld(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteUserFld_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteUserFld_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteUserFld(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteUserFld_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstablished_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDateEstablished, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstablished_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstablished_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDateEstablished, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstablished_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDateEstablished_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDateEstablished, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDateEstablished_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Public Function DMValidate(oDm As clsDmForm) As Boolean
    '+++ VB/Rig Begin Push +++
    #If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
    #End If
    '+++ VB/Rig End +++
    '********************************************************************
    '   Description:
    '       DMValidate is called from the DataManager before it attempts
    '       to save the record.  Here, entries will be checked for validity.
    '       If DMValidate returns true, the Data Manager will save the
    '       record.  Otherwise, it will stop processing, and the form
    '       will remain in the same state as if the use had not pressed
    '       the finish button.
    '
    '   Parameters:
    '       oDM <in>    - is the DataManager Object that caused the
    '                     DMValidate to be called.
    '   Return Values:
    '       True    - the entry is valid
    '       False   - the Entry is invalid because anyone of the components
    '                 tested for validity is invalid.
    '********************************************************************
    
    DMValidate = False
         
    If oDm Is moDmForm Or oDm Is moDmFormArAddr Or oDm Is moDmContact Or oDm Is moDmSTax Then
        DMValidate = True
                
    ElseIf oDm Is moDmFormCiAddr Then
        If Len(Trim(txtLatitude)) = 0 And Len(Trim(txtLongitude)) <> 0 Then
            If Not bRequiredIsValid(oDm, txtLatitude, lblLatitude) Then 'Latitude is conditionally required
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function
            End If
            
        ElseIf Len(Trim(txtLongitude)) = 0 And Len(Trim(txtLatitude)) <> 0 Then
            If Not bRequiredIsValid(oDm, txtLongitude, lblLongitude) Then 'Longitude is conditionally required
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function
            End If
            
        Else
            DMValidate = True
        End If
        
    Else
    
        '---------------------------------------------------------------------
        'This should be the last check.  It is assumed that if you make it
        'here, then the oDm is coming from Customizer.  So if you modify this
        'app, be sure to add your oDm check above.
        '---------------------------------------------------------------------
        #If CUSTOMIZER Then
            If Not moFormCust Is Nothing Then
                DMValidate = True
            End If
        #End If
    
    End If
    
    
    '+++ VB/Rig Begin Pop +++
            Exit Function
    
VBRigErrorRoutine:
            gSetSotaErr Err, sMyName, "DMValidate", VBRIG_IS_FORM
            Select Case VBRIG_IS_NON_EVENT
            Case VBRIG_IS_NON_EVENT
                    Err.Raise guSotaErr.Number
            Case Else
                    Call giErrorHandler: Exit Function
            End Select
    '+++ VB/Rig End +++
End Function

Private Function bRequiredIsValid(ByVal oDm As clsDmForm, ByVal ctlReqdControl As Control, ByVal sAssosLabel As String, Optional iPage As Variant) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Desc: Called from DMValidate.
'         bRequiredIsValid will return True if the ctlReqdControl
'         is not empty.
'         If Control is empty diplay message with assigned label and
'         set focus back to control.
'
'  Parms: ctlReqdControl - Required control to be verified
'         sAssosLabel   - Label associated to control to be displayed
'         iPage         - tabPage where control is
'***********************************************************************
    bRequiredIsValid = True

    'If ctl is invisible due to Options then it is not required
    If ctlReqdControl.Visible Then
        If Trim(ctlReqdControl) = Empty Then
            If Not oDm Is Nothing Then oDm.CancelAction 'Release table before giving ErrMsg
        
            '100078, kmsgEntryRequired, You must specify a value for {0}
            Call giSotaMsgBox(Me, moClass.moSysSession, kmsgEntryRequired, CVar(sAssosLabel))
            bRequiredIsValid = False
            If Not IsMissing(iPage) Then tabC.Tab = iPage    'Set correct tabPage before Setting focus to control
            If ctlReqdControl.Enabled Then ctlReqdControl.SetFocus
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRequiredIsValid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtLatitude_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLatitude, True
    #End If
'+++ End Customizer Code Push +++
    If mbDMStateChanged Then
        txtLatitude.Text = Format(txtLatitude, "##0.0#####")
    End If
    gLatLonChange txtLatitude, True
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLatitude_Change", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLatitude_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLatitude, True
    #End If
'+++ End Customizer Code Push +++
 txtLatitude.Tag = txtLatitude.Text
 mbDMStateChanged = False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLatitude_GotFocus", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLatitude_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLatitude, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = giLatLonKeyPress(txtLatitude, KeyAscii, True)
    If KeyAscii = 0 Then
        SendKeys "{End}", True
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLatitude_KeyPress", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLatitude_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLatitude, True
    #End If
'+++ End Customizer Code Push +++
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtLatitude_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongitude_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLongitude, True
    #End If
'+++ End Customizer Code Push +++
    If mbDMStateChanged Then
        txtLongitude.Text = Format(txtLongitude, "###0.0#####")
        mbDMStateChanged = False
    End If
    gLatLonChange txtLongitude, False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongitude_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongitude_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLongitude, True
    #End If
'+++ End Customizer Code Push +++
 txtLongitude.Tag = txtLongitude.Text
 mbDMStateChanged = False
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongitude_GotFocus", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongitude_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLongitude, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = giLatLonKeyPress(txtLongitude, KeyAscii, False)
    If KeyAscii = 0 Then
        SendKeys "{End}", True
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLongitude_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLongitude_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLongitude, True
    #End If
'+++ End Customizer Code Push +++
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtLongitude_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And Controls Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And Controls Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property



Private Sub CustVendEmailConfig()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************
' Desc: Show Address form and allow user to update addresses
'***********************************************************
On Error GoTo ExpectedErrorRoutine


    Dim oChild As Object              'Address Form Object
    Dim sCustID             As String
    Dim lCustkey            As Long
    Dim lContKey            As Long
    Dim iStatus             As Integer
    Dim sCustName           As String
  
'Main Lookup
  'Customer ID, Name & used for Caption
    sCustID = txtCustomerID.Text
    sCustName = txtCustName.Text
    
  'Customer Key & Primary Address Key used in data Manager Where Clause
    lCustkey = moDmForm.GetColumnValue("CustKey")
    lContKey = gvCheckNull(moDmFormArAddr.GetColumnValue("DfltCntctKey"), SQL_INTEGER)
    
'Combo Boxes - StaticList
    If cboStatus.ListIndex = kItemNotSelected Then
        iStatus = 0
    Else
        iStatus = cboStatus.ItemData(cboStatus.ListIndex)
    End If
    
    SetHourglass True   'Set Mouse Pointer
        
  'Load Address form if hasn't been loaded yet
    Set oChild = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
       kclsCICustVendEmailMNT, ktskCICustVendEmailMNT, kAOFRunFlags, kContextAOF)

    SetHourglass False  'Reset Mouse Pointer
  
    If oChild Is Nothing Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingContacts
    Else
      'Send startup parameters to Address form
        oChild.EnterContacts lCustkey, sCustID, sCustName, 1, iStatus
    End If
    
  'Renable the form and set focus back on the form
    Me.SetFocus
    Set oChild = Nothing      'Clean up Address Form

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

ExpectedErrorRoutine:
    MyErrMsg moClass, Err.Description, Err, sMyName, "CustVendEmailConfig"
    On Error Resume Next
    Me.SetFocus
    Set oChild = Nothing      'Clean up Address Form
    giSotaMsgBox Me, moClass.moSysSession, kmsgARErrorCreatingContacts
    SetHourglass False  'Reset Mouse Pointer
    gClearSotaErr
Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustVendEmailConfig", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub CustEmailSave(Optional lCustkey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If

Dim sSQL As String
Dim rs As Object
Dim iTranType As Integer
Dim lCustomerKey As Long


'+++ VB/Rig End +++
'check if there are any records written to tarCustDocTrnsmit Table
 
   sSQL = "select TranType from tciTranType Where BusDocType = 1"
   
   Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
   If rs.IsEOF Then
        Set rs = Nothing
        Exit Sub
   End If
   'this is to insure that all documents have at least a hardcopy option selected
   'routine will attempt to insert a record...if an error occures means there is something already there for the trantype
   'if no error occures write out a hardcopy record type
   rs.MoveFirst
   If moDmForm.GetColumnValue("CustKey") = "" Then
        lCustomerKey = lCustkey
   Else
        lCustomerKey = moDmForm.GetColumnValue("CustKey")
   End If
   
   
   Do Until rs.IsEOF
        Let iTranType = rs.Field("TranType")
        sSQL = "insert into tarCustDocTrnsmit (CustKey, TranType, EMail, EMailFormat, Fax, HardCopy) values (" & lCustomerKey & ", " & iTranType & ",0,1,0,1)"
        'sSQL = "insert into tarCustDocTrnsmit (CustKey, TranType, EMail, EMailFormat, Fax, HardCopy) values (" & moDmForm.GetColumnValue("CustKey") & ", " & iTranType & ",0,1,0,1)"
        On Error Resume Next
        moClass.moAppDB.ExecuteSQL (sSQL)
        'Debug.Print "error:" & Err & " descrip:" & Err.Description
        rs.MoveNext
   Loop


'+++ VB/Rig Begin Pop +++
   Set rs = Nothing
   Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustEmailSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub CustEmailDel()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If

Dim sSQL As String

'+++ VB/Rig End +++

    sSQL = "delete from tarCustDocTrnsmit where CustKey = " & miCustKey
    On Error Resume Next
    moClass.moAppDB.ExecuteSQL (sSQL)

'+++ VB/Rig Begin Pop +++
   Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustEmailDel", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Sub SendSalespersonEmail()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If

'+++ VB/Rig End +++

Dim rsSperEmail As Object 'Salespersons email addresses
Dim sSQL As String
Dim bRetval As Boolean
Dim iEmailTransport As Integer
Dim iErrType As Integer
Dim lErrStrNo As Long
Dim sPathArray(0) As String 'don't use but need to pass an array in SendEmail for attachments
Dim bAutoSend As Boolean
Dim bReturnReceipt As Boolean
Dim sEmailAddr As String
Dim sEmailCC As String
Dim sEmailBcc As String
Dim sSubject As String
Dim sBodyText As String
Dim sOutboxFolder As String
Dim sUserName As String
Dim sOrigStatus As String
Dim sCreditCheckChange As String
Dim sCreditAmount As String

Dim bCreditLimitChange As Boolean
Dim bCreditLimitUsedChange As Boolean
Dim bStatusChange As Boolean
Dim bHoldChange As Boolean

Const kNoEmail = 0
Const kExchange = 1
Const kSMTP = 2

'Error types returned by SendEmail
Const kInvalidMailboxOrServerError = 2
Const kNoMailboxPermissionError = 3

bCreditLimitChange = moDmForm.Columns("CreditLimit").IsDirty
bCreditLimitUsedChange = moDmForm.Columns("CreditLimitUsed").IsDirty

bStatusChange = moDmForm.Columns("Status").IsDirty
bHoldChange = moDmForm.Columns("Hold").IsDirty

'send all sales people associated with customer email when credit limit, status or hold changes
If bCreditLimitChange Or bCreditLimitUsedChange Or bStatusChange Or bHoldChange Then

    iEmailTransport = giGetValidInt(moClass.moAppDB.Lookup("EMailTransport", "tciOptions", _
   "CompanyID = " & gsQuoted(msCompanyID)))

    If iEmailTransport = kNoEmail Or (iEmailTransport = kExchange And Len(Trim(msCreditMgrMailBox)) = 0) Or _
        (iEmailTransport = kSMTP And Len(Trim(msCreditMgrEmail)) = 0) Then
        Exit Sub
    End If
   
    sPathArray(0) = ""
    bAutoSend = True
    bReturnReceipt = False
    sEmailCC = ""
    sEmailBcc = ""
    sOutboxFolder = ""
    
    sSQL = "SELECT DISTINCT EMailAddr, SperID FROM tarSalesperson" & _
            " JOIN  tarCustAddr ON tarCustAddr.SperKey = tarSalesperson.SperKey " & _
            " WHERE tarCustAddr.CustKey = " & moDmForm.GetColumnValue("CustKey") & _
            " AND tarSalesperson.EMailAddr IS NOT NULL"

    Set rsSperEmail = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

       
    Do Until rsSperEmail.IsEOF
    
        sEmailAddr = rsSperEmail.Field("EMailAddr")
        sCreditAmount = ""
        
        'Customer updates for {customer name} ({customer ID}).
        sSubject = gsBuildString(kARCustomerUpdate, moClass.moAppDB, moClass.moSysSession, _
                   Trim(txtCustName) & "~" & Trim(txtCustomerID))
                  
        If bCreditLimitChange Then
        
            'Credit limit has been changed from {curOrigCreditLimit} to {curCreditLimit}.
            sBodyText = Chr(10) & Chr(10) & gsBuildString(kARCustCreditLimitChg, moClass.moAppDB, _
            moClass.moSysSession, curOrigCreditLimit.MaskedText & "~" & curCreditLimit.MaskedText)
       
        End If
        
        If bCreditLimitUsedChange Then
        
            'Apply Credit Limit Changed.
            If chkCreditLimitUsed.Value = 1 Then
                sCreditCheckChange = gsBuildString(kSOCreditCheckChgUA, moClass.moAppDB, moClass.moSysSession)
                If Len(Trim(sBodyText)) = 0 Then
                    sCreditAmount = gsBuildString(kARZRE001CrdtLmtCol, moClass.moAppDB, moClass.moSysSession) & curCreditLimit.MaskedText
                End If
                
                If Len(Trim(sCreditAmount)) <> 0 Then
                    sCreditCheckChange = sCreditCheckChange & Chr(10) & Chr(10) & sCreditAmount
                End If
            Else
                sCreditCheckChange = gsBuildString(kSOCreditCheckChgAU, moClass.moAppDB, moClass.moSysSession)
            End If
            
            sBodyText = sBodyText & Chr(10) & Chr(10) & sCreditCheckChange
      
        End If
        
        If bStatusChange Then
                   
            sOrigStatus = cboStatus.List(giListIndexFromItemData(cboStatus, giGetValidInt(moDmForm.GetColumnValue("Status"))))
            If sOrigStatus = "" Then
                sOrigStatus = gsBuildString(kNewNormal, moClass.moAppDB, moClass.moSysSession) 'New
            End If
            
            'Status has been changed from {orig status} to {new status}
            sBodyText = sBodyText & Chr(10) & Chr(10) & gsBuildString(kARStatusChange, moClass.moAppDB, _
            moClass.moSysSession, sOrigStatus & "~" & cboStatus)

        End If
        
        If bHoldChange Then
        
            Select Case chkOnHold
                Case vbChecked
                    'Customer has been placed on hold
                    sBodyText = sBodyText & Chr(10) & Chr(10) & gsBuildString(kAROnHold, moClass.moAppDB, _
                    moClass.moSysSession)
                Case Else
                    'Customer has been removed from hold
                    sBodyText = sBodyText & Chr(10) & Chr(10) & gsBuildString(kARRemoveHold, moClass.moAppDB, _
                    moClass.moSysSession)
            End Select

        End If
        
       '-- sUserName = gsGetValidStr(moClass.moAppDB.Lookup("UserName", "tsmUser", "UserID=" & gsQuoted(moClass.moSysSession.UserId)))
        sUserName = moClass.moSysSession.UserName
        
        sBodyText = sBodyText & Chr(10) & Chr(10) & gsBuildString(kARChangedBy, moClass.moAppDB, _
        moClass.moSysSession, sUserName)
        
        bRetval = SendEmail(msCompanyID, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, _
                 bAutoSend, bReturnReceipt, sEmailAddr, sEmailBcc, sEmailCC, sSubject, sBodyText, msCreditMgrMailBox, _
                 sOutboxFolder, msCreditMgrEmail, sPathArray, iErrType, lErrStrNo)
                     
        If Not bRetval Then
        
            'customize error message for two cases, otherwise use generic error message string
            If iErrType = kInvalidMailboxOrServerError Then
                lErrStrNo = kARInvalidMailboxOrServer
            ElseIf iErrType = kNoMailboxPermissionError Then
                lErrStrNo = kARNoCrMgrMailboxPermission
            End If
            
            giSotaMsgBox Me, moClass.moSysSession, kmsgARCrLimitEmailError, rsSperEmail.Field("SperID"), _
                 gsBuildString(lErrStrNo, moClass.moAppDB, moClass.moSysSession)
                      
        End If
                    
        'go to next record
        If Not rsSperEmail.IsEOF Then rsSperEmail.MoveNext
            
    Loop
    
    If iEmailTransport = kExchange Then
        ShutdownSession 'shutdown MAPI session if necessary
    End If
End If

'+++ VB/Rig Begin Pop +++
   Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SendSalespersonEmail", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Function bDeleteCustomerRecord(ByVal lCustkey As Long, ByRef bActivityDetected As Boolean) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'---------------------------------------------------------------------
'This function attempts to delete a customer record from the database.
'Returns true if the record was deleted, false if not.
'---------------------------------------------------------------------
    Dim lRetVal As Long
   
    bDeleteCustomerRecord = False
    bActivityDetected = False
    
    If lCustkey <= 0 Then
        Exit Function
    End If
         
   
    With moClass.moAppDB
        .SetInParam lCustkey
        .SetInParam 4           'Treat as deleted for full delete processing
        .SetInParam Null        'No activity since date restriction
        .SetInParam 0           'delete temporary not applicable here.
        .SetOutParam lRetVal
        On Error Resume Next
        .ExecuteSP ("sparDeleteCustomer")
        'If the record could not be deleted due to referential integrity, ignore the error.
        'The stored procedure relies on the delete trigger to see if related records have
        'been deleted (i.e. invoices, sales orders, etc.).  If not, the delete will fail.
        'These are the DAS errors related to RI.
        'Most likely error is 4261.
        'kerrReferentialIntegrityViolation = 4260
        'kerrParentDeleteRestriction       = 4261
        'kerrChildInsertRestriction        = 4262
        'kerrParentUpdateRestriction       = 4263
        'kerrCascadeUpdateFail             = 4264
        'kerrChildUpdateRestriction        = 4265
        'kerrChildDeleteRestriction        = 4266
        'kerrParentInsertRestriction       = 4267
        If Err.Number >= kerrReferentialIntegrityViolation And Err.Number <= kerrParentInsertRestriction Then
            'Customer not delete due to activity.
            bActivityDetected = True
        ElseIf Err.Number <> 0 And Err.Number < kerrReferentialIntegrityViolation _
           And Err.Number > kerrParentInsertRestriction Then
            .ReleaseParams
            GoTo VBRigErrorRoutine
        End If
        
        On Error GoTo VBRigErrorRoutine 'Reset error handling for any other errors
        .ReleaseParams
    
    End With

    If lRetVal = 0 And bActivityDetected = False Then     'SP successfully deleted the record
        bDeleteCustomerRecord = True
    End If

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeleteCustomerRecord", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

Private Function IsValidRetAccount() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
'   Description:
'       IsValidGLAccount will check if a valid GLAccount Number entry
'       exists. If nothing was entered or there was no change in the Number
'       entered then continue.  Otherwise, check the Account Number to determine
'       if there is an existing one. If not then create the GLAccount object,
'       if necessary, and run the AddOnTheFly.
'   Returns: 0 - Invalid Account
'            1 - Valid Account Or Empty
'*******************************************************************************

    Dim rs          As Object   'SOTADAS recordset Object
    Dim bValid      As Boolean  'Boolean Valid Flag
    Dim sGLAcctNo   As String   'String GL Account Number
    Dim sCurrID     As String
    
    IsValidRetAccount = 1        'Initialize the return Value
    
     
  'No acct Entered Clear out Values for key and reset tag
    If Len(Trim(glaReturns.Text)) = 0 Then
       glaReturns.Tag = glaReturns.Text
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If glaReturns.IsValid = False Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblDfltReturnsAcc)
        glaReturns.Text = glaReturns.Tag
        gbSetFocus Me, glaReturns
        IsValidRetAccount = 0
    Else
        glaReturns.Tag = glaReturns.Text
    End If
    
        
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidRetAccount", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bExtShipSystemON() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
    bExtShipSystemON = False
    sSQL = "SELECT ExtShipSystem FROM tsoOptions WHERE CompanyID = " & gsQuoted(msCompanyID)
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
        If Not rs Is Nothing Then
            If rs.RecordCount > 0 Then
                miExtShipSystem = rs.IntField("ExtShipSystem")
                If miExtShipSystem <> kNone Then
                    bExtShipSystemON = True
                End If
            End If
        End If
        rs.Close
        Set rs = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bExtShipSystemON", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function CopyOtherData() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    ' Determine if Addresses and Contacts need to be copied from
    ' source customer to current/new customer if copy dialog was accessed

    Dim SQL         As String
    Dim rs          As Object
    Dim newKey      As Long
    Dim newDate     As String
    Dim newUserID   As String

    ' Exit if no source to copy from
    If (m_SourceKey = 0) Then
        CopyOtherData = True
        Exit Function
    End If
    
    ' Init
    CopyOtherData = False
    newDate = gsQuoted(gsFormatDateToDB(Now))
    newUserID = gsQuoted(moClass.moSysSession.UserId)
    
    ' Get 'other' addresses from source customer
    SQL = "SELECT * " & _
          "  FROM tarCustAddr WITH (NOLOCK)" & _
          " WHERE CustKey = " & m_SourceKey & _
          "   AND AddrKey <> " & m_SourceAddrKey
    Set rs = moClass.moAppDB.OpenRecordset(SQL, kSnapshot, kOptionNone)
        
    ' If there are addresses, loop through and create records
    If Not (rs.IsEmpty) Then
        rs.MoveFirst
        While Not (rs.IsEOF)
            
            ' Get new key first
            With moClass.moAppDB
                .SetInParam "tciAddress"
                .SetOutParam newKey
                .ExecuteSP ("spGetNextSurrogateKey")
                newKey = .GetOutParam(2)
                .ReleaseParams
            End With
        
            ' Create new tciAddress record
            SQL = "INSERT INTO tciAddress "
            SQL = SQL & "(AddrKey, AddrLine1, AddrLine2, AddrLine3, AddrLine4,"
            SQL = SQL & "AddrLine5, AddrName, City, CountryID, CRMAddrID,"
            SQL = SQL & "Fax, FaxExt, Phone, PhoneExt, PostalCode, Residential,"
            SQL = SQL & "StateID , TransactionOverride, UpdateCounter, Latitude, Longitude) "
            SQL = SQL & "SELECT " & CStr(newKey) & ", "
            SQL = SQL & "       AddrLine1, AddrLine2, AddrLine3, "
            SQL = SQL & "       AddrLine4, AddrLine5, AddrName, "
            SQL = SQL & "       City, CountryID, CRMAddrID, Fax, "
            SQL = SQL & "       FaxExt, Phone, PhoneExt, PostalCode, "
            SQL = SQL & "       Residential, StateID, TransactionOverride, 0, Latitude, Longitude "
            SQL = SQL & "  FROM tciAddress WITH (NOLOCK) "
            SQL = SQL & " WHERE AddrKey = " & CStr(rs.Field("AddrKey"))
            moClass.moAppDB.ExecuteSQL SQL
            
            ' Create new tarCustAddr record
            SQL = "INSERT INTO tarCustAddr "
            SQL = SQL & "       (AddrKey, AllowInvtSubst, BackOrdPrice, BOLReqd, CarrierAcctNo, CarrierBillMeth, CloseSOLineOnFirstShip,"
            SQL = SQL & "       CloseSOOnFirstShip, CommPlanKey, CreateDate, CreateType, CreateUserID, CurrExchSchdKey, CurrID,"
            SQL = SQL & "       CustAddrID, CustKey, CustPriceGroupKey, DfltCntctKey, FOBKey, FreightMethod, ImportLogKey,  InvcFormKey,"
            SQL = SQL & "       InvcMsg, InvoiceReqd, LanguageID, PackListContentsReqd, PackListFormKey, PackListReqd, PmtTermsKey,"
            SQL = SQL & "       PriceAdj, PriceBase, PrintOrderAck, RequireSOAck, SalesTerritoryKey, ShipComplete, ShipDays, ShipLabelFormKey,"
            SQL = SQL & "       ShipLabelsReqd, ShipMethKey, ShipZoneKey, SOAckFormKey, SOAckMeth, SperKey, STaxSchdKey, UpdateDate,"
            SQL = SQL & "       UpdateUserID , UsePromoPrice, WhseKey)"
            SQL = SQL & "  SELECT " & CStr(newKey) & ", "
            SQL = SQL & "       AllowInvtSubst, BackOrdPrice, BOLReqd, "
            SQL = SQL & "       CarrierAcctNo, CarrierBillMeth, CloseSOOnFirstShip, "
            SQL = SQL & "       CloseSOOnFirstShip, CommPlanKey, " & newDate & ", 0, "
            SQL = SQL & "       " & newUserID & ", CurrExchSchdKey, CurrID, "
            SQL = SQL & "       CustAddrID, " & CStr(moDmForm.GetColumnValue("CustKey")) & ", "
            SQL = SQL & "       CustPriceGroupKey, DfltCntctKey, FOBKey, FreightMethod, "
            SQL = SQL & "       ImportLogKey, InvcFormKey, InvcMsg, InvoiceReqd, LanguageID, "
            SQL = SQL & "       PackListContentsReqd, PackListFormKey, PackListReqd, "
            SQL = SQL & "       PmtTermsKey, PriceAdj, PriceBase, PrintOrderAck, RequireSOAck, SalesTerritoryKey, "
            SQL = SQL & "       ShipComplete, ShipDays, ShipLabelFormKey, ShipLabelsReqd, ShipMethKey, "
            SQL = SQL & "       ShipZoneKey, SOAckFormKey, SOAckMeth, SperKey, STaxSchdKey, " & newDate & ", "
            SQL = SQL & "       " & newUserID & ", UsePromoPrice, WhseKey "
            SQL = SQL & "  FROM tarCustAddr WITH (NOLOCK) "
            SQL = SQL & " WHERE AddrKey = " & CStr(rs.Field("AddrKey"))
            SQL = SQL & "   AND CustAddrID = " & gsQuoted(rs.Field("CustAddrID"))
            SQL = SQL & "   AND CustKey = " & CStr(rs.Field("CustKey"))
            moClass.moAppDB.ExecuteSQL SQL
            
            ' Increment to next source record
            rs.MoveNext
        Wend
    End If

    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If

    ' Get 'other' contacts from source customer
    SQL = "SELECT * " & _
          "  FROM tciContact WITH (NOLOCK)" & _
          " WHERE tciContact.CntctOwnerKey = " & m_SourceKey & _
          "   AND EntityType = " & CStr(kEntTypeARCustomer) & _
          "   AND tciContact.CntctKey <> " & m_SourceCntcKey
    Set rs = moClass.moAppDB.OpenRecordset(SQL, kSnapshot, kOptionNone)

    ' If there are contacts, loop through and create records
    If Not (rs.IsEmpty) Then
        rs.MoveFirst
        While Not (rs.IsEOF)
            
            ' Get new key first
            With moClass.moAppDB
                .SetInParam "tciContact"
                .SetOutParam newKey
                .ExecuteSP ("spGetNextSurrogateKey")
                newKey = .GetOutParam(2)
                .ReleaseParams
            End With
        
            ' Create new tciContact record
            SQL = "INSERT " & _
                  "  INTO tciContact " & _
                  "(CntctKey, CntctOwnerKey, CreateDate, CreateType, CreateUserID, CRMContactID, EMailAddr, EMailFormat, EntityType, " & _
                  "ExtUser, Fax, FaxExt, ImportLogKey, Name, Phone, PhoneExt, Title, UpdateCounter, UpdateDate, UpdateUserID)" & _
                  "SELECT " & CStr(newKey) & ", " & _
                  "       " & CStr(moDmForm.GetColumnValue("CustKey")) & ", " & newDate & ", 1, " & newUserID & ", " & _
                  "       CRMContactID, EMailAddr, EMailFormat, " & CStr(kEntTypeARCustomer) & ", " & _
                  "       ExtUser, Fax, FaxExt, ImportLogKey, Name, " & _
                  "       Phone, PhoneExt, Title, 0, " & newDate & ", " & newUserID & _
                  "  FROM tciContact WITH (NOLOCK) " & _
                  " WHERE CntctKey = " & CStr(rs.Field("CntctKey"))
            moClass.moAppDB.ExecuteSQL SQL
            
            ' Increment to next source record
            rs.MoveNext
        Wend
    End If

    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If

    CopyOtherData = True
               
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CopyOtherData", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub OfficeInitialization()
'+++ VB/Rig Skip +++
On Error Resume Next
'*************************************************************************
'      Desc:  The Office class has been initialized elsewhere and has
'             already received this form�s reference. Therefore,
'             this method is additive if the client task wishes to add
'             clsDMForm and/or clsDMGrid references.
'     Parms:  N/A
'   Returns:  N/A
'************************************************************************
    With tbrMain.Office
        .AddFormObject moDmForm, "Customer"
        .AddFormObject moDmFormArAddr, "CustomerAddress"
        .AddFormObject moDmFormCiAddr, "Address"
        .AddFormObject moDmContact, "Contact"
        .AddGridObject moDmSTax, "SalesTax"
    End With
    Err.Clear
End Sub
Private Function IsValidCustDefaultCreditCard() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
    Dim sWhere As String
    Dim lCustkey As Long
    IsValidCustDefaultCreditCard = 1

    If Len(Trim$(lkuDefaultCreditCard.Text)) > 0 Then
        lCustkey = moDmForm.GetColumnValue("CustKey")
    
        sWhere = "CreditCardName = " & gsQuoted(Trim$(lkuDefaultCreditCard.Text)) & " AND CustKey = " & glGetValidLong(lCustkey)
        If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "tccCustCreditCard", sWhere)) = 0 Then
    
            lkuDefaultCreditCard.Text = lkuDefaultCreditCard.Tag
            giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblDefaultCreditCard)
            gbSetFocus Me, lkuDefaultCreditCard
            IsValidCustDefaultCreditCard = 0
        Else
            If lkuDefaultCreditCard.Tag <> lkuDefaultCreditCard.Text Then
                'moDmFormArAddr.SetColumnValue "CreditCardName", lkuDefaultCreditCard.Text
                lkuDefaultCreditCard.KeyValue = moClass.moAppDB.Lookup("CreditCardKey", "tccCustCreditCard", "CustKey = " & glGetValidLong(lCustkey) & " AND CreditCardName = " & gsQuoted(lkuDefaultCreditCard.Text))
               'moDmFormCiAddr.SetDirty True
               lkuDefaultCreditCard.Tag = lkuDefaultCreditCard.Text
            End If
        End If
    End If
    

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidCustDefaultCreditCard"
lkuDefaultCreditCard.Text = lkuDefaultCreditCard.Tag
IsValidCustDefaultCreditCard = 0
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidCustDefaultCreditCard", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function



'RKL DEJ 2016-05-10 (START)
'This process is to link missing links between SLX and SAGE Customers for the current Company.
Public Sub LinkAllCustomers()
    On Error GoTo Error
    
    Dim icnt As Long
    
    Dim CustKey As Long
    Dim CustID As String
    Dim CustName As String
    
    Dim AddrKey As Long
    Dim AddrName As String
    Dim AddrLine1 As String
    Dim AddrLine2 As String
    Dim AddrLine3 As String
    Dim AddrLine4 As String
    Dim City As String
    Dim State As String
    Dim Zip As String
    Dim Country As String

    Dim rs      As New DASRecordSet       'SOTADAS recordset Object
    Dim sSQL    As String       'SQL String
    
    'Get Customer not linked to SLX
    sSQL = "select c.CompanyID, c.CustKey, c.CustID, Coalesce(c.CustName,'') 'CustName', "
    sSQL = sSQL & "Coalesce(a.AddrKey,0) 'AddrKey', Coalesce(a.AddrName, '') 'AddrName', "
    sSQL = sSQL & "Coalesce(a.AddrLine1,'') 'AddrLine1', Coalesce(a.AddrLine2,'') 'AddrLine2', Coalesce(a.AddrLine3,'') 'AddrLine3', Coalesce(a.AddrLine4,'') 'AddrLine4', Coalesce(a.AddrLine5,'') 'AddrLine5', "
    sSQL = sSQL & "Coalesce(a.City,'') 'City', Coalesce(a.StateID, '') 'StateID', Coalesce(a.PostalCode,'') 'PostalCode', Coalesce(a.CountryID,'') 'CountryID' "
    sSQL = sSQL & "From vluSLXCustomer_SGS slx "
    sSQL = sSQL & "Inner Join tarCustomer c with(NOLock) "
    sSQL = sSQL & "    on SLX.CustKey = c.custkey "
    sSQL = sSQL & "    and c.Status = 1 "
    sSQL = sSQL & "Inner Join tciAddress a with(NoLock) "
    sSQL = sSQL & "    on c.PrimaryAddrKey = a.AddrKey "
    sSQL = sSQL & "Inner Join tciCompanySendToSLX_RKL comp with(NoLock) "
    sSQL = sSQL & "    on c.CompanyID = comp.CompanyID "
    sSQL = sSQL & "Where SLX.AccountID Is Null "
    sSQL = sSQL & "and c.companyid = " & gsQuoted(msCompanyID)
    
    'Run query to get Missing Customer Links
    Set rs = moDmForm.Database.OpenRecordset(sSQL, kSnapshot, kOptionNone)
       
    
    If rs.RecordCount > 0 Then
        icnt = 0
        While Not rs.IsEOF
            icnt = icnt + 1
            rs.MoveNext
        Wend
        
        rs.MoveFirst
    
        If MsgBox("There are " & icnt & " customers records that are about to be linked between SAGE and SalesLogix.  Do you want to continue?", vbYesNo, "Confirm Link Action") <> vbYes Then
            Exit Sub
        End If
    Else
        MsgBox "There are no records to link.", vbExclamation, "SAGE 500"
        Exit Sub
    End If
            
    Me.MousePointer = vbHourglass
    
    'There is customer class information
    While Not rs.IsEOF
        CustKey = glGetValidLong(rs.Field("CustKey"))
        CustID = Trim(gsGetValidStr(rs.Field("CustID")))
        CustName = Trim(gsGetValidStr(rs.Field("CustName")))
        
        AddrKey = glGetValidLong(rs.Field("AddrKey"))
        
        AddrName = Trim(gsGetValidStr(rs.Field("AddrName")))
        AddrLine1 = Trim(gsGetValidStr(rs.Field("AddrLine1")))
        AddrLine2 = Trim(gsGetValidStr(rs.Field("AddrLine2")))
        AddrLine3 = Trim(gsGetValidStr(rs.Field("AddrLine3")))
        AddrLine4 = Trim(gsGetValidStr(rs.Field("AddrLine4")))
        City = Trim(gsGetValidStr(rs.Field("City")))
        State = Trim(gsGetValidStr(rs.Field("StateID")))
        Zip = Trim(gsGetValidStr(rs.Field("PostalCode")))
        Country = Trim(gsGetValidStr(rs.Field("CountryID")))
        
        ProcessSLXData oClass, msCompanyID, CustKey, CustID, CustName, AddrKey, AddrName, AddrLine1, AddrLine2, AddrLine3, AddrLine4, City, State, Zip, Country
        
        rs.MoveNext
    Wend

    Me.MousePointer = vbDefault
    
    MsgBox "The link process is complete.", vbExclamation, "SAGE 500"
    
    Exit Sub
Error:
    Me.MousePointer = vbDefault

    MsgBox Me.Name & ".LinkAllCustomers()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear
End Sub

'RKL DEJ 2016-05-10 (START)

