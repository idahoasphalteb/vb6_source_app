VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#47.0#0"; "sotacalendar.ocx"
Begin VB.Form frmExemptions 
   Caption         =   "Sales Tax Exemptions"
   ClientHeight    =   6615
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9555
   HelpContextID   =   17779651
   Icon            =   "Exemptions.frx":0000
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   6615
   ScaleWidth      =   9555
   StartUpPosition =   3  'Windows Default
   Begin SOTAToolbarControl.SOTAToolbar tbrExemptions 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   0
      Width           =   9555
      _ExtentX        =   16854
      _ExtentY        =   741
      Style           =   6
   End
   Begin FPSpreadADO.fpSpread grdExemptions 
      Height          =   5775
      Left            =   120
      TabIndex        =   0
      Top             =   600
      WhatsThisHelpID =   17779653
      Width           =   9255
      _Version        =   524288
      _ExtentX        =   16325
      _ExtentY        =   10186
      _StockProps     =   64
      DisplayRowHeaders=   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "Exemptions.frx":058A
      AppearanceStyle =   0
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   1
      Top             =   6240
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   7
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   8
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   2
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   12
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmExemptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmExemptions
'     Desc: Sales Tax Exemption
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 06/12/06 JLW
'     Mods: mm/dd/yy XXX
'************************************************************************************
Option Explicit


#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

  'Sales Tax Schedule
   
 'Private Form Properties
    Private moClass         As Object   'Parent Forms Class
    Private moDM            As Object   'Data Manager Object for update grid
    Private mlCustKey       As Long     'Customer Key
    Private miStatus        As Integer  'Customer Status
    Private mlAddrKey       As Long     'Current address being edited
    Private mlOtherAddrKey  As Long     'Address when coming from Maintain Customer Addresses
    Private miSecurityLevel As Integer
    Private mbExemptionsChanged As Boolean  'Exemptions changed
    Private miSortDirection As Integer
    
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    
    Public moDmSTaxCodes        As New clsDmGrid
    Private moGMSTaxCodes       As New clsGridMgr
    
    Private moContextMenuObj        As clsContextMenu
    
 'Column Number Constants
    Const kColTaxCode       As Long = 1  'Tax Code Column
    Const kColTaxCodeKey    As Long = 2  'Tax Code Key Column
    Const kColTaxCodeDesc   As Long = 3  'Tax Code Description Column
    Const kColTaxPayerID    As Long = 4  'TaxPayer Id Column
    Const kColExmpt         As Long = 5  'Exemption Column
    Const kColSort          As Long = 6  'Used for sorting
    
    Const kColMaxCols       As Long = 6  'max Number Of Columns
    
    Const kSortDirectionAsc  As Integer = 1
    Const kSortDirectionDesc As Integer = 2
    
    Const VBRIG_MODULE_ID_STRING = "Exemptions.FRM"

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moClass = oNewClass
    
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Public Property Get oDm() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oDm = moDM
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oDm_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Set oDm(oNewValue As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDM = oNewValue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oDm_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'************************************************************************
' iStatus - Customer Status
'************************************************************************
Public Property Get iStatus() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    iStatus = miStatus
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iStatus_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let iStatus(iNewValue As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    miStatus = iNewValue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iStatus_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property



Public Property Let lCustkey(lNewValue As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlCustKey = lNewValue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lCustKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lAddrKey(lNewValue As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlAddrKey = lNewValue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lAddrKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bExemptionsChanged() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bExemptionsChanged = mbExemptionsChanged
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExemptionsChanged_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bExemptionsChanged(bNewValue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbExemptionsChanged = bNewValue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bExemptionsChanged_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let SecurityLevel(iNewValue As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    miSecurityLevel = iNewValue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Securitylevel_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "ARZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "ARZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Sub UpdateExemptions(Optional bThisAddrOnly As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Desc: update Parent grid with new exemptions
'
'********************************************************************
Dim iRetval As Integer
 
    If mbExemptionsChanged Then
        'Don't need to save the DM if coming from address form, as nothing has changed.
        'Just need to create the exemptions for the new address
        If Not bThisAddrOnly Then
            moDmSTaxCodes.Save
        End If
        
        With moClass.moAppDB
            .SetInParam mlAddrKey
            .SetInParam mlCustKey
            If bThisAddrOnly Then
                .SetInParam 1
            Else
                .SetInParam 0
            End If
            .SetOutParam iRetval
            .ExecuteSP ("sparUpdateExemptions")
            iRetval = .GetOutParam(4)
            .ReleaseParams
        End With
        
        'Refresh the main form grid that contains the tarCustSTaxExmpt rows for exemptions.
        'Note - the only reason for this grid/DM is that the sales tax form uses it to
        'retrieve the exemptions for display.  It should probably be changed.
        oDm.Refresh
    End If
        
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UpdateExemptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SaveExemptions(Optional bNewAddr As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
    
    If mbExemptionsChanged Then
        'Save sales tax exemptions
        
        'If this is a new address then only need to add for the new addrses
        If Not bNewAddr Then
            sSQL = "DELETE tarCustSTaxExmpt FROM tarCustSTaxExmpt JOIN tarCustAddr WITH (NOLOCK) "
            sSQL = sSQL & "ON tarCustSTaxExmpt.AddrKey = tarCustAddr.AddrKey "
            sSQL = sSQL & "WHERE CustKey = " & mlCustKey & ";"
        End If
        
        sSQL = sSQL & "INSERT tarCustSTaxExmpt (AddrKey, ExmptNo, STaxCodeKey) "
        sSQL = sSQL & "SELECT AddrKey, ExmptNo, STaxCodeKey FROM #tarCustSTaxExmpt"
        
        moClass.moAppDB.ExecuteSQL sSQL
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SaveExemptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub CreateSTaxWorkTables()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'Create the temp tables used for managing customer sales tax exemptions.
'
'#tarCustSTaxExmpt will hold the information for tarCustSTaxExmpt.  There is one
'row for each customer address, sales tax code and exemption
'#TaxCodes holds ALL tax codes

Dim sSQL As String

    
    sSQL = "IF OBJECT_ID('tempdb..#tarCustSTAxExmpt') IS NULL "
    sSQL = sSQL & "CREATE TABLE #tarCustSTaxExmpt (AddrKey INT, ExmptNo VARCHAR(15), STaxCodeKey INT);"
    sSQL = sSQL & "IF OBJECT_ID('tempdb..#TaxCodes') IS NULL "
    sSQL = sSQL & "CREATE TABLE #TaxCodes (STaxCodeID VARCHAR(15), STaxCodeKey INT, "
    sSQL = sSQL & "Description VARCHAR(30), RgstrNo VARCHAR(20), ExmptNo VARCHAR(15), SortCol VARCHAR(16))"
    
    moClass.moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CreateSTaxWorkTables", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Public Sub SetGridProperties()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Desc: Setup the Grid Look, Columns, Locked and Hidden
'********************************************************************

    'How many rows and columns, how many Visible, hidden
    gGridSetProperties grdExemptions, kColMaxCols, kGridDataSheetNoAppend
    
    With grdExemptions
        'Skip disabled columns
        .EditModePermanent = True
    End With
    
'Specify column widths and column headers
  'Tax Code
    gGridSetColumnWidth grdExemptions, kColTaxCode, 15
    gGridSetHeader grdExemptions, kColTaxCode, "Tax Code"
    gGridSetColumnType grdExemptions, kColTaxCode, SS_CELL_TYPE_EDIT
    gGridLockColumn grdExemptions, kColTaxCode

  'Tax Code Key
    gGridSetHeader grdExemptions, kColTaxCodeKey, "Tax Code Key"
    gGridSetColumnType grdExemptions, kColTaxCodeKey, SS_CELL_TYPE_EDIT
    gGridHideColumn grdExemptions, kColTaxCodeKey

  'Tax Code Description
    gGridSetColumnWidth grdExemptions, kColTaxCodeDesc, 29
    gGridSetHeader grdExemptions, kColTaxCodeDesc, "Description"
    gGridSetColumnType grdExemptions, kColTaxCodeDesc, SS_CELL_TYPE_EDIT
    gGridLockColumn grdExemptions, kColTaxCodeDesc
    
  'TaxPayer ID
    gGridSetColumnWidth grdExemptions, kColTaxPayerID, 15
    gGridSetHeader grdExemptions, kColTaxPayerID, "TaxPayer ID"
    gGridSetColumnType grdExemptions, kColTaxPayerID, SS_CELL_TYPE_EDIT
    gGridLockColumn grdExemptions, kColTaxPayerID
    
  'Exemptions
    gGridSetColumnWidth grdExemptions, kColExmpt, 15
    gGridSetHeader grdExemptions, kColExmpt, "Exemption #"
    gGridSetColumnType grdExemptions, kColExmpt, SS_CELL_TYPE_EDIT, 15
    
    'SortCol -- hidden column used for sorting
    gGridHideColumn grdExemptions, kColSort
     
     
'Put Special initializations last
    grdExemptions.OperationMode = SS_OP_MODE_NORMAL        'row mode Operation
    gGridSetColors grdExemptions                           'Set Grid Colors

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridProperties", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PopulateSTaxCodes()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iRetval As Integer

        With moClass.moAppDB
            .SetInParam mlCustKey
            .SetInParam moClass.moSysSession.CompanyId
            .SetOutParam iRetval
            .ExecuteSP ("sparPopulateSTaxCodes")
            iRetval = .GetOutParam(3)
            .ReleaseParams
        End With
                
        'Refresh the hidden exemptions grid on the main form
        moDM.Refresh
        
   
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PopulateSTaxCodes", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
                     

Public Sub Setup()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Desc:  Setup and Load Grid
'****************************************************************
    
    'Disable Controls if Customer Status is Deleted
    'Enable Controls if Customer Status is not Deleted
    If miStatus = kvDeleted Then
        gGridLockColumn grdExemptions, kColExmpt      'Lock Exemptions Column in Grid
    Else
        gGridUnlockColumn grdExemptions, kColExmpt    'Unlock Exemptions Column in Grid
    End If
    
    'Refresh grid with tax codes and exemptions from temp table
    moDmSTaxCodes.Refresh
    
    'Manage sort of exemption field to insure it always sorts descending on a new customer
    'first time in so that the exemptions float to the top of the grid.   By setting the
    'current sort direction to ascending will cause descending sort.
    miSortDirection = kSortDirectionDesc
    grdExemptions_Click kColExmpt, 0
       
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Setup", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
   
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Desc:  Setup the grid and the parent Class Object
'****************************************************************
    
    
    Set moContextMenuObj = New clsContextMenu      ' from **PRESTO**
    Set moContextMenuObj.Form = frmExemptions         ' from **PRESTO**
    
    BindGrid

  'Setup the grid
    SetGridProperties
    
   With tbrExemptions
                
        .RemoveButton kTbPrint

       
        If miSecurityLevel = sotaTB_DISPLAYONLY Then
            .Buttons(kTbFinishExit).Enabled = False
        Else
            .Buttons(kTbFinishExit).Enabled = True
        End If
   
    End With
    
    'Save the form size variable to use for resize later
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'         run generic OSTA Function Key processing
'********************************************************************

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16              'user pressed a function key
            gProcessFKeys Me, KeyCode, Shift  'run Generic Sage MAS 500 Function Keys processing
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'         runs tab key whenever the user presses enter
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter/return
            If moClass.moSysSession.EnterAsTab Then    'enter as tab is true
                gProcessSendKeys "{tab}"    'send tab key to keyboard buffer
                KeyAscii = 0        'cancel processing of enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
'****************************************************************
' Desc:  Hide the form if called from the cancel box
'****************************************************************
    
    If UnloadMode = vbFormControlMenu Then
       'Clear the grid changes when the user clicks X in the corner
        If mbExemptionsChanged Then
            moDmSTaxCodes.Refresh
        End If
        Me.Hide
        Cancel = True                           '  do not run unload code
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++

    'resize the form height and height all ctls on the screen (form height doesn't
    'get resize smaller than its original height)
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdExemptions
     
    'resize the form Width and Width all ctls on the screen (form Width doesn't
    'get resize smaller than its original Width)
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth
    
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
  

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

'****************************************************************
' Desc:  Clean up Behind yourself
'****************************************************************

    Set moDM = Nothing         'Clean Up data Manager Object for update Grid
    moDmSTaxCodes.UnloadSelf
    Set moDmSTaxCodes = Nothing 'Sales tax codes/exemptions grid
    moGMSTaxCodes.UnloadSelf
    Set moGMSTaxCodes = Nothing
    Set moContextMenuObj = Nothing     ' from **PRESTO**
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Setup Sales Tax Grid to Customer tax exemptions
    With moDmSTaxCodes
        .Table = "#TaxCodes"
        .UniqueKey = "STaxCodeKey"
        Set .Form = frmExemptions 'frmSalesTax
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdExemptions
        .NoAppend = True
        .BindColumn "STaxCodeKey", kColTaxCodeKey, SQL_INTEGER
        .BindColumn "STaxCodeID", kColTaxCode, SQL_VARCHAR
        .BindColumn "Description", kColTaxCodeDesc, SQL_VARCHAR
        .BindColumn "RgstrNo", kColTaxPayerID, SQL_VARCHAR
        .BindColumn "ExmptNo", kColExmpt, SQL_VARCHAR, , kDmSetNull
        .BindColumn "SortCol", kColSort, SQL_VARCHAR
        .OrderBy = "SortCol"
        .Init
    End With
    
    Set moGMSTaxCodes = New clsGridMgr
    With moGMSTaxCodes
        Set .Grid = grdExemptions
        
        Set .DM = moDmSTaxCodes
        Set .Form = frmExemptions
        .GridType = kGridDataSheet
        
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSortBlank As String
Dim sExmptNo As String
    
    moGMSTaxCodes.Grid_Change Col, Row
    
    If Col = kColExmpt Then
        'Blank columns should display below non-blank exemptions when sorted
        'ascending.   Use string of "Z"s for the sort column, which is VARCHAR(16)
        'while Exemption No is VARCHAR(15), so will insure this is the largest
        'value.
        sSortBlank = String(16, "Z")
    
        sExmptNo = gsGridReadCell(grdExemptions, Row, Col)
        
        If Len(Trim(sExmptNo)) = 0 Then
            gGridUpdateCell grdExemptions, Row, kColSort, sSortBlank
        Else
            gGridUpdateCell grdExemptions, Row, kColSort, sExmptNo
        End If
    End If
    
    mbExemptionsChanged = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sOrderBy As String
Dim bIsDirty As Boolean

    'Manage column sorting manually, so don't call grid manager Click

    
    If Row = 0 Then
        moDmSTaxCodes.Save
        If Col = kColExmpt Then
            'Sort by SortCol so all exemptions float to top when in ascending order
            'followed by blank exemptions.
            If miSortDirection = kSortDirectionAsc Then
                sOrderBy = "SortCol DESC"
                miSortDirection = kSortDirectionDesc
            Else
                sOrderBy = "SortCol ASC"
                miSortDirection = kSortDirectionAsc
            End If

            moDmSTaxCodes.OrderBy = sOrderBy
            moDmSTaxCodes.Init

        Else
            moDmSTaxCodes.SortByColumn Col

        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGMSTaxCodes.Grid_ColWidthChange Col1

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
     moGMSTaxCodes.Grid_EditMode Col, Row, Mode, ChangeMade
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_EditMode", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************
' Desc Set Up Tab Key to move between cells
'************************************************
    grdExemptions.ProcessTab = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdExemptions_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    moGMSTaxCodes.Grid_KeyDown KeyCode, Shift

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
     Cancel = Not moGMSTaxCodes.Grid_LeaveCell(Col, Row, NewCol, NewRow)

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdExemptions_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    moGMSTaxCodes.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdExemptions_TopLeftChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmExemptions"
End Function
#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = Nothing
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Private Sub tbrExemptions_ButtonClick(button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrExemptions_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
    
End Sub

Private Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
  'force last control lost_focus
    gbSetFocus Me, tbrExemptions
    DoEvents
    
    Select Case sKey
        Case kTbFinishExit
             
            If mbExemptionsChanged Then
                UpdateExemptions
            End If
    
            Me.Hide     'Hide form
        
        Case kTbCancelExit
            'Clear the grid changes when the user clicks on Cancel Exit
            If mbExemptionsChanged Then
                moDmSTaxCodes.Refresh
            End If
            Me.Hide
        
        Case kTbHelp
            
            gDisplayFormLevelHelp Me
            
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

