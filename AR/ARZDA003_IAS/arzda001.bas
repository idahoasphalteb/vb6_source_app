Attribute VB_Name = "basInvcEntry"
'************************************************************************************
'     Name: basInvoiceEntry
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 12/10/95 EDT
'     Mods: mm/dd/yy XXX
'************************************************************************************
Option Explicit
Public miHomeDigAfterDec    As Integer
Public msHomeCurrSymbol     As String
Public miHomeRoundPrec      As Integer
Public miHomeRoundMeth      As Integer
Public mlHomeCurrencyLocale As Long
Public mbEnterAsTab         As Boolean

    
'tarCustomer | BillingType
    Public Const kvOpenItem = 1
    Public Const kvCashOnly = 3
    Public Const kvBalanceForward = 2
    
Public Type CustOptions
    AllowCustRefund     As Boolean
    AllowInvtSubst      As Boolean
    AllowWriteOff       As Boolean
    BillingType         As Byte
    DfltItemKey         As Long
    DfltSalesAcctKey    As Long
    Hold                As Boolean
    PriceListKey        As Long
    PrimaryAddrKey      As Long
    ReqPO               As Boolean
    RetntPct            As Double
    ShipComplete        As Boolean
    Status              As Byte
    TradeDiscPct        As Double
    'from bill to
    CurrExchSchdKey     As Long
    CustClassKey        As Long
    ClassOvrdSegKey     As Long
    ClassOvrdSegVal     As String
    STaxSchdKey         As Long
     'national account information
    NatlAcctKey         As Long
    BillToParent        As Boolean
    PmtByParent         As Boolean
    NatlAcctLevelKey    As Long
    NatlAcctLevel       As Integer
    NatlAcctParentKey   As Long
    NatlAcctOnHold      As Boolean
    NatlAcctCrLimitUsed As Boolean
End Type

Public muCustDflts               As CustOptions
 
Public Type AROptions
    ChkCreditLimit      As Boolean
    CustMask            As String
    InclTradeDiscInSls  As Boolean
    InvcDetlRetnt       As Boolean
    InvcHdrRetnt        As Boolean
    InvcMask            As String
    PrintInvcs          As Boolean
    TrackSTaxOnSales    As Boolean
    UseMultCurr         As Boolean
    UseSper             As Boolean
    WarnForUnapplPmt    As Boolean
    SameNoRangeForMemo  As Boolean
    BatchSalesOvrd      As Boolean
    ClassSalesOvrd      As Boolean
    UseNatlAccts        As Boolean
End Type

Public muARDflts                As AROptions

Public Type IMOptions
    ItemMask            As String
    QtyDecPlaces        As Byte
    UnitCostDecPlaces   As Byte
    UnitPriceDecPlaces  As Byte
End Type

Public Type CompanyOptions
    msCompanyID         As String
    msDfltCurrency      As String
    mlLanguage          As Long
    msDfltCountry       As String
    mvBusinessDate      As Variant
    BatchOvrdSegKey     As Long
    BatchOvrdSegVal     As String
End Type

Public Type LocalStrings
    msInvoiceString         As String
    msARInvoiceNoString     As String
    msInvoiceTypeString     As String
    msBillToAddrString      As String
    msShipToAddrString      As String
    msInvoiceKeyString      As String
    msColumnString          As String
    msShipViaString         As String
    msReasonString          As String
    msPaymentTermsString    As String
    msBatchString           As String
    msCustomerTitle         As String
    msCustClassTitle        As String
End Type

Public Type InvcDetail
    InvcKey                 As Long
    InvoiceLineKey          As Long
    InvoiceLineDistKey      As Long
    CmntOnly                As Byte
    CommBase                As Byte
    ItemKey                 As Long
    AcctKey                 As Long
    AcctDesc                As String
    SeqNo                   As Long
    UnitPriceOvrd           As Byte
    SalesAmt                As Double
    TradeDiscAmt            As Double
    OrigCommAmt             As Double
    ActCommAmt              As Double
    Qty                     As Double
    SubjToTradeDisc         As Integer
    OvrdPrice               As Integer
    OvrdCost                As Integer
    STaxClassKey            As Long
    STaxClassID             As String
    FOBKey                  As Long
    FOBID                   As String
    STaxTranKey             As Long
    STaxSchdKey             As Long
    ShipMethKey             As Long
    ShipMethID              As String
    ShipZoneKey             As Long
    ShipZoneID              As String
    FreightAmt              As Double
    STaxAmt                 As Double
    UnitCost                As Double
End Type

Public Type GLOptions
    AcctMask                As String
    AllowAcctAOF            As Integer
    AllowAcctAdd            As Integer
End Type

  'Invoice detail grid columns
    Public Const kColIEItemIDmsk            As Integer = 1  'Masked Item ID
    Public Const kcolIEDescription          As Integer = 2  'Item Description
    Public Const kColIEItemID               As Integer = 3  'Item ID
    Public Const kcolIEItemKey              As Integer = 4  'Item Key
    Public Const kcolIEQtyView              As Integer = 5  'Quantity (viewable)
    Public Const kcolIEQty                  As Integer = 6  'Quantity (bound)
    Public Const kcolIEUnitMeasID           As Integer = 7  'Unit of Measure ID
    Public Const kcolIEUnitMeasKey          As Integer = 8  'Unit of Measure Key
    Public Const kcolIEUnitPrice            As Integer = 9  'Unit Price
    Public Const kcolIEUnitPriceOvrd        As Integer = 10 'Unit Price Override
    Public Const kcolIESalesAmtView         As Integer = 11 'Viewable Sales Amount
    Public Const kcolIESalesAmt             As Integer = 12 'Sales Amount (bound)
    Public Const kcolIESalesAcctIDmsk       As Integer = 13 'Sales acct Id Masked
    Public Const kcolIESalesAcctKey         As Integer = 14 'Sales acct Key
    Public Const kcolIESalesAcctID          As Integer = 15 'Sales acct ID (not Masked)
    Public Const kcolIESalesAcctDesc        As Integer = 16 'Sales acct Description
    Public Const kcolIESTaxClassID          As Integer = 17 'Sales Tax Class ID
    Public Const kcolIESTaxClassKey         As Integer = 18 'Sales Tax Class Key
    Public Const kcolIETradeDiscAmtView     As Integer = 19 'Trade discount Amount (viewable)
    Public Const kcolIETradeDiscAmt         As Integer = 20 'Trade Discount Amount(Bound)
    Public Const kcolIETradeDiscPct         As Integer = 21 'Trade discount Percent
    Public Const kcolIESubjToTradeDisc      As Integer = 22 'Item is Subject to trade discount
    Public Const kcolIEUnitCost             As Integer = 23 'Unit Cost (bound)
    Public Const kcolIEUnitCostView         As Integer = 24 'Unit Cost (viewable)
    Public Const kcolIECommPlanID           As Integer = 25 'Commission Plan ID
    Public Const kcolIECommPlanDtlKey       As Integer = 26 'Commission Plan Key
    Public Const kcolIECommClassID          As Integer = 27 'Commission Class ID
    Public Const kcolIECommClassDtlKey      As Integer = 28 'Commission Class Key
    Public Const kcolIECommBase             As Integer = 29 'Commission base
    Public Const kcolIEOrigCommAmtView      As Integer = 30 'Calculated Commission Amount
    Public Const kcolIEOrigCommAmt          As Integer = 31 'Original Commission Amount (bound)
    Public Const kcolIEActCommAmtView       As Integer = 32 'Actual Commission Amount
    Public Const kcolIEActCommAmt           As Integer = 33 'Actual Commission Amount (bound)
    Public Const kcolIEInvcKey              As Integer = 34 'Invoice Key
    Public Const kcolIECmntOnly             As Integer = 35 'Comment Only
    Public Const kcolIESeqNo                As Integer = 36 'Sequence Number
    Public Const kcolIEExtCmnt              As Integer = 37 'Extended Comment
    Public Const kcolIEProjClientID         As Integer = 38 'Project Client ID
    Public Const kcolIEProjID               As Integer = 39 'Project ID
    Public Const kcolIEInvoiceLineDistKey   As Integer = 40 'Invoice Line Dist Key
    Public Const kcolIEAcctRefKey           As Integer = 41 'Account Reference Key
    Public Const kcolIEAcctRefCode          As Integer = 42 'Account Reference Code
    Public Const kcolIEDistExtAmt           As Integer = 43 'Distribution line extended amount
    Public Const kcolIEFOBKey               As Integer = 44 'FOB Key
    Public Const kcolIEFOBID                As Integer = 45 'FOB ID
    Public Const kcolIEFreightAmt           As Integer = 46 'Freight Amount (bound)
    Public Const kcolIEFreightAmtView       As Integer = 47 'Freight amount (viewable)
    Public Const kcolIEInvoiceLineKey       As Integer = 48 'Invoice Line Key
    Public Const kcolIEQtyShipped           As Integer = 49 'Quantity Shipped
    Public Const kcolIEShipMethKey          As Integer = 50 'Shipping Method Key
    Public Const kcolIEShipMethID           As Integer = 51 'Shipping Method ID
    Public Const kcolIEShipZoneKey          As Integer = 52 'Shipping Zone Key
    Public Const kcolIEShipZoneID           As Integer = 53 'Shipping Zone ID
    Public Const kcolIESTaxSchdKey          As Integer = 54 'Sales Tax Schedule Key
    Public Const kcolIESTaxSchdID           As Integer = 55 'Sales Tax Schedule ID
    Public Const kcolIESTaxTranKey          As Integer = 56 'Sales Tax Transaction Key
    Public Const kcolIESTaxAmt              As Integer = 57 'Sales Tax Amount (bound)
    Public Const kcolIESTaxAmtView          As Integer = 58 'Sales Tax Amount (viewable)
    Public Const kcolIEKitInvoiceLineKey    As Integer = 59 'Kit invoice line key
    Public Const kcolIERtrnType             As Integer = 60 'Return Type
    Public Const kcolIEShipLineKey          As Integer = 61 'Ship line key
    Public Const kcolIESOLineKey            As Integer = 62 'SO Line Key
    Public Const kcolIEShipLineDistKey      As Integer = 63 'Ship line dist key
    Public Const kcolIESOLineDistKey        As Integer = 64 'SO Line Dist Key
    Public Const kcolIESOKey                As Integer = 65 'SO Key
    Public Const kcolIESOTranNoRel          As Integer = 66 'Sales Order
    'Intellisol start
    Public Const kColProjectID              As Integer = 67
    Public Const kColProjectKey             As Integer = 68
    Public Const kColPhaseID                As Integer = 69
    Public Const kColPhaseKey               As Integer = 70
    Public Const kColTaskID                 As Integer = 71
    Public Const kColTaskKey                As Integer = 72
    Public Const kColCostClassID            As Integer = 73
    Public Const kColCostClassKey           As Integer = 74
    Public Const kColVarianceStr            As Integer = 75
    Public Const kColVariance               As Integer = 76
    Public Const kColJobLineKey             As Integer = 77
    Public Const kColEstSale                As Integer = 78
    Public Const kColActSale                As Integer = 79
    Public Const kColInvDesc                As Integer = 80
    Public Const kColInvDescKey             As Integer = 81
    
    '*************************************************************
    '*************************************************************
    'RKL DEJ 2016-10-10 (START)
    '*************************************************************
    '*************************************************************
    Public Const kcolSOFrghtInPrice             As Integer = 82  'Include Freight in Price
    Public Const kcolSOFreightAmt               As Integer = 83  'Freight Amt from SO Line
    Public Const kcolSOSpreadInPrice            As Integer = 84  'Include Spreader In Price
    Public Const kcolSOSpreadAmt                As Integer = 85  'Spreader Amt from SO Line
    Public Const kcolSOItemAmt                  As Integer = 86  'Product Amount from SO Line Price Break
    Public Const kcolSOPriceBrkAmt              As Integer = 87  'Price Break Amt from SO Line Price Break
    Public Const kcolSOPriceBrkKey              As Integer = 88  'Price Break Key from SO Line
    Public Const kcolSOFrghtRate              As Integer = 89  'Price Break Freight Rate - From SO Line
    
    Public Const kcolSOSpreadTon              As Integer = 90  'Price Break Spread/Ton - From SO Header
    Public Const kcolSOSpreadHr              As Integer = 91  'Price Break Spread/Hr - From SO Header
    '*************************************************************
    '*************************************************************
    'RKL DEJ 2016-10-10 (STOP)
    '*************************************************************
    '*************************************************************
    
'    Public Const kMaxColsDetail             As Integer = 66 'Maximum Detail Columns
    Public Const kMaxColsDetail             As Integer = 91  'Maximum Detail Columns                'RKL DEJ 2016-10-10 Changed from 81 to 91
    Public Const kColBillMeth               As Integer = 92 'not used just need constant defined    'RKL DEJ 2016-10-10 Changed from 82 to 92
    Public Const kColBillMethKey            As Integer = 93 'not used just need constant            'RKL DEJ 2016-10-10 Changed from 83 to 93
    
    'Intellisol end
    
    Public Const kcolIEDefault = kColIEItemIDmsk 'Default Column
    
    ' Invoice Line Dist hidden grid columns
    Public Const kColChildInvoiceLineDistKey    As Integer = 1  'Invoice Line Dist Key
    Public Const kColChildAcctRefKey            As Integer = 2  'Account Ref Key
    Public Const kColChildAcctRefCode           As Integer = 3  'Account Ref Code
    Public Const kColChildExtAmt                As Integer = 4  'Extended line dist amt
    Public Const kColChildFOBKey                As Integer = 5  'FOB Key
    Public Const kColChildFOBID                 As Integer = 6  'FOB ID
    Public Const kColChildFreightAmt            As Integer = 7  'Freight Amount
    Public Const kColChildInvoiceLineKey        As Integer = 8  'Invoice Line Key
    Public Const kColChildQtyShipped            As Integer = 9  'Quantity Shipped
    Public Const kColChildSalesAcctKey          As Integer = 10  'Sales Account Key
    Public Const kColChildSalesAcctNo           As Integer = 11 'Sales Account number
    Public Const kColChildDescription           As Integer = 12 'Sales Account description
    Public Const kColChildShipMethKey           As Integer = 13 'Shipping Method Key
    Public Const kColChildShipMethID            As Integer = 14 'Shipping Method ID
    Public Const kColChildShipZoneKey           As Integer = 15 'Shipping Zone Key
    Public Const kColChildShipZoneID            As Integer = 16 'Shipping Zone ID
    Public Const kColChildSTaxSchdKey           As Integer = 17 'Sales Tax Schedule Key
    Public Const kColChildSTaxSchdID            As Integer = 18 'Sales Tax Schedule ID
    Public Const kColChildSTaxTranKey           As Integer = 19 'Sales Tax Transaction Key
    Public Const kColChildSTaxAmt               As Integer = 20 'Sales Tax Amount
    Public Const kColChildTradeDiscAmt          As Integer = 21 'Trade Discount Amount
    Public Const kColChildShipLineDistKey       As Integer = 22 'Ship Line Dist Key
    Public Const kColChildSOLineDistKey         As Integer = 23 'SO Line Dist Key
    Public Const kMaxColsLineDist               As Integer = 23 'Maximum distribution columns
    
    
'    Public Const ktxtIntlLanguage = 0 'for frmIntl
    
'    Public Const kcurIntlExchRate = 0
    
    Public Const klblIntlCurrDesc = 2
    Public Const knavIntlCurrency = 1
'    Public Const ktxtIntlCurrency = 1

  'Process id for unique commission work table key
    Public mlSpid                       As Long

  'Commissions form grid columns
    Public Const kColCommSper               As Integer = 1   'Salesperson
    Public Const kColCommSperName           As Integer = 2   'Salesperson
    Public Const kColCommSubjSalesView      As Integer = 3   'Amt Subj Sales
    Public Const kColCommSubjSales          As Integer = 4   'Amt Subj Sales
    Public Const kColCommSubjCOSView        As Integer = 5   'Amt Subj Cost Of Sales
    Public Const kColCommSubjCOS            As Integer = 6   'Amt Subj Cost Of Sales
    Public Const kColCommSperKey            As Integer = 7   'Salesperson Key
    Public Const kColCommBatchKey           As Integer = 8   'batch Key
    Public Const kColCommPaidAmt            As Integer = 9   'Commission Paid Amount
    Public Const kColCommOvrdUserID         As Integer = 10  'Commission Override User ID
    Public Const kColCommSalesCommKey       As Integer = 11   'Sales Commission Key
    Public Const kColCommSelected           As Integer = 12   'Commission Selected
    Public Const kColCommStatus             As Integer = 13  'Commission Status
    Public Const kColCommActCommAmtView     As Integer = 14  'Actual Commission Amount
    Public Const kColCommActCommAmt         As Integer = 15  'Actual Commission Amount
    Public Const kColCommCalcCommAmtView    As Integer = 16  'Calculated Commission Amount
    Public Const kColCommCalcCommAmt        As Integer = 17  'Calculated Commission Amount
    Public Const kColCommType               As Integer = 18  'Commission Type (code)
    Public Const kColCommTypeView           As Integer = 19  'Commission Type (text)
    Public Const kColCommEditAmt            As Integer = 20
    Public Const kColCommSplitPct           As Integer = 21
    Public Const kMaxCommCols               As Integer = 21  'Maximum Commission Columns
    
Const VBRIG_MODULE_ID_STRING = "ARZDA001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("arzda003.clsViewEditInvcMnt")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Public Function dSetValue(dAmt As Double) As Double
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
'  Returns a Value based on the transaction (-1 if a credit memo)
'**********************************************************************
        
    If frmInvcEntry.iGetTranType() = kTranTypeARCM Then
        dSetValue = -1 * dAmt
    Else
        dSetValue = dAmt
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "dSetValue", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basInvcEntry"
End Function
Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
Dim sText As String

    If lErr = guSotaErr.Number And Trim(guSotaErr.Description) <> Trim(sDesc) Then
        sDesc = sDesc & " " & guSotaErr.Description
    End If


    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & "  Company: " & oClass.moSysSession.CompanyID
        sText = sText & "  AppName:  " & App.Title
        sText = sText & "    Error < " & Format(lErr)
        sText = sText & " >   occurred at < " & sSub
        sText = sText & " >  in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

End Sub




