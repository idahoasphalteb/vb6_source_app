VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Begin VB.Form frmInvcEntry 
   Caption         =   "View / Edit Invoices"
   ClientHeight    =   7485
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9495
   HelpContextID   =   94870
   Icon            =   "arzda003.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7485
   ScaleWidth      =   9495
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   149
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin FPSpreadADO.fpSpread grdInvcLineDist 
      Height          =   1215
      Left            =   0
      TabIndex        =   147
      TabStop         =   0   'False
      Top             =   7680
      Visible         =   0   'False
      WhatsThisHelpID =   95073
      Width           =   9255
      _Version        =   524288
      _ExtentX        =   16325
      _ExtentY        =   2143
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "arzda003.frx":23D2
      AppearanceStyle =   0
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   137
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   138
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   139
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   140
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   141
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox txtPONum 
      Height          =   285
      Left            =   7760
      MaxLength       =   15
      TabIndex        =   9
      Top             =   540
      WhatsThisHelpID =   95067
      Width           =   1635
   End
   Begin VB.Frame fraInvcEntry 
      Caption         =   "&Invoice"
      Height          =   735
      Index           =   1
      Left            =   90
      TabIndex        =   2
      Top             =   495
      Width           =   4200
      Begin VB.ComboBox cboInvcType 
         Height          =   315
         Left            =   135
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   270
         WhatsThisHelpID =   95064
         Width           =   1545
      End
      Begin LookupViewControl.LookupView navMain 
         Height          =   285
         Left            =   3825
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   270
         WhatsThisHelpID =   14
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtInvcID 
         Height          =   285
         Left            =   2160
         TabIndex        =   5
         Top             =   270
         WhatsThisHelpID =   95062
         Width           =   1644
         _Version        =   65536
         _ExtentX        =   2900
         _ExtentY        =   508
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.34
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblInvoiceID 
         AutoSize        =   -1  'True
         Caption         =   "&No"
         Height          =   195
         Left            =   1845
         TabIndex        =   4
         Top             =   315
         Width           =   210
      End
   End
   Begin VB.TextBox txtPaddedCust 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      Height          =   285
      Left            =   9585
      MaxLength       =   15
      TabIndex        =   135
      TabStop         =   0   'False
      Top             =   3015
      Visible         =   0   'False
      WhatsThisHelpID =   95060
      Width           =   1500
   End
   Begin VB.TextBox txtPaddedInvc 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      Height          =   285
      Left            =   9585
      MaxLength       =   15
      TabIndex        =   134
      TabStop         =   0   'False
      Top             =   2655
      Visible         =   0   'False
      WhatsThisHelpID =   95059
      Width           =   1500
   End
   Begin TabDlg.SSTab tabInvcEntry 
      Height          =   5730
      Left            =   45
      TabIndex        =   13
      Top             =   1305
      WhatsThisHelpID =   95058
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   10107
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   6
      TabHeight       =   529
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Header"
      TabPicture(0)   =   "arzda003.frx":2810
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblSOnum"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "pnlHeaderTab"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtSONum"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "&Detail"
      TabPicture(1)   =   "arzda003.frx":282C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "pnlDetailTab"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Totals"
      TabPicture(2)   =   "arzda003.frx":2848
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "pnlTotalsTab"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "CustomTab"
      TabPicture(3)   =   "arzda003.frx":2864
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "pnlCustomizerTab"
      Tab(3).ControlCount=   1
      Begin Threed.SSPanel pnlCustomizerTab 
         Height          =   4605
         Left            =   -74955
         TabIndex        =   201
         Top             =   360
         Width           =   9285
         _Version        =   65536
         _ExtentX        =   16378
         _ExtentY        =   8123
         _StockProps     =   15
         BackColor       =   15790320
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelWidth      =   0
         BorderWidth     =   0
         BevelOuter      =   0
      End
      Begin VB.TextBox txtSONum 
         Enabled         =   0   'False
         Height          =   285
         Left            =   7695
         MaxLength       =   15
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         WhatsThisHelpID =   95057
         Width           =   1635
      End
      Begin Threed.SSPanel pnlDetailTab 
         Height          =   5235
         Left            =   -74910
         TabIndex        =   62
         Top             =   405
         Width           =   9195
         _Version        =   65536
         _ExtentX        =   16219
         _ExtentY        =   9234
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin FPSpreadADO.fpSpread grdInvcDetl 
            Height          =   2085
            Left            =   45
            TabIndex        =   80
            Top             =   3060
            WhatsThisHelpID =   95055
            Width           =   9105
            _Version        =   524288
            _ExtentX        =   16060
            _ExtentY        =   3678
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "arzda003.frx":2880
            AppearanceStyle =   0
         End
         Begin VB.PictureBox picLineDetail 
            BorderStyle     =   0  'None
            Height          =   2745
            Left            =   45
            Picture         =   "arzda003.frx":2CCA
            ScaleHeight     =   2745
            ScaleWidth      =   9105
            TabIndex        =   63
            TabStop         =   0   'False
            Top             =   45
            Width           =   9105
            Begin TabDlg.SSTab tabInvcDetail 
               Height          =   1575
               Left            =   0
               TabIndex        =   151
               TabStop         =   0   'False
               Top             =   1080
               WhatsThisHelpID =   95053
               Width           =   8160
               _ExtentX        =   14393
               _ExtentY        =   2778
               _Version        =   393216
               TabOrientation  =   1
               Style           =   1
               Tabs            =   10
               TabsPerRow      =   10
               TabHeight       =   529
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               TabCaption(0)   =   "Project"
               TabPicture(0)   =   "arzda003.frx":2DCC
               Tab(0).ControlEnabled=   -1  'True
               Tab(0).Control(0)=   "SSPanel4"
               Tab(0).Control(0).Enabled=   0   'False
               Tab(0).ControlCount=   1
               TabCaption(1)   =   "&Account"
               TabPicture(1)   =   "arzda003.frx":2DE8
               Tab(1).ControlEnabled=   0   'False
               Tab(1).Control(0)=   "SSPanel8"
               Tab(1).ControlCount=   1
               TabCaption(2)   =   "Freight"
               TabPicture(2)   =   "arzda003.frx":2E04
               Tab(2).ControlEnabled=   0   'False
               Tab(2).Control(0)=   "SSPanel5"
               Tab(2).ControlCount=   1
               TabCaption(3)   =   "Trade Disc"
               TabPicture(3)   =   "arzda003.frx":2E20
               Tab(3).ControlEnabled=   0   'False
               Tab(3).Control(0)=   "SSPanel1"
               Tab(3).ControlCount=   1
               TabCaption(4)   =   "Unit Cost"
               TabPicture(4)   =   "arzda003.frx":2E3C
               Tab(4).ControlEnabled=   0   'False
               Tab(4).Control(0)=   "SSPanel3"
               Tab(4).ControlCount=   1
               TabCaption(5)   =   "Commi&ssion"
               TabPicture(5)   =   "arzda003.frx":2E58
               Tab(5).ControlEnabled=   0   'False
               Tab(5).Control(0)=   "SSPanel6"
               Tab(5).ControlCount=   1
               TabCaption(6)   =   "Comm&ent"
               TabPicture(6)   =   "arzda003.frx":2E74
               Tab(6).ControlEnabled=   0   'False
               Tab(6).Control(0)=   "SSPanel7"
               Tab(6).ControlCount=   1
               TabCaption(7)   =   "Sales Ta&x"
               TabPicture(7)   =   "arzda003.frx":2E90
               Tab(7).ControlEnabled=   0   'False
               Tab(7).Control(0)=   "SSPanel2"
               Tab(7).ControlCount=   1
               TabCaption(8)   =   "Price Break"
               TabPicture(8)   =   "arzda003.frx":2EAC
               Tab(8).ControlEnabled=   0   'False
               Tab(8).Control(0)=   "pnlPriceBreak"
               Tab(8).Control(0).Enabled=   0   'False
               Tab(8).ControlCount=   1
               TabCaption(9)   =   "Custom"
               TabPicture(9)   =   "arzda003.frx":2EC8
               Tab(9).ControlEnabled=   0   'False
               Tab(9).Control(0)=   "SSPanel9"
               Tab(9).ControlCount=   1
               Begin Threed.SSPanel SSPanel8 
                  Height          =   735
                  Left            =   -74970
                  TabIndex        =   152
                  Top             =   30
                  Width           =   7500
                  _Version        =   65536
                  _ExtentX        =   13229
                  _ExtentY        =   1296
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin NEWSOTALib.SOTAMaskedEdit txtSalesAcct 
                     Height          =   285
                     Left            =   1770
                     TabIndex        =   153
                     Top             =   60
                     WhatsThisHelpID =   95051
                     Width           =   2265
                     _Version        =   65536
                     _ExtentX        =   3995
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                  End
                  Begin NEWSOTALib.SOTAMaskedEdit txtAcctRefCode 
                     Height          =   285
                     Left            =   1770
                     TabIndex        =   154
                     Top             =   390
                     WhatsThisHelpID =   95050
                     Width           =   2265
                     _Version        =   65536
                     _ExtentX        =   3995
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     lMaxLength      =   20
                  End
                  Begin VB.Label lblSalesAcct 
                     AutoSize        =   -1  'True
                     Caption         =   "Account"
                     Height          =   195
                     Left            =   240
                     TabIndex        =   157
                     Top             =   120
                     Width           =   600
                  End
                  Begin VB.Label lblSalesAcctDesc 
                     Height          =   195
                     Left            =   4170
                     TabIndex        =   156
                     Top             =   120
                     Width           =   3735
                  End
                  Begin VB.Label lblRef 
                     Height          =   195
                     Left            =   240
                     TabIndex        =   155
                     Top             =   405
                     Width           =   1455
                  End
               End
               Begin Threed.SSPanel SSPanel3 
                  Height          =   615
                  Left            =   -74880
                  TabIndex        =   158
                  Top             =   30
                  Width           =   7095
                  _Version        =   65536
                  _ExtentX        =   12515
                  _ExtentY        =   1085
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin NEWSOTALib.SOTACurrency curUnitCost 
                     Height          =   285
                     Left            =   1020
                     TabIndex        =   159
                     Top             =   120
                     WhatsThisHelpID =   95045
                     Width           =   1875
                     _Version        =   65536
                     bShowCurrency   =   0   'False
                     _ExtentX        =   3307
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin VB.Label lblUnitCost 
                     AutoSize        =   -1  'True
                     Caption         =   "Cost"
                     Height          =   195
                     Left            =   240
                     TabIndex        =   161
                     Top             =   180
                     Width           =   315
                  End
                  Begin VB.Label lblUnitCostCurrID 
                     Height          =   195
                     Left            =   2985
                     TabIndex        =   160
                     Top             =   180
                     Width           =   1695
                  End
               End
               Begin Threed.SSPanel SSPanel1 
                  Height          =   735
                  Left            =   -74970
                  TabIndex        =   162
                  Top             =   30
                  Width           =   6525
                  _Version        =   65536
                  _ExtentX        =   11509
                  _ExtentY        =   1296
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin NEWSOTALib.SOTACurrency curTradeDiscAmt 
                     Height          =   285
                     Left            =   1335
                     TabIndex        =   163
                     Top             =   60
                     WhatsThisHelpID =   95041
                     Width           =   1650
                     _Version        =   65536
                     bShowCurrency   =   0   'False
                     _ExtentX        =   2900
                     _ExtentY        =   508
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTANumber numTradeDiscPct 
                     Height          =   285
                     Left            =   1335
                     TabIndex        =   164
                     Top             =   375
                     WhatsThisHelpID =   95040
                     Width           =   675
                     _Version        =   65536
                     _ExtentX        =   1191
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   16777215
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
                     text            =   "  0.00"
                     sIntegralPlaces =   3
                     sDecimalPlaces  =   2
                  End
                  Begin VB.Label lblTradeDiscPct 
                     AutoSize        =   -1  'True
                     Caption         =   "Percent"
                     Height          =   195
                     Left            =   120
                     TabIndex        =   167
                     Top             =   420
                     Width           =   555
                  End
                  Begin VB.Label lblTradeDiscAmt 
                     AutoSize        =   -1  'True
                     Caption         =   "Amount"
                     Height          =   195
                     Left            =   120
                     TabIndex        =   166
                     Top             =   90
                     Width           =   540
                  End
                  Begin VB.Label lblTradeDiscPctSign 
                     Caption         =   "%"
                     Height          =   240
                     Index           =   55
                     Left            =   2040
                     TabIndex        =   165
                     Top             =   420
                     Width           =   240
                  End
               End
               Begin Threed.SSPanel SSPanel6 
                  Height          =   735
                  Left            =   -74940
                  TabIndex        =   168
                  Top             =   30
                  Width           =   7500
                  _Version        =   65536
                  _ExtentX        =   13229
                  _ExtentY        =   1296
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin NEWSOTALib.SOTAMaskedEdit txtCommClassDtl 
                     Height          =   285
                     Left            =   810
                     TabIndex        =   169
                     Top             =   390
                     WhatsThisHelpID =   95035
                     Width           =   1845
                     _Version        =   65536
                     _ExtentX        =   3254
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                  End
                  Begin NEWSOTALib.SOTAMaskedEdit txtCommPlanDtl 
                     Height          =   285
                     Left            =   810
                     TabIndex        =   170
                     Top             =   60
                     WhatsThisHelpID =   95034
                     Width           =   1845
                     _Version        =   65536
                     _ExtentX        =   3254
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                  End
                  Begin NEWSOTALib.SOTACurrency curActCommAmt 
                     Height          =   285
                     Left            =   3960
                     TabIndex        =   171
                     Top             =   330
                     WhatsThisHelpID =   95033
                     Width           =   1875
                     _Version        =   65536
                     _ExtentX        =   3307
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   16777215
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency curOrigCommAmt 
                     Height          =   285
                     Left            =   3960
                     TabIndex        =   172
                     TabStop         =   0   'False
                     Top             =   30
                     WhatsThisHelpID =   95032
                     Width           =   1875
                     _Version        =   65536
                     _ExtentX        =   3307
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     sBorder         =   0
                     mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin VB.Label lblOrigCommAmt 
                     AutoSize        =   -1  'True
                     Caption         =   "Orig Amt"
                     Height          =   195
                     Left            =   3285
                     TabIndex        =   178
                     Top             =   60
                     Width           =   600
                  End
                  Begin VB.Label lblActCommAmt 
                     AutoSize        =   -1  'True
                     Caption         =   "Act Amt"
                     Height          =   195
                     Left            =   3285
                     TabIndex        =   177
                     Top             =   375
                     Width           =   555
                  End
                  Begin VB.Label lblCommPlanDtl 
                     AutoSize        =   -1  'True
                     Caption         =   "Plan"
                     Height          =   195
                     Left            =   90
                     TabIndex        =   176
                     Top             =   105
                     Width           =   315
                  End
                  Begin VB.Label lblCommClassDtl 
                     AutoSize        =   -1  'True
                     Caption         =   "Class"
                     Height          =   195
                     Left            =   90
                     TabIndex        =   175
                     Top             =   420
                     Width           =   375
                  End
                  Begin VB.Label lblOrigAmtCurrID 
                     Height          =   195
                     Left            =   5910
                     TabIndex        =   174
                     Top             =   60
                     Width           =   1695
                  End
                  Begin VB.Label lblActAmtCurrID 
                     Height          =   195
                     Left            =   5910
                     TabIndex        =   173
                     Top             =   375
                     Width           =   1695
                  End
               End
               Begin Threed.SSPanel SSPanel2 
                  Height          =   615
                  Left            =   -74880
                  TabIndex        =   179
                  Top             =   30
                  Width           =   7245
                  _Version        =   65536
                  _ExtentX        =   12771
                  _ExtentY        =   1085
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   13.5
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin VB.CommandButton cmdSTaxDetl 
                     Caption         =   "..."
                     Height          =   285
                     Left            =   5880
                     TabIndex        =   181
                     ToolTipText     =   "Sales Tax Detail"
                     Top             =   165
                     WhatsThisHelpID =   95024
                     Width           =   285
                  End
                  Begin NEWSOTALib.SOTAMaskedEdit txtSTaxClass 
                     Height          =   285
                     Left            =   840
                     TabIndex        =   180
                     Top             =   165
                     WhatsThisHelpID =   95023
                     Width           =   2295
                     _Version        =   65536
                     _ExtentX        =   4048
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                  End
                  Begin NEWSOTALib.SOTACurrency curSTaxDetl 
                     Height          =   285
                     Left            =   4080
                     TabIndex        =   182
                     Top             =   165
                     WhatsThisHelpID =   95022
                     Width           =   1800
                     _Version        =   65536
                     bShowCurrency   =   0   'False
                     _ExtentX        =   3175
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin VB.Label lblSTaxClass 
                     AutoSize        =   -1  'True
                     Caption         =   "Class"
                     Height          =   195
                     Left            =   240
                     TabIndex        =   184
                     Top             =   210
                     Width           =   375
                  End
                  Begin VB.Label lblSTaxAmtDtl 
                     Caption         =   "Amount"
                     Height          =   165
                     Left            =   3360
                     TabIndex        =   183
                     Top             =   210
                     Width           =   615
                  End
               End
               Begin Threed.SSPanel SSPanel7 
                  Height          =   735
                  Left            =   -74160
                  TabIndex        =   185
                  Top             =   30
                  Width           =   6375
                  _Version        =   65536
                  _ExtentX        =   11245
                  _ExtentY        =   1296
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin VB.TextBox txtCommentDtl 
                     Height          =   495
                     Left            =   0
                     MaxLength       =   255
                     MultiLine       =   -1  'True
                     ScrollBars      =   2  'Vertical
                     TabIndex        =   186
                     Top             =   120
                     WhatsThisHelpID =   95018
                     Width           =   3645
                  End
               End
               Begin Threed.SSPanel SSPanel5 
                  Height          =   735
                  Left            =   -74970
                  TabIndex        =   187
                  Top             =   30
                  Width           =   7335
                  _Version        =   65536
                  _ExtentX        =   12938
                  _ExtentY        =   1296
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin NEWSOTALib.SOTAMaskedEdit txtShipMethDetl 
                     Height          =   285
                     Left            =   960
                     TabIndex        =   188
                     Top             =   60
                     WhatsThisHelpID =   95016
                     Width           =   2100
                     _Version        =   65536
                     _ExtentX        =   3704
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   -2147483633
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                  End
                  Begin NEWSOTALib.SOTACurrency curFrtAmtDetl 
                     Height          =   285
                     Left            =   4920
                     TabIndex        =   189
                     Top             =   60
                     WhatsThisHelpID =   95015
                     Width           =   1995
                     _Version        =   65536
                     bShowCurrency   =   0   'False
                     _ExtentX        =   3519
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTAMaskedEdit txtFOBDetl 
                     Height          =   285
                     Left            =   960
                     TabIndex        =   190
                     Top             =   390
                     WhatsThisHelpID =   95014
                     Width           =   2100
                     _Version        =   65536
                     _ExtentX        =   3704
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   -2147483633
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                  End
                  Begin VB.Label lblFreightDetl 
                     Caption         =   "Freight Amount"
                     Height          =   195
                     Left            =   3600
                     TabIndex        =   193
                     Top             =   120
                     Width           =   1215
                  End
                  Begin VB.Label lblFOBDetl 
                     Caption         =   "FOB"
                     Height          =   195
                     Left            =   180
                     TabIndex        =   192
                     Top             =   405
                     Width           =   855
                  End
                  Begin VB.Label lblShipViaDetl 
                     Caption         =   "Ship Via"
                     Height          =   195
                     Left            =   180
                     TabIndex        =   191
                     Top             =   120
                     Width           =   855
                  End
               End
               Begin Threed.SSPanel SSPanel4 
                  Height          =   735
                  Left            =   120
                  TabIndex        =   194
                  Top             =   30
                  Width           =   7515
                  _Version        =   65536
                  _ExtentX        =   13256
                  _ExtentY        =   1296
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelOuter      =   0
                  Begin NEWSOTALib.SOTACurrency curEstSale 
                     Height          =   285
                     Left            =   690
                     TabIndex        =   85
                     Top             =   400
                     WhatsThisHelpID =   95009
                     Width           =   1650
                     _Version        =   65536
                     bShowCurrency   =   0   'False
                     _ExtentX        =   2910
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency curActSale 
                     Height          =   285
                     Left            =   3330
                     TabIndex        =   87
                     Top             =   405
                     WhatsThisHelpID =   95008
                     Width           =   1650
                     _Version        =   65536
                     bShowCurrency   =   0   'False
                     _ExtentX        =   2910
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "            0.00"
                     sIntegralPlaces =   13
                     sDecimalPlaces  =   2
                  End
                  Begin EntryLookupControls.TextLookup lkuPhase 
                     Height          =   285
                     Left            =   3330
                     TabIndex        =   81
                     Top             =   60
                     WhatsThisHelpID =   95007
                     Width           =   1965
                     _ExtentX        =   3466
                     _ExtentY        =   503
                     ForeColor       =   -2147483640
                     Enabled         =   0   'False
                     MaxLength       =   15
                     EnabledLookup   =   0   'False
                     EnabledText     =   0   'False
                     VisibleLookup   =   0   'False
                     Protected       =   -1  'True
                     IsSurrogateKey  =   -1  'True
                     LookupID        =   "PhaseNum_FAN"
                     ParentIDColumn  =   "chrPhaseNumber"
                     ParentKeyColumn =   "intPhaseKey"
                     ParentTable     =   "tPA00002"
                     BoundColumn     =   "intPhaseKey"
                     BoundTable      =   "paarInvcLine"
                     IsForeignKey    =   -1  'True
                     sSQLReturnCols  =   "chrPhaseNumber,lkuPhase,;chrPhaseName,,;"
                  End
                  Begin EntryLookupControls.TextLookup lkuTask 
                     Height          =   285
                     Left            =   5760
                     TabIndex        =   83
                     Top             =   60
                     WhatsThisHelpID =   95006
                     Width           =   1965
                     _ExtentX        =   3466
                     _ExtentY        =   503
                     ForeColor       =   -2147483640
                     Enabled         =   0   'False
                     MaxLength       =   15
                     EnabledLookup   =   0   'False
                     EnabledText     =   0   'False
                     VisibleLookup   =   0   'False
                     Protected       =   -1  'True
                     IsSurrogateKey  =   -1  'True
                     LookupID        =   "TaskNum_FAN "
                     ParentIDColumn  =   "chrTaskNumber"
                     ParentKeyColumn =   "intTaskKey"
                     ParentTable     =   "tPA00005"
                     BoundColumn     =   "intTaskKey"
                     BoundTable      =   "paarInvcLine"
                     IsForeignKey    =   -1  'True
                     sSQLReturnCols  =   "chrTaskNumber,lkuTask,;chrTaskName,,;"
                  End
                  Begin EntryLookupControls.TextLookup lkuInvDesc 
                     Height          =   285
                     Left            =   5760
                     TabIndex        =   89
                     Top             =   400
                     WhatsThisHelpID =   95005
                     Width           =   1965
                     _ExtentX        =   3466
                     _ExtentY        =   503
                     ForeColor       =   -2147483640
                     Enabled         =   0   'False
                     MaxLength       =   15
                     EnabledLookup   =   0   'False
                     EnabledText     =   0   'False
                     VisibleLookup   =   0   'False
                     Protected       =   -1  'True
                     IsSurrogateKey  =   -1  'True
                     LookupID        =   "Inv_Desc_FAN"
                     ParentIDColumn  =   "chrInvoiceDescrptn"
                     ParentKeyColumn =   "intInvDescrKey"
                     ParentTable     =   " tPA00403"
                     BoundColumn     =   "intInvDescrKey"
                     BoundTable      =   "paarInvcLine"
                     IsForeignKey    =   -1  'True
                     sSQLReturnCols  =   "chrInvoiceDescrptn,lkuInvDescr,;"
                  End
                  Begin EntryLookupControls.TextLookup lkuProject 
                     Height          =   285
                     Left            =   690
                     TabIndex        =   79
                     Top             =   60
                     WhatsThisHelpID =   95004
                     Width           =   1965
                     _ExtentX        =   3466
                     _ExtentY        =   503
                     ForeColor       =   -2147483640
                     Enabled         =   0   'False
                     MaxLength       =   15
                     EnabledLookup   =   0   'False
                     EnabledText     =   0   'False
                     VisibleLookup   =   0   'False
                     Protected       =   -1  'True
                     IsSurrogateKey  =   -1  'True
                     LookupID        =   "Project"
                     ParentIDColumn  =   "chrJobNumber"
                     ParentKeyColumn =   "intJobKey"
                     ParentTable     =   "tPA00175"
                     BoundColumn     =   "intJobKey"
                     BoundTable      =   "paarInvcLine"
                     IsForeignKey    =   -1  'True
                     sSQLReturnCols  =   "chrJobNumber,lkuProject,;chrJobName,,;"
                  End
                  Begin VB.Label lblEstSale 
                     AutoSize        =   -1  'True
                     Caption         =   "Est. Sale"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   200
                     Top             =   400
                     Width           =   630
                  End
                  Begin VB.Label lblActSale 
                     AutoSize        =   -1  'True
                     Caption         =   "Act. Sale"
                     Height          =   195
                     Left            =   2640
                     TabIndex        =   199
                     Top             =   405
                     Width           =   645
                  End
                  Begin VB.Label lblProject 
                     AutoSize        =   -1  'True
                     Caption         =   "Project"
                     Height          =   195
                     Left            =   0
                     TabIndex        =   198
                     Top             =   60
                     Width           =   495
                  End
                  Begin VB.Label lblTask 
                     AutoSize        =   -1  'True
                     Caption         =   "Task"
                     Height          =   195
                     Left            =   5310
                     TabIndex        =   197
                     Top             =   60
                     Width           =   360
                  End
                  Begin VB.Label lblPhase 
                     AutoSize        =   -1  'True
                     Caption         =   "Phase"
                     Height          =   195
                     Left            =   2640
                     TabIndex        =   196
                     Top             =   60
                     Width           =   450
                  End
                  Begin VB.Label lblInvDesc 
                     AutoSize        =   -1  'True
                     Caption         =   "Inv Desc."
                     Height          =   195
                     Left            =   5040
                     TabIndex        =   195
                     Top             =   400
                     Width           =   690
                  End
               End
               Begin Threed.SSPanel SSPanel9 
                  Height          =   705
                  Left            =   -75000
                  TabIndex        =   202
                  Top             =   0
                  Width           =   7545
                  _Version        =   65536
                  _ExtentX        =   13317
                  _ExtentY        =   1244
                  _StockProps     =   15
                  BackColor       =   15790320
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelWidth      =   0
                  BorderWidth     =   0
                  BevelOuter      =   0
               End
               Begin Threed.SSPanel pnlPriceBreak 
                  Height          =   1100
                  Left            =   -74880
                  TabIndex        =   203
                  Top             =   120
                  Width           =   7500
                  _Version        =   65536
                  _ExtentX        =   13229
                  _ExtentY        =   1940
                  _StockProps     =   15
                  BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                     Name            =   "MS Sans Serif"
                     Size            =   12.05
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  BevelWidth      =   0
                  BorderWidth     =   0
                  BevelOuter      =   0
                  Begin VB.CheckBox chSpreader 
                     Alignment       =   1  'Right Justify
                     Caption         =   "Include Spreader In Price"
                     Enabled         =   0   'False
                     Height          =   255
                     Left            =   120
                     TabIndex        =   205
                     Top             =   360
                     Width           =   2175
                  End
                  Begin VB.CheckBox chFreight 
                     Alignment       =   1  'Right Justify
                     Caption         =   "Include Freight In Price"
                     Enabled         =   0   'False
                     Height          =   255
                     Left            =   120
                     TabIndex        =   204
                     Top             =   0
                     Width           =   2175
                  End
                  Begin NEWSOTALib.SOTACurrency numFreightPrice 
                     Height          =   285
                     Left            =   3600
                     TabIndex        =   206
                     Top             =   0
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency numSpreaderPrice 
                     Height          =   285
                     Left            =   3600
                     TabIndex        =   207
                     Top             =   360
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency numChildPrice 
                     Height          =   285
                     Left            =   3600
                     TabIndex        =   208
                     Top             =   780
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BackColor       =   -2147483637
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     bLocked         =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency numProdPrice 
                     Height          =   285
                     Left            =   1200
                     TabIndex        =   209
                     Top             =   720
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency numFreightRate 
                     Height          =   285
                     Left            =   6240
                     TabIndex        =   210
                     Top             =   0
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency numSpreadTon 
                     Height          =   285
                     Left            =   6240
                     TabIndex        =   211
                     Top             =   360
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin NEWSOTALib.SOTACurrency numSpreadHr 
                     Height          =   285
                     Left            =   6240
                     TabIndex        =   212
                     Top             =   720
                     Width           =   1260
                     _Version        =   65536
                     _ExtentX        =   2222
                     _ExtentY        =   503
                     _StockProps     =   93
                     BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                        Name            =   "MS Sans Serif"
                        Size            =   8.25
                        Charset         =   0
                        Weight          =   400
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Enabled         =   0   'False
                     bProtected      =   -1  'True
                     mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
                     text            =   "           0.00"
                     sDecimalPlaces  =   2
                  End
                  Begin VB.Label Label6 
                     Caption         =   "Spreader Amt"
                     Height          =   315
                     Left            =   2520
                     TabIndex        =   219
                     Top             =   360
                     Width           =   1125
                  End
                  Begin VB.Label Label5 
                     Caption         =   "Freight Amt"
                     Height          =   315
                     Left            =   2520
                     TabIndex        =   218
                     Top             =   0
                     Width           =   1005
                  End
                  Begin VB.Label Label4 
                     Caption         =   "Product Amt"
                     Height          =   315
                     Left            =   135
                     TabIndex        =   217
                     Top             =   750
                     Width           =   1005
                  End
                  Begin VB.Label lblChildPrice 
                     Caption         =   "Total Amt"
                     Height          =   315
                     Left            =   2520
                     TabIndex        =   216
                     Top             =   810
                     Width           =   1005
                  End
                  Begin VB.Label Label1 
                     Caption         =   "Freight Rate"
                     Height          =   315
                     Left            =   5160
                     TabIndex        =   215
                     Top             =   0
                     Width           =   1005
                  End
                  Begin VB.Label Label2 
                     Caption         =   "Spread/Ton"
                     Height          =   315
                     Left            =   5160
                     TabIndex        =   214
                     Top             =   360
                     Width           =   1005
                  End
                  Begin VB.Label Label3 
                     Caption         =   "Spread/Hr"
                     Height          =   315
                     Left            =   5160
                     TabIndex        =   213
                     Top             =   720
                     Width           =   1005
                  End
                  Begin VB.Line Line1 
                     X1              =   5040
                     X2              =   5040
                     Y1              =   0
                     Y2              =   1080
                  End
               End
               Begin VB.Label lblCommentDtl 
                  AutoSize        =   -1  'True
                  Caption         =   "Comment"
                  Height          =   195
                  Left            =   180
                  TabIndex        =   132
                  Top             =   -526
                  Width           =   660
               End
            End
            Begin VB.CommandButton cmdDtl 
               Caption         =   "&Undo"
               Height          =   360
               Index           =   1
               Left            =   8280
               TabIndex        =   78
               Top             =   1800
               WhatsThisHelpID =   94996
               Width           =   705
            End
            Begin VB.CommandButton cmdDtl 
               Caption         =   "&OK"
               Height          =   360
               Index           =   0
               Left            =   8280
               TabIndex        =   77
               Top             =   1200
               WhatsThisHelpID =   94995
               Width           =   705
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtItemDesc 
               Height          =   285
               Left            =   3300
               TabIndex        =   66
               Top             =   90
               WhatsThisHelpID =   94994
               Width           =   5685
               _Version        =   65536
               _ExtentX        =   10028
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               lMaxLength      =   40
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtUOM 
               Height          =   285
               Left            =   2880
               TabIndex        =   71
               Top             =   675
               WhatsThisHelpID =   94993
               Width           =   1695
               _Version        =   65536
               _ExtentX        =   2990
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtSalesAcctLoad 
               Height          =   285
               Left            =   6660
               TabIndex        =   76
               TabStop         =   0   'False
               Top             =   1560
               Visible         =   0   'False
               WhatsThisHelpID =   94992
               Width           =   2265
               _Version        =   65536
               _ExtentX        =   3995
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin NEWSOTALib.SOTAMaskedEdit TxtItemID 
               Height          =   285
               Left            =   495
               TabIndex        =   65
               Top             =   90
               WhatsThisHelpID =   94991
               Width           =   2550
               _Version        =   65536
               _ExtentX        =   4498
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTACurrency curSalesAmt 
               Height          =   285
               Left            =   6660
               TabIndex        =   75
               Top             =   675
               WhatsThisHelpID =   94990
               Width           =   2325
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   4101
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curUnitPrice 
               Height          =   285
               Left            =   4680
               TabIndex        =   73
               Top             =   675
               WhatsThisHelpID =   94989
               Width           =   1875
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numQty 
               Height          =   285
               Left            =   495
               TabIndex        =   69
               Top             =   675
               WhatsThisHelpID =   94988
               Width           =   2040
               _Version        =   65536
               _ExtentX        =   3598
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               mask            =   "<ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtItemIdLoad 
               Height          =   285
               Left            =   480
               TabIndex        =   67
               TabStop         =   0   'False
               Top             =   360
               Visible         =   0   'False
               WhatsThisHelpID =   94987
               Width           =   2550
               _Version        =   65536
               _ExtentX        =   4498
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label lblItemID 
               AutoSize        =   -1  'True
               Caption         =   "Ite&m"
               Height          =   195
               Left            =   90
               TabIndex        =   64
               Top             =   135
               Width           =   300
            End
            Begin VB.Label lblQty 
               AutoSize        =   -1  'True
               Caption         =   "&Quantity"
               Height          =   195
               Left            =   495
               TabIndex        =   68
               Top             =   450
               Width           =   585
            End
            Begin VB.Label lblUOM 
               AutoSize        =   -1  'True
               Caption         =   "Unit o&f Measure"
               Height          =   195
               Left            =   2880
               TabIndex        =   70
               Top             =   450
               Width           =   1125
            End
            Begin VB.Label lblUnitPrice 
               AutoSize        =   -1  'True
               Caption         =   "Unit P&rice"
               Height          =   195
               Left            =   4680
               TabIndex        =   72
               Top             =   450
               Width           =   690
            End
            Begin VB.Label lblSalesAmt 
               AutoSize        =   -1  'True
               Caption         =   "Sa&les Amount"
               Height          =   195
               Left            =   6660
               TabIndex        =   74
               Top             =   450
               Width           =   975
            End
         End
      End
      Begin Threed.SSPanel pnlHeaderTab 
         Height          =   5205
         Left            =   45
         TabIndex        =   16
         Top             =   360
         Width           =   9285
         _Version        =   65536
         _ExtentX        =   16378
         _ExtentY        =   9181
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraInvcEntry 
            Caption         =   "T&erms"
            Height          =   1230
            Index           =   5
            Left            =   4185
            TabIndex        =   55
            Top             =   3840
            Width           =   5055
            Begin NEWSOTALib.SOTAMaskedEdit txtPmtTerms 
               Height          =   285
               Left            =   1875
               TabIndex        =   57
               Top             =   210
               WhatsThisHelpID =   94979
               Width           =   1830
               _Version        =   65536
               _ExtentX        =   3228
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin SOTACalendarControl.SOTACalendar txtDiscDate 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   1875
               TabIndex        =   61
               Top             =   840
               WhatsThisHelpID =   94978
               Width           =   1830
               _ExtentX        =   3228
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin SOTACalendarControl.SOTACalendar txtDueDate 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   1875
               TabIndex        =   59
               Top             =   525
               WhatsThisHelpID =   94977
               Width           =   1830
               _ExtentX        =   3228
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin VB.Label lblPmtTerms 
               AutoSize        =   -1  'True
               Caption         =   "Payment Terms"
               Height          =   195
               Left            =   135
               TabIndex        =   56
               Top             =   270
               Width           =   1095
            End
            Begin VB.Label lblDiscDate 
               AutoSize        =   -1  'True
               Caption         =   "Discount Date"
               Height          =   195
               Left            =   135
               TabIndex        =   60
               Top             =   900
               Width           =   1020
            End
            Begin VB.Label lblDueDate 
               AutoSize        =   -1  'True
               Caption         =   "Due Date"
               Height          =   195
               Left            =   135
               TabIndex        =   58
               Top             =   585
               Width           =   690
            End
         End
         Begin VB.Frame fraInvcEntry 
            Caption         =   "C&ustomer"
            Height          =   1320
            Index           =   2
            Left            =   45
            TabIndex        =   17
            Top             =   60
            Width           =   9195
            Begin VB.CheckBox chkInDispute 
               Caption         =   "In Dispute"
               Height          =   285
               Left            =   7530
               TabIndex        =   31
               Top             =   930
               WhatsThisHelpID =   94972
               Width           =   1425
            End
            Begin VB.CommandButton cmdBillToAddr 
               Caption         =   "&Bill To Addr..."
               Height          =   285
               Left            =   7785
               TabIndex        =   21
               Top             =   270
               WhatsThisHelpID =   94971
               Width           =   1335
            End
            Begin VB.CommandButton cmdShipToAddr 
               Caption         =   "Ship To &Addr..."
               Height          =   285
               Left            =   7785
               TabIndex        =   25
               Top             =   585
               WhatsThisHelpID =   94970
               Width           =   1335
            End
            Begin VB.TextBox txtContName 
               Height          =   285
               Left            =   1665
               MaxLength       =   40
               TabIndex        =   27
               Top             =   900
               WhatsThisHelpID =   38
               Width           =   2940
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtCustClass 
               Height          =   285
               Left            =   5790
               TabIndex        =   30
               Top             =   900
               WhatsThisHelpID =   94968
               Width           =   1605
               _Version        =   65536
               _ExtentX        =   2831
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtShipTo 
               Height          =   285
               Left            =   1665
               TabIndex        =   23
               Top             =   585
               WhatsThisHelpID =   94967
               Width           =   1500
               _Version        =   65536
               _ExtentX        =   2646
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin LookupViewControl.LookupView navContact 
               Height          =   285
               Left            =   4650
               TabIndex        =   28
               TabStop         =   0   'False
               Top             =   900
               WhatsThisHelpID =   94966
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtShipToName 
               Height          =   285
               Left            =   3540
               TabIndex        =   24
               Top             =   600
               WhatsThisHelpID =   94965
               Width           =   4005
               _Version        =   65536
               _ExtentX        =   7064
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtBillToName 
               Height          =   285
               Left            =   3540
               TabIndex        =   20
               Top             =   300
               WhatsThisHelpID =   94964
               Width           =   4005
               _Version        =   65536
               _ExtentX        =   7064
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtBillTo 
               Height          =   285
               Left            =   1665
               TabIndex        =   19
               Top             =   270
               WhatsThisHelpID =   94963
               Width           =   1500
               _Version        =   65536
               _ExtentX        =   2646
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin VB.Label lblBillTo 
               AutoSize        =   -1  'True
               Caption         =   "Bill To"
               Height          =   195
               Left            =   135
               TabIndex        =   18
               Top             =   315
               Width           =   435
            End
            Begin VB.Label lblShipTo 
               AutoSize        =   -1  'True
               Caption         =   "Ship To"
               Height          =   195
               Left            =   135
               TabIndex        =   22
               Top             =   630
               Width           =   555
            End
            Begin VB.Label lblCustClass 
               AutoSize        =   -1  'True
               Caption         =   "Class"
               Height          =   195
               Left            =   5220
               TabIndex        =   29
               Top             =   945
               Width           =   375
            End
            Begin VB.Label lblContact 
               AutoSize        =   -1  'True
               Caption         =   "Contact"
               Height          =   195
               Left            =   135
               TabIndex        =   26
               Top             =   945
               Width           =   555
            End
         End
         Begin VB.Frame fraInvcEntry 
            Caption         =   "In&voice"
            Height          =   2220
            Index           =   3
            Left            =   45
            TabIndex        =   32
            Top             =   1470
            Width           =   9195
            Begin VB.TextBox txtCommentExt 
               Height          =   885
               Left            =   1680
               MaxLength       =   500
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   48
               Top             =   1185
               WhatsThisHelpID =   94739
               Width           =   6075
            End
            Begin VB.ComboBox cboReasonCode 
               Height          =   315
               Left            =   5940
               Style           =   2  'Dropdown List
               TabIndex        =   42
               Top             =   495
               WhatsThisHelpID =   94957
               Width           =   1800
            End
            Begin VB.CommandButton cmdCurrency 
               Caption         =   "Cu&rrency..."
               Height          =   285
               Left            =   7785
               TabIndex        =   37
               Top             =   180
               WhatsThisHelpID =   94956
               Width           =   1335
            End
            Begin VB.TextBox txtComment 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1680
               MaxLength       =   50
               TabIndex        =   49
               TabStop         =   0   'False
               Top             =   1185
               WhatsThisHelpID =   94955
               Width           =   6075
            End
            Begin VB.TextBox txtInvoiceForm 
               Height          =   285
               Left            =   1680
               MaxLength       =   15
               TabIndex        =   39
               Top             =   510
               WhatsThisHelpID =   94954
               Width           =   1815
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtCommPlan 
               Height          =   285
               Left            =   5940
               TabIndex        =   46
               Top             =   840
               WhatsThisHelpID =   94953
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtSalesperson 
               Height          =   285
               Left            =   1680
               TabIndex        =   44
               Top             =   840
               WhatsThisHelpID =   94952
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin LookupViewControl.LookupView navInvoiceForm 
               Height          =   285
               Left            =   3540
               TabIndex        =   40
               TabStop         =   0   'False
               Top             =   510
               WhatsThisHelpID =   94951
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtRecurInvcID 
               Height          =   285
               Left            =   5940
               TabIndex        =   36
               Top             =   180
               WhatsThisHelpID =   94950
               Width           =   1785
               _Version        =   65536
               _ExtentX        =   3149
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
            End
            Begin SOTACalendarControl.SOTACalendar txtInvcDate 
               CausesValidation=   0   'False
               Height          =   315
               Left            =   1680
               TabIndex        =   34
               Top             =   180
               WhatsThisHelpID =   94949
               Width           =   1815
               _ExtentX        =   3201
               _ExtentY        =   556
               BackColor       =   -2147483633
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               MaskedText      =   "  /  /    "
               Protected       =   -1  'True
               Text            =   "  /  /    "
               Object.CausesValidation=   0   'False
            End
            Begin VB.Label lblComment 
               AutoSize        =   -1  'True
               Caption         =   "Comment"
               Height          =   195
               Left            =   135
               TabIndex        =   47
               Top             =   1200
               Width           =   660
            End
            Begin VB.Label lblReasonCode 
               AutoSize        =   -1  'True
               Caption         =   "Reason"
               Height          =   195
               Left            =   4305
               TabIndex        =   41
               Top             =   570
               Width           =   555
            End
            Begin VB.Label lblCommPlan 
               AutoSize        =   -1  'True
               Caption         =   "Commission Plan"
               Height          =   195
               Left            =   4305
               TabIndex        =   45
               Top             =   915
               Width           =   1185
            End
            Begin VB.Label lblSalesperson 
               AutoSize        =   -1  'True
               Caption         =   "Salesperson"
               Height          =   195
               Left            =   135
               TabIndex        =   43
               Top             =   900
               Width           =   870
            End
            Begin VB.Label lblInvoiceForm 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Form"
               Height          =   195
               Left            =   135
               TabIndex        =   38
               Top             =   600
               Width           =   915
            End
            Begin VB.Label lblInvcDate 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Date"
               Height          =   195
               Left            =   135
               TabIndex        =   33
               Top             =   270
               Width           =   915
            End
            Begin VB.Label lblRecurInvcID 
               AutoSize        =   -1  'True
               Caption         =   "Recurring Invoice"
               Height          =   195
               Left            =   4305
               TabIndex        =   35
               Top             =   225
               Width           =   1260
            End
         End
         Begin VB.Frame fraInvcEntry 
            Caption         =   "&Shipping"
            Height          =   1230
            Index           =   4
            Left            =   45
            TabIndex        =   50
            Top             =   3840
            Width           =   4065
            Begin NEWSOTALib.SOTAMaskedEdit txtFOB 
               Height          =   285
               Left            =   1650
               TabIndex        =   54
               Top             =   525
               WhatsThisHelpID =   94940
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtShipMeth 
               Height          =   285
               Left            =   1650
               TabIndex        =   52
               Top             =   210
               WhatsThisHelpID =   94939
               Width           =   1815
               _Version        =   65536
               _ExtentX        =   3201
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
            End
            Begin VB.Label lblFOB 
               AutoSize        =   -1  'True
               Caption         =   "F.O.B."
               Height          =   195
               Left            =   135
               TabIndex        =   53
               Top             =   585
               Width           =   450
            End
            Begin VB.Label lblShipMeth 
               AutoSize        =   -1  'True
               Caption         =   "Ship Via"
               Height          =   195
               Left            =   135
               TabIndex        =   51
               Top             =   270
               Width           =   585
            End
         End
      End
      Begin Threed.SSPanel pnlTotalsTab 
         Height          =   4590
         Left            =   -74955
         TabIndex        =   82
         Top             =   360
         Width           =   9315
         _Version        =   65536
         _ExtentX        =   16425
         _ExtentY        =   8086
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraInvcEntry 
            Height          =   1425
            Index           =   8
            Left            =   4740
            TabIndex        =   122
            Top             =   1890
            Width           =   4515
            Begin NEWSOTALib.SOTAMaskedEdit txtBatch 
               Height          =   285
               Left            =   1620
               TabIndex        =   124
               Top             =   210
               WhatsThisHelpID =   94934
               Width           =   1875
               _Version        =   65536
               lAlignment      =   1
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
            End
            Begin NEWSOTALib.SOTACurrency curBatchTotal 
               Height          =   285
               Left            =   2205
               TabIndex        =   128
               TabStop         =   0   'False
               Top             =   900
               WhatsThisHelpID =   58
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTAMaskedEdit txtPostDate 
               Height          =   285
               Left            =   2280
               TabIndex        =   126
               Top             =   570
               WhatsThisHelpID =   94932
               Width           =   1200
               _Version        =   65536
               lAlignment      =   1
               _ExtentX        =   2117
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
            End
            Begin VB.Label lblCurrIDHC 
               AutoSize        =   -1  'True
               Caption         =   "             "
               Height          =   195
               Index           =   1
               Left            =   3675
               TabIndex        =   129
               Top             =   960
               Width           =   585
            End
            Begin VB.Label lblBatchTotal 
               AutoSize        =   -1  'True
               Caption         =   "Batch Total"
               Height          =   195
               Left            =   150
               TabIndex        =   127
               Top             =   960
               Width           =   825
            End
            Begin VB.Label lblPostDate 
               AutoSize        =   -1  'True
               Caption         =   "Posting Date"
               Height          =   195
               Left            =   150
               TabIndex        =   125
               Top             =   600
               Width           =   915
            End
            Begin VB.Label lblBatch 
               AutoSize        =   -1  'True
               Caption         =   "Batch"
               Height          =   195
               Left            =   150
               TabIndex        =   123
               Top             =   240
               Width           =   420
            End
         End
         Begin VB.CommandButton cmdComms 
            Caption         =   "Co&mmissions..."
            Height          =   330
            Left            =   7950
            TabIndex        =   131
            Top             =   4140
            WhatsThisHelpID =   94927
            Width           =   1275
         End
         Begin VB.CommandButton cmdCreditLimit 
            Caption         =   "C&redit Limit..."
            Height          =   330
            Left            =   7950
            TabIndex        =   130
            Top             =   3780
            WhatsThisHelpID =   94926
            Width           =   1275
         End
         Begin VB.Frame fraInvcEntry 
            Height          =   1785
            Index           =   7
            Left            =   4725
            TabIndex        =   111
            Top             =   45
            Width           =   4515
            Begin NEWSOTALib.SOTACurrency curTranAmtHC 
               Height          =   285
               Left            =   2205
               TabIndex        =   120
               TabStop         =   0   'False
               Top             =   1320
               Visible         =   0   'False
               WhatsThisHelpID =   94924
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curBalanceDue 
               Height          =   285
               Left            =   2190
               TabIndex        =   116
               TabStop         =   0   'False
               Top             =   645
               WhatsThisHelpID =   94923
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curTermsDiscAmt 
               Height          =   285
               Left            =   2190
               TabIndex        =   118
               TabStop         =   0   'False
               Top             =   990
               WhatsThisHelpID =   94922
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curInvcAmt 
               Height          =   285
               Left            =   2190
               TabIndex        =   113
               TabStop         =   0   'False
               Top             =   270
               WhatsThisHelpID =   94921
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblTranAmtHC 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Total"
               Height          =   195
               Left            =   150
               TabIndex        =   119
               Top             =   1350
               Visible         =   0   'False
               Width           =   930
            End
            Begin VB.Label lblCurrIDHC 
               AutoSize        =   -1  'True
               Caption         =   "             "
               Height          =   195
               Index           =   0
               Left            =   3675
               TabIndex        =   121
               Top             =   1350
               Visible         =   0   'False
               Width           =   585
            End
            Begin VB.Label lblDocCurrID 
               AutoSize        =   -1  'True
               Caption         =   "                  "
               Height          =   195
               Left            =   3675
               TabIndex        =   114
               Top             =   300
               Width           =   750
            End
            Begin VB.Label lblBalanceDue 
               AutoSize        =   -1  'True
               Caption         =   "Balance Due"
               Height          =   195
               Left            =   135
               TabIndex        =   115
               Top             =   645
               Width           =   930
            End
            Begin VB.Label lblInvcAmt 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Total"
               Height          =   195
               Left            =   135
               TabIndex        =   136
               Top             =   315
               Width           =   930
            End
            Begin VB.Label lblTermsDiscAmt 
               AutoSize        =   -1  'True
               Caption         =   "Scheduled Terms Discount"
               Height          =   195
               Left            =   135
               TabIndex        =   117
               Top             =   990
               Width           =   2160
            End
         End
         Begin VB.Frame fraInvcEntry 
            Height          =   3255
            Index           =   6
            Left            =   90
            TabIndex        =   84
            Top             =   45
            Width           =   4515
            Begin VB.CommandButton cmdFreight 
               Caption         =   "..."
               Height          =   285
               Left            =   3465
               TabIndex        =   148
               ToolTipText     =   "Allocate Freight"
               Top             =   630
               WhatsThisHelpID =   94913
               Width           =   285
            End
            Begin VB.Frame linInvcEntry 
               Height          =   45
               Index           =   1
               Left            =   1500
               TabIndex        =   108
               Top             =   2730
               Visible         =   0   'False
               Width           =   1905
            End
            Begin VB.Frame linInvcEntry 
               Height          =   75
               Index           =   2
               Left            =   1500
               TabIndex        =   99
               Top             =   1530
               Width           =   1905
            End
            Begin VB.CommandButton cmdSalesTax 
               Caption         =   "..."
               Height          =   285
               Left            =   3465
               TabIndex        =   98
               ToolTipText     =   "Sales Tax Detail"
               Top             =   1230
               WhatsThisHelpID =   94910
               Width           =   285
            End
            Begin NEWSOTALib.SOTANumber numTotTradeDiscPct 
               Height          =   285
               Left            =   3495
               TabIndex        =   94
               TabStop         =   0   'False
               Top             =   930
               WhatsThisHelpID =   94909
               Width           =   735
               _Version        =   65536
               _ExtentX        =   1296
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curHandling 
               Height          =   285
               Left            =   1530
               TabIndex        =   103
               TabStop         =   0   'False
               Top             =   2115
               Visible         =   0   'False
               WhatsThisHelpID =   94908
               Width           =   1875
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber numRetntPct 
               Height          =   285
               Left            =   3465
               TabIndex        =   106
               TabStop         =   0   'False
               Top             =   2430
               Visible         =   0   'False
               WhatsThisHelpID =   94907
               Width           =   735
               _Version        =   65536
               _ExtentX        =   1296
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
               text            =   "  0.00"
               sIntegralPlaces =   3
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curInvcTotal 
               Height          =   285
               Left            =   1530
               TabIndex        =   110
               TabStop         =   0   'False
               Top             =   2790
               Visible         =   0   'False
               WhatsThisHelpID =   94906
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curTotTradeDiscAmt 
               Height          =   285
               Left            =   1530
               TabIndex        =   93
               Top             =   930
               WhatsThisHelpID =   94905
               Width           =   1875
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curRetntAmt 
               Height          =   285
               Left            =   1530
               TabIndex        =   105
               TabStop         =   0   'False
               Top             =   2430
               Visible         =   0   'False
               WhatsThisHelpID =   94904
               Width           =   1875
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BackColor       =   12632256
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curSubTotal 
               Height          =   285
               Left            =   1530
               TabIndex        =   101
               TabStop         =   0   'False
               Top             =   1620
               WhatsThisHelpID =   94903
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curSalesTaxAmt 
               Height          =   285
               Left            =   1530
               TabIndex        =   97
               Top             =   1230
               WhatsThisHelpID =   94902
               Width           =   1875
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curFreightAmt 
               Height          =   285
               Left            =   1530
               TabIndex        =   91
               Top             =   630
               WhatsThisHelpID =   94901
               Width           =   1875
               _Version        =   65536
               bShowCurrency   =   0   'False
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTACurrency curTotalSalesAmt 
               Height          =   285
               Left            =   1530
               TabIndex        =   88
               TabStop         =   0   'False
               Top             =   330
               WhatsThisHelpID =   94900
               Width           =   1875
               _Version        =   65536
               _ExtentX        =   3307
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Enabled         =   0   'False
               bProtected      =   -1  'True
               sBorder         =   0
               mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "            0.00"
               sIntegralPlaces =   13
               sDecimalPlaces  =   2
            End
            Begin VB.Label lblHandling 
               AutoSize        =   -1  'True
               Caption         =   "Handling"
               Height          =   195
               Left            =   135
               TabIndex        =   102
               Top             =   2160
               Visible         =   0   'False
               Width           =   630
            End
            Begin VB.Label lblRetntPctSign 
               Caption         =   "%"
               Height          =   240
               Left            =   4230
               TabIndex        =   107
               Top             =   2475
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Label lblTotTradeDiscPctSign 
               Caption         =   "%"
               Height          =   240
               Index           =   53
               Left            =   4230
               TabIndex        =   95
               Top             =   960
               Width           =   240
            End
            Begin VB.Label lblInvcTotal 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Total"
               Height          =   195
               Left            =   135
               TabIndex        =   109
               Top             =   2835
               Visible         =   0   'False
               Width           =   930
            End
            Begin VB.Label lblTotTradeDiscAmt 
               AutoSize        =   -1  'True
               Caption         =   "Trade Discount"
               Height          =   195
               Left            =   135
               TabIndex        =   92
               Top             =   990
               Width           =   1095
            End
            Begin VB.Label lblRetentAmt 
               AutoSize        =   -1  'True
               Caption         =   "Retention"
               Height          =   195
               Left            =   135
               TabIndex        =   104
               Top             =   2475
               Visible         =   0   'False
               Width           =   690
            End
            Begin VB.Label lblSubTotal 
               AutoSize        =   -1  'True
               Caption         =   "Invoice Total"
               Height          =   195
               Left            =   135
               TabIndex        =   100
               Top             =   1650
               Width           =   930
            End
            Begin VB.Label lblSalesTaxAmt 
               AutoSize        =   -1  'True
               Caption         =   "Sales Tax"
               Height          =   195
               Left            =   135
               TabIndex        =   96
               Top             =   1305
               Width           =   705
            End
            Begin VB.Label lblFreightAmt 
               AutoSize        =   -1  'True
               Caption         =   "Freight"
               Height          =   195
               Left            =   135
               TabIndex        =   90
               Top             =   675
               Width           =   480
            End
            Begin VB.Label lblTotalSalesAmt 
               AutoSize        =   -1  'True
               Caption         =   "Sales"
               Height          =   195
               Left            =   135
               TabIndex        =   86
               Top             =   360
               Width           =   390
            End
         End
      End
      Begin VB.Label lblSOnum 
         AutoSize        =   -1  'True
         Caption         =   "Sales Order"
         Height          =   195
         Left            =   6660
         TabIndex        =   14
         Top             =   45
         Visible         =   0   'False
         Width           =   825
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   142
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   143
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   144
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   145
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   112
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCustID 
      Height          =   285
      Left            =   5250
      TabIndex        =   12
      Top             =   930
      WhatsThisHelpID =   94882
      Width           =   1650
      _Version        =   65536
      _ExtentX        =   2900
      _ExtentY        =   508
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.34
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
   End
   Begin NEWSOTALib.SOTACurrency curCostOfSales 
      Height          =   285
      Left            =   7500
      TabIndex        =   133
      TabStop         =   0   'False
      Top             =   1335
      Visible         =   0   'False
      WhatsThisHelpID =   94881
      Width           =   1875
      _Version        =   65536
      _ExtentX        =   3307
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin SysInfoLib.SysInfo sysInvoiceEntry 
      Left            =   10215
      Top             =   450
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7095
      WhatsThisHelpID =   73
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   688
   End
   Begin EntryLookupControls.TextLookup lkuMainProject 
      Height          =   285
      Left            =   5250
      TabIndex        =   8
      Top             =   540
      WhatsThisHelpID =   94878
      Width           =   1965
      _ExtentX        =   3466
      _ExtentY        =   503
      ForeColor       =   -2147483640
      Enabled         =   0   'False
      MaxLength       =   15
      EnabledLookup   =   0   'False
      EnabledText     =   0   'False
      VisibleLookup   =   0   'False
      Protected       =   -1  'True
      IsSurrogateKey  =   -1  'True
      LookupID        =   "JobMstAR_FAN"
      ParentIDColumn  =   "chrJobNumber"
      ParentKeyColumn =   "intJobKey"
      ParentTable     =   "vPA00175"
      BoundColumn     =   "intJobKey"
      BoundTable      =   "paarInvcHdr"
      IsForeignKey    =   -1  'True
      sSQLReturnCols  =   "chrJobNumber,lkuMainProject,;chrJobName,,;"
   End
   Begin FPSpreadADO.fpSpread grdJobLine 
      Height          =   1695
      Left            =   -75000
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   94877
      Width           =   1695
      _Version        =   524288
      _ExtentX        =   2990
      _ExtentY        =   2990
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "arzda003.frx":2EE4
      AppearanceStyle =   0
   End
   Begin VB.Label lblMainProject 
      AutoSize        =   -1  'True
      Caption         =   "Project"
      Height          =   195
      Left            =   4440
      TabIndex        =   150
      Top             =   585
      Width           =   495
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   146
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblCustName 
      BackStyle       =   0  'Transparent
      Height          =   225
      Left            =   7170
      TabIndex        =   11
      Top             =   960
      UseMnemonic     =   0   'False
      Width           =   2235
   End
   Begin VB.Label lblCustID 
      AutoSize        =   -1  'True
      Caption         =   "&Customer"
      Height          =   195
      Left            =   4440
      TabIndex        =   7
      Top             =   960
      Width           =   660
   End
   Begin VB.Label lblPONum 
      AutoSize        =   -1  'True
      Caption         =   "&PO"
      Height          =   195
      Left            =   7425
      TabIndex        =   10
      Top             =   585
      Width           =   225
   End
   Begin VB.Shape shpFocusRect 
      Height          =   420
      Left            =   9585
      Top             =   1080
      Visible         =   0   'False
      Width           =   555
   End
End
Attribute VB_Name = "frmInvcEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmInvcEntry
'     Desc: Invoice data entry
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 12/11/95 EDT
'     Mods:
'************************************************************************************
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If


'Context Menu Object
    Private moContextMenu As New clsContextMenu
    
'Don't run combo box click event code
    Public mbDontChkclick As Boolean
    
    Public m_MenuInfo           As New Collection       ' Collection for toolbar button menus

' Private variables of Form Properties
    Private mlRunMode               As Long
    Private mbSaved                 As Boolean
    Private mbCancelShutDown        As Boolean
    Private mbLoadSuccess           As Boolean
    Private mbValidating            As Boolean
    Private mbScrolling             As Boolean
    Private mbFormLoading           As Boolean
    Private mbFromSalesTaxKeypress  As Boolean
    Private mbFromFreightKeypress   As Boolean
    Private moAddr                  As Object
    Private moInvcCred              As Object
    Private oPrintInvcs             As Object
    
' Public Form Variables
    Public moClass                  As Object
    Public moSotaObjects            As New Collection
    Public moMapSrch                As New Collection
    Private moLastControl           As Object
    
' Binding Object Variables
    Public moDmHeader               As New clsDmForm            'Invoice Header
    Public moDmDetail               As New clsDmGrid            'Invoice Detail
    Public moDmLineDist             As New clsDmGrid            'Invoice Line Dist
    Public moDmComms                As New clsDmGrid            'Commissions
    Public moDmBTAddress            As New clsDmForm            'Bill To Addresses
    Public moDmSTAddress            As New clsDmForm            'Ship to addresses
    Public moSTax                   As Object                   'Sales Tax common window

    Public moDmHeaderExt            As New clsDmForm            'RKL DEJ 2016-07-07 - Invoice Header Extension
    

    Private miFilter                As Integer
  
  'Validation/Line entry
    Public moLEGridInvcDetl         As New clsLineEntry
    Private moLostFocus             As New clsValidationManager
    Private moValidate              As New clsValidate
  
    Private Const ktskprint = 84479301
    
'Add on the fly variables
    Private mbAllowUserFieldValueAOF    As Boolean
    Private mbAllowCustomerAOF          As Boolean
    Private mbAllowCustClassAOF         As Boolean
    Private mbAllowGLAccountAOF         As Boolean
    Private mbAllowSalesPersonAOF       As Boolean
    Private mbAllowShipMethAOF          As Boolean
    Private mbAllowReasonCodeAOF        As Boolean
    Private mbAllowPmtTermsAOF          As Boolean
    Private mbAllowContactAOF           As Boolean
    Private mbAllowCommPlanAOF          As Boolean
    Private mbAllowCommClassAOF         As Boolean
    Private mbAllowSTaxClassAOF         As Boolean
    Private mbAllowUOMAOF               As Boolean
    Private mbAllowBusFormAOF           As Boolean
    Private mbAllowItemAOF              As Boolean
    
' Miscellaneous Variables
    Private muIMDflts               As IMOptions
    Private muCompanyDflts          As CompanyOptions
    Private muGLDflts               As GLOptions
    Private muInvcDtl               As InvcDetail
    Private muLS                    As LocalStrings
    Private miSecurityLevel         As Integer
    Private msVBGLAcctNoMask        As String
    Private mlInvcsRunMode          As Long
    Private ErrorNumber             As Integer
    Private miApplyMeth             As Integer
    Private msRefCodeLabel          As String
    Private miRefCodeUsage          As Integer
    
'Intellisol start
    'The following 2 variables are never used, but were added to allow the form to compile
    'with a new version of Project.cls
    Public mlSTaxTranKey            As Long
    Public mlSTaxSchdKey            As Long
'Intellisol end
    
' Resize Form Variables
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    
' Column Editable setting
    Private Const kModeReadOnly = 1                         'No editing allowed on Tax form - read only
    Private Const kModeEditable = 2                         'Editing allowed on tax form
    Private Const kModeScheduleOnly = 3                     'Editing is allowed only on Tax schedule field
    
' Currency control constants
    Private Const kiMaxB4DecFor153 = 12
    Private Const kiMaxB4DecFor126 = 6


' Main Tab constants
    Private Const ktabIEHeader = 0
    Private Const ktabIEDetail = 1
    Private Const ktabIETotals = 2
    Private Const ktabIECustomizer = 3
    
    
' Detail tab constants
'Intellisol start
'    Private Const ktabIEAccount = 0
'    Private Const ktabIEFreight = 1
'    Private Const ktabIETradeDisc = 2
'    Private Const ktabIEUnitCost = 3
'    Private Const ktabIECommission = 4
'    Private Const ktabIEComment = 5
'    Private Const ktabIESalesTax = 6
'    Private Const ktabIEProjAcct = 7
    Private Const kiProjectTab = 0
    Private Const ktabIEProjAcct = 0
    Private Const ktabIEAccount = 1
    Private Const ktabIEFreight = 2
    Private Const ktabIETradeDisc = 3
    Private Const ktabIEUnitCost = 4
    Private Const ktabIECommission = 5
    Private Const ktabIEComment = 6
    Private Const ktabIESalesTax = 7
    '*************************************************************
    '*************************************************************
    'RKL DEJ 2016-10-10 (START)
    '*************************************************************
    '*************************************************************
    Private Const ktabPriceBreaks = 8     'RKL DEJ 2016-10-10 - This is new
    Private Const ktabIESubCustomizer = 9   'RKL DEJ 2016-10-10 - This was 8 changed to 9
    '*************************************************************
    '*************************************************************
    'RKL DEJ 2016-10-10 (STOP)
    '*************************************************************
    '*************************************************************
'Intellisol end

' LE Ok and Cancel constants
    Private Const kLEOK = 0
    Private Const kLEUndo = 1
    
' Invoice Type list box constants
    Private msarrTranTypeID() As String 'holds 2-char tran ids
    
    
    ' *** RMM 5/2/01, Context menu project
    Private mbIMActivated As Boolean

'Status bar messages
    Private Const kstatusReady = 1
    Private Const kstatusnewinvoice = 2
    Private Const kstatusloading = 3
    Private Const kstatusenterinvctype = 4
    
    Private Const kInvcsRunModeDTE = 2
    Private Const kInvcsRunModeAOF = 1

    Private Const kmaxInvoiceIDChars = 10
    Private Const kmaxCustIDChars = 12
    
'context sensitive help menu ids
    Private Const ktskARApplDD As Integer = 100
    
Private Const ktskViewShipmentsDD = 134611744
Private Const kclsViewShipmentsDD = "sozdr001.clsSOViewShip"

'Offsets for Sage MAS 500 Status control
    Private iTopDiff                    As Integer
    Private iRightDiff                  As Integer
    
'Intellisol start
    Private moProject As clsProject
'Intellisol end

' AvaTax integration - General Declarations
    Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
    Public mbAvaTaxEnabled          As Boolean ' Defines if AvaTax is installed on the DB & Client , and is turned on for this company
' AvaTax end

Const VBRIG_MODULE_ID_STRING = "ARZDA003.FRM"

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "ARZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let lInvcsRunMode(lNewInvcsRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlInvcsRunMode = lNewInvcsRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInvcsRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get lInvcsRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lInvcsRunMode = mlInvcsRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInvcsRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "ARZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Private Sub MapControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'ZZDebug.Print "MapControls"
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    'Map Search buttons to the object collection for them
  'Contacts
    giCollectionAdd moMapSrch, navContact, txtContName.hwnd
  'Invoice form
    giCollectionAdd moMapSrch, navInvoiceForm, txtInvoiceForm.hwnd
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'************************************************************************************
'   bSaved is used specifically for Add-On-The-Fly.  It tells the
'   the calling object whether the record was saved or not.
'************************************************************************************
Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   bLoadSuccess is used to assist the class in determining whether or
'   not the form loaded correctly.
'************************************************************************
    bLoadSuccess = mbLoadSuccess
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadSuccess_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   bCancelshutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Private Sub SetCurrencyControl(CurCtl As Control, lCurrLocale As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
' Setup The Currency Controls
'************************************************************************************
Dim bOldMb As Boolean
    bOldMb = mbFormLoading
    mbFormLoading = True
'    CurCtl.CurrencySymbol = sCurrSymbol
'    CurCtl.DecimalPlaces = miDecimalPlaces
'    CurCtl.Amount = CurCtl.Amount
    CurCtl.CurrencyLocale = lCurrLocale
    mbFormLoading = bOldMb
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetCurrencyControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DMDataDisplayed(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMDataDisplayed is called by the data Manager from within the
'   KeyChange method, after attempting to select a recordset from
'   the database.  By the time this subroutine is called, the
'   DataManager's state has changed to either kDmStateAdd or
'   kDmStateEdit.  This routine is called whether or not the
'   record exists After all links and all data has been displayed.
'
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
'====================================================================
'   **** Special NOTE ****
'   If you had any New Record Defaults in the SetFormControls for
'   and Add,AddAOF state, then you would move that code here.
'   If you had any variable/tag initializations, you would move that
'   code here from the SetForm controls.
'
'   The DMReposition subroutine is NOT REQUIRED by the data Manager.
'====================================================================
Dim sCurrID     As String
Dim lCustkey    As Long
Dim lBatchKey   As Long
Dim rs          As Object
Dim sSQL        As String
Dim bNegDoc     As Boolean

    If oDm Is moDmHeader Then
      
      'Set currency controls from the values passed in (multiplied by -1 if credit memo)
        bSetFormCurrencyControls gvCheckNull(moDmHeader.GetColumnValue("CurrID"), SQL_CHAR)
      'Sales Amount
        curTotalSalesAmt.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("SalesAmt"), SQL_INTEGER))
      'Freight Amount
        curFreightAmt.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("ShipAmt"), SQL_INTEGER))
      'Trade Discount
        curTotTradeDiscAmt.Amount = -1 * dSetValue(gvCheckNull(moDmHeader.GetColumnValue("TradeDiscAmt"), SQL_INTEGER))
      'Sales Tax
        If muARDflts.TrackSTaxOnSales Then
            sCurrID = gvCheckNull(moDmHeader.GetColumnValue("CurrID"), SQL_CHAR)
            If moDmHeader.GetColumnValue("TranType") = kTranTypeARCM Then         ' Credit Memo
                bNegDoc = True
            Else
                bNegDoc = False
            End If
            moSTax.bLoadDocTaxes glGetValidLong(oDm.GetColumnValue("InvcKey")), sCurrID, bNegDoc
        Else
            curSalesTaxAmt.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("STaxAmt"), SQL_INTEGER))
        End If
        
        curSalesTaxAmt.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("STaxAmt"), SQL_INTEGER))
      'Handling
        curHandling.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("HandlAmt"), SQL_INTEGER))
      'Retention
        curRetntAmt.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("RetntAmt"), SQL_INTEGER))
      'Terms Discount
        curTermsDiscAmt.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("DiscAmt"), SQL_INTEGER))
      'Cost Of Sales
        curCostOfSales.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("CostOfSales"), SQL_INTEGER))
      'Get Remaing Totals
        curBalanceDue.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("Balance"), SQL_INTEGER))
        
        sCurrID = gvCheckNull(moDmHeader.GetColumnValue("CurrID"))
        lblDocCurrID.Caption = sCurrID
        If sCurrID = muCompanyDflts.msDfltCurrency Then
            curTranAmtHC.Visible = False
            lblTranAmtHC.Visible = False
            lblCurrIDHC(0).Visible = False
        Else
          'Set Transaction Amount in Home Currency
            curTranAmtHC.Amount = dSetValue(gvCheckNull(moDmHeader.GetColumnValue("TranAmtHC"), SQL_INTEGER))
            curTranAmtHC.Visible = True
            lblTranAmtHC.Visible = True
            lblCurrIDHC(0).Visible = True
        End If
        
        lBatchKey = gvCheckNull(moDmHeader.GetColumnValue("BatchKey"), SQL_INTEGER)
    
        sSQL = "SELECT PostDate, BatchID, BatchTotal FROM tciBatchLog WHERE BatchKey = "
        sSQL = sSQL & Format(lBatchKey)
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                
        If rs.IsEOF Then
            txtBatch.Text = ""
            txtPostDate.Text = ""
            curBatchTotal.Amount = 0
        Else
            txtBatch.Text = gvCheckNull(rs.Field("BatchID"))
            txtPostDate.Text = Format(gsFormatDateFromDB( _
                Format(gvCheckNull(rs.Field("PostDate")), "SHORT DATE")), _
                gsGetLocalVBDateMask())
            curBatchTotal.Amount = gvCheckNull(rs.Field("BatchTotal"), SQL_INTEGER)
        End If
        
        Set rs = Nothing
        
        CreateLineJoin moDmHeader
        
        'Intellisol start
        moProject.DMGetViewEditInvoice glGetValidLong(oDm.GetColumnValue("InvcKey"))
        moProject.ResetJobGrid
        'Intellisol end
        
    End If
    
    moLostFocus.SetAllValid

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DMReposition(oDm As clsDmForm)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMReposition is called by the Data Manager from within the
'   KeyChange method, after attempting to select a recordset from
'   the database.  By the time this subroutine is called, the
'   DataManager's state has changed to either kDmStateAdd or
'   kDmStateEdit.  This routine is called whether or not the
'   record exists (in either add or edit state).
'
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
    Dim oToolbarMenu As clsToolbarMenuInfo
    Dim lInvcKey As Long
    Dim lCustkey As Long
    
    If oDm Is moDmHeader Then

        'Set Memo button state (for drop-down multiple-memo implementation)
        lInvcKey = gvCheckNull(moDmHeader.GetColumnValue("InvcKey"), SQL_INTEGER)
        lCustkey = gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeARInvoice)
        oToolbarMenu.lKey2 = lInvcKey
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeARCustomer)
        oToolbarMenu.lKey2 = lCustkey
        gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain
          
    End If
                
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DMStateChange(oDMO As Object, iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Description:
'        Automatically fired by DM when the form is cleared or loaded.
'************************************************************************************
    
  'Set toolbar buttons
    
    
    Select Case True
        Case oDMO Is moDmHeader
            
            tbrMain.SetState (iNewState)
            Select Case iNewState
                Case kDmStateAdd, kDmStateEdit
                    If muARDflts.PrintInvcs Then
                        tbrMain.Buttons(kTbPrint).Enabled = True
                        moClass.lUIActive = kChildObjectActive
                    End If
                    ' AvaTax Integration - DMStateChange - enable kTbAvalara
                    If mbAvaTaxEnabled Then
                        tbrMain.ButtonEnabled(kTbAvalara) = True
                    End If
                    ' AvaTax end

                Case kDmStateNone
                    tbrMain.Buttons(kTbPrint).Enabled = False
                    curTranAmtHC.Visible = False
                    lblTranAmtHC.Visible = False
                    lblCurrIDHC(0).Visible = False
                                        
                    txtBatch.Text = ""
                    txtPostDate.Text = ""
                    curBatchTotal.Amount = 0
                    
                    moClass.lUIActive = kChildObjectInactive
                  'reenable billto
                    miApplyMeth = 0
                    sbrMain.Message = ""
                    sbrMain.MessageVisible = False
                    muCustDflts.Hold = False
                    DisplayStatus
                    cboInvcType.Enabled = True
                    If Me.Visible Then
                        gbSetFocus Me, cboInvcType
                    End If
                    bSetFormCurrencyControls muCompanyDflts.msDfltCurrency

                    ' AvaTax Integration - DMStateChange - disable kTbAvalara
                    If mbAvaTaxEnabled Then
                        tbrMain.ButtonEnabled(kTbAvalara) = False
                    End If
                    ' AvaTax end

            End Select
    
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************************
' Unload any additional forms
' Clean up Sage MAS 500 Objects
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next
    
    Dim i As Integer
    
    For i = 0 To Forms.Count - 1
        If (Not Forms(i) Is Me And Not Forms(i) Is Nothing) Then
            Unload Forms(i)
        End If
    Next
     
    gUnloadChildForms Me ' Address form
    
    'Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1

    ' AvaTax Integration - PerformCleanShutDown
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    ' AvaTax end

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************************
' Set up data manager objects - hook controls to fields and setup
'****************************************************************************

   
  'Invoice header data manger object
    
  'Invoice header - tarPendInvoice
    With moDmHeader
        Set .Form = frmInvcEntry
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        .SaveOrder = 3
        .Table = "tarInvoice"
        .Where = ""
        '.UniqueKey = "TranNo, TranType, CompanyID, BatchKey"
        .UniqueKey = "TranNo, TranType, CompanyID"
        .AccessType = kDmBuildQueries
        '.GenerateSPs = True
        
        .Bind Nothing, "InvcKey", SQL_INTEGER
        .Bind Nothing, "AuthOvrdAmt", SQL_NUMERIC
        .Bind Nothing, "Balance", SQL_FLOAT
        .Bind Nothing, "BatchKey", SQL_INTEGER
        .Bind Nothing, "BillToAddrKey", SQL_INTEGER
        .Bind Nothing, "BillToCustAddrKey", SQL_INTEGER
        .Bind Nothing, "CommPlanKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind Nothing, "ConfirmToCntctKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CostOfSales", SQL_NUMERIC
        .Bind Nothing, "CreateType", SQL_SMALLINT
        .Bind Nothing, "CurrExchRate", SQL_NUMERIC
        .Bind Nothing, "CurrExchSchdKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "CurrID", SQL_CHAR
        .Bind Nothing, "CustClassKey", SQL_INTEGER
        .Bind Nothing, "CustKey", SQL_INTEGER
        .Bind txtPONum, "CustPONo", SQL_CHAR, kDmSetNull
        .Bind Nothing, "DiscAmt", SQL_NUMERIC
        .Bind txtDiscDate, "DiscDate", SQL_DATE, kDmSetNull
        .Bind txtDueDate, "DueDate", SQL_DATE, kDmSetNull
        .Bind Nothing, "FOBKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "HandlAmt", SQL_NUMERIC
        .Bind Nothing, "InvcFormKey", SQL_INTEGER, kDmSetNull
        .Bind chkInDispute, "InDispute", SQL_INTEGER
        .Bind Nothing, "PmtTermsKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "PrimarySperKey", SQL_INTEGER, kDmSetNull
        .BindComboBox cboReasonCode, "ReasonCodeKey", SQL_INTEGER, kDmUseItemData Or kDmSetNull
        .Bind Nothing, "RecurInvoiceKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "RetntAmt", SQL_NUMERIC
        .Bind Nothing, "RetntPct", SQL_NUMERIC
        .Bind Nothing, "STaxAmt", SQL_NUMERIC
        .Bind Nothing, "STaxCalc", SQL_SMALLINT
        .Bind Nothing, "STaxTranKey", SQL_INTEGER, kDmUseItemData Or kDmSetNull
        .Bind Nothing, "SalesAmt", SQL_NUMERIC
        .Bind Nothing, "ShipAmt", SQL_NUMERIC
        .Bind Nothing, "ShipMethKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "ShipToAddrKey", SQL_INTEGER
        .Bind Nothing, "ShipToCustAddrKey", SQL_INTEGER
        .Bind Nothing, "ShipZoneKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "SourceModule", SQL_SMALLINT
        .Bind Nothing, "Status", SQL_SMALLINT
        .Bind Nothing, "TradeDiscAmt", SQL_NUMERIC
        .Bind Nothing, "TranAmt", SQL_NUMERIC
        .Bind Nothing, "TranAmtHC", SQL_NUMERIC
        .Bind txtComment, "TranCmnt", SQL_CHAR, kDmSetNull
        .Bind txtInvcDate, "TranDate", SQL_DATE
        .Bind Nothing, "TranID", SQL_CHAR, kDmSetNull
        .Bind txtPaddedInvc, "TranNo", SQL_CHAR
        .Bind Nothing, "TranType", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
     
         
        .LinkSource "tciBusinessForm", "BusinessFormKey=<<InvcFormKey>>" 'and bus form = 2
        .Link txtInvoiceForm, "BusinessFormID"
        
        .LinkSource "tarCustAddr", "AddrKey=<<BillToCustAddrKey>>"
        .Link txtBillTo, "CustAddrID"

        .LinkSource "tarCustAddr", "AddrKey=<<ShipToCustAddrKey>>"
        .Link txtShipTo, "CustAddrID"
'        .Link txtSTaxSchd, "STaxSchdKey"
        
        .LinkSource "tciShipMethod", "ShipMethKey=<<ShipMethKey>>"
        .Link txtShipMeth, "ShipMethID"
        
        .LinkSource "tciFOB", "FOBKey=<<FOBKey>>"
        .Link txtFOB, "FOBID"
        
        .LinkSource "tciPaymentTerms", "PmtTermsKey=<<PmtTermsKey>>"
        .Link txtPmtTerms, "PmtTermsID"
        
        .LinkSource "tarCustomer", "CustKey=<<CustKey>>"
        .Link txtCustID, "CustID"
        .Link lblCustName, "CustName"
        
        .LinkSource "tarCustClass", "CustClassKey=<<CustClassKey>>"
        .Link txtCustClass, "CustClassID"
        
        .LinkSource "tarSalesperson", "SperKey=<<PrimarySperKey>>"
        .Link txtSalesperson, "SperID"
        
        .LinkSource "tciContact", "CntctKey=<<ConfirmToCntctKey>>"
        .Link txtContName, "Name"
        
        .LinkSource "tarRecurInvoice", "RecurInvoiceKey=<<RecurInvoiceKey>>"
        .Link txtRecurInvcID, "RecurInvcID"
        
        'these are combo boxes
        
        .LinkSource "tarCommPlan", "CommPlanKey=<<CommPlanKey>>"
        .Link txtCommPlan, "CommPlanID"
        
        .Init
    End With
    
    '*******************************************************************************************
    'RKL Custom (START)
    '*******************************************************************************************
    'RKL DEJ 2016-07-07
    With moDmHeaderExt
        Set .Form = frmInvcEntry
        Set .Session = moClass.moSysSession
        .AppName = Me.Caption
        Set .Database = moClass.moAppDB
        
        .Table = "tarInvoiceExt_SGS"
        .UniqueKey = "InvcKey"
        Set .Parent = moDmHeader
        Set .SOTAStatusBar = sbrMain
        Set .Toolbar = tbrMain
'        .SaveOrder = 9

        .Bind txtCommentExt, "Comment", SQL_VARCHAR, kDmSetNull
        
        .ParentLink "InvcKey", "InvcKey", SQL_INTEGER

        .Init

    End With
    '*******************************************************************************************
    'RKL Custom (STOP)
    '*******************************************************************************************
    
    'Grid detail lines
    
    With moDmDetail
        .Table = "tarInvoiceDetl"
        .UniqueKey = "InvoiceLineKey"
        .OrderBy = "SeqNo"
        .NoAppend = True
        .SaveOrder = 5
        
        Set .Parent = moDmHeader
        Set .Grid = grdInvcDetl
        Set .Form = frmInvcEntry
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        
        '.BindColumn "InvcKey", kColIEInvcKey, SQL_INTEGER
        .BindColumn "InvoiceLineKey", kcolIEInvoiceLineKey, SQL_INTEGER
        .BindColumn "ActCommAmt", kcolIEActCommAmt, SQL_NUMERIC
        .BindColumn "CalcCommAmt", kcolIEOrigCommAmt, SQL_NUMERIC
        .BindColumn "CmntOnly", kcolIECmntOnly, SQL_SMALLINT
        .BindColumn "CommBase", kcolIECommBase, SQL_SMALLINT
        .BindColumn "CommClassKey", kcolIECommClassDtlKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "CommPlanKey", kcolIECommPlanDtlKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "Description", kcolIEDescription, SQL_CHAR, txtItemDesc
        .BindColumn "ExtAmt", kcolIESalesAmt, SQL_NUMERIC
        .BindColumn "ExtCmnt", kcolIEExtCmnt, SQL_CHAR, txtCommentDtl, kDmSetNull
        .BindColumn "ItemKey", kcolIEItemKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "SeqNo", kcolIESeqNo, SQL_INTEGER
        .BindColumn "STaxClassKey", kcolIESTaxClassKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "TradeDiscAmt", kcolIETradeDiscAmt, SQL_NUMERIC
        .BindColumn "UnitCost", kcolIEUnitCost, SQL_NUMERIC, curUnitCost
        .BindColumn "UnitMeasKey", kcolIEUnitMeasKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "UnitPrice", kcolIEUnitPrice, SQL_NUMERIC, curUnitPrice, kDmSetNull
        .BindColumn "UnitPriceOvrd", kcolIEUnitPriceOvrd, SQL_SMALLINT
'Intellisol start
'        .BindColumn "ProjClientID", kcolIEProjClientID, SQL_CHAR, txtProjClient, kDmSetNull
'        .BindColumn "ProjID", kcolIEProjID, SQL_CHAR, txtProjID, kDmSetNull
'Intellisol end
        .BindColumn "KitInvoiceLineKey", kcolIEKitInvoiceLineKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "RtrnType", kcolIERtrnType, SQL_SMALLINT, Nothing
        .BindColumn "ShipLineKey", kcolIEShipLineKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "SOLineKey", kcolIESOLineKey, SQL_INTEGER, Nothing, kDmSetNull
        
        .ParentLink "InvcKey", "InvcKey", SQL_INTEGER
        
        .LinkSource "tsoSOLine", "tarInvoiceDetl.SOLineKey*=tsoSOLine.SOLineKey", kDmJoin
        .Link kcolIESOKey, "SOKey"
        
        .LinkSource "timItem", "tarInvoiceDetl.ItemKey*=timItem.ItemKey", kDmJoin
        .Link kColIEItemID, "ItemID", TxtItemID
        .Link kcolIESubjToTradeDisc, "SubjToTradeDisc"
        
        .LinkSource "tciSTaxClass", "tarInvoiceDetl.STaxClassKey*=tciSTaxClass.STaxClassKey", kDmJoin
        .Link kcolIESTaxClassID, "STaxClassID", txtSTaxClass
        
        .LinkSource "tarCommClass", "tarInvoiceDetl.CommClassKey*=tarCommClass.CommClassKey", kDmJoin
        .Link kcolIECommClassID, "CommClassID", txtCommClassDtl
        
        .LinkSource "tarCommPlan", "tarInvoiceDetl.CommPlanKey*=tarCommPlan.CommPlanKey", kDmJoin
        .Link kcolIECommPlanID, "CommPlanID", txtCommPlanDtl
        
        .LinkSource "tciUnitMeasure", "tarInvoiceDetl.UnitMeasKey*=tciUnitMeasure.UnitMeasKey", kDmJoin
        .Link kcolIEUnitMeasID, "UnitMeasID", txtUOM
        
        .LinkSource "#tarInvoiceLineJoin", "tarInvoiceDetl.InvoiceLineKey *= #tarInvoiceLineJoin.InvoiceLineKey", kDmJoin
        .Link kcolIEInvoiceLineDistKey, "InvoiceLineDistKey"
        .Link kcolIEAcctRefKey, "AcctRefKey"
        .Link kcolIEAcctRefCode, "AcctRefCode"
        .Link kcolIEDistExtAmt, "ExtAmt", curSalesAmt
        .Link kcolIEFOBKey, "FOBKey"
        .Link kcolIEFOBID, "FOBID"
        .Link kcolIEFreightAmt, "FreightAmt", curFrtAmtDetl
        .Link kcolIEQty, "QtyShipped"
        .Link kcolIESalesAcctKey, "SalesAcctKey"
        .Link kcolIESalesAcctID, "SalesAcctNo", txtSalesAcct
        .Link kcolIESalesAcctDesc, "Description", lblSalesAcctDesc
        .Link kcolIEShipMethKey, "ShipMethKey"
        .Link kcolIEShipMethID, "ShipMethID"
        .Link kcolIEShipZoneKey, "ShipZoneKey"
        .Link kcolIEShipZoneID, "ShipZoneID"
        .Link kcolIESTaxSchdKey, "STaxSchdKey"
        .Link kcolIESTaxSchdID, "STaxSchdID"
        .Link kcolIESTaxTranKey, "STaxTranKey"
        .Link kcolIESTaxAmt, "STaxAmt"
        .Link kcolIETradeDiscAmt, "TradeDiscAmt", curTradeDiscAmt
        .Link kcolIEShipLineDistKey, "ShipLineDistKey"
        .Link kcolIESOLineDistKey, "SOLineDistKey"
        
        '*******************************************************************************
        '*******************************************************************************
        'RKL DEJ 2016-10-10 (START)
        '*******************************************************************************
        '*******************************************************************************
        .LinkSource "tsoSOLineExt_SGS", "tarInvoiceDetl.SOLineKey=tsoSOLineExt_SGS.SOLineKey", kDmJoin, LeftOuter
        .Link kcolSOFrghtInPrice, "IncludeFreightInPrice", chFreight
        .Link kcolSOFreightAmt, "FreightAmt", numFreightPrice
        .Link kcolSOSpreadInPrice, "IncludeSpreaderInPrice", chSpreader
        .Link kcolSOSpreadAmt, "SpreaderAmt", numSpreaderPrice
        .Link kcolSOItemAmt, "ProductAmt", numProdPrice
        .Link kcolSOPriceBrkAmt, "PriceBreakPrice", numChildPrice
        .Link kcolSOFrghtRate, "FreightRate", numFreightRate
        
        '*******************************************************************************
        '*******************************************************************************
        'RKL DEJ 2016-10-10 (STOP)
        '*******************************************************************************
        '*******************************************************************************
        
        .Init

    End With
    
    ' Line Distribution hidden grid
    With moDmLineDist
        Set .Form = frmInvcEntry
        Set .Session = moClass.moSysSession
        Set .Grid = grdInvcLineDist
        Set .Parent = moDmDetail
        Set .Database = moClass.moAppDB
        .SaveOrder = 4
        .Table = "tarInvoiceLineDist"
        .UniqueKey = "tarInvoiceLineDist.InvoiceLineKey, InvoiceLineDistKey"
        .NoAppend = True
        .ReadOnly = True

                
'        .BindColumn "InvoiceLineKey", kColChild, SQL_INTEGER
        .BindColumn "InvoiceLineDistKey", kColChildInvoiceLineDistKey, SQL_INTEGER
        .BindColumn "AcctRefKey", kColChildAcctRefKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ExtAmt", kColChildExtAmt, SQL_DECIMAL
        .BindColumn "FOBKey", kColChildFOBKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "FreightAmt", kColChildFreightAmt, SQL_DECIMAL
        .BindColumn "QtyShipped", kColChildQtyShipped, SQL_DECIMAL
        .BindColumn "SalesAcctKey", kColChildSalesAcctKey, SQL_INTEGER
        .BindColumn "ShipMethKey", kColChildShipMethKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "ShipZoneKey", kColChildShipZoneKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "STaxTranKey", kColChildSTaxTranKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "TradeDiscAmt", kColChildTradeDiscAmt, SQL_DECIMAL, , kDmSetNull
        .BindColumn "ShipLineDistKey", kColChildShipLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "SOLineDistKey", kColChildSOLineDistKey, SQL_INTEGER, , kDmSetNull
        
        .ParentLink "InvoiceLineKey", "InvoiceLineKey", SQL_INTEGER
        .Init
    
        .LinkSource "tciFOB", "tarInvoiceLineDist.FOBKey *= tciFOB.FOBKey", kDmJoin
        .Link kColChildFOBID, "FOBID"
    
        .LinkSource "tglAccount", "tarInvoiceLineDist.SalesAcctKey *= tglAccount.GLAcctKey", kDmJoin
        .Link kColChildSalesAcctNo, "GLAcctNo"
        .Link kColChildDescription, "Description"

        .LinkSource "tciShipMethod", "tarInvoiceLineDist.ShipMethKey*=tciShipMethod.ShipMethKey", kDmJoin
        .Link kColChildShipMethID, "ShipMethID"

        .LinkSource "tciShipZone", "tarInvoiceLineDist.ShipZoneKey*=tciShipZone.ShipZoneKey", kDmJoin
        .Link kColChildShipZoneID, "ShipZoneID"
        
        .LinkSource "tglAcctRef", "tarInvoiceLineDist.AcctRefKey*=tglAcctRef.AcctRefKey", kDmJoin
        .Link kColChildAcctRefCode, "AcctRefCode", txtAcctRefCode
                    
        .LinkSource "#tarInvoiceLineJoin", "tarInvoiceLineDist.STaxTranKey *= #tarInvoiceLineJoin.STaxTranKey", kDmJoin
        .Link kColChildSTaxSchdKey, "STaxSchdKey"
        .Link kColChildSTaxSchdID, "STaxSchdID"
        .Link kColChildSTaxAmt, "STaxAmt"

        .Init
        
    End With
    
    'Common Information Address Binds Bill To Address
    With moDmBTAddress
         
        .Table = "tciAddress"
        .UniqueKey = "AddrKey"
        .SaveOrder = 1              'Save 1st (parent of tarPendInvoice)
'        .AccessType = kDmStoredProcs
        Set .Parent = moDmHeader
        Set .Form = frmInvcEntry
        Set .Database = moClass.moAppDB
        Set .Session = moClass.moSysSession
        '.GenerateSPs = True
        
        .Bind txtBillToName, "AddrName", SQL_CHAR
        .Bind Nothing, "AddrLine1", SQL_CHAR
        .Bind Nothing, "AddrLine2", SQL_CHAR
        .Bind Nothing, "AddrLine3", SQL_CHAR
        .Bind Nothing, "AddrLine4", SQL_CHAR
        .Bind Nothing, "AddrLine5", SQL_CHAR
        .Bind Nothing, "CountryID", SQL_CHAR
        'State, City And Postal Code must be bound after country
        .Bind Nothing, "StateID", SQL_CHAR, kDmSetNull
        .Bind Nothing, "City", SQL_CHAR
        .Bind Nothing, "PostalCode", SQL_CHAR, kDmSetNull
        .Bind Nothing, "Latitude", SQL_DECIMAL, kDmSetNull
        .Bind Nothing, "Longitude", SQL_DECIMAL, kDmSetNull
        '.ParentLink "AddrKey", "BillToAddrKey", SQL_INTEGER
        .Bind Nothing, "AddrKey", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        
        .Init
    
    End With

    'Common Information Address Binds
    With moDmSTAddress
         
        .Table = "tciAddress"
        .UniqueKey = "AddrKey"
        .SaveOrder = 2              'Save 2nd (parent of tarPendInvoice)
'        .AccessType = kDmStoredProcs
        Set .Parent = moDmHeader
        Set .Form = frmInvcEntry
        Set .Database = moClass.moAppDB
        Set .Session = moClass.moSysSession
        '.GenerateSPs = True
        
        .Bind txtShipToName, "AddrName", SQL_CHAR
        .Bind Nothing, "AddrLine1", SQL_CHAR
        .Bind Nothing, "AddrLine2", SQL_CHAR
        .Bind Nothing, "AddrLine3", SQL_CHAR
        .Bind Nothing, "AddrLine4", SQL_CHAR
        .Bind Nothing, "AddrLine5", SQL_CHAR
        .Bind Nothing, "CountryID", SQL_CHAR
        'State, City And Postal Code must be bound after country
        .Bind Nothing, "StateID", SQL_CHAR, kDmSetNull
        .Bind Nothing, "City", SQL_CHAR
        .Bind Nothing, "PostalCode", SQL_CHAR, kDmSetNull
        .Bind Nothing, "Latitude", SQL_DECIMAL, kDmSetNull
        .Bind Nothing, "Longitude", SQL_DECIMAL, kDmSetNull
        '.ParentLink "AddrKey", "ShipToAddrKey", SQL_INTEGER
        .Bind Nothing, "AddrKey", SQL_INTEGER
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        
        .Init
    
    End With

    
    
  'New schema tied to Address record
    With moDmComms
        .Table = "tarSalesComm"
        .UniqueKey = "SalesCommKey"
        .SaveOrder = 6
        Set .Parent = moDmHeader
        Set .Database = moDmHeader.Database
        .ParentLink "InvcKey", "InvcKey", SQL_INTEGER
        Set .Form = frmInvcEntry
        Set .Session = moClass.moSysSession
        Set .Grid = frmCommissions.grdComms
        .NoAppend = True
        
        .BindColumn "SperKey", kColCommSperKey, SQL_INTEGER
        .BindColumn "BatchKey", kColCommBatchKey, SQL_INTEGER, Nothing, kDmSetNull
        .BindColumn "ActCommAmt", kColCommActCommAmt, SQL_DECIMAL
        .BindColumn "CalcCommAmt", kColCommCalcCommAmt, SQL_DECIMAL
        .BindColumn "CommType", kColCommType, SQL_INTEGER
        .BindColumn "CommPaidAmt", kColCommPaidAmt, SQL_DECIMAL
        .BindColumn "OvrdUserID", kColCommOvrdUserID, SQL_CHAR
        .BindColumn "SalesCommKey", kColCommSalesCommKey, SQL_INTEGER
        .BindColumn "Selected", kColCommSelected, SQL_INTEGER
        .BindColumn "Status", kColCommStatus, SQL_INTEGER
        .BindColumn "SubjCOS", kColCommSubjCOS, SQL_INTEGER
        .BindColumn "SubjSales", kColCommSubjSales, SQL_INTEGER
        .BindColumn "EditCommAmt", kColCommEditAmt, SQL_DECIMAL
        .BindColumn "CommSplitPct", kColCommSplitPct, SQL_DECIMAL, Nothing, kDmSetNull
        
        .LinkSource "tarSalesperson", "tarSalesperson.SperKey=tarSalesComm.SperKey", kDmJoin
        .Link kColCommSper, "SperID"
        .Link kColCommSperName, "SperName"
   
        .Init
    
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function CreateLineJoin(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim lKey    As Long
Dim sSQL    As String
Static bdid As Boolean

    If oDm Is moDmHeader Then
        
        lKey = moDmHeader.GetColumnValue("InvcKey")
        
        If Not bdid Then
            sSQL = "CREATE TABLE #tarInvoiceLineJoin"
            sSQL = sSQL & "(InvoiceLineDistKey  int         NULL, "
            sSQL = sSQL & "AcctRefKey           int         NULL, "
            sSQL = sSQL & "AcctRefCode          varchar(20)    NULL, "
            sSQL = sSQL & "ExtAmt               dec(15,3)   NULL, "
            sSQL = sSQL & "FOBKey               int         NULL, "
            sSQL = sSQL & "FOBID                varchar(15)    NULL, "
            sSQL = sSQL & "FreightAmt           dec(15,3)   NULL, "
            sSQL = sSQL & "InvoiceLineKey       int         NULL, "
            sSQL = sSQL & "QtyShipped           dec(16,8)   NULL, "
            sSQL = sSQL & "SalesAcctKey         int         NULL, "
            sSQL = sSQL & "SalesAcctNo          varchar(100)   NULL, "
            sSQL = sSQL & "Description          varchar(40)    NULL, "
            sSQL = sSQL & "ShipMethKey          int         NULL, "
            sSQL = sSQL & "ShipMethID           varchar(15)    NULL, "
            sSQL = sSQL & "ShipZoneKey          int         NULL, "
            sSQL = sSQL & "ShipZoneID           varchar(15)    NULL, "
            sSQL = sSQL & "STaxSchdKey          int         NULL, "
            sSQL = sSQL & "STaxSchdID           varchar(15)    NULL, "
            sSQL = sSQL & "STaxTranKey          int         NULL, "
            sSQL = sSQL & "STaxAmt              dec(18,6)   NULL, "
            sSQL = sSQL & "TradeDiscAmt         dec(15,3)   NULL, "
            sSQL = sSQL & "ShipLineDistKey      int         NULL, "
            sSQL = sSQL & "SOLineDistKey        int         NULL)"
            
            moClass.moAppDB.ExecuteSQL sSQL
            
            bdid = True
            
        End If
        
'       sSQL = " EXEC sparGetLineDist " & Format(lKey) & ""
'balzer 01/31/2000  added additional parameter to exec statement.
        sSQL = " EXEC sparGetLineDist " & Format(lKey) & " , " & Format(miDigAfterDec)
        
        
        moClass.moAppDB.ExecuteSQL sSQL
        
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function
        
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CreateLineJoin", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bLoadInvoiceTypeListBox() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Debug.Print "LoadInvoiceTypeListBox"

'***********************************************************************
' Desc: bLoadInvoiceTypeListBox() loads the Invoice Type
'       list box on the top of the form with allowable values.
' Note: are user defined transaction types allowed
' Parameters: none
' Returns: True if successful, else false.
'***********************************************************************
    Dim sSQL        As String
    Dim rs          As New DASRecordSet
    Dim lTranType   As Long
    Dim sTranDesc   As String

    bLoadInvoiceTypeListBox = False

    sSQL = "SELECT TranType, LocalText FROM tciTranType, tsmLocalString WHERE TranType IN ("
    sSQL = sSQL & Format$(kTranTypeARIN) & ", " & Format$(kTranTypeARFC) & ", "
    sSQL = sSQL & Format$(kTranTypeARCM) & ", " & Format$(kTranTypeARDM) & ", "
    sSQL = sSQL & Format$(kTranTypeARRF) & ") AND TranDescStrNo = StringNo AND LanguageID = "
    sSQL = sSQL & Format$(moClass.moSysSession.Language)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.IsEOF Then
        cboInvcType.Clear
        gComboAddItem cboInvcType, "Finance Chg", 505
        gComboAddItem cboInvcType, "Invoice", 501
        gComboAddItem cboInvcType, "Credit Memo", 502
        gComboAddItem cboInvcType, "Debit Memo", 503
        gComboAddItem cboInvcType, "Refund", 504
    Else
        With rs
            Do While Not .IsEOF
                lTranType = gvCheckNull(.Field("TranType"), SQL_INTEGER)
                sTranDesc = gvCheckNull(.Field("LocalText"), SQL_INTEGER)
                
                gComboAddItem cboInvcType, sTranDesc, lTranType
                
                .MoveNext  'Get next tran Type Group record
            Loop
            
        End With
    End If
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    bLoadInvoiceTypeListBox = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadInvoiceTypeListBox", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bLoadHeaderListBoxes() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************************
' Load Header list boxes
'********************************************************************************
    
    bLoadHeaderListBoxes = False
    
    
  'Load Reason Code Combo box
    gRefreshCIReasonCode moClass.moAppDB, moClass.moSysSession, cboReasonCode, _
                         mbAllowReasonCodeAOF, kComboNotRequired, muCompanyDflts.msCompanyID
    
                                  
    bLoadHeaderListBoxes = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bLoadHeaderListBoxes", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub ClearControls(TextCtrls As Boolean, TagCtrls As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick

    If TextCtrls Then
        txtBillTo.Text = ""
        txtShipTo.Text = ""
        txtContName.Text = ""
        txtCustClass.Text = ""
        txtSONum.Text = ""
        txtInvcDate.Text = ""
        txtInvoiceForm.Text = ""
        txtSalesperson.Text = ""
        txtComment.Text = ""
        txtInvoiceForm.Text = ""
        txtInvcDate.Text = ""
        txtDueDate.Text = ""
        txtDiscDate.Text = ""
        txtItemDesc.Text = ""
        txtCommentDtl.Text = ""
        txtFOB.Text = ""
    End If
    If TagCtrls Then
        txtBillTo.Tag = ""
        txtShipTo.Tag = ""
        txtContName.Tag = ""
        txtCustClass.Tag = ""
        txtSONum.Tag = ""
        txtInvcDate.Tag = ""
        txtInvoiceForm.Tag = ""
        txtSalesperson.Tag = ""
        txtComment.Tag = ""
        txtShipMeth.Tag = ""
        txtInvcDate.Tag = ""
        txtDueDate.Tag = ""
        txtDiscDate.Tag = ""
        txtItemDesc.Tag = ""
        txtCommentDtl.Tag = ""
    End If

    If TextCtrls Then
        txtInvcID.Text = ""
        txtCustID.Text = ""
        txtPaddedCust.Text = ""
        TxtItemID.Text = ""
        txtSalesAcct.Text = ""
    End If
    If TagCtrls Then
        txtInvcID.Tag = ""
        txtCustID.Tag = ""
        txtPaddedCust.Tag = ""
        TxtItemID.Tag = ""
        txtSalesAcct.Tag = ""
    End If

    bOldmbClick = mbDontChkclick
    mbDontChkclick = True
    If TextCtrls Then
        txtCommPlan.Text = ""
        cboReasonCode.ListIndex = kItemNotSelected
        txtPmtTerms.Text = ""
        txtUOM.Text = ""
        txtSTaxClass.Text = ""
        txtCommPlanDtl.Text = ""
        txtCommClassDtl.Text = ""
    End If
    mbDontChkclick = bOldmbClick
       
    If TagCtrls Then
        txtCommPlan.Tag = txtCommPlan.Text
        cboReasonCode.Tag = kItemNotSelected
        txtPmtTerms.Tag = txtPmtTerms.Text
        txtUOM.Tag = txtUOM.Text
        txtSTaxClass.Tag = txtSTaxClass.Text
        txtCommPlanDtl.Tag = txtCommPlanDtl.Text
        txtCommClassDtl.Tag = txtCommClassDtl.Text
    End If

    
    If TextCtrls Then
        curUnitPrice.Amount = 0
        curSalesAmt.Amount = 0
        curTradeDiscAmt.Amount = 0
        curUnitCost.Amount = 0
        curOrigCommAmt.Amount = 0
        curActCommAmt.Amount = 0
        curTotalSalesAmt.Amount = 0
        curFreightAmt.Amount = 0
        curTotTradeDiscAmt.Amount = 0
        curSalesTaxAmt.Amount = 0
        curSubTotal.Amount = 0
        curHandling.Amount = 0
        curRetntAmt.Amount = 0
        curInvcTotal.Amount = 0
        curInvcAmt.Amount = 0
        curBalanceDue.Amount = 0
        curTermsDiscAmt.Amount = 0
        curCostOfSales.Amount = 0
    End If
    If TagCtrls Then
        curUnitPrice.Tag = 0
        curSalesAmt.Tag = 0
        curTradeDiscAmt.Tag = 0
        curUnitCost.Tag = 0
        curOrigCommAmt.Tag = 0
        curActCommAmt.Tag = 0
        curTotalSalesAmt.Tag = 0
        curFreightAmt.Tag = 0
        curTotTradeDiscAmt.Tag = 0
        curSalesTaxAmt.Tag = 0
        curSubTotal.Tag = 0
        curHandling.Tag = 0
        curRetntAmt.Tag = 0
        curInvcTotal.Tag = 0
        curInvcAmt.Tag = 0
        curBalanceDue.Tag = 0
        curTermsDiscAmt.Tag = 0
        curCostOfSales.Tag = 0
    End If

    If TextCtrls Then
        numQty.Value = 0
        numTradeDiscPct.Value = 0
        numTotTradeDiscPct.Value = 0
        numRetntPct.Value = 0
    End If
    If TagCtrls Then
        numQty.Tag = 0
        numTradeDiscPct.Tag = 0
        numTotTradeDiscPct.Tag = 0
        numRetntPct.Tag = 0
    End If

    If TextCtrls Then
        lblCustName = ""
        txtBillToName.Text = ""
        txtShipToName.Text = ""
    End If
    
    'Intellisol start
    moProject.ClearInvoiceCtrls
    'Intellisol end

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DisplayStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    
    If muCustDflts.Hold Then
        sbrMain.Message = "*** Customer On Hold ***"
        sbrMain.MessageVisible = True
    Else
        sbrMain.Message = ""
        sbrMain.MessageVisible = False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DisplayStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub InitializeGlobalVariables()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
'  Clear out customer and detail line structures
'****************************************************************

  'Clear out Customer defaults structure  Structure
    With muCustDflts
        .DfltItemKey = 0                'Default Item surrogate key
        .PrimaryAddrKey = 0             'Primary Address Surrogate key
        .ReqPO = False                  'P/O required
        .Status = 0                     'Customer Status
        .TradeDiscPct = 0               'Trade Discount percent
        .AllowCustRefund = False        'Allow customer refunds
        .AllowInvtSubst = False         'Allow inventory substitutions
        .AllowWriteOff = False          'Allow Write Offs
        .BillingType = 0                'Billing Type
        .Hold = False                   'Credit Hold
        .CurrExchSchdKey = 0            'Currency exchange schedule surrogate Key
        .BillToParent = False           'Bill to national account parent
        .PmtByParent = False            'Payment by national account parent
        .NatlAcctKey = 0                'National account surrogate key
        .NatlAcctLevel = 0              'Customer's level in national acct
        .NatlAcctLevelKey = 0           'National account level key
        .NatlAcctParentKey = 0          'National account parent key
        .NatlAcctOnHold = False         'National account on hold
        .NatlAcctCrLimitUsed = False    'National account credit limit used
    End With
    
  'Clear out Detail lines  Structure
    With muInvcDtl
        .InvcKey = 0                    'Invoice Surrogate key
        .InvoiceLineKey = 0             'Invoice Line Dist Surrogate Key
        .CmntOnly = 0                   'Comment Only
        .CommBase = 1                   'Commission base
        .ItemKey = 0                    'Item Surrogate Key
        .AcctKey = 0                    'G/L Account Surrogate key
        .SeqNo = 0                      'sequence Number
        .UnitPriceOvrd = 0              'Unit Price override
        .SalesAmt = 0                   'Sales Amount
        .TradeDiscAmt = 0               'trade Discount Amount
        .OrigCommAmt = 0                'Original commission amount
        .ActCommAmt = 0                 'Actual commission amount
        .Qty = 0                        'Quantity
        .SubjToTradeDisc = 0            'Item is Subject to trade discounts
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeGlobalVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboInvcType_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick cboInvcType, True
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************************************
' Desc: based on the Invoice Type Selected disable/enable the apply to field
'*********************************************************************************************
    'ZZZZbInvcTypeAllowed = True
    
    moLostFocus.IsValidControl cboInvcType
    
  'Nothing Selected open up apply to Invoice field
    If cboInvcType.ListIndex = kItemNotSelected Then
        txtDiscDate.Enabled = True
        txtDueDate.Enabled = True
    Else
    
      'Invoice can't apply to anything clear out and disable apply to Invoice id
      'Enable the due date and Discount date
        If iGetTranType() = kTranTypeARIN Then
            txtDiscDate.Enabled = True
            txtDueDate.Enabled = True
        
        Else
          
          'Credit memo, Finance Charge disable the due date and Discount date
            If iGetTranType() = kTranTypeARCM Then
                txtDiscDate.Enabled = False
                txtDueDate.Enabled = False
            Else
                If iGetTranType() = kTranTypeARFC Then
                    txtDiscDate.Enabled = False
                    txtDueDate.Enabled = True
                Else
                    txtDiscDate.Enabled = True
                    txtDueDate.Enabled = True
                End If
            End If
            
        End If

    End If

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboInvcType_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboInvcType_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboInvcType, True
    #End If
'+++ End Customizer Code Push +++
    If Me.ActiveControl Is txtInvcID Or _
       Me.ActiveControl Is navMain Then
        'do nothing
    Else
        gbSetFocus Me, cboInvcType
    End If
End Sub
Private Sub cboReasonCode_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick cboReasonCode, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Run Standard Combo Box Validation including Add On the Fly
'       Will Load New Value and surrogate Key if Added
'********************************************************************
On Error GoTo ExpectedErrorRoutine
   
    If Not mbDontChkclick Then  'Don't Run if global Don't check click
     
        If mbValidating Then Exit Sub   'Currently Validating another control
    
        mbValidating = True             'Set Global Validating Flag
        
        Set moValidate.oDmForm = moDmHeader
        
        moValidate.CIReasonCode cboReasonCode, lblReasonCode, "ReasonCodeKey", _
                mbAllowReasonCodeAOF, True, False
    
    End If
    
    mbValidating = False  'Reset Global Validating Flag
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "cboReasonCode_Click"
mbValidating = False
cboReasonCode.ListIndex = cboReasonCode.Tag
' **PRESTO ** WinHookInvcEntry.KeyboardHook = moValidate.iHooktype
gClearSotaErr

Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReasonCode_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cboReasonCode_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'********************************************************************
' Desc: Refresh Combo Box if F3 key Pressed
'********************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick

    bOldmbClick = mbDontChkclick
    mbDontChkclick = True

    If Shift = 0 And KeyCode = vbKeyF3 Then
      
      'Refresh Combo Box
        gRefreshCIReasonCode moClass.moAppDB, moClass.moSysSession, cboReasonCode, _
                             mbAllowReasonCodeAOF, kComboNotRequired, muCompanyDflts.msCompanyID
    
        cboReasonCode.Tag = cboReasonCode.ListIndex
    
    End If
    
    mbDontChkclick = bOldmbClick

'+++ VB/Rig Begin Pop +++
        Exit Sub
ExpectedErrorRoutine:
    mbDontChkclick = bOldmbClick
Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReasonCode_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cmdComms_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdComms, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'********************************************************
' Desc: if user has rights to view commissions then
'       recalculate commissions and show commissions form
'********************************************************
        Dim iHooktype As Integer
    
    If moDmHeader.State = kDmStateNone Then
        Exit Sub
    End If
    
    If moLostFocus.IsValidDirtyCheck() = 0 Then
        Exit Sub
    End If

  'View commissions security event allowed
    If bOverrideSecEvent("VIEWCOMMS") Then
        
        ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
        ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
        ' **PRESTO ** WinHookInvcEntry.HookEnabled = False

      'Show the form
        frmCommissions.Show vbModal

        ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype
           
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdComms_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cmdCreditLimit_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdCreditLimit, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    If moDmHeader.State = kDmStateNone Then
        Exit Sub
    End If
    
    If moLostFocus.IsValidDirtyCheck() = 0 Then
        Exit Sub
    End If
    
    bCheckInvcCredit
    ShowCreditLimitForm '
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCreditLimit_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cmdCurrency_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdCurrency, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim iHooktype As Integer
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    
    If moDmHeader.State = kDmStateNone Then
        Exit Sub
    End If
    
    If moLostFocus.IsValidDirtyCheck() = 0 Then
        Exit Sub
    End If
    
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False
    
    Static bdid         As Boolean
    Dim sCurrID         As String
    Dim dExchRate       As Double
    Dim lAddrKey        As Long
    Dim lCustkey        As Long
    
    If Not bdid Then
      'Currency
        Load frmCurrency
        frmCurrency.CompanyID = muCompanyDflts.msCompanyID
        frmCurrency.Init moClass
        bdid = True
    End If
    
    With frmCurrency
      'Home Currency
        .HomeCurr = muCompanyDflts.msDfltCurrency
     
      'Document Currency
        sCurrID = gvCheckNull(moDmHeader.GetColumnValue("CurrID"), SQL_CHAR)
        .DocCurrency = sCurrID
          
      'Document Date
        .TranDate = gsFormatDateToDB(txtInvcDate.Text)
        
      'Current Exchange Rate
        dExchRate = gvCheckNull(moDmHeader.GetColumnValue("CurrExchRate"), SQL_FLOAT)
        .ExchRate = dExchRate
            
      'Current Exchange Schedule Key
        .ExchSchedKey = gvCheckNull(moDmHeader.GetColumnValue("CurrExchSchdKey"), SQL_INTEGER)
        
      'Address Schedule Key
        lAddrKey = gvCheckNull(moDmHeader.GetColumnValue("BillToCustAddrKey"), SQL_INTEGER)
'        .AddrSchedKey = gvCheckNull(moClass.moAppDB.Lookup("CurrExchSchdKey", _
                    "tarCustAddr", "AddrKey = " & lAddrKey), SQL_INTEGER)
        .AddrSchedKey = lAddrKey
      
      'Customer Schedule Key
        lCustkey = gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)
'        .CustSchedKey = gvCheckNull(moClass.moAppDB.Lookup("CurrExchSchdKey", _
                    "tarCustomer", "CustKey = " & lCustkey), SQL_INTEGER)
        .CustSchedKey = lCustkey
        
    End With
        
    frmCurrency.SetReadOnly
    frmCurrency.Display
    
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdCurrency_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDtl_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdDtl(Index), True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************
'   Description:
'           cmdOK is Enabled only if the detail data contained in the
'           detail edit controls have been altered.
'   Parameters:
'           Index - 0 = move controls to grid(OK)
'                   1 = move grid back to controls(Undo)
'************************************************************************
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick
    
    If Not gbSetFocus(Me, cmdDtl(Index)) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    If Index = kLEOK Then
        
        bOldmbClick = mbDontChkclick
        mbDontChkclick = True
        moLEGridInvcDetl.GridEditOk
        mbDontChkclick = bOldmbClick
        
'        gbSetFocus Me, txtItemID
    
    Else
        
        bOldmbClick = mbDontChkclick
        mbDontChkclick = True
        moLEGridInvcDetl.GridEditUndo
        mbDontChkclick = bOldmbClick
    
    End If


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdDtl_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdFreight_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdFreight, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'**********************************************************************************
'*  Run Freight Allocations
'**********************************************************************************
    
  On Error GoTo ExpectedErrorRoutine
    
    Dim oFrt            As Object       'Freight Object
    Dim lActiveRow      As Long         'Current Active Row
    Dim lChildRow       As Long         'Child Row
    Dim lParentRow      As Long         'Parent Row
    
    Dim lLineDistKey    As Long         'Line Distribution Surrogate Key
    Dim sShipMethID     As String       'Ship Method ID
    Dim dQuantity       As Double       'Quantity
    Dim dExtAmt         As Double       'Extended Amount
    Dim dFrtAmt         As Double       'Freight Amount
    Dim bChangesMade    As Boolean      'Chenages Were Made
    Dim bRecalcTaxes    As Boolean      'Recalculate Taxes
    Dim dLineFrtAmt     As Double       'Freight Amount for Line
    Dim sItemID         As String       'Item ID
    Dim iTrackTax       As Integer      'Track Sales Tax Flag
    Dim iFreightAllocMeth As Integer
    Dim lTranUOMKey     As Long
    Dim sItemDesc       As String
    Dim bReturn         As Boolean
    
    'If no customer selected yet, don't start form.
    If txtCustID = "" Then
        Exit Sub
    End If
    
  'Get active row to reset to
    lActiveRow = moLEGridInvcDetl.ActiveRow
    
  'No active row reset to 1
    If moLEGridInvcDetl.ActiveRow = 0 Then
        lActiveRow = 1
    End If


'*************************************************************
'*  Initialize the freight allocations object/form
'*************************************************************
    
  'Create the freight allocation object
    Set oFrt = CreateObject("cizdcdl1.clsFrtAlloc")

  'An error occurred Creating the freight allocation object
    If oFrt Is Nothing Then
        MsgBox "An error occurred bringing up the freight form", , "Sage 500 ERP"
        Exit Sub
    End If
    
  'Initialize the freight allocation class
    oFrt.Init moClass.moSysSession, moClass.moAppDB, moClass.moFramework
    
    
  'Set up the freight allocation class form
    iTrackTax = muARDflts.TrackSTaxOnSales
    oFrt.bSetupForm kModuleAR, numQty.DecimalPlaces, curFreightAmt.DecimalPlaces, 0, iTrackTax, True
    
'*************************************************************
'*  Load the freight allocations grid
'*************************************************************
  'Loop through the parent to read the grid
    For lParentRow = 1 To grdInvcDetl.DataRowCnt
        
      'Now set the specified row active
        mbDontChkclick = True
        moLEGridInvcDetl.Grid_Click 1, lParentRow
        mbDontChkclick = False
        
        lTranUOMKey = glGetValidLong(gsGridReadCell(grdInvcDetl, lParentRow, kcolIEUnitMeasKey))
        
      'get item ID, it will be save for all line dists
      sItemID = gsGridReadCell(grdInvcDetl, lParentRow, kColIEItemID)
      sItemDesc = gsGridReadCell(grdInvcDetl, lParentRow, kcolIEDescription)

      'Loop through the child to load the grid
        For lChildRow = 1 To grdInvcLineDist.DataRowCnt
                lLineDistKey = Val(gsGridReadCellText(grdInvcLineDist, lChildRow, kColChildInvoiceLineDistKey))
'/************** dm fix ***************************
'                sShipMethID = gsGridReadCellText(grdInvcLineDist, lChildRow, kColChildShipMethID)
'                dExtAmt = gsGridReadCellText(grdInvcLineDist, lChildRow, kColChildExtAmt)
                sShipMethID = txtShipMethDetl.Text
                dExtAmt = curSalesAmt.Amount
'/*****************************************
                dQuantity = dSetValue(Val(gsGridReadCell(grdInvcLineDist, lChildRow, kColChildQtyShipped)))
                dFrtAmt = dSetValue(Val(gsGridReadCell(grdInvcLineDist, lChildRow, kColChildFreightAmt)))
                
                    oFrt.bLoadGrid 0, muInvcDtl.InvoiceLineKey, lLineDistKey, _
                            sItemID, sItemDesc, 0, dQuantity, lTranUOMKey, dExtAmt, sShipMethID, dFrtAmt
    
        Next lChildRow
    
    Next lParentRow
    
'*************************************************************
'*  Show Freight Allocation form
'*************************************************************
    dFrtAmt = curFreightAmt.Amount
    bReturn = oFrt.ShowFrt(bChangesMade, dFrtAmt, False, iFreightAllocMeth)
    
  'Unload the freight object
    oFrt.UnloadMe
    
 'Clean up
    Set oFrt = Nothing

  'Now set old active row as the specified row active
    mbDontChkclick = True
    moLEGridInvcDetl.Grid_Click 1, lActiveRow
    mbDontChkclick = False
Exit Sub

ExpectedErrorRoutine:
mbDontChkclick = False

'Clean up
If Not oFrt Is Nothing Then
   
  'Unload the freight object
    oFrt.UnloadMe
    Set oFrt = Nothing

End If
MyErrMsg moClass, Err.Description, Err, sMyName, "cmdFreight_Click"
gClearSotaErr

Exit Sub


End Sub

Private Sub cmdSalesTax_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSalesTax, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim iHooktype As Integer
    Dim dTotalTaxAmt    As Double
    Dim dLineSTaxAmt    As Double
    Dim bChangesMade    As Boolean
    
    If moDmHeader.State = kDmStateNone Then
        Exit Sub
    End If
    
    If moLostFocus.IsValidDirtyCheck() = 0 Then
        Exit Sub
    End If
    
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False

    If muARDflts.TrackSTaxOnSales Then
        moSTax.ShowTaxes True, kModeReadOnly, dTotalTaxAmt, dLineSTaxAmt, bChangesMade
    End If

    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdSalesTax_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSTaxDetl_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSTaxDetl, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

    Dim iHooktype       As Integer
    Dim dTotalTaxAmt    As Double
    Dim dLineSTaxAmt    As Double
    Dim bChangesMade    As Boolean
    Dim lSTaxClassKey   As Long
    
    If moDmHeader.State = kDmStateNone Then
'+++ VB/Rig Begin Pop +++
        Exit Sub
    End If
    
    ' No taxes on comment only lines
    If muInvcDtl.CmntOnly = 1 Then
'+++ VB/Rig Begin Pop +++
        Exit Sub
    End If
    
    If Not moLostFocus.IsValidDirtyCheck() Then
'+++ VB/Rig Begin Pop +++
        Exit Sub
    End If
    
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False
    
    moSTax.ShowTaxes False, kModeReadOnly, dTotalTaxAmt, dLineSTaxAmt, bChangesMade

    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdSalesTax_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub

Private Sub curFreightAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curFreightAmt, True
    #End If
'+++ End Customizer Code Push +++
    
    curSubTotal.Amount = curTotalSalesAmt.Amount + curSalesTaxAmt.Amount + _
                        curFreightAmt.Amount + curTotTradeDiscAmt.Amount
                        

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curFreightAmt_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub curSalesTaxAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curSalesTaxAmt, True
    #End If
'+++ End Customizer Code Push +++
    
    curSubTotal.Amount = curTotalSalesAmt.Amount + curSalesTaxAmt.Amount + _
                        curFreightAmt.Amount + curTotTradeDiscAmt.Amount
                        

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curSalesTaxAmt_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub curSubTotal_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curSubTotal, True
    #End If
'+++ End Customizer Code Push +++
    curInvcTotal.Amount = curSubTotal.Amount
    curInvcAmt.Amount = curSubTotal.Amount
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curSubTotal_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub curTotalSalesAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curTotalSalesAmt, True
    #End If
'+++ End Customizer Code Push +++
'***********************************************************************************
' Set the Value into the structure (multiplied by -1 if Credit memo)
' then recalculate the trade Discount percent out
'***********************************************************************************
    If curTotalSalesAmt.Amount = 0 Then
    
      'No Sales amount set percent = 0
        numTotTradeDiscPct.Value = 0
    
    Else
                  
      'Calculate Trade Discount Percent
        numTotTradeDiscPct.Value = _
            ((-1 * curTotTradeDiscAmt.Amount) / curTotalSalesAmt.Amount) * 100
        
    End If

    curSubTotal.Amount = curTotalSalesAmt.Amount + curSalesTaxAmt.Amount + _
                        curFreightAmt.Amount + curTotTradeDiscAmt.Amount
                        
                        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curTotalSalesAmt_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub curTotTradeDiscAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curTotTradeDiscAmt, True
    #End If
'+++ End Customizer Code Push +++
'***********************************************************************************
' Recalculate the trade Discount percent out
'***********************************************************************************
    If curTotalSalesAmt.Amount = 0 Then
    
      'No Sales amount set percent = 0
        numTotTradeDiscPct.Value = 0
    
    Else
                  
      'Calculate Trade Discount Percent
        numTotTradeDiscPct.Value = _
            ((-1 * curTotTradeDiscAmt.Amount) / curTotalSalesAmt.Amount) * 100
        
    End If

    curSubTotal.Amount = curTotalSalesAmt.Amount + curSalesTaxAmt.Amount + _
                        curFreightAmt.Amount + curTotTradeDiscAmt.Amount
                        


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curTotTradeDiscAmt_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub curTradeDiscAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curTradeDiscAmt, True
    #End If
'+++ End Customizer Code Push +++
        
  'Recalculate the trade Discount pct
    If curSalesAmt.Amount = 0 Then
        numTradeDiscPct.Value = 0
    Else
        numTradeDiscPct.Value = _
            (curTradeDiscAmt.Amount / curSalesAmt.Amount) * 100
    End If
            

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curTradeDiscAmt_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitCost_Change()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curUnitCost, True
    #End If
'+++ End Customizer Code Push +++
    muInvcDtl.UnitCost = curUnitCost.Amount
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   'ZZDebug.Print "forminit"
    mbCancelShutDown = False
    mbLoadSuccess = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_Load sets up the form by opening the session object and
'         database, initializing static list box (combo box) values and
'         either putting the form in ADD MODE if called as an AOF
'         (Add-On-the-Fly) form or StartUp mode when in used as a
'         maintenance form.
'********************************************************************
    Dim iReturnValue As Integer
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick
    Dim sDateMask As String

  Set sbrMain.Framework = moClass.moFramework

    
    mbFormLoading = True
    
    bOldmbClick = mbDontChkclick
    mbDontChkclick = True
    
    miFilter = RSID_UNFILTERED
  
    'Load sub forms
    'frmCommissions.Hide 'Commissions by salesperson
      
    mbLoadSuccess = False
    
   
    txtSONum.BackColor = vbButtonFace

    With moLostFocus
        Set .Form = frmInvcEntry
        Set .TabControl = tabInvcEntry
        .SkipForAll tbrMain, sbrMain
        .BindKey txtInvcID, , True
        .SkipControls navMain, cboInvcType
        .Bind cboInvcType, , True
        .SkipControls navMain, txtInvcID
        .Bind txtContName, ktabIEHeader, False
        .Bind txtDueDate, ktabIEHeader, False
        .Bind txtDiscDate, ktabIEHeader, False
        .Bind txtInvoiceForm, ktabIEHeader, False
    End With
  
    
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmHeader, moDmDetail, _
                                       moDmComms, moDmBTAddress, moDmSTAddress)
    
  'Set up the Sage MAS 500 toolbar
    If mlRunMode = kContextAOF Then
        tbrMain.LocaleID = moClass.moSysSession.Language
        navMain.Visible = False               'Bind toolbar browse to browse MS toolbar
        tbrMain.Build sotaTB_AOF                   'Create a transaction toolbar
        tbrMain.AddButton kTbPrint, 4
        tbrMain.AddSeparator 4
        tbrMain.Buttons(kTbPrint).Enabled = False
    Else
        tbrMain.LocaleID = moClass.moSysSession.Language
        tbrMain.Build sotaTB_MULTI_ROW                   'Create a transaction toolbar
        tbrMain.RemoveButton kTbRenameId
        tbrMain.RemoveButton kTbDelete
    End If
    
'    'Intellisol start
'    PAShowHelpIcon Me, tbrMain
'    'Intellisol end
    
    SetHourglass True
    
  'set masks for date controls
    sDateMask = gsGetLocalDateMask
' DATE Presto Comment:     txtInvcDate.Mask = sDateMask
' DATE Presto Comment:     txtDueDate.Mask = sDateMask
' DATE Presto Comment:     txtDiscDate.Mask = sDateMask
    txtPostDate.Mask = sDateMask
    
     bGetDefaults
     
     miRefCodeUsage = iGetGLOptions

    
  'Setup validation class
    With moValidate
        Set .oFramework = moClass.moFramework   'Sage MAS 500 framework
        Set .oDB = moClass.moAppDB              'Database
        Set .oForm = Me                         'Form (for message Box)
        Set .oSotaObjects = moSotaObjects       'Sage MAS 500 Objects Collection (AOF)
        Set .oDmForm = moDmHeader               'data Manager Object
        Set .oTabCtl = tabInvcEntry             'tab control
        ' **PRESTO ** Set .Hook = WinHookInvcEntry
        ' **PRESTO ** .iHooktype = WinHookInvcEntry.KeyboardHook
        .bSetFocusBack = True                   'SetFocus Back on error flag
        .bShowMessage = True                    'Show messages
        .bResetValue = True                     'Change Value back Flag
        .sCompanyID = muCompanyDflts.msCompanyID 'CompanyID
    End With
    
    BindForm
    
    MapControls
    
    'miSecurityLevel = giGetSecurityLevel(moClass.moFramework, ktskARInvoicesDTE)

    ' *** RMM 5/2/01, Context menu project
    mbIMActivated = moClass.moSysSession.IsModuleActivated(kModuleIM)
 
    'Intellisol start
'    FormatGrid
    'Intellisol end
    
    BindLE
    Debug.Print "AFTER BIND le"
    BindContextMenu
    
    ' Configure memo menus for toolbar button
    ConfigureMemos
    tbrMain.ChangeButtonStyle kTbMemo, tbrDropdown, m_MenuInfo
    
    'Intellisol start
    Set moProject = New clsProject
    moProject.Init JOB_INVOICE, moClass, Me, moSotaObjects, moDmDetail, grdInvcDetl, _
                   moLEGridInvcDetl, grdJobLine, lkuProject, lkuPhase, lkuTask, , _
                    , , , , , kiProjectTab

    moProject.InitInvoice moDmHeader, lkuMainProject, curEstSale, _
                          curActSale, lkuInvDesc
    
    
    moProject.CreatePATmpTable   'scopus 36668 perfomance enhancement
    
    'PA Integration: added by dnk 11/12/01
    If Not Me.lkuMainProject.Visible Then
        Me.lblMainProject.Visible = False
        Me.lblCustID.Top = Me.lblMainProject.Top
        Me.txtCustID.Top = Me.lkuMainProject.Top
        Me.lblCustName.Left = Me.txtCustID.Left
        Me.lblCustName.Width = 4125
    End If
    
    FormatGrid
    'Intellisol end
    
    If bLoadInvoiceTypeListBox() = False Then
    End If
    
    If bLoadHeaderListBoxes() = False Then
    End If
    

    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    
    miMinFormHeight = Me.Height
    miMinFormWidth = Me.Width
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    
    InitializeGlobalVariables
    tabInvcEntry.Tab = ktabIEHeader
    
    frmInvcEntry.Width = 9600
    frmInvcEntry.Height = 7200
    
    cboInvcType.ListIndex = 0
    
    mbLoadSuccess = True
    
    'StatusBar.Panels(2).Text = "*** Invoice ON Hold ***"
  'Debug.Print "clear controls"

    ClearControls True, True
 
'    vsbDtlQty.TabStop = False

    pnlDetailTab.Enabled = False
    pnlTotalsTab.Enabled = False

    'AvaTax Integration - FormLoad
    If muARDflts.TrackSTaxOnSales Then
        CheckAvaTax
    Else
        mbAvaTaxEnabled = False
    End If

    ' Add Avatax button to tool bar if AvaTax is enabled, but
    ' button will only be enabled when AvaTaxOnDemandEnabled is True
    If mbAvaTaxEnabled Then
        tbrMain.AddSeparator
        tbrMain.AddButton kTbAvalara
        tbrMain.AddSeparator
        tbrMain.ButtonEnabled(kTbAvalara) = False
    End If
    ' AvaTax end

    SetHourglass False
    mbDontChkclick = bOldmbClick
    mbFormLoading = False
    If moClass.bHourGlass Then
        moClass.bHourGlass = False
        SetHourglass False
    End If
    
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Paint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If moClass.bHourGlass Then
        moClass.bHourGlass = False
        SetHourglass False
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Allow user to resize form except for Width of the form.
'   Parameters: None.
'************************************************************************
    If Me.WindowState = vbNormal Or Me.WindowState = vbMaximized Then
'        If Me.Height < miMinFormHeight Then
'            Me.Height = miMinFormHeight
'        End If
'
'        If Me.Width < miMinFormWidth Then
'            Me.Width = miMinFormWidth
'        End If
'
'        pnlHeaderTab.Height = pnlHeaderTab.Height + (Me.Height - miOldFormHeight)
'        pnlHeaderTab.Width = pnlHeaderTab.Width + (Me.Width - miOldFormWidth)
'
'        pnlDetailTab.Height = pnlDetailTab.Height + (Me.Height - miOldFormHeight)
'        pnlDetailTab.Width = pnlDetailTab.Width + (Me.Width - miOldFormWidth)
'
'        grdInvcDetl.Height = pnlDetailTab.Height - grdInvcDetl.Top
'        grdInvcDetl.Width = pnlDetailTab.Width - 90
'
'        pnlTotalsTab.Height = pnlTotalsTab.Height + (Me.Height - miOldFormHeight)
'        pnlTotalsTab.Width = pnlTotalsTab.Width + (Me.Width - miOldFormWidth)
'
'        tabInvcEntry.Height = tabInvcEntry.Height + (Me.Height - miOldFormHeight)
'        tabInvcEntry.Width = tabInvcEntry.Width + (Me.Width - miOldFormWidth)
'
'        gAdjustGrid moLEGridInvcDetl, grdInvcDetl.Width, max(grdInvcDetl.Height, glGetMinGridHeight(grdInvcDetl))
        
      'resize Height
       gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
                    tabInvcEntry, pnlHeaderTab, pnlDetailTab, _
                    pnlTotalsTab, grdInvcDetl
           
      'resize Width
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
                    tabInvcEntry, pnlHeaderTab, pnlDetailTab, _
                    pnlTotalsTab, grdInvcDetl
                    
        moLEGridInvcDetl.ShowFocus
       
        miOldFormHeight = Me.Height
        miOldFormWidth = Me.Width
        
'        aniStatus.Top = Me.Height - iTopDiff
'        tbarBrowse.Top = Me.Height - iTopDiff
'        aniStatus.Left = Me.Width - iRightDiff

    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bDetailsEmpty() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Desc:   Determine if any data has been entered by the user into
'           the required detail controls
'***********************************************************************
'ZZDebug.Print "detailsempty"
    bDetailsEmpty = False
    
    If TxtItemID.Text = "" Then
        bDetailsEmpty = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDetailsEmpty", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'   Parameters:
'           KeyCode, Shift - see VB reference Manual.
'   Original: 07/29/95 HHO
'   Modified:
'   Copyright (c) 1995-2005 Sage Software, Inc.
'
'   NOTE: THIS ROUTINE SHOULD CALL A GLOBAL SUBROUTINE TO HANDLE ALL THE
'         AVAILABLE HOTKEYS.
'********************************************************************
    Dim iReturnValue As Integer

'ZZDebug.Print "formkeydown"
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
        Case vbKeyN
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'********************************************************************
'ZZDebug.Print "formkeypress"
     Select Case KeyAscii
        Case vbKeyReturn
            If (Me.ActiveControl.TabIndex = cmdDtl(kLEOK).TabIndex - 1) Then 'And _
                '(iDetailEditMode <> kDtlEditModeNone) Then
               gProcessSendKeys "{TAB}"         ' Fire last control's LostFocus
               DoEvents
               'cmdDtl_Click kOK        ' Accept the line
               KeyAscii = 0             ' Swallow the keystroke
            
            ElseIf mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If
        
        Case Else
            'Other KeyAscii routines.
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
'***********************************************************************
'Desc: If the data has changed then Prompt to save changes.
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    Dim iReturnValue        As Integer
    Dim iConfirmUnload      As Integer
    Dim i                   As Integer
    Dim bOldmbClick         As Boolean      'Old mbdontcheck clcick


    SetHourglass True

    mbCancelShutDown = False
    On Error GoTo ExpectedErrorRoutine
    
    If moDmHeader.State <> kDmStateNone Then
        If moLostFocus.IsValidDirtyCheck = 0 Then GoTo CancelShutDown
    End If
    
    If cmdDtl(kLEOK).Enabled Then
        cmdDtl_Click (kLEOK)
    End If

    If moClass.mlError = 0 Then
        'If the Finish button is not Enabled, then the form's record state
        'is none.  This makes the last control validation and the call to the
        'data Manager's ConfirmUnload method unnecessary.
        If moDmHeader.State <> kDmStateNone Then
            
            On Error GoTo ContinueWithUnload
            SetHourglass False
            bOldmbClick = mbDontChkclick
            mbDontChkclick = True
            iConfirmUnload = moDmHeader.ConfirmUnload(False)
            mbDontChkclick = bOldmbClick
            SetHourglass True
            On Error GoTo ExpectedErrorRoutine
            
            Select Case iConfirmUnload
                Case kDmSuccess
                    'Do nothing
                
                Case kDmFailure
                    GoTo CancelShutDown
                
                Case kDmError
                    GoTo CancelShutDown
                    
                Case Else
                    'Error processing
                    'Give an error message to the user
                    'Unexpected Confirm Unload Return Value: {0}
                    SetHourglass False

                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, CVar(iConfirmUnload)
                    SetHourglass True

            End Select
        End If

ContinueWithUnload:
        'Check all other forms that may have been loaded from this main form.
        'If there are any Visible forms, then this means the form is Active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then
            'Yes, there WAS an Active child form
            GoTo CancelShutDown
        End If
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
            'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form and cancel the unload.
            'If the context is 'data entry', hide the form and cancel the unload.
            'If the context is normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kInvcsRunModeAOF, kInvcsRunModeDTE
                        bOldmbClick = mbDontChkclick
                        mbDontChkclick = True
                        ClearControls True, True

                        If Not moLEGridInvcDetl Is Nothing Then
                            moLEGridInvcDetl.InitDataReset
                        End If
                        
                        txtInvcID.Protected = False
                        gbSetFocus Me, txtInvcID
                        'txtInvcID.Refresh
                        cboInvcType.Enabled = True
                        cboInvcType.BackColor = vbWindowBackground
                        If tabInvcEntry.Tab <> ktabIEHeader Then
                           tabInvcEntry.Tab = ktabIEHeader
                        End If
                        mbDontChkclick = bOldmbClick
                        moDmHeader.Clear True
                        Me.Hide
                        GoTo CancelShutDown
                    
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    End If
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        
        Case Else
            'kFrameworkShutDown
            'Do nothing
    End Select

    SetHourglass False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    SetHourglass False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
'***********************************************************************
'   Desc: Cleanup before the Framework unloads the form.
'***********************************************************************
'ZZDebug.Print "formunload"

    'Intellisol start
    lkuMainProject.Terminate
    lkuProject.Terminate
    lkuPhase.Terminate
    lkuTask.Terminate
    
    If Not moProject Is Nothing Then
        moProject.UnloadSelf
        Set moProject = Nothing
    End If
    'Intellisol end
    
    Set moSotaObjects = Nothing
    Set moMapSrch = Nothing
    
    If Not moDmHeader Is Nothing Then
        moDmHeader.UnloadSelf
        Set moDmHeader = Nothing
    End If

    'RKL DEJ 2016-07-07 Start
    If Not moDmHeaderExt Is Nothing Then
        moDmHeaderExt.UnloadSelf
        Set moDmHeaderExt = Nothing
    End If
    'RKL DEJ 2016-07-07 Stop
    
    If Not moDmDetail Is Nothing Then
        moDmDetail.UnloadSelf
        Set moDmDetail = Nothing
    End If
    
    If Not moDmLineDist Is Nothing Then
        moDmLineDist.UnloadSelf
        Set moDmLineDist = Nothing
    End If

    If Not moDmBTAddress Is Nothing Then
        moDmBTAddress.UnloadSelf
        Set moDmBTAddress = Nothing
    End If
    
    If Not moDmSTAddress Is Nothing Then
        moDmSTAddress.UnloadSelf
        Set moDmSTAddress = Nothing
    End If
    
    If Not moSTax Is Nothing Then
        moSTax.UnloadMe
        Set moSTax = Nothing
    End If
    
    If Not moLEGridInvcDetl Is Nothing Then
        moLEGridInvcDetl.UnloadSelf
        Set moLEGridInvcDetl = Nothing
    End If
    
    If Not moDmComms Is Nothing Then
        moDmComms.UnloadSelf
        Set moDmComms = Nothing
    End If
    
    If Not moAddr Is Nothing Then
        moAddr.UnloadMe
        Set moAddr = Nothing
    End If
    
    If Not moInvcCred Is Nothing Then
        moInvcCred.UnloadMe
        Set moInvcCred = Nothing
    End If
    
    Unload frmCommissions
    Set frmCommissions = Nothing
    
    If Not moLostFocus Is Nothing Then
        moLostFocus.UnloadSelf
        Set moLostFocus = Nothing
    End If
    
    Set moValidate = Nothing
    Set moLastControl = Nothing
    Set moContextMenu = Nothing
    Set moClass = Nothing
    Set moSTax = Nothing
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdInvcDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moLEGridInvcDetl.Grid_GotFocus

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub moHook_WndMessage(ByVal lWndHandle As Long, lMsg As Long, ByVal wParam As Long, ByVal lParam As Long, ByVal lXp As Long, ByVal lYp As Long)
End Sub



Private Sub navContact_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Static bdid As Boolean
Dim sWhere As String
    
    If Len(Trim(txtCustID.Text)) = 0 Then Exit Sub
    
    
    If moDmHeader.State = kDmStateAdd Or moDmHeader.State = kDmStateEdit Then
    
        
        If muARDflts.UseNatlAccts And muCustDflts.BillToParent And muCustDflts.NatlAcctParentKey <> 0 Then
            
            sWhere = "CntctOwnerKey = " & _
                muCustDflts.NatlAcctParentKey & _
                " AND EntityType = " & kEntTypeARCustomer
        Else
        
            sWhere = "CntctOwnerKey = " & _
                glGetValidLong(moDmHeader.GetColumnValue("CustKey")) & _
                " AND EntityType = " & kEntTypeARCustomer
                
        End If
        
        If Not bdid Then
            bdid = True
            gbLookupInit navContact, moClass, moClass.moAppDB, "Contact", ""
navContact.ColumnMasks = ""
        End If
      
      'update restrict Clause with current Customer
        navContact.RestrictClause = sWhere
        
        gcLookupClick Me, navContact, txtContName, "Name"

    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navContact_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub





Private Sub navInvoiceForm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Static bdid As Boolean
Dim sWhere  As String
    
    If Len(Trim(txtCustID.Text)) = 0 Then Exit Sub
    
    If moDmHeader.State = kDmStateAdd Or moDmHeader.State = kDmStateEdit Then
        
        If Not bdid Then
            
            bdid = True

          'Setup where Clause (Comapny & Form Type = 2 (Invoice))
            sWhere = "CompanyID = " & gsQuoted(muCompanyDflts.msCompanyID)
            sWhere = sWhere & " AND BusinessFormType = 2"
          
          'Setup the Navigator
            gbLookupInit navInvoiceForm, moClass, moClass.moAppDB, "BusinessForm", sWhere
navInvoiceForm.ColumnMasks = ""
        End If
    
        gcLookupClick Me, navInvoiceForm, txtInvoiceForm, "BusinessFormID"
    
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navInvoiceForm_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub navMain_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iConfirmUnload  As Integer
    Dim sWhere          As String
    Static bdid         As Boolean
    Dim bOldmbClick     As Boolean      'Old mbdontcheck clcick
    Dim vParseRet       As Variant
    Dim sOldInvoiceID   As String
    Dim lTranKey        As Long
    Dim lTranType       As Long
    
    bSetupNavMain
    
    If moDmHeader.State <> kDmStateNone Then
        If moLostFocus.IsValidDirtyCheck = 0 Then Exit Sub
    End If


    If moDmHeader.State = kDmStateNone Then
        iConfirmUnload = kDmSuccess
    Else
        bOldmbClick = mbDontChkclick
        mbDontChkclick = True
        iConfirmUnload = moDmHeader.ConfirmUnload(True)
        mbDontChkclick = bOldmbClick
    End If
        
    Select Case iConfirmUnload
        
        Case kDmSuccess
            
            sOldInvoiceID = Trim(txtPaddedInvc.Text)
            
            gcLookupClick Me, navMain, txtPaddedInvc, "TranNo"
            
            If Not navMain.ReturnColumnValues.Count = 0 Then
                
                If sOldInvoiceID <> Trim(navMain.ReturnColumnValues("TranNo")) Then
                  
                    txtPaddedInvc.Text = Trim(navMain.ReturnColumnValues("TranNo"))
                    
                  'Load Control
                    LoadMaskedControl kmaxInvoiceIDChars, txtInvcID, txtPaddedInvc
                  
                  'Get Tran Type for search
                    lTranKey = gvCheckNull(Trim(navMain.ReturnColumnValues("InvcKey")), SQL_INTEGER)
                    If lTranKey <> 0 Then
                        lTranType = gvCheckNull(moClass.moAppDB.Lookup("TranType", _
                                "tarInvoice", "InvcKey = " & Format(lTranKey)), SQL_INTEGER)
                        If lTranType <> 0 Then
                            cboInvcType.ListIndex = giListIndexFromItemData(cboInvcType, lTranType)
                        End If
                     End If
                    
                  'New Invoice
                    IsValidInvoice
                
                End If
            
            End If
                    
        Case kDmFailure
            'GoTo exitroutine
        Case kDmError
            'GoTo exitroutine
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, CVar(iConfirmUnload)
            'GoTo exitroutine
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navMain_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub sysInvoiceEntry_SysColorsChanged()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moLEGridInvcDetl.SysColorsChanged
    
    gGridSetColors frmCommissions.grdComms
    frmCommissions.grdComms.LockBackColor = vbWindowBackground

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sysInvoiceEntry_SysColorsChanged", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub tabInvcDetail_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************************
' Enable controls for the tab you are going to - disable the remaining tabs
'**********************************************************************************
  
  'Disable all controls on all detail tabs
    txtCommentDtl.Enabled = False       'Comment
    
  'Pick tab and enable appropriate controls
    Select Case tabInvcDetail.Tab
    
        Case Is = ktabIEComment
            If grdInvcDetl.MaxRows > 0 Then
                txtCommentDtl.Enabled = True            'Comment
                txtCommentDtl.BackColor = vbWindowBackground
            End If
    
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabInvcDetail_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub sbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function PadWithZero(iLen As Integer, ctlMask As Control) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc:  Load a masked control with a zero padded Number if
'        a Number is passed in
' Parms: iLen - Maximum Number of characters to use if No mask Set
'        ctlMask - Masked edit control
' Returns: Value sent in padded with zeros if a Number is passed in
'************************************************************************
    Dim sSetText        As String
    Dim bIsANumber      As Boolean
    Dim TheNumber       As Long
    
    On Error GoTo ExpectedErrorRoutine
    
    bIsANumber = True
    sSetText = Trim$(ctlMask.Text)
    
    If Len(sSetText) > 0 Then
        TheNumber = CLng(sSetText)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
        If TheNumber <= 0 Then 'invalid - must be a Number > 0
            bIsANumber = False
        End If
    End If
    
    If ctlMask Is txtCustID Then
        PadWithZero = ctlMask.Text
    Else
        If bIsANumber Then
            PadWithZero = String(iLen - Len(sSetText), "0") & sSetText
        Else
            PadWithZero = ctlMask.Text
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
ExpectedErrorRoutine:
    bIsANumber = False
    gClearSotaErr
    Resume Next

Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PadWithZero", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function PadWithSpace(iLen As Integer, sString As String) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Desc:  Pad a string with spaces
' Parms: iLen - Maximum Number of characters to pad string
'
' Returns: Value sent in padded with spaces on the left
'************************************************************************
    
    PadWithSpace = sString & Space(iLen - Len(sString))
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PadWithSpace", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function IsValidInvoice() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************************
' Desc: Run Key Change event
'*************************************************************************************
    
    Dim sInvoiceId      As String   'Invoice ID
    Dim sInvoiceIdTag   As String   'Invoice ID Tag Value
    Dim iResult         As Integer  'Result of validating the Invoice
    Dim iKeyChangeCode  As Integer  'result of key change event
    Dim bOldmbClick     As Boolean      'Old mbdontcheck clcick
    Dim sBatchId        As String
    Dim bFound          As Boolean
    
    IsValidInvoice = 1
    
    bFound = True
  
  'Set global loading flag
    bOldmbClick = mbDontChkclick
    mbDontChkclick = True
      
  'get Invoice id and put in a variable
    sInvoiceId = Trim$(txtInvcID.Text)
      
    moDmHeader.SetColumnValue "CompanyID", muCompanyDflts.msCompanyID

    If cboInvcType.ListIndex = kItemNotSelected Then
        cboInvcType.ListIndex = giListIndexFromItemData(cboInvcType, kTranTypeARIN)
    End If
    
  'Load the bound control (padded with spaces on the right)
    txtPaddedInvc.Text = PadWithSpace(kmaxInvoiceIDChars, sInvoiceId)
    txtPaddedInvc.Tag = txtPaddedInvc.Text
    LoadMaskedControl kmaxInvoiceIDChars, txtInvcID, txtPaddedInvc
    
  
    If iValidateInvoiceID() = 0 Then        ' could not find invoice, now try padded with zeroes
  
        'Load the bound control (padded with zeroes)
        'then reload the masked control (padded with zeroes to the length of the mask)
        txtInvcID = sInvoiceId
        txtPaddedInvc.Text = PadWithZero(kmaxInvoiceIDChars, txtInvcID)
        txtPaddedInvc.Tag = txtPaddedInvc.Text
        LoadMaskedControl kmaxInvoiceIDChars, txtInvcID, txtPaddedInvc
                
        If iValidateInvoiceID() = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotAddNewVoucher, _
                    cboInvcType.Text, cboInvcType.Text
            txtInvcID.Text = ""
            txtPaddedInvc.Text = ""
            txtPaddedInvc.Tag = ""
            txtInvcID.Protected = False                 'disable Invoice id
            gbSetFocus Me, txtInvcID
            moDmHeader.Clear True
            bFound = False
        End If
    End If
    
    If bFound Then
              
        If cboInvcType.ListIndex <> kItemNotSelected Then
            moDmHeader.SetColumnValue "TranType", cboInvcType.ItemData(cboInvcType.ListIndex)
        End If
        
     'Run the key change
        mbDontRunGridClick = True
        iKeyChangeCode = moDmHeader.KeyChange() 'get the record - we know it exists
        mbDontRunGridClick = False
        
        Select Case iKeyChangeCode
              
            Case kDmKeyNotFound
                Debug.Print "kDmKeyNotFound"
              'Setup a new record
                moClass.lUIActive = kChildObjectActive      'Set up UI
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotAddNewVoucher, _
                        cboInvcType.Text, cboInvcType.Text
                txtInvcID.Protected = False                 'disable Invoice id
                gbSetFocus Me, txtInvcID
                moDmHeader.Clear True
                txtFOB.Tag = txtFOB.Text
                CreateLineJoin moDmHeader
            Case kDmKeyFound
              'Record found and loaded setup UI
                moClass.lUIActive = kChildObjectActive          'Set up UI
                gbSetFocus Me, txtCustID   'set focus on customer
                txtInvcID.Protected = True                 'disable Invoice id
                txtPaddedCust.Text = PadWithZero(kmaxCustIDChars, txtCustID)
                bGetCustDefaults 1, True
                moLEGridInvcDetl.InitDataLoaded                 'initialize Line entry
'Intellisol start
'                If gvCheckNull(moDmHeader.GetColumnValue("SourceModule"), SQL_SMALLINT) = 19 Then
'                    tabInvcDetail.TabVisible(ktabIEProjAcct) = True
'                Else
'                    tabInvcDetail.TabVisible(ktabIEProjAcct) = False
'                End If
'Intellisol end
                txtFOB.Tag = txtFOB.Text
            Case kDmKeyNotComplete
                'this should not occur -
                'we have the TranNo, CompanyID, BatchID, and Type
                'MsgBox ("keynotcomplete")
                IsValidInvoice = 0
            
            Case kDmError
              'database error occurred trying to get row
                MsgBox ("Serious database error")
                IsValidInvoice = 0
            
            Case Else
              'An error occurred - not programmmed for
                MsgBox "Unexpected KeyChangeCode: " & iKeyChangeCode
                IsValidInvoice = 0
        
        End Select
    
    End If
    
  'reset loading flag
    mbDontChkclick = bOldmbClick

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidInvoice", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub tabInvcEntry_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' Description: Allow only the current tab's control to receive focus.
'              This avoids the tabbing into inactive tab's controls.
' Parameters: PreviousTab - See VB Custom Control reference.
'************************************************************************
   'ZZDebug.Print "tabclick " & PreviousTab

    Static mbTabChange As Boolean
    
    Dim bValidTC As Boolean
    
   ' If Not tabInvcEntry.Visible Then
   '     Exit Sub
   ' End If

    'Recursion sentinel
    If mbTabChange Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
    
    mbTabChange = True
    
    'Do nothing if tab page has not changed
    If tabInvcEntry.Tab = PreviousTab Then
        mbTabChange = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
        
   'Only Set grid detail mode if
   'You haven't loaded an initial record
    
    
    'Determine which tab the user just clicked
    Select Case tabInvcEntry.Tab
        Case Is = ktabIEHeader
          'The user clicked on the 'Header' tab
            pnlHeaderTab.Enabled = True
            pnlDetailTab.Enabled = False
            pnlTotalsTab.Enabled = False
            pnlCustomizerTab.Enabled = False
            
        Case Is = ktabIEDetail
          'The user clicked on the 'Detail' tab
            pnlDetailTab.Enabled = True
            
          'Pull the grid out in front
            gShowOnTop grdInvcDetl.hwnd
'            gbSetFocus Me, grdInvcDetl

          'Disable the header tab
            pnlHeaderTab.Enabled = False
            
          'Disable the totals tab
            pnlTotalsTab.Enabled = False
                
          'Disable the customizer tab
            pnlCustomizerTab.Enabled = False

        Case Is = ktabIETotals
          'The user clicked on the 'Totals' tab
            pnlTotalsTab.Enabled = True
            pnlDetailTab.Enabled = False
            pnlHeaderTab.Enabled = False
            pnlCustomizerTab.Enabled = False
            
          'Only Calculate Sales Taxes if track sales taxes on
            
        Case Is = ktabIECustomizer
          'The user clicked on the 'Totals' tab
            pnlTotalsTab.Enabled = False
            pnlDetailTab.Enabled = False
            pnlHeaderTab.Enabled = False
            pnlCustomizerTab.Enabled = True
        
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedCtrlArryIndex
    End Select
    
    'txtCustID.Refresh
    mbTabChange = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tabInvcEntry_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
    Dim iActionCode  As Integer
    Dim iReturnValue As Integer
    Dim sInvoiceString As String
    Dim kSkip As Integer
    Dim bOldmbClick As Boolean
    Dim sWhere          As String
    Dim sNewKey         As String
    Dim vParseRet       As Variant
    Dim lRet            As Long
    Dim lTranKey        As Long
    Dim lTranType       As Long
    Dim lCustkey        As Long


    kSkip = -99
    
   'ZZDebug.Print "tbarmainclick"
    
    If Not (moLastControl Is Nothing) Then
        gbSetFocus Me, moLastControl
    End If
    
    If cmdDtl(kLEOK).Enabled Then
        cmdDtl_Click (kLEOK)
    End If
    
    SetHourglass True
    iActionCode = kDmFailure
    bOldmbClick = mbDontChkclick
    mbDontChkclick = True
    
    Select Case sKey
        Case kTbClose
            iActionCode = moDmHeader.Action(kDmCancel)
            If iActionCode = kDmSuccess Then
                Me.Hide
            End If
        
        Case kTbFinishExit
            If moLostFocus.IsValidDirtyCheck() Then
                iActionCode = moDmHeader.Action(kDmFinish)
                If iActionCode = kDmSuccess Then
                    Me.Hide
                End If
            End If
        Case kTbCancelExit
            iActionCode = moDmHeader.Action(kDmCancel)
            If iActionCode = kDmSuccess Then
                Me.Hide
            End If
        
        Case kTbFinish
            sbrMain.Message = ""
            sbrMain.MessageVisible = False
            If moLostFocus.IsValidDirtyCheck() Then
                iActionCode = moDmHeader.Action(kDmFinish)
            End If
        Case kTbSave
            If moLostFocus.IsValidDirtyCheck() Then
                iActionCode = moDmHeader.Save(True)
            End If
            mbDontChkclick = bOldmbClick
            If iActionCode = kDmSuccess Then
                CreateLineJoin moDmHeader
            End If
    
            SetHourglass False
            Exit Sub
        Case kTbCancel
            sbrMain.Message = ""
            sbrMain.MessageVisible = False
            iActionCode = moDmHeader.Action(kDmCancel)
        Case kTbPrint
            If moLostFocus.IsValidDirtyCheck() Then
                If moDmHeader.ConfirmUnload(True) = kDmSuccess Then
                    Set oPrintInvcs = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                        kClsPrintInvoiBAT, ktskRePrintInvc, kAOFRunFlags, kContextAOF)
                    oPrintInvcs.PrintInvoices txtBatch.Text, txtInvcID.MaskedText, _
                        cboInvcType.ItemData(cboInvcType.ListIndex)
                End If
            End If
        
        Case kTbMemo
            'This is memo stuff which is not supported in beta, because tciEntity has no IM entries.
            SetHourglass False
            CMMemoSelected txtInvcID
            
        Case kTbHelp                                          'Help was Selected
                                                              
            gDisplayFormLevelHelp Me                          'Show Form level Help
        
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            If moDmHeader.State <> kDmStateNone Then
                If moLostFocus.IsValidDirtyCheck = 0 Then
                    SetHourglass False
                    mbDontChkclick = bOldmbClick
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                End If
            End If
            
            
          'Ask User if they want to save changes and run save if Answer = Yes
            If moDmHeader.ConfirmUnload(True) = kDmSuccess Then
                bSetupNavMain
                lRet = glLookupBrowse(navMain, sKey, miFilter, sNewKey)
                Select Case lRet
                    Case MS_SUCCESS
                        If navMain.ReturnColumnValues.Count = 0 Then
                            mbDontChkclick = bOldmbClick
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                            Exit Sub
                        End If
                        
                        If Trim(txtPaddedInvc) <> Trim(navMain.ReturnColumnValues("TranNo")) Then
                            txtPaddedInvc.Text = Trim(navMain.ReturnColumnValues("TranNo"))
                    
                          'Load Control
                            LoadMaskedControl kmaxInvoiceIDChars, txtInvcID, txtPaddedInvc
                  
                          'Get Tran Type for search
                            lTranKey = gvCheckNull(Trim(navMain.ReturnColumnValues("InvcKey")), SQL_INTEGER)
                            If lTranKey <> 0 Then
                                lTranType = gvCheckNull(moClass.moAppDB.Lookup("TranType", _
                                        "tarInvoice", "InvcKey = " & Format(lTranKey)), SQL_INTEGER)
                                If lTranType <> 0 Then
                                    cboInvcType.ListIndex = giListIndexFromItemData(cboInvcType, lTranType)
                                End If
                             End If
                    
                  'New Invoice
                            IsValidInvoice
                        End If
                    Case MS_BORS
                        HandleToolBarClick kTbFirst
            
                    Case MS_EMPTY_RS
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavEmptyRecordSet
            
                    Case MS_EORS
                        HandleToolBarClick kTbLast
            
                    Case MS_ERROR
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavBrowseError
            
                    Case MS_NO_CURRENT_KEY
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavNoCurrentKey
            
                    Case MS_UNDEF_RS
                        giSotaMsgBox Me, moClass.moSysSession, kmsgNavUndefinedError
            
                    Case Else
                        gLookupBrowseError lRet, Me, moClass
                
                End Select
            End If
        
        Case kTbFilter
            miFilter = IIf(miFilter = RSID_UNFILTERED, RSID_FILTERED, RSID_UNFILTERED)
            
'        'Intellisol start
'        Case PAGetHelpButton()
'            PAShowFormHelp Me, kPAHelpViewEditInvoices
'        'Intellisol end

        'AvaTax Integration - ToolbarClick: "AvaTax History"
        Case kTbAvalara
            If mbAvaTaxEnabled Then
                moAvaTax.GetTaxHistory moDmHeader.GetColumnValue("InvcKey")
            End If
        'AvaTax end

        Case Else
            tbrMain.GenericHandler sKey, Me, moDmHeader, moClass
    End Select
                        
    Select Case iActionCode
        Case kDmSuccess
            ClearControls True, True
            InitializeGlobalVariables
            moClass.lUIActive = kChildObjectInactive
            cboInvcType.Enabled = True
            cboInvcType.BackColor = vbWindowBackground
            gbSetFocus Me, cboInvcType
            txtInvcID.Protected = False
            moLEGridInvcDetl.InitDataReset
            If tabInvcEntry.Tab <> ktabIEHeader Then
                tabInvcEntry.Tab = ktabIEHeader
            End If
            'Intellisol start
            moProject.ResetJobGrid
            'Intellisol end
        Case kDmFailure
            'MsgBox "kdmfailure"
        Case kDmError
            'MsgBox "kdmerror"
        Case kSkip
            'do nothing
    End Select
    
    mbDontChkclick = bOldmbClick
    
    SetHourglass False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************************
' Run toolbar click event code for toolbar click
'**************************************************************************************
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bGetCustDefaults(iOnlyCust As Integer, Optional vExisting As Variant) As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*******************************************************************************************
' Desc:     Pull in customer defaults - Load boxes if they want to load defaults
' Parms:    iOnlyCust - Only Load customer defaults
'           vExisting - From an existing Invoice loading
'*******************************************************************************************
On Error GoTo ExpectedErrorRoutine2
    
    bGetCustDefaults = False    'Set return code
    SetHourglass True           'Set mouse pointer to an hourglass
    
    Dim lCustkey           As Long     'Customer Key
    Dim iAllowCustRefund   As Integer  'Allow Customer refunds
    Dim iAllowInvtSubst    As Integer  'Allow Inventory substitutions
    Dim iAllowWriteOff     As Integer  'Allow Write Offs
    Dim iBillingType       As Integer  'Billing Type
    Dim dCreditLimit       As Double   'Credit Limit
    Dim iCreditLimitAgeCat As Integer  'Credit Limit Aging category
    Dim sCustID            As String   'Customer id
    Dim sCustName          As String   'Customer Name
    Dim lBillToAddrKey     As Long     'Bill To Address
    Dim sBillToCustAddrID  As String   'Bill To Customer Address id
    Dim sBillToAddrName    As String   'Bill To Address Name
    Dim lDfltItemKey       As Long     'Default Item Key
    Dim lDfltSalesAcctKey  As Long     'Default Sales Account
    Dim lShipToAddrKey     As Long     'Ship To Address Key
    Dim sShipToCustAddrID  As String   'Ship To Customer Address id
    Dim sShipToAddrName    As String   'Ship To Address Name
    Dim iHold              As Integer  'On Hold
    Dim lPriceListKey      As Long     'Price List
    Dim lPrimaryAddrKey    As Long     'Primary Address
    Dim iReqPO             As Integer  'Purchase Order required
    Dim dRetntPct          As Double   'Retention Percent
    Dim iShipComplete      As Integer  'Ship Complete
    Dim iStatus            As Integer  'Customer Status
    Dim dTradeDiscPct      As Double   'Trade Discount Percent
    Dim sUserFld1          As String   'User field #1
    Dim sUserFld2          As String   'User field #2
    Dim lPmtTermsKey       As Long     'Payment Terms
    Dim lInvcFormKey       As Long     'Invoice Form
    Dim lCurrExchSchdKey   As Long     'Currency exchange schedule
    Dim sCurrID            As String   'Currency ID
    Dim lDfltCntctKey      As Long     'Default Contact Key
    Dim lShipMethKey       As Long     'Shipping Method
    Dim lSperKey           As Long     'salesperson Key
    Dim lSTaxSchdKey       As Long     'Sales Tax Schedule key
    Dim lFOBKey            As Long     'FOB Key
    Dim lCommPlanKey       As Long     'Commission Plan
    Dim lCustClassKey      As Long     'Customer Class
    Dim iRetVal            As Integer  'SP return code
    Dim sItem              As String   'Default Item ID
    Dim lClassOvrdKey      As Long     'Customer Class Override Key
    Dim sClassOvrdVal      As String   'Customer Class Override Value
    Dim iBillToParent           As Integer  'Bill to national account
    Dim iPmtByParent            As Integer  'Payment by national account parent
    Dim lNatlAcctKey            As Long     'National account surrogate key
    Dim lNatlAcctLevel          As Integer  'Customer's level in national acct
    Dim lNatlAcctLevelKey       As Long     'National account level key
    Dim lNatlAcctParentKey      As Long     'National account parent key
    Dim iNatlAcctOnHold         As Integer  'National account on hold
    Dim iNatlAcctCrLimitUsed    As Integer  'Is credit limit on Natl acct used.
    Dim lCurrBillToAddrKey      As Long     'Invoice BillToAddrKey
    Dim lCurrShipToAddrKey      As Long     'Invoice ShipToAddrKey
    
  'Get Customer Key
    lCustkey = gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)

  'get Customer defaults
    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB
        .SetInParam lCustkey             'Customer Key
        .SetInParam iOnlyCust            'Only Retrieve customer piece
        .SetOutParam iAllowCustRefund    'Allow Customer refunds
        .SetOutParam iAllowInvtSubst     'Allow Inventory substitutions
        .SetOutParam iAllowWriteOff      'Allow Write Offs
        .SetOutParam iBillingType        'Billing Type
        .SetOutParam dCreditLimit        'Credit Limit
        .SetOutParam iCreditLimitAgeCat  'Credit Limit Aging category
        .SetOutParam sCustID             'Customer id
        .SetOutParam sCustName           'Customer Name
        .SetOutParam lBillToAddrKey      'Bill To Address
        .SetOutParam sBillToCustAddrID   'Bill To Customer Address id
        .SetOutParam sBillToAddrName     'Bill To Address Name
        .SetOutParam lDfltItemKey        'Default Item Key
        .SetOutParam lDfltSalesAcctKey   'Default Sales Account
        .SetOutParam lShipToAddrKey      'Ship To Address Key
        .SetOutParam sShipToCustAddrID   'Ship To Customer Address id
        .SetOutParam sShipToAddrName     'Ship To Address Name
        .SetOutParam iHold               'On Hold
        .SetOutParam lPrimaryAddrKey     'Primary Address
        .SetOutParam iReqPO              'Purchase Order required
        .SetOutParam dRetntPct           'Retention Percent
        .SetOutParam iShipComplete       'Ship Complete
        .SetOutParam iStatus             'Customer Status
        .SetOutParam dTradeDiscPct       'Trade Discount Percent
        .SetOutParam sUserFld1           'User field #1
        .SetOutParam sUserFld2           'User field #2
        .SetOutParam lPmtTermsKey        'Payment Terms
        .SetOutParam lInvcFormKey        'Invoice Form
        .SetOutParam lCurrExchSchdKey    'Currency exchange schedule
        .SetOutParam sCurrID             'Currency ID
        .SetOutParam lDfltCntctKey       'Default Contact Key
        .SetOutParam lShipMethKey        'Shipping Method
        .SetOutParam lSperKey            'salesperson Key
        .SetOutParam lSTaxSchdKey        'Sales Tax Schedule key
        .SetOutParam lFOBKey             'FOB Key
        .SetOutParam lCommPlanKey        'Commission Plan
        .SetOutParam lCustClassKey       'Customer Class
        .SetOutParam lClassOvrdKey       'Customer Class Override Key
        .SetOutParam sClassOvrdVal       'Customer Class Override Value
        .SetOutParam iBillToParent          'Bill to national account
        .SetOutParam iPmtByParent           'Payment by national account parent
        .SetOutParam lNatlAcctKey           'National account surrogate key
        .SetOutParam lNatlAcctLevel         'Customer's level in national acct
        .SetOutParam lNatlAcctLevelKey      'National account level key
        .SetOutParam lNatlAcctParentKey     'National account parent key
        .SetOutParam iNatlAcctOnHold        'National account hold
        .SetOutParam iNatlAcctCrLimitUsed   'Is credit limit used on Natl Acct.
        .SetOutParam iRetVal             'return Value
        .ExecuteSP ("spGetARCustDflts")             'Run the stored procedure
        iAllowCustRefund = gvCheckNull(.GetOutParam(3), SQL_INTEGER)  'Allow Customer refunds
        iAllowInvtSubst = gvCheckNull(.GetOutParam(4), SQL_INTEGER)   'Allow Inventory substitutions
        iAllowWriteOff = gvCheckNull(.GetOutParam(5), SQL_INTEGER)    'Allow Write Offs
        iBillingType = gvCheckNull(.GetOutParam(6), SQL_INTEGER)      'Billing Type
        dCreditLimit = gvCheckNull(.GetOutParam(7), SQL_INTEGER)      'Credit Limit
        iCreditLimitAgeCat = gvCheckNull(.GetOutParam(8), SQL_INTEGER) 'Credit Limit Aging category
        sCustID = gvCheckNull(.GetOutParam(9))                        'Customer id
        sCustName = gvCheckNull(.GetOutParam(10))                     'Customer Name
        lBillToAddrKey = gvCheckNull(.GetOutParam(11), SQL_INTEGER)   'Bill To Address
        sBillToCustAddrID = gvCheckNull(.GetOutParam(12))             'Bill To Customer Address id
        sBillToAddrName = gvCheckNull(.GetOutParam(13))               'Bill To Address Name
        lDfltItemKey = gvCheckNull(.GetOutParam(14), SQL_INTEGER)     'Default Item Key
        lDfltSalesAcctKey = gvCheckNull(.GetOutParam(15), SQL_INTEGER) 'Default Sales Account
        lShipToAddrKey = gvCheckNull(.GetOutParam(16), SQL_INTEGER)   'Ship To Address Key
        sShipToCustAddrID = gvCheckNull(.GetOutParam(17))             'Ship To Customer Address id
        sShipToAddrName = gvCheckNull(.GetOutParam(18))               'Ship To Address Name
        iHold = gvCheckNull(.GetOutParam(19), SQL_INTEGER)            'On Hold
        lPrimaryAddrKey = gvCheckNull(.GetOutParam(20), SQL_INTEGER)  'Primary Address
        iReqPO = gvCheckNull(.GetOutParam(21), SQL_INTEGER)           'Purchase Order required
        dRetntPct = gvCheckNull(.GetOutParam(22), SQL_INTEGER)        'Retention Percent
        iShipComplete = gvCheckNull(.GetOutParam(23), SQL_INTEGER)    'Ship Complete
        iStatus = gvCheckNull(.GetOutParam(24), SQL_INTEGER)          'Customer Status
        dTradeDiscPct = gvCheckNull(.GetOutParam(25), SQL_FLOAT)      'Trade Discount Percent
        sUserFld1 = gvCheckNull(.GetOutParam(26))                     'User field #1
        sUserFld2 = gvCheckNull(.GetOutParam(27))                     'User field #2
        lPmtTermsKey = gvCheckNull(.GetOutParam(28), SQL_INTEGER)     'Payment Terms
        lInvcFormKey = gvCheckNull(.GetOutParam(29), SQL_INTEGER)     'Invoice Form
        lCurrExchSchdKey = gvCheckNull(.GetOutParam(30), SQL_INTEGER) 'Currency exchange schedule
        sCurrID = gvCheckNull(.GetOutParam(31))                       'Currency ID
        lDfltCntctKey = gvCheckNull(.GetOutParam(32), SQL_INTEGER)    'Default Contact Key
        lShipMethKey = gvCheckNull(.GetOutParam(33), SQL_INTEGER)     'Shipping Method
        lSperKey = gvCheckNull(.GetOutParam(34), SQL_INTEGER)         'salesperson Key
        lSTaxSchdKey = gvCheckNull(.GetOutParam(35), SQL_INTEGER)     'Sales Tax Schedule key
        lFOBKey = gvCheckNull(.GetOutParam(36), SQL_INTEGER)          'FOB Key
        lCommPlanKey = gvCheckNull(.GetOutParam(37), SQL_INTEGER)     'Commission Plan
        lCustClassKey = gvCheckNull(.GetOutParam(38), SQL_INTEGER)    'Customer Class Key
        lClassOvrdKey = gvCheckNull(.GetOutParam(39), SQL_INTEGER)    'Customer Class Override Key
        sClassOvrdVal = gvCheckNull(.GetOutParam(40))                 'Customer Class Override Value
        iBillToParent = giGetValidInt(.GetOutParam(41))               'Bill to national account
        iPmtByParent = giGetValidInt(.GetOutParam(42))                'Payment by national account parent
        lNatlAcctKey = glGetValidLong(.GetOutParam(43))               'National account surrogate key
        lNatlAcctLevel = glGetValidLong(.GetOutParam(44))             'Customer's level in national acct
        lNatlAcctLevelKey = glGetValidLong(.GetOutParam(45))          'National account level key
        lNatlAcctParentKey = glGetValidLong(.GetOutParam(46))         'National account parent key
        iNatlAcctOnHold = giGetValidInt(.GetOutParam(47))            'National account hold
        iNatlAcctCrLimitUsed = giGetValidInt(.GetOutParam(48))       'Is credit limit used on
        iRetVal = gvCheckNull(.GetOutParam(49), SQL_INTEGER)          'return Code
        .ReleaseParams
    End With
    On Error GoTo ExpectedErrorRoutine
   
  'Default existing flag to false
    If IsMissing(vExisting) Then
        vExisting = False
    End If
    
'Load Customer id (Pad with zeros)
    txtPaddedCust.Text = sCustID
    LoadMaskedControl kmaxCustIDChars, txtCustID, txtPaddedCust

    muCustDflts.AllowCustRefund = iAllowCustRefund
    muCustDflts.AllowInvtSubst = iAllowInvtSubst
    muCustDflts.AllowWriteOff = iAllowWriteOff
    muCustDflts.BillingType = iBillingType
    muCustDflts.Hold = iHold                            'Hold
    muCustDflts.DfltItemKey = lDfltItemKey              'Default Item
    muCustDflts.DfltSalesAcctKey = lDfltSalesAcctKey    'Default Slaes Account
    muCustDflts.PriceListKey = lPriceListKey            'Price List
    muCustDflts.PrimaryAddrKey = lPrimaryAddrKey        'Primary Address
    muCustDflts.ReqPO = iReqPO                          'P.O. Required
    muCustDflts.ShipComplete = iShipComplete            'Ship Complete
    muCustDflts.Status = iStatus                        'Status
    muCustDflts.TradeDiscPct = dTradeDiscPct            'Trade Discount Percent
    muCustDflts.CurrExchSchdKey = lCurrExchSchdKey      'Currency exchange Schedule
    muCustDflts.CustClassKey = lCustClassKey            'Customer Class
    muCustDflts.ClassOvrdSegKey = lClassOvrdKey         'Customer Class Override Key
    muCustDflts.ClassOvrdSegVal = sClassOvrdVal         'Customer Class Override Value
     muCustDflts.BillToParent = (iBillToParent = 1)          'Bill to Natl Acct parent?
    muCustDflts.PmtByParent = (iPmtByParent = 1)            'Pmt to Natl Acct parent?
    muCustDflts.NatlAcctKey = lNatlAcctKey                  'National Account surrogate key
    muCustDflts.NatlAcctParentKey = lNatlAcctParentKey      'National Account parent surrogate key
    muCustDflts.NatlAcctLevelKey = lNatlAcctLevelKey        'National Account Level surrogate key
    muCustDflts.NatlAcctLevel = lNatlAcctLevel              '1 = parent, 2 = subsidiary
    muCustDflts.NatlAcctOnHold = (iNatlAcctOnHold = 1)      'Is national acct on hold
    muCustDflts.NatlAcctCrLimitUsed = (iNatlAcctCrLimitUsed = 1) 'Is natl acct credit limit used
      
    
  'Display whether the customer is on Hold or not
    DisplayStatus
    
    'Load bill to, ship to
    'For existing invoice, use the existing ShipToAddrKey and BillToAddrKey,
    'otherwise, default the Bill/ShipToCustAddrKey for the Bill/ShipToAddrKey
    lCurrShipToAddrKey = glGetValidLong(moDmHeader.GetColumnValue("ShipToAddrKey"))
    If lCurrShipToAddrKey = 0 Then
        lCurrShipToAddrKey = glGetValidLong(moDmHeader.GetColumnValue("ShipToCustAddrKey"))
        moDmHeader.SetColumnValue "ShipToAddrKey", lCurrShipToAddrKey
    End If
    moDmSTAddress.SetColumnValue "AddrKey", lCurrShipToAddrKey
    moDmSTAddress.KeyChange
    
    
    lCurrBillToAddrKey = glGetValidLong(moDmHeader.GetColumnValue("BillToAddrKey"))
    If lCurrBillToAddrKey = 0 Then
       lCurrBillToAddrKey = glGetValidLong(moDmHeader.GetColumnValue("BillToCustAddrKey"))
       moDmHeader.SetColumnValue "BillToAddrKey", lCurrBillToAddrKey
       
    End If
    moDmBTAddress.SetColumnValue "AddrKey", lCurrBillToAddrKey
    moDmBTAddress.KeyChange
        
    If Len(gvCheckNull(moDmHeader.GetColumnValue("CurrID"), SQL_CHAR)) > 0 Then
        bSetFormCurrencyControls gvCheckNull(moDmHeader.GetColumnValue("CurrID"), SQL_CHAR)
    End If
    
    txtFOB.Tag = txtFOB.Text
    
    SetHourglass False
    bGetCustDefaults = True
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
ExpectedErrorRoutine:
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
    "spGetARCustDflts: " & Error$(Err)
SetHourglass False
gClearSotaErr
Exit Function

ExpectedErrorRoutine2:
MyErrMsg moClass, Err.Description, Err, sMyName, "bGetCustDefaults"
SetHourglass False
gClearSotaErr
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetCustDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
  'Set initial grid properties
    gGridSetProperties grdInvcDetl, kMaxColsDetail, kGridLineEntry
    gGridSetMaxRows grdInvcDetl, 1
    grdInvcDetl.FloatDefDecimalChar = Asc(curSalesAmt.DecimalSymbol) 'Asc(".") '
    grdInvcDetl.FloatDefSepChar = Asc(curSalesAmt.ThousandSymbol) 'Asc(",") '
    grdInvcDetl.TypeFloatSeparator = True
    gGridSetMaxRows grdInvcLineDist, kMaxColsLineDist
    
    'Intellisol start
    gGridSetProperties grdJobLine, kMaxColsJ, kGridLineEntry
    gGridHideColumn grdInvcDetl, kcolIEProjClientID
    gGridHideColumn grdInvcDetl, kcolIEProjID
    'Intellisol end
    
  'Setup Grid headers
    gGridSetHeader grdInvcDetl, kColIEItemIDmsk, "Item"
    gGridSetHeader grdInvcDetl, kcolIEDescription, "Item Desc"
    gGridSetHeader grdInvcDetl, kcolIEQtyView, "Qty"
    gGridSetHeader grdInvcDetl, kcolIEUnitMeasID, "UOM"
    gGridSetHeader grdInvcDetl, kcolIEUnitPrice, "Unit Price"
    gGridSetHeader grdInvcDetl, kcolIESalesAmtView, "Sales Amt"
    gGridSetHeader grdInvcDetl, kcolIEExtCmnt, "Comment"
    gGridSetHeader grdInvcDetl, kcolIESTaxClassID, "Sales Tax Class"
    gGridSetHeader grdInvcDetl, kcolIETradeDiscAmtView, "Trade Disc Amt"
    gGridSetHeader grdInvcDetl, kcolIETradeDiscPct, "Trade Disc Pct"
    gGridSetHeader grdInvcDetl, kcolIEUnitCostView, "Unit Cost"
    gGridSetHeader grdInvcDetl, kcolIECommPlanID, "Comm Plan"
    gGridSetHeader grdInvcDetl, kcolIECommClassID, "Comm Class"
    gGridSetHeader grdInvcDetl, kcolIEOrigCommAmtView, "Orig Comm Amt"
    gGridSetHeader grdInvcDetl, kcolIEActCommAmtView, "Act Comm Amt"
    gGridSetHeader grdInvcDetl, kcolIESalesAcctIDmsk, "Account"
    gGridSetHeader grdInvcDetl, kcolIEAcctRefCode, msRefCodeLabel
    gGridSetHeader grdInvcDetl, kcolIEProjClientID, "Project Client ID"
    gGridSetHeader grdInvcDetl, kcolIEProjID, "Project ID"
    gGridSetHeader grdInvcDetl, kcolIEFOBID, "FOB"
    gGridSetHeader grdInvcDetl, kcolIEFreightAmtView, "Freight Amt"
    gGridSetHeader grdInvcDetl, kcolIEShipMethID, "Shipping Method"
    gGridSetHeader grdInvcDetl, kcolIESTaxAmtView, "Sales Tax Amt"
    gGridSetHeader grdInvcDetl, kcolIESOTranNoRel, "Sales Order"
    
    '****************************************************************************
    '****************************************************************************
    'RKL DEJ 2016-10-10 (START)
    '****************************************************************************
    '****************************************************************************
    gGridSetHeader grdInvcDetl, kcolSOFrghtInPrice, "PB-Freight In Price"
    gGridSetHeader grdInvcDetl, kcolSOFreightAmt, "PB-Freight Amt"
    gGridSetHeader grdInvcDetl, kcolSOSpreadInPrice, "PB-Spreader In Price"
    gGridSetHeader grdInvcDetl, kcolSOSpreadAmt, "PB-Spreader Amt"
    gGridSetHeader grdInvcDetl, kcolSOItemAmt, "PB-Product Amt"
    gGridSetHeader grdInvcDetl, kcolSOPriceBrkAmt, "PB-Total Amt"
    gGridSetHeader grdInvcDetl, kcolSOPriceBrkKey, "PB-Price Break Key"
    
    gGridSetHeader grdInvcDetl, kcolSOFrghtRate, "SO-Freight Rate"
    gGridSetHeader grdInvcDetl, kcolSOSpreadTon, "SO-Spread/Ton"
    gGridSetHeader grdInvcDetl, kcolSOSpreadHr, "SO-Spread/Hr"
    
    gGridSetColumnWidth grdInvcDetl, kcolSOFreightAmt, 15
    gGridSetColumnType grdInvcDetl, kcolSOFreightAmt, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    gGridSetColumnWidth grdInvcDetl, kcolSOSpreadAmt, 15
    gGridSetColumnType grdInvcDetl, kcolSOSpreadAmt, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    gGridSetColumnWidth grdInvcDetl, kcolSOItemAmt, 15
    gGridSetColumnType grdInvcDetl, kcolSOItemAmt, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    gGridSetColumnWidth grdInvcDetl, kcolSOPriceBrkAmt, 15
    gGridSetColumnType grdInvcDetl, kcolSOPriceBrkAmt, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    gGridHideColumn grdInvcDetl, kcolSOFrghtInPrice
    gGridHideColumn grdInvcDetl, kcolSOSpreadInPrice
    gGridHideColumn grdInvcDetl, kcolSOPriceBrkKey
    
    gGridSetColumnWidth grdInvcDetl, kcolSOFrghtRate, 15
    gGridSetColumnType grdInvcDetl, kcolSOFrghtRate, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    gGridSetColumnWidth grdInvcDetl, kcolSOSpreadTon, 15
    gGridSetColumnType grdInvcDetl, kcolSOSpreadTon, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    gGridSetColumnWidth grdInvcDetl, kcolSOSpreadHr, 15
    gGridSetColumnType grdInvcDetl, kcolSOSpreadHr, SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces, curSTaxDetl.IntegralPlaces
    
    '****************************************************************************
    '****************************************************************************
    'RKL DEJ 2016-10-10 (STOP)
    '****************************************************************************
    '****************************************************************************
  
    
  'Set grid column widths
  'Masked Item id
    gGridSetColumnWidth grdInvcDetl, kColIEItemIDmsk, 20
    gGridSetColumnType grdInvcDetl, kColIEItemIDmsk, SS_CELL_TYPE_EDIT
    grdInvcDetl.ColsFrozen = kColIEItemIDmsk
    
  'Item Description
    gGridSetColumnWidth grdInvcDetl, kcolIEDescription, 20
    gGridSetColumnType grdInvcDetl, kcolIEDescription, SS_CELL_TYPE_EDIT
    grdInvcDetl.Row = -1
    grdInvcDetl.col = kcolIEDescription
    grdInvcDetl.TypeMaxEditLen = 255
    
  'Masked Sales Account
    gGridSetColumnWidth grdInvcDetl, kcolIESalesAcctIDmsk, 15
    gGridSetColumnType grdInvcDetl, kcolIESalesAcctIDmsk, SS_CELL_TYPE_EDIT
    
  'Reference Code column
    gGridSetColumnWidth grdInvcDetl, kcolIEAcctRefCode, 20
    gGridSetColumnType grdInvcDetl, kcolIEAcctRefCode, SS_CELL_TYPE_EDIT

  'Quantity shipped
    gGridSetColumnWidth grdInvcDetl, kcolIEQtyView, 15
    gGridSetColumnType grdInvcDetl, kcolIEQtyView, _
            SS_CELL_TYPE_FLOAT, numQty.DecimalPlaces
    
  'Unit of measure
    gGridSetColumnWidth grdInvcDetl, kcolIEUnitMeasID, 17
    gGridSetColumnType grdInvcDetl, kcolIEUnitMeasID, SS_CELL_TYPE_EDIT
    
  'Unit Price
    gGridSetColumnWidth grdInvcDetl, kcolIEUnitPrice, 17
    gGridSetColumnType grdInvcDetl, kcolIEUnitPrice, SS_CELL_TYPE_FLOAT, _
                        curUnitPrice.DecimalPlaces, curUnitPrice.IntegralPlaces
            

    
  'Unit Cost
    gGridSetColumnWidth grdInvcDetl, kcolIEUnitCostView, 17
    gGridSetColumnType grdInvcDetl, kcolIEUnitCostView, _
            SS_CELL_TYPE_FLOAT, curUnitCost.DecimalPlaces
  
  'sales amount
    gGridSetColumnWidth grdInvcDetl, kcolIESalesAmtView, 17
    gGridSetColumnType grdInvcDetl, kcolIESalesAmtView, _
            SS_CELL_TYPE_FLOAT, curSalesAmt.DecimalPlaces
    grdInvcDetl.TypeFloatSeparator = True
    
  'extended comment
    gGridSetColumnWidth grdInvcDetl, kcolIEExtCmnt, 20
    gGridSetColumnType grdInvcDetl, kcolIEExtCmnt, SS_CELL_TYPE_EDIT
    
  'sales tax class id
    gGridSetColumnWidth grdInvcDetl, kcolIESTaxClassID, 15
    gGridSetColumnType grdInvcDetl, kcolIESTaxClassID, SS_CELL_TYPE_EDIT
    
  'trade Discount amount
    gGridSetColumnWidth grdInvcDetl, kcolIETradeDiscAmtView, 15
    gGridSetColumnType grdInvcDetl, kcolIETradeDiscAmtView, _
                SS_CELL_TYPE_FLOAT, curTradeDiscAmt.DecimalPlaces
    grdInvcDetl.TypeFloatSeparator = True
    
    gGridSetColumnType grdInvcDetl, kcolIETradeDiscPct, SS_CELL_TYPE_FLOAT, 2
    
  'Commission Plan id
    gGridSetColumnWidth grdInvcDetl, kcolIECommPlanID, 15
    gGridSetColumnType grdInvcDetl, kcolIECommPlanID, SS_CELL_TYPE_EDIT
    
  'Commission Class
    gGridSetColumnWidth grdInvcDetl, kcolIECommClassID, 15
    gGridSetColumnType grdInvcDetl, kcolIECommClassID, SS_CELL_TYPE_EDIT
    
  'Original commission amount
    gGridSetColumnWidth grdInvcDetl, kcolIEOrigCommAmtView, 10
    gGridSetColumnType grdInvcDetl, kcolIEOrigCommAmtView, SS_CELL_TYPE_FLOAT, _
            curOrigCommAmt.DecimalPlaces, curOrigCommAmt.IntegralPlaces
    grdInvcDetl.TypeFloatSeparator = True
    
    gGridSetColumnWidth grdInvcDetl, kcolIEActCommAmtView, 10
    gGridSetColumnType grdInvcDetl, kcolIEActCommAmtView, SS_CELL_TYPE_FLOAT, _
                        curActCommAmt.DecimalPlaces, curActCommAmt.IntegralPlaces
    grdInvcDetl.TypeFloatSeparator = True
    
  ' sales tax amount
    gGridSetColumnWidth grdInvcDetl, kcolIESTaxAmtView, 15
    gGridSetColumnType grdInvcDetl, kcolIESTaxAmtView, _
            SS_CELL_TYPE_FLOAT, curSTaxDetl.DecimalPlaces
    grdInvcDetl.TypeFloatSeparator = True
            
  ' freight amount
    gGridSetColumnWidth grdInvcDetl, kcolIEFreightAmtView, 15
    gGridSetColumnType grdInvcDetl, kcolIEFreightAmtView, _
            SS_CELL_TYPE_FLOAT, curFrtAmtDetl.DecimalPlaces
        grdInvcDetl.TypeFloatSeparator = True

    'Project Client ID
    gGridSetColumnWidth grdInvcDetl, kcolIEProjClientID, 12
    gGridSetColumnType grdInvcDetl, kcolIEProjClientID, SS_CELL_TYPE_EDIT
    
    'Project ID
    gGridSetColumnWidth grdInvcDetl, kcolIEProjID, 20
    gGridSetColumnType grdInvcDetl, kcolIEProjID, SS_CELL_TYPE_EDIT

  ' Sales Order
    gGridSetColumnWidth grdInvcDetl, kcolIESOTranNoRel, 15
    gGridSetColumnType grdInvcDetl, kcolIESOTranNoRel, SS_CELL_TYPE_EDIT
    
    
  'Hide columns
    gGridHideColumn grdInvcDetl, kcolIEInvcKey              'Invoice key
    gGridHideColumn grdInvcDetl, kcolIECmntOnly             'Comment only
    gGridHideColumn grdInvcDetl, kcolIECommBase             'Commission base
    gGridHideColumn grdInvcDetl, kcolIECommClassDtlKey      'Commission Class Key
    gGridHideColumn grdInvcDetl, kcolIECommPlanDtlKey       'Commission Plan Key
    gGridHideColumn grdInvcDetl, kcolIEItemKey              'Item Key
    gGridHideColumn grdInvcDetl, kcolIESalesAcctKey         'Sales acct Key
    gGridHideColumn grdInvcDetl, kcolIESalesAcctID          'Sales acct Id (unmasked)
    gGridHideColumn grdInvcDetl, kcolIESalesAcctDesc        'Sales acct description
    gGridHideColumn grdInvcDetl, kcolIESeqNo                'sequence Number
    gGridHideColumn grdInvcDetl, kcolIESTaxClassKey         'Sales Tax Class Key
    gGridHideColumn grdInvcDetl, kcolIEUnitMeasKey          'Unit of measure key
    gGridHideColumn grdInvcDetl, kcolIEUnitPriceOvrd        'Unit Price override
    gGridHideColumn grdInvcDetl, kColIEItemID               'Item id
    gGridHideColumn grdInvcDetl, kcolIESalesAmt             'Sales Amount (bound)
    gGridHideColumn grdInvcDetl, kcolIEUnitCost             'Unit Cost (bound)
    gGridHideColumn grdInvcDetl, kcolIETradeDiscAmt         'Trade Discount Amount(bound)
    gGridHideColumn grdInvcDetl, kcolIEOrigCommAmt          'Calculated commission amount(bound)
    gGridHideColumn grdInvcDetl, kcolIEActCommAmt           'Actual Commission amount (bound)
    gGridHideColumn grdInvcDetl, kcolIEQty                  'quantity (bound)
    gGridHideColumn grdInvcDetl, kcolIESubjToTradeDisc      'Item Subject to trade Discount flag
    gGridHideColumn grdInvcDetl, kcolIEInvoiceLineDistKey   'Invoice Line Dist Key
    gGridHideColumn grdInvcDetl, kcolIEAcctRefKey           'Account Reference Key
    If miRefCodeUsage = 0 Then
        gGridHideColumn grdInvcDetl, kcolIEAcctRefCode      'Account Reference Code
    End If
    gGridHideColumn grdInvcDetl, kcolIEDistExtAmt           'Distribution line extended amount
    gGridHideColumn grdInvcDetl, kcolIEFOBKey               'FOB Key
    gGridHideColumn grdInvcDetl, kcolIEFreightAmt           'Freight Amount
    gGridHideColumn grdInvcDetl, kcolIEInvoiceLineKey       'Invoice Line Key
    gGridHideColumn grdInvcDetl, kcolIEQtyShipped           'Quantity Shipped
    gGridHideColumn grdInvcDetl, kcolIEShipMethKey           'Shipping Method Key
    gGridHideColumn grdInvcDetl, kcolIEShipZoneKey          'Shipping Zone Key
    gGridHideColumn grdInvcDetl, kcolIEShipZoneID           'Shipping Zone ID
    gGridHideColumn grdInvcDetl, kcolIESTaxSchdID           'Sales Tax Schedule ID
    gGridHideColumn grdInvcDetl, kcolIESTaxSchdKey          'Sales Tax Schedule Key
    gGridHideColumn grdInvcDetl, kcolIESTaxTranKey          'Sales Tax Transaction Key
    gGridHideColumn grdInvcDetl, kcolIESTaxAmt              'Sales Tax Amount
    gGridHideColumn grdInvcDetl, kcolIEFreightAmt           'Freight Amount
    gGridHideColumn grdInvcDetl, kcolIEKitInvoiceLineKey    'Kit Invoice Key
    gGridHideColumn grdInvcDetl, kcolIERtrnType             'Return Type
    gGridHideColumn grdInvcDetl, kcolIEShipLineKey          'Ship Line Key
    gGridHideColumn grdInvcDetl, kcolIESOLineKey            'SO Line Key
    gGridHideColumn grdInvcDetl, kcolIEShipLineDistKey      'Ship Line Dist Key
    gGridHideColumn grdInvcDetl, kcolIESOLineDistKey        'SO Line Dist Key
    gGridHideColumn grdInvcDetl, kcolIESOKey                'SO Key
    
    'Intellisol start
    moProject.InitializeJobGrid
    moProject.InitializeJobGridInvoice curSalesAmt.DecimalPlaces
    'Intellisol end
    
  'Lock all columns in the grid and set the default colors
    gGridSetColors grdInvcDetl

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DMGridRowLoaded(oDMO As Object, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Description:
'      Fires when each row of data is loaded by the DM into the grid.
'
'      This is a manual movement of a copy of the data from the DM to the correct
'      grid cell on the same row.
'************************************************************************************
    
    Dim sDispExtComment As String   'Display Extended Comment
    Dim sGLAcctNo       As String   'General Ledger Account Number
    Dim dTradeDiscAmt   As Double   'Trade Discount Amount
    Dim dSalesAmt       As Double   'Sales Amount
    Dim sTradeDiscPct   As String   'Trade Discount Percent
    Dim iCommType       As Integer  'Commission Type
    Dim dQty            As Double   'Quantity
    Dim dOrigCommAmt    As Double   'Original Commission Amount
    Dim dActCommAmt     As Double   'Actual Commission Amount
    Dim sItemID         As String
    Dim dAmt            As Double
    Dim dFrt            As Double   'Freight
    Dim dSTax           As Double   'Sales Tax
    Dim sSalesOrder     As String
    Dim dUnitCostAmt    As Double
     
    If oDMO Is moDmDetail Then
      'Assign the Masked G/L Account No. column
       txtSalesAcctLoad.Text = gsGridReadCell(grdInvcDetl, lRow, kcolIESalesAcctID)
        gGridUpdateCell grdInvcDetl, lRow, kcolIESalesAcctIDmsk, _
                        txtSalesAcctLoad.MaskedText
      
      'Assign the Masked Item ID column
        txtItemIdLoad.Text = gsGridReadCell(grdInvcDetl, lRow, kColIEItemID)
        gGridUpdateCell grdInvcDetl, lRow, kColIEItemIDmsk, txtItemIdLoad.MaskedText
        
      'Load Numeric values (multiplied by -1 if credit memo)
      'Quantity
        dQty = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIEQty)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIEQtyView, Format$(dQty)
      
      'Freight Amt
        dFrt = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIEFreightAmt)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIEFreightAmtView, Format$(dFrt)
      
      'Sales Tax Amt
        dSTax = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIESTaxAmt)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIESTaxAmtView, Format$(dSTax)
      'Sales Amount
        dSalesAmt = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIESalesAmt)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIESalesAmtView, Format$(dSalesAmt)
        
      'UnitCost
        dUnitCostAmt = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIEUnitCost)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIEUnitCostView, Format$(dUnitCostAmt)
        
      'Trade Discount Amount
        dTradeDiscAmt = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIETradeDiscAmt)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIETradeDiscAmtView, Format$(dTradeDiscAmt)
        
      'Original Commission Amount
        dOrigCommAmt = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIEOrigCommAmt)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIEOrigCommAmtView, _
             gfRound(dOrigCommAmt, miHomeRoundMeth, miHomeDigAfterDec)
      
      'Actual Commission Amount
        dActCommAmt = dSetValue(Val(gsGridReadCell(grdInvcDetl, lRow, kcolIEActCommAmt)))
        gGridUpdateCell grdInvcDetl, lRow, kcolIEActCommAmtView, _
            gfRound(dActCommAmt, miHomeRoundMeth, miHomeDigAfterDec)

      'Calculate the Trade Discount Percent
        If dSalesAmt = 0 Then
            sTradeDiscPct = "0.0000"
        Else
            sTradeDiscPct = Format(((dTradeDiscAmt / dSalesAmt) * 100), "##0.00")
        End If
        gGridUpdateCell grdInvcDetl, lRow, kcolIETradeDiscPct, sTradeDiscPct
         
    
      'Assign the Displayed Extended Comment column
      'This column is 'copied' from the full Extended Comment column
        'sDispExtComment = gsGridReadCell(grdVoucherDetl(miDetlType), lRow, mlColExtComment)
        'gGridUpdateCell grdVoucherDetl(miDetlType), lRow, mlColDispExtComment, sDispExtComment
        
        
        ' Get the SalesOrder
        If gsGridReadCell(grdInvcDetl, lRow, kcolIESOKey) <> "" Then
            sSalesOrder = gvCheckNull(moClass.moAppDB.Lookup("TranNoRel", "tsoSalesOrder WITH (NOLOCK)", _
                          "tsoSalesOrder.SOKey = " & _
                          glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESOKey))), SQL_CHAR)
            gGridUpdateCell grdInvcDetl, lRow, kcolIESOTranNoRel, sSalesOrder
        End If
        
        '*****************************************************************
        '*****************************************************************
        'RKL DEJ 2016-10-10 (START)
        'Get the Spread/Ton and Spread/Hr from SO Header
        '*****************************************************************
        '*****************************************************************
        Dim lcSpreadTon As Double
        Dim lcSpreadHr As Double
        
        lcSpreadTon = gvCheckNull(moClass.moAppDB.Lookup("SpreadTon", "tsoSalesOrderExt_SGS WITH (NOLOCK)", _
                      "tsoSalesOrderExt_SGS.SOKey = " & glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESOKey))), SQL_DECIMAL)
        gGridUpdateCell grdInvcDetl, lRow, kcolSOSpreadTon, CStr(lcSpreadTon)
        
        lcSpreadHr = gvCheckNull(moClass.moAppDB.Lookup("SpreadHour", "tsoSalesOrderExt_SGS WITH (NOLOCK)", _
                      "tsoSalesOrderExt_SGS.SOKey = " & glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESOKey))), SQL_DECIMAL)
        gGridUpdateCell grdInvcDetl, lRow, kcolSOSpreadHr, CStr(lcSpreadHr)
        '*****************************************************************
        '*****************************************************************
        'RKL DEJ 2016-10-10 (STOP)
        '*****************************************************************
        '*****************************************************************
        
        'Intellisol start
        '-- Load the associated PA link record into the link grid
        moProject.GridRowLoaded lRow
        'Intellisol end

    ElseIf oDMO Is moDmComms Then
      
      'Load Commission Type Description
        iCommType = Val(gsGridReadCell(frmCommissions.grdComms, lRow, kColCommType))
        
      'Load Commission Type Description based on the code
        If iCommType = 1 Then
            gGridUpdateCell frmCommissions.grdComms, lRow, kColCommTypeView, frmCommissions.sCommType1
        Else
            gGridUpdateCell frmCommissions.grdComms, lRow, kColCommTypeView, frmCommissions.sCommType2
        End If
        
        moDmComms.Grid.Row = lRow
      'Right Align Actual Commission Amount
        frmCommissions.grdComms.col = kColCommActCommAmtView
        frmCommissions.grdComms.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
      'Right Align calculated Commission Amount
        frmCommissions.grdComms.col = kColCommCalcCommAmtView
        frmCommissions.grdComms.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
      'Right Align  Subj Cost of Sales
        frmCommissions.grdComms.col = kColCommSubjCOSView
        frmCommissions.grdComms.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
      'Right Align Subject Sales Amount
        frmCommissions.grdComms.col = kColCommSubjSalesView
        frmCommissions.grdComms.TypeHAlign = SS_CELL_H_ALIGN_RIGHT
        
      'Load values multiplied by -1
        dAmt = dSetValue(Val(gsGridReadCell(frmCommissions.grdComms, lRow, kColCommActCommAmt)))
        gGridUpdateCell frmCommissions.grdComms, lRow, kColCommActCommAmtView, msHomeCurrSymbol & gfRound(dAmt, kvRoundStandard, miHomeDigAfterDec)
        dAmt = dSetValue(Val(gsGridReadCell(frmCommissions.grdComms, lRow, kColCommCalcCommAmt)))
        gGridUpdateCell frmCommissions.grdComms, lRow, kColCommCalcCommAmtView, msHomeCurrSymbol & gfRound(dAmt, kvRoundStandard, miHomeDigAfterDec)
        dAmt = dSetValue(Val(gsGridReadCell(frmCommissions.grdComms, lRow, kColCommSubjCOS)))
        gGridUpdateCell frmCommissions.grdComms, lRow, kColCommSubjCOSView, msHomeCurrSymbol & gfRound(dAmt, kvRoundStandard, miHomeDigAfterDec)
        dAmt = dSetValue(Val(gsGridReadCell(frmCommissions.grdComms, lRow, kColCommSubjSales)))
        gGridUpdateCell frmCommissions.grdComms, lRow, kColCommSubjSalesView, msHomeCurrSymbol & gfRound(dAmt, kvRoundStandard, miHomeDigAfterDec)
    
    End If
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function LECheckRequiredData(oLE As clsLineEntry) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Soft touch validation in case hard validation at the
'       source control was click-bypassed.
' Parameters: oLE - the line entry class object calling this procedure.
'***********************************************************************
    'To warn the user when project is closed and to revert the changes on edit.
    If moProject.LECheckRequiredData() = False Then
        moLEGridInvcDetl.GridEditUndo
        Exit Function
    End If
    LECheckRequiredData = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LECheckRequiredData", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Sub LEClearDetailControls(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    'Intellisol start
    moProject.LEClearDetailControls
    
    'RKL DEJ 2016-10-10 (START)
    numFreightRate.ClearData
    numSpreadTon.ClearData
    numSpreadHr.ClearData
    'RKL DEJ 2016-10-10 (STOP)
    
    'Intellisol end

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEClearDetailControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub BindLE()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Desc:   Binds grids to standard line entry object manager.
'
'  Parms:   None.
'************************************************************************************
   'ZZDebug.Print "BindLE"
    
    'Initiate a new line entry object for the Invoice Detail data
    
    With moLEGridInvcDetl
        .AllowGotoLine = True
Set .Form = frmInvcEntry
        Set .Grid = grdInvcDetl
        Set .TabControl = tabInvcEntry
        Set .DM = moDmDetail
        Set .Ok = cmdDtl(kLEOK)
        Set .Undo = cmdDtl(kLEUndo)
'        Set .Menu = mnuGridRightMouse
'        Set .MenuAdd = mnuAdd
'        Set .MenuInsert = mnuInsert
'        Set .MenuDelete = mnuDelete
'        Set .MenuGoto = mnuGoto
'        Set .MenuWhatsThis = mnuWhatsThis
        ' **PRESTO ** Set .Hook = WinHook1
        Set .FocusRect = shpFocusRect
        .SeqNoCol = kcolIESeqNo
        .GridType = kGridLENoAppend
        .TabDetailIndex = ktabIEDetail
        .Init
    End With

'    With moLEGridInvcDetl.Hook
'        .HookEnabled = False
'        ' **PRESTO ** .HookType = shkGetMessage
'        ' **PRESTO ** .KeyboardEvent = shkKbdExtended
'        ' **PRESTO ** .KeyboardHook = shkKbdThisTask
'        ' **PRESTO ** .KeyboardNotify = shkWhenHooked
'        .Keys = vbKeyTab
'        .Keys = vbKeyReturn
'        .Keys = vbKeyEscape
'        .Keys = vbKeyPageUp
'        .Keys = vbKeyPageDown
'        .Keys = vbKeyEnd
'        .Keys = vbKeyHome
'        .Keys = vbKeyUp
'        .Keys = vbKeyDown
'        .Keys = vbKeyDelete
'        .Keys = vbKeyTab Or vbShiftMask * &H10000
'        .Keys = vbKeyEnd Or vbAltMask * &H10000
'        .Keys = vbKeyHome Or vbAltMask * &H10000
'        .Keys = vbKeyG Or vbCtrlMask * &H10000
'        .Keys = vbKeyUp Or vbCtrlMask * &H10000
'        .Keys = vbKeyDown Or vbCtrlMask * &H10000
'        ' **PRESTO ** .Monitor = shkThisForm       'This Form
'        ' **PRESTO ** .Notify = shkPosted      'When Hooked
'        .Messages = WM_MOUSEMOVE
' '       .Messages = WM_LBUTTONDOWN
'        .Messages = WM_LBUTTONUP
'        .Messages = WM_RBUTTONDOWN
'        .HookEnabled = True
'    End With

   'ZZDebug.Print "LeaveBindLE"
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLE", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_Change(ByVal col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Desc:   Fires when data in the current cell is changed.  use this to
'           signal the data manager that it should handle data change.
' Params:   Index - 0 = Full Detail, 1 = Abbreviated
'           Col   - current column.
'           Row   - current row.
'***********************************************************************
'    moDmDetail.ProcessEventChange col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_Click(ByVal col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'           Select the row that the user Selected.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           Row, Col - The cell coordinates that were clicked.
'***********************************************************************
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick
    Dim bDirty      As Boolean

    
    bOldmbClick = mbDontChkclick
    mbDontChkclick = True
'    bDirty = moLEGridInvcDetl.IsDirty
    moLEGridInvcDetl.Grid_Click col, Row
'    If bDirty = False Then
'        moDmLineDist.SetDirty False
'        moDmDetail.SetDirty False
'    End If

    mbDontChkclick = bOldmbClick
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Allow user to resize column widths.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           Col1, Col2 - see Farpoint manual.
'************************************************************************
'    moLEGridInvcDetl.Grid_ColWidthChange Col1, Col2
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_ColWidthChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_DblClick(ByVal col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Initiate edit mode where the row contents can be changed.
'           Assumes a Click has preceeded this event.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           Row, Col - The cell coordinates that were clicked.
'************************************************************************
    moLEGridInvcDetl.Grid_DblClick col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_DragDrop(Source As Control, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           A drag-and-drop terminated here, check if the drop should
'           result in an actual movement of a dropped row.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           mlsourceRow - the row from where the drag was initiated.
'           source, x - not used.
'           y - the drop Position.  See VB Manual.
'************************************************************************
    moLEGridInvcDetl.Grid_DragDrop x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_DragDrop", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_DragOver(Source As Control, x As Single, y As Single, State As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Indicate whether drop is allowed or disallowed at this point.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           source, etc - See VB Manual.
'************************************************************************
    moLEGridInvcDetl.Grid_DragOver x, y, State
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_DragOver", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Provide keyboard interface.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           KeyCode, etc - See VB Manual.
'************************************************************************
    moLEGridInvcDetl.Grid_KeyDown KeyCode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_LeaveCell(ByVal col As Long, ByVal Row As Long, _
    ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moDmDetail.ProcessEventLeaveCell col, Row, NewCol, NewRow
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_LeaveRow(ByVal Row As Long, _
                                 ByVal RowWasLast As Boolean, _
                                 ByVal RowChanged As Boolean, _
                                 ByVal AllCellsHaveData As Boolean, _
                                 ByVal NewRow As Long, _
                                 ByVal NewRowIsLast As Long, _
                                Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moDmDetail.ProcessEventLeaveRow Row, NewRow

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_LeaveRow", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Determine if focus is lost to a edit control.  If so,
'           begin the edit or add mode process.
'************************************************************************
    moLEGridInvcDetl.Grid_LostFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_MouseDown(Button As Integer, _
                                  Shift As Integer, _
                                  x As Single, _
                                  y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Mark the x,y Position in preparation for drag-and-drop.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           Button, etc - See VB Manual.
'************************************************************************
    moLEGridInvcDetl.Grid_MouseDown Button, Shift, x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_MouseMove(Button As Integer, _
                                  Shift As Integer, _
                                  x As Single, _
                                  y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Check if mouse movement qualifies to be a drap-and-drop.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           Button, etc - See VB Manual.
'************************************************************************
    moLEGridInvcDetl.Grid_MouseMove Button, Shift, x, y
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_RightClick(ByVal ClickType As Integer, _
                                   ByVal col As Long, _
                                   ByVal Row As Long, _
                                   ByVal MouseX As Long, _
                                   ByVal MouseY As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Provide a context PopUp menu on right click.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           ClickType, Col, Row, MouseX, MouseY - see Spread manual.
'************************************************************************
 '   moLEGridInvcDetl.Grid_RightClick ClickType, Col, Row, MouseX, MouseY
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_RightClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub grdInvcDetl_TopLeftChange(ByVal OldLeft As Long, _
                                      ByVal OldTop As Long, _
                                      ByVal NewLeft As Long, _
                                      ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
'   Description:
'           Except for the real last Visible column, do not
'           dynamically alter the column widths.
'   Parameters:
'           Index - 0 = Full Detail, 1 = Abbreviated
'           OldLeft, OldTop, NewLeft, NewTop - see Farpoint manual.
'************************************************************************
'    moLEGridInvcDetl.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdInvcDetl_TopLeftChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonMenuClick(Button As String, ButtonMenu As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarMenuClick Button, ButtonMenu
'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonMenuClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBillTo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBillTo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommentDtl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCommentDtl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    moLEGridInvcDetl.GridEditChange txtCommentDtl
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtCommentDtl_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'RKL DEJ 2016-07-07
Private Sub txtCommentExt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCommentExt, True
    #End If
'+++ End Customizer Code Push +++

    txtComment.Text = Mid(Trim(txtCommentExt.Text), 1, 50)

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentExt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

'RKL DEJ 2016-07-07
Private Sub txtCommentExt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCommentExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentExt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

'RKL DEJ 2016-07-07
Private Sub txtCommentExt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCommentExt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentExt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

'RKL DEJ 2016-07-07
Private Sub txtCommentExt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCommentExt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentExt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub

Private Sub txtContName_LostFocus()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtContName, True
    #End If
'+++ End Customizer Code Push +++
On Error GoTo ExpectedErrorRoutine

Dim iHooktype As Integer
' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
' **PRESTO ** WinHookInvcEntry.HookEnabled = False

    moLostFocus.IsValidControl txtContName

' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype

'+++ VB/Rig Begin Pop +++
        Exit Sub
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "txtContName_LostFocus"
' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype
gClearSotaErr

Exit Sub


VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtContName_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidContact() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'*********************************************************************************
On Error GoTo ExpectedErrorRoutine
    Dim lErr        As Long
    Dim sErrDesc    As String
    Dim rs          As Object
    Dim bValid As Boolean
    
    IsValidContact = 1
    
    If txtContName.Text <> txtContName.Tag Then
           
        If Len(txtCustID.Text) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgAREnter0Before1, _
                sRemoveAmpersand(lblContact.Caption), sRemoveAmpersand(lblCustID.Caption)
            txtContName.Text = txtContName.Tag
            gbSetFocus Me, txtCustID
            IsValidContact = 0
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Else
            If Len(Trim(txtContName.Text)) = 0 Then
                moDmHeader.SetColumnValue "ConfirmToCntctKey", Empty
                IsValidContact = 1
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            Else
                bValid = gbValidCIContact(moClass.moFramework, moSotaObjects, _
                        moClass.moAppDB, rs, mbAllowContactAOF, (txtContName.Text), _
                        kEntTypeARCustomer, gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER), _
                        "", gvCheckNull(moDmBTAddress.GetColumnValue("CountryID")))
                If bValid Then
                    IsValidContact = 1
                    txtContName.Tag = txtContName.Text
                    moDmHeader.SetColumnValue "ConfirmToCntctKey", glGetValidLong(rs.Field("CntctKey"))
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgARBadField, sRemoveAmpersand(lblContact)
                    txtContName.Text = txtContName.Tag
                    gbSetFocus Me, txtContName
                    IsValidContact = 0
                End If
            End If
        End If
    End If
    Set rs = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
lErr = Err
sErrDesc = Err.Description
IsValidContact = 0
txtContName.Text = txtContName.Tag
Set rs = Nothing
MyErrMsg moClass, sErrDesc, lErr, sMyName, "IsValidContact"
gClearSotaErr

Exit Function


'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidContact", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtCustClass_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    KeyCode = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtCustClass_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtCustClass_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustClass, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
        KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtCustClass_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtDiscDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDiscDate, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtDiscDate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDiscDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Function IsValidDiscDate() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard date Validation
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

Dim bValid As Boolean       'Validation Flag
IsValidDiscDate = 1

    If Trim(txtDiscDate.Text) <> Trim(txtDiscDate.Tag) Then     'date Has Been Changed
        'Require valid date if there is a terms discount amount or if there is an entry in txtDiscDate.
        If (curTermsDiscAmt.Amount <> 0 Or Len(Trim(txtDiscDate.Text)) > 0) Then
          'Perform date validation
           bValid = gbCheckDate(Me, lblDiscDate, txtDiscDate, kDmSuccess, _
                tabInvcEntry, ktabIEHeader)
        
         'If Valid Set up Old Value otherwise reset Value back
          If Not bValid Then
              IsValidDiscDate = 0
              txtDiscDate.Text = txtDiscDate.Tag
          End If
        
          If (Year(Trim(txtDiscDate)) < 1900) Or (Year(Trim(txtDiscDate)) > 2100) Then
            txtDiscDate.Text = txtDiscDate.Tag
            IsValidDiscDate = 0
          End If
        
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidDiscDate"
mbValidating = False
txtDiscDate.Text = txtDiscDate.Tag
IsValidDiscDate = 0
gClearSotaErr

Exit Function


'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidDiscDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtDueDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDueDate, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtDueDate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDueDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub
Private Function IsValidDueDate() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard date Validation
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

Dim bValid As Boolean       'Validation Flag
IsValidDueDate = 1

    If Trim(txtDueDate.Text) <> Trim(txtDueDate.Tag) Then     'date Has Been Changed
        
      'Perform date validation
        bValid = gbCheckDate(Me, lblDueDate, txtDueDate, kDmSuccess, _
                tabInvcEntry, ktabIEHeader)
        
      'If Valid Set up Old Value otherwise reset Value back
        If Not bValid Then
            IsValidDueDate = 0
            txtDueDate.Text = txtDueDate.Tag
        End If
        
        If (Year(Trim(txtDueDate)) < 1900) Or (Year(Trim(txtDueDate)) > 2100) Then
            txtDueDate.Text = txtDueDate.Tag
            IsValidDueDate = 0
        End If
    
    End If
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidDueDate"
mbValidating = False
txtDueDate.Text = txtDueDate.Tag
IsValidDueDate = 0
gClearSotaErr

Exit Function


'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidDueDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtFOB_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtFOB, True
    #End If
'+++ End Customizer Code Push +++

    If IsValidFOB = 0 Then      ' invalid FOB
        gbSetFocus Me, txtFOB
    Else
        moDmHeader.SetColumnValue "FOBKey", IsValidFOB
        txtFOB.Tag = txtFOB.Text
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtFOB_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidFOB() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return Surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine

    IsValidFOB = gvCheckNull(moClass.moAppDB.Lookup("FOBKey", "tciFOB", _
            "FOBID = " & gsQuoted(txtFOB.Text)), SQL_INTEGER)
    
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidFOB"
IsValidFOB = 0
txtFOB.Text = txtFOB.Tag
gClearSotaErr
Set moValidate.oRS = Nothing

Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidFOB", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtInvcID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtInvcID, True
    #End If
'+++ End Customizer Code Push +++
'***************************************************************************************
' Desc: as Invoice id is changed it will disable/enable Invoice Type combo box
'***************************************************************************************
    
  'No Invoice Selected enable Invoice Type
    If Len(Trim$(txtInvcID.Text)) = 0 Then
        cboInvcType.Enabled = True
        cboInvcType.BackColor = vbWindowBackground
    Else
        cboInvcType.Enabled = False
        cboInvcType.BackColor = vbButtonFace
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtInvcID_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtInvcID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtInvcID, True
    #End If
'+++ End Customizer Code Push +++
    
    If Len(txtInvcID.Text) = 0 And _
       Not (Me.ActiveControl Is navMain Or _
       Me.ActiveControl Is cboInvcType) Then
        gbSetFocus Me, txtInvcID
        Exit Sub
    End If
    
    moLostFocus.IsValidControl txtInvcID

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtInvcID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtInvoiceForm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtInvoiceForm, True
    #End If
'+++ End Customizer Code Push +++
    moLostFocus.IsValidControl txtInvoiceForm
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = moValidate.iHooktype

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtInvoiceForm_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function IsValidInvoiceForm() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'*********************************************************************************
' Desc: Run Standard Validation against the database return surrogate key
'********************************************************************************
On Error GoTo ExpectedErrorRoutine
 
    
    Set moValidate.oDmForm = moDmHeader
    moValidate.iTab = ktabIEHeader

    IsValidInvoiceForm = Abs(CInt(moValidate.CIBusinessForm(txtInvoiceForm, _
            lblInvoiceForm, "InvcFormKey", mbAllowBusFormAOF, True, 2, False)))
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function


'An error occurred reset Global Validating flag and Value
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidInvoiceForm"
IsValidInvoiceForm = 0
txtInvoiceForm.Text = txtInvoiceForm.Tag
gClearSotaErr

Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidInvoiceForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub txtItemDesc_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    KeyCode = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtItemDesc_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtItemDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    moLEGridInvcDetl.GridEditChange txtItemDesc
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtItemDesc_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtSalesperson_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    KeyCode = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSalesperson_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub txtSalesperson_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSalesperson, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSalesperson_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtShipMeth_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    KeyCode = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtShipMeth_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtShipMeth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipMeth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtShipMeth_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtShipTo_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    KeyCode = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtShipTo_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtShipTo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipTo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtShipTo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtSONum_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    KeyCode = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSONum_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtSONum_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSONum, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    KeyAscii = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtSONum_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub LESetDetailFocus(oLE As clsLineEntry, Optional col As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Double-clicking on any grid column will drive focus to the
'       detail control representing the Selected grid column.
' Parameters: oLE - the line entry class object calling this procedure.
'             Col - The column to set the focus into.
'***********************************************************************
    
    Dim Control As Control

    If IsMissing(col) Then
        col = kcolIEDefault
    End If
    
    'Select Case Col
       'Case Is = mlColMaskedGLAcctNo
       '     Set Control = txtGLAcctNo(miDetlType)

       'Case Is = mlColshortDesc
       '     Set Control = txtShortDesc(miDetlType)

       'Case Is = mlColExtAmt
       '     Set Control = curExtAmt(miDetlType)

       'Case Is = mlColDispExtComment
       '     Set Control = txtExtComment(miDetlType)

       'Case Else
       '     If mbUseMiscItem Then
       '         Set Control = txtItemID
       '     Else
       '         Set Control = txtGLAcctNo(kiAbbrDetl)
       '     End If
    'End Select
    
    On Error Resume Next
'    Debug.Print "lesetdtlfocus"
    If Not Control Is Nothing Then
        gbSetFocus Me, Control
    End If
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
'Exit this subroutine
    Exit Sub

'+++ VB/Rig Begin Pop +++
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LESetDetailFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub LEDetailToGrid(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: If any detail entry controls need to be manually linked to
'       the grid after entry, then do it here.
' Parameters: oLE - the line entry class object calling this procedure.
'***********************************************************************
    Dim lRow                    As Long     'current row

  'Find out what row of the grid we're on
    lRow = moLEGridInvcDetl.ActiveRow
  'UnitCost
    gGridUpdateCell grdInvcDetl, lRow, kcolIEUnitCostView, Format$(curUnitCost.Amount)
    gGridUpdateCell grdInvcDetl, lRow, kcolIEUnitCost, Format$(muInvcDtl.UnitCost)
    
    gGridUpdateCell grdInvcDetl, lRow, kcolIEDescription, txtItemDesc.Text
        
    'Intellisol start
    Dim JobLineMode As JobLineModeE
    Dim iCurLineMode As Integer
    
    lRow = oLE.ActiveRow
    
    If (Len(Trim$(lkuProject.Text)) > 0) Then
        '-- Update the invoice detail grid columns
        gGridUpdateCell grdInvcDetl, lRow, kColProjectID, lkuProject.Text
        gGridUpdateCell grdInvcDetl, lRow, kColProjectKey, CStr(lkuProject.KeyValue)
        gGridUpdateCell grdInvcDetl, lRow, kColPhaseID, lkuPhase.Text
        gGridUpdateCell grdInvcDetl, lRow, kColPhaseKey, CStr(lkuPhase.KeyValue)
        gGridUpdateCell grdInvcDetl, lRow, kColTaskID, lkuTask.Text
        gGridUpdateCell grdInvcDetl, lRow, kColTaskKey, CStr(lkuTask.KeyValue)
        gGridUpdateCell grdInvcDetl, lRow, kColEstSale, curEstSale.Amount
        gGridUpdateCell grdInvcDetl, lRow, kColActSale, curActSale.Amount
        gGridUpdateCell grdInvcDetl, lRow, kColInvDesc, lkuInvDesc.Text
        gGridUpdateCell grdInvcDetl, lRow, kColInvDescKey, CStr(lkuInvDesc.KeyValue)
        
        '-- Update the job grid columns
        With moProject
            gGridUpdateCell grdJobLine, .lJobGridRow, kColProjectIDJ, lkuProject.Text
            gGridUpdateCell grdJobLine, .lJobGridRow, kColProjectKeyJ, CStr(lkuProject.KeyValue)
            gGridUpdateCell grdJobLine, .lJobGridRow, kColPhaseIDJ, lkuPhase.Text
            gGridUpdateCell grdJobLine, .lJobGridRow, kColPhaseKeyJ, CStr(lkuPhase.KeyValue)
            gGridUpdateCell grdJobLine, .lJobGridRow, kColTaskIDJ, lkuTask.Text
            gGridUpdateCell grdJobLine, .lJobGridRow, kColTaskKeyJ, CStr(lkuTask.KeyValue)
            gGridUpdateCell grdJobLine, .lJobGridRow, kColJobLineKeyJ, _
                CStr(glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kColJobLineKey)))
            gGridUpdateCell grdJobLine, .lJobGridRow, kColDocLineKeyJ, _
                CStr(glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIEInvoiceLineKey)))
            gGridUpdateCell grdJobLine, .lJobGridRow, kColEstSaleJ, curEstSale.Amount
            gGridUpdateCell grdJobLine, .lJobGridRow, kColActSaleJ, curActSale.Amount
            gGridUpdateCell grdJobLine, .lJobGridRow, kColInvDescJ, lkuInvDesc.Text
            gGridUpdateCell grdJobLine, .lJobGridRow, kColInvDescKeyJ, CStr(lkuInvDesc.KeyValue)
                
            If (moProject.iJobGridState = kGridAdd) Then
                JobLineMode = LINE_INSERT
                moProject.AppendJobGridRow
            Else
                JobLineMode = LINE_UPDATE
            End If
            
            '-- Don't overwrite Insert flag with Update flag
            iCurLineMode = giGetValidInt(gsGridReadCell(grdJobLine, .lJobGridRow, kColAddUpdDelJ))
            If (iCurLineMode <> LINE_INSERT) Or (JobLineMode = LINE_DELETE) Then
                gGridUpdateCell grdJobLine, .lJobGridRow, kColAddUpdDelJ, CStr(JobLineMode)
            End If
        End With
    End If
    'Intellisol end
                
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEDetailToGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub LEGridToDetail(oLE As clsLineEntry)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: If any grid columns need to be manually linked to the detail
'       entry controls, then do it here.
' Parameters: oLE - the line entry class object calling this procedure.
'***********************************************************************
    
    Dim lRow        As Long         'current Row
    Dim lDistRow    As Long         'current distribution row
    Dim sExtAmt     As String       'Extension Amount
    Dim sGLAcctKey  As String       'G/L Account Key
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick
    Dim sVal        As String       'String Value
    Dim iLenHCS     As Integer      'Length of the Home Currency Symbol
    Dim lUnitMeasKey    As Long

    
  'Get Grid Active Row
    lRow = moLEGridInvcDetl.ActiveRow
    lDistRow = grdInvcLineDist.ActiveRow
    
    If grdInvcDetl.ActiveRow < 0 Then       'Invoice had no details
        txtItemDesc.Enabled = False
        txtCommentDtl.Enabled = False
        txtCommentDtl.BackColor = vbButtonFace
        cmdDtl(0).Enabled = False
        cmdDtl(1).Enabled = False
        Exit Sub
    End If
 
    iLenHCS = Len(msHomeCurrSymbol)
 
   'Load Invoice Detail structure
    With muInvcDtl
      'Invoice Key
        .InvcKey = gvCheckNull(moDmDetail.GetColumnValue(lRow, "InvcKey"), SQL_INTEGER)
      'Invoice Line Key
        .InvoiceLineKey = gvCheckNull(moDmDetail.GetColumnValue(lRow, "InvoiceLineKey"), SQL_INTEGER)
      'Comment Only
        .CmntOnly = giGetValidInt(gsGridReadCell(grdInvcDetl, lRow, kcolIECmntOnly))
      'Commission base
        .CommBase = giGetValidInt(gsGridReadCell(grdInvcDetl, lRow, kcolIECommBase))
      'Item Surrogate Key
        .ItemKey = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIEItemKey))
      'G/L Account Key
        .AcctKey = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESalesAcctKey))
      'sequence Number
        .SeqNo = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESeqNo))
      'Unit Price Override
        .UnitPriceOvrd = giGetValidInt(gsGridReadCell(grdInvcDetl, lRow, kcolIEUnitPriceOvrd))
      'Item is Subject to trade Discount
        .SubjToTradeDisc = giGetValidInt(gsGridReadCell(grdInvcDetl, lRow, kcolIESubjToTradeDisc))
      'Shipping Method Key and ID
        .ShipMethID = gsGridReadCell(grdInvcDetl, lRow, kcolIEShipMethID)
        txtShipMethDetl.Text = .ShipMethID
      'Shipping Zone key and ID
        .ShipZoneID = gsGridReadCell(grdInvcDetl, lRow, kcolIEShipZoneID)
      'FOB ID
        .FOBID = gsGridReadCell(grdInvcDetl, lRow, kcolIEFOBID)
        txtFOBDetl.Text = .FOBID
    End With

    
  'Set Global don't Check click Variable
    bOldmbClick = mbDontChkclick
    mbDontChkclick = True
  
  'Load UOM
    txtUOM.Text = gsGridReadCell(grdInvcDetl, lRow, kcolIEUnitMeasID)
    txtUOM.Tag = txtUOM.Text
    
  'Load Commission Class
    If muARDflts.UseSper Then
        txtCommClassDtl.Text = gsGridReadCell(grdInvcDetl, lRow, kcolIECommClassID)
        txtCommClassDtl.Tag = txtCommClassDtl.Text
    End If
  'Load Commission Plan
    If muARDflts.UseSper Then
        txtCommPlanDtl.Text = gsGridReadCell(grdInvcDetl, lRow, kcolIECommPlanID)
        txtCommPlanDtl.Tag = txtCommPlanDtl.Text
    End If
    
  'Load Sales Tax Class Combo Box
    If muARDflts.TrackSTaxOnSales Then
        txtSTaxClass.Text = gsGridReadCell(grdInvcDetl, lRow, kcolIESTaxClassID)
        txtSTaxClass.Tag = txtSTaxClass.Text
    End If
    
   'Part is not Subject to trade discounts disable trade Discount fields
    If muInvcDtl.SubjToTradeDisc = 0 And TxtItemID <> "" Then
      
      'Set amounts & values to 0
        curTradeDiscAmt.Amount = 0
        numTradeDiscPct.Value = 0
        curTradeDiscAmt.Tag = 0
        numTradeDiscPct.Tag = 0
      
      'Make controls view only
        curTradeDiscAmt.Protected = True
        numTradeDiscPct.Protected = True
    
    Else
        
      'Load unbound Trade Discount Percent Sage MAS 500 Number Control
        numTradeDiscPct.Value = Val(gsGridReadCell(grdInvcDetl, lRow, kcolIETradeDiscPct))
        numTradeDiscPct.Tag = numTradeDiscPct.Value
        
    End If
        
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEUnitPrice))
    If IsNumeric(sVal) Then
        curUnitPrice.Amount = CDbl(sVal)
    Else
        curUnitPrice.Amount = 0
    End If
    
    mbDontChkclick = bOldmbClick
    
  'Load structure(bound) and Number/currency controls(unbound)
  'Because the bound numbers may be negative (if credit memo)
  'Quantity
    
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEQtyView))
    If IsNumeric(sVal) Then
        numQty.Value = CDbl(sVal)
    Else
        numQty.Value = 0
    End If
    numQty.Tag = numQty.Value
    
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEQty))
    If IsNumeric(sVal) Then
        muInvcDtl.Qty = CDbl(sVal)
    Else
        muInvcDtl.Qty = 0
    End If
  
  'Sales Amount
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIESalesAmtView))
    If IsNumeric(sVal) Then
        curSalesAmt.Amount = CDbl(sVal)
    Else
        curSalesAmt.Amount = 0
    End If
    curSalesAmt.Tag = curSalesAmt.Amount
    
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIESalesAmt))
    If IsNumeric(sVal) Then
        muInvcDtl.SalesAmt = CDbl(sVal)
    Else
        muInvcDtl.SalesAmt = 0
    End If
    
    'Unit Cost
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEUnitCost))
    If IsNumeric(sVal) Then
        muInvcDtl.UnitCost = CDbl(sVal)
    Else
        muInvcDtl.UnitCost = 0
    End If

  'Freight Amount
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEFreightAmtView))
    If IsNumeric(sVal) Then
        curFrtAmtDetl.Amount = CDbl(sVal)
    Else
        curFrtAmtDetl.Amount = 0
    End If
    curFrtAmtDetl.Tag = curFrtAmtDetl.Amount
    
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEFreightAmt))
    If IsNumeric(sVal) Then
        muInvcDtl.FreightAmt = CDbl(sVal)
    Else
        muInvcDtl.FreightAmt = 0
    End If
  
  'Trade Discount amount (set to 0 if Item not Subject to trade discounts)
    If muInvcDtl.SubjToTradeDisc = 0 And TxtItemID <> "" Then
        curTradeDiscAmt.Amount = 0
        curTradeDiscAmt.Tag = 0
        muInvcDtl.TradeDiscAmt = 0
    Else
        
        sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIETradeDiscAmtView))
        If IsNumeric(sVal) Then
            curTradeDiscAmt.Amount = CDbl(sVal)
        Else
            curTradeDiscAmt.Amount = 0
        End If
        curTradeDiscAmt.Tag = curTradeDiscAmt.Amount
        
        sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIETradeDiscAmt))
        If IsNumeric(sVal) Then
            muInvcDtl.TradeDiscAmt = CDbl(sVal)
        Else
            muInvcDtl.TradeDiscAmt = 0
        End If
        
'        curTradeDiscAmt.Amount = Val(gsGridReadCell(grdInvcDetl, lRow, kcolIETradeDiscAmt))
'        curTradeDiscAmt.Tag = curTradeDiscAmt.Amount
       
    End If
  
  'Original(calculated) commission amount
    'If iLenHCS = 0 Then
        sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEOrigCommAmtView))
    'Else
    '    sVal = Trim(Mid(gsGridReadCell(grdInvcDetl, lRow, kcolIEOrigCommAmtView), iLenHCS))
    'End If
    If IsNumeric(sVal) Then
        curOrigCommAmt.Amount = CDbl(sVal)
    Else
        curOrigCommAmt.Amount = 0
    End If
    curOrigCommAmt.Tag = curOrigCommAmt.Amount
    
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEOrigCommAmt))
    If IsNumeric(sVal) Then
        muInvcDtl.OrigCommAmt = CDbl(sVal)
    Else
        muInvcDtl.OrigCommAmt = 0
    End If
  
  'Actual Commission amount
    'If iLenHCS = 0 Then
        sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEActCommAmtView))
    'Else
    '    sVal = Trim(Mid(gsGridReadCell(grdInvcDetl, lRow, kcolIEActCommAmtView), iLenHCS))
    'End If
    If IsNumeric(sVal) Then
        curActCommAmt.Amount = CDbl(sVal)
    Else
        curActCommAmt.Amount = 0
    End If
    curActCommAmt.Tag = curActCommAmt.Amount
    
    sVal = Trim(gsGridReadCell(grdInvcDetl, lRow, kcolIEActCommAmt))
    If IsNumeric(sVal) Then
        muInvcDtl.ActCommAmt = CDbl(sVal)
    Else
        muInvcDtl.ActCommAmt = 0
    End If
    
    'Load taxes in hidden grid
    If muARDflts.TrackSTaxOnSales Then
        If muInvcDtl.CmntOnly = 0 Then
            muInvcDtl.STaxTranKey = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESTaxTranKey))
            If muInvcDtl.STaxTranKey = 0 Then
                curSTaxDetl.Amount = 0
                muInvcDtl.STaxAmt = 0
            Else
                'Intellisol start
'                moSTax.bLoadDistTaxes muInvcDtl.STaxTranKey, muInvcDtl.STaxClassKey, muInvcDtl.ShipMethKey, _
'                        numQty.Value, curSalesAmt.Amount, curFrtAmtDetl.Amount, moDmHeader.GetColumnValue("ShipToCustAddrKey")
                moSTax.bLoadDistTaxes muInvcDtl.STaxTranKey, muInvcDtl.STaxClassKey, muInvcDtl.ShipMethKey, _
                        numQty.Value, curSalesAmt.Amount, curFrtAmtDetl.Amount, moDmHeader.GetColumnValue("ShipToCustAddrKey"), _
                        glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kColProjectKey))
                'Intellisol end
                curSTaxDetl.Amount = moSTax.dLineTax
                muInvcDtl.STaxSchdKey = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kcolIESTaxSchdKey))
                muInvcDtl.STaxAmt = curSTaxDetl.Amount
            End If
        End If
    End If
    
    If muInvcDtl.CmntOnly Then
        txtItemDesc.Enabled = False
    Else
        txtItemDesc.Enabled = True
    End If
    
    txtCommentDtl.Enabled = True            'Comment
    txtCommentDtl.BackColor = vbWindowBackground
    
    'Intellisol start
    Dim sIDJ As String
    Dim lKeyJ As Long
    
    lRow = oLE.ActiveRow
    
    moProject.SetJobGridRow muInvcDtl.InvoiceLineKey
    
    sIDJ = gsGetValidStr(gsGridReadCell(grdInvcDetl, lRow, kColProjectID))
    lKeyJ = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kColProjectKey))
    lkuProject.SetTextAndKeyValue sIDJ, lKeyJ
    
    sIDJ = gsGetValidStr(gsGridReadCell(grdInvcDetl, lRow, kColPhaseID))
    lKeyJ = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kColPhaseKey))
    lkuPhase.SetTextAndKeyValue sIDJ, lKeyJ
    
    sIDJ = gsGetValidStr(gsGridReadCell(grdInvcDetl, lRow, kColTaskID))
    lKeyJ = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kColTaskKey))
    lkuTask.SetTextAndKeyValue sIDJ, lKeyJ
    lkuTask.RestrictClause = moProject.sGetTaskRestrict()
    
    curEstSale.Amount = gdGetValidDbl(gsGridReadCell(grdInvcDetl, lRow, kColEstSale))
    curActSale.Amount = gdGetValidDbl(gsGridReadCell(grdInvcDetl, lRow, kColActSale))

    sIDJ = gsGetValidStr(gsGridReadCell(grdInvcDetl, lRow, kColInvDesc))
    lKeyJ = glGetValidLong(gsGridReadCell(grdInvcDetl, lRow, kColInvDescKey))
    lkuInvDesc.SetTextAndKeyValue sIDJ, lKeyJ
    'Intellisol end
    
    '************************************************************************
    '************************************************************************
    'RKL DEJ 2016-10-10 (START)
    '************************************************************************
    '************************************************************************
    numSpreadTon.Amount = gdGetValidDbl(gsGridReadCell(grdInvcDetl, lRow, kcolSOSpreadTon))
    numSpreadHr.Amount = gdGetValidDbl(gsGridReadCell(grdInvcDetl, lRow, kcolSOSpreadHr))
    '************************************************************************
    '************************************************************************
    'RKL DEJ 2016-10-10 (STOP)
    '************************************************************************
    '************************************************************************
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LEGridToDetail", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With moContextMenu
        .BindGrid moLEGridInvcDetl, grdInvcDetl.hwnd
        .Bind "ARINVCDETL", grdInvcDetl.hwnd, kEntTypeARInvoiceDetl
        
        .Bind "ARINVOICE", txtInvcID.hwnd, kEntTypeARInvoice
        
        .Bind "PAPROJECT", lkuProject.hwnd, kEntTypePAProject
        .Bind "PAPHASE", lkuPhase.hwnd, kEntTypePAPhase
        
        .Bind "GLDA01", txtSalesAcct.hwnd, kEntTypeGLAccount
        .Bind "ARDA04", txtCustID.hwnd, kEntTypeARCustomer
        .Bind "ARDA02", txtCustClass.hwnd, kEntTypeARCustClass
        .Bind "ARDA03", txtSalesperson.hwnd, kEntTypeARSalesperson
        .Bind "IMDA06", TxtItemID.hwnd, kEntTypeIMItem
        
        Set .Form = frmInvcEntry
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cmdBillToAddr_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdBillToAddr, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'*************************************************************************
'  This will show the bill to Address form
'*************************************************************************
    Dim iHooktype As Integer
    
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    
    If moDmHeader.State = kDmStateNone Then
        Exit Sub
    End If
    
    If moLostFocus.IsValidDirtyCheck() = 0 Then
        Exit Sub
    End If
    
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False

'force Validation for previous control
    If gbSetFocus(Me, cmdBillToAddr) Then
        LoadAddrForm moDmBTAddress
    End If
    
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdBillToAddr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub cmdShipToAddr_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdShipToAddr, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'*************************************************************************
'  This will show the Ship To Address form
'*************************************************************************
    Dim iHooktype As Integer
    
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    
    If moDmHeader.State = kDmStateNone Then
        Exit Sub
    End If
    
    If moLostFocus.IsValidDirtyCheck() = 0 Then
        Exit Sub
    End If
    
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False
    
  'force Validation for previous control
    If gbSetFocus(Me, cmdShipToAddr) Then
        LoadAddrForm moDmSTAddress
    End If
    
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdShipToAddr_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub LoadMaskedControl(iMaxChars As Integer, MskCtl As Control, TxtCtl As Control)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**************************************************************************
' Desc:     Loads masked tran id
' Parms:    iMaxChars - Maximum Number of characters
'           MskCtl    - masked Control to load
'           TxtCtl    - Bound Textcontrol to load from
'**************************************************************************
    Dim iLoop       As Integer      'Looping Variable
    Dim iNumChars   As Integer      'Number of characters in mask
    Dim bNotNum     As Integer      'flag all characters are not numeric
  
  'Get Number of edit characters in mask
    For iLoop = 1 To Len(MskCtl.Mask)    'Loop Through each character in mask
        
        If Mid$(MskCtl.Mask, iLoop, 1) = "N" Then    'Check for edit character
            iNumChars = iNumChars + 1                       'Increment Counter
        End If
    
    Next iLoop
    
  
  'No Edit characters found so assume no mask - Set To max Characters
    If iNumChars = 0 Then
        iNumChars = iMaxChars              'Cust id = varchar(12)
    End If
    
    
  'Check to see if there are any nonnumeric characters in this
    For iLoop = 1 To Len(TxtCtl.Text)      'Loop through each character
        
        If InStr("0123456789", Mid(TxtCtl.Text, iLoop, 1)) = 0 Then  'if a non-numeric character
            
            bNotNum = True                          'Set not numeric flag
        
        End If
    
    Next iLoop
    
    
    If bNotNum Then     'String was numeric
       MskCtl.Text = TxtCtl.Text
    Else
      'Truncate viewable Invoice id (on Left) for ID
       MskCtl.Text = Right(TxtCtl.Text, iNumChars)
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadMaskedControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function sGetTranTypeText(lTranType As Long) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:    Returns local text for Transaction Type fom Invoice Type listbox
' Parms:   iTranType - tran Type text
' returns: Local Text For tran Type from the Invoice Type list box
'*************************************************************************

Dim iLoop As Integer  'Looping variable
    
  'initialize to ""
    sGetTranTypeText = ""
    
  'Loop Through the Invoice Type and get the text for the transaction Type
    For iLoop = 0 To cboInvcType.ListCount - 1
        If lTranType = cboInvcType.ItemData(iLoop) Then
            sGetTranTypeText = cboInvcType.List(iLoop)
        End If
    Next iLoop
    
    If Len(sGetTranTypeText) = 0 Then
      'initialize to unknown
        sGetTranTypeText = gsBuildString(kUnknown, moClass.moAppDB, moClass.moSysSession)
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetTranTypeText", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function sGetTranStatusText(iTranStatus As Integer) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:    Returns local text for Transaction Status fom database or
'          From Status Collection if posted
' Parms:   iTranStatus - Database Value for Status
' returns: Local Text For tran Status from tarInvoiceLog
'*************************************************************************
    
    Dim sSQL As String  'SQL String
    Dim rs   As New DASRecordSet  'SOTADAS recordset object
  

  'Status comes from tciBatchLog
  'Setup query to get tciBatchLog Status From The static List and local string tables
    sSQL = "#smGetStaticListLocalText;" & kSQuote
    sSQL = sSQL & "tarInvoiceLog" & kSQuote & ";" & kSQuote
    sSQL = sSQL & "TranStatus" & kSQuote & ";" & iTranStatus
    sSQL = sSQL & ";" & moClass.moSysSession.Language & ";"

  'Run Query to Get batch Status(days or months)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If rs.IsEOF Then
        sGetTranStatusText = gsBuildString(kUnknown, moClass.moAppDB, moClass.moSysSession)
    Else
        sGetTranStatusText = gvCheckNull(rs.Field("LocalText"))
    End If

    Set rs = Nothing    'Clear out recordset object

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetTranStatusText", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub SetCommentOnlyDtlCtls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'******************************************************************************
' Clear out detail controls for comment only items (except Item id, key
' Invoice key, entry and sequence numbers and comment
'******************************************************************************
    Dim bOldmbClick As Boolean      'Old mbdontcheck clcick

    With muInvcDtl
        .AcctKey = 0            'Set default account key to 0
        .SubjToTradeDisc = 0    'Set subj to trade disc = 0
        .Qty = 0                'Quantity Shipped
        .SalesAmt = 0           'Sales Amount (structure)
        .TradeDiscAmt = 0       'Trade Discount Amount
        .OrigCommAmt = 0        'Original commission amount
        .ActCommAmt = 0         'Commission Amount
        .CommBase = 1           'Commission base
        .UnitPriceOvrd = 0      'Unit Price override
     End With
    
        
    txtItemDesc.Text = ""                        'Blank out Item Description
    
    bOldmbClick = mbDontChkclick
    mbDontChkclick = True                        'Dont run Click event on combos
    
    numQty.Value = 0                        'Quantity Shipped
    txtUOM.Text = ""                        'Unit Of Measure
    curUnitPrice.Amount = 0                 'Unit Cost
    curSalesAmt.Amount = 0                  'Sales Amount (control)
    
    txtSTaxClass.Text = ""                  'Sales Tax Class
    
    curTradeDiscAmt.Amount = 0              'Trade Discount Amount
    numTradeDiscPct.Value = 0               'Trade Discount Percent
    
    curUnitCost.Amount = 0                  'Unit Cost set to 0
    
    txtCommClassDtl.Text = ""               'Commission Class
    txtCommPlanDtl.Text = ""                'Commission Plan
    curOrigCommAmt.Amount = 0               'Original commission amount
    curActCommAmt.Amount = 0                'Commission Amount
                
    txtSalesAcct.Text = ""                  'Clear out Sales account
    
    mbDontChkclick = bOldmbClick            'reset Dont run Click event on combos

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetCommentOnlyDtlCtls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function bOverrideSecEvent(sEventId As String)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***************************************************************
' Can the user override the security event passed in
'***************************************************************
On Error GoTo ExpectedErrorRoutine
    
    Dim iHooktype   As Integer    'Hook Type
    Dim vPrompt     As Variant
    Dim sUser       As String
    
    bOverrideSecEvent = False   'Set initial return code
    
  'disable winhook for this form
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False

  'User is Authorized set good return code
  'Check the user id typed in can override (no cancel hit)
    sUser = CStr(moClass.moSysSession.UserId)
    vPrompt = True
    If moClass.moFramework.GetSecurityEventPerm(sEventId, sUser, vPrompt) <> 0 Then
        bOverrideSecEvent = True
    End If
      
  're-enable winhook for this form
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = True
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype
    

Exit Function
          

ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "bOverrideSecEvent"
' **PRESTO ** WinHookInvcEntry.HookEnabled = True
' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype
gClearSotaErr


Exit Function
'+++ VB/Rig Begin Pop +++

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bOverrideSecEvent", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function bCheckInvcCredit()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************
' returns kdmsuccess or kdmfailure based on credit limit on Hold Value
'**********************************************************************
    Dim dTranAmtHC As Double
    Dim lCustkey As Long
    Dim lInvcKey As Long
    Dim lPmtKey As Long
    Dim dOvrdAmt As Double
    Dim iCredHold As Integer
    Dim iCustHold As Integer
    Dim sTranDesc As String
    Dim dPmtAmtHC As Double

    bCheckInvcCredit = kDmSuccess
    
    dTranAmtHC = 0
    
    lCustkey = gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)
    lInvcKey = gvCheckNull(moDmHeader.GetColumnValue("InvcKey"), SQL_INTEGER)
    lPmtKey = 0
    dOvrdAmt = gvCheckNull(moDmHeader.GetColumnValue("AuthOvrdAmt"), SQL_INTEGER)
    iCredHold = 0
    sTranDesc = cboInvcType.Text
    dPmtAmtHC = 0
    
    
    If muCustDflts.Hold Then
        iCustHold = 1
    End If
    
    If moInvcCred Is Nothing Then
        Set moInvcCred = CreateObject("InvcCred.ClsInvcCred")
        moInvcCred.Init moClass.moSysSession, moClass.moAppDB, muCompanyDflts.msCompanyID, _
            mbEnterAsTab, moClass.moAppDB, moSotaObjects, moClass.moFramework
    End If
    
    moInvcCred.CurrSymbol = msHomeCurrSymbol
    moInvcCred.decplaces = miHomeDigAfterDec
    
    moInvcCred.CalcAdjBal dTranAmtHC, lCustkey, lInvcKey, lPmtKey, dPmtAmtHC, dOvrdAmt, _
            iCredHold, iCustHold, iGetTranType(), False, sTranDesc

    If iCredHold = 1 Then
        bCheckInvcCredit = kDmFailure
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCheckInvcCredit", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Sub LoadAddrForm(oDm As clsDmForm)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************
' Setup and show the Voucher Address form
'****************************************
    Dim sName As String
    Dim sA1 As String
    Dim sA2 As String
    Dim sA3 As String
    Dim sA4 As String
    Dim sA5 As String
    Dim sCity As String
    Dim sState As String
    Dim sZip As String
    Dim sCountry As String
    Dim dLatitude As Double
    Dim dLongitude As Double
    Dim sCaption As String
    Dim lAddrKey As Long
    Dim lDfltAddrKey As Long
    Dim bIsDirty As Boolean
    Dim iHooktype As Integer
    Dim sHold As String
    
    '-- Need to turn off the WinHook for the main form while the Address
    '-- form is loaded; WinHooks step on each other otherwise
    ' **PRESTO ** iHooktype = WinHook1.KeyboardHook
    ' **PRESTO ** WinHook1.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHook1.HookEnabled = False
    
    '-- Create the Address DLL if it does not exist
    If moAddr Is Nothing Then
        Set moAddr = CreateObject("addr.clsAddress")
        
        moAddr.Init moClass.moSysSession, moClass.moAppDB, muCompanyDflts.msCompanyID, mbEnterAsTab, _
            moClass.moAppDB, moSotaObjects, moClass.moFramework
    End If
    
    '-- Set up form caption and address name
    If oDm Is moDmBTAddress Then
        sCaption = gsBuildString(ksBillToAddress, moClass.moAppDB, moClass.moSysSession)
        sName = txtBillToName
        lDfltAddrKey = glGetValidLong(moDmHeader.GetColumnValue("BillToCustAddrKey"))
    Else
        sCaption = gsBuildString(ksShipToAddress, moClass.moAppDB, moClass.moSysSession)
        sName = txtShipToName
        lDfltAddrKey = glGetValidLong(moDmHeader.GetColumnValue("ShipToCustAddrKey"))
    End If
    
    '-- Setup the remaining fields from the data manager object
    With oDm
        lAddrKey = glGetValidLong(.GetColumnValue("AddrKey"))
        sA1 = gsGetValidStr(.GetColumnValue("AddrLine1"))
        sA2 = gsGetValidStr(.GetColumnValue("AddrLine2"))
        sA3 = gsGetValidStr(.GetColumnValue("AddrLine3"))
        sA4 = gsGetValidStr(.GetColumnValue("AddrLine4"))
        sA5 = gsGetValidStr(.GetColumnValue("AddrLine5"))
        sCity = gsGetValidStr(.GetColumnValue("City"))
        sState = gsGetValidStr(.GetColumnValue("StateID"))
        sCountry = gsGetValidStr(.GetColumnValue("CountryID"))
        sZip = gsGetValidStr(.GetColumnValue("PostalCode"))
        If Not .IsDirty Then
            sHold = gsGetValidStr(moClass.moAppDB.Lookup("Latitude", "tciAddress WITH (NOLOCK)", "AddrKey = " & CStr(lAddrKey)))
            dLatitude = IIf(sHold = "", -999, gdGetValidDbl(sHold))
            sHold = gsGetValidStr(moClass.moAppDB.Lookup("Longitude", "tciAddress WITH (NOLOCK)", "AddrKey = " & CStr(lAddrKey)))
            dLongitude = IIf(sHold = "", -999, gdGetValidDbl(sHold))
        Else
            dLatitude = IIf(gsGetValidStr(.GetColumnValue("Latitude")) = "", -999, gdGetValidDbl(.GetColumnValue("Latitude")))
            dLongitude = IIf(gsGetValidStr(.GetColumnValue("Longitude")) = "", -999, gdGetValidDbl(.GetColumnValue("Longitude")))
        End If
    End With
    
    '-- Save the address object's dirty state
    bIsDirty = oDm.IsDirty
    
    '-- Display the Address form
    moAddr.ShowAddr sName, sA1, sA2, sA3, sA4, sA5, _
        sCity, sState, sZip, sCountry, sCaption, , dLatitude, dLongitude

    '-- Assign address values from the address form
    If oDm Is moDmBTAddress Then
        txtBillToName = sName
    Else
        txtShipToName = sName
    End If
    
    If moAddr.bAddrChanged Then
       
        '-- Put address changes back into the data manager object
        With oDm
            If lAddrKey = lDfltAddrKey Then
                oDm.Action kDmCancel
                lAddrKey = glGetNextSurrogateKey(moClass.moAppDB, "tciAddress")
                .SetColumnValue "AddrKey", lAddrKey
                .KeyChange
                If oDm Is moDmBTAddress Then
                    moDmHeader.SetColumnValue "BillToAddrKey", lAddrKey
                Else
                    moDmHeader.SetColumnValue "ShipToAddrKey", lAddrKey
                End If
            End If
            .SetColumnValue "AddrLine1", sA1
            .SetColumnValue "AddrLine2", sA2
            .SetColumnValue "AddrLine3", sA3
            .SetColumnValue "AddrLine4", sA4
            .SetColumnValue "AddrLine5", sA5
            .SetColumnValue "City", sCity
            .SetColumnValue "StateID", sState
            .SetColumnValue "CountryID", sCountry
            .SetColumnValue "PostalCode", sZip
            .SetColumnValue "Latitude", IIf(dLatitude = -999, Null, dLatitude)
            .SetColumnValue "Longitude", IIf(dLongitude = -999, Null, dLongitude)
        End With
        
         '-- Assign address values from the address form
        If oDm Is moDmBTAddress Then
            txtBillToName = sName
        Else
            txtShipToName = sName
        End If
    
    End If

    '-- Need to turn the WinHook for the main form back on
    ' **PRESTO ** WinHook1.KeyboardHook = iHooktype
    ' **PRESTO ** WinHook1.HookEnabled = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadAddrForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub ShowCreditLimitForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'  Disable winhook, show form and enable the hook
'*********************************************************************

Dim iHooktype As Integer

  'disable winhook for this form
    ' **PRESTO ** iHooktype = WinHookInvcEntry.KeyboardHook
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = shkKbdDisabled
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = False

  'Show the credit Limit form
    moInvcCred.ShowMe (txtCustID.MaskedText), (lblCustName.Caption)
    
    DisplayStatus

  're-enable winhook for this form
    ' **PRESTO ** WinHookInvcEntry.HookEnabled = True
    ' **PRESTO ** WinHookInvcEntry.KeyboardHook = iHooktype

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowCreditLimitForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function iGetTranType() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************
'  Returns the transaction Type of the current record 0 if not present
'**********************************************************************
    If cboInvcType.ListIndex = kItemNotSelected Then
        iGetTranType = kTranTypeARIN
    Else
        iGetTranType = cboInvcType.ItemData(cboInvcType.ListIndex)
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetTranType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iGetGLOptions() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sWhere      As String
    Dim lAcctRefTitle   As Long

    sWhere = "CompanyID = " & gsQuoted(muCompanyDflts.msCompanyID)

    iGetGLOptions = gvCheckNull(moClass.moAppDB.Lookup("AcctRefUsage", "tglOptions", sWhere))
    
    If iGetGLOptions = 1 Or iGetGLOptions = 2 Then
        lAcctRefTitle = moClass.moAppDB.Lookup("AcctRefTitleStrNo", "tglOptions", "CompanyID =" & gsQuoted(muCompanyDflts.msCompanyID))
        msRefCodeLabel = moClass.moAppDB.Lookup("LocalText", "tsmLocalString", _
                            "StringNo = " & lAcctRefTitle & " AND LanguageID = " & moClass.moSysSession.Language)
    Else
        lblRef.Visible = False
        txtAcctRefCode.Visible = False
        msRefCodeLabel = ""
    End If
    
    lblRef.Caption = msRefCodeLabel

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetGLOptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function bSetFormCurrencyControls(sCurrID As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************************
' Desc: sets up the Currency Controls w decimal places & Currency Controls
'****************************************************************************
    Dim i               As Integer  'Integer
    Dim iDecPlaces      As Integer
    Static sLastCurrID  As String
    Dim uCurrInfo       As CurrencyInfo
    Dim Oldmb           As Boolean
    
    Oldmb = mbFormLoading
    mbFormLoading = True

  On Error GoTo ExpectedErrorRoutine
  
  'Currency Id same as last time do not change controls
    If sLastCurrID = sCurrID Then
        bSetFormCurrencyControls = True
        mbFormLoading = Oldmb
        Exit Function
    End If

    iDecPlaces = miDigAfterDec
    
  'Set Return Code
    bSetFormCurrencyControls = False
  
  'Get the Currency attributes for this Currency ID
  'sSQL = "SELECT * FROM tmcCurrency WHERE CurrID = %s"
    If sCurrID = muCompanyDflts.msDfltCurrency Then

      'Get the Currency attributes for this Currency ID
        msCurrSymbol = msHomeCurrSymbol
        miDigAfterDec = miHomeDigAfterDec
        miRoundPrec = miHomeRoundPrec
        miRoundMeth = miHomeRoundMeth
        mlCurrencyLocale = mlHomeCurrencyLocale
        
    Else
        
        If Not gbGetCurrInfo(moClass, sCurrID, uCurrInfo) Then
            msCurrSymbol = msHomeCurrSymbol
            miDigAfterDec = miHomeDigAfterDec
            miRoundPrec = miHomeRoundPrec
            miRoundMeth = miHomeRoundMeth
            mlCurrencyLocale = mlHomeCurrencyLocale
        Else
            mlCurrencyLocale = mlHomeCurrencyLocale
            miRoundPrec = uCurrInfo.iRoundPrecision
            miRoundMeth = uCurrInfo.iRoundMeth
            msCurrSymbol = uCurrInfo.sCurrSymbol
            miDigAfterDec = uCurrInfo.iDecPlaces
        End If
        
    End If
    
    
  'Setup form currency controls
  'Detail Line Controls
    lblDocCurrID.Caption = sCurrID
    
    gGridSetColumnType grdInvcDetl, kcolIESalesAmtView, _
                SS_CELL_TYPE_FLOAT, miDigAfterDec   'Grid sales amount
        grdInvcDetl.TypeFloatSeparator = True

    gGridSetColumnType grdInvcDetl, kcolIETradeDiscAmtView, _
                SS_CELL_TYPE_FLOAT, miDigAfterDec               'grid TD amt
        grdInvcDetl.TypeFloatSeparator = True

    
    gbSetCurrCtls moClass, sCurrID, uCurrInfo, curSalesAmt, curTradeDiscAmt, curTotalSalesAmt, _
        curFreightAmt, curTotTradeDiscAmt, curSalesTaxAmt, curSubTotal, curHandling, curRetntAmt, _
        curInvcTotal, curInvcAmt, curBalanceDue, curTermsDiscAmt, curCostOfSales
    
  'Set return Code
    bSetFormCurrencyControls = True
    sLastCurrID = sCurrID
    mbFormLoading = Oldmb

    Exit Function
    
ExpectedErrorRoutine:

MyErrMsg moClass, Err.Description, Err, sMyName, "bSetFormCurrencyControls"
gClearSotaErr
mbFormLoading = Oldmb
Exit Function
    

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetFormCurrencyControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iValidateInvoiceID() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
' Desc:     Validate the Invoice id is valid
' Returns:  0: Need to enter a valid transaction Type
'           1: Invoice Number not found, let them use it
'           2: Invoice Number found, need to load
'***********************************************************************

Dim iTranType       As Integer  'Transaction Type
Dim sMsgText        As String   'Message text temp until Sage MAS 500 message boxes
Dim lInvcKey        As Long     'Invoice Key
Dim lBatchKey       As Long     'batch Key
Dim lTranTypeOut    As Long     'Transaction Type
Dim iTranStatus     As Integer  'Transaction Status
Dim iRetVal         As Integer  'Transaction Status


  'Force the user to select a transaction Type
'    If cboInvcType.ListIndex = kItemNotSelected Then
'        MsgBox "You must select an Invoice Type"
'        gbSetFocus Me, cboInvcType
'        iValidateInvoiceID = 0
'        Exit Function
'    Else
        iTranType = iGetTranType()
'    End If
    
    
  'Run a stored procedure to validate the transaction Number chosen
    With moClass.moAppDB
        .SetInParam iTranType                   'Transaction Type
        .SetInParam (txtPaddedInvc.Text)        'Invoice Number
        If muARDflts.SameNoRangeForMemo Then
            .SetInParam 1                       'use Same Range for invoices & memos
        Else
            .SetInParam 0                       'Don't use Same Range for invoices & memos
        End If
        .SetInParam muCompanyDflts.msCompanyID  'Company
        .SetOutParam lInvcKey                   'Invoice Key
        .SetOutParam lBatchKey                  'batch Key
        .SetOutParam lTranTypeOut               'Transaction Type of record found
        .SetOutParam iTranStatus                'Transaction Status
        .SetOutParam iRetVal                    'Return Code
        .ExecuteSP ("sparGetValidInvoice")      'Run stored procedure
        lInvcKey = .GetOutParam(5)              'Get Invoice Key
        lBatchKey = .GetOutParam(6)             'Get batch Key
        lTranTypeOut = .GetOutParam(7)          'Get Transaction Type
        iTranStatus = .GetOutParam(8)           'Get Transaction Status
        iRetVal = .GetOutParam(9)               'Get Return Code
        .ReleaseParams                          'Get Release parameters
    End With


    Select Case iRetVal
        
      'An unexpected error occurred in our stored procedure
        Case 0
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                    "spValidateARInvoice 0"
        
      'This is a new Invoice set Invoice key and run key change
        Case 1
            'No record was found
      'An Invoice/Credit Memo/Debit Memo was found validate further
        Case 2
            
          'Check if transaction is in correct batch (or no batch assigned)
            If lBatchKey <> 0 Then
                
             'User must use same range for memo/invoices and this Number exists
              'for a different Type ask user if they wish to load the existing row
                If muARDflts.SameNoRangeForMemo And _
                   iTranType <> lTranTypeOut Then
                    cboInvcType.ListIndex = _
                            giListIndexFromItemData(cboInvcType, lTranTypeOut)
                    iValidateInvoiceID = 1
                Else
                    iValidateInvoiceID = 1
                End If
            
            End If
            
      'An unexpected error occurred in our stored procedure
        Case Else
            
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                    "spValidateARInvoice " & iRetVal
    
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iValidateInvoiceID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bGetDefaults() As Boolean
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
    
    Dim iStat               As Integer
    Dim sTitle              As String
    Dim iUseMultiCurr       As Integer
    Dim iTrackSTaxOnSales   As Integer
    Dim iPrintInvcs         As Integer
    Dim iUseSper            As Integer
    Dim iChkCreditLimit     As Integer
    Dim iInclTradeDiscInSls As Integer
    Dim iSameNoRangeForMemo As Integer
    Dim iBatchOvrdSales     As Integer
    Dim iClassOvrdSales     As Integer
    Dim lCurrencyLocale     As Long
    Dim iRetVal             As Integer
    Dim iAcctAdd            As Integer
    Dim iDecPlaces          As Integer
    Dim iWarnForUnapplPmt   As Integer
    Dim uCurrInfo           As CurrencyInfo
    Dim lLocale             As Long
    Dim iUseNatlAccts       As Integer
    

    muCompanyDflts.msCompanyID = moClass.moSysSession.CompanyID
    muCompanyDflts.mvBusinessDate = moClass.moSysSession.BusinessDate
    muCompanyDflts.mlLanguage = moClass.moSysSession.Language
    muCompanyDflts.msDfltCurrency = moClass.moSysSession.CurrencyID

    'sSQL = "SELECT * FROM tarOptions WHERE CompanyID = %s"
    With moClass.moAppDB
        .SetInParam muCompanyDflts.msCompanyID
        .SetInParam muCompanyDflts.msDfltCurrency
        .SetInParam muCompanyDflts.mlLanguage
        .SetOutParam iUseMultiCurr
        .SetOutParam iTrackSTaxOnSales
        .SetOutParam iPrintInvcs
        .SetOutParam iUseSper
        .SetOutParam iChkCreditLimit
        .SetOutParam iInclTradeDiscInSls
        .SetOutParam iSameNoRangeForMemo
        .SetOutParam iBatchOvrdSales
        .SetOutParam iClassOvrdSales
        .SetOutParam iDecPlaces
        .SetOutParam iDecPlaces
        .SetOutParam iDecPlaces
        .SetOutParam muGLDflts.AcctMask
        .SetOutParam iAcctAdd
        .SetOutParam frmCommissions.sCommType1
        .SetOutParam frmCommissions.sCommType2
        .SetOutParam msHomeCurrSymbol
        .SetOutParam miHomeDigAfterDec
        .SetOutParam miHomeRoundPrec
        .SetOutParam miHomeRoundMeth
        .SetOutParam lCurrencyLocale
        .SetOutParam iWarnForUnapplPmt
        .SetOutParam iUseNatlAccts
        .SetOutParam iRetVal
        .ExecuteSP ("sparGetInvoiceOptions")
        iUseMultiCurr = gvCheckNull(.GetOutParam(4), SQL_INTEGER)
        iTrackSTaxOnSales = gvCheckNull(.GetOutParam(5), SQL_INTEGER)
        iPrintInvcs = gvCheckNull(.GetOutParam(6), SQL_INTEGER)
        iUseSper = gvCheckNull(.GetOutParam(7), SQL_INTEGER)
        iChkCreditLimit = gvCheckNull(.GetOutParam(8), SQL_INTEGER)
        iInclTradeDiscInSls = gvCheckNull(.GetOutParam(9), SQL_INTEGER)
        iSameNoRangeForMemo = gvCheckNull(.GetOutParam(10), SQL_INTEGER)
        iBatchOvrdSales = gvCheckNull(.GetOutParam(11), SQL_INTEGER)
        iClassOvrdSales = gvCheckNull(.GetOutParam(12), SQL_INTEGER)
        muIMDflts.UnitCostDecPlaces = gvCheckNull(.GetOutParam(13), SQL_INTEGER)
        muIMDflts.UnitPriceDecPlaces = gvCheckNull(.GetOutParam(14), SQL_INTEGER)
        muIMDflts.QtyDecPlaces = gvCheckNull(.GetOutParam(15), SQL_INTEGER)
        muGLDflts.AcctMask = gvCheckNull(.GetOutParam(16))
        iAcctAdd = gvCheckNull(.GetOutParam(17), SQL_INTEGER)
        frmCommissions.sCommType1 = gvCheckNull(.GetOutParam(18))
        frmCommissions.sCommType2 = gvCheckNull(.GetOutParam(19))
        msHomeCurrSymbol = gvCheckNull(.GetOutParam(20))
        miHomeDigAfterDec = gvCheckNull(.GetOutParam(21), SQL_INTEGER)
        miHomeRoundPrec = gvCheckNull(.GetOutParam(22), SQL_INTEGER)
        miHomeRoundMeth = gvCheckNull(.GetOutParam(23), SQL_INTEGER)
        lCurrencyLocale = gvCheckNull(.GetOutParam(24), SQL_INTEGER)
        iWarnForUnapplPmt = gvCheckNull(.GetOutParam(25), SQL_INTEGER)
        iUseNatlAccts = giGetValidInt(.GetOutParam(26))
        iRetVal = gvCheckNull(.GetOutParam(27), SQL_INTEGER)
        .ReleaseParams
    End With
    
  'No AR Options Record Exists - Send a Message to the User
    If iRetVal = 1000 Then
       ' mbUnload = True
        giSotaMsgBox Me, moClass.moSysSession, kmsgNeedAROptionRec
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    msCurrSymbol = msHomeCurrSymbol
    miDigAfterDec = miHomeDigAfterDec
    miRoundPrec = miHomeRoundPrec
    miRoundMeth = miHomeRoundMeth
    mlHomeCurrencyLocale = lCurrencyLocale
    
    gbSetCurrCtls moClass, muCompanyDflts.msDfltCurrency, uCurrInfo, _
        curOrigCommAmt, curActCommAmt, curUnitPrice, curBatchTotal, curUnitCost
    
    bSetFormCurrencyControls muCompanyDflts.msDfltCurrency
   
  'Set up number controls
    lLocale = moClass.moSysSession.CountryLocale
 
    numTotTradeDiscPct.Locale = lLocale
    numTradeDiscPct.Locale = lLocale
    numRetntPct.Locale = lLocale
    numQty.Locale = lLocale
    
    numTradeDiscPct.DecimalPlaces = 2
    numTotTradeDiscPct.DecimalPlaces = 2
    numRetntPct.DecimalPlaces = 2
    
    curUnitCost.DecimalPlaces = muIMDflts.UnitCostDecPlaces
    curUnitPrice.DecimalPlaces = muIMDflts.UnitPriceDecPlaces
    numQty.DecimalPlaces = muIMDflts.QtyDecPlaces
    
    
  'Setup Home Currency Controls(Commissions)
    gGridSetColumnType grdInvcDetl, kcolIEOrigCommAmtView, _
                SS_CELL_TYPE_FLOAT, miHomeDigAfterDec
    grdInvcDetl.TypeFloatSeparator = True

    gGridSetColumnType grdInvcDetl, kcolIEActCommAmtView, _
                SS_CELL_TYPE_FLOAT, miHomeDigAfterDec
    grdInvcDetl.TypeFloatSeparator = True
    
  'Setup the Status bar
         
  'Turn Off MultiCurrency
    If iUseMultiCurr = 0 Then
        muARDflts.UseMultCurr = False
        lblDocCurrID.Visible = False
        cmdCurrency.Visible = False
        curOrigCommAmt.ShowCurrency = False
        curActCommAmt.ShowCurrency = False
        lblCurrIDHC(1).Visible = False
    Else
        muARDflts.UseMultCurr = True
        lblCurrIDHC(0).Caption = muCompanyDflts.msDfltCurrency
        lblCurrIDHC(1).Caption = muCompanyDflts.msDfltCurrency
        gbSetCurrCtls moClass, muCompanyDflts.msDfltCurrency, uCurrInfo, curTranAmtHC
        lblUnitCostCurrID.Caption = muCompanyDflts.msDfltCurrency
        lblOrigAmtCurrID.Caption = muCompanyDflts.msDfltCurrency
        lblActAmtCurrID.Caption = muCompanyDflts.msDfltCurrency
    End If
    
  'No Sales Taxes on Finance Charges
    If iTrackSTaxOnSales = 0 Then
        muARDflts.TrackSTaxOnSales = False
        tabInvcDetail.TabEnabled(ktabIESalesTax) = False
        cmdSalesTax.Enabled = False
    Else
        muARDflts.TrackSTaxOnSales = True
        tabInvcDetail.TabEnabled(ktabIESalesTax) = True
        cmdSalesTax.Enabled = True
        frmInvcEntry.curSalesTaxAmt.Protected = True
        frmInvcEntry.curSalesTaxAmt.Border = 0
        Set moSTax = Nothing
        Set moSTax = CreateObject("cizdadl1.clsciSTaxes")
        moSTax.Init moClass.moSysSession, moClass.moAppDB, kModuleAR, False, moClass.moAppDB, _
                    moSotaObjects, moClass.moFramework
    End If

    If iPrintInvcs = 0 Then
        muARDflts.PrintInvcs = False
        txtInvoiceForm.Enabled = False
        txtInvoiceForm.BackColor = vbButtonFace
        navInvoiceForm.Enabled = False
    Else
        muARDflts.PrintInvcs = True
    End If

    If iUseSper = 0 Then
        muARDflts.UseSper = False
        tabInvcDetail.TabEnabled(ktabIECommission) = False
        cmdComms.Enabled = False
    Else
        muARDflts.UseSper = True
    End If
        
    If iChkCreditLimit = 0 Then
        muARDflts.ChkCreditLimit = False
    Else
        muARDflts.ChkCreditLimit = True
    End If
                
    If iInclTradeDiscInSls = 0 Then
        muARDflts.InclTradeDiscInSls = False
        frmCommissions.bIncludeTradeDisInSls = False
    Else
        muARDflts.InclTradeDiscInSls = True
        frmCommissions.bIncludeTradeDisInSls = True
    End If
        
    If iSameNoRangeForMemo = 0 Then
        muARDflts.SameNoRangeForMemo = False
    Else
        muARDflts.SameNoRangeForMemo = True
    End If
    
    If iBatchOvrdSales = 0 Then
        muARDflts.BatchSalesOvrd = False
    Else
        muARDflts.BatchSalesOvrd = True
    End If
     
    If iClassOvrdSales = 0 Then
        muARDflts.ClassSalesOvrd = False
    Else
        muARDflts.ClassSalesOvrd = True
    End If

    
    muGLDflts.AllowAcctAOF = False
    mbAllowGLAccountAOF = False
    
    If iAcctAdd = 0 Then
        muGLDflts.AllowAcctAdd = False
    Else
        muGLDflts.AllowAcctAdd = True
    End If

    muARDflts.UseNatlAccts = (iUseNatlAccts = 1)

    txtSalesAcct.Mask = muGLDflts.AcctMask
    txtSalesAcctLoad.Mask = txtSalesAcct.Mask
    
    'Intellisol start
    '-- Determine if PA is active
    mbIntPAActive = gbIsModuleActive(moClass, kModulePA)

    If mbIntPAActive Then
        tabInvcDetail.TabVisible(kiProjectTab) = True
        tabInvcDetail.Tab = kiProjectTab
    Else
        tabInvcDetail.TabVisible(kiProjectTab) = False
    End If
    'Intellisol end

Exit Function

ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "bGetDefaults"
gClearSotaErr

Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetDefaults", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function VMIsValidKey() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    VMIsValidKey = IsValidInvoice
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function VMIsValidControl(oCtl As Control) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case True
        Case oCtl Is cboInvcType
            VMIsValidControl = IsValidInvcType
        Case oCtl Is txtInvcID
            VMIsValidControl = IsValidInvoice
        Case oCtl Is txtContName
            VMIsValidControl = IsValidContact
        Case oCtl Is txtDueDate
            VMIsValidControl = IsValidDueDate
        Case oCtl Is txtDiscDate
            VMIsValidControl = IsValidDiscDate
        Case oCtl Is txtInvoiceForm
            VMIsValidControl = IsValidInvoiceForm
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function IsValidInvcType() As Integer
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
    
    IsValidInvcType = 1
  'this is the only place you can losefocus to when invctype is Enabled
    If Me.ActiveControl Is txtInvcID Or _
        Me.ActiveControl Is navMain Then
    
    Else
        gbSetFocus Me, cboInvcType
    End If


'+++ VB/Rig Begin Pop +++
        Exit Function
ExpectedErrorRoutine:
MyErrMsg moClass, Err.Description, Err, sMyName, "IsValidInvcType"
IsValidInvcType = 0
gClearSotaErr

Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidInvcType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function bSetupNavMain() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim sWhere As String

    bSetupNavMain = False
    
    If Len(Trim(navMain.Tag)) = 0 Then
    
        sWhere = "CompanyID = " & gsQuoted(moClass.moSysSession.CompanyID)
    
        bSetupNavMain = gbLookupInit(navMain, moClass, moClass.moAppDB, "InvoiceWithCustomer", sWhere)
navMain.ColumnMasks = ""
        
        If bSetupNavMain Then
            navMain.Tag = 1
        End If
        
    Else
    
        bSetupNavMain = True
        
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupNavMain", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function CheckNationalAcct(lCustkey As Long) As Boolean
'****************************************************************************************
'Check if customer uses national acct
'****************************************************************************************

  Dim iRet     As Integer
  
  CheckNationalAcct = False

  iRet = giGetValidInt(moClass.moAppDB.Lookup("1", "tarOptions O, tarCustomer C ", _
        "O.CompanyID = C.CompanyID AND O.UseNationalAccts = 1 AND C.NationalAcctLevelKey <> NULL AND C.CustKey = " & lCustkey))
        
  If iRet = 1 Then
    CheckNationalAcct = True
  End If
  
End Function

Public Sub CMAddDrillMenu(oCtl As Object, wFlags As Long, lTaskID As Long)
    Dim lItemKey As Long
    Dim lCustkey As Long
    
    If oCtl Is txtCustID Then
        If lTaskID = ktskARMaintNatlAccts Then
            lCustkey = gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)
            If CheckNationalAcct(lCustkey) = False Then
                wFlags = MF_GRAYED
            End If
        End If
    End If
    
End Sub

Public Sub CMMemoSelected(oCtl As Control)
'Called from  HandleToolBarClick
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lCustkey As Long
Dim lInvcKey As Long
Dim oToolbarMenu As clsToolbarMenuInfo
    
Me.Enabled = False

lCustkey = gvCheckNull(moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)
lInvcKey = gvCheckNull(moDmHeader.GetColumnValue("InvcKey"), SQL_INTEGER)

    If oCtl Is txtInvcID Then
      
        gLaunchMemo Me, moClass.moFramework, moSotaObjects, kEntTypeARInvoice, lInvcKey, _
                    "Invoice:", txtInvcID.Text, moClass.moSysSession.BusinessDate
        
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeARInvoice)
        oToolbarMenu.lKey2 = lInvcKey
        
        gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain   ' Set in case change in memos
    End If

    If oCtl Is txtCustID Then
        'Abort if empty
        If Len(Trim(txtCustID)) = 0 Then Exit Sub
        
        gLaunchMemo Me, moClass.moFramework, moSotaObjects, kEntTypeARCustomer, lCustkey, _
                    txtCustID, lblCustName, moClass.moSysSession.BusinessDate
        
        Set oToolbarMenu = m_MenuInfo.Item(kTbMemo & "K" & kEntTypeARCustomer)
        oToolbarMenu.lKey2 = lCustkey
        
        gSetMemoToolBarStates moClass.moAppDB, m_MenuInfo, tbrMain   ' Set in case change in memos
    End If
    
Me.Enabled = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        Me.Enabled = True
        gSetSotaErr Err, sMyName, "CMMemoSelected", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler
                Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next
    
    Select Case True
        Case ctl Is txtInvcID
            ETWhereClause = "InvcKey = " & _
                            CStr(glGetValidLong(moDmHeader.GetColumnValue("InvcKey")))
        Case ctl Is grdInvcDetl
            ETWhereClause = "InvoiceLineKey = " & _
                            CStr(glGetValidLong(moDmDetail.GetColumnValue(moLEGridInvcDetl.ActiveRow, "InvoiceLineKey")))
        
        Case Else
            ETWhereClause = ""
    End Select
    
    Err.Clear
    
End Function

Private Function sLookupRDF(sInvcNo, sCustID) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Given an invoice ID and customer ID, looks up Project accounting .rdf file to use for drill down.
    Dim rs As DASRecordSet
'
' Intellisol PA Integration. Remove Proamics PA SP's
'
'    With moClass.moAppDB
'        On Error Resume Next
'        Set rs = .OpenRecordset("{Call int_GetInvoiceFormat (" & gsQuoted(sInvcNo) & " ," & gsQuoted(sCustID) & ")} ", kSnapshot, kOptionNone)
'        If Err.Number <> 0 Or rs.IsEmpty Then
    '            MsgBox "There is no default report definition file for this Proamics invoice.  Drill down operation canceled.", vbOKOnly + vbExclamation, "Sage 500 ERP"
'            Exit Function
'        Else
'            sLookupRDF = rs.Field(0)
'        End If
'        rs.Close
'        Set rs = Nothing
'    End With

    sLookupRDF = ""

'+++ VB/Rig Begin Pop +++
  Exit Function

VBRigErrorRoutine:
  gSetSotaErr Err, sMyName, "sLookupRDF", VBRIG_IS_FORM
  Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
      Err.Raise guSotaErr.Number
    Case Else
      Call giErrorHandler: Exit Function
  End Select
'+++ VB/Rig End +++
    
End Function
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmInvcEntry"
End Function
#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            'Before we initialize, set the custom tab to invisible.
            'The initialize will make visible if needed.
            'Pass into the initialize the name of the tab control and the index of the custom tab
            tabInvcDetail.TabVisible(ktabIESubCustomizer) = False
            tabInvcDetail.TabsPerRow = tabInvcDetail.Tabs - 1
            tabInvcEntry.TabVisible(ktabIECustomizer) = False
            tabInvcEntry.TabsPerRow = tabInvcEntry.TabsPerRow - 1
            
            moFormCust.Initialize Me, goClass, tabInvcDetail.Name & ";" & ktabIESubCustomizer, _
                                               tabInvcEntry.Name & ";" & ktabIECustomizer
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyDataBindings moDmHeader
            moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub cmdSTaxDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSTaxDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSTaxDetl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSTaxDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSTaxDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSTaxDetl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDtl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDtl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDtl_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDtl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdBillToAddr_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdBillToAddr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdBillToAddr_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdBillToAddr_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdBillToAddr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdBillToAddr_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdShipToAddr_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdShipToAddr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdShipToAddr_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdShipToAddr_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdShipToAddr, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdShipToAddr_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCurrency_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCurrency, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCurrency_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCurrency_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCurrency, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCurrency_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComms_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdComms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComms_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdComms_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdComms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdComms_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreditLimit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCreditLimit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdCreditLimit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdCreditLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdCreditLimit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdFreight_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdFreight, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdFreight_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdFreight_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdFreight, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdFreight_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSalesTax_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSalesTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSalesTax_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSalesTax_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSalesTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSalesTax_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtPONum_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPONum, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPONum_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPONum_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvcID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtInvcID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvcID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvcID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtInvcID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvcID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedCust_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPaddedCust, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedCust_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedCust_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPaddedCust, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedCust_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedCust_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPaddedCust, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedCust_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedCust_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPaddedCust, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedCust_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedInvc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPaddedInvc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedInvc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedInvc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPaddedInvc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedInvc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedInvc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPaddedInvc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedInvc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPaddedInvc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPaddedInvc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPaddedInvc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSONum_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSONum_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSONum_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSONum_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSONum_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSONum, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSONum_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSalesAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSalesAcct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSalesAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSalesAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAcctRefCode_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtAcctRefCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAcctRefCode_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAcctRefCode_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtAcctRefCode, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAcctRefCode_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAcctRefCode_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtAcctRefCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAcctRefCode_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtAcctRefCode_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtAcctRefCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtAcctRefCode_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommClassDtl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCommClassDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommClassDtl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommClassDtl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCommClassDtl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommClassDtl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommClassDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCommClassDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommClassDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommClassDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCommClassDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommClassDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlanDtl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCommPlanDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlanDtl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlanDtl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCommPlanDtl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlanDtl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlanDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCommPlanDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlanDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlanDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCommPlanDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlanDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSTaxClass_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSTaxClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSTaxClass_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSTaxClass_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSTaxClass, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSTaxClass_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSTaxClass_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSTaxClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSTaxClass_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSTaxClass_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSTaxClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSTaxClass_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommentDtl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentDtl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommentDtl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentDtl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommentDtl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCommentDtl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommentDtl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMethDetl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipMethDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMethDetl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMethDetl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipMethDetl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMethDetl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMethDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipMethDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMethDetl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMethDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipMethDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMethDetl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOBDetl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtFOBDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOBDetl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOBDetl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtFOBDetl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOBDetl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOBDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtFOBDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOBDetl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOBDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtFOBDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOBDetl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtUOM, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtUOM_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtUOM, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtUOM_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcctLoad_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSalesAcctLoad, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcctLoad_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcctLoad_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtSalesAcctLoad, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcctLoad_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcctLoad_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSalesAcctLoad, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcctLoad_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesAcctLoad_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSalesAcctLoad, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesAcctLoad_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TxtItemID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange TxtItemID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TxtItemID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TxtItemID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress TxtItemID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TxtItemID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TxtItemID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus TxtItemID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TxtItemID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub TxtItemID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus TxtItemID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "TxtItemID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemIdLoad_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtItemIdLoad, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemIdLoad_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemIdLoad_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtItemIdLoad, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemIdLoad_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemIdLoad_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtItemIdLoad, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemIdLoad_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtItemIdLoad_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtItemIdLoad, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtItemIdLoad_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTerms_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPmtTerms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTerms_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTerms_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPmtTerms, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTerms_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTerms_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPmtTerms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTerms_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTerms_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPmtTerms, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTerms_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtContName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtContName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtContName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustClass_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustClass_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustClass_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustClass_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustClass_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustClass, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustClass_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipTo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipTo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipTo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipTo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipTo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipTo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipToName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipToName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipToName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipToName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtShipToName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipToName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipToName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipToName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipToName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipToName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipToName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipToName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillToName_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBillToName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillToName_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillToName_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBillToName, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillToName_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillToName_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBillToName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillToName_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillToName_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBillToName, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillToName_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBillTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillTo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBillTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillTo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBillTo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBillTo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBillTo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtComment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtComment_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtComment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtComment_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceForm_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtInvoiceForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceForm_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceForm_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtInvoiceForm, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceForm_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvoiceForm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtInvoiceForm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvoiceForm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlan_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCommPlan, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlan_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlan_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCommPlan, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlan_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlan_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCommPlan, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlan_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCommPlan_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCommPlan, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCommPlan_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesperson_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtSalesperson, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesperson_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesperson_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtSalesperson, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesperson_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtSalesperson_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtSalesperson, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtSalesperson_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRecurInvcID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtRecurInvcID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRecurInvcID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRecurInvcID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtRecurInvcID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRecurInvcID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRecurInvcID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtRecurInvcID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRecurInvcID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtRecurInvcID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtRecurInvcID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtRecurInvcID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOB_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtFOB, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOB_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOB_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtFOB, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOB_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtFOB_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtFOB, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtFOB_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMeth_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtShipMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMeth_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMeth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtShipMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMeth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtShipMeth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtShipMeth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtShipMeth_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatch_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatch_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatch_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatch, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatch_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatch_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatch_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatch_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatch_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPostDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPostDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPostDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPostDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPostDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPostDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCustID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCustID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCustID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCustID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuPhase, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuPhase, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuPhase, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuPhase_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuPhase, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuPhase_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuTask, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuTask, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuTask, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTask_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuTask, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTask_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuInvDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuInvDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuInvDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuInvDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuInvDesc, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuInvDesc, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuInvDesc_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuInvDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuInvDesc_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuProject, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuProject_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuProject_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuMainProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuMainProject, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuMainProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuMainProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuMainProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuMainProject, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuMainProject_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuMainProject, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuMainProject_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curUnitCost_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curUnitCost, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitCost_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitCost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitCost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitCost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curUnitCost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitCost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTradeDiscAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curTradeDiscAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTradeDiscAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTradeDiscAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curTradeDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTradeDiscAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTradeDiscAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curTradeDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTradeDiscAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTradeDiscPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTradeDiscPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTradeDiscPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTradeDiscPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTradeDiscPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTradeDiscPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActCommAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curActCommAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActCommAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActCommAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curActCommAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActCommAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActCommAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curActCommAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActCommAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActCommAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curActCommAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActCommAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCommAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curOrigCommAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCommAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCommAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curOrigCommAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCommAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCommAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curOrigCommAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCommAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curOrigCommAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curOrigCommAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curOrigCommAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSTaxDetl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curSTaxDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSTaxDetl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSTaxDetl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curSTaxDetl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSTaxDetl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSTaxDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curSTaxDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSTaxDetl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSTaxDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curSTaxDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSTaxDetl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFrtAmtDetl_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curFrtAmtDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFrtAmtDetl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFrtAmtDetl_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curFrtAmtDetl, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFrtAmtDetl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFrtAmtDetl_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curFrtAmtDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFrtAmtDetl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFrtAmtDetl_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curFrtAmtDetl, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFrtAmtDetl_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curEstSale_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curEstSale, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curEstSale_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curEstSale_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curEstSale, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curEstSale_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curEstSale_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curEstSale, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curEstSale_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curEstSale_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curEstSale, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curEstSale_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActSale_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curActSale, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActSale_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActSale_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curActSale, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActSale_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActSale_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curActSale, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActSale_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curActSale_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curActSale, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curActSale_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curSalesAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curSalesAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curSalesAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curSalesAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitPrice_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curUnitPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitPrice_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitPrice_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curUnitPrice, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitPrice_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitPrice_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curUnitPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitPrice_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curUnitPrice_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curUnitPrice, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curUnitPrice_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numQty_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numQty_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numQty_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numQty, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numQty_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numQty_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numQty_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numQty_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numQty, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numQty_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curBatchTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curBatchTotal, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curBatchTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curBatchTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmtHC_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curTranAmtHC, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmtHC_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmtHC_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curTranAmtHC, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmtHC_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmtHC_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curTranAmtHC, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmtHC_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTranAmtHC_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curTranAmtHC, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTranAmtHC_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalanceDue_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curBalanceDue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalanceDue_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalanceDue_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curBalanceDue, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalanceDue_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalanceDue_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curBalanceDue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalanceDue_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBalanceDue_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curBalanceDue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBalanceDue_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTermsDiscAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curTermsDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTermsDiscAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTermsDiscAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curTermsDiscAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTermsDiscAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTermsDiscAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curTermsDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTermsDiscAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTermsDiscAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curTermsDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTermsDiscAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curInvcAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curInvcAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curInvcAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curInvcAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTotTradeDiscPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numTotTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTotTradeDiscPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTotTradeDiscPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numTotTradeDiscPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTotTradeDiscPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTotTradeDiscPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numTotTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTotTradeDiscPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numTotTradeDiscPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numTotTradeDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numTotTradeDiscPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curHandling_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curHandling, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curHandling_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curHandling_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curHandling, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curHandling_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curHandling_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curHandling, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curHandling_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curHandling_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curHandling, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curHandling_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRetntPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numRetntPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRetntPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRetntPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numRetntPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRetntPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRetntPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numRetntPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRetntPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numRetntPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numRetntPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numRetntPct_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcTotal_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curInvcTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcTotal_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcTotal_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curInvcTotal, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcTotal_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcTotal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curInvcTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcTotal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curInvcTotal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curInvcTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curInvcTotal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTotTradeDiscAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curTotTradeDiscAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTotTradeDiscAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTotTradeDiscAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curTotTradeDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTotTradeDiscAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTotTradeDiscAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curTotTradeDiscAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTotTradeDiscAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curRetntAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curRetntAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curRetntAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curRetntAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curRetntAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curRetntAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curRetntAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curRetntAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curRetntAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curRetntAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curRetntAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curRetntAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSubTotal_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curSubTotal, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSubTotal_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSubTotal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curSubTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSubTotal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSubTotal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curSubTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSubTotal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesTaxAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curSalesTaxAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesTaxAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesTaxAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curSalesTaxAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesTaxAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curSalesTaxAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curSalesTaxAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curSalesTaxAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFreightAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curFreightAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFreightAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFreightAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curFreightAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFreightAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curFreightAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curFreightAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curFreightAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTotalSalesAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curTotalSalesAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTotalSalesAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTotalSalesAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curTotalSalesAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTotalSalesAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curTotalSalesAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curTotalSalesAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curTotalSalesAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCostOfSales_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curCostOfSales, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCostOfSales_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCostOfSales_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curCostOfSales, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCostOfSales_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCostOfSales_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curCostOfSales, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCostOfSales_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curCostOfSales_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curCostOfSales, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curCostOfSales_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInDispute_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkInDispute, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInDispute_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInDispute_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInDispute, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInDispute_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInDispute_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInDispute, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInDispute_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboInvcType_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboInvcType, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboInvcType_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboInvcType_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboInvcType, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboInvcType_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReasonCode_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboReasonCode, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReasonCode_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReasonCode_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboReasonCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReasonCode_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReasonCode_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboReasonCode, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReasonCode_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDiscDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDiscDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDiscDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDiscDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDiscDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDiscDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDueDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDueDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDueDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDueDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDueDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDueDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDueDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDueDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDueDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvcDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtInvcDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvcDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvcDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtInvcDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvcDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvcDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtInvcDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvcDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtInvcDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtInvcDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtInvcDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If



















Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Sub ForceNavigation()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    txtInvcID_LostFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ForceNavigation", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ConfigureMemos()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim oToolbarMenu    As clsToolbarMenuInfo
    
    ' Invoices
    Set oToolbarMenu = New clsToolbarMenuInfo
    oToolbarMenu.CompanyID = moClass.moSysSession.CompanyID
    oToolbarMenu.lKey1 = kEntTypeARInvoice
    oToolbarMenu.lKey2 = 0
    oToolbarMenu.ToolbarButtonKey = kTbMemo
    oToolbarMenu.ToolbarMenuKey = "K" & kEntTypeARInvoice
    oToolbarMenu.MenuEnabled = True
    oToolbarMenu.MenuVisible = True
    oToolbarMenu.MenuText = "Invoice"
    m_MenuInfo.Add oToolbarMenu, oToolbarMenu.ToolbarButtonKey & oToolbarMenu.ToolbarMenuKey

    ' Customers
    Set oToolbarMenu = New clsToolbarMenuInfo
    oToolbarMenu.CompanyID = moClass.moSysSession.CompanyID
    oToolbarMenu.lKey1 = kEntTypeARCustomer
    oToolbarMenu.lKey2 = 0
    oToolbarMenu.ToolbarButtonKey = kTbMemo
    oToolbarMenu.ToolbarMenuKey = "K" & kEntTypeARCustomer
    oToolbarMenu.MenuEnabled = True
    oToolbarMenu.MenuVisible = True
    oToolbarMenu.MenuText = "Customer"
    m_MenuInfo.Add oToolbarMenu, oToolbarMenu.ToolbarButtonKey & oToolbarMenu.ToolbarMenuKey
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ConfigureMemos", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolbarMenuClick(ButtonConst As String, menu As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
'On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim oToolbarMenu   As clsToolbarMenuInfo
    
    Me.SetFocus
    
  ' See which key you were called from
    Select Case ButtonConst
        Case kTbMemo
            ' See which menu was selected
            Select Case menu
                Case "K" & kEntTypeARInvoice
                
                    ' Default Memo
                      Me.Enabled = False
                      CMMemoSelected txtInvcID
                      Me.Enabled = True
                
                Case "K" & kEntTypeARCustomer
            
                    ' Additional Memo
                      Me.Enabled = False
                      CMMemoSelected txtCustID
                      Me.Enabled = True
            
            End Select
            
        Case Else   'Unknown button
            'New Command Button not programmed for
            tbrMain.GenericHandler ButtonConst, Me, moDmHeader, moClass
    End Select
   

'+++ VB/Rig Begin Pop +++
        Exit Sub
Resume
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarMenuClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub OfficeInitialization()
'+++ VB/Rig Skip +++
On Error Resume Next
'*************************************************************************
'      Desc:  The Office class has been initialized elsewhere and has
'             already received this form�s reference. Therefore,
'             this method is additive if the client task wishes to add
'             clsDMForm and/or clsDMGrid references.
'     Parms:  N/A
'   Returns:  N/A
'************************************************************************
    With tbrMain.Office
        .AddFormObject moDmHeader, "Invoice"
        .AddFormObject moDmBTAddress, "BillToAddress"
        .AddFormObject moDmSTAddress, "ShipToAddress"
        .AddGridObject moDmDetail, "InvoiceLines"
        .AddGridObject moDmLineDist, "LineDist"
        .AddGridObject moDmComms, "Commissions"
    End With
    Err.Clear
End Sub


Private Sub CheckAvaTax()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Avatax Integration: Check the availability of Avatax for the CompanyID.
'***********************************************************************

On Error GoTo ExpectedErrorRoutine

    'Assume AvaTax feature not configured
    mbAvaTaxEnabled = False

    mbAvaTaxEnabled = gbGetValidBoolean(moClass.moAppDB.Lookup("AvaTaxEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(muCompanyDflts.msCompanyID)))

    If mbAvaTaxEnabled Then
        On Error GoTo ExpectedErrorRoutine_AvaTaxObj
        Set moAvaTax = Nothing
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'This is how we find out if AvaTax is not installed.
'The missing table (err 4050) will tell us not to fire the AvaTax code
'throughout the program.

    Select Case Err.Number
    
        Case 4050 ' SQL Object Not Found
            'tavConfiguration not found meaning AvaTax not installed.
            'No Error action required.

        Case Else
            'Unexpected Error reading tavConfiguration
            giSotaMsgBox Me, moClass.moSysSession, kmsgtavConfigurationError

    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine_AvaTaxObj:
'Expected Error:
'If the DB Avatax table was found, AvaTax is likely used for this Site.
'But when creating the local client object, the object was likely not installed
'This is not a good error, let the administrator know to check that AvaTax was properly implemented.

    giSotaMsgBox Me, moClass.moSysSession, kmsgAvaObjectError

    mbAvaTaxEnabled = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckAvaTax", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

