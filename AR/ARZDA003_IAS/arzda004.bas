Attribute VB_Name = "BasARSalesTax"
Option Explicit

'Const kColSTaxClassKey = 1
'Const kColOvrdSTaxExmpt = 2
'Const kColQtyShipped = 3
'Const kColUnitPrice = 4

Public mbDontRunGridClick       As Boolean
Public moSTax                   As Object
Public mlUniquekey              As Long
Public miDigAfterDec            As Integer
Public miRoundPrec              As Integer
Public mfrmMain                 As Form
Public mdSTaxTot                As Double
Public miRoundMeth              As Integer
Public msCurrSymbol             As String
Public mlCurrencyLocale         As Long

Public Const kColSTaxCode = 1
Public Const kColSTaxTxblSalesView = 2
Public Const kColSTaxCodeKey = 3
Public Const kColSTaxTxblSales = 4
Public Const kColSTaxTxblFrtView = 5
Public Const kColSTaxTxblFrt = 6
Public Const kColSTaxTxblSTaxView = 7
Public Const kColSTaxTxblSTax = 8
Public Const kColSTaxExmptAmtView = 9
Public Const kColSTaxExmptAmt = 10
Public Const kColSTaxTotSTaxView = 11
Public Const kColSTaxTotSTax = 12
Public Const kColSTaxExmpt = 13
Public Const kColSTaxExmptNo = 14

Public Const kLastSTaxCol = 14
Const VBRIG_MODULE_ID_STRING = "ARZDA004.BAS"

Public Function bRunSalesTax(Form As Form, moDmHeader As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim dFreight As Double
Dim lFreightTax As Long
Dim lCustkey As Long
Dim lSTaxKey As Long
Dim SSMKey As String
Dim iRetVal As Integer
   
    
    SSMKey = "ShipMethKey = " & gvCheckNull(Form.moDmHeader.GetColumnValue _
                                                  ("ShipMethKey"), SQL_INTEGER)
    
  'Get Freight tax key
    lFreightTax = gvCheckNull(Form.moClass.moAppDB.Lookup("STaxClassKey", _
                                    "tciShipMethod", SSMKey), SQL_INTEGER)

  'Get Customer key
    lCustkey = gvCheckNull(Form.moDmHeader.GetColumnValue("CustKey"), SQL_INTEGER)
   
  'Get Sales Tax Schedule Key
    lSTaxKey = gvCheckNull(Form.moDmHeader.GetColumnValue("STaxSchdKey"), SQL_INTEGER)
    
  'Get Freight Amount
    dFreight = Form.curFreightAmt.Amount
    
  'Create sales tax calculation object
    If moSTax Is Nothing Then
        Set moSTax = CreateObject("SalesTaxCalculation.ClsSTaxCalc")
    End If
    
  'Create a unique key(Session ID) for the session
    If moSTax.lUniqueKey = 0 Then
        If mlUniquekey = 0 Then
            mlUniquekey = lCreateKey()
        End If
        moSTax.lUniqueKey = mlUniquekey
    End If
    
    
'  'Calculate the sales taxes
'    mdSTaxTot = moSTax.gfARSTaxCalc(Form.grdInvcDetl, kcolIESTaxClassKey, kcolIEOvrdSTaxExmpt, _
'                kcolIESalesAmtView, kcolIETradeDiscAmtView, dFreight, lFreightTax, lSTaxKey, _
'                gvCheckNull(Form.moDmHeader.GetColumnValue("CurrID"), SQL_CHAR), _
'                Form.moClass.moAppDB, iRetVal)
'

  'Set global unique key, digits after decimals and rounding precision
    mlUniquekey = moSTax.lUniqueKey
    miDigAfterDec = moSTax.DigAfterDec
    miRoundPrec = moSTax.RoundPrec

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRunSalesTax", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Function lCreateKey() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'Description:
'   This function uses a stored procedure in the System Database to get a unique
'   long integer identifier for the given table.
'Return Value:
'   Long Integer Unique for table
'************************************************************************************
Dim lKey As Long
    With mfrmMain.moClass.moAppDB
        .SetInParam "tarSTaxWorkTable"
        .SetOutParam lKey
        .ExecuteSP ("spGetNextSurrogateKey")
        lKey = .GetOutParam(2)
        .ReleaseParams
    End With
    lCreateKey = lKey
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lCreateKey", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "BasARSalesTax"
End Function
