Attribute VB_Name = "basPrintInvoices"
'***********************************************************************
'     Name: basPrintInvoices
'     Desc: Main module of the Print/Reprint Invoices task
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 08/30/95 DSN
'     Mods: 11/13/96 KMKD
'***********************************************************************
Option Explicit

Public Const kAllTypes = -1

Public Const kSortTypeCust = 1
Public Const kSortTypeNum = 2
Public Const kSortCustType = 3
Public Const kSortNumType = 4
'EB-JMM 2/16/2018
Public Const kSortDateCust = 5
Public Const kSortDateNum = 6
Public Const kSortDateType = 7
Public Const kSortCustDate = 8
Public Const kSortNumDate = 9
Public Const kSortTypeDate = 10
'End EB-JMM 2/16/2018

Public Const ktskPrintInvoices = 84479299
Public Const ktskPrintInvoicesBatch = 84280296
Public Const ktskRePrintInvoices = 84479301

Public Const kQuickPrint = 1
Public Const kInvoicePrint = 2

Public Const kTransmitMethPrint = 0
Public Const kTransmitMethPreview = 1
Public Const kTransmitMethManual = 2

Public Const kSecChangeReprint = "CHANGERPRI"

'Variables for finding preview windows
Private TargetName As String
Private TargetHwnd As Long

 ' *** Used to find preview windows to hide before transmitting
Private Declare Function EnumWindows Lib "USER32.DLL" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Private Const WM_CLOSE = &H10

Const VBRIG_MODULE_ID_STRING = "ARZTC001.BAS"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basPrintInvoices"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, sPerEndDate, iPerNo, sPerYear, _
    Optional iFileType As Variant, _
    Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo badexit
    
    Dim bUI As Boolean
    Dim iInvcType As Integer
    Dim lSessionID As Long
    Dim sCompanyID As String
    Dim sSPName As String
    
    Dim iRetVal As Integer
    Dim lRetVal As Long
    Dim bRetVal As Boolean
    
    Dim SelectObj As clsSelection
    Dim ReportObj As clsReportEngine
    
    Dim iNumTablesUsed As Integer
    Dim sInsert As String
    Dim sSelect As String
    Dim sTableColumnName As String
    Dim sTablesUsed As String
    Dim sWhereClause As String
    Dim rs As DASRecordSet
    
    Dim iBusinessFormKey As Long
    Dim iCount As Integer
    Dim sBusinessFormID As String
    Dim sRptFileName As String
    Dim iUnitPriceDecPlaces As Integer
    Dim iQtyDecPlaces      As Integer
    Dim sPrintDest As String
    Dim iPrintCount         As Integer
    Dim iTransmitCount      As Integer
    Dim iTransmitOnlyCount  As Integer
    Dim iNoData             As Integer
    
    Dim sSQLQuery As String
  
    Dim sRestrictClause As String
    Dim sSQLClause As String
    Dim iNoOfErrors As Integer
    
    Dim lPreviewhWnd        As Long
    Dim bPreviewAndTransmit As Boolean
    Dim sOrigFormCaption    As String
    Dim sUniquePreviewCaption   As String
    
    'to determine if print is canceled
    Dim iPrinted As Boolean
    Dim bCancel As Boolean      'Variable to show if print was canceled at the print dialoge
    
    
    'Intellisol start
    Dim bBadInvcDesc As Boolean
    'Intellisol end
    
    Dim oSort As clsSort
    Set oSort = New clsSort
    
    'Initialize function return value.
    lStartReport = kFailure
    
    frm.CleanupWorkTables
    
    'Set local flag to indicate whether or not UI is visible.
    bUI = Not (bPeriodEnd Or (frm.iMode = kQuickPrint))
    
    If bUI Then
        ShowStatusBusy frm
    End If

    'Set references to selection class & report class.
    Set SelectObj = frm.moSelect
    Set ReportObj = frm.oreport
    
    'Set local vars to track session and company IDs.
    sCompanyID = frm.oClass.moSysSession.CompanyId
    lSessionID = frm.GetSessionID()
    
    '-- Store the form's original caption
    sOrigFormCaption = frm.Caption

    'Validate user-entered selection grid data.
    lRetVal = SelectObj.lValidateGrid(True)
    
    'Build WHERE clause for selection of keys into main work table:
    'Restrict to current company.
    SelectObj.AppendToWhereClause sWhereClause, frm.sInvcTable & ".CompanyID = '" & sCompanyID & kSQuote
        
    'Restrict to unprinted invoices if user chose "Only Unprinted Invoices" option.
    If frm.chkOnlyUnprinted Then
        SelectObj.AppendToWhereClause sWhereClause, "tarPendInvoice.Printed = 0"
    End If
    
    'Restrict to chosen invoice type.
    iInvcType = frm.cboInvcTypeToPrint.ItemData(frm.cboInvcTypeToPrint.ListIndex)
    If iInvcType = kAllTypes Then
        SelectObj.AppendToWhereClause sWhereClause, frm.sInvcTable & ".TranType IN (501,502,503,505)"
    Else
        SelectObj.AppendToWhereClause sWhereClause, frm.sInvcTable & ".TranType = " & iInvcType
    End If
    
    'Restrict according to selection grid choices.
    If Not (SelectObj.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)) Then
        Debug.Print "bGetWhereClause failed in lStartReport in basPrintInvoices"
    End If
    
    'Specify join keys for WHERE clause.
    SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustomer", frm.sInvcTable & ".CustKey = tarCustomer.CustKey"
    SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciBatchLog", frm.sInvcTable & ".BatchKey = tciBatchLog.BatchKey"
    SelectObj.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciBusinessForm", frm.sInvcTable & ".InvcFormKey = tciBusinessForm.BusinessFormKey"
    
    'Check validity of WHERE clause, prepending "WHERE" if necessary.
    If Not (SelectObj.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)) Then
        Debug.Print "bCheckWhereClause failed in lStartReport in basPrintInvoices"
        GoTo badexit
    End If
    
    'Intellisol start
    If InStr(sTablesUsed, "tPA00175") > 0 Then
        sTablesUsed = sTablesUsed & ",paarInvcLine WITH (NOLOCK), tarInvoiceDetl WITH (NOLOCK)"
        SelectObj.AppendToWhereClause sWhereClause, "paarInvcLine.intJobKey = tPA00175.intJobKey "
        SelectObj.AppendToWhereClause sWhereClause, "paarInvcLine.InvoiceLineKey=tarInvoiceDetl.InvoiceLineKey"
        SelectObj.AppendToWhereClause sWhereClause, "tarInvoiceDetl.InvcKey=" & frm.sInvcTable & ".InvcKey"
    End If
    'Intellisol end
    
    'If print invoice task is selected, include only pending invoices entered in Process Invoice and linked to Committed Shipment.
    If InStr(sTablesUsed, "tarPendInvoice") > 0 Then
        SelectObj.AppendToWhereClause sWhereClause, "tarPendInvoice.InvcKey IN (SELECT ivd.InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) JOIN tsoShipLine sl WITH (NOLOCK) ON ivd.ShipLineKey = sl.ShipLineKey JOIN tsoShipmentLog slog WITH (NOLOCK) ON sl.ShipKey = slog.ShipKey WHERE slog.TranStatus IN (3, 6) AND ivd.ShipLineKey IS NOT NULL UNION SELECT ivd.InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) WHERE ivd.ShipLineKey IS NULL)"
        SelectObj.AppendToWhereClause sWhereClause, "tarPendInvoice.BatchKey <> " & giGetValidInt(frm.oClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(sCompanyID)))
    End If
    
    'Build the SELECT clause of the INSERT statement
    sSelect = "SELECT DISTINCT " & frm.sInvcTable & ".InvcKey, " & _
        frm.sInvcTable & ".InvcFormKey, " & lSessionID & " FROM " & sTablesUsed
    sSelect = sSelect & " " & sWhereClause
    
    'Build INSERT statement and run against the application database.
    sInsert = "INSERT INTO tarPrintInvcHdrWrk (InvcKey, BusinessFormKey, SessionID) " & sSelect
    
    Debug.Print sInsert
    
    frm.oClass.moAppDB.ExecuteSQL sInsert
    
    'Check to see if any records with the current SessionID have been inserted into the work
    'table.  If none exist, bRecordsToPrintExist handles message to user (if last param True)
    'and transfers control to error handler with Err.Number = 0.
    If IsMissing(iFileType) Then
        If Not SelectObj.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", lSessionID, True) Then
            GoTo badexit
        End If
    End If
    
    'Choose appropriate stored procedure to populate work tables with remaining data.
    If ReportObj.TaskID = ktskRePrintInvoices Then
        sSPName = "spARPopInvcRePrintWrkTbls"
    Else
        sSPName = "spARPopInvcPrintWrkTbls"
    End If
    
    'If not period end or quick print, check for null business forms.  If found, check whether
    'all invoices have null forms.  If so, cancel processing.  If not, allow user to cancel
    'processing.
    If bUI Then
        iCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & Str(lSessionID) & " AND BusinessFormKey IS NULL")
        If iCount <> 0 Then
            iCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & Str(lSessionID) & " AND BusinessFormKey IS NOT NULL")
            If iCount = 0 Then
                MsgBox "None of the selected invoices have a specified form." & Chr(13) & "Report processing cancelled.", vbOKOnly + vbInformation, "Sage 500 ERP"
                GoTo badexit
            Else
                iRetVal = giSotaMsgBox(frm, frm.oClass.moSysSession, kmsgNullInvcForm)
                If iRetVal = kretCancel Then
                    GoTo badexit
                End If
            End If
        End If
    End If
    
    'Open recordset of business forms included.
    sSelect = "SELECT DISTINCT tarPrintInvcHdrWrk.BusinessFormKey, tciBusinessForm.PrintDest, " & _
            "tciBusinessForm.BusinessFormID FROM tarPrintInvcHdrWrk, tciBusinessForm " & _
            "WHERE tciBusinessForm.BusinessFormKey = tarPrintInvcHdrWrk.BusinessFormKey " & _
            "AND SessionID = " & Str(lSessionID)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSelect, kSnapshot, kOptionNone)
    
    frm.oreport.CleanupWorkTables
    
    'Loop through recordset.  Where Business Form is not null, look up filename of .rpt.
    'Clean up work tables.  Select keys of records for current business form into main
    'work table.  Execute stored procedure to populate work tables with remaining data.
    
    iNoOfErrors = 0
    If Not rs.IsEOF Then
        Do Until rs.IsEOF
            If Not IsNull(rs.Field("BusinessFormKey")) Then
                iBusinessFormKey = rs.Field("BusinessFormKey")
                sBusinessFormID = rs.Field("BusinessFormID")
                sRptFileName = frm.oClass.moAppDB.Lookup("RptName", "tciBusinessForm", _
                    "BusinessFormKey = " & Str(iBusinessFormKey) & "AND CompanyID = '" _
                    & sCompanyID & kSQuote)
                
                lSessionID = frm.GetSessionID()
                
                
                'Build INSERT statement and run against the application database.
                sInsert = "INSERT INTO tarPrintInvcHdrWrk (InvcKey, BusinessFormKey, SessionID) " _
                        & "SELECT DISTINCT " & frm.sInvcTable & ".InvcKey, " _
                        & frm.sInvcTable & ".InvcFormKey, " & lSessionID & " FROM " & sTablesUsed _
                        & " " & sWhereClause _
                        & " AND InvcFormKey = " & Str(iBusinessFormKey)
                
                Debug.Print sInsert
                
                frm.oClass.moAppDB.ExecuteSQL sInsert
                
                With frm.oClass.moAppDB
                    .SetInParam frm.oreport.SessionID
                    .SetInParam frm.oClass.moSysSession.CompanyId
                    .SetInParam frm.oClass.moSysSession.Language
                    .SetInParam frm.sInvcMsgLocal
                    .SetInParam frm.iInclInvcMsgCust
                    .SetInParam frm.iIncExtLineCmnt
                    .SetInParam frm.sReprintText
                    .SetOutParam lRetVal
                    On Error Resume Next
                    Debug.Print "Executing SP:  " & sSPName
                    .ExecuteSP sSPName
                End With
                    
                'Check for error in stored procedure.
                If (Err.Number = 0) Then
                    lRetVal = frm.oClass.moAppDB.GetOutParam(8)
                    'Intellisol start
                    bBadInvcDesc = (lRetVal = -99999)
                    'Intellisol end
                Else
                    Debug.Print "Error Executing SP:  " + sSPName
                    frm.oClass.moAppDB.ReleaseParams
                    GoTo badexit
                End If
                
                On Error GoTo badexit
                
                'Get printer for business for if specified
                If Not IsNull(rs.Field("PrintDest")) Then
                    sPrintDest = rs.Field("PrintDest")
                    ReportObj.PrinterName = sPrintDest
                End If
                            
                'Assign report file and set up report.
                ReportObj.ReportFileName() = sRptFileName
                If ReportObj.lSetupReport = kFailure Then
                    'Errors in lSetupReport display own messages.
                    GoTo badexit
                Else
                
                    'set standard formulas, business date, run time, company name etc.
                    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
                        GoTo badexit
                    End If
                
                    lRetVal = ReportObj.bSetReportFormula("UseMultiCurrency", frm.iUseMultiCurr)
                    If lRetVal = 0 Then
                        ReportObj.ShowCrystalError
                    End If
                    
                    'Set up ShowPmtApplOpt in the report
                    lRetVal = ReportObj.bSetReportFormula("ShowPmtApplOpt", frm.oClass.ShowPmtApplOpt)
                    If lRetVal = 0 Then
                        ReportObj.ShowCrystalError
                    End If
           
                    sSQLQuery = ReportObj.GetSQLQuery
                    sSQLQuery = sParse(sSQLQuery, "ORDER BY")
                    
                    Select Case frm.cboSortBy.ItemData(frm.cboSortBy.ListIndex)
                        Case kSortTypeCust
                            'Intellisol start
'                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk." & Chr$(34) & "TranType" & Chr$(34) & _
'                                " ASC,tarPrintInvcHdrWrk." & Chr$(34) & "CustID" & Chr$(34) & _
'                                " ASC, tarPrintInvcHdrWrk." & Chr$(34) & "TranNo" & Chr$(34) & _
'                                " ASC, tarPrintInvcDtlWrk." & Chr$(34) & "InvoiceLineKey" & Chr$(34) & "ASC"
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk." & Chr$(34) & "TranType" & Chr$(34) & _
                                " ASC,tarPrintInvcHdrWrk." & Chr$(34) & "CustID" & Chr$(34) & _
                                " ASC, tarPrintInvcHdrWrk." & Chr$(34) & "TranNo" & Chr$(34) & " ASC "
                            
                            oSort.lInitSort frm, frm.moDDData, 2
                            oSort.lInsSort "CustID", "tarPrintInvcHdrWrk", moDDData
                            ReportObj.lSetSortCriteria oSort
                            'Intellisol end
                        Case kSortTypeNum
                            'Intellisol start
'                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk." & Chr$(34) & "TranType" & Chr$(34) & _
'                                " ASC,tarPrintInvcHdrWrk." & Chr$(34) & "TranNo" & Chr$(34) & _
'                                " ASC, tarPrintInvcDtlWrk." & Chr$(34) & "InvoiceLineKey" & Chr$(34) & "ASC"
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk." & Chr$(34) & "TranType" & Chr$(34) & _
                                " ASC,tarPrintInvcHdrWrk." & Chr$(34) & "TranNo" & Chr$(34) & " ASC "
                            'Intellisol end
                        Case kSortCustType
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk." & Chr$(34) & "CustID" & Chr$(34) & _
                                " ASC,tarPrintInvcHdrWrk." & Chr$(34) & "TranType" & Chr$(34) & _
                                " ASC, tarPrintInvcHdrWrk." & Chr$(34) & "TranNo" & Chr$(34) & " ASC "
                        Case kSortNumType
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk." & Chr$(34) & "TranNo" & Chr$(34) & _
                                " ASC,tarPrintInvcHdrWrk." & Chr$(34) & "TranType" & Chr$(34) & " ASC "
                        Case kSortDateCust
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk.TranDate ASC, tarPrintInvcHdrWrk.CustID ASC "
                        Case kSortDateNum
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk.TranDate ASC, tarPrintInvcHdrWrk.TranNo ASC "
                        Case kSortDateType
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk.TranDate ASC, tarPrintInvcHdrWrk.TranType ASC "
                        Case kSortCustDate
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk.CustID, tarPrintInvcHdrWrk.TranDate ASC ASC "
                        Case kSortNumDate
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk.TranNo ASC, tarPrintInvcHdrWrk.TranDate ASC "
                        Case kSortTypeDate
                            sSQLQuery = sSQLQuery & " ORDER BY tarPrintInvcHdrWrk.TranType ASC, tarPrintInvcHdrWrk.TranDate ASC "
                    End Select
                    
                    'Intellisol start
                    'invoice description format
                    If InStr(sSQLQuery, "tarPrintInvcDtlWrk") > 0 Then
                        sSQLQuery = sSQLQuery & ", tarPrintInvcDtlWrk." & Chr$(34) & "SeqNo" & Chr$(34) & "ASC"
                    Else
                        If bBadInvcDesc Then
                            MsgBox "Description Only Invoices contain lines with no invoice description specified." & Chr$(13) & Chr$(10) & "Aborting Print Job"
                            GoTo badexit
                        End If
                    End If
                    'Intellisol end
                    
                   ReportObj.printJob.SQLQueryString = sSQLQuery
                    'If lRetVal = 0 Then
                        'ReportObj.ShowCrystalError
                    'End If
                    
                    If ReportObj.bSetReportFormula("SessionID", CStr(lSessionID)) Then
                        ReportObj.ShowCrystalError
                    End If
                    
                    iUnitPriceDecPlaces = frm.oClass.moAppDB.Lookup("UnitPriceDecPlaces", _
                         "tciOptions", "CompanyID = " & gsQuoted(sCompanyID))
                    iQtyDecPlaces = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", _
                    "CompanyID = " & gsQuoted(sCompanyID)))
                    
                    ReportObj.bSetReportFormula "UnitPriceDecPlaces", CInt(iUnitPriceDecPlaces)
                    ReportObj.bSetReportFormula "QtyDecPlaces", CInt(iQtyDecPlaces)
                       
                    On Error GoTo badexit
                    
                    'check to see if any customer's invoices are marked for physical printing.
                    iPrintCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & _
                        Str(lSessionID) & " AND tarPrintInvcHdrWrk.HardCopy = 1")
                    'check to see if any customer's invoices are marked for transmittal
                    If frm.chkResendElecDocs = vbChecked Then
                        iTransmitCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & _
                            Str(lSessionID) & " AND tarPrintInvcHdrWrk.EMail = 1 AND tarPrintInvcHdrWrk.ConfirmToCntctKey IS NOT NULL")
                        'check to see if any customer's invoices are marked for transmit only
                        iTransmitOnlyCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & _
                            Str(lSessionID) & " AND tarPrintInvcHdrWrk.EMail = 1 AND tarPrintInvcHdrWrk.ConfirmToCntctKey IS NOT NULL AND tarPrintInvcHdrWrk.HardCopy = 0")
                    Else
                        iTransmitCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & _
                            Str(lSessionID) & " AND tarPrintInvcHdrWrk.EMail = 1 AND tarPrintInvcHdrWrk.ConfirmToCntctKey IS NOT NULL AND tarPrintInvcHdrWrk.Transmitted = 0")
                        'check to see if any customer's invoices are marked for transmit only
                        iTransmitOnlyCount = frm.oClass.moAppDB.Lookup("Count(*) Cnt", "tarPrintInvcHdrWrk", "SessionID = " & _
                            Str(lSessionID) & " AND tarPrintInvcHdrWrk.EMail = 1 AND tarPrintInvcHdrWrk.ConfirmToCntctKey IS NOT NULL AND tarPrintInvcHdrWrk.HardCopy = 0 " & _
                                    " AND tarPrintInvcHdrWrk.Transmitted = 0")
                    End If

                    sRestrictClause = "{tarPrintInvcHdrWrk.SessionID} = " & lSessionID
                       
                    'Process according to print mode choice.
                    Select Case sButton
                        Case kTbPrint
                            If bUI And iPrintCount > 0 Then
                                iRetVal = giSotaMsgBox(frm, frm.oClass.moSysSession, kmsgLoadInvcForm, sBusinessFormID)
                            Else
                                iRetVal = kretOK
                            End If
                            If iRetVal <> kretOK Then
                                GoTo badexit
                            End If
                            lRetVal = ReportObj.lRestrictBy(sRestrictClause & " AND {tarPrintInvcHdrWrk.HardCopy} = 1")
                            If lRetVal = 0 Then
                                ReportObj.ShowCrystalError
                            End If
                        Case kTbPreview
                            'include transmit only documents?
                            If frm.chkPreviewTransmitOnly Then
                                lRetVal = ReportObj.lRestrictBy(sRestrictClause)
                            Else
                                lRetVal = ReportObj.lRestrictBy(sRestrictClause & " AND {tarPrintInvcHdrWrk.HardCopy} = 1")
                            End If
                            If lRetVal = 0 Then
                                ReportObj.ShowCrystalError
                            End If
                    End Select
            
                    If (iPrintCount > 0 And sButton <> kTbEmail) Or (sButton = kTbPreview And frm.chkPreviewTransmitOnly) Then
                        If iTransmitCount > 0 Then
                            'Set flag to prevent print or preview from closing the printjob and causing errors for transmit
                            ReportObj.IsTransmitType = True
                        End If
                        If sButton = kTbPreview And frm.cboTransmit.ListIndex = kTransmitMethPreview And iTransmitCount > 0 Then
                            'set form caption to something unique and save it to find preview windows for hiding, set variable
                            bPreviewAndTransmit = True
                            sUniquePreviewCaption = sOrigFormCaption & " [" & Trim(CStr(lSessionID)) & "]"
                            frm.Caption = sUniquePreviewCaption
                        End If
                        frm.oreport.ProcessReport frm, sButton, iFileType, sFileName, , bCancel
                        If bPreviewAndTransmit Then
                            ' reset form caption
                            frm.Caption = sOrigFormCaption
                        End If
                        If Not (bCancel) Then   'Print dialoge not canceled
                             iPrinted = True
                        End If
                        
                    End If
                         
                    iRetVal = -1
                    
                    'For Print or Print Preview modes, check for invoices printed okay.
                    If (sButton = kTbPreview Or sButton = kTbPrint) Then
                        If (bUI And sButton = kTbPrint And iPrinted) And Not bPreviewAndTransmit Then
                            iRetVal = giSotaMsgBox(frm, frm.oClass.moSysSession, kmsgPrintInvcOK)
                        Else
                            'needed for instances when previewing but checkbox for including in preview is not checked
                            If Not bPreviewAndTransmit And iTransmitCount > 0 Then
                               iRetVal = kretYes
                            End If
                        End If
                    End If
                    
                    'if previewing and transmittal prompt the user to proceed with transmit and close preview window
                    If bPreviewAndTransmit Then
                        iRetVal = giSotaMsgBox(frm, frm.oClass.moSysSession, kmsgGenericExclYesNo, "Proceed with transmittal processing?  " & vbCrLf & "Preview window(s) will be closed.")
                        If iRetVal = kretYes Then
                            'call subroutine here to find and hide preview windows with matching unique caption
                            lPreviewhWnd = HidePreviewWindow(sUniquePreviewCaption)
                        End If
                    End If
                    
                    
                    If iTransmitCount > 0 Then
                        If ((sButton = kTbPreview And frm.cboTransmit.ListIndex = kTransmitMethPreview And iRetVal = kretYes) Or _
                            (sButton = kTbPrint And frm.cboTransmit.ListIndex = kTransmitMethPrint And iRetVal = kretYes And iPrinted) Or _
                            sButton = kTbEmail) Then
                                  
                            'Restrict documents based upon user's selections
                            If frm.chkResendElecDocs = vbChecked Then
                                sSQLClause = "a.SessionID = " & CStr(lSessionID)
                            Else
                                sRestrictClause = sRestrictClause & " AND {tarPrintInvcHdrWrk.Transmitted} = 0"
                                sSQLClause = "a.SessionID = " & CStr(lSessionID) & " AND a.Transmitted = 0 "
                            End If
                           
                            On Error Resume Next
                            'use DocDistribution to export documents to be transmitted to the customers' preferred format
                            '    after export DocDistribution calls the appropriate email routines from EmailUtils to send
                            
                            'disable form before call to transmit
                            frm.Enabled = False
                            
                            bRetVal = ProcessDocDistribution(frm, ReportObj, "TranID", "ConfirmToCntctKey", "TranType", "tarPrintInvcHdrWrk", "InvcKey", sSQLClause, sRestrictClause, "tarInvoiceTrnsmit")
                            
                            'enable form after transmit
                            frm.Enabled = True
                            
                            If bPreviewAndTransmit Then
                                'close hidden preview window
                                If lPreviewhWnd > 0 Then
                                    ClosePreviewWindow (lPreviewhWnd)
                                End If
                            End If
                             
                            If Not bRetVal Then
                                iNoOfErrors = iNoOfErrors + 1
                            Else
                                'set Transmitted flag
                                sSelect = "UPDATE tarPendInvoice SET Transmitted = 1 FROM tarPendInvoice, " _
                                  & "tarPrintInvcHdrWrk WHERE tarPrintInvcHdrWrk.InvcKey = " _
                                  & "tarPendInvoice.InvcKey AND tarPendInvoice.InvcFormKey = " _
                                  & iBusinessFormKey & " AND tarPrintInvcHdrWrk.SessionID = " & lSessionID _
                                  & " AND tarPrintInvcHdrWrk.EMail = 1 "

                                Debug.Print sSelect
                                frm.oClass.moAppDB.ExecuteSQL sSelect
                            
                            End If
   
                        End If      'transmit
                    Else
                        'display message if manually transmitting and no documents to transmit
                        If sButton = kTbEmail Then
                            iNoData = giSotaMsgBox(frm, frm.oClass.moSysSession, kmsgNoReportRecords)
                        End If
                    End If      'transmit count > 0
                    
                    If ((sButton = kTbPrint Or sButton = kTbPreview) And iRetVal = kretYes) Or (sButton = kTbEmail And frm.chkTransmitPrint _
                            And iTransmitOnlyCount > 0 And bRetVal) Then
                        'Initialize UPDATE statement to update printed flag to 1 in tarPendInvoice.
                        '(This statement will not do anything if reprinting invoices.)
                        sSelect = "UPDATE tarPendInvoice SET Printed = 1 FROM tarPendInvoice, " _
                            & "tarPrintInvcHdrWrk WHERE tarPrintInvcHdrWrk.InvcKey = " _
                            & "tarPendInvoice.InvcKey AND tarPendInvoice.InvcFormKey = " _
                            & iBusinessFormKey & " AND tarPrintInvcHdrWrk.SessionID = " & lSessionID
                          
                        If frm.chkTransmitPrint And iTransmitOnlyCount > 0 Then
                            'if flag transmit only as printed and there are transmit only documents
                            sSelect = sSelect & " AND (tarPrintInvcHdrWrk.HardCopy = 1 OR " _
                                & "(tarPrintInvcHdrWrk.EMail = 1 AND tarPrintInvcHdrWrk.HardCopy = 0))"
                        Else
                            'flag only hardcopy documents as printed
                            sSelect = sSelect & " AND (tarPrintInvcHdrWrk.HardCopy = 1)"
                        End If
                                            
                        Debug.Print sSelect
                        frm.oClass.moAppDB.ExecuteSQL sSelect
                    
                    End If      'update print flag
                End If      'setup report
            
            End If      'business forms
            'If other business forms to print, repeat.
            If Not rs.IsEOF Then rs.MoveNext
            
            bRetVal = False
                        
            If ReportObj.IsTransmitType() Then
                ReportObj.bClosePrintJob
            End If
                                    
        Loop
        
    End If
    
    'Finish up.
    rs.Close
    Set rs = Nothing

    If bUI Then
        'if error occurred during processing email, show the message here
        If iNoOfErrors > 0 Then
            giSotaMsgBox frm, frm.oClass.moSysSession, kmsgErrorProcessEmail
        End If
        
        frm.sbrMain.Status = SOTA_SB_START
    End If
    
    lStartReport = kSuccess
    
    Set oSort = Nothing
    
    Set SelectObj = Nothing
    Set ReportObj = Nothing

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
badexit:
    'May have gotten here without need to display error.
    gClearSotaErr
    frm.Caption = sOrigFormCaption
    frm.CleanupWorkTables
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    Set SelectObj = Nothing
    Set ReportObj = Nothing
    If bUI Then frm.sbrMain.Status = SOTA_SB_START
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        frm.Caption = sOrigFormCaption
        gSetSotaErr Err, sMyName, "lStartReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function HidePreviewWindow(sUniqueCaption As String) As Long
   
' Use EnumWindows to see if the preview window exists.
    TargetName = sUniqueCaption & " ("      'add parenthesis to exclude the main form
    TargetHwnd = 0
    
    ' Examine the window names.
    EnumWindows AddressOf WindowEnumerator, 0
    ' See if we got an hwnd.
    If TargetHwnd > 0 Then
      ShowWindow TargetHwnd, SW_HIDE
      HidePreviewWindow = TargetHwnd
    End If
End Function

' Return False to stop the enumeration.
Private Function WindowEnumerator(ByVal app_hwnd As Long, _
    ByVal lParam As Long) As Long
Dim buf As String * 256
Dim title As String
Dim Length As Long

    ' Get the window's title.
    Length = GetWindowText(app_hwnd, buf, Len(buf))
    title = Left$(buf, Length)

    ' See if the title contains the target.
    If InStr(title, TargetName) > 0 Then
        ' Save the hwnd and end the enumeration.
        TargetHwnd = app_hwnd
        WindowEnumerator = False
    Else
        ' Continue the enumeration.
        WindowEnumerator = True
    End If
End Function
Private Sub ClosePreviewWindow(PreviewHandle As Long)
    On Error Resume Next
    Dim retval As Long
    
    retval = PostMessage(PreviewHandle, WM_CLOSE, 0&, 0&)
    
End Sub
Public Sub Main()
'+++ VB/Rig Skip +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("arztc001.clsPrintInvoices")
        StartAppInStandaloneMode oApp, Command$()
    End If


      Exit Sub
End Sub






