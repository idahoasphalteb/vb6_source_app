VERSION 1.0 CLASS
BEGIN
  MultiUse = 0   'False
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPrintInvoices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'************************************************************************************
'     Name: clsPrintInvoices
'     Desc: The clsPrintInvoices class handles the interface for
'           the framework and the VB server
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 08/30/95  DSN
'     Mods: 11/03/95  Dave N. -> added functions in place of code in some subs
'           11/14/96  KMKD - upgraded to new report template
'************************************************************************************
Option Explicit
       
    Public miShutDownRequester  As Integer
    Public mlError              As Long
    Public mlContext            As Long
    Public moAppDB              As Object
    Public moDASSession         As Object
    Public moFramework          As Object
    Public moSysSession         As Object
 
    Private mbPeriodEnd         As Boolean
    Public mfrmMain            As Form
    Public moCTI As Object
    Private mlRunFlags          As Long
    Private mlUIActive          As Long
    Private miShowPmtApplOpt    As Integer
    

Const VBRIG_MODULE_ID_STRING = "ARZTC001.CLS"

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "clsPrintInvoices"
End Function
Public Property Let lUIActive(lNewActive As Long)
'+++ VB/Rig Skip +++
    mlUIActive = lNewActive
End Property
Public Property Get lUIActive() As Long
'+++ VB/Rig Skip +++
    lUIActive = mlUIActive
End Property
Public Property Get lRunFlags() As Long
'+++ VB/Rig Skip +++
    lRunFlags = mlRunFlags
End Property
Public Property Let lRunFlags(ltRunFlags As Long)
'+++ VB/Rig Skip +++
    mlRunFlags = ltRunFlags
End Property
Public Property Set frmMain(frm As Object)
'+++ VB/Rig Skip +++
    Set mfrmMain = frm
End Property
Public Property Get frmMain() As Object
'+++ VB/Rig Skip +++
    Set frmMain = mfrmMain
End Property
Public Property Get ShowPmtApplOpt() As Integer
'+++ VB/Rig Skip +++
    ShowPmtApplOpt = miShowPmtApplOpt
End Property
Public Property Let ShowPmtApplOpt(NewShowPmtApplOpt As Integer)
'+++ VB/Rig Skip +++
    miShowPmtApplOpt = NewShowPmtApplOpt
End Property
Public Property Let bPeriodEnd(bSetPeriodEnd As Boolean)
'+++ VB/Rig Skip +++
    mbPeriodEnd = bSetPeriodEnd
End Property
Public Property Get bPeriodEnd() As Boolean
'+++ VB/Rig Skip +++
    bPeriodEnd = mbPeriodEnd
End Property
Private Sub Class_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ClassInitialize Me
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Initialize", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Private Sub Class_Terminate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ClassTerminate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Terminate", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    InitializeObject = kFailure
    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title
    mbPeriodEnd = False
    If (moAppDB.Lookup("PrintInvcs", "tarOptions", "CompanyID = " & kSQuote & moSysSession.CompanyId & kSQuote) = 1) Then
        InitializeObject = kSuccess
    Else
        giSotaMsgBox Nothing, moSysSession, kmsgInvcPrintOFF
        InitializeObject = EFW_C_CANCEL
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "InitializeObject", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function LoadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    LoadUI = kFailure

    Set mfrmMain = frmPrintInvoices
    Set mfrmMain.oClass = Me
    mfrmMain.lRunMode = mlContext And kRunModeMask
    mfrmMain.iMode = kInvoicePrint
    
    Load mfrmMain
    
    If mlError Then Err.Raise mlError
    
    If mfrmMain.bLoadSuccess Then
        LoadUI = kSuccess
    Else
        LoadUI = EFW_C_FAIL
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "LoadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function GetUIHandle(ByVal ltContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mfrmMain Is Nothing Then
        GetUIHandle = mfrmMain.hwnd
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "GetUIHandle", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function DisplayUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
        DisplayUI = EFW_CT_MODALEXIT
    #Else
        DisplayUI = kFailure
        If ClassDisplayUI(Me, lContext) = kFailure Then Exit Function
        DisplayUI = kSuccess
    #End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "DisplayUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function ShowUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ShowUI = kFailure
    If ClassShowUI(Me, lContext) = kFailure Then Exit Function
    ShowUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ShowUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function QueryShutDown(lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    QueryShutDown = kFailure
    If ClassQueryShutdown(Me, lContext) = kFailure Then Exit Function
    QueryShutDown = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "QueryShutDown", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function MinimizeUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
        mfrmMain.WindowState = vbMinimized
    #Else
        MinimizeUI = kFailure
        If ClassMinimizeUI(Me, lContext) = kFailure Then Exit Function
        MinimizeUI = kSuccess
    #End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "MinimizeUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function RestoreUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    RestoreUI = kFailure
    If ClassRestoreUI(Me, lContext) = kFailure Then Exit Function
    RestoreUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "RestoreUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function HideUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
        HideUI = kSuccess
    #Else
        HideUI = kFailure
        If ClassHideUI(Me, lContext) = kFailure Then Exit Function
        HideUI = kSuccess
    #End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "HideUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function UnloadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    UnloadUI = kFailure
    If ClassUnloadUI(Me, lContext) = kFailure Then Exit Function
    UnloadUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "UnloadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function TerminateObject(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    TerminateObject = kFailure
    If ClassTermObj(Me, lContext) = kFailure Then Exit Function
    TerminateObject = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "TerminateObject", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
Public Function PrintNoUI(lContext As Long, Optional sPeriodEndDate, Optional iPerNo, Optional sPerYear) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    PrintNoUI = kFailure
    
    Set mfrmMain = frmPrintInvoices
    
    Set mfrmMain.oClass = Me
    mfrmMain.lRunMode = lContext And kRunModeMask
    mbPeriodEnd = True
    Load mfrmMain
    
    If mfrmMain.bLoadSuccess Then
        PrintNoUI = lStartReport(kTbPrint, mfrmMain, True, sPeriodEndDate, iPerNo, sPerYear)
    Else
        PrintNoUI = EFW_C_FAIL
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "PrintNoUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
'***********************CUSTOM FORM CODE FOR ARZTCDL1.CLS*************************************'
Public Sub PrintInvoices(newbatch As String, Optional TranNo As Variant, Optional iBatchType As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
Dim i As Integer
Dim s As String

    If mfrmMain Is Nothing Then
        If moFramework.LoadUI(Me) = kFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
   
    mfrmMain.msBatchID = newbatch
    
    If IsMissing(iBatchType) Then
        mfrmMain.miBatchType = 0
    Else
        If iBatchType = kBatchTypeARFC Then
            mfrmMain.cboInvcTypeToPrint.Clear
            mfrmMain.cboInvcTypeToPrint.AddItem "Finance Charges"
            mfrmMain.cboInvcTypeToPrint.ItemData(0) = -1
            mfrmMain.cboInvcTypeToPrint.ListIndex = 0
            mfrmMain.cboInvcTypeToPrint.Refresh
            mfrmMain.miBatchType = kBatchTypeARFC
        End If
    End If
        
    mfrmMain.moSelect.ResetGrid
    mfrmMain.moSelect.bSetRowInitialValues mfrmMain.moSelect.lGetGrdRowFromTblCol("tciBatchLog", "BatchID"), kOperatorEqual, True, newbatch
    mfrmMain.chkInvcMsgFromCust = 1
    mfrmMain.chkExtLineItemCmnt = 1
    mfrmMain.chkOverrideReprint = 0
    mfrmMain.chkOnlyUnprinted = 0
    mfrmMain.txtInvcMsgLocal = ""
    mfrmMain.cboInvcTypeToPrint.ListIndex = 0
    mfrmMain.cboSortBy.ListIndex = 0
    
    gGridLockCell mfrmMain.grdSelection, 3, 1
    
    If IsMissing(TranNo) Then
        mfrmMain.Show vbModal
    Else
        mfrmMain.iMode = kQuickPrint
        mfrmMain.moSelect.bSetRowInitialValues mfrmMain.moSelect.lGetGrdRowFromTblCol(mfrmMain.sInvcTable, "TranNo"), kOperatorEqual, True, TranNo
        mfrmMain.HandleToolbarClick kTbPrint
    End If
    
    If mlError Then
        Err.Raise mlError
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "PrintInvoices", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Function lGetSettings(ByRef sSetting, ByRef sSelect, Optional oResponse As Variant) As Long
'************************************************************************************
'      Desc: Retrieve the report settings as a string.
'     Parms: sSetting - Output; String of saved setting id/setting key pairs.
'            sSelect - Output; String of saved setting key/select info pairs.
'            oResponse as Variant - Optional object used for debugging
'   Returns: kSuccess; kFailure
'            sSetting - Each setting is a string and a key. Each string and key is
'            separated by a tab (vbTab). Each setting is delimited by a CRLF (vbCrLf).


'            The first setting in the list is the most recently used by the user.
'            If no settings class exists, then empty string " is returned.
'            If error, then empty string " is returned.
'            sSelect - Each setting is a key and selection row information.
'            Each row is separated by a semicolon. Values within a row
'            are separated by commas.  Each row contains setting key,
'            caption, operator, start value, end value.  E.g.,
'            <setting key>, <caption>, <opertor>,<start value>, <end value>;
'            <setting key>, <caption>, <opertor>,<start value>, <end value>.
'            If no settings, then empty string (") is returned.
'   Assumes: Settings class proc Initialize has been run.
'            Selection control proc bInitSelect has been run.
'************************************************************************************
    lGetSettings = kFailure

    On Error Resume Next
    sSetting = mfrmMain.moSettings.sGetSettings()
    If Err <> 0 Then
        sSetting = ""
    End If
    
    sSelect = mfrmMain.moSelect.sGetSettings()
    If Err <> 0 Then
        sSelect = ""
    End If
    
    lGetSettings = kSuccess
End Function
Public Sub GenerateReport(iFileType As Integer, sFileName As String, _
    Optional lSettingKey As Variant, Optional sSelect As Variant, _
    Optional oResponse As Variant)
'************************************************************************************
'      Desc: Generate web report file by calling HandleToolbarClick.
'     Parms: iFileType as Integer - File type to create (0 = HTML, 1 = RPT)
'            sFileName as String - File name to create (including path)
'            lSettingKey as Variant - Optional; setting selected by user
'            sSelect as Variant - Optional; selection grid values
'            oResponse as Variant - Optional object used for debugging
'************************************************************************************
    '    Get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            ' see cboReportSettings_Click for additional notes
            mfrmMain.moSettings.bLoadSettingsByKey (CLng(lSettingKey))
        End If
    End If
    
    '   Put Web Client Selection Grid values on Web Server Selection Grid
    If Not IsMissing(sSelect) Then
        On Error Resume Next    '   if selection grid does not exist
        mfrmMain.moSelect.lPutSelectGrid sSelect
        On Error GoTo 0     '   normal error processing
    End If
    
    mfrmMain.HandleToolbarClick kTbPreview, iFileType, sFileName
End Sub
Public Function sGetSelection(Optional oResponse As Variant) As String
'************************************************************************************
'      Desc: Retrieve the report selection grid information as a string.
'     Parms: oResponse as Variant - Optional object used for debugging
'   Returns: String of selection grid information.
'            String of variable values, selection array values, and selection grid values.
'            Variable values, selection array values, and selection grid values
'            are separated from each other by two semicolons.
'            Variable values are separated from each other by a comma.
'            Each selection array row is separated by a semicolon.  Values in the
'            selection array are separated by a comma.
'            Each grid row is separated by a semicolon. Values in the selection
'            grid row are separated by a comma.
'            Assumes: Selection Grid's proc bInitSelect has been run.
'************************************************************************************
    On Error Resume Next
    sGetSelection = mfrmMain.moSelect.sGetSelection()
    If Err <> 0 Then
        sGetSelection = ""
    End If
End Function
