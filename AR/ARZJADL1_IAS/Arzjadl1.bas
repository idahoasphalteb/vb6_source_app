Attribute VB_Name = "basRegister"
'***********************************************************************
'     Name: bas{Name} (source: Template)
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: {date&Inits}
'     Mods: {date&Inits};
'***********************************************************************
Option Explicit
Global Const RPT_MODULE = "AR" 'enter the two character module Name here
Public Declare Sub Sleep Lib "kernel32" _
 (ByVal dwMilliseconds As Long)
Private Const kGLPosting = 151424
Private Const kGLPostingComplete = 151425
    
Public gbSkipPrintDialog As Boolean      '   whether to show Print Dialog window. HBS.
Public gbCancelPrint As Boolean     '   whether user selected cancel on print dialog. HBS.
    
Public Type uLocalizedStrings
    NotPrinted      As String
    Printed         As String
    FatalErrors     As String
    NotBalanced     As String
    Balanced        As String
    posting         As String
    posted          As String
    PostingFailed   As String
    JournalDesc     As String
    INJournalDesc   As String
    CRJournalDesc   As String
    MCJournalDesc   As String
    ARPAJournalDesc   As String
    CMJournalDesc   As String
    ProcessingRegister As String
    PrintingRegister As String
    RegisterPrinted As String
    PostingComplete As String
    PostingModule As String
    ModulePosted As String
    Validating As String
    PrintError As String
    PrintErrorCompleted As String
    ProcessingGLReg As String
    CheckingBatch As String
    PostingModule2 As String
    ModulePosted2 As String
    GLPosting As String
    GLPosted As String
    ModuleCleanup As String
End Type

Public Type RegStatus
    iBatchStatus As Long
    iRecordsExist As Long
    iPrintStatus As Long
    iErrorStatus As Long
End Type

Public Enum GroupingIndex   'The second index for GroupingParams()
    GroupName = 0           'Crystal report group header name, such as "GH1"
    SortDirection = 1       'Sort direction, 0 for ascending, 1 for descending (comes from CRAXDRT.CRSortDirection
    TableName = 2           'Name of table holding group column to group by
    ColumnName = 3          'Column name to group by
    HighestIndex = 3        'The highest index of the fields described above, used for dimensioning
End Enum

Const VBRIG_MODULE_ID_STRING = "ARZJADL1.BAS"


Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Main is required as the standard 'Startup Form'.
'           Main can be empty.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub




Public Sub GetRegisterStatus(DAS As Object, msCompanyID As String, lBatchKey As Long, lBatchType As Long, uRegStat As RegStatus)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
        
        DAS.SetInParam msCompanyID
        DAS.SetInParam lBatchKey
        DAS.SetInParam lBatchType
        DAS.SetOutParam uRegStat.iPrintStatus
        DAS.SetOutParam uRegStat.iErrorStatus
        DAS.SetOutParam uRegStat.iBatchStatus
        DAS.SetOutParam uRegStat.iRecordsExist
        DAS.ExecuteSP ("sparCheckRgstrStatus2")
                                                                                                                               
        Sleep 0&
                
        uRegStat.iPrintStatus = DAS.GetOutParam(4)
        uRegStat.iErrorStatus = DAS.GetOutParam(5)
        uRegStat.iBatchStatus = DAS.GetOutParam(6)
        uRegStat.iRecordsExist = DAS.GetOutParam(7)
        
        DAS.ReleaseParams

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetRegisterStatus", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub BuildLocalizedStrings(SysDB As Object, SysSession As Object, guLocalizedStrings As uLocalizedStrings)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    With guLocalizedStrings
        .NotPrinted = gsBuildString(kJrnlNotPrinted, SysDB, SysSession)
        .Printed = gsBuildString(kJrnlPrinted, SysDB, SysSession)
        .FatalErrors = gsBuildString(kJrnlFatalErrors, SysDB, SysSession)
        .NotBalanced = gsBuildString(kJrnlNotBalanced, SysDB, SysSession)
        .Balanced = gsBuildString(kJrnlBalanced, SysDB, SysSession)
        .posting = gsBuildString(kJrnlPosting, SysDB, SysSession)
        .posted = gsBuildString(kJrnlPosted, SysDB, SysSession)
        .PostingFailed = gsBuildString(kJrnlPostingFailed, SysDB, SysSession)
        .INJournalDesc = gsBuildString(kARZJA001Invoice, SysDB, SysSession)
        .CRJournalDesc = gsBuildString(kARZJB001CRR, SysDB, SysSession)
        .ARPAJournalDesc = gsBuildString(kARZJC001PAR, SysDB, SysSession)
        .ProcessingRegister = gsBuildString(kARZJA001ProcessRegister, SysDB, SysSession)
        .PrintingRegister = gsBuildString(kARZJA001PrintingRegister, SysDB, SysSession)
        .RegisterPrinted = gsBuildString(kARZJA001RegisterPrinted, SysDB, SysSession)
        .PostingComplete = gsBuildString(kARZJA001PostingComplete, SysDB, SysSession)
        .PostingModule = gsBuildString(kVSDelCustKey, SysDB, SysSession)
        .ModulePosted = gsBuildString(kVCustIactive, SysDB, SysSession)
        .Validating = gsBuildString(kARZJA001Validating, SysDB, SysSession)
        .PrintError = gsBuildString(kARZJA001PrintError, SysDB, SysSession)
        .PrintErrorCompleted = gsBuildString(kARZJA001PrintErrorCompleted, SysDB, SysSession)
        .ProcessingGLReg = gsBuildString(kARZJA001ProcessingGLReg, SysDB, SysSession)
        .CheckingBatch = gsBuildString(kARZJA001CheckingBatch, SysDB, SysSession)
        .PostingModule2 = gsBuildString(kPostingModule, SysDB, SysSession)
        .ModulePosted2 = gsBuildString(kModulePosted, SysDB, SysSession)
        .GLPosting = gsBuildString(kGLPosting, SysDB, SysSession)
        .GLPosted = gsBuildString(kGLPostingComplete, SysDB, SysSession)
        .ModuleCleanup = gsBuildString(kModuleCleanup, SysDB, SysSession)
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BuildLocalizedStrings", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basRegister"
End Function

Public Function lStartReport(sButton As String, frm As Form, bPeriodEnd As Boolean, sFormulas() As String, sPerEndDate, iPerNo, sPerYear, _
    Optional iFileType As Variant, _
    Optional sFileName As Variant, _
    Optional GroupingParams As Variant, _
    Optional sAppendSort As String = "") As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

    Dim ReportObj As clsReportEngine
    Dim DBObj As Object
    Dim iNumberOfGroupings As Integer
    Dim i As Integer

    
    On Error GoTo badexit

    lStartReport = kFailure
    
    If Not bPeriodEnd Then ShowStatusBusy frm

    Set ReportObj = frm.moReport
    Set DBObj = frm.oClass.moAppDB
    
    'Delete all records from work tables that have the same session ID as current session ID.
    ReportObj.CleanupWorkTables
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'THE ORDER OF THE FOLLOWING EVENTS IS IMPORTANT!
    
    'CUSTOMIZE:  The .RPT file to be used should be set here.  (More than one .RPT file
    'may exist for the task.)
'    ReportObj.ReportPath = frm.moReport.ReportPath
'    ReportObj.ReportFileName = frm.moReport.ReportFileName

    'Start Crystal print engine, open a print job, and get localized strings from
    'tsmLocalString table.
    If (ReportObj.lSetupReport = kFailure) Then
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    ReportObj.Orientation() = vbPRORPortrait
    
    
    'CUSTOMIZE:  Include these calls if you have named subtotal & header labels on the report
    'using the "lbl" convention on formula field names so that label text will handled automatically
    ReportObj.UseSubTotalCaptions() = 0
    ReportObj.UseHeaderCaptions() = 0
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (ReportObj.lSetStandardFormulas(frm) = kFailure) Then
        GoTo badexit
    End If

    
    If Len(sFormulas(0, 0)) Then
        For i = 0 To UBound(sFormulas, 2)
            If ReportObj.bSetReportFormula(sFormulas(0, i), sFormulas(1, i)) = False Then
                GoTo badexit
            End If
        Next
    End If
   
    'GroupingParams(x,y), if specified, will contain a list of report group headers that need to be
    'specified at run-time.  There are x groups to be modified (start x at 1, 0 is unused), and there
    'are four columns (y) for each x.  Specify the column (y) by using the GroupingIndex enumeration.
    If Not IsMissing(GroupingParams) Then
        iNumberOfGroupings = UBound(GroupingParams, 1)
        For i = 1 To iNumberOfGroupings
            ReportObj.bModifyGroupSort _
                (GroupingParams(i, GroupingIndex.GroupName)), _
                (GroupingParams(i, GroupingIndex.SortDirection)), _
                (GroupingParams(i, GroupingIndex.TableName)), _
                (GroupingParams(i, GroupingIndex.ColumnName))
        Next i
    End If
   
   
    If Len(sAppendSort) > 0 Then
        ReportObj.AppendSort sAppendSort
    End If
   
    'Retrieve the SQL statement stored with the .RPT file and modify it as needed.
    ReportObj.BuildSQL
    ReportObj.SetSQL
    
    
    

    'CUSTOMIZE:  Include this call if you have named column labels on the report
    'using the "lbl" convention and wish label text to be handled automatically for you.
    ReportObj.SetReportCaptions
        
    'Always print summary section
    If ReportObj.lSetSummarySection(1, frm.moOptions) = kFailure Then
        GoTo badexit
    End If
    
    If InStr(RTrim(frm.RestrictClause), "=") < Len(RTrim(frm.RestrictClause)) Then
        If (ReportObj.lRestrictBy(frm.RestrictClause) = kFailure) Then
            GoTo badexit
        End If
    End If
   
    'Print the report and skip the printer dialog because we already had it.
    ReportObj.ProcessReport frm, sButton, , , True
            
    If Not bPeriodEnd Then ShowStatusNone frm
    
    lStartReport = kSuccess
    
    Set ReportObj = Nothing
    Set DBObj = Nothing
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
 Resume
badexit:
    ReportObj.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set ReportObj = Nothing
    Set DBObj = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function




