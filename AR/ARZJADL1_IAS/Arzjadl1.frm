VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmRegister 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Process Register"
   ClientHeight    =   5250
   ClientLeft      =   3810
   ClientTop       =   3165
   ClientWidth     =   7815
   HelpContextID   =   43709
   Icon            =   "Arzjadl1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5250
   ScaleWidth      =   7815
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   43752
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   26
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chk 
      Alignment       =   1  'Right Justify
      Caption         =   "Include &Project Details"
      Height          =   375
      Index           =   2
      Left            =   3360
      TabIndex        =   21
      Top             =   3000
      Visible         =   0   'False
      WhatsThisHelpID =   43745
      Width           =   3345
   End
   Begin VB.Frame fraRegister 
      Height          =   4215
      Left            =   60
      TabIndex        =   2
      Top             =   480
      Width           =   7665
      Begin MSComCtl2.UpDown spnCopies 
         Height          =   285
         Left            =   2160
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   675
         WhatsThisHelpID =   43727
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtCopies"
         BuddyDispid     =   196623
         OrigLeft        =   2175
         OrigTop         =   675
         OrigRight       =   2415
         OrigBottom      =   960
         Max             =   10000
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.CommandButton cmdProperties 
         Caption         =   "Proper&ties"
         Height          =   315
         Left            =   5610
         TabIndex        =   5
         Top             =   180
         WhatsThisHelpID =   43742
         Width           =   1035
      End
      Begin VB.ComboBox cboFormat 
         Height          =   315
         Left            =   1530
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   1530
         WhatsThisHelpID =   43741
         Width           =   1545
      End
      Begin VB.ComboBox cboSort 
         Height          =   315
         Left            =   1530
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   1080
         WhatsThisHelpID =   43740
         Width           =   1545
      End
      Begin VB.CheckBox chk 
         Alignment       =   1  'Right Justify
         Caption         =   "Include &Line Item Comments"
         Height          =   375
         Index           =   1
         Left            =   3300
         TabIndex        =   19
         Top             =   1860
         WhatsThisHelpID =   43739
         Width           =   3345
      End
      Begin VB.CheckBox chk 
         Alignment       =   1  'Right Justify
         Caption         =   "Include &Invoice Comments"
         Height          =   375
         Index           =   0
         Left            =   3300
         TabIndex        =   20
         Top             =   2190
         WhatsThisHelpID =   43738
         Width           =   3345
      End
      Begin VB.Frame fraOptions 
         Caption         =   "Register &Options"
         Height          =   1260
         Left            =   3330
         TabIndex        =   9
         Top             =   585
         Width           =   3315
         Begin VB.OptionButton optRegister 
            Caption         =   "Post Only"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   13
            Top             =   960
            WhatsThisHelpID =   43736
            Width           =   2535
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Register Only"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   12
            Top             =   720
            WhatsThisHelpID =   43735
            Width           =   2280
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Register, then Prompt for Post"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   11
            Top             =   495
            Value           =   -1  'True
            WhatsThisHelpID =   43734
            Width           =   2430
         End
         Begin VB.OptionButton optRegister 
            Caption         =   "Register, then Post"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   10
            Top             =   270
            WhatsThisHelpID =   43733
            Width           =   2280
         End
      End
      Begin VB.TextBox txtMessageHeader 
         Height          =   285
         Index           =   0
         Left            =   1500
         MaxLength       =   50
         TabIndex        =   24
         Top             =   3750
         WhatsThisHelpID =   18
         Width           =   5955
      End
      Begin VB.CheckBox chkDefer 
         Alignment       =   1  'Right Justify
         Caption         =   "&Defer Report"
         Height          =   285
         Left            =   60
         TabIndex        =   16
         Top             =   1950
         Visible         =   0   'False
         WhatsThisHelpID =   43731
         Width           =   1665
      End
      Begin VB.TextBox txtCopies 
         Height          =   285
         Left            =   1545
         TabIndex        =   7
         Text            =   "1"
         Top             =   675
         WhatsThisHelpID =   7
         Width           =   615
      End
      Begin VB.ComboBox cboReportDest 
         Height          =   315
         Left            =   1545
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   180
         WhatsThisHelpID =   17
         Width           =   3990
      End
      Begin VB.Label Label2 
         Caption         =   "&Format"
         Height          =   285
         Left            =   105
         TabIndex        =   18
         Top             =   1590
         Width           =   1065
      End
      Begin VB.Label Label1 
         Caption         =   "&Sort By"
         Height          =   285
         Left            =   105
         TabIndex        =   17
         Top             =   1170
         Width           =   1035
      End
      Begin VB.Label lblRptMessage 
         Caption         =   "Report &Message"
         Height          =   195
         Left            =   120
         TabIndex        =   23
         Top             =   3780
         Width           =   1305
      End
      Begin VB.Label lblStatusMsg 
         Caption         =   "Pending"
         Height          =   630
         Left            =   4140
         TabIndex        =   27
         Top             =   3060
         Width           =   3300
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblStatus 
         AutoSize        =   -1  'True
         Caption         =   "Status:"
         Height          =   195
         Left            =   3315
         TabIndex        =   22
         Top             =   3045
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label lblCopies 
         AutoSize        =   -1  'True
         Caption         =   "&Copies to Print"
         Height          =   285
         Left            =   105
         TabIndex        =   6
         Top             =   720
         Width           =   1020
      End
      Begin VB.Label lblReportDest 
         AutoSize        =   -1  'True
         Caption         =   "&Report Destination"
         Height          =   285
         Left            =   105
         TabIndex        =   3
         Top             =   225
         Width           =   1320
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   35
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   28
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4860
      WhatsThisHelpID =   1
      Width           =   7815
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   1
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: frmRegister
'     Desc: Post Transactions
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 11/22/95 LLJ
'     Mods: {date&Inits};
'***********************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Context Menu Object
    Public moContextMenu        As clsContextMenu

 
'Private variables of Form Properties
    Private moAppDAS            As Object
    Private moClass             As clsInvRegIP
    Private mlRunMode           As Long
    Private mbCancelShutDown    As Boolean
    Private mbEnterAsTab        As Boolean
    Private mlRetValue          As Long
    Private msHomeCurrency      As String
    Private mbunload            As Boolean
    Private miPreview           As Integer
    Private mdBusinessDate      As Date
    Private miCommExpOpt        As Integer
    Private miUseSper           As Integer
    Private msOrigCaption       As String
    Private miInclTDinSales     As Integer
    Private miUnitPriceDecPlaces   As Integer
    Private miQtyDecPlaces      As Integer
    Private msCRWSelect         As String
    Private moDDData            As clsDDData
    
'Public Form Variables
    Public msARReportPath       As String
    Public msGLReportPath       As String
    Public msBatchID            As String
    Public moSotaObjects        As New Collection
    Public mlBatchKey           As Long
    Public mlBatchType          As Long
    Public mlTaskNumber         As Long
    Public mdPostDate           As Date
    Public miErrorsOccured      As Integer
    Public mbPostOnly           As Boolean
    Public moReport             As clsReportEngine
    Public sWorkTableCollection As Collection
   
    
 'variables used in posting multiple batches
    Public miMode               As Integer  '1 = default, show form, 0 = posting multiple batches, no UI
    Public miPrint              As Integer
    Public miPost               As Integer
    Public miUseMultCurr        As Integer
    Public miGLPostRgstrFormat  As Integer
    Public miIntegrateWithCM    As Integer
    Public moOptions            As clsOptions
    Public moPrinter            As Printer

'Private form variables
    Private msCompanyID         As String
    Private miFullGL            As Integer
    Private mlSessionID         As Long
    Private miSessionIDItem     As Integer
    Private msReportName        As String
    Private msWorkTable         As String
    Private msSPCreateTable     As String
    Private msSPDeleteTable     As String
    Private msBatchType         As String
    Private guLocalizedStrings  As uLocalizedStrings
    Private mbRegChecked        As Boolean
    Private muRegStat           As RegStatus
    Private mFiscPer            As Integer
    Private mFiscYear           As String
    Private mPostError          As Integer
    Private mbSkipedToPost      As Boolean
    Private mlLockID            As Long
    Private msReportPath        As String
    Private msReportFileName    As String
    Private mbPostingAllowed    As Boolean
    Private mbUserIsOwner        As Boolean
    
    
'Options constants

    Private Const kPostXXBatches        As Integer = 0
    Private Const kInteractiveBatch     As Integer = 1
    Private Const kNoErrors             As Integer = 0
    Private Const kFatalErrors          As Integer = 1
    Private Const kWarnings             As Integer = 2
    
    'Indexes of optRegister
    Private Const kPrintPost            As Integer = 0
    Private Const kPrintPromptPost      As Integer = 1
    Private Const kPrintNoPost          As Integer = 2
    Private Const KPostOnly             As Integer = 3
    
    Private Const kPreviewOnly          As Integer = 3

    Private Const kProjectAccounting    As Integer = 2
    
    Private Const kSourceModule         As Integer = 5
    
    Private Const kAcctRefUsageNotUsed  As Integer = 0
    
    Private Const kGLPostRgstrFormat_None = 1
    Private Const kGLPostRgstrFormat_Summary = 2
    Private Const kGLPostRgstrFormat_Detail = 3
    
    
'General Constants
                                                        'dave? fix
    Private Const kSP_POSTGL                As String = "sp_XXGLPost"   ' // LLJ:  change to correct sp Name
    Private Const kclsRegisterPRC           As String = "clsRegisterPRC"
    Private Const ktskRegisterPRC           As Long = 84607753  '??               ' // LLJ:  need correct task Number
    
    'AR task Number constants
        Private Const ktskValidateInvoice       As Long = 7320
        Private Const ktskValidateCashReceipts  As Long = 7321
        Private Const ktskValidateARPmtAppl     As Long = 7322
        Private Const ktskValidateComm          As Long = 7323
        Private Const ktskInvoice               As Long = 84607642
        Private Const ktskCashReceipts          As Long = 84607753
        Private Const ktskARPmtAppl             As Long = 84607755
        Private Const ktskARComm                As Long = 84607756
        Private Const kExitSub                  As Long = 2
        Private Const ktskCommissions           As Long = 43
    
    'Invoice validation constants
        Private Const kERROR_LOG_RPT_NAME       As String = "ARZJA003.RPT"
        Private Const PRE_POST                  As String = "spciPrePost"
        Private Const MODULE_POST               As String = "spciModPost"
        Private Const GL_POST                   As String = "spciGLPost"
        Private Const MODULE_CLEANUP            As String = "spciModCleanup"
        
    'Invoice constants
        Private Const kIN_MC_SUMMARY_RPT_Name   As String = "ARZJA001.RPT"
        Private Const kIN_MC_DETAIL_RPT_Name    As String = "ARZJA002.RPT"
        Private Const kIN_SUMMARY_RPT_Name      As String = "ARZJA005.RPT"
        Private Const kIN_DETAIL_RPT_Name       As String = "ARZJA006.RPT"
        
        Private Const kIN_WRK_table             As String = "tarInvcRegWrk"
        Private Const kIN_SP_CREATEWRK          As String = "sparRegInvoice"
        Private Const kIN_SP_DELETEWRK          As String = "sparRegInvoiceDel"
        Private Const kIN_batch_Type            As Integer = 501
        
    'Intellisol start
        Private Const kIN_MC_SUMMARY_RPT_PA     As String = "ARZJA001_IPA.RPT"
        Private Const kIN_MC_DETAIL_RPT_PA      As String = "ARZJA002_IPA.RPT"
        Private Const kIN_SUMMARY_RPT_PA        As String = "ARZJA005_IPA.RPT"
        Private Const kIN_DETAIL_RPT_PA         As String = "ARZJA006_IPA.RPT"
    'Intellisol end
    
    'Cash Receipts constants
        Private Const kCR_MC_SUMMARY_RPT_Name   As String = "ARZJB001.RPT"
        Private Const kCR_MC_DETAIL_RPT_Name    As String = "arzjb002.rpt"
        Private Const kCR_SUMMARY_RPT_Name      As String = "ARZJB003.RPT"
        Private Const kCR_DETAIL_RPT_Name       As String = "arzjb004.rpt"
        
        Private Const kCR_WRK_table             As String = "tarCashRRegWrk"
        Private Const kCR_SP_CREATEWRK          As String = "sparRegCashRcpt"
        Private Const kCR_SP_DELETEWRK          As String = "sparRegCashRcptDel"
        Private Const kCR_batch_Type            As Integer = 502
    
    'AR Payment Applications constants
        Private Const kARPA_MC_RPT_Name           As String = "ARZJC001.RPT"
        Private Const kARPA_RPT_Name              As String = "ARZJC002.RPT"
        
        Private Const kARPA_WRK_table             As String = "tarPayApplRegWrk"
        Private Const kARPA_SP_CREATEWRK          As String = "sparRegPmtAppl"
        Private Const kARPA_SP_DELETEWRK          As String = "sparRegPmtApplDel"
        Private Const kARPA_batch_Type            As Integer = 503
        
        Private Const kGL_MC_SUMMARY_RPT          As String = "GLZJE006.rpt"
        Private Const kGL_SUMMARY_RPT             As String = "GLZJE005.rpt"

        Private Const kAR_COMM_REGISTER           As String = "ARZJD001.RPT"
        Private Const kAR_COMM_REGISTER2          As String = "ARZJD002.RPT"
        
    'Print cash receipts regiter recap if integrated with CM
        Private Const kAR_CR_RECEIPT_REG          As String = "ARZJB005.RPT"
        Private Const kAR_CR_RECAP_REG            As String = "ARZJB006.RPT"
                    
    'Batch type constants
        Private Const kINBatchType As Integer = 501
        Private Const kFCBatchType As Integer = 502
        Private Const kCRBatchType As Integer = 503
        Private Const kPABatchType As Integer = 504
        Private Const kCOBatchType As Integer = 505
        
    'Intellisol start
    Public mbCalledFromPA As Boolean
    'Intellisol end

' AvaTax integration - General Declarations
    Private mbTrackSTaxOnSales      As Boolean ' Used to check AR TrackSTaxOnSales option
    Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
    Public mbAvaTaxEnabled          As Boolean ' Defines if AvaTax is installed on the DB & Client , and is turned on for this company
' AvaTax end

    Private cypHelper       As Object 'SGS-JMM 2/10/2011

Const VBRIG_MODULE_ID_STRING = "ARZJADL1.FRM"

Public Property Let UserIsOwner(ByVal bnewvalue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iSecuritylevel As Integer
    
    mbUserIsOwner = bnewvalue
    
    iSecuritylevel = giGetSecurityLevel(moClass.moFramework, moClass.moFramework.GetTaskID)
    
    If iSecuritylevel = sotaTB_SUPERVISORY Or (iSecuritylevel = sotaTB_NORMAL And mbUserIsOwner) Then
        mbPostingAllowed = True
    Else
        mbPostingAllowed = False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UserIsOwner_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get UserIsOwner() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    UserIsOwner = mbUserIsOwner
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UserIsOwner_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bUnload(bnewvalue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbunload = bnewvalue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bunload_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bUnload() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bUnload = mbunload
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bunload_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub cboFormat_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboFormat, True
    #End If
'+++ End Customizer Code Push +++
    If moClass.moFramework.GetTaskID() = ktskInvoice Or moClass.moFramework.GetTaskID() = ktskFinChgReg Then
        If cboFormat.ListIndex = 1 Then
            chk(0).Enabled = True
        Else
            chk(0).Enabled = False
        End If
        'Intellisol start
        '6/16/00 cec - changed default visibility to false and set it here
        ' we only want the option on Invoices, nothing else.
        chk(2).Visible = True
        'Intellisol end
        
    ElseIf moClass.moFramework.GetTaskID() = ktskCashReceipts Then
        If cboFormat.ListIndex = 1 Then
            chk(0).Enabled = True
        Else
            chk(0).Enabled = False
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboFormat_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub




Private Sub cmdProperties_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdProperties, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Call DestSetup(Me)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdProperties_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
    miErrorsOccured = 0
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim aniPos As Long
Dim lRetval As Long

  Set sbrMain.Framework = moClass.moFramework
  sbrMain.BrowseVisible = False


    msCompanyID = moClass.moSysSession.CompanyID
    miFullGL = moClass.moSysSession.FullGL
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    mdBusinessDate = moClass.moSysSession.BusinessDate
    mPostError = kNoErrors
    Set moAppDAS = moClass.moAppDB
   
    tbrMain.Build sotaTB_REGISTER
    tbrMain.AddSeparator 4
    tbrMain.AddButton kTbPreview, 5
    
    GetModVariables
    GetHomeCurrency
    
    BuildLocalizedStrings moClass.moAppDB, moClass.moSysSession, guLocalizedStrings
    
    TaskNumber = moClass.moFramework.GetTaskID()
    msOrigCaption = Me.Caption
    
    'populate list of print devices
    BuildPrintDeviceCombo Me, cboReportDest.Name
    'populate &/or enable/disable the other 2 combo's based upon task initiated.
    
   ' SetUIStatus
    mlSessionID = GetUniqueTableKey(moClass.moAppDB, msWorkTable)
    If TaskNumber = ktskARComm Then
        cboSort.AddItem gsBuildString(kSalespersonIDDomain, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kSalesTerritoryTbl, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kCustomerIDDomain, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kCustNameCol, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kCustClassTbl, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kInvoiceTbl, moClass.moAppDB, moClass.moSysSession)
        cboSort.ListIndex = 0
        
        cboFormat.AddItem gsBuildString(kARZJA001Summary, moClass.moAppDB, moClass.moSysSession)
        cboFormat.ListIndex = 0
        
        chk(1).Visible = False
        chk(0).Visible = False
    End If
    If TaskNumber = ktskInvoice Or TaskNumber = ktskFinChgReg Then
        cboSort.AddItem gsBuildString(kARZJA001InvoiceNumber, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJA001InvoiceType, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJA001CustomerID, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJA001CustomerName, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJA001CustomerClass, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJA001EntryOrder, moClass.moAppDB, moClass.moSysSession)
        cboSort.ListIndex = 0
        
        cboFormat.AddItem gsBuildString(kARZJA001Summary, moClass.moAppDB, moClass.moSysSession)
        cboFormat.AddItem gsBuildString(kARZJA001Detail, moClass.moAppDB, moClass.moSysSession)
        cboFormat.ListIndex = 0
        
        chk(1).Caption = gsBuildString(kARZJA001IncInvcCmnt, moClass.moAppDB, moClass.moSysSession)
        chk(0).Caption = gsBuildString(kARZJA001IncLICmnt, moClass.moAppDB, moClass.moSysSession)
    End If
    If TaskNumber = ktskCashReceipts Then
        cboSort.AddItem gsBuildString(kARZJB001custID, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001custName, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001custClass, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001custPaymentNo, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001EntryOrder, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001u1, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001u2, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001u3, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJB001u4, moClass.moAppDB, moClass.moSysSession)
        cboSort.ListIndex = 0
        
        chk(0).Caption = gsBuildString(kARZJB001IncInvcCmnt, moClass.moAppDB, moClass.moSysSession)
        chk(1).Caption = gsBuildString(kARZJB001IncPaymntCmnt, moClass.moAppDB, moClass.moSysSession)
        
        cboFormat.AddItem gsBuildString(kARZJA001Summary, moClass.moAppDB, moClass.moSysSession)
        cboFormat.AddItem gsBuildString(kARZJA001Detail, moClass.moAppDB, moClass.moSysSession)
        cboFormat.ListIndex = 0
    
    End If
    If TaskNumber = ktskARPmtAppl Then
        cboSort.AddItem gsBuildString(kARZJC001CustomerID, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001CustomerName, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001CustomerClass, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001AppliedFrom, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001AppliedToInvoice, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001EntryOrder, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001u1, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001u2, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001u3, moClass.moAppDB, moClass.moSysSession)
        cboSort.AddItem gsBuildString(kARZJC001u4, moClass.moAppDB, moClass.moSysSession)
        cboSort.ListIndex = 0

        cboFormat.Visible = False
        Label2.Visible = False
        
        chk(0).Caption = gsBuildString(kARZJC001IncInvcCmnt, moClass.moAppDB, moClass.moSysSession)
        chk(1).Caption = gsBuildString(kARZJC001IncPaymntCmnt, moClass.moAppDB, moClass.moSysSession)
        chk(2).Visible = False
    End If
    
    msBatchID = sGetBatchID(Me)
    GetReportPaths
        
    Set moContextMenu = New clsContextMenu
    With moContextMenu
        ' **PRESTO ** Set .Hook = Me.WinHook2
        Set .Form = frmRegister
        .Init
    End With
    
    sbrMain.Status = SOTA_SB_START

    Set sWorkTableCollection = New Collection
    'CUSTOMIZE:  Invoke Add method of sWorkTableCollection for each table which
    'will serve as a work table for the report.
    With sWorkTableCollection
        .Add kIN_WRK_table
    End With
    
    lRetval = lInitializeReport("AR", kIN_MC_SUMMARY_RPT_Name, "arzjadl1")
    If lRetval <> 0 Then
        moReport.ReportError lRetval
        GoTo badexit
    End If

    'AvaTax Integration - FormLoad
    mbTrackSTaxOnSales = gbGetValidBoolean(moClass.moAppDB.Lookup("TrackSTaxonSales", "tarOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    If mbTrackSTaxOnSales Then
        CheckAvaTax
    Else
        mbAvaTaxEnabled = False
    End If
    ' AvaTax Integration end

    'SGS-JMM 1/31/2011
    On Error Resume Next
    Set cypHelper = CreateObject("CypressIntegrationHelper.MAS500Form")
    If Err.Number = 0 Then
        With cypHelper
            .Init Me, moClass, msCompanyID
            .AddToolbarButton 0
        End With
    Else
        Set cypHelper = Nothing
    End If
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM

'+++ VB/Rig Begin Pop +++
        Exit Sub

badexit:

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    Dim iRet As Integer
    Dim lRetval As Long

    If Not moReport Is Nothing Then
        moReport.CleanupWorkTables
    End If
    
    If moClass.mlError = 0 Then
        'if a report preview window is active, query user to continue closing.
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
    End If
    
    'run this procedure to delete the row from the work tables used for register preview
    If TaskNumber = ktskInvoice Then
        With moClass.moAppDB
             .SetInParam mlSessionID
             .SetOutParam lRetval
             .ExecuteSP (kIN_SP_DELETEWRK)
             
             lRetval = .GetOutParam(2)
             .ReleaseParams
        End With
        
        If lRetval <> 0 Then
           WorkTableError mlSessionID, lRetval, "Delete"
           sbrMain.Status = SOTA_SB_START
        End If
    End If
    
    If UnloadMode <> vbFormCode Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
        'Do Nothing
        
    End Select
    
    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.Dispose
    Set cypHelper = Nothing
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    If Not mbCancelShutDown Then
        Set moClass = Nothing
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub hlpHelp_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    gDisplayFormLevelHelp Me

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "hlpHelp_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optRegister(Index), True
    #End If
'+++ End Customizer Code Push +++

    Select Case Index
    
        Case kPrintPost
        
            If optRegister(kPrintPost) Then
                chkDefer.Enabled = True
            Else
                chkDefer.Enabled = False
            End If
        Case kPrintPromptPost
        
            If optRegister(kPrintPromptPost) Then
                chkDefer.Enabled = True
            Else
                chkDefer.Enabled = False
            End If
        
        Case kPrintNoPost
        
            If optRegister(kPrintNoPost) Then
                chkDefer.Enabled = True
            Else
                chkDefer.Enabled = False
            End If
        
        Case KPostOnly
        
            If optRegister(KPostOnly) Then
                chkDefer = False
                chkDefer.Enabled = False
            Else
                chkDefer.Enabled = True
            End If
    
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "optRegister_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    HandleToolBarClick Button

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessRegister(lTaskNumber As Long, sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetval             As Long
    Dim oSessionID          As Long
    Dim iMsgReturn          As Integer
    Dim lReturnCode         As Long
    Dim sSortBy             As String
    Dim sFormulas()         As String
    Dim GroupingParams()    As String      'Use GroupingIndex enumeration for second index specification
    Dim iLoop               As Integer
    Dim prn                 As Integer
    Dim iCount              As Integer
    Dim mcSPInCreateTable   As New Collection
    Dim mcSPOutCreateTable  As New Collection
    Dim mcSPInDeleteTable   As New Collection
    Dim mcSPOutDeleteTable  As New Collection
    Dim sortNum             As Integer
    Dim lAcctRefTitle       As Long
    Dim sAcctRefTitle       As String
    
    ReDim sFormulas(1, 0)

    SetVariables lTaskNumber
    
    msReportPath = msARReportPath
    moReport.ReportFileName = msReportName
    
    sbrMain.Status = SOTA_SB_BUSY
        
    'create work table to print register from
    Screen.MousePointer = vbHourglass
                            
        Select Case TaskNumber
            Case ktskFinChgReg
                mcSPInCreateTable.Add mlBatchKey
                mcSPInCreateTable.Add msCompanyID
                mcSPInCreateTable.Add mlSessionID
                Select Case cboFormat.ListIndex
                    Case -1 To 0 '
                        mcSPInCreateTable.Add 0 'summary
                    Case 1
                        mcSPInCreateTable.Add 1 'detail
                End Select
                    
                mcSPOutCreateTable.Add mlRetValue
               
                'Setup report grouping defined by user selected sort
                ReDim GroupingParams(1, GroupingIndex.HighestIndex) As String
                GroupingParams(1, GroupingIndex.SortDirection) = "0"
                GroupingParams(1, GroupingIndex.TableName) = "tarInvcRegWrk"
               
                If miUseMultCurr = 1 Then
                    GroupingParams(1, GroupingIndex.GroupName) = "GH2"
                Else
                    GroupingParams(1, GroupingIndex.GroupName) = "GH1"
                End If
                
                sSortBy = "{tarInvcRegWrk.SessionID} = "
                
                Select Case cboSort.ListIndex
                   Case -1 To 0 '
                      sortNum = 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "TranID"
                   Case 1 ' Invoice Type fix
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "TranType"
                   Case 2
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustID"
                   Case 3
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustName"
                   Case 4
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"
                   Case 5
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "SeqNo"
                End Select
        
                ReDim sFormulas(1, 3)
                        
                sFormulas(0, 0) = "sortType"
                sFormulas(1, 0) = sortNum
                sFormulas(0, 1) = "invcComments"
                sFormulas(1, 1) = chk(1).Value
                sFormulas(0, 2) = "LineItemComments"
                sFormulas(1, 2) = chk(0).Value
                sFormulas(0, 3) = "ProjectAccounting"
                sFormulas(1, 3) = chk(2).Value
            
            Case ktskInvoice
                mcSPInCreateTable.Add mlBatchKey
                mcSPInCreateTable.Add msCompanyID
                mcSPInCreateTable.Add mlSessionID
                Select Case cboFormat.ListIndex
                    Case -1 To 0 '
                        mcSPInCreateTable.Add 0 'summary
                    Case 1
                        mcSPInCreateTable.Add 1 'detail
                End Select
                    
                mcSPOutCreateTable.Add mlRetValue
                
                'Setup report grouping defined by user selected sort
                ReDim GroupingParams(1, GroupingIndex.HighestIndex) As String
                GroupingParams(1, GroupingIndex.SortDirection) = "0"
                GroupingParams(1, GroupingIndex.TableName) = "tarInvcRegWrk"
               
                If miUseMultCurr = 1 Then
                    GroupingParams(1, GroupingIndex.GroupName) = "GH2"
                Else
                    GroupingParams(1, GroupingIndex.GroupName) = "GH1"
                End If
                
                sSortBy = "{tarInvcRegWrk.SessionID} = "
                
                Select Case cboSort.ListIndex
                   Case -1 To 0 '
                      sortNum = 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "TranID"
                   Case 1 ' Invoice Type fix
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "TranType"
                   Case 2
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustID"
                   Case 3
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustName"
                   Case 4
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"
                   Case 5
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "SeqNo"
                End Select
                        
                ReDim sFormulas(1, 6)
                        
                sFormulas(0, 0) = "sortType"
                sFormulas(1, 0) = sortNum
                sFormulas(0, 1) = "invcComments"
                sFormulas(1, 1) = chk(1).Value
                sFormulas(0, 2) = "LineItemComments"
                sFormulas(1, 2) = chk(0).Value
                sFormulas(0, 3) = "ProjectAccounting"
                sFormulas(1, 3) = chk(2).Value
                sFormulas(0, 4) = "InclTDinSales"
                sFormulas(1, 4) = miInclTDinSales
                sFormulas(0, 5) = "UnitPriceDecPlaces"
                sFormulas(1, 5) = miUnitPriceDecPlaces
                sFormulas(0, 6) = "QtyDecPlaces"
                sFormulas(1, 6) = miQtyDecPlaces
                
                   
                'Fill Formula for Custom-AcctRefTitle if AcctRefCodes are used by this company
                If (UCase$(Left$(moReport.ReportFileName, 8)) = "ARZJA002" _
                    Or UCase$(Left$(moReport.ReportFileName, 8)) = "ARZJA006") Then
                    If kAcctRefUsageNotUsed <> giGetValidInt(moClass.moAppDB.Lookup("AcctRefUsage", "tglOptions", "CompanyID = " & gsQuoted(msCompanyID))) Then
                        
                        ReDim Preserve sFormulas(1, 7)
                        
                        'Find the Localized Title from GLOptions
                        lAcctRefTitle = glGetValidLong(moClass.moAppDB.Lookup("AcctRefTitleStrNo", "tglOptions", "CompanyID = " & gsQuoted(msCompanyID)))
                        sAcctRefTitle = gsGetValidStr(moClass.moAppDB.Lookup("LocalText", "tsmLocalString", "StringNo = " & lAcctRefTitle & " AND LanguageID = " & moClass.moSysSession.Language))
                        sFormulas(0, 7) = "AcctRefTitle"
                        sFormulas(1, 7) = gsQuoted(sAcctRefTitle)
                    End If
                End If
                   
            Case ktskCashReceipts
                        
                sSortBy = "{tarCashRRegWrk.SessionID} = "
                
                'Setup report grouping defined by user selected sort
                ReDim GroupingParams(1, GroupingIndex.HighestIndex) As String
                GroupingParams(1, GroupingIndex.GroupName) = "GH2"  'Both MC and non-MC versions of rpt use second group
                GroupingParams(1, GroupingIndex.SortDirection) = "0"
                GroupingParams(1, GroupingIndex.TableName) = "tarCashRRegWrk"
                
                Select Case cboSort.ListIndex
                    Case -1 To 0 '
                        sortNum = 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "CustID"
                    Case 1
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "CustName"
                    Case 2
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"
                    Case 3
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "TranID"
                    Case 4
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "SeqNo"
                    Case 5
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "UserFld1"
                    Case 6
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "UserFld2"
                    Case 7
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "UserFld3"
                    Case 8
                        sortNum = cboSort.ListIndex + 1
                        GroupingParams(1, GroupingIndex.ColumnName) = "UserFld4"
                
                End Select
        
                mcSPInCreateTable.Add mlBatchKey
                mcSPInCreateTable.Add msCompanyID
                mcSPInCreateTable.Add mlSessionID
                Select Case cboFormat.ListIndex
                    Case -1 To 0 '
                        ReDim sFormulas(1, 1)
                        
                        sFormulas(0, 0) = "sortType"
                        sFormulas(0, 1) = "paymentCmnt"
                        sFormulas(1, 0) = sortNum
                        sFormulas(1, 1) = chk(1).Value

                        mcSPInCreateTable.Add 0 'summary
                    Case 1
                        
                        ReDim sFormulas(1, 2)
                        
                        sFormulas(0, 0) = "sortType"
                        sFormulas(1, 0) = sortNum
                        sFormulas(0, 1) = "paymentCmnt"
                        sFormulas(1, 1) = chk(1).Value
                        sFormulas(0, 2) = "invcComment"
                        sFormulas(1, 2) = chk(0).Value
                        mcSPInCreateTable.Add 1 'detail
                End Select
                    
                mcSPOutCreateTable.Add mlRetValue
            
            Case ktskARPmtAppl
            
                ReDim sFormulas(1, 3)
        
                'Setup report grouping defined by user selected sort
                ReDim GroupingParams(1, GroupingIndex.HighestIndex) As String
                GroupingParams(1, GroupingIndex.SortDirection) = "0"
                GroupingParams(1, GroupingIndex.TableName) = "tarPayApplRegWrk"
        
                If miUseMultCurr = 1 Then
                    GroupingParams(1, GroupingIndex.GroupName) = "GH2"
                Else
                    GroupingParams(1, GroupingIndex.GroupName) = "GH1"
                End If
                
                Select Case cboSort.ListIndex
                   Case -1 To 0 '
                      sortNum = 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustID"
                   Case 1
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustName"
                   Case 2
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"
                   Case 3
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "ApplyFromTranID"
                   Case 4
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "ApplyToTranID"
                   Case 5
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "SeqNo"
                   Case 6
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "UserFld1"
                   Case 7
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "UserFld2"
                   Case 8
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "UserFld3"
                   Case 9
                      sortNum = cboSort.ListIndex + 1
                      GroupingParams(1, GroupingIndex.ColumnName) = "UserFld4"
         
                End Select
        
                mcSPInCreateTable.Add mlBatchKey
                mcSPInCreateTable.Add msCompanyID
                mcSPInCreateTable.Add mlSessionID
                mcSPInCreateTable.Add sortNum
                mcSPOutCreateTable.Add mlRetValue
                
                sFormulas(0, 0) = "sortType"
                sFormulas(1, 0) = sortNum
                        
                sFormulas(0, 1) = "multicurrency"
                sFormulas(1, 1) = 1
                sSortBy = "{tarPayApplRegWrk.SessionID} = "
                        
                sFormulas(0, 2) = "payComments"
                sFormulas(1, 2) = chk(1).Value
                sFormulas(0, 3) = "invComments"
                sFormulas(1, 3) = chk(0).Value


        End Select
                            
        'Add user-displayed sort criteria to end of report
        iCount = UBound(sFormulas, 2) + 1
        ReDim Preserve sFormulas(1, iCount)
        sFormulas(0, iCount) = "SortBy1"
        sFormulas(1, iCount) = "'" & cboSort.Text & "'"
        
        If miMode <> kPostXXBatches Then
            lblStatusMsg = guLocalizedStrings.ProcessingRegister
        Else
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ProcessingRegister
        End If
        
        
        If TaskNumber = ktskARComm Then
            PrintCommissionRegister kAR_COMM_REGISTER2  '"ARZJD002.RPT"
        Else
            lRetval = lCreateWorkTable(msSPCreateTable, mcSPInCreateTable, mcSPOutCreateTable)
                    
            Screen.MousePointer = vbDefault
                
            If miPreview = 1 Then
                prn = 3
            Else
                prn = 1
            End If
    
            If miMode <> kPostXXBatches Then
                lblStatusMsg = guLocalizedStrings.PrintingRegister
            Else
                frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PrintingRegister
            End If
            
            moReport.ReportFileName = msReportName
            
            sSortBy = sSortBy & gsGetValidStr(mlSessionID)
            PrintRegister mlSessionID, prn, chkDefer.Value, cboReportDest.Text, Val(txtCopies.Text), sSortBy, sFormulas, GroupingParams()
            
            If TaskNumber = ktskCashReceipts Then
                If bHasChecks Then
                    moReport.ReportFileName = "ARCheckRegister.rpt"
                    moReport.ReportTitle1 = "Check Register"
                    
                    ReDim sFormulas(1, 1)
                    
                    sSortBy = "{tarCashRRegWrk.SessionID} = " & gsGetValidStr(mlSessionID)
                    
                    PrintRegister mlSessionID, prn, chkDefer.Value, cboReportDest.Text, Val(txtCopies.Text), sSortBy, sFormulas, , "{tarCashRRegWrk.RefNo}"
                End If
            End If
        End If
        
        'run register stored procedure to delete work tables
        If TaskNumber <> ktskInvoice Then
            Screen.MousePointer = vbHourglass
        
            mcSPInDeleteTable.Add mlSessionID
            mcSPOutDeleteTable.Add mlRetValue
        
            lRetval = lDeleteWorkTable(msSPDeleteTable, mcSPInDeleteTable, mcSPOutDeleteTable)
        
            If lRetval <> 0 Then
                WorkTableError oSessionID, lRetval, "Delete"
                sbrMain.Status = SOTA_SB_START
                Screen.MousePointer = vbDefault
                moClass.lUIActive = kChildObjectInactive
                Set mcSPInCreateTable = Nothing
                Set mcSPOutCreateTable = Nothing
                Set mcSPInDeleteTable = Nothing
                Set mcSPOutDeleteTable = Nothing
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            End If
        End If
        
        If miMode <> kPostXXBatches Then
            lblStatusMsg = guLocalizedStrings.RegisterPrinted
        Else
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.RegisterPrinted
        End If
        
        Screen.MousePointer = vbDefault
            
                
        'note for finance charges, do not print commission reg
        If TaskNumber = ktskInvoice Then
            If miUseSper = 1 Then
                PrintCommissionRegister kAR_COMM_REGISTER   '"ARZJD001.RPT"
            End If
        End If
        
        'print cash receipts regiter recap if integrated with CM
        If TaskNumber = ktskCashReceipts Then
            If miIntegrateWithCM = 1 Then
                If miUseMultCurr = 1 Then
                    'Reset the Report Title
                    SetReportTitle kAR_CR_RECEIPT_REG
                    PrintCRRecapRegister kAR_CR_RECEIPT_REG '"ARZJB005.RPT"
                Else
                    'Reset the Report Title
                    SetReportTitle kAR_CR_RECAP_REG
                    PrintCRRecapRegister kAR_CR_RECAP_REG   '"ARZJB006.RPT"
                End If
            End If
        End If
        
        sbrMain.Status = SOTA_SB_START
            
    Set mcSPInCreateTable = Nothing
    Set mcSPOutCreateTable = Nothing
    Set mcSPInDeleteTable = Nothing
    Set mcSPOutDeleteTable = Nothing
            
            
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bHasChecks() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim sSQL As String

    'Remove all non-checks and non-check reversals from the work table for the check register.
    sSQL = "DELETE tarCashRRegWrk WHERE TranType <> " & kTranTypeARRC & " AND TranType <> " & kTranTypeARRV & " AND SessionID = " & mlSessionID
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "DELETE tarCashRRegWrk FROM tarCashRRegWrk JOIN tarPendCustPmt ON tarCashRRegWrk.CustPmtKey = tarPendCustPmt.CustPmtKey "
    sSQL = sSQL & "JOIN tarCustPmt Rev on tarPendCustPmt.RevrsCustPmtKey = Rev.CustPmtKey "
    sSQL = sSQL & "WHERE Rev.TranType <> " & kTranTypeARRC
    moClass.moAppDB.ExecuteSQL sSQL
    
    If giGetValidInt(moClass.moAppDB.Lookup("COUNT(*)", "tarCashRRegWrk", "")) > 0 Then
        bHasChecks = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bHasChecks", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function iPostDocuments(lTaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetval                 As Long
    Dim iMsgReturn              As Integer
    Dim lReturnCode             As Long
    Dim response                As Integer
    Dim bGood                   As Boolean
    Dim iRegisterPrinted        As Integer
    Dim lStringNo               As Long
    Dim i                       As Integer


    'batch mode, no post exit
    If miMode = kPostXXBatches And miPost = 0 Then
        'if Print-Only, release the lock and inactivate the form --HC, 11/11/96
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        moClass.lUIActive = kChildObjectInactive
        'end of HC
        Exit Function
    End If
    
    'turn lock off during prompt for post
    If miMode = kInteractiveBatch And optRegister(kPrintPromptPost) Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
    End If
    
    msBatchID = sGetBatchID(Me)
    
    'single mode print but no post exit
    If miMode = kInteractiveBatch And optRegister(kPrintNoPost) Then
        'if Register-Only, release the lock --HC, 11/11/96
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        'end of HC
        
        SetUIStatus
        ResetBatchStatus
        Exit Function
    End If
    
    If muRegStat.iErrorStatus = 1 And miMode = kInteractiveBatch Then       ' fatal errors exist
        If miMode <> kPostXXBatches Then
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
        End If
        moClass.lUIActive = kChildObjectInactive
        Exit Function
    End If
    
    If (optRegister(kPrintPromptPost)) And miMode = kInteractiveBatch Then
        iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostRegister, guLocalizedStrings.JournalDesc)
        If iMsgReturn = kretNo Then
        
            If miMode = kInteractiveBatch Then
                ResetBatchStatus    'errors occurred in batch post, reset status from posting to balanced.
            End If
         
            SetUIStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        
        Else    'answered yes, set lock
        
            If miMode <> kPostXXBatches Then
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
            Else
                bGood = True
            End If
        
            If Not bGood Then
                'error in logical lock, exit
                mPostError = kFatalErrors
                miErrorsOccured = 1
                sbrMain.Status = SOTA_SB_START
                Screen.MousePointer = vbDefault
                ResetBatchStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        End If
    
    End If
        
    'Start posting......
    sbrMain.Status = SOTA_SB_BUSY
    Screen.MousePointer = vbHourglass
    
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.PostingModule2
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PostingModule2
    End If
        
    'if we entered here from a post only situation, need to set lock,
    'otherwise, lock is already set.
    If mbSkipedToPost = True Then
        'don't set locks if in batch mode
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
            If Not bGood Then
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
                sbrMain.Status = SOTA_SB_START
                Screen.MousePointer = vbDefault
                ResetBatchStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
        Else
            bGood = True
        End If
    Else
        mbSkipedToPost = False
        bGood = True
    End If
        
    If optRegister(KPostOnly) = True And miMode <> kPostXXBatches Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
    End If
        
        
    'register must be printed...
    If (iCheckRegisterPrinted(Me)) = 0 Then
        'unlock here
        mPostError = kFatalErrors
        miErrorsOccured = 1
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        ResetBatchStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    
    End If

    If bGood Then
        lRetval = lModPost(MODULE_POST, mlBatchKey, kSourceModule)
    Else
    
        'error in logical lock, exit
        mPostError = kFatalErrors
        miErrorsOccured = 1
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        ResetBatchStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
       
    'take care of resetting lock & status & error flags...
    If lRetval <> 0 Then
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        End If
    
        If miMode = kInteractiveBatch Then
            ResetBatchStatus    'errors occurred in batch post, reset status from posting to balanced.
        End If
        
        mPostError = kFatalErrors
        miErrorsOccured = 1
    End If
       
    'display errors in single mode only
    If lRetval <> 0 Then
        If miMode = kInteractiveBatch Then
        
            Select Case lRetval
                Case -1
                    '{0} Register cannot post.  Invalid data encountered.    Check for invalid accounts, closed periods and out-of-balance journals.
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
                    'Batch {0}: Register cannot post.  Invalid data encountered.
                    lStringNo = kVNoPost
                    iLogError mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 2
                'Any other error is logged in spciModPost
            End Select
            
            ' Errors found during Posting.  Would you like to print the Error Log?
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
            If iMsgReturn = kretOK Then
                PrintErrorLog
            End If
            
        End If
           
        mPostError = kFatalErrors
        miErrorsOccured = 1
           
        lblStatusMsg = guLocalizedStrings.PostingFailed
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        moClass.lUIActive = kChildObjectInactive
        ResetBatchStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
          
    End If
                       
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.GLPosting
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.GLPosting
    End If
                       
    lRetval = lPostGLTrans(GL_POST, mlBatchKey, kSourceModule)
   
    If lRetval <> 0 Then 'And miMode = kInteractiveBatch Then
        mPostError = kFatalErrors
        miErrorsOccured = 1
        
        If miMode <> kPostXXBatches Then
            Select Case lRetval
                Case -1
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
                    lStringNo = kVNoPost
            End Select
            
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
            If iMsgReturn = kretOK Then
                
                i = iLogError(mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 2)
                PrintErrorLog
            End If
        End If
        
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        End If
        
        lblStatusMsg = guLocalizedStrings.PostingFailed
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        moClass.lUIActive = kChildObjectInactive
        ResetBatchStatus

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    Else
        If miMode <> kPostXXBatches Then
            lblStatusMsg = guLocalizedStrings.GLPosted
        Else
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.GLPosted
        End If
            
        SetUIStatus
    End If
                        
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.ModuleCleanup
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ModuleCleanup
    End If

    lRetval = lModCleanup(MODULE_CLEANUP, mlBatchKey, kSourceModule)
   
    If lRetval <> 0 Then 'And miMode = kInteractiveBatch Then
        mPostError = kFatalErrors
        miErrorsOccured = 1
        
        If miMode <> kPostXXBatches Then
            Select Case lRetval
                Case -1
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgCannotPostFatalErrors, guLocalizedStrings.JournalDesc)
                    lStringNo = kVNoPost
            End Select
            
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgPostErrors, guLocalizedStrings.JournalDesc)
            If iMsgReturn = kretOK Then
                
                i = iLogError(mlBatchKey, lStringNo, msBatchID, "", "", "", "", 1, 2)
                PrintErrorLog
            End If
        End If
        
        If miMode <> kPostXXBatches Then
            bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
        End If
        
        lblStatusMsg = guLocalizedStrings.PostingFailed
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        moClass.lUIActive = kChildObjectInactive
        ResetBatchStatus

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    Else
        If miMode <> kPostXXBatches Then
            lblStatusMsg = guLocalizedStrings.PostingComplete
        Else
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PostingComplete
        End If
            
        SetUIStatus
    End If
                        
    If miMode <> kPostXXBatches Then
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
    End If

    ' AvaTax Integration - iPostDocuments
    If mbAvaTaxEnabled And mlBatchType = 1 Then
        moAvaTax.PostTax mlBatchKey
    End If
    ' AvaTax Integration end

    sbrMain.Status = SOTA_SB_START
    Screen.MousePointer = vbDefault
    moClass.lUIActive = kChildObjectInactive
    
    tbrMain.ButtonEnabled(kTbProceed) = False
    tbrMain.ButtonEnabled(kTbPreview) = False
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iPostDocuments", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub txtCopies_Change()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCopies, True
    #End If
'+++ End Customizer Code Push +++

    On Error Resume Next
    
    If IsNumeric(txtCopies.Text) Then
        moPrinter.Copies = txtCopies.Text
    End If
    
End Sub

Private Sub txtCopies_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCopies, True
    #End If
'+++ End Customizer Code Push +++
On Error GoTo ExpectedErrorRoutine:

  txtCopies = IIf((Val(txtCopies) > spnCopies.Max) Or (Val(txtCopies) < spnCopies.Min), spnCopies.Min, txtCopies)
  Exit Sub
  
ExpectedErrorRoutine:
  txtCopies = spnCopies.Min
  Err.Clear
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "txtCopies_LostFocus", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
On Error GoTo ExpectedErrorRoutine
On Error Resume Next

    gUnloadChildForms Me
    
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    Set moSotaObjects = Nothing
    Set moPrinter = Nothing
    Set sWorkTableCollection = Nothing
    Set moReport = Nothing
    Set moDDData = Nothing
    Set moPrinter = Nothing
    Set moContextMenu = Nothing
    
    If Not moAppDAS Is Nothing Then
        Set moAppDAS = Nothing
    End If
    
    If Not moOptions Is Nothing Then
        Set moOptions = Nothing
    End If

    ' AvaTax Integration - PerformCleanShutDown
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    ' AvaTax end

    'Exit this subroutine
    Exit Sub

ExpectedErrorRoutine:
    'Do nothing
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetUIStatus(Optional ByVal bInitialDisplay As Boolean = False)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bSet            As Boolean
    Dim sSQL            As String
    Dim rs              As DASRecordSet
    Dim iApproved       As Integer
    Dim iInvCount       As Integer
   
    muRegStat.iBatchStatus = 0
    muRegStat.iPrintStatus = 0
    muRegStat.iErrorStatus = 0
    bSet = False
    
    GetRegisterStatus moClass.moAppDB, msCompanyID, moClass.mlBatchKey, mlBatchType, muRegStat
    
    lblStatusMsg = ""
    lblStatus.Visible = True
        
        
    If muRegStat.iPrintStatus = 1 Then
        lblStatusMsg = lblStatusMsg & guLocalizedStrings.Printed
    Else
        lblStatusMsg = lblStatusMsg & guLocalizedStrings.NotPrinted
    End If
    
    If muRegStat.iErrorStatus = 1 Then
        lblStatusMsg = lblStatusMsg & " - " & guLocalizedStrings.FatalErrors
    End If
    
          
    'if batch is Inuse(1),OnHold(2) or OutOfBalance(3) print register only
    If muRegStat.iBatchStatus = 3 Or muRegStat.iBatchStatus = 1 _
       Or muRegStat.iBatchStatus = 2 Then
        If muRegStat.iBatchStatus = 3 Then
            lblStatusMsg = lblStatusMsg & guLocalizedStrings.NotBalanced
        End If
        optRegister(kPrintPost).Enabled = False
        optRegister(kPrintPromptPost).Enabled = False
        optRegister(KPostOnly).Enabled = False
        optRegister(kPrintNoPost) = True
        'in this case exit
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If
        
    'PA Integration: dnk only enable "Project Details" if PA is activated
    chk(kProjectAccounting).Enabled = gbIsModuleActive(moClass, kModulePA)
        
        
    If muRegStat.iPrintStatus = 0 And muRegStat.iErrorStatus = 1 Then
       'disable posting options
        optRegister(kPrintNoPost) = True
        optRegister(kPrintNoPost).Enabled = True
        optRegister(kPrintPost) = False
        optRegister(kPrintPromptPost) = False
        optRegister(kPrintPost).Enabled = False
        optRegister(kPrintPromptPost).Enabled = False
      
        optRegister(KPostOnly) = False
        optRegister(KPostOnly).Enabled = False
        bSet = True
    End If
        
     If muRegStat.iPrintStatus = 0 And muRegStat.iErrorStatus = 0 Then
       'disable posting options
        optRegister(kPrintNoPost).Enabled = True
        If mbRegChecked = False Then
            optRegister(kPrintPromptPost) = mbPostingAllowed
        End If
        
        optRegister(kPrintPost).Enabled = mbPostingAllowed
        optRegister(kPrintPromptPost).Enabled = mbPostingAllowed
        optRegister(KPostOnly) = False
        optRegister(KPostOnly).Enabled = False
        bSet = True
    End If
       
    If muRegStat.iPrintStatus = 1 And muRegStat.iBatchStatus = 4 Then
        optRegister(kPrintNoPost).Enabled = True
        optRegister(kPrintPost).Enabled = mbPostingAllowed
        optRegister(kPrintPromptPost).Enabled = mbPostingAllowed
        optRegister(KPostOnly).Enabled = mbPostingAllowed
        bSet = True
    End If
       
    'batch interrupted
    If muRegStat.iBatchStatus = 5 Or muRegStat.iBatchStatus = 7 Then
        optRegister(kPrintPost).Enabled = mbPostingAllowed
        optRegister(kPrintPromptPost).Enabled = mbPostingAllowed
        optRegister(kPrintNoPost).Enabled = True
        optRegister(KPostOnly).Enabled = mbPostingAllowed
        bSet = True
    End If

    'batch is posted
    If muRegStat.iBatchStatus = 6 Then
        optRegister(kPrintPost) = False
        optRegister(kPrintPromptPost) = False
        optRegister(kPrintNoPost) = False
        optRegister(KPostOnly) = False
        optRegister(kPrintPost).Enabled = False
        optRegister(kPrintPromptPost).Enabled = False
        optRegister(kPrintNoPost).Enabled = False
        optRegister(KPostOnly).Enabled = False
        lblStatusMsg = "Batch Posted"
        Me.Caption = msOrigCaption
        bSet = True
    End If

    mbRegChecked = True
        
    'I don't see how this can happen, bug 3779, I can't reproduce it but
    'if somehow it does occur, the following code will reset gui
    If bSet = False Then
       'disable posting options
        optRegister(kPrintNoPost).Enabled = True
        If mbRegChecked = False Then
            optRegister(kPrintPromptPost) = mbPostingAllowed
        End If
        
        optRegister(kPrintPost).Enabled = mbPostingAllowed
        optRegister(kPrintPromptPost).Enabled = mbPostingAllowed
        optRegister(KPostOnly) = False
        optRegister(KPostOnly).Enabled = False
    End If
    
    If bInitialDisplay Then
        If optRegister(kPrintPromptPost).Enabled = False Then
            optRegister(kPrintPromptPost) = False
        Else
            optRegister(kPrintPromptPost) = mbPostingAllowed
        End If
        
        If optRegister(kPrintPromptPost).Enabled = False Then
            optRegister(kPrintPromptPost) = False
        Else
            optRegister(kPrintPromptPost) = mbPostingAllowed
        End If
        
        If optRegister(KPostOnly).Enabled = False Then
            optRegister(KPostOnly) = False
        Else
            optRegister(KPostOnly) = mbPostingAllowed
        End If
            
    End If
    
    'If All posting options are disabled
    'Set kPrintNoPost option button to TRUE to prevent system to post
    If optRegister(kPrintPost).Enabled = False And _
        optRegister(kPrintPromptPost).Enabled = False And _
        optRegister(KPostOnly).Enabled = False And _
        optRegister(kPrintNoPost).Enabled = True Then

            optRegister(kPrintNoPost) = True
    End If


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetUIStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Sub SetVariables(mlTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'need better definition for BatchType

'Intellisol start
Dim iProjectDetails As Integer

iProjectDetails = chk(2)
'Intellisol end

    Select Case mlTaskNumber
    
        Case ktskARComm
            Me.Caption = "Commission Register"
            msWorkTable = "tarCommRegWrk"
            msSPCreateTable = "sparRegComm"
            msSPDeleteTable = "sparRegCommDel"
            mlBatchType = 4
        
        Case ktskCashReceipts
            Me.Caption = guLocalizedStrings.CRJournalDesc
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0 '
                    If miUseMultCurr = 1 Then
                        msReportName = kCR_MC_SUMMARY_RPT_Name
                    Else
                        msReportName = kCR_SUMMARY_RPT_Name
                    End If
                Case 1
                    If miUseMultCurr = 1 Then
                        msReportName = kCR_MC_DETAIL_RPT_Name
                    Else
                        msReportName = kCR_DETAIL_RPT_Name
                    End If
            End Select
            
            msWorkTable = kCR_WRK_table
            msSPCreateTable = kCR_SP_CREATEWRK
            msSPDeleteTable = kCR_SP_DELETEWRK
            msBatchType = kCR_batch_Type
            mlBatchType = 2
         
         Case ktskValidateCashReceipts
            Me.Caption = guLocalizedStrings.CRJournalDesc
            msReportName = kERROR_LOG_RPT_NAME
            msWorkTable = kCR_WRK_table
            msSPCreateTable = kCR_SP_CREATEWRK
            msSPDeleteTable = kCR_SP_DELETEWRK
            msBatchType = kCR_batch_Type
            
         Case ktskValidateARPmtAppl
            Me.Caption = guLocalizedStrings.ARPAJournalDesc
            msReportName = kERROR_LOG_RPT_NAME
         
         Case ktskValidateInvoice
            Me.Caption = guLocalizedStrings.INJournalDesc
            msReportName = kERROR_LOG_RPT_NAME
    
         Case ktskValidateComm
            Me.Caption = guLocalizedStrings.INJournalDesc
            msReportName = kERROR_LOG_RPT_NAME
    
         Case ktskInvoice
            mlBatchType = 1
            Me.Caption = guLocalizedStrings.INJournalDesc
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0 '
                  'Intellisol start
                  If iProjectDetails = vbChecked Then
                    If miUseMultCurr = 1 Then
                        msReportName = kIN_MC_SUMMARY_RPT_PA
                    Else
                        msReportName = kIN_SUMMARY_RPT_PA
                    End If
                  Else
                  'Intellisol end
                    If miUseMultCurr = 1 Then
                        msReportName = kIN_MC_SUMMARY_RPT_Name
                    Else
                        msReportName = kIN_SUMMARY_RPT_Name
                    End If
                  'Intellisol start
                  End If
                  'Intellisol end
                Case 1
                  'Intellisol start
                  If iProjectDetails = vbChecked Then
                    If miUseMultCurr = 1 Then
                        msReportName = kIN_MC_DETAIL_RPT_PA
                    Else
                        msReportName = kIN_DETAIL_RPT_PA
                    End If
                  Else
                  'Intellisol end
                    If miUseMultCurr = 1 Then
                        msReportName = kIN_MC_DETAIL_RPT_Name
                    Else
                        msReportName = kIN_DETAIL_RPT_Name
                    End If
                  'Intellisol start
                  End If
                  'Intellisol end
            End Select
            
            msWorkTable = kIN_WRK_table
            msSPCreateTable = kIN_SP_CREATEWRK
            msSPDeleteTable = kIN_SP_DELETEWRK
            msBatchType = kIN_batch_Type
            
        Case ktskFinChgReg
            mlBatchType = 1
            Me.Caption = "Finance Charge Register"
            Select Case cboFormat.ListIndex  ' Evaluate Number.
                Case -1 To 0 '
                    If miUseMultCurr = 1 Then
                        msReportName = kIN_MC_SUMMARY_RPT_Name
                    Else
                        msReportName = kIN_SUMMARY_RPT_Name
                    End If
                Case 1
                    If miUseMultCurr = 1 Then
                        msReportName = kIN_MC_DETAIL_RPT_Name
                    Else
                        msReportName = kIN_DETAIL_RPT_Name
                    End If
            End Select
            
            msWorkTable = kIN_WRK_table
            msSPCreateTable = kIN_SP_CREATEWRK
            msSPDeleteTable = kIN_SP_DELETEWRK
            msBatchType = kIN_batch_Type
        
        Case ktskARPmtAppl
            Me.Caption = guLocalizedStrings.ARPAJournalDesc
            If miUseMultCurr = 1 Then
                msReportName = kARPA_MC_RPT_Name
            Else
                msReportName = kARPA_RPT_Name
            End If
            
            msWorkTable = kARPA_WRK_table
            msSPCreateTable = kARPA_SP_CREATEWRK
            msSPDeleteTable = kARPA_SP_DELETEWRK
            msBatchType = kARPA_batch_Type
            mlBatchType = 3
        
        Case Else    'Incorrect Task Number
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
            mlTaskNumber

    End Select
    
    SetReportTitle msReportName
       
        
    gSetFormCaption Me, moClass.moSysSession

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetReportTitle(sReportName As String)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moReport Is Nothing Then
        Select Case sReportName
        
            'Error Log
            Case kERROR_LOG_RPT_NAME       '"ARZJA003.RPT"
                moReport.ReportTitle1 = "Invoice Register - Summary"
                moReport.ReportTitle2 = "Error Log Listing"

            'Invoice constants
            Case kIN_MC_SUMMARY_RPT_Name   '"ARZJA001.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Summary"
            Case kIN_MC_DETAIL_RPT_Name    '"ARZJA002.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Detail"
            Case kIN_SUMMARY_RPT_Name      '"ARZJA005.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Summary"
            Case kIN_DETAIL_RPT_Name       '"ARZJA006.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Detail"
            
            'Intellisol
            Case kIN_MC_SUMMARY_RPT_PA     '"ARZJA001_IPA.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Summary"
            Case kIN_MC_DETAIL_RPT_PA      '"ARZJA002_IPA.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Detail"
            Case kIN_SUMMARY_RPT_PA        '"ARZJA005_IPA.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Summary"
            Case kIN_DETAIL_RPT_PA         '"ARZJA006_IPA.RPT"
                moReport.ReportTitle1 = ComputeArzja001ReportName & " - Detail"
        
            'Cash Receipts constants
            Case kCR_MC_SUMMARY_RPT_Name   '"ARZJB001.RPT"
                moReport.ReportTitle1 = "Cash Receipts Register - Summary"
            Case kCR_MC_DETAIL_RPT_Name    '"arzjb002.rpt"
                moReport.ReportTitle1 = "Cash Receipts Register - Detail"
            Case kCR_SUMMARY_RPT_Name      '"ARZJB003.RPT"
                moReport.ReportTitle1 = "Cash Receipts Register - Summary"
            Case kCR_DETAIL_RPT_Name       '"arzjb004.rpt"
                moReport.ReportTitle1 = "Cash Receipts Register - Detail"
            
            'AR Payment Applications constants
            Case kARPA_MC_RPT_Name           '"ARZJC001.RPT"
                moReport.ReportTitle1 = "Payment Application Register"
            Case kARPA_RPT_Name              '"ARZJC002.RPT"
                moReport.ReportTitle1 = "Payment Application Register"
            
            Case kAR_COMM_REGISTER           '"ARZJD001.RPT"
                moReport.ReportTitle1 = "Invoice Register - Commission"
            Case kAR_COMM_REGISTER2          '"ARZJD002.RPT"
                moReport.ReportTitle1 = "Commission Register"
            
            'Print cash receipts regiter recap if integrated with CM
            Case kAR_CR_RECEIPT_REG          '"ARZJB005.RPT"
                moReport.ReportTitle1 = "Cash Receipts Register - Recap" '"Invoice Register - Recap"
            Case kAR_CR_RECAP_REG            '"ARZJB006.RPT"
                moReport.ReportTitle1 = "Cash Receipts Register - Recap" '"Invoice Register - Recap"
            
        End Select
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bRecordsExist(muRegStat As RegStatus) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim iMsgReturn      As Integer

    bRecordsExist = True
    'no batch records exist
    If muRegStat.iRecordsExist <> 1 Then
        If miMode <> kPostXXBatches Then
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgNoBatches, guLocalizedStrings.JournalDesc)
        End If
        
        bRecordsExist = False
    End If


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bRecordsExist", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub WorkTableError(oSessionID As Long, lRetval As Long, sType As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iMsgReturn As Integer

    If miMode <> kPostXXBatches Then
    Select Case sType
           
        Case "Create"
            
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgErrorGeneratingRegister, guLocalizedStrings.JournalDesc)
    
        Case "Delete"
        
            iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgErrorGeneratingRegister, guLocalizedStrings.JournalDesc)
        
    End Select
    End If


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WorkTableError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    FormHelpPrefix = "CIZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    WhatHelpPrefix = "CIZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Function GetUniqueTableKey(SysDB As Object, TableName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim key As Long

    With SysDB
        .SetInParam TableName
        .SetOutParam key
        .ExecuteSP ("spGetNextSurrogateKey")
        key = .GetOutParam(2)
        .ReleaseParams
    End With
    GetUniqueTableKey = key

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetUniqueTableKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iPrePost(lTaskNumber As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetval             As Long
    Dim oSessionID          As Long
    Dim iMsgReturn          As Integer
    Dim lReturnCode         As Long
    Dim i                   As Integer
    Dim bGood               As Boolean
    Dim lStringNo           As Long

    iPrePost = kFailure
    msBatchID = sGetBatchID(Me)
    SetVariables lTaskNumber
    
    sbrMain.Status = SOTA_SB_BUSY
   
    'create work table to print register from
    Screen.MousePointer = vbHourglass
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.PostingModule
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PostingModule
    End If
    
    'set lock if in single batch mode
    If miMode = kInteractiveBatch Then
        'changed lock to exclusive as per P. Steel 10/1/96
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, True)
    Else
        'multibatch mode, don't set locks so set variable to induce pre post
        bGood = True
    End If
    
    'failure in setting lock
    If miMode = kInteractiveBatch And bGood = False Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        moClass.lUIActive = kChildObjectInactive
        
        'release lock
        bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
         
    If bGood Then
        lRetval = lPostModuleTrans(PRE_POST, mlBatchKey, mlSessionID, kSourceModule)
    End If
    
    'keep logical lock ON until done COMPLETE process or error
   
    If miMode <> kPostXXBatches Then 'don't display messages in multi-batch mode
        If lRetval <> 0 Then
            Select Case lRetval
                Case -1:
                    iMsgReturn = giSotaMsgBox(Me, moClass.moSysSession, kmsgFatalErrorPrePost, guLocalizedStrings.JournalDesc)
                    lStringNo = kVFatalErrorPrePost
            End Select
            
            sbrMain.Status = SOTA_SB_START
            Screen.MousePointer = vbDefault
            moClass.lUIActive = kChildObjectInactive
            iPrePost = kFailure
            
            'error situation, release lock
            If miMode = kInteractiveBatch Then
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
                ResetBatchStatus
            End If
            
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    iPrePost = kSuccess
            
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.ModulePosted
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ModulePosted
    End If
        
    Screen.MousePointer = vbDefault
    sbrMain.Status = SOTA_SB_START
                     
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iPrePost", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Function iValidateData(lTaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetval             As Long
    Dim iMsgReturn          As Long
    Dim lReturnCode         As Long
    Dim response            As Integer
    Dim iLoop               As Integer
    Dim mcSPInValidate      As Collection
    Dim mcSPOutValidate     As Collection
    Dim sValSP              As String
    Dim iBatchType          As Integer
            
    iValidateData = 0

    Set mcSPInValidate = New Collection
    Set mcSPOutValidate = New Collection

    Select Case lTaskNumber
        Case ktskARComm
            SetVariables ktskValidateComm
        Case ktskFinChgReg
            SetVariables ktskValidateInvoice
        Case ktskInvoice
            SetVariables ktskValidateInvoice
        Case ktskCashReceipts
            SetVariables ktskValidateCashReceipts
        Case ktskARPmtAppl
            SetVariables ktskValidateARPmtAppl
    End Select
                            
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.Validating
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.Validating
    End If
    
    sbrMain.Status = SOTA_SB_BUSY
            
    'create work table to print register from
    Screen.MousePointer = vbHourglass
              
    Select Case lTaskNumber
        Case ktskFinChgReg
            iBatchType = kFCBatchType
        Case ktskInvoice
            iBatchType = kINBatchType
        Case ktskCashReceipts
            iBatchType = kCRBatchType
        Case ktskARPmtAppl
            iBatchType = kPABatchType
        Case ktskARComm
            iBatchType = kCOBatchType
    End Select
    
    sValSP = "spciValidatePosting"
    lRetval = lValidateData(sValSP, mlBatchKey, mlSessionID, kSourceModule, iBatchType)
        
    If lRetval = -1 Or lRetval = 2 Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        
        iValidateData = -1
        mPostError = kFatalErrors
        miErrorsOccured = 1
        Set mcSPInValidate = Nothing
        Set mcSPOutValidate = Nothing
               
        moClass.lUIActive = kChildObjectInactive
        ' reset global variables based on current task number
        SetVariables TaskNumber
        Exit Function
    ElseIf lRetval = 1 Then   'warnings only
        mPostError = 2
    End If

    ' if running cash rcpt validation, call pmt applications validation next
    If lTaskNumber = ktskCashReceipts Then
        iValidateData = iValidateData(ktskARPmtAppl)
    Else
        iValidateData = 1
    End If

    Screen.MousePointer = vbDefault

    ' reset global variables based on current task number
    SetVariables TaskNumber

      
    'check the Status of the batch records and determine the ui
    'Status of the controls on the form
                
    SetUIStatus
                
    sbrMain.Status = SOTA_SB_START
    
    Set mcSPInValidate = Nothing
    Set mcSPOutValidate = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iValidateData", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub PrintErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim Ret As Integer
    Dim sortby As String
    Dim prn As Integer
    Dim sFormulas(0, 0) As String 'Error log has no formulas to set
    
    
    ProcessErrorLog mlBatchKey
    
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.PrintError
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PrintError
    End If

    moReport.ReportPath = msReportPath
    moReport.ReportFileName = kERROR_LOG_RPT_NAME
    moReport.ReportTitle2 = "Error Log Listing"
    
    'Set title based on task number
    Select Case mlTaskNumber
    
        Case ktskFinChgBatch
            moReport.ReportTitle1 = "Invoice Register"
        Case ktskInvoice
            moReport.ReportTitle1 = "Invoice Register"
        Case ktskCashReceipts
            moReport.ReportTitle1 = "Cash Receipts Register"
        Case ktskARPmtAppl
            moReport.ReportTitle1 = "Payment Application Register"
        Case ktskFinChgReg
            moReport.ReportTitle1 = "Finance Charge Register"
        Case ktskARComm
            moReport.ReportTitle1 = "Commission Register"
        Case Else
            moReport.ReportTitle1 = "Invoice Register"
            
    End Select
    
    msCRWSelect = "{tciErrorLog.SessionID} = " & Str(mlBatchKey)
       
    If miPreview = 1 Then
        lStartReport kTbPreview, Me, False, sFormulas(), "", "", "", "", ""
    Else
        lStartReport kTbPrint, Me, False, sFormulas(), "", "", "", "", ""
    End If
        
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.PrintErrorCompleted
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.PrintErrorCompleted
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ProcessErrorLog(oSessionID As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetval As Long

    With moClass.moAppDB
        
        .SetInParam oSessionID
        .SetInParam moClass.moSysSession.Language
        .SetInParam moClass.moSysSession.CompanyID
        .SetOutParam lRetval

        .ExecuteSP ("spciPopulateErrorCmt")

        While .IsExecuting = True
            DoEvents
        Wend

        lRetval = .GetOutParam(4)
        
        .ReleaseParams
        
    End With
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ProcessErrorLog", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Function SetPrintedFlag(iFlag As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetval         As Long
    Dim response        As Integer

    With moClass.moAppDB
    
        .SetInParam mlBatchKey
        .SetInParam iFlag
        .SetInParam msCompanyID
        .SetOutParam lRetval
        
        ' run the stored procedure
        .ExecuteSP ("sparSetRegPrintFlg")
        
        Sleep 0&
        
        lRetval = .GetOutParam(4)
        .ReleaseParams

    End With
    
    
    If lRetval <> 0 Then
        If miMode <> kPostXXBatches Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        response = giSotaMsgBox(Me, moClass.moSysSession, kmsgFlagNotFound, guLocalizedStrings.JournalDesc)
        End If
    End If
    
    SetPrintedFlag = lRetval

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPrintedFlag", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function iAssignJrnlNo() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetval As Long
Dim response As Integer

    iAssignJrnlNo = kSuccess
    
    With moClass.moAppDB
    
        .SetInParam mlBatchKey
        .SetInParam 0               'not used anymore
        .SetInParam msCompanyID
        .SetOutParam lRetval
        
        ' run the stored procedure
        .ExecuteSP ("sparAssignJrnlNo")
        
        Sleep 0&
        
        lRetval = .GetOutParam(4)
        .ReleaseParams

    End With
    
    
    If lRetval <> 0 Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbDefault
        iAssignJrnlNo = kFailure
        response = giSotaMsgBox(Me, moClass.moSysSession, kmsgBatchNotFound, guLocalizedStrings.JournalDesc)
    End If
    


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iAssignJrnlNo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function PrintGLPostingRecap(lTaskNumber As Long) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lRetval As Long
Dim response As Integer
Dim sortby As String
Dim iLoop As Integer
Dim prn As Integer
Dim sFormulas() As String
Dim lAcctRefTitle As Long
Dim sAcctRefTitle As String


    If TaskNumber = ktskARComm And miCommExpOpt = 2 Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    'if ar options is set to no gl register, exit
    If miGLPostRgstrFormat = kGLPostRgstrFormat_None Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    ReDim sFormulas(1, 2)

    If miUseMultCurr = 1 Then
        moReport.ReportFileName = kGL_MC_SUMMARY_RPT  'summary MC
    Else
        moReport.ReportFileName = kGL_SUMMARY_RPT  'summary
    End If

    ReportPath = msGLReportPath
    moReport.ReportPath = ReportPath
    
    PrintGLPostingRecap = 0
    
    'Assign the titles of the GL posting register, which vary based upon the calling task.
    Select Case miGLPostRgstrFormat
        
        Case kGLPostRgstrFormat_Summary
        
            Select Case TaskNumber
            
                Case ktskARComm
                    moReport.ReportTitle1 = "Commission Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Summary"
                
                Case ktskARPmtAppl
                    moReport.ReportTitle1 = "Payment Application Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Summary"
                
                Case ktskCashReceipts
                    moReport.ReportTitle1 = "Cash Receipts Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Summary"
            
                Case ktskFinChgReg
                    moReport.ReportTitle1 = "Finance Charge Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Summary"
                
                Case Else  'Most likely invoice.  Use this as the default for unhandled cases.
                    moReport.ReportTitle1 = "Invoice Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Summary"
                    
            End Select
            
        Case kGLPostRgstrFormat_Detail
            
            Select Case TaskNumber
            
                Case ktskARComm
                    moReport.ReportTitle1 = "Commission Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Detail"
                
                Case ktskARPmtAppl
                    moReport.ReportTitle1 = "Payment Application Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Detail"
                
                Case ktskCashReceipts
                    moReport.ReportTitle1 = "Cash Receipts Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Detail"
            
                Case ktskFinChgReg
                    moReport.ReportTitle1 = "Finance Charge Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Detail"
            
                Case Else 'Most likely invoice.  Use this as the default for unhandled cases.
                    moReport.ReportTitle1 = "Invoice Register"
                    moReport.ReportTitle2 = "General Ledger Posting Register - Detail"
    
            End Select
    
    End Select
    
    sbrMain.Status = SOTA_SB_BUSY
            
    'Screen.MousePointer = vbHourglass
    If miMode <> kPostXXBatches Then
        lblStatusMsg = guLocalizedStrings.ProcessingGLReg
    Else
        frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.ProcessingGLReg
    End If
    
    If miPreview = 1 Then
        prn = 3
    Else
        prn = 1
    End If

    sFormulas(0, 0) = "module"
    sFormulas(1, 0) = """" & "Accounts Receivable" & """"

    sFormulas(0, 1) = "RptParent"
    If mlBatchType = 1 Then
        sFormulas(1, 1) = """" & "Invoice Register" & """"
    ElseIf mlBatchType = 2 Then
        sFormulas(1, 1) = """" & "Cash Receipts" & """"
    ElseIf mlBatchType = 3 Then
        sFormulas(1, 1) = """" & "Payment Applications" & """"
    End If

    sFormulas(0, 2) = "ReportFormat"
  
    If miGLPostRgstrFormat = 2 Then
        'Summary
        sFormulas(1, 2) = "0"
    Else
        'Detail
        sFormulas(1, 2) = "1"
        
        'Fill Formula for Custom-AcctRefTitle if AcctRefCodes are used by this company
        If kAcctRefUsageNotUsed <> giGetValidInt(moClass.moAppDB.Lookup("AcctRefUsage", "tglOptions", "CompanyID = " & gsQuoted(msCompanyID))) Then
            
            ReDim Preserve sFormulas(1, 3)
            
            'Find the Localized Title from GLOptions
            lAcctRefTitle = glGetValidLong(moClass.moAppDB.Lookup("AcctRefTitleStrNo", "tglOptions", "CompanyID = " & gsQuoted(msCompanyID)))
            sAcctRefTitle = gsGetValidStr(moClass.moAppDB.Lookup("LocalText", "tsmLocalString", "StringNo = " & lAcctRefTitle & " AND LanguageID = " & moClass.moSysSession.Language))
            sFormulas(0, 3) = "AcctRefTitle"
            sFormulas(1, 3) = gsQuoted(sAcctRefTitle)
          
        End If
    End If

        
    sortby = "{tglPosting.BatchKey} = " & Str(mlBatchKey)
    PrintRegister mlSessionID, prn, chkDefer.Value, cboReportDest.Text, _
                                            Val(txtCopies.Text), sortby, sFormulas
    
    
    If miMode <> kPostXXBatches Then
        If mPostError = kFatalErrors Then
            lblStatusMsg = guLocalizedStrings.NotPrinted & " - " & guLocalizedStrings.FatalErrors
        Else
            lblStatusMsg = "GL Register completed."
        End If
    Else
        If mPostError = kFatalErrors Then
            frmPrintPostStatus.lblStatusMsg = guLocalizedStrings.NotPrinted & " - " & guLocalizedStrings.FatalErrors
        Else
            frmPrintPostStatus.lblStatusMsg = "GL Register completed."
        End If
    End If
    Me.Caption = msOrigCaption
   
    Screen.MousePointer = vbDefault
       
    'SetUIStatus
                
    sbrMain.Status = SOTA_SB_START

    ReportPath = msARReportPath
       
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintGLPostingRecap", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function setDates() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL            As String
    Dim rs              As DASRecordSet
    Dim Status          As Integer
    Dim response        As Integer
    Dim i               As Integer

    setDates = kSuccess
    
    sSQL = "Select Status from tglFiscalPeriod "
    sSQL = sSQL & " where StartDate <=  " & gsQuoted(gsFormatDateToDB(Str(mdPostDate))) & " and CompanyID = " & gsQuoted(msCompanyID) & " and EndDate in ( select distinct EndDate from tglFiscalPeriod where EndDate >= " & gsQuoted(gsFormatDateToDB(Str(mdPostDate))) & "  )"
       
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        msBatchID = sGetBatchID(Me)
        Set rs = Nothing
        setDates = kSuccess
        mPostError = kFatalErrors
        i = iLogError(mlBatchKey, kVNonPer, msBatchID, "", "", "", "", 1, 2)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    Status = rs.Field("Status")
    
    Set rs = Nothing
           
    If Status = 2 Then
        msBatchID = sGetBatchID(Me)
        i = iLogError(mlBatchKey, kVClosedPer, msBatchID, "", "", "", "", 1, 2)
        setDates = kSuccess
        mPostError = kFatalErrors
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "setDates", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub UnloadCollection(ByRef inCollection As Collection)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iLoop As Integer

    For iLoop = 1 To inCollection.Count
        inCollection.Remove iLoop
    Next iLoop

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UnloadCollection", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub GetHomeCurrency()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL        As String
    Dim rs          As DASRecordSet

    sSQL = "Select CurrID from tsmCompany where CompanyID = "
    sSQL = sSQL & gsQuoted(msCompanyID)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        Set rs = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Sub
    End If

    msHomeCurrency = rs.Field("CurrID")
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetHomeCurrency", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function CheckBalance() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetval         As Long

    CheckBalance = kSuccess
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam mlSessionID
        .SetOutParam lRetval
        
        .ExecuteSP ("sparCheckBalance")         ' run the stored procedure
        
        Sleep 0&
        lRetval = .GetOutParam(3)
        .ReleaseParams

    End With
    
    If lRetval = kFailure Then
        mPostError = kFatalErrors
        miErrorsOccured = 1
    End If
    
    CheckBalance = lRetval
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckBalance", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Skip +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
Dim Ret As Integer
Dim iPostStatus As Integer
Dim bGood As Boolean

    On Error GoTo ExpectedErrorRoutine

    'flag indicating we went directly to posting (PostStatus >= 250)
    mbSkipedToPost = False
    
    Select Case sKey
        Case kTbPreview
            miPreview = 1
    End Select
    
    Select Case sKey
        Case kTbProceed, kTbPreview
        
            'clean up previous errors in case of reprint
            cleanErrorLog
            miErrorsOccured = 0
            mPostError = kNoErrors
            mlLockID = 0
            
            moClass.lUIActive = kChildObjectActive
            
            SetVariables mlTaskNumber
            
            ReportPath = msARReportPath
            moReport.ReportPath = msARReportPath
            
            'Show print dialog if Post Batches and first time through. HBS.
            If miMode = kPostXXBatches And Not gbSkipPrintDialog Then
                If gbCancelPrint Then
                    GoTo Cleanup
                Else
                    gbSkipPrintDialog = True    '   subsequent batches don't show dialog. HBS.
                End If
            End If
            
            If miMode = kPostXXBatches Then
                If lSetBatchType = kFailure Then
                    Exit Sub
                End If
            End If
            
            lblStatusMsg = guLocalizedStrings.CheckingBatch
                    
            iPostStatus = iCheckPostStatus(Me)
            
            'batch already posted, nothing to do
            If iPostStatus = 500 Then
                Ret = giSotaMsgBox(Me, moClass.moSysSession, kmsgAlreadyPosted, guLocalizedStrings.JournalDesc)
                Exit Sub
            End If
            
            If setDates = kFailure Then
                moClass.miShutDownRequester = kUnloadSelfShutDown
                Me.Hide
                If miMode = kPostXXBatches Then
                    miErrorsOccured = 1
                    ResetBatchStatus
                End If
                Exit Sub
            End If

            'validate data
            Ret = iValidateData(TaskNumber)
                                                 
            If (optRegister(KPostOnly) = True And muRegStat.iPrintStatus = 1 And miMode <> kPostXXBatches) Or _
               (mbPostOnly = True And miMode = kPostXXBatches) Then
                
                SetUIStatus
                
                If mPostError = kNoErrors Or mPostError = kWarnings Then
                    
                    If CheckBalance = kSuccess Then
                    
                        If iPostDocuments(TaskNumber) Then
                            lblStatusMsg = guLocalizedStrings.posted
                            sbrMain.Status = SOTA_SB_START
                            Screen.MousePointer = vbDefault
                        End If
                    
                    End If
                
                Else
                    PrintErrorLog
                    cleanErrorLog
                    If miMode = kPostXXBatches Then
                        ResetBatchStatus
                    End If
                End If
                Exit Sub
            End If
            
            If iPrePost(TaskNumber) = kFailure Then
                mPostError = kFatalErrors
            Else
                'Check for Non-fatal errors during pre-post
                If mPostError = kNoErrors And bErrorExists Then
                    mPostError = kWarnings
                End If
            End If
            
            'validate GL currencies in tglPosting
            ValidateGLCurrencies
            
            'reset register Printed flag in tciBatchSySDetl
            Ret = SetPrintedFlag(0)
                            
            ' process and print register
            ProcessRegister TaskNumber, sKey
            
            'if producing invoice register, process cash receipts if they exist
            If lCheckInvoiceRegister(sKey) = kExitSub Then
                Exit Sub
            End If
                    
            'check register balance for errors
            CheckBalance

            
            If (mPostError = kFatalErrors) Then
                PrintErrorLog
                cleanErrorLog
                bGood = bSetPostStatus(mlBatchKey, 1, 0, mlLockID, msBatchID, False)
                If miMode = kPostXXBatches Then
                    ResetBatchStatus
                End If
                'ReleaseLock
            Else
                SetPrintedFlag (1)  'clean register, set printed flag
            End If
                                
            'if just warnings, print log only
            If (mPostError = kWarnings) Then
                PrintErrorLog
                cleanErrorLog
            End If

            ' update PostError status in tciBatchLog
            If mPostError = kFatalErrors Then
                SetPostError 2
            ElseIf mPostError = kWarnings Then
                SetPostError 1
            Else
                SetPostError 0
            End If

            SetUIStatus
            
            Ret = PrintGLPostingRecap(TaskNumber)
               
            'SGS-JMM 1/31/2011
            On Error Resume Next
            If Not (cypHelper Is Nothing) Then
                If cypHelper.IsCypressPrinter(cboReportDest.Text) Then
                    cypHelper.ShowViewer
                End If
            End If
            On Error GoTo ExpectedErrorRoutine
            'End SGS-JMM 1/31/2011
                              
PostOnly:
            'If no errors found during validation
            If iPostDocuments(TaskNumber) Then
                lblStatusMsg = guLocalizedStrings.posted
                sbrMain.Status = SOTA_SB_START
                Screen.MousePointer = vbDefault
            End If
                
            If (mPostError = kNoErrors Or mPostError = kWarnings) Then
                'If no errors found during validation
                If miPreview = 1 Then
                    miPreview = 0   'reset Preview flag
                End If
                
            Else
                miPreview = 0
                mPostError = kNoErrors 'reset flag
            End If
                           
        Case kTbClose
            giCollectionDel moClass.moFramework, moSotaObjects, -1

        Case kTbSave
        
        Case kTbCopyFrom
        
        Case kTbDelete
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case kTbPrint
                
        Case Else
            'moToolbar.GenericHandler sKey, Me, moDMForm, moClass
    End Select
    
    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.HandleToolBarClick sKey, 1
    On Error GoTo ExpectedErrorRoutine
    'End SGS-JMM 1/31/2011
    
Cleanup:    '   HBS
    Exit Sub

ExpectedErrorRoutine:
    'March 15, 2000: APK: Defect #17857
    If miMode = 0 Then
        giCollectionDel moClass.moFramework, moSotaObjects, 84607754 & "AOF"
    End If
    
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "HandleToolbarClick"
    gClearSotaErr
End Sub

Public Function cleanErrorLog()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String

    sSQL = "delete tciErrorLog where BatchKey = " & Str(mlBatchKey)
    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cleanErrorLog", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function bErrorExists() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bErrorExists = False
    
    If glGetValidLong(moClass.moAppDB.Lookup("1", "tciErrorLog", "BatchKey = " & mlBatchKey)) = 1 Then
        bErrorExists = True
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bErrorExists", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub PrintCommissionRegister(sRptName As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL            As String
    Dim rs              As DASRecordSet
    Dim lRet            As Long
    Dim GroupingParams() As String      'Use GroupingIndex enumeration for second index specification
    Dim sSortBy         As String
    Dim sFormulas()     As String
    Dim sortNum         As Integer
    Dim caller          As Integer

    CleanCommReg
    
    moReport.ReportFileName = sRptName
    SetReportTitle sRptName

    ReDim sFormulas(0, 0)
    
    If TaskNumber = ktskARComm Then
        caller = 2
    Else
        caller = 1
    End If
    
    
    'do not product commission register is invoice posting and commission payment
    If caller = 1 And miCommExpOpt = 2 Then
        Exit Sub
    End If
    
    
    sbrMain.Status = SOTA_SB_BUSY
     
    If miMode <> kPostXXBatches Then
        lblStatusMsg = "Processing Commission Register" 'guLocalizedStrings.ProcessingGLReg
    Else
        frmPrintPostStatus.lblStatusMsg = "Processing Commission Register" 'guLocalizedStrings.ProcessingGLReg
    End If
    
    'Run stored proc to populate commission register work table
      With moClass.moAppDB
          .SetInParam mlBatchKey
          .SetInParam msCompanyID
          .SetInParam mlSessionID
          .SetInParam caller
          .SetOutParam lRet
          .ExecuteSP ("sparRegComm")
          lRet = .GetOutParam(5)
          .ReleaseParams
      End With
    
    msWorkTable = "tarCommRegWrk"
             
    Screen.MousePointer = vbHourglass
    If miMode <> kPostXXBatches Then
        lblStatusMsg = "Printing Commission Register" 'guLocalizedStrings.ProcessingGLReg
    Else
        frmPrintPostStatus.lblStatusMsg = "Printing Commission Register" 'guLocalizedStrings.ProcessingGLReg
    End If
                 
    sSortBy = "{tarCommRegWrk.SessionID} = " & Str(mlSessionID)
    
    'Setup a report grouping specification
    ReDim GroupingParams(1, GroupingIndex.HighestIndex)
    GroupingParams(1, GroupingIndex.GroupName) = "GH1"
    GroupingParams(1, GroupingIndex.TableName) = msWorkTable
    GroupingParams(1, GroupingIndex.SortDirection) = "0"  'Ascending order
               
    If TaskNumber <> ktskARComm Then
        'called from invoice register...
        Select Case cboSort.ListIndex
            Case -1 To 0 '
                GroupingParams(1, GroupingIndex.ColumnName) = "TranID"
                sortNum = 1
           Case 1 ' Invoice Type fix
                sortNum = cboSort.ListIndex + 1
                'This grouping is supposed to be for Invoice Type, but the invoice type is not
                'currently available in the work table, so another field was chosen by design
                'to be used until invoice type is added to the work table.
                GroupingParams(1, GroupingIndex.ColumnName) = "SubjSales"
           Case 2
              sortNum = cboSort.ListIndex + 1
              GroupingParams(1, GroupingIndex.ColumnName) = "CustID"
           Case 3
                sortNum = cboSort.ListIndex + 1
                GroupingParams(1, GroupingIndex.ColumnName) = "CustName"
           Case 4
                sortNum = cboSort.ListIndex + 1
                GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"
           Case 5   'this condition is here only for compatibility with invoice register.
                    'The commission register does not have a seq no so the sort is meaningless.
                sortNum = cboSort.ListIndex + 1
                GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"

        End Select
    Else
        Select Case cboSort.ListIndex
           Case -1 To 0 '
              GroupingParams(1, GroupingIndex.ColumnName) = "SperID"
              sortNum = 1
           Case 1 ' should be SalesTerritory, need to fix table
              sortNum = cboSort.ListIndex + 1
              GroupingParams(1, GroupingIndex.ColumnName) = "SperID"
           Case 2
              sortNum = cboSort.ListIndex + 1
              GroupingParams(1, GroupingIndex.ColumnName) = "CustID"
           Case 3
              sortNum = cboSort.ListIndex + 1
              GroupingParams(1, GroupingIndex.ColumnName) = "CustName"
           Case 4
              sortNum = cboSort.ListIndex + 1
              GroupingParams(1, GroupingIndex.ColumnName) = "CustClassID"
           Case 5
              sortNum = cboSort.ListIndex + 1
              GroupingParams(1, GroupingIndex.ColumnName) = "TranID"
              
        End Select
    End If
    
    'Update report formulas
    ReDim sFormulas(1, 1)
                   
    sFormulas(0, 0) = "sortType"
    sFormulas(1, 0) = sortNum
    sFormulas(0, 1) = "SortBy1"
    sFormulas(1, 1) = "'" & cboSort.Text & "'"
             
    
    msCRWSelect = sSortBy
    
    If miPreview = 1 Then
        lStartReport kTbPreview, Me, False, sFormulas(), "", "", "", "", "", GroupingParams()
    Else
        lStartReport kTbPrint, Me, False, sFormulas(), "", "", "", "", "", GroupingParams()
    End If
    
    Screen.MousePointer = vbDefault
                
    sbrMain.Status = SOTA_SB_START
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintCommissionRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub PrintCRRecapRegister(sRptName As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL            As String
    Dim rs              As DASRecordSet
    Dim lRet            As Long
    Dim iPrn            As Integer
    Dim sSortBy         As String
    Dim sFormulas()     As String
    Dim sortNum         As Integer
    Dim caller          As Integer

    ReDim sFormulas(0, 0)
    sbrMain.Status = SOTA_SB_BUSY
     
    If miMode <> kPostXXBatches Then
        lblStatusMsg = "Processing Cash Receipts Recap Register" 'guLocalizedStrings.ProcessingGLReg
    Else
        frmPrintPostStatus.lblStatusMsg = "Processing Cash Receipts Recap Register" 'guLocalizedStrings.ProcessingGLReg
    End If
    
    'Run stored proc to populate commission register work table
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam msCompanyID
        .SetInParam mlSessionID
        .SetOutParam lRet
        .ExecuteSP ("sparRegCRRecap")
        lRet = .GetOutParam(4)
        .ReleaseParams
    End With
    
    If lRet = 2 And mPostError = kNoErrors Then
        mPostError = kWarnings
    End If
    
    msWorkTable = "tcmCashRcptSumWrk"
             
    Screen.MousePointer = vbHourglass
    If miMode <> kPostXXBatches Then
        lblStatusMsg = "Printing Cash Receipts Recap Register" 'guLocalizedStrings.ProcessingGLReg
    Else
        frmPrintPostStatus.lblStatusMsg = "Printing Cash Receipts Recap Register" 'guLocalizedStrings.ProcessingGLReg
    End If
    
    If miPreview = 1 Then
        iPrn = 3
    Else
        iPrn = 1
    End If
                 
    sSortBy = "{tcmCashRcptSumWrk.SessionID} = " & gsGetValidStr(mlSessionID)
            
    moReport.ReportFileName = sRptName

    PrintRegister mlSessionID, iPrn, chkDefer.Value, cboReportDest.Text, _
                                            Val(txtCopies.Text), sSortBy, sFormulas
    CleanCRREcapReg

    Screen.MousePointer = vbDefault
    sbrMain.Status = SOTA_SB_START
        
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintCRRecapRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ResetInterruptedStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
    sSQL = "UPDATE tciBatchLog SET Status = 5 WHERE BatchKey = " & Str(mlBatchKey) & "  AND Status = 7"
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetInterruptedStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetPostError(iPostError As Integer)
'+++ VB/Rig Skip +++
    Dim sSQL As String

    On Error GoTo ExpectedErrorRoutine

    sSQL = "UPDATE tciBatchLog SET PostError = " & iPostError
    sSQL = sSQL & " WHERE BatchKey = " & Str(mlBatchKey)
    moClass.moAppDB.ExecuteSQL sSQL

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "SetPostError"
    gClearSotaErr

End Sub

Private Sub ResetBatchStatus()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

Dim sSQL As String
On Error GoTo ExpectedErrorRoutine
    sSQL = "UPDATE tciBatchLog SET Status = 4 WHERE BatchKey = " & Str(mlBatchKey) & "  AND Status = 5"
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetBatchStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function lSetBatchType() As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim sSQL        As String
    Dim iBatchType  As Integer
    Dim rs          As DASRecordSet

    lSetBatchType = kFailure
    On Error GoTo ExpectedErrorRoutine
    
   sSQL = "SELECT BatchType FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    
   
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        Set rs = Nothing
        
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    iBatchType = rs.Field("BatchType")
        
    Select Case iBatchType
        Case 505
            mlBatchType = 4
            TaskNumber = ktskARComm
        Case 504
            mlBatchType = 3
            TaskNumber = ktskARPmtAppl
        Case 503
            mlBatchType = 2
            TaskNumber = ktskCashReceipts
        Case 501
            mlBatchType = 1
            TaskNumber = ktskInvoice
        Case 502
            mlBatchType = 1
            TaskNumber = ktskFinChgReg
            
    End Select
   Set rs = Nothing
   
   lSetBatchType = kSuccess
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
   Exit Function
   
ExpectedErrorRoutine:
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lSetBatchType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iCheckRegisterPrinted(frm As Form) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL        As String
    Dim rs          As DASRecordSet

    iCheckRegisterPrinted = 0
    
    sSQL = "SELECT RgstrPrinted FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iCheckRegisterPrinted = rs.Field("RgstrPrinted")
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckRegisterPrinted", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sGetBatchID(frm As Form) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL        As String
    Dim rs          As DASRecordSet

    sGetBatchID = ""
    
    sSQL = "SELECT BatchID FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        sGetBatchID = rs.Field("BatchID")
    End If
    
    Set rs = Nothing
    

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetBatchID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Private Function iCheckPostStatus(frm As Form) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL        As String
    Dim rs          As DASRecordSet

    iCheckPostStatus = 0
    
    sSQL = "SELECT PostStatus FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        iCheckPostStatus = rs.Field("PostStatus")
    End If
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iCheckPostStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'this functionality will be put into the stored proc
Private Sub CleanCommReg()
    Dim sSQL        As String
    Dim rs          As DASRecordSet

    On Error GoTo ExpectedErrorRoutine
        
    sSQL = "DELETE tarCommRegWrk WHERE SessionID = " & Str(mlSessionID)
    moClass.moAppDB.ExecuteSQL sSQL
    Exit Sub

ExpectedErrorRoutine:
    gClearSotaErr
    Exit Sub

End Sub

Private Sub CleanCRREcapReg()
    Dim sSQL        As String
    Dim rs          As DASRecordSet

    On Error GoTo ExpectedErrorRoutine
        
    sSQL = "DELETE tcmCashRcptSumWrk WHERE SessionID = " & Str(mlSessionID)
    moClass.moAppDB.ExecuteSQL sSQL
    Exit Sub

ExpectedErrorRoutine:
    gClearSotaErr
    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CleanCRREcapReg", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function iLogError(lBatchKey As Long, iStringNo As Long, s1 As String, _
    s2 As String, s3 As String, s4 As String, s5 As String, iErrorType As Integer, _
    iSeverity As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL        As String
    Dim rs          As DASRecordSet
    Dim lRet        As Long
    Dim iEntryNo    As Integer

    iLogError = 0
    
    'first get max entry no for this batch
    sSQL = "SELECT MAX(EntryNo) MaxEntryNo FROM tciErrorLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        If Not IsNull(rs.Field("MaxEntryNo")) Then
        iEntryNo = rs.Field("MaxEntryNo") + 1
        Else
            iEntryNo = 1
        End If
    End If
    
    Set rs = Nothing

         
    'Run stored proc to populate commission register work table
      With moClass.moAppDB
          .SetInParam lBatchKey
          .SetInParam iEntryNo
          .SetInParam iStringNo
          .SetInParam s1
          .SetInParam s2
          .SetInParam s3
          .SetInParam s4
          .SetInParam s5
          .SetInParam iErrorType
          .SetInParam iSeverity
          
          .SetOutParam lRet
          .ExecuteSP ("spglCreateErrSuspenseLog")
          lRet = .GetOutParam(11)
          .ReleaseParams
      End With
     
    iLogError = lRet
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iLogError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmRegister"
End Function

Private Sub GetModVariables()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL    As String
    Dim rs      As DASRecordSet
    
    sSQL = "SELECT UseSper, UseMultCurr, GLPostRgstrFormat, IntegrateWithCM, CommExpOpt, InclTradeDiscInSls "
    sSQL = sSQL & "FROM tarOptions WHERE CompanyID = " & gsQuoted(msCompanyID)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If rs Is Nothing Then Exit Sub
    
    If Not rs.IsEOF Then
        miUseMultCurr = rs.Field("UseMultCurr")
        miGLPostRgstrFormat = rs.Field("GLPostRgstrFormat")
        miIntegrateWithCM = rs.Field("IntegrateWithCM")
        miUseSper = rs.Field("UseSper")
        miCommExpOpt = rs.Field("CommExpOpt")
        miInclTDinSales = rs.Field("InclTradeDiscInSls")
    End If

    rs.Close
    Set rs = Nothing
    
    sSQL = "SELECT Active FROM tsmCompanyModule WHERE CompanyID = " & gsQuoted(msCompanyID) & "AND ModuleNo = " & kModulePA
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If rs Is Nothing Then Exit Sub
    
    miUnitPriceDecPlaces = moClass.moAppDB.Lookup("UnitPriceDecPlaces", "tciOptions", "CompanyID = " & gsQuoted(msCompanyID))
    miQtyDecPlaces = moClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", "CompanyID = " & gsQuoted(msCompanyID))
     
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetModVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub GetReportPaths()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iAttr       As Integer
    Dim lRet        As Long

    msARReportPath = moClass.moSysSession.ModuleReportPath("AR")
    lRet = lValidateReportPath(msARReportPath)
    msGLReportPath = moClass.moSysSession.ModuleReportPath("GL")
    lRet = lValidateReportPath(msGLReportPath)
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetReportPaths", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lValidateReportPath(sPath As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine
Dim iAttr As Integer

    lValidateReportPath = kFailure
    
    If Not Len(sPath) > 0 Then
        GoTo ExpectedErrorRoutine
    Else
        If Right(sPath, 1) = BACKSLASH Then
            iAttr = GetAttr(Mid(sPath, 1, Len(sPath) - 1)) And vbDirectory
        Else
            iAttr = GetAttr(sPath) And vbDirectory
            sPath = sPath & BACKSLASH
        End If
        If iAttr <> vbDirectory Then
            MsgBox "Report Path: " & sPath & " is not a valid directory on this workstation;" & vbCr & " may not exist on this client workstation", vbCritical, "frmRegister.lValidateReportPath"
            GoTo ExpectedErrorRoutine
        End If
    End If
    
    Exit Function

ExpectedErrorRoutine:
    gClearSotaErr
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateReportPath", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub ValidateGLCurrencies()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRet        As Long

    lRet = 0
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam msHomeCurrency
        .SetInParam msCompanyID
        .SetOutParam lRet
        .ExecuteSP ("spARValGLCurr")
        lRet = .GetOutParam(4)
        .ReleaseParams
    End With
     

    If lRet = -1 Then
        mPostError = kFatalErrors
        miErrorsOccured = 1
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValidateGLCurrencies", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub ValidateGainLoss()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRet        As Long

    lRet = 0
    
    With moClass.moAppDB
        .SetInParam mlBatchKey
        .SetInParam mlSessionID
        .SetInParam msCompanyID
        .SetOutParam lRet
        .ExecuteSP ("sparValRealGain")
        lRet = .GetOutParam(4)
        .ReleaseParams
    End With
     

    If lRet = -1 Then
        mPostError = kFatalErrors
        miErrorsOccured = 1
    End If
        
     

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ValidateGainLoss", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'invoice register may contain downpayments, in this case process cash receipts register also
Public Function lCheckInvoiceRegister(sKey As String) As Long
'+++ VB/Rig Skip +++
    Dim sSQL                As String
    Dim rs                  As DASRecordSet
    Dim cnt                 As Integer
    Dim iRet                As Integer
    Dim lOrigTaskNumber     As Long

    lOrigTaskNumber = TaskNumber
    
    If TaskNumber <> ktskInvoice Then
        lCheckInvoiceRegister = kSuccess
        Exit Function
    End If
    
    lCheckInvoiceRegister = kFailure

    On Error GoTo ExpectedErrorRoutine
    
    cnt = 0
    sSQL = "SELECT COUNT(*) cnt FROM tarPendCustPmt WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        cnt = rs.Field("cnt")
    End If
    
    If cnt > 0 Then
        TaskNumber = ktskCashReceipts
        iRet = iValidateData(TaskNumber)
        
        If miMode = kPostXXBatches And iRet = 0 Then
            ResetBatchStatus
            lCheckInvoiceRegister = kExitSub
            Set rs = Nothing
            TaskNumber = lOrigTaskNumber
            Exit Function 'no data move to next batch
        End If
        
            If (optRegister(KPostOnly) = True And muRegStat.iPrintStatus = 1 And miMode <> kPostXXBatches) Or _
               (mbPostOnly = True And miMode = kPostXXBatches) Then
                
                SetUIStatus
                
                If mPostError = kNoErrors Or mPostError = kWarnings Then
                    
                    If CheckBalance = kSuccess Then
                    
                        If iPostDocuments(TaskNumber) Then
                            lblStatusMsg = guLocalizedStrings.posted
                            sbrMain.Status = SOTA_SB_START
                            Screen.MousePointer = vbDefault
                        End If
                    
                    End If
                
                Else
                    PrintErrorLog
                    cleanErrorLog
                    If miMode = kPostXXBatches Then
                        ResetBatchStatus
                    End If
                End If
                lCheckInvoiceRegister = kExitSub
                Set rs = Nothing
                TaskNumber = lOrigTaskNumber
                Exit Function
            End If
            
            
            'validate GL currencies in tglPosting
            ValidateGLCurrencies
            
            'reset register Printed flag in tciBatchSySDetl
            iRet = SetPrintedFlag(0)
                            
            ' process and print register
            ProcessRegister TaskNumber, sKey
               
    End If
    
    lCheckInvoiceRegister = kSuccess
    Set rs = Nothing
    TaskNumber = lOrigTaskNumber
    Exit Function
    
ExpectedErrorRoutine:
    Set rs = Nothing
    TaskNumber = lOrigTaskNumber

End Function

Private Function bSetPostStatus(lBatchKey As Long, iPost As Integer, _
            iPrint As Integer, ByRef lLockID As Long, sBatch As String, _
            bTurnOn As Boolean) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************************
' Desc:  Sets or removes posting Status from batches chosen
' Parms: lBatchKey - Array of batch Keys
'        iPost     - Array Of posting Flags
'        bTurnOn   - Wheteher to turn on or of posting Status
'*************************************************************************************

    Dim iResult     As Integer      'Stored procedure results
    Dim lShared     As Long         'count of shared locks
    Dim lExcl       As Long         'count of exclusive locks
    Dim sEntity     As String       'entity name to be passed to logical lock routines
    Dim lLock       As Long         'lock id for logical lock, out param for setting, in param for clearing

    bSetPostStatus = True
    
    sEntity = kLockEntBatch & Format$(lBatchKey)
   
   'Don't allow them to print or post if already posting (turning Lock On)
    If bTurnOn Then
        
        gCountLogical moClass.moAppDB, sEntity, lShared, lExcl
        If lExcl > 0 Then
            'MsgBox "A batch that you have chosen to print or post is already posting."
            giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, _
                    sBatch
            bSetPostStatus = False
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
            Exit Function
        End If
        
    End If
      
    'Post this batch
    If iPost = 1 Then
          
        'Turn on Lock
        If bTurnOn Then
        
            iResult = glLockLogical(moClass.moAppDB, sEntity, kLockTypeExclusive, lLock)
            
            Select Case iResult
            Case kLockRetSuccess
                lLockID = lLock
            
                'Get Status for batch make sure it is balanced or interrupted
                'The IF condition is added by HC on 11-13-96. If Register-Only,
                'the register should be able to run.
                If Me.optRegister(kPrintNoPost) = False Then
                    With moClass.moAppDB
                        .SetInParam lBatchKey
                        .SetOutParam iResult
                        .ExecuteSP ("spCIGetBatchPostStatus")
                        iResult = .GetOutParam(2)
                        .ReleaseParams
                    End With
            
                    If iResult = 0 Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgCIBadStatus, _
                        sBatch
                        bSetPostStatus = False
                        Exit Function
                    End If
                End If

            Case kLockRetNoTemp
                giSotaMsgBox Me, moClass.moSysSession, kmsgCantGetBatchLock, _
                sBatch
                bSetPostStatus = False
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function

            Case kLockRetExclusiveAlready
                giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, _
                sBatch
                bSetPostStatus = False
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function

            Case kLockRetSharedAlready
                giSotaMsgBox Me, moClass.moSysSession, kmsgCIInUseOrPosting, _
                sBatch
                bSetPostStatus = False
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function

            End Select
              
        Else

            If lLockID <> 0 Then
                lLock = lLockID
                iResult = glUnlockLogical(moClass.moAppDB, lLock)
            End If

        End If
      
    Else    'for shared lock on printing
          
        If bTurnOn Then
        
            iResult = glLockLogical(moClass.moAppDB, sEntity, kLockTypeShared, lLock)
        
            Select Case iResult
            Case kLockRetSuccess
                lLockID = lLock

            Case kLockRetNoTemp
                giSotaMsgBox Me, moClass.moSysSession, kmsgCantGetBatchLock, _
                    sBatch
                bSetPostStatus = False
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function

            Case kLockRetExclusiveAlready
                giSotaMsgBox Me, moClass.moSysSession, kmsgCIAlreadyPosting, _
                sBatch
                bSetPostStatus = False
                '+++ VB/Rig Begin Pop +++
                '+++ VB/Rig End +++
                Exit Function
            End Select
        Else
        
            If lLockID <> 0 Then
                lLock = lLockID
                iResult = glUnlockLogical(moClass.moAppDB, lLock)
            End If
        
        End If
      
    End If  'Post is turned on
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetPostStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub
    
    'Intellisol start
    If mbCalledFromPA Then
        Me.Caption = "Project Invoice Register"
        guLocalizedStrings.INJournalDesc = Me.Caption
        msOrigCaption = Me.Caption
    End If
    'Intellisol end

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub cmdProperties_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdProperties_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdProperties_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdProperties_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtMessageHeader_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMessageHeader(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopies_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCopies, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopies_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCopies_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCopies, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCopies_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chk_Click(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chk(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chk_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chk_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chk(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chk_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chk_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chk(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chk_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDefer_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkDefer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDefer_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDefer_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkDefer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDefer_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkDefer_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkDefer, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkDefer_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optRegister(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRegister_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optRegister(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRegister_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optRegister_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optRegister(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optRegister_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboFormat, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboFormat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboFormat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSort_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSort_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSort_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSort, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSort_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSort_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSort_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSort_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSort, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSort_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboReportDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboReportDest, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboReportDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportDest_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboReportDest, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportDest_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Private Property Get TaskNumber() As Long
    TaskNumber = mlTaskNumber
End Property

Private Property Let TaskNumber(lTaskNum As Long)
    mlTaskNumber = lTaskNum
    SetVariables mlTaskNumber
End Property
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If


Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get ReportFileName() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ReportFileName = msReportFileName
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ReportFileName", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get ReportPath() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    ReportPath = msReportPath
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ReportPath", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
  
Public Property Let ReportPath(sPath As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    msReportPath = sPath
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ReportPath", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
  
Public Property Get RestrictClause() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    RestrictClause = msCRWSelect
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "RestrictClause", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
  
Public Property Get CompanyID() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    CompanyID = msCompanyID
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CompanyID", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Function lCreateWorkTable(SP_CREATEWRK As String, colInSPParams As Collection, ByRef colOutSPParams As Collection) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iLoop As Variant
    Dim vCollectionItem As Variant
    Dim iNoOfOutParams As Variant
    Dim iNoOfInParams As Long
    Dim vOutParam As Variant
    
    'run register stored procedure to create work table and tglPosting table rows
    moClass.lUIActive = kChildObjectActive
    iNoOfInParams = colInSPParams.Count
    iNoOfOutParams = colOutSPParams.Count
    
    With moAppDAS
    
        ' set the "in" parameters from the "in" collection
        iLoop = 1
        For Each vCollectionItem In colInSPParams
            .SetInParam colInSPParams.Item(iLoop)
            iLoop = iLoop + 1
        Next
        
        ' set the "out" parameters from the "out" collection
        For Each vCollectionItem In colOutSPParams
            vOutParam = vCollectionItem
            ColSetOutParam vOutParam
        Next
        
        ' run the stored procedure
        .ExecuteSP (SP_CREATEWRK)
        
        ' clear out all elements of the out collection
        iLoop = 1
        For Each vCollectionItem In colOutSPParams
            colOutSPParams.Remove iLoop
        Next
        
        ' get the out param values from the stored procedure
        For iLoop = iNoOfInParams + 1 To iNoOfInParams + iNoOfOutParams
            colOutSPParams.Add .GetOutParam(iLoop)
        Next iLoop
        
        .ReleaseParams

    End With
    
    ' set the return value of this function to the last out param value
    lCreateWorkTable = colOutSPParams.Item(iNoOfOutParams)
        
    moClass.lUIActive = kChildObjectInactive
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lCreateWorkTable", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function


Public Function lDeleteWorkTable(SP_DELETEWRK As String, colInSPParams As Collection, ByRef colOutSPParams As Collection) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iLoop As Variant
    Dim vCollectionItem As Variant
    Dim iNoOfInParams As Long
    Dim iNoOfOutParams As Variant
    Dim vOutParam As Variant

    'run register stored procedure to delete work table
    moClass.lUIActive = kChildObjectActive
    iNoOfInParams = colInSPParams.Count
    iNoOfOutParams = colOutSPParams.Count
    
    With moAppDAS
    
        ' set the "in" parameters from the "in" collection
        iLoop = 1
        For Each vCollectionItem In colInSPParams
            .SetInParam colInSPParams.Item(iLoop)
            iLoop = iLoop + 1
        Next
        
        ' set the "out" parameters from the "out" collection
        For Each vCollectionItem In colOutSPParams
            vOutParam = vCollectionItem
            ColSetOutParam vOutParam
        Next
        
          ' run the stored procedure
        .ExecuteSP (SP_DELETEWRK)
        
        ' clear out all elements of the out collection
        iLoop = 1
        For Each vCollectionItem In colOutSPParams
            colOutSPParams.Remove iLoop
        Next
        
        ' get the out param values from the stored procedure
        For iLoop = iNoOfInParams + 1 To iNoOfInParams + iNoOfOutParams
            colOutSPParams.Add .GetOutParam(iLoop)
        Next iLoop
        
        .ReleaseParams

    End With
    
    ' set the return value of this function to the last out param value
    lDeleteWorkTable = colOutSPParams.Item(iNoOfOutParams)
    
    moClass.lUIActive = kChildObjectInactive

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lDeleteWorkTable", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function PrintRegister(ByRef oSessionID As Long, iRegOption As Integer, bDeferred As Boolean, _
                            sPrinter As String, iCopies As Integer, sSortBy As String, _
                            sFormulas() As String, Optional GroupingParams As Variant, Optional sAppendSort As String = "") As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    
    moClass.lUIActive = kChildObjectActive
    
    PrintRegister = kFailure
    
    'Build select statements for lookup and for crystal
    msCRWSelect = sSortBy
    sSortBy = ""
    
    moReport.PrinterName = sPrinter
    
    'check work table to be sure that records exist in the work table for the "current" session
    '   If records exist, then  check for method of "printing" register
    '                           StartReport with 0 for preview
    '                                            1 for deferred
    '                                            2 for normal
    If iRegOption = kPreviewOnly Then
        If Not IsMissing(GroupingParams) Then
            PrintRegister = lStartReport(kTbPreview, Me, False, sFormulas(), "", "", "", "", "", GroupingParams, sAppendSort)
        Else
            PrintRegister = lStartReport(kTbPreview, Me, False, sFormulas(), "", "", "", "", "", , sAppendSort)
        End If
    ElseIf bDeferred Then
        If Not IsMissing(GroupingParams) Then
            PrintRegister = lStartReport(kTbDefer, Me, False, sFormulas(), "", "", "", "", "", GroupingParams, sAppendSort)
        Else
            PrintRegister = lStartReport(kTbDefer, Me, False, sFormulas(), "", "", "", "", "", , sAppendSort)
        End If
    Else
        If Not IsMissing(GroupingParams) Then
            PrintRegister = lStartReport(kTbPrint, Me, False, sFormulas(), "", "", "", "", "", GroupingParams, sAppendSort)
        Else
            PrintRegister = lStartReport(kTbPrint, Me, False, sFormulas(), "", "", "", "", "", , sAppendSort)
        End If
    End If
    
    'Exit this function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "PrintRegister", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Private Sub ColSetOutParam(vColItem As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case TypeName(vColItem)
    
        Case "String"
            Dim sColItem As String
            sColItem = CStr(vColItem)
            moAppDAS.SetOutParam sColItem
        Case "Integer"
            Dim iColItem As Integer
            iColItem = CInt(vColItem)
            moAppDAS.SetOutParam iColItem
        Case "Long"
            Dim lColItem As Long
            lColItem = CLng(vColItem)
            moAppDAS.SetOutParam lColItem
        Case "Single"
            Dim sngColItem As Single
            sngColItem = CSng(vColItem)
            moAppDAS.SetOutParam sngColItem
        Case "Double"
            Dim dblColItem
            dblColItem = CDbl(vColItem)
            moAppDAS.SetOutParam dblColItem
        Case "Date"
            Dim dteColItem As Date
            dteColItem = CDate(vColItem)
            moAppDAS.SetOutParam dteColItem
        Case "Boolean"
            Dim boolColItem As Boolean
            boolColItem = CBool(vColItem)
            moAppDAS.SetOutParam boolColItem
        Case "Byte"
            Dim byteColItem As Byte
            byteColItem = CByte(vColItem)
            moAppDAS.SetOutParam byteColItem
        Case "Currency"
            Dim curColItem As Currency
            curColItem = CCur(vColItem)
            moAppDAS.SetOutParam curColItem
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ColSetOutParam", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Function sGetWhere(oSessionID As Long) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim sWhere As String
    Dim sSQL   As String
    Dim rs As Object

    sWhere = "({tciErrorLog.BatchKey} = " & Str(oSessionID)
    
    sSQL = "SELECT LinkedBatchKey FROM tciBatchLink WHERE BatchKey = " & Str(oSessionID)
    
    Set rs = moAppDAS.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    With rs
        .MoveFirst
        While Not .IsEOF
            sWhere = sWhere & " OR {tciErrorLog.BatchKey} = " & .Field("LinkedBatchKey")
          .MoveNext
        Wend
      .Close
    End With
    
    sWhere = sWhere & ")"
    
    Set rs = Nothing
        
    sGetWhere = sWhere

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sGetWhere", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lInitializeReport = -1
    
    Set moSotaObjects = New Collection
    Set moReport = New clsReportEngine
    Set moDDData = New clsDDData
    Set moPrinter = Printer
    
    If Not moDDData.lInitDDData(sWorkTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If Not (moDDData.lDDBuildData(App.EXEName) = kSuccess) Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, False) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
   
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function lModPost(SP_MOD_POST As String, lBatchKey As Long, iSourceModule As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
       
    Dim lRetval As Long
  
    With moAppDAS
    
        .SetInParam lBatchKey
        .SetInParam lBatchKey
        .SetInParam iSourceModule
        
        .SetOutParam lRetval
        
        ' run the stored procedure
        .ExecuteSP (SP_MOD_POST)
        
        While .IsExecuting = True
            DoEvents
        Wend
        
        lRetval = .GetOutParam(4)
        .ReleaseParams
    
    End With
    
    lModPost = lRetval
    
    Exit Function
    
ExpectedErrorRoutine:

    MsgBox Err.Description
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lModPost", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function lPostGLTrans(SP_GL_POST As String, lBatchKey As Long, iSourceModule As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
  On Error GoTo ExpectedErrorRoutine
    
  Dim lRetval As Long
  
    With moAppDAS
    
        .SetInParam lBatchKey
        .SetInParam iSourceModule
        
        .SetOutParam lRetval
        
        ' run the stored procedure
        .ExecuteSP (SP_GL_POST)
        
        While .IsExecuting = True
            DoEvents
        Wend

        lRetval = .GetOutParam(3)
        .ReleaseParams

    End With
    
    lPostGLTrans = lRetval
    
    Exit Function
    
ExpectedErrorRoutine:

    MsgBox Err.Description
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lPostGLTrans", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function


Public Function lModCleanup(SP_MOD_CLEANUP As String, lBatchKey As Long, iSourceModule As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
       
    Dim lRetval As Long
  
    With moAppDAS
    
        .SetInParam lBatchKey
        .SetInParam iSourceModule
        
        .SetOutParam lRetval
        
        ' run the stored procedure
        .ExecuteSP (SP_MOD_CLEANUP)
        
        While .IsExecuting = True
            DoEvents
        Wend
        
        lRetval = .GetOutParam(3)
        .ReleaseParams
    
    End With
    
    lModCleanup = lRetval
    
    Exit Function
    
ExpectedErrorRoutine:

    MsgBox Err.Description
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lModCleanup", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function lPostModuleTrans(SP_APP_POST As String, lBatchKey As Long, lSessionID As Long, iSourceModule As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
        
    Dim lRetval As Long
    
    On Error GoTo ExpectedErrorRoutine

    With moAppDAS
    
        .SetInParam lBatchKey
        .SetInParam lSessionID
        .SetInParam iSourceModule
        
        .SetOutParam lRetval
        
        .ExecuteSP (SP_APP_POST)
        
        While .IsExecuting = True
            DoEvents
        Wend
        
        lRetval = .GetOutParam(4)
        
        .ReleaseParams

    End With
    
    lPostModuleTrans = lRetval

'+++ VB/Rig Begin Pop +++
        Exit Function
        
ExpectedErrorRoutine:

    MsgBox Err.Description
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lPostModuleTrans", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function


Public Function lValidateData(SP_VALIDATE As String, lBatchKey As Long, lSessionID As Long, iSourceModule As Integer, iBatchType As Integer) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetval As Long

    With moAppDAS
    
        .SetInParam lBatchKey
        .SetInParam lSessionID
        .SetInParam iSourceModule
        .SetInParam iBatchType
        
        .SetOutParam lRetval
        
        .ExecuteSP (SP_VALIDATE)
        
        While .IsExecuting = True
            DoEvents
        Wend
        lRetval = .GetOutParam(5)
        .ReleaseParams

    End With
    
    ' set the return value of this function to the last out param value
    lValidateData = lRetval
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lValidateData", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Private Function ComputeArzja001ReportName() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Select Case mlTaskNumber
    
        Case ktskFinChgReg
            ComputeArzja001ReportName = "Finance Charge Register"
        Case Else
            ComputeArzja001ReportName = "Invoice Register"
            
    End Select
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ComputeArzja001ReportName", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub CheckAvaTax()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Avatax Integration: Check the availability of Avatax for the CompanyID.
'***********************************************************************

On Error GoTo ExpectedErrorRoutine

    'Assume AvaTax feature not configured
    mbAvaTaxEnabled = False

    mbAvaTaxEnabled = gbGetValidBoolean(moClass.moAppDB.Lookup("AvaTaxEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(msCompanyID)))

    If mbAvaTaxEnabled Then
        On Error GoTo ExpectedErrorRoutine_AvaTaxObj
        Set moAvaTax = Nothing
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'This is how we find out if AvaTax is not installed.
'The missing table (err 4050) will tell us not to fire the AvaTax code
'throughout the program.

    Select Case Err.Number
    
        Case 4050 ' SQL Object Not Found
            'tavConfiguration not found meaning AvaTax not installed.
            'No Error action required.

        Case Else
            'Unexpected Error reading tavConfiguration
            giSotaMsgBox Me, moClass.moSysSession, kmsgtavConfigurationError

    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine_AvaTaxObj:
'Expected Error:
'If the DB Avatax table was found, AvaTax is likely used for this Site.
'But when creating the local client object, the object was likely not installed
'This is not a good error, let the administrator know to check that AvaTax was properly implemented.

    giSotaMsgBox Me, moClass.moSysSession, kmsgAvaObjectError

    mbAvaTaxEnabled = False

    ' Continue into standard VBRig

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckAvaTax", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'SGS-JMM 1/31/2011
Public Function GetCypressQuery() As String
    Dim batchID As String
    
    On Error Resume Next
    batchID = moClass.moSysSession.AppDatabase.Lookup("BatchID", "tciBatchLog", "BatchKey = " & Me.CONTROLS(0).Parent.mlBatchKey)
    GetCypressQuery = "[Company ID] = """ & moClass.moSysSession.CompanyID & """ and [Batch ID] = """ & batchID & """"
End Function
'End SGS-JMM

