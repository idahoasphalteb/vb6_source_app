VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Begin VB.Form frmSendSLX 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Send SAGE Data to SLX"
   ClientHeight    =   4515
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6405
   HelpContextID   =   17770001
   Icon            =   "frmSendSLX.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4515
   ScaleWidth      =   6405
   Begin VB.CheckBox chShowLog 
      Caption         =   "Show Process Log"
      Height          =   195
      Left            =   2280
      TabIndex        =   9
      ToolTipText     =   "Capture the process flow for the last record processsed and display to user."
      Top             =   3600
      Width           =   2175
   End
   Begin VB.TextBox txtCustCurrRec 
      BackColor       =   &H00C0FFFF&
      Height          =   375
      Left            =   360
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   3120
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.TextBox txtCustLinkStatus 
      BackColor       =   &H00C0FFFF&
      Height          =   375
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   2040
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.CommandButton cmdCancelLink 
      Caption         =   "Stop Link Process"
      Height          =   495
      Left            =   2040
      TabIndex        =   7
      Top             =   2520
      Visible         =   0   'False
      Width           =   2055
   End
   Begin TabDlg.SSTab tabData 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   5953
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Customer Data"
      TabPicture(0)   =   "frmSendSLX.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "cmdSendCust"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "chAutoLink"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmdSendAddress"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Contract Data"
      TabPicture(1)   =   "frmSendSLX.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "cmdSendContract"
      Tab(1).Control(1)=   "cmdSendQuotes"
      Tab(1).ControlCount=   2
      Begin VB.CommandButton cmdSendAddress 
         Caption         =   "Send Cust Missing Addr"
         Height          =   495
         Left            =   3720
         TabIndex        =   3
         ToolTipText     =   "Send Primary Address for Customer that exist in SLX but are missing an address in SLX."
         Top             =   600
         Width           =   2055
      End
      Begin VB.CheckBox chAutoLink 
         Caption         =   "Auto Link Matching Records"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   1200
         Width           =   2535
      End
      Begin VB.CommandButton cmdSendContract 
         Caption         =   "Send Contracts to SLX"
         Height          =   495
         Left            =   -74760
         TabIndex        =   4
         Top             =   600
         Width           =   1935
      End
      Begin VB.CommandButton cmdSendQuotes 
         Caption         =   "Send Quotes to SLX"
         Height          =   495
         Left            =   -71040
         TabIndex        =   5
         Top             =   600
         Width           =   1935
      End
      Begin VB.CommandButton cmdSendCust 
         Caption         =   "Send Customers to SLX"
         Height          =   495
         Left            =   360
         TabIndex        =   1
         Top             =   600
         Width           =   2055
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   15
      Top             =   4320
      Visible         =   0   'False
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   14
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -10000
      Style           =   2  'Dropdown List
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      Width           =   1245
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   20
      Top             =   645
      Visible         =   0   'False
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   4125
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   0
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   741
      Style           =   6
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   21
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmSendSLX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Private miSecurityLevel     As Integer

Public moSotaObjects        As New Collection

Private mbEnterAsTab        As Boolean

'Private msCompanyID         As String
'
Private moContextMenu       As New clsContextMenu

Private miOldFormHeight     As Long

Private miOldFormWidth      As Long

Private miMinFormHeight     As Long

Private miMinFormWidth      As Long

Private mbLoadSuccess       As Boolean

Public mlLanguage As Long

Private Sub cmdCancelLink_Click()
    msBStopProcess = True
End Sub

Private Sub cmdSendAddress_Click()
    On Error GoTo Error
    
    Static bInhere As Boolean
    
    If bInhere = True Then Exit Sub
    
    strProcessLog = strProcessLog & "Send Customer Addresses to SLX: Show Link Import Status" & vbCrLf
    
    txtCustLinkStatus.Text = Empty
    txtCustCurrRec.Text = Empty
    
    cmdCancelLink.Visible = True
    txtCustLinkStatus.Visible = True
    txtCustCurrRec.Visible = True
    
    bInhere = True
    
    Dim ErrMsg As String
    
    msBStopProcess = False
    
    sbrMain.Message = "Sending Customers Address to SLX"
    
    Me.MousePointer = vbHourglass
    
'    Me.Enabled = False
    
    strProcessLog = strProcessLog & "Validate SLX Connection" & vbCrLf
    'Check to see if we have a connection to SLX
    If IsValidSLXConn(oClass, ErrMsg) = False Then
        strProcessLog = strProcessLog & "SLX Connection is not valid" & vbCrLf
        Me.Enabled = True
        
        Me.MousePointer = vbDefault
        
        sbrMain.Message = Empty
        
        MsgBox "Could not obtain a connection to SLX.  No records were linked." & vbCrLf & _
        "The error message is: " & vbCrLf & ErrMsg, vbExclamation, "SAGE 500"
        
        bInhere = False
        cmdCancelLink.Visible = False
        txtCustLinkStatus.Visible = False
        txtCustCurrRec.Visible = False
        Exit Sub
    End If
    
    strProcessLog = strProcessLog & "Check for Auto Link of Customer" & vbCrLf
    
    'Send data to SLX
    'Import Customers
'    If chAutoLink.Value = vbChecked Then
'        Call LinkAllCustomers(oClass, True, True)
'    Else
        Call LinkAllCustomers(oClass, True, False, True)
'    End If

    strProcessLog = strProcessLog & "Display Process Log" & vbCrLf
    
    If chShowLog.Value = vbChecked Then
        frmProcessLog.Show vbModal, Me
    End If

    Me.Enabled = True
    
    Me.MousePointer = vbDefault
    
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSendAddress_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear

    Me.Enabled = True
    Me.MousePointer = vbDefault
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False

End Sub

Private Sub cmdSendContract_Click()
    On Error GoTo Error
    
    Static bInhere As Boolean
    
    If bInhere = True Then Exit Sub
    
    txtCustLinkStatus.Text = Empty
    txtCustCurrRec.Text = Empty
    
    cmdCancelLink.Visible = True
    txtCustLinkStatus.Visible = True
    txtCustCurrRec.Visible = True
    
    bInhere = True
    
    Dim ErrMsg As String
    
    sbrMain.Message = "Sending Contracts(BSO) to SLX"
    
    Me.MousePointer = vbHourglass
    
    msBStopProcess = False
    
'    Me.Enabled = False
    
    'Check to see if we have a connection to SLX
    If IsValidSLXConn(oClass, ErrMsg) = False Then
        Me.Enabled = True
        
        Me.MousePointer = vbDefault
        
        sbrMain.Message = Empty
        
        MsgBox "Could not obtain a connection to SLX.  No records were linked." & vbCrLf & _
        "The error message is: " & vbCrLf & ErrMsg, vbExclamation, "SAGE 500"
        
        bInhere = False
        cmdCancelLink.Visible = False
        txtCustLinkStatus.Visible = False
        txtCustCurrRec.Visible = False
        Exit Sub
    End If
    
    'Send data to SLX
    'Import Contracts
    Call LinkAllContracts(moClass, True)
    
    If chShowLog.Value = vbChecked Then
        frmProcessLog.Show vbModal, Me
    End If
    
    Me.Enabled = True
    
    Me.MousePointer = vbDefault
    
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSendContract_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear

    Me.Enabled = True
    Me.MousePointer = vbDefault
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
End Sub

Private Sub cmdSendCust_Click()
    On Error GoTo Error
    
    Static bInhere As Boolean
    
    If bInhere = True Then Exit Sub
    
    strProcessLog = strProcessLog & "Send Customers to SLX: Show Link Import Status" & vbCrLf
    
    txtCustLinkStatus.Text = Empty
    txtCustCurrRec.Text = Empty
    
    cmdCancelLink.Visible = True
    txtCustLinkStatus.Visible = True
    txtCustCurrRec.Visible = True
    
    bInhere = True
    
    Dim ErrMsg As String
    
    msBStopProcess = False
    
    sbrMain.Message = "Sending Customers to SLX"
    
    Me.MousePointer = vbHourglass
    
'    Me.Enabled = False
    
    strProcessLog = strProcessLog & "Validate SLX Connection" & vbCrLf
    
    'Check to see if we have a connection to SLX
    If IsValidSLXConn(oClass, ErrMsg) = False Then
        strProcessLog = strProcessLog & "SLX Connection is not valid" & vbCrLf
        Me.Enabled = True
        
        Me.MousePointer = vbDefault
        
        sbrMain.Message = Empty
        
        MsgBox "Could not obtain a connection to SLX.  No records were linked." & vbCrLf & _
        "The error message is: " & vbCrLf & ErrMsg, vbExclamation, "SAGE 500"
        
        bInhere = False
        cmdCancelLink.Visible = False
        txtCustLinkStatus.Visible = False
        txtCustCurrRec.Visible = False
        Exit Sub
    End If
    
    strProcessLog = strProcessLog & "Check for Auto Link of Customer" & vbCrLf
    
    'Send data to SLX
    'Import Customers
    If chAutoLink.Value = vbChecked Then
        strProcessLog = strProcessLog & "Call Link Process" & vbCrLf
        Call LinkAllCustomers(oClass, True, True)
    Else
        strProcessLog = strProcessLog & "Call Link Process" & vbCrLf
        Call LinkAllCustomers(oClass, True, False)
    End If
    
    strProcessLog = strProcessLog & "Display Process Log" & vbCrLf
    
    If chShowLog.Value = vbChecked Then
        frmProcessLog.Show vbModal, Me
    End If
    
    Me.Enabled = True
    
    Me.MousePointer = vbDefault
    
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSendCust_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear

    Me.Enabled = True
    Me.MousePointer = vbDefault
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
End Sub

Private Sub cmdSendQuotes_Click()
    On Error GoTo Error
    
    Static bInhere As Boolean
    
    If bInhere = True Then Exit Sub
    
    txtCustLinkStatus.Text = Empty
    txtCustCurrRec.Text = Empty
    
    cmdCancelLink.Visible = True
    txtCustLinkStatus.Visible = True
    txtCustCurrRec.Visible = True
    
    bInhere = True
        
    Dim ErrMsg As String
    
    sbrMain.Message = "Sending Quotes to SLX"
    
    Me.MousePointer = vbHourglass
    
    msBStopProcess = False
    
'    Me.Enabled = False
    
    'Check to see if we have a connection to SLX
    If IsValidSLXConn(oClass, ErrMsg) = False Then
        Me.Enabled = True
        
        Me.MousePointer = vbDefault
        
        sbrMain.Message = Empty
        
        MsgBox "Could not obtain a connection to SLX.  No records were linked." & vbCrLf & _
        "The error message is: " & vbCrLf & ErrMsg, vbExclamation, "SAGE 500"
        
        bInhere = False
        cmdCancelLink.Visible = False
        txtCustLinkStatus.Visible = False
        txtCustCurrRec.Visible = False
        Exit Sub
    End If
    
    'Send data to SLX
    'Import Quotes
    Call LinkAllQuotes(moClass, True)
    
    If chShowLog.Value = vbChecked Then
        frmProcessLog.Show vbModal, Me
    End If
    
    Me.Enabled = True
    
    Me.MousePointer = vbDefault
    
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSendQuotes_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear

    Me.Enabled = True
    Me.MousePointer = vbDefault
    sbrMain.Message = Empty
    
    bInhere = False
    cmdCancelLink.Visible = False
    txtCustLinkStatus.Visible = False
    txtCustCurrRec.Visible = False
End Sub


Private Sub Form_Initialize()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mbCancelShutDown = False
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) Form_Initialize - " & Err.Description    'Generic Error Rig
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) Form_KeyDown - " & Err.Description   'Generic Error Rig
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) Form_KeyPress - " & Err.Description  'Generic Error Rig
End Sub

Private Sub Form_Load()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    mbLoadSuccess = False

    msCompanyID = moClass.moSysSession.CompanyID
    mbEnterAsTab = moClass.moSysSession.EnterAsTab

    BindToolbar
    Set sbrMain.FrameWork = moClass.moFramework

    sbrMain.NavigatorVisible = False
    
    ' set form size variables
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    mbLoadSuccess = True
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) Form_Load - " & Err.Description      'Generic Error Rig
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    On Error Resume Next
    
    Dim iConfirmUnload  As Integer


   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
    
        'Check all other forms  that may have been loaded from this main form.
        'If there are any visible forms, then this means the form is active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
           'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is Normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                        
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
                
        End Select
        
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
            'Do nothing
            
    End Select
    
Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) Form_QueryUnload - " & Err.Description    'Generic Error Rig
End Sub

Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    'Unload all forms loaded from this main form
    gUnloadChildForms Me

    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me

    ' perform proper cleanup
    Set moContextMenu = Nothing         ' context menu class
    Set moSotaObjects = Nothing         ' SOTA Child objects collection

Exit Sub

ExpectedErrorRoutine:

End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Set moClass = Nothing
     
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) Form_Unload - " & Err.Description    'Generic Error Rig
End Sub

Public Sub HandleToolBarClick(sKey As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig

    Select Case sKey
        
        Case kTbClose
            moClass.miShutDownRequester = kUnloadSelfShutDown
            Unload Me
'            moClass.miShutDownRequester = kFrameworkShutDown
      
      'Save changes and close the form
        Case kTbFinishExit
            
      'Cancel changes and close the form
        Case kTbCancelExit
                        
      'Display form level help
        Case kTbHelp
            gDisplayFormLevelHelp Me
            
      'Example code to save changes and display report form
        Case kTbPrint
            
        
        Case Else
                
    End Select
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) HandleToolBarClick - " & Err.Description    'Generic Error Rig
End Sub




Private Sub tbrMain_ButtonClick(Button As String)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
   HandleToolBarClick Button
    
    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) tbrMain_ButtonClick - " & Err.Description    'Generic Error Rig
End Sub

Private Sub BindToolbar()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    
    'Initialize toolbar.
    tbrMain.Init sotaTB_Styles.sotaTB_NO_STYLE, mlLanguage
    
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
    End With
    
    tbrMain.AddButton kTbClose

    Exit Sub                                                                              'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Procedure) BindToolbar - " & Err.Description    'Generic Error Rig
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = Me.Name
End Function

Public Property Get FormHelpPrefix() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    FormHelpPrefix = "ARZ"   ' Place your help prefix here
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) FormHelpPrefix - " & Err.Description    'Generic Error Rig
End Property

Public Property Get WhatHelpPrefix() As String
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    WhatHelpPrefix = "ARZ"    ' Put your identifier here
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) WhatHelpPrefix - " & Err.Description    'Generic Error Rig
End Property

Public Property Get oClass() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set oClass = moClass
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) oClass - " & Err.Description      'Generic Error Rig
End Property
Public Property Set oClass(oNewClass As Object)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set moClass = oNewClass
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Set) oClass - " & Err.Description      'Generic Error Rig
End Property

Public Property Get lRunMode() As Long
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    lRunMode = mlRunMode
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) lRunMode - " & Err.Description    'Generic Error Rig
End Property
Public Property Let lRunMode(lNewRunMode As Long)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mlRunMode = lNewRunMode
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Let) lRunMode - " & Err.Description    'Generic Error Rig
End Property

Public Property Get bSaved() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bSaved = mbSaved
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) bSaved - " & Err.Description      'Generic Error Rig
End Property
Public Property Let bSaved(bNewSaved As Boolean)
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    mbSaved = bNewSaved
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Let) bSaved - " & Err.Description      'Generic Error Rig
End Property

Public Property Get bCancelShutDown()
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bCancelShutDown = mbCancelShutDown
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) bCancelShutDown - " & Err.Description    'Generic Error Rig
End Property

Public Property Get bLoadSuccess() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    bLoadSuccess = mbLoadSuccess
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) bLoadSuccess - " & Err.Description    'Generic Error Rig
End Property

Public Property Get MyApp() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set MyApp = App
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) MyApp - " & Err.Description       'Generic Error Rig
End Property

Public Property Get MyForms() As Object
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    Set MyForms = Forms
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) MyForms - " & Err.Description     'Generic Error Rig
End Property

Public Property Get UseHTMLHelp() As Boolean
    On Error GoTo GenericErrorHandler                                                     'Generic Error Rig
    UseHTMLHelp = True   ' Form uses HTML Help (True/False)
    Exit Property                                                                         'Generic Error Rig
GenericErrorHandler:                                                                      'Generic Error Rig
    MsgBox "Error in module frmSendSLX,  (Property Get) UseHTMLHelp - " & Err.Description 'Generic Error Rig
End Property

