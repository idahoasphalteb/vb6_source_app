Attribute VB_Name = "basSendSLX"
Option Explicit

Const VBRIG_MODULE_ID_STRING = "ARZXXSLX.BAS"

Public msCompanyID         As String

Public msBStopProcess As Boolean

Public strDebug As String

Global strProcessLog As String
Global strSglProcLog As String


Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("ARZXXSLX.clsSendSLX")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSendSLX"
End Function


'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
'RKL DEJ 2016-12-14 - Insert/Update SLX Opportunity and SLX Account/Customer   (START)
'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
Sub ProcessSLXCustData(oClass As Object, CompanyID As String, CustKey As Long, CustID As String, CustName As String, _
AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String, AddrLine4 As String, _
City As String, State As String, Zip As String, Country As String, Optional bAutoLink As Boolean = False)
    On Error GoTo Error
    
    Dim AccountID As String
    Dim AddressID As String
    Dim SAGECust As New clsSAGECustomer
    Dim SLXCusts As New clsSLXCustomers
    
    strSglProcLog = strSglProcLog & "Parce Cust Data" & vbCrLf
    
    SAGECust.CompanyID = CompanyID
    SAGECust.CustKey = CustKey
    SAGECust.CustID = CustID
    SAGECust.CustName = CustName
    SAGECust.AddrKey = AddrKey
    SAGECust.AddrName = AddrName
    SAGECust.AddrLine1 = AddrLine1
    SAGECust.AddrLine2 = AddrLine2
    SAGECust.AddrLine3 = AddrLine3
    SAGECust.AddrLine4 = AddrLine4
    SAGECust.City = City
    SAGECust.State = State
    SAGECust.Zip = Zip
    SAGECust.Country = Country
    
strDebug = "ProcessSLXCustData"
    
    strSglProcLog = strSglProcLog & "Validate Company" & vbCrLf
    
    If glGetValidLong(oClass.moAppDB.Lookup("Count(*)", "tciCompanySendToSLX_RKL", "CompanyID = " & gsQuoted(msCompanyID))) <= 0 Then
        'We are only passing for IDA and PEA (and WEI 2016-05-10)
        strDebug = "Failed - This company is not setup to pass data to SLX from CompanyID = " & CompanyID
        Exit Sub
    End If
    
'    If UCase(Trim(CompanyID)) <> "IDA" And UCase(Trim(CompanyID)) <> "PEA" And UCase(Trim(CompanyID)) <> "WEI" Then
'        'We are only passing for IDA and PEA (and WEI 2016-05-10)
'strDebug = "Failed - We are only passing for IDA and PEA (and WEI 2016-05-10) - CompanyID = " & CompanyID
'
'        Exit Sub
'    End If
    
    strSglProcLog = strSglProcLog & "Create/Verify SLX Account" & vbCrLf
    
    'Create the SLX Account
    If InsSLXAccount(oClass, SAGECust, AccountID, SLXCusts, bAutoLink) = False Then
        strSglProcLog = strSglProcLog & "Failed Customer Create/Verify" & vbCrLf
    
'    If InsSLXAccount(oClass, CompanyID, CustKey, CustID, CustName, AccountID) = False Then
        'Did not create the Account
strDebug = "Failed InsSLXAccount"
        Exit Sub
    End If
    
    strSglProcLog = strSglProcLog & "Create/Verify the SLX Address" & vbCrLf
    
    'Create the SLX Address
    If InsSLXAddress(oClass, SAGECust, SLXCusts) = False Then
        strSglProcLog = strSglProcLog & "Failed Address Create/Verify" & vbCrLf
strDebug = "Failed InsSLXAddress"
'    If InsSLXAddress(oClass, AccountID, AddressID, SAGECust, SLXCusts) = False Then
'    If InsSLXAddress(oClass, CompanyID, AccountID, AddressID, AddrKey, AddrName, AddrLine1, AddrLine2, AddrLine3, AddrLine4, City, State, Zip, Country) = False Then
        'Did not Create the Address
    End If
    
    strSglProcLog = strSglProcLog & "End of Process SLX Cust Address" & vbCrLf
    
    Exit Sub
Error:
    MsgBox sMyName & ".ProcessSLXCustData()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not insert/update the Sales Logix Opportunity with the Blanket Sales Order/Contract data you will need to do this manually." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub


Public Function InsSLXAccount(oClass As Object, SAGECust As clsSAGECustomer, ByRef AccountID As String, ByRef SLXAccts As clsSLXCustomers, Optional bAutoLink As Boolean = False) As Boolean
'Public Function InsSLXAccount(oClass As Object, CompanyID As String, CustKey As Long, CustID As String, CustName As String, ByRef AccountID As String) As Boolean
    On Error GoTo Error
    
    Dim CompanyID As String, CustKey As Long, CustID As String, CustName As String

strDebug = "InsSLXAccount"
    strSglProcLog = strSglProcLog & "Get SAGE CompanyID and Cust data" & vbCrLf
    
    CompanyID = SAGECust.CompanyID
    CustKey = SAGECust.CustKey
    CustID = SAGECust.CustID
    CustName = SAGECust.CustName
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    
    Dim MASUserID As String
    Dim MASDate As Date
    
    Dim SECCODEID As String
    Dim AddressID As String
    Dim MASLink_SGSID As String
    
    Dim SLXCusts As New clsSLXCustomers
    Dim SLXCust As New clsSLXCustomer
    Dim NbrAutoMatches As Integer
    
    strSglProcLog = strSglProcLog & "Get SAGE User" & vbCrLf
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    MASDate = Date + Time
        
    strSglProcLog = strSglProcLog & "Get SLX Connection" & vbCrLf
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    strSglProcLog = strSglProcLog & "Open SLX Connection" & vbCrLf
    Conn.Open
    
    strSglProcLog = strSglProcLog & "Create SQL to verify Existance of Account" & vbCrLf
    
    'Check to see if the SLX Account Already Exists
    SQL = "Select ACCOUNTID, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, MASCUSTKEY, MASCOMPANYID, MASCUSTID From sysdba.AccountMASLink_SGS Where MASCustID = " & gsQuoted(CustID)
'    SQL = "Select ACCOUNTID, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, MASCUSTKEY, MASCOMPANYID, MASCUSTID From AccountMASLink_SGS Where MASCustID = " & gsQuoted(CustID)
    
    strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf
    
    If RS.State = 1 Then RS.Close
    
    strSglProcLog = strSglProcLog & "Run SQL" & vbCrLf
    
    Set RS = Conn.Execute(SQL)
    
    strSglProcLog = strSglProcLog & "Check if records were returned" & vbCrLf
    
    If RS.RecordCount > 0 Then
        strSglProcLog = strSglProcLog & "Records were returned" & vbCrLf
        
        AccountID = gsGetValidStr(RS("AccountID").Value)
        
        SLXCust.AccountID = AccountID
        SLXCust.AccountType = "Customer"
        SLXCusts.AddExisting SLXCust, SLXCusts
        
strDebug = strDebug & vbCrLf & "InsSLXAccount - AccountID = gsGetValidStr(RS('AccountID').Value) = " & AccountID
        
        strSglProcLog = strSglProcLog & "Loop Through returned records to see if the SAGE CustKey matches" & vbCrLf
        
        RS.MoveFirst
        While Not RS.BOF And Not RS.EOF
            If CustKey = glGetValidLong(RS("MASCustKey").Value) Then
                strSglProcLog = strSglProcLog & "The CustKey was found.  This Customer exists in SLX" & vbCrLf
                
strDebug = strDebug & vbCrLf & "InsSLXAccount - The records has already been created... do not create again"
                'The records has already been created... do not create again
                InsSLXAccount = True
                GoTo CleanUP
            End If
            RS.MoveNext
        Wend
        
        strSglProcLog = strSglProcLog & "The Cust/Account exists in SLX but the link to SAGE is missing" & vbCrLf
        
        GoTo InsertLink
    End If
    
    strSglProcLog = strSglProcLog & "Check to see if there is a Customer in SLX that may match this SAGE Customer.  This will require the user to verify 'Yes/No'" & vbCrLf
    
    'Check to see if there is a Customer in SLX that may match this SAGE Customer.  This will require the user to verify "Yes/No"
    NbrAutoMatches = 0
    Set SLXCusts = GetSLXAccounts(oClass, SAGECust, bAutoLink, NbrAutoMatches)
    
    If SLXCusts.Count > 0 Then
        strSglProcLog = strSglProcLog & "Possible matching records were found.  Check for Auto link" & vbCrLf
        
        If bAutoLink = False Or (bAutoLink = True And NbrAutoMatches <= 0) Then
            strSglProcLog = strSglProcLog & "Auto Link was not selected or no exact match was found." & vbCrLf
            
            Set FrmMatch.oClass = oClass
            Set FrmMatch.SAGECust = SAGECust
            Set FrmMatch.SLXCusts = SLXCusts
            
            strSglProcLog = strSglProcLog & "Load Matching Screen" & vbCrLf
            
            FrmMatch.Show vbModal
            
            If FrmMatch.bCancel = True Then
                strSglProcLog = strSglProcLog & "User Canceled the import or sckipped this record" & vbCrLf
                'User Canceled the import or skipped this record
                InsSLXAccount = False
                GoTo CleanUP
            End If
                        
            Set SLXCusts = Nothing
            
            Set SLXCusts = FrmMatch.SLXCusts
        End If
        
        strSglProcLog = strSglProcLog & "Check to see if user found linking record" & vbCrLf
        
        If SLXCusts.Count > 0 Then
            strSglProcLog = strSglProcLog & "User found linking record" & vbCrLf
        
            GoTo InsertLink
        End If
        
    End If
    
strDebug = strDebug & vbCrLf & "InsSLXAccount - SLXCusts.Count #1 = " & CStr(SLXCusts.Count)
    
    
    strSglProcLog = strSglProcLog & "Get Values to Create Account in SLX" & vbCrLf
    
    'Get Values for creation of new account.
    If RS.State = 1 Then RS.Close
    
    SQL = "slx_dbids('Account', 1)"

    Set RS = Conn.Execute(SQL)

    If RS.RecordCount > 0 Then
        strSglProcLog = strSglProcLog & "New AccountID was obtained" & vbCrLf
        AccountID = gsGetValidStr(RS("ID").Value)
    Else
        strSglProcLog = strSglProcLog & "Could not get the next AccountID" & vbCrLf
        InsSLXAccount = False
        MsgBox "Could not get the next key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
        GoTo CleanUP
    End If

    If RS.State = 1 Then RS.Close
    
    strSglProcLog = strSglProcLog & "Create SQL for Insert int SLX Account" & vbCrLf
    
    SECCODEID = "SYST00000001"  'Hard Coded Per Steve Estes 6/16/2010

    SQL = "Insert Into Account (AccountID, Account, SecCodeID, AccountManagerID, CreateUser, CreateDate, ModifyUser, ModifyDate, Account_UC, Type " & _
    " ) "

    SQL = SQL & "Values(" & _
    gsQuoted(AccountID) & ", " & _
    gsQuoted(CustName) & ", " & _
    gsQuoted(SECCODEID) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(MASDate) & ", " & _
    gsQuoted(CustName) & ", " & _
    "'Customer' " & _
    " ) "

strDebug = strDebug & vbCrLf & "InsSLXAccount - SQL = " & SQL
    strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf

    Conn.Execute SQL
    
    Set SLXCusts = Nothing
    Set SLXCusts = New clsSLXCustomers
    
    SLXCust.AccountID = AccountID
    SLXCust.Account = CustName
    SLXCust.Account_UC = CustName
    SLXCust.AccountType = "Customer"
    SLXCusts.AddExisting SLXCust, SLXCusts
    
InsertLink:
    strSglProcLog = strSglProcLog & "Insert into SLX Account Link Table" & vbCrLf
    
    'Insert into the Link Table
    If RS.State = 1 Then RS.Close
    
    strSglProcLog = strSglProcLog & "Start Loop Through Accounts records to Link" & vbCrLf

strDebug = strDebug & vbCrLf & "InsSLXAccount - AccountID = " & AccountID & " SLXCusts.Count #2 = " & CStr(SLXCusts.Count)
    For Each SLXCust In SLXCusts
        strSglProcLog = strSglProcLog & "Create SQL to Insert Account Link" & vbCrLf
        
        SQL = "Insert into AccountMASLink_SGS (AccountID, CreateUser, CreateDate, ModifyUser, ModifyDate, MASCompanyID, MASCustKey, MASCustID ) "
    
        SQL = SQL & "Values(" & _
        gsQuoted(SLXCust.AccountID) & ", " & _
        gsQuoted(MASUserID) & ", " & _
        gsQuoted(MASDate) & ", " & _
        gsQuoted(MASUserID) & ", " & _
        gsQuoted(MASDate) & ", " & _
        gsQuoted(CompanyID) & ", " & _
        CustKey & ", " & _
        gsQuoted(CustID) & " " & _
        " ) "
        
        strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf
        
strDebug = strDebug & vbCrLf & "InsSLXAccount - SQL = " & SQL
        
        Conn.Execute SQL
    Next
    
    strSglProcLog = strSglProcLog & "End Loop Through Accounts records to Link" & vbCrLf
    
'    Set SLXAccts = SLXCusts
    
    InsSLXAccount = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    Set SLXAccts = SLXCusts
        
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox sMyName & ".InsSLXAccount()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function

Public Function InsSLXAddress(oClass As Object, SAGECust As clsSAGECustomer, SLXAccts As clsSLXCustomers) As Boolean
'Public Function InsSLXAddress(oClass As Object, AccountID As String, ByRef AddressID As String, SAGECust As clsSAGECustomer, SLXAccts As clsSLXCustomers) As Boolean
'Public Function InsSLXAddress(oClass As Object, CompanyID, AccountID As String, ByRef AddressID As String, AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String, AddrLine4 As String, City As String, State As String, Zip As String, Country As String) As Boolean
' AccountID As String, AddressID As String, AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String, AddrLine4 As String, City As String, State As String, Zip As String, Country As String
    
    On Error GoTo Error
    
    Dim CompanyID, AddrKey As Long, AddrName As String, AddrLine1 As String, AddrLine2 As String, AddrLine3 As String
    Dim AddrLine4 As String, City As String, State As String, Zip As String, Country As String
    
    Dim AccountID As String
    Dim AddressID As String
    
    Static bHideSQL As Boolean
    
    strSglProcLog = strSglProcLog & "Get Cust Addr Data" & vbCrLf
    
    CompanyID = SAGECust.CompanyID
    AddrName = SAGECust.AddrName
    AddrLine1 = SAGECust.AddrLine1
    AddrLine2 = SAGECust.AddrLine2
    AddrLine3 = SAGECust.AddrLine3
    AddrLine4 = SAGECust.AddrLine4
    City = SAGECust.City
    State = SAGECust.State
    Zip = SAGECust.Zip
    Country = SAGECust.Country
    
    Dim SLXCust As New clsSLXCustomer
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    
    Dim MASUserID As String
    Dim MASDate As Date
    
    Dim MASLink_SGSID As String
    
    strSglProcLog = strSglProcLog & "Get SAGE User" & vbCrLf
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    MASDate = Date + Time
        
    strSglProcLog = strSglProcLog & "Get SLX Connection" & vbCrLf
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    strSglProcLog = strSglProcLog & "Open SLX Connection" & vbCrLf
    
    Conn.Open
    
    strSglProcLog = strSglProcLog & "Start Loop through each address" & vbCrLf
    
    For Each SLXCust In SLXAccts
        
        strSglProcLog = strSglProcLog & "Inside Address Loop" & vbCrLf
        
        AccountID = Empty
        AddressID = Empty
        
        AccountID = SLXCust.AccountID
        
        strSglProcLog = strSglProcLog & "Create SQL to Verify if Address Exists" & vbCrLf
        
        'Check to see if the SLX Address Already Exists
        SQL = "Select * From Address Where IsPrimary = 'Y' and EntityID = " & gsQuoted(AccountID)
        
        strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf
        
        If RS.State = 1 Then RS.Close
        
        strSglProcLog = strSglProcLog & "Run SQL" & vbCrLf
        
        Set RS = Conn.Execute(SQL)
        
        strSglProcLog = strSglProcLog & "Check if any records were returned" & vbCrLf
        
        If RS.RecordCount > 0 Then
            strSglProcLog = strSglProcLog & "Records were returned" & vbCrLf
            
            AddressID = gsGetValidStr(RS("AddressID").Value)
                    
            If RS.State = 1 Then RS.Close
            
            strSglProcLog = strSglProcLog & "Create SQL to see if the Address is Linked to SAGE" & vbCrLf
            
            SQL = "Select ADDRESSID, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, MASADDRKEY, MASCOMPANYID From AddressMASLink_SGS Where AddressID = " & gsQuoted(AddressID)
            
            Set RS = Conn.Execute(SQL)
            If RS.RecordCount > 0 Then
                RS.MoveFirst
                While Not RS.EOF And Not RS.BOF
                    If AddrKey = glGetValidLong(RS("MASAddrKey").Value) Then
                        strSglProcLog = strSglProcLog & "Address Link Already Exists" & vbCrLf
                        'Link already exists
                        GoTo CleanUP
                    End If
                    RS.MoveNext
                Wend
            
            End If
            
            GoTo InsertLink
        End If
        
        strSglProcLog = strSglProcLog & "Get New AddressID" & vbCrLf
        
        'Get Values for creation of new address.
        If RS.State = 1 Then RS.Close
        
        SQL = "slx_dbids('Address', 1)"
    
        Set RS = Conn.Execute(SQL)
    
        If RS.RecordCount > 0 Then
            AddressID = gsGetValidStr(RS("ID").Value)
        Else
            strSglProcLog = strSglProcLog & "Could not get Address ID" & vbCrLf
            InsSLXAddress = False
            MsgBox "Could not get the next key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
            GoTo CleanUP
        End If
    
        strSglProcLog = strSglProcLog & "Create SQL to Insert New Address" & vbCrLf
        
    '    SQL = "Insert Into Address (EntityID, AddressID, Description, IsMailing, IsPrimary, Address1, Address2, Address3, Address4, City, State, PostalCode, Country, CreateUser, CreateDate, ModifyUser, ModifyDate "
        SQL = "Insert Into Address (EntityID, AddressID, Description, IsMailing, IsPrimary, Address1, Address2, Address3,  City, State, PostalCode, Country, CreateUser, CreateDate, ModifyUser, ModifyDate " & _
        " ) "
        
    '    gsQuoted(AddrLine4) & ", " & _

        SQL = SQL & "Values(" & _
        gsQuoted(AccountID) & ", " & _
        gsQuoted(AddressID) & ", " & _
        gsQuoted(AddrName) & ", " & _
        "'Y', " & _
        "'Y', " & _
        gsQuoted(AddrLine1) & ", " & _
        gsQuoted(AddrLine2) & ", " & _
        gsQuoted(AddrLine3) & ", " & _
        gsQuoted(City) & ", " & _
        gsQuoted(State) & ", " & _
        gsQuoted(Zip) & ", " & _
        gsQuoted(Country) & ", " & _
        gsQuoted(MASUserID) & ", " & _
        gsQuoted(MASDate) & ", " & _
        gsQuoted(MASUserID) & ", " & _
        gsQuoted(MASDate) & " " & _
        " ) "
    
        strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf

'If bHideSQL = False Then
'    If MsgBox("Insert SQL is: " & vbCrLf & SQL & vbCrLf & vbCrLf & "Do you want to continue Debug?", vbYesNo, "Show SQL") <> vbYes Then
'        bHideSQL = True
'    End If
'End If
        strSglProcLog = strSglProcLog & "Run SQL" & vbCrLf
        
        Conn.Execute SQL
        
        strSglProcLog = strSglProcLog & "Create SQL to Update Account Primary Shipping Address" & vbCrLf
        
        'Update Account Primary and Shipping Address
        SQL = "Update Account Set AddressID = " & gsQuoted(AddressID) & ", ShippingID = " & gsQuoted(AddressID) & " Where AccountID = " & gsQuoted(AccountID)
        
        strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf

'If bHideSQL = False Then
'    If MsgBox("Insert SQL is: " & vbCrLf & SQL & vbCrLf & vbCrLf & "Do you want to continue Debug?", vbYesNo, "Show SQL") <> vbYes Then
'        bHideSQL = True
'    End If
'End If
        strSglProcLog = strSglProcLog & "Run SQL" & vbCrLf
        
        Conn.Execute SQL
        
InsertLink:
        'Insert into the Link Table
        If RS.State = 1 Then RS.Close
    
        
        strSglProcLog = strSglProcLog & "Create SQL to Insert Address Link to SAGE" & vbCrLf
        
        SQL = "Insert into AddressMASLink_SGS (AddressID, CreateUser, CreateDate, ModifyUser, ModifyDate, MASAddrKey, MASCompanyID ) "
    
        SQL = SQL & "Values(" & _
        gsQuoted(AddressID) & ", " & _
        gsQuoted(MASUserID) & ", " & _
        gsQuoted(MASDate) & ", " & _
        gsQuoted(MASUserID) & ", " & _
        gsQuoted(MASDate) & ", " & _
        AddrKey & ", " & _
        gsQuoted(CompanyID) & " " & _
        " ) "
        
        strSglProcLog = strSglProcLog & "SQL = " & SQL & vbCrLf & vbCrLf
        
'If bHideSQL = False Then
'    If MsgBox("Insert SQL is: " & vbCrLf & SQL & vbCrLf & vbCrLf & "Do you want to continue Debug?", vbYesNo, "Show SQL") <> vbYes Then
'        bHideSQL = True
'    End If
'End If
        
        strSglProcLog = strSglProcLog & "Run SQL" & vbCrLf
        
        Conn.Execute SQL
        
    Next
    
    strSglProcLog = strSglProcLog & "End Loop through each address" & vbCrLf
    
    InsSLXAddress = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox sMyName & ".InsSLXAddress()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "SQL = " & SQL, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


Public Function GetSLXConnStr(oClass As Object) As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    On Error Resume Next
    Dim ConnStr As String
    
    ConnStr = gsGetValidStr(oClass.moAppDB.Lookup("SLXConnStr", "tciSLXConnStr_SGS", Empty))
    
    ConnStr = Decrypt(ConnStr)
    
    GetSLXConnStr = ConnStr
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetSLXConnStr", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function Decrypt(Val As String) As String
    On Error GoTo Error
    
    Dim i As Long
    Dim str As String
    Dim NewVal As Variant
    Dim DecryptedVal As String
    
    For i = 1 To Len(Val)
        str = Mid(Val, i, 4)
        
        If IsNumeric(str) Then
            str = str - 1000
        End If
        
        i = i + 3
        
        NewVal = Chr(CLng(str))
        DecryptedVal = DecryptedVal & NewVal
        
    Next
    
    Decrypt = DecryptedVal
    
    Exit Function
Error:
    MsgBox sMyName & ".Decrypt(Val As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

End Function


'This process is to link missing links between SLX and SAGE Customers for the current Company.
Public Sub LinkAllCustomers(oClass As Object, Optional bUpdForm As Boolean = False, Optional bAutoLink As Boolean = False, Optional bCheckMissingAddr As Boolean = False)
    On Error GoTo Error
    
    Dim icnt As Long
    Dim iCurrRec As Long
    
    Dim CustKey As Long
    Dim CustID As String
    Dim CustName As String
    
    Dim AddrKey As Long
    Dim AddrName As String
    Dim AddrLine1 As String
    Dim AddrLine2 As String
    Dim AddrLine3 As String
    Dim AddrLine4 As String
    Dim City As String
    Dim State As String
    Dim Zip As String
    Dim Country As String

    Dim RS      As New DASRecordSet       'SOTADAS recordset Object
    Dim sSQL    As String       'SQL String
        
    If bCheckMissingAddr = False Then
        strProcessLog = strProcessLog & "Create SQL to Import Missing Customers" & vbCrLf
        
        'Get Customer not linked to SLX
        sSQL = "select c.CompanyID, c.CustKey, c.CustID, Coalesce(c.CustName,'') 'CustName', "
        sSQL = sSQL & "Coalesce(a.AddrKey,0) 'AddrKey', Coalesce(a.AddrName, '') 'AddrName', "
        sSQL = sSQL & "Coalesce(a.AddrLine1,'') 'AddrLine1', Coalesce(a.AddrLine2,'') 'AddrLine2', Coalesce(a.AddrLine3,'') 'AddrLine3', Coalesce(a.AddrLine4,'') 'AddrLine4', Coalesce(a.AddrLine5,'') 'AddrLine5', "
        sSQL = sSQL & "Coalesce(a.City,'') 'City', Coalesce(a.StateID, '') 'StateID', Coalesce(a.PostalCode,'') 'PostalCode', Coalesce(a.CountryID,'') 'CountryID' "
        sSQL = sSQL & "From vluSLXCustomer_SGS slx "
        sSQL = sSQL & "Inner Join tarCustomer c with(NOLock) "
        sSQL = sSQL & "    on SLX.CustKey = c.custkey "
        sSQL = sSQL & "    and c.Status = 1 "
        sSQL = sSQL & "Inner Join tciAddress a with(NoLock) "
        sSQL = sSQL & "    on c.PrimaryAddrKey = a.AddrKey "
        sSQL = sSQL & "Inner Join tciCompanySendToSLX_RKL comp with(NoLock) "
        sSQL = sSQL & "    on c.CompanyID = comp.CompanyID "
        sSQL = sSQL & "Where SLX.AccountID Is Null "
        sSQL = sSQL & "and c.companyid = " & gsQuoted(msCompanyID)
        sSQL = sSQL & " and Ltrim(RTrim(coalesce(c.CustName, ''))) <> '' "
    Else
        strProcessLog = strProcessLog & "Create SQL to Import Missing SLX Address" & vbCrLf
        
        'Get Customer linked to SLX but are missing an address (Address is required to lookup Customer in SLX)
        sSQL = "select c.CompanyID, c.CustKey, c.CustID, Coalesce(c.CustName,'') 'CustName', "
        sSQL = sSQL & "Coalesce(a.AddrKey,0) 'AddrKey', Coalesce(a.AddrName, '') 'AddrName', "
        sSQL = sSQL & "Coalesce(a.AddrLine1,'') 'AddrLine1', Coalesce(a.AddrLine2,'') 'AddrLine2', Coalesce(a.AddrLine3,'') 'AddrLine3', Coalesce(a.AddrLine4,'') 'AddrLine4', Coalesce(a.AddrLine5,'') 'AddrLine5', "
        sSQL = sSQL & "Coalesce(a.City,'') 'City', Coalesce(a.StateID, '') 'StateID', Coalesce(a.PostalCode,'') 'PostalCode', Coalesce(a.CountryID,'') 'CountryID' "
        sSQL = sSQL & "From vluSLXCustomer_SGS slx "
        sSQL = sSQL & "Inner Join tarCustomer c with(NOLock) "
        sSQL = sSQL & "    on SLX.CustKey = c.custkey "
        sSQL = sSQL & "    and c.Status = 1 "
        sSQL = sSQL & "Inner Join tciAddress a with(NoLock) "
        sSQL = sSQL & "    on c.PrimaryAddrKey = a.AddrKey "
        sSQL = sSQL & "Inner Join tciCompanySendToSLX_RKL comp with(NoLock) "
        sSQL = sSQL & "    on c.CompanyID = comp.CompanyID "
        sSQL = sSQL & "Where SLX.AccountID Is not Null "
        sSQL = sSQL & "and c.companyid = " & gsQuoted(msCompanyID)
        sSQL = sSQL & " and Ltrim(RTrim(coalesce(c.CustName, ''))) <> '' "
        sSQL = sSQL & " and coalesce(slx.MASLinkAddrCnt, 0) < = 0 "
    End If
    
    strProcessLog = strProcessLog & "SQL = " & sSQL & vbCrLf & vbCrLf
    
    strProcessLog = strProcessLog & "Check to See if User Stopped the process" & vbCrLf
    
    DoEvents
    If msBStopProcess = True Then Exit Sub
    
    strProcessLog = strProcessLog & "Run the SQL" & vbCrLf
    'Run query to get Missing Customer Links
    Set RS = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
       
    
    strProcessLog = strProcessLog & "Check for Results from the SQL" & vbCrLf
    
    If RS.RecordCount > 0 Then
        strProcessLog = strProcessLog & "Records were returned" & vbCrLf
        icnt = 0
        While Not RS.IsEOF
            icnt = icnt + 1
            DoEvents
            RS.MoveNext
        Wend
        
        RS.MoveFirst
    
        If MsgBox("There are " & icnt & " Customers records that are about to be linked between SAGE and SalesLogix.  Do you want to continue?", vbYesNo, "Confirm Link Action") <> vbYes Then
            strProcessLog = strProcessLog & "User Choose not to continue with the import process" & vbCrLf
            
            Exit Sub
        End If
    Else
        strProcessLog = strProcessLog & "No Records were returned, nothing to import" & vbCrLf
        MsgBox "There are no Customer records to link.", vbExclamation, "SAGE 500"
        Exit Sub
    End If
    
    strProcessLog = strProcessLog & "Check to See if User Stopped the process" & vbCrLf
    
    DoEvents
    If msBStopProcess = True Then Exit Sub
    
    iCurrRec = 0
    
    strProcessLog = strProcessLog & "Start Loop through records to import." & vbCrLf
    
    'There is customer class information
    While Not RS.IsEOF
        strSglProcLog = Empty   'Clear this off for each record we will only capture one record
        
        strSglProcLog = strSglProcLog & "Check to See if User Stopped the process" & vbCrLf
        
        DoEvents
        If msBStopProcess = True Then Exit Sub
        
        strSglProcLog = strSglProcLog & "Get Customer and Cust Address Data" & vbCrLf
        
        iCurrRec = iCurrRec + 1
        
        CustKey = glGetValidLong(RS.Field("CustKey"))
        CustID = Trim(gsGetValidStr(RS.Field("CustID")))
        CustName = Trim(gsGetValidStr(RS.Field("CustName")))
        
        AddrKey = glGetValidLong(RS.Field("AddrKey"))
        
        AddrName = Trim(gsGetValidStr(RS.Field("AddrName")))
        AddrLine1 = Trim(gsGetValidStr(RS.Field("AddrLine1")))
        AddrLine2 = Trim(gsGetValidStr(RS.Field("AddrLine2")))
        AddrLine3 = Trim(gsGetValidStr(RS.Field("AddrLine3")))
        AddrLine4 = Trim(gsGetValidStr(RS.Field("AddrLine4")))
        City = Trim(gsGetValidStr(RS.Field("City")))
        State = Trim(gsGetValidStr(RS.Field("StateID")))
        Zip = Trim(gsGetValidStr(RS.Field("PostalCode")))
        Country = Trim(gsGetValidStr(RS.Field("CountryID")))
        
        DoEvents
        
        If bUpdForm = True Then
            DoEvents
            frmSendSLX.txtCustLinkStatus.Text = CStr(iCurrRec) & " of " & CStr(icnt)
            frmSendSLX.txtCustCurrRec.Text = "CustID: " & CustID & " || Cust Name: " & CustName
            DoEvents
        End If
        
        strSglProcLog = strSglProcLog & "Process SLX Cust Data" & vbCrLf
        
        ProcessSLXCustData oClass, msCompanyID, CustKey, CustID, CustName, AddrKey, AddrName, AddrLine1, AddrLine2, AddrLine3, AddrLine4, City, State, Zip, Country, bAutoLink
        
        DoEvents
        
        RS.MoveNext
    Wend
    strProcessLog = strProcessLog & strSglProcLog & vbCrLf
    
    strProcessLog = strProcessLog & "End Loop through records to import." & vbCrLf
    
    RS.Close

    strProcessLog = strProcessLog & "The Customer link process is complete." & vbCrLf
    
    MsgBox "The Customer link process is complete.", vbExclamation, "SAGE 500"
'MsgBox "Debug message: " & strDebug
    
    Exit Sub
Error:
    MsgBox sMyName & ".LinkAllCustomers()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear
End Sub


Public Sub ProcessSLXOpporData(NewSOKey As Long, oClass As Object, Optional ProjDesc As String = Empty)
    On Error GoTo Error
    
    Dim lTranType As Long
    Dim SLXAccountID As String
    Dim BSOTranNo As String
    Dim QuoteBidNumber As String
    Dim lCustKey As Long
    Dim OpportunityID As String
    
    Dim SLXContractNumber As String

    Dim dCloseDate As Date
    Dim sDesc As String
    Dim sClosed As String
    Dim sStatus As String
    Dim sAcctMgrID As String
    Dim lCloseProb As Long
    Dim dOpened As Date
    Dim SalesPotential As Double
    Dim var As Variant

    'Set Some Defaults
    dOpened = Date + Time
    SalesPotential = 0#
    sStatus = "Open"
    sClosed = "F"
    lCloseProb = 1

strDebug = "ProcessSLXOpporData"

strDebug = strDebug & vbCrLf & " - NewSOKey = " & CStr(NewSOKey)

    'Check to ensure that new Record exists
    If glGetValidLong(oClass.moAppDB.Lookup("Count(*)", "tsoSalesOrder", "SOKey = " & NewSOKey)) <= 0 Then
'        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
strDebug = strDebug & vbCrLf & " - Could not insert/update SLX Opportunity... the Quote or Contract does not exist in MAS 500"
            MsgBox "Could not insert/update SLX Opportunity... the Quote or Contract does not exist in MAS 500" & vbCrLf & _
            "SoKey = " & NewSOKey, vbExclamation, "MAS 500"
'        End If
        Exit Sub
    End If

    'Check the Transaction Type (only use BSO or Quote... do not do anything with SO)
    lTranType = glGetValidLong(oClass.moAppDB.Lookup("TranType", "tsoSalesOrder", "SOKey = " & NewSOKey))

    Select Case lTranType
        Case 801    'Sales Order - return with no action
            Exit Sub
        Case 802    'BSO - Insert/Update SLX
            'Continue On
        Case 840    'Quote - Insert/Update SLX
            'Continue On
        Case Else   'Unknown - return with no action
            Exit Sub
    End Select

strDebug = strDebug & vbCrLf & " - TranType = " & CStr(lTranType)

    'Get EstimatedClose from SO TranDate
    var = gsGetValidStr(oClass.moAppDB.Lookup("TranDate", "tsoSalesOrder", "SOKey = " & NewSOKey))
    If IsDate(var) = True Then
        dCloseDate = CDate(var)
    Else
        dCloseDate = Date
    End If

strDebug = strDebug & vbCrLf & " - dCloseDate = " & CStr(dCloseDate)


    'Get Opportunity Description - Default to MAS SO Project Number
    sDesc = Left(Trim(gsGetValidStr(oClass.moAppDB.Lookup("ProjectDesc", "tsoSalesOrderExt_SGS", "SOKey = " & NewSOKey))), 64)
    If Trim(sDesc) = Empty Then
        If Trim(ProjDesc) = Empty Then
            sDesc = "(None)"
        Else
            sDesc = Left(Trim(ProjDesc), 64)
        End If
    End If

    'Get BSO TranNo
    BSOTranNo = gsGetValidStr(oClass.moAppDB.Lookup("TranNo", "tsoSalesOrder", "SOKey = " & NewSOKey))

strDebug = strDebug & vbCrLf & " - BSOTranNo = " & BSOTranNo

    'Get QuoteBidNumber
    QuoteBidNumber = gsGetValidStr(oClass.moAppDB.Lookup("BidNo", "tsoSalesOrderExt_SGS", "SOKey = " & NewSOKey))
strDebug = strDebug & vbCrLf & " - QuoteBidNumber = " & QuoteBidNumber

    If Trim(QuoteBidNumber) = Empty Then
        If lTranType = 840 Then
            QuoteBidNumber = BSOTranNo
'            MsgBox "Could not insert/update SLX Opportunity... the Bid Number is missing.  You will need to manually create this Opportunity.", vbExclamation, "MAS 500"
'            Exit Sub
        Else
'            QuoteBidNumber = BSOTranNo
        End If
    End If

    'Check to see if the Customer exists in SLX
    lCustKey = glGetValidLong(oClass.moAppDB.Lookup("CustKey", "tsoSalesOrder", "SOKey = " & NewSOKey))
    If glGetValidLong(oClass.moAppDB.Lookup("Count(*)", "vluSLXCustomer_SGS", "AccountReferenceID is not null and CustKey = " & lCustKey)) <= 0 Then
'        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(oClass.moSysSession.CompanyID) <> "WEI" Then
strDebug = strDebug & vbCrLf & " - Could not insert/update SLX Opportunity... the Customer/Account does not exist in SalesLogix."
            MsgBox "Could not insert/update SLX Opportunity... the Customer/Account does not exist in SalesLogix.  You will need to manually create this Opportunity.", vbExclamation, "MAS 500"
'        End If
        Exit Sub
    End If
strDebug = strDebug & vbCrLf & " - lCustKey = " & CStr(lCustKey)

    'Get SLX Account ID
    SLXAccountID = gsGetValidStr(oClass.moAppDB.Lookup("AccountID", "vluSLXCustomer_SGS", "AccountReferenceID is not null and CustKey = " & lCustKey))
strDebug = strDebug & vbCrLf & " - SLXAccountID = " & SLXAccountID

'****************************************************************
'****************************************************************
'RKL DEJ 2016-12-20 (START)
'****************************************************************
'****************************************************************
    'For Quotes/TranType 840: We only push to SLX the Orig Bid Number One time
    '(This is located in tsoSalesOrderExt_SGS.BidNo and on the
    'SO Screens (Header tab - Proj Misc sub tab - Bid No)
    'We lookup the SLX Opportunity by Bid No to verify that Bid only exists one time
    
    'For BSO(Contracts)/TranType 802: We push all BSO to SLX as an opportunity.
    'We first lookup the opportunity by Bid Number.  If the Opportunity exists then
    'we update the Opportunity in SLX:
    'Update:(Account/Customer, ContractNumber, MASSOKeyLink, MASContractSOKey)
    '***NOTE (2016-12-21) this currently does not update the MASQuoteSOKey field (so if the Contract was awared to a different Customer than the original quote then this will not point to the same customer...)
    'If the Opportunity is not found using the Bid Number then look to see if the Opportunity
    'can be found using the ContractNumber/BSO Number.  If found then the same update logic is used
    'If the Opportunity still does not exist then create a new opportunity record
    
'    'Check to see if SLX has an existing Opportunity (If so then update if a 802 else if not then Insert)
    
    If lTranType = 840 Then
        'For Quotes always lookup by BidNumber
        OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
        
    Else
        'For Contracts(BSO) Lookup first by SOKey
        'Second Lookup by BidNumber
        'Thrid Lookup by Contract (BSO) Number
        
        OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndContracts_SGS", "SOKey = " & gsGetValidStr(NewSOKey) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
        
        If Trim(OpportunityID) = Empty Then
            OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
            
            'The following logic only comes into play if looking up and found by BidNumber
            If Trim(OpportunityID) <> Empty Then
                'Check to see what the contract number in SLX is for this Bid Number
                
                'SGS DEJ 4/26/12 (START)
                'Some BSO are copied from BSO to BSO and that has been overwriting the SLX Opportuinity Contract Number.
                'On 4/26/12 Hal C. said if the SLX Opportuinity Exists and Contract Number in SLX has already been assigned, then create a
                'New SLX Opportunity with no Bid Number
                
                SLXContractNumber = gsGetValidStr(oClass.moAppDB.Lookup("ContractNumber", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
                
                If Trim(SLXContractNumber) <> Empty Then
                    If UCase(Trim(BSOTranNo)) <> UCase(Trim(SLXContractNumber)) Then
                        OpportunityID = Empty
                        QuoteBidNumber = Empty
                    End If
                End If
            End If
        End If
        
        If Trim(OpportunityID) = Empty Then
            OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndContracts_SGS", "ContractNumber = " & gsQuoted(BSOTranNo) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
        End If
        
        
    End If
    
    
'****************************************************************
'****************************************************************
'RKL DEJ 2016-12-20 (STOP)
'****************************************************************
'****************************************************************
    
'    'Check to see if SLX has an existing Opportunity (If so then update if not then Insert)
'    OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
'
'    If Trim(OpportunityID) = Empty And lTranType = 802 Then
'        'Search by Contract ID (BSO TranNo)
'        OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndContracts_SGS", "ContractNumber = " & gsQuoted(BSOTranNo) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
'
'    ElseIf Trim(OpportunityID) <> Empty And lTranType = 802 Then
'        'Check to see what the contract number in SLX is for this Bid Number
'        'SGS DEJ 4/26/12 (START)
'        'Some BSO are copied from BSO to BSO and that has been overwriting the SLX Opportuinity Contract Number.
'        'On 4/26/12 Hal C. said if the SLX Opportuinity Exists and Contract Number in SLX has already been assigned, then create a
'        'New SLX Opportunity with no Bid Number
'        SLXContractNumber = gsGetValidStr(oClass.moAppDB.Lookup("ContractNumber", "vluSLXOpportunityAndQuotes_SGS", "BidNumber = " & gsQuoted(QuoteBidNumber) & " And CompanyID = " & gsQuoted(oClass.moSysSession.CompanyID)))
'
'        If Trim(SLXContractNumber) > Empty Then
'            If UCase(Trim(BSOTranNo)) <> UCase(Trim(SLXContractNumber)) Then
'                OpportunityID = Empty
'                QuoteBidNumber = Empty
'            End If
'        End If
'        'SGS DEJ 4/26/12 (End)
'
'    End If

strDebug = strDebug & vbCrLf & " - OpportunityID = " & OpportunityID & " - SLXContractNumber = " & SLXContractNumber & " - OpportunityID = " & OpportunityID & " - QuoteBidNumber = " & QuoteBidNumber

    If Trim(OpportunityID) = Empty Then
        'Create New Opportunity

        If lTranType = 840 Then
            BSOTranNo = Empty
        End If

strDebug = strDebug & vbCrLf & " - BSOTranNo = " & BSOTranNo
        
        InsSLXOppor oClass, OpportunityID, SLXAccountID, BSOTranNo, QuoteBidNumber, dCloseDate, sDesc, sClosed, sStatus, sAcctMgrID, lCloseProb, dOpened, SalesPotential, NewSOKey

    Else
strDebug = strDebug & vbCrLf & " - Update Opportunity"
        'Update Opportunity
        If lTranType = 802 Then
            'Only need to update Contracts/BSO
            UpdateSLXOppor oClass, OpportunityID, SLXAccountID, BSOTranNo, NewSOKey
        End If

    End If
    
    Exit Sub
Error:
    MsgBox sMyName & ".ProcessSLXOpporData()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not insert/update the Sales Logix Opportunity with the Blanket Sales Order/Contract data you will need to do this manually." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub


Public Function UpdateSLXOppor(oClass As Object, OpportunityID As String, SLXAccountID As String, BSOTranNo As String, SOkey As Long) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim SQL As String
    Dim C_UnilateralID As String
    Dim MASUserID As String
    
strDebug = strDebug & vbCrLf & " - Method: UpdateSLXOppor"
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    SQL = "Update OPPORTUNITY Set " & _
    "ContractNumber = " & gsQuoted(BSOTranNo) & ", " & _
    "AccountID = " & gsQuoted(SLXAccountID) & ", " & _
    "ModifyUser = " & gsQuoted(Left(MASUserID, 12)) & ", " & _
    "ModifyDate = " & gsQuoted(Date + Time) & ", " & _
    "MASSOKeyLink = " & SOkey & ", " & _
    "MASContractSOKey = " & SOkey & " "

    SQL = SQL & "Where OpportunityID = " & gsQuoted(OpportunityID)

    Conn.Execute SQL
    
strDebug = strDebug & vbCrLf & " - Method: SQL = " & SQL
    
    UpdateSLXOppor = True

CleanUP:
    Err.Clear
    On Error Resume Next
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox sMyName & ".UpdateSLXOppor()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function

Public Function InsSLXOppor(oClass As Object, ByRef OpportunityID As String, SLXAccountID As String, BSOTranNo As String, QuoteBidNumber As String, _
dCloseDate As Date, sDesc As String, sClosed As String, sStatus As String, sAcctMgrID As String, lCloseProb As Long, dOpened As Date, SalesPotential As Double, SOkey As Long) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    Dim C_UnilateralID As String
    Dim MASUserID As String
    Dim SECCODEID As String
    
strDebug = strDebug & vbCrLf & " - Method: InsSLXOppor"
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    SQL = "slx_dbids('Opportunity', 1)"

    Set RS = Conn.Execute(SQL)

    If RS.RecordCount > 0 Then
        OpportunityID = gsGetValidStr(RS("ID").Value)
strDebug = strDebug & vbCrLf & " - RS.RecordCount > 0 = OpportunityID = " & OpportunityID
    Else
        InsSLXOppor = False
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not get the next key value to create the record in SalesLogix.", vbExclamation, "MAS 500"
'        End If
strDebug = strDebug & vbCrLf & " - Could not get the next key value to create the record in SalesLogix."

        GoTo CleanUP
    End If

    SECCODEID = gsGetValidStr(oClass.moAppDB.Lookup("SECCODEID", "vluSLXCustomer_SGS", "AccountID = " & gsQuoted(SLXAccountID)))
    'Get SLX Account Manager ID from account
    sAcctMgrID = gsGetValidStr(oClass.moAppDB.Lookup("AccountManagerID", "vluSLXCustomer_SGS", "AccountID = " & gsQuoted(SLXAccountID)))
    
    If Trim(QuoteBidNumber) = Empty Then
        SQL = "Insert Into OPPORTUNITY (OpportunityID, ACCOUNTID, BidNumber, ContractNumber, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, SECCODEID, " & _
        "Description, Closed, Status, EstimatedClose, AccountManagerID, CloseProbability, DateOpened, SalesPotential, ResellerID, MASSOKeyLink, MASContractSOKey, MASCompanyID " & _
        " ) "
    Else
        SQL = "Insert Into OPPORTUNITY (OpportunityID, ACCOUNTID, BidNumber, ContractNumber, CREATEUSER, CREATEDATE, MODIFYUSER, MODIFYDATE, SECCODEID, " & _
        "Description, Closed, Status, EstimatedClose, AccountManagerID, CloseProbability, DateOpened, SalesPotential, ResellerID, MASSOKeyLink, MASQuoteSOKey, MASCompanyID " & _
        " ) "
    End If

    SQL = SQL & "Values(" & _
    gsQuoted(OpportunityID) & ", " & _
    gsQuoted(SLXAccountID) & ", " & _
    gsQuoted(QuoteBidNumber) & ", " & _
    gsQuoted(BSOTranNo) & ", " & _
    gsQuoted(Left(MASUserID, 12)) & ", " & _
    gsQuoted(Date + Time) & ", " & _
    gsQuoted(Left(MASUserID, 12)) & ", " & _
    gsQuoted(Date + Time) & ", " & _
    gsQuoted(SECCODEID) & ", " & _
    gsQuoted(sDesc) & ", " & _
    gsQuoted(sClosed) & ", " & _
    gsQuoted(sStatus) & ", " & _
    gsQuoted(dCloseDate) & ", " & _
    gsQuoted(sAcctMgrID) & ", " & _
    lCloseProb & ", " & _
    gsQuoted(dOpened) & ", " & _
    SalesPotential & ", " & _
    "'', " & _
    SOkey & ", " & _
    SOkey & ", " & _
    gsQuoted(oClass.moSysSession.CompanyID) & _
    " ) "

    Conn.Execute SQL
    
strDebug = strDebug & vbCrLf & " - SQL = " & SQL
    
    InsSLXOppor = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox sMyName & ".InsSLXOppor()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


Sub ProcessSLXOpporLineData(SOkey As Long, SOLineKey As Long, SOLineDistKey As Long, ItemKey As Long, WhseKey As Long, QtyOrd As Double, oClass As Object)
    On Error GoTo Error
    
    Dim MASOppPrdtsID As String
    Dim OpportunityID As String
    Dim iRetVal As Long
    Dim SQL As String
    
    Dim ItemID As String
    Dim ShortDesc As String
    Dim LongDesc As String
    Dim WhseID As String
    Dim WhseDesc As String
    
strDebug = strDebug & vbCrLf & "ProcessSLXOpporLineData"
    
    'Check to see if SLX has an existing Opportunity (If not then exit)
    OpportunityID = gsGetValidStr(oClass.moAppDB.Lookup("OpportunityID", "vluSLXOpportunityAndQuotes_SGS", "SOKey = " & SOkey))
    
    If Trim(OpportunityID) = Empty Then
        'No Need to process... there is no opportunity to update
        Exit Sub
    End If
    
        
    'Get ID's and Descriptions
    ItemID = gsGetValidStr(oClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey = " & ItemKey))
    ShortDesc = gsGetValidStr(oClass.moAppDB.Lookup("ShortDesc", "timItemDescription", "ItemKey = " & ItemKey))
    LongDesc = gsGetValidStr(oClass.moAppDB.Lookup("LongDesc", "timItemDescription", "ItemKey = " & ItemKey))
    
    WhseID = gsGetValidStr(oClass.moAppDB.Lookup("WhseID", "timWarehouse", "WhseKey = " & WhseKey))
    WhseDesc = gsGetValidStr(oClass.moAppDB.Lookup("Description", "timWarehouse", "WhseKey = " & WhseKey))
    
    MASOppPrdtsID = gsGetValidStr(oClass.moAppDB.Lookup("MASOpportunityProductsID", "vluSLXOpportunityProducts_SGS", "SOLineDistKey = " & SOLineDistKey))
    
    If Trim(MASOppPrdtsID) = Empty Then
        'Insert New record
        
        If InsSLXProd(oClass, OpportunityID, SOLineKey, SOLineDistKey, SOkey, ItemKey, ItemID, ShortDesc, LongDesc, _
        WhseKey, WhseID, WhseDesc, QtyOrd) = False Then
            'Did not insert new record
        End If
        
    Else
        'Update Existing record
        
        If UpdSLXProd(oClass, MASOppPrdtsID, OpportunityID, SOLineKey, SOLineDistKey, SOkey, ItemKey, ItemID, ShortDesc, LongDesc, _
        WhseKey, WhseID, WhseDesc, QtyOrd) = False Then
            'Did not update record
        End If
    End If
    
    
    Exit Sub
Error:
    MsgBox sMyName & ".ProcessSLXOpporLineData()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not insert/update the Sales Logix Opportunity Product." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    
End Sub

Function GetNextOppProdKey(oClass As Object) As String
    On Error GoTo Error

    Dim MASUserID As String
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String

    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)

    ConnStr = GetSLXConnStr(oClass)

    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr

    Conn.Open

    SQL = "slx_dbids('MASOpportunityProducts', 1)"

    Set RS = Conn.Execute(SQL)

    If RS.RecordCount > 0 Then
        GetNextOppProdKey = gsGetValidStr(RS("ID").Value)
    Else
        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not get the next key value to create the Product record in SalesLogix.", vbExclamation, "MAS 500"
'        End If
    End If

CleanUP:
    Err.Clear
    On Error Resume Next

    If RS.State = 1 Then RS.Close
    Set RS = Nothing

    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing

    Exit Function
Error:
    MsgBox sMyName & ".GetNextOppProdKey()" & vbCrLf & Date + Time & vbCrLf & _
    "Could not obtain new Key for SLX table: MASOpportunityProducts." & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"

    Err.Clear
    GoTo CleanUP

End Function


Public Function InsSLXProd(oClass As Object, OpportunityID As String, _
MASSOLineKey As Long, MASSOLineDistKey As Long, MASSOKey As Long, _
MASItemKey As Long, MASItemID As String, MASItemShrtDesc As String, MASItemLongDesc As String, _
MASWhseKey As Long, MASWhseID As String, MASWhseDesc As String, _
MASPlannedTons As Double) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    Dim MASUserID As String
    Dim InsDate As Date
    Dim MASOppPrdtsID As String
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    InsDate = Date + Time
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    'Get Next TableKey/ID
    SQL = "slx_dbids('MASOpportunityProducts', 1)"

    Set RS = Conn.Execute(SQL)

    If RS.RecordCount > 0 Then
        MASOppPrdtsID = gsGetValidStr(RS("ID").Value)
    Else
        InsSLXProd = False
'        'RKL DEJ 2016-06-07 (Added if for CompanyID) email from Kevin asking to not show the SLX messages for the WEI company
'        If UCase(frmSalesOrd.oClass.moSysSession.CompanyID) <> "WEI" Then
            MsgBox "Could not get the next key value to create the product record in SalesLogix.", vbExclamation, "MAS 500"
'        End If
        GoTo CleanUP
    End If
    
    'Create Insert Statement
    SQL = "Insert into MASOpportunityProducts(MASOpportunityProductsID, OpportunityID, MASSOLineKey, MASSOLineDistKey, " & _
    "MASItemKey, MASItemID, MASItemShortDesc, MASItemLongDesc, " & _
    "MASWhseKey, MASWhseID, MASWhseDesc, MASPlannedTons, CreateUser, CreateDate, ModifyUser, ModifyDate) "

    SQL = SQL & "Values(" & _
    gsQuoted(MASOppPrdtsID) & ", " & _
    gsQuoted(OpportunityID) & ", " & _
    MASSOLineKey & ", " & _
    MASSOLineDistKey & ", " & _
    MASItemKey & ", " & _
    gsQuoted(MASItemID) & ", " & _
    gsQuoted(MASItemShrtDesc) & ", " & _
    gsQuoted(MASItemLongDesc) & ", " & _
    MASWhseKey & ", " & _
    gsQuoted(MASWhseID) & ", " & _
    gsQuoted(MASWhseDesc) & ", " & _
    MASPlannedTons & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(InsDate) & ", " & _
    gsQuoted(MASUserID) & ", " & _
    gsQuoted(InsDate) & _
    " ) "

    'Execute insert Statment
    Conn.Execute SQL
    
    
    InsSLXProd = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox sMyName & ".InsSLXProd()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function


Public Function UpdSLXProd(oClass As Object, ByRef MASOppPrdtsID As String, OpportunityID As String, _
MASSOLineKey As Long, MASSOLineDistKey As Long, MASSOKey As Long, _
MASItemKey As Long, MASItemID As String, MASItemShrtDesc As String, MASItemLongDesc As String, _
MASWhseKey As Long, MASWhseID As String, MASWhseDesc As String, _
MASPlannedTons As Double) As Boolean
    On Error GoTo Error
    
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    Dim MASUserID As String
    Dim InsDate As Date
    
    MASUserID = gsGetValidStr(oClass.moSysSession.UserId)
    InsDate = Date + Time
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
     
    'Create Update Statement
    SQL = "Update MASOpportunityProducts Set " & _
    "ModifyUser = " & gsQuoted(MASUserID) & _
    ", ModifyDate = " & gsQuoted(InsDate) & _
    ", MASItemKey = " & MASItemKey & _
    ", MASItemID = " & gsQuoted(MASItemID) & _
    ", MASItemShortDesc = " & gsQuoted(MASItemShrtDesc) & _
    ", MASItemLongDesc = " & gsQuoted(MASItemLongDesc) & _
    ", MASWhseKey = " & gsQuoted(MASWhseKey) & _
    ", MASWhseID = " & gsQuoted(MASWhseID) & _
    ", MASWhseDesc = " & gsQuoted(MASWhseDesc) & _
    ", MASPlannedTons = " & gsQuoted(MASPlannedTons) & _
    "Where MASOpportunityProductsID = " & gsQuoted(MASOppPrdtsID)


    'Execute update Statment
    Conn.Execute SQL
    
    
    UpdSLXProd = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Function
Error:
    MsgBox sMyName & ".UpdSLXProd()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "MAS 500"
    
    Err.Clear
    GoTo CleanUP

End Function

'This process is to link missing links between SLX and SAGE Customers for the current Company.
Public Sub LinkAllQuotes(oClass As Object, Optional bUpdForm As Boolean = False)
    On Error GoTo Error
    
    Dim icnt As Long
    Dim iCurrRec As Long
    
    Dim lSOKey As Long
    Dim lProjectDesc As String
    
    Dim lSOLineDistKey As Long
    Dim lSOLineKey As Long
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim lQtyOrd As Double
    Dim sTranID As String

    Dim RS      As New DASRecordSet       'SOTADAS recordset Object
    Dim rsLines      As New DASRecordSet       'SOTADAS recordset Object
    Dim sSQL    As String       'SQL String
    
    'Get Customer not linked to SLX
    sSQL = "select Distinct q.SOKey, q.TranID, q.TranNo, q.TranType, q.ProjectDesc, q.ProjectNumber "
    sSQL = sSQL & "From vluQuotes_SGS q with(ReadUncommitted) "
    sSQL = sSQL & "Left Outer Join vluSLXOpportunityAndQuotes_SGS slx with(ReadUncommitted) "

'    sSQL = sSQL & "    on q.SOKey = slx.SOKey "
    sSQL = sSQL & "    on q.BidNo = slx.BidNumber "
    sSQL = sSQL & "    and q.CompanyID = slx.CompanyID "
    
    sSQL = sSQL & "Where q.AccountID Is Not Null "
    sSQL = sSQL & "And slx.SOKey is Null "
    sSQL = sSQL & "and q.CompanyID = " & gsQuoted(msCompanyID)
    
    'Run query to get Missing Quote Links
    Set RS = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
       
    DoEvents
    If msBStopProcess = True Then Exit Sub
    
    If RS.RecordCount > 0 Then
        icnt = 0
        While Not RS.IsEOF
            icnt = icnt + 1
            DoEvents
            RS.MoveNext
        Wend
        
        DoEvents
        
        RS.MoveFirst
    
        If MsgBox("There are " & icnt & " Sales Order Quote records that are about to be linked between SAGE and SalesLogix.  Do you want to continue?", vbYesNo, "Confirm Link Action") <> vbYes Then
            Exit Sub
        End If
    Else
        MsgBox "There are no Sales Order Quote records to link.", vbExclamation, "SAGE 500"
        Exit Sub
    End If
    
    DoEvents
    If msBStopProcess = True Then Exit Sub
    
    iCurrRec = 0
    
    'There is customer class information
    While Not RS.IsEOF
        DoEvents
        If msBStopProcess = True Then Exit Sub
        
        lSOKey = glGetValidLong(RS.Field("SOKey"))
        lProjectDesc = gsGetValidStr(RS.Field("ProjectDesc"))
        sTranID = gsGetValidStr(RS.Field("TranID"))
        
        DoEvents
        iCurrRec = iCurrRec + 1
        
        If bUpdForm = True Then
            DoEvents
            frmSendSLX.txtCustLinkStatus.Text = CStr(iCurrRec) & " of " & CStr(icnt)
            frmSendSLX.txtCustCurrRec.Text = "TranID: " & sTranID & " || Proj Desc: " & lProjectDesc
            DoEvents
        End If
        
        DoEvents
        
        'Create Link for the Header
        ProcessSLXOpporData lSOKey, oClass, lProjectDesc
        
        DoEvents
        
        'Now Create Link for the Lines
        sSQL = "Select so.SOKey, sl.SOLineKey, sld.SOLineDistKey, sld.WhseKey, sl.ItemKey, sld.QtyOrd "
        sSQL = sSQL & "From tsoSalesOrder so with(ReadUncommitted) "
        sSQL = sSQL & "Inner Join tsoSOLine sl with(ReadUncommitted) "
        sSQL = sSQL & "    on so.sokey = sl.sokey "
        sSQL = sSQL & "inner join tsosolinedist sld with(ReadUncommitted) "
        sSQL = sSQL & "    on sl.solinekey = sld.solinekey "
        sSQL = sSQL & "Where so.SOkey = " & lSOKey
        
        'Run query to get Quote Lines
        Set rsLines = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
        DoEvents
        
        If rsLines.RecordCount > 0 Then
            rsLines.MoveFirst
            While Not rsLines.IsEOF
                DoEvents
                
                lSOLineDistKey = glGetValidLong(rsLines.Field("SOLineDistKey"))
                lSOLineKey = glGetValidLong(rsLines.Field("SOLineKey"))
                lWhseKey = glGetValidLong(rsLines.Field("WhseKey"))
                lQtyOrd = gdGetValidDbl(rsLines.Field("QtyOrd"))
                lItemKey = glGetValidLong(rsLines.Field("ItemKey"))
                
                Call ProcessSLXOpporLineData(lSOKey, lSOLineKey, lSOLineDistKey, lItemKey, lWhseKey, lQtyOrd, oClass)
                
                DoEvents
                
                rsLines.MoveNext
            Wend
            rsLines.Close
            
        End If
        
        DoEvents
        RS.MoveNext
    Wend
    
    RS.Close

    MsgBox "The Sales Order Quote link process is complete.", vbExclamation, "SAGE 500"
    
'MsgBox "Debug Message: " & strDebug, vbInformation, "Debug Message"
    Exit Sub
Error:
    MsgBox sMyName & ".LinkAllQuotes()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear
End Sub

'This process is to link missing links between SLX and SAGE Customers for the current Company.
Public Sub LinkAllContracts(oClass As Object, Optional bUpdForm As Boolean = False)
    On Error GoTo Error
    
    Dim icnt As Long
    Dim iCurrRec As Long
    
    Dim lSOKey As Long
    Dim lProjectDesc As String
    
    Dim lSOLineDistKey As Long
    Dim lSOLineKey As Long
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim lQtyOrd As Double
    Dim sTranID As String

    Dim RS      As New DASRecordSet       'SOTADAS recordset Object
    Dim rsLines As New DASRecordSet       'SOTADAS recordset Object
    Dim sSQL    As String       'SQL String
    
    'Get Customer not linked to SLX
    sSQL = "select Distinct q.SOKey, q.TranID, q.TranNo, q.TranType, q.ProjectDesc, q.ProjectNumber "
    sSQL = sSQL & "From vluContracts_SGS q with(ReadUncommitted) "
    sSQL = sSQL & "Left Outer Join vluSLXOpportunityAndContracts_SGS slx with(ReadUncommitted) "
    sSQL = sSQL & "    on q.SOKey = slx.SOKey "
    sSQL = sSQL & "Where q.AccountID Is Not Null "
    sSQL = sSQL & "And slx.SOKey is Null "
    sSQL = sSQL & "And q.CompanyID = " & gsQuoted(msCompanyID)
    
    'Run query to get Missing Quote Links
    Set RS = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
       
    
    If RS.RecordCount > 0 Then
        icnt = 0
        While Not RS.IsEOF
            icnt = icnt + 1
            DoEvents
            RS.MoveNext
        Wend
        
        RS.MoveFirst
    
        If MsgBox("There are " & icnt & " Sales Order Contracts(BSO) records that are about to be linked between SAGE and SalesLogix.  Do you want to continue?", vbYesNo, "Confirm Link Action") <> vbYes Then
            Exit Sub
        End If
    Else
        MsgBox "There are no Sales Order Contracts(BSO) records to link.", vbExclamation, "SAGE 500"
        Exit Sub
    End If
    
    DoEvents
    
    If msBStopProcess = True Then Exit Sub
                
    iCurrRec = 0
    
    'There is customer class information
    While Not RS.IsEOF
        DoEvents
        If msBStopProcess = True Then Exit Sub
    
        lSOKey = glGetValidLong(RS.Field("SOKey"))
        lProjectDesc = gsGetValidStr(RS.Field("ProjectDesc"))
        sTranID = gsGetValidStr(RS.Field("TranID"))
        
        DoEvents
        iCurrRec = iCurrRec + 1
        
        If bUpdForm = True Then
            DoEvents
            frmSendSLX.txtCustLinkStatus.Text = CStr(iCurrRec) & " of " & CStr(icnt)
            frmSendSLX.txtCustCurrRec.Text = "TranID: " & sTranID & " || Proj Desc: " & lProjectDesc
            DoEvents
        End If
        
        'Create Link for the Header
        ProcessSLXOpporData lSOKey, oClass, lProjectDesc
        
        DoEvents
        
        'Now Create Link for the Lines
        sSQL = "Select so.SOKey, sl.SOLineKey, sld.SOLineDistKey, sld.WhseKey, sl.ItemKey, sld.QtyOrd "
        sSQL = sSQL & "From tsoSalesOrder so with(ReadUncommitted) "
        sSQL = sSQL & "Inner Join tsoSOLine sl with(ReadUncommitted) "
        sSQL = sSQL & "    on so.sokey = sl.sokey "
        sSQL = sSQL & "inner join tsosolinedist sld with(ReadUncommitted) "
        sSQL = sSQL & "    on sl.solinekey = sld.solinekey "
        sSQL = sSQL & "Where so.SOkey = " & lSOKey
        
        'Run query to get Quote Lines
        Set rsLines = oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            
        If rsLines.RecordCount > 0 Then
            rsLines.MoveFirst
            While Not rsLines.IsEOF
                                
                DoEvents
                
                lSOLineDistKey = glGetValidLong(rsLines.Field("SOLineDistKey"))
                lSOLineKey = glGetValidLong(rsLines.Field("SOLineKey"))
                lWhseKey = glGetValidLong(rsLines.Field("WhseKey"))
                lQtyOrd = gdGetValidDbl(rsLines.Field("QtyOrd"))
                lItemKey = glGetValidLong(rsLines.Field("ItemKey"))
                
                Call ProcessSLXOpporLineData(lSOKey, lSOLineKey, lSOLineDistKey, lItemKey, lWhseKey, lQtyOrd, oClass)
                
                DoEvents
                
                rsLines.MoveNext
            Wend
            rsLines.Close
            
        End If
        
        DoEvents
        
        RS.MoveNext
    Wend
    
    RS.Close

    MsgBox "The Sales Order Contracts(BSO) link process is complete.", vbExclamation, "SAGE 500"
    
'MsgBox "Debug Message: " & strDebug, vbInformation, "Debug Message"
    Exit Sub
Error:
    MsgBox sMyName & ".LinkAllContracts()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "SAGE 500"
    
    Err.Clear
End Sub

Function IsValidSLXConn(oClass As Object, Optional ErrMsg As String = Empty) As Boolean
On Error GoTo Error
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    Conn.Close
    
    IsValidSLXConn = True

    Exit Function
Error:
    ErrMsg = "Error: " & Err.Number & " " & Err.Description
    
    IsValidSLXConn = False
    
    Err.Clear

End Function

'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************
'RKL DEJ 2016-12-14 - Insert/Update SLX Opportunity and SLX Account/Customer   (STOP)
'***************************************************************************************************************************
'***************************************************************************************************************************
'***************************************************************************************************************************


Function GetSLXAccounts(oClass As Object, SAGECust As clsSAGECustomer, Optional bAutoLink As Boolean = False, Optional NbrAutoMatches As Integer = 0) As clsSLXCustomers
'Function GetSLXAccounts(oClass As Object, SAGECustName As String, Optional bAutoLink As Boolean = False) As clsSLXCustomers
    On Error GoTo Error
    
    Dim SLXCust As New clsSLXCustomer
        
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    Dim bAutoSelect As Boolean
    Dim SLXCusts As New clsSLXCustomers
    
    NbrAutoMatches = 0
    
    ConnStr = GetSLXConnStr(oClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    'Get Possible Matching Accounts from SLX
    SQL = "select a.AccountID, a.Account, a.Account_UC, a.Type, a.Status, a.Region, a.ExternalAccountNo, a.CreateDate, "
    SQL = SQL & "aa.AddressID , aa.Description, aa.IsPrimary 'IsPrimaryAddress', aa.Address1, aa.Address2, aa.Address3, aa.City, aa.State, aa.PostalCode, aa.Country "
    SQL = SQL & "From sysdba.Account a "
    SQL = SQL & "Left Outer Join sysdba.Address aa "
    SQL = SQL & "     On a.AccountID = aa.EntityID "
    SQL = SQL & "    and aa.IsPrimary = 'Y' "
    SQL = SQL & "Where 1 = 1 "
    SQL = SQL & "and Ltrim(Rtrim(Coalesce(a.Account,''))) <> '' "   'Don 't lookup Account that are missing the name
    SQL = SQL & "and a.Account = " & gsQuoted(SAGECust.CustName)
    SQL = SQL & "Order by a.Account "
    
    If RS.State = 1 Then RS.Close
    Set RS = Conn.Execute(SQL)
    
    If RS.RecordCount > 0 Then
        
        RS.MoveFirst
        
        While Not RS.EOF And Not RS.BOF
            SLXCust.AccountID = gsGetValidStr(RS("AccountID").Value)
            SLXCust.Account = gsGetValidStr(RS("Account").Value)
            SLXCust.Account_UC = gsGetValidStr(RS("Account_UC").Value)
            SLXCust.AccountType = gsGetValidStr(RS("Type").Value)
            SLXCust.Status = gsGetValidStr(RS("Status").Value)
            SLXCust.Region = gsGetValidStr(RS("Region").Value)
            SLXCust.ExternalAccountNo = gsGetValidStr(RS("ExternalAccountNo").Value)
            SLXCust.CreateDate = gsGetValidStr(RS("CreateDate").Value)
            
            SLXCust.AddressID = gsGetValidStr(RS("AddressID").Value)
            SLXCust.Description = gsGetValidStr(RS("Description").Value)
            SLXCust.IsPrimaryAddress = gsGetValidStr(RS("IsPrimaryAddress").Value)
            SLXCust.Address1 = gsGetValidStr(RS("Address1").Value)
            SLXCust.Address2 = gsGetValidStr(RS("Address2").Value)
            SLXCust.Address3 = gsGetValidStr(RS("Address3").Value)
            SLXCust.City = gsGetValidStr(RS("City").Value)
            SLXCust.State = gsGetValidStr(RS("State").Value)
            SLXCust.PostalCode = gsGetValidStr(RS("PostalCode").Value)
            SLXCust.Country = gsGetValidStr(RS("Country").Value)
            
            If Trim(UCase(SLXCust.AccountType)) = "CUSTOMER" And Trim(UCase(SLXCust.Address1)) = Trim(UCase(SAGECust.AddrLine1)) Then
'            If Trim(UCase(SLXCust.AccountType)) = "CUSTOMER" Then
                SLXCust.Link = True
                bAutoSelect = True
                NbrAutoMatches = NbrAutoMatches + 1
            End If
            
            SLXCusts.AddExisting SLXCust, SLXCusts, SLXCust.AccountID & SLXCust.AddressID
            
            RS.MoveNext
        Wend
    End If
    
    If bAutoLink = True And bAutoSelect = True And SLXCusts.Count > 0 Then
        'There is one or more records that match...
        'Remove records not set to link
        
        For Each SLXCust In SLXCusts
            If SLXCust.Link = False Then
                SLXCusts.Remove SLXCust.AccountID & SLXCust.AddressID
            End If
        Next
    End If
        
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Set GetSLXAccounts = SLXCusts
    
    Exit Function
Error:
    MsgBox "basSendSLX.GetSLXAccounts()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation
    
    Err.Clear
    
    GoTo CleanUP

End Function



