VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSLXCustomers"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Description = "Collection of clsSLXCusomter objects"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Collection" ,"clsSLXCustomer"
Attribute VB_Ext_KEY = "Member0" ,"clsSLXCustomer"
'local variable to hold collection
Private mCol As Collection

Public Function Add(Country As String, PostalCode As String, State As String, City As String, Address3 As String, Address2 As String, Address1 As String, IsPrimaryAddress As String, Description As String, AddressID As String, CreateDate As String, ExternalAccountNo As String, Region As String, Status As String, AccountType As String, Account_UC As String, Account As String, AccountID As String, Link As String, clsSLXCustomers As clsSLXCustomers, Optional sKey As String) As clsSLXCustomer
    'create a new object
    Dim objNewMember As clsSLXCustomer
    Set objNewMember = New clsSLXCustomer


    'set the properties passed into the method
    objNewMember.Country = Country
    objNewMember.PostalCode = PostalCode
    objNewMember.State = State
    objNewMember.City = City
    objNewMember.Address3 = Address3
    objNewMember.Address2 = Address2
    objNewMember.Address1 = Address1
    objNewMember.IsPrimaryAddress = IsPrimaryAddress
    objNewMember.Description = Description
    objNewMember.AddressID = AddressID
    objNewMember.CreateDate = CreateDate
    objNewMember.ExternalAccountNo = ExternalAccountNo
    objNewMember.Region = Region
    objNewMember.Status = Status
    objNewMember.AccountType = AccountType
    objNewMember.Account_UC = Account_UC
    objNewMember.Account = Account
    objNewMember.AccountID = AccountID
    objNewMember.Link = Link
    Set objNewMember.clsSLXCustomers = clsSLXCustomers
    If Len(sKey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, sKey
    End If


    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


End Function

Public Function AddExisting(ExistignSLXCust As clsSLXCustomer, clsSLXCustomers As clsSLXCustomers, Optional sKey As String) As clsSLXCustomer
    'create a new object
    Set ExistignSLXCust.clsSLXCustomers = clsSLXCustomers
    If Len(sKey) = 0 Then
        mCol.Add ExistignSLXCust
    Else
        mCol.Add ExistignSLXCust, sKey
    End If


    'return the object created
    Set AddExisting = ExistignSLXCust


End Function


Public Property Get Item(vntIndexKey As Variant) As clsSLXCustomer
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property



Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)


    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

