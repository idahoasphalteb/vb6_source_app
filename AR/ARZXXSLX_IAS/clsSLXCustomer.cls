VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSLXCustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"clsSLXCustomers"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private msLink As Boolean
Private msAccountID As String
Private msAccount As String
Private msAccount_UC As String
Private msType As String
Private msStatus As String
Private msRegion As String
Private msExternalAccountNo As String
Private msCreateDate As String
Private msAddressID As String
Private msDescription As String
Private msIsPrimaryAddress As String
Private msAddress1 As String
Private msAddress2 As String
Private msAddress3 As String
Private msCity As String
Private msState As String
Private msPostalCode As String
Private msCountry As String
Private mvarclsSLXCustomers As clsSLXCustomers


Public Property Get clsSLXCustomers() As clsSLXCustomers
    If mvarclsSLXCustomers Is Nothing Then
        Set mvarclsSLXCustomers = New clsSLXCustomers
    End If


    Set clsSLXCustomers = mvarclsSLXCustomers
End Property


Public Property Set clsSLXCustomers(vData As clsSLXCustomers)
    Set mvarclsSLXCustomers = vData
End Property
Private Sub Class_Terminate()
    Set mvarclsSLXCustomers = Nothing
End Sub





Public Property Let Link(StrVal As Boolean)
    msLink = StrVal
End Property

Public Property Get Link() As Boolean
    Link = msLink
End Property

Public Property Let AccountID(StrVal As String)
    msAccountID = StrVal
End Property

Public Property Get AccountID() As String
    AccountID = msAccountID
End Property

Public Property Let Account(StrVal As String)
    msAccount = StrVal
End Property

Public Property Get Account() As String
    Account = msAccount
End Property

Public Property Let Account_UC(StrVal As String)
    msAccount_UC = StrVal
End Property

Public Property Get Account_UC() As String
    Account_UC = msAccount_UC
End Property

Public Property Let AccountType(StrVal As String)
    msType = StrVal
End Property

Public Property Get AccountType() As String
    AccountType = msType
End Property

Public Property Let Status(StrVal As String)
    msStatus = StrVal
End Property

Public Property Get Status() As String
    Status = msStatus
End Property

Public Property Let Region(StrVal As String)
    msRegion = StrVal
End Property

Public Property Get Region() As String
    Region = msRegion
End Property

Public Property Let ExternalAccountNo(StrVal As String)
    msExternalAccountNo = StrVal
End Property

Public Property Get ExternalAccountNo() As String
    ExternalAccountNo = msExternalAccountNo
End Property

Public Property Let CreateDate(StrVal As String)
    msCreateDate = StrVal
End Property

Public Property Get CreateDate() As String
    CreateDate = msCreateDate
End Property

Public Property Let AddressID(StrVal As String)
    msAddressID = StrVal
End Property

Public Property Get AddressID() As String
    AddressID = msAddressID
End Property

Public Property Let Description(StrVal As String)
    msDescription = StrVal
End Property

Public Property Get Description() As String
    Description = msDescription
End Property

Public Property Let IsPrimaryAddress(StrVal As String)
    msIsPrimaryAddress = StrVal
End Property

Public Property Get IsPrimaryAddress() As String
    IsPrimaryAddress = msIsPrimaryAddress
End Property

Public Property Let Address1(StrVal As String)
    msAddress1 = StrVal
End Property

Public Property Get Address1() As String
    Address1 = msAddress1
End Property

Public Property Let Address2(StrVal As String)
    msAddress2 = StrVal
End Property

Public Property Get Address2() As String
    Address2 = msAddress2
End Property

Public Property Let Address3(StrVal As String)
    msAddress3 = StrVal
End Property

Public Property Get Address3() As String
    Address3 = msAddress3
End Property

Public Property Let City(StrVal As String)
    msCity = StrVal
End Property

Public Property Get City() As String
    City = msCity
End Property

Public Property Let State(StrVal As String)
    msState = StrVal
End Property

Public Property Get State() As String
    State = msState
End Property

Public Property Let PostalCode(StrVal As String)
    msPostalCode = StrVal
End Property

Public Property Get PostalCode() As String
    PostalCode = msPostalCode
End Property

Public Property Let Country(StrVal As String)
    msCountry = StrVal
End Property

Public Property Get Country() As String
    Country = msCountry
End Property


