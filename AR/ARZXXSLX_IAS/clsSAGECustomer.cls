VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsSAGECustomer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private msCompanyID As String
Private msCustKey As Long
Private msCustID As String
Private msCustName As String

Private msAddrKey As Long
Private msAddrName As String
Private msAddrLine1 As String
Private msAddrLine2 As String
Private msAddrLine3 As String
Private msAddrLine4 As String
Private msAddrLine5 As String
Private msCity As String
Private msState As String
Private msZip As String
Private msCountry As String


Public Property Let CompanyID(StrVal As String)
    msCompanyID = StrVal
End Property

Public Property Get CompanyID() As String
    CompanyID = msCompanyID
End Property

Public Property Let CustKey(LongVal As Long)
    msCustKey = LongVal
End Property

Public Property Get CustKey() As Long
    CustKey = msCustKey
End Property

Public Property Let CustID(StrVal As String)
    msCustID = StrVal
End Property

Public Property Get CustID() As String
    CustID = msCustID
End Property

Public Property Let CustName(StrVal As String)
    msCustName = StrVal
End Property

Public Property Get CustName() As String
    CustName = msCustName
End Property

Public Property Let AddrKey(LongVal As Long)
    msAddrKey = LongVal
End Property

Public Property Get AddrKey() As Long
    AddrKey = msAddrKey
End Property

Public Property Let AddrName(StrVal As String)
    msAddrName = StrVal
End Property

Public Property Get AddrName() As String
    AddrName = msAddrName
End Property

Public Property Let AddrLine1(StrVal As String)
    msAddrLine1 = StrVal
End Property

Public Property Get AddrLine1() As String
    AddrLine1 = msAddrLine1
End Property

Public Property Let AddrLine2(StrVal As String)
    msAddrLine2 = StrVal
End Property

Public Property Get AddrLine2() As String
    AddrLine2 = msAddrLine2
End Property

Public Property Let AddrLine3(StrVal As String)
    msAddrLine3 = StrVal
End Property

Public Property Get AddrLine3() As String
    AddrLine3 = msAddrLine3
End Property

Public Property Let AddrLine4(StrVal As String)
    msAddrLine4 = StrVal
End Property

Public Property Get AddrLine4() As String
    AddrLine4 = msAddrLine4
End Property

Public Property Let AddrLine5(StrVal As String)
    msAddrLine5 = StrVal
End Property

Public Property Get AddrLine5() As String
    AddrLine5 = msAddrLine5
End Property

Public Property Let City(StrVal As String)
    msCity = StrVal
End Property

Public Property Get City() As String
    City = msCity
End Property

Public Property Let State(StrVal As String)
    msState = StrVal
End Property

Public Property Get State() As String
    State = msState
End Property

Public Property Let Zip(StrVal As String)
    msZip = StrVal
End Property

Public Property Get Zip() As String
    Zip = msZip
End Property

Public Property Let Country(StrVal As String)
    msCountry = StrVal
End Property

Public Property Get Country() As String
    Country = msCountry
End Property




