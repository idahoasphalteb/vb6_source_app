VERSION 5.00
Begin VB.Form frmProcessLog 
   Caption         =   "Process Log"
   ClientHeight    =   4920
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7140
   ControlBox      =   0   'False
   Icon            =   "frmProcessLog.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4920
   ScaleWidth      =   7140
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdOk 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   495
      Left            =   2880
      TabIndex        =   1
      Top             =   4320
      Width           =   1215
   End
   Begin VB.TextBox txtLog 
      Height          =   4095
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      Top             =   120
      Width           =   6855
   End
End
Attribute VB_Name = "frmProcessLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
    txtLog.Text = Empty
    strProcessLog = Empty
    Unload Me
End Sub

Private Sub Form_Load()
    txtLog.Text = strProcessLog
End Sub
