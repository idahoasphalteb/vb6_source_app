VERSION 5.00
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Begin VB.Form FrmMatch 
   Caption         =   "Match SAGE Customer to SLX Customer"
   ClientHeight    =   8340
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10380
   Icon            =   "FrmMatch.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8340
   ScaleWidth      =   10380
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdSkip 
      Caption         =   "Skip This Record"
      Height          =   495
      Left            =   7800
      TabIndex        =   27
      Top             =   1440
      Width           =   2055
   End
   Begin VB.CommandButton cmdCancelLink 
      Caption         =   "Stop Link Process"
      Height          =   495
      Left            =   7800
      TabIndex        =   26
      Top             =   840
      Width           =   2055
   End
   Begin FPSpreadADO.fpSpread grdClient 
      Height          =   4545
      Left            =   120
      TabIndex        =   24
      Top             =   3720
      Width           =   10065
      _Version        =   524288
      _ExtentX        =   17754
      _ExtentY        =   8017
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "FrmMatch.frx":23D2
   End
   Begin VB.CommandButton cmdMerge 
      Caption         =   "Link the Selected Record(s)"
      Default         =   -1  'True
      Height          =   375
      Left            =   7800
      TabIndex        =   23
      Top             =   3120
      Width           =   2295
   End
   Begin VB.CommandButton cmdCreate 
      Caption         =   "Create New SLX Record"
      Height          =   375
      Left            =   7800
      TabIndex        =   22
      Top             =   2520
      Width           =   2295
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCustID 
      Height          =   300
      Left            =   1800
      TabIndex        =   1
      Top             =   120
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCustName 
      Height          =   300
      Left            =   1800
      TabIndex        =   2
      Top             =   480
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtAddrLine1 
      Height          =   300
      Left            =   1800
      TabIndex        =   4
      Top             =   1200
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtAddrLine2 
      Height          =   300
      Left            =   1800
      TabIndex        =   6
      Top             =   1560
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtAddrLine3 
      Height          =   300
      Left            =   1800
      TabIndex        =   8
      Top             =   1920
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtAddrLine4 
      Height          =   300
      Left            =   1800
      TabIndex        =   10
      Top             =   2280
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCity 
      Height          =   300
      Left            =   1800
      TabIndex        =   12
      Top             =   2640
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtState 
      Height          =   300
      Left            =   1800
      TabIndex        =   14
      Top             =   3000
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtZip 
      Height          =   300
      Left            =   3600
      TabIndex        =   16
      Top             =   3000
      Width           =   1575
      _Version        =   65536
      _ExtentX        =   2778
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtAddrName 
      Height          =   300
      Left            =   1800
      TabIndex        =   18
      Top             =   840
      Width           =   5535
      _Version        =   65536
      _ExtentX        =   9763
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtCountry 
      Height          =   300
      Left            =   6480
      TabIndex        =   20
      Top             =   3000
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   529
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      bLocked         =   -1  'True
   End
   Begin VB.Label Label11 
      Caption         =   "SalesLogix Account(s):"
      Height          =   255
      Left            =   120
      TabIndex        =   25
      Top             =   3480
      Width           =   3135
   End
   Begin VB.Label Label10 
      Caption         =   "Country"
      Height          =   255
      Left            =   5880
      TabIndex        =   21
      Top             =   3000
      Width           =   615
   End
   Begin VB.Label Label9 
      Caption         =   "Address Name"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   960
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Zip"
      Height          =   375
      Left            =   3240
      TabIndex        =   17
      Top             =   3000
      Width           =   255
   End
   Begin VB.Label Label7 
      Caption         =   "State"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "City"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   2640
      Width           =   1575
   End
   Begin VB.Label Label5 
      Caption         =   "Addr Line 4"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Addr Line 3"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1920
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "Addr Line 2"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "Addr Line 1"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Cust Name:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   1575
   End
   Begin VB.Label lclCustID 
      Caption         =   "Cust ID:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1575
   End
End
Attribute VB_Name = "FrmMatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public MSoClass As Object
Private MSSAGECust As clsSAGECustomer
Private msSLXCusts As clsSLXCustomers

Private msbCancel As Boolean

Const CnstLink = 1
Const CnstAccountID = 2
Const CnstAccount = 3
Const CnstAccount_UC = 4
Const CnstType = 5
Const CnstStatus = 6
Const CnstRegion = 7
Const CnstExternalAccountNo = 8
Const CnstCreateDate = 9
Const CnstAddressID = 10
Const CnstDescription = 11
Const CnstIsPrimaryAddress = 12
Const CnstAddress1 = 13
Const CnstAddress2 = 14
Const CnstAddress3 = 15
Const CnstCity = 16
Const CnstState = 17
Const CnstPostalCode = 18
Const CnstCountry = 19

Const CnstMaxCols = 19


Const MinFrmHeight = 8945
Const MinFrmWidth = 10500
Const MinTabHeight = 6495
Const MinTabWidth = 10335
Const MinGridClientHeight = 4545
Const MinGridClientWidth = 10065
Const MinGridSvrHeight = 4545
Const MinGridSvrWidth = 10065
Const MincmdExitTop = 6840
Const MincmdSelectTop = 5520
Const MincmdOtherTop = 5400


Public Property Set oClass(oClassVal As Object)
    Set MSoClass = oClassVal
End Property

Public Property Set SAGECust(SAGECustVal As clsSAGECustomer)
    Set MSSAGECust = SAGECustVal
End Property

Public Property Set SLXCusts(SLXCustomers As clsSLXCustomers)
    Set msSLXCusts = SLXCustomers
End Property

Public Property Get SLXCusts() As clsSLXCustomers
    Set SLXCusts = msSLXCusts
End Property

Public Property Let bCancel(InputVal As Boolean)
    msbCancel = InputVal
End Property

Public Property Get bCancel() As Boolean
    bCancel = msbCancel
End Property

Private Sub cmdCancelLink_Click()
    msBStopProcess = True
    msbCancel = True
    Unload Me
    
End Sub

Private Sub cmdCreate_Click()
    Set msSLXCusts = New clsSLXCustomers
    
    Unload Me
    
End Sub

Private Sub cmdMerge_Click()
    Call GetSelectedAccounts
    
    If msSLXCusts.Count <= 0 Then
        MsgBox "You must select one or more records to link first.", vbInformation, "SAGE 500"
        Exit Sub
    End If
    
    Unload Me
End Sub

Private Sub cmdSkip_Click()
    msbCancel = True
    Unload Me
End Sub

Private Sub Form_Load()
    msbCancel = False
    
    If Not MSSAGECust Is Nothing Then
        txtCustID.Text = MSSAGECust.CustID
        txtCustName.Text = MSSAGECust.CustName
        txtAddrName.Text = MSSAGECust.AddrName
        txtAddrLine1.Text = MSSAGECust.AddrLine1
        txtAddrLine2.Text = MSSAGECust.AddrLine2
        txtAddrLine3.Text = MSSAGECust.AddrLine3
        txtAddrLine4.Text = MSSAGECust.AddrLine4
        txtCity.Text = MSSAGECust.City
        txtState.Text = MSSAGECust.State
        txtCountry.Text = MSSAGECust.Country
    End If
    
    Call SetupGrid
    
    'Call GetSLXAccounts(MSSAGECust.CustName)
    
    Call GetSLXByCls
    
End Sub




Sub SetupGrid()
    On Error GoTo Error
    
    grdClient.MaxCols = CnstMaxCols
    grdClient.MaxRows = 0
    
    
    grdClient.ColHeadersAutoText = DispBlank
    grdClient.ColHeaderRows = 1
    
    grdClient.RowHeadersAutoText = DispBlank
    grdClient.Row = 0
    grdClient.RowHeaderCols = 0
    grdClient.RowHidden = False
    
    grdClient.Col = CnstLink
    grdClient.Text = "Create Link"
    grdClient.ColWidth(CnstLink) = 10
    
    
    'Account Information
    grdClient.Col = CnstAccountID
    grdClient.Text = "AccountID"
    grdClient.ColWidth(CnstAccountID) = 20
    grdClient.ColHidden = True
    
    grdClient.Col = CnstAccount
    grdClient.Text = "Account"
    grdClient.ColWidth(CnstAccount) = 20
    
    grdClient.Col = CnstAccount_UC
    grdClient.Text = "Account_UC"
    grdClient.ColWidth(CnstAccount_UC) = 20
    grdClient.ColHidden = True
    
    grdClient.Col = CnstType
    grdClient.Text = "Type"
    grdClient.ColWidth(CnstType) = 16
    
    grdClient.Col = CnstStatus
    grdClient.Text = "Status"
    grdClient.ColWidth(CnstStatus) = 16
    
    grdClient.Col = CnstRegion
    grdClient.Text = "Region"
    grdClient.ColWidth(CnstRegion) = 16
    
    grdClient.Col = CnstExternalAccountNo
    grdClient.Text = "ExternalAccountNo"
    grdClient.ColWidth(CnstExternalAccountNo) = 16
    grdClient.ColHidden = True
    
    grdClient.Col = CnstCreateDate
    grdClient.Text = "CreateDate"
    grdClient.ColWidth(CnstCreateDate) = 32
    
    
    'Address Information
    grdClient.Col = CnstAddressID
    grdClient.Text = "AddressID"
    grdClient.ColWidth(CnstAddressID) = 32
    grdClient.ColHidden = True
    
    grdClient.Col = CnstDescription
    grdClient.Text = "Description"
    grdClient.ColWidth(CnstDescription) = 16
    
    grdClient.Col = CnstIsPrimaryAddress
    grdClient.Text = "IsPrimaryAddress"
    grdClient.ColWidth(CnstIsPrimaryAddress) = 16
    
    grdClient.Col = CnstAddress1
    grdClient.Text = "Address1"
    grdClient.ColWidth(CnstAddress1) = 16
    
    grdClient.Col = CnstAddress2
    grdClient.Text = "Address2"
    grdClient.ColWidth(CnstAddress2) = 16
    
    grdClient.Col = CnstAddress3
    grdClient.Text = "Address3"
    grdClient.ColWidth(CnstAddress3) = 16
    
    grdClient.Col = CnstCity
    grdClient.Text = "City"
    grdClient.ColWidth(CnstCity) = 16
    
    grdClient.Col = CnstState
    grdClient.Text = "State"
    grdClient.ColWidth(CnstState) = 16
    
    grdClient.Col = CnstPostalCode
    grdClient.Text = "PostalCode"
    grdClient.ColWidth(CnstPostalCode) = 16
    
    grdClient.Col = CnstCountry
    grdClient.Text = "Country"
    grdClient.ColWidth(CnstCountry) = 16
    
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SetupGrid()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub

Sub AddSLXRow(SLXCust As clsSLXCustomer, Optional bMakeRed As Boolean = False)
    On Error GoTo Error
    
    Dim lRow As Long
    Dim vForColor As Variant
    
    lRow = grdClient.MaxRows + 1
    
    grdClient.MaxRows = lRow
    
    grdClient.Row = lRow
    
    grdClient.Col = CnstLink
    grdClient.CellType = CellTypeCheckBox
    grdClient.TypeCheckCenter = True
    grdClient.TypeCheckType = TypeCheckTypeNormal
    
    If SLXCust.Link = True Then
        grdClient.Value = vbChecked
    Else
        grdClient.Value = vbUnchecked
    End If
    
    If bMakeRed = False Then
        vForColor = grdClient.ForeColor
    Else
        vForColor = vbRed
    End If
    
    grdClient.Col = CnstAccountID
    grdClient.Text = SLXCust.AccountID
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstAccount
    grdClient.Text = SLXCust.Account
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstAccount_UC
    grdClient.Text = SLXCust.Account_UC
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstType
    grdClient.Text = SLXCust.AccountType
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstStatus
    grdClient.Text = SLXCust.Status
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstRegion
    grdClient.Text = SLXCust.Region
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstExternalAccountNo
    grdClient.Text = SLXCust.ExternalAccountNo
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstCreateDate
    grdClient.Text = SLXCust.CreateDate
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstAddressID
    grdClient.Text = SLXCust.AddressID
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstDescription
    grdClient.Text = SLXCust.Description
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstIsPrimaryAddress
    grdClient.Text = SLXCust.IsPrimaryAddress
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstAddress1
    grdClient.Text = SLXCust.Address1
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstAddress2
    grdClient.Text = SLXCust.Address2
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstAddress3
    grdClient.Text = SLXCust.Address3
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstCity
    grdClient.Text = SLXCust.City
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstState
    grdClient.Text = SLXCust.State
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = CnstPostalCode
    grdClient.Text = SLXCust.PostalCode
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = 1
    grdClient.Col = CnstCountry
    grdClient.Text = SLXCust.Country
    grdClient.Lock = True
    grdClient.ForeColor = vForColor
    
    grdClient.Col = 1
    
    Exit Sub
Error:
    MsgBox Me.Name & ".AddSLXRow()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub


Sub GetSLXAccounts(SAGECustName As String)
    On Error GoTo Error
    
    Dim SLXCust As New clsSLXCustomer
        
    Dim ConnStr As String
    Dim Conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    
    ConnStr = GetSLXConnStr(MSoClass)
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactBrowse
    Conn.ConnectionString = ConnStr
    
    Conn.Open
    
    'Get Possible Matching Accounts from SLX
    SQL = "select a.AccountID, a.Account, a.Account_UC, a.Type, a.Status, a.Region, a.ExternalAccountNo, a.CreateDate, "
    SQL = SQL & "aa.AddressID , aa.Description, aa.IsPrimary 'IsPrimaryAddress', aa.Address1, aa.Address2, aa.Address3, aa.City, aa.State, aa.PostalCode, aa.Country "
    SQL = SQL & "From Account a "
    SQL = SQL & "Left Outer Join Address aa "
    SQL = SQL & "     On a.AccountID = aa.EntityID "
    SQL = SQL & "    and aa.IsPrimary = 'Y' "
    SQL = SQL & "Where 1 = 1 "
    SQL = SQL & "and Ltrim(Rtrim(Coalesce(a.Account,''))) <> '' "   'Don 't lookup Account that are missing the name
    SQL = SQL & "and a.Account = " & gsQuoted(SAGECustName)
    SQL = SQL & "Order by a.Account "
    
    If RS.State = 1 Then RS.Close
    Set RS = Conn.Execute(SQL)
    
    If RS.RecordCount > 0 Then
        
        RS.MoveFirst
        
        While Not RS.EOF And Not RS.BOF
            SLXCust.AccountID = gsGetValidStr(RS("AccountID").Value)
            SLXCust.Account = gsGetValidStr(RS("Account").Value)
            SLXCust.Account_UC = gsGetValidStr(RS("Account_UC").Value)
            SLXCust.AccountType = gsGetValidStr(RS("Type").Value)
            SLXCust.Status = gsGetValidStr(RS("Status").Value)
            SLXCust.Region = gsGetValidStr(RS("Region").Value)
            SLXCust.ExternalAccountNo = gsGetValidStr(RS("ExternalAccountNo").Value)
            SLXCust.CreateDate = gsGetValidStr(RS("CreateDate").Value)
            
            SLXCust.AddressID = gsGetValidStr(RS("AddressID").Value)
            SLXCust.Description = gsGetValidStr(RS("Description").Value)
            SLXCust.IsPrimaryAddress = gsGetValidStr(RS("IsPrimaryAddress").Value)
            SLXCust.Address1 = gsGetValidStr(RS("Address1").Value)
            SLXCust.Address2 = gsGetValidStr(RS("Address2").Value)
            SLXCust.Address3 = gsGetValidStr(RS("Address3").Value)
            SLXCust.City = gsGetValidStr(RS("City").Value)
            SLXCust.State = gsGetValidStr(RS("State").Value)
            SLXCust.PostalCode = gsGetValidStr(RS("PostalCode").Value)
            SLXCust.Country = gsGetValidStr(RS("Country").Value)
            
            Call AddSLXRow(SLXCust, False)
            
            RS.MoveNext
        Wend
    End If
        
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If Conn.State = 1 Then Conn.Close
    Set Conn = Nothing
    
    Exit Sub
Error:
    MsgBox Me.Name & ".GetSLXAccounts()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation
    
    Err.Clear
    
    GoTo CleanUP

End Sub


Sub GetSLXByCls()
    On Error GoTo Error
    
    Dim SLXCust As New clsSLXCustomer
    Dim i As Integer
        
    For Each SLXCust In msSLXCusts
        Call AddSLXRow(SLXCust, False)
    Next
        
CleanUP:
    Err.Clear
    On Error Resume Next
    
    Exit Sub
Error:
    MsgBox Me.Name & ".GetSLXByCls()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation
    
    Err.Clear
    
    GoTo CleanUP

End Sub


Private Sub Form_Resize()
    On Error Resume Next
    
    Dim HeightChange As Long
    Dim WidthChange As Long
    
    If Me.Height > MinFrmHeight Then
        HeightChange = Me.Height - MinFrmHeight
        
        Me.grdClient.Height = MinGridClientHeight + HeightChange
        

    End If
    
    If Me.Width > MinFrmWidth Then
        WidthChange = Me.Width - MinFrmWidth
        
        Me.grdClient.Width = MinGridClientWidth + WidthChange
    End If
    
    Err.Clear

End Sub


Sub GetSelectedAccounts()
    
    Dim lRow As Long
    
    Dim SLXCust As New clsSLXCustomer
    
    Set msSLXCusts = New clsSLXCustomers
    
    For lRow = 1 To grdClient.MaxRows
        
        grdClient.Row = lRow
        
        grdClient.Col = CnstLink
        
        If grdClient.Value = vbChecked Then
            'This row has been selected for linking
            grdClient.Col = CnstAccountID
            SLXCust.AccountID = grdClient.Value
            
            grdClient.Col = CnstAccount
            SLXCust.Account = grdClient.Value
            
            grdClient.Col = CnstAccount_UC
            SLXCust.Account_UC = grdClient.Value
            
            grdClient.Col = CnstType
            SLXCust.AccountType = grdClient.Value
            
            grdClient.Col = CnstStatus
            SLXCust.Status = grdClient.Value
            
            grdClient.Col = CnstRegion
            SLXCust.Region = grdClient.Value
            
            grdClient.Col = CnstExternalAccountNo
            SLXCust.ExternalAccountNo = grdClient.Value
            
            grdClient.Col = CnstCreateDate
            SLXCust.CreateDate = grdClient.Value
            
            grdClient.Col = CnstAddressID
            SLXCust.AddressID = grdClient.Value
            
            grdClient.Col = CnstDescription
            SLXCust.Description = grdClient.Value
            
            grdClient.Col = CnstIsPrimaryAddress
            SLXCust.IsPrimaryAddress = grdClient.Value
            
            grdClient.Col = CnstAddress1
            SLXCust.Address1 = grdClient.Value
            
            grdClient.Col = CnstAddress2
            SLXCust.Address2 = grdClient.Value
            
            grdClient.Col = CnstAddress3
            SLXCust.Address3 = grdClient.Value
            
            grdClient.Col = CnstCity
            SLXCust.City = grdClient.Value
            
            grdClient.Col = CnstState
            SLXCust.State = grdClient.Value
            
            grdClient.Col = CnstPostalCode
            SLXCust.PostalCode = grdClient.Value
            
            grdClient.Col = CnstCountry
            SLXCust.Country = grdClient.Value
                        
            msSLXCusts.AddExisting SLXCust, msSLXCusts
            
        End If
        
    Next
    
End Sub
