VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmBatch 
   Caption         =   "Batch"
   ClientHeight    =   6090
   ClientLeft      =   795
   ClientTop       =   945
   ClientWidth     =   9090
   HelpContextID   =   51470
   Icon            =   "Arztl001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6090
   ScaleWidth      =   9090
   Begin VB.CommandButton cmdPrintChecks 
      Caption         =   "Print Refund &Checks"
      Height          =   510
      Left            =   6900
      TabIndex        =   57
      Top             =   3915
      WhatsThisHelpID =   50
      Width           =   2070
   End
   Begin VB.CommandButton cmdMerge 
      Caption         =   "&Merge Invoices"
      Height          =   510
      Left            =   6900
      TabIndex        =   26
      Top             =   2152
      Width           =   2070
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   44
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   52
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   53
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.TextBox txtCashAllowMCDep 
      Height          =   315
      Left            =   7620
      TabIndex        =   47
      Top             =   4560
      Visible         =   0   'False
      WhatsThisHelpID =   51524
      Width           =   165
   End
   Begin VB.TextBox txtCashCurrID 
      Height          =   315
      Left            =   7410
      TabIndex        =   46
      Top             =   4560
      Visible         =   0   'False
      WhatsThisHelpID =   51523
      Width           =   165
   End
   Begin VB.Frame fraTranGrid 
      Height          =   1950
      Left            =   3135
      TabIndex        =   22
      Top             =   3720
      Width           =   3615
      Begin FPSpreadADO.fpSpread grdBatchTran 
         Height          =   1695
         Left            =   60
         TabIndex        =   23
         Top             =   195
         WhatsThisHelpID =   60
         Width           =   3480
         _Version        =   524288
         _ExtentX        =   6138
         _ExtentY        =   2990
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   3
         MaxRows         =   0
         SpreadDesigner  =   "Arztl001.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.TextBox txtBatchNo 
      Height          =   285
      Left            =   660
      MaxLength       =   7
      TabIndex        =   0
      Top             =   540
      WhatsThisHelpID =   49
      Width           =   1215
   End
   Begin VB.CommandButton cmdBatchRgstrPost 
      Caption         =   "&Register/Post..."
      Height          =   510
      Left            =   6900
      TabIndex        =   29
      Top             =   4500
      WhatsThisHelpID =   50
      Width           =   2070
   End
   Begin VB.Frame Frame3 
      Height          =   1755
      Left            =   90
      TabIndex        =   10
      Top             =   1890
      Width           =   6645
      Begin VB.TextBox txtDepSlipNumber 
         Height          =   285
         Left            =   1665
         MaxLength       =   10
         TabIndex        =   16
         Top             =   960
         WhatsThisHelpID =   51517
         Width           =   1830
      End
      Begin VB.TextBox txtBankAcct 
         Height          =   285
         Left            =   1665
         MaxLength       =   15
         TabIndex        =   14
         Top             =   600
         WhatsThisHelpID =   51516
         Width           =   1830
      End
      Begin SOTACalendarControl.SOTACalendar txtDepositDate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1665
         TabIndex        =   18
         Top             =   1320
         WhatsThisHelpID =   51515
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin LookupViewControl.LookupView navBankAcct 
         Height          =   285
         Left            =   3540
         TabIndex        =   33
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   600
         WhatsThisHelpID =   51514
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin LookupViewControl.LookupView navBatchOvrdSegValue 
         Height          =   285
         Left            =   2745
         TabIndex        =   32
         TabStop         =   0   'False
         ToolTipText     =   "Lookup"
         Top             =   240
         WhatsThisHelpID =   51513
         Width           =   285
         _ExtentX        =   503
         _ExtentY        =   503
         LookupMode      =   1
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtBatchOvrdSegValue 
         Height          =   285
         Left            =   1665
         TabIndex        =   12
         Top             =   240
         WhatsThisHelpID =   51512
         Width           =   1050
         _Version        =   65536
         _ExtentX        =   1852
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblDepositDate 
         AutoSize        =   -1  'True
         Caption         =   "Dep&osit Date"
         Height          =   195
         Left            =   150
         TabIndex        =   17
         Top             =   1350
         Width           =   930
      End
      Begin VB.Label lblDepSlipNumber 
         AutoSize        =   -1  'True
         Caption         =   "Deposit Slip &No"
         Height          =   195
         Left            =   150
         TabIndex        =   15
         Top             =   990
         Width           =   1095
      End
      Begin VB.Label lblBankAcct 
         AutoSize        =   -1  'True
         Caption         =   "&Bank Account"
         Height          =   195
         Left            =   150
         TabIndex        =   13
         Top             =   630
         Width           =   1020
      End
      Begin VB.Label lblBatchOvrdSegValueDesc 
         AutoSize        =   -1  'True
         Caption         =   "Segment Value Description Here"
         Height          =   195
         Left            =   3120
         TabIndex        =   34
         Top             =   270
         UseMnemonic     =   0   'False
         Width           =   2310
      End
      Begin VB.Label lblBatchOvrdSegValue 
         AutoSize        =   -1  'True
         Caption         =   "&Acct Ovrd Segment"
         Height          =   195
         Left            =   150
         TabIndex        =   11
         Top             =   285
         Width           =   1395
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   90
      TabIndex        =   3
      Top             =   870
      Width           =   6645
      Begin VB.CheckBox chkBatchHold 
         Caption         =   "On &Hold"
         Height          =   195
         Left            =   210
         TabIndex        =   7
         Top             =   600
         WhatsThisHelpID =   55
         Width           =   975
      End
      Begin VB.CheckBox chkBatchPrivate 
         Caption         =   "&Private"
         Height          =   195
         Left            =   210
         TabIndex        =   4
         Top             =   240
         WhatsThisHelpID =   53
         Width           =   1125
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtBatchHoldReason 
         Height          =   285
         Left            =   3075
         TabIndex        =   9
         Top             =   585
         WhatsThisHelpID =   56
         Width           =   2235
         _Version        =   65536
         _ExtentX        =   3942
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         lMaxLength      =   20
      End
      Begin SOTACalendarControl.SOTACalendar txtBatchPostDate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   3075
         TabIndex        =   6
         Top             =   210
         WhatsThisHelpID =   54
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin VB.Label lblHoldReason 
         AutoSize        =   -1  'True
         Caption         =   "Hold Reas&on"
         Height          =   195
         Left            =   2055
         TabIndex        =   8
         Top             =   600
         Width           =   930
      End
      Begin VB.Label lblPostingDate 
         AutoSize        =   -1  'True
         Caption         =   "Posting Da&te"
         Height          =   195
         Left            =   2055
         TabIndex        =   5
         Top             =   240
         Width           =   915
      End
   End
   Begin VB.CommandButton cmdTransactions 
      Caption         =   "&Enter Invoices"
      Height          =   510
      Left            =   6900
      TabIndex        =   27
      Top             =   2745
      WhatsThisHelpID =   51498
      Width           =   2070
   End
   Begin VB.CommandButton cmdSelect 
      Caption         =   "&Select Recurring..."
      Height          =   510
      Left            =   6900
      TabIndex        =   24
      Top             =   975
      WhatsThisHelpID =   51497
      Width           =   2070
   End
   Begin VB.Frame fraBatchTran 
      Height          =   1950
      Left            =   90
      TabIndex        =   19
      Top             =   3720
      Width           =   3015
      Begin VB.Frame fraLine 
         Height          =   120
         Left            =   1110
         TabIndex        =   39
         Top             =   975
         Width           =   1785
      End
      Begin NEWSOTALib.SOTACurrency curBatchTotal 
         Height          =   285
         Left            =   1110
         TabIndex        =   38
         Top             =   675
         WhatsThisHelpID =   58
         Width           =   1815
         _Version        =   65536
         _ExtentX        =   3201
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTACurrency curBatchVariance 
         Height          =   285
         Left            =   1095
         TabIndex        =   41
         Top             =   1185
         WhatsThisHelpID =   59
         Width           =   1815
         _Version        =   65536
         _ExtentX        =   3201
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTACurrency curBatchCtrlAmt 
         Height          =   285
         Left            =   1125
         TabIndex        =   21
         Top             =   270
         WhatsThisHelpID =   57
         Width           =   1815
         _Version        =   65536
         _ExtentX        =   3201
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<HL> <ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin VB.Label lblBatchTotal 
         AutoSize        =   -1  'True
         Caption         =   "Batch Total"
         Height          =   195
         Left            =   150
         TabIndex        =   37
         Top             =   690
         Width           =   825
      End
      Begin VB.Label lblVarianceTotal 
         AutoSize        =   -1  'True
         Caption         =   "Variance"
         Height          =   195
         Left            =   150
         TabIndex        =   40
         Top             =   1200
         Width           =   630
      End
      Begin VB.Label lblControlTotal 
         AutoSize        =   -1  'True
         Caption         =   "Control &Amt"
         Height          =   195
         Left            =   150
         TabIndex        =   20
         Top             =   315
         Width           =   810
      End
   End
   Begin VB.CommandButton cmdPrintInvoices 
      Caption         =   "Print &Invoices..."
      Height          =   510
      Left            =   6900
      TabIndex        =   28
      Top             =   3330
      WhatsThisHelpID =   51488
      Width           =   2070
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   54
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   55
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   56
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   48
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   5700
      WhatsThisHelpID =   1
      Width           =   9090
      _ExtentX        =   0
      _ExtentY        =   688
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtBatchDesc 
      Height          =   285
      Left            =   3165
      TabIndex        =   2
      Top             =   555
      WhatsThisHelpID =   51
      Width           =   3105
      _Version        =   65536
      _ExtentX        =   5477
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   50
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtBatchStatus 
      Height          =   285
      Left            =   7410
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   570
      WhatsThisHelpID =   52
      Width           =   1530
      _Version        =   65536
      _ExtentX        =   2699
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
      text            =   "Display Status Here"
   End
   Begin LookupViewControl.LookupView navMain 
      Height          =   285
      Left            =   1920
      TabIndex        =   31
      TabStop         =   0   'False
      ToolTipText     =   "Navigator"
      Top             =   540
      WhatsThisHelpID =   14
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
   End
   Begin VB.CommandButton cmdSelectShipments 
      Caption         =   "Select Ship&ments..."
      Height          =   510
      Left            =   6900
      TabIndex        =   25
      Top             =   1560
      WhatsThisHelpID =   51476
      Width           =   2070
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   45
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblComment 
      AutoSize        =   -1  'True
      Caption         =   "&Desc"
      Height          =   195
      Left            =   2655
      TabIndex        =   1
      Top             =   600
      Width           =   375
   End
   Begin VB.Label lblStatus 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Status:"
      Height          =   195
      Left            =   6840
      TabIndex        =   35
      Top             =   600
      Width           =   495
   End
   Begin VB.Label lblBatchNumber 
      AutoSize        =   -1  'True
      Caption         =   "Batch"
      Height          =   195
      Left            =   90
      TabIndex        =   30
      Top             =   585
      Width           =   420
   End
End
Attribute VB_Name = "frmBatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmBatch
'     Desc: Batch Processing Project
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 11/09/95 KMC
'     Mods: 02/01/96 AEW   Got it to work with AR.
'************************************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Declare filter state tracking variable
    Private miFilter As Integer

'Form-Level Constants
'TranType Column Constants
    Private Const kColTranTypeID = 1            'Transaction Type ID
    Private Const kColTranTypeKey = 2           'Transaction Type Key
    Private Const kColTranCount = 3             'Transaction Count
    Private Const kColTranTotal = 4             'Transaction Total
    
    Private Const kMaxCols = 4                  'Maximum Number of Grid Columns

    Private Const kBatchNoLength = 7            'Length of Batch No

'Child Sage MAS 500 Objects created from this Form
    Public moSotaObjects        As Collection   'Sage MAS 500 Objects
    Public moVM                 As clsValidationManager 'LostFocus class
    
'Button Mapping Collection Variables
    Public moMapSrch            As Collection   'Search Buttons
    Public moMapMemo            As Collection   'Memo Buttons
        
        
'Private Form Level Property Variables
    Private moClass             As Object           'Form Class Module
    Private mlRunMode           As Long             'Run Mode (AOF, normal etc...)
    Private mbCancelShutDown    As Boolean          'Cancel shutdown (used with AOF)
    Private mbUnload            As Boolean          'Force Unload
    Private mbAutoNum           As Boolean          'Auto numbered
    Private miSecurityLevel     As Integer          'Security Level
    Private mbCaseSens          As Boolean          'Case Sensitive server?
    Private mbMCEnabledForAR    As Boolean          'Is multicurrency enabled for AR Module
    Private mbCCActivated       As Boolean          'Is credit card module activated?

'Currency info
    Private msHomeCurrID        As String           'Home currency ID
    Private muHomeCurrInfo      As CurrencyInfo     'currency infor for home currency
    
'Data Manager Class Variables
    Private moDmForm            As clsDmForm        ' tarBatch
    Private moDmGridBatchTran   As clsDmGrid        ' tciBatchTran
    Private moDmCashTranLog     As clsDmForm        ' tcmCashTranLog
    
'Context Menu Class Variable
    Private moContextMenu       As clsContextMenu   'Right-Click Context Menu Class

'Session Object Property Variables
    Private mbEnterAsTab        As Boolean          'Tab to next control if user presses enter
    Private msCompanyID         As String           'Company Id
    Private msUserID            As String           'User Id
    Private msBusinessDate      As String           'Business date
    
'Control Validation Variable
    Private mbDontChkClick      As Boolean          'Global dont run click event processing
    
  'Resize Variables
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
        
'Batch Status variables user for display of the batch Status for each batch.
    Private miBatchLogSysDetlStatus     As Integer          'Status For batch Log System detail
    Private moBatchStatus               As New Collection   'batch Status Object

'Batch Type (based on TaskID of program)
    Private mlBatchType                 As Long

'Type Variables for tables
    Private muAROptions           As AROptionsType            'AR Options
    Private muGLSegment           As GLSegmentType            'GL Segment mask/Segment etc...
    Private muCIBatchTypCompany   As CIBatchTypCompanyType    'batch Type Company Defaults
    Private muCIBatchLogSysDetl   As CIBatchLogSysDetlType    'batch Sys Detail Defaults
    Private muCIBatchLog          As CIBatchLogType           'batch Log Defaults
    Private muCMCashAcct          As CMCashAccountType        'cash (bank) account

'Variable indicating form is read-only (called from a drill around)
    Private mbReadOnly            As Boolean

'Trigger to load a single batch
    Private mbLoadBatch           As Boolean

'Position of command buttons
    Private miCmdsTop(6)                As Integer
    Private miCmdsLeft(6)               As Integer

'Current fiscal per start/end dates
    Private mdStartDate                 As Date
    Private mdEndDate                   As Date

'Intellisol start
    Private mbNavMainClick As Boolean
'Intellisol end

    ' RMM 41057, Part of fix
    Private mbBatchExists As Boolean
    
    Private Const kTranTypeARIN = 501
    Private Const kBatchTypeARRA = 510
    Private mbInvcOverflow  As Boolean
    
    Private Const kApplReverseTypeMemo = 1
    Private Const kApplReverseTypePayment = 2

    'store procedure return value
    Private Const kRetError = 0

' AvaTax integration - General Declarations
    Private mbTrackSTaxOnSales      As Boolean ' Used to check AR TrackSTaxOnSales option
    Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
    Public mbAvaTaxEnabled          As Boolean ' Defines if AvaTax is installed on the DB & Client , and is turned on for this company
' AvaTax end

'RKL DEJ 2016-07-21 (START)
    Private lcbTempExist As Boolean
'RKL DEJ 2016-07-21 (STOP)

Const VBRIG_MODULE_ID_STRING = "ARZTL001.FRM"

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = "ARZ"
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = "ARZ"
End Property
'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   bUnload contains a Boolean to tell LoadUI method in the class to shut the
'   form down.
'************************************************************************
Public Property Get bUnload() As Boolean
'+++ VB/Rig Skip +++
    bUnload = mbUnload
End Property

Public Property Let bUnload(bNewUnload As Boolean)
'+++ VB/Rig Skip +++
    mbUnload = bNewUnload
End Property

'************************************************************************
'   lRunMode contains the user-defined run mode context in which the
'   class object was Initialzed. The run mode is extracted from the
'   context passed in through the InitializeObject.
'************************************************************************
Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
    lRunMode = mlRunMode
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Skip +++
    bCancelShutDown = mbCancelShutDown
End Property


Private Sub chkBatchHold_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkBatchHold, True
    #End If
'+++ End Customizer Code Push +++
'****************************************************************************
' Desc: If On Hold is Clicked Enable the Hold Reason Textbox an Label
'       otherwise clear out the Value in the Hold text box and enable the
'       Hold Reason Text Box and Label
'****************************************************************************

    If Not gbGotFocus(Me, chkBatchHold) Then Exit Sub
    
    SetupHoldReason
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkBatchHold_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupHoldReason()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If chkBatchHold = vbChecked Then
        'Placing batch On Hold Enable Hold Reason Controls
        txtBatchHoldReason.Protected = False
    Else
        'batch Was taken off Hold clear out Hold reason
        'disable Hold reason controls
        txtBatchHoldReason = Empty
        txtBatchHoldReason.Protected = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupHoldReason", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBatchHold_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkBatchHold, True
    #End If
'+++ End Customizer Code Push +++
'****************************************************************************
' Desc: If Hold Status has been changed the reset the batch Status
'****************************************************************************
    
  'Value Was Changed
    If CInt(Val(chkBatchHold.Tag)) <> chkBatchHold Then
        chkBatchHold.Tag = chkBatchHold               'Reset Tag
        DisplayBatchStatus                  'Reset batch Status
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkBatchHold_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ResetToolbarForCreditCard()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSQL As String
Dim rs As Object
Dim lBatchKey As Long
Dim iTranState As Integer
Dim iTranType As Integer
Dim iOrigTranType As Integer

Const kCCTranStateDeposited As Integer = 2
Const kCCTranStateVoided    As Integer = 3
Const kCCTranStateSettled   As Integer = 4
Const kCCTranStateRefunded  As Integer = 5




    If mbCCActivated Then
        tbrMain.ButtonEnabled(kTbDelete) = True
               
        'Check to see if credit card transaction exists.  If so, disable the delete toolbar button.

        lBatchKey = glGetValidLong(moDmForm.GetColumnValue("BatchKey"))
        
        sSQL = "SELECT TranState, TranType, RevrsCustPmtKey FROM tccTranLog WITH (NOLOCK) JOIN tarPendCustPmt WITH (NOLOCK) "
        sSQL = sSQL & " ON tccTranLog.CustPmtKey = tarPendCustPmt.CustPmtKey "
        sSQL = sSQL & " WHERE tarPendCustPmt.BatchKey = " & lBatchKey

        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not (rs Is Nothing) Then
            rs.MoveFirst
            Do While Not rs.IsEOF
            
                
                iTranState = giGetValidInt(rs.Field("TranState"))
                iTranType = giGetValidInt(rs.Field("TranType"))

                'Disable deletion if payment is deposited or settled, reversed or refunded.  If the tran type is not cash receipt, then it is either reversal or refund.
                'Reversals and refunds both have a state of refunded if the cc credit has been approve.
                If ((iTranState = kCCTranStateDeposited Or iTranState = kCCTranStateSettled) And iTranType = kTranTypeARCR) Or (iTranState = kCCTranStateRefunded And iTranType <> kTranTypeARCR) Then
                    tbrMain.ButtonEnabled(kTbDelete) = False
                    Exit Do
                End If
                
                  'Refunds may also be reversed.  In this case the CC transaction is voided, and the TranState is voided.  The batch cannot be deleted in this case.
                If iTranState = kCCTranStateVoided And iTranType = kTranTypeARRV Then
                    iOrigTranType = moClass.moAppDB.Lookup("TranType", "tarCustPmtLog", "CustPmtKey = " & glGetValidLong(rs.Field("RevrsCustPmtKey")))
                    If iOrigTranType = kTranTypeARRE Then
                        tbrMain.ButtonEnabled(kTbDelete) = False
                        Exit Do
                    End If
                End If
                rs.MoveNext
            Loop
            rs.Close
        End If

        If Not rs Is Nothing Then
            Set rs = Nothing
        End If
                
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ResetToolbarForCreditCard", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetNewBatch()
'+++ VB/Rig Skip +++
'****************************************************************************
' Desc: Ask User if they want to save changes Then create a new batch Number
'       and run batch key change processing
'****************************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim OldTxtBatch As String

    'Ask user to save changes made.
    If Not bConfirmUnloadSuccess() Then Exit Sub
    
    'Get old Batch Number to reset.
    OldTxtBatch = txtBatchNo.Text
  
    'Get Next Valid Batch Number.
    txtBatchNo = CStr(lGetNextBatchNo())
        
    If mbUnload Then
        txtBatchNo.Text = OldTxtBatch
        mbUnload = False
        gbSetFocus Me, txtBatchNo
    Else
        'Set focus to the batch field and disable it.  This causes a lostfocus
        'to fire which will load the batch.
        If Me.Visible Then
            bSetFocus txtBatchNo
            gDisableControls txtBatchNo
        End If
    End If

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "GetNewBatch"
    gClearSotaErr
    Exit Sub

End Sub


Private Sub PrintPost()
'+++ VB/Rig Skip +++
'************************************************************************************
'   Description: Call Print register (Gl Post) Object
'************************************************************************************

    On Error GoTo ExpectedErrorRoutine
    
    Dim lBatchKey           As Long      'current batch Key
    Dim oPrintPost          As Object    'Print/Post register object
    Dim iPosted             As Integer   'Register posted Flag
    Dim dtPostDate          As Date      'posting date
    Dim iStatus             As Integer   'batch Status
    Dim sBatchNo            As String    'batch number
    Dim iHookType           As Integer   'Save keyboard hook Type
    'Intellisol start
    Dim lIndex              As Long      'Used as counter for loop
    'Intellisol end

    lBatchKey = moDmForm.GetColumnValue("BatchKey")
    
   

    ' update current batch status
    DisplayBatchStatus

    ' if batch is in use, can't continue
    If muCIBatchLogSysDetl.Status = kvInUse Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchInUse
        Exit Sub
    End If

    ' if batch is not in an editable state, stop user
    If Not bCanEditBatch() Then Exit Sub

    ' check post date before posting (give warning if not in current period)
    If Not bIsValidPostDate() Then Exit Sub

  'Check if batch is in a Status you can save it
    If Not bCanSaveBatch() Then Exit Sub
  
  'Save current batch Header exit on error
    If Not bSaveBatch() Then Exit Sub
        
  'Show new batch Status
    DisplayBatchStatus

    ' disable winhook
    ' **PRESTO ** iHookType = WinHook1.KeyboardHook
    ' **PRESTO ** WinHook1.KeyboardHook = shkKbdDisabled
    Me.Enabled = False

    

  'Create register object based on Type of batch open
    Select Case muCIBatchTypCompany.BatchType
        Case kBatchTypeARIN
            'Invoices
           'Open Invoice Register
            Set oPrintPost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoicePrintPostRPT, ktskInvoicePrintPost, _
                              kAOFRunFlags, kContextAOF)
            
        Case kBatchTypeARCR
            'Cash Receipts/Customer Payments
           'Open Cash Receipt Register
            Set oPrintPost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoicePrintPostRPT, ktskCashRcptPrintPost, _
                              kAOFRunFlags, kContextAOF)
            
        Case kBatchTypeARPA
            'apply Payments
           'Open apply Payment Register
            Set oPrintPost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoicePrintPostRPT, ktskPmtAppPrintPost, _
                              kAOFRunFlags, kContextAOF)
                              
        Case kBatchTypeARRA
            'Reverse Applications
           'Open apply Payment Register
            Set oPrintPost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoicePrintPostRPT, ktskReverseApplPrintPost, _
                              kAOFRunFlags, kContextAOF)

        Case kBatchTypeARCO
            'sales commissions
           'Open Sales Comm Register
            Set oPrintPost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoicePrintPostRPT, ktskSalesCommPrintPost, _
                              kAOFRunFlags, kContextAOF)

        Case kBatchTypeARFC
            'finance charges
           'Finance Charges Register is the invoices register
            Set oPrintPost = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoicePrintPostRPT, ktskFinChgReg, _
                              kAOFRunFlags, kContextAOF)

    End Select

    If oPrintPost Is Nothing Then
        giSotaMsgBox Me, moClass.moSysSession, kNoPrintPost
    Else
        dtPostDate = CDate(txtBatchPostDate.Text)
        lBatchKey = moDmForm.GetColumnValue("BatchKey")
        oPrintPost.StartInvoiceRegister dtPostDate, lBatchKey, bUserIsOwner
    End If

    'Intellisol start
    For lIndex = 1 To moSotaObjects.Count
        If moSotaObjects.Item(lIndex) Is oPrintPost Then
            giCollectionDel moClass.moFramework, moSotaObjects, lIndex
        End If
    Next lIndex
    'Intellisol end

    Set oPrintPost = Nothing

    Me.Enabled = True
    If Me.Visible Then
        Me.SetFocus
    End If

    'Get the Batch Status.
    If Not bGetBatchStatus(iStatus) Then
        giSotaMsgBox Me, moClass.moSysSession, kNoBatchStatus
    Else
        Select Case iStatus
            Case kvPosted
                moDmForm.Clear
            
            Case kvInterrupted
                ' remember batch no
                sBatchNo = txtBatchNo
                ' clear the form
                moDmForm.Clear True
                ' restore the batch
                txtBatchNo = sBatchNo
                ' trigger the DM keychange which will display the
                ' new batch status
                moVM.IsValidControl txtBatchNo, True
            
            Case Else
                
                'If the Register is printed but not posted, remain in the batch.
                'Any posting errors will have been be displayed by the
                'Register posting process.
        
        End Select

    End If

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "PrintPost"

    If Not Me.Enabled Then
        Me.Enabled = True
        If Me.Visible Then
            Me.SetFocus
        End If
    End If
    gClearSotaErr

    ' re-enable winhook
    ' **PRESTO ** WinHook1.KeyboardHook = iHookType

    Exit Sub
    
End Sub

Private Sub chkBatchPrivate_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkBatchPrivate, True
    #End If
'+++ End Customizer Code Push +++

    ' make sure changes are ignored if a record is not pulled up
    If moDmForm.State = kDmStateNone Then
        chkBatchPrivate.Value = vbUnchecked
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkBatchPrivate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'Merge invoices based on sales order enhancement
Private Sub cmdMerge_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
On Error GoTo VBRigErrorRoutine                                                           'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdMerge, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

Dim lRetVal             As Integer
Dim lBatchKey           As Long     'batch Surrogate Key
Dim iStatus             As Integer  'Current batch status
Dim lLockID             As Long     'batch lock ID
Dim sStatus             As String
Dim sSQL                As String
Dim iResp               As Integer

Dim bProceed            As Boolean
Dim lCountInvc          As Long
Dim lCountPreMerge      As Long
Dim lCountPostMerge     As Long
Dim sInvcList           As String

'*********************************************************
'RKL DEJ 2016-07-21 (START)
'*********************************************************
Dim sRtnErrMsg          As String
'*********************************************************
'RKL DEJ 2016-07-21 (STOP)
'*********************************************************


    Static bInHere As Boolean

    If bInHere Then Exit Sub
    bInHere = True

    If Not gbGotFocus(Me, cmdMerge) Then bInHere = False: Exit Sub

    'Update current batch status
    DisplayBatchStatus

    'If batch is not in an editable state, prevent edits
    If Not bCanEditBatch() Then bInHere = False: Exit Sub

 
    'Check if batch is in a Status you can save it
    If Not bCanSaveBatch() Then bInHere = False: Exit Sub


    'Save current batch Header exit on error
    If Not bSaveBatch() Then bInHere = False: Exit Sub


    'Get batch Key
    lBatchKey = moDmForm.GetColumnValue("BatchKey")

    'Check if there are any pending invoices in batch
    lCountInvc = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendInvoice", "BatchKey = " & lBatchKey & " AND TranType = " & kTranTypeARIN))
    If lCountInvc = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvNoPendInv
        bInHere = False
        Exit Sub
    End If
    
    '*****************************************************************
    'RKL DEJ 2016-07-21 (START)
    '*****************************************************************
    Dim bCanceled As Boolean
    Dim bMergByBSO As Boolean
    
    bMergByBSO = False
    bCanceled = False
    
    frmMergeOptions.Show vbModal, Me
    
    bCanceled = frmMergeOptions.bCanceled
    bMergByBSO = frmMergeOptions.bMergByBSO
    
    Unload frmMergeOptions
    
    If bCanceled = True Then
        bInHere = False
        Exit Sub
    End If
    
    If bMergByBSO = True Then
        'Create and populate merge temp table (Custom Code)
        InitBSOMergeTempTable lBatchKey
    Else
        'Create and populate merge temp table (BASE Code)
        InitMergeTempTable lBatchKey
    End If
    '*****************************************************************
    'RKL DEJ 2016-07-21 (STOP)
    '*****************************************************************
    
    'Determine if there are invoices to merge
    lCountPreMerge = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "#InvcToMerge", ""))
    If lCountPreMerge = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvNoMrgInvoice
        bInHere = False
        Exit Sub
    End If
    
    'Exclusively Lock batch - No one else can be in this batch during merging
    lLockID = lLockBatch(lBatchKey, kLockTypeExclusive)
 
    If lLockID < kLockRetSuccess Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvNoLock
        bInHere = False
        Exit Sub
    End If

    'If not already in use, mark it as in use.  If we can't mark it as in use,
    ' we can't let user go into transactions
    If iStatus <> kvInUse Then
        If iUpdateCIBatchLogSysDetl(kvInUse, False) = kDmFailure Then
            DropLock lLockID
            bInHere = False
            Exit Sub
        End If
    End If
    DisplayBatchStatus
 
    'Reset register printed flag
    If Not bResetRegPrintFlag() Then
        DropLock lLockID
        bInHere = False
        Exit Sub
    End If

    'Disable the form to prevent user from closing the form prematurely
    Me.Enabled = False
    
    iResp = giSotaMsgBox(Me, moClass.moSysSession, kmsgARMrgInvNotReverse)
    
    If iResp = vbYes Then
    
        'Display message of all invoices that cannot be merged
        If lCountInvc <> lCountPreMerge Then
            mbInvcOverflow = False
            sInvcList = sGetUnMergedInvcText(lBatchKey)
            
            If Not mbInvcOverflow Then
                iResp = giSotaMsgBox(Me, moClass.moSysSession, kmsgARMrgInvListInvoice, lCountInvc, lCountPreMerge, sInvcList)
            Else
                iResp = giSotaMsgBox(Me, moClass.moSysSession, kmsgARMrgInvListInvoice15, lCountInvc, lCountPreMerge, sInvcList)
            End If
            
            If iResp <> vbYes Then
                GoTo ExitMerge
            End If
        End If

        With sbrMain
            .MessageVisible = True
            .Message = "Merging Invoices....Please wait."
        End With

        With moClass.moAppDB
          .SetInParam lBatchKey
          .SetOutParam lCountPreMerge
          .SetOutParam lCountPostMerge
          .SetOutParam lRetVal
          
          '*********************************************************
          'RKL DEJ 2016-07-21 (START)
          '*********************************************************
          If bMergByBSO = True Then
            .SetOutParam sRtnErrMsg       'Added a new optional param
          
            .ExecuteSP ("sparMergeInvoicesByBSO_SGS")
          Else
            .ExecuteSP ("sparMergeInvoices")
          End If
          '*********************************************************
          'RKL DEJ 2016-07-21 (STOP)
          '*********************************************************
          
          lCountPreMerge = glGetValidLong(.GetOutParam(2))
          lCountPostMerge = glGetValidLong(.GetOutParam(3))
          lRetVal = .GetOutParam(4)
          
          '*********************************************************
          'RKL DEJ 2016-07-21 (START)
          '*********************************************************
          If bMergByBSO = True Then
                sRtnErrMsg = gsGetValidStr(.GetOutParam(5))
          End If
          '*********************************************************
          'RKL DEJ 2016-07-21 (START)
          '*********************************************************
          
          .ReleaseParams
        End With
        
        Select Case lRetVal
            Case 1
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvSuccess, lCountPostMerge, lCountPreMerge
                'Avalara Integration - BEGIN
                 If mbAvaTaxEnabled And mlBatchType = kBatchTypeARIN Then
                    moAvaTax.MergeInvoice moDmForm.GetColumnValue("BatchKey")
                 End If
                'Avalara Integration - END
            Case 2
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvNoMrgInvoice
            Case 3
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrTempTable
            Case 4
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrUpdInvDetl
            Case 5
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrDelSTax
            Case 6
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrInsSTax
            Case 7
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrUpdPendInvc
            Case 8
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrDelPendInvc
            Case 9
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrCalcComAmt
            Case 10
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrDelComm
            Case 11
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrSOPmtAppl
            Case 12
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrUpdInvLog
            Case 13
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrInsMrgHist
            Case 14
                giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvErrUpdAddr
            Case Else
                '*********************************************************
                'RKL DEJ 2016-07-21 (START)
                '*********************************************************
                If bMergByBSO = True Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, lRetVal & " sparMergeInvoicesByBSO_SGS"
                    MsgBox "Sproc sparMergeInvoicesByBSO_SGS Returned: lRetVal = " & CStr(lRetVal) & " and sRtnErrMsg = " & sRtnErrMsg, vbExclamation, "SAGE 500"
                Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, lRetVal & " sparMergeInvoices"
                End If
                '*********************************************************
                'RKL DEJ 2016-07-21 (STOP)
                '*********************************************************
        End Select
        
        sbrMain.MessageVisible = False
    
    End If
    
ExitMerge:

    'Drop batch lock
    DropLock lLockID

    'Re-enable form
    Me.Enabled = True
    If Me.Visible Then
        Me.SetFocus
    End If

    'Update batch status
    DisplayBatchStatus

    'Refresh the transaction Grid
    'In case transactions were Added/Deleted/Changed
    'Update batch Status
    RefreshTranGrid
    bInHere = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        'Re-enable form
        Me.Enabled = True
        If Me.Visible Then
            Me.SetFocus
        End If
        bInHere = False

        '*********************************************************
        'RKL DEJ 2016-07-21 (START)
        '*********************************************************
        If bMergByBSO = True And Trim(sRtnErrMsg) <> Empty Then
            MsgBox "VBRigErrorRoutine: Sproc sparMergeInvoicesByBSO_SGS Returned: lRetVal = " & CStr(lRetVal) & " and sRtnErrMsg = " & sRtnErrMsg, vbExclamation, "SAGE 500"
        End If
        '*********************************************************
        'RKL DEJ 2016-07-21 (STOP)
        '*********************************************************
        
        gSetSotaErr Err, sMyName, "cmdMerge_Click", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdPrintChecks_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPrintChecks, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim oPrintChecks    As Object
    Dim lBatchKey       As Long
    Dim lCashAcctKey    As Long
    Dim lLockID         As Long
    
    Static bInHere As Boolean
        
    If bInHere Then Exit Sub
    bInHere = True
    
    If moDmForm.State = kDmStateNone Then bInHere = False: Exit Sub
    
    If Not gbGotFocus(Me, cmdPrintChecks) Then bInHere = False: Exit Sub
    
    ' update current batch status
    DisplayBatchStatus

    ' if batch is not in an editable state, it can't be printed
    If Not bCanEditBatch() Then bInHere = False: Exit Sub
    
    'Check if batch is in a Status you can save it
    If Not bCanSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end
  
  'Save current batch Header exit on error
    If Not bSaveBatch() Then bInHere = False: Exit Sub
    
    lBatchKey = moDmForm.GetColumnValue("BatchKey")
    
    On Error GoTo ExpectedErrorRoutine
     
     'Lock the Batch Exclusively
    lLockID = lLockBatch(lBatchKey, kLockTypeExclusive)
   
    If lLockID < kLockRetSuccess Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgARMrgInvNoLock
        bInHere = False
        Exit Sub
    End If
   
    On Error GoTo RemoveExclusiveLock
    
'    If CIBatchLog.Status <> kvBalanced Then
'        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchBadStatus, (txtBatchStatus)
'        GoTo RemoveExclusiveLock
'    End If
    
    DisplayBatchStatus
    
    Set oPrintChecks = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                                      kclsARPrintChecksSingleBatch, ktskARPrintChecksSingleBatch, _
                                      kAOFRunFlags, kContextAOF)
        
    If oPrintChecks Is Nothing Then GoTo RemoveExclusiveLock
    
    lCashAcctKey = gvCheckNull(moDmForm.GetColumnValue("CashAcctKey"), SQL_INTEGER)
    
    
    If lCashAcctKey = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, gsStripChar(lblBankAcct, "&")
        txtBankAcct.SetFocus
        GoTo RemoveExclusiveLock
    End If
        
  
    oPrintChecks.iInitSingleCheckPrinting lBatchKey, lCashAcctKey, True, , False
    
    Set oPrintChecks = Nothing
        
      
RemoveExclusiveLock:
    bInHere = False
    
    DropLock lLockID
    DisplayBatchStatus
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++

Exit Sub
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdPrintChecks_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
    

Private Sub cmdPrintInvoices_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdPrintInvoices, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim oPrintInvoices      As Object
    Dim oDepSlip            As Object
    Dim oEnterTrans         As Object
    Dim iStatus             As Integer
    Dim lBatchKey           As Long
    Dim iApplReverseType    As Integer
    
    'Intellisol start
    Static bInHere As Boolean
    'Intellisol end
    
    'Intellisol start
    If bInHere Then Exit Sub
    bInHere = True
    'Intellisol end

    'Intellisol start
    'If moDmForm.State = kDmStateNone Then Exit Sub
    If moDmForm.State = kDmStateNone Then bInHere = False: Exit Sub
    'Intellisol end

    'Intellisol start
    'If Not gbGotFocus(Me, cmdPrintInvoices) Then Exit Sub
    If Not gbGotFocus(Me, cmdPrintInvoices) Then bInHere = False: Exit Sub
    'Intellisol end

    ' update current batch status
    DisplayBatchStatus

    ' if batch is not in an editable state, it can't be printed
    'Intellisol start
    'If Not bCanEditBatch() Then Exit Sub
    If Not bCanEditBatch() Then bInHere = False: Exit Sub
    'Intellisol end

    'Check if batch is in a Status you can save it
    'Intellisol start
    'If Not bCanSaveBatch() Then Exit Sub
    If Not bCanSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end
  
  'Save current batch Header exit on error
    'Intellisol start
    'If Not bSaveBatch() Then Exit Sub
    If Not bSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end

    Select Case muCIBatchTypCompany.BatchType
        Case kBatchTypeARIN, kBatchTypeARFC
            'Invoices and finance charges
            Set oPrintInvoices = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsPrintInvoiBAT, ktskPrintInvoiceBatch, _
                              kAOFRunFlags, kContextAOF)
            If Not oPrintInvoices Is Nothing Then
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                oPrintInvoices.PrintInvoices muCIBatchLog.BatchID, , muCIBatchTypCompany.BatchType
            End If

        Case kBatchTypeARCR
            'Call Deposit Slip task
            Set oDepSlip = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsDepositSlip, ktskDepositSlip, _
                              kAOFRunFlags, kContextAOF)
        
            ' check for errors
            If Not oDepSlip Is Nothing Then
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                oDepSlip.DoDepositSlip kModuleAR, lBatchKey
            End If
            
         Case kBatchTypeARRA
            'Call Reverse Payment Application
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsReverseApplDTE, ktskReversePmtAppl, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
            
                iApplReverseType = kApplReverseTypePayment
                            
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                oEnterTrans.ReverseAppl lBatchKey, muCIBatchLog.BatchID, iApplReverseType
                
            End If

    End Select

    Set oPrintInvoices = Nothing
    Set oDepSlip = Nothing
    
    ' update batch status
    DisplayBatchStatus

    'Refresh the transaction Grid
    'In case transactions were Added/Deleted/Changed
    'update batch Status
    RefreshTranGrid
    
    
    'Intellisol start
    bInHere = False
    'Intellisol end

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdPrintInvoices_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                'Intellisol start
                'Call giErrorHandler: Exit Sub
                Call giErrorHandler: bInHere = False: Exit Sub
                'Intellisol end
        End Select
        'Intellisol start
        bInHere = False
        'Intellisol end
'+++ VB/Rig End +++
End Sub

Private Sub cmdBatchRgstrPost_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdBatchRgstrPost, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim bData       As Boolean
    Dim sSQL        As String
    Dim lVal        As Long
    Dim lBatchKey   As Long
'Intellisol start
    Static bInHere As Boolean
    
    If bInHere Then Exit Sub
    bInHere = True
'Intellisol end
    
    bData = False
    lBatchKey = moDmForm.GetColumnValue("BatchKey")

    'TRM need to see if we have anything to post, if not show a message and exit
    Select Case muCIBatchTypCompany.BatchType
        Case kBatchTypeARIN   'Invoices
            lVal = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendInvoice", "BatchKey = " & lBatchKey))
            If lVal <> 0 Then bData = True
        Case kBatchTypeARFC   'Finance Charges
            lVal = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendInvoice", "BatchKey = " & lBatchKey))
            If lVal <> 0 Then bData = True
        Case kBatchTypeARCR   'Cash Receipts
            lVal = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendCustPmt", "BatchKey = " & lBatchKey))
            If lVal <> 0 Then bData = True
            
                      
        Case kBatchTypeARPA   'Payment Applications
            lVal = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendCustPmtAppl", "BatchKey = " & lBatchKey))
            If lVal <> 0 Then bData = True
            
        Case kBatchTypeARRA   'Reverse Applications
            lVal = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendCustPmtAppl", "BatchKey = " & lBatchKey))
            If lVal <> 0 Then bData = True
            
        Case kBatchTypeARCO   'Sales Commission
            lVal = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarSalesComm", "BatchKey = " & lBatchKey))
            If lVal <> 0 Then bData = True
        ' Next 3 types are currently not implemented
'        Case kBatchTypeARWO   'Write Off
'        Case kBatchTypeARBD   'Bad Debt
    End Select
    
    'If module post is complete then continue on.
    If moClass.moAppDB.Lookup("PostStatus", "tciBatchLog", "BatchKey = " & lBatchKey) >= 250 Then bData = True
    
    If Not bData Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgNoBatches, ""
        'Intellisol start
        bInHere = False
        'Intellisol end
'+++ VB/Rig Begin Pop +++
        Exit Sub
    End If
    
   
    'Intellisol start
    'If Not gbGotFocus(Me, cmdBatchRgstrPost) Then Exit Sub
    If Not gbGotFocus(Me, cmdBatchRgstrPost) Then bInHere = False: Exit Sub
    'Intellisol end

    PrintPost
    'Intellisol start
    bInHere = False
    'Intellisol end
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdBatchRgstrPost_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                'Intellisol start
'                Call giErrorHandler: Exit Sub
                Call giErrorHandler: bInHere = False: Exit Sub
                'Intellisol end
        End Select
'+++ VB/Rig End +++
        'Intellisol start
        bInHere = False
        'Intellisol end
End Sub

Private Sub cmdSelect_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Call Select Recurring Invoices Object
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim lBatchKey           As Long      'current batch Key
    Dim oSelect             As Object    'Select recurring object
    Dim iStatus             As Integer   'Current batch status
    Dim iHookType           As Integer   'winhook type
    Dim lLockID             As Long      'Batch lock ID
    
    Dim rs                  As DASRecordSet ' AvaTax Recordset
    Dim sSQL                As String       'AvaTax
    
    Const kPullSOPmts = 2
    
'Intellisol start
    Static bInHere As Boolean
    
    If bInHere Then Exit Sub
    bInHere = True
'Intellisol end

    'Intellisol start
'    If Not gbGotFocus(Me, cmdSelect) Then Exit Sub
    If Not gbGotFocus(Me, cmdSelect) Then bInHere = False: Exit Sub
    'Intellisol end

    'Intellisol start
    If moClass.mbCalledFromPA Then
        LoadGenProjInvoices
        bInHere = False
        Exit Sub
    End If
    'Intellisol end

    ' update current batch status
    DisplayBatchStatus

    ' if batch is not in an editable state, don't let user edit it
    'Intellisol start
'    If Not bCanEditBatch() Then Exit Sub
    If Not bCanEditBatch() Then bInHere = False: Exit Sub
    'Intellisol end

  'Check if batch is in a Status you can save it
    'Intellisol start
'    If Not bCanSaveBatch() Then Exit Sub
    If Not bCanSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end
  
    'Save current batch Header exit on error
    'Intellisol start
'    If Not bSaveBatch() Then Exit Sub
    If Not bSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end

  'Get batch Key, batch ID
    lBatchKey = moDmForm.GetColumnValue("BatchKey")
    muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))

    ' Lock batch
    lLockID = lLockBatch(lBatchKey)

    'if not already in use, mark it as in use.  If we can't mark it as in use,
    ' we can't let user select transactions
    If iStatus <> kvInUse Then
        If iUpdateCIBatchLogSysDetl(kvInUse, False) = kDmFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            'Intellisol start
            bInHere = False
            'Intellisol end
            Exit Sub
        End If
    End If
    DisplayBatchStatus

  'Reset register printed flag
    'Intellisol start
'    If Not bResetRegPrintFlag() Then Exit Sub
    If Not bResetRegPrintFlag() Then bInHere = False: Exit Sub
    'Intellisol end

    ' disable winhook
    ' **PRESTO ** iHookType = WinHook1.KeyboardHook
    ' **PRESTO ** WinHook1.KeyboardHook = shkKbdDisabled

    ' disable form
    Me.Enabled = False

      'Run Select Recurring program
    Select Case muCIBatchTypCompany.BatchType

        'Invoices - Select Recurring Invoices
        Case kBatchTypeARIN
            Set oSelect = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsSelectRecurr, ktskSelectRecurr, _
                              kAOFRunFlags, kContextAOF)
            If Not oSelect Is Nothing Then
                oSelect.Init lBatchKey, muCIBatchLog.BatchID
            End If

        ' Recurring Sales Commissions
        Case kBatchTypeARCO
            Set oSelect = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsSelectCommACT, ktskSelectComm, _
                              kAOFRunFlags, kContextAOF)
            If Not oSelect Is Nothing Then
                oSelect.Init lBatchKey, muCIBatchLog.BatchID
            End If

        ' Generate Fin Chgs
        Case kBatchTypeARFC
            Set oSelect = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsGenFinCharges, ktskGenFinCharges, _
                              kAOFRunFlags, kContextAOF)
            If Not oSelect Is Nothing Then
                oSelect.Init lBatchKey, muCIBatchLog.BatchID
            End If
            
        ' Select SO Payments
         Case kBatchTypeARCR
            Set oSelect = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                        kclsCISplitBatch, ktskCISplitBatch, _
                        kAOFRunFlags, kContextAOF)
        
            If Not (oSelect Is Nothing) Then
                oSelect.lLockID = lLockID
                oSelect.CashTranKey = glGetValidLong(moDmForm.GetColumnValue("CashTranKey"))
                oSelect.CashAcctCurrID = muCMCashAcct.CurrID
                oSelect.AllowMCDeposit = muCMCashAcct.AllowMCDep
                oSelect.SplitBatch lBatchKey, _
                       FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo)), _
                        mlBatchType, kPullSOPmts
        
                lLockID = oSelect.lLockID
            End If
            
        Case Else

            giSotaMsgBox Me, moClass.moSysSession, kBadRecurrBatchType, muCIBatchTypCompany.BatchType
    End Select
  
    'DROP TABLE HERE TO MARK batch AS IN use
    DropLock lLockID

    Set oSelect = Nothing
  
    'Avalara Integration - Start
    If mbAvaTaxEnabled Then
        'Read InvoiceKey generated through RecurInvoices for the current batch
        sSQL = "SELECT InvcKey FROM tarPendInvoice WITH (NOLOCK) WHERE RecurInvoiceKey IS NOT NULL AND BatchKey = " & lBatchKey
        'Debug.Print sSQL
        
        'Run Database query
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        With rs
            Do While Not .IsEOF
                    moAvaTax.bCalculateTax CLng(.Field("InvcKey")), kModuleAR
                .MoveNext
            Loop
            .Close
        End With
        
        'Clean up recordset object
        Set rs = Nothing
    End If
    'Avalara Integration - End
    
    'Remove In use Status
    RefreshTranGrid

    Me.Enabled = True
    If Me.Visible Then
        Me.SetFocus
    End If

    DisplayBatchStatus

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    
    'Intellisol start
    bInHere = False
    'Intellisol end
    
    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "cmdSelect_Click"
    If Not Me.Enabled Then
        Me.Enabled = True
        If Me.Visible Then
            Me.SetFocus
        End If
    End If
    ' in case of error, drop logical lock
    DropLock lLockID
    DisplayBatchStatus
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    'Intellisol start
    bInHere = False
    'Intellisol end
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdSelect_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                'Intellisol start
'                Call giErrorHandler: Exit Sub
                Call giErrorHandler: bInHere = False: Exit Sub
                'Intellisol end
        End Select
'+++ VB/Rig End +++
    'Intellisol start
    bInHere = False
    'Intellisol end
End Sub

Private Sub cmdSelectShipments_Click()

'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelectShipments, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Call Select Recurring Invoices Object
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim lBatchKey           As Long      'current batch Key
    Dim oSelect             As Object    'Select recurring object
    Dim iStatus             As Integer   'Current batch status
    Dim iHookType           As Integer   'winhook type
    Dim lLockID             As Long      'Batch lock ID
    
    Const kPull = 1

'Intellisol start
    Static bInHere As Boolean
    
    If bInHere Then Exit Sub
    bInHere = True
'Intellisol end

    'Intellisol start
'    If Not gbGotFocus(Me, cmdSelectShipments) Then Exit Sub
    If Not gbGotFocus(Me, cmdSelectShipments) Then bInHere = False: Exit Sub
    'Intellisol end

    ' update current batch status
    DisplayBatchStatus

    ' if batch is not in an editable state, don't let user edit it
    'Intellisol start
'    If Not bCanEditBatch() Then Exit Sub
    If Not bCanEditBatch() Then bInHere = False: Exit Sub
    'Intellisol end

  'Check if batch is in a Status you can save it
    'Intellisol start
'    If Not bCanSaveBatch() Then Exit Sub
    If Not bCanSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end
  
    'Save current batch Header exit on error
    'Intellisol start
'    If Not bSaveBatch() Then Exit Sub
    If Not bSaveBatch() Then bInHere = False: Exit Sub
    'Intellisol end

  'Get batch Key, batch ID
    lBatchKey = moDmForm.GetColumnValue("BatchKey")
    muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))

    ' Lock batch
    lLockID = lLockBatch(lBatchKey)

    'if not already in use, mark it as in use.  If we can't mark it as in use,
    ' we can't let user select transactions
    If iStatus <> kvInUse Then
        If iUpdateCIBatchLogSysDetl(kvInUse, False) = kDmFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            'Intellisol start
            bInHere = False
            'Intellisol end
            Exit Sub
        End If
    End If
    DisplayBatchStatus

  'Reset register printed flag
    'Intellisol start
'    If Not bResetRegPrintFlag() Then Exit Sub
    If Not bResetRegPrintFlag() Then bInHere = False: Exit Sub
    'Intellisol end

    ' disable winhook
    ' **PRESTO ** iHookType = WinHook1.KeyboardHook
    ' **PRESTO ** WinHook1.KeyboardHook = shkKbdDisabled

    ' disable form
    Me.Enabled = False

    Set oSelect = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                      kclsCISplitBatch, ktskCISplitBatch, _
                      kAOFRunFlags, kContextAOF)
        
    If Not (oSelect Is Nothing) Then
        oSelect.lLockID = lLockID
        oSelect.SplitBatch lBatchKey, _
                           FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo)), _
                           mlBatchType, kPull
        
        lLockID = oSelect.lLockID
        Set oSelect = Nothing
        Me.Enabled = True
        Me.SetFocus
        VMIsValidKey
    Else
        Me.Enabled = True
    End If
        
    DropLock lLockID
  
    RefreshTranGrid

    DisplayBatchStatus

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    'Intellisol start
    bInHere = False
    'Intellisol end
    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "cmdSelectShipments_Click"
    If Not Me.Enabled Then
        Me.Enabled = True
        If Me.Visible Then
            Me.SetFocus
        End If
    End If
    ' in case of error, drop logical lock
    DropLock lLockID
    DisplayBatchStatus
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    'Intellisol start
    bInHere = False
    'Intellisol end
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdSelectShipments_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                'Intellisol start
'                Call giErrorHandler: Exit Sub
                Call giErrorHandler: bInHere = False: Exit Sub
                'Intellisol end
        End Select
'+++ VB/Rig End +++
    'Intellisol start
    bInHere = False
    'Intellisol end
End Sub

Private Sub cmdTransactions_Click()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdTransactions, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Enter A/R Transactions based on batch Type Selected
'                Invoices, Cash receipts, apply Memos Payments
'************************************************************************************

    On Error GoTo ExpectedErrorRoutine

    Dim lBankAcctKey        As Long     'Bank acct Surrogate Key
    Dim lBatchKey           As Long     'batch Surrogate Key
    Dim oEnterTrans         As Object   'Enter Transactions Object
    Dim iHookType           As Integer  'Save keyboard hook Type
    Dim iStatus             As Integer  'Current batch status
    Dim lLockID             As Long     'batch lock ID
    Dim sStatus             As String
    Dim iApplReverseType    As Integer  'Application Reversal Type
    
    Static bInHere As Boolean
    
    If bInHere Then Exit Sub
    bInHere = True
   
    If Not gbGotFocus(Me, cmdTransactions) Then bInHere = False: Exit Sub


' update current batch status
    DisplayBatchStatus

' if batch is not in an editable state, prevent edits
    If Not bCanEditBatch() Then bInHere = False: Exit Sub


'Check if batch is in a Status you can save it
    If Not bCanSaveBatch() Then bInHere = False: Exit Sub

  
'Save current batch Header exit on error
    If Not bSaveBatch() Then bInHere = False: Exit Sub


'Get batch Key
    lBatchKey = moDmForm.GetColumnValue("BatchKey")

' Lock batch
    lLockID = lLockBatch(lBatchKey)

'if not already in use, mark it as in use.  If we can't mark it as in use,
' we can't let user go into transactions
    If iStatus <> kvInUse Then
        If iUpdateCIBatchLogSysDetl(kvInUse, False) = kDmFailure Then
            DropLock lLockID
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            'Intellisol start
            bInHere = False
            'Intellisol end
            Exit Sub
        End If
    End If
    DisplayBatchStatus

  'Reset register printed flag
    If Not bResetRegPrintFlag() Then
        DropLock lLockID
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        bInHere = False
        Exit Sub
    End If

    ' disable the form to prevent user from closing the form
    ' prematurely
    Me.Enabled = False

    'Based on the batch Type perform the following
    'Create Object - Invoices, Enter Cash Receipts, apply Memos Payments
    'Load BankAcct And batch Surrogate Keys
    'Open Form And Start data entry

    Select Case muCIBatchTypCompany.BatchType
        Case kBatchTypeARIN
            'Invoices
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsInvoice, ktskInvoice, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                With oEnterTrans
                    .CashAcctCurrID = muCMCashAcct.CurrID
                    .AllowMCDeposit = muCMCashAcct.AllowMCDep
                End With
                
                oEnterTrans.EnterInvoices lBatchKey, muCIBatchLog.BatchID
                
                SetTBState kDmStateEdit
            End If
            
        Case kBatchTypeARCR
            'Cash Receipts/Customer Payments
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsCustPmtDTE, ktskCustPmtDTE, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                With oEnterTrans
                    .CashTranKey = Val(moDmForm.GetColumnValue("CashTranKey"))
                    .CashAcctCurrID = muCMCashAcct.CurrID
                    .CashAcctKey = muCMCashAcct.CashAcctKey
                    .AllowMCDeposit = muCMCashAcct.AllowMCDep
                    .EnterCashRcpts lBatchKey, muCIBatchLog.BatchID
                End With
                
                SetTBState kDmStateEdit
            End If
            
        Case kBatchTypeARPA
            'apply Payments
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsAppMemoPmtDTE, ktskAppMemoPmtDTE, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                oEnterTrans.ApplyMemosPmts lBatchKey, muCIBatchLog.BatchID
            End If
            
        Case kBatchTypeARRA
            'Reverse Application
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kClsReverseApplDTE, ktskReverseMemoAppl, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
            
                iApplReverseType = kApplReverseTypeMemo
                            
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                oEnterTrans.ReverseAppl lBatchKey, muCIBatchLog.BatchID, iApplReverseType
                
            End If
        
        Case kBatchTypeARCO
            'Sales Commissions
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsEditCommACT, ktskEditComm, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                oEnterTrans.Init lBatchKey, muCIBatchLog.BatchID
            End If

        Case kBatchTypeARFC
            'Finance Charges
            Set oEnterTrans = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsEditFinChg, ktskEditFinChg, _
                              kAOFRunFlags, kContextAOF)
            If Not oEnterTrans Is Nothing Then
                lBatchKey = moDmForm.GetColumnValue("BatchKey")
                muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
                oEnterTrans.EnterInvoices lBatchKey, muCIBatchLog.BatchID
            End If

    End Select

    ' DROP batch lock
    DropLock lLockID

    ' destroy transactions object
    Set oEnterTrans = Nothing

    ' re-enable form
    Me.Enabled = True
    If Me.Visible Then
        Me.SetFocus
    End If

    ' update batch status
    DisplayBatchStatus

    'Refresh the transaction Grid
    'In case transactions were Added/Deleted/Changed
    'update batch Status
    RefreshTranGrid
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    bInHere = False
    Exit Sub

ExpectedErrorRoutine:
    If Not Me.Enabled Then
        Me.Enabled = True
    End If

    If Err.Number = 91 Then
        gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        bInHere = False
        Exit Sub
    End If

    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "cmdTransactions_Click"
    If Me.Visible Then
        Me.SetFocus
    End If

    DropLock lLockID
    DisplayBatchStatus

    ' re-enable winhook on errors
    ' **PRESTO ** WinHook1.KeyboardHook = iHookType
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    'Intellisol start
    bInHere = False
    'Intellisol end
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cmdTransactions_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                'Intellisol start
'                Call giErrorHandler: Exit Sub
                Call giErrorHandler: bInHere = False: Exit Sub
                'Intellisol end
        End Select
'+++ VB/Rig End +++
        'Intellisol start
        bInHere = False
        'Intellisol end
End Sub

Private Sub curBatchCtrlAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curBatchCtrlAmt, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: update the variance dollars
'************************************************************************************
    If curBatchCtrlAmt = 0 Then   'No transaction control amount
        curBatchVariance = 0      'No Variance needed
        HideVariance True
    Else
      'A tran Control amount was entered
        curBatchVariance = curBatchCtrlAmt - curBatchTotal  'Calculate variance
        HideVariance False
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curBatchCtrlAmt_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchCtrlAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curBatchCtrlAmt, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Change Status Flag (based on Value of control)
'************************************************************************************
    moVM.IsValidControl curBatchCtrlAmt
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curBatchCtrlAmt_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_Initaialize Initializes the Global Validating flag and
'         The cancel shutdown property on the form
'********************************************************************
    
    mbCancelShutDown = False    'Cancel Shutdown property

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True process predefined function keys.
'********************************************************************
    Dim sKey    As String
    Dim lKey    As Long
    

    Select Case keycode
        Case vbKeyF1 To vbKeyF16              'Function Keys pressed
            gProcessFKeys Me, keycode, Shift  'Run Sage MAS 500 Function Key Processing
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview
'         property of the form is set to True process the enter key
'         as if the user pressed the tab key.
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter key
            If mbEnterAsTab Then    'enter as tab set in session
                gProcessSendKeys "{Tab}"    'send tab key
                KeyAscii = 0        'cancel enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Form_Load initializes variables to be used throughout the
'       form's user interface.  It also sets up control
'       specific properties.  For example, here you would fill a
'       combo box with static/dynamic list information.
'********************************************************************
    Dim sCaption As String
    Dim lTaskNumber As Long
    Dim sSQL As String

    Set sbrMain.Framework = moClass.moFramework
    
    'Instantiate objects
    Set moSotaObjects = New Collection         'Sage MAS 500 Objects
    Set moMapSrch = New Collection             'Search Buttons
    Set moMapMemo = New Collection             'Memo Buttons
 
    'Resize Variable Assignments
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    'Standard Form Load Session Variables
    With moClass.moSysSession
        msCompanyID = .CompanyID
        mbEnterAsTab = .EnterAsTab
        msUserID = .UserId
        msBusinessDate = .BusinessDate
    End With

    'Get original position of command buttons.
    GetButtonPos

    mbUnload = False

    'Get Batch Type based on Task ID called from.
    mlBatchType = lGetBatchType()

    'Get program defaults.
    GetDefaults

    'Error when obtaining defaults
    If mbUnload Then Exit Sub

    'Set up form based on Batch Type.
    SetupBatchType

   'Set up Toolbar control.
    SetupToolbar
    
    'Intellisol start
    If moClass.moFramework.GetTaskID() = ktskInvoiceBatch And moClass.mbCalledFromPA Then
'        PAShowHelpIcon Me, tbrMain
    End If
    'Intellisol end

    'Initialize browse filter tracking
    miFilter = RSID_UNFILTERED
  
    'Determine if multi-currency is in use for the AR module.
    sSQL = "CompanyID = " & gsQuoted(msCompanyID)
    mbMCEnabledForAR = _
        gbGetValidBoolean(moClass.moAppDB.Lookup("UseMultCurr", "tarOptions", sSQL))
  
    'Set up Initial Grid Properties (Headers, Column Widths, Types Etc).
    FormatGrid

    'Bind Validation Manager Class.
    BindVM

    'Binding to Data Manager, Context Menu Class.
    BindForm
    BindContextMenu

    'If SO is not active then disable Select Shipments and Merge Invoices.
    If Not muAROptions.SOActive Then
        gDisableControls cmdSelectShipments
        gDisableControls cmdMerge
    End If
  
    'Get security level.
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDmForm)
    If miSecurityLevel = sotaTB_DISPLAYONLY Then
        tbrMain.ButtonEnabled(kTbNextNumber) = False
        gDisableControls cmdSelect, cmdSelectShipments, cmdTransactions, cmdBatchRgstrPost, cmdPrintInvoices
    End If
    
    'Initialize Control Properties.
    lblBatchOvrdSegValueDesc.Caption = Empty                'Clear out Description Label
  
    'Is this not a cr batch?
    If muCIBatchTypCompany.BatchType <> kBatchTypeARCR Then
        'Does user print invoices?
        If Not muAROptions.PrintInvoices Then
            gDisableControls cmdPrintInvoices
        End If
    End If


    'Setup Batch Override Segment controls.
    SetupBatchOvrdSegmentCtrls muAROptions.BatchOvrdSegKey, True

    'Get Local Text Values for placing into Batch Status textbox.
    With moBatchStatus
        'Add Batch Status flags.
        .Add gsLocStr(ksBSInUse, moClass), "1"
        .Add gsLocStr(ksBSOnHold, moClass), "2"
        .Add gsLocStr(ksBSOutOfBal, moClass), "3"
        .Add gsLocStr(ksBSBalanced, moClass), "4"
        .Add gsLocStr(ksBSPosting, moClass), "5"
        .Add gsLocStr(ksBSPosted, moClass), "6"
        .Add gsLocStr(ksBSInterrupted, moClass), "7"
        
        'Add Posting Status flags.
        .Add gsLocStr(ksBSOpen, moClass), "0"                ' This is usually associated with one of the above
        .Add gsLocStr(ksBSPreProcSt, moClass), "100"
        .Add gsLocStr(ksBSPreProcCom, moClass), "150"
        .Add gsLocStr(ksBSModPostSt, moClass), "200"
        .Add gsLocStr(ksBSModPostCom, moClass), "250"
        .Add gsLocStr(ksBSGLPostSt, moClass), "300"
        .Add gsLocStr(ksBSGLPostCom, moClass), "350"
        .Add gsLocStr(ksBSPosted, moClass), "500"
        .Add gsLocStr(ksBSDeleted, moClass), "999"
    End With
    
    'Map Search/Memo/Drill Down Buttons to form controls.
    MapControls

    'Start variance hidden.
    HideVariance True

    'Start deposit fields as disabled.
    gDisableControls txtDepSlipNumber, txtDepositDate
 
    mbCCActivated = moClass.moSysSession.IsModuleActivated(kModuleCC)


    'Find Single Batch if applicable.
    mbLoadBatch = False
    If bSingleBatch Then
        If Not bGetSingleBatch Then
            mbUnload = True
        Else
            mbLoadBatch = True
        End If
    End If

    'AvaTax Integration - FormLoad
    mbTrackSTaxOnSales = moClass.moAppDB.Lookup("TrackSTaxonSales", "tarOptions", "CompanyID = " & gsQuoted(msCompanyID))
    If mbTrackSTaxOnSales Then
        CheckAvaTax
    Else
        mbAvaTaxEnabled = False
    End If

    ' Add Avatax button to tool bar if AvaTax is enabled, but
    ' button will only be enabled when AvaTaxOnDemandEnabled is True
    If mbAvaTaxEnabled Then
        tbrMain.AddSeparator
        tbrMain.AddButton kTbAvalara
        tbrMain.AddSeparator
        tbrMain.ButtonEnabled(kTbAvalara) = False
    End If
    ' AvaTax end

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bGetSingleBatch() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bGetSingleBatch = True

    Select Case mlBatchType
        Case kBatchTypeARCO
            If Not bGetBatch(kBatchTypeARCO) Then
                giSotaMsgBox Me, moClass.moSysSession, kNoSlsCommBatch
                bGetSingleBatch = False
            End If

        Case kBatchTypeARFC
            If Not bGetBatch(kBatchTypeARFC) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgNoFinChgBatch
                bGetSingleBatch = False
            End If

        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kBadSingleBatchType, mlBatchType
            bGetSingleBatch = False

    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bGetSingleBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub Form_Paint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' this routine handles calling up a single batch.  this would be done at
    ' form load, but the validation manager does not support direct validation
    ' of a control by calling the .IsValidControl method.  A sendkeys "TAB"
    ' could be used, but to ensure the focus is on the txtBatchNo control, you
    ' would need to do a txtBatchNo.SetFocus -- which is not allowed in a
    ' form load.  Thus, we will check if the load batch flag has been set
    If mbLoadBatch Then
        mbLoadBatch = False
        gDisableControls txtBatchNo
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
'***********************************************************************
' Desc:  If the data has changed then Prompt to save changes
' Parms: Cancel     - Cancel shutdown of the form if set to 1
'        UnloadMode - Where form unload was called from
'***********************************************************************

   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then

        If moVM.IsValidDirtyCheck = 0 Then GoTo CancelShutDown
       
       'Check if user wants to save record if form is dirty
        If Not bConfirmUnloadSuccess(True) Then
            GoTo CancelShutDown
        End If

      'Check all other forms  that may have been loaded from this main form.
      'If there are any Visible forms, then this means the form is Active.
      'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
           'Most likely the user has requested to shut down the form.
            moClass.miShutDownRequester = kUnloadSelfShutDown
        End Select
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else 'kFrameworkShutDown
            'Do nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub

'An error occured cancel shutdown
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub PerformCleanShutDown()
'+++ VB/Rig Skip +++
'***********************************************************************
'Desc: Unload Child forms and AOF/Sage MAS 500 Objects
'***********************************************************************
    
On Error Resume Next

    
    'Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If

    'AvaTax Integration - PerformCleanShutDown
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    'AvaTax end

Exit Sub

ExpectedErrorRoutine:

End Sub
Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'Desc: Run Generic resizing code due to grid in form
'***********************************************************************

    'resize height
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, _
        grdBatchTran, fraBatchTran, fraTranGrid
           
    'resize Width
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, _
        grdBatchTran, fraBatchTran, fraTranGrid

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
'***********************************************************************
'Desc: Clean up data manager/other created Objects & Collections
'***********************************************************************
    
    On Error Resume Next

    If Not moDmForm Is Nothing Then     'Unload Form Object
        moDmForm.UnloadSelf
        Set moDmForm = Nothing
    End If
    
    If Not moDmGridBatchTran Is Nothing Then      'Unload grid object
        moDmGridBatchTran.UnloadSelf
        Set moDmGridBatchTran = Nothing
    End If

    If Not moDmCashTranLog Is Nothing Then
        moDmCashTranLog.UnloadSelf
        Set moDmCashTranLog = Nothing
    End If

    If Not moVM Is Nothing Then
        moVM.UnloadSelf
        Set moVM = Nothing
    End If
    
    Set moContextMenu = Nothing         'Context Menu Class
    Set moSotaObjects = Nothing         'Sage MAS 500 Objects
    Set moBatchStatus = Nothing         'batch Status collection
    Set moMapSrch = Nothing             'Search Mapped control collection
    Set moMapMemo = Nothing             'Memo Controls Collection
    
    Set moClass = Nothing               'form Class
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
    gClearSotaErr
    Resume Next
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub navBankAcct_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If moDmForm.State = kDmStateNone Then
        Exit Sub
    End If

    If Len(Trim(navBankAcct.Tag)) = 0 Then
        If Not gbLookupInit(navBankAcct, moClass, moClass.moAppDB, "BankAcct", "CompanyID = " & gsQuoted(msCompanyID)) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
        navBankAcct.Tag = "Init"
    End If

    gcLookupClick Me, navBankAcct, txtBankAcct, "CashAcctID"

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navBankAcct_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navBatchOvrdSegValue_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Pull Up G/L Override Segment Value Search Window
'***********************************************************************
    'Only Run Code if a valid key has been entered
    If (moDmForm.State = kDmStateEdit _
    Or moDmForm.State = kDmStateAdd) Then
        gcLookupClick Me, navBatchOvrdSegValue, txtBatchOvrdSegValue, "AcctSegValue"      'Display Search Window
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navBatchOvrdSegValue_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub navMain_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc:  Pull up search window for batches.
'***********************************************************************
    If Not gbGotFocus(Me, navMain) Then
        Exit Sub
    End If

    If Not bSetupNavMain Then
        Exit Sub
    End If

    If bConfirmUnloadSuccess(True) Then             'Ask user if they wish to save.
        mbNavMainClick = True
        gcLookupClick Me, navMain, txtBatchNo, "BatchNo"    'Show Batches Search window.
        mbNavMainClick = False

        If navMain.ReturnColumnValues.Count > 0 Then
            txtBatchNo = navMain.ReturnColumnValues("BatchNo")
            moVM.IsValidControl txtBatchNo, True
            
            'If active control was the batch comment when the navigator was pressed,
            'it will still be the active control after the record is loaded.  However,
            'if the commend field was high-lighted, when DM places the comment of the
            'new record in the field, it will overwrite and un-highlight the field.
            'This check makes sure the batch comment stays highlighted.
            If Me.ActiveControl Is txtBatchDesc Then
                txtBatchDesc_GotFocus
            End If
        End If
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navMain_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                mbNavMainClick = False
        End Select

        mbNavMainClick = False
'+++ VB/Rig End +++
End Sub

Private Function bIsValidBatchOvrdSegValue() As Boolean
'+++ VB/Rig Skip +++
'*******************************************************************************
' Desc: IsValidClassOvrdSegValue checks whether a Valid entry has been made
'       for the Segment Code.
'*******************************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim rs                  As DASRecordSet     'SOTADAS recordset Object
    Dim bValid              As Boolean          'Valid flag
    Dim bAllowAOF           As Boolean       'Allow Addon the fly flag
    Dim sBatchOvrdSegValue  As String        'Override segment string
    
    bIsValidBatchOvrdSegValue = True          'Initialize return Code
    
    
  'No Key was entered no need to process
    If Len(Trim(txtBatchOvrdSegValue)) = 0 Then
        lblBatchOvrdSegValueDesc = ""
        Exit Function
   End If
   
   sBatchOvrdSegValue = txtBatchOvrdSegValue  'Set up batch override Value into a string
   
  'run Segment validation (AOF If allowed)
   bValid = gbValidGLSegmentCode(moClass.moFramework, moSotaObjects, moClass.moAppDB, rs, _
                                  False, sBatchOvrdSegValue, muGLSegment.SegmentKey)
             
  'Invalid segment entered reset Value set return code to bad
  'reset Value set focus back to control
    If Not bValid Then
        bIsValidBatchOvrdSegValue = False
    Else
      'entry exists in the database or has been added on the fly.
        txtBatchOvrdSegValue = gvCheckNull(rs.Field("AcctSegValue"))
        lblBatchOvrdSegValueDesc = gvCheckNull(rs.Field("Description"))
        
    End If

  'clear out SOTADAS recordset object
    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bIsValidBatchOvrdSegValue"
    gClearSotaErr
    Exit Function

End Function
Public Function VMIsValidKey() As Boolean
'+++ VB/Rig Skip +++
'********************************************************************
'   Desc:   Checks for a valid Value in the batch form
'  Parms:   None
'  Returns: False - If invalid batch Key
'           True  - If Valid batch Key
'********************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim iKeyChangeCode      As Integer  'Key Change Return Code
    Dim sErrDesc            As String   'error text
    
  'Initialize function return
    VMIsValidKey = True

  'Set primary Key and run key change event
    moDmForm.SetColumnValue "BatchKey", muCIBatchLog.BatchKey

  'run batch id key change event
    mbDontChkClick = True
    iKeyChangeCode = moDmForm.KeyChange()
    mbDontChkClick = False

    'Check return code from key change
    Select Case iKeyChangeCode
        Case kDmKeyFound, kDmKeyNotFound    'Set up UI Valid key
            moClass.lUIActive = kChildObjectActive
            gDisableControls txtBatchNo       'Disable batch id
            If Not moDmCashTranLog Is Nothing Then
    
                moDmCashTranLog.SetColumnValue "CashTranKey", Val(moDmForm.GetColumnValue("CashTranKey"))
                moDmCashTranLog.KeyChange
                
            End If
            
            SetTBState moDmForm.State
                
        Case kDmKeyNotComplete             'Incomplete Key Bad batch
            VMIsValidKey = False

        Case kDmNotAllowed
            gEnableControls txtBatchNo
            txtBatchNo.Text = Empty
            bSetFocus txtBatchNo
            VMIsValidKey = False
        
        Case kDmError                      'Error Occurred Bad batch
            VMIsValidKey = False
        
        Case Else               'An Unknown return Code from key change code
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, iKeyChangeCode
            VMIsValidKey = False
    End Select

    Exit Function

'An error occurred
ExpectedErrorRoutine:
    sErrDesc = Err.Description
    
    mbDontChkClick = False
    VMIsValidKey = False
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, sErrDesc, "VMIsValidKey"
    gClearSotaErr
    Exit Function

End Function

Private Function bSetBatchInterrupted(lBatchKey As Long) As Boolean
'+++ VB/Rig Skip +++
    Dim sSQL        As String

    On Error GoTo ExpectedErrorRoutine

    bSetBatchInterrupted = False

'    sSQL = "UPDATE tciBatchLog SET Status = 7 WHERE BatchKey = " & lBatchKey
    sSQL = "#arSetBatchInterr;" & lBatchKey & ";"

    sSQL = moClass.moAppDB.BuildSQLString(sSQL)
    moClass.moAppDB.ExecuteSQL sSQL

    bSetBatchInterrupted = True

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgSetIntrFailed
    gClearSotaErr
    Exit Function
End Function


Public Function DMBeforeInsert(oDm As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Called from the data Manager within a database transaction for
'       InsertRow  Create a batch log system detail record
'   Return Values:
'       True    - success
'       False   - failure
'********************************************************************
    Dim lCashTranKey As Long
    
    DMBeforeInsert = False
    
  'Create a batch log system detail record
    Select Case True
        Case oDm Is moDmForm
           
          'Set up the segment key
            If muGLSegment.SegmentKey = 0 Then
                moDmForm.SetColumnValue "BatchOvrdSegKey", Empty
            Else
                If Len(Trim(txtBatchOvrdSegValue.Text)) = 0 Then
                    moDmForm.SetColumnValue "BatchOvrdSegKey", Empty
                Else
                    moDmForm.SetColumnValue "BatchOvrdSegKey", muGLSegment.SegmentKey
                End If
            End If
            
          'update override segment key and segment Value
            UpdateCIBatchLog

            'Intellisol start
            If moClass.mbCalledFromPA Then
                On Error Resume Next
                moClass.moAppDB.ExecuteSQL "INSERT INTO paarBatch VALUES (" & Trim(Str(glGetValidLong(CLng(moDmForm.GetColumnValue("BatchKey"))))) & ")"
                #If ERRORTRAPON Then
                    On Error GoTo VBRigErrorRoutine
                #End If
            End If
            'Intellisol end

        Case oDm Is moDmCashTranLog
            Debug.Print "Create CashTranLog"

            'Get the next Cash Tran Key.
            lCashTranKey = glGetNextSurrogateKey(moClass.moAppDB, "tcmCashTranLog")
            Do While glGetValidLong(moClass.moAppDB.Lookup("COUNT (1)", "tcmCashTranLog WITH (NOLOCK)", "CashTranKey = " & lCashTranKey)) > 0
                lCashTranKey = glGetNextSurrogateKey(moClass.moAppDB, "tcmCashTranLog")
            Loop
            
            moDmCashTranLog.SetColumnValue "CashTranKey", lCashTranKey
            moDmCashTranLog.SetColumnValue "TranAmtHC", 0

            'Get some of the values from the batch.
            If bCopyBatchValues() = False Then
                DMBeforeInsert = False
                Exit Function
            End If
    End Select
            
    DMBeforeInsert = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMBeforeInsert", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function DMBeforeUpdate(oDm As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Called from the data Manager within a database transaction for
'       UpdateRow
'       update Status in batch Log System detail
'   Return Values:
'       True    - success
'       False   - failure
'********************************************************************
    DMBeforeUpdate = False
    
    Select Case True
        Case oDm Is moDmForm
            'Set up the Segment Key.
            If muGLSegment.SegmentKey = 0 Then
                moDmForm.SetColumnValue "BatchOvrdSegKey", Empty
            Else
                If Len(Trim(txtBatchOvrdSegValue.Text)) = 0 Then
                    moDmForm.SetColumnValue "BatchOvrdSegKey", Empty
                Else
                    moDmForm.SetColumnValue "BatchOvrdSegKey", muGLSegment.SegmentKey
                End If
            End If

            DisplayBatchStatus     'Update Batch Status on the form.

            'Update the tciBatchLog Status.
            If iUpdateCIBatchLogSysDetl(miBatchLogSysDetlStatus, True) = kDmSuccess Then
                'Set Return Code.
                DMBeforeUpdate = True
              
                'Update Override Segment Key and Segment Value.
                UpdateCIBatchLog
            End If

        Case oDm Is moDmCashTranLog
            Debug.Print "Update CashTranLog"
            
            'Get some of the values from the batch.
            If bCopyBatchValues() = False Then
                'Error processing
                'An error message was already given to the user.
                DMBeforeUpdate = False
                Exit Function
            End If

            DMBeforeUpdate = True
        
        Case Else
             DMBeforeUpdate = True
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMBeforeUpdate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function CMAppendContextMenu(oCtl As Control, lMenu As Long) As Boolean
    CMAppendContextMenu = False

    If (oCtl Is txtBatchNo) And moDmForm.State <> kDmStateNone Then
        AppendMenu lMenu, MF_ENABLED, 21000, "Split Batch..."
        CMAppendContextMenu = True
    End If
End Function
Public Sub CMMenuSelected(oCtl As Control, lTaskID As Long)
    Dim oSplit      As Object
    Dim iHookType   As Integer

    Set oSplit = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                      kclsCISplitBatch, ktskCISplitBatch, _
                      kAOFRunFlags, kContextAOF)

    If Not (oSplit Is Nothing) Then
        muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))
        oSplit.SplitBatch gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER), muCIBatchLog.BatchID, mlBatchType
        Set oSplit = Nothing

        Me.SetFocus
        VMIsValidKey
    End If

End Sub
Public Function DMBeforeDelete(oDm As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Called from the data Manager within a database transaction for
'       DeleteRow.  Deletes CI batch Log record
'   Return Values:
'       True    - success
'       False   - failure
'********************************************************************
    DMBeforeDelete = False
    
    Select Case True
        Case oDm Is moDmForm
            'Delete CI batch Log records
            DMBeforeDelete = bDeleteBatchLog

            'Intellisol start
            If moClass.mbCalledFromPA Then
              If DMBeforeDelete Then
                On Error Resume Next
                gClearSotaErr
                moClass.moAppDB.ExecuteSQL "DELETE FROM paarBatch WHERE BatchKey = " & _
                  Trim(Str(glGetValidLong(CLng(moDmForm.GetColumnValue("BatchKey")))))
                If Err.Number <> 0 Then
                    DMBeforeDelete = False
                Else
                    DMBeforeDelete = True
                End If
                #If ERRORTRAPON Then
                    On Error GoTo VBRigErrorRoutine
                #End If
              End If
            End If
            'Intellisol end

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
    End Select
            
    DMBeforeDelete = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMBeforeDelete", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub DMReposition(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMReposition is called by the data Manager from within the
'   KeyChange method, after attempting to select a recordset from
'   the database.  By the time this subroutine is called, the
'   DataManager's state has changed to either kDmStateAdd or
'   kDmStateEdit.  This routine is called whether or not the
'   record exists (in either add or edit state).
'   Set Form tags, defaults and Status
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
    
    
    If oDm Is moDmForm Then
        'Reset old values
        chkBatchHold.Tag = 0
        curBatchCtrlAmt.Tag = 0
        txtBatchOvrdSegValue.Tag = Empty
        
        If mlBatchType = kBatchTypeARPA Or mlBatchType = kBatchTypeARRA Then
            RefreshTranGrid
        End If
        
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DMStateChange(oDm As Object, iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************************************
'   Description
'       DMStateChange is called by the data Manager anytime there is a change
'       in the state of the record because of a KeyChange or some Type of action
'       (i.e. Save, Cancel, Delete)
'       This project is using the state change to set the class's UI Active
'       property.  Because some controls are not bound to the data manager,
'       they must be cleared when the state changes to none.
'***************************************************************************

'Enable/Disable form buttons based on form state
    
    If oDm Is moDmCashTranLog Or oDm Is moDmForm Then
       
        Select Case iNewState
            Case kDmStateNone
                'Clear Controls that aren't cleared by the data manager
                ClearUnboundControls
                mbAutoNum = False
                'Enable next Number key if single batch
                If miSecurityLevel <> sotaTB_DISPLAYONLY Then
                    
                    tbrMain.ButtonEnabled(kTbNextNumber) = True
                    
                    ' make sure command buttons are enabled
                    gEnableControls cmdTransactions, cmdSelect
                    
                    ' if SO is active then enable select shipments
                    If muAROptions.SOActive Then
                        gEnableControls cmdSelectShipments
                    End If
                
                End If

                ' disable deposit information fields
                gDisableControls txtDepSlipNumber, txtDepositDate

                ' set all validated controls to valid state
                moVM.SetAllValid

                ' AvaTax Integration - DMStateChange - disable kTbAvalara
                If mbAvaTaxEnabled Then
                    tbrMain.ButtonEnabled(kTbAvalara) = False
                End If
                ' AvaTax end
           Case kDmStateAdd
                ' AvaTax Integration - DMStateChange - enable kTbAvalara
                If mbAvaTaxEnabled Then
                    tbrMain.ButtonEnabled(kTbAvalara) = True
                End If
                ' AvaTax end
            Case kDmStateEdit
                ' AvaTax Integration - DMStateChange - enable kTbAvalara
                If mbAvaTaxEnabled Then
                    tbrMain.ButtonEnabled(kTbAvalara) = True
                End If
                ' AvaTax end
                
                'Disable next Number key during editing of single batch
                If bSingleBatch() Then
                    tbrMain.ButtonEnabled(kTbNextNumber) = False
                End If

            Case Else
                'Disable next Number key during editing of single batch
                If bSingleBatch() Then
                    tbrMain.ButtonEnabled(kTbNextNumber) = False
                End If
                                
        End Select
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMStateChange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub DMDataDisplayed(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   The DMDataDisplayed is called by the data Manager from within the
'   KeyChange method, after refreshing the Form controls with values
'   from the record to be edited.  By the time this subroutine is called, the
'   DataManager's state has changed to either kDmStateAdd or
'   kDmStateEdit.  This routine is called only in the edit state.
'   Set Form tags and form Status
'   Parameters:
'       oDM <in>    - is the DataManager Object that caused the
'                     DMValidate to be called.
'********************************************************************
    If oDm Is moDmForm Then
        moVM.SetAllValid                        'All validation controls

        SetupHoldReason                         ' determine if hold reason should be enabled
        DetermineDepositFieldStatus             ' see if deposit fields should be enabled

        With muCMCashAcct
            .CashAcctKey = glGetValidLong(moDmForm.GetColumnValue("CashAcctKey"))
            .CashAcctID = txtBankAcct
            .CurrID = txtCashCurrID
            .AllowMCDep = (txtCashAllowMCDep = "1")
        End With
        
        If cmdPrintChecks.Visible Then
        
            If giGetValidInt(moClass.moAppDB.Lookup("PrintChks", "tcmCashAcct", "CashAcctKey = " & muCMCashAcct.CashAcctKey)) = 1 Then
                cmdPrintChecks.Enabled = True
            Else
                cmdPrintChecks.Enabled = False
            End If
            
        End If

        ' if dep slip# is blank, this batch was created in SP.
        ' fire off validation routine to load bank acct defaults
        ' (this will be moved into SP for 3.0)
        If txtDepSlipNumber.Visible And Len(Trim(txtDepSlipNumber)) = 0 Then
            bIsValidBankAcct
        End If

        If oDm.State = kDmStateEdit Then        'Set tags
            chkBatchHold.Tag = chkBatchHold     'Hold checkbox
            DisplayBatchStatus                  'Setup batch Status
            If muCIBatchLogSysDetl.Status = kvInterrupted And muCIBatchLogSysDetl.PostStatus > kvPreProc Then
                gDisableControls cmdTransactions, cmdSelect, cmdSelectShipments
            Else
                If miSecurityLevel <> sotaTB_DISPLAYONLY Then
                    
                    gEnableControls cmdTransactions, cmdSelect
                    
                    'if SO is active then enable select shipments
                    If muAROptions.SOActive Then
                        gEnableControls cmdSelectShipments
                    End If
                
                End If
            End If
            OwnerCheck                          ' determine status of 'Private' checkbox
        End If

    End If

    If oDm Is moDmCashTranLog Then
        moVM.SetAllValid                        'All validation controls

        Select Case moDmCashTranLog.State

            Case kDmStateAdd
                GetNextDepNumber                    ' get next dep # from default
                                                    ' bank account
                txtDepositDate = txtBatchPostDate   ' default deposit date to post date
                txtDepositDate.Tag = txtBatchPostDate ' keep valid value for ValMgr

        End Select
    End If
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
' Desc:  Bind Data Manager objects to form controls and to grid.
'*************************************************************************************
    Dim sLinkCriteria As String  'Link Criteria

    Set moDmForm = New clsDmForm   'New form data manger object

    'Bind controls to tarBatch table
    With moDmForm
        .Table = "tarBatch"
        .UniqueKey = "BatchKey"
        .SaveOrder = 1
        
        Set .Form = frmBatch
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        
        .Bind Nothing, "BatchKey", SQL_INTEGER
        .Bind Nothing, "CashAcctKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "BatchOvrdSegKey", SQL_INTEGER, kDmSetNull
        .Bind Nothing, "BankDepDate", SQL_CHAR, kDmSetNull
        .Bind txtBatchOvrdSegValue, "BatchOvrdSegValue", SQL_CHAR, kDmSetNull
        .Bind chkBatchHold, "Hold", SQL_SMALLINT
        .Bind txtBatchHoldReason, "HoldReason", SQL_CHAR
        .Bind txtBatchDesc, "BatchCmnt", SQL_CHAR
        .Bind Nothing, "OrigUserID", SQL_CHAR
        .Bind txtBatchPostDate, "PostDate", SQL_DATE
        .Bind chkBatchPrivate, "Private", SQL_SMALLINT
        .Bind curBatchCtrlAmt, "TranCtrl", SQL_DECIMAL
        .Bind Nothing, "UpdateCounter", SQL_INTEGER
        .Bind Nothing, "CashTranKey", SQL_INTEGER, kDmSetNull
      
        'Define Links
        sLinkCriteria = "SegmentKey=" & muAROptions.BatchOvrdSegKey
        sLinkCriteria = sLinkCriteria & " AND AcctSegValue=<<BatchOvrdSegValue>>"
        .LinkSource "tglSegmentCode", sLinkCriteria
        .Link lblBatchOvrdSegValueDesc, "Description"

        .LinkSource "tcmCashAcct", "CashAcctKey = <<CashAcctKey>>"
        .Link txtBankAcct, "CashAcctID"
        .Link txtCashCurrID, "CurrID"
        .Link txtCashAllowMCDep, "AllowMCDep"

        'Initialize Form
        .Init
    End With
    
    'Bind BatchTran grid for all batch types except AR Payment Applications.  ARPA batches
    'will not have any entries in tciBatchTran.  The data for ARPA will be in tarPendCustPmtAppl,
    'and that data needs to be rolled up for display so it cannot be bound.
    If (mlBatchType <> kBatchTypeARPA) And (mlBatchType <> kBatchTypeARRA) Then
    
        Set moDmGridBatchTran = New clsDmGrid  'Grid object for transaction grid
        
        'Bind grid to batch Transaction table
        With moDmGridBatchTran
            .Table = "tciBatchTran"
            
            Set .Session = moClass.moSysSession
            Set .Form = frmBatch
            Set .Parent = moDmForm
            Set .Grid = grdBatchTran
            
            .NoAppend = True
            
            .UIType = kDmUINone
            .UniqueKey = "BatchKey, TranType"
            
            'BindColumn statements.  Place these in the database column order.
            .BindColumn "TranType", kColTranTypeKey, SQL_INTEGER
            .BindColumn "TranCount", kColTranCount, SQL_INTEGER
            .BindColumn "TranTotal", kColTranTotal, SQL_DECIMAL
            
            'Pull in based on batch key from above.
            .ParentLink "BatchKey", "BatchKey", SQL_INTEGER
            
            'Pull in Transaction Type ID.
            .LinkSource "tciTranTypeCompany", "TranType=<<TranType>> AND CompanyID=" & gsQuoted(msCompanyID)
            .Link kColTranTypeID, "TranTypeID"
            
            'NOTE: If you do not have a control that determines what data is to be displayed
            'in the grid, then place your .Where and .Init methods here.
            'See cboUserFldDesc_Click event for format of these methods.
            .Init
        End With
        
    End If

    'If cash receipts batch and we integrate with CM, bind CashTranLog.
    If (mlBatchType = kBatchTypeARCR _
    And muAROptions.IntegrateCM) Then
        Set moDmCashTranLog = New clsDmForm
    
        With moDmCashTranLog
            .Table = "tcmCashTranLog"
            .CompanyID = msCompanyID
            
            Set .Session = moClass.moSysSession
            Set .Form = frmBatch
            Set .Parent = moDmForm
            
            .Bind Nothing, "CashTranKey", SQL_INTEGER
            .UniqueKey = "CashTranKey"
    
            .Bind txtDepSlipNumber, "TranNo", SQL_CHAR
            .Bind Nothing, "TranID", SQL_CHAR
            .Bind txtDepositDate, "TranDate", SQL_DATE
            .Bind Nothing, "CashAcctKey", SQL_INTEGER
            .Bind Nothing, "TranAmtHC", SQL_DECIMAL
            .Bind Nothing, "TranStatus", SQL_SMALLINT
            .Bind Nothing, "TranType", SQL_INTEGER
    
            .Init
        End With
    End If
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc: Bind Right Click Context menu
'***********************************************************************

    Set moContextMenu = New clsContextMenu  'Context Menu Class
    
    With moContextMenu
    
    'Add any Special .Bind or .BindGrid commands here for Drill Around or
    'Drill Down or for Grids - Here
    'Example:
    
        ' **PRESTO ** Set .Hook = WinHook2
        Set .Form = frmBatch
        .Bind "CMDA01", txtBankAcct.hwnd, kEntTypeCICashAcct
        If mlBatchType = kBatchTypeARIN Then
            .Bind "*APPEND", txtBatchNo.hwnd
        End If
        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub MapControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    
   'Map Account Override Search button to it's associated text box
    giCollectionAdd moMapSrch, navBatchOvrdSegValue, txtBatchOvrdSegValue.hwnd
    giCollectionAdd moMapSrch, navBankAcct, txtBankAcct.hwnd
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBankAcct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBankAcct, True
    #End If
'+++ End Customizer Code Push +++

    moVM.IsValidControl txtBankAcct

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBankAcct_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatchNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    ' check non-control keypresses
    If KeyAscii > 31 Then
        ' only allow numeric values
        If KeyAscii < Asc("0") Or KeyAscii > Asc("9") Then
            KeyAscii = 0
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBatchNo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatchNo, True
    #End If
'+++ End Customizer Code Push +++
    'Intellisol start
    If mbNavMainClick Then
        Exit Sub
    End If
    'Intellisol end

    ' RMM 41057, Part of fix
    mbBatchExists = False
    
    moVM.IsValidControl txtBatchNo

        ' RMM 41057, This task writes out batch header and goes immediately into edit mode. Causing DM to fire OnEdit event into
        ' Form Customizer. The Form Customizer OnNew event is never fired. So here we fire the OnNew event if a new batch number
        ' has been entered. This is a patch; better solution is to not save batch header until header/detail lines entered or save
        ' clicked.
#If CUSTOMIZER Then
        If (Not mbBatchExists) And (Not Me.ActiveControl Is navMain) Then
            If Not moFormCust Is Nothing Then
                moFormCust.OnNew moDmForm
            End If
        End If
#End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBatchNo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lGetNextBatchNo() As Long
'+++ VB/Rig Skip +++
'**************************************************************************
' Desc: lGetNextBatchNo returns the new batch Number after updating
'       the CI BatchLog with the new record.
' Returns: Next New batch Number (0 if an error occurred)
'**************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim lNextBatchNo    As Long      'Next Valid batch Number
    Dim iRetVal         As Integer   'return Code
    Dim bInTrans        As Boolean   'In transaction
    Dim lNextBatchKey   As Long      'Key for next valid batch Number
    Dim sErrDesc        As String    'error code

    lGetNextBatchNo = 0       'initialize next batch Number
    
  'run procedure to create batch Number
    With moClass.moAppDB
      .SetInParam msCompanyID                       'Company Id
      .SetInParam CInt(kModuleAR)                   'Module No
      .SetInParam muCIBatchTypCompany.BatchType     'batch Type
      .SetInParam msUserID                          'zuma user id
      .SetInParam muCIBatchTypCompany.DfltBatchCmnt 'Default batch Comment
      .SetInParam gsFormatDateToDB(Format(sbrMain.BusinessDate, "SHORT DATE"))
      .SetOutParam lNextBatchKey
      .SetOutParam lNextBatchNo                     'Next batch Number
      .SetOutParam iRetVal                          'return code
      .BeginTrans                                   'Start transaction
      bInTrans = True                               'set flag
      .ExecuteSP ("spGetNextBatch")                 'Create next open A/R batch
      lNextBatchKey = .GetOutParam(7)
      lNextBatchNo = .GetOutParam(8)                'get batch Number
      iRetVal = .GetOutParam(9)                     'get return code
      .ReleaseParams                                'release parameters
      .CommitTrans                                  'Commit transaction
      bInTrans = False                              'reset flag
    End With
    
    'Intellisol start
      ' Insert a flag into paarBatch if this is a new invoice #
      If moClass.mbCalledFromPA Then
        If iRetVal = 1 Then
            On Error Resume Next
            moClass.moAppDB.ExecuteSQL "INSERT INTO paarBatch VALUES (" & _
              Trim(Str(lNextBatchKey)) & ")"
            On Error GoTo ExpectedErrorRoutine
        End If
      End If
    'Intellisol end

    'check return Value
    Select Case iRetVal
        
        Case 0  'unknown stored procedure error
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                    "0  spGetNextBatch"
    
        Case 1  'Good Number Found
            lGetNextBatchNo = lNextBatchNo
            mbAutoNum = True
    
        Case 2  'all numbers have been used
            giSotaMsgBox Me, moClass.moSysSession, kmsgNoBatchNumbersLeft
    
        Case 3  'No batch Type Company record found
            giSotaMsgBox Me, moClass.moSysSession, kmsgCreateBatchControlRecord, _
                Str$(muCIBatchTypCompany.BatchType) & " (" & Me.Caption & ")"
            mbUnload = True
        
        Case Else 'unknown stored procedure error
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                        iRetVal & " spGetNextBatch"
    
    End Select

    If lGetNextBatchNo = 0 Then
        mbUnload = True
    End If

Exit Function
ExpectedErrorRoutine:
    '--- ROLL BACK TRANSACTION ---
    sErrDesc = Err.Description

    If bInTrans Then
        moClass.moAppDB.Rollback
    End If
    mbUnload = True

    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, sErrDesc, "lGetNextBatchNo"
    gClearSotaErr
    Exit Function

End Function

Public Sub UpdateCIBatchLog()
'+++ VB/Rig Skip +++
'**************************************************************************
'   Description:  update batch Log with Override key and Value
'**************************************************************************
    Dim sSegVal     As String   'Segment Value
    Dim vSegKey     As Variant  'Segment key
    Dim lBatchKey   As Long     'batch key
    Dim sSQL        As String   'SQL String

    On Error GoTo ExpectedErrorRoutine
        
  'batch Override key set to null if not there
    If Len(moDmForm.GetColumnValue("BatchOvrdSegKey")) = 0 Or _
       IsEmpty(moDmForm.GetColumnValue("BatchOvrdSegKey")) Or _
       IsNull(moDmForm.GetColumnValue("BatchOvrdSegKey")) Then
       
        vSegKey = "Null"
    Else
        If Len(Trim(txtBatchOvrdSegValue.Text)) = 0 Then
            vSegKey = Null
        Else
            vSegKey = moDmForm.GetColumnValue("BatchOvrdSegKey")
        End If
            
    End If
    
  'batch Override Value set to null if not there
    If Len(Trim(txtBatchOvrdSegValue)) = 0 Then
        sSegVal = "Null"
    Else
        sSegVal = gsQuoted(txtBatchOvrdSegValue)
    End If
    
  'retrieve batch Key
    lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)
    
  'update batch Log
    sSQL = "UPDATE tciBatchLog SET BatchOvrdSegValue = Null, BatchOvrdSegKey = Null WHERE BatchKey = " & lBatchKey
    
    moClass.moAppDB.ExecuteSQL sSQL

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "UpdateCIBatchLog"
    gClearSotaErr
    Exit Sub

End Sub

Public Function iUpdateCIBatchLogSysDetl(iStatus As Integer, bOutsidetran As Boolean) As Integer
'+++ VB/Rig Skip +++
'**********************************************************************************
' Desc:    update Status in tciBatchLog record for the batch key.  This should be
'          done in the same transaction as the module batch.
' Parms:   Status          - Status to set batch Sys detail to (1-7)
'          iAddToUserCount - Amount To Add To User Count 0, 1 or -1
'          bOutsidetran    - Outside transaction dont run your own
' Returns: kdmSuccess - Successful update
'          kDmError   - Unsuccessful update
'*******************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim bInTrans     As Boolean   'In transaction
    Dim lBatchKey    As Long      'batch Key
    Dim iRetVal      As Integer
    Dim iStatusOut   As Integer
    Dim sSQL         As String
    
    iRetVal = 0
    iStatusOut = 0
    
    iUpdateCIBatchLogSysDetl = kDmFailure       'initialize for failure

  'Get current batch Key
    lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)
    iStatus = gvCheckNull(iStatus, SQL_INTEGER)
  
  'run procedure to create batch Number
    With moClass.moAppDB
      .SetInParam lBatchKey                     'batch Key
      .SetInParam iStatus                       'Status you want to set it to
      .SetOutParam iStatusOut                   'Status Out
      .SetOutParam iRetVal                      'return code
      If Not bOutsidetran Then                  'Don't Setup transaction if one is running
          .BeginTrans                           'Start transaction
          bInTrans = True                       'set flag
      End If
      .ExecuteSP ("spUpdateBatchStatus")        'update batch Log posting Status
      iStatusOut = .GetOutParam(3)              'get Status Was set to
      iRetVal = .GetOutParam(4)                 'get return code
      .ReleaseParams                            'release parameters
      If Not bOutsidetran Then                  'Don't Setup transaction if one is running
          .CommitTrans                          'Commit transaction
          bInTrans = False                      'reset flag
      End If
    End With
    
  
  'Set up flags to send message outside of transaction
    If iRetVal = 1 Then
        iUpdateCIBatchLogSysDetl = kDmSuccess   'Good return
    Else
        If bOutsidetran Then                    'In an outside transaction
            moDmForm.CancelAction               'Roll Back Transaction
        End If
        SysDetailErrorMsg iRetVal, iStatusOut   'Send message
    End If
    
Exit Function
ExpectedErrorRoutine:
    '--- ROLL BACK TRANSACTION ---
       
    If bInTrans Then
        moClass.moAppDB.Rollback
    End If
    
    If Not bOutsidetran Then   'Send message if not in transaction
        moDmForm.CancelAction   'Roll Back Transaction
    End If
    SysDetailErrorMsg iRetVal, iStatusOut
    gClearSotaErr
    Exit Function

End Function

Public Function bCanSaveBatch() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************************
' Desc:    Checks if can Save ar batch for the batch key.
'          Based on the batch Status
' Returns: kdmSuccess - Can update
'          kDmError   - May Not update (posting, posted or Deleted)
'*******************************************************************************
    Dim oStatus     As Integer   'Status
    
    bCanSaveBatch = kDmFailure       'initialize next batch Number
    
    If moDmForm.State <> kDmStateEdit And moDmForm.State <> kDmStateAdd Then Exit Function

    ' make sure all fields are valid
    If moVM.IsValidDirtyCheck = 0 Then Exit Function

    ' if batch is exclusively locked, we can't update it
    If bBatchLocked(kLockTypeExclusive) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchExclLock
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    If Not bGetBatchStatus(oStatus) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
  
  'Check return Value
    If oStatus = kvPosting Then   'posting
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchBadStatus, _
                sGetBatchStatusText(oStatus)
    Else
        bCanSaveBatch = kDmSuccess    'Good
    End If
              
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCanSaveBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bGetBatchStatus(iStatus As Integer) As Boolean
'+++ VB/Rig Skip +++
'**********************************************************************************
' Desc:     Get batch Status.  Return true and batch Status if successful
'           Otherwise, return false
'*******************************************************************************
    Dim lBatchKey    As Long      'batch Key
    Dim iRetVal      As Integer   'Return Value

    On Error GoTo ExpectedErrorRoutine

    bGetBatchStatus = False
    
    If moDmForm.State <> kDmStateEdit And moDmForm.State <> kDmStateAdd Then Exit Function
    
  'Get current batch Key
    lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)
  
  'run procedure to Check batch Status
    With moClass.moAppDB
      .SetInParam lBatchKey                    'batch Key
      .SetOutParam iStatus                     'Status Out
      .SetOutParam iRetVal                     'return code
      .ExecuteSP ("spGetBatchStatus")          'get batch Log System detail
      iStatus = .GetOutParam(2)                'get Status Was set to
      iRetVal = .GetOutParam(3)                'get return code
      .ReleaseParams                           'release parameters
    End With

  'Check return Value
    Select Case iRetVal
        
        Case 0  'unknown stored procedure error
        
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                    "0  spGetBatchStatus"
        
        Case 1  'Good Stored procedure return
        
            bGetBatchStatus = True

            ' if batch is in use, make sure at least one person is using it
            If iStatus = kvInUse Then
                If Not bBatchLocked Then
                    ' no one is using batch
                    iStatus = iGetFormBatchStatus
                End If
            End If

        Case 2  'No batch Log record

            giSotaMsgBox Me, moClass.moSysSession, kNoBatchLog
        
        Case Else 'unknown stored procedure error
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                        iRetVal & " spGetBatchStatus"
    
    End Select

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bGetBatchStatus"
    gClearSotaErr
    Exit Function

End Function

Private Sub SysDetailErrorMsg(iRetVal As Integer, iStatusOut As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************************
' Desc:    Sends message to user about Value of return from update
'          tciBatchLog.Status stored procedure
'*******************************************************************************
  
  'Check return Value
    Select Case iRetVal
        
        Case 0  'unknown stored procedure error
        
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                    "0  spUpdateBatchLogSysDetail"
        
        Case 1  'No error Good return code
        
        Case 2  'No batch Log record
                
            giSotaMsgBox Me, moClass.moSysSession, kmsgRecDeletedByAnother, _
                gsLocStr(kBatch, moClass)
                
        Case 3  'No batch Type Company record found
            giSotaMsgBox Me, moClass.moSysSession, kmsgBatchBadStatus, _
                    sGetBatchStatusText(iStatusOut)
        
        Case Else 'unknown stored procedure error
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                        iRetVal & " spUpdateBatchLogSysDetail"
    
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SysDetailErrorMsg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function sGetBatchStatusText(iBatchStatus As Integer) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:    Returns local text for batch Status fom database or
'          From Status Collection if posted (Already have tcibatch Status)
' Parms:   iBatchStatus - Database Value for Status
' returns: Local Text For batch Status from tciBatchLog
'*************************************************************************

    sGetBatchStatusText = moBatchStatus(Trim(Str$(iBatchStatus)))

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetBatchStatusText", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Desc:   Formats the grid for detail entry - most of this code will
'           go away as soon as we can do design-mode formatting.
'***********************************************************************
    gGridSetProperties grdBatchTran, kMaxCols, kGridLineEntry
    
    grdBatchTran.redraw = False
    
    gGridSetColors grdBatchTran         ' set grid colors to match system colors
    
    'Define maximum rows and columns - temporary until interface designer works
    
    If mlBatchType = kBatchTypeARPA Or mlBatchType = kBatchTypeARRA Then
        gGridSetMaxRows grdBatchTran, 3
    Else
        gGridSetMaxRows grdBatchTran, 1
    End If

    'Hide the Selector Column
    gGridHideColumn grdBatchTran, 0
    
    'Set column headers  - temporary until interface designer works
    gGridSetHeader grdBatchTran, kColTranTypeID, gsLocStr(ksTranTypeID, moClass)
    gGridSetHeader grdBatchTran, kColTranCount, gsLocStr(ksBatchCount, moClass)
    gGridSetHeader grdBatchTran, kColTranTotal, gsLocStr(ksBatchAmt, moClass)
    
    'Set column widths   - temporary until interface designer works
    gGridSetColumnWidth grdBatchTran, kColTranTypeID, 9.125
    gGridSetColumnWidth grdBatchTran, kColTranCount, 6.625
    gGridSetColumnWidth grdBatchTran, kColTranTotal, 12.375
    
    'Set column types    - temporary until interface designer works
    gGridSetColumnType grdBatchTran, kColTranTypeKey, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdBatchTran, kColTranTypeID, SS_CELL_TYPE_STATIC_TEXT
    gGridSetColumnType grdBatchTran, kColTranCount, SS_CELL_TYPE_INTEGER
    
    gGridHideColumn grdBatchTran, kColTranTypeKey
    
    With grdBatchTran
        'Set Float Type for tran Total Column
        .Col = kColTranTotal
        .Row = -1
        .TypeFloatSeparator = True
        .TypeFloatMax = "999999999999"
        .TypeFloatDecimalPlaces = muHomeCurrInfo.iDecPlaces
        
        'Center the tran Count column
        .Col = kColTranCount
        .Row = -1
        .Position = SS_POSITION_CENTER_CENTER
        
        'Center the tran Type ID column
        .Col = kColTranTypeID
        .Row = -1
        .Position = SS_POSITION_CENTER_CENTER
        
        .redraw = True
    End With
    
    
    If mlBatchType = kBatchTypeARPA Then
    
        'ARPA batches have three fixed tran type rows
        gGridUpdateCellText grdBatchTran, 1, kColTranTypeID, "Applications"
        gGridUpdateCellText grdBatchTran, 2, kColTranTypeID, "Discounts"
        gGridUpdateCellText grdBatchTran, 3, kColTranTypeID, "Write-Offs"
        
        'Do not show amount column for ARPA batches when MultiCurrency is being used by AR.
        'This is because the amounts are in the naturual currency of the source document, and
        'summing them up may not make sense if different currencies are involved.
        If mbMCEnabledForAR Then
            gGridHideColumn grdBatchTran, kColTranTotal
            gGridSetMaxCols grdBatchTran, kMaxCols - 1
        End If
    
    End If
    
    If mlBatchType = kBatchTypeARRA Then
    
        'ARPA batches have three fixed tran type rows
        gGridUpdateCellText grdBatchTran, 1, kColTranTypeID, "Invoices"
        gGridUpdateCellText grdBatchTran, 2, kColTranTypeID, "Payments"
        gGridUpdateCellText grdBatchTran, 3, kColTranTypeID, "Memos"

        If mbMCEnabledForAR Then
            gGridHideColumn grdBatchTran, kColTranTotal
            gGridSetMaxCols grdBatchTran, kMaxCols - 1
        End If

    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function lGetBatchType() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*****************************************************************************
' Desc:
'       return batch type based on Task ID
'*****************************************************************************

    Dim lTaskNumber As Long   'Task User Called Sub From

    lTaskNumber = moClass.moFramework.GetTaskID()  'Get Task ID for program
    
    Select Case lTaskNumber
        
        Case ktskInvoiceBatch, ktskPAProcessInvoices            'Enter Invoices
            
            lGetBatchType = kBatchTypeARIN
                                       
        Case ktskCashRcptBatch          'Enter Cash Receipts
            
            lGetBatchType = kBatchTypeARCR

        Case ktskAppMemoPmtBatch        'Memo/Payment Applications
            
            lGetBatchType = kBatchTypeARPA

        Case ktskSalesCommBatch         'Sales Commissions
            
            lGetBatchType = kBatchTypeARCO

        Case ktskFinChgBatch            'Finance charges

            lGetBatchType = kBatchTypeARFC

        Case ktskARReverseApplBatch
            
            lGetBatchType = kBatchTypeARRA   'Reverse Application

        Case Else                       'Incorrect Task Number
            
          'Send Message to the user
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedOperation, _
                         lTaskNumber
    
    End Select
    

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lGetBatchType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub HideDepositFields()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lblDepSlipNumber.Visible = False
    txtDepSlipNumber.Visible = False
    lblDepositDate.Visible = False
    txtDepositDate.Visible = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideDepositFields", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ShowDepositFields()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lblDepSlipNumber.Visible = True
    txtDepSlipNumber.Visible = True
    lblDepositDate.Visible = True
    txtDepositDate.Visible = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowDepositFields", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub txtBatchDesc_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatchDesc, True
    #End If
'+++ End Customizer Code Push +++
    txtBatchDesc.SetSel 0, Len(txtBatchDesc)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBatchDesc_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchOvrdSegValue_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatchOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Validate batch Override Segment Value
'************************************************************************************
    moVM.IsValidControl txtBatchOvrdSegValue
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBatchOvrdSegValue_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bConfirmUnloadSuccess(Optional vclear As Variant) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
' Desc:    Successful confirmation of unload (true if form is not dirty)
' Returns: true - successful confirmation of unload
'          false - unsuccessful confirm unload
'************************************************************************************
    
    Dim iConfirmUnload   'Confirm unload return code
    
    bConfirmUnloadSuccess = False                   'set initial return

    If miSecurityLevel = sotaTB_DISPLAYONLY Then
        iConfirmUnload = kDmSuccess
    Else
        iConfirmUnload = moDmForm.ConfirmUnload(True)   'run data manger confirm unload
    End If
    
    Select Case iConfirmUnload
        Case kDmSuccess                             'Yes & successful save, no or form not dirty
            
            bConfirmUnloadSuccess = True            'Set return code
            
            If IsMissing(vclear) Then
                If moDmForm.State <> kDmStateNone Then  'clear form
                    mbDontChkClick = True
                    moDmForm.Clear True
                    mbDontChkClick = False
                    ClearUnboundControls
                End If
            End If
        Case kDmFailure, kDmError               'Unsuccessful or an error occured
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        
        Case Else                               'Unknown return code
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, _
                iConfirmUnload
    
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bConfirmUnloadSuccess", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub HandleToolBarClick(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sButton) Then
            Exit Sub
        End If
    End If
#End If
'**************************************************************************
' Desc: tlbMaint is a control array for the user interface command buttons
'       that determine what to do with the current record being maintained
'       or viewed.
'   Parameters:
'       index <in> - which command button was pressed.  The choices are
'                    Finish/OK, Cancel, Delete.  Depending on the state
'                    of the form, some of these buttons may be Disabled
'                    or invisible.
'**************************************************************************
    Dim iConfirmUnload  As Integer
    'Fixing scopus # 20169, change the variable count datetype to long
    'Dim oCount          As Integer      ' count of records from sp
    'Dim oRetVal         As Integer      ' return Value from sp
    Dim lCount          As Long         ' count of records from sp
    Dim iRetVal         As Integer      ' return Value from sp
    Dim iResp           As Integer      ' Msg Box return Value
    Dim lBatchKey       As Long         ' current batch key
    Dim bSkipConfirm    As Boolean      ' skip DM delete confirmation?
    Dim iDelResult      As Integer      ' result of delete
    Dim iMouse          As Integer      ' save/restore mouse pointer
    
    Select Case sButton

        Case kTbFinish, kTbFinishExit

            If moVM.IsValidDirtyCheck = 0 Then Exit Sub

            If bCanSaveBatch() Then
                Select Case moDmForm.Action(kDmFinish)
                    Case kDmSuccess     'Successful update
                        If sButton = kTbFinish Then
                            moClass.lUIActive = kChildObjectInactive
                            
                            'have to clear moDmCashTranLog manually
                            'because it is not a child of moDmForm
                            If mlBatchType = kBatchTypeARCR And muAROptions.IntegrateCM Then
                                With moDmCashTranLog
                                    .SetColumnValue ("TranID"), ""
                                    .SetColumnValue ("CashAcctKey"), ""
                                    .SetColumnValue ("TranAmtHC"), ""
                                    .SetColumnValue ("TranStatus"), ""
                                    .SetColumnValue ("TranType"), ""
                                    .SetColumnValue ("CashTranKey"), ""
                                End With
                            End If
                        Else
                            moClass.miShutDownRequester = kUnloadSelfShutDown
                            Unload Me
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                            Exit Sub
                        End If

                    Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                
                End Select
            
            End If
        
        Case kTbSave

            If bCanSaveBatch() Then
                
                If bSaveBatch() Then
                    moClass.lUIActive = kChildObjectInactive
                End If
                Exit Sub
            
            End If
        
        Case kTbCancel, kTbCancelExit 'Cancel button pressed
         
            Select Case moDmForm.Action(kDmCancel)
                Case kDmSuccess
                    If Not moDmCashTranLog Is Nothing Then
                        moDmCashTranLog.Action kDmCancel
                    End If
                    If sButton = kTbCancel Then
                        moClass.lUIActive = kChildObjectInactive
                    Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        Unload Me
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If


                Case kDmFailure, kDmError
                    'Exit Sub
            End Select
            
        Case kTbDelete 'Delete button pressed

            If Not bCanDelete() Then

                giSotaMsgBox Me, moClass.moSysSession, kNoBatchDelete
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub

            End If
            
             ' Get current batch Key
            lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)
            
            If bSystemChecksPrinted(lBatchKey) Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgPrintedSystemChecks
                Exit Sub
            End If

           
        
            ' Check if this batch has been used in other tables
            ' (we don't want RI to automatically do a cascading
            '  delete without user confirmation!)
            With moClass.moAppDB
                .SetInParam lBatchKey               ' batch key
                .SetInParam mlBatchType             ' batch Type
                .SetOutParam lCount                 ' return # of transactions using batch key
                .SetOutParam iRetVal                ' return Status
                .ExecuteSP "spGetOtherBatchUse"     ' check for other use of batch key
                lCount = .GetOutParam(3)
                iRetVal = .GetOutParam(4)
                .ReleaseParams
            End With

            bSkipConfirm = False
            If iRetVal = 0 Then
                ' couldn't get status of batch usage -- this is not good so we
                ' won't allow the delete
                giSotaMsgBox Me, moClass.moSysSession, kNoBatchUsage
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Sub
            Else
                ' if transactions exist, confirm the delete here
                If lCount > 0 Then
                    ' make sure transactions grid reflects the most current
                    ' status of batch totals
                    RefreshTranGrid

                    iResp = giSotaMsgBox(Me, moClass.moSysSession, kConfirmBatchDel)
                    If iResp = vbNo Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Sub
                    End If
                    ' user has confirmed delete, so we tell data manager to skip
                    ' it's confirmation
                    bSkipConfirm = True
                End If
            End If
            
            'Intellisol start
            moClass.moAppDB.ExecuteSQL "IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id = object_id('tempdb..#STaxKeys')) DROP TABLE #STaxKeys"
            'Intellisol end

            ' create temp table outside of transaction
            moClass.moAppDB.ExecuteSQL "CREATE TABLE #STaxKeys (STaxTranKey INTEGER NULL)"

            iMouse = Me.MousePointer                        ' save mouse pointer
            Me.MousePointer = vbHourglass                   ' make it busy

            ' AvaTax Integration - HandleTooBarClick - kTbDelete
            If mbAvaTaxEnabled And mlBatchType = kBatchTypeARIN Then
                moAvaTax.CancelTax kModuleAR, 0, 0, lBatchKey
            End If
            ' AvaTax Integration end

            iDelResult = moDmForm.DeleteRow(bSkipConfirm)   ' delete the row
            Me.MousePointer = iMouse                        ' restore mouse pointer

            ' drop temp table
            moClass.moAppDB.ExecuteSQL "DROP TABLE #STaxKeys"

            Select Case iDelResult
                Case kDmSuccess
                    moClass.lUIActive = kChildObjectInactive
                    If bSingleBatch Then
                        SetTBState kDmStateNone
                    End If

                Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
            End Select

        Case kTbNextNumber

'            If moVM.IsValidDirtyCheck = 0 Then Exit Sub

'            If mlBatchType = kBatchTypeARCO Then Exit Sub

            If moDmForm.State <> kDmStateNone Then
                If moVM.IsValidDirtyCheck = 0 Then
                    Exit Sub
                End If
            End If
            
            iConfirmUnload = moDmForm.ConfirmUnload()
            
            If iConfirmUnload = kDmSuccess Then
                moDmForm.Clear True
                ClearUnboundControls
                GetNewBatch               'Get Next Customer Number
            End If
            
            gbSetFocus Me, txtBatchDesc

            ' RMM 41057, This task writes out batch header and goes immediately into edit mode. Causing DM to fire OnEdit event into
            ' Form Customizer. The Form Customizer OnNew event is never fired. So here we fire the OnNew event if a new batch number
            ' has been entered. This is a patch; better solution is to not save batch header until header/detail lines entered or save
            ' clicked. DoEvents required to process LostFocus event prior to OnNew being executed
            DoEvents
#If CUSTOMIZER Then
            If Not moFormCust Is Nothing Then
                moFormCust.OnNew moDmForm
            End If
#End If
        
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            'Make sure navigator has been initialized.
            If Not bSetupNavMain Then
                Exit Sub
            End If

            'Do lost focus processing.
            If moDmForm.State <> kDmStateNone Then
                If moVM.IsValidDirtyCheck = 0 Then
                    Exit Sub
                End If
            End If
        
            'Confirm the browse if record has changed.
            If bConfirmUnloadSuccess() Then
                iConfirmUnload = kDmSuccess
            Else
                iConfirmUnload = kDmError
            End If

            Select Case iConfirmUnload
                Case kDmSuccess
                    'Do Nothing
                Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
                Case Else
                    giSotaMsgBox Me, moClass.moSysSession, _
                        kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
            End Select

            BrowseBatch sButton
        
        Case kTbFilter
            miFilter = IIf(miFilter = RSID_UNFILTERED, RSID_FILTERED, RSID_UNFILTERED)

        'AvaTax Integration - ToolbarClick - kTbAvaTax "AvaTax ReCalculate Taxes"
        Case kTbAvalara
            SetHourglass True
            Dim lRecordCount As Long
            lRecordCount = glGetValidLong(moClass.moAppDB.Lookup("COUNT (*)", "tarPendInvoice", "BatchKey = " & moDmForm.GetColumnValue("BatchKey")))
            If Not Len(txtBatchNo) = 0 And lRecordCount > 0 Then
                moAvaTax.RecalculateTaxes moDmForm.GetColumnValue("BatchKey"), txtBatchNo.Tag
                RefreshTranGrid
            End If
            SetHourglass False
        'AvaTax end

        Case Else
            'New Command Button not programmed for
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedCtrlArryIndex
            
'        'Intellisol start
'        Case PAGetHelpButton()
'            If moClass.moFramework.GetTaskID() = ktskInvoiceBatch And moClass.mbCalledFromPA Then
'                PAShowFormHelp Me, kPAHelpProcessInvoices
'            End If
'        'Intellisol end
    
    End Select
  
    DisplayBatchStatus
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bSystemChecksPrinted(lBatchKey As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iPrinted As Integer
    
    bSystemChecksPrinted = False
    
    iPrinted = giGetValidInt(moClass.moAppDB.Lookup("Count(*)", "tarPendCustPmt", "BatchKey = " & CStr(lBatchKey) _
             & " AND Printed = 1 AND ManualChk = 0"))
    
    If iPrinted > 0 Then
        bSystemChecksPrinted = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bAnyChecksPrinted", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub SetupBatchOvrdSegmentCtrls(lSegmentKey As Long, Optional ByVal vInitialSetup As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************************
' Desc: The navBatchOvrdSeg, txtBatchOvrdSegValue mask and lblBatchOvrdSegValue
'       will be setup if the segments are different.
'       Assumption that  the GLSegment Type contains the last Segment used for a
'       batch and this Type will initially contain the AR Options information.
'       For a new record, the segment in aroptions will be used.  Because of this,
'       the default mask and Description are stored in AROptions.BatchOvrdSegMask
'       and AROptions.BatchOvrdSegDesc, respectively.
'***********************************************************************************
    Dim sRestrictClause As String
    
    If lSegmentKey > 0 Then                 'Segment Key is present
        If muGLSegment.SegmentKey <> lSegmentKey Then     'Segment Key was changed
            If Not IsMissing(vInitialSetup) Then          'Not initial setup
                If CBool(vInitialSetup) Then              'Initial Setup
                    'Get Segment Information and load it into ar options structure.
                    GLGetSegment moClass.moAppDB, muGLSegment, muAROptions.BatchOvrdSegKey
                    muAROptions.BatchOvrdSegMask = muGLSegment.Mask
                    muAROptions.BatchOvrdSegDesc = muGLSegment.Description
                End If
                
                lblBatchOvrdSegValue = Empty   'Clear out segment label
            ElseIf lSegmentKey = muAROptions.BatchOvrdSegKey Then  'Segment same as options
                'Load segment from options structure.
                muGLSegment.SegmentKey = muAROptions.BatchOvrdSegKey
                muGLSegment.Mask = muAROptions.BatchOvrdSegMask
                muGLSegment.Description = muAROptions.BatchOvrdSegDesc
            Else
                'Load segment with new information.
                GLGetSegment moClass.moAppDB, muGLSegment, lSegmentKey
            End If
            
            'Set up Navigator, Label text and Segment mask
            'Set up restrict clause for navigator.
            sRestrictClause = "SegmentKey = " & Format$(muGLSegment.SegmentKey)
            navBatchOvrdSegValue.Enabled = gbLookupInit(navBatchOvrdSegValue, moClass, moClass.moAppDB, "SegmentCode", sRestrictClause)

            lblBatchOvrdSegValue = muGLSegment.Description  'Load Override Label
            txtBatchOvrdSegValue.Mask = muGLSegment.Mask    'Setup segment mask
        End If

        'Segment override is allowed Display the following segment override controls
        txtBatchOvrdSegValue.Protected = False  'Textbox
        navBatchOvrdSegValue.Enabled = True          'Navigator
        lblBatchOvrdSegValueDesc.Enabled = True      'Description Label
    Else
        'Segment override not allowed Hide the following segment override controls
        txtBatchOvrdSegValue.Protected = True   'Textbox
        navBatchOvrdSegValue.Enabled = False         'Navigator
        lblBatchOvrdSegValueDesc.Enabled = False     'Description Label
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBatchOvrdSegmentCtrls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub CIGetBatchLogSysDetl()
'+++ VB/Rig Skip +++
'***********************************************************************************
' Desc: Gets batch Log System detail record and update batch Total Also retrieve
'       Status from tciBatchLog
'***********************************************************************************

    On Error GoTo ExpectedErrorRoutine
    
    Dim rs              As DASRecordSet 'Sage MAS 500 Das Recordset Obect
    Dim sSQL            As String       'SQL String
    Dim lBatchKey       As Long         'batch Key

  'New Record Defaults
    With muCIBatchLogSysDetl
        .IsEmpty = True
        .Status = -1                'Invalid Status
        .BatchTotal = 0             'Initial total
    End With
    
  
  'Retrieve The current batch Key from data Manager
    lBatchKey = moDmForm.GetColumnValue("BatchKey")
    
  
  'Read batch Status, batch Total from tciBatchLog
    sSQL = "SELECT PostStatus, Status, BatchTotal FROM tciBatchLog WITH (NOLOCK) WHERE BatchKey = " & lBatchKey
    
  'Run Database query
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

  'No Sys Detail Record
    If rs.IsEmpty Then
        muCIBatchLogSysDetl.IsEmpty = rs.IsEmpty
    Else
        With muCIBatchLogSysDetl
            .PostStatus = gvCheckNull(rs.Field("PostStatus"), SQL_INTEGER) 'posting Status
            .Status = gvCheckNull(rs.Field("Status"), SQL_INTEGER)         'batch Status
            .BatchTotal = gvCheckNull(rs.Field("BatchTotal"), SQL_DECIMAL) 'Total Amount
        End With
    End If
        
  'Clean up recordset object
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
  'batch Log system detail not same as what is in batch Total
    If muCIBatchLogSysDetl.BatchTotal <> curBatchTotal Then
        
        curBatchTotal = muCIBatchLogSysDetl.BatchTotal          'recalculate batch total
        
        If curBatchCtrlAmt = 0 Then                                 'No tran Control Amount
            curBatchVariance = 0                                     'Don't Calculate Variance
        Else
            curBatchVariance = curBatchCtrlAmt - curBatchTotal           'Recalculate variance
        End If
    
    End If

  'If batch is in useGet actual batch Status from lock record
    If muCIBatchLogSysDetl.Status = kvInUse Then
        If Not bBatchLocked Then
            ' no one is using batch, reset status and update table
            muCIBatchLogSysDetl.Status = iGetFormBatchStatus
            iUpdateCIBatchLogSysDetl muCIBatchLogSysDetl.Status, (moDmForm.IsInTransaction = False)
        End If
    End If

    Exit Sub

ExpectedErrorRoutine:
    Exit Sub

End Sub
Private Function iGetFormBatchStatus() As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************************
' Desc: Returns batch Status Based on what is currently displayed on the form
' Returns: On Hold, Balanced or Out of Balance
'***********************************************************************************
    
    iGetFormBatchStatus = -1                        'Cleared out
    
    If chkBatchHold = vbChecked Then                      'Hold Checked set Status to On Hold
        iGetFormBatchStatus = kvOnHold
    Else
        If curBatchCtrlAmt = 0 Or curBatchVariance = 0 Then   'No variance
            iGetFormBatchStatus = kvBalanced        'Set Status to Balanced
        Else                                         'Variance Exists
            iGetFormBatchStatus = kvOutofBalance    'Set Status to Out of Balance
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iGetFormBatchStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub DisplayBatchStatus()
'+++ VB/Rig Skip +++
'******************************************************************************************
' Desc: For New batch Record where the tarBatch and tciBatchLog records have not been
'       written, the Status can always change based on the current state of the screen.
'       otherwise set to Value from batch log system detail
'******************************************************************************************
    Dim lBatchKey       As Long
    Dim lCount          As Long
    Dim iRetVal         As Integer

    On Error GoTo ExpectedErrorRoutine

    If moDmForm Is Nothing Then
        Exit Sub
    End If

    Select Case moDmForm.State             'Check form state (fake Status on new state)
    
        Case kDmStateNone
            miBatchLogSysDetlStatus = -1   'Status is cleared out (no record present)

        Case kDmStateAdd                   'new record
            
            'tciBatchLog/tarBatch created in stored procs -- will never be in Add mode

        Case kDmStateEdit
        
          'Load batch Log System detail record (update batch Total and variance)
            CIGetBatchLogSysDetl
            
            Select Case muCIBatchLogSysDetl.Status                   'Get Status from batch log system detail
                
                Case kvOnHold, kvBalanced, kvOutofBalance            'if not in use or posting
                
                    Select Case True
                        Case chkBatchHold = vbChecked                     'Hold Checked
                            miBatchLogSysDetlStatus = kvOnHold       'Set to On Hold
                        Case chkBatchHold = vbUnchecked And curBatchCtrlAmt <> 0 And curBatchVariance = 0
                            miBatchLogSysDetlStatus = kvBalanced     'Balanced if Not on Hold
                                                                     'Balanced w/tran Control
                        Case chkBatchHold = vbUnchecked And curBatchCtrlAmt <> 0 And curBatchVariance <> 0
                            miBatchLogSysDetlStatus = kvOutofBalance  'Out of Balance if Not on
                                                                      'Hold and variance > 0
                        Case chkBatchHold = vbUnchecked And curBatchCtrlAmt = 0
                            miBatchLogSysDetlStatus = kvBalanced      'Balanced if not on Hold
                                                                      'And No tran control Amt
                        Case Else
                            miBatchLogSysDetlStatus = muCIBatchLogSysDetl.Status 'set to Status
                            
                    End Select
                    
                Case kvInUse, kvInterrupted                            'Set Status to in use
                    miBatchLogSysDetlStatus = muCIBatchLogSysDetl.Status
                Case Else                                              'set staus (posting?)
                    miBatchLogSysDetlStatus = muCIBatchLogSysDetl.PostStatus
            
            End Select
        
    End Select
        
    txtBatchStatus = Empty                                  'Clear out Status textbox
    On Error Resume Next                               'Be prepared for bad Value
    txtBatchStatus = moBatchStatus(Trim(Str(miBatchLogSysDetlStatus))) 'set Value of Status

    ' if bank account is used for this batch type, we need to disable
    ' it if any transactions exist for the batch
    If Not txtBankAcct.Visible Then
        Exit Sub
    End If

    ' if transactions exist, we must disable the bank account since those
    ' transactions could be specific for the bank account

    ' Get current batch Key
    lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)

    With moClass.moAppDB
        .SetInParam lBatchKey               ' batch key
        .SetInParam mlBatchType             ' batch Type
        .SetOutParam lCount                 ' return # of transactions using batch key
        .SetOutParam iRetVal                ' return Status
        .ExecuteSP "spGetOtherBatchUse"     ' check for other use of batch key
        lCount = .GetOutParam(3)
        iRetVal = .GetOutParam(4)
        .ReleaseParams
    End With

    If iRetVal = 0 Then
        ' couldn't get status of batch usage
        giSotaMsgBox Me, moClass.moSysSession, kNoBatchUsage
        Exit Sub
    End If

    ' if batch has transactions, lock down the bank account and deposit fields
    If lCount > 0 Then
        gDisableControls txtBankAcct, navBankAcct, txtDepSlipNumber, txtDepositDate
    Else
        gEnableControls txtBankAcct, navBankAcct
        If Len(Trim(txtBankAcct)) <> 0 And muAROptions.IntegrateCM Then
            gEnableControls txtDepSlipNumber, txtDepositDate
        End If
    End If
        
    Exit Sub

ExpectedErrorRoutine:
    gClearSotaErr
    txtBatchStatus = Empty
    Exit Sub

    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "DisplayBatchStatus"
    gClearSotaErr
    Exit Sub
End Sub

Private Sub ClearUnboundControls()
'+++ VB/Rig Skip +++
'******************************************************************************************
' Desc: Clear out all unbound controls.
'******************************************************************************************
    On Error GoTo ExpectedErrorRoutine

    txtBatchStatus = Empty          'Clear out Status
    txtBatchNo = Empty              'Clear out batch Number
    curBatchVariance = 0            'Clear out Variance
    curBatchTotal = 0               'Clear out batch Total
    txtDepSlipNumber = Empty
    txtDepositDate = Empty
    
    'grdBatchTran is unbound for ARPA batches.
    If mlBatchType = kBatchTypeARPA Then
        gGridUpdateCellText grdBatchTran, 1, kColTranCount, ""
        gGridUpdateCellText grdBatchTran, 2, kColTranCount, ""
        gGridUpdateCellText grdBatchTran, 3, kColTranCount, ""
        If mbMCEnabledForAR = False Then
            'The amount column is only displayed for ARPA batches when MC is not being
            'used in AR.
            gGridUpdateCellText grdBatchTran, 1, kColTranTotal, ""
            gGridUpdateCellText grdBatchTran, 2, kColTranTotal, ""
            gGridUpdateCellText grdBatchTran, 3, kColTranTotal, ""
        End If
    
    End If
    
    If mlBatchType = kBatchTypeARRA Then
        gGridUpdateCellText grdBatchTran, 1, kColTranCount, ""
        gGridUpdateCellText grdBatchTran, 2, kColTranCount, ""
        gGridUpdateCellText grdBatchTran, 3, kColTranCount, ""
        gGridUpdateCellText grdBatchTran, 1, kColTranTotal, ""
        gGridUpdateCellText grdBatchTran, 2, kColTranTotal, ""
        gGridUpdateCellText grdBatchTran, 3, kColTranTotal, ""
    End If
    
    gEnableControls txtBatchNo      'ReEnable batch text control
    On Error Resume Next            'set up for Disabled control/Form
    bSetFocus txtBatchNo            'Set focus back to batch Number textbox
    On Error GoTo ExpectedErrorRoutine   'reset error processing

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "ClearUnboundControls"
    gClearSotaErr

End Sub

Private Function bSaveBatch() As Boolean
'+++ VB/Rig Skip +++
'******************************************************************************************
' Desc:    Save current batch
' Returns: true - Successful save occurred
'          false - unsuccessful save occurred
'******************************************************************************************

    On Error GoTo ExpectedErrorRoutine

    Dim iSaveCode   As Integer   'return code from data manager save
    
    bSaveBatch = False           'initialize return Value

    ' if batch is exclusively locked, we can't update it
    If bBatchLocked(kLockTypeExclusive) Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchExclLock
        Exit Function
    End If

    ' check if post date has changed.  If so, the register printed flag
    ' needs to be reset (since the post date will affect how the GL
    ' transactions get posted)

    ' try to prevent problems with the date conversions
    On Error Resume Next
    If CDate(txtBatchPostDate.Text) <> CDate(moDmForm.GetColumnValue("PostDate")) Then
        If Not bResetRegPrintFlag Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgRegResetFailed
        End If
    End If
    On Error GoTo ExpectedErrorRoutine

    ' if cash tran log record is dirty, mark batch as dirty
    If Not moDmCashTranLog Is Nothing Then
        If moDmCashTranLog.IsDirty Then
            moDmForm.SetDirty True
        End If
    End If
  'Save the changes to the batch but does not clear the form.
    iSaveCode = moDmForm.Save(True)   'run data manager save

    Select Case iSaveCode            'Check return code from data manager save
        Case kDmSuccess              'successful save set return code
            bSaveBatch = True
        Case kDmFailure, kDmError
            'Unsuccessful save
    End Select

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bSaveBatch"
    gClearSotaErr
    Exit Function

End Function

Private Function bResetRegPrintFlag() As Boolean
'+++ VB/Rig Skip +++
'******************************************************************************************
' Desc:    reset register printed flag in tciBatchLog
' Returns: true - Successful
'          false - unsuccessful
'******************************************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim sSQL            As String
    Dim lBatchKey       As Long

    bResetRegPrintFlag = False

    lBatchKey = moDmForm.GetColumnValue("BatchKey")

    sSQL = "UPDATE tciBatchLog SET RgstrPrinted = 0 WHERE BatchKey = " & lBatchKey
    
    moClass.moAppDB.ExecuteSQL sSQL

    bResetRegPrintFlag = True

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bResetRegPrintFlag"
    gClearSotaErr
    Exit Function

End Function


Private Sub txtBatchPostDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatchPostDate, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
' Desc: Make sure you have entered a valid Post date
'********************************************************************
    moVM.IsValidControl txtBatchPostDate
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtBatchPostDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Function bIsValidPostDate() As Boolean
'+++ VB/Rig Skip +++
'********************************************************************
' Desc: Make sure you have entered a valid Post date
'********************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim bValid          As Boolean
    Dim iErr            As Integer
    Dim iRet            As Integer
    
  'Assume valid. Prove Invalidity.
    bValid = True
    iErr = 0
    
    If Len(Trim(txtBatchPostDate)) = 0 Then      'Post date cleared out
        bValid = False
        iErr = 1
    Else
        If Not gbValidLocalDate(txtBatchPostDate.Text) Then  'valid Local date
            bValid = False                                      'set error for return code
            iErr = 1
        Else
            If CDate(txtBatchPostDate.Text) < mdStartDate Or _
                CDate(txtBatchPostDate.Text) > mdEndDate Then
                    bValid = False
                    iErr = 2
            End If
        End If
    End If

    If Not bValid Then
        If moDmForm.IsInTransaction Then                    'if this is called from DMPreSave
            moDmForm.CancelAction                           'rollback transaction before
        End If                                              'displaying message box
        Select Case iErr
            Case 1
                giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidDate

            Case 2
                ' give warning only
                iRet = giSotaMsgBox(Nothing, moClass.moSysSession, kmsgDateInCurPeriod, _
                    sRemoveAmpersand(lblPostingDate), mdStartDate, mdEndDate)
                If iRet = kretOK Then
                    bValid = True
                End If

        End Select
    End If

    'return to caller
    bIsValidPostDate = bValid
    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bIsValidPostDate"
    gClearSotaErr
    Exit Function
End Function

Private Function bIsValidBankAcct() As Boolean
    On Error GoTo ExpectedErrorRoutine
    
    Dim oRS As Object            'SOTADAS Recordset
    Dim sSQL As String           'SQL String

    'Assume Valid.  Prove Invalidity.
    bIsValidBankAcct = True

    'Clear out internal CM cash acct structure.
    With muCMCashAcct
        .CashAcctKey = 0
        .CurrID = ""
        .CashAcctID = ""
        .AllowMCDep = False
    End With

    'If cleared out, reset key to NULL and disable deposit information.
    If Len(Trim(txtBankAcct)) = 0 Then
        moDmForm.SetColumnValue "CashAcctKey", ""
        moDmForm.SetColumnValue "CashTranKey", ""
        txtDepSlipNumber = ""
        txtDepositDate = ""
        If cmdPrintChecks.Visible Then
            cmdPrintChecks.Enabled = False
        End If
        DetermineDepositFieldStatus
        Exit Function
    End If

    'Validate the bank account.
    sSQL = "SELECT * FROM tcmCashAcct WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & " AND CashAcctID = " & gsQuoted(txtBankAcct)
    Set oRS = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    If oRS Is Nothing Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidFKFieldValue, sRemoveAmpersand(lblBankAcct)
        bIsValidBankAcct = False
        Exit Function
    End If

    If oRS.IsEmpty Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidFKFieldValue, sRemoveAmpersand(lblBankAcct)
        bIsValidBankAcct = False
        Exit Function
    End If

    ' move valid record values to cash account structure
    With muCMCashAcct
        .CashAcctKey = oRS.Field("CashAcctKey")
        .CashAcctID = txtBankAcct
        .CurrID = oRS.Field("CurrID")
        .AllowMCDep = (oRS.Field("AllowMCDep") = 1)
    End With

    'Save the cash acct key.
    moDmForm.SetColumnValue "CashAcctKey", muCMCashAcct.CashAcctKey
    
    If cmdPrintChecks.Visible Then
    
        If giGetValidInt(oRS.Field("PrintChks")) = 1 Then
            cmdPrintChecks.Enabled = True
        Else
            cmdPrintChecks.Enabled = False
        End If
               
    End If


    'Clean up the recordset object.
    If Not oRS Is Nothing Then
        oRS.Close
        Set oRS = Nothing
    End If

    'At this point, the bank account has changed and is valid.  Clear out dep # so
    'it will default based on the new bank account.
    txtDepSlipNumber = ""

    'Valid bank account.  Set up deposit information fields.
    DetermineDepositFieldStatus
    
    'Get the next deposit number if AR is integrated with Cash Management.
    If Not moDmCashTranLog Is Nothing Then
        GetNextDepNumber
    End If

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bIsValidBankAcct"
    gClearSotaErr
    
End Function

Private Function bIsValidTranCtrl() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'   Description: update the batch Status if a change was made to the tran control
'************************************************************************************
    
    bIsValidTranCtrl = True

    If curBatchCtrlAmt.Amount <> curBatchCtrlAmt.Tag Then

      'Round tran Control
        With muHomeCurrInfo
            curBatchCtrlAmt.Amount = gfRound((curBatchCtrlAmt.Amount), .iRoundMeth, _
                                               .iDecPlaces, .iRoundPrecision)
        End With
    
        ' make sure amount is within limits
        If curBatchCtrlAmt.Amount > 999999999999.999 Then
            bIsValidTranCtrl = False
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                sRemoveAmpersand(lblControlTotal.Caption), gsLocStr(ksVal15Dot3, moClass)
        End If

      'A Change was made to the transaction Control
        DisplayBatchStatus                          'Display New batch Status
    
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidTranCtrl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub txtDepositDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDepositDate, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Validate Deposit date
'************************************************************************************
    moVM.IsValidControl txtDepositDate

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDepositDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepSlipNumber_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDepSlipNumber, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii > 32 And KeyAscii < 128 Then
        If InStr("0123456789", Chr$(KeyAscii)) = 0 Then
            KeyAscii = 0
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDepSlipNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepSlipNumber_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDepSlipNumber, True
    #End If
'+++ End Customizer Code Push +++
'************************************************************************************
'   Description: Validate Deposit Slip Number
'************************************************************************************
    moVM.IsValidControl txtDepSlipNumber

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDepSlipNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Function lLockBatch(lBatchKey As Long, Optional vLockType As Variant) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
' Desc:  create a logical lock for the batch
'*******************************************************************************
    
'    gbCreateLock moclass.moappdb, kLockEntBatch, kLockActInUse, _
'        lBatchKey, msCompanyID, ""
    Dim lLockID     As Long
    Dim lRet        As Long
    Dim lLockType   As Long

    ' default to shared lock
    lLockType = kLockTypeShared
    ' if a lock type was passed in, use that
    If Not IsMissing(vLockType) Then
        lLockType = CLng(vLockType)
    End If

    lRet = glLockLogical(moClass.moAppDB, sLockEntity, lLockType, lLockID)

    If lRet < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCantGetBatchLock, txtBatchNo
        lLockBatch = lRet
    Else
        lLockBatch = lLockID
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lLockBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub DropLock(lLockID As Long)
'+++ VB/Rig Skip +++
'*******************************************************************************
' Desc:  remove lock for current batch Key
'*******************************************************************************

    ' don't crash app if we can't drop the lock
    On Error Resume Next
    glUnlockLogical moClass.moAppDB, lLockID
    Exit Sub

End Sub

Private Function bCanDelete() As Boolean
'+++ VB/Rig Skip +++
'*******************************************************************************
' Desc:     see if batch can be Deleted (can't be Deleted if it's
'           posting or in use)
'           Also need to check for other transactions using
'           this batch
'*******************************************************************************
    Dim iStatus     As Integer      ' batch Status

    On Error GoTo ExpectedErrorRoutine

    bCanDelete = False

    ' Get batch Status
    If Not bGetBatchStatus(iStatus) Then Exit Function

    ' since this is called from the delete routine, make sure user is
    ' aware of the most current batch status
    DisplayBatchStatus

    ' Can't delete if posting, in use or module posting has completed.
    If (iStatus = kvPosting Or iStatus = kvInUse Or iStatus = kvInterrupted) Or muCIBatchLogSysDetl.PostStatus >= 200 Then
        Exit Function
    End If

    ' let user delete batch
    bCanDelete = True
    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bCanDelete"
    gClearSotaErr
    Exit Function

End Function

Private Function bDeleteBatchLog() As Boolean
'+++ VB/Rig Skip +++
'****************************************************
' Desc: Deletes the batch log record and cleans up the
'       tciBatchControl record if Number was auto numbered
'****************************************************
    Dim iRetVal     As Integer

    bDeleteBatchLog = False

    On Error GoTo ExpectedErrorRoutine

    With moClass.moAppDB
        .SetInParamLong CLng(moDmForm.GetColumnValue("BatchKey")) 'BatchKey
        .SetInParamLong Abs(CLng(mbAutoNum))                ' Auto Numbering used?
        .SetInParamStr msCompanyID                          ' Company ID
        .SetInParamLong mlBatchType                         ' batch Type
        .SetInParamInt 1                                    ' delete distributions
        .SetOutParam iRetVal                                ' delete status
        .ExecuteSP "spDeleteBatchLog"                       ' Run stored procedure
        iRetVal = .GetOutParam(6)                           ' get status of delete
        .ReleaseParams                                      ' Release Parameters
    End With

    If iRetVal = 1 Then
        bDeleteBatchLog = True
    End If

    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bDeleteBatchLog"
    gClearSotaErr
    Exit Function

End Function

Public Function DMPreSave(oDm As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    DMPreSave = False

    If oDm Is moDmForm Then
        If moVM.IsValidControl(txtBatchPostDate) Then
            If Len(Trim(txtBankAcct)) > 0 And Not (moDmCashTranLog Is Nothing) Then
                If moDmCashTranLog.Save(True) = kDmSuccess Then
                    DMPreSave = True
                End If
            Else
                DMPreSave = True
                moDmForm.SetColumnValue "CashTranKey", ""
            End If
        End If
    Else
        DMPreSave = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMPreSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub BindVM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moVM = New clsValidationManager

    With moVM
        Set .Form = frmBatch
        .BindKey txtBatchNo
        .SkipControls navMain

        .Bind txtBankAcct
        .Bind curBatchCtrlAmt
        .Bind txtBatchOvrdSegValue
        .Bind txtBatchPostDate

        If (mlBatchType = kBatchTypeARCR _
        And muAROptions.IntegrateCM) Then
            .Bind txtDepSlipNumber, , True
            .Bind txtDepositDate
        End If

        .SkipForAll tbrMain, sbrMain

        .Init
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindVM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function VMIsValidControl(oCtl As Control) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    VMIsValidControl = True

    Select Case True

        Case oCtl Is txtBatchNo
            VMIsValidControl = bIsValidBatch

            If Not VMIsValidControl And bSingleBatch() Then
                Select Case moDmForm.Action(kDmCancel)
                    Case kDmSuccess
                        moClass.lUIActive = kChildObjectInactive

                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        Unload Me
                End Select
            End If

        Case oCtl Is txtBankAcct
            If moDmForm.State <> kDmStateNone Then VMIsValidControl = bIsValidBankAcct

        Case oCtl Is curBatchCtrlAmt
            If moDmForm.State <> kDmStateNone Then VMIsValidControl = bIsValidTranCtrl

        Case oCtl Is txtBatchOvrdSegValue
            If moDmForm.State <> kDmStateNone Then VMIsValidControl = bIsValidBatchOvrdSegValue

        Case oCtl Is txtBatchPostDate
            If moDmForm.State <> kDmStateNone Then VMIsValidControl = bIsValidPostDate

        Case oCtl Is txtDepSlipNumber
            If moDmForm.State <> kDmStateNone Then VMIsValidControl = bIsValidDepSlipNum

        Case oCtl Is txtDepositDate
            If moDmForm.State <> kDmStateNone Then VMIsValidControl = bIsValidDepositDate

    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bGetBatch(iBatchType As Integer) As Boolean
'+++ VB/Rig Skip +++
    Dim iRetVal     As Integer
    Dim lBatchNo    As Long
    Dim lBatchKey   As Long

    bGetBatch = False

    On Error GoTo ExpectedErrorRoutine

    With moClass.moAppDB
        .SetInParam msCompanyID
        .SetInParam iBatchType
        .SetOutParam lBatchNo
        .SetOutParam lBatchKey
        .SetOutParam iRetVal
        .ExecuteSP "spGetExistingBatch"
        lBatchNo = .GetOutParam(3)
        iRetVal = .GetOutParam(5)
        .ReleaseParams
    End With

    If iRetVal = 0 Then
        GetNewBatch
    Else
        txtBatchNo = lBatchNo
    End If

    bGetBatch = True
    Exit Function

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "bGetBatch"
    gClearSotaErr
    Exit Function

End Function

' public property for DrillAround function

Public Property Let txtTemplateID(sTemplateID As String)
'+++ VB/Rig Skip +++
    txtBatchNo = sTemplateID
End Property

' IsValidTemplateID for DrillAround function

Public Function IsValidTemplateID() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    IsValidTemplateID = VMIsValidKey
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "IsValidTemplateID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SetupToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    ' Setup Sage MAS 500 Toolbar
    tbrMain.LocaleID = moClass.moSysSession.Language
    If bSingleBatch() Then
        tbrMain.Build sotaTB_SINGLE_ROW
        tbrMain.AddButton kTbDelete, 4
        tbrMain.AddButton kTbNextNumber, 6
    Else
        tbrMain.Build sotaTB_MULTI_ROW
        tbrMain.AddButton kTbNextNumber, 8
        tbrMain.RemoveButton kTbRenameId
    End If

    ' remove print button and memo button for batch form
    tbrMain.RemoveButton kTbPrint
    tbrMain.RemoveButton kTbMemo

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupToolbar", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function bSingleBatch() As Boolean
'+++ VB/Rig Skip +++
    bSingleBatch = False

    Select Case mlBatchType
        Case kBatchTypeARIN, kBatchTypeARCR, kBatchTypeARPA, kBatchTypeARRA
            bSingleBatch = False

        Case Else
            bSingleBatch = True

    End Select

End Function


Public Property Get ReadOnly() As Boolean
'+++ VB/Rig Skip +++
    ReadOnly = mbReadOnly
End Property

Public Property Let ReadOnly(bNewValue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbReadOnly = bNewValue
    If mbReadOnly Then
        moDmForm.ReadOnly = True
        cmdTransactions.Visible = False
        cmdPrintInvoices.Visible = False
        cmdBatchRgstrPost.Visible = False
        cmdSelect.Visible = False
        cmdSelectShipments.Visible = False
        cmdMerge.Visible = False
        navBatchOvrdSegValue.Enabled = False
        txtBatchOvrdSegValue.Protected = True
        curBatchCtrlAmt.Protected = True
        grdBatchTran.Enabled = False
        txtBankAcct.Enabled = False
        chkBatchHold.Enabled = False
        txtBatchPostDate.Enabled = False
    End If
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ReadOnly_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Function NavReturn(oNavCtl As Control, vReturnArr As Variant) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    NavReturn = False

    If oNavCtl Is navMain Then
        NavReturn = True
        txtBatchNo = vReturnArr(1)
        moVM.IsValidControl txtBatchNo, True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "NavReturn", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bBatchLocked(Optional vLockType As Variant) As Boolean
'+++ VB/Rig Skip +++
    Dim lBatchKey       As Long
    Dim lLockCount      As Long
    Dim lExclLocks      As Long
    Dim lShrdLocks      As Long

    ' prevent app crash if we can't get a lock status
    On Error Resume Next

    lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)
'    iLockCount = giCheckLock(moclass.moappdb, kLockEntBatch, kLockActInUse, _
'                            lBatchKey, msCompanyID, "")

    lShrdLocks = 0
    lExclLocks = 0
    gCountLogical moClass.moAppDB, sLockEntity, lShrdLocks, lExclLocks

    ' if no lock type passed in, get any lock type
    If IsMissing(vLockType) Then

        lLockCount = lShrdLocks + lExclLocks
        If lLockCount > 0 Then
            bBatchLocked = True
        Else
            bBatchLocked = False
        End If

    Else

        If CLng(vLockType) = kLockTypeExclusive Then
            bBatchLocked = CBool(lExclLocks > 0)
        End If
        If CLng(vLockType) = kLockTypeShared Then
            bBatchLocked = CBool(lShrdLocks > 0)
        End If

    End If

    gClearSotaErr
    Exit Function

End Function

Private Sub sbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function bIsValidBatch(Optional vSkipMsgs As Variant) As Boolean
'+++ VB/Rig Skip +++
    Dim lBatchKey           As Long     'batch Key
    Dim lBatchDescStringNo  As Long     'String No for Batc Description
    Dim iRetVal             As Integer  'Return Value
    Dim iStatus             As Integer  'Status of batch
    Dim iPostStatus         As Integer  'Posting Status of batch
    Dim bInTrans            As Boolean  'In transaction flag
    Dim sUserID             As String   'User ID
    Dim sProcName           As String
    Dim sSQL                As String
    Dim rs                  As DASRecordSet
    Dim sErrDesc            As String   'error text
    Dim bSkipMsgs           As Boolean  ' flag to skip the display of error messages

    On Error GoTo ExpectedErrorRoutine

    bSkipMsgs = False
    If Not IsMissing(vSkipMsgs) Then
        bSkipMsgs = CBool(vSkipMsgs)
    End If

  'Initialize function return
    bIsValidBatch = True

  'Zero fill batch number
    txtBatchNo = gsZeroFill(txtBatchNo, kBatchNoLength)
    
  'Clear Out Common Information batch Log
    With muCIBatchLog
        .BatchKey = 0
        .BatchID = Empty
        .IsEmpty = True
    End With
    
    'Intellisol start
    ' Check for an existing invoice
    If muCIBatchTypCompany.BatchType = 501 Then
        sSQL = "tciBatchLog.BatchNo = " & Trim(Str(Val(txtBatchNo))) & _
            " AND tciBatchLog.BatchType = 501" & _
            " AND tciBatchLog.SourceCompanyID = " & gsQuoted(msCompanyID) & _
            " AND tciBatchLog.BatchKey = tarBatch.BatchKey"

        If moClass.mbCalledFromPA Then
            ' find invoices without a PA link
            sSQL = sSQL & " AND tarBatch.BatchKey NOT IN (SELECT BatchKey FROM paarBatch)"
            If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "tciBatchLog, tarBatch", _
              sSQL)) > 0 Then
                If Not bSkipMsgs Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgPAInvcBatchExistsAR
                End If
                bIsValidBatch = False
                Exit Function
            End If
        Else
            ' find invoices that have a PA link
            sSQL = sSQL & " AND tarBatch.BatchKey IN (SELECT BatchKey FROM paarBatch)"
            If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "tciBatchLog, tarBatch", _
              sSQL)) > 0 Then
                If Not bSkipMsgs Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgPAInvcBatchExistsPA
                End If
                bIsValidBatch = False
                Exit Function
            End If
        End If
    End If
    'Intellisol end

    ' if form is read-only, see if batch exists (the spGetBatch routine
    ' will automatically create a batch if it does not exist -- we want
    ' to skip that)
    If miSecurityLevel = sotaTB_DISPLAYONLY Then
'        sSQL = "SELECT BatchKey FROM tciBatchLog WHERE SourceCompanyID = "
'        sSQL = sSQL & gsQuoted(msCompanyID) & " AND BatchType = "
'        sSQL = sSQL & Str$(muCIBatchTypCompany.BatchType) & " AND BatchNo = "
'        sSQL = sSQL & Str$(txtBatchNo)

        sSQL = "#arFindBatch;" & gsQuoted(msCompanyID) & ";" & Str$(muCIBatchTypCompany.BatchType) & ";" & Str$(txtBatchNo) & ";"
        
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

        ' if this batch doesn't exist, we exit with failure (since batch form
        ' is display only)
        If rs Is Nothing Then
            bIsValidBatch = False
            Exit Function
        End If

        If rs.IsEOF Then
            bIsValidBatch = False
            Exit Function
        End If

    End If
    
    ' RMM 41057, Part of fix
    sSQL = "#arFindBatch;" & gsQuoted(msCompanyID) & ";" & Str$(muCIBatchTypCompany.BatchType) & ";" & Str$(txtBatchNo) & ";"
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If (Not rs Is Nothing) Then
        If (Not rs.IsEOF) Then
            mbBatchExists = True
        End If
    End If
    
    sProcName = "spGetBatch"

  '--- BEGIN TRANSACTION ---
  'Run Stored procedure to get batch key
    With moClass.moAppDB
      .SetInParam msCompanyID                         'Company ID
      .SetInParam CLng(muCIBatchTypCompany.ModuleNo)  'Module Number
      .SetInParam msUserID                            'User ID
      .SetInParam CLng(txtBatchNo)                    'batch Number
      .SetInParam CLng(muCIBatchTypCompany.BatchType) 'batch Type
      .SetInParam muCIBatchTypCompany.BatchTypeID     'batch Type ID
      .SetInParam muCIBatchTypCompany.DfltBatchCmnt   'Default batch Comment
      .SetInParam gsFormatDateToDB(Format(sbrMain.BusinessDate, "SHORT DATE"))
      .SetOutParam lBatchKey                          'batch Key
      .SetOutParam iStatus                            'Status Code of batch
      .SetOutParam sUserID                            'User ID for Private batch
      .SetOutParam iRetVal                            'Return code
      .SetOutParam iPostStatus                        'Posting Status
      .BeginTrans                                     'Begin Transaction
      bInTrans = True                                 'Set transaction flag
      .ExecuteSP (sProcName)                          'Run stored procedure
      lBatchKey = .GetOutParam(9)                     'batch Key
      iStatus = .GetOutParam(10)                      'Status Code of batch
      sUserID = .GetOutParam(11)                      'User ID for Private batch
      iRetVal = .GetOutParam(12)                      'Return code
      iPostStatus = .GetOutParam(13)                  'Posting Status
      .ReleaseParams                                  'Release Parameters
      .CommitTrans                                    'Commit Transaction
      bInTrans = False                                'Reset Transaction Flag
    End With
    
    'Intellisol start
    ' Insert a flag into paarBatch if this is a new invoice #
    If moClass.mbCalledFromPA Then
      If iRetVal = 1 Then
        If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "paarBatch", _
            "BatchKey=" & Trim(Str(lBatchKey)))) = 0 Then
          On Error Resume Next
          moClass.moAppDB.ExecuteSQL "INSERT INTO paarBatch VALUES (" & _
            Trim(Str(lBatchKey)) & ")"
          gClearSotaErr
          On Error GoTo ExpectedErrorRoutine
        End If
      End If
    End If
    'Intellisol end

    Select Case iRetVal
        
        Case 1  'batch may be good, check for any locks
            If Not bBatchLocked(kLockTypeExclusive) Then
                muCIBatchLog.BatchKey = lBatchKey
                muCIBatchLog.IsEmpty = False
            Else
                If Not bSkipMsgs Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBatchNoEdit
                End If
                bIsValidBatch = False
            End If

        Case 2  'batch is no good

            ' if batch is deleted, alert user
            If iPostStatus = kvDeleted Then
                If Not bSkipMsgs Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBatchBadStatus, sGetBatchStatusText(kvDeleted)
                End If
                bIsValidBatch = False
            Else
                ' check lock count
                If (iStatus = kvPosting And bBatchLocked) Or iStatus = kvPosted Then
                    ' batch is really no good
                    If Not bSkipMsgs Then
                        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchBadStatus, sGetBatchStatusText(iStatus)
                    End If
                    bIsValidBatch = False
                Else
                    ' update batch status to 7 (interrupted) and
                    ' allow batch to be pulled up
                    If Not bSetBatchInterrupted(lBatchKey) Then
                        bIsValidBatch = False
                    Else
                        muCIBatchLog.BatchKey = lBatchKey
                        muCIBatchLog.IsEmpty = False
                    End If
            
                End If

            End If

        Case 3  'batch Type Company record is missing
            If Not bSkipMsgs Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCreateBatchControlRecord, _
                    Str$(muCIBatchTypCompany.BatchType) & " (" & Me.Caption & ")"
            End If
            mbUnload = True
            bIsValidBatch = False
        
        Case 4  'batch is Another users Private batch
           
          'User is a supervisor Can pull up Private batches
            If giGetSecurityLevel(moClass.moFramework, _
                    moClass.moFramework.GetTaskID) = sotaTB_SUPERVISORY Then
                
                muCIBatchLog.BatchKey = lBatchKey
                muCIBatchLog.IsEmpty = False
            
            Else
              
              'User Can Not pull up a Private batch which belongs to someone else
                If Not bSkipMsgs Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgBatchNotMyPrivate, Trim(sUserID)
                End If
                bIsValidBatch = False
            
            End If

        Case Else     'An unknown error occurred
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, Str$(iRetVal) & sProcName
            bIsValidBatch = False
    
    End Select

    Exit Function

'An error occurred set return code and rollback tran if still in transaction
ExpectedErrorRoutine:
    sErrDesc = Err.Description

    '--- ROLL BACK TRANSACTION ---
    If bInTrans Then
        moClass.moAppDB.Rollback
    End If

    mbDontChkClick = False
    bIsValidBatch = False
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, sErrDesc, "bIsValidBatch"
    gClearSotaErr
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmBatch"
End Function

Private Function bSetupNavMain() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sRestrictClause As String

    bSetupNavMain = False

    If Len(Trim(navMain.Tag)) = 0 Then
        sRestrictClause = "SourceCompany = " & gsQuoted(msCompanyID) & _
                          " AND BatchType = " & Format$(mlBatchType) & _
                          " AND PostStatus NOT IN (" & kvPostComplete & _
                          ", " & kvDeleted & ")"

        If moClass.mbCalledFromPA Then
            sRestrictClause = sRestrictClause & " AND BatchKey IN (SELECT BatchKey FROM paarBatch WITH (NOLOCK))"
        Else
            sRestrictClause = sRestrictClause & " AND BatchKey NOT IN (SELECT BatchKey FROM paarBatch WITH (NOLOCK))"
        End If

        navMain.Enabled = gbLookupInit(navMain, moClass, moClass.moAppDB, "ARBatch", sRestrictClause)

        If navMain.Enabled Then
            navMain.Tag = "Init"
            bSetupNavMain = True
        End If
    Else
        bSetupNavMain = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupNavMain", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub GetButtonPos()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    ' first button
    miCmdsTop(0) = cmdSelect.Top
    miCmdsLeft(0) = cmdSelect.Left
    
    ' second button
    miCmdsTop(1) = cmdSelectShipments.Top
    miCmdsLeft(1) = cmdSelectShipments.Left
    
    ' third button
    miCmdsTop(2) = cmdMerge.Top
    miCmdsLeft(2) = cmdMerge.Left
   
    ' fourth button
    miCmdsTop(3) = cmdTransactions.Top
    miCmdsLeft(3) = cmdTransactions.Left
    
    ' fifth button
    miCmdsTop(4) = cmdPrintInvoices.Top
    miCmdsLeft(4) = cmdPrintInvoices.Left
    
   ' sixth button
    miCmdsTop(5) = cmdPrintChecks.Top
    miCmdsLeft(5) = cmdPrintChecks.Left
    
    ' seventh button
    miCmdsTop(6) = cmdBatchRgstrPost.Top
    miCmdsLeft(6) = cmdBatchRgstrPost.Left

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetButtonPos", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub PositionButtons(ParamArray vButtons() As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iLoop       As Integer

    For iLoop = 0 To UBound(vButtons)
        vButtons(iLoop).Top = miCmdsTop(iLoop)
        vButtons(iLoop).Left = miCmdsLeft(iLoop)
    Next iLoop

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PositionButtons", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DMGridRowLoaded(oDm As Object, lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If oDm Is moDmGridBatchTran Then
        gGridSetCellType grdBatchTran, lRow, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMGridRowLoaded", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sLockEntity() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lBatchKey       As Long

    lBatchKey = gvCheckNull(moDmForm.GetColumnValue("BatchKey"), SQL_INTEGER)
    sLockEntity = kLockEntBatch & lBatchKey
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sLockEntity", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

' determine if the current user is the owner of the batch
Private Function bUserIsOwner() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If mbCaseSens Then
        bUserIsOwner = (moClass.moSysSession.UserId = moDmForm.GetColumnValue("OrigUserID"))
    Else
        bUserIsOwner = (UCase(moClass.moSysSession.UserId) = UCase(moDmForm.GetColumnValue("OrigUserID")))
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bUserIsOwner", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub OwnerCheck()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If muAROptions.AllowPrivateBatch Then

        ' if user is owner, they can make the batch private
        If bUserIsOwner() Then
            gEnableControls chkBatchPrivate
        Else
            ' if user is not owner, they can only make batch private if
            ' they have supervisor level security
            If giGetSecurityLevel(moClass.moFramework, moClass.moFramework.GetTaskID) = sotaTB_SUPERVISORY Then
                gEnableControls chkBatchPrivate
            Else
                gDisableControls chkBatchPrivate
            End If
    
        End If

    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "OwnerCheck", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub RefreshTranGrid()
'+++ VB/Rig Skip +++

    Dim lCount As Long
    Dim dAmt As Double
    Dim sSQL As String
    Dim rs As Object
    Dim lBatchKey As Long
    
    lBatchKey = glGetValidLong(moDmForm.GetColumnValue("BatchKey"))

    If mlBatchType = kBatchTypeARPA Then
    
        'ARPA batches are not bound to a DM tran grid.  The tran grid must be
        'manually updated for this batch type.
       
        'Get application info
        sSQL = "BatchKey = " & lBatchKey & " AND PmtAmt <> 0"
        
        lCount = glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "tarPendCustPmtAppl", sSQL))
        gGridUpdateCellText grdBatchTran, 1, kColTranCount, CStr(lCount)
        
        If (lCount > 0) And (mbMCEnabledForAR = False) Then
            gGridSetCellType grdBatchTran, 1, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
            dAmt = gdGetValidDbl(moClass.moAppDB.Lookup("SUM(PmtAmt)", "tarPendCustPmtAppl", sSQL))
            gGridUpdateCellText grdBatchTran, 1, kColTranTotal, CStr(dAmt)
        End If
        
        'Get discount info
        sSQL = "BatchKey = " & lBatchKey & " AND DiscTakenAmt <> 0"
        
        lCount = glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "tarPendCustPmtAppl", sSQL))
        gGridUpdateCellText grdBatchTran, 2, kColTranCount, CStr(lCount)
        
        If (lCount > 0) And (mbMCEnabledForAR = False) Then
            gGridSetCellType grdBatchTran, 2, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
            dAmt = gdGetValidDbl(moClass.moAppDB.Lookup("SUM(DiscTakenAmt)", "tarPendCustPmtAppl", sSQL))
            gGridUpdateCellText grdBatchTran, 2, kColTranTotal, CStr(dAmt)
        End If
        
        'Get write-off info
        sSQL = "BatchKey = " & lBatchKey & " AND WriteOffAmt <> 0"
        
        lCount = glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "tarPendCustPmtAppl", sSQL))
        gGridUpdateCellText grdBatchTran, 3, kColTranCount, CStr(lCount)
        
        If (lCount > 0) And (mbMCEnabledForAR = False) Then
            gGridSetCellType grdBatchTran, 3, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
            dAmt = gdGetValidDbl(moClass.moAppDB.Lookup("SUM(WriteOffAmt)", "tarPendCustPmtAppl", sSQL))
            gGridUpdateCellText grdBatchTran, 3, kColTranTotal, CStr(dAmt)
        End If
    ElseIf mlBatchType = kBatchTypeARRA Then
    
        'ARRA batches are not bound to a DM tran grid.  The tran grid must be
        'manually updated for this batch type.
        
        'Get Invoice application info
        lCount = 0
        dAmt = 0
        
        sSQL = "SELECT COUNT(*) AS [TranCount], SUM(cpa.PmtAmt) AS [TranAmt] FROM tarPendCustPmtAppl cpa WITH (NOLOCK)"
        sSQL = sSQL & " JOIN tarInvoice i WITH (NOLOCK) ON cpa.ApplyToInvcKey = i.InvcKey"
        sSQL = sSQL & " WHERE i.TranType = " & kTranTypeARIN
        sSQL = sSQL & " AND ApplyFromPmtKey IS NULL"
        sSQL = sSQL & " AND cpa.BatchKey = " & lBatchKey
            
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEmpty Then
            lCount = glGetValidLong(rs.Field("TranCount"))
            dAmt = gdGetValidDbl(rs.Field("TranAmt"))
        End If
        
        gGridSetCellType grdBatchTran, 1, kColTranCount, SS_CELL_TYPE_INTEGER
        gGridUpdateCellText grdBatchTran, 1, kColTranCount, CStr(lCount)
    
        gGridSetCellType grdBatchTran, 1, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
        gGridUpdateCellText grdBatchTran, 1, kColTranTotal, CStr(dAmt)
    
        Set rs = Nothing
        
         'Get Payment application info
        lCount = 0
        dAmt = 0
        
        sSQL = "SELECT COUNT(*) AS [TranCount], SUM(PmtAmt) AS [TranAmt] FROM tarPendCustPmtAppl WITH (NOLOCK)"
        sSQL = sSQL & " WHERE ApplyFromPmtKey IS NOT NULL"
        sSQL = sSQL & " AND BatchKey = " & lBatchKey
            
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEmpty Then
            lCount = glGetValidLong(rs.Field("TranCount"))
            dAmt = gdGetValidDbl(rs.Field("TranAmt"))
        End If
        
        gGridSetCellType grdBatchTran, 2, kColTranCount, SS_CELL_TYPE_INTEGER
        gGridUpdateCellText grdBatchTran, 2, kColTranCount, CStr(lCount)
    
        gGridSetCellType grdBatchTran, 2, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
        gGridUpdateCellText grdBatchTran, 2, kColTranTotal, CStr(dAmt)

        Set rs = Nothing

        'Get Memo application info
        lCount = 0
        dAmt = 0
        
        sSQL = "SELECT COUNT(*) AS [TranCount], SUM(cpa.PmtAmt) AS [TranAmt] FROM tarPendCustPmtAppl cpa WITH (NOLOCK)"
        sSQL = sSQL & " JOIN tarInvoice i WITH (NOLOCK) ON cpa.ApplyToInvcKey = i.InvcKey"
        sSQL = sSQL & " WHERE i.TranType IN ( " & kTranTypeARCM & "," & kTranTypeARDM & ")"
        sSQL = sSQL & " AND ApplyFromPmtKey IS NULL"
        sSQL = sSQL & " AND cpa.BatchKey = " & lBatchKey
            
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEmpty Then
            lCount = glGetValidLong(rs.Field("TranCount"))
            dAmt = gdGetValidDbl(rs.Field("TranAmt"))
        End If
        
        gGridSetCellType grdBatchTran, 3, kColTranCount, SS_CELL_TYPE_INTEGER
        gGridUpdateCellText grdBatchTran, 3, kColTranCount, CStr(lCount)
    
        gGridSetCellType grdBatchTran, 3, kColTranTotal, SS_CELL_TYPE_FLOAT, muHomeCurrInfo.iDecPlaces
        gGridUpdateCellText grdBatchTran, 3, kColTranTotal, CStr(dAmt)
    
        Set rs = Nothing
    Else
        'Use DM to update the grid
        On Error Resume Next
        moDmGridBatchTran.Init
        gClearSotaErr
        Exit Sub
    End If

End Sub

Private Sub SetTBState(iState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If iState = kDmStateNone Then
        ' normally, a single row maintenance does not have a 'None' state, so
        ' the toolbar buttons must be disabled when the state goes to none
        If bSingleBatch Then
            tbrMain.ButtonEnabled(kTbFinishExit) = False
            tbrMain.ButtonEnabled(kTbCancelExit) = False
            tbrMain.ButtonEnabled(kTbDelete) = False
        Else
            tbrMain.SetState (iState)
        End If
    Else
        If bSingleBatch Then
            tbrMain.ButtonEnabled(kTbFinishExit) = True
            tbrMain.ButtonEnabled(kTbCancelExit) = True
            If miSecurityLevel > sotaTB_DISPLAYONLY Then
                tbrMain.ButtonEnabled(kTbDelete) = True
            End If
        Else
            tbrMain.SetState (iState)
            If tbrMain.ButtonEnabled(kTbDelete) = True Then 'don't reset if the button has been disabled
                ResetToolbarForCreditCard
            End If
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetTBState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BrowseBatch(sButton As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sNewKey         As String
    Dim lRet            As Long
    Dim bExit           As Boolean

    'Put browse functions into a loop to allow this function to keep
    'browsing if a batch is returned that is private and cannot be pulled
    'up by the current user.
    bExit = False
    Do
        'Execute requested move.
        lRet = glLookupBrowse(navMain, sButton, miFilter, sNewKey)
    
        'Evaluate outcome of requested browse move.
        Select Case lRet
            Case MS_SUCCESS
                If navMain.ReturnColumnValues.Count = 0 Then
                    Exit Sub
                End If

                If Trim(txtBatchNo.Text) <> Trim(navMain.ReturnColumnValues("BatchNo")) Then
                    txtBatchNo.Text = Trim(navMain.ReturnColumnValues("BatchNo"))

                    If bIsValidBatch(True) Then
                        moVM.IsValidControl txtBatchNo
                        bExit = True
                    Else
                        'Bowsed to a batch that user cannot open.
                        Select Case sButton
                            Case kTbFirst
                                'Start browsing forward to find an available record.
                                BrowseBatch kTbNext
                                bExit = True

                            Case kTbLast
                                'Start browsing backward to find an available record.
                                BrowseBatch kTbPrevious
                                bExit = True
                        End Select
                    End If
                End If
    
            Case MS_BORS
                HandleToolBarClick kTbFirst
                bExit = True
    
            Case MS_EMPTY_RS
                giSotaMsgBox Me, moClass.moSysSession, kmsgNavEmptyRecordSet
                bExit = True
    
            Case MS_EORS
                HandleToolBarClick kTbLast
                bExit = True
    
            Case MS_ERROR
                giSotaMsgBox Me, moClass.moSysSession, kmsgNavBrowseError
                bExit = True
    
            Case MS_NO_CURRENT_KEY
                giSotaMsgBox Me, moClass.moSysSession, kmsgNavNoCurrentKey
                bExit = True
    
            Case MS_UNDEF_RS
                giSotaMsgBox Me, moClass.moSysSession, kmsgNavUndefinedError
                bExit = True
    
            Case Else
                gLookupBrowseError lRet, Me, moClass
                bExit = True
        End Select
    Loop Until bExit

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BrowseBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub HideVariance(bHide As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If bHide Then
        curBatchVariance.Visible = False
        lblVarianceTotal.Visible = False
        fraLine.Visible = False
    Else
        curBatchVariance.Visible = True
        lblVarianceTotal.Visible = True
        fraLine.Visible = True
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideVariance", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub DetermineDepositFieldStatus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Len(Trim(txtBankAcct)) <> 0 And muAROptions.IntegrateCM Then
        gEnableControls txtDepSlipNumber, txtDepositDate
        ' after enabling deposit date, if its blank, default it
        If Len(Trim(txtDepositDate)) = 0 Then
            txtDepositDate = txtBatchPostDate
        End If
    Else
        gDisableControls txtDepSlipNumber, txtDepositDate
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DetermineDepositFieldStatus", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'Copy information into cash tran log (most of the information is not
'displayed and is thus bound to Nothing).
Private Function bCopyBatchValues() As Boolean
    Dim lCashAcctKey As Long

    bCopyBatchValues = False

    With moDmCashTranLog
        moDmForm.SetColumnValue "CashTranKey", .GetColumnValue("CashTranKey")
        .SetColumnValue "TranID", Trim(txtDepSlipNumber) & "-DE"

        'Get the Cash Account Key we are going to write to the tcmCashTranLog table.
        lCashAcctKey = glGetValidLong(moDmForm.GetColumnValue("CashAcctKey"))

        'Do we have a valid Cash Account?
        If lCashAcctKey > 0 Then
            'Yes, we DO have a valid Cash Account, so use it.
            .SetColumnValue "CashAcctKey", Format$(lCashAcctKey)
        Else
            'No, we do NOT have a valid Cash Account.
            'We will use the Default Cash Account (tarOptions.DfltCashAcctKey), if we can.
            'Do we have a valid Default Cash Account?
            If muAROptions.DfltBankAcctKey <> 0 Then
                'Yes, we DO have a valid Default Cash Account.
                .SetColumnValue "CashAcctKey", Format$(muAROptions.DfltBankAcctKey)
            Else
                'No, we do NOT have a valid Default Cash Account.
                'Error processing
                'Give an error message to the user.
                '{0} is required.  Please either enter a {1} or set up a default bank account in the Set Up AR Options task.
                Me.MousePointer = vbDefault
                giSotaMsgBox Me, moClass.moSysSession, kmsgARBankAcctRequired, _
                             CVar(gsStripChar(lblBankAcct.Caption, kAmpersand)), _
                             CVar(gsStripChar(lblBankAcct.Caption, kAmpersand))
                bSetFocus txtBankAcct
                bCopyBatchValues = False
                Exit Function
            End If
        End If

        .SetColumnValue "TranStatus", 2     'Pending
        .SetColumnValue "TranType", kTranTypeCMDE
    End With

    bCopyBatchValues = True
    
End Function

Public Function bSetFocus(oControl As Object) As Boolean
    On Error GoTo ExpectedErrorRoutine

    bSetFocus = False

    If Not (oControl Is Nothing) Then
        If oControl.Enabled = True Then
            If oControl.Visible = True Then
                On Error Resume Next
                oControl.SetFocus
            End If
        End If
    End If
    
    bSetFocus = True
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSetFocus", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub GetNextDepNumber()
'+++ VB/Rig Skip +++
    Dim iRetVal         As Integer
    Dim lNextDepNo      As Long

    On Error GoTo ExpectedErrorRoutine

    If txtDepSlipNumber.Enabled And Len(Trim(txtDepSlipNumber)) = 0 And moDmCashTranLog.State <> kDmStateNone Then
        ' get next dep #
        With moClass.moAppDB
            .SetInParam msCompanyID
            .SetInParam muCMCashAcct.CashAcctKey

            .SetOutParam iRetVal                          'return code
            .SetOutParam lNextDepNo                       'next deposit #
            .ExecuteSP ("spCMGetNextDepNo")

            iRetVal = .GetOutParam(3)                     'get return code
            lNextDepNo = .GetOutParam(4)                  'get next dep #

            .ReleaseParams                                'release parameters
        End With

        If iRetVal = 1 Then
            txtDepSlipNumber = lNextDepNo
            ZeroFill
        End If
    End If

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "GetNextDepNumber"
    gClearSotaErr
    Exit Sub

End Sub

Private Sub SetupBatchType()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sCaption As String
    Dim iSecurityLevel As Integer
    
    'Default to hidden, show if Cash Receipts task.
    cmdPrintChecks.Visible = False
    
    Select Case mlBatchType
        
        Case kBatchTypeARIN             'Enter Invoices
            
          'Set Form and Transaction Button Captions
            Me.Caption = gsLocStr(ksProcessInvoice, moClass)
            cmdTransactions.Caption = gsLocStr(kEnterInvoice, moClass)
            
            If moClass.moFramework.GetSecurityEventPerm("ARMERGEINVC", msUserID, False) <> 0 Then
                cmdMerge.Enabled = True
            Else
                cmdMerge.Enabled = False
            End If
            
            'If called from the PA module then set up the form accordingly
            If moClass.mbCalledFromPA Then
              Me.Caption = "Process Project Invoices"
              cmdTransactions.Caption = "&Enter Invoices"
              'cmdPrintInvoices.Caption = "Print Project &Invoices..."
              cmdSelect.Caption = "&Generate Project Invoices..."
              
              cmdSelectShipments.Visible = False    'Hide "Select Shipments..." Button
              cmdMerge.Visible = False              'Hide "Merge Shipments..." Button
              
              cmdTransactions.Left = 6850
              cmdPrintInvoices.Left = cmdTransactions.Left
              cmdSelect.Left = cmdTransactions.Left
              cmdBatchRgstrPost.Left = cmdTransactions.Left
              
              cmdTransactions.Width = cmdTransactions.Width + 100
              cmdPrintInvoices.Width = cmdTransactions.Width
              cmdSelect.Width = cmdTransactions.Width
              cmdBatchRgstrPost.Width = cmdTransactions.Width
              
              cmdTransactions.Top = cmdSelectShipments.Top
              cmdPrintInvoices.Top = cmdTransactions.Top + 585
              cmdBatchRgstrPost.Top = cmdPrintInvoices.Top + 585
              
              ' This section holds the department and bank account fields - comment out to restore them
              Frame3.Visible = False
              fraBatchTran.Top = Frame3.Top
              fraTranGrid.Top = Frame3.Top
              miOldFormHeight = Me.Height - Frame3.Height
              miMinFormHeight = miOldFormHeight
              Me.Height = miOldFormHeight
              Frame3.Height = 0
              'End of dept. and bank acct section
            Else
                PositionButtons cmdSelect, cmdSelectShipments, cmdMerge, cmdTransactions, cmdPrintInvoices, cmdBatchRgstrPost
            End If

            HideDepositFields
            
                        
        Case kBatchTypeARCR             'Enter Cash Receipts
            
          'Set Form and Transaction Button Captions
            Me.Caption = gsLocStr(ksProcessCashReceipts, moClass)
            cmdTransactions.Caption = gsLocStr(kEnterCashReceipts, moClass)

            ShowDepositFields

            cmdSelect.Visible = True           'Select SO Payments
            cmdSelect.Caption = gsLocStr(kARSelectSOPmts, moClass)
            
            If moClass.moSysSession.IsModuleActivated(kModuleSO) Then
                cmdSelect.Enabled = True
            Else
                cmdSelect.Enabled = False
            End If
            
            cmdSelectShipments.Visible = False  'Hide "Select Shipments..." Button
            cmdMerge.Visible = False            'Hide "Merge Shipments..." Button
            
            cmdPrintChecks.Visible = True
            'disable until a cash account is entered that allows check printing
            cmdPrintChecks.Enabled = False
                              
            If muAROptions.IntegrateCM Then
                ' create 'Deposit Slip' button
                cmdPrintInvoices.Caption = "Deposit &Slip..."
                PositionButtons cmdSelect, cmdTransactions, cmdPrintInvoices, cmdPrintChecks, cmdBatchRgstrPost
            Else
                cmdPrintInvoices.Visible = False    'Hide "Print Invoices..." Button
                PositionButtons cmdSelect, cmdTransactions, cmdPrintChecks, cmdBatchRgstrPost
            End If
            
            
        Case kBatchTypeARPA                     'Memo/Payment Applications
            
          'Set Form and Transaction Button Captions
            'Me.Caption = gsLocStr(ksProcessMemoPmt, moClass)
            Me.Caption = gsLocStr(ksAppMemoCaption, moClass)

            cmdTransactions.Caption = gsLocStr(kApplyMemoPmts, moClass)

            HideDepositFields
            HideBankAcct
            HideControlAmtFields
            
            
            cmdPrintInvoices.Visible = False   'Hide "Print Invoices..." Button
            cmdSelect.Visible = False          'Hide "Select Recurring..." Button
            cmdSelectShipments.Visible = False 'Hide "Select Shipments..." Button
            cmdMerge.Visible = False           'Hide "Merge Shipments..." Button

            curBatchCtrlAmt.Protected = True   'No batch totals needed for pmt applications
            curBatchCtrlAmt.Enabled = False

            PositionButtons cmdTransactions, cmdBatchRgstrPost
            
        Case kBatchTypeARRA
        
            'Setup Apply Memo Applications Button
            sCaption = gsBuildString(ksProcessReverseAppl1, moClass.moAppDB, moClass.moSysSession)
            Me.Caption = sCaption
            Me.txtBatchDesc = sCaption

            sCaption = gsBuildString(ksProcessReverseAppl, moClass.moAppDB, moClass.moSysSession, "Memo")
            cmdTransactions.Caption = sCaption
            
            iSecurityLevel = giGetSecurityLevel(moClass.moFramework, ktskReverseMemoAppl)

            If (iSecurityLevel = sotaTB_SUPERVISORY) Or (iSecurityLevel = sotaTB_NORMAL) Then
                cmdTransactions.Enabled = True
            Else
                cmdTransactions.Enabled = False
            End If
            
            'Setup Apply Payment Applications Button
            sCaption = gsBuildString(ksProcessReverseAppl, moClass.moAppDB, moClass.moSysSession, "Payment")
            cmdPrintInvoices.Caption = sCaption
            
            iSecurityLevel = giGetSecurityLevel(moClass.moFramework, ktskReversePmtAppl)

            If (iSecurityLevel = sotaTB_SUPERVISORY) Or (iSecurityLevel = sotaTB_NORMAL) Then
                cmdPrintInvoices.Enabled = True
            Else
                cmdPrintInvoices.Enabled = False
            End If

            'Setup Register\Post Button
            iSecurityLevel = giGetSecurityLevel(moClass.moFramework, ktskReverseApplPrintPost)

            If (iSecurityLevel = sotaTB_SUPERVISORY) Or (iSecurityLevel = sotaTB_NORMAL) Then
                cmdBatchRgstrPost.Enabled = True
            Else
                cmdBatchRgstrPost.Enabled = False
            End If

            'HideDepositFields
            'HideBankAcct
            HideControlAmtFields
            Frame3.Visible = False
            
            'cmdPrintInvoices.Visible = False   'Hide "Print Invoices..." Button
            cmdSelect.Visible = False          'Hide "Select Recurring..." Button
            cmdSelectShipments.Visible = False 'Hide "Select Shipments..." Button
            cmdMerge.Visible = False           'Hide "Merge Shipments..." Button

            curBatchCtrlAmt.Protected = True   'No batch totals needed for pmt applications
            curBatchCtrlAmt.Enabled = False

            PositionButtons cmdTransactions, cmdPrintInvoices, cmdBatchRgstrPost
        

        Case kBatchTypeARCO                     'Sales Commissions
            
          'Set Form and Transaction Button Captions
            Me.Caption = gsLocStr(kARProcessComm, moClass)
            cmdTransactions.Caption = gsLocStr(kAREditComm, moClass)
            cmdSelect.Caption = gsLocStr(ksSelComm, moClass)
            
            HideDepositFields
            HideBankAcct

            cmdPrintInvoices.Visible = False    'Hide "Print Invoices..." Button
            cmdSelectShipments.Visible = False  'Hide "Select Shipments..." Button
            cmdMerge.Visible = False            'Hide "Merge Shipments..." Button

            PositionButtons cmdSelect, cmdTransactions, cmdBatchRgstrPost

        Case kBatchTypeARFC                     'Finance charges
            Me.Caption = gsLocStr(ksProcFinChg, moClass)
            cmdTransactions.Caption = gsLocStr(ksEditView, moClass)
            cmdSelect.Caption = gsLocStr(ksGen, moClass)
            
            cmdSelectShipments.Visible = False  'Hide "Select Shipments..." Button
            cmdMerge.Visible = False            'Hide "Merge Shipments..." Button

            HideDepositFields
            HideBankAcct

            PositionButtons cmdSelect, cmdTransactions, cmdPrintInvoices, cmdBatchRgstrPost

        Case Else                    'Incorrect batch type

            giSotaMsgBox Me, moClass.moSysSession, kmsgUnkBatchType, mlBatchType
    
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBatchType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub GetDefaults()
'+++ VB/Rig Skip +++
    Dim iRetVal             As Integer      ' sp return value
    Dim sCurrID             As String       ' default currency
    Dim sStartDate          As String       ' fiscal per/year starting date
    Dim sEndDate            As String       ' fiscal per/year ending date
    Dim sModuleID           As String       ' 2 character module id
    Dim sBatchTypeID        As String       ' batch type id
    Dim sDefBatchCmnt       As String       ' localized default batch comment
    Dim lBatchOvrdSegKey    As Long         ' batch override segment key
    Dim iAllowPrivate       As Integer      ' allow private batches?
    Dim lDefCashKey         As Long         ' default cash acct key
    Dim iPrintInvcs         As Integer      ' print invoices?
    Dim iUseSper            As Integer      ' use salespersons?
    Dim iIntegrateCM        As Integer      ' integrate with CM?
    Dim iCaseSens           As Integer      ' is server case sensitive
    Dim iSOActive           As Integer      ' is SO module active?

    On Error GoTo ExpectedErrorRoutine

    ' set up defaults
    With muAROptions
        .IsEmpty = True
        .BatchOvrdSegKey = 0
        .AllowMultBatch = True
        .AllowPrivateBatch = False
        .DfltBankAcctKey = 0
        .PrintInvoices = False
        .UseSper = False
        .IntegrateCM = False
        .SOActive = False
    End With

    With muCIBatchTypCompany
        .IsEmpty = True
        .ModuleNo = kModuleAR
        .ModuleID = Empty
        .BatchType = mlBatchType
        .BatchTypeID = Empty
        .DfltBatchCmnt = Empty
    End With

    With moClass.moAppDB
        .SetInParam msCompanyID                     ' pass company id
        .SetInParam kModuleAR                       ' module No
        .SetInParam mlBatchType                     ' batch type
        .SetInParam moClass.moSysSession.Language   ' language id

        .SetOutParam iRetVal                        ' return value
        .SetOutParam sCurrID
        .SetOutParam sStartDate
        .SetOutParam sEndDate
        .SetOutParam sModuleID
        .SetOutParam sBatchTypeID
        .SetOutParam sDefBatchCmnt
        .SetOutParam lBatchOvrdSegKey
        .SetOutParam iAllowPrivate
        .SetOutParam lDefCashKey
        .SetOutParam iPrintInvcs
        .SetOutParam iUseSper
        .SetOutParam iIntegrateCM
        .SetOutParam iCaseSens
        .SetOutParam iSOActive

        .ExecuteSP ("spARBatchDefs")                ' run sp

        iRetVal = .GetOutParam(5)                   ' get output from SP
        sCurrID = .GetOutParam(6)
        sStartDate = .GetOutParam(7)
        sEndDate = .GetOutParam(8)
        sModuleID = .GetOutParam(9)
        sBatchTypeID = .GetOutParam(10)
        sDefBatchCmnt = .GetOutParam(11)
        lBatchOvrdSegKey = .GetOutParam(12)
        iAllowPrivate = .GetOutParam(13)
        lDefCashKey = .GetOutParam(14)
        iPrintInvcs = .GetOutParam(15)
        iUseSper = .GetOutParam(16)
        iIntegrateCM = .GetOutParam(17)
        iCaseSens = .GetOutParam(18)
        iSOActive = .GetOutParam(19)

        .ReleaseParams
    End With

    'On error, set unload flag.
    If iRetVal = 0 Then
        mbUnload = True
        Exit Sub
    End If

    'Set up currency.
    msHomeCurrID = sCurrID
    gbSetCurrCtls moClass, msHomeCurrID, muHomeCurrInfo, curBatchTotal, curBatchCtrlAmt, curBatchVariance

    'Save fiscal start/end dates.
    Mid(sStartDate, 5, 1) = "-"
    Mid(sStartDate, 8, 1) = "-"
    Mid(sEndDate, 5, 1) = "-"
    Mid(sEndDate, 8, 1) = "-"
    mdStartDate = Format(sStartDate, gsGetLocalVBDateMask())
    mdEndDate = Format(sEndDate, gsGetLocalVBDateMask())

    'Save AR Options data.
    With muAROptions
        .IsEmpty = False
        .BatchOvrdSegKey = lBatchOvrdSegKey
        .AllowPrivateBatch = (iAllowPrivate = 1)
        .DfltBankAcctKey = lDefCashKey
        .PrintInvoices = (iPrintInvcs = 1)
        .UseSper = (iUseSper = 1)
        .IntegrateCM = (iIntegrateCM = 1)
        .SOActive = (iSOActive = 1)
    End With

    'Show Private Batches (if allowed from AR Options).
    If Not muAROptions.AllowPrivateBatch Then
        chkBatchPrivate.Enabled = False
    Else
        chkBatchPrivate.Enabled = True
    End If

    'Save Batch Type information.
    With muCIBatchTypCompany
        .IsEmpty = False
        .ModuleID = sModuleID
        .BatchTypeID = sBatchTypeID
        .DfltBatchCmnt = sDefBatchCmnt
    End With

    'Intellisol start
    If moClass.mbCalledFromPA Then
        muCIBatchTypCompany.DfltBatchCmnt = "PA Invoices"
    End If
    'Intellisol end

    'Save case sensitive flag.
    mbCaseSens = (iCaseSens = 1)

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "GetDefaults"
    gClearSotaErr
    mbUnload = True
    Exit Sub

End Sub

Private Function bIsValidDepositDate() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bIsValidDepositDate = True

    ' if field is disabled because of options, exit validation with
    ' a valid status
    If txtDepositDate.Enabled = False Then Exit Function

    If Len(Trim(txtDepositDate)) = 0 Then
        bIsValidDepositDate = False
        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, sRemoveAmpersand(lblDepositDate)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Not gbValidLocalDate(txtDepositDate.Text) Then     'valid Local date
        bIsValidDepositDate = False
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidDate
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDepositDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidDepSlipNum() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bIsValidDepSlipNum = True

    ' if field is disabled because of options, exit validation with
    ' a valid status
    If txtDepSlipNumber.Enabled = False Then Exit Function

    If Len(Trim(txtDepSlipNumber)) = 0 Then
        bIsValidDepSlipNum = False
        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, sRemoveAmpersand(lblDepSlipNumber)
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    ZeroFill

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDepSlipNum", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bCanEditBatch() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCanEditBatch = False

    'Can't edit if batch is posted, posting or deleted

    If muCIBatchLogSysDetl.Status = kvPosting Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchUseNoPost
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    If muCIBatchLogSysDetl.Status = kvPosted Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchWasPosted
        HandleToolBarClick kTbCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    If muCIBatchLogSysDetl.PostStatus = kvDeleted Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchDeleted
        HandleToolBarClick kTbCancel
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    bCanEditBatch = True

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCanEditBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub HideBankAcct()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lblBankAcct.Visible = False
    txtBankAcct.Visible = False
    navBankAcct.Visible = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HideBankAcct", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub HideControlAmtFields()

    fraBatchTran.Visible = False

End Sub
Private Sub ZeroFill()
    Dim sNumber     As String

    sNumber = txtDepSlipNumber

    If Len(Trim(sNumber)) = 0 Then Exit Sub

    If Not IsNumeric(sNumber) Then Exit Sub

    txtDepSlipNumber = Right("0000000000" & LTrim(sNumber), 10)

End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDmForm, moDmCashTranLog
                moFormCust.ApplyFormCust
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If


Private Sub cmdPrintChecks_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPrintChecks, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintChecks_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintChecks_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPrintChecks, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintChecks_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMerge_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdMerge, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMerge_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdMerge_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdMerge, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdMerge_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdBatchRgstrPost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdBatchRgstrPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdBatchRgstrPost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdBatchRgstrPost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdBatchRgstrPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdBatchRgstrPost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdTransactions_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdTransactions, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdTransactions_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdTransactions_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdTransactions, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdTransactions_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintInvoices_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPrintInvoices, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintInvoices_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPrintInvoices_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPrintInvoices, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPrintInvoices_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectShipments_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelectShipments, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectShipments_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectShipments_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelectShipments, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectShipments_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtCashAllowMCDep_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCashAllowMCDep, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashAllowMCDep_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashAllowMCDep_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCashAllowMCDep, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashAllowMCDep_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashAllowMCDep_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCashAllowMCDep, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashAllowMCDep_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashAllowMCDep_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCashAllowMCDep, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashAllowMCDep_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashCurrID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtCashCurrID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashCurrID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashCurrID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtCashCurrID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashCurrID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashCurrID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtCashCurrID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashCurrID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtCashCurrID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtCashCurrID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtCashCurrID_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatchNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatchNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepSlipNumber_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDepSlipNumber, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDepSlipNumber_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepSlipNumber_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDepSlipNumber, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDepSlipNumber_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBankAcct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBankAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBankAcct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBankAcct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBankAcct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBankAcct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBankAcct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBankAcct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBankAcct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchOvrdSegValue_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatchOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchOvrdSegValue_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchOvrdSegValue_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatchOvrdSegValue, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchOvrdSegValue_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchOvrdSegValue_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatchOvrdSegValue, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchOvrdSegValue_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchHoldReason_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatchHoldReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchHoldReason_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchHoldReason_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatchHoldReason, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchHoldReason_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchHoldReason_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatchHoldReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchHoldReason_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchHoldReason_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatchHoldReason, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchHoldReason_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchDesc_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatchDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchDesc_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchDesc_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatchDesc, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchDesc_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchDesc_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatchDesc, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchDesc_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchStatus_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatchStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchStatus_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchStatus_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatchStatus, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchStatus_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchStatus_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatchStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchStatus_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchStatus_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtBatchStatus, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchStatus_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curBatchTotal_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curBatchTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curBatchTotal, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curBatchTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchTotal_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curBatchTotal, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchTotal_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchVariance_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curBatchVariance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchVariance_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchVariance_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curBatchVariance, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchVariance_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchVariance_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curBatchVariance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchVariance_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchVariance_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curBatchVariance, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchVariance_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchCtrlAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curBatchCtrlAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchCtrlAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curBatchCtrlAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curBatchCtrlAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curBatchCtrlAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBatchHold_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkBatchHold, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkBatchHold_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBatchPrivate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkBatchPrivate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkBatchPrivate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkBatchPrivate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkBatchPrivate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkBatchPrivate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepositDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDepositDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDepositDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepositDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDepositDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDepositDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDepositDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDepositDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDepositDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchPostDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtBatchPostDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchPostDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchPostDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtBatchPostDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchPostDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtBatchPostDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtBatchPostDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtBatchPostDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If


'Intellisol start
Private Sub LoadGenProjInvoices()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************************
'   Description: Call Generate Project Invoices Object
'************************************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    Dim lBatchKey           As Long      'current batch Key
    Dim oGenerate             As Object  'Generate Proj Invoices Object
    Dim iStatus             As Integer   'Current batch status
    Dim iHookType           As Integer   'winhook type
    Dim lLockID             As Long      'Batch lock ID
    Dim iNewCount           As Integer   'For new value of update counter for form
    Dim iTemp               As Integer   'For new value of update counter for form
    Dim lIndex As Long
    
    ' update current batch status
    DisplayBatchStatus

    ' if batch is not in an editable state, don't let user edit it
    If Not bCanEditBatch() Then Exit Sub

  'Check if batch is in a Status you can save it
    If Not bCanSaveBatch() Then Exit Sub
  
    'Save current batch Header exit on error
    If Not bSaveBatch() Then Exit Sub

    'Get batch Key, batch ID
    lBatchKey = moDmForm.GetColumnValue("BatchKey")
    muCIBatchLog.BatchID = FormatBatchID(muCIBatchTypCompany.ModuleID, muCIBatchTypCompany.BatchTypeID, (txtBatchNo))

    ' Lock batch
    lLockID = lLockBatch(lBatchKey)

    'if not already in use, mark it as in use.  If we can't mark it as in use,
    ' we can't let user select transactions
    If iStatus <> kvInUse Then
        If iUpdateCIBatchLogSysDetl(kvInUse, False) = kDmFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
    DisplayBatchStatus

  'Reset register printed flag
    If Not bResetRegPrintFlag() Then Exit Sub

    ' disable winhook
    ' **PRESTO ** iHookType = WinHook1.KeyboardHook
    ' **PRESTO ** WinHook1.KeyboardHook = shkKbdDisabled

    ' disable form
    Me.Enabled = False

      'Run Select Recurring program
    Select Case muCIBatchTypCompany.BatchType

        'Invoices - Generate Project Invoices
        Case kBatchTypeARIN
            Set oGenerate = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                              kclsPAGenerateInvoices, ktskPAGenerateInvoices, _
                              kAOFRunFlags, kContextAOF)
            If Not oGenerate Is Nothing Then
                iTemp = oGenerate.Init(lBatchKey, muCIBatchLog.BatchID)
            ''JPT 09/22/00 added to fix problem reports JTID-4P6KSS, AMER-4NXJZ6
                iNewCount = moDmForm.GetColumnValue("UpdateCounter")
                iNewCount = iNewCount + iTemp
                '02/05/01 JPT modified to correct problem report JTID-4TJK5T
                If iTemp <> 0 Then
                    moDmForm.SetColumnValue "UpdateCounter", iNewCount
                End If
            End If

        Case Else

            giSotaMsgBox Me, moClass.moSysSession, kBadRecurrBatchType, muCIBatchTypCompany.BatchType
    End Select
  
    'DROP TABLE HERE TO MARK batch AS IN use
'    DropLock lBatchKey
    DropLock lLockID
    
    For lIndex = 1 To moSotaObjects.Count
        If moSotaObjects.Item(lIndex) Is oGenerate Then
            giCollectionDel moClass.moFramework, moSotaObjects, lIndex
        End If
    Next lIndex
    
    Set oGenerate = Nothing
  
    'Remove In-Use Status.
    RefreshTranGrid

    Me.Enabled = True
    If Me.Visible Then
        Me.SetFocus
    End If

    DisplayBatchStatus

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "LoadGenProjInvoices"
    If Not Me.Enabled Then
        Me.Enabled = True
        If Me.Visible Then
            Me.SetFocus
        End If
    End If
    ' in case of error, drop logical lock
    DropLock lLockID
    DisplayBatchStatus
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadGenProjInvoices", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'Intellisol end

Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Function DMPostSave(oDm As clsDmForm) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Called from the Data Manager within a database transaction for
'       PostSave
'
'   Return Values:
'       True    - success
'       False   - failure
'********************************************************************
    
    Dim iRetVal As Integer
    
    DMPostSave = False
    
        If oDm Is moDmForm Then
            'Update the tciBatchLog.BatchCmnt with the value in tapBatch.BatchCmnt
            'so the batch form lookup can display the BatchCmnt
            With oClass.moAppDB
              .SetInParam moDmForm.GetColumnValue("BatchKey")
              .SetOutParam iRetVal                          'return code
              .SetInParam CInt(kModuleAR)                   'Module No
              .ExecuteSP ("spciUpdteCIBatchLogCmnt")
              iRetVal = .GetOutParam(2)                     'get return code
              .ReleaseParams                                'release parameters
            End With
        End If

    If iRetVal = kRetError Then
        ' Unexpected Stored Procedure Return Value: {0}
        giSotaMsgBox Me, oClass.moSysSession, kmsgUnexpectedSPReturnValue, _
                    "0  spciUpdteCIBatchLogCmnt"
        Exit Function
    End If

    DMPostSave = True
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMPostSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub InitMergeTempTable(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
'    Static bTempExist As Boolean    'RKL DEJ 2016-07-21 commented out
    
    If Not lcbTempExist Then  'RKL DEJ 2016-07-21 Replaced bTempExist with lcbTempExist
        sSQL = "CREATE TABLE #InvcToMerge ("
        sSQL = sSQL & " InvcKey INTEGER NOT NULL,"
        sSQL = sSQL & " SOKey INTEGER NULL)"
        
        lcbTempExist = True   'RKL DEJ 2016-07-21 Replaced bTempExist with lcbTempExist
    Else
        sSQL = "TRUNCATE TABLE #InvcToMerge"
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "INSERT #InvcToMerge (InvcKey)"
    sSQL = sSQL & " SELECT invc.InvcKey"
    sSQL = sSQL & " FROM tarPendInvoice invc WITH (NOLOCK)"
    sSQL = sSQL & "   LEFT OUTER JOIN tarPendCustPmtAppl pmtappl WITH (NOLOCK)"
    sSQL = sSQL & "     ON invc.InvcKey = pmtAppl.ApplyToInvcKey"
    sSQL = sSQL & "     AND COALESCE(pmtappl.SalesOrderPmtKey, 0) = 0"
    sSQL = sSQL & " WHERE invc.BatchKey = " & lBatchKey
    sSQL = sSQL & " AND invc.TranType = " & kTranTypeARIN
    sSQL = sSQL & " AND invc.CurrExchRate = 1"
    sSQL = sSQL & " AND COALESCE(pmtappl.ApplyToInvcKey, 0) = 0"
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "UPDATE #InvcToMerge SET SOKey = subqry.SOKey"
    sSQL = sSQL & " FROM #InvcToMerge"
    sSQL = sSQL & "   JOIN (SELECT #InvcToMerge.InvcKey, MIN(SOLine.SOKey) AS SOKey"
    sSQL = sSQL & "         FROM #InvcToMerge"
    sSQL = sSQL & "           JOIN tarPendInvoice invc WITH (NOLOCK)"
    sSQL = sSQL & "             ON #InvcToMerge.InvcKey = invc.InvcKey"
    sSQL = sSQL & "           JOIN tarInvoiceDetl invcdtl WITH (NOLOCK)"
    sSQL = sSQL & "             ON invc.invckey = invcdtl.InvcKey"
    sSQL = sSQL & "           JOIN tsoSOLine soline WITH (NOLOCK)"
    sSQL = sSQL & "             ON invcdtl.SOLineKey = soline.SOLineKey"
    sSQL = sSQL & "         WHERE invc.BatchKey = " & lBatchKey
    sSQL = sSQL & "         AND invc.TranType = " & kTranTypeARIN
    sSQL = sSQL & "         AND invc.CurrExchRate = 1"
    sSQL = sSQL & "         GROUP BY #InvcToMerge.InvcKey"
    sSQL = sSQL & "         HAVING COUNT(DISTINCT soline.SOKey) = 1) AS subqry"
    sSQL = sSQL & "     ON #InvcToMerge.InvcKey = subqry.InvcKey"
    
    moClass.moAppDB.ExecuteSQL sSQL
     
    sSQL = "DELETE #InvcToMerge"
    sSQL = sSQL & " WHERE COALESCE(SOKey, 0) = 0"
    sSQL = sSQL & " OR SOKey IN (SELECT SOKey"
    sSQL = sSQL & "            FROM #InvcToMerge"
    sSQL = sSQL & "            GROUP BY SOKey"
    sSQL = sSQL & "            HAVING COUNT(SOKey) = 1)"
     
    moClass.moAppDB.ExecuteSQL sSQL
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitMergeTempTable", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
 
Private Function sGetUnMergedInvcText(lBatchKey As Long) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim rs As Object
    Dim sSQL As String
    Dim sStr As String
    Dim sInvcNos As String
    Dim iCounter As Integer
    
    sSQL = sSQL & "SELECT TranID FROM tarPendInvoice WITH (NOLOCK)"
    sSQL = sSQL & " WHERE TranType = " & kTranTypeARIN
    sSQL = sSQL & " AND BatchKey = " & lBatchKey
    sSQL = sSQL & " AND InvcKey NOT IN (SELECT InvcKey FROM #InvcToMerge)"
        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    iCounter = 1
    rs.MoveFirst
    
    sInvcNos = vbCrLf
    
    Do While Not rs.IsEOF
        sStr = (gsGetValidStr(rs.Field("TranID")))
        
        sInvcNos = sInvcNos & vbCrLf & sStr
        
        If iCounter = 16 Then
            mbInvcOverflow = True
            Exit Do
        End If
        
        iCounter = iCounter + 1
        rs.MoveNext
    Loop
    
    sGetUnMergedInvcText = sInvcNos & vbCrLf & vbCrLf

    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetUnMergedInvcText", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
End Function


Private Sub CheckAvaTax()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Avatax Integration: Check the availability of Avatax for the CompanyID.
'***********************************************************************

On Error GoTo ExpectedErrorRoutine

    'Assume AvaTax feature not configured
    mbAvaTaxEnabled = False

    mbAvaTaxEnabled = gbGetValidBoolean(moClass.moAppDB.Lookup("AvaTaxEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(msCompanyID)))

    If mbAvaTaxEnabled Then
        On Error GoTo ExpectedErrorRoutine_AvaTaxObj
        Set moAvaTax = Nothing
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'This is how we find out if AvaTax is not installed.
'The missing table (err 4050) will tell us not to fire the AvaTax code
'throughout the program.

    Select Case Err.Number
    
        Case 4050 ' SQL Object Not Found
            'tavConfiguration not found meaning AvaTax not installed.
            'No Error action required.

        Case Else
            'Unexpected Error reading tavConfiguration
            giSotaMsgBox Me, moClass.moSysSession, kmsgtavConfigurationError

    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine_AvaTaxObj:
'Expected Error:
'If the DB Avatax table was found, AvaTax is likely used for this Site.
'But when creating the local client object, the object was likely not installed
'This is not a good error, let the administrator know to check that AvaTax was properly implemented.

    'kmsgAvaObjectError = "The AvaTax options are enabled for this company, but the client object AVZDADL1 was not found on this machine.  AvaTax will not be available during this process."
    giSotaMsgBox Me, moClass.moSysSession, kmsgAvaObjectError

    mbAvaTaxEnabled = False

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckAvaTax", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


'**************************************************************************
'**************************************************************************
'RKL DEJ 2016-07-21 (START)
'**************************************************************************
'**************************************************************************
Private Sub InitBSOMergeTempTable(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
'    Static bTempExist As Boolean
    
    If Not lcbTempExist Then
        sSQL = "CREATE TABLE #InvcToMerge ("
        sSQL = sSQL & " InvcKey INTEGER NOT NULL,"
        sSQL = sSQL & " SOKey INTEGER NULL)"
        
        lcbTempExist = True
    Else
        sSQL = "TRUNCATE TABLE #InvcToMerge"
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "INSERT #InvcToMerge (InvcKey)"
    sSQL = sSQL & " SELECT invc.InvcKey"
    sSQL = sSQL & " FROM tarPendInvoice invc WITH (NOLOCK)"
    sSQL = sSQL & "   LEFT OUTER JOIN tarPendCustPmtAppl pmtappl WITH (NOLOCK)"
    sSQL = sSQL & "     ON invc.InvcKey = pmtAppl.ApplyToInvcKey"
    sSQL = sSQL & "     AND COALESCE(pmtappl.SalesOrderPmtKey, 0) = 0"
    sSQL = sSQL & " WHERE invc.BatchKey = " & lBatchKey
    sSQL = sSQL & " AND invc.TranType = " & kTranTypeARIN
    sSQL = sSQL & " AND invc.CurrExchRate = 1"
    sSQL = sSQL & " AND COALESCE(pmtappl.ApplyToInvcKey, 0) = 0"
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "UPDATE #InvcToMerge SET SOKey = subqry.SOKey"
    sSQL = sSQL & " FROM #InvcToMerge"
    sSQL = sSQL & "   JOIN (SELECT #InvcToMerge.InvcKey, MIN(BSO.SOKey) AS SOKey"    'RKL DEJ 2016-07-21 Changed MIN(SOLine.SOKey) to MIN(BSO.SOKey)
    sSQL = sSQL & "         FROM #InvcToMerge"
    sSQL = sSQL & "           JOIN tarPendInvoice invc WITH (NOLOCK)"
    sSQL = sSQL & "             ON #InvcToMerge.InvcKey = invc.InvcKey"
    sSQL = sSQL & "           JOIN tarInvoiceDetl invcdtl WITH (NOLOCK)"
    sSQL = sSQL & "             ON invc.invckey = invcdtl.InvcKey"
    sSQL = sSQL & "           JOIN tsoSOLine soline WITH (NOLOCK)"
    sSQL = sSQL & "             ON invcdtl.SOLineKey = soline.SOLineKey"
    
    '*******************************************************
    'RKL DEJ 2016-07-21 (START)
    '*******************************************************
    sSQL = sSQL & "           JOIN tsoSalesOrder SO with(NOLock) "
    sSQL = sSQL & "             ON soline.SOKey = SO.SOKey "
    sSQL = sSQL & "             and so.TranType = 801 "
    
    sSQL = sSQL & "           JOIN tsoSalesOrder BSO with(NOLock) "
    sSQL = sSQL & "             On SO.BlnktSOKey = BSO.SOKey "
    sSQL = sSQL & "             and BSO.TranType = 802 "
    '*******************************************************
    'RKL DEJ 2016-07-21 (STOP)
    '*******************************************************
    
    sSQL = sSQL & "         WHERE invc.BatchKey = " & lBatchKey
    sSQL = sSQL & "         AND invc.TranType = " & kTranTypeARIN
    sSQL = sSQL & "         AND invc.CurrExchRate = 1"
    sSQL = sSQL & "         GROUP BY #InvcToMerge.InvcKey"
    sSQL = sSQL & "         HAVING COUNT(DISTINCT BSO.SOKey) = 1) AS subqry"     'RKL DEJ 2016-07-21 Changed soline.SOKey to BSO.SOKey
    sSQL = sSQL & "     ON #InvcToMerge.InvcKey = subqry.InvcKey"
    
    moClass.moAppDB.ExecuteSQL sSQL
     
    sSQL = "DELETE #InvcToMerge"
    sSQL = sSQL & " WHERE COALESCE(SOKey, 0) = 0"
    sSQL = sSQL & " OR SOKey IN (SELECT SOKey"
    sSQL = sSQL & "            FROM #InvcToMerge"
    sSQL = sSQL & "            GROUP BY SOKey"
    sSQL = sSQL & "            HAVING COUNT(SOKey) = 1)"
     
    moClass.moAppDB.ExecuteSQL sSQL
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitBSOMergeTempTable", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'**************************************************************************
'**************************************************************************
'RKL DEJ 2016-07-21 (STOP)
'**************************************************************************
'**************************************************************************

