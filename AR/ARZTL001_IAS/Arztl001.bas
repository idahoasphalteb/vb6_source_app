Attribute VB_Name = "basBatch"
Option Explicit

'//* KMC This should probably be in Constant.BAS.
    Public Const kMaskBatchNo = "9999999"          'batch Mask
    Public Const kFormatBatchNo = "0000000"        'Mask for formatting batch Number & ID

' tciBatchLog | Status
    Public Const kvInUse = 1           'In use
    Public Const kvOnHold = 2          'On Hold
    Public Const kvOutofBalance = 3    'Out Of Balance
    Public Const kvBalanced = 4        'Balanced
    Public Const kvPosting = 5         'Posting
    Public Const kvPosted = 6          'Posted
    Public Const kvInterrupted = 7     'Posting Interrupted
    
 ' tciBatchLog | PostStatus
    Public Const kvOpen = 0             'Open                           Str=50034 *Default*
    Public Const kvPreProc = 150        'PreProcessing Complete
    Public Const kvPostComplete = 500   'posted                         Str=50058
    Public Const kvDeleted = 999        'Deleted                        Str=50033

' tglSegment | SegmentType
    Public Const kvMain = 1             'Main                           Str=50208 *Default*
    Public Const kvSubaccount = 2       'Sub account                    Str=50207
    Public Const kvSegValidated = 3     'Validated                      Str=50133
    Public Const kvCompany = 4          'Company                        Str=50209

' AR Options that apply to batch
    Public Type AROptionsType
        IsEmpty             As Boolean  'NO AR Options record found
        AllowMultBatch      As Boolean  'Allow Multiple batches
        AllowPrivateBatch   As Boolean  'Allow Private Batches
        BatchOvrdSegKey     As Long     'GL Override segment Key for batch data entry
        DfltBankAcctKey     As Long     'Default Bank Account
        BatchOvrdSegMask    As String   'batch Override G/L Segment Mask
        BatchOvrdSegDesc    As String   'G/L Override Segment Description
        PrintInvoices       As Boolean  'Print Invoices?
        UseSper             As Boolean  'Use salespersons and commissions
        IntegrateCM         As Boolean  'Integrate AR with CM?
        SOActive            As Boolean  'Is SO module active?
    End Type
    
'  GL Override Segment Characteristics
    Public Type GLSegmentType
        IsEmpty             As Boolean  'No G/L Information for a given segment
        SegmentKey          As Long     'Segment Surrogate Key for Override segment chosen
        Mask                As String   'Mask for segmant chosen to override
        Description         As String   'Description for segment chosen to override
    End Type
    
' batch Control Structure
    Public Type CIBatchTypCompanyType
        IsEmpty             As Boolean  'batch Type Company empty
        ModuleNo            As Long     'Module Number
        ModuleID            As String   'Module id (2 Char)
        BatchType           As Long     'batch Type id (numeric)
        BatchTypeID         As String   'batch Type id (Character)
        DfltBatchCmnt       As String   'Default batch Comment
    End Type
    
    
' batch Log Structure
    Public Type CIBatchLogType
        IsEmpty             As Boolean   'batch Log Empty
        BatchKey            As Long      'Key for current batch
        BatchID             As String    'batch ID (ModuleID + batch Type ID + batch Number 0 Padded)
    End Type
    
' batch Log Sytem Detail
    Public Type CIBatchLogSysDetlType
        IsEmpty             As Boolean   'BLSD empoty
        Status              As Integer   'Status of current batch
        PostStatus          As Integer   'posting Status of current batch
        BatchTotal          As Currency  'Total For Current batch
    End Type

' Cash Account Structure
    Public Type CMCashAccountType
        CashAcctKey         As Long     'primary key
        CashAcctID          As String   'cash account id
        CurrID              As String   'curr id of bank account
        AllowMCDep          As Boolean  'allow m/c deposits
    End Type
    
Const VBRIG_MODULE_ID_STRING = "ARZTL001.BAS"

Sub Main()
'+++ VB/Rig Skip +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("Arztl001.clsBatchMNT")
        StartAppInStandaloneMode oApp, Command$()
    End If

End Sub

Public Sub GLGetSegment(oDB As Object, uGLSegment As GLSegmentType, lSegKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************************
'   Description:
'       GLGetSegment retrieves the GLSegment recordset for the current company.
'       The columns are set into the Type.
'*************************************************************************************

    Dim rs As Object
    Dim sSQL As String
    
    'New Read Defaults
    With uGLSegment
        .IsEmpty = True
        .Mask = Empty
        .Description = Empty
        .SegmentKey = 0
    End With

    sSQL = "#glGetSegmentFromKey;" & lSegKey & ";"
    Set rs = oDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    uGLSegment.IsEmpty = rs.IsEmpty
    If uGLSegment.IsEmpty Then GoTo CloseRecordset
    
    With uGLSegment
        .SegmentKey = lSegKey
        .Mask = gvCheckNull(rs.Field("Mask"))
        .Description = gvCheckNull(rs.Field("Description"))
    End With
    
CloseRecordset:
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GLGetSegment", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function FormatBatchID(sModuleID As String, sBatchTypeID As String, _
                              ByVal lBatchNo As Long) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'**********************************************************************************
' Desc:    Returns Formatted batch ID (MMTT-99999)
' Parms:   sModuleID    - MM 2 character Module ID
'          sBatchTypeID - TT Type of batch
'          lBatchNumber - batch Number
' Returns: Formatted batch ID (MMTT-99999) with Match Number padded with 0's
'**********************************************************************************
    
    FormatBatchID = sModuleID & sBatchTypeID & "-" & Format(lBatchNo, kFormatBatchNo)

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatBatchID", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basBatch"
End Function
