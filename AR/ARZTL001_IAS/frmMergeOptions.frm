VERSION 5.00
Begin VB.Form frmMergeOptions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Merge Options"
   ClientHeight    =   1920
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3405
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1920
   ScaleWidth      =   3405
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   1800
      TabIndex        =   4
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Default         =   -1  'True
      Height          =   375
      Left            =   480
      TabIndex        =   3
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Frame frmOptions 
      Caption         =   "Merge Options"
      Height          =   975
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   2895
      Begin VB.OptionButton OptnByBSO 
         Caption         =   "Merge By Blanket Sales Order"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   2655
      End
      Begin VB.OptionButton OptnBySO 
         Caption         =   "Merge By Sales Order"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Value           =   -1  'True
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmMergeOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bCanceled As Boolean
Public bMergByBSO As Boolean

Private Sub cmdCancel_Click()
    bCanceled = True
    bMergByBSO = False
    Me.Hide
End Sub

Private Sub cmdOk_Click()
    bCanceled = False
    
    bMergByBSO = OptnByBSO.Value
'    If OptnBySO.Value = True Then
'        bMergByBSO = False
'    Else
'        bMergByBSO = True
'    End If
    
    Me.Hide
End Sub
