VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmPmtTerms 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Set Up Payment Terms"
   ClientHeight    =   7290
   ClientLeft      =   2280
   ClientTop       =   1755
   ClientWidth     =   5835
   FillStyle       =   0  'Solid
   ForeColor       =   &H00404040&
   HelpContextID   =   36269
   Icon            =   "Cizmh001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   Picture         =   "Cizmh001.frx":23D2
   ScaleHeight     =   7290
   ScaleWidth      =   5835
   Begin VB.TextBox txtSalesRptDesc 
      Height          =   735
      Left            =   90
      MaxLength       =   70
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   1560
      Width           =   5640
   End
   Begin LookupViewControl.LookupView navMain 
      Height          =   285
      Left            =   3030
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   540
      WhatsThisHelpID =   14
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   5835
      _ExtentX        =   10292
      _ExtentY        =   741
      Style           =   0
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   40
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame fraDiscDueTerms 
      Caption         =   "D&iscount"
      ForeColor       =   &H00000000&
      Height          =   1800
      Left            =   90
      TabIndex        =   26
      Top             =   4905
      Width           =   5640
      Begin VB.VScrollBar SpnDiscMinDays 
         Height          =   285
         Left            =   2310
         Max             =   0
         Min             =   999
         TabIndex        =   35
         Tag             =   "0"
         Top             =   1020
         WhatsThisHelpID =   36312
         Width           =   225
      End
      Begin VB.VScrollBar SpnDiscDayOrMonth 
         Height          =   285
         Left            =   2310
         Max             =   -1
         Min             =   1
         TabIndex        =   33
         Tag             =   "0"
         Top             =   615
         WhatsThisHelpID =   36311
         Width           =   225
      End
      Begin VB.Frame fraApplyDiscountTo 
         Caption         =   "Apply Discount to:"
         Height          =   1065
         Left            =   3855
         TabIndex        =   32
         Top             =   525
         Width           =   1650
         Begin VB.CheckBox chkApplyToFreight 
            Caption         =   "Freight"
            Height          =   285
            Left            =   300
            TabIndex        =   17
            Top             =   315
            WhatsThisHelpID =   36309
            Width           =   780
         End
         Begin VB.CheckBox chkApplyToSTax 
            Caption         =   "Sales Tax"
            Height          =   285
            Left            =   300
            TabIndex        =   18
            Top             =   645
            WhatsThisHelpID =   36308
            Width           =   1035
         End
      End
      Begin VB.ComboBox cboDiscDateOption 
         Height          =   315
         Left            =   1755
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   195
         WhatsThisHelpID =   36307
         Width           =   1950
      End
      Begin SOTACalendarControl.SOTACalendar txtDiscFixedDate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   -75000
         TabIndex        =   29
         Top             =   225
         Visible         =   0   'False
         WhatsThisHelpID =   36306
         Width           =   1950
         _ExtentX        =   3440
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin NEWSOTALib.SOTANumber numDiscPct 
         Height          =   315
         Left            =   1755
         TabIndex        =   16
         Top             =   1425
         WhatsThisHelpID =   36305
         Width           =   1005
         _Version        =   65536
         _ExtentX        =   1773
         _ExtentY        =   556
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.26
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##<ILp0>#<IRp0>|.##"
         text            =   "  0.00"
         sIntegralPlaces =   3
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber numDiscMinDays 
         Height          =   285
         Left            =   1755
         TabIndex        =   15
         Top             =   1020
         WhatsThisHelpID =   36304
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##<ILp0>#"
         text            =   "  0"
         dMaxValue       =   999
         sIntegralPlaces =   3
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numDiscDayOrMonth 
         Height          =   285
         Left            =   1755
         TabIndex        =   14
         Top             =   615
         WhatsThisHelpID =   36303
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##<ILp0>#"
         text            =   "  0"
         sIntegralPlaces =   3
         sDecimalPlaces  =   0
      End
      Begin VB.Label lblDiscDayOrMonth 
         AutoSize        =   -1  'True
         Caption         =   "Months"
         Height          =   195
         Left            =   135
         TabIndex        =   28
         Top             =   675
         Width           =   525
      End
      Begin VB.Label lblDiscPct 
         AutoSize        =   -1  'True
         Caption         =   "Discount Percent"
         Height          =   195
         Left            =   150
         TabIndex        =   31
         Top             =   1440
         Width           =   1230
      End
      Begin VB.Label lblDiscMinDays 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BackStyle       =   0  'Transparent
         Caption         =   "Minimum Days"
         Height          =   195
         Left            =   135
         TabIndex        =   30
         Top             =   1080
         Width           =   1020
      End
      Begin VB.Label lblDiscDateOption 
         AutoSize        =   -1  'True
         Caption         =   "Discount Date Option"
         Height          =   195
         Left            =   135
         TabIndex        =   27
         Top             =   255
         Width           =   1530
      End
   End
   Begin VB.Frame fraPmtDueTerms 
      Caption         =   "&Due Date"
      ForeColor       =   &H00000000&
      Height          =   1440
      Left            =   90
      TabIndex        =   21
      Top             =   3390
      Width           =   5640
      Begin VB.VScrollBar SpnDueMinDays 
         Height          =   285
         Left            =   2265
         Max             =   0
         Min             =   999
         TabIndex        =   48
         Tag             =   "0"
         Top             =   975
         WhatsThisHelpID =   36297
         Width           =   225
      End
      Begin VB.VScrollBar SpnDueDayOrMonth 
         Height          =   285
         Left            =   2265
         Max             =   -1
         Min             =   1
         TabIndex        =   47
         Tag             =   "0"
         Top             =   570
         WhatsThisHelpID =   36296
         Width           =   225
      End
      Begin VB.ComboBox cboDueDateOption 
         Height          =   315
         Left            =   1710
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   150
         WhatsThisHelpID =   36295
         Width           =   1950
      End
      Begin SOTACalendarControl.SOTACalendar txtDueFixedDate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   -75000
         TabIndex        =   24
         Top             =   585
         Visible         =   0   'False
         WhatsThisHelpID =   36294
         Width           =   1950
         _ExtentX        =   3440
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin NEWSOTALib.SOTANumber numDueMinDays 
         Height          =   285
         Left            =   1710
         TabIndex        =   12
         Top             =   990
         WhatsThisHelpID =   36293
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##<ILp0>#"
         text            =   "  0"
         dMaxValue       =   999
         sIntegralPlaces =   3
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTANumber numDueDayOrMonth 
         Height          =   285
         Left            =   1710
         TabIndex        =   11
         Top             =   585
         WhatsThisHelpID =   36292
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##<ILp0>#"
         text            =   "  0"
         sIntegralPlaces =   3
         sDecimalPlaces  =   0
      End
      Begin VB.Label lblDueDateOption 
         Caption         =   "Due Date Option"
         Height          =   195
         Left            =   150
         TabIndex        =   22
         Top             =   270
         Width           =   1245
      End
      Begin VB.Label lblDueDayOrMonth 
         Caption         =   "Months"
         Height          =   195
         Left            =   150
         TabIndex        =   23
         Top             =   660
         Width           =   615
      End
      Begin VB.Label lblDueMinDays 
         Caption         =   "Minimum Days"
         Height          =   195
         Left            =   150
         TabIndex        =   25
         Top             =   1020
         Width           =   1050
      End
   End
   Begin VB.Frame Frame2 
      Height          =   990
      Left            =   90
      TabIndex        =   19
      Top             =   2310
      Width           =   5640
      Begin VB.CheckBox chkCOD 
         Caption         =   "COD"
         Height          =   225
         Left            =   135
         TabIndex        =   8
         Top             =   225
         WhatsThisHelpID =   17774000
         Width           =   1050
      End
      Begin VB.ComboBox cboCODLabelFormID 
         Height          =   315
         Left            =   1695
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   525
         WhatsThisHelpID =   36287
         Width           =   1950
      End
      Begin VB.Label lblCODLabelFormID 
         AutoSize        =   -1  'True
         Caption         =   "COD Label Form"
         Height          =   195
         Left            =   435
         TabIndex        =   20
         Top             =   600
         Width           =   1170
      End
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   41
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   46
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtPmtTermsID 
      Height          =   285
      Left            =   1500
      TabIndex        =   3
      Top             =   525
      WhatsThisHelpID =   36276
      Width           =   1500
      _Version        =   65536
      _ExtentX        =   2646
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   15
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6900
      WhatsThisHelpID =   73
      Width           =   5835
      _ExtentX        =   10292
      _ExtentY        =   688
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtDescription 
      Height          =   315
      Left            =   1500
      TabIndex        =   5
      Top             =   840
      WhatsThisHelpID =   36286
      Width           =   3000
      _Version        =   65536
      _ExtentX        =   5292
      _ExtentY        =   556
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.26
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   30
   End
   Begin VB.Label LblSalesDesc 
      AutoSize        =   -1  'True
      Caption         =   "Sales Report(s) Description:"
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   1320
      Width           =   1965
   End
   Begin VB.Label lblDescription 
      AutoSize        =   -1  'True
      Caption         =   "Description"
      Height          =   240
      Left            =   150
      TabIndex        =   4
      Top             =   930
      Width           =   795
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   45
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblPaymentTerms 
      AutoSize        =   -1  'True
      Caption         =   "&Payment &Term"
      Height          =   195
      Left            =   135
      TabIndex        =   2
      Top             =   555
      Width           =   1290
   End
End
Attribute VB_Name = "frmPmtTerms"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: Common Information Payment Terms
'
'     Desc: The Payment Terms form will handle the maintenance of
'           Payment Terms from the Common Information menu.
'
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 04/14/95 KMC
'     Mods:
'***********************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If
    
'Private variables of Form Properties
    Private moClass                 As Object
    Private mlRunMode               As Long
    Private mbSaved                 As Boolean
    Private mbCancelShutDown        As Boolean
    Private msPmtTermsID            As String

'Public Form Variables
    Public moSotaObjects            As New Collection

'Payment Terms specific form variables
    Private moDMForm                As clsDmForm
    Private moContextMenu           As clsContextMenu
    Private moVM                    As clsValidationManager
    
    Private moDMFormExt             As clsDmForm    'RKL DEJ 2018-02-27 Added Additinal Description Field
    
    Private miFilter                As Integer
    Private msCurrentKeyValue       As String
    Private msCompanyID             As String
    Private miSecurityLevel         As Integer
    
    Private msNone                  As String
    Private msDay                   As String
    Private msDays                  As String
    Private msMonths                As String
    Private msFixedDate             As String
    Private mbEnterAsTab            As Boolean
    Private miDfltDueDateOption     As Integer
    Private miDfltDiscDateOption    As Integer
    Private mbChange                As Boolean
    Private mbMaintCIBusinessFormAOF    As Boolean
    
Const ktskPaymentTermsList = 34209832

Const VBRIG_MODULE_ID_STRING = "CIZMH001.FRM"



Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


'************************************************************************
'   lRunMode contains the user-defined run mode context in which the
'   class object was Initialzed. The run mode is extracted from the
'   context passed in through the InitializeObject.
'************************************************************************
Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level and
'   What's This Help.  This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = Data Entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "CIZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'***************
'*********************************************************
'   WhatHelpPrefix will contain the help prefix for the Form Level and
'   What's This Help.  This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = Data Entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "CIZ"
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


'************************************************************************
'   bSaved is used specifically for Add-On-The-Fly.  It tells the
'   the calling object whether the record was saved or not.
'************************************************************************
Public Property Get bSaved() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bSaved = mbSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbSaved = bNewSaved
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSaved_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   PmtTermsID is used to assist the class in controlling the form's
'   window state'
'************************************************************************
Public Property Get PmtTermsID() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    PmtTermsID = msPmtTermsID
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PmtTermsID_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bCancelShutDown = mbCancelShutDown
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCancelShutDown_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub cboCODLabelFormID_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If keycode = vbKeyF3 And Shift = 0 Then
        gRefreshCIBusinessForm moClass.moAppDB, moClass.moSysSession, cboCODLabelFormID, _
                               mbMaintCIBusinessFormAOF, kComboNotRequired, msCompanyID, kvLabel
        cboCODLabelFormID.Tag = cboCODLabelFormID.ListIndex
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCODLabelFormID_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCODLabelFormID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboCODLabelFormID, True
    #End If
'+++ End Customizer Code Push +++
    moVM.IsValidControl cboCODLabelFormID
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboCODLabelFormID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCOD_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkCOD, True
    #End If
'+++ End Customizer Code Push +++
    If chkCOD.Value = 1 Then
        lblCODLabelFormID.Enabled = True
        cboCODLabelFormID.Enabled = True
    Else
        cboCODLabelFormID.ListIndex = -1
        lblCODLabelFormID.Enabled = False
        cboCODLabelFormID.Enabled = False
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "chkCOD_Click", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mbCancelShutDown = False

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Form_Load starts out with an initial state where it initializes
'       any controls on the form.  Then, based on runmode context of
'       how this object was instantiated, it changes states.
'********************************************************************
    Dim sRestrictClause As String
    Dim sDateMask       As String

    Set sbrMain.Framework = moClass.moFramework
    
    miFilter = RSID_UNFILTERED
    
    '=============================================
    'Create the Toolbar Class.
    '=============================================
    BindToolbar
    
    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab
    
    If mlRunMode = kContextAOF Then
        navMain.Visible = False
    End If
    
    mbMaintCIBusinessFormAOF = False
    
    txtDueFixedDate.Top = numDueDayOrMonth.Top
    txtDueFixedDate.Left = numDueDayOrMonth.Left
    txtDiscFixedDate.Top = numDiscDayOrMonth.Top
    txtDiscFixedDate.Left = numDiscDayOrMonth.Left
    sDateMask = gsGetLocalDateMask()
' DATE Presto Comment:     txtDiscFixedDate.Mask = sDateMask
' DATE Presto Comment:     txtDueFixedDate.Mask = sDateMask
    numDiscPct.ValueMax = 100
    numDiscPct.ValueMin = 0
    
    gRefreshCIBusinessForm moClass.moAppDB, moClass.moSysSession, cboCODLabelFormID, _
                           mbMaintCIBusinessFormAOF, kComboNotRequired, msCompanyID, kvLabel
    
    InitcboDateOptions
    
    BindValMgr
    
    BindForm
    
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain, moDMForm)
    
    BindContextMenu
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDiscDateOption_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboDiscDateOption, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
'   Desc: cboDiscDateOption_Click calls the procedure that handles
'         what should happen with the discount option changes.
'
'  Parms: None
'********************************************************************
    
    If cboDiscDateOption.ListIndex = CInt(cboDiscDateOption.Tag) Then Exit Sub
    
    If cboDiscDateOption.ListIndex = kItemNotSelected Then
        InitToStateDiscDate kItemNotSelected
    Else
        InitToStateDiscDate cboDiscDateOption.ItemData(cboDiscDateOption.ListIndex)
    End If
    
    cboDiscDateOption.Tag = cboDiscDateOption.ListIndex

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboDiscDateOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub cboDueDateOption_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboDueDateOption, True
    #End If
'+++ End Customizer Code Push +++
'********************************************************************
'   Desc: cboDueDateOption_Click handles what should happen with the
'         due date option changes.
'
'  Parms: None
'********************************************************************
    
    If cboDueDateOption.ListIndex = CInt(cboDueDateOption.Tag) Then Exit Sub
    
    If cboDueDateOption.ListIndex = kItemNotSelected Then
        InitToStateDueDate kItemNotSelected
    Else
        InitToStateDueDate cboDueDateOption.ItemData(cboDueDateOption.ListIndex)
    End If
    
    cboDueDateOption.Tag = cboDueDateOption.ListIndex
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboDueDateOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub Form_Paint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    Set moClass = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'********************************************************************
    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
            
            
    End Select
    
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
'***********************************************************************
'Desc: If the data has changed then prompt to save changes
'***********************************************************************
    
    Dim i               As Integer
    Dim sFinishKey      As String
    Dim bNoClear        As Boolean
    

   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
    
        Select Case mlRunMode
            Case kContextAOF
                sFinishKey = kTbFinishExit
                bNoClear = False
            Case Else
                sFinishKey = kTbFinish
                bNoClear = True
        End Select
        
        If tbrMain.ButtonEnabled(sFinishKey) Then
            If Not moVM.IsValidDirtyCheck() Then GoTo CancelShutDown
            If Not bConfirmUnloadSuccess(bNoClear) Then GoTo CancelShutDown
        End If
    End If
    
    'Check all other forms  that may have been loaded from this main form.
    'If there are any visible forms, then this means the form is active.
    'Therefore, cancel the shutdown.
    If gbActiveChildForms(Me) Then GoTo CancelShutDown


    Select Case UnloadMode
        Case vbFormCode
        'Do Nothing.
        'If the unload is caused by form code, then the form
        'code should also have the miShutDownRequester set correctly.
        
        Case Else
       'Most likely the user has requested to shut down the form.
        'If the context is Add-on-the-fly, hide the form
        'and cancel the unload.
        'If the context is Normal or Drill-Around, have the object unload itself.
             Select Case mlRunMode
                Case kContextAOF
                    txtPmtTermsID = Empty
                    moClass.moFramework.SavePosSelf
                    Me.Hide
                    GoTo CancelShutDown
                Case Else
                    moClass.miShutDownRequester = kUnloadSelfShutDown
            End Select
    End Select
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else 'kFrameworkShutDown
            'Do nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'
'********************************************************************
   
   
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case 6, 7, 12
            KeyAscii = 0
        Case Else
        
            'Other KeyAscii routines.
    End Select
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub





Private Sub InitcboDateOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************************************************
' Desc:    InitcboDateOptions sets all the value for the Due and Discount
'               Date Option combo boxes.
'               This routine is called only once during the Form_Load event.
' Parms:  None
'****************************************************************************************
    Dim StringArr(4) As String, ConstArr(4) As Long
    
    ConstArr(0) = kNone
    ConstArr(1) = kDay
    ConstArr(2) = kDays
    ConstArr(3) = kMonths
    ConstArr(4) = kFixedDate
    
    gBuildArrStrings StringArr, ConstArr, moClass.moAppDB, moClass.moSysSession
    
    msNone = StringArr(0)
    msDay = StringArr(1)
    msDays = StringArr(2)
    msMonths = StringArr(3)
    msFixedDate = StringArr(4)
    miDfltDueDateOption = giFillStaticList(moClass.moAppDB, moClass.moSysSession, cboDueDateOption, "tciPaymentTerms", "DueDateOption")
    If miDfltDueDateOption = kItemNotSelected Then
        miDfltDueDateOption = giListIndexFromItemData(cboDueDateOption, kvDueSpecificDayofMonth)
    End If
    
    miDfltDiscDateOption = giFillStaticList(moClass.moAppDB, moClass.moSysSession, cboDiscDateOption, "tciPaymentTerms", "DiscDateOption")
    If miDfltDiscDateOption = kItemNotSelected Then
        miDfltDiscDateOption = giListIndexFromItemData(cboDiscDateOption, kvDiscnone)
    End If
 
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitcboDateOptions", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub




Private Sub NavMain_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bEnabled As Boolean
    
    If Not gbGotFocus(Me, navMain) Then
        Exit Sub
    End If

    If Not bConfirmUnloadSuccess() Then
        Exit Sub
    End If
    
    If Not bSetupNavMain() Then
        Exit Sub
    End If
    
    bEnabled = txtPmtTermsID.Enabled
    
    gcLookupClick Me, navMain, txtPmtTermsID, "PmtTermsID"
    DoEvents
    Me.SetFocus

    If (navMain.ReturnColumnValues.Count > 0 _
    And Not bEnabled) Then
        txtPmtTermsID.Text = navMain.ReturnColumnValues.Item("PmtTermsID")
        VMIsValidKey
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "NavMain_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub













Private Sub numDiscDayOrMonth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numDiscDayOrMonth, True
    #End If
'+++ End Customizer Code Push +++
        If cboDiscDateOption.ListIndex = kItemNotSelected Then Exit Sub
        mbChange = True
        SpnDiscDayOrMonth.Value = numDiscDayOrMonth
        mbChange = False
    '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numDiscDayOrMonth_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub numDiscMinDays_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numDiscMinDays, True
    #End If
'+++ End Customizer Code Push +++
        mbChange = True
        SpnDiscMinDays.Value = numDiscMinDays
        mbChange = False
    '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numDiscMinDays_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscPct_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numDiscPct, True
    #End If
'+++ End Customizer Code Push +++
    moVM.IsValidControl numDiscPct
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numDiscPct_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueDayOrMonth_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numDueDayOrMonth, True
    #End If
'+++ End Customizer Code Push +++
        mbChange = True
        If moDMForm.State = kDmStateNone Then
            SpnDueDayOrMonth.Min = -1
            SpnDueDayOrMonth.Max = 1
            SpnDueDayOrMonth.Value = numDueDayOrMonth
        End If
        mbChange = False
    '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numDueDayOrMonth_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub






Private Sub numDueMinDays_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus numDueMinDays, True
    #End If
'+++ End Customizer Code Push +++
        mbChange = True
        SpnDueMinDays.Value = numDueMinDays
        mbChange = False
    '+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "numDueMinDays_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SpnDiscDayOrMonth_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Static bInHere As Boolean
    Dim iCurrentValue As Integer
    
    If bInHere Then Exit Sub
    
    If mbChange Then Exit Sub
    
    iCurrentValue = giGetValidInt(numDiscDayOrMonth.Value)
    
    If SpnDiscDayOrMonth > SpnDiscDayOrMonth.Tag Then
        If iCurrentValue < numDiscDayOrMonth.ValueMax Then numDiscDayOrMonth = iCurrentValue + 1
    Else
        If iCurrentValue > numDiscDayOrMonth.ValueMin Then numDiscDayOrMonth = iCurrentValue - 1
    End If
    
    bInHere = True
    SpnDiscDayOrMonth.Tag = SpnDiscDayOrMonth.Value
    'txtSpin(Index).Tag = txtSpin(Index)
    bInHere = False
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "spnDiscDayOrMonth_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SpnDiscMinDays_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Static bInHere As Boolean
    Dim iCurrentValue As Integer
    
    If bInHere Then Exit Sub
    
    If mbChange Then Exit Sub
    
    iCurrentValue = giGetValidInt(numDiscMinDays.Value)
    
    If SpnDiscMinDays > SpnDiscMinDays.Tag Then
        If iCurrentValue < numDiscMinDays.ValueMax Then numDiscMinDays = iCurrentValue + 1
    Else
        If iCurrentValue > numDiscMinDays.ValueMin Then numDiscMinDays = iCurrentValue - 1
    End If
    
    bInHere = True
    SpnDiscMinDays.Tag = SpnDiscMinDays.Value
    'txtSpin(Index).Tag = txtSpin(Index)
    bInHere = False
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "spnDiscMinDays_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SpnDueDayOrMonth_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Static bInHere As Boolean
    Dim iCurrentValue As Integer
    
    If bInHere Then Exit Sub
    
    If mbChange Then Exit Sub
    
    iCurrentValue = giGetValidInt(numDueDayOrMonth.Value)
           
    If SpnDueDayOrMonth > SpnDueDayOrMonth.Tag Then
        If iCurrentValue < numDueDayOrMonth.ValueMax Then numDueDayOrMonth = iCurrentValue + 1
    Else
        If iCurrentValue > numDueDayOrMonth.ValueMin Then numDueDayOrMonth = iCurrentValue - 1
    End If
    
    bInHere = True
    SpnDueDayOrMonth.Tag = SpnDueDayOrMonth.Value
    'txtSpin(Index).Tag = txtSpin(Index)
    bInHere = False
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "spnDueDayOrMonth_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SpnDueMinDays_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Static bInHere As Boolean
    Dim iCurrentValue As Integer
    
    If bInHere Then Exit Sub
    If mbChange Then Exit Sub
    
    iCurrentValue = giGetValidInt(numDueMinDays.Value)
    
    If SpnDueMinDays > SpnDueMinDays.Tag Then
        If iCurrentValue < numDueMinDays.ValueMax Then numDueMinDays = iCurrentValue + 1
    Else
        If iCurrentValue > numDueMinDays.ValueMin Then numDueMinDays = iCurrentValue - 1
    End If
    
    bInHere = True
    SpnDueMinDays.Tag = SpnDueMinDays.Value
    'txtSpin(Index).Tag = txtSpin(Index)
    bInHere = False
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "spnDueMinDays_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscFixedDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDiscFixedDate, True
    #End If
'+++ End Customizer Code Push +++
    moVM.IsValidControl txtDiscFixedDate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDiscFixedDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub InitToStateDiscDate(icboDiscDateOption As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
 
 
    
    Select Case icboDiscDateOption
    
        Case kItemNotSelected
            lblDiscDayOrMonth = msDay
            txtDiscFixedDate = Empty: moVM.SetControlValid txtDiscFixedDate
            txtDiscFixedDate.Visible = False
            numDiscDayOrMonth.Visible = True
            SpnDiscDayOrMonth.Visible = True
            numDiscDayOrMonth.ValueMin = 0
            numDiscDayOrMonth = numDiscDayOrMonth.ValueMin
            numDiscPct = numDiscPct.ValueMin:  moVM.SetControlValid numDiscPct
            txtDiscFixedDate = Empty
            chkApplyToFreight = vbUnchecked
            chkApplyToSTax = vbUnchecked
        
        Case kvDiscnone
            lblDiscDayOrMonth = msNone
            txtDiscFixedDate = Empty:  moVM.SetControlValid txtDiscFixedDate
            txtDiscFixedDate.Visible = False
            numDiscDayOrMonth.Visible = True
            SpnDiscDayOrMonth.Visible = True
            numDiscDayOrMonth.ValueMin = 0
            numDiscDayOrMonth = numDiscDayOrMonth.ValueMin
            numDiscMinDays = numDiscMinDays.ValueMin
            numDiscPct = numDiscPct.ValueMin:  moVM.SetControlValid numDiscPct
            numDiscDayOrMonth = Empty
            numDiscMinDays = Empty
            numDiscDayOrMonth = Empty
            chkApplyToFreight = vbUnchecked
            chkApplyToSTax = vbUnchecked
        
        Case kvDiscSpecificDayofMonth
            lblDiscDayOrMonth = msDay
            txtDiscFixedDate = Empty:  moVM.SetControlValid txtDiscFixedDate
            txtDiscFixedDate.Visible = False
            numDiscDayOrMonth.Visible = True
            numDiscDayOrMonth.ValueMax = 31
            numDiscDayOrMonth.ValueMin = 1
            SpnDiscDayOrMonth.Visible = True
            SpnDiscDayOrMonth.Max = 1
            SpnDiscDayOrMonth.Min = 31
            If CInt(cboDiscDateOption.Tag) <> -1 Then
                numDiscDayOrMonth = numDiscDayOrMonth.ValueMin
                numDiscMinDays = numDiscMinDays.ValueMin
            End If
            
            
        Case kvDiscNumberofDays
            lblDiscDayOrMonth = msDays
            txtDiscFixedDate = Empty:  moVM.SetControlValid txtDiscFixedDate
            txtDiscFixedDate.Visible = False
            lblDiscDayOrMonth = msDays
            txtDiscFixedDate.Visible = False
            numDiscDayOrMonth.Visible = True
            numDiscDayOrMonth.ValueMax = 999
            numDiscDayOrMonth.ValueMin = 0
            SpnDiscDayOrMonth.Visible = True
            SpnDiscDayOrMonth.Max = 0
            SpnDiscDayOrMonth.Min = 999
            numDiscMinDays = numDiscMinDays.ValueMin
            numDiscMinDays = Empty
            If CInt(cboDiscDateOption.Tag) <> -1 Then
                numDiscDayOrMonth = numDiscDayOrMonth.ValueMin
            End If
        
        Case kvDiscNumberofMonths
            lblDiscDayOrMonth = msMonths
            txtDiscFixedDate = Empty:  moVM.SetControlValid txtDiscFixedDate
            txtDiscFixedDate.Visible = False
            numDiscDayOrMonth.Visible = True
            numDiscDayOrMonth.ValueMax = 999
            numDiscDayOrMonth.ValueMin = 1
            SpnDiscDayOrMonth.Visible = True
            SpnDiscDayOrMonth.Max = 1
            SpnDiscDayOrMonth.Min = 999
            numDiscMinDays = Empty
            If CInt(cboDiscDateOption.Tag) <> -1 Then
                numDiscDayOrMonth = numDiscDayOrMonth.ValueMin
            End If
        
        Case kvDiscFixedDate
            lblDiscDayOrMonth = msFixedDate
            numDiscDayOrMonth = numDiscDayOrMonth.ValueMin
            numDiscDayOrMonth.Visible = False
            SpnDiscDayOrMonth.Visible = False
            txtDiscFixedDate.Visible = True
            numDiscMinDays = numDiscMinDays.ValueMin
            numDiscMinDays = Empty
           
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedColumnValue, _
                         "tciPaymentTerms", "DiscDateOption"

    End Select

    'If Security is DisplayOnly then exit sub.  The Select statement deals
    'with enabling/disabling controls dependant upon the DateOption Selection.
        
    Select Case icboDiscDateOption
            
        Case kItemNotSelected
            numDiscDayOrMonth.Enabled = True
            SpnDiscDayOrMonth.Enabled = False
            numDiscMinDays.Enabled = True
            SpnDiscMinDays.Enabled = False
            numDiscPct.Enabled = True
            chkApplyToFreight.Enabled = True
            chkApplyToSTax.Enabled = True
            fraApplyDiscountTo.Enabled = True
            
        Case kvDiscnone
            fraApplyDiscountTo.Enabled = False
            chkApplyToFreight.Enabled = False
            chkApplyToSTax.Enabled = False
            numDiscPct.Enabled = False
            numDiscDayOrMonth.Enabled = False
            SpnDiscDayOrMonth.Enabled = False
            numDiscMinDays.Enabled = False
            SpnDiscMinDays.Enabled = False
            
        Case kvDiscSpecificDayofMonth
            fraApplyDiscountTo.Enabled = True
            chkApplyToFreight.Enabled = True
            chkApplyToSTax.Enabled = True
            numDiscPct.Enabled = True
            numDiscDayOrMonth.Enabled = True
            SpnDiscDayOrMonth.Enabled = True
            numDiscMinDays.Enabled = True
            SpnDiscMinDays.Enabled = True
    
        Case kvDiscNumberofDays
            fraApplyDiscountTo.Enabled = True
            chkApplyToFreight.Enabled = True
            chkApplyToSTax.Enabled = True
            numDiscPct.Enabled = True
            numDiscDayOrMonth.Enabled = True
            SpnDiscDayOrMonth.Enabled = True
            numDiscMinDays.Enabled = False
            numDiscMinDays = Empty
            SpnDiscMinDays.Enabled = False

    
        Case kvDiscNumberofMonths
            fraApplyDiscountTo.Enabled = True
            chkApplyToFreight.Enabled = True
            chkApplyToSTax.Enabled = True
            numDiscPct.Enabled = True
            numDiscDayOrMonth.Enabled = True
            SpnDiscDayOrMonth.Enabled = True
            numDiscMinDays.Enabled = False
            SpnDiscMinDays.Enabled = False
    
    
        Case kvDiscFixedDate
            fraApplyDiscountTo.Enabled = True
            chkApplyToFreight.Enabled = True
            chkApplyToSTax.Enabled = True
            numDiscPct.Enabled = True
            numDiscMinDays.Enabled = False
            SpnDiscMinDays.Enabled = False
            txtDiscFixedDate.Enabled = True
            
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedColumnValue, _
                         "tciPaymentTerms", "DiscDateOption"
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitToStateDiscDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub InitToStateDueDate(icboDueDateOption As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++


    Select Case icboDueDateOption

        Case kItemNotSelected
            lblDueDayOrMonth = msDay
            txtDueFixedDate = Empty:  moVM.SetControlValid txtDueFixedDate
            txtDueFixedDate.Visible = False
            numDueDayOrMonth.Visible = True
            SpnDueDayOrMonth.Visible = True
            
            numDueMinDays = numDueMinDays.ValueMin
            
            numDueDayOrMonth.ValueMin = 0
            numDueDayOrMonth = numDueDayOrMonth.ValueMin
            txtDueFixedDate = Empty

        Case kvDueSpecificDayofMonth
            lblDueDayOrMonth = msDay
            txtDueFixedDate = Empty:  moVM.SetControlValid txtDueFixedDate
            txtDueFixedDate.Visible = False
            numDueDayOrMonth.Visible = True
            numDueDayOrMonth.ValueMax = 31
            numDueDayOrMonth.ValueMin = 1
            SpnDueDayOrMonth.Max = 1
            SpnDueDayOrMonth.Min = 31
            SpnDueDayOrMonth.Visible = True
            If CInt(cboDueDateOption.Tag) <> -1 Then
                numDueDayOrMonth = numDueDayOrMonth.ValueMin
                numDueMinDays = numDueMinDays.ValueMin
            End If

        Case kvDueNumberofDays
            lblDueDayOrMonth = msDays
            txtDueFixedDate = Empty:  moVM.SetControlValid txtDueFixedDate
            txtDueFixedDate.Visible = False
            lblDueDayOrMonth = msDays
            txtDueFixedDate.Visible = False
            numDueDayOrMonth.Visible = True
            numDueDayOrMonth.ValueMax = 999
            numDueDayOrMonth.ValueMin = 0
            SpnDueDayOrMonth.Visible = True
            SpnDueDayOrMonth.Max = 0
            SpnDueDayOrMonth.Min = 999
            numDueMinDays = numDueMinDays.ValueMin
            If CInt(cboDueDateOption.Tag) <> -1 Then
                numDueDayOrMonth = numDueDayOrMonth.ValueMin
            End If

        Case kvDueNumberofMonths
            lblDueDayOrMonth = msMonths
            txtDueFixedDate = Empty:  moVM.SetControlValid txtDueFixedDate
            txtDueFixedDate.Visible = False
            numDueDayOrMonth.Visible = True
            numDueDayOrMonth.ValueMax = 999
            numDueDayOrMonth.ValueMin = 1
            SpnDueDayOrMonth.Visible = True
            SpnDueDayOrMonth.Max = 1
            SpnDueDayOrMonth.Min = 999
            numDueMinDays = numDueMinDays.ValueMin
            If CInt(cboDueDateOption.Tag) <> -1 Then
                numDueDayOrMonth = numDueDayOrMonth.ValueMin
            End If

        Case kvDueFixedDate
            lblDueDayOrMonth = msFixedDate
            numDueDayOrMonth = numDueDayOrMonth.ValueMin
            numDueDayOrMonth.Visible = False
            SpnDueDayOrMonth.Visible = False
            numDueMinDays = numDueMinDays.ValueMin
            txtDueFixedDate.Visible = True

        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedColumnValue, _
                         "tciPaymentTerms", "DueDateOption"

    End Select

    'If Security is DisplayOnly then exit sub.  The Select statement deals
    'with enabling/disabling controls dependant upon the DateOption Selection.

    Select Case icboDueDateOption

        Case kItemNotSelected
            numDueDayOrMonth.Enabled = True
            SpnDueDayOrMonth.Enabled = False
            numDueMinDays.Enabled = True
            SpnDueMinDays.Enabled = False

        Case kvDueSpecificDayofMonth
            numDueDayOrMonth.Enabled = True
            SpnDueDayOrMonth.Enabled = True
            numDueMinDays.Enabled = True
            SpnDueMinDays.Enabled = True

        Case kvDueNumberofDays
            numDueDayOrMonth.Enabled = True
            SpnDueDayOrMonth.Enabled = True
            numDueMinDays.Enabled = False
            numDueMinDays = Empty
            SpnDueMinDays.Enabled = False

        Case kvDueNumberofMonths
            numDueDayOrMonth.Enabled = True
            SpnDueDayOrMonth.Enabled = True
            numDueMinDays.Enabled = False
            SpnDueMinDays.Enabled = False


        Case kvDueFixedDate
            txtDueFixedDate.Enabled = True
            numDueMinDays.Enabled = False
            SpnDueMinDays.Enabled = False

        Case Else
                    giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedColumnValue, _
                                 "tciPaymentTerms", "DuecDateOption"
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitToStateDueDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub





Public Function DMValidate(oDm As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Description:
'       Entries will be checked for validity.
'
'   Return Values:
'       True    - the entry is valid
'       False   - the Entry is invalid because anyone of the components
'                 tested for validity is invalid.
'********************************************************************

    DMValidate = False
    
    Select Case True
        Case oDm Is moDMForm
            'If the user is a record state other than none, then force a lost focus.
            'If the entry of the last control was not valid, do not allow the form to
            'be unloaded.
            
            If cboDueDateOption.ListIndex = kItemNotSelected Then Exit Function
            If cboDiscDateOption.ListIndex = kItemNotSelected Then Exit Function
             
            'Check Values based on the Due Date Option
            Select Case cboDueDateOption.ItemData(cboDueDateOption.ListIndex)
             
                Case kvDueFixedDate
                    If Len(Trim(txtDueFixedDate)) = 0 Then
                        oDm.CancelAction
                        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, _
                                         gsStripChar(lblDueDayOrMonth, "&")
                        txtDueFixedDate.SetFocus
                        DoEvents
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
                Case kvDueSpecificDayofMonth
                     If numDueDayOrMonth.Value > 31 Then
                        oDm.CancelAction
                        numDueDayOrMonth.SetFocus
                        giSotaMsgBox Me, moClass.moSysSession, _
                                     kmsgInvalidField, gsStripChar(lblDueDayOrMonth, "&")
                        DoEvents
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                     
            End Select
            
            'Check Values based on the Discount Date Option
            Select Case cboDiscDateOption.ItemData(cboDiscDateOption.ListIndex)
             
                Case kvDiscFixedDate
                    If Len(Trim(txtDiscFixedDate)) = 0 Then
                        oDm.CancelAction
                        txtDiscFixedDate.SetFocus
                        giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, _
                                    gsStripChar(lblDiscDayOrMonth, "&")
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
                Case kvDiscSpecificDayofMonth
                    '//* KMC Workaround for the number control not reseting
                    '        when the ValueMin and ValueMax properties change.
                    If numDiscDayOrMonth.Value > 31 Then
                        oDm.CancelAction
                        numDiscDayOrMonth.SetFocus
                        giSotaMsgBox Me, moClass.moSysSession, _
                                     kmsgInvalidField, gsStripChar(lblDiscDayOrMonth, "&")
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                        Exit Function
                    End If
                    
            End Select
            
            If numDiscPct.Value < 0 Or numDiscPct.Value > 100 Then
                oDm.CancelAction
                numDiscPct.SetFocus
                giSotaMsgBox Me, moClass.moSysSession, _
                             kmsgInvalidField, gsStripChar(lblDiscPct, "&")
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            
    End Select
    
DMValidate = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMValidate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function




Public Sub DMReposition(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case True
        Case oDm Is moDMForm
            Select Case oDm.State
                Case kDmStateAdd
                    cboDueDateOption.ListIndex = miDfltDueDateOption
                    cboDiscDateOption.ListIndex = miDfltDiscDateOption
                    
                    moVM.SetAllValid
                
            End Select
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMReposition", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Function DMPostSave(oDm As Object) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    DMPostSave = False
    Select Case True
        Case oDm Is moDMForm
            mbSaved = True
    End Select
    
    DMPostSave = True
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMPostSave", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Sub DMStatechange(oDm As Object, iOldState As Integer, iNewState As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Select Case True
        Case oDm Is moDMForm
        
            tbrMain.SetState (iNewState)
            
            Select Case iNewState
                Case kDmStateEdit
                    moClass.lUIActive = kChildObjectActive
                Case kDmStateAdd
                    moClass.lUIActive = kChildObjectActive
                Case kDmStateNone
                    moClass.lUIActive = kChildObjectInactive
                    ClearUnboundControls
                Case Else
                    'Unknown state
            End Select
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMStatechange", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMForm = New clsDmForm
    
    With moDMForm
        .Table = "tciPaymentTerms"
        .UniqueKey = "PmtTermsID,CompanyID"
        Set .Session = moClass.moSysSession
Set .Form = frmPmtTerms
        
        .Bind Nothing, "PmtTermsKey", SQL_INTEGER, kDmSurrogateKey
        .Bind Nothing, "CompanyID", SQL_CHAR
        .Bind txtPmtTermsID, "PmtTermsID", SQL_CHAR
        .Bind txtDescription, "Description", SQL_CHAR
        .Bind Nothing, "DiscPct", SQL_DECIMAL
        .Bind chkApplyToFreight, "ApplyToFreight", SQL_SMALLINT
        .Bind chkCOD, "COD", SQL_SMALLINT
        .Bind chkApplyToSTax, "ApplyToSTax", SQL_SMALLINT
        .BindComboBox cboCODLabelFormID, "CODLabelFormKey", SQL_SMALLINT, kDmUseItemData Or kDmSetNull
        .BindComboBox cboDueDateOption, "DueDateOption", SQL_SMALLINT, kDmUseItemData
        .Bind numDueDayOrMonth, "DueDayOrMonth", SQL_DECIMAL
        .Bind txtDueFixedDate, "DueFixedDate", SQL_DATE
        .Bind numDueMinDays, "DueMinDays", SQL_SMALLINT
        .BindComboBox cboDiscDateOption, "DiscDateOption", SQL_SMALLINT, kDmUseItemData
        .Bind numDiscDayOrMonth, "DiscDayOrMonth", SQL_DECIMAL
        .Bind txtDiscFixedDate, "DiscFixedDate", SQL_DATE
        .Bind numDiscMinDays, "DiscMinDays", SQL_SMALLINT
        .Bind Nothing, "UpdateCounter", SQL_SMALLINT

        .Init
    End With
    
    '***********************************************************************************
    'RKL DEJ 2018-02-27 (Start)
    '***********************************************************************************
    Set moDMFormExt = New clsDmForm
    
    With moDMFormExt
        .Table = "tciPaymentTermsExt_SGS"
        .UniqueKey = "PmtTermsKey"
        Set .Session = moClass.moSysSession
        Set .Form = frmPmtTerms
        Set .Parent = moDMForm
        
        .Bind txtSalesRptDesc, "SalesRptsDesc", SQL_VARCHAR

        .ParentLink "PmtTermsKey", "PmtTermsKey", SQL_INTEGER
        .Init
    End With
        
    '***********************************************************************************
    'RKL DEJ 2018-02-27 (Stop)
    '***********************************************************************************
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub DMDataDisplayed(oDm As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case True
        Case oDm Is moDMForm
            With oDm
                moVM.SetAllValid
                Select Case cboDueDateOption.ItemData(cboDueDateOption.ListIndex)
                    Case kvDueSpecificDayofMonth, kvDueNumberofDays, kvDueNumberofMonths
                        numDueDayOrMonth = .GetColumnValue("DueDayOrMonth")
                    Case kvDueFixedDate
                        txtDueFixedDate = .GetColumnValue("DueFixedDate")
                    Case Else
                        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedColumnValue, _
                                     "tciPaymentTerms", "DueDateOption"
                End Select
            
                Select Case cboDiscDateOption.ItemData(cboDiscDateOption.ListIndex)
                    Case kvDiscnone
                        numDiscDayOrMonth = Empty
                        numDiscMinDays = Empty
                    Case kvDiscSpecificDayofMonth, kvDiscNumberofDays, kvDiscNumberofMonths
                        numDiscDayOrMonth = .GetColumnValue("DiscDayOrMonth")
                        numDiscPct = CSng(.GetColumnValue("DiscPct")) * 100
                    Case kvDiscFixedDate
                        txtDiscFixedDate = .GetColumnValue("DiscFixedDate")
                        numDiscPct = CSng(.GetColumnValue("DiscPct")) * 100
                    Case Else
                        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedColumnValue, _
                                     "tciPaymentTerms", "DiscDateOption"
                End Select
            End With
            moVM.SetAllValid
    End Select
    If chkCOD.Value = 1 Then
        lblCODLabelFormID.Enabled = True
        cboCODLabelFormID.Enabled = True
    Else
        lblCODLabelFormID.Enabled = False
        cboCODLabelFormID.Enabled = False
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "DMDataDisplayed", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'************************************************************************************
'   Description:
'       PerformCleanShutDown will attempt to cleanup all forms and child objects that
'       may have been loaded from this form.
'************************************************************************************
On Error GoTo ExpectedErrorRoutine
On Error Resume Next

    'Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moVM Is Nothing Then
        moVM.UnloadSelf
        Set moVM = Nothing
    End If
    
    '***************************************************
    'RKL DEJ 2018-02-27 (Start)
    '***************************************************
    If Not moDMFormExt Is Nothing Then
        moDMFormExt.UnloadSelf
        Set moDMFormExt = Nothing
    End If
    '***************************************************
    'RKL DEJ 2018-02-27 (Stop)
    '***************************************************
    
    If Not moDMForm Is Nothing Then
        moDMForm.UnloadSelf
        Set moDMForm = Nothing
    End If
    
    Set moContextMenu = Nothing
    Set moSotaObjects = Nothing
   

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub txtDueFixedDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDueFixedDate, True
    #End If
'+++ End Customizer Code Push +++
    moVM.IsValidControl txtDueFixedDate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtDueFixedDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub txtPmtTermsID_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtPmtTermsID, True
    #End If
'+++ End Customizer Code Push +++
    If txtPmtTermsID.Enabled Then moVM.IsValidControl txtPmtTermsID
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtPmtTermsID_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub



Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set moContextMenu = New clsContextMenu
    
    With moContextMenu
    
    'Add any special .Bind or .BondGrid commands here for Drill Around or
    'Drill Down or for Grids - Here
    'Example:
    
        ' **PRESTO ** Set .Hook = WinHook1
        Set .Form = frmPmtTerms
        
        .Init
    End With

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function bConfirmUnloadSuccess(Optional ByVal vNoClear As Variant) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***************************************************
' Possiby a public function?????
'***************************************************
    
    Dim iConfirmUnload
    Dim bNoClear            As Boolean
    
    bConfirmUnloadSuccess = False
    If IsMissing(vNoClear) Then
        bNoClear = False
    Else
        bNoClear = CBool(vNoClear)
    End If
        
    iConfirmUnload = moDMForm.ConfirmUnload(bNoClear)
    
    Select Case iConfirmUnload
        Case kDmSuccess
            bConfirmUnloadSuccess = True
        Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedConfirmUnloadRV, iConfirmUnload
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bConfirmUnloadSuccess", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function VMIsValidKey() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim iKeyChangeCode  As Integer
    Dim iReturnCode     As Integer
    
    VMIsValidKey = True
    
    
    txtPmtTermsID = Trim(txtPmtTermsID)
    msPmtTermsID = txtPmtTermsID
    
    If Len(Trim(txtPmtTermsID)) = 0 Then GoTo Invalid
    
    moDMForm.SetColumnValue "CompanyID", msCompanyID
    iKeyChangeCode = moDMForm.KeyChange()

    Select Case iKeyChangeCode
        Case kDmKeyFound
            
            'Set UIActive to inactive when the record exists from AOF
            Select Case mlRunMode
               Case kContextNormal
               Case kContextAOF
                   iReturnCode = giSotaMsgBox(Me, moClass.moSysSession, kmsgAddedByAnother)
                   Select Case iReturnCode
                       Case kretYes
                            mbSaved = True
                            moDMForm.Action kDmCancel
                            moClass.moFramework.SavePosSelf
                            Me.Hide
                       Case kretNo
                            mbSaved = False
                            moDMForm.Action kDmCancel
                   End Select
            End Select
        Case kDmKeyNotFound
        Case kDmKeyNotComplete
            GoTo Invalid
        Case kDmError
            GoTo Invalid
        Case kDmNotAllowed
            moVM.AllowMessage = False
            txtPmtTermsID.Protected = False
            GoTo Invalid
        Case Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedKeyChangeCode, _
                         iKeyChangeCode
            GoTo Invalid
    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
Invalid:
    VMIsValidKey = False

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidDueFixedDate() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    'Assumed to be Valid - Prove Invalidity.
    bIsValidDueFixedDate = True
    
    
    If Len(Trim(txtDueFixedDate)) = 0 Then GoTo Valid
    
    If Not gbValidLocalDate(txtDueFixedDate.Text) Then GoTo Invalid
    
Valid:

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
Invalid:
    'test 1
    'moVM.SetSotaMsgBox kmsgInvalidDate
    
    'test 2
    'moVM.SetSotaMsgBox kmsgUnexpectedColumnValue, _
                         "tciPaymentTerms", "DiscDateOption"


    bIsValidDueFixedDate = False


'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDueFixedDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
 
Private Function bIsValidDiscFixedDate() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    bIsValidDiscFixedDate = True
    
    If Len(Trim(txtDiscFixedDate)) = 0 Then GoTo Valid
     
    If Not gbValidLocalDate(txtDiscFixedDate.Text) Then GoTo Invalid
    
Valid:

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
Invalid:
    bIsValidDiscFixedDate = False
    'moVM.SetSotaMsgBox kmsgInvalidDate
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDiscFixedDate", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function bIsValidCODLabelForm() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim rs              As Object
    Dim bValid          As Boolean
    Dim sCODLabelFormID As String
    
    bIsValidCODLabelForm = True
    
    If gbValidComboSelect(cboCODLabelFormID) Then GoTo Valid

    sCODLabelFormID = ""
    bValid = gbValidCIBusinessForm(moClass.moFramework, moSotaObjects, moClass.moAppDB, rs, _
                                   mbMaintCIBusinessFormAOF, sCODLabelFormID, msCompanyID, kvLabel)
    
    If Not bValid Then GoTo Invalid
    
    cboCODLabelFormID.ListIndex = giComboAddItem(cboCODLabelFormID, _
                                  gsGetValidStr(rs.Field("BusinessFormID")), _
                                  glGetValidLong(rs.Field("BusinessFormKey")))
    
Valid:
    GoTo CloseRecordset
    
Invalid:
    bIsValidCODLabelForm = False
    GoTo CloseRecordset

CloseRecordset:
    If Not rs Is Nothing Then rs.Close: Set rs = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidCODLabelForm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function VMIsValidControl(ctlLostFocus As Control) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    VMIsValidControl = True
    
    Select Case True
            
        Case ctlLostFocus Is cboCODLabelFormID
            VMIsValidControl = bIsValidCODLabelForm()
        
        Case ctlLostFocus Is numDiscPct
            VMIsValidControl = bIsValidDiscPct()

        Case ctlLostFocus Is txtDiscFixedDate
            VMIsValidControl = bIsValidDiscFixedDate()
            
        Case ctlLostFocus Is txtDueFixedDate
            VMIsValidControl = bIsValidDueFixedDate()
    
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "VMIsValidControl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function




Private Function bIsValidDiscPct() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    bIsValidDiscPct = True
    
    moDMForm.SetColumnValue "DiscPct", (numDiscPct.Value / 100)
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bIsValidDiscPct", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
    Dim iActionCode     As Integer
    Dim lReturnCode     As Long
    Dim sNewKey         As String
    Dim vParseReturn    As Variant
    
     Select Case sKey
        
        Case kTbFinish, kTbFinishExit
            If Not moVM.IsValidDirtyCheck() Then Exit Sub
            iActionCode = kDmFailure
            iActionCode = moDMForm.Action(kDmFinish)
            Select Case iActionCode
                Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
            End Select
            
            If txtPmtTermsID.Enabled Then
                txtPmtTermsID.SetFocus: DoEvents
            End If
            
            If mlRunMode = kContextAOF Then
                moClass.moFramework.SavePosSelf
                Me.Hide
            End If
            
        Case kTbSave
            If Not moVM.IsValidDirtyCheck() Then Exit Sub
            iActionCode = kDmFailure
            iActionCode = moDMForm.Save(True)
            Select Case iActionCode
                Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
            End Select
            
        Case kTbCancel, kTbCancelExit
        
            iActionCode = kDmFailure
            iActionCode = moDMForm.Action(kDmCancel)
            Select Case iActionCode
                Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
            End Select
            If txtPmtTermsID.Enabled Then
                txtPmtTermsID.SetFocus: DoEvents
            End If
            
            If mlRunMode = kContextAOF Then
                moClass.moFramework.SavePosSelf
                Me.Hide
            End If

            
        Case kTbDelete
                    
            iActionCode = kDmFailure
            iActionCode = moDMForm.Action(kDmDelete)
            Select Case iActionCode
                Case kDmFailure, kDmError
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                    Exit Sub
            End Select
            If txtPmtTermsID.Enabled Then
                txtPmtTermsID.SetFocus: DoEvents
            End If
            
            If mlRunMode = kContextAOF Then
                moClass.moFramework.SavePosSelf
                Me.Hide
            End If

            
        Case kTbHelp
            
            gDisplayFormLevelHelp Me
            
        Case kTbPrint
            If Not moVM.IsValidDirtyCheck() Then Exit Sub
            If Not bConfirmUnloadSuccess() Then Exit Sub
            gPrintTask moClass.moFramework, ktskCIPaymentTermsRPT
            
        'BROWSE BUTTONS
        Case kTbFirst, kTbPrevious, kTbLast, kTbNext
            If Not moVM.IsValidDirtyCheck() Then
                Exit Sub
            End If

            If Not bConfirmUnloadSuccess(True) Then
                Exit Sub
            End If

            If Not bSetupNavMain() Then
                Exit Sub
            End If

            lReturnCode = glLookupBrowse(navMain, sKey, miFilter, sNewKey)
            Select Case lReturnCode
                Case MS_SUCCESS
                    'Success
                    If navMain.ReturnColumnValues.Count = 0 Then
                        Exit Sub
                    End If

                    txtPmtTermsID.Text = navMain.ReturnColumnValues.Item("PmtTermsID")
                    VMIsValidKey
                
                Case MS_EORS, MS_BORS
                    'Refresh the record currently displayed
                    VMIsValidKey
                
                Case Else
                     gLookupBrowseError lReturnCode, Me, moClass
            End Select
            
        Case kTbFilter
           miFilter = giToggleLookupFilter(miFilter)
           
        Case kTbFilter
    End Select
  
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub BindToolbar()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With tbrMain
        .LocaleID = moClass.moSysSession.Language
        
        If mlRunMode = kContextAOF Then
            .Build sotaTB_AOF
        Else
            .Build sotaTB_MULTI_ROW
            .RemoveSeparator kTbPrint
            .RemoveButton kTbCopyFrom
            .RemoveButton kTbMemo
            .RemoveButton kTbRenameId
       End If
    End With
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindToolbar", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub BindValMgr()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moVM = New clsValidationManager
    
    With moVM
    
Set .Form = frmPmtTerms
        Set .Class = moClass
        
        .BindKey txtPmtTermsID, lblPaymentTerms
        .SkipControls navMain
        
        .Bind cboCODLabelFormID, lblCODLabelFormID
        .Bind cboDueDateOption, lblDueDateOption
        .Bind cboDiscDateOption, lblDiscDateOption
        .Bind numDiscPct, lblDiscPct
        .Bind txtDiscFixedDate, msFixedDate
        .Bind txtDueFixedDate, msFixedDate
        
        .Init
        
    End With
        
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindValMgr", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub sbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bSetupNavMain() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sRestrictClause As String

    bSetupNavMain = False
    
    If Len(Trim(navMain.Tag)) = 0 Then
        'Navigator Setup
        sRestrictClause = "CompanyID = " & gsQuoted(msCompanyID)
        bSetupNavMain = gbLookupInit(navMain, moClass, moClass.moAppDB, kNavEntCIPaymentTerms, sRestrictClause)

        If bSetupNavMain Then
            navMain.Tag = kNavEntCIPaymentTerms
            DoEvents
        End If
    Else
        bSetupNavMain = True
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSetupNavMain", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function



Private Sub ClearUnboundControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moVM.SetAllValid
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearUnboundControls", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmPmtTerms"
End Function
#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyDataBindings moDMForm
                moFormCust.ApplyFormCust
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#End If


Private Sub txtPmtTermsID_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtPmtTermsID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsID_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTermsID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtPmtTermsID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtPmtTermsID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtPmtTermsID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtPmtTermsID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDescription, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDescription_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtDescription, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDescription_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub numDiscPct_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscPct_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscPct_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numDiscPct, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscPct_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscPct_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numDiscPct, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscPct_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscMinDays_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numDiscMinDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscMinDays_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscMinDays_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numDiscMinDays, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscMinDays_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscMinDays_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numDiscMinDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscMinDays_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscDayOrMonth_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numDiscDayOrMonth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscDayOrMonth_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscDayOrMonth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numDiscDayOrMonth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscDayOrMonth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDiscDayOrMonth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numDiscDayOrMonth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDiscDayOrMonth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueMinDays_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numDueMinDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDueMinDays_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueMinDays_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numDueMinDays, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDueMinDays_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueMinDays_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numDueMinDays, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDueMinDays_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueDayOrMonth_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange numDueDayOrMonth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDueDayOrMonth_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueDayOrMonth_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress numDueDayOrMonth, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDueDayOrMonth_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub numDueDayOrMonth_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus numDueDayOrMonth, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "numDueDayOrMonth_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkApplyToFreight_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkApplyToFreight, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkApplyToFreight_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkApplyToFreight_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkApplyToFreight, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkApplyToFreight_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkApplyToFreight_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkApplyToFreight, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkApplyToFreight_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkApplyToSTax_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkApplyToSTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkApplyToSTax_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkApplyToSTax_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkApplyToSTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkApplyToSTax_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkApplyToSTax_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkApplyToSTax, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkApplyToSTax_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCOD_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkCOD, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCOD_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkCOD_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkCOD, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkCOD_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDiscDateOption_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboDiscDateOption, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboDiscDateOption_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDiscDateOption_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboDiscDateOption, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboDiscDateOption_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDiscDateOption_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboDiscDateOption, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboDiscDateOption_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDueDateOption_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboDueDateOption, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboDueDateOption_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDueDateOption_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboDueDateOption, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboDueDateOption_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboDueDateOption_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboDueDateOption, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboDueDateOption_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCODLabelFormID_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboCODLabelFormID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCODLabelFormID_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCODLabelFormID_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboCODLabelFormID, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCODLabelFormID_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboCODLabelFormID_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboCODLabelFormID, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboCODLabelFormID_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscFixedDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDiscFixedDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDiscFixedDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscFixedDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDiscFixedDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDiscFixedDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDiscFixedDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDiscFixedDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDiscFixedDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDueFixedDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtDueFixedDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDueFixedDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDueFixedDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtDueFixedDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDueFixedDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtDueFixedDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtDueFixedDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtDueFixedDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If


















Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property







