VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "EntryLookupControls.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.ocx"
Begin VB.Form frmSplitBatch 
   Caption         =   "Split Batches"
   ClientHeight    =   6960
   ClientLeft      =   795
   ClientTop       =   840
   ClientWidth     =   9495
   HelpContextID   =   57585
   Icon            =   "cizdd001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6960
   ScaleWidth      =   9495
   Begin SOTAVM.SOTAValidationMgr valSplitBatch 
      Left            =   4440
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   741
      Style           =   0
   End
   Begin VB.CommandButton cmdClearAll 
      Caption         =   "&Clear All"
      Height          =   375
      Left            =   5880
      TabIndex        =   15
      Top             =   6000
      WhatsThisHelpID =   57615
      Width           =   1215
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "&Select All"
      Height          =   375
      Left            =   2160
      TabIndex        =   14
      Top             =   6000
      WhatsThisHelpID =   57614
      Width           =   1215
   End
   Begin VB.Frame fraTranList 
      Caption         =   "&Transactions"
      Height          =   4605
      Left            =   120
      TabIndex        =   12
      Top             =   1200
      Width           =   9255
      Begin FPSpreadADO.fpSpread grdTranList 
         Height          =   4065
         Left            =   120
         TabIndex        =   13
         Top             =   240
         WhatsThisHelpID =   57612
         Width           =   8985
         _Version        =   524288
         _ExtentX        =   15849
         _ExtentY        =   7170
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "cizdd001.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   2
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   16
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6570
      WhatsThisHelpID =   1
      Width           =   9495
      _ExtentX        =   16748
      _ExtentY        =   688
   End
   Begin Threed.SSPanel pnlPullInfo 
      Height          =   705
      Left            =   90
      TabIndex        =   7
      Top             =   480
      Visible         =   0   'False
      Width           =   3645
      _Version        =   65536
      _ExtentX        =   6429
      _ExtentY        =   1244
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin EntryLookupControls.TextLookup lkuShipment 
         Height          =   285
         Left            =   1200
         TabIndex        =   9
         Top             =   0
         WhatsThisHelpID =   57595
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   503
         ForeColor       =   -2147483640
         MaxLength       =   13
         LookupID        =   "Shipment"
         Datatype        =   0
         sSQLReturnCols  =   "TranID,,;"
      End
      Begin EntryLookupControls.TextLookup lkuSalesOrder 
         Height          =   285
         Left            =   1200
         TabIndex        =   11
         Top             =   360
         WhatsThisHelpID =   57594
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   503
         ForeColor       =   -2147483640
         LookupID        =   "SalesOrderWithCustomer"
         Datatype        =   0
         sSQLReturnCols  =   "TranNo,,;"
      End
      Begin VB.Label lblShipment 
         Caption         =   "Shi&pment"
         Height          =   195
         Left            =   30
         TabIndex        =   8
         Top             =   60
         Width           =   975
      End
      Begin VB.Label lblSalesOrder 
         Caption         =   "Sales &Order"
         Height          =   255
         Left            =   30
         TabIndex        =   10
         Top             =   390
         Width           =   975
      End
   End
   Begin Threed.SSPanel pnlPushInfo 
      Height          =   705
      Left            =   90
      TabIndex        =   3
      Top             =   510
      Width           =   3645
      _Version        =   65536
      _ExtentX        =   6429
      _ExtentY        =   1244
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin VB.CheckBox chkNewBatch 
         Alignment       =   1  'Right Justify
         Caption         =   "Create &New Batch"
         Height          =   285
         Left            =   0
         TabIndex        =   4
         Top             =   30
         WhatsThisHelpID =   57590
         Width           =   1785
      End
      Begin EntryLookupControls.TextLookup lkuTargetBatch 
         Height          =   285
         Left            =   1590
         TabIndex        =   6
         Top             =   330
         WhatsThisHelpID =   57589
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   503
         ForeColor       =   -2147483640
         MaxLength       =   7
         LookupID        =   "ARBatch"
         Datatype        =   0
         sSQLReturnCols  =   "BatchNo,lkuTargetBatch,;BatchID,,;BatchKey,,;"
      End
      Begin VB.Label lblTargetBatch 
         AutoSize        =   -1  'True
         Caption         =   "&Destination Batch"
         Height          =   195
         Left            =   30
         TabIndex        =   5
         Top             =   390
         Width           =   1260
      End
   End
   Begin SOTACalendarControl.SOTACalendar dteShipmentDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   6210
      TabIndex        =   26
      Top             =   480
      WhatsThisHelpID =   88377
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin VB.Label lblShipmentDate 
      Caption         =   "Shipment Date"
      Height          =   285
      Left            =   4920
      TabIndex        =   27
      Top             =   510
      Width           =   1185
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   0
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmSplitBatch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmSplitBatch
'     Desc: CI Split Batches
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
'************************************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Private variables of Form Properties
    Private moClass             As clsSplitBatch
    
    Private mlRunMode           As Long
    Private mbSaved             As Boolean
    Private mbCancelShutDown    As Boolean
    Private miSecurityLevel     As Integer
    
'Context Menu Object
    Private moContextMenu       As clsContextMenu

'Data Manager Object(s)
    Private WithEvents moDMTranList        As clsDmGrid
Attribute moDMTranList.VB_VarHelpID = -1

'Public Form Variables
    Public moSotaObjects        As Collection
    Public moMapSrch            As Collection

'Miscellaneous Variables
    Private mbEnterAsTab        As Boolean
    Private msCompanyID         As String
    Private mbLookupClick       As Boolean
       
    Private TotalRowSelected    As Long
    
' Resize Form Variables
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long

' Grid constants
    Private Const kColSelect = 1
    Private Const kColTranID = 2
    Private Const kColTranID2 = 3
    Private Const kColShipID = 4
    Private Const kColTranDate = 5
    Private Const kColEntity = 6
    Private Const kColEntityName = 7
    Private Const kColTranAmt = 8
    Private Const kColTranAmtHC = 9
    Private Const kColDigitsAfterDec = 10
    Private Const kColSOKey = 11
    Private Const kColShipKey = 12
    Private Const kColDocKey = 13
    Private Const kColTranStatus = 14
    Private Const kMaxCols = 14

    Private Const kTranDateReq As Integer = 1
    Private Const kTranDateInvalid As Integer = 2
    Private Const kTranDateValid As Integer = 3
    
    Private Const kTranStatusPending As Integer = 2 'Sales order payments transaction status
    
    Const kPush = 0 ' push into new batch
    Const kPull = 1 ' pull shipments from system batch
    Const kPullSOPmts = 2 ' pull SO payments from SO payment batch.

Const kmsgBatchNoExists = 140468                  ' Batch {0} exists.  Batch to create must be a new batch number.
Const kmsgInvalidBatch = 100347

'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Const VBRIG_MODULE_ID_STRING = "CIZDD001.FRM"

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = "CIZ"
End Property

'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = "CIZ"
End Property

'************************************************************************
'   oClass contains the reference to the parent class object.  The form
'   needs this reference to use the public variables created within the
'   class object.
'************************************************************************
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set oClass = moClass

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Set moClass = oNewClass

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

'************************************************************************
'   lRunMode contains the user-defined run mode context in which the
'   class object was Initialzed. The run mode is extracted from the
'   context passed in through the InitializeObject.
'************************************************************************
Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
    lRunMode = mlRunMode
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property

'************************************************************************************
'   bSaved is used specifically for Add-On-The-Fly.  It tells the
'   the calling object whether the record was saved or not.
'************************************************************************************
Public Property Get bSaved() As Boolean
'+++ VB/Rig Skip +++
    bSaved = mbSaved
End Property

Public Property Let bSaved(bNewSaved As Boolean)
'+++ VB/Rig Skip +++
    mbSaved = bNewSaved
End Property

'************************************************************************
'   bCancelShutDown tells the framework whether the form has requested
'   the shutdown process to be cancelled.
'************************************************************************
Public Property Get bCancelShutDown()
'+++ VB/Rig Skip +++
    bCancelShutDown = mbCancelShutDown
End Property

Private Sub cmdClearAll_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdClearAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRow    As Long
    
    For lRow = 1 To grdTranList.MaxRows
        gGridUpdateCell grdTranList, lRow, kColSelect, 0
    Next lRow
End Sub

Private Sub cmdSelectAll_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelectAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRow    As Long
    
    For lRow = 1 To grdTranList.MaxRows
        gGridUpdateCell grdTranList, lRow, kColSelect, 1
    Next lRow
End Sub

Private Sub dteShipmentDate_LostFocus() 'SGS PTC IA
    Dim sWhere  As String
    
    sWhere = "BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    If LTrim(RTrim(lkuSalesOrder.Text)) <> "" Then
        sWhere = sWhere & " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl WITH (NOLOCK) "
        sWhere = sWhere & " WHERE SOLineKey IN (SELECT SOLineKey FROM tsoSOLine WITH (NOLOCK) "
        sWhere = sWhere & " WHERE SOKey IN (SELECT SOKey FROM tsoSalesOrder WITH (NOLOCK) "
        sWhere = sWhere & " WHERE TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text))) & ")))"
    End If
    If LTrim(RTrim(lkuShipment.Text)) <> "" Then
        sWhere = sWhere & " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl WITH (NOLOCK) "
        sWhere = sWhere & " WHERE ShipLineKey IN (SELECT ShipLineKey FROM tsoShipLine WITH (NOLOCK) "
        sWhere = sWhere & " WHERE ShipKey IN (SELECT ShipKey FROM tsoShipment WITH (NOLOCK) "
        sWhere = sWhere & " WHERE TranNo = " & gsQuoted(LTrim(RTrim(lkuShipment.Text))) & ")))"
    End If
    If LTrim(RTrim(dteShipmentDate.Value)) <> "" Then 'ptc
        sWhere = sWhere & " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl (NOLOCK) " 'ptc
        sWhere = sWhere & " WHERE TranDate = " & gsQuoted(LTrim(RTrim(dteShipmentDate.Value))) & ")" 'ptc
    End If 'ptc
    moDMTranList.Where = sWhere
    moDMTranList.Refresh
End Sub


Private Sub Form_Initialize()
'+++ VB/Rig Skip +++
    mbCancelShutDown = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True.
'********************************************************************

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview property
'         of the form is set to True.
'
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Paint()
'+++ VB/Rig Skip +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Skip +++
'********************************************************************
'   Description:
'       Form_Load initializes variables to be used throughout the
'       form's user interface.  It also sets up control
'       specific properties.  For example, here you would fill a
'       combo box with static/dynamic list information.
'********************************************************************
    On Error GoTo ExpectedErrorRoutine

    Set sbrMain.Framework = moClass.moFramework
    sbrMain.BrowseVisible = False
    

    TotalRowSelected = 0

    ' Instantiate collection objects.
    Set moSotaObjects = New Collection
    Set moMapSrch = New Collection

    ' Initialize Form Resizing variables.
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth

    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab

    ' Setup the toolbar.
    With tbrMain
        .LocaleID = moClass.moSysSession.Language
        .Build sotaTB_PROCESS
    End With

    BindLookup
    BindContextMenu
    FormatGrid
    BindForm
    
    ' Setup Display Only UI.
    miSecurityLevel = giSetAppSecurity(moClass, tbrMain)

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "Form_Load"
    gClearSotaErr

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
    Dim i As Integer

   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    'Check all other forms  that may have been loaded from this main form.
    'If there are any Visible forms, then this means the form is Active.
    'Therefore, cancel the shutdown.
    If gbActiveChildForms(Me) Then GoTo CancelShutDown

    Select Case UnloadMode
        Case vbFormCode
        'Do Nothing.
                
        Case Else
       'Most likely the user has requested to shut down the form.
        'If the context is Add-on-the-fly, hide the form
        'and cancel the unload.
        'If the context is normal or Drill-Around, have the object unload itself.
             Select Case mlRunMode
                Case kContextAOF
                    CloseForm
                    GoTo CancelShutDown
                Case Else
                    moClass.miShutDownRequester = kUnloadSelfShutDown
            End Select
    End Select
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else 'kFrameworkShutDown
            'Do nothing
    End Select
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Skip +++
    On Error Resume Next
    
    'Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me
    
    Exit Sub

End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
' *** SAMPLE CODE FOR RESIZING THE FORM
' *** You must put any controls that do not have a handle, and that need to
'     be moved when the resizable control resizes, in a panel.
'     For example, Label, Line, certain Sage MAS 500 controls (SOTAHLP, SOTASTAT) should be
'     in a panel.  Certain controls such as sysinfo (no handle)  that does not
'     display does not need to be in a panel.
'
' *** If the grid is on a panel (i.e. pnlUnderGrid), then it must also be included
'     in the resizable list.  This applies for tabs with grids on panels on the tab.
'
' *** IF you want to  resize in only a certain direction and not the other, then you
'     will need code to keep the form from sizing in that direction.  Do not call the
'     gResizeForm routine for that direction

    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraTranList, grdTranList
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraTranList, grdTranList

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Skip +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    
    Dim sSQL            As String

    On Error Resume Next

    sSQL = "DROP TABLE #tciAPVouchers"
    moClass.moAppDB.ExecuteSQL sSQL

    Set moSotaObjects = Nothing
    Set moMapSrch = Nothing
    Set moContextMenu = Nothing
    

    lkuTargetBatch.Terminate
    lkuSalesOrder.Terminate
    lkuShipment.Terminate

    If Not moDMTranList Is Nothing Then
        moDMTranList.UnloadSelf
        Set moDMTranList = Nothing
    End If

End Sub

Private Sub grdTranList_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)

    Dim lRow                As Long
    
    'For performance, get out when loading the data, don't need to do this until user performs an action.
    'ButtonClicked is firing in DMGridRowLoaded when setting the selected column to unchecked.
    If moDMTranList.State = kDmStateNone Then
        Exit Sub
    End If
    
    If (Col = kColSelect) Then
       If gsGridReadCell(grdTranList, Row, kColSelect) = 1 Then
             TotalRowSelected = TotalRowSelected + 1
       Else
            If (TotalRowSelected > 0) Then
                TotalRowSelected = TotalRowSelected - 1
            End If
       End If
        
        If (TotalRowSelected > 0) Then
            tbrMain.ButtonEnabled(kTbProceed) = True
        Else
            tbrMain.ButtonEnabled(kTbProceed) = False
        End If
        
    End If

End Sub



Private Sub lkuSalesOrder_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
    If IsNumeric(lkuSalesOrder.Text) <> 0 Then
        lkuSalesOrder.Text = gsZeroFill(lkuSalesOrder.Text, 10)
    End If
End Sub

Private Sub lkuSalesOrder_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSalesOrder, True
    #End If
'+++ End Customizer Code Push +++
    Dim sWhere  As String
    
    sWhere = "BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))

    sWhere = sWhere & sGetSelectionFromLookup
    
    If LTrim(RTrim(dteShipmentDate.Value)) <> "" Then 'SGS PTC IA
        sWhere = sWhere & " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl (NOLOCK) " 'SGS PTC IA
        sWhere = sWhere & " WHERE TranDate = " & gsQuoted(LTrim(RTrim(dteShipmentDate.Value))) & ")" 'SGS PTC IA
    End If 'SGS PTC IA
    
    moDMTranList.Where = sWhere
    If moClass.BatchType = kBatchTypeARIN Then PopulateSOTranID
    moDMTranList.Refresh
    
    lkuSalesOrder.Tag = lkuSalesOrder.Text
End Sub

Private Sub lkuSalesOrder_LostFocusText()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuSalesOrder, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim(lkuSalesOrder.Text)) > 0 Then
        If IsNumeric(lkuSalesOrder.Text) Then
            lkuSalesOrder.Text = gsZeroFill(lkuSalesOrder.Text, 10)
        End If
    End If
End Sub

Private Sub lkuShipment_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
    Dim sTemp   As String
    Dim iDash   As Integer
    
    If Len(lkuShipment.Text) <> 0 And Len(lkuShipment.Text) < 13 Then
        iDash = InStr(1, lkuShipment.Text, "-")
        If iDash > 0 Then
            sTemp = Left$(lkuShipment.Text, iDash - 1)
            sTemp = gsZeroFill(sTemp, 10)
            lkuShipment.Text = sTemp & Mid$(lkuShipment.Text, iDash)
        Else
            lkuShipment.Text = gsZeroFill(lkuShipment.Text, 10)
        End If
    End If
    
End Sub

Private Sub lkuShipment_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuShipment, True
    #End If
'+++ End Customizer Code Push +++
    Dim sWhere  As String
    
    sWhere = "BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))

    sWhere = sWhere & sGetSelectionFromLookup
    
    If LTrim(RTrim(dteShipmentDate.Value)) <> "" Then 'SGS PTC IA
        sWhere = sWhere & " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl (NOLOCK) " 'SGS PTC IA
        sWhere = sWhere & " WHERE TranDate = " & gsQuoted(LTrim(RTrim(dteShipmentDate.Value))) & ")" 'SGS PTC IA
    End If 'SGS PTC IA
    
    moDMTranList.Where = sWhere
    If moClass.BatchType = kBatchTypeARIN Then PopulateSOTranID
    moDMTranList.Refresh
End Sub
Private Function sGetSelectionFromLookup() As String

    Dim sWhere  As String
   
    sWhere = ""
   
    If LTrim(RTrim(lkuSalesOrder.Text)) <> "" And LTrim(RTrim(lkuShipment.Text)) <> "" Then
        sWhere = " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) "
        sWhere = sWhere & " JOIN tsoShipLine sl WITH (NOLOCK) ON ivd.ShipLineKey = sl.ShipLineKey "
        sWhere = sWhere & " JOIN tsoShipment s WITH (NOLOCK) ON sl.ShipKey = s.ShipKey "
        sWhere = sWhere & " JOIN tsoSOLine sol WITH (NOLOCK) ON ivd.SOLineKey = sol.SOLineKey "
        sWhere = sWhere & " JOIN tsoSalesOrder so WITH (NOLOCK) ON sol.SOKey = so.SOKey "
        
        If (gsGetValidStr(lkuSalesOrder.Tag) <> lkuSalesOrder.Text) Then
            If (lkuSalesOrder.ReturnColumnValues.Count > 0) And mbLookupClick = True Then
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text)))
                sWhere = sWhere & " AND so.TranNoRelChngOrd = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.ReturnColumnValues("TranNoRelChngOrd"))))
            Else
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text)))
            End If
        Else
            If mbLookupClick = False Then
                 sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text)))
            Else
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text)))
                sWhere = sWhere & " AND so.TranNoRelChngOrd = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.ReturnColumnValues("TranNoRelChngOrd"))))
            End If
        End If
        sWhere = sWhere & " AND s.TranNo = " & gsQuoted(LTrim(RTrim(lkuShipment.Text))) & ")"
    ElseIf LTrim(RTrim(lkuSalesOrder.Text)) <> "" Then
        sWhere = " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) "
        sWhere = sWhere & " JOIN tsoShipLine sl WITH (NOLOCK) ON ivd.ShipLineKey = sl.ShipLineKey "
        sWhere = sWhere & " JOIN tsoShipment s WITH (NOLOCK) ON sl.ShipKey = s.ShipKey "
        sWhere = sWhere & " JOIN tsoSOLine sol WITH (NOLOCK) ON ivd.SOLineKey = sol.SOLineKey "
        sWhere = sWhere & " JOIN tsoSalesOrder so WITH (NOLOCK) ON sol.SOKey = so.SOKey "
        If (gsGetValidStr(lkuSalesOrder.Tag) <> lkuSalesOrder.Text) Then
            If (lkuSalesOrder.ReturnColumnValues.Count > 0) And mbLookupClick = True Then
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text)))
                sWhere = sWhere & " AND so.TranNoRelChngOrd = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.ReturnColumnValues("TranNoRelChngOrd")))) & ")"
            Else
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text))) & ")"
            End If
        Else
            If mbLookupClick = False Then
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text))) & ")"
            Else
                sWhere = sWhere & " WHERE so.TranNo = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.Text)))
                sWhere = sWhere & " AND so.TranNoRelChngOrd = " & gsQuoted(LTrim(RTrim(lkuSalesOrder.ReturnColumnValues("TranNoRelChngOrd")))) & ")"
            End If
        End If
    ElseIf LTrim(RTrim(lkuShipment.Text)) <> "" Then
        sWhere = " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) "
        sWhere = sWhere & " JOIN tsoShipLine sl WITH (NOLOCK) ON ivd.ShipLineKey = sl.ShipLineKey "
        sWhere = sWhere & " JOIN tsoShipment s WITH (NOLOCK) ON sl.ShipKey = s.ShipKey "
        sWhere = sWhere & " WHERE s.TranNo = " & gsQuoted(LTrim(RTrim(lkuShipment.Text))) & ")"
    Else
        sWhere = " AND InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) "
        sWhere = sWhere & " JOIN tsoShipLine sl WITH (NOLOCK) ON ivd.ShipLineKey = sl.ShipLineKey "
        sWhere = sWhere & " JOIN tsoShipment s WITH (NOLOCK) ON sl.ShipKey = s.ShipKey )"
    End If
    
    mbLookupClick = False
    
    sGetSelectionFromLookup = sWhere

End Function

Private Sub lkuShipment_LostFocusText()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuShipment, True
    #End If
'+++ End Customizer Code Push +++
    Dim sTemp   As String
    Dim iDash   As Integer

    If Len(Trim(lkuShipment.Text)) > 0 And Len(lkuShipment.Text) < 13 Then
        iDash = InStr(1, lkuShipment.Text, "-")
        If iDash > 0 Then
            sTemp = Left$(lkuShipment.Text, iDash - 1)
            sTemp = gsZeroFill(sTemp, 10)
            lkuShipment.Text = sTemp & Mid$(lkuShipment.Text, iDash)
        Else
            lkuShipment.Text = gsZeroFill(lkuShipment.Text, 10)
        End If
    End If
End Sub

Private Sub lkuTargetBatch_BeforeValidate(Text As String, iReturn As EntryLookupControls.ENTRY_LKU_CTRL_ValidateTypes)
    lkuTargetBatch.Text = gsZeroFill(Text, 7)
End Sub

Private Sub lkuTargetBatch_KeyPress(KeyAscii As Integer)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuTargetBatch, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If KeyAscii > 31 And KeyAscii < 128 Then
        If KeyAscii < Asc("0") Or KeyAscii > Asc("9") Then
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub lkuTargetBatch_LostFocusText()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus lkuTargetBatch, True
    #End If
'+++ End Customizer Code Push +++
    If Len(Trim(lkuTargetBatch.Text)) > 0 Then
        lkuTargetBatch.Text = gsZeroFill(lkuTargetBatch.Text, 7)
    End If
End Sub

Private Sub moDMTranList_DMGridRowLoaded(lRow As Long)
    Dim lKey            As Long     'VoucherKey or InvcKey
    Dim sCurrID         As String   'Document currency
    Dim iDecimalPlaces  As Integer  'document currency decimal places
    Dim sTranID         As String   'Sales order Tran ID
    Dim lSOKey          As Long     'Sales order key
    
    gGridUpdateCell grdTranList, lRow, kColSelect, 0
    
    iDecimalPlaces = giGetValidInt(gsGridReadCell(grdTranList, lRow, kColDigitsAfterDec))
        
    With grdTranList
        .Col = kColTranAmt
        .Col2 = kColTranAmt
        .Row = lRow
        .Row2 = lRow
        .BlockMode = True
        .TypeFloatDecimalPlaces = iDecimalPlaces
        .BlockMode = False
        .redraw = True
    End With
        
End Sub



Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    HandleToolBarClick Button
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bPreSave() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    bPreSave = False
    
    'force last control lost_focus
    gbSetFocus Me, tbrMain
    DoEvents
       
    If Not valSplitBatch.ValidateForm Then
        lkuTargetBatch.Text = ""
        Exit Function
    End If
    
    bPreSave = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bPreProcessSave", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Skip +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
    Dim lNextRow        As Long
    Dim iType           As Integer

    On Error GoTo ExpectedErrorRoutine

    Me.SetFocus
    Select Case sKey
        Case kTbClose
            CloseForm

        Case kTbHelp
            gDisplayFormLevelHelp Me

        Case kTbProceed

            Select Case moClass.PushPull
                Case kPush
                    If Not bPreSave Then
                        Exit Sub
                    End If
                    
                    If PushVouchersOrInvoices() Then
                        CloseForm
                    End If

                Case kPull
                    If PullInvoices() Then
                        CloseForm
                    End If
                    
                Case kPullSOPmts
                    If PullCustPmts() Then
                        CloseForm
                    End If

            End Select

        Case kTbFirst

        Case kTbLast

        Case kTbNext

        Case kTbPrevious
        
        Case Else
            tbrMain.GenericHandler sKey, Me, Nothing, moClass
        
    End Select

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "HandleToolbarClick"
    gClearSotaErr
        
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Set moContextMenu = New clsContextMenu  'Instantiate Context Menu Class

    With moContextMenu
        Set .Form = frmSplitBatch
        .Init                      'Init will set properties of Winhook control
    End With

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Private Sub BindForm()
'+++ VB/Rig Skip +++
    On Error GoTo ExpectedErrorRoutine

   ' create new data Manager class(es)
    Set moDMTranList = New clsDmGrid

    Select Case moClass.BatchType
        Case kBatchTypeAPVO, kBatchTypePOVO
            BindVouchers

        Case kBatchTypeARIN
            BindInvoices
            
        Case kBatchTypeARCR
            BindCustPmts

    End Select

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "BindForm"
    gClearSotaErr

End Sub

Private Sub BindVouchers()

    With moDMTranList
        .Table = "tapPendVoucher"
        .UniqueKey = "VoucherKey"
        .UIType = kDmUINone
        .OrderBy = "TranID"
        .NoInsert = True
        .NoAppend = True
        
        Set .Form = frmSplitBatch
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdTranList
        
        .BindColumn "BatchKey", Nothing, SQL_INTEGER
        .BindColumn "VoucherKey", kColDocKey, SQL_INTEGER
        .BindColumn "VendKey", Nothing, SQL_INTEGER
        .BindColumn "TranID", kColTranID, SQL_CHAR
        .BindColumn "TranAmt", kColTranAmt, SQL_DECIMAL
        .BindColumn "TranAmtHC", kColTranAmtHC, SQL_DECIMAL
        .BindColumn "TranDate", kColTranDate, SQL_DATE

        .LinkSource "tapVendor", "tapVendor.VendKey = tapPendVoucher.VendKey", kDmJoin
        .Link kColEntity, "VendID"
        .Link kColEntityName, "VendName"
        
        .LinkSource "tmcCurrency", "tmcCurrency.CurrID = tapPendVoucher.CurrID", kDmJoin
        .Link kColDigitsAfterDec, "DigitsAfterDecimal"

    End With

End Sub

Private Sub BindInvoices()

    With moDMTranList
        .Table = "tarPendInvoice"
        .UniqueKey = "InvcKey"
        .UIType = kDmUINone
        .OrderBy = "TranID"
        .NoInsert = True
        .NoAppend = True
        
        Set .Form = frmSplitBatch
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdTranList
        
        .BindColumn "BatchKey", Nothing, SQL_INTEGER
        .BindColumn "InvcKey", kColDocKey, SQL_INTEGER
        .BindColumn "CustKey", Nothing, SQL_INTEGER
        .BindColumn "TranID", kColTranID, SQL_VARCHAR
        .BindColumn "TranAmt", kColTranAmt, SQL_DECIMAL
        .BindColumn "TranAmtHC", kColTranAmtHC, SQL_DECIMAL
        .BindColumn "TranDate", kColTranDate, SQL_DATE

        .LinkSource "tarCustomer", "tarCustomer.CustKey = tarPendInvoice.CustKey", kDmJoin
        .Link kColEntity, "CustID"
        .Link kColEntityName, "CustName"
        
        .LinkSource "tmcCurrency", "tmcCurrency.CurrID = tarPendInvoice.CurrID", kDmJoin
        .Link kColDigitsAfterDec, "DigitsAfterDecimal"
                
        .LinkSource "#SOTranID", "tarPendInvoice.InvcKey = #SOTranID.TmpInvcKey", kDmJoin, LeftOuter
        .Link kColTranID2, "TmpTranID"
    End With

End Sub

Private Sub BindCustPmts()

    With moDMTranList
        .Table = "tarPendCustPmt"
        .UniqueKey = "CustPmtKey"
        .UIType = kDmUINone
        .OrderBy = "tarPendCustPmt.TranID"
        .NoInsert = True
        .NoAppend = True
        
        Set .Form = frmSplitBatch
        Set .Session = moClass.moSysSession
        Set .Database = moClass.moAppDB
        Set .Grid = grdTranList
        
        .BindColumn "BatchKey", Nothing, SQL_INTEGER
        .BindColumn "CustPmtKey", kColDocKey, SQL_INTEGER
        .BindColumn "CustKey", Nothing, SQL_INTEGER
        .BindColumn "TranID", kColTranID, SQL_CHAR
        .BindColumn "TranDate", kColTranDate, SQL_DATE
        .BindColumn "TranAmt", kColTranAmt, SQL_DECIMAL
        .BindColumn "TranAmtHC", kColTranAmtHC, SQL_DECIMAL

        .LinkSource "tarCustomer", "tarCustomer.CustKey = tarPendCustPmt.CustKey", kDmJoin
        .Link kColEntity, "CustID"
        .Link kColEntityName, "CustName"
        
        .LinkSource "tmcCurrency", "tmcCurrency.CurrID = tarPendCustPmt.CurrID", kDmJoin
        .Link kColDigitsAfterDec, "DigitsAfterDecimal"
        
        .LinkSource "tarCustPmtLog", "tarCustPmtLog.CustPmtKey = tarPendCustPmt.CustPmtKey", kDmJoin
        .Link kColTranStatus, "TranStatus"
        
        .LinkSource "tarSalesOrderPmt", "tarPendCustPmt.CustPmtKey = tarSalesOrderPmt.CustPmtKey", kDmJoin, LeftOuter
        .Link kColSOKey, "SOKey"
        .Link kColShipKey, "ShipKey"
        
        .LinkSource "tsoSalesOrder", "tsoSalesOrder.SOKey = tarSalesOrderPmt.SOKey", kDmJoin, LeftOuter
        .Link kColTranID2, "TranID"
        
        .LinkSource "tsoPendShipment", "tsoPendShipment.ShipKey = tarSalesOrderPmt.ShipKey", kDmJoin, LeftOuter
        .Link kColShipID, "TranID"
                     
    End With

End Sub

Private Sub BindLookup()
'+++ VB/Rig Skip +++
    On Error GoTo ExpectedErrorRoutine
    
    Dim sSQL As String
    
    Select Case moClass.BatchType
        Case kBatchTypeAPVO, kBatchTypePOVO
            With lkuTargetBatch
                .LookupID = "Batch"

                Set .Framework = moClass.moFramework
                Set .SysDB = moClass.moAppDB
                Set .AppDatabase = moClass.moAppDB
            End With

            sSQL = "CREATE TABLE #tciAPVouchers (VouchKey INTEGER NOT NULL)"
            moClass.moAppDB.ExecuteSQL sSQL

        Case kBatchTypeARIN
            Select Case moClass.PushPull
                Case kPull
                    'Initialize for shipment pull.
                    With lkuSalesOrder
                        Set .Framework = moClass.moFramework
                        Set .SysDB = moClass.moAppDB
                        Set .AppDatabase = moClass.moAppDB
                    End With
            
                    With lkuShipment
                        Set .Framework = moClass.moFramework
                        Set .SysDB = moClass.moAppDB
                        Set .AppDatabase = moClass.moAppDB
                    End With
                    
               Case Else
                    'kPush = Initialize for invoice push.
                    With lkuTargetBatch
                        .LookupID = "ARBatch"

                        Set .Framework = moClass.moFramework
                        Set .SysDB = moClass.moAppDB
                        Set .AppDatabase = moClass.moAppDB
                    End With
            End Select

            sSQL = "CREATE TABLE #tciARInvoices (InvcKey INTEGER NOT NULL)"
            moClass.moAppDB.ExecuteSQL sSQL
            
            sSQL = "CREATE TABLE #SOTranID (TmpInvcKey INTEGER NOT NULL, TmpTranID VARCHAR(19) NULL)"
            moClass.moAppDB.ExecuteSQL sSQL
            
        Case kBatchTypeARCR
            sSQL = "CREATE TABLE #tciARCustPmts (CustPmtKey INTEGER NOT NULL)"
            moClass.moAppDB.ExecuteSQL sSQL
    End Select
    
    Exit Sub
ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "BindLookup"
    gClearSotaErr
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmSplitBatch"
End Function

Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sCurrID As String           'Home currency ID
    Dim iDecimalPlaces As Integer   'Number of decimal places for home currency
    
    grdTranList.MaxCols = kMaxCols

    gGridSetProperties grdTranList, grdTranList.MaxCols, kGridDataSheetNoAppend

    gGridLockColumn grdTranList, kColTranID
    gGridLockColumn grdTranList, kColTranID2
    gGridLockColumn grdTranList, kColTranDate
    gGridLockColumn grdTranList, kColEntity
    gGridLockColumn grdTranList, kColEntityName
    gGridLockColumn grdTranList, kColTranAmt
    gGridLockColumn grdTranList, kColTranAmtHC
    gGridLockColumn grdTranList, kColShipID

    grdTranList.DisplayRowHeaders = False

    gGridSetColumnWidth grdTranList, kColSelect, 6
    gGridSetColumnWidth grdTranList, kColTranID, 12
    gGridSetColumnWidth grdTranList, kColTranID2, 12
    gGridSetColumnWidth grdTranList, kColShipID, 12
    gGridSetColumnWidth grdTranList, kColTranDate, 8
    gGridSetColumnWidth grdTranList, kColEntity, 10
    gGridSetColumnWidth grdTranList, kColEntityName, 20
    gGridSetColumnWidth grdTranList, kColTranAmt, 15
    gGridSetColumnType grdTranList, kColTranAmt, SS_CELL_TYPE_FLOAT, 3, 12 'will be set to correct dec places in DMGridRowLoaded
   
    grdTranList.TypeFloatSeparator = True
    gGridSetColumnWidth grdTranList, kColTranAmtHC, 15
    
    sCurrID = moClass.moAppDB.Lookup("CurrID", "tsmCompany", "CompanyID = " & gsQuoted(msCompanyID))
    iDecimalPlaces = moClass.moAppDB.Lookup("DigitsAfterDecimal", "tmcCurrency", "CurrID = " & gsQuoted(sCurrID))
    
    gGridSetColumnType grdTranList, kColTranAmtHC, SS_CELL_TYPE_FLOAT, iDecimalPlaces
    
    gGridHideColumn grdTranList, kColDigitsAfterDec
    gGridHideColumn grdTranList, kColDocKey
    'Used by SO payments
    gGridHideColumn grdTranList, kColSOKey
    gGridHideColumn grdTranList, kColTranStatus
    gGridHideColumn grdTranList, kColShipKey

    gGridSetHeader grdTranList, kColSelect, "Select"
        
    Select Case moClass.BatchType
        Case kBatchTypeARCR
            gGridSetHeader grdTranList, kColTranID, "Payment Ref"
            gGridSetHeader grdTranList, kColTranDate, "Payment Date"
            gGridSetHeader grdTranList, kColTranID2, "Sales Order"
            gGridSetHeader grdTranList, kColShipID, "Shipment"
            gGridSetHeader grdTranList, kColTranAmt, "Payment Amt"
            gGridSetHeader grdTranList, kColTranAmtHC, "Payment Amt HC"
            gGridShowColumn grdTranList, kColTranID2
            gGridShowColumn grdTranList, kColShipID
            
        Case kBatchTypeARIN
            gGridSetHeader grdTranList, kColTranID, "Tran ID"
            gGridSetHeader grdTranList, kColTranDate, "Tran Date"
            gGridSetHeader grdTranList, kColTranID2, "Sales Order"
            gGridSetHeader grdTranList, kColTranAmt, "Tran Amt"
            gGridSetHeader grdTranList, kColTranAmtHC, "Tran Amt HC"
            gGridHideColumn grdTranList, kColShipID
        
        Case Else
            gGridSetHeader grdTranList, kColTranID, "Tran ID"
            gGridSetHeader grdTranList, kColTranDate, "Tran Date"
            gGridSetHeader grdTranList, kColTranID2, "Tran ID 2"
            gGridSetHeader grdTranList, kColTranAmt, "Tran Amt"
            gGridSetHeader grdTranList, kColTranAmtHC, "Tran Amt HC"
            gGridHideColumn grdTranList, kColTranID2
            gGridHideColumn grdTranList, kColShipID
    End Select
    
    Select Case moClass.BatchType
        Case kBatchTypeAPVO, kBatchTypePOVO
            gGridSetHeader grdTranList, kColEntity, "Vendor"
            gGridSetHeader grdTranList, kColEntityName, "Vendor Name"

        Case kBatchTypeARIN, kBatchTypeARCR
            gGridSetHeader grdTranList, kColEntity, "Customer"
            gGridSetHeader grdTranList, kColEntityName, "Customer Name"

        Case Else
            gGridSetHeader grdTranList, kColEntity, "Entity ID"
            gGridSetHeader grdTranList, kColEntityName, "Entity Name"

    End Select
    gGridSetColumnType grdTranList, kColSelect, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdTranList, kColTranDate, SS_CELL_TYPE_DATE

    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub FormSetup()
'+++ VB/Rig Skip +++
    On Error GoTo ExpectedErrorRoutine

    Dim iRetVal As Integer
    Dim sCaption As String
    Dim sSQL As String
    Dim sRestrict As String
    Dim lBatchType As Long
    
    'Initialize the proceed button to disabled.   Will be enabled when the user selects a transaction in the grid.
    tbrMain.ButtonEnabled(kTbProceed) = False
    
    BindLookup

    'Set up form title with Batch ID.
    Select Case moClass.PushPull
        Case kPush
            'Move transactions into another batch.
            sCaption = "Move Transactions from Batch "
            
            pnlPushInfo.Visible = True
            pnlPullInfo.Visible = False
            
            With moDMTranList
                .Where = "BatchKey = " & moClass.BatchKey
                .Init
            End With

            Select Case moClass.BatchType
                Case kBatchTypeAPVO
                    lBatchType = moClass.BatchType 'kBatchTypeAPVO
                    sSQL = "TRUNCATE TABLE #tciAPVouchers"
                    moClass.moAppDB.ExecuteSQL sSQL
                
                    With lkuTargetBatch
                        .LookupID = "APBatch"
                        .RestrictClause = "BatchNo <> " & Format$(moClass.BatchNo) & _
                                          " AND BatchType = " & Format$(lBatchType) & _
                                          " AND Status in (3,4) " & _
                                          " AND PostStatus = 0 " & _
                                          " AND SourceCompany = " & gsQuoted(msCompanyID)
                        .Text = ""
                    End With
                    
                    
                Case kBatchTypePOVO
                    lBatchType = moClass.BatchType 'kBatchTypeAPVO
                    sSQL = "TRUNCATE TABLE #tciAPVouchers"
                    moClass.moAppDB.ExecuteSQL sSQL
                
                    With lkuTargetBatch
                        .LookupID = "POBatch"
                        .RestrictClause = "BatchNo <> " & Format$(moClass.BatchNo) & _
                                          " AND BatchType = " & Format$(lBatchType) & _
                                          " AND Status in (3,4) " & _
                                          " AND PostStatus = 0 " & _
                                          " AND SourceCompany = " & gsQuoted(msCompanyID)
                        .Text = ""
                    End With

                Case Else
                    'kBatchType ARIN
                    lBatchType = kBatchTypeARIN
                    sSQL = "TRUNCATE TABLE #tciARInvoices"
                    moClass.moAppDB.ExecuteSQL sSQL

                    With lkuTargetBatch
                        .LookupID = "ARBatch"
                        .RestrictClause = "BatchNo <> " & Format$(moClass.BatchNo) & _
                                          " AND BatchType = " & Format$(lBatchType) & _
                                          " AND Status IN (3,4) " & _
                                          " AND PostStatus = 0 " & _
                                          " AND SourceCompany = " & gsQuoted(msCompanyID)
                        .Text = ""
                    End With
            End Select

        Case kPull
            'Move non-invoiced shipments into AR batch.
            sCaption = "Move Transactions into Batch "
            
            pnlPushInfo.Visible = False
            pnlPullInfo.Visible = True
            
            'Restrict to only show committed or posted shipments that have invoices in system batch.
            With moDMTranList
                '.Where = "BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
            
                sRestrict = "InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) JOIN tsoShipLine sl WITH (NOLOCK) "
                sRestrict = sRestrict & " ON ivd.ShipLineKey = sl.ShipLineKey JOIN tsoShipmentLog slog WITH (NOLOCK) ON sl.ShipKey = slog.ShipKey "
                sRestrict = sRestrict & " WHERE slog.TranStatus IN (3, 6))"
                sRestrict = sRestrict & " AND BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
                .Where = sRestrict
                .Init
            End With

            sSQL = "TRUNCATE TABLE #tciARInvoices"
            moClass.moAppDB.ExecuteSQL sSQL
            
            'Restrict to only show committed or posted shipments that have invoices in system batch.
            sRestrict = "ShipKey in (SELECT ShipKey FROM tsoShipLine WITH (NOLOCK) "
            sRestrict = sRestrict & "WHERE ShipLineKey IN (SELECT ShipLineKey "
            sRestrict = sRestrict & "FROM tarInvoiceDetl WITH (NOLOCK) "
            sRestrict = sRestrict & "WHERE InvcKey in (SELECT InvcKey "
            sRestrict = sRestrict & "FROM tarPendInvoice WITH (NOLOCK) "
            sRestrict = sRestrict & "WHERE  BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID)) & ")))"
            sRestrict = sRestrict & " AND TranStatus IN (3,6)"       'Restrict to Committed and Posted

            With lkuShipment
                .RestrictClause = sRestrict
                .Text = ""
            End With

            'Restrict to only show Sales Orders that have invoices in system batch.
            sRestrict = "SOKey in (SELECT SOKey FROM tsoSOLine WITH (NOLOCK) "
            sRestrict = sRestrict & "WHERE SOLineKey IN (SELECT SOLineKey "
            sRestrict = sRestrict & "FROM tarInvoiceDetl WITH (NOLOCK) "
            sRestrict = sRestrict & "WHERE InvcKey in (SELECT InvcKey "
            sRestrict = sRestrict & "FROM tarPendInvoice WITH (NOLOCK) "
            sRestrict = sRestrict & "WHERE InvcKey IN (SELECT InvcKey FROM tarInvoiceDetl ivd WITH (NOLOCK) JOIN tsoShipLine sl WITH (NOLOCK) "
            sRestrict = sRestrict & " ON ivd.ShipLineKey = sl.ShipLineKey JOIN tsoShipmentLog slog WITH (NOLOCK) ON sl.ShipKey = slog.ShipKey "
            sRestrict = sRestrict & " WHERE slog.TranStatus IN (3, 6))"
            sRestrict = sRestrict & " AND BatchKey = " & moClass.moAppDB.Lookup("SystemBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID)) & ")))"
            
            
            With lkuSalesOrder
                .RestrictClause = sRestrict
                .Text = ""
            End With
            
            PopulateSOTranID
            moDMTranList.Refresh
             
        Case kPullSOPmts
            sCaption = "Move SO Payments into Batch "
            
            pnlPushInfo.Visible = False
            pnlPullInfo.Visible = False
            
            fraTranList.Top = 495
            fraTranList.Height = 5325
            grdTranList.Top = 240
            grdTranList.Height = 4905
            
                       
            With moDMTranList
                .Where = "tarPendCustPmt.BatchKey = " & moClass.moAppDB.Lookup("SOPaymentBatchKey", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID)) & _
                    " And TranStatus = " & kTranStatusPending
                .Init
            End With
            

            sSQL = "TRUNCATE TABLE #tciARCustPmts"
            moClass.moAppDB.ExecuteSQL sSQL
            

        Case Else
            sCaption = "Get Stuff for Batch "
    End Select
           
    sCaption = sCaption & " [" & gsLocStr(kBatchID, moClass) & ": " & moClass.BatchID & "]"
    Me.Caption = sCaption

    Exit Sub

ExpectedErrorRoutine:
    giSotaMsgBox Me, moClass.moSysSession, kmsgExpError, Err.Description, "FormSetup"
    gClearSotaErr

End Sub

Private Function sRemoveAmpersand(sText As String) As String
'+++ VB/Rig Skip +++
    Dim sTemp       As String
    Dim i           As Integer

    On Error GoTo ExpectedErrorRoutine

    sTemp = sText
    Do
        i = InStr(sTemp, "&")
        If i = 0 Then Exit Do
        sTemp = Left(sTemp, i - 1) & Mid(sTemp, i + 1)
    Loop

    sRemoveAmpersand = sTemp
    Exit Function

ExpectedErrorRoutine:

End Function












#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If
    
    valSplitBatch.Bind lkuTargetBatch, lblTargetBatch
    valSplitBatch.Init

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub cmdClearAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelectAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelectAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub lkuShipment_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuShipment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipment_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipment_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuShipment, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipment_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipment_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuShipment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipment_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipment_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuShipment, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipment_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipment_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuShipment, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipment_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuShipment_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuShipment, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuShipment_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSalesOrder_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuSalesOrder, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSalesOrder_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSalesOrder_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress lkuSalesOrder, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSalesOrder_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSalesOrder_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuSalesOrder, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSalesOrder_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSalesOrder_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuSalesOrder, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSalesOrder_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSalesOrder_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuSalesOrder, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSalesOrder_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuSalesOrder_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuSalesOrder, True
    #End If
'+++ End Customizer Code Push +++

    mbLookupClick = True

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuSalesOrder_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTargetBatch_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange lkuTargetBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTargetBatch_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTargetBatch_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus lkuTargetBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTargetBatch_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTargetBatch_BeforeLookupReturn(colSQLReturnVal As Collection, bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnBeforeLookupReturn(lkuTargetBatch, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTargetBatch_BeforeLookupReturn()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTargetBatch_LookupClick(bCancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnLookupClick(lkuTargetBatch, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTargetBatch_LookupClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub lkuTargetBatch_LookupClicked()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLookupClicked lkuTargetBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "lkuTargetBatch_LookupClicked()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub chkNewBatch_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkNewBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNewBatch_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkNewBatch_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkNewBatch, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkNewBatch_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Private Sub chkNewBatch_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkNewBatch, True
    #End If
'+++ End Customizer Code Push +++
    If chkNewBatch = vbChecked Then
        lkuTargetBatch.Enabled = False
    Else
        lkuTargetBatch.Enabled = True
    End If
End Sub

Public Function PushVouchersOrInvoices() As Boolean
'+++ VB/Rig Skip +++
    Dim lDestBatchKey       As Long
    Dim lRow                As Long
    Dim sSQL                As String
    Dim iRetVal             As Integer
    Dim oRS                 As DASRecordSet
    Dim lLockID             As Long
    Dim bInTrans            As Boolean
    Dim lPostStatus        As Long

    On Error GoTo ErrHandler

    PushVouchersOrInvoices = False

    'Get batch destination info.
    sSQL = "SELECT PostStatus, BatchKey FROM tciBatchLog WHERE BatchNo = " & Val(lkuTargetBatch.Text)
    sSQL = sSQL & " AND BatchType = " & moClass.BatchType
    sSQL = sSQL & " AND SourceCompanyID = " & gsQuoted(msCompanyID)
    Set oRS = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If oRS Is Nothing Then
        ' query failed
        Exit Function
    End If

    'Verify destination batch does NOT exist if user is creating a new batch.
    If chkNewBatch.Value = vbChecked And (Not oRS.IsEmpty) And (Trim(lkuTargetBatch.Text) <> "") Then
        oRS.Close
        Set oRS = Nothing
        giSotaMsgBox Me, moClass.moSysSession, kmsgBatchNoExists, lkuTargetBatch.Text
        Exit Function
    End If

    If Not oRS.IsEmpty Then
        lDestBatchKey = oRS.Field("BatchKey")
        lPostStatus = oRS.Field("PostStatus")
    End If
    oRS.Close
    Set oRS = Nothing

    'Verify destination batch exist if user is not creating a new batch.
    If chkNewBatch.Value = vbUnchecked Then
        'PostStatus = 999 is a deleted batch
        If lDestBatchKey = 0 Or lPostStatus = 999 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidBatch, lkuTargetBatch.Text
            Exit Function
        End If
    End If
    
    lLockID = lLockBatch(kLockTypeExclusive)
    If lLockID < 0 Then Exit Function

    With moClass.moAppDB
        .BeginTrans
        bInTrans = True

        For lRow = 1 To grdTranList.MaxRows
            If gsGridReadCell(grdTranList, lRow, kColSelect) = 1 Then
                'Get value from grid.   DM GetColumnValue is slow.
                If moClass.BatchType = kBatchTypeAPVO Or moClass.BatchType = kBatchTypePOVO Then
                    sSQL = "INSERT #tciAPVouchers (VouchKey) VALUES (" & glGetValidLong(gsGridReadCell(grdTranList, lRow, kColDocKey)) & ")"
                Else 'batch type = kBatchTypeARIN
                    sSQL = "INSERT #tciARInvoices (InvcKey) VALUES (" & glGetValidLong(gsGridReadCell(grdTranList, lRow, kColDocKey)) & ")"
                End If
                Debug.Print sSQL
                .ExecuteSQL sSQL
            End If
        Next lRow

        .CommitTrans
        bInTrans = False

        .SetInParam msCompanyID
        .SetInParam moClass.BatchKey
        
        If chkNewBatch.Value = vbChecked Then
            If moClass.BatchType = kBatchTypeAPVO Or moClass.BatchType = kBatchTypePOVO Then
                .SetInParam 0
                .SetInParam Trim(lkuTargetBatch.Text)
                .SetInParam moClass.BatchType
            Else
                .SetInParam 0
                .SetInParam Trim(lkuTargetBatch.Text)
            
            End If
        Else
            If moClass.BatchType = kBatchTypeAPVO Or moClass.BatchType = kBatchTypePOVO Then
                .SetInParam lDestBatchKey
                .SetInParam ""
                .SetInParam moClass.BatchType
            Else
                .SetInParam lDestBatchKey
                .SetInParam ""
            End If
        End If
        
        .SetOutParam iRetVal

        If moClass.BatchType = kBatchTypeAPVO Or moClass.BatchType = kBatchTypePOVO Then
            .ExecuteSP "spapPushVouchers"
        Else
            .ExecuteSP "sparPushInvoices"
        End If
        
        If moClass.BatchType = kBatchTypeAPVO Or moClass.BatchType = kBatchTypePOVO Then
            iRetVal = .GetOutParam(6)
        Else
            iRetVal = .GetOutParam(5)
        End If
        
        .ReleaseParams
    End With

    DropLock lLockID

    If iRetVal = 4 Then
        PushVouchersOrInvoices = True
    Else
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, iRetVal
    End If

    Exit Function

ErrHandler:
    If bInTrans Then
        moClass.moAppDB.Rollback
    End If
    DropLock lLockID
    PushVouchersOrInvoices = False

End Function

Private Function lLockBatch(Optional lLockType As Long = kLockTypeShared) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*******************************************************************************
' Desc:  create a logical lock for the batch
'*******************************************************************************
    
    Dim lLockID     As Long
    Dim lRet        As Long

    lLockBatch = -1

    lRet = glLockLogical(moClass.moAppDB, sLockBatchEntity(), lLockType, lLockID)

    If lRet < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgCantGetBatchLock, moClass.BatchNo
        lLockBatch = lRet
    Else
        lLockBatch = lLockID
    End If

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lLockBatch", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'*****************************************************************************
' Desc:
'       create a lock entity string for current batch.  used for logical locking routines
'       to provide a unique entity id.
'*****************************************************************************
Private Function sLockBatchEntity() As String
'+++ VB/Rig Skip +++
    sLockBatchEntity = kLockEntBatch & moClass.BatchKey
End Function

Private Sub DropLock(lLockID As Long)
'+++ VB/Rig Skip +++
'*******************************************************************************
' Desc:  remove lock based on Lock ID
'*******************************************************************************

    ' don't crash app if we can't drop the lock
    On Error Resume Next

    ' check if a bad lock id was passed in
    If lLockID < 0 Then Exit Sub

    glUnlockLogical moClass.moAppDB, lLockID

    Exit Sub

End Sub

Private Sub CloseForm()
    moClass.moFramework.SavePosSelf
    Me.Hide
End Sub

Private Function PullInvoices() As Boolean
'+++ VB/Rig Skip +++
    Dim lRow                As Long
    Dim sSQL                As String
    Dim iRetVal             As Integer
    Dim lLockID             As Long
    Dim bInTrans            As Boolean
   
    On Error GoTo ErrHandler

    PullInvoices = False

    'drop the current shared lock lock
    DropLock moClass.lLockID
                
    lLockID = lLockBatch(kLockTypeExclusive)
    If lLockID < 0 Then
        'reestablish the shared lock
        lLockID = lLockBatch(kLockTypeShared)
        moClass.lLockID = lLockID
        Exit Function
    End If
    
    With moClass.moAppDB
        .BeginTrans
        bInTrans = True

        For lRow = 1 To grdTranList.MaxRows
            If gsGridReadCell(grdTranList, lRow, kColSelect) = 1 Then
                'Get value from grid.   DM GetColumnValue is slow.
                sSQL = "INSERT #tciARInvoices (InvcKey) VALUES (" & glGetValidLong(gsGridReadCell(grdTranList, lRow, kColDocKey)) & ")"
                .ExecuteSQL sSQL
            End If
        Next lRow

        .CommitTrans
        bInTrans = False

        .SetInParam msCompanyID
        .SetInParam moClass.BatchKey
        .SetOutParam iRetVal

        .ExecuteSP "sparPullInvoices"

        iRetVal = .GetOutParam(3)
        .ReleaseParams

    End With

    DropLock lLockID

    'reestablish teh shared lock
    lLockID = lLockBatch(kLockTypeShared)
    moClass.lLockID = lLockID
    
    If iRetVal = 2 Then
        PullInvoices = True
    End If
    
    Exit Function

ErrHandler:
    If bInTrans Then
        moClass.moAppDB.Rollback
    End If
    DropLock lLockID
    PullInvoices = False

End Function

Private Function PullCustPmts() As Boolean
'+++ VB/Rig Skip +++
    Dim lRow                As Long
    Dim sSQL                As String
    Dim iRetVal             As Integer
    Dim lLockID             As Long
    Dim bInTrans            As Boolean

    On Error GoTo ErrHandler

    PullCustPmts = False

    'drop the current shared lock lock
    DropLock moClass.lLockID
                
    lLockID = lLockBatch(kLockTypeExclusive)
    If lLockID < 0 Then
        'reestablish the shared lock
        lLockID = lLockBatch(kLockTypeShared)
        moClass.lLockID = lLockID
        Exit Function
    End If
    
    With moClass.moAppDB
        .BeginTrans
        bInTrans = True

        For lRow = 1 To grdTranList.MaxRows
            If gsGridReadCell(grdTranList, lRow, kColSelect) = 1 Then
                'Get value from grid.   DM GetColumnValue is slow.
                sSQL = "INSERT #tciARCustPmts (CustPmtKey) VALUES (" & glGetValidLong(gsGridReadCell(grdTranList, lRow, kColDocKey)) & ")"
                .ExecuteSQL sSQL
            End If
        Next lRow

        .CommitTrans
        bInTrans = False

        .SetInParam msCompanyID
        .SetInParam moClass.BatchKey
        .SetInParam moClass.CashTranKey
        .SetInParam IIf(moClass.AllowMCDeposit, 1, 0)
        .SetInParam moClass.CashAcctCurrID
        
        .SetOutParam iRetVal

        .ExecuteSP "sparPullCustPmts"

        iRetVal = .GetOutParam(6)
        .ReleaseParams

    End With

    DropLock lLockID

    'reestablish teh shared lock
    lLockID = lLockBatch(kLockTypeShared)
    moClass.lLockID = lLockID
    
    If iRetVal = 2 Then
        PullCustPmts = True
    End If

    Exit Function

ErrHandler:
    If bInTrans Then
        moClass.moAppDB.Rollback
    End If
    DropLock lLockID
    PullCustPmts = False

End Function

'View sales order number field on the grid by populating temp table #SOTranID
Private Sub PopulateSOTranID()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String

    'If table does not already exist, then drop
'    sSQL = "IF object_id('tempdb..#SOTranID') IS NOT NULL "
'    sSQL = sSQL & vbCrLf & "DROP TABLE #SOTranID"
'    moClass.moAppDB.ExecuteSQL sSQL

    sSQL = "TRUNCATE TABLE #SOTranID"
    moClass.moAppDB.ExecuteSQL sSQL

    'Display the sales order number if there is one order per invoice
    'and display the word 'multiple' if there are multiple orders per invoice
    sSQL = "INSERT INTO #SOTranID (TmpInvcKey, TmpTranID)"
    sSQL = sSQL & vbCrLf & " SELECT tarPendInvoice.InvcKey, SO.TranID"
    sSQL = sSQL & vbCrLf & " FROM tarPendInvoice WITH (NOLOCK)"
    sSQL = sSQL & vbCrLf & " INNER JOIN (SELECT tarInvoiceDetl.InvcKey,"
    sSQL = sSQL & vbCrLf & "            CASE WHEN COUNT(DISTINCT tsoSalesOrder.SOKey) = 1 THEN MIN(tsoSalesOrder.TranNoRelChngOrd) ELSE 'Multiple' END AS TranID"
    sSQL = sSQL & vbCrLf & "            FROM tarInvoiceDetl WITH (NOLOCK)"
    sSQL = sSQL & vbCrLf & "            INNER JOIN tsoSOLine WITH (NOLOCK)"
    sSQL = sSQL & vbCrLf & "              ON tarInvoiceDetl.SOLineKey = tsoSOLine.SOLineKey"
    sSQL = sSQL & vbCrLf & "            INNER JOIN tsoSalesOrder WITH (NOLOCK)"
    sSQL = sSQL & vbCrLf & "              ON tsoSOLine.SOKey = tsoSalesOrder.SOKey"
    sSQL = sSQL & vbCrLf & "            WHERE tsoSalesOrder.CompanyID = " & gsQuoted(msCompanyID)
    sSQL = sSQL & vbCrLf & "            GROUP BY tarInvoiceDetl.InvcKey) SO"
    sSQL = sSQL & vbCrLf & " ON tarPendInvoice.InvcKey = SO.InvcKey"
    
    moClass.moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PopulateSOTranID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

















Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property








