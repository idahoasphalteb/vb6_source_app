VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Begin VB.Form frmCommonRegister 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Register/Post"
   ClientHeight    =   7935
   ClientLeft      =   1200
   ClientTop       =   1620
   ClientWidth     =   9345
   HelpContextID   =   1012775
   Icon            =   "CommonRegister.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7935
   ScaleWidth      =   9345
   Begin VB.Frame frmRegOption 
      Caption         =   "Register &Options"
      Height          =   3015
      Left            =   4620
      TabIndex        =   11
      Top             =   960
      Width           =   4665
      Begin VB.CheckBox chk 
         Alignment       =   1  'Right Justify
         Caption         =   "Include &Invoice Comments"
         Height          =   315
         Index           =   0
         Left            =   240
         TabIndex        =   14
         Top             =   720
         WhatsThisHelpID =   43738
         Width           =   3345
      End
      Begin VB.CheckBox chk 
         Alignment       =   1  'Right Justify
         Caption         =   "Include &Line Item Comments"
         Height          =   315
         Index           =   1
         Left            =   240
         TabIndex        =   15
         Top             =   1080
         WhatsThisHelpID =   43739
         Width           =   3345
      End
      Begin VB.CheckBox chk 
         Alignment       =   1  'Right Justify
         Caption         =   "Include &Project Details"
         Height          =   315
         Index           =   2
         Left            =   240
         TabIndex        =   16
         Top             =   1440
         Visible         =   0   'False
         WhatsThisHelpID =   43745
         Width           =   3345
      End
      Begin SOTADropDownControl.SOTADropDown ddnFormat 
         Height          =   315
         Left            =   1800
         TabIndex        =   13
         Top             =   300
         WhatsThisHelpID =   1012796
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ddnFormat"
      End
      Begin VB.Frame fraPosPay 
         Caption         =   "Positive Pay File Extract"
         Height          =   705
         Left            =   120
         TabIndex        =   17
         Top             =   2040
         Visible         =   0   'False
         Width           =   4455
         Begin VB.CommandButton cmdPosPayProperties 
            Caption         =   "Set Directory ..."
            Height          =   375
            Left            =   2760
            TabIndex        =   19
            Top             =   240
            Width           =   1335
         End
         Begin VB.CheckBox chkPosPay 
            Caption         =   "Extract Check Information to File"
            Height          =   375
            Left            =   120
            TabIndex        =   18
            Top             =   240
            Width           =   2655
         End
      End
      Begin VB.Frame fraACH 
         Caption         =   "EFT File Extract"
         Height          =   705
         Left            =   120
         TabIndex        =   20
         Top             =   2040
         Visible         =   0   'False
         Width           =   4455
         Begin VB.CommandButton cmdACHProperties 
            Caption         =   "Set Directory ..."
            Height          =   300
            Left            =   1200
            TabIndex        =   21
            Top             =   200
            Width           =   1785
         End
      End
      Begin VB.Label lblFormat 
         Caption         =   "Format"
         Height          =   195
         Left            =   240
         TabIndex        =   12
         Top             =   330
         Width           =   1095
      End
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7545
      WhatsThisHelpID =   73
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin VB.PictureBox CommonDialog1 
      Height          =   480
      Left            =   15000
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   44
      Top             =   600
      Width           =   1200
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   41
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   72
      Width           =   9345
      _ExtentX        =   16484
      _ExtentY        =   741
      Style           =   5
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   33
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   35
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame fraMessage 
      Caption         =   "&Message"
      Height          =   1695
      Left            =   60
      TabIndex        =   24
      Top             =   5700
      Width           =   9195
      Begin VB.PictureBox pctMessage 
         Height          =   1320
         Left            =   135
         ScaleHeight     =   1260
         ScaleWidth      =   8775
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   270
         Width           =   8835
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   0
            Left            =   0
            MaxLength       =   50
            TabIndex        =   25
            Top             =   0
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   1
            Left            =   0
            MaxLength       =   50
            TabIndex        =   26
            Top             =   255
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   2
            Left            =   0
            MaxLength       =   50
            TabIndex        =   27
            Top             =   510
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   3
            Left            =   0
            MaxLength       =   50
            TabIndex        =   28
            Top             =   765
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.TextBox txtMessageHeader 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            Height          =   240
            Index           =   4
            Left            =   0
            MaxLength       =   50
            TabIndex        =   29
            Top             =   1020
            WhatsThisHelpID =   18
            Width           =   8770
         End
         Begin VB.Line Line1 
            Index           =   3
            X1              =   0
            X2              =   8775
            Y1              =   1010
            Y2              =   1010
         End
         Begin VB.Line Line1 
            Index           =   2
            X1              =   0
            X2              =   8775
            Y1              =   750
            Y2              =   750
         End
         Begin VB.Line Line1 
            Index           =   1
            X1              =   0
            X2              =   8775
            Y1              =   500
            Y2              =   500
         End
         Begin VB.Line Line1 
            Index           =   0
            X1              =   0
            X2              =   8775
            Y1              =   240
            Y2              =   240
         End
      End
   End
   Begin VB.Frame fraSort 
      Caption         =   "&Sort"
      Height          =   1905
      Left            =   60
      TabIndex        =   22
      Top             =   3720
      Width           =   9195
      Begin FPSpreadADO.fpSpread grdSort 
         Height          =   1545
         Left            =   90
         TabIndex        =   23
         Top             =   270
         WhatsThisHelpID =   25
         Width           =   9015
         _Version        =   524288
         _ExtentX        =   15901
         _ExtentY        =   2725
         _StockProps     =   64
         DisplayRowHeaders=   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaxCols         =   1
         MaxRows         =   1
         SpreadDesigner  =   "CommonRegister.frx":23D2
         AppearanceStyle =   0
      End
   End
   Begin VB.Frame frmPrintPost 
      Caption         =   "&Print/Post"
      Height          =   1935
      Left            =   60
      TabIndex        =   5
      Top             =   960
      Width           =   4425
      Begin VB.CheckBox chkConfirm 
         Caption         =   "Confirm Before Posting"
         Height          =   285
         Left            =   450
         TabIndex        =   10
         Top             =   1290
         Value           =   1  'Checked
         WhatsThisHelpID =   1012793
         Width           =   2785
      End
      Begin VB.CheckBox chkPost 
         Caption         =   "Post"
         Height          =   285
         Left            =   180
         TabIndex        =   9
         Top             =   930
         Value           =   1  'Checked
         WhatsThisHelpID =   1012792
         Width           =   1125
      End
      Begin VB.CheckBox chkRegister 
         Caption         =   "Register"
         Height          =   285
         Left            =   240
         TabIndex        =   6
         Top             =   270
         Value           =   1  'Checked
         WhatsThisHelpID =   1012791
         Width           =   1515
      End
      Begin SOTADropDownControl.SOTADropDown ddnOutput 
         Height          =   315
         Left            =   1260
         TabIndex        =   8
         Top             =   600
         WhatsThisHelpID =   1012790
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   556
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ddnOutput"
      End
      Begin VB.Label lblOutput 
         Caption         =   "Output"
         Height          =   195
         Left            =   450
         TabIndex        =   7
         Top             =   630
         Width           =   915
      End
   End
   Begin VB.CheckBox chkSummary 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   4620
      TabIndex        =   4
      Top             =   525
      WhatsThisHelpID =   31
      Width           =   3225
   End
   Begin VB.ComboBox cboReportSettings 
      Height          =   315
      Left            =   780
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Top             =   510
      WhatsThisHelpID =   23
      Width           =   3705
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   36
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   39
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.PictureBox dlgCreateRPTFile 
      Height          =   480
      Left            =   10200
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   45
      Top             =   1785
      Width           =   1200
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   1000
      Left            =   -30000
      TabIndex        =   42
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin MSComctlLib.ImageList imlToolbar 
      Left            =   9510
      Top             =   510
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog CommonDialog2 
      Left            =   1440
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label1 
      Caption         =   "Pending"
      Height          =   375
      Left            =   0
      TabIndex        =   46
      Top             =   0
      Width           =   975
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   40
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblStatusMsg 
      Caption         =   "Pending"
      Height          =   375
      Left            =   10860
      TabIndex        =   1
      Top             =   3180
      Width           =   975
   End
   Begin VB.Label lblSetting 
      Caption         =   "S&etting"
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   570
      Width           =   555
   End
End
Attribute VB_Name = "frmCommonRegister"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: frmCommonRegister
'     Desc: Common Register/Post Screen for the modules AP,AR,GL,MC,
'           Project Invoice and to preview/print the register
'Copyright: Copyright (c) 1995-2012 Sage Software, Inc.
'***********************************************************************
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If
           
' GL Register options
    Private Const kGLPostNone       As Integer = 1
    Private Const kGLPostSummary    As Integer = 2
    Private Const kGLPostDetail     As Integer = 3
    
' Format and Output dropdown constants
    Private Const kOutputScreen     As Integer = 2
    Private Const kOutputPrinter    As Integer = 1
    Private Const kFormatSummary    As Integer = 1
    Private Const kOutputDetail     As Integer = 2
    
    Private Const kStandard         As Integer = 1
    Private Const kTrans            As Integer = 2
    Private Const kBegBal           As Integer = 3
    
    Private Const kPostXXBatches    As Integer = 0
    Private Const kInteractiveBatch As Integer = 1
    Private Const kModuleNoAP       As Integer = 4
    Private Const kModuleNoAR       As Integer = 5
    ' Added by Gil to reorganize the sorts from one register to another
    '-- Grid columns
    Private Const kColSort = 1
    Private Const kColAscending = 2
    Private Const kColSubtotal = 3
    Private Const kColPageBreak = 4
    
    ' Added by Ashish on 11 Dec 1998 for fixing 6225
    Public mbBatchOnHold            As Boolean
    Public mbBatchDispOnly          As Boolean
    
'Intellisol Start
    Public mbCalledFromPA           As Boolean
'Intellisol End
    
    'Flag to identify extract options
    ' 0 - None
    ' 1 - ACH
    ' 2 - Positive Pay
    Public miExtractType   As Integer
    
    'Positive Pay Flag will enable only if
    '(A) Non-EFT payment method
    '(B) System OR Manual Check Batch
    Public mbDoPosPay               As Boolean
    
    Public mbInitialLoad            As Boolean
    Public mbPrenote                As Boolean
    
    Public ofrmReg                  As Object
    Public ofrmPost                 As Object
    Public moGLRegister             As Object
    Public moCommRegister           As Object
    Public moAPRegister             As Object
    Public moARRegister             As Object
    Public moCommReg                As Object
    Public moMCRegister             As Object
    Public msSortField              As Collection
    Public msSortOrder              As Collection
    Public mlSubTotal               As Collection
    Public mlPageBreak              As Collection
    Public sRealTableCollection     As Collection
    Public sWorkTableCollection     As Collection
    Public moSotaObjects            As Collection
    Public moReport                 As clsReportEngine
    Public moDDData                 As clsDDData
    Public moOptions                As clsOptions
    Public moSort                   As clsSort
    Public mbSortGrid               As Boolean
    Public mbPostOnly               As Boolean
    Public mbErrorOccured           As Boolean
    Public mbAPARSettlement         As Boolean
    ' For Register and Post flags
    Public mbRegisterFlag           As Boolean          ' Holds a flag to indicate whether register is needed or not
    Public mbPostFlag               As Boolean          ' Holds a flag to indicate whether posting is to be done or not.
    Public mbConfirmFlag            As Boolean          ' Holds a flag for confirmation required.
    Public mbDetailFlag             As Boolean          ' Holds a flag If true then Detail,  else summary
    Public mlBatchKey               As Long
    Public lBatchKey                As Long
    Public mdPostDate               As Date

    Private moContextMenu           As clsContextMenu
    Private moFrmSysSession         As Object
    Private moAppDAS                As Object
    Private moSysDAS                As Object
           
'Controls
    Private mbFormat                As Boolean
    Private mbInvoice               As Boolean
    Private mbLine                  As Boolean
    Private mbProject               As Boolean
    Private mbPositive              As Boolean
    Private mbIsChk0CtrlVisible     As Boolean
    Private mbIsChk1CtrlVisible     As Boolean
    Private mbIsChk2CtrlVisible     As Boolean
    Private mbACH                   As Boolean
    Private mbUserIsOwner           As Boolean
    Private mbCancelShutDown        As Boolean
    Private mbLoading               As Boolean
    Private mbPeriodEnd             As Boolean
    Private mbLoadSuccess           As Boolean
    Private mbLoadingSettings       As Boolean
    Private mbEnterAsTab            As Boolean
    Private bCheckStatus            As Boolean
    Private mbPAActivated           As Boolean
    Private mdBusinessDate          As Date
    Private miSecurityLevel         As Integer
    Private mlRunMode               As Long
    Private moClass                 As Object
    
' Added by Gil to fix bug 5005 (in case of validation error,
' both IM and Error registers should be displayed
    Private mbPreprocessingError    As Boolean
    
    Dim sRptProgramName As String   'Stores base filename for identification purposes.
    Dim sModule As String           'The report's module: AR, AP, SM, etc.
    Dim sDefaultReport As String    'The default .RPT file name

    Dim mlLockID                    As Long
    Dim miLockType                  As Integer      ' 1 Shared, 2 Exclusive, 0 Nolock
    Dim lRetVal                     As Integer

    Private cypHelper       As Object 'SGS DEJ 1/12/12 - Orig Customized in 7-3

    Const VBRIG_MODULE_ID_STRING = "CommonRegsiter.frm"

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    WhatHelpPrefix = "CIZ"
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    FormHelpPrefix = "CIZ"
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
    Set oClass = moClass
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property


Public Property Get oreport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oreport = moReport
End Property

Public Property Set oreport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property


Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Public Property Get bCancelShutDown() As Boolean
'+++ VB/Rig Skip +++
  bCancelShutDown = mbCancelShutDown
End Property

Public Property Let bCancelShutDown(bCancel As Boolean)
'+++ VB/Rig Skip +++
    mbCancelShutDown = bCancel
End Property

Public Property Let BatchKey(lBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlSourceCompBatchKey = lBatchKey
    mlBatchKey = lBatchKey
    
    vBatchID = moAppDAS.Lookup("BatchID", "tciBatchLog WITH (NOLOCK)", "SourceCompanyID = " & _
                gsQuoted(msCompanyID) & " AND BatchKey = " & CStr(mlBatchKey))
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BatchKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
                
Public Property Let lBatchStatus(lBatchStatus As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    iBatchStatus = lBatchStatus
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lBatchStatus_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let TaskNumber(lTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlTaskNumber = lTaskNumber
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "TaskNumber", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Property Get lVOBatchKey() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lVOBatchKey = mlVOBatchKey
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lVOBatchKey_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lVOBatchKey(lNewVOBatchKey As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlVOBatchKey = lNewVOBatchKey

  
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lVOBatchKey_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let PostDate(dPostDate As Date)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mdPostDate = dPostDate
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PostDate_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub SetPosPayState()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If chkPosPay.Value = vbChecked Then
        'SGS-JMM 4/26/2011
'        mbDoPosPay = True
'        gEnableControls cmdPosPayProperties
'        IsFileDirSet
    
        If PosPayAllowed Then
            mbDoPosPay = True
            gEnableControls cmdPosPayProperties
            IsFileDirSet
        Else
            chkPosPay.Value = vbUnchecked
            MsgBox "You are not authorized to export positive pay files.", vbOKOnly + vbInformation, Me.Caption
        End If  'End SGS-JMM
    Else
        mbDoPosPay = False
        gDisableControls cmdPosPayProperties
    End If
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetPosPayState", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

'Look to see if the property is set, if not read it from the registry.
'If the registry turns up blank the user will be warned down the road
'before posting occurs in the bDoPost routine.
Private Sub IsFileDirSet()
    Dim sKeyString As String
    Dim fso As Object   'SGS-JMM 4/25/2011
    
    If miExtractType = kExtractACH Then
        sKeyString = "ACH"
    ElseIf miExtractType = kExtractPosPay Then
        sKeyString = "PositivePay"
    End If

    On Error GoTo IsFileDirSet_ErrHandler
    
    If IsNull(frmProperties.sFileExtractDir) Or Len(Trim(frmProperties.sFileExtractDir)) = 0 Then
        frmProperties.sFileExtractDir = GetSetting(sKeyString, "DefaultSettings", "Dir", "")
        'SGS-JMM 4/25/2011
        frmProperties.sFileExtractDir = GetDefaultPosPayExportDir
        If Len(Trim(frmProperties.sFileExtractDir)) <= 0 Then
            MsgBox "Warning: the positive pay export directory could not be retrieved from the database.", vbExclamation, Me.Caption
            frmProperties.sFileExtractDir = GetSetting(sKeyString, "DefaultSettings", "Dir", "")
        Else
            Set fso = CreateObject("Scripting.FileSystemObject")
            If Not fso.FolderExists(frmProperties.sFileExtractDir) Then
                MsgBox "Warning: the positive pay export directory " & vbCrLf & vbCrLf & frmProperties.sFileExtractDir & vbCrLf & vbCrLf & " is not accessible.", vbExclamation, Me.Caption
            End If
            Set fso = Nothing
        End If
        'End SGS-JMM
    End If
    If Len(Trim(frmProperties.sFileExtractDir)) > 1 Then
        frmProperties.bProcessExtract = True
    Else
        frmProperties.bProcessExtract = False
    End If
    
    Exit Sub
  
IsFileDirSet_ErrHandler:
    MsgBox "The following error occurred in IsFileDirSet: " & _
    vbCrLf & "Error Number: " & Err.Number & _
    vbCrLf & "Error Description: " & Err.Description, _
    vbExclamation + vbOKOnly, Me.Caption
End Sub

'Validate EFT Payment Method is turned on
Private Function bValidateEFT() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lBatchVendPmtMethKey As Long
    Dim lEFTVendPmtMethKey As Long
    
    bValidateEFT = False
       
    lEFTVendPmtMethKey = glGetValidLong(moClass.moAppDB.Lookup("VendPmtMethKey", "tapVendPmtMethod WITH (NOLOCK)", "TranType = 416 AND CompanyID = " & gsQuoted(msCompanyID)))
    
    If lEFTVendPmtMethKey <> 0 Then
      lBatchVendPmtMethKey = glGetValidLong(moClass.moAppDB.Lookup("VendPmtMethKey", "tapBatch WITH (NOLOCK)", "BatchKey =" & mlBatchKey))
      If lBatchVendPmtMethKey <> 0 Then
        If lEFTVendPmtMethKey = lBatchVendPmtMethKey Then
          bValidateEFT = True
        End If
      End If
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidateEFT", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub chkPosPay_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPosPay, True
    #End If
'+++ End Customizer Code Push +++
    SetPosPayState
End Sub

Private Sub chkPost_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkPost, True
    #End If
'+++ End Customizer Code Push +++
    ' If no posting required then dont ask user whether he wants confirmation or not
    If chkPost.Value = vbChecked Then
        chkConfirm.Enabled = True
        chkConfirm.Value = vbChecked
    Else
        chkConfirm.Enabled = False
        chkConfirm.Value = vbUnchecked
    End If
End Sub

Private Sub chkRegister_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkRegister, True
    #End If
'+++ End Customizer Code Push +++
    ' If no register is to be printed, then don't allow selection of register destination
    ' And no need of style (Summary or Detail)
    If Not chkRegister.Value = vbChecked Then
        ddnFormat.Enabled = False
        ddnOutput.Enabled = False
    Else
        ddnFormat.Enabled = True
        ddnOutput.Enabled = True
    End If

    SetPrintPostCtrls
    
    mbLoadingSettings = False
End Sub

Public Sub SetPrintPostCtrls()

    Dim iRegisterPrinted As Integer
    Dim iPostStatus As Integer
        
    If glGetValidLong(mlBatchKey) <> 0 Then
        
        iRegisterPrinted = moCommRegister.iCheckRegisterPrinted(mlBatchKey, moAppDAS)
        iPostStatus = moCommRegister.iCheckStatus(mlBatchKey, moAppDAS)
                
        If tbrMain.SecurityLevel = sotaTB_DISPLAYONLY Then
            mbBatchDispOnly = True
        Else
            mbBatchDispOnly = False
        End If
        
        If iPostStatus = 2 Then
            mbBatchOnHold = True
        Else
            mbBatchOnHold = False
        End If

        If chkRegister.Value = vbUnchecked Then
            If iRegisterPrinted = 0 Then
                'Don't allow post if register is not printed.
                SetPrintPostCtrlsAttri False, iRegisterPrinted
            Else
                SetPrintPostCtrlsAttri True, iRegisterPrinted
            End If
        Else
            SetPrintPostCtrlsAttri True, iRegisterPrinted
        End If
    End If

End Sub


Private Sub SetPrintPostCtrlsAttri(bState As Boolean, iRegPrinted As Integer)
    '-- Enable post controls if not already enabled
    '-- and passed state is true
    '-- If passed state is false, disable controls
    If bState = True And chkPost.Enabled = False Then
        If Not mbBatchOnHold Then
                chkPost.Enabled = True
                If Not mbLoadingSettings Then
                    chkPost.Value = vbChecked
                End If
        End If
    ElseIf bState = False And iRegPrinted = 0 Then
        If Not mbBatchOnHold Then
            chkPost.Value = vbUnchecked
            chkPost.Enabled = False
            chkConfirm.Enabled = False
        End If
    End If
    
    'If the Post checkbox is checked, force user to also print register if it hasn't been printed.
    If iRegPrinted = 0 And chkPost.Value = vbChecked Then
        chkRegister.Enabled = True
        chkRegister.Value = vbChecked
    End If
    
    'If Batch is on hold. Only allow register to be printed.
    If mbBatchOnHold Or mbBatchDispOnly Then
        chkPost.Value = vbUnchecked
        chkPost.Enabled = False
        chkConfirm.Value = vbUnchecked
        chkConfirm.Enabled = False
        tbrMain.ButtonEnabled(kTbProceed) = True
    End If
    
End Sub
Private Sub DisableControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    
    chk(0).Visible = False
    chk(1).Visible = False
    chk(2).Visible = False
    mbIsChk0CtrlVisible = False
    mbIsChk1CtrlVisible = False
    mbIsChk2CtrlVisible = False
    lblFormat.Visible = False
    ddnFormat.Visible = False
    fraACH.Visible = False
    fraPosPay.Visible = False
    fraSort.Visible = False
    fraMessage.Visible = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "DisableControls", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Sub cmdACHProperties_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdACHProperties, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    frmProperties.mbProcessPosPay = False
    frmProperties.Caption = "ACH Properties"
    frmProperties.Show vbModal
End Sub

Private Sub cmdPosPayProperties_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdPosPayProperties, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
'SGS DEJ 1/12/12 Added Custom Comment for upgrade from 7-3 to 7-4 (START)
'    frmProperties.mbProcessPosPay = True
'    frmProperties.Caption = "Positive Pay Properties"
'    frmProperties.Show vbModal
    
    Dim shell As Object
    Dim fldr As Object
    
    Set shell = CreateObject("Shell.Application")
    Set fldr = shell.BrowseForFolder(Me.hwnd, "Select the Export Directory", 1)
    
    If Not (fldr Is Nothing) Then
        frmProperties.sFileExtractDir = fldr.Self.Path
    End If
    
    Set fldr = Nothing
    Set shell = Nothing
'SGS DEJ 1/12/12 Added Custom Comment for upgrade from 7-3 to 7-4 (STOP)

End Sub

Private Sub ddnFormat_Click(Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(ddnFormat, PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRetVal As Long
    
    Select Case mlTaskNumber
        Case ktskCashRcptPrintPost, ktskAPVoucherRegisterREG, ktskAPCheckRegisterREG, ktskAPManualCheckRegisterREG, ktskAPPaymentAppsRegisterREG
            Select Case ddnFormat.ListIndex
                Case 1
                    chk(2).Enabled = False
                Case 0
                    chk(2).Enabled = True
           End Select
           
        Case ktskInvoicePrintPost, ktskFinChgReg
           Select Case ddnFormat.ListIndex
                Case 1
                    chk(1).Enabled = False
                Case 0
                    chk(1).Enabled = True
           End Select
            
    End Select
    
    If PrevIndex <> NewIndex And PrevIndex <> -1 Then
        Me.MousePointer = vbHourglass
'Intellisol Start
        If mbCalledFromPA Then
            On Error Resume Next
            If Not moReport.bShutdownEngine Then
            End If
        End If
'Intellisol End
        lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
        Me.MousePointer = vbNormal
        If lRetVal <> 0 Then
            moReport.ReportError lRetVal
        End If
    End If
End Sub

Public Sub HandleToolbarClick(sKey As String, Optional iFileType As Variant, Optional sFileName As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If
  
    Select Case sKey
        Case kTbSave
             moSettings.ReportSettingsSaveAs
            lUpdateTaskSettings

        Case kTbProceed, kTbFinish
            'RKL DEJ 2/19/15 (START)
            'Changed for upgrade of customizations from 7-4 ot 2014
'             ProceedRegister
             ProceedRegister sKey
            'RKL DEJ 2/19/15 (STOP)

        Case kTbCancel, kTbDelete
            moSettings.ReportSettingsDelete

        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        'RKL DEJ 2/19/15 (START)
        'SGS added for Cypress Helper
        Case Else
            'SGS-JMM 1/31/2011
            'RKL DEJ 2/19/15 - moved code from Apzjxdl1.clsProcessRegisterREG
            On Error Resume Next
            If Not (cypHelper Is Nothing) Then cypHelper.HandleToolbarClick sKey, 1
            Err.Clear
            On Error GoTo VBRigErrorRoutine
            'End SGS-JMM 1/31/2011
        'SGS added for Cypress Helper
        'RKL DEJ 2/19/15 (STOP)
            
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number

        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub GetPrintPostFlags()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If chkRegister.Value = 1 Then
        mbRegisterFlag = True
    Else
        mbRegisterFlag = False
    End If
    
    If chkPost.Value = 1 Then
        mbPostFlag = True
    Else
        mbPostFlag = False
    End If
    
    If chkConfirm.Value = 1 Then
        mbConfirmFlag = True
    Else
        mbConfirmFlag = False
    End If
    
    ' For detail and ummary
    If ddnOutput.ListIndex = 0 Then
        msPrintButton = kTbPrint
    Else
        msPrintButton = kTbPreview
    End If
    
    ' For Format
    If ddnFormat.ListIndex = 0 Then
        mbDetailFlag = True
    Else
        mbDetailFlag = False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetPrintPostFlags", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub Init()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
' Desc:     load Objects/variables associated with this class
'***********************************************************************
    Set moGLRegister = CreateObject("CommonPostBL.clsGLRegister")
    Set moCommRegister = CreateObject("CommonPostBL.clsCommRegister")
    Set moAPRegister = CreateObject("CommonPostBL.clsAPRegister")
    Set moARRegister = CreateObject("CommonPostBL.clsARRegister")
    Set moMCRegister = CreateObject("CommonPostBL.clsMCRegister")

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Init", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Sub ProceedRegister(Optional sKey As String = Empty)   'RKL DEJ 2/19/15 added optional param sKey for upgrade from 7-4 to 2015
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

On Error GoTo ExpectedErrorRoutine
    
    If miMode = kSingleBatch And mlTaskNumber <> ktskAPARSettlementRegisterREG Then
        GetPrintPostFlags
    Else
        msPrintButton = kTbPrint
    End If
    
    Select Case mlModuleNumber
    
        Case kModuleGL
             With moGLRegister
                .sSourceCompID = msSourceCompanyID
                .iSourceCompUseMultCurr = miSourceCompUseMultCurr
                .iSourceCompAcctRefUsage = miSourceCompAcctRefUsage
                .iJrnlType = miJrnlType
                .PostDate = mdPostDate
                .BatchKey = mlSourceCompBatchKey
                .lTaskNumber = mlTaskNumber
                .lSourceCompBatchKey = mlSourceCompBatchKey
                .sCurrencyID = msCurrencyId
                .iPostMode = miMode
                .ReportFileName = msReportFileName
                
                Set .cypHelper = cypHelper  'SGS-JMM 1/31/2011  ->RKL DEJ 2/19/15 - moved code from Apzjxdl1.clsProcessRegisterREG
                
                Set .ofrmReg = Me
                Set .ofrmPost = frmPrintPostStat
                .InitGLRegister msPrintButton, mbRegisterFlag, mbPostFlag, mbConfirmFlag, _
                             mbDetailFlag, moClass, moReport
             End With
             
        Case kModuleAP
            With moAPRegister
                .sCompanyID = msCompanyID
                .PostDate = mdPostDate
                .BatchKey = mlBatchKey
                .lBatchKey = mlVOBatchKey
                .lTaskNumber = mlTaskNumber
                .sCurrencyID = msCurrencyId
                .iPostMode = miMode
                .ReportFileName = msReportFileName
                .ReportName = msReportName
                
                .PosPayAllowed = PosPayAllowed
                Set .cypHelper = cypHelper  'SGS-JMM 1/31/2011  ->RKL DEJ 2/19/15 - moved code from Apzjxdl1.clsProcessRegisterREG
                
                Set .ofrmReg = Me
                Set .ofrmPost = frmPrintPostStat
                Set .ofrmProp = frmProperties
                .InitAPRegister msPrintButton, mbRegisterFlag, mbPostFlag, mbConfirmFlag, _
                             mbDetailFlag, moClass, moReport
             End With
            
        Case kModuleAR, kModulePA
             With moARRegister
                .sCompanyID = msCompanyID
                .PostDate = mdPostDate
                .BatchKey = mlBatchKey
                .lTaskNumber = mlTaskNumber
                .sCurrencyID = msCurrencyId
                .iPostMode = miMode
                .ReportFileName = msReportFileName
                
                Set .cypHelper = cypHelper  'SGS-JMM 1/31/2011  ->RKL DEJ 2/19/15 - moved code from Apzjxdl1.clsProcessRegisterREG
                
                Set .ofrmReg = Me
                Set .ofrmPost = frmPrintPostStat
                Set .ofrmProp = frmProperties
                .InitARRegister msPrintButton, mbRegisterFlag, mbPostFlag, mbConfirmFlag, _
                             mbDetailFlag, moClass, moReport, moSort
             End With
            
        Case kModuleMC
            With moMCRegister
                .sCompanyID = msCompanyID
                .sRevalDate = msRevalDate
                .sReversalDate = msReversalDate
                .RevaluationBatchKey = mlBatchKey
                .lTaskNumber = mlTaskNumber
                .sCurrencyID = msCurrencyId
                .iPostMode = miMode
                .iHold = iMCHold
                .iModuleNo = iMCModuleNo
                
                Set .cypHelper = cypHelper  'SGS-JMM 1/31/2011  ->RKL DEJ 2/19/15 - moved code from Apzjxdl1.clsProcessRegisterREG
                
                Set .ofrmReg = Me
                Set .ofrmPost = frmPrintPostStat
                .InitMCRegister msPrintButton, mbRegisterFlag, mbPostFlag, mbConfirmFlag, _
                             mbDetailFlag, moClass, moReport
            End With
            
    End Select
    
    'SGS-JMM 1/31/2011
    'RKL DEJ 2/19/15 - moved code from Apzjxdl1.clsProcessRegisterREG
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.HandleToolbarClick sKey, 1
    Err.Clear
    On Error GoTo ExpectedErrorRoutine
    'End SGS-JMM 1/31/2011
    
    
'+++ VB/Rig Begin Pop +++
        Exit Sub
        
ExpectedErrorRoutine:

    Select Case mlModuleNumber
        Case kModuleGL
            Set moCommReg = moGLRegister
            
        Case kModuleAP
            Set moCommReg = moAPRegister
            
        Case kModuleAR
            Set moCommReg = moARRegister
            
        Case kModuleMC
            Set moCommReg = moMCRegister
    End Select
    
    moCommReg.TerminateObject 1
    Set moCommRegister = Nothing
    Set moAPRegister = Nothing
    Set moARRegister = Nothing
    Set moGLRegister = Nothing
    Set moCommReg = Nothing
    Set moMCRegister = Nothing
     
    Exit Sub
    
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProceedRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lInitializeReport = -1
    Dim lRetVal As Long

    If mbSortGrid = True Then
        If Not moSort Is Nothing Then
            moSort.CleanSortGrid
            Set moSort = Nothing
        End If
        Set moSort = New clsSort
    End If
    
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If

    Set moDDData = New clsDDData

    If moReport Is Nothing Or mbLoadSuccess = False Then
        Set moReport = New clsReportEngine
    End If

    If moSettings Is Nothing Then
        Set moSettings = New clsSettings
    End If

    If moOptions Is Nothing Then
        Set moOptions = New clsOptions
    End If

    If moPrinter Is Nothing Then
        Set moPrinter = Printer
    End If

    If moSotaObjects Is Nothing Then
        Set moSotaObjects = New Collection
    End If

    Dim i As Integer

    If mbPeriodEnd Then
        moReport.UI = False
    End If
    
    lReorganizeWorkTables mlTaskNumber
    
    sRptPrgname = mlTaskNumber

    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If Not moDDData.lDDBuildData(sRptPrgname) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    moReport.AppOrSysDB = kAppDB
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData, False) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    If mbSortGrid = True Then
        'CUSTOMIZE: set Groups and Sorts as required
        'note there are two optional parameters: iNumGroups,iNumSorts for lInitSort
        If (moSort.lInitSort(Me, moDDData) = kFailure) Then
            lInitializeReport = kmsgFataSortInit
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    If mbSortGrid = True Then
        lReinitializeSorts mlTaskNumber
    End If
    
    lInitializeReport = 0

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmCommonRegister"
End Function

Private Sub cboReportSettings_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++
    mbLoadingSettings = True
    moSettings.bLoadReportSettings
    SetPrintPostCtrls
    mbLoadingSettings = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReportSettings_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboReportSettings, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++
    If Len(cboReportSettings.Text) > 40 Then
        Beep
        KeyAscii = 0
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReportSettings_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Initialize()
'+++ VB/Rig Skip +++
    mbCancelShutDown = False
    mbLoadSuccess = False
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iNavControl As Integer

#If CUSTOMIZER Then
    If (Shift = 4) Then
        gProcessFKeys Me, keycode, Shift
    End If
#End If

    If Shift = 0 Then
        Select Case keycode
            Case vbKeyF1
                If Me.ActiveControl.Name = "tbrMain" Then
                    ' gDisplayWhatsThisHelp uses the control's hwnd property
                    ' but tbrSOTAMain does not have such property
                    gDisplayFormLevelHelp Me
                Else
                    'gDisplayWhatsThisHelp Me, Me.ActiveControl
                    gProcessFKeys Me, keycode, Shift
                End If
            Case vbKeyF5
                'iNavControl = moSelect.SelectionGridF5KeyDown(Me.ActiveControl)
                If iNavControl > 0 Then
                   ' navControl_GotFocus iNavControl
                End If
        End Select
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Skip +++
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
               gProcessSendKeys "{TAB}"
               KeyAscii = 0
            End If

        Case Else
            'Other KeyAscii routines.
    End Select
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRetVal As Long             'Captures Long function return value(s).
    Dim iRetVal As Integer          'Captures Integer function return value(s).
    mbLoading = True
    mbLoadSuccess = False

    'Assign system object properties to module-level variables for easier access
    ModuleVariables
   
    miMode = kSingleBatch   'Single Batch as default,
                'multiple batch mode is set later if applicable

    SetVariables mlTaskNumber
    Init
    
    gbPostBatchesFlag = False
    mbInitialLoad = True
    If Not gbPostBatchesFlag Then
        MakeUIChanges
    End If
    
    miSessionId = GetUniqueTableKey(moAppDAS, msWorkTable)

    lRetVal = lInitializeReport(sModule, sDefaultReport, sRptProgramName)
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
        GoTo badexit
    End If
    
    'Perform initialization of form and control properties only if you need to display UI.
    If Not mbPeriodEnd Then
        'Initialize toolbar.
        tbrMain.Init sotaTB_PROCESS, mlLanguage
        tbrMain.RemoveButton kTbClose
        tbrMain.AddButton kTbSave, 3
        tbrMain.AddButton kTbDelete, 4
        tbrMain.RemoveButton kTbPrintSetup
        
        With moClass.moFramework
            tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
        End With

        '   Initialize status bar.
        Set sbrMain.Framework = moClass.moFramework
        If tbrMain.SecurityLevel > sotaTB_DISPLAYONLY Then
            sbrMain.Status = SOTA_SB_START
            sbrMain.Message = ""
        Else
            sbrMain.Status = SOTA_SB_LOCKED
            tbrMain.ButtonEnabled(kTbProceed) = False
        End If

        'Initialize context menu.  WinHook2 will capture right mouse clicks to activate
        'context-sensitive help.
        Set moContextMenu = New clsContextMenu
        With moContextMenu
            Set .Form = Me
            .Init
        End With
    End If

    'CUSTOMIZE:  Add all controls on Options page for which you will want to save settings
    'to the moOptions collection, using its Add method.
    With moOptions
         .Add chkConfirm
         .Add chkPost
         .Add chkRegister
         .Add ddnOutput, lblOutput
         If mbFormat = True Then .Add ddnFormat, lblFormat
         If mbIsChk0CtrlVisible Then .Add chk(0)
         If mbIsChk1CtrlVisible Then .Add chk(1)
         If mbIsChk2CtrlVisible Then .Add chk(2)
         If mbPositive Then .Add chkPosPay
    End With
    
    'Initialize the report settings combo box.
    If mbSortGrid = True Then
        moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, , tbrMain, sbrMain, mlTaskNumber
    Else
        moSettings.Initialize Me, cboReportSettings, , , moOptions, , tbrMain, sbrMain, mlTaskNumber
    End If

    'If the report is being invoked from the Period End Processing task, attempt to load the
    'setting named "Period End".  If such setting does not exist, or if not being invoked from
    'the Period End Processing task, load last used setting.
    If mbPeriodEnd Then
        moSettings.lLoadPeriodEndSettings
    End If

    '--Setup Sage MAS 500 Status bar control
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.BrowseVisible = False
    sbrMain.Status = SOTA_SB_START

    'Update status flags to indicate successful load has completed.
    mbLoadSuccess = True
    mbLoading = False

    'SGS-JMM 1/31/2011
    On Error Resume Next
    Set cypHelper = CreateObject("CypressIntegrationHelper.MAS500Form")
    If Err.Number = 0 Then
        With cypHelper
            .Init Me, moClass, msCompanyID
            .AddToolbarButton 0
        End With
    Else
        Set cypHelper = Nothing
    End If
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

badexit:

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
   
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub lReorganizeWorkTables(mlTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If gbPostBatchesFlag Then Exit Sub
    Dim sSQL As String
    Dim rs   As DASRecordSet
    Dim rsResult As Object
    Dim FASCompany As String
    Dim FASDatabase As String

    Set sRealTableCollection = New Collection
    Set sWorkTableCollection = New Collection
    
    sRptProgramName = msReportName
    sDefaultReport = msReportName
    
    Select Case mlTaskNumber
 
        Case ktskGLJrnlTransReg, ktskGLAllocationReg, ktskGLBudRevReg
            
            sModule = "GL"
            With sWorkTableCollection
                .Add msWorkTable
            End With
            
        Case ktskAPVoucherRegisterREG, ktskAPCheckRegisterREG, ktskAPManualCheckRegisterREG, ktskAPPaymentAppsRegisterREG, ktskAPARSettlementRegisterREG
        
            sModule = "AP"
            
            sSQL = "IF OBJECT_ID('tempdb..#tapAddFASAssetWrk') IS NOT NULL DROP TABLE #tapAddFASAssetWrk "
            On Error Resume Next
            moClass.moAppDB.ExecuteSQL sSQL
        
            sSQL = " CREATE TABLE #tapAddFASAssetWrk ( "
            sSQL = sSQL + " SessionID   int NULL"
            sSQL = sSQL + " ,VouchNo        varchar(10) NULL"
            sSQL = sSQL + " ,VendID     varchar(12) NULL"
            sSQL = sSQL + " ,VendName   varchar(40) NULL"
            sSQL = sSQL + " ,AddedToFAS varchar(3) NULL"
            sSQL = sSQL + " ,FASSystemNo    varchar(999) NULL"
            sSQL = sSQL + " ,Template   varchar(999) NULL"
            sSQL = sSQL + " ,AcquisitionValue   decimal(15,3) NOT NULL DEFAULT 0"
            sSQL = sSQL + " ,ServiceDate    datetime NULL"
            sSQL = sSQL + " ,AcquisitionDate    datetime NULL"
            sSQL = sSQL + " ,Descript   varchar(40) NULL"
            sSQL = sSQL + " ,TranNoChngOrd  varchar(14) NULL"
            sSQL = sSQL + " ,FASCompany     varchar(32) NULL)"
        
            On Error Resume Next
            moClass.moAppDB.ExecuteSQL sSQL
            
            With sRealTableCollection
                .Add "tapAddFASAssetWrk" 'the "Driving Table" name, such as "tarCustomer"
            End With
            
            With sWorkTableCollection
                .Add "tapAddFASAssetWrk"
            End With
            
        Case ktskInvoicePrintPost, ktskCashRcptPrintPost, ktskPmtAppPrintPost, ktskSalesCommPrintPost, ktskCommissions, ktskFinChgReg, ktskReverseApplPrintPost
        
            sModule = "AR"
            With sWorkTableCollection
                .Add msWorkTable
            End With
        
            With sRealTableCollection
               ' .Add "tarInvoice"
            End With
            
        Case ktskGLRevalPosting, ktskAPRevalPosting, ktskARRevalPosting
            sModule = "MC"
            With sWorkTableCollection
                .Add msWorkTable
            End With
        
    End Select

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lReorganizeWorkTables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub ResizeRegisterOptionsFrame(ByVal bFormat As Boolean, _
    ByVal bInvoice As Boolean, ByVal bLine As Boolean, ByVal bProject As Boolean, _
    ByVal bPositive As Boolean, ByVal bACH As Boolean, ByVal bSortGrid As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim kTopPos As Long
    Dim kLeftPos As Long
    
    Dim ltopPos As Long
    Dim lleftPos As Long
    
    Dim kHSpacer As Integer
    Dim kVSpacer As Integer
    
    kHSpacer = 65
    kVSpacer = 120
           
    kTopPos = 120 '960
    kLeftPos = kHSpacer ' 4620
        
    ltopPos = kTopPos
    lleftPos = kLeftPos
    
    ltopPos = ltopPos + kVSpacer
    lleftPos = lleftPos + kHSpacer
    
    If bFormat = True Then
       lblFormat.Top = ltopPos
       lblFormat.Left = lleftPos
       
       ddnFormat.Top = ltopPos
              
       lblFormat.Visible = True
       ddnFormat.Visible = True
       
       ltopPos = ltopPos + ddnFormat.Height + kVSpacer
    End If
    
    If bInvoice = True Then
       chk(0).Top = ltopPos
       chk(0).Left = lleftPos
       chk(0).Visible = True
       mbIsChk0CtrlVisible = True
       ltopPos = ltopPos + chk(0).Height + kVSpacer
    End If
        
    If bLine = True Then
       chk(1).Top = ltopPos
       chk(1).Left = lleftPos
       chk(1).Visible = True
       mbIsChk1CtrlVisible = True
       ltopPos = ltopPos + chk(1).Height + kVSpacer
    End If
    
    If bProject = True Then
       chk(2).Top = ltopPos
       chk(2).Left = lleftPos
       chk(2).Visible = True
       mbIsChk2CtrlVisible = True
       ltopPos = ltopPos + chk(2).Height + kVSpacer
    End If

    If bPositive = True Then
       fraPosPay.Top = ltopPos
       fraPosPay.Left = lleftPos
       fraPosPay.Visible = True
       ltopPos = ltopPos + fraPosPay.Height + kVSpacer
    End If

    If bACH = True Then
       fraACH.Top = ltopPos
       fraACH.Left = lleftPos
       fraACH.Visible = True
       ltopPos = ltopPos + fraACH.Height + kVSpacer
    End If

    frmRegOption.Height = ltopPos + kVSpacer
    
    If mlTaskNumber = ktskAPARSettlementRegisterREG Then
        frmRegOption.Left = kLeftPos
        frmRegOption.Height = frmRegOption.Height
        fraSort.Visible = True
        fraSort.Top = frmRegOption.Height + frmRegOption.Top + kVSpacer
        fraMessage.Visible = True
        fraMessage.Top = fraSort.Top + fraSort.Height + kVSpacer
        Me.Height = fraMessage.Top + fraMessage.Height + sbrMain.Height + tbrMain.Height + kVSpacer
    Else
        frmPrintPost.Height = IIf(frmPrintPost.Height > frmRegOption.Height, frmPrintPost.Height, frmRegOption.Height)
        frmRegOption.Height = IIf(frmPrintPost.Height > frmRegOption.Height, frmPrintPost.Height, frmRegOption.Height)
        
        If mbSortGrid = True Then
            fraSort.Top = frmPrintPost.Height + frmPrintPost.Top + kVSpacer
            fraSort.Visible = True
            fraMessage.Top = fraSort.Top + fraSort.Height + kVSpacer
            fraMessage.Visible = True
            Me.Height = fraMessage.Top + fraMessage.Height + sbrMain.Height + tbrMain.Height + kVSpacer
        Else
            fraMessage.Top = frmPrintPost.Height + frmPrintPost.Top + kVSpacer
            fraMessage.Visible = True
            Me.Height = fraMessage.Top + fraMessage.Height + sbrMain.Height + tbrMain.Height + kVSpacer
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ResizeRegisterOptionsFrame", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub
Public Function SetVariables(mlTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case mlTaskNumber
    
        Case ktskGLJrnlTransReg
            SetVarsForGLTrans  'GL Transaction Register
                            
        Case ktskGLAllocationReg
            SetVarsForGLAlloc 'GL Allocation Register
        
        Case ktskGLBudRevReg
            SetVarsForGLBudget 'GL Revise Budget Register
    
        Case ktskSalesCommPrintPost
            SetVarsForSalesComm 'Sales Commission Register
        
        Case ktskCashRcptPrintPost
            SetVarsForCashRcpts 'Cash Receipt Register
            
        Case ktskValidateCashReceipts
            SetVarsForValCashRcpt 'Validate Cash Receipts
            
        Case ktskValidateARPmtAppl
            SetVarsForValARPmtAppl 'Validate AR Payment Application

        Case ktskValidateInvoice
            SetVarsForValInvoice 'Validate Invoice

        Case ktskValidateComm
            SetVarsForValComm 'Validate Commission

        Case ktskInvoicePrintPost
            SetVarsForInvoiceReg 'Invoice Register
            
        Case ktskFinChgReg
            SetVarsForFinChgReg 'Finance Register
            
        Case ktskPmtAppPrintPost
            SetVarsForARPmtAppReg 'AR Payment Application
            
        Case ktskReverseApplPrintPost
            SetVarsForARReverseAppReg 'AR Reverse Application
            
        Case ktskAPVoucherRegisterREG
            SetVarsForVoucherReg 'Voucher Register
            
        Case ktskAPCheckRegisterREG, ktskAPManualCheckRegisterREG
            SetVarsForCheckReg 'Check Register
            
        Case ktskAPPaymentAppsRegisterREG
            SetVarsForAPPmtAppReg 'AP Payment Application
            
        Case ktskAPARSettlementRegisterREG
            SetVarsForSettleReg  'Settlement Register
            
        Case ktskGLRevalPosting
            SetVarsForGLRevalue 'GL Revalue Register
        
        Case ktskARRevalPosting
            SetVarsForARRevalue 'AR Revalue Register
                
        Case ktskAPRevalPosting
            SetVarsForAPRevalue 'AP Revalue Register

        Case Else    'Incorrect Task Number
            If miMode = kSingleBatch Then giSotaMsgBox Me, moFrmSysSession, _
                            kmsgUnexpectedOperation, mlTaskNumber
            moClass.lUIActive = kChildObjectInactive
            moClass.moFramework.SavePosSelf
            Me.Hide
            bCheckStatus = False

    End Select
    
    msReportFileName = msReportName
 
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForGLBudget()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Budget Revision Journal"
    Select Case ddnFormat.ListIndex
        Case Is = kBR_SUMMARY
            'Summary Format
            msReportName = kBR_RPT_Name_SUM
        
        Case Is = kBR_DETAIL
            'Detail Format
            msReportName = kBR_RPT_Name
            
        Case Else
            'Detail Format
            msReportName = kBR_RPT_Name
    End Select
    msWorkTable = kBR_WRK_table
    mlSourceCompBatchType = kBR_Batch_Type
    mlSourceCompBatchType_NF = 0

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForGLBudget", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForSalesComm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Commission Register [Batch: " & CStr(vBatchID) & "]"
    msWorkTable = "tarCommRegWrk"
    msSPCreateTable = "sparRegComm"
    msSPDeleteTable = "sparRegCommDel"
    mlBatchType = 4

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForSalesComm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForCashRcpts()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Cash Receipts Register [Batch: " & CStr(vBatchID) & "]"
    Select Case ddnFormat.ListIndex  ' Evaluate Number.
        Case 1
            If miUseMultCurr = 1 Then
                msReportName = kCR_MC_SUMMARY_RPT_Name
            Else
                msReportName = kCR_SUMMARY_RPT_Name
            End If
        Case -1 To 0  '
            If miUseMultCurr = 1 Then
                msReportName = kCR_MC_DETAIL_RPT_Name
            Else
                msReportName = kCR_DETAIL_RPT_Name
            End If
    End Select
    msWorkTable = kCR_WRK_table
    msSPCreateTable = kCR_SP_CREATEWRK
    msSPDeleteTable = kCR_SP_DELETEWRK
    msBatchType = kCR_batch_Type
    mlBatchType = 2

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForCashRcpts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForInvoiceReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    mlBatchType = 1
    Me.Caption = "Invoice Register [Batch: " & CStr(vBatchID) & "]"
    Select Case ddnFormat.ListIndex  ' Evaluate Number.
        Case 1
          'Intellisol start
          If chk(2) = vbChecked Then
            If miUseMultCurr = 1 Then
                msReportName = kIN_MC_SUMMARY_RPT_PA
            Else
                msReportName = kIN_SUMMARY_RPT_PA
            End If
          Else
          'Intellisol end
            If miUseMultCurr = 1 Then
                msReportName = kIN_MC_SUMMARY_RPT_Name
            Else
                msReportName = kIN_SUMMARY_RPT_Name
            End If
          'Intellisol start
          End If
          'Intellisol end
        Case -1 To 0 '
          'Intellisol start
          If chk(2) = vbChecked Then
            If miUseMultCurr = 1 Then
                msReportName = kIN_MC_DETAIL_RPT_PA
            Else
                msReportName = kIN_DETAIL_RPT_PA
            End If
          Else
          'Intellisol end
            If miUseMultCurr = 1 Then
                msReportName = kIN_MC_DETAIL_RPT_Name
            Else
                msReportName = kIN_DETAIL_RPT_Name
            End If
          'Intellisol start
          End If
          'Intellisol end
    End Select
    msWorkTable = kIN_WRK_table
    msSPCreateTable = kIN_SP_CREATEWRK
    msSPDeleteTable = kIN_SP_DELETEWRK
    msBatchType = kIN_batch_Type

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForInvoiceReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function SetVarsForValCashRcpt()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Me.Caption = guLocalizedStrings.CRJournalDesc
    msReportName = kERROR_LOG_RPT_NAME
    msWorkTable = kCR_WRK_table
    msSPCreateTable = kCR_SP_CREATEWRK
    msSPDeleteTable = kCR_SP_DELETEWRK
    msBatchType = kCR_batch_Type
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForValCashRcpt", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForVoucherReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Voucher Register [Batch: " & CStr(vBatchID) & "]"
    Select Case ddnFormat.ListIndex  ' Evaluate Number.
        Case 1
            If miUseMultCurr = 1 Then
                msReportName = kVO_SUMMARY_RPT_NAME_MC
            Else
                msReportName = kVO_SUMMARY_RPT_NAME
            End If
        Case -1 To 0
'PA start
            If chk(2) = vbChecked Then
                If miUseMultCurr = 1 Then
                    msReportName = kVO_DETAIL_RPT_NAME_MC_PA
                Else
                    msReportName = kVO_DETAIL_RPT_NAME_PA
                End If
            Else
'PA end
                If miUseMultCurr = 1 Then
                    msReportName = kVO_DETAIL_RPT_NAME_MC
                Else
                    msReportName = kVO_DETAIL_RPT_NAME
                End If
'PA start
            End If
'PA end
    End Select
    msWorkTable = kVO_WRK_TABLE
    msSPCreateTable = kVO_SP_CREATEWRK
    msSPDeleteTable = kVO_SP_DELETEWRK
    mlBatchType = kVO_BATCH_TYPE
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForVoucherReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForGLRevalue()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "GL Revaluation Register [Batch: " & CStr(vBatchID) & "]"
    msReportName = kRVGL_CURR_RPT_NAME
    msWorkTable = kRV_WRK_TABLE
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForGLRevalue", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForARRevalue()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "AR Revaluation Register [Batch: " & CStr(vBatchID) & "]"
    msReportName = kRVAR_CURR_RPT_NAME
    msWorkTable = kRV_WRK_TABLE
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForARRevalue", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForAPRevalue()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "AP Revaluation Register [Batch: " & CStr(vBatchID) & "]"
    msReportName = kRVAP_CURR_RPT_NAME
    msWorkTable = kRV_WRK_TABLE
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForAPRevalue", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForSettleReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "AP-AR Settlement Register [Batch: " & CStr(vBatchID) & "]"
    mbAPARSettlement = True
    msReportName = kSE_RPT_NAME
    msWorkTable = kSE_WRK_TABLE
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForSettleReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForValARPmtAppl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Me.Caption = guLocalizedStrings.ARPAJournalDesc
    msReportName = kERROR_LOG_RPT_NAME
    msWorkTable = kARPA_WRK_table
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForValARPmtAppl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function SetVarsForValARReverseAppl()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Me.Caption = guLocalizedStrings.ARPAJournalDesc
    msReportName = kERROR_LOG_RPT_NAME
    msWorkTable = kARRA_WRK_table
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForValARReverseAppl", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Public Function SetVarsForValInvoice()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = guLocalizedStrings.INJournalDesc
    msReportName = kERROR_LOG_RPT_NAME
    msWorkTable = kIN_WRK_table
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForValInvoice", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForValComm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = guLocalizedStrings.INJournalDesc
    msReportName = kERROR_LOG_RPT_NAME
    msWorkTable = kIN_WRK_table
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForValComm", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForAPPmtAppReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Payment Application Register [Batch: " & CStr(vBatchID) & "]"
    Select Case ddnFormat.ListIndex  ' Evaluate Number.
        Case 1
                msReportName = kPA_SUMMARY_RPT_NAME
        Case -1 To 0
                msReportName = kPA_DETAIL_RPT_NAME
    End Select
    
    msReportName = kPA_RPT_NAME
    msWorkTable = kPA_WRK_TABLE
    msSPCreateTable = kPA_SP_CREATEWRK
    msSPDeleteTable = kPA_SP_DELETEWRK
    mlBatchType = kPA_BATCH_TYPE

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForAPPmtAppReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForCheckReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Check Register [Batch: " & CStr(vBatchID) & "]"
    Select Case ddnFormat.ListIndex  ' Evaluate Number.
        Case 1
            If miUseMultCurr = 1 Then
                msReportName = kCK_SUMMARY_RPT_NAME_MC
            Else
                msReportName = kCK_SUMMARY_RPT_NAME
            End If
        Case -1 To 0
            If miUseMultCurr = 1 Then
                msReportName = kCK_DETAIL_RPT_NAME_MC
            Else
                msReportName = kCK_DETAIL_RPT_NAME
            End If
    End Select
    msWorkTable = kCK_WRK_TABLE
    msSPCreateTable = kCK_SP_CREATEWRK
    msSPDeleteTable = kCK_SP_DELETEWRK
    mlBatchType = kCK_BATCH_TYPE

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForCheckReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForARPmtAppReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Me.Caption = "Payment Application Register [Batch: " & CStr(vBatchID) & "]"
    If miUseMultCurr = 1 Then
        msReportName = kARPA_MC_RPT_Name
    Else
        msReportName = kARPA_RPT_Name
    End If
    msWorkTable = kARPA_WRK_table
    msSPCreateTable = kARPA_SP_CREATEWRK
    msSPDeleteTable = kARPA_SP_DELETEWRK
    msBatchType = kARPA_batch_Type
    mlBatchType = 3

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForARPmtAppReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function SetVarsForARReverseAppReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Me.Caption = "Reverse Application Register [Batch: " & CStr(vBatchID) & "]"
    If miUseMultCurr = 1 Then
        msReportName = kARRA_MC_RPT_Name
    Else
        msReportName = kARRA_RPT_Name
    End If
    msWorkTable = kARRA_WRK_table
    msSPCreateTable = kARRA_SP_CREATEWRK
    msSPDeleteTable = kARRA_SP_DELETEWRK
    msBatchType = kARRA_batch_Type
    mlBatchType = 3

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForARReverseAppReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function
Public Function SetVarsForFinChgReg()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlBatchType = 1
    Me.Caption = "Finance Charge Register [Batch: " & CStr(vBatchID) & "]"
    Select Case ddnFormat.ListIndex  ' Evaluate Number.
        Case 1
            If miUseMultCurr = 1 Then
                msReportName = kIN_MC_SUMMARY_RPT_Name
            Else
                msReportName = kIN_SUMMARY_RPT_Name
            End If
        Case -1 To 0 '
            If miUseMultCurr = 1 Then
                msReportName = kIN_MC_DETAIL_RPT_Name
            Else
                msReportName = kIN_DETAIL_RPT_Name
            End If
    End Select
    msWorkTable = kIN_WRK_table
    msSPCreateTable = kIN_SP_CREATEWRK
    msSPDeleteTable = kIN_SP_DELETEWRK
    msBatchType = kIN_batch_Type

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForFinChgReg", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForGLTrans()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "General Journal [Batch: " & CStr(vBatchID) & "]"
    msReportName = kGJ_RPT_Name
    
    Select Case True
        Case miSourceCompUseMultCurr = kOptionNone _
        And miSourceCompAcctRefUsage = kvAcctRefUsageNotUsed _
        And (miJrnlType = kStandard Or miJrnlType = kBegBal)
            'No Multi-Currency
            'No AcctRefCodes
            'Standard or Beg Bal
             msReportName = kGJ_RPT_Name
        
        Case miSourceCompUseMultCurr = kOptionNone _
        And miSourceCompAcctRefUsage <> kvAcctRefUsageNotUsed _
        And (miJrnlType = kStandard Or miJrnlType = kBegBal) 'RS 98
            'No Multi-Currency
            'AcctRefCodes
            'Standard or Beg Bal
             msReportName = kGJ_RPT_Name_RefCd
             
        Case miSourceCompUseMultCurr = kOptionNone _
        And miJrnlType = kTrans
            'No Multi-Currency
            'Transaction
             msReportName = kGJ_RPT_Name_TRANS
        
        Case miSourceCompUseMultCurr = kOptionTrue _
        And miJrnlType = kStandard
            'Multi-Currency
            'Standard
             msReportName = kGJ_RPT_NameMC
        
        Case miSourceCompUseMultCurr = kOptionTrue _
        And miJrnlType = kTrans
            'Multi-Currency
            'Transaction
             msReportName = kGJ_RPT_Name_TransMC
        
        Case miSourceCompUseMultCurr = kOptionTrue _
        And miSourceCompAcctRefUsage = kvAcctRefUsageNotUsed _
        And miJrnlType = kBegBal
            'Multi-Currency
            'No AcctRefCodes
            'Beg Bal
             msReportName = kGJ_RPT_Name 'kGJ_RPT_Name_BBMC FUTURE MC/BEGBAL not supported
        
        Case miSourceCompUseMultCurr = kOptionTrue _
        And miSourceCompAcctRefUsage <> kvAcctRefUsageNotUsed _
        And miJrnlType = kBegBal
            'Multi-Currency
            'AcctRefCodes
            'Beg Bal
             msReportName = kGJ_RPT_Name_RefCd
    End Select
    msWorkTable = kGJ_WRK_table
    mlSourceCompBatchType = kGJ_Batch_Type
    mlSourceCompBatchType_NF = 0

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForGLTrans", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function SetVarsForGLAlloc()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Me.Caption = "Allocation Journal [Batch: " & CStr(vBatchID) & "]"
    msReportName = kAJ_RPT_Name
    msWorkTable = kAJ_WRK_table
    mlSourceCompBatchType = kAJ_Batch_Type
    mlSourceCompBatchType_NF = kAJ_Batch_Type_NF

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetVarsForGLAlloc", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function SetControlsFlag() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    mbFormat = False
    mbInvoice = False
    mbLine = False
    mbProject = False
    mbPositive = False
    mbACH = False
    mbSortGrid = False
    chkConfirm.Visible = True
    
    Select Case mlTaskNumber
        Case ktskGLJrnlTransReg
            mbProject = True
            chk(2).Caption = "Print Detail Comments"
        
        Case ktskGLAllocationReg
            'No controls
            frmRegOption.Visible = False
            frmPrintPost.Width = 9195
        
        Case ktskGLBudRevReg
            mbFormat = True
            lblFormat.Caption = "Format"
        
        Case ktskAPVoucherRegisterREG
           mbFormat = True
           mbProject = True
           lblFormat.Caption = "Format"
           chk(0).Caption = "Include Project details"
           chk(0).Enabled = False
           
        Case ktskAPPaymentAppsRegisterREG
           mbFormat = True
           lblFormat.Caption = "Format"
       
        Case ktskAPARSettlementRegisterREG
           frmPrintPost.Visible = False
           frmRegOption.Width = 9195
           mbFormat = True
            lblFormat.Caption = "Format"
            mbSortGrid = True
        
        Case ktskInvoicePrintPost
           mbFormat = True
           mbInvoice = True
           mbProject = True
           mbLine = True
           lblFormat.Caption = "Format"
           chk(0).Caption = "Include Invoice Comments"
           chk(1).Caption = "Include Line Item Comments"
           chk(2).Caption = "Include Project details"
           mbSortGrid = True
        
        Case ktskCashRcptPrintPost
           mbFormat = True
           mbInvoice = True
           mbProject = True
           lblFormat.Caption = "Format"
           chk(0).Caption = "Include Payment Comments"
           chk(2).Caption = "Include Invoice Comments"
           mbSortGrid = True
        
        Case ktskPmtAppPrintPost
           mbInvoice = True
           mbProject = True
           chk(0).Caption = "Include Payment Comments"
           chk(2).Caption = "Include Invoice Comments"
           mbSortGrid = True
           
        Case ktskReverseApplPrintPost
           mbInvoice = True
           mbProject = True
           chk(0).Caption = "Include Payment Comments"
           chk(2).Caption = "Include Invoice Comments"
           mbSortGrid = True
        
        Case ktskSalesCommPrintPost
            mbFormat = True
            lblFormat.Caption = "Format"
            mbSortGrid = True
            
        Case ktskGLRevalPosting, ktskAPRevalPosting, ktskARRevalPosting
            lblFormat.Caption = "Format"
            mbSortGrid = True
            chkConfirm.Visible = False
            frmRegOption.Visible = False
            frmPrintPost.Width = 9195
        
        Case ktskFinChgReg
           mbFormat = True
           mbInvoice = True
           mbProject = True
           mbLine = True
           lblFormat.Caption = "Format"
           chk(0).Caption = "Include Invoice Comments"
           chk(1).Caption = "Include Line Item Comments"
           chk(2).Caption = "Include Project details"
           mbSortGrid = True
          
        Case ktskAPManualCheckRegisterREG, ktskAPCheckRegisterREG
            If miMode = 1 Then
                If mbInitialLoad Then
                    fraPosPay.Visible = False
                    fraACH.Visible = False
                    mbFormat = True
                    mbACH = False
                    mbPositive = False
                    miExtractType = kExtractNone
          
                    If mlTaskNumber = ktskAPCheckRegisterREG Then
                        If bValidateEFT Then
                            miExtractType = kExtractACH
                            fraACH.Visible = True
                            mbACH = True
                            gEnableControls cmdACHProperties
                            IsFileDirSet
                        Else
                            miExtractType = kExtractPosPay
                            fraPosPay.Visible = True
                            mbPositive = True
                            SetPosPayState
                        
                            If PosPayAllowed Then chkPosPay.Value = vbChecked 'SGS-JMM 4/28/2011
                        End If
        
                    ElseIf mlTaskNumber = ktskAPManualCheckRegisterREG Then
                        miExtractType = kExtractPosPay
                        chkPosPay.Value = vbUnchecked
                        fraPosPay.Visible = True
                        mbPositive = True
                        SetPosPayState
                    
                        If PosPayAllowed Then chkPosPay.Value = vbChecked 'SGS-JMM 4/28/2011
                    End If
        
                   mbInitialLoad = False
                End If
            End If
    End Select
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "SetControlsFlag", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Private Function lValidateReportPath(sPath As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    ' -----------------------------------
    ' Function Name     lValidateReportPath
    ' Written By        Ashish
    ' Purpose           Validates the path on the current machine
    ' Input Args        sPath           Path to be validated
    ' Return values     0       Failure
    ' ------------------------------------

Dim iAttr As Integer
On Error GoTo ExpectedErrorRoutine

    lValidateReportPath = kFailure          ' Initializing
    
    ' Path length should be greater than 0
    If Not Len(sPath) > 0 Then
        GoTo ExpectedErrorRoutine
    Else
        ' Till the final element curtail the path elements
        If Right(sPath, 1) = BACKSLASH Then
            iAttr = GetAttr(Mid(sPath, 1, Len(sPath) - 1))
            
        Else
            ' Get attributes of the final element of the path
            iAttr = GetAttr(sPath)
            sPath = sPath & BACKSLASH
        End If
        
        ' If the final element is not a directory then
        ' Gil: Made the change at Loretta Johnson request (for compressed disk)
        ' If iAttr <> vbDirectory Then
        If iAttr And vbDirectory = 0 Then
            'MsgBox "Report Path: " & sPath & " is not a valid directory on this workstation;" & vbCr & " may not exist on this client workstation", vbCritical, "frmRegister.lValidateReportPath"
            GoTo ExpectedErrorRoutine
        End If
    End If
    lValidateReportPath = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
ExpectedErrorRoutine:
    If Err.Number = 53 Then
        ' raise error message here
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidReportPaths

        ' No need to continue with the error
        lValidateReportPath = 0
        Exit Function
    End If
    
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateReportPath", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub MakeUIChanges()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   
    ddnOutput.AddItem gsBuildString(kIMPrinter, moClass.moAppDB, moClass.moSysSession), 0
    ddnOutput.ItemData(0) = kIMPrinter
    ddnOutput.AddItem gsBuildString(kIMScreen, moClass.moAppDB, moClass.moSysSession), 1
    ddnOutput.ItemData(1) = kIMScreen
    ddnOutput.ListIndex = 0
    
    DisableControls

    SetControlsFlag
    
    If mbFormat = True And mlTaskNumber <> ktskAPPaymentAppsRegisterREG And mlTaskNumber <> ktskSalesCommPrintPost Then
        ddnFormat.AddItem gsBuildString(kIMSummary, moClass.moAppDB, moClass.moSysSession), 2
        ddnFormat.ItemData(0) = kIMSummary
        ddnFormat.AddItem gsBuildString(kIMDetail, moClass.moAppDB, moClass.moSysSession), 3
        ddnFormat.ItemData(1) = kIMDetail
        ddnFormat.ListIndex = 1
    Else
        ddnFormat.AddItem gsBuildString(kIMSummary, moClass.moAppDB, moClass.moSysSession), 2
        ddnFormat.ItemData(0) = kIMSummary
        ddnFormat.ListIndex = 0
    End If
 
    ResizeRegisterOptionsFrame mbFormat, mbInvoice, mbLine, mbProject, mbPositive, mbACH, mbSortGrid
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MakeUIChanges", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
   
Public Sub ModuleVariables()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
   'Assign system object properties to module-level variables for easier access
    With moClass
        msSourceCompanyID = .moSysSession.CompanyID
        msSourceCompanyName = .moSysSession.CompanyName
        msCompanyID = .moSysSession.CompanyID
        msUserID = .moSysSession.UserId
        mbEnterAsTab = .moSysSession.EnterAsTab
        mlLanguage = .moSysSession.Language
        mdBusinessDate = .moSysSession.BusinessDate
        miFullGL = .moSysSession.FullGL
        mbPAActivated = .moSysSession.IsModuleActivated(kModulePA)
        msCurrencyId = .moSysSession.CurrencyID
        Set moAppDAS = .moAppDB
        Set moSysDAS = .moAppDB
        miSourceCompUseMultCurr = .moAppDB.Lookup("UseMultCurr", "tglOptions", "CompanyID = " & gsQuoted(msSourceCompanyID))
        miSourceCompAcctRefUsage = .moAppDB.Lookup("AcctRefUsage", "tglOptions", "CompanyID = " & gsQuoted(msSourceCompanyID))
        .mlPostStatus = kRegisterNotPosted
        mlModuleNumber = .moFramework.GetTaskModule()
        mlTaskNumber = .moFramework.GetTaskID()
    End With
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ModuleVariables", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function bValidateACHPrenote() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs As Object
    Dim iACHTranCode As Integer
    Dim iPrenoteCount As Integer
    Dim iLiveCount As Integer
    
    bValidateACHPrenote = False
    
    'In order to continue processing the batch must be either ALL live checks or ALL prenotification
    sSQL = "SELECT DISTINCT tapVendor.ACHTranCode "
    sSQL = sSQL & "FROM tapPendVendPmt WITH (NOLOCK) INNER JOIN tapVendor WITH (NOLOCK) "
    sSQL = sSQL & "ON tapPendVendPmt.VendKey = tapVendor.VendKey "
    sSQL = sSQL & "AND tapPendVendPmt.BatchKey = " & Str(mlBatchKey)
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    iPrenoteCount = 0
    iLiveCount = 0
    
    While Not rs.IsEOF
        iACHTranCode = giGetValidInt(rs.Field("ACHTranCode"))
              
        'Check for Prenotification ACH Tran Code
        '23 - Checking Credit Prenotification
        '28 - Checking Debit Prenotification
        '33 - Savings Credit Prenotification
        '38 - Savings Debit Prenotification
        
        If iACHTranCode = 23 Or iACHTranCode = 28 Or iACHTranCode = 33 Or iACHTranCode = 38 Then
            iPrenoteCount = iPrenoteCount + 1
            mbPrenote = True
        Else
           iLiveCount = iLiveCount + 1
        End If
        
        rs.MoveNext
    Wend
    
    If iPrenoteCount > 0 And iLiveCount > 0 Then
        'Batch contains both prenote and live checks, validation should fail here
        Exit Function
    Else
        bValidateACHPrenote = True
    End If
    
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bValidateACHPrenote", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
#End If

    Dim iRet As Integer

    moReport.CleanupWorkTables
    
    If moClass.mlError = 0 Then
        'if a report preview window is active, query user to continue closing.
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
    End If
    
    If UnloadMode <> vbFormCode Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
        'Do Nothing
        
    End Select
    
    frmProperties.sFileExtractDir = ""
    frmProperties.bProcessExtract = False

    'SGS-JMM 1/31/2011
    On Error Resume Next
    If Not (cypHelper Is Nothing) Then cypHelper.Dispose
    Set cypHelper = Nothing
    On Error GoTo VBRigErrorRoutine
    'End SGS-JMM

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    gUnloadChildForms Me
    giCollectionDel moClass.moFramework, moSotaObjects, -1
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Skip +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    UnloadObjects
End Sub

Private Sub UnloadObjects()
'+++ VB/Rig Skip +++
    Set moSettings = Nothing
    Set moSort = Nothing
    Set moReport = Nothing
    Set moContextMenu = Nothing
    Set moClass = Nothing
    Set moDDData = Nothing
    Set moOptions = Nothing
    Set moPrinter = Nothing
    Set sRealTableCollection = Nothing
    Set sWorkTableCollection = Nothing
    Set moAppDAS = Nothing
    Set moAPRegister = Nothing
    Set moARRegister = Nothing
    Set moCommRegister = Nothing
    Set moGLRegister = Nothing
    Set moMCRegister = Nothing
End Sub

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, X, Y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, X, Y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, X, Y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

#If CUSTOMIZER Then
Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If mlTaskNumber = ktskAPCheckRegisterREG Then
        mbInitialLoad = True
        SetControlsFlag
        ResizeRegisterOptionsFrame mbFormat, mbInvoice, mbLine, mbProject, mbPositive, mbACH, mbSortGrid
    End If

    If Not mbLoadSuccess Then
       Form_Load
    End If

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
Private Sub cmdPosPayProperties_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdPosPayProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPosPayProperties_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdPosPayProperties_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdPosPayProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdPosPayProperties_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdACHProperties_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdACHProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdACHProperties_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdACHProperties_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdACHProperties, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdACHProperties_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMessageHeader(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMessageHeader_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMessageHeader(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMessageHeader_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chk_Click(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chk(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chk_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chk_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chk(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chk_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chk_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chk(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chk_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPosPay_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPosPay, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPosPay_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPosPay_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPosPay, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPosPay_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkConfirm_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkConfirm, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkConfirm_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPost_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPost_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkPost_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkPost, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkPost_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRegister_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkRegister, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRegister_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkRegister_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkRegister, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkRegister_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkSummary_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkSummary, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkSummary_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportSettings_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboReportSettings_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboReportSettings, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboReportSettings_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFormat_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnFormat, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFormat_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFormat_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFormat_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnFormat_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnFormat, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnFormat_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_Click(bCancel As Boolean, ByVal lPrevIndex As Long, ByVal lNewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            bCancel = moFormCust.OnDropDownClick(ddnOutput, lPrevIndex, lNewIndex, True)
            If bCancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress ddnOutput, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub ddnOutput_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus ddnOutput, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "ddnOutput_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

'************************ Sort Stuff *******************************
Private Sub grdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_LeaveCell", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_KeyUp(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridKey keycode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridChange Col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Added by Atul on Dec'18 to disable click on CheckBox For Physical Count Batch
    'No Subtotalling and PageBreaks in PC trantype
'    If (Col = 3 Or Col = 4) And glBatchType = kBatchTypeIMPC Then
'        Exit Sub
'    End If

    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.SortGridGotFocus
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.SortGridKeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'*********************** End Sort Stuff *****************************

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If

Public Function SaveCurrentSorts() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sSortField As String
    Dim sSortOrder As String
    Dim lSubTotal As Long
    Dim lPageBreak As Long
    Dim lSortRow As Long

    If gbPostBatchesFlag Then Exit Function

    Set msSortField = Nothing
    Set msSortOrder = Nothing
    Set mlSubTotal = Nothing
    Set mlPageBreak = Nothing

    Set msSortField = New Collection
    Set msSortOrder = New Collection
    Set mlSubTotal = New Collection
    Set mlPageBreak = New Collection

    For lRow = 1 To grdSort.MaxRows
        sSortField = Trim$(gsGridReadCellText(grdSort, lRow, kColSort))

        If Len(sSortField) <> 0 Then
            sSortOrder = Trim$(gsGridReadCellText(grdSort, lRow, kColAscending))
            lSubTotal = CLng(gsGridReadCell(grdSort, lRow, kColSubtotal))
            lPageBreak = CLng(gsGridReadCell(grdSort, lRow, kColPageBreak))

            msSortField.Add sSortField
            msSortOrder.Add sSortOrder
            mlSubTotal.Add lSubTotal
            mlPageBreak.Add lPageBreak
            lSortRow = lRow
        End If
    Next lRow
    
    SaveCurrentSorts = lSortRow
'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SaveCurrentSorts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Sub lReinitializeSorts(mlTaskNumber As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If gbPostBatchesFlag Then Exit Sub
   
    lReorganizeWorkTables mlTaskNumber

    If Not moSort Is Nothing Then
        moSort.CleanSortGrid
    End If

    Set moSort = New clsSort

    'CUSTOMIZE: Set Groups and Sorts as required.
    'Note there are two optional parameters: iNumGroups,iNumSorts for lInitSort.
    If (moSort.lInitSort(Me, moDDData, 0, 1) = kFailure) Then
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
        Exit Sub
    End If

    mlSessionID = moGLRegister.GetUniqueTableKey(moClass.moAppDB, msWorkTable)
    
    With moSort
        Select Case mlTaskNumber
        
            Case ktskSalesCommPrintPost
                .lInsSort "SperID", "tarCommRegWrk", moDDData, gsBuildString(kSalespersonIDDomain, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "SalesTerritoryID", "tarCommRegWrk", moDDData, gsBuildString(kSalesTerritoryTbl, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustID", "tarCommRegWrk", moDDData, gsBuildString(kCustomerIDDomain, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustName", "tarCommRegWrk", moDDData, gsBuildString(kCustNameCol, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustClassName", "tarCommRegWrk", moDDData, gsBuildString(kCustClassTbl, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "TranID", "tarCommRegWrk", moDDData, gsBuildString(kInvoiceTbl, moClass.moAppDB, moClass.moSysSession)
            
            Case ktskInvoicePrintPost, ktskFinChgReg, ktskAPARSettlementRegisterREG
                .lInsSort "TranID", "tarInvcRegWrk", moDDData, gsBuildString(kARZJA001InvoiceNumber, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "TranType", "tarInvcRegWrk", moDDData, gsBuildString(kARZJA001InvoiceType, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustID", "tarInvcRegWrk", moDDData, gsBuildString(kARZJA001CustomerID, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustName", "tarInvcRegWrk", moDDData, gsBuildString(kARZJA001CustomerName, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustClassName", "tarInvcRegWrk", moDDData, gsBuildString(kARZJA001CustomerClass, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "SeqNo", "tarInvcRegWrk", moDDData, gsBuildString(kARZJA001EntryOrder, moClass.moAppDB, moClass.moSysSession)
            
            Case ktskCashRcptPrintPost
                .lInsSort "CustID", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001custID, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustName", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001custName, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustClassID", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001custClass, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "TranID", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001custPaymentNo, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "SeqNo", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001EntryOrder, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld1", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001u1, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld2", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001u2, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld3", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001u3, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld4", "tarCashRRegWrk", moDDData, gsBuildString(kARZJB001u4, moClass.moAppDB, moClass.moSysSession)
            
            Case ktskPmtAppPrintPost
                .lInsSort "CustID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001CustomerID, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustName", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001CustomerName, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustClassID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001CustomerClass, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "ApplyFromTranID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001AppliedFrom, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "ApplyToTranID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001AppliedToInvoice, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "SeqNo", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001EntryOrder, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld1", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u1, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld2", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u2, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld3", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u3, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld4", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u4, moClass.moAppDB, moClass.moSysSession)
                
            Case ktskReverseApplPrintPost
                .lInsSort "CustID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001CustomerID, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustName", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001CustomerName, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "CustClassID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001CustomerClass, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "ApplyFromTranID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001AppliedFrom, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "ApplyToTranID", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001AppliedToInvoice, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "SeqNo", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001EntryOrder, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld1", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u1, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld2", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u2, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld3", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u3, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "UserFld4", "tarPayApplRegWrk", moDDData, gsBuildString(kARZJC001u4, moClass.moAppDB, moClass.moSysSession)
            
            Case ktskGLRevalPosting
                .lInsSort "Currency", "tmcRevalRgstrWrk", moDDData, gsBuildString(kMCZJC001CurrID, moClass.moAppDB, moClass.moSysSession)
            
            Case ktskARRevalPosting
                .lInsSort "Currency", "tmcRevalRgstrWrk", moDDData, gsBuildString(kMCZJC001CurrID, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "Customer", "tmcRevalRgstrWrk", moDDData, gsBuildString(kMCZJC001CustID, moClass.moAppDB, moClass.moSysSession)
            
            Case ktskAPRevalPosting
                .lInsSort "Currency", "tmcRevalRgstrWrk", moDDData, gsBuildString(kMCZJC001CurrID, moClass.moAppDB, moClass.moSysSession)
                .lInsSort "Vendor", "tmcRevalRgstrWrk", moDDData, gsBuildString(kMCZJB001VendID, moClass.moAppDB, moClass.moSysSession)
            
        End Select
    End With
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lReinitializeSorts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub lRestoreCurrentSorts()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim lRow As Long
    Dim sSortField As String
    Dim sSortOrder As String
    Dim lSubTotal As Long
    Dim lPageBreak As Long
    
    If gbPostBatchesFlag Then Exit Sub

    For lRow = 1 To msSortField.Count
        sSortField = msSortField.Item(lRow)
        sSortOrder = msSortOrder.Item(lRow)
        lSubTotal = mlSubTotal.Item(lRow)
        lPageBreak = mlPageBreak.Item(lRow)

        gGridUpdateCellText grdSort, lRow, kColSort, sSortField
        gGridUpdateCellText grdSort, lRow, kColAscending, sSortOrder
        gGridUpdateCell grdSort, lRow, kColSubtotal, CLng(lSubTotal)
        gGridUpdateCell grdSort, lRow, kColPageBreak, CLng(lPageBreak)
        gGridUnlockCell grdSort, kColAscending, lRow
    Next lRow
    
    '+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRestoreCurrentSorts", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function GetUniqueTableKey(SysDB As Object, TableName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim key As Long

    With SysDB
        .SetInParam TableName
        .SetOutParam key
        .ExecuteSP ("spGetNextSurrogateKey")
        key = .GetOutParam(2)
        .ReleaseParams
    End With
    GetUniqueTableKey = key

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetUniqueTableKey", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Property Let UserIsOwner(ByVal bnewvalue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iSecuritylevel As Integer
    
    mbUserIsOwner = bnewvalue
    
    iSecuritylevel = giGetSecurityLevel(moClass.moFramework, moClass.moFramework.GetTaskID)
    
    If iSecuritylevel = sotaTB_SUPERVISORY Or (iSecuritylevel = sotaTB_NORMAL And mbUserIsOwner) Then
        mbPostingAllowed = True
    Else
        mbPostingAllowed = False
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UserIsOwner_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get UserIsOwner() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    UserIsOwner = mbUserIsOwner
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UserIsOwner_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
Public Function lUpdateTaskSettings()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim sSQL            As String
    Dim sReportFileName As String
    Dim sDisplaySettingName As String
    Dim lRptSetKey As Long
       
    lUpdateTaskSettings = kFailure
    
    On Error GoTo ExpectedErrorRoutine
    
    Screen.MousePointer = vbHourglass
    
    sDisplaySettingName = Trim$(cboReportSettings.Text)
    sRptPrgname = mlTaskNumber
    sReportFileName = msReportName
    
    sSQL = "CompanyID = '" & msCompanyID & "' AND RptSettingID = " & gsQuoted(sDisplaySettingName) & " AND RptProgram = " & gsQuoted(sRptPrgname)
    
    'check if setting exists
    lRptSetKey = glGetValidLong(moClass.moAppDB.Lookup("RptSettingKey", "tsmReportSetting (NOLOCK)", sSQL))
  
    'updates the settings from App.EXEName to TaskNumber
    sSQL = "UPDATE tsmReportSetting " & _
        "Set RptFile = '" & sReportFileName & "', RptProgram = '" & sRptPrgname & "'" _
        & " WHERE RptSettingKey = " & lRptSetKey & " AND CompanyID = '" & msCompanyID & "'"
    
    moAppDAS.BeginTrans
    
    On Error GoTo ExpectedDBError
    
    moAppDAS.ExecuteSQL sSQL
    
    moAppDAS.CommitTrans
    On Error GoTo ExpectedErrorRoutine
        
    Screen.MousePointer = vbDefault
    
    lUpdateTaskSettings = kSuccess
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedDBError:
    moAppDAS.Rollback

ExpectedErrorRoutine:
        gSetSotaErr Err, sMyName, "MakeUIChanges", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

'**************************************************************
'**************************************************************
'SGS-JMM 1/31/2011 (START)
'**************************************************************
'**************************************************************
Public Function GetCypressQuery() As String
    Dim batchID As String
    
    On Error Resume Next
    batchID = moClass.moSysSession.AppDatabase.Lookup("BatchID", "tciBatchLog", "BatchKey = " & mlBatchKey)
    GetCypressQuery = "[Company ID] = """ & moClass.moSysSession.CompanyID & """ and [Batch ID] = """ & batchID & """"
End Function

Private Function GetDefaultPosPayExportDir() As String
    Dim retVal As String
    
    retVal = gsGetValidStr(moClass.moAppDB.Lookup("ExportDir", "tapPosPayExpDir_SGS", "CompanyID = " & gsQuoted(msCompanyID)))
    If Right(retVal, 1) = "\" Then
        retVal = Left(retVal, Len(retVal) - 1)
    End If
    
    GetDefaultPosPayExportDir = retVal
End Function

Private Function PosPayAllowed() As Boolean
    Dim prmpt As Variant
    Dim evtID As String
    Dim evtUser As String
    
    prmpt = False
    evtID = "APPOSPAYEXP_SGS"
    evtUser = CStr(moClass.moSysSession.UserId)
        
    PosPayAllowed = CBool(moClass.moFramework.GetSecurityEventPerm(evtID, evtUser, prmpt))
End Function
'**************************************************************
'**************************************************************
'SGS-JMM 1/31/2011 (STOP)
'**************************************************************
'**************************************************************



