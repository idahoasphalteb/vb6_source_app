Attribute VB_Name = "basRegister"
'***********************************************************************
'     Name: basRegister
'     Desc: Main Module of this Object containing 'Sub Main ()'.
'Copyright: Copyright (c) 1995-2012 Sage Software, Inc.
' Original:
'     Mods: 08/15/1996 JCT
'***********************************************************************
Option Explicit

'// LLJ: Do i need to keep this here?
Global Const RPT_MODULE = "GL"   'Enter the two character module Name here

Public gbCancelPrint As Boolean    '   Cancel Print Dialog from Post Batches. HBS.
Public gbSkipPrintDialog As Boolean   '   don't show print dialog. HBS.

Public Type uLocalizedStrings
    Printed           As String
    NotPrinted        As String
    Printing          As String
    PrintingCompleted As String
    FatalErrors       As String
    NotBalanced       As String
    Balanced          As String
    Posting           As String
    Posted            As String
    PostingFailed     As String
    INJournalDesc     As String
    CRJournalDesc     As String
    MCJournalDesc     As String
    ARPAJournalDesc   As String
    CMJournalDesc     As String
    JournalDesc       As String
    GJJournalDesc     As String
    RJJournalDesc     As String
    AJJournalDesc     As String
    BRJournalDesc     As String
    PrintError        As String
    Detail            As String
    Summary           As String
    CheckingBatch     As String
End Type

Public Type RegStatus
    iBatchStatus As Long
    iRecordsExist As Long
    iPrintStatus As Long
    iErrorStatus As Long
    iBalanceStatus As Long
End Type

Public Const BACKSLASH = "\"
Public Const kNumZeroPad = 1
Public Const kNumJustifyRight = 2

Public Const kARPrintingRegister = 151826                   ' Printing AR Register...
Public Const kARPostingRegister = 151827                    ' Posting AR Register...
Public Const kARPostingFailed = 151828                      ' AR Posting failed, AP Posting rolled back.
Public Const kARPostingCompleted = 151829                   ' AR Posting completed.
Public Const kRegisterPosted    As Long = 1
Public Const kRegisterNotPosted As Long = 0
Public Const kFileTypeDel = 1
Public Const kFileTypeFix = 2
Public Const kStrJustifyLeft = 1
Public Const kStrJustifyRight = 2
Public Const kDateYYMMDD = 1
Public Const kDateYYYYMMDD = 2
Public Const kDateMMDDYY = 3
Public Const kDateMMDDYYYY = 4
Public Const kDateJulian = 5
Public Const kDateDDMMYY = 6
Public Const kExtractNone = 1
Public Const kExtractACH = 2
Public Const kExtractPosPay = 3

Public Const kvAcctRefUsageNotUsed = 0
Public mcSPInCreateTable        As New Collection
Public mcSPOutCreateTable       As New Collection
Public mcSPInDeleteTable        As New Collection
Public mcSPOutDeleteTable       As New Collection
Public mcSPInModulePost         As New Collection
Public mcSPOutModulePost        As New Collection
Public msSPCreateTable          As String
Public msSPDeleteTable          As String
Public msSPModulePost           As String
Public msWorkTable              As String
Public msReportFileName         As String
Public miSessionId              As Long
Public msCurrencyId             As String
Public msReportName             As String
Public miSourceCompUseMultCurr  As Integer
Public miSourceCompAcctRefUsage As Integer
Public msSourceCompanyID        As String
Public msSourceCompanyName      As String
Public mlSourceCompBatchKey     As Long
Public miUseMultCurr            As Integer
Public miJrnlType               As Integer
Public mlBatchType              As Long
Public mlSourceCompBatchType    As Long
Public mlSourceCompBatchType_NF As Long
Public msUserID                 As String
Public mlSessionID              As Long
Public miSessionIDItem          As Integer
Public miFullGL                 As Integer
Public msCompanyID              As String
Public miGLRgstrType            As Integer
Public lBatchKey                As Long
Public mlModuleNumber           As Long
Public miGLPostDetailFlag       As Integer          ' 1  None, 2 Summary,  Detail
Public msEntity                 As String           ' Holds the Batch Entity ID
Public mdPostDate               As Date
Public mlPostDate               As Date         ' Holds the post date
Public iBatchStatus             As Integer
Public miErrorsOccured          As Integer
Public miMode                   As Integer      ' 0 for register 1 for Post XX Batches
Public msBatchID                As String
Public mlLanguage               As Long
Public msBatchType              As String

Public mlTaskNumber             As Long
Public guLocalizedStrings       As uLocalizedStrings
Public moSettings               As clsSettings

Public moClass                  As Object
Public mbProcessBatch           As Boolean
Public moGLRegister             As Object
Public moCommRegister           As Object
Public msPrintButton            As String           ' Preview or Print
Public moSort                   As clsSort
Public moPrinter                As Printer
Public mbPostingAllowed         As Boolean
Public mbRegChecked             As Boolean
Public mbRegPrinted             As Boolean
Public msCRWSelect              As String
Public gbPostBatchesFlag        As Boolean   ' Holds whether the invocation is from Post Im bates or not


Public mPostError               As Integer
Public mlVOBatchKey             As Long
Public sRptPrgname              As String
Public iMCHold                  As Integer
Public iMCModuleNo              As Integer
Public vBatchID                 As Variant
Public msReversalDate           As String
Public msRevalDate              As String

Const VBRIG_MODULE_ID_STRING = "CIZJADL1.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Main is required as the standard 'Startup Form'.
'           Main can be empty.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basRegister"
End Function


