VERSION 5.00
Begin VB.Form frmProperties 
   Caption         =   "ACH Properties"
   ClientHeight    =   4095
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4530
   Icon            =   "frmProperties.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   4095
   ScaleWidth      =   4530
   StartUpPosition =   3  'Windows Default
   Begin VB.DriveListBox drvExtract 
      Height          =   315
      Left            =   870
      TabIndex        =   3
      Top             =   135
      Width           =   3480
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   435
      Left            =   3135
      TabIndex        =   2
      Top             =   3570
      Width           =   1230
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   435
      Left            =   1710
      TabIndex        =   1
      Top             =   3570
      Width           =   1230
   End
   Begin VB.DirListBox dirExtract 
      Height          =   2115
      Left            =   150
      TabIndex        =   0
      Top             =   525
      Width           =   4215
   End
   Begin VB.Label lblDirPath 
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Height          =   330
      Left            =   150
      TabIndex        =   6
      Top             =   3120
      Width           =   4275
   End
   Begin VB.Label lblSaveDir 
      Caption         =   "Directory for saving file :"
      Height          =   240
      Left            =   150
      TabIndex        =   5
      Top             =   2760
      Width           =   2670
   End
   Begin VB.Label lblPPDrive 
      Caption         =   "Look in :"
      Height          =   225
      Left            =   180
      TabIndex        =   4
      Top             =   165
      Width           =   810
   End
End
Attribute VB_Name = "frmProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: clsExtract
'     Desc: Positive Pay Process
'Copyright: Copyright (c) 1995-2012 Sage Software, Inc.
'************************************************************************************

Option Explicit

Public mbProcessPosPay    As Boolean
Private mbProcessExtract  As Boolean
Private msFileExtractDir  As String

Dim sCurrDrive            As String
Dim sSavedDrive           As String
Dim sSavedPath            As String
Dim sLastDrive            As String
Dim sLastPath             As String

Private Sub cmdCancel_Click()
    Me.Hide
End Sub

Private Sub cmdOK_Click()
    If sSavedDrive <> "" And sSavedPath <> "" Then
        mbProcessExtract = True
        Me.Hide
    Else
        mbProcessExtract = False
        MsgBox "Directory is not set!", vbExclamation, "Positive Pay"
    End If
End Sub

Private Sub dirExtract_Change()
    If drvExtract.ListIndex = -1 Then
        drvExtract.Drive = dirExtract.Path
    End If
    SaveToReg dirExtract.Path
    lblDirPath.Caption = sSavedPath
    
End Sub

Private Sub drvExtract_Change()
    Dim iresult As Integer
    
    On Error GoTo ErrorTrap
    If Trim(Left(drvExtract.Drive, 2)) = Trim(sLastDrive) Then
        dirExtract.Path = sLastPath
    Else
        dirExtract.Path = drvExtract.Drive
    End If
    Exit Sub

ErrorTrap:
    If Err.Number = 68 Then
        iresult = MsgBox("Device is not ready or unavailable!", vbRetryCancel, "Device Error!")
        If iresult = 4 Then
            drvExtract_Change
        Else
            'Change back to last saved settings.
            drvExtract.Drive = sSavedDrive
            dirExtract.Path = sSavedPath
            Exit Sub
        End If
    Else
        Exit Sub
    End If
End Sub

Private Sub Form_Load()
    mbProcessExtract = False
    GetSavedInfo
    drvExtract.Drive = sSavedDrive
    
    On Error Resume Next
    dirExtract.Path = sSavedPath
End Sub

Private Sub GetSavedInfo()
    Dim sKeyString As String
    
    If mbProcessPosPay = True Then
        sKeyString = "PositivePay"
    Else
        sKeyString = "ACH"
    End If

    sSavedPath = GetSetting(sKeyString, "DefaultSettings", "Dir", "C:\")
    If Len(sSavedPath) >= 2 Then
        sSavedDrive = Left(sSavedPath, 2)
    End If
End Sub

Private Sub SaveLastInfo()
    sLastDrive = sSavedDrive
    sLastPath = sSavedPath
End Sub

Public Property Get bProcessExtract() As Boolean
  bProcessExtract = mbProcessExtract
End Property

Public Property Get sFileExtractDir() As String
  sFileExtractDir = msFileExtractDir
End Property

Public Property Let bProcessExtract(bDoExtract As Boolean)
  mbProcessExtract = bDoExtract
End Property

Public Property Let sFileExtractDir(sExtractDir As String)
  msFileExtractDir = sExtractDir
End Property


Private Sub SaveToReg(sExtractDir As String)
    Dim sKeyString As String
    
    If mbProcessPosPay = True Then
        sKeyString = "PositivePay"
    Else
        sKeyString = "ACH"
    End If

  SaveLastInfo
  SaveSetting sKeyString, "DefaultSettings", "Dir", sExtractDir
  GetSavedInfo
  msFileExtractDir = sExtractDir
End Sub


