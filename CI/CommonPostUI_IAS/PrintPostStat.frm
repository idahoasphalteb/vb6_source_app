VERSION 5.00
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Begin VB.Form frmPrintPostStat 
   Caption         =   "Print/Post Status"
   ClientHeight    =   3615
   ClientLeft      =   5280
   ClientTop       =   2175
   ClientWidth     =   5805
   HelpContextID   =   1012821
   Icon            =   "PrintPostStat.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3615
   ScaleWidth      =   5805
   Begin VB.Frame Frame2 
      Height          =   2535
      Left            =   300
      TabIndex        =   0
      Top             =   330
      Width           =   5235
      Begin VB.Label Label1 
         Caption         =   "Currently Processing Batch: "
         Height          =   315
         Left            =   300
         TabIndex        =   6
         Top             =   600
         Width           =   2085
      End
      Begin VB.Label lblBatch 
         Caption         =   "ARIN-000122"
         Height          =   285
         Left            =   2520
         TabIndex        =   5
         Top             =   600
         Width           =   2175
      End
      Begin VB.Label Label6 
         Caption         =   "Status:"
         Height          =   225
         Left            =   300
         TabIndex        =   4
         Top             =   1500
         Width           =   1155
      End
      Begin VB.Label lblStatusMsg 
         Caption         =   "Validating Data"
         Height          =   375
         Left            =   2490
         TabIndex        =   3
         Top             =   1500
         Width           =   2505
      End
      Begin VB.Label Label5 
         Caption         =   "Options Selected:"
         Height          =   345
         Left            =   300
         TabIndex        =   2
         Top             =   1050
         Width           =   2115
      End
      Begin VB.Label lblOptions 
         Caption         =   "Print and Post"
         Height          =   315
         Left            =   2520
         TabIndex        =   1
         Top             =   1050
         Width           =   2115
      End
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   7
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmPrintPostStat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public mfrmMain      As Form
Private miDoit       As Integer
Private mlBatchKey() As Long
Private mdPostDate() As Date
Private miPrint()    As Integer
Private miPost()     As Integer
Private gsBatchID()  As String

Const VBRIG_MODULE_ID_STRING = "CommRegister.cls"

Public Sub ShowMe()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    miDoit = 1
    Me.Show vbModal

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ShowMe", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Public Sub Initialize(dPostDate() As Date, lBatchKey() As Long, iPrint() As Integer, _
    iPost() As Integer, sBatchID() As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim i As Integer
Dim J As Integer

    i = UBound(dPostDate)
    ReDim mdPostDate(i)
    ReDim mlBatchKey(i)
    ReDim miPrint(i)
    ReDim miPost(i)
    ReDim gsBatchID(i)
    
    For J = 0 To i
        mdPostDate(J) = dPostDate(J)
        mlBatchKey(J) = lBatchKey(J)
        miPrint(J) = iPrint(J)
        miPost(J) = iPost(J)
        gsBatchID(J) = sBatchID(J)
    Next J
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim i As Integer
Dim iNumBatches As Integer
Dim sBatchID As String
Dim iBatchType As Integer
Dim sSQL As String
Dim rs As Object
Dim lTaskNumber As Long

    If miDoit = 1 Then
        miDoit = 0
        iNumBatches = UBound(mdPostDate)
        
        For i = 0 To iNumBatches
            If miPost(i) = 1 Then
                mfrmMain.mbPostFlag = True
            Else
                mfrmMain.mbPostFlag = False
            End If
            
            If miPrint(i) = 1 Then
                mfrmMain.mbRegisterFlag = True
            Else
                mfrmMain.mbRegisterFlag = False
            End If
                        
            mlPostDate = mdPostDate(i)
            mfrmMain.mlBatchKey = mlBatchKey(i)
            
            lblBatch = gsBatchID(i)
            
            If miPost(i) = 1 And miPrint(i) = 1 Then
                lblOptions = "Print and Post"
            ElseIf miPost(i) = 1 Then
                lblOptions = "Post Only"
            Else
                lblOptions = "Print Only"
            End If
            
            lTaskNumber = oClass.moFramework.GetTaskID()
            miMode = kMultipleBatch
            mlSourceCompBatchKey = mlBatchKey(i)
            mlModuleNumber = oClass.moFramework.GetTaskModule()
            With mfrmMain
                .mdPostDate = mdPostDate(i)
                .TaskNumber = lTaskNumber
                .HandleToolbarClick (kTbProceed)
            End With
        Next i
    End If
    
    If gbCancelPrint Then   '   Is user canceled from print dialog, don't continue. HBS.
        Exit Sub
    End If
    
    If mfrmMain.mbErrorOccured And Not mfrmMain.mbAPARSettlement Then
        lblStatusMsg = "Process Complete, errors occurred in one or more batches"
    Else
        lblStatusMsg = "Process Complete"
        Screen.MousePointer = vbDefault
        If mfrmMain.mbAPARSettlement Then
            Unload Me
            mfrmMain.mbAPARSettlement = False
        End If
    End If
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmPrintPostStat"
End Function




Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property




