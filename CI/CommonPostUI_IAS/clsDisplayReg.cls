VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDisplayReg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'************************************************************************************
'     Name: clsDisplayReg
'     Desc: Handles the Register/Post user interface for the modules AP,AR,GL,MC
'           and Project Invoice
'Copyright: Copyright (c) 1995-2012 Sage Software, Inc.
' Original: 05/03/1995 KMC
'     Mods: 07/23/1995 KMC; 09/04/1997 JCT;
'************************************************************************************
Option Explicit
    '=======================================================================
    '           Public Variables for use by other modules
    '           -----------------------------------------
    '
    '   moFramework     - required to give other modules the ablitiy to
    '                     call the framework methods and properties, such
    '                     as UnloadSelf, LoadSotaObject, . . .
    '                     Object reference is set in InitializeObject.
    '
    '   mlContext       - Contains information of how this class object
    '                     was invoked.  It is set in InitializeObject
    '
    '
    '   moSysSession    - system Manager Session Object which contains
    '                     information specific to the user's session with
    '                     this accounting application.  For example, current
    '                     CompanyID, Business date, UserID, UserName, . . .
    '
    '   miShutDownRequester
    '                   - The ShutDownRequester is the object responsible
    '                     for requesting this object to shut down.  There are
    '                     two possible choices: 1) the Framework requests the
    '                     object to shut itself down (kFrameworkShutDown; and
    '                     2) the User Interface or UnloadSelf call requests
    '                     the framework to shut this object down (kUnloadSelfShutDown).
    '                     The main purpose of this flag is to
    '                     ensure that the framework reference isn't removed (or
    '                     set to nothing) before the UI.
    '=======================================================================
    

    Public moFramework                  As Object
    Public mlContext                    As Long
    Public moAppDB                      As Object
    Public moSysSession                 As Object
    Public miShutDownRequester          As Integer
    Public mlError                      As Long
    Public moDasSession                 As Object
    Public mlPostStatus                 As Long
    Public mbBatchProcess               As Boolean
    Public mlBatchKey                   As Long

    '=======================================================================
    'Private Variables for Class Properties
    '=======================================================================
    Private mlRunFlags                  As Long
    Private mlUIActive                  As Long
    Private mbPreviewOpen               As Boolean
    Private msCompanyID                 As String

    '=======================================================================
    'Private variables for use within this class only
    '=======================================================================
    Public mfrmMain                     As Object
    Public moCTI                        As Object


Const VBRIG_MODULE_ID_STRING = "clsDisplayReg.CLS"

Public Property Get lUIActive() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       lUIActive Property Get informs the parent object of its Active state.
'       A class is Active if it is in the middle of processing a record.
'       This property is Required for all Sage MAS 500 Interface 3 classes.
'***********************************************************************
    lUIActive = mlUIActive
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lUIActive_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Let bPreviewOpen(bOpen As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    mbPreviewOpen = bOpen
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "bPreviewOpen_Let", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Let sCompanyID(sCompanyID As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    msCompanyID = sCompanyID

'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "sCompanyID_Let", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property


Public Property Get bPreviewOpen() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    bPreviewOpen = mbPreviewOpen
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "bPreviewOpen_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Let lUIActive(lNewActive As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       lUIActive Property Let is contains the new Active state of the
'       class.  This is set internally within the form or method within
'       this class
'       This property is **Required** for all Sage MAS 500 Interface 3 classes.
'***********************************************************************
    mlUIActive = lNewActive
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lUIActive_Let", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Get lRunFlags() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       lRunFlags Property Get can be used to inform other objects or
'       modules of how this object was invoked by the framework or
'       parent object.
'       This property is **Required** for all Sage MAS 500 Interface classes.
'***********************************************************************
    lRunFlags = mlRunFlags
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lRunFlags_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Let lRunFlags(ltRunFlags As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       lRunFlags Property Let is set by the Framework no matter which
'       interface Type is being used.  This property is set before any
'       other method call of this class.
'       This property is **Required** for all Sage MAS 500 Interface classes.
'***********************************************************************
    mlRunFlags = ltRunFlags
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lRunFlags_Let", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Private Sub Class_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'
'       Class_Initialize re-dimensions the Hide and Minimize Form Arrays
'       to 0.  The purpose of this is to ensure that the UBound function
'       doesn't error if the ShowUI or RestoreUI are called out of order.
'
'       Class_Initialize also sets the ShutDownRequester variable to say that
'       if anything goes wrong initially within this class, that the
'       framework will automatically attempt to shut down this class
'       object.

'***********************************************************************
    miShutDownRequester = kFrameworkShutDown
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Initialize", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
'     Description:
'       InitalizeObject performs setup required for all applications.
'
'       InitalizeObject sets the context in which this class was invoked.
'       The context contains information from both the framework and
'       application on how this class was invoked.
'
'       InitalizeObject also gets the system Manager Session Object.
'       The system Session Object provides our applications with user
'       specific session information such as the current CompanyID,
'       Business date, etc and more importantly Application Database Object,
'       and system Database Object.
'
'       InitializeObject is called for Sage MAS 500 Inteferface 1, Sage MAS 500 Interface 2
'       and Sage MAS 500 Interface 3.
'
'    Parameters:
'       oFramework <in> - reference to the framework event object which
'                         gives this object access to the public
'                         framework properties and methods.
'       lContext <in>   - The context contains information from both the
'                         framework and application on how this class was
'                         invoked.
'
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     In case of kFailure, the Framework will execute
'                     the TerminateObject method of this class and shut
'                     down this object.
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    
    InitializeObject = kFailure
    
    mlContext = lContext
    Set moFramework = oFramework    ' set the event object for this form
    
    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title
    'mbPeriodEnd = False
    InitializeObject = kSuccess
    
    'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "InitializeObject", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function LoadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
'     Description:
'       LoadUI is responsible for loading the main form(s) required for
'       user interface of this class.  LoadUI performs the following
'       tasks:
'           1) Instantiate (or set a pointer to) the main form(s) of
'              the class.
'           2) In order for the form to use the class' public
'              variables, a reference back to the class is required.
'              This is done using the form's oClass Property Set and setting
'              the object property to Me (this class instance).
'           3) For the application to use the form in a variety of ways,
'              (Maintenance, Add-on-the-Fly, Drill Around), the Form needs
'              to know the user-defined context in which it was invoked.
'              The form contains a property to Hold this Value which it
'              refers to as the Run Mode.  The Run Mode can
'              be extracted by performing a bit-wise comparison of the
'              context originally passed in through the InitializeObject method.
'
'       LoadUI is called automatically by the framework for SotaInterface 2,3 and
'       the AutoLoadUI run flag is set.
'
'    Parameters:
'           lContext <in>   - Context of the LoadUI or how it is being
'                             invoked by the framework
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     In case of kFailure, the Framework will proceed
'                     to shut down this object starting with the
'                     QueryShutDown method, proceeding to UnloadUI and
'                     TerminateObject.
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine

    LoadUI = kFailure

    Set mfrmMain = New frmCommonRegister
    Set mfrmMain.oClass = Me

    With mfrmMain
        .lRunMode = mlContext And kRunModeMask
        '.CompanyID = msCompanyID
    End With

    Load mfrmMain
    If mlError Then
        Err.Raise mlError
    End If
    
    LoadUI = kSuccess
    
    'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "LoadUI", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function GetUIHandle(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mfrmMain Is Nothing Then
        GetUIHandle = mfrmMain.hwnd
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "GetUIHandle", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function DisplayUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       The framework will call this method when the class object is
'       initially invoked and the RunFlags are are set to AutoDisplayUI.
'
'       DisplayUI is called automatically by the framework for SotaInterface 2,3 and
'       the AutoDisplayUI run flag is set.
'
'    Parameters:
'           lContext <in>   - Context of the DisplayUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     In case of kFailure, the Framework will proceed
'                     to shut down this object starting with the
'                     QueryShutDown method, proceeding to UnloadUI and
'                     TerminateObject.
'*********************************************************************
    DisplayUI = EFW_CT_MODALEXIT
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "DisplayUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function ShowUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       The framework will call this method when
'       the UI of this object had been hidden in an
'       explicit HideUI because the framework was minimized.
'       When the framework is restored, it requests the UI
'       of its objects to show themselves.
'
'       ShowUI is called automatically by the framework for SotaInterface 2,3 and
'
'    Parameters:
'           lContext <in>   - Context of the ShowUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Currently the framework ignores a failure.
'*********************************************************************
    ShowUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ShowUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function MinimizeUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       MinimizeUI minimizes the object's main form and hides all other forms
'       launched directly from the main form or from the class.
'       Forms already minimized or already hidden are ignored.
'       Forms launched from loaded objects are also ignored.
'
'
'    Parameters:
'           lContext <in>   - Context of the MinimizeUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     The framework currently ignores a failure.
'*********************************************************************
    MinimizeUI = kFailure
    If Not mfrmMain Is Nothing Then
        mfrmMain.WindowState = vbMinimized
    End If
    MinimizeUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "MinimizeUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function RestoreUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       Calls the general procedure which restores the object's main
'       form and all other forms explicitly minimized in a previous
'       MinimizeUI.
'
'    Parameters:
'           lContext <in>   - Context of the RestoreUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     The framework currently ignores a failure.
'
'   Original: 06/12/95 HHO
'
'   Modified:
'
'   Copyright (c) 1995-2005 Sage Software, Inc.
'
'*********************************************************************
    RestoreUI = kFailure
    If Not mfrmMain Is Nothing Then
        mfrmMain.WindowState = vbNormal
    End If
    RestoreUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "RestoreUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function HideUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       If any child objects are Active, then this form is the parent
'       and it cannot hide itself.
'
'    Parameters:
'           lContext <in>   - Context of the HIdeUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     The framework currently ignores a failure.
'
'   Original: 06/12/95 HHO
'
'   Modified:
'
'   Copyright (c) 1995-2005 Sage Software, Inc.
'
'*********************************************************************
    HideUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "HideUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

'***********************************************************************
'                   Class Object ShutDown Process
'                   -----------------------------
'
'   Sage MAS 500 Interface 3:
'       If InitializeObject returns kFailure, then the TerminateObject
'       method is called immediately and this object is guaranteed to
'       shutdown whether or not the TerminateObject returned kSuccess
'       or kFailure.
'
'       If LoadUI or DisplayUI returned kFailure, the QueryShutDown method is
'       called, followed by UnloadUI and TerminateObject.  The framework will
'       ignore failures from the QueryShutDown, UnloadUI, and Terminate object,
'       because the object is in a failure.
'
'       If the Framework requests shutdown or the UnloadSelf call requests
'       shutdown, then the QueryShutDown method is invoked.
'       If a successful QueryShutDown is performed, the UnloadUI method is
'       called. And if a successful UnloadUI is performed, TerminateObject
'       method is called.  Once the TerminateObject method is called, the
'       object will be shut down whether or not the TerminateObject returned
'       success or failure.
'
'       Special NOTE:
'           The QueryShutDown is designed to check for Active child objects
'           and return failure to the framework if any have been found.  If
'           there are any Active Child Objects remaining after a successful
'           QueryShutDown, the framework will shut them down prior to the
'           UnloadUI method call.
'
'           The UnloadUI is designed to unload its UI and within that it will unload
'           all its child objects (Sage MAS 500 children).  If there are
'           any Sage MAS 500 child objects remaining after a successful UnloadUI,
'           the framework will shut them down prior to the TerminateObject
'           method call.
'
'           The TerminateObject is designed to unload/remove all Non-UI
'           object references.  If there are any child Non-UI (No Sage MAS 500
'           Interface or Sage MAS 500 Interface 1 (?)) remaining after the
'           TerminateObject call, then the framework will remove or
'           shut them down.
'***********************************************************************

Public Function QueryShutDown(lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       QueryShutDown method to determine if this form can be
'       shutdown.  This is done by checking all child objects (created
'       via a LoadSotaObject) for the lUIActive property set to
'       kChildObjectActive.
'
'       QueryShutDown is called automatically by the framework for
'       Sage MAS 500 Interface 1, 2, 3.
'
'    Parameters:
'           lContext <in>   - Context of the QueryShutDown or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'                     Proceed with the ShutDown
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Do not proceed with the ShutDown because there is
'                     an Active child object.
'***********************************************************************
    QueryShutDown = kFailure

    If Not mfrmMain Is Nothing Then
        If gbActiveChildObjects(mfrmMain.moSotaObjects) Then
            'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If

    QueryShutDown = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "QueryShutDown", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function UnloadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       UnloadUI method attempts to Unload the UI or any forms loaded
'       within the life of this class object.  During the Unload of
'       the form, all Sage MAS 500 child Objects (Sage MAS 500 Interface 2,3) must
'       be unloaded.  This is currently coded in the form's Query_Unload
'       event.
'
'       UnLoadUI is called automatically by the framework for SotaInterface 2,3
'
'    Parameters:
'           lContext <in>   - Context of the UnloadUI or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'                     Proceed with the ShutDown
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Do not proceed with the ShutDown because there is
'                     a problem or the user has cancelled the framwork
'                     shutdown request.  This can be done if a dirty
'                     record is up an the form asks to save changes.
'                     If the user presses the cancel button, then the
'                     user is requesting the entire shutdown of the
'                     application to cancel its shutdown.
'***********************************************************************
    UnloadUI = kFailure

    If Not mfrmMain Is Nothing Then
        If miShutDownRequester = kFrameworkShutDown Then
            Unload mfrmMain
            If mfrmMain.bCancelShutDown Then
                'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
                Exit Function
            End If
            Set mfrmMain = Nothing
        Else
            If mlError Then
                mfrmMain.PerformCleanShutDown
            End If
        End If
    End If
    
    Unload mfrmMain
    Set mfrmMain = Nothing

    UnloadUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "UnloadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function TerminateObject(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
'   Description:
'       TerminateObject method removes all Non-UI object references
'       created in through the InitializeObject method.  If the framework
'       requested to shutdown of this object, then setting the framework
'       reference to Nothing is executed.  Otherwise, the form requested
'       an UnloadSelf with its QueryUnload.  After this TerminatObject
'       method has finished executing, control is relinquished
'       back to the that form event.  At that point, it is okay to set
'       the framework reference to nothing.
'
'    Parameters:
'           lContext <in>   - Context of the TerminateObject or how it is being
'                             invoked by the framework.
'   Return Values:
'       kSuccess    - if the function executed its code without error.
'                     Proceed with the ShutDown
'       kFailure    - if any of the code executed in this
'                     function produced an error or invalid result.
'                     Failure is ignored and the Object is guaranteed
'                     to shutdown at this point.
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    TerminateObject = kFailure
    
    If Not mfrmMain Is Nothing Then
        Set mfrmMain = Nothing
    End If
  
    DefaultTerminateObject Me
  
    TerminateObject = kSuccess

    'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
  
ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "TerminateObject", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function lProcessRegister(ByVal lBatchKey As Long, _
                                 ByVal vPostDate As Variant, Optional lVOBatchKey As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim bRegChecked As Boolean
    Dim dPostDate As Date
    Dim vJrnlType As Variant

    If (vPostDate <> 0 And vPostDate <> "") Then
        dPostDate = CDate(vPostDate)
    Else
        dPostDate = gsFormatDateToDB("01-01-1900")
    End If
    
    If Not mfrmMain Is Nothing Then
        Set mfrmMain = Nothing
    End If
    
    If moFramework.LoadUI(Me) = kFailure Then
        'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If
    
    lProcessRegister = mlPostStatus

    'Do any 'pre-show' procedures here
    With mfrmMain
        .BatchKey = lBatchKey
        .PostDate = dPostDate
        .lVOBatchKey = lVOBatchKey
      
        'Continue if record(s) exist.
        'If .bNewBatch() Then
            vJrnlType = moAppDB.Lookup("GeneralJrnlType", "tglBatch", "BatchKey = " & lBatchKey)
            If Not IsNull(vJrnlType) Then
                miJrnlType = vJrnlType
            Else
                miJrnlType = 0
            End If
        
            .SetVariables mlTaskNumber
            .SetPrintPostCtrls
            .Show vbModal
        'End If
    End With
    
    If mlError Then
        Err.Raise mlError
    Else
        lProcessRegister = mlPostStatus
    End If

    'Exit this function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lProcessRegister", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "clsDisplayReg"
End Function

Public Sub PostBatches(frmPostXXBatches As Object, dPostDate() As Date, lBatchKey() As Long, iPrint() As Integer, _
                iPost() As Integer, sBatchID() As String, lBatchRow() As Long, Optional bShowUI As Boolean = True)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
'     Description:
'    Parameters:
'   Return Values:
'***********************************************************************
    On Error GoTo ExpectedErrorRoutine

    Dim i As Integer
    Dim iNumBatches As Integer
    Dim iRegPrinted As Integer
    Dim lBatchType As Long
    Dim sSQL As String
    Dim vJrnlType As Variant
    Dim rs As Object
    
    gbCancelPrint = False   '   initialize that user did not cancel from print dialog. HBS.
    gbSkipPrintDialog = False   '   initialize to show print dialong on first batch. HBS.
    
    If mlTaskNumber = ktskAPARSettlementRegisterREG Then
         Set frmPrintPostStat.mfrmMain = frmPostXXBatches
    Else
        If mfrmMain Is Nothing Then
            If moFramework.LoadUI(Me) = kFailure Then
                'Exit this subroutine
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                Exit Sub
            End If
        End If
        
        miMode = kMultipleBatch
        Set frmPrintPostStat.mfrmMain = mfrmMain
        
        With mfrmMain
            vJrnlType = moAppDB.Lookup("GeneralJrnlType", "tglBatch", "BatchKey = " & lBatchKey(i))
            If Not IsNull(vJrnlType) Then
                miJrnlType = vJrnlType
            Else
                miJrnlType = 0
            End If
        End With
    End If
    
    frmPrintPostStat.Initialize dPostDate(), lBatchKey(), iPrint(), iPost(), sBatchID()
    frmPrintPostStat.ShowMe
    
    If mlError Then
        Err.Raise mlError
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PostBatches", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Sub StartInvoiceRegister(PostDate As Date, BatchKey As Long, bUserIsOwner As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
   
    mlBatchKey = BatchKey
    If mfrmMain Is Nothing Then
        If moFramework.LoadUI(Me) = kFailure Then
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Sub
        End If
    End If
       
    With mfrmMain
        miMode = 1
        .mdPostDate = PostDate
        .mlBatchKey = BatchKey
        .BatchKey = BatchKey
        .UserIsOwner = bUserIsOwner
        .SetVariables mlTaskNumber
        .SetPrintPostCtrls
        .Show vbModal
    End With
       
    If mlError Then
        Err.Raise mlError
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "startInvoiceRegister", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Sub PostARBatches(dPostDate() As Date, lBatchKey() As Long, iPrint() As Integer, _
    iPost() As Integer, sBatchID() As String, Optional sPrinter As String = "", _
    Optional iCopies As Integer = -1, Optional iFormat As Integer = -1, Optional iARSort As Integer = -1, _
    Optional sReportMsg As String = "", Optional bShowUI As Boolean = True, Optional frmPostXXBatches As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim i               As Integer
    Dim iNumBatches     As Integer

    gbCancelPrint = False   '   initialize that user did not cancel from print dialog. HBS.
    gbSkipPrintDialog = False   '   initialize to show print dialong on first batch. HBS.
    
    If mlTaskNumber = ktskAPARSettlementRegisterREG Then
        Set frmPrintPostStat.mfrmMain = frmPostXXBatches
    Else
        If mfrmMain Is Nothing Then
            If moFramework.LoadUI(Me) = kFailure Then
    '+++ VB/Rig Begin Pop +++
    '+++ VB/Rig End +++
                Exit Sub
            End If
        End If
            
        miMode = kMultipleBatch
        Set frmPrintPostStat.mfrmMain = mfrmMain
    End If
    
    frmPrintPostStat.Initialize dPostDate(), lBatchKey(), iPrint(), iPost(), sBatchID()
    frmPrintPostStat.ShowMe
    
    If mlError Then
        Err.Raise mlError
    End If
    

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "PostBatches", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Function InitRegister(lBatchKey As Long, sRevalDate As String, _
    sReversalDate As String, iHold As Integer, iModuleNo As Integer, iBatchStatus As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++

    mlBatchKey = lBatchKey
    msRevalDate = sRevalDate
    msReversalDate = sReversalDate
    iMCHold = iHold * -1
    iMCModuleNo = iModuleNo

    If mfrmMain Is Nothing Then
        If moFramework.LoadUI(Me) = kFailure Then
            'Exit this function
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
            Exit Function
        End If
    End If
    
    With mfrmMain
        .BatchKey = lBatchKey
        .lBatchStatus = iBatchStatus
        .SetVariables mlTaskNumber
        .SetPrintPostCtrls
        .Show vbModal
    End With
   
    If mlError Then Err.Raise mlError

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "InitRegister", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function
