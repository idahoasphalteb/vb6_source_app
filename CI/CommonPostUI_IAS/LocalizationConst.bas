Attribute VB_Name = "basLocalizationConst"
Option Explicit

'
'*********************************************************************************
' This file contains the String and Message constant definitions for this project
' created by the SDK Wizard.
'
'*********************************************************************************
'
' Strings
'
Public Const kNone = 103                                    ' (none)
Public Const kAPInvoiceNo = 6408                            ' Invoice No

'
' Messages
'
Public Const kmsgCannotBeBlank = 130082                     ' {0} cannot be blank.
Public Const kmsgVoucherPurchAmtNeg = 140044                ' A {0} may not have a Purchases Amount less than zero!The current Purchases Amount Total is:  {1}Please adjust one or more detail lines so that the Purchases Amount is greater than or equal to zero.
Public Const kmsgVoucherInvTotalNeg = 140043                ' A {0} may not have an Invoice Total less than zero!The current Invoice Total is:  {1}Please adjust the detail line(s), Sales Tax amount or Freight amount so that the Invoice Total is greater than or equal to zero.
Public Const kmsgVoucherOutOfBalance = 140005               ' This {0} is out of balance!The current Undistributed Balance is:  {1}Do you want the system to automatically adjust the Invoice Amount for you?
Public Const kmsgVoucherTermsDiscNeg = 140065               ' A {0} may not have a Terms Discount Amount less than zero!Please enter a valid Terms Discount Amount.
Public Const kmsgSetCurrControlsError = 110098              ' The controls on this form which accept currency values cannot be set based on the parameters for the following Currency:Currency ID: {0}
Public Const kmsgInvTotalCannotBeZero = 140320              ' The Invoice Total cannot be zero for a {0} that is applied to a Check.
Public Const kmsgVoucherLogError = 140012                   ' An error occurred while attempting to update the {0} log.Please call Product Support for technical assistance.
Public Const kmsgUnexpectedConfirmUnloadRV = 100009         ' Unexpected Confirm Unload Return Value: {0}
Public Const kmsgDDNNotRegistered = 220056                  ' The drill down "{0}" is not registered on your machine. Please contact your system administrator.
Public Const kmsgSetupSelectionGridFail = 153368            ' Setup of the Selection Grid Failed.
Public Const kmsgProc = 153223                              ' Unexpected Error running Stored Procedure {0}.
Public Const kmsgEntryRequired = 100078                     ' You must specify a value for {0}
Public Const kmsgUnexpectedCtrlArryIndex = 100007           ' Unexpected Control Array Index Value
Public Const kmsgCantLoadDDData = 153369                    ' Unable to load data from Data Dictionary.
Public Const kmsgFatalReportInit = 153302                   ' Unexpected Error Initializing Report Engine.
Public Const kmsgFataSortInit = 153370                      ' Unable to initialize Sort Grid.
Public Const kIMPrinter = 165533                            ' Printer
Public Const kIMScreen = 165534                             ' Screen
Public Const kIMSummary = 165535                            ' Summary
Public Const kIMDetail = 165536                             ' Detail
Public Const kmsgInvalidReportPaths = 165383                ' Required report file is not present in the system.
Public Const kmsgAPGenFileConnectErr = 140500               ' Unable to connect to the database. {0} extract file cannot be generated.
Public Const kmsgAPGenFileSuccess = 140498                  ' Successfully completed {0} process. Did it generate the proper file format?
Public Const kmsgAPGenFileUnknownErr = 140499               ' Unknown error. {0} extract file cannot be generated.
Public Const kmsgAPGenFileDirErr = 140501                   ' Invalid directory. {0} extract file cannot be generated.
Public Const kmsgAPACHBlankAcct = 140502                    ' Bank account number from [{0}] default Remit-To address cannot be blank. ACH extract file cannot be generated.
Public Const kmsgAPACHBlankRouting = 140503                 ' Bank routing number from [{0}] default Remit-To address cannot be blank. ACH extract file cannot be generated.
Public Const kmsgAPACHBlankOptions = 140504                 ' Unable to retrieve records in option screen. You must complete entries in Set Up ACH Options. ACH extract file cannot be generated.
Public Const kmsgAPACHNoChecks = 140505                     ' Batch does not contain any valid checks (ACH can only process standard checks). ACH extract file cannot be generated.
Public Const kmsgAPACHNextFileNo = 140506                   ' Unable to retrieve the next available Canadian ACH/AFT file number. Canadian ACH/AFT extract file cannot be generated.
Public Const kmsgAPACHBlankRtrn = 140507                    ' Canadian ACH/AFT return account number or return routing number cannot be blank. Canadian ACH/AFT extract file cannot be generated.
Public Const kmsgAPACHPrenotePost = 140515                  ' Warning: This batch cannot be posted because one or more vendors have a Prenotification ACH transaction code. Before you can post the batch, you must change the ACH transaction code in Maintain Vendors to an option other than Prenotification.'
Public Const kmsgAPACHMixedPrenoteBatch = 140516            ' The ACH file cannot be generated and the batch cannot be posted because the batch contains both prenotification and non-prenotification transactions. Change the ACH transaction code in Maintain Vendors to the same type for all vendors in this batch.'
Public Const kmsgAPACHAlphaNumRouting = 140517              ' Invalid Routing Number. {0} default Remit-To address routing number of {1} cannot contain characters that are not aplhanumeric. ACH extract file cannot be generated.
Public Const kSalespersonIDDomain = 15112                   ' Salesperson
Public Const kSalesTerritoryTbl = 15223                     ' Sales Territory
Public Const kCustomerIDDomain = 15033                      ' Customer
Public Const kCustNameCol = 15737                           ' Customer Name
Public Const kCustClassTbl = 15192                          ' Customer Class
Public Const kInvoiceTbl = 15209                            ' Invoice
Public Const kARZJA001InvoiceNumber = 151091                ' Invoice Number
Public Const kARZJA001InvoiceType = 151092                  ' Invoice Type
Public Const kARZJA001CustomerID = 151093                   ' Customer ID
Public Const kARZJA001CustomerClass = 151095                ' Customer Class
Public Const kARZJA001EntryOrder = 151096                   ' Entry Order
Public Const kARZJB001custID = 151072                       ' Customer ID
Public Const kARZJB001custName = 151073                     ' Customer Name
Public Const kARZJB001custClass = 151074                    ' Customer Class
Public Const kARZJB001EntryOrder = 151075                   ' Entry Order
Public Const kARZJB001u1 = 151076                           ' Control Field 1
Public Const kARZJB001u2 = 151077                           ' Control Field 2
Public Const kARZJB001u3 = 151078                           ' Control Field 3
Public Const kARZJB001u4 = 151079                           ' Control Field 4
Public Const kARZJC001CustomerID = 151098                   ' Customer ID
Public Const kARZJC001CustomerName = 151099                 ' Customer Name
Public Const kARZJC001CustomerClass = 151097                ' Customer Class
Public Const kARZJC001AppliedFrom = 151100                  ' Applied From
Public Const kARZJC001AppliedToInvoice = 151101             ' Applied To Invoice
Public Const kARZJC001EntryOrder = 151102                   ' Entry Order
Public Const kARZJC001u1 = 151103                           ' Control Field 1
Public Const kARZJC001u3 = 151105                           ' Control Field 3
Public Const kARZJC001u4 = 151106                           ' Control Field 4
Public Const kARZJA001CustomerName = 151094                 ' Customer Name
Public Const kARZJB001custPaymentNo = 151071                ' Customer Payment No
Public Const kARZJC001u2 = 151104                           ' Control Field 2
Public Const kmsgUnexpectedOperation = 100015               ' Unexpected Operation Value: {0}
Public Const kmsgIMFASAPINotAvailable = 166207              ' Warning: The Fixed Asset System is currently unavailable. Assets will not be created.
Public Const kmsgIMFASInitializeFailed = 166208             ' Warning: Unexpected error while initializing interface to the Fixed Asset Accounting System. Assets will not be created.
Public Const kFASImportStatusReport = 141198                ' FA Import Status Report
Public Const kMCZJC001CurrID = 170003                       ' Currency
Public Const kMCZJC001CustID = 170004                       ' Customer
Public Const kMCZJB001VendID = 170007                       ' Vendor
Public Const kmsgAlreadyPosted = 153282                     ' The batch has been posted.
Public Const kVClosedPer = 151418                           ' Batch {0}: Posting to a closed period.
Public Const kmsgNoBatches = 100076                         ' No {0} batches for posting.
Public Const kmsgGLYearNotSetup = 140475                    ' The necessary fiscal year for the company is not set up in GL. Click OK to have the system set up the fiscal year and post the batch. This may take a long time. Click Cancel to cancel posting. Use GL Setup to set up the fiscal year, then post the batch.
Public Const kmsgCannotPostFatalErrors = 100073             ' {0} Register cannot post.  Invalid data encountered.Check for invalid accounts, closed periods and out-of-balance journals.
Public Const kmsgPostRegister = 100074                      ' Do you want to post the {0} Register?
Public Const kmsgPostErrors = 140072                        ' Errors found during Posting.  Would you like to print the Error Log?
Public Const kmsgPostingSuccessful = 130261                 ' The posting process completed successfully.
Public Const kmsgFiscPerClosed = 130035                     ' The fiscal period for this post date is closed
Public Const kValidating = 1612                             ' Validating
Public Const kPrinting = 18263                              ' Printing
Public Const kPrintingRegister = 6502                       ' Printing Register
Public Const kPrintedWarnings = 6551                        ' Printed-Warnings
Public Const kRegisterPrinted = 6503                        ' Register Printed
Public Const kRegisterPrintedWarnings = 6552                ' Register Printed - Warnings
Public Const kPrinted = 6553                                ' Printed
Public Const kPosted = 6555                                 ' Posted
Public Const kPostingCompleted = 6556                       ' Posting Completed
Public Const kPostingRegister = 6714                        ' Posting Register
Public Const kErrors = 6557                                 ' Errors
Public Const kErrorsOccurred = 6558                         ' Error(s) occurred during validation
Public Const kPostingToGL = 6554                            ' Posting to General Ledger
Public Const kmsgCantGetBatchLock = 120098                  ' Error occurred while attempting to lock batch  {0}.
Public Const kmsgCIAlreadyPosting = 120088                  ' You may not print or post Batch {0}.  It is already posting.
Public Const kmsgCIBadPost = 120091                         ' An error occurred while trying to Print/Post Batches.
Public Const kmsgCIBadStatus = 120090                       ' You may not post batch {0}. It is not in a balanced or interrupted status.
Public Const kmsgCIBatchOnHold = 120092                     ' You may not post batch {0} it  is currently on hold.
Public Const kmsgCIInUseOrPosting = 120089                  ' You may not post batch {0}.  It is either in use or posting.
Public Const kmsgCIPrintRgstr = 120087                      ' The register has not been printed for Batch: {0}.  You must also select print to post this batch.
Public Const kmsgCIPrivateBatch = 120093                    ' Batch {0} is a private batch belonging to another user.  You may not print or post this batch.
