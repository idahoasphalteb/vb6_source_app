VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGridCol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Public ObjColumnName As String
Public TableName As String
Public ColumnName As String
Public IsSelect As Boolean
Public Caption As String
Public SQLDataType As DT_SQLAppDataTypes
Public GridCellType As GridUtils.FPSS_CellTypes
Public GridColumnWidth As Integer
Public GridColumnTypeOption1 As Variant
Public GridColumnTypeOption2 As Variant

Public WhereClause As String

Private Sub Class_Initialize()
    GridCellType = SS_CELL_TYPE_STATIC_TEXT
    GridColumnTypeOption1 = Null
    GridColumnTypeOption2 = Null
    GridColumnWidth = 15
End Sub
