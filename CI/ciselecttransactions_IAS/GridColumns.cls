VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsGridCols"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

'local variable to hold collection
Private mCol As Collection

Public Function Add(ByVal ObjectColumnName As String, ByVal TableName As String, ByVal ColumnName As String, ByVal Caption As String, _
                    Optional IsSelect As Boolean = True, Optional SQLDataType As datatypeutils.DT_SQLAppDataTypes = SQL_VARCHAR, _
                    Optional ByVal GridColumnWidth As Integer = 15, _
                    Optional ByVal GridCellType As GridUtils.FPSS_CellTypes = SS_CELL_TYPE_STATIC_TEXT, _
                    Optional vGridColumnTypeOption1 As Variant, Optional vGridColumnTypeOption2 As Variant, _
                    Optional sKey As String) As clsGridCol
                    
 
    'create a new object
    Dim objNewMember As clsGridCol
    Set objNewMember = New clsGridCol


    If IsMissing(vGridColumnTypeOption1) Then
        vGridColumnTypeOption1 = Null
    End If
    
    If IsMissing(vGridColumnTypeOption2) Then
        vGridColumnTypeOption2 = Null
    End If
    
    'set the properties passed into the method
    
    With objNewMember
        .ObjColumnName = ObjectColumnName
        .TableName = TableName
        .ColumnName = ColumnName
        .Caption = Caption
        .IsSelect = IsSelect
        .SQLDataType = SQLDataType
        .GridColumnWidth = GridColumnWidth
        .GridCellType = GridCellType
        .GridColumnTypeOption1 = vGridColumnTypeOption1
        .GridColumnTypeOption2 = vGridColumnTypeOption2
    End With
    
    If Len(sKey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, sKey
    End If


    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As clsGridCol
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property



Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)


    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property



Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

