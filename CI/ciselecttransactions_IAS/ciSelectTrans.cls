VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCISelectTrans"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'************************************************************************************
'     Name: clsTemplateMNT
'     Desc: The clsTemplateMNT class handles the user interface for
'           maintenance of the Template table.
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 05/03/95 KMC
'     Mods: 07/23/95 KMC
'************************************************************************************
Option Explicit


 Public Event Proceed(ByVal lSettingKey As Long)
 Public Event SelectTransactions(ByVal sWhereClause As String)
 Public Event DeSelectTransactions(ByVal sWhereClause As String)
 Public Event ClearAll()
 
 
 
    Public Enum SelectDialogResults
        CancelDialog = 0
        Proceed = 1
    End Enum
    


    '=======================================================================
    '           Public Variables for use by other modules
    '           -----------------------------------------
    '
    '   moFramework     - required to give other modules the ablitiy to
    '                     call the framework methods and properties, such
    '                     as UnloadSelf, LoadSotaObject, . . .
    '                     Object reference is set in InitializeObject.
    '
     '
    '   moSysSession    - system Manager Session Object which contains
    '                     information specific to the user's session with
    '                     this accounting application.  For example, current
    '                     CompanyID, Business date, UserID, UserName, . . .
    '=======================================================================

    Public moFramework          As Object
    Public moAppDB              As Object
    Public moSysSession         As Object
    Public moDASSession         As Object
    '=======================================================================
    'Private Variables for Class Properties
    '=======================================================================
    Public mlError             As Long
    '=======================================================================
    'Private variables for use within this class only
    '=======================================================================
    Private WithEvents mfrmMain     As frmCISelectTransactions
Attribute mfrmMain.VB_VarHelpID = -1
    
    Private m_ProceedSettingKey     As Long
    Private m_DialogResult          As SelectDialogResults
    Private m_Caption               As String
    
Const VBRIG_MODULE_ID_STRING = "ciSelectTrans.CLS"

Public Property Let Caption(ByVal sNewCaption As String)
    m_Caption = sNewCaption
End Property

Public Property Get ProceedSettingKey() As Long
    ProceedSettingKey = m_ProceedSettingKey
End Property

Public Function ShowMe(Optional ByVal lSettingKey As Long) As SelectDialogResults
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    With mfrmMain
        .Caption = m_Caption
        .ShowMe lSettingKey
        .ZOrder
    
    End With
    
    mfrmMain.Show vbModal
    m_ProceedSettingKey = mfrmMain.ProceedSettingKey
    ShowMe = mfrmMain.SelectDialogResult
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ShowMe", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function



Public Function UnloadMe()
    If Not mfrmMain Is Nothing Then
        On Error Resume Next
        Unload mfrmMain
        Set mfrmMain = Nothing
    End If
    Set moFramework = Nothing
    Set moAppDB = Nothing
    Set moDASSession = Nothing
    Set moSysSession = Nothing
End Function



Public Sub InitSelectTrans(oSelGridColMgr As clsSelectGridColMgr, ByVal lTaskID As Long, ByVal sAppName As String, _
                           sObjectCommandName As String, _
                           Optional EntityType As Integer = 0, _
                           Optional SQLColumnName As String = "", _
                           Optional DMColumnName As String = "")
    
    If mfrmMain Is Nothing Then
        Set mfrmMain = New frmCISelectTransactions
        Set mfrmMain.oClass = Me
        Load mfrmMain
        If mlError Then Err.Raise mlError
    End If
    
    With mfrmMain
        Set .SelectGridColMgr = oSelGridColMgr
        .taskID = lTaskID
        .AppName = sAppName
        .ObjectCommandName = sObjectCommandName
        .BindEntity EntityType, SQLColumnName, DMColumnName
        .SetupFormControls
    End With
End Sub


Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = TypeName(Me)
End Function


Private Sub Class_Initialize()
    m_Caption = "Select Transactions"
End Sub

Private Sub mfrmMain_DeSelectTransactions(ByVal sWhereClause As String)
    RaiseEvent DeSelectTransactions(sWhereClause)
End Sub

Private Sub mfrmMain_Proceed(ByVal lSettingKey As Long)
    RaiseEvent Proceed(lSettingKey)
End Sub

Private Sub mfrmMain_SelectTransactions(ByVal sWhereClause As String)
    RaiseEvent SelectTransactions(sWhereClause)
End Sub
