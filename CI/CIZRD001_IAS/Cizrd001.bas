Attribute VB_Name = "basSalesTaxRPT"
'***********************************************************************
'     Name: basSalesTaxRPT
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: 07/15/96  JSP
'     Mods: xx/xx/xx
'           09/05/04  RBM  Populate sales tax detail in VB instead of SP
'                          because SP temp table may not have a unique
'                          key.
'***********************************************************************
Option Explicit

' Temporary constants until StrConst.bas/MsgConst.bas is updated

'-- Miscellaneous constants/variables
    Public gbIsAP      As Boolean
    Public giRptFormat As Integer
    Public gsType      As String
    Public giModuleNo  As Integer
    Public gbSkipPrintDialog As Boolean '   show print dialog. HBS.

    Public Const kSummaryRpt As Integer = 1
    Public Const kDetailRpt  As Integer = 2
    
    Public Const kPeriodEnd As Long = 50219
    Public Const kPurchases As Long = 50103
    'Public Const kSales  As Long = 50037

Const VBRIG_MODULE_ID_STRING = "CIZRD001.BAS"

Public Function lStartReport(sButton As String, frm As Form, _
                             Optional vPeriodEndFlag As Variant, _
                             Optional vFiscPer As Variant, _
                             Optional vFiscYear As Variant, _
                             Optional vPeriodEndDate As Variant, _
    Optional iFileType As Variant, _
    Optional sFileName As Variant) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim lBadRow As Long
    Dim bPeriodEnd As Boolean
    Dim bValid As Boolean
    Dim sWhereClause As String
    Dim sTablesUsed As String
    Dim iNumTablesUsed As Integer
    Dim sCompanyID As String
    Dim sSQL As String
    Dim sInsert As String
    Dim sPerEndWhere As String
    Dim lSessionID As Long
    Dim oRet As Integer
    Dim lCnt As Long
    Dim dAmt As Double
    Dim iRet As Integer
'    Dim iJob As Integer
    Dim iIndex As Integer
    Dim sSort As String
    Dim oSysSession As Object
    Dim oAppDB As Object
    Dim oSysDB As Object
    Dim oreport As clsReportEngine
    Dim oSelect As clsSelection
    Dim i As Long
    Dim sColumnName As String
    Dim sSortOrder As String
    Dim sFormula As String
    
    On Error GoTo BadExit
    
    lStartReport = kFailure
    
    bPeriodEnd = False
    If Not IsMissing(vPeriodEndFlag) Then
        bPeriodEnd = CBool(vPeriodEndFlag)
    End If
    
    Set oSysSession = frm.oClass.moSysSession
    Set oAppDB = frm.oClass.moAppDB
    Set oSysDB = frm.oClass.moAppDB
    Set oreport = frm.moReport
    Set oSelect = frm.moSelect
       
    ' determine report format
    If frm.optSummDetl(0) = True Then
        giRptFormat = kSummaryRpt   ' Summary
    ElseIf frm.optSummDetl(1) = True Then
        giRptFormat = kDetailRpt   ' Detail
    End If
    
    sCompanyID = frm.msCompanyID
    
    ' if this is a period end report, retrieve the appropriate settings
    If bPeriodEnd Then
        If IsMissing(vFiscPer) Then vFiscPer = ""
        If IsMissing(vFiscYear) Then vFiscYear = ""
        If IsMissing(vPeriodEndDate) Then vPeriodEndDate = ""
        sPerEndWhere = sGetPeriodEnd(oSysSession, oAppDB, CStr(vFiscPer), CStr(vFiscYear), _
            CStr(vPeriodEndDate), giRptFormat, sCompanyID, frm)
    Else
        ShowStatusBusy frm
    End If
    
    ' delete a previous run - delete all records with SessionID in worktables
    oreport.CleanupWorkTables

    ' correct bad user input
    lBadRow = oSelect.lValidateGrid(True)
    
    ' create the where clause - does not have a "WHERE" on the front of sWhereClause
    ' if the user selects "All" for all selections, sWhereClause will be blank
    bValid = oSelect.bGetWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, True)
    
    ' set report options variables
    lSessionID = oreport.SessionID
    If gbIsAP Then
        giModuleNo = kModuleAP
    Else
        giModuleNo = kModuleAR
    End If
    
    If giRptFormat = kSummaryRpt Then ' Summary format
                
        '-- If tciSTaxHist is in the sTablesUsed string, we must link it to tciSTaxCode
        
        oSelect.AppendToWhereClause sWhereClause, "tciSTaxCode.STaxCodeKey = tciSTaxHist.STaxCodeKey"
        
        '-- if tciSTaxCodeCompany is in the sTablesUsed string, we must link it to tciSTaxCode
        If InStr(sTablesUsed, "tciSTaxCodeCompany") <> 0 Then
            oSelect.AppendToWhereClause sWhereClause, "tciSTaxCode.STaxCodeKey = tciSTaxCodeCompany.STaxCodeKey"
        End If
        
        '-- Add the period end restriction
        sWhereClause = sWhereClause & sPerEndWhere
        
        ' look for an invalid or too complex Where clause, prepend "WHERE" if necessary
        If Not oSelect.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, False) Then
            GoTo BadExit
        End If
            
        StripTable sWhereClause, sTablesUsed, "tciSTaxDetail"
        
        If InStr(sTablesUsed, "tciSTaxHist") = 0 Then
            sTablesUsed = sTablesUsed & ", tciSTaxHist WITH (NOLOCK)"
        End If
        
        '-- Add CompanyID on to the end of this Where clause
         oSelect.AddJoinIfNecessary sWhereClause, sTablesUsed, "tciSTaxHist", "tciSTaxHist.CompanyID = " & gsQuoted(gsFormatQuotes(sCompanyID))
                
        '-- Build the Insert statement for #tciSTaxHistWrk
        If gbIsAP Then
            '-- AP version
            sSQL = "SELECT tciSTaxHist.STaxCodeKey, " & lSessionID & ", tciSTaxHist.FiscPer, "
            sSQL = sSQL & "tciSTaxHist.FiscYear, MAX(tciSTaxHist.PostDate), "
            sSQL = sSQL & "SUM(tciSTaxHist.APTaxInvc), SUM(tciSTaxHist.ExmptPurch), "
            sSQL = sSQL & "SUM(tciSTaxHist.NonSubjPurch), SUM(tciSTaxHist.SubjAPFreight), "
            sSQL = sSQL & "SUM(tciSTaxHist.SubjPurch), SUM(tciSTaxHist.SubjAPSalesTax), "
            sSQL = sSQL & "SUM(tciSTaxHist.TaxCollected), SUM(tciSTaxHist.TaxPaid) "
            sSQL = sSQL & "FROM " & sTablesUsed & " " & sWhereClause
            sSQL = sSQL & " GROUP BY tciSTaxCode.STaxCodeID, tciSTaxHist.STaxCodeKey, "
            sSQL = sSQL & "tciSTaxHist.FiscYear, tciSTaxHist.FiscPer"
            
            #If RPTDEBUG Then
                sInsert = "INSERT INTO tciSTaxHistWrk (STaxCodeKey, SessionID, FiscPer, FiscYear, PostDate, "
            #Else
                sInsert = "INSERT INTO #tciSTaxHistWrk (STaxCodeKey, SessionID, FiscPer, FiscYear, PostDate, "
            #End If
            
            sInsert = sInsert & "APTaxInvc, ExmptPurch, NonSubjPurch, SubjAPFreight, SubjPurch, "
            sInsert = sInsert & "SubjAPSalesTax, TaxCollected, TaxPaid) " & sSQL
         Else
            '-- AR version
            sSQL = "SELECT tciSTaxHist.STaxCodeKey, " & lSessionID & ", tciSTaxHist.FiscPer, "
            sSQL = sSQL & "tciSTaxHist.FiscYear, MAX(tciSTaxHist.PostDate), "
            sSQL = sSQL & "SUM(tciSTaxHist.ARTaxInvc), SUM(tciSTaxHist.ExmptSales), "
            sSQL = sSQL & "SUM(tciSTaxHist.NonSubjSales), SUM(tciSTaxHist.SubjARFreight), "
            sSQL = sSQL & "SUM(tciSTaxHist.SubjSales), SUM(tciSTaxHist.SubjARSalesTax), "
            sSQL = sSQL & "SUM(tciSTaxHist.TaxCollected), SUM(tciSTaxHist.TaxPaid), "
            sSQL = sSQL & "SUM(tciSTaxHist.SubjSales + tciSTaxHist.NonSubjSales) "
            sSQL = sSQL & "FROM " & sTablesUsed & " " & sWhereClause
            sSQL = sSQL & " GROUP BY tciSTaxCode.STaxCodeID, tciSTaxHist.STaxCodeKey, "
            sSQL = sSQL & "tciSTaxHist.FiscYear, tciSTaxHist.FiscPer"
            
            #If RPTDEBUG Then
                sInsert = "INSERT INTO tciSTaxHistWrk (STaxCodeKey, SessionID, FiscPer, FiscYear, PostDate, "
            #Else
                sInsert = "INSERT INTO #tciSTaxHistWrk (STaxCodeKey, SessionID, FiscPer, FiscYear, PostDate, "
            #End If
            
            sInsert = sInsert & "ARTaxInvc, ExmptSales, NonSubjSales, SubjARFreight, SubjSales, "
            sInsert = sInsert & "SubjARSalesTax, TaxCollected, TaxPaid, TotalSales) " & sSQL
        End If
        
        '-- Insert keys into #tciSTaxHistWrk from tciSTaxCode
        On Error Resume Next
        oAppDB.ExecuteSQL sInsert
        If Err.Number <> 0 Then
            oreport.ReportError kmsgProc, sInsert
            GoTo BadExit
        End If
        On Error GoTo BadExit
                
        '-- Check whether any records were inserted; if not, go to error handler.
        If IsMissing(iFileType) Then
            #If RPTDEBUG Then
                If Not oSelect.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", lSessionID, True) Then
            #Else
                If Not oSelect.bRecsToPrintExist("#" & Left$(frm.sWorkTableCollection(1), 19), "SessionID", lSessionID, True) Then
            #End If
                    GoTo BadExit
                End If
        End If
        
        '-- Finish building the work tables
        With oAppDB
            .SetInParam gsFormatQuotes(sCompanyID)
            .SetInParam lSessionID
            .SetOutParam oRet
            
            On Error Resume Next
            .ExecuteSP "spciSTaxRptSum"
                    
            If Err.Number <> 0 Then
                oRet = Err.Number
            Else
                oRet = .GetOutParam(3)
            End If
            
            .ReleaseParams
            
            If oRet <> 0 Then
                oreport.ReportError kmsgProc, "spciSTaxRptSum"
                GoTo BadExit
            End If
        End With
        
        On Error GoTo BadExit
        gClearSotaErr
    
        '-- Since the summary table has no flag to differentiate between AP and AR
        '-- records, also need to check the amount fields in the work table. If all
        '-- AP or AR amount fields are 0 (depending on the report), don't display a
        '-- report.
        If gbIsAP Then
            sSQL = "SUM(APTaxInvc + ExmptPurch + NonSubjPurch + SubjAPFreight + "
            sSQL = sSQL & "SubjPurch + SubjAPSalesTax + TaxPaid)"
        Else
            sSQL = "SUM(ARTaxInvc + ExmptSales + NonSubjSales + SubjARFreight + "
            sSQL = sSQL & "SubjSales + SubjARSalesTax + TaxCollected)"
        End If
    
        #If RPTDEBUG Then
            dAmt = oAppDB.Lookup(sSQL, "tciSTaxHistWrk", "SessionID = " & lSessionID)
        #Else
            dAmt = oAppDB.Lookup(sSQL, "#tciSTaxHistWrk", "SessionID = " & lSessionID)
        #End If
        
        If dAmt = 0 Then
            oreport.ReportError kmsgNoReportRecords
            GoTo BadExit
        End If
        
    ElseIf giRptFormat = kDetailRpt Then   ' Detail format

        ' if tciSTaxDetail is in the sTablesUsed string, we must link it to tciSTaxCode
     
        oSelect.AppendToWhereClause sWhereClause, "tciSTaxCode.STaxCodeKey = tciSTaxDetail.STaxCodeKey"
        
        '-- if tciSTaxCodeCompany is in the sTablesUsed string, we must link it to tciSTaxCode
        If InStr(sTablesUsed, "tciSTaxCodeCompany") <> 0 Then
            oSelect.AppendToWhereClause sWhereClause, "tciSTaxCode.STaxCodeKey = tciSTaxCodeCompany.STaxCodeKey"
        End If
         
        ' add the period end restriction
        sWhereClause = sWhereClause & sPerEndWhere
        
        'April 13, 2000 - APK Added
        sWhereClause = sWhereClause & " AND tciSTaxDetail.CompanyID = " & gsQuoted(gsFormatQuotes(sCompanyID))
        
        ' look for an invalid or too complex Where clause, prepend "WHERE" if necessary
        If Not oSelect.bCheckWhereClause(sWhereClause, sTablesUsed, iNumTablesUsed, False) Then
            GoTo BadExit
        End If
            
        ' replace <selection grid> references (ie. post date) from tciSTaxHist with Detail _
          which is now  the driving table, also remove tciSTaxHist from tables used.
        sWhereClause = Replace(sWhereClause, "tciSTaxHist", "tciSTaxDetail")
        sTablesUsed = Replace(sTablesUsed, ",tciSTaxHist WITH (NOLOCK)", "")
        
        If InStr(sTablesUsed, "tciSTaxDetail") = 0 Then
            sTablesUsed = sTablesUsed & ", tciSTaxDetail WITH (NOLOCK)"
        End If
              
        ' build the Insert statement for #tciSTaxDetailWrk
        sSQL = "SELECT tciSTaxCode.STaxCodeKey, " & lSessionID
        sSQL = sSQL & ", tciSTaxDetail.Type, tciSTaxDetail.TranID, " & _
            "tciSTaxDetail.TranDate, tciSTaxDetail.PostDate, " & _
            "ExmptSales, TranAmt - (SubjSales + SubjFreightAmt + SubjSalesTax), " & _
            "STaxAmt, STaxExmptNo, SubjFreightAmt, SubjSales, SubjSalesTax, " & _
            "TotalSales, TranAmt"
        
        sSQL = sSQL & " FROM " & sTablesUsed
        sSQL = sSQL & " " & sWhereClause
        If gbIsAP Then   ' AP Report
            sSQL = sSQL & " AND tciSTaxDetail.Type = 1"
        Else             ' AR Report
            sSQL = sSQL & " AND tciSTaxDetail.Type = 2"
        End If
        
        #If RPTDEBUG Then
            sInsert = "INSERT INTO tciSTaxDetailWrk "
        #Else
            sInsert = "INSERT INTO #tciSTaxDetailWrk "
        #End If
        
        If gbIsAP Then
            '-- For AP vouchers, there are two transaction identifies, TranID and VouchNo,  VouchNo is unique,
            'but TranID is not unique,  tciSTaxDetail table stores the TranID field from the voucher record.  We
            'need to lookup the related VouchNo later, so to avoid confusion, we store the TranID field from voucher
            'to the TranIDAP field in the work table, and later update the TranID field in the work table with VouchNo.
            sInsert = sInsert & "(STaxCodeKey, SessionID, Type, TranIDAP, TranDate, " & _
                "PostDate, ExmptSales, NonSubjAmt, STaxAmt, STaxExmptNo, " & _
                "SubjFreightAmt, SubjSales, SubjSalesTax, TotalSales, TranAmt) " & sSQL
        Else
            sInsert = sInsert & "(STaxCodeKey, SessionID, Type, TranID, TranDate, " & _
               "PostDate, ExmptSales, NonSubjAmt, STaxAmt, STaxExmptNo, " & _
               "SubjFreightAmt, SubjSales, SubjSalesTax, TotalSales, TranAmt) " & sSQL
        End If
        
        '-- insert details into #tciSTaxCodeWrk
        On Error Resume Next
        oAppDB.ExecuteSQL sInsert
        If Err.Number <> 0 Then
            oreport.ReportError kmsgProc, sInsert
            GoTo BadExit
        End If
        On Error GoTo BadExit
        
        '-- Update Voucher No for AP vouchers
        If gbIsAP Then
            sInsert = "UPDATE detl SET TranID = v.VouchNo FROM "
            
            #If RPTDEBUG Then
                sInsert = sInsert & "tciSTaxDetailWrk detl"
            #Else
                sInsert = sInsert & "#tciSTaxDetailWrk detl"
            #End If
            
            sInsert = sInsert & " JOIN tapVoucher v WITH (NOLOCK) ON detl.TranIDAP = v.TranID AND detl.PostDate = v.PostDate AND detl.TranAmt = v.TranAmtHC"
            
            oAppDB.ExecuteSQL sInsert
            If Err.Number <> 0 Then
                oreport.ReportError kmsgProc, sInsert
                GoTo BadExit
            End If
        End If
                
        '-- Check whether any records were inserted; if not, go to error handler.
        If IsMissing(iFileType) Then
            #If RPTDEBUG Then
                If Not oSelect.bRecsToPrintExist(frm.sWorkTableCollection(1), "SessionID", lSessionID, True) Then
            #Else
                If Not oSelect.bRecsToPrintExist("#" & Left$(frm.sWorkTableCollection(1), 19), "SessionID", lSessionID, True) Then
            #End If
                    GoTo BadExit
                End If
        End If
        
        ' finish building the work tables
        With oAppDB
            .SetInParam gsFormatQuotes(sCompanyID)
            .SetInParam lSessionID
            .SetOutParam oRet
            
            On Error Resume Next
            'SGS DEJ 9/28/11 START
'            .ExecuteSP "spciSTaxRptDetl"
            If gbIsAP = False Then
                'This is to add the Customer Key, Name, and ID to the table
                .ExecuteSP "spciSTaxRptDetl_SGS"
            Else
            .ExecuteSP "spciSTaxRptDetl"
            End If
            'SGS DEJ 9/28/11 STOP
                    
            If Err.Number <> 0 Then
                oRet = Err.Number
            Else
                oRet = .GetOutParam(3)
            End If
            
            .ReleaseParams
            
            If oRet <> 0 Then
'SGS DEJ 9/28/11 START
'                oreport.ReportError kmsgProc, "spciSTaxRptDetl"
                oreport.ReportError kmsgProc, "spciSTaxRptDetl_SGS"
'SGS DEJ 9/28/11 STOP
                GoTo BadExit
            End If
        End With
        
        On Error GoTo BadExit
        gClearSotaErr
    End If

    ' start Crystal print engine, open print job, get strings for report
    If (oreport.lSetupReport = kFailure) Then GoTo BadExit
    
    ' work around if you print without previewing first.
    ' Crystal does not provide a way of getting page orientation
    ' used to create report. use VB constants: vbPRORPortrait, vbPRORLandscape
    oreport.Orientation = vbPRORPortrait
    
    ' there are two report title fields in the report template.
    ' the strings themselves should come out of the string table
    oreport.ReportTitle1 = gsBuildString(kRptSalesTax, oSysDB, oSysSession) & _
        " " & gsBuildString(kReportTbl, oSysDB, oSysSession)
    
    If giRptFormat = kSummaryRpt Then
        oreport.ReportTitle2 = gsType & " " & gsBuildString(kARZJA001Summary, oSysDB, oSysSession)
    ElseIf giRptFormat = kDetailRpt Then
        oreport.ReportTitle2 = gsType & " " & gsBuildString(kARZJA001Detail, oSysDB, oSysSession)
    End If
    
    ' automated means of labeling header and subtotal fields.
    oreport.UseHeaderCaptions = 1
    oreport.UseSubTotalCaptions = 1
    
     'set standard formulas, business date, run time, company name etc.
    If (oreport.lSetStandardFormulas(frm) = kFailure) Then GoTo BadExit

    For i = 1 To frm.moSort.MaxRows
        sColumnName = gsGridReadCellText(frm.grdSort, i, 1)
        sSortOrder = gsGridReadCellText(frm.grdSort, i, 2)
        If (sColumnName = "Sales Tax Code") And (sSortOrder = "Descending") Then
            oreport.bModifyGroupSort "GH6", crDescendingOrder
'            ReportObj.lDeleteGroup (1)
        End If
    Next
    ' translate sort selections into Crystal commands
    If (oreport.lSetSortCriteria(frm.moSort) = kFailure) Then GoTo BadExit
         
      
    oreport.BuildSQL
    
'------------------------- specific to this report --------------------------
'----------------------------------------------------------------------------
    
    ' set SQL statement according to user selections in sort grid
    oreport.SetSQL
        
    ' set default labels on the report
    oreport.SetReportCaptions
    
    'used in the Summary section on the report
    oreport.SelectString = oSelect.sGetUserReadableWhereClause(100)
    
    ' display the summary section of the report
    If (oreport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 3) = kFailure) Then
        GoTo BadExit
    End If
        
    ' add the SessionID restriction
    If giRptFormat = kSummaryRpt Then   ' Summary report
        If (oreport.lRestrictBy("{tciSTaxHistWrk.SessionID} = " & lSessionID) = kFailure) Then
            GoTo BadExit
        End If
    ElseIf giRptFormat = kDetailRpt Then   ' Detail report
        If (oreport.lRestrictBy("{tciSTaxDetailWrk.SessionID} = " & lSessionID) = kFailure) Then
            GoTo BadExit
        End If
    End If
    
    ' set report-specific formulas
    If iSetNonStandardFormulas(frm, oreport, giModuleNo) = kFailure Then GoTo BadExit
    'Debug.Print oReport.GetSQLQuery
    
    If bPeriodEnd Then
        gbSkipPrintDialog = True    '   no print dialog on Period End Processing. HBS.
    End If
    
    oreport.ProcessReport frm, sButton, iFileType, sFileName, gbSkipPrintDialog
    gbSkipPrintDialog = True    '   show print dialog one time only. HBS.
    
    If Not bPeriodEnd Then frm.sbrMain.Status = SOTA_SB_START
    
    Set oSysSession = Nothing
    Set oAppDB = Nothing
    Set oSysDB = Nothing
    Set oreport = Nothing
    Set oSelect = Nothing

    lStartReport = kSuccess
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

BadExit:
    oreport.CleanupWorkTables
    If Not bPeriodEnd Then ShowStatusNone frm
    Set oSysSession = Nothing
    Set oAppDB = Nothing
    Set oSysDB = Nothing
    Set oreport = Nothing
    Set oSelect = Nothing
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function iSetNonStandardFormulas(frm As Form, oreport As clsReportEngine, _
                                        iModuleNo As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iRet As Integer
    Dim sFormula As String
    Dim oSysSession As Object
    Dim oSysDB As Object
    
    Const COLONCON = ":"
    Const QUOTECON = """"

    iSetNonStandardFormulas = kSuccess
    
    Set oSysSession = frm.oClass.moSysSession
    Set oSysDB = frm.oClass.moAppDB
        
    sFormula = gvCheckNull(oSysDB.Lookup("ModuleDefName", "tsmModuleStrDef", "ModuleNo = " & iModuleNo))
    If Not oreport.bSetReportFormula("ModuleModified", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
    
    sFormula = gsBuildString(kPageIDCol, oSysDB, oSysSession)
    If Not oreport.bSetReportFormula("PageLbl", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit

    sFormula = gsBuildString(kARZRG001ReportTotalCap, oSysDB, oSysSession)
    If Not oreport.bSetReportFormula("ReportTotals", QUOTECON & sFormula & ":" & QUOTECON) Then GoTo BadExit

   ' formulas specific to Detail report
   If giRptFormat = kDetailRpt Then
       If giModuleNo = kModuleAP Then
           sFormula = gsBuildString(kNonSubject, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("NonSubjlbl", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
           
           sFormula = gsBuildString(kAmount, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("Amountlbl", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
           
           ' Change the Report labels since the default value of the labels are from temp table column name which
           ' are Sales or AR.
           If Not oreport.bSetReportFormula("lbl2SubjSales", QUOTECON & "Purchase" & QUOTECON) Then GoTo BadExit
                
           If Not oreport.bSetReportFormula("lbl2SubjFreightAmt", QUOTECON & "AP Freight" & QUOTECON) Then GoTo BadExit
        
           If Not oreport.bSetReportFormula("lbl2SubjSalesTax", QUOTECON & "AP Sales Tax" & QUOTECON) Then GoTo BadExit
              
           If Not oreport.bSetReportFormula("lbl2ExmptSales", QUOTECON & "Purchase" & QUOTECON) Then GoTo BadExit
    
           sFormula = gsBuildString(kVouchNo, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("lblVouchNo", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit

           ' Change the Report Totals Label to read "Total Sales Tax" for AP Detail report only
           sFormula = gsBuildString(kSalesTaxTotal, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("ReportTotals", QUOTECON & sFormula & ":" & QUOTECON) Then GoTo BadExit

           sFormula = gsBuildString(kSalesTaxCode, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("SortBySTax", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
        Else 'AR
           sFormula = gsBuildString(klblTaxable, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("lbl1SubjSales", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
       
           sFormula = gsBuildString(klblTaxable, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("lbl1SubjFreightAmt", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
       
           sFormula = gsBuildString(klblTaxable, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("lbl1SubjSalesTax", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
         
           sFormula = gsBuildString(kseedSTaxClsNonTax, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("NonSubjLbl", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
           sFormula = gsBuildString(kLblInvoice, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("lbl1STaxAmt", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
           sFormula = gsBuildString(kRptSalesTax, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("lbl2STaxAmt", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
       
           sFormula = gsBuildString(kAmount, oSysDB, oSysSession)
           If Not oreport.bSetReportFormula("Amountlbl", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        End If
     ElseIf giRptFormat = kSummaryRpt And giModuleNo = kModuleAR Then
    ' Summary report formulas - AR columns only
       sFormula = gsBuildString(klblTaxable, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl1SubjSales", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
               
       sFormula = gsBuildString(klblTaxable, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl1SubjARFreight", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
       
       sFormula = gsBuildString(kRptFreight, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl2SubjARFreight", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
       sFormula = gsBuildString(klblTaxable, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl1SubjARSalesTax", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
             
       sFormula = gsBuildString(kRptSalesTax, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl2SubjARSalesTax", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
       sFormula = gsBuildString(kseedSTaxClsNonTax, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl1NonSubjSales", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
       sFormula = gsBuildString(klblSales, oSysDB, oSysSession)
       If Not oreport.bSetReportFormula("lbl2NonSubjSales", QUOTECON & sFormula & QUOTECON) Then GoTo BadExit
        
    End If
    
    Set oSysSession = Nothing
    Set oSysDB = Nothing
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
BadExit:
    iSetNonStandardFormulas = kFailure
    Set oSysSession = Nothing
    Set oSysDB = Nothing

'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iSetNonStandardFormulas", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("cizrd001.clsSalesTaxRPT")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSalesTaxRPT"
End Function

Private Function sGetPeriodEnd(oSysSession As Object, oAppDB As Object, _
                               sFiscPer As String, sFiscYear As String, _
                               sPeriodEndDate As String, iRptFormat As Integer, _
                               sCompanyID As String, frm As Form) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sQuery As String
    Dim sWhereClause As String
    Dim rs As DASRecordSet
    Dim vCurFiscPer As Variant
    Dim vCurFiscYear As Variant
    Dim sStartDate As String, sEndDate As String
    
    If iRptFormat = 1 Then
        If (Trim$(sFiscPer) = "") Or (Trim$(sFiscYear) = "") Then
            If gbIsAP Then
                ' get current AP period
                sQuery = "#apGetOptions;" & "CurntFiscPer, CurntFiscYear;"
                sQuery = sQuery & kSQuote & gsFormatQuotes(sCompanyID) & kSQuote & ";"
            Else
                ' get current AR period
                sQuery = "#arGetOptionsAllCols;" & kSQuote & gsFormatQuotes(sCompanyID) & kSQuote & ";"
            End If
                                    
            Set rs = oAppDB.OpenRecordset(sQuery, kSnapshot, kOptionNone)
            If Not rs.IsEmpty Then
                vCurFiscPer = gvCheckNull(rs.Field("CurntFiscPer"))
                vCurFiscYear = gvCheckNull(rs.Field("CurntFiscYear"))
            End If
            
            If (CStr(vCurFiscPer) <> "") And (CStr(vCurFiscYear) <> "") Then
                sGetPeriodEnd = " AND tciSTaxHist.FiscYear = " & kSQuote & CStr(vCurFiscYear) & kSQuote
                sGetPeriodEnd = sGetPeriodEnd & " AND tciSTaxHist.FiscPer = " & CStr(vCurFiscPer)
            Else
                sGetPeriodEnd = ""
            End If
        Else
            sGetPeriodEnd = " AND tciSTaxHist.FiscYear = " & kSQuote & sFiscYear & kSQuote
            sGetPeriodEnd = sGetPeriodEnd & " AND tciSTaxHist.FiscPer = " & sFiscPer
        End If
    Else
        If (Trim$(sFiscPer) = "") Or (Trim$(sFiscYear) = "") Then
            If gbIsAP Then
                ' get current AP period
                sQuery = "#apGetOptions;" & "CurntFiscPer, CurntFiscYear;"
                sQuery = sQuery & kSQuote & gsFormatQuotes(sCompanyID) & kSQuote & ";"
            Else
                ' get current AR period
                sQuery = "#arGetOptionsAllCols;" & kSQuote & gsFormatQuotes(sCompanyID) & kSQuote & ";"
            End If
                                    
            Set rs = oAppDB.OpenRecordset(sQuery, kSnapshot, kOptionNone)
            If Not rs.IsEmpty Then
                sFiscPer = gvCheckNull(rs.Field("CurntFiscPer"))
                sFiscYear = gvCheckNull(rs.Field("CurntFiscYear"))
            End If
        End If
                
        ' get the current period start and end date
        sWhereClause = "WHERE CompanyID = " & kSQuote & sCompanyID & kSQuote
        sWhereClause = sWhereClause & " AND FiscYear = " & kSQuote & sFiscYear & kSQuote
        sWhereClause = sWhereClause & " AND FiscPer = " & sFiscPer
        sQuery = "SELECT StartDate, EndDate " & "FROM tglFiscalPeriod " & sWhereClause
                
        Set rs = oAppDB.OpenRecordset(sQuery, kSnapshot, kOptionNone)
        If Not rs.IsEmpty Then
            sStartDate = CStr(gvCheckNull(rs.Field("StartDate")))
            sEndDate = CStr(gvCheckNull(rs.Field("EndDate")))
                
            sGetPeriodEnd = " AND tciSTaxDetail.TranDate BETWEEN " & kSQuote & sStartDate & kSQuote
            sGetPeriodEnd = sGetPeriodEnd & " AND " & kSQuote & sEndDate & kSQuote
        Else
            sGetPeriodEnd = ""
        End If
    End If

    If Not rs Is Nothing Then
        rs.Close: Set rs = Nothing
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetPeriodEnd", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub StripTable(ByRef sWhereClause As String, ByRef sTablesUsed As String, _
                       sWhichTbl As String)
'************************************************************************
'   Desc: Because of the ridiculous weirdness of this report having to
'         use different tables for Summary and Detail reports, and all
'         the behind the scenes work I have to do to hide the appropriate
'         entries from the Sort and Selection grids, it is possible that
'         the wrong table could be included in the where clause or the
'         tables used list. This routine parses the bad table out of the
'         strings.
'************************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim iPos As Integer
    Dim iAndPos As Integer, iComPos As Integer
    Dim iBtwPos As Integer
    Dim i As Integer
    Dim sTempTbl As String
    Dim sTempWhere As String
    Dim iTblLen As Integer
    
    ' strip the other table out of the tables list
    sTempTbl = sTablesUsed
    
    iPos = InStr(sTempTbl, sWhichTbl)
    If iPos > 0 Then
        iComPos = InStr(iPos + 1, sTempTbl, ",")
        If iComPos > 0 Then
            sTempTbl = Left$(sTempTbl, iPos - 1) & Mid$(sTempTbl, iComPos + 1)
        Else
            sTempTbl = Left$(sTempTbl, iPos - 1)
        End If
    End If
    
    ' strip the ending comma, if there
    If Right$(sTempTbl, 1) = "," Then
        sTempTbl = Left$(sTempTbl, Len(sTempTbl) - 1)
    End If
    
    sTablesUsed = sTempTbl
    
    ' strip the other table out of the where clause
    sTempWhere = sWhereClause
    
    iTblLen = Len(sWhichTbl)
    
    iPos = InStr(sTempWhere, sWhichTbl)
    
    Do While iPos > 0
        sTempWhere = Left$(sTempWhere, iPos - 1) & Mid$(sTempWhere, iPos + iTblLen + 1)
        iPos = InStr(sTempWhere, sWhichTbl)
    Loop
    
    sWhereClause = sTempWhere

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StripTable", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub


