VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "NEWSOTA.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "SOTASbar.ocx"
Object = "{9504980C-B928-4BF5-A5D0-13E1F649AECB}#35.0#0"; "SOTAVM.OCX"
Object = "{2A076741-D7C1-44B1-A4CB-E9307B154D7C}#157.0#0"; "ENTRYLOOKUPCONTROLS.OCX"
Begin VB.Form frmAllowBOLPrinting 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Allow BOL Printing"
   ClientHeight    =   2250
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   4635
   HelpContextID   =   17770001
   Icon            =   "frmAllowBOLPrinting.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2250
   ScaleWidth      =   4635
   Begin VB.CheckBox chBOLCanPrint 
      Caption         =   "Allow BOL To Print"
      Height          =   255
      Left            =   360
      TabIndex        =   13
      ToolTipText     =   "Allow the BOL to print on Scale Pass even though the COA is required and is not ready"
      Top             =   1560
      Width           =   2415
   End
   Begin EntryLookupControls.TextLookup lkuContract 
      Height          =   285
      Left            =   360
      TabIndex        =   12
      Top             =   720
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   503
      ForeColor       =   -2147483640
      LookupID        =   "SalesOrder"
      Datatype        =   0
      sSQLReturnCols  =   "TranNo,lkuContract,;TranNoRelChngOrd,,;TranNoRel,,;"
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   5
      Top             =   4320
      Visible         =   0   'False
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   4
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -10000
      Style           =   2  'Dropdown List
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      Width           =   1245
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   10
      Top             =   645
      Visible         =   0   'False
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   1860
      Visible         =   0   'False
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   688
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   0
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   741
      Style           =   6
   End
   Begin SOTAVM.SOTAValidationMgr valMgr 
      Left            =   -10000
      Top             =   180
      _ExtentX        =   741
      _ExtentY        =   661
      CtlsCount       =   0
   End
   Begin VB.Label Label2 
      Caption         =   "BSO/Contract Number"
      Height          =   255
      Left            =   360
      TabIndex        =   15
      Top             =   480
      Width           =   2175
   End
   Begin VB.Label lblCustName 
      Height          =   255
      Left            =   360
      TabIndex        =   14
      Top             =   1080
      Width           =   3855
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmAllowBOLPrinting"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private moClass             As Object

Private mlRunMode           As Long

Private mbSaved             As Boolean

Private mbCancelShutDown    As Boolean

Private miSecurityLevel     As Integer

Public moSotaObjects        As New Collection

Private mbEnterAsTab        As Boolean

Private msCompanyID         As String

Private moContextMenu       As New clsContextMenu

Private miOldFormHeight     As Long

Private miOldFormWidth      As Long

Private miMinFormHeight     As Long

Private miMinFormWidth      As Long

Private mbLoadSuccess       As Boolean

Public mlLanguage As Long

Private msSOKey As Long


Private Sub Form_Initialize()
    mbCancelShutDown = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

End Sub

Private Sub Form_Load()
    
    mbLoadSuccess = False

    msCompanyID = moClass.moSysSession.CompanyId
    mbEnterAsTab = moClass.moSysSession.EnterAsTab

    BindToolbar
    Set sbrMain.FrameWork = moClass.moFramework


    ' set form size variables
    miOldFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormHeight = miOldFormHeight
    miMinFormWidth = miOldFormWidth
    
    SetupLookups
    
    

    mbLoadSuccess = True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim iConfirmUnload  As Integer


   'Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    If moClass.mlError = 0 Then
    
        'Check all other forms  that may have been loaded from this main form.
        'If there are any visible forms, then this means the form is active.
        'Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
            'Do Nothing.
            'If the unload is caused by form code, then the form
            'code should also have the miShutDownRequester set correctly.
            
            Case Else
           'Most likely the user has requested to shut down the form.
            'If the context is Add-on-the-fly, hide the form
            'and cancel the unload.
            'If the context is Normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                        
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                        
                End Select
                
        End Select
        
    End If
    
    
    'If execution gets to this point, the form and class object of the form
    'will be shut down.  Perform all operations necessary for a clean shutdown.
    
    PerformCleanShutDown
    
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
            'Do nothing
            
    End Select
    
Exit Sub

CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    
End Sub

Public Sub PerformCleanShutDown()
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next

    'Unload all forms loaded from this main form
    gUnloadChildForms Me

    'remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    TerminateControls Me

    ' perform proper cleanup
    Set moContextMenu = Nothing         ' context menu class
    Set moSotaObjects = Nothing         ' SOTA Child objects collection

Exit Sub

ExpectedErrorRoutine:

End Sub

Private Sub Form_Unload(Cancel As Integer)

    Set moClass = Nothing
     
End Sub

Public Sub HandleToolBarClick(sKey As String)

    Select Case sKey
        
      'Save changes and close the form
        Case kTbSave, kTbFinish, kTbFinishExit
            SaveBSOPrintStatus
            ClearForm
            
      'Cancel changes and close the form
        Case kTbCancel, kTbCancelExit
            ClearForm
                        
      'Display form level help
        Case kTbHelp
            gDisplayFormLevelHelp Me
            
      'Example code to save changes and display report form
        Case kTbPrint
            
        
        Case Else
                
    End Select
    
End Sub

Private Sub lkuContract_Validate(Cancel As Boolean)
    
    Dim lSOKey As Long
    
    If Trim(lkuContract.Text) = Empty Then
        ClearForm
        Exit Sub
    End If
    
    lSOKey = glGetValidLong(moClass.moAppDB.Lookup("SOKey", "tsoSalesOrder", "CompanyID = " & gsQuoted(msCompanyID) & " And TranType = 802 and TranNo = " & gsQuoted(Trim(lkuContract.Text))))
    
    If lSOKey <= 0 Then
        Cancel = True
        MsgBox "Not a valid BSO/Contract Number.", vbExclamation, "MAS 500"
    Else
        msSOKey = lSOKey
        'Get BSO Data
        GetContractData
        lkuContract.Protected = True

    End If
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
    
   HandleToolBarClick Button
    
End Sub

Private Sub BindToolbar()
    
    'Initialize toolbar.
    tbrMain.Init sotaTB_NO_STYLE, mlLanguage
    With moClass.moFramework
        tbrMain.SecurityLevel = .GetTaskPermission(.GetTaskID())
    End With
    tbrMain.AddButton kTbSave ',  4
    tbrMain.AddButton kTbCancel ', 4
    
    tbrMain.RemoveButton kTbFinishExit
    tbrMain.RemoveButton kTbCancelExit
    tbrMain.RemoveButton kTbHelp
    tbrMain.RemoveButton kTbPrint


End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = Me.Name
End Function

Public Property Get FormHelpPrefix() As String
    FormHelpPrefix = "SOZ"   ' Place your help prefix here
End Property

Public Property Get WhatHelpPrefix() As String
    WhatHelpPrefix = "SOZ"    ' Put your identifier here
End Property

Public Property Get oClass() As Object
    Set oClass = moClass
End Property
Public Property Set oClass(oNewClass As Object)
    Set moClass = oNewClass
End Property

Public Property Get lRunMode() As Long
    lRunMode = mlRunMode
End Property
Public Property Let lRunMode(lNewRunMode As Long)
    mlRunMode = lNewRunMode
End Property

Public Property Get bSaved() As Boolean
    bSaved = mbSaved
End Property
Public Property Let bSaved(bNewSaved As Boolean)
    mbSaved = bNewSaved
End Property

Public Property Get bCancelShutDown()
    bCancelShutDown = mbCancelShutDown
End Property

Public Property Get bLoadSuccess() As Boolean
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Public Property Get UseHTMLHelp() As Boolean
    UseHTMLHelp = True   ' Form uses HTML Help (True/False)
End Property

Private Sub SetupLookups()
    With lkuContract
        Set .FrameWork = moClass.moFramework
        Set .AppDatabase = moClass.moAppDB
        .RestrictClause = "CompanyID = " & gsQuoted(msCompanyID) & " And TranType = 802 "
    End With
End Sub

Sub GetContractData()
        
    If msSOKey <= 0 Then Exit Sub
    
    lblCustName.Caption = gsGetValidStr(moClass.moAppDB.Lookup("CustName", "vluRawSOContracts_SGS", "sokey = " & msSOKey))
    chBOLCanPrint.Value = glGetValidLong(moClass.moAppDB.Lookup("CanPrintBOL", "vluRawSOContracts_SGS", "sokey = " & msSOKey))
    
End Sub

Sub ClearForm()
    
    msSOKey = 0
    lkuContract.Text = Empty
    lblCustName.Caption = Empty
    chBOLCanPrint.Value = 0
    
    lkuContract.Protected = False

End Sub


Sub SaveBSOPrintStatus()

    Dim sSQL As String
    
    If msSOKey <= 0 Then Exit Sub
    
    sSQL = "Update tsoSalesOrderExt_SGS Set CanPrintBOL = " & chBOLCanPrint.Value & " Where SOKey = " & msSOKey
    
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub

