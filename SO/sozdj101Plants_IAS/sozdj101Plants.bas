Attribute VB_Name = "basAllowBOLPrinting"
Option Explicit


Const VBRIG_MODULE_ID_STRING = "sozdj101Plants.BAS"



Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozdj101Plants.clsAllowBOLPrinting")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basAllowBOLPrinting"
End Function
