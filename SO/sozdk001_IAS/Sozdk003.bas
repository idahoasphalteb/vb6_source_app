Attribute VB_Name = "basCustReturns"
Option Explicit

'**********************************************************************
'     Name: basCustReturns
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 09-03-2001
'     Mods: mm/dd/yy XXX
'**********************************************************************
Public mfrmMain                 As Form
Public mbEnterAsTab             As Boolean

'Run modes for this project, since it has many versatile uses.
Public Enum lMyRunModes
    kEnterCustReturns = 1
    kViewCustReturns = 2
End Enum

'tarCustomer | Status
Public Const kvCustomerStatusActive As Integer = 1
Public Const kvCustomerStatusInactive As Integer = 2
Public Const kvCustomerStatusTemporary As Integer = 3
Public Const kvCustomerStatusDeleted As Integer = 4

'timItem | Status
Public Const kvItemStatusActive As Integer = 1
Public Const kvItemStatusInactive As Integer = 2
Public Const kvItemStatusDiscontinued As Integer = 3
Public Const kvItemStatusDeleted As Integer = 4

'tsoShipmentLog | TranStatus
Public Const kvShipLogStatusIncomplete As Integer = 1
Public Const kvShipLogStatusPending As Integer = 2
Public Const kvShipLogStatusPosted As Integer = 3
Public Const kvShipLogStatusPurged As Integer = 4
Public Const kvShipLogStatusVoid As Integer = 5
Public Const kvShipLogStatusCommitted As Integer = 6

'timInvtTranLog | TranStatus
Public Const kvInvtTranLogStatusIncomplete As Integer = 1
Public Const kvInvtTranLogStatusPending As Integer = 2
Public Const kvInvtTranLogStatusPosted As Integer = 3
Public Const kvInvtTranLogStatusPurged As Integer = 4
Public Const kvInvtTranLogStatusVoid As Integer = 5

'tsoRMA | Status
Public Const kvRMAStatusOpen As Integer = 1
Public Const kvRMAStatusClosed As Integer = 2

'tarSalesperson | Status
Public Const kvSalespersonStatusActive As Integer = 1
Public Const kvSalespersonStatusInactive As Integer = 2
Public Const kvSalespersonStatusDeleted As Integer = 3

'tsoCustRtrnShipment | CreateType
Public Const kvCreateTypeOther As Integer = 0
Public Const kvCreateTypeStandard As Integer = 1
Public Const kvCreateTypeImport As Integer = 2
Public Const kvCreateTypeSeed As Integer = 3
Public Const kvCreateTypeUpgrade As Integer = 4
Public Const kvCreateTypeImportPending As Integer = 5

'tsoCustRtrnShipLine | ReturnType
Public Const kvReturnTypeNotAReturn As Integer = 0
Public Const kvReturnTypeCredit As Integer = 1
Public Const kvReturnTypeReplacement As Integer = 2

'tciTranType | TranType
Public Const kvTranTypeReturnGoods As Integer = 811

'timWarehouse | TrackQtyAtBin
Public Const kvWhseDoesNotTrackQtyAtBin As Integer = 0
Public Const kvWhseDoesTrackQtyAtBin As Integer = 1

'timWarehouse | UseBins
Public Const kvWhseDoesNotUseBins As Integer = 0
Public Const kvWhseDoesUseBins As Integer = 1

'Miscellaneous Constants
Public Const kiOn = 1
Public Const kiOff = 0

Public Const kDistBin_LkuRetCol_BinID As Integer = 1
Public Const kDistBin_LkuRetCol_BinKey As Integer = 2
Public Const kDistBin_LkuRetCol_QtyOnHand As Integer = 3
Public Const kDistBin_LkuRetCol_AvailQty As Integer = 4
Public Const kDistBin_LkuRetCol_UOMKey As Integer = 5

Public Const kDistBinLot_LkuRetCol_BinID As Integer = 1
Public Const kDistBinLot_LkuRetCol_BinKey As Integer = 2
Public Const kDistBinLot_LkuRetCol_LotNo As Integer = 3
Public Const kDistBinLot_LkuRetCol_LotKey As Integer = 4
Public Const kDistBinLot_LkuRetCol_ExpDate As Integer = 5
Public Const kDistBinLot_LkuRetCol_QtyOnHand As Integer = 6
Public Const kDistBinLot_LkuRetCol_AvailQty As Integer = 7
Public Const kDistBinLot_LkuRetCol_UOMKey As Integer = 8

Public Const kDistBinSerial_LkuRetCol_BinID As Integer = 1
Public Const kDistBinSerial_LkuRetCol_BinKey As Integer = 2
Public Const kDistBinSerial_LkuRetCol_QtyOnHand As Integer = 3
Public Const kDistBinSerial_LkuRetCol_AvailQty As Integer = 4
Public Const kDistBinserial_LkuRetCol_UOMKey As Integer = 5

Public Const kDistBinSerialLot_LkuRetCol_BinID As Integer = 1
Public Const kDistBinSerialLot_LkuRetCol_BinKey As Integer = 2
Public Const kDistBinSerialLot_LkuRetCol_LotNo As Integer = 3
Public Const kDistBinSerialLot_LkuRetCol_LotKey As Integer = 4
Public Const kDistBinSerialLot_LkuRetCol_ExpDate As Integer = 5
Public Const kDistBinSerialLot_LkuRetCol_QtyOnHand As Integer = 6
Public Const kDistBinSerialLot_LkuRetCol_AvailQty As Integer = 7
Public Const kDistBinserialLot_LkuRetCol_UOMKey = 8

'Report
Public gbSkipPrintDialog As Boolean     '   Show print dialog
Public gbCancelPrint     As Boolean
Const VBRIG_MODULE_ID_STRING = "SOZDK003.BAS"

Public Sub MyErrMsg(oClass As Object, sDesc As String, lErr As Long, sSub As String, sProc As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sText As String

    If lErr = guSotaErr.Number And Trim$(guSotaErr.Description) <> Trim$(sDesc) Then
        sDesc = sDesc & Space$(1) & guSotaErr.Description
    End If

    If oClass Is Nothing Then
        sText = sText & " AppName:  " & App.Title
        sText = sText & " Error < " & Format$(lErr)
        sText = sText & " > occurred at < " & sSub
        sText = sText & " > in Procedure < " & sProc
        sText = sText & " > " & sDesc
    Else
        sText = "Module: " & oClass.moSysSession.MenuModule
        sText = sText & " Company: " & oClass.moSysSession.CompanyId
        sText = sText & " AppName:  " & App.Title
        sText = sText & " Error < " & Format$(lErr)
        sText = sText & " > occurred at < " & sSub
        sText = sText & " > in Procedure < " & sProc
        sText = sText & " > " & sDesc
    End If
    
    MsgBox sText, vbExclamation, "Sage 500 ERP"

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyErrMsg", VBRIG_IS_MODULE                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub Main()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("Sozdkdl3.clsEnterCustReturns")
        StartAppInStandaloneMode oApp, Command$()
    End If

    'Exit this subroutine
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basCustReturns"
End Function

Public Function lStartErrorReport(sButton As String, frm As Form, lSessionID As Long, _
                        bSkipPrintDialog As Boolean, bCancelPrint As Boolean) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++

Dim iRetVal As Integer
Dim sTitle1 As String, sTitle2 As String
Dim iSeverity As Integer
Dim sRestriction As String

    lStartErrorReport = kFailure

    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    frm.moReport.ReportFileName() = "sozdv003.rpt"
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
    
    'there are two report title fields in the report template.
    'the strings themselves should come out of the string table
    
    frm.moReport.ReportTitle1() = "Customer Returns"
'    frm.moReport.ReportTitle2() = gsBuildString(kSOSRRptErrorTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
    
    'automated means of labeling subtotal fields.
    'this is not manditory if you choose not to use this feature
    frm.moReport.UseSubTotalCaptions() = 0
    frm.moReport.UseHeaderCaptions() = 0
    
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If
        
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    
    frm.moReport.SetReportCaptions

    If frm.miSeverity > 0 Then
        sRestriction = "{tciErrorLog.SessionID} = " & lSessionID & " AND {tciErrorLog.Severity} = " & frm.miSeverity
    Else
        sRestriction = "{tciErrorLog.SessionID} = " & lSessionID
    End If

    If (frm.moReport.lRestrictBy(sRestriction) = kFailure) Then
        MsgBox "RestrictBy failed"
        GoTo badexit
    End If

    frm.moReport.ProcessReport frm, sButton, , , bSkipPrintDialog, bCancelPrint   '   Added parameters. HBS.

    
    lStartErrorReport = kSuccess

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lStartErrorReport", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

