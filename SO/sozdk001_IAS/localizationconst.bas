Attribute VB_Name = "basLocalizationConst"
Option Explicit

'
'*********************************************************************************
' This file contains the String and Message constant definitions for this project
' based upon the LocalString and LocalMessage tables.
'
' Created On: 1/8/2004 6:13:53 AM
'*********************************************************************************
'
' Strings
'
Public Const kBatch = 6413                                  ' Batch
Public Const kDescri = 165071                               ' Description
Public Const kDetail = 6471                                 ' Detail
Public Const kEditMode = 18                             ' Edit
Public Const kGLJournal = 130311                            ' GL Journal
Public Const kIMColComponent = 165528                       ' Component
Public Const kIMQtyPerKit = 166207                          ' Qty/Kit
Public Const kIMTotalQty = 166267                           ' Total Qty
Public Const kIMUOM = 165296                                ' UOM
Public Const kItem = 6472                                   ' Item
Public Const kItemDescription = 160044                      ' Item Description
Public Const kPosted = 6555                                 ' Posted
Public Const kReason = 6410                                 ' Reason
Public Const ksAcctRecv = 151290                            ' Accounts Receivable
Public Const ksIMRenumber = 166241                          ' Renumber
Public Const ksLine = 220063                                ' Line
Public Const kSOComponents = 250657                         ' Kit Components
Public Const kSOCustomerReturn = 250681                     ' Customer Return
Public Const kSOCustReturnTotals = 250701                   ' Customer Return Totals
Public Const kSOEntCustRT = 250610                          ' Enter Customer Returns
Public Const kSOLotSerialBin = 250478                       ' Lot/Serial/Bin
Public Const kSOMaintainItem = 250660                       ' Maintain Item
Public Const kSOPutAwayBin = 250689                         ' Put Away Bin
Public Const kSOReserved = 250686                           ' Reserved
Public Const kSOReturnQty = 250687                          ' Return Qty
Public Const kSORplc = 250684                               ' Rplc
Public Const kSOSalesOrder = 250016                         ' Sales Order
Public Const kSOShipment = 250234                           ' Shipment
Public Const kSOSRegSalesOrder = 250333                     ' Sales Order
Public Const kSotaTitle = 1100                              ' MAS 500
Public Const kSOUsePrefBin = 250688                         ' Use Pref Bin
Public Const kSOViewCustReturns = 250612                    ' View Customer Returns
Public Const kSTaxClass = 6476                              ' Sales Tax Class
Public Const ksTaxSchedule = 100120                         ' Tax Schedule
Public Const ksUOM = 220066                                 ' UOM
Public Const kTranIncomplete = 604                          ' Incomplete
Public Const kTranPending = 601                             ' Pending
Public Const kIMCommit = 165675                             ' Committed
Public Const kUnitPrice = 165796                            ' Unit Price
'
' Messages
'
Public Const kmsgDataError = 165208                         ' General failure in populating worktable. Invalid system data.
Public Const kmsgSotaErr = 1002                             '             Module: {0}  Company:  {1}  AppName:  {2}Error < {3} >  occurred at < {4} >  in Procedure < {5} >{6}                                    This application will now close.
Public Const kmsgUnexpectedKeyChangeCode = 100008           ' Unexpected KeyChangeCode: {0}
Public Const kmsgUnexpectedConfirmUnloadRV = 100009         ' Unexpected Confirm Unload Return Value: {0}
Public Const kmsgRequiredField = 100012                     ' {0} required.
Public Const kmsgInvalidField = 100013                      ' Invalid {0}.
Public Const kmsgUnexpectedOperation = 100015               ' Unexpected Operation Value: {0}
Public Const kmsgSaveChangesError = 100017                  ' An error occurred while attempting to save this data.  The changes cannot be saved at this time.
Public Const kmsgRecDeletedByAnother = 100029               ' This {0} record has been deleted by another user.
Public Const kmsgUnexpectedSPReturnValue = 100071           ' Unexpected Stored Procedure Return Value: {0}
Public Const kmsgNoOptionsRecFound = 100329                 ' An Options record was not found for module "{0}", company "{1}". Unable to retrieve the Option property you requested.
Public Const kmsgNavigatorSetupError = 100084               ' An error occurred while setting up the properties for one of the Lookup buttons on this form.
Public Const kmsgCannotEqual = 100086                       ' {0} cannot equal {1}
Public Const kmsgDMSaveChangesGen = 100241                  ' Save changes?
Public Const kmsgSaveBeforeAdd = 100246                     ' {0} must be saved before continuing.  Do you wish to save now?
Public Const kmsgErrTmpTblCreate = 230010                   ' An error occurred attempting to create the temporary table.  {0}: {1}
Public Const kmsgSPBadReturn = 210017                       ' Stored Procedure '{0}' returned an unexpected value: {1}
Public Const kmsgARNoPosIfNeg = 153410                      ' You may only enter a positive {0} if the {1} is positive.
Public Const kmsgExpError = 153450                          ' Error: "{0}" in {1}
Public Const kmsgProc = 153223                              ' Unexpected Error running Stored Procedure {0}.
Public Const kmsgReqField = 153230                          ' '{0}' is a required field.
Public Const kIMmsgInvalidWhsePPGCPG = 165609               ' Invalid Warehouse {0}.
Public Const kmsgKitComponent = 160153                      ' Kit must contain at least one component.
Public Const kmsgTaxCalcError = 140464                      ' An error occurred while calculating taxes.
Public Const kIMKitCompValidate = 165069                    ' Component item cannot be blank.
Public Const kmsgCheckBeforeSave = 150007                   ' You must enter a valid {0} to perform this function.
Public Const kIMmsgInvItem = 165158                         ' Invalid Item.
Public Const kmsgFatalError = 165173                        ' Unexpected Error.
Public Const kMsgUnspecifiedWarehouse = 165271              ' Warehouse not specified.
Public Const kIMmsgDuplicateItem = 165146                   ' Item already exists.
Public Const kmsgGetLocalStringsError = 110094              ' An error occurred while retrieving text for messages used in this function.
Public Const kmsgGenericExclamation = 110162                ' {0}
Public Const kmsgCannotBeGreaterThan = 120017               ' {0} cannot be greater than {1}
Public Const kmsgCannotBeNegative = 120019                  ' {0} cannot be a negative value.
Public Const kmsgGetCIOptionsFailure = 120028               ' The Common Information Options information cannot be accessed at this time.Please call Product Support for technical assistance.
Public Const kmsgBatchIsPosted = 130047                     ' The selected {0} has been posted.Please select another {1}.
Public Const kmsgCannotBeBlank = 130082                     ' {0} cannot be blank.
Public Const kmsgErrorInsertIntoWrkTable = 160365           ' Error #{0} occured while trying to insert data into the work table {1}.
Public Const kmsgIMTrnsfrOrderVoided = 160376               ' This {0} record has been voided previously.  Please enter a different value.
Public Const kmsgErrorLoadTask = 100353                     ' Error in Loading {0}
Public Const kSOShipLogAlreadyExists = 250120               ' This Customer Return exists in the Shipment Log with a status of {0}.  Another user may have created it.  Do you want to use it?
Public Const kSOmsgInvalidReturnQty = 250121                ' The quantity returned cannot be greater than the quantity on the original transaction.
Public Const kSOmsgInvalidReturnQtyRMA = 250367             ' The quantity returned cannot be greater than the quantity specified on the RMA.
Public Const kmsgSOAllCustRtrnNumsUsed = 250126             ' All Customer Return numbers have been used.  Please call Product Support for technical assistance.
Public Const kmsgSOMustEnterOpenRMA = 250127                ' You can only enter an RMA in an Open Status.
Public Const kmsgSORMAHasExpired = 250128                   ' This RMA has expired so it may not be used for a Customer Return.
Public Const kmsgSORtrnDateAfterRMAExpDate = 250129         ' The Customer Return Date cannot be after the RMA Expiration Date: {0}
Public Const kmsgSORtrnErrSetPurgeStatus = 250130           ' An error occurred setting the Customer Return to a Purged Status.  Please call Product Support for Technical Assistance.
Public Const kmsgSORtrnLineUpdateCtrError = 250131          ' One or more Customer Return Ship Lines have been modified by another user.  Your changes will not be saved.
Public Const kmsgSOErrGettingPendShipInfo = 250132          ' Error getting pending shipment information from the database for this Customer Return.
Public Const kmsgSOErrDeletingCustReturn = 250133           ' An error occurred while deleting this Customer Return.  Please call Product Support for technical assistance.
Public Const kmsgQtyNotFullyDistributed = 250134            ' The distribution quantity does not match the transaction quantity for {0} line {1}.  Inventory distributions must be completed for every line before the {2} can be saved.
Public Const kmsgSORtrnErrSetPendingStatus = 250136         ' An error occurred setting the Customer Return to a Pending Status.  Please call Product Support for Technical Assistance.
Public Const kmsgSOErrCreatingDistObject = 250137           ' Error creating the Customer Return distribution object.
Public Const kmsgSOErrInsertPendShipment = 250138           ' Error inserting a new row into the Pending Shipment table for this Customer Return.  Please call Product Support for technical assistance.
Public Const kmsgSOErrUpdatePendShipment = 250139           ' Error updating the row in the Pending Shipment table for this Customer Return.  Please call Product Support for technical assistance.
Public Const kmsgSOErrReserveInvtTran = 250140              ' An error occurred while reserving one or more Inventory Transactions for this Customer Return.  Please call Product Support for technical assistance.
Public Const kmsgSOErrUpdateInvtTranLog = 250141            ' Error updating the row in the Inventory Transaction Log table for this Customer Return.  Please call Product Support for technical assistance.
Public Const kSOmsgReturnCompQtyTooHigh = 250142            ' Cannot authorize return of more than the {0} quantity of this component.
Public Const kSOmsgReturnExceedsThreshold = 250143          ' Total return amount {0} exceeds RMA threshold amount {1}.
Public Const kmsgSOCustRtrnNotInThisBatch = 250147          ' The {0} you entered is not in this batch so it may not be edited or viewed.Batch Number for {1} you entered:  {2}Please enter another {3}.
Public Const kmsgSOCustReturnQtyTooLarge = 250148           ' The Return Quantity in the selected Unit of Measure may not exceed {0}.
Public Const kSONegativeReturnQty = 250151                  ' Return quantity must be greater than 0.
Public Const kSONegReturnAmount = 250152                    ' Return amount cannot be less than 0.
Public Const kSOBadPositiveQty = 250153                     ' Return quantity must be less than 0.
Public Const kmsgSOSomeRMALinesInOtherWhse = 250154         ' There are {0} lines on the RMA for another warehouse.  Do you wish to view these lines?
Public Const kmsgSOMiscOrServiceWhenRMA = 250155            ' Only active Misc or Service items may be added to an RMA return.
Public Const kmsgSOMiscOrServiceWhenSOReq = 250156          ' Only active Misc or Service items may be added when SO required.
Public Const kmsgSORestockFeeTooLarge = 250158              ' The Restock Fee may not exceed the Return Amount to be credited or replaced on each line.
Public Const kmsgSODetailRestockFeeTooLarge = 250159        ' The Restock Fee may not exceed the Return Amount.
Public Const kmsgSOTotalNotLessThanZero = 250160            ' The total of the {0} cannot be less than zero.
Public Const kmsgSOErrGettingShipmentInfo = 250162          ' Error getting shipment information from the database for this Customer Return.
Public Const kmsgSORtrnLineInRMATwiceError = 250166         ' Another user has selected one or more lines from this RMA in a different Customer Return.  An RMA Line can only be used in one Customer Return.  Your changes will not be saved.
Public Const kmsgSORtrnLineSelectTwiceError = 250167        ' Another user has selected one or more ship lines in a different Customer Return.  Your changes will not be saved.
Public Const kSOHighLowTradePct = 250169                    ' Trade Discount Percent must be between 0 and 100
Public Const kmsgSOBadPositiveAmt = 250170                  ' Return amount must be less than 0.
Public Const kmsgSOSperDiff = 250174                        ' The salesperson you have selected for commission reversals is not the same salesperson found on one or more of the lines being returned.
Public Const kmsgFatalReportInit = 153302                   ' Unexpected Error Initializing Report Engine.
Public Const kmsgCantLoadDDData = 153369                    ' Unable to load data from Data Dictionary.
Public Const kIMPrinter = 165533                            ' Printer
Public Const kPreviewCol = 15881                            ' kPreviewCol
Public Const kImScreen = 165534                             ' Screen
Public Const kARAll = 151183                                ' All
Public Const ksFatal = 50060                                ' Fatal
Public Const ksWarning = 50061                              ' Warning
Public Const kSOMsgErrorInCommitShip = 250295               '{0} error(s) were generated.  The transaction cannot be committed at this time.
Public Const kSOMsgViewCommitShipWarnings = 250296          'Transaction committed with warnings. Would you like to view the warnings?
Public Const kSOMsgAdditionalErrorCommitShi = 250297        'Additional errors have occurred.  This transaction cannot be committed at this time.
Public Const kSOMsgErrorWarnCommitShip = 250298             '{0} warning(s) and {1} error(s) were generated.  The transaction cannot be committed at this time.
Public Const kSOmsgCommitShipProceedComfirm = 250299        '{0} warnings were generated.  Do you still want to commit the transaction?
Public Const kSOMsgTranValidatedSuccessful = 250300         'Transaction can be committed.
Public Const kmsgSOIntegrateIM = 250082                             ' Unable to start the selected task. This task requires that the IM module be integrated for this company.
Public Const kmsgShipmentCommitted = 250364                 ' This shipment has been committed by another process.
Public Const kSOmsgPriceOverride = 250223                   ' The price has been overriden.  Do you want to retain it?
Public Const kmsgTranQtyDistQtyDifferent = 165386           ' Transaction quantity does not match the quantity distributed.
Public Const kmsgIMSNoInvtRec = 160055                      ' An inventory record does not exist for Item: {0} in Warehouse: {1}.
