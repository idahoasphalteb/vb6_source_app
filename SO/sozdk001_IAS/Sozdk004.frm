VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#69.0#0"; "lookupview.ocx"
Begin VB.Form frmCompItem 
   Caption         =   "Components"
   ClientHeight    =   5565
   ClientLeft      =   60
   ClientTop       =   3930
   ClientWidth     =   11130
   HelpContextID   =   17375382
   Icon            =   "Sozdk004.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5565
   ScaleWidth      =   11130
   StartUpPosition =   2  'CenterScreen
   Begin NEWSOTALib.SOTAMaskedEdit txtlkuAutoDistBin 
      Height          =   255
      Left            =   3120
      TabIndex        =   12
      Top             =   5640
      Width           =   1575
      _Version        =   65536
      _ExtentX        =   2778
      _ExtentY        =   450
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin LookupViewControl.LookupView lkuAutoDistBin 
      Height          =   285
      Left            =   4680
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   5640
      Visible         =   0   'False
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupMode      =   1
   End
   Begin VB.CommandButton cmdDistribution 
      Caption         =   "&Dist..."
      Height          =   285
      Left            =   9720
      TabIndex        =   7
      Top             =   600
      WhatsThisHelpID =   17375383
      Width           =   1335
   End
   Begin VB.TextBox txtNavReturnKit 
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   5640
      Visible         =   0   'False
      WhatsThisHelpID =   17375384
      Width           =   1275
   End
   Begin LookupViewControl.LookupView navGridKit 
      Height          =   285
      Left            =   1560
      TabIndex        =   6
      Top             =   5640
      WhatsThisHelpID =   17375385
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   503
      LookupMode      =   1
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtKitUOM 
      Height          =   285
      Left            =   5280
      TabIndex        =   5
      Top             =   5640
      WhatsThisHelpID =   17375386
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtItemID 
      Height          =   285
      Left            =   480
      TabIndex        =   2
      Top             =   600
      WhatsThisHelpID =   17375387
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   11130
      _ExtentX        =   19632
      _ExtentY        =   741
      Style           =   6
   End
   Begin FPSpreadADO.fpSpread grdCompItem 
      Height          =   4350
      Left            =   90
      TabIndex        =   4
      Top             =   1080
      WhatsThisHelpID =   17375389
      Width           =   10950
      _Version        =   524288
      _ExtentX        =   19315
      _ExtentY        =   7673
      _StockProps     =   64
      AllowCellOverflow=   -1  'True
      DisplayRowHeaders=   0   'False
      EditModeReplace =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ScrollBarExtMode=   -1  'True
      ScrollBars      =   2
      SpreadDesigner  =   "Sozdk004.frx":23D2
      AppearanceStyle =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtItemDesc 
      Height          =   285
      Left            =   2520
      TabIndex        =   8
      Top             =   600
      WhatsThisHelpID =   17375390
      Width           =   3135
      _Version        =   65536
      _ExtentX        =   5530
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      sBorder         =   0
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtKitQty 
      Height          =   285
      Left            =   8640
      TabIndex        =   10
      Top             =   600
      WhatsThisHelpID =   17375391
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
   End
   Begin VB.Label lblReturnQty 
      AutoSize        =   -1  'True
      Caption         =   "Return Qty"
      Height          =   195
      Left            =   7680
      TabIndex        =   9
      Top             =   645
      Width           =   765
   End
   Begin VB.Label lblItem 
      AutoSize        =   -1  'True
      Caption         =   "Item"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   645
      Width           =   300
   End
End
Attribute VB_Name = "frmCompItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************************************
'Date       Project     SE          Description of modification
'****************************************************************************************
Option Explicit
    
Private Declare Function AllowSetForegroundWindow Lib "user32" (ByVal dwProcessID As Long) As Boolean
Private Const ASFW_ANY = -1
    
    Private Const kColSOCompKey = 1
    Private Const kColSOCompItemKey = 2
    Private Const kColSOCompItemID = 3
    Private Const kColSOCompItemDesc = 4
    Private Const kColSOCompItemUOM = 5
    Private Const kColSOCompItemQty = 6
    Private Const kColSOCompItemStdQty = 7
    Private Const kColSOCompItemSOLineQty = 8
    Private Const kColSOCompItemRMALineQty = 9
    Private Const kColSOCompItemTotalQty = 10
    Private Const kColSOCompItemQtyDistributed = 11
    
    Private Const kColAutoDist = 12
    Private Const kColAutoDistBinID = 13
    Private Const kColAutoDistBinKey = 14
    Private Const kColAutoDistBinQtyAvail = 15
    Private Const kColAutoDistBinUOMKey = 16
    Private Const kColAutoDistLotNo = 17
    Private Const kColAutoDistLotExpDate = 18
    
    Private Const kColSOCompItemInvtTranKey = 19
    Private Const kColSOCompItemCompItemShipLineKey = 20
    Private Const kColSOCompItemCompItemOrigShipLineKey = 21
    Private Const kColSOCompItemType = 22
    Private Const kColSOCompItemTrackMeth = 23
    Private Const kColSOCompItemUOMKey = 24
    Private Const kColSOCompItemTranIdentifier = 25
    Private Const kColSOCompUsePrefBin = 26
    Private Const kSOCompMaxCols = 26
    
    Private Const kTypeBTOKit = 7
    
    Private Const kOpen = 1
    Private Const kPosted = 2
    
    Private Const kPrimaryDistTmp = 1
    Private Const kSecondaryDistTmp = 2
    
    Private Const kTM_None                   As Integer = 0
    Private Const kTM_Lot                    As Integer = 1
    Private Const kTM_Serial                 As Integer = 2
    Private Const kTM_Both                   As Integer = 3
    
    Private Const kDistBin_LkuRetCol_BinID As Integer = 1
    Private Const kDistBin_LkuRetCol_BinKey As Integer = 2
    Private Const kDistBin_LkuRetCol_QtyOnHand As Integer = 3
    Private Const kDistBin_LkuRetCol_AvailQty As Integer = 4
    Private Const kDistBin_LkuRetCol_UOMKey As Integer = 5

    Private Const kDistBinLot_LkuRetCol_BinID As Integer = 1
    Private Const kDistBinLot_LkuRetCol_BinKey As Integer = 2
    Private Const kDistBinLot_LkuRetCol_LotNo As Integer = 3
    Private Const kDistBinLot_LkuRetCol_LotKey As Integer = 4
    Private Const kDistBinLot_LkuRetCol_ExpDate As Integer = 5
    Private Const kDistBinLot_LkuRetCol_QtyOnHand As Integer = 6
    Private Const kDistBinLot_LkuRetCol_AvailQty As Integer = 7
    Private Const kDistBinLot_LkuRetCol_UOMKey As Integer = 8
    
    Private WithEvents moGM         As clsGridMgr
Attribute moGM.VB_VarHelpID = -1
    Private moGridNavKit        As clsGridLookup
    Private moAutoDistBin       As clsGridLookup
    Private mcolAutoDistBinRetCols As Collection

    Private moContextMenu       As New clsContextMenu
    Private moOptions           As clsModuleOptions
    
    Private msCompanyID         As String
    Private miOldFormHeight As Long
    Private miOldFormWidth As Long
    Private miMinFormHeight As Long
    Private miMinFormWidth As Long
    Private mbGridDirty         As Boolean
    Private msSelectItem        As String
    Private mbIsValidating      As Boolean
    Private mbIMActivated       As Boolean
    Private miQtyDP             As Integer
    Private mlRMALineKey        As Long
    Private mlKitShipLineKey    As Long
    Private mdKitQty            As Double
    Private mlKitItemKey        As Long
    Private msTranDate          As String
    Private msCustReturnNo      As String
    Private mlOrigShipLineKey   As Long
    Private mlRcvgWhseKey       As Long
    Private mlSOLineKey         As Long
    Private mlPutAwayBinKey     As Long
    Private mbUsePrefBin        As Boolean
    Private mdTotalPrice        As Double
    Private mbDataChanged       As Boolean
    Private mbAllowAllMod       As Boolean
    Private mbAllowDecreaseOnly As Boolean
    Private miSource            As Integer
    Private mdCurrExchRate      As Double
    Private msCurrID            As String
    Private miCurrDigits        As Integer
    Private moClass             As Object
    Private moDist              As Object
    Private mbCustomizedBTO     As Boolean
    Private muCallerType        As Caller
    Private miLineStatus        As Integer
    Private mbTrackQtyAtBin     As Boolean
    Private mdStdKitPrice       As Double   'Std Pricing for Kit based upon original item components
    

    Public gbFormIsDirty        As Boolean
    
Public Property Get KitPrice() As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    KitPrice = mdTotalPrice

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "KitPrice", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Let StdKitPrice(dStdPrice As Double)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mdStdKitPrice = dStdPrice

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "StdKitPrice", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get DataChanged() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    DataChanged = mbDataChanged

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DataChanged", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Set oDist(oNewDist As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDist = oNewDist

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oDist", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Sub DeleteAllKitDists(lShipKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

Dim sSQL As String
Dim rs As Object


    sSQL = "SELECT a.ShipLineKey FROM #tsoCustRtrnShipLine a WITH (NOLOCK), timItem b WITH (NOLOCK) " & _
"WHERE a.ItemKey = b.ItemKey AND a.ShipKey = " & lShipKey & _
" AND b.ItemType = " & kTypeBTOKit
        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    rs.MoveFirst
    While Not rs.IsEOF
        DeleteKitDists rs.Field("ShipLineKey")
        rs.MoveNext
    Wend



'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteAllKitDists", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub DeleteKitDists(lKitShipLineKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim arrInvtTranKey() As Long
    Dim lCount As Long
    Dim sSQL As String
    Dim rs As Object
    
    lCount = -1
    
    sSQL = "SELECT InvtTranKey FROM #tsoCompItem a WITH (NOLOCK), timItem b WITH (NOLOCK) " & _
            "WHERE a.CompItemKey = b.ItemKey AND b.ItemType IN (5, 6, 8) " & _
            "AND a.KitShipLineKey = " & lKitShipLineKey & " AND InvtTranKey <> 0"
        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    rs.MoveFirst
    While Not rs.IsEOF
        lCount = lCount + 1
        ReDim arrInvtTranKey(lCount)
        arrInvtTranKey(lCount) = rs.Field("InvtTranKey")
        rs.MoveNext
    Wend
    If lCount > -1 Then
        moDist.CancelDistributions arrInvtTranKey
        moDist.DeleteDistSOCustRet arrInvtTranKey
        
        If moDist.oError.Number <> 0 Then
            giSotaMsgBox Me, moClass.moSysSession, moDist.oError.MessageConstant
            moDist.oError.Clear
            Exit Sub
        End If
        
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteKitDists", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub CancelAllKitDists(lShipKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    
    Dim sSQL As String
    Dim rs As Object


    sSQL = "SELECT a.ShipLineKey FROM #tsoCustRtrnShipLine a WITH (NOLOCK), timItem b WITH (NOLOCK) " & _
"WHERE a.ItemKey = b.ItemKey AND a.ShipKey = " & lShipKey & _
" AND b.ItemType = " & kTypeBTOKit
        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

    rs.MoveFirst
    While Not rs.IsEOF
        CancelKitDists rs.Field("ShipLineKey")
        rs.MoveNext
    Wend

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CancelAllKitDists", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub CancelKitDists(lKitShipLineKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim arrInvtTranKey() As Long
    Dim lCount As Long
    Dim sSQL As String
    Dim rs As Object
    
    lCount = -1
    
    sSQL = "SELECT InvtTranKey FROM #tsoCompItem a WITH (NOLOCK), timItem b WITH (NOLOCK) " & _
            "WHERE a.ItemKey = b.ItemKey AND b.ItemType IN (5, 6, 8) " & _
            "AND a.KitShipLineKey = " & lKitShipLineKey & " AND InvtTranKey <> 0"
        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    rs.MoveFirst
    While Not rs.IsEOF
        lCount = lCount + 1
        ReDim arrInvtTranKey(lCount)
        arrInvtTranKey(lCount) = rs.Field("InvtTranKey")
        rs.MoveNext
    Wend
    
    moDist.CancelDistributions arrInvtTranKey

    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CancelKitDists", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function bAutoCreateKitDist(lKitShipLineKey As Long, lRcvgWhseKey As Long, sCustReturnNo As String, sTranDate As String, _
                                    dReturnQty As Double, bUsePrefBin As Boolean, lPutAwayBinKey As Long, _
                                    Optional lOrigShipLineKey As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sSQL As String
    Dim rs As Object
    Dim rs2 As Object
    Dim lInvtTranKey As Long
    Dim lRetVal As Long
    Dim dDistQty As Double
    Dim iItemType As Integer
    Dim lCompOrigShipLineKey As Long
    Dim lTranIdentifier As Long

    'This routine tries to auto distribute all of the components that are part of a Kit's ShipLineKey.
    'This routine will return a False if at least one of the components were not fully distributed.
    'Calling this routine will not cause the Lot/Serial/Bin UI to appear if a component is not fully
    'distributed as this routine is meant to be called in the save routine of the parent form.
    'Instead, if not all of the components were fully distributed, the component UI will be displayed.

    'Default to true.  Will be set to False if one of the components were not completely distributed.
    bAutoCreateKitDist = True
    
    'If the warehouse does not TrackQtyAtBin, then there is only 1 default bin.
    'Get that WhseBinKey and assign it to the lPutAwayBinKey variable.
    mbTrackQtyAtBin = glGetValidLong(moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse WITH (NOLOCK)", "WhseKey=" & lRcvgWhseKey))
    If mbTrackQtyAtBin = False Then
        lPutAwayBinKey = glGetValidLong(moClass.moAppDB.Lookup("WhseBinKey", "timWhseBin WITH (NOLOCK)", "DfltBin = 1 AND WhseKey=" & lRcvgWhseKey))
        bUsePrefBin = False
        If lPutAwayBinKey = 0 Then
            bAutoCreateKitDist = False
        End If
    End If
    
    If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "#tsoCompItem", "KitShipLineKey = " & lKitShipLineKey & _
                                            " AND DistQty <> (CompItemQty * " & dReturnQty & ")")) > 0 Then
        'Distributions are not complete.
        sSQL = "SELECT * FROM #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey & " AND DistQty <> (CompItemQty * " & dReturnQty & ")"
    ElseIf lPutAwayBinKey <> 0 And bUsePrefBin = False Then
        'Check if distributions are saved or about to be saved against the defined PutAwayBinKey.
        If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "#tsoCompItem c " & _
                                                " LEFT OUTER JOIN vimSavedDistributions d ON c.InvtTranKey = d.InvtTranKey " & _
                                                " LEFT OUTER JOIN #timInvtDistWrk idw ON c.InvtTranKey = idw.TranKey ", _
                                                "c.KitShipLineKey = " & lKitShipLineKey & _
                                                " AND COALESCE(d.WhseBinKey, 0) <> " & lPutAwayBinKey & _
                                                " AND COALESCE(idw.WhseBinKey, 0) <> " & lPutAwayBinKey)) > 0 Then
            sSQL = "SELECT DISTINCT c.* " & _
                    " FROM #tsoCompItem c " & _
                    " LEFT OUTER JOIN vimSavedDistributions d ON c.InvtTranKey = d.InvtTranKey " & _
                    " LEFT OUTER JOIN #timInvtDistWrk idw ON c.InvtTranKey = idw.TranKey " & _
                    " WHERE c.KitShipLineKey = " & lKitShipLineKey & _
                    " AND COALESCE(d.WhseBinKey, 0) <> " & lPutAwayBinKey & _
                    " AND COALESCE(idw.WhseBinKey, 0) <> " & lPutAwayBinKey
        End If
    ElseIf lPutAwayBinKey = 0 And bUsePrefBin = True Then
        'Check if distributions are saved or about to be saved against the component's preferred bin.
        If glGetValidLong(moClass.moAppDB.Lookup("COUNT(*)", "#tsoCompItem c INNER JOIN timInvtBinList ibl ON c.CompItemKey = ibl.ItemKey " & _
                                                " AND ibl.WhseKey = " & lRcvgWhseKey & " AND PrefNo = 1 " & _
                                                " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(" & lRcvgWhseKey & ",c.CompItemKey," & kTranTypeSORT & ")" & _
                                                " LEFT OUTER JOIN (SELECT COALESCE(idw.ItemKey, d.ItemKey) 'ItemKey', " & _
                                                " COALESCE(idw.WhseKey, d.WhseKey) 'WhseKey', idw.WhseBinKey 'IDW_WhseBinKey', " & _
                                                " d.WhseBinKey 'SavedDistWhseBinKey' " & _
                                                " FROM #tsoCompItem tmp " & _
                                                " LEFT OUTER JOIN vimSavedDistributions d ON tmp.InvtTranKey = d.InvtTranKey" & _
                                                " LEFT OUTER JOIN #timInvtDistWrk idw ON tmp.InvtTranKey = idw.TranKey ) drv " & _
                                                " ON ibl.ItemKey = drv.ItemKey AND ibl.WhseKey = drv.WhseKey " & _
                                                " AND (ibl.WhseBinKey = COALESCE(drv.IDW_WhseBinKey, 0) " & _
                                                " OR ibl.WhseBinKey = COALESCE(drv.SavedDistWhseBinKey, 0))", _
                                                "c.KitShipLineKey = " & lKitShipLineKey & " AND drv.ItemKey IS NULL")) > 0 Then
            sSQL = "SELECT DISTINCT c.* " & _
                    " FROM #tsoCompItem c " & _
                    " INNER JOIN timInvtBinList ibl ON c.CompItemKey = ibl.ItemKey " & _
                    " AND ibl.WhseKey = " & lRcvgWhseKey & _
                    " AND PrefNo = 1 " & _
                    " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(" & lRcvgWhseKey & ",c.CompItemKey," & kTranTypeSORT & ")" & _
                    " LEFT OUTER JOIN (SELECT COALESCE(idw.ItemKey, d.ItemKey) 'ItemKey', " & _
                            " COALESCE(idw.WhseKey, d.WhseKey) 'WhseKey', idw.WhseBinKey 'IDW_WhseBinKey', d.WhseBinKey 'SavedDistWhseBinKey'" & _
                            " FROM #tsoCompItem tmp " & _
                            " LEFT OUTER JOIN vimSavedDistributions d ON tmp.InvtTranKey = d.InvtTranKey " & _
                            " LEFT OUTER JOIN #timInvtDistWrk idw ON tmp.InvtTranKey = idw.TranKey ) drv " & _
                            " ON ibl.ItemKey = drv.ItemKey AND ibl.WhseKey = drv.WhseKey " & _
                            " AND (ibl.WhseBinKey = COALESCE(drv.IDW_WhseBinKey, 0) " & _
                            " OR ibl.WhseBinKey = COALESCE(drv.SavedDistWhseBinKey, 0)) " & _
                            " WHERE c.KitShipLineKey = " & lKitShipLineKey & " AND drv.ItemKey IS NULL"
        End If
    End If
    
    If Len(sSQL) > 0 Then
    'get rows from temp table #tsoCOmpItem.  All that don't have an invt trankey need to be distributed. (Though -- some lines
    'won't need distributions due to their item type or tracking.
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    rs.MoveFirst
    While Not rs.IsEOF
        iItemType = moClass.moAppDB.Lookup("ItemType", "timItem", "ItemKey = " & rs.Field("CompItemKey"))
        Select Case iItemType
            Case IMS_FINISHED_GOOD, IMS_RAW_MATERIAL, IMS_ASSEMBLED_KIT
                
                'A put away bin or indication to use the preferred bin is required.  If not available,
                'return false so the calling app can display the Component UI.  Skip the distribution.
                If bUsePrefBin = False And lPutAwayBinKey = 0 Then
                    bAutoCreateKitDist = False
                Else
                    'reserve invt trankey
                    msTranDate = sTranDate
                    msCustReturnNo = sCustReturnNo
                    mlRcvgWhseKey = lRcvgWhseKey
                    mdKitQty = dReturnQty
                    mlOrigShipLineKey = glGetValidLong(lOrigShipLineKey)
                    mlKitShipLineKey = lKitShipLineKey
                    lInvtTranKey = glGetValidLong(rs.Field("InvtTranKey"))
                    
                    If lInvtTranKey = 0 Then
                        With moClass.moAppDB
                            .SetInParam moClass.moSysSession.CompanyId
                            .SetInParam gsFormatDateToDB(msTranDate)
                            .SetInParam msCustReturnNo
                            .SetInParam kTranTypeSORT
                            .SetOutParam lInvtTranKey
                            .SetOutParam lRetVal
                            
                            .ExecuteSP "spimReserveInvtTran"
                
                            lInvtTranKey = glGetValidLong(.GetOutParam(5))
                            lRetVal = glGetValidLong(.GetOutParam(6))
                            .ReleaseParams
                        End With
                        If lRetVal <> 0 Then
                            bAutoCreateKitDist = False
                            giSotaMsgBox Me, moClass.moSysSession, kmsgSOErrReserveInvtTran
                            Exit Function
                        End If
                        
                        'Add InvtTranKey to #ReservedInvtTranKeys temp table.
                        AddToReservedInvtTranKeys moClass.moAppDB, lInvtTranKey
                        
                        'Update #tsoCompItem with the InvtTranKey that was just reserved.
                        'Do this before calling EditDistSOCustReturn because the InvtTranKey
                        'is used to tell if an item is a component of a BTO Kit.
                        moClass.moAppDB.ExecuteSQL "UPDATE #tsoCompItem SET InvtTranKey = " & lInvtTranKey & _
                                                    " WHERE CompItemKey = " & rs.Field("CompItemKey") & " AND KitShipLineKey = " & mlKitShipLineKey
                    End If
                    
                    lCompOrigShipLineKey = lGetCompOrigShipLineKey(mlOrigShipLineKey, rs.Field("CompItemKey"))
            
                    dDistQty = moDist.EditDistSOCustReturn(kTranTypeSORT, lInvtTranKey, _
                               rs.Field("CompItemKey"), _
                               mlRcvgWhseKey, _
                               moClass.moAppDB.Lookup("StockUnitMeasKey", "timItem", "ItemKey =" & gsQuoted(rs.Field("CompItemKey"))), _
                               mdKitQty * rs.Field("CompItemQty"), _
                               lCompOrigShipLineKey, , _
                               lPutAwayBinKey, _
                               bUsePrefBin, _
                               False, False, 0, lKitShipLineKey)
            
                    
                    If moDist.oError.Number <> 0 Then
                        bAutoCreateKitDist = False
                        giSotaMsgBox Me, moClass.moSysSession, moDist.oError.MessageConstant
                        moDist.oError.Clear
                        Exit Function
                    Else
                        sSQL = "UPDATE #tsoCompItem SET InvtTranKey = " & lInvtTranKey & ", DistQty = " & dDistQty & _
                              " WHERE CompItemKey = " & rs.Field("CompItemKey") & " AND KitShipLineKey = " & mlKitShipLineKey
                        moClass.moAppDB.ExecuteSQL sSQL
                        moDist.bRememberDistInfo lInvtTranKey
                        
                        'Check if it has been fully distributed.  If not, set the
                        'bAutoCreateKitDist to False but continue to auto distribute.
                        If dDistQty <> (mdKitQty * rs.Field("CompItemQty")) Then
                            bAutoCreateKitDist = False
                        End If
                        
                    End If
                End If
        End Select
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        bAutoCreateKitDist = False
        gSetSotaErr Err, sMyName, "bAutoCreateKitDist", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Public Function bSaveComps(sCustRtrnNo As String) As Boolean
'This sub will create #tsoCustRtrnShipLine records for each component.  It will
'delete #tsoCustRtrnShipLine rows for components deleted, and update rows for
'existing.
'
'Note - this is currently only used for Returns.
Dim lRetVal As Long

    bSaveComps = False
    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB
        .SetInParam gsGetValidStr(sCustRtrnNo)
        .SetOutParam lRetVal
        .ExecuteSP "spsoSaveCompItem"
        lRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    On Error GoTo ExpectedErrorRoutine2
    
    If lRetVal <> 1 Then
        bSaveComps = False
        Exit Function
    End If
    
    bSaveComps = True
    Exit Function

ExpectedErrorRoutine:
    moClass.moAppDB.ReleaseParams
    Exit Function
ExpectedErrorRoutine2:
'do VBRig error processing
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bSaveComps", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bValidateCell(ByVal Col As Long, ByVal Row As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sItemID         As String
    Dim lItemKey        As Long
    Dim i               As Long
    Dim dStdQty         As Double
    Dim dSOLineQty      As Double
    Dim dRMALineQty     As Double
    Dim rs              As Object
    Dim sSQL            As String
    Dim dQtyPerKit      As Double
    Dim bDifferentItem As Boolean
           
    If mbIsValidating Then
        Exit Function
    End If
    
    mbIsValidating = True

    If navGridKit.Visible = True Then
        gbSetFocus Me, navGridKit
        navGridKit.Visible = False
    End If
    
    Select Case Col
        Case kColSOCompItemID
            sItemID = Trim(gsGridReadCell(grdCompItem, Row, Col))
            
            If mbAllowDecreaseOnly Then
                If miSource = kSrcNone Then 'only allow items from the standard build.  Customizing the build is not allowed.
                    lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, timInventory b, timKitCompList c", _
                                "a.ItemKey=b.ItemKey AND a.ItemKey=c.CompItemKey AND a.ItemID=" & gsQuoted(sItemID) & _
                                " AND c.KitItemKey=" & mlKitItemKey & _
                                " AND b.WhseKey=" & mlRcvgWhseKey & " AND a.ItemType IN (5, 6, 8)"))
                ElseIf miSource = kSrcSalesOrder Then 'only allow items from the original SO.
                    lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, timInventory b, tsoSOLineCompItem c", _
                                "a.ItemKey=b.ItemKey AND a.ItemKey=c.CompItemKey AND a.ItemID=" & gsQuoted(sItemID) & _
                                " AND c.SOLineKey=" & mlSOLineKey & _
                                " AND b.WhseKey=" & mlRcvgWhseKey & " AND a.ItemType IN (5, 6, 8)"))
                ElseIf miSource = kSrcRMA Then 'only allow items from the RMA.
                    lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, timInventory b, tsoRMALineCompItem c", _
                                "a.ItemKey=b.ItemKey AND a.ItemKey=c.CompItemKey AND a.ItemID=" & gsQuoted(sItemID) & _
                                " AND c.RMALineKey=" & mlRMALineKey & _
                                " AND b.WhseKey=" & mlRcvgWhseKey & " AND a.ItemType IN (5, 6, 8)"))
                End If
            Else
                'Item must either: have an inventory record in the receiving warehouse and be item type 5, 6, or 8;
                lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, timInventory b", " a.ItemKey = b.ItemKey AND a.ItemID =" & gsQuoted(sItemID) & _
                            " AND b.WhseKey =" & mlRcvgWhseKey & " AND a.ItemType IN (5, 6, 8)"))
            End If
            
            If lItemKey = 0 Then
                If mbAllowDecreaseOnly Then
                    If miSource = kSrcNone Then
                        lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, timKitCompList b", _
                                   "a.ItemKey=b.CompItemKey AND b.KitItemKey=" & mlKitItemKey & " AND ItemType IN (1)"))
                    ElseIf miSource = kSrcSalesOrder Then
                        lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, tsoSOLineCompItem b", _
                                   "a.ItemKey=b.CompItemKey AND b.SOLineKey=" & mlSOLineKey & " AND ItemType IN (1)"))
                    ElseIf miSource = kSrcRMA Then
                        lItemKey = glGetValidLong(moClass.moAppDB.Lookup("a.ItemKey", "timItem a, tsoRMALineCompItem b", _
                                   "a.ItemKey=b.CompItemKey AND b.RMALineKey=" & mlRMALineKey & " AND ItemType IN (1)"))
                    End If
                Else
                    'or be a non-inventory (non-chargeback) item (item types 1 (Misc.)).
                    lItemKey = glGetValidLong(moClass.moAppDB.Lookup("ItemKey", "timItem", "ItemType IN (1) AND ItemID =" & gsQuoted(sItemID) & " AND CompanyID = " & gsQuoted(msCompanyID)))
                End If
                
                If lItemKey = 0 Then
                    If sItemID <> "" Then
                        giSotaMsgBox Me, moClass.moSysSession, kIMmsgInvItem
                        gGridUpdateCell grdCompItem, Row, Col, msSelectItem
                    Else
                        bValidateCell = True
                    End If
                    mbIsValidating = False
                    Exit Function
                End If
            End If
            
            'check for duplicate items exist by comparing the item row by row for grid with 2 rows or more
            If grdCompItem.MaxRows > 1 Then
                For i = 1 To grdCompItem.DataRowCnt
                
                    'do not compare to selected row itself
                    If i <> Row Then
                        If UCase(gsGridReadCell(grdCompItem, i, Col)) = UCase(sItemID) Then
                            'Display error message when there is duplicate item in the kit
                            giSotaMsgBox Me, moClass.moSysSession, kIMmsgDuplicateItem
                            'reset to previously chosen valid item
                            gGridUpdateCell grdCompItem, Row, Col, msSelectItem
                            mbIsValidating = False
                            Exit Function
                        End If
                    End If
                Next i
            End If
            
           sSQL = "SELECT a.AllowDecimalQty, b.ShortDesc, " & _
                  " c.UnitMeasID, d.CompItemQty StdQty, e.CompItemQty SOLineQty, " & _
                  " f.CompItemQty RMALineQty, a.ItemType, a.TrackMeth " & _
                  " FROM timItem a WITH (NOLOCK) " & _
                  " JOIN timItemDescription b WITH (NOLOCK) ON b.ItemKey = a.ItemKey " & _
                  " JOIN tciUnitMeasure c WITH (NOLOCK) ON c.UnitMeasKey = a.StockUnitMeasKey" & _
                  " JOIN timKitCompList d WITH (NOLOCK) ON d.CompItemKey = a.ItemKey " & _
                                                        " AND d.KitItemKey = " & mlKitItemKey & _
                  " LEFT OUTER JOIN tsoSOLineCompItem e WITH (NOLOCK) ON e.CompItemKey = a.ItemKey " & _
                                                        " AND e.SOLineKey = " & mlSOLineKey & _
                  " LEFT OUTER JOIN tsoRMALineCompItem f WITH (NOLOCK) ON  f.CompItemKey = a.ItemKey " & _
                                                        " AND f.RMALineKey = " & mlRMALineKey & _
                  " WHERE a.ItemKey = " & lItemKey

           Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
           
            'Note -- might not be part of standard/SO/RMA build -- lookup will return a null, which Val() will convert to 0.
            dStdQty = Val(gsGetValidStr(rs.Field("StdQty")))
            dSOLineQty = Val(gsGetValidStr(rs.Field("SOLineQty")))
            dRMALineQty = Val(gsGetValidStr(rs.Field("RMALineQty")))

            'Determine if the item changed.
            If lItemKey <> glGetValidLong(gsGridReadCell(grdCompItem, Row, kColSOCompItemKey)) Then
                bDifferentItem = True
            Else
                bDifferentItem = False
            End If
            
            'update the grid with new values
            gGridUpdateCell grdCompItem, Row, kColSOCompItemKey, CStr(lItemKey)
            gGridUpdateCell grdCompItem, Row, kColSOCompItemID, sItemID
            gGridUpdateCell grdCompItem, Row, kColSOCompItemDesc, rs.Field("ShortDesc")
            gGridUpdateCell grdCompItem, Row, kColSOCompItemUOM, rs.Field("UnitMeasID")
            gGridUpdateCell grdCompItem, Row, kColSOCompItemQty, IIf((dStdQty < 1) And mbAllowDecreaseOnly, dStdQty, 1)
            gGridUpdateCell grdCompItem, Row, kColSOCompItemStdQty, CStr(dStdQty)
            gGridUpdateCell grdCompItem, Row, kColSOCompItemSOLineQty, CStr(dSOLineQty)
            gGridUpdateCell grdCompItem, Row, kColSOCompItemRMALineQty, CStr(dRMALineQty)
            gGridUpdateCell grdCompItem, Row, kColSOCompItemTotalQty, (Val(IIf((dStdQty < 1) And mbAllowDecreaseOnly, dStdQty, 1)) * mdKitQty)
            gGridUpdateCell grdCompItem, Row, kColSOCompItemType, rs.Field("ItemType")
            gGridUpdateCell grdCompItem, Row, kColSOCompItemTrackMeth, rs.Field("TrackMeth")
            
            'If the item is a non-inventory item, set the Tot Put Away qty to the item's Total Qty.
            If Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemType)) = IMS_MISC_ITEM Then
                gGridUpdateCell grdCompItem, Row, kColSOCompItemQtyDistributed, gsGridReadCell(grdCompItem, Row, kColSOCompItemTotalQty)
            Else
                gGridUpdateCell grdCompItem, Row, kColSOCompItemQtyDistributed, 0
            End If
            
            'If the item changed, empty the ShipLineKey and InvtTranKey columns
            'in order to get new keys to be assigned to the new item.
            'Must also cancel any existing distributions.
            If bDifferentItem Then
                moDist.CancelDistributions glGetValidLong(gsGridReadCell(grdCompItem, Row, kColSOCompItemInvtTranKey))
                gGridUpdateCell grdCompItem, Row, kColSOCompItemCompItemShipLineKey, ""
            End If
            
            'Enable the distribution button if needed.
            cmdDistribution.Enabled = bEnableCmdDistribution(Row)
            
            If rs.Field("AllowDecimalQty") = 0 Then
                gGridSetCellType grdCompItem, Row, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, 0
                gGridSetCellType grdCompItem, Row, kColSOCompItemTotalQty, SS_CELL_TYPE_FLOAT, 0
                gGridSetCellType grdCompItem, Row, kColSOCompItemQtyDistributed, SS_CELL_TYPE_FLOAT, 0
            Else
                gGridSetCellType grdCompItem, Row, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, miQtyDP
                gGridSetCellType grdCompItem, Row, kColSOCompItemTotalQty, SS_CELL_TYPE_FLOAT, miQtyDP
                gGridSetCellType grdCompItem, Row, kColSOCompItemQtyDistributed, SS_CELL_TYPE_FLOAT, miQtyDP
            End If
            
            rs.Close
            Set rs = Nothing
                
            If Row = grdCompItem.MaxRows Then
                grdCompItem.MaxRows = grdCompItem.MaxRows + 1
            End If
            
        Case kColSOCompItemQty
            'make sure there is no item qty equal or less than 0
            lItemKey = Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemKey))
            If lItemKey <> 0 And Val(gsGridReadCell(grdCompItem, Row, Col)) <= 0 Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotEqual, "Quantity", "0 or less."
                mbIsValidating = False
                Exit Function
            End If
            
            If mbAllowDecreaseOnly And miSource = kSrcNone Then
                If Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemStdQty)) < Val(gsGridReadCell(grdCompItem, Row, Col)) Then
                    '''giSotaMsgBox Me, moClass.moSysSession, kSOmsgReturnCompQtyTooHigh, "standard kit quantity"
                    giSotaMsgBox Me, moClass.moSysSession, kmsgGenericExclamation, "Cannot authorize return of more than the standard kit quantity of this component."
                    dStdQty = Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemStdQty))
                    gGridUpdateCell grdCompItem, Row, kColSOCompItemQty, CStr(dStdQty)
                    mbIsValidating = False
                    Exit Function
                End If
            End If
            
            If miSource = kSrcSalesOrder Then
                If Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemSOLineQty)) < Val(gsGridReadCell(grdCompItem, Row, Col)) Then
                    '''giSotaMsgBox Me, moClass.moSysSession, kSOmsgReturnCompQtyTooHigh, "original sales order quantity"
                    giSotaMsgBox Me, moClass.moSysSession, kmsgGenericExclamation, "Cannot authorize return of more than the original sales order quantity of this component."
                    dSOLineQty = Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemSOLineQty))
                    gGridUpdateCell grdCompItem, Row, kColSOCompItemQty, CStr(dSOLineQty)
                    mbIsValidating = False
                    Exit Function
                End If
            End If
            
            If miSource = kSrcRMA Then
                If Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemRMALineQty)) < Val(gsGridReadCell(grdCompItem, Row, Col)) Then
                    '''giSotaMsgBox Me, moClass.moSysSession, kSOmsgReturnCompQtyTooHigh, "RMA quantity"
                    giSotaMsgBox Me, moClass.moSysSession, kmsgGenericExclamation, "Cannot authorize return of more than the RMA quantity of this component."
                    dRMALineQty = Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemRMALineQty))
                    gGridUpdateCell grdCompItem, Row, kColSOCompItemQty, CStr(dRMALineQty)
                    mbIsValidating = False
                    Exit Function
                End If
            End If
            
            'Format the decimal places per the item on the quantity field.
            If lItemKey <> 0 Then
                If moClass.moAppDB.Lookup("AllowDecimalQty", "timItem", "ItemKey =" & lItemKey) = 0 Then
                    gGridSetCellType grdCompItem, Row, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, 0
                Else
                    gGridSetCellType grdCompItem, Row, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, miQtyDP
                End If
            End If
            
            'Re-Calculate the Qty to Distribute.
            dQtyPerKit = Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemQty))
            gGridUpdateCell grdCompItem, Row, kColSOCompItemTotalQty, mdKitQty * dQtyPerKit
            
            'If the item is a non-inventory item, set the Tot Put Away qty to the item's Total Qty.
            If Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemType)) = IMS_MISC_ITEM Then
                gGridUpdateCell grdCompItem, Row, kColSOCompItemQtyDistributed, gsGridReadCell(grdCompItem, Row, kColSOCompItemTotalQty)
            End If

            'Enable the distribution button if needed.
            cmdDistribution.Enabled = bEnableCmdDistribution(Row)
           
    End Select

    mbIsValidating = False
    bValidateCell = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bValidateCell", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    FormHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Private Property Get sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmCompItem"
End Property
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    WhatHelpPrefix = "SOZ"

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "WhatHelpPrefix", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Private Sub FormatGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    gGridSetProperties grdCompItem, kSOCompMaxCols, kGridDataSheet
    gGridSetColumnWidth grdCompItem, 0, 3
    
    'Set column widths
    grdCompItem.UnitType = SS_CELL_UNIT_TWIPS
    
    'Set up the Component Item column -- allow editing if
    gGridSetHeader grdCompItem, kColSOCompItemID, gsBuildString(kIMColComponent, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColSOCompItemID, 1500 '3000
    If mbAllowAllMod Or mbAllowDecreaseOnly Then
        gGridSetColumnType grdCompItem, kColSOCompItemID, SS_CELL_TYPE_EDIT, 30 'max length
        gGridUnlockColumn grdCompItem, kColSOCompItemID
    Else
        gGridLockColumn grdCompItem, kColSOCompItemID
    End If
    
    'Set up the Description column & lock it
    gGridSetHeader grdCompItem, kColSOCompItemDesc, gsBuildString(kDescri, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColSOCompItemDesc, 3500 '4000
    gGridLockColumn grdCompItem, kColSOCompItemDesc
    
    'Set up the Stock UOM column & lock it
    gGridSetHeader grdCompItem, kColSOCompItemUOM, gsBuildString(kIMUOM, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnWidth grdCompItem, kColSOCompItemUOM, 550 '1100
    gGridLockColumn grdCompItem, kColSOCompItemUOM
    
    'Set up the Quantity column.  Note: Number of decimal places allowed will vary according to timItem.AllowDecimalQty value.
    gGridSetHeader grdCompItem, kColSOCompItemQty, gsBuildString(kIMQtyPerKit, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, miQtyDP, 7 'last two args are dec & integral places
    gGridSetColumnWidth grdCompItem, kColSOCompItemQty, 1100
    If Not mbAllowAllMod And Not mbAllowDecreaseOnly Then
        gGridLockColumn grdCompItem, kColSOCompItemQty
    Else
        gGridUnlockColumn grdCompItem, kColSOCompItemQty
    End If
    
    'Set up the Total Qty column & lock it
    gGridSetHeader grdCompItem, kColSOCompItemTotalQty, gsBuildString(kIMTotalQty, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColSOCompItemTotalQty, SS_CELL_TYPE_FLOAT, miQtyDP, 7 'last two args are dec & integral places
    gGridSetColumnWidth grdCompItem, kColSOCompItemTotalQty, 1100
    gGridLockColumn grdCompItem, kColSOCompItemTotalQty
    
    'Set up the Total Put Away column & lock it
    gGridSetHeader grdCompItem, kColSOCompItemQtyDistributed, "Total Put Away" 'gsBuildString(kIMTotalQty, moClass.moAppDB, moClass.moSysSession)
    gGridSetColumnType grdCompItem, kColSOCompItemQtyDistributed, SS_CELL_TYPE_FLOAT, miQtyDP, 7 'last two args are dec & integral place
    gGridSetColumnWidth grdCompItem, kColSOCompItemQtyDistributed, 1500
    gGridLockColumn grdCompItem, kColSOCompItemQtyDistributed
    
    'Hide the CompItemKey, KitItemKey, Std Qty, SO Line Qty, and RMA Line Qty
    'of Component Item for Kit (in case mbAllowDecreaseOnly is true)
    gGridHideColumn grdCompItem, kColSOCompItemKey
    gGridHideColumn grdCompItem, kColSOCompKey
    gGridHideColumn grdCompItem, kColSOCompItemStdQty
    gGridHideColumn grdCompItem, kColSOCompItemSOLineQty
    gGridHideColumn grdCompItem, kColSOCompItemRMALineQty
    gGridHideColumn grdCompItem, kColSOCompItemInvtTranKey
    gGridHideColumn grdCompItem, kColSOCompItemTranIdentifier
    gGridHideColumn grdCompItem, kColSOCompItemCompItemShipLineKey
    gGridHideColumn grdCompItem, kColSOCompItemCompItemOrigShipLineKey
    gGridHideColumn grdCompItem, kColSOCompItemType
    gGridHideColumn grdCompItem, kColSOCompItemTrackMeth
    gGridHideColumn grdCompItem, kColAutoDist
    gGridHideColumn grdCompItem, kColAutoDistBinUOMKey
    gGridHideColumn grdCompItem, kColAutoDistBinQtyAvail
    gGridHideColumn grdCompItem, kColAutoDistLotNo
    gGridHideColumn grdCompItem, kColAutoDistLotExpDate
    gGridHideColumn grdCompItem, kColSOCompItemUOMKey
    gGridHideColumn grdCompItem, kColSOCompUsePrefBin
    gGridHideColumn grdCompItem, kColAutoDistBinKey
    
    'Setup Auto distribution columns.
    gGridSetHeader grdCompItem, kColAutoDistBinID, "BinID"
    gGridSetColumnWidth grdCompItem, kColAutoDistBinID, 1500
    gGridSetColumnType grdCompItem, kColAutoDistBinID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdCompItem, kColAutoDist, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdCompItem, kColAutoDistBinUOMKey, SS_CELL_TYPE_STATIC_TEXT
    gGridSetColumnType grdCompItem, kColAutoDistBinQtyAvail, SS_CELL_TYPE_STATIC_TEXT
    gGridSetColumnType grdCompItem, kColAutoDistLotNo, SS_CELL_TYPE_STATIC_TEXT
    gGridSetColumnType grdCompItem, kColAutoDistLotExpDate, SS_CELL_TYPE_STATIC_TEXT
    
'    If mbAllowAllMod Or mbAllowDecreaseOnly Then
'        gGridSetMaxRows grdCompItem, 1
'    Else
'        gGridSetMaxRows grdCompItem, 0
'    End If
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "FormatGrid", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

' ********************************************************************************
'    Desc:  Binds the grid manager class tot he grid manager object.
'   Parms:  None
' ********************************************************************************
Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    If moGM Is Nothing Then
        Set moGM = New clsGridMgr
        With moGM
            Set .Grid = grdCompItem
            Set .Form = frmCompItem
            .GridType = IIf(mbAllowAllMod = False And mbAllowDecreaseOnly = False, kGridDataSheetNoAppend, kGridDataSheet)
            If mbAllowDecreaseOnly Then
                .AllowAdd = False
                .AllowGotoLine = False
                .AllowInsert = False
                .AllowMoveRow = False
            End If
            Set moGridNavKit = .BindColumn(kColSOCompItemID, navGridKit)
            Set moGridNavKit.ReturnControl = txtNavReturnKit
            Set moAutoDistBin = .BindColumn(kColAutoDistBinID, lkuAutoDistBin)
            Set moAutoDistBin.ReturnControl = txtlkuAutoDistBin
            .Init
        End With
    End If
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


'************************************************************************
'   Description:
'       create context menu object and bind controls to it
'
'   Param:
'       <none>
'
'   Returns:
'
'************************************************************************

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = New clsContextMenu      'Instantiate Context Menu Class

   With moContextMenu
        .BindGrid moGM, grdCompItem.hWnd 'define standard grid context menu (add/delete line)
        .Bind "*APPEND", grdCompItem.hWnd       'instructs context menu mgr to call CMAppendContextMenu for the grid
        .Bind "IMDA06", txtItemID.hWnd, kEntTypeIMItem
        Set .Form = frmCompItem
        .Init                                   'Init will set properties of Winhook control
    End With
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function CMAppendContextMenu(ctl As Object, hmenu As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'This is for the grid only.
    
    If (ctl Is grdCompItem) And (grdCompItem.ActiveCol = kColSOCompItemID Or (Not mbAllowDecreaseOnly And Not mbAllowAllMod)) Then
        AppendMenu hmenu, MF_ENABLED, 100, gsBuildString(kSOMaintainItem, moClass.moAppDB, _
moClass.moSysSession)
        AppendMenu hmenu, MF_SEPARATOR, 0, ""
    End If
    
Exit Function

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub lkuAutoDistBin_Click()

    Dim iEffOnInvt As Integer
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lWhseKey As Long
    Dim dQtyReq As Double
    Dim lTranType As Long
    Dim lInvtTranKey As Long
    Dim lRow As Long
    Dim lOvrdTrackMeth As Long
    Dim lCompOrigShipLineKey As Long
    
    lRow = grdCompItem.ActiveRow

    lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
    lUOMKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemUOMKey))
    lWhseKey = glGetValidLong(mlRcvgWhseKey)
    dQtyReq = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTotalQty))
    lTranType = glGetValidLong(kTranTypeSORT)
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemInvtTranKey))
    lCompOrigShipLineKey = lGetCompOrigShipLineKey(mlOrigShipLineKey, lItemKey)

    'If we are tied to an original shipment override the bin LookupID to non-track
    'because we only want to display bins in the lookup.  The specific lot/serial numbers
    'will be determined later within EditDistSOCustReturn.
    If lCompOrigShipLineKey <> 0 Then
        lOvrdTrackMeth = IMS_TRACK_METHD_NONE
    Else
        lOvrdTrackMeth = -1
    End If
    
    Set mcolAutoDistBinRetCols = Nothing
    'Call common routine to set up the lookup's restrict clause, LookupID, and UDF parameter.
    If moDist.bAutoDistBinLkuSetupClick(lkuAutoDistBin, lWhseKey, lItemKey, lTranType, lUOMKey, dQtyReq, lInvtTranKey, lOvrdTrackMeth) Then
        'This part will copy the text from the grid to the text box.
        'It will send the value to the filter section of the lookup to pre-filter the dataset.
        grdCompItem.Col = kColAutoDistBinID
        txtlkuAutoDistBin.Text = Trim(grdCompItem.Value)
        
        Set mcolAutoDistBinRetCols = gcLookupClick(Me, lkuAutoDistBin, txtlkuAutoDistBin, "Bin")
        
        If lkuAutoDistBin.ReturnColumnValues.count > 0 Then
            txtlkuAutoDistBin.Text = Trim$(lkuAutoDistBin.ReturnColumnValues("Bin"))
            If gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemTrackMeth) = kTM_Lot Then
                If gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColAutoDistBinID) = Trim$(lkuAutoDistBin.ReturnColumnValues("Bin")) Then
                    'User selected the same bin but likely a different lot.  Normally, the grid manager change
                    'event will not fire because the column value has not change but we will force it to fire here
                    'so the auto-distributions can be created.
                    moGM_CellChange grdCompItem.ActiveRow, kColAutoDistBinID
                End If
            End If
        Else
            Set mcolAutoDistBinRetCols = Nothing
            Exit Sub
        End If
    End If
    
    If Not moGM Is Nothing Then
        moGM.LookupClicked
    End If

End Sub

Private Sub moGM_CellChange(ByVal lRow As Long, ByVal lCol As Long)

    Dim lInvtTranKey As Long
    Dim lNextRowToDist As Long
    Dim sPrefWhseBinID As String
    Dim lPrefWhseBinKey As Long
    Dim lItemKey As Long
    Dim iTrackMeth As Integer
    
    Select Case lCol
        Case kColAutoDistBinID, kColSOCompItemQty
            If Not bIsValidAutoDistBin(lRow) Then Exit Sub
            AutoDistFromBinLku lRow
        Case kColSOCompItemID
            'Clear the auto-dist bin.
            gGridUpdateCell grdCompItem, lRow, kColAutoDist, "0"
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, ""
            
            lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
            iTrackMeth = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTrackMeth))
            
            'For put-away transactions or increase to inventory, we always default the preferred bin.
            moDist.GetPrefBinForInvtItem mlRcvgWhseKey, lItemKey, 1, sPrefWhseBinID, lPrefWhseBinKey, kTranTypeSORT
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, sPrefWhseBinID
                
            'If item is non-track, see if the auto-dist flag can be set.
            'If it is a lot-tracked, do not set flag because we don't know
            'the lot info yet.  User has to select it from the auto-bin lookup
            'or use the Dist UI.
            If lPrefWhseBinKey <> 0 And iTrackMeth = kTM_None Then
                If Not bIsValidAutoDistBin(lRow) Then Exit Sub
                AutoDistFromBinLku lRow
            End If
    End Select

End Sub

Private Sub moGM_GridBeforeDelete(bContinue As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lInvtTranKey As Long
        
    'Skip the delete if the grid is read-only
    If grdCompItem.DataRowCnt > 1 And (mbAllowAllMod Or mbAllowDecreaseOnly) Then
        'If this line was added to the component list after the form was invoked and is now being deleted,
        'it's not in the main temp table, so cancel the distributions now
        'instead of when the undo information is saved.
        lInvtTranKey = Val(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemInvtTranKey))
        If lInvtTranKey <> 0 Then
            If (moClass.moAppDB.Lookup("COUNT(*)", "#tsoCompItem", "InvtTranKey = " & lInvtTranKey)) <> 0 Then
                moDist.CancelDistributions lInvtTranKey
            End If
        End If
        
        gGridDeleteRow grdCompItem, grdCompItem.ActiveRow
        gbFormIsDirty = True
    ElseIf grdCompItem.DataRowCnt <= 1 Then
        'do not allow a kit with no component items
        giSotaMsgBox Me, moClass.moSysSession, kmsgKitComponent
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "moGM_GridBeforeDelete", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMMenuSelected(ctl As Object, lTaskID As Long)
    'This function handles the selection of context menu items appended to the menu in CMAppendContextMenu.

    Dim oDrillAround        As Object
    Dim sItemID             As String
    Dim sParam              As String
    
    On Error GoTo ExpectedErrorRoutine

    CMMenuSelected = False

    If ctl Is txtItemID Then

    Else
        Select Case lTaskID
            Case 100
                sItemID = Trim$(gsGridReadCellText(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemID))
                
                Screen.MousePointer = vbHourglass
                
                On Error Resume Next
                
                ' For Vista OS (to ensure task comes to foreground)
                Dim bRetVal As Boolean
                bRetVal = AllowSetForegroundWindow(ASFW_ANY)
                Err.Clear
                
                Set oDrillAround = moClass.moFramework.LoadSOTAObject(ktskIMItemMaint, kSOTA1RunFlags, kContextNormal)
                
                If (oDrillAround Is Nothing) Or Err.Number Then
                    On Error GoTo ExpectedErrorRoutine
                    GoTo BadLoad
                Else
                    Me.Enabled = False
                    oDrillAround.DrillAround sItemID
                    If Err.Number Then GoTo BadLoad
                    
                    On Error GoTo ExpectedErrorRoutine
                    Set oDrillAround = Nothing
                    Screen.MousePointer = vbDefault
                    Me.Enabled = True
                End If
        
            Case 10000 'for future use when we add the standard Item Context Menu Options
    
        End Select
    
    End If

    CMMenuSelected = True
    
    Exit Function
    
BadLoad:
        Screen.MousePointer = vbDefault
        Me.Enabled = True
        Me.SetFocus
        If Not (oDrillAround Is Nothing) Then
            Set oDrillAround = Nothing
        End If

        sParam = gsBuildString(kSOMaintainItem, moClass.moAppDB, moClass.moSysSession)
        giSotaMsgBox Me, moClass.moSysSession, kmsgErrorLoadTask, sParam
        
    Exit Function
    
ExpectedErrorRoutine:
    Exit Function
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON = 0 Then                                                                  'Repository Error Rig  {1.1.1.0.0}
    Err.Raise Err                                                                         'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CMMenuSelected", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function



Private Sub cmdDistribution_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lItemKey As Long
    Dim lInvtTranKey As Long
    Dim dDistQty As Double
    Dim lCompOrigShipLineKey As Long
    Dim lTranIdentifier As Long
    Dim bMultipleBin As Boolean
    Dim sWhseBinID As String
    Dim sLotNo As String
    Dim sLotExpDate As String
    Dim lWhseBinKey As Long
    Dim lUOMKey As Long
    Dim iTrackMeth As Integer
    
    Dim lPutAwayBinKey As Long
    Dim bUsePrefBin As Boolean
    
    lkuAutoDistBin.Visible = False
    
    If grdCompItem.ActiveRow = 0 Or grdCompItem.ActiveRow > grdCompItem.DataRowCnt Then
        Exit Sub
    End If
    
    'Should probably disable button if not on a valid row.
    If Trim(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemID)) = "" Then Exit Sub
    
    lItemKey = Trim(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemKey))
    If lItemKey = 0 Then Exit Sub
    lInvtTranKey = Val(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemInvtTranKey))
    
    If lInvtTranKey = 0 Then
        lInvtTranKey = lReserveInvtTranKey(grdCompItem.ActiveRow)
    End If
        
    lCompOrigShipLineKey = lGetCompOrigShipLineKey(mlOrigShipLineKey, lItemKey)
    lTranIdentifier = glGetValidLong(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemTranIdentifier))
    lPutAwayBinKey = glGetValidLong(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColAutoDistBinKey))
    lUOMKey = glGetValidLong(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemUOMKey))
    If lPutAwayBinKey = 0 And mbUsePrefBin = True Then
        bUsePrefBin = True
    End If
    
    dDistQty = moDist.EditDistSOCustReturn(kTranTypeSORT, lInvtTranKey, _
               lItemKey, mlRcvgWhseKey, _
               lUOMKey, _
               mdKitQty * Val(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemQty)), _
               lCompOrigShipLineKey, , lPutAwayBinKey, _
               bUsePrefBin, True, IIf(miLineStatus = kOpen, False, True), lTranIdentifier, mlKitShipLineKey)
        
    If moDist.oError.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, moDist.oError.MessageConstant
        moDist.oError.Clear
        Exit Sub
    Else
        If miLineStatus = kPosted Then
            gbFormIsDirty = False
        Else
            If moDist.CancelClicked = False Then
                'This stuff doesn't get saved to the temp table yet.
                gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColSOCompItemQtyDistributed, gsGetValidStr(dDistQty)
                gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColSOCompItemInvtTranKey, gsGetValidStr(lInvtTranKey)
                gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColSOCompItemTranIdentifier, gsGetValidStr(lTranIdentifier)
                
                iTrackMeth = giGetValidInt(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemTrackMeth))
                If iTrackMeth = kTM_None Or iTrackMeth = kTM_Lot Then
                    'If distributions are already saved, get the Bin ID of the saved dist value.
                    moDist.sGetBinLkuCtrlSavedDistValues lItemKey, lTranIdentifier, lInvtTranKey, bMultipleBin, sWhseBinID, lWhseBinKey, sLotNo, , sLotExpDate
                    If bMultipleBin Then
                        gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColAutoDistBinID, "Multiple"
                        txtlkuAutoDistBin.Text = ""
                    Else
                        gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColAutoDistBinID, sWhseBinID
                        gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColAutoDistBinKey, CStr(lWhseBinKey)
                    End If
                    
                    gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColAutoDistLotNo, sLotNo
                    gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColAutoDistLotExpDate, Format(sLotExpDate, gsGetLocalVBDateMask)
                    
                End If
            End If
            'Set the FormDirty flag so #tsoCompItem is recreated off the grid
            'and the InvtTranKey's will be saved.
            gbFormIsDirty = True
        End If
    End If
    
    'Set the row to the next likely row that will be distributed.
    gGridSetActiveCell grdCompItem, lGetNextRowToDistribute(grdCompItem.ActiveRow), kColSOCompItemQty
    If cmdDistribution.Enabled = True And cmdDistribution.Visible = True Then
        cmdDistribution.SetFocus
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "cmdDistribution_Click", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            If Shift = 0 And KeyCode = vbKeyF5 And grdCompItem.ActiveCol = kColSOCompItemID And _
                (mbAllowAllMod Or mbAllowDecreaseOnly) Then
                Call navGridKit_Click
            Else
                gProcessFKeys Me, KeyCode, Shift
            End If
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
            'Other KeyAscii routines.
    End Select


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'Save the form size variable to use for resize later
    miOldFormHeight = Me.Height
    miMinFormHeight = Me.Height
    miOldFormWidth = Me.Width
    miMinFormWidth = Me.Width
    
    'Set up form level variables
    msCompanyID = mfrmMain.oClass.moSysSession.CompanyId
    mbIMActivated = mfrmMain.oClass.moSysSession.IsModuleActivated(kModuleIM)
    
    'Set up toolbar
    tbrMain.LocaleID = mfrmMain.oClass.moSysSession.Language
    tbrMain.RemoveButton kTbPrint
    
    BindGM
    BindContextMenu
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim iRetVal As Integer
    
    'When user click Save or Cancel exit button from the toolbar, don't display msg box
    If UnloadMode = vbFormCode Or Not gbFormIsDirty Then Exit Sub
    
    iRetVal = giSotaMsgBox(Me, moClass.moSysSession, kmsgDMSaveChangesGen)
   
    Select Case iRetVal
        Case vbYes
            Call HandleToolbarClick(kTbFinishExit)
            
        Case vbNo
            Call HandleToolbarClick(kTbCancelExit)
            
        Case vbCancel
            Cancel = True
            Exit Sub
            
    End Select


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'Repository Error Rig  {1.1.1.0.0}

    'resize the form height and height all ctls on the screen (form height doesn't
    'get resize smaller than its original height)
    gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, grdCompItem
     
    'resize the form Width and Width all ctls on the screen (form Width doesn't
    'get resize smaller than its original Width)
    gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth
    
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdCompItem_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
       
    If mbGridDirty Then
          bValidateCell Col, Row
    End If
    
    moGM_CellChange Row, Col
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_Change", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Row > 0 Then
        If Col = kColSOCompItemID And (mbAllowAllMod Or mbAllowDecreaseOnly) Then
            navGridKit.Visible = True
        End If

        moGM.Grid_Click Col, Row
        'Enable the distribution button if needed.
        cmdDistribution.Enabled = bEnableCmdDistribution(Row)
    Else
        gGridSortByRow grdCompItem, Col
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_Click", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_EditChange(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sItemKey    As String
    Dim sItemID     As String
    
    sItemKey = Trim(gsGridReadCell(grdCompItem, Row, kColSOCompItemKey))
    sItemID = Trim(gsGridReadCell(grdCompItem, Row, kColSOCompItemID))
    
    'check for blank or empty item id on the Component Item column
    If sItemID = "" Then
        mbGridDirty = False

        'if there is no data in the Component Item column then no quantity should be entered
        If Col = kColSOCompItemQty Then gGridUpdateCell grdCompItem, Row, kColSOCompItemQty, ""
        
        If Row <> grdCompItem.MaxRows Then
            giSotaMsgBox Me, moClass.moSysSession, kIMKitCompValidate
            If sItemKey <> "" Then
                gGridUpdateCell grdCompItem, Row, kColSOCompItemID, msSelectItem
            End If
            gGridSetActiveCell grdCompItem, Row, kColSOCompItemID
        End If
    Else
        gbFormIsDirty = True
        mbGridDirty = True
    End If
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_EditChange", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub grdCompItem_EditMode(ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_EditMode Col, Row, Mode, ChangeMade
    Dim lItemKey As Long
        
    lItemKey = Val(gsGridReadCell(grdCompItem, Row, kColSOCompItemKey))
    msSelectItem = gsGetValidStr(moClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey=" & lItemKey))


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_EditMode", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_GotFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Scroll

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_GotFocus", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub grdCompItem_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'do not leave the cell unless user enter quantity greater than zero in the Qty/Kit UOM column
    If Col = kColSOCompItemQty And Trim(gsGridReadCell(grdCompItem, Row, Col)) <= 0 Then
        gGridSetActiveCell grdCompItem, Row, Col
        Exit Sub
    End If
    
'    'Only one empty row should be displayed at all times
'    If grdCompItem.MaxRows > Row And Trim(gsGridReadCell(grdCompItem, Row, Col)) = "" Then
'        grdCompItem.MaxRows = Row
'    End If
    
    moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
    
    'Enable the distribution button if needed.
    If Me.ActiveControl.Name = grdCompItem.Name Then
        cmdDistribution.Enabled = bEnableCmdDistribution(NewRow)
    End If
    
    'set the flag back to false after the cell has been updated
    mbGridDirty = False

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_LeaveCell", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_LeaveRow(ByVal Row As Long, ByVal RowWasLast As Boolean, ByVal RowChanged As Boolean, ByVal AllCellsHaveData As Boolean, ByVal NewRow As Long, ByVal NewRowIsLast As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    cmdDistribution.Enabled = bEnableCmdDistribution(NewRow)
    moGM.Grid_LeaveRow Row, NewRow
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_LeaveRow", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdCompItem_LostFocus()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lItemKey As Long
        
    lItemKey = Val(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemKey))
    msSelectItem = gsGetValidStr(moClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey =" & lItemKey))


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "grdCompItem_LostFocus", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navGridKit_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim lItemKey    As Long
    Dim sItemID     As String
        
     'Get the previous Item ID.
    lItemKey = Val(gsGridReadCell(grdCompItem, grdCompItem.ActiveRow, kColSOCompItemKey))
    sItemID = gsGetValidStr(moClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey =" & lItemKey))
    
    'This part will copy the text from the grid to the text box.
    'It also stores the search filter into a string for comparison to return from navigator.
    grdCompItem.Col = kColSOCompItemID
    txtNavReturnKit.Text = Trim(grdCompItem.Value)

    'Load the search screen.
    gcLookupClick Me, navGridKit, txtNavReturnKit, "ItemID"

    'Update the component item grid with the data selected from Search screen.
    If navGridKit.ReturnColumnValues.count <> 0 Then
        txtNavReturnKit.Text = navGridKit.ReturnColumnValues("ItemID")
        gGridUpdateCell grdCompItem, grdCompItem.ActiveRow, kColSOCompItemID, navGridKit.ReturnColumnValues("ItemID")
    End If
    
    'Do not update the grid if the same item is selected.
    If Trim(txtNavReturnKit.Text) = Trim(sItemID) Then
        mbGridDirty = False
    Else
        'Update the grid if the selected data is not the same as before.
        bValidateCell kColSOCompItemID, grdCompItem.ActiveRow
        'Calling moGM_CellChange instead of moGM.LookupClicked because the wrong column
        'is being passed into moGM_CellChange.  Decided to call it manually with correct column.
        moGM_CellChange grdCompItem.ActiveRow, kColSOCompItemID
        gbFormIsDirty = True
        mbGridDirty = True

    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "navGridKit_Click", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub LoadComp(lKitShipLineKey As Long, bUsePrefBin As Boolean, lPutAwayBinKey As Long, dCurrExchRate As Double, sCurrID As String, _
Optional dKitQty As Double = 0, Optional lKitItemKey As Long = 0, Optional lRcvgWhseKey As Long = 0, _
Optional lRMALineKey As Long = 0, Optional lSOLineKey As Long = 0, Optional sTranDate As String = "", _
Optional sCustReturnNo As String = "", Optional lOrigShipLineKey As Long = 0, Optional iLineStatus As Integer = kOpen)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim sSQL    As String
    Dim rs      As Object
    Dim lRow    As Long
    Dim bCustomAllowed As Boolean
    Dim bPartialAllowed As Boolean
    Dim arrInvtTranKeys() As Long
    Dim lCount As Long
    Dim lItemKey As Long
    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim bMultipleBin As Boolean
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim sPrefWhseBinID As String
    Dim lPrefWhseBinKey As Long
    Dim iTrackMeth As Integer
    Dim sLotNo As String
    Dim sLotExpDate As String
    
    gbFormIsDirty = False
    mbDataChanged = False
    mbAllowDecreaseOnly = False
    mbAllowAllMod = False
    mdCurrExchRate = dCurrExchRate
    msCurrID = sCurrID
    miCurrDigits = giGetValidInt(mfrmMain.oClass.moAppDB.Lookup("DigitsAfterDecimal", "tmcCurrency", "CurrID=" & gsQuoted(sCurrID)))
    miLineStatus = iLineStatus
    
    mlKitShipLineKey = lKitShipLineKey
    mbUsePrefBin = bUsePrefBin
    
    sSQL = "SELECT * FROM #tsoCustRtrnShipLine WITH (NOLOCK) WHERE ShipLineKey = " & mlKitShipLineKey & " AND ItemKey = " & lKitItemKey
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEmpty Then
        With rs
            .MoveFirst
            mdKitQty = dKitQty
            mlKitItemKey = glGetValidLong(.Field("ItemKey"))
            mlRcvgWhseKey = lRcvgWhseKey
            mlRMALineKey = glGetValidLong(.Field("RMALineKey"))
            mlSOLineKey = glGetValidLong(.Field("OrigSOLineKey"))
            msTranDate = gsGetValidStr(.Field("ShipDate"))
            msCustReturnNo = gsGetValidStr(.Field("CustReturnNo"))
            mlOrigShipLineKey = glGetValidLong(.Field("OrigShipLineKey"))
        End With
    Else
        mdKitQty = dKitQty
        mlKitItemKey = lKitItemKey
        mlRcvgWhseKey = lRcvgWhseKey
        mlRMALineKey = lRMALineKey
        mlSOLineKey = lSOLineKey
        msTranDate = sTranDate
        msCustReturnNo = sCustReturnNo
        mlOrigShipLineKey = lOrigShipLineKey
    End If
    rs.Close
    
    'This should really never happen -- the message is not meant for users.
    If mlKitItemKey = 0 Or mdKitQty = 0 Or mlRcvgWhseKey = 0 Or msTranDate = "" Or msCustReturnNo = "" Then
        MsgBox "Must have a kit item, kit quantity, rcvg whse, tran date, and return # passed or available in #tsoCustRtrnShipLine."
        Exit Sub
    End If
    
    mbTrackQtyAtBin = moClass.moAppDB.Lookup("TrackQtyAtBin", "timWarehouse WITH (NOLOCK)", "WhseKey=" & mlRcvgWhseKey)
    
    If mlRMALineKey <> 0 Then
        miSource = kSrcRMA
    ElseIf mlSOLineKey <> 0 Then
        miSource = kSrcSalesOrder
    Else
        miSource = kSrcNone
    End If
    
    sSQL = "SELECT * FROM timKit WHERE KitItemKey = " & mlKitItemKey
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    With rs
        .MoveFirst
        bCustomAllowed = gbGetValidBoolean(.Field("AllowRtrnsCustomBTO"))
        bPartialAllowed = gbGetValidBoolean(.Field("AllowRtrnsPartialBTO"))
        .Close
    End With
    
    'Setup the Module Options Class.
    If moOptions Is Nothing Then
        Set moOptions = New clsModuleOptions
        Set moOptions.oAppDB = moClass.moAppDB
        Set moOptions.oSysSession = moClass.moSysSession
        moOptions.sCompanyID = mfrmMain.oClass.moSysSession.CompanyId
        'Assign the number of Decimal places used in FormatGrid.
        miQtyDP = giGetValidInt(moOptions.CI("QtyDecPlaces"))
    End If
    
    If Not bPartialAllowed Or iLineStatus = kPosted Then
        mbAllowAllMod = False
    ElseIf (Not bCustomAllowed And bPartialAllowed) Or miSource = kSrcSalesOrder Or miSource = kSrcRMA Then
        mbAllowDecreaseOnly = True
    Else 'Kit allows both partial and custom returns, and is manual
        mbAllowAllMod = True
    End If
    
    FormatGrid
     
    txtItemID.Text = gsGetValidStr(moClass.moAppDB.Lookup("ItemID", "timItem", "ItemKey =" & mlKitItemKey))
    txtItemDesc.Text = gsGetValidStr(moClass.moAppDB.Lookup("ShortDesc", "timItemDescription", "ItemKey=" & mlKitItemKey))
    txtKitUOM.Text = gsGetValidStr(moClass.moAppDB.Lookup("UnitMeasID", "tciUnitMeasure, timItem", "tciUnitMeasure.UnitMeasKey=timItem.StockUnitMeasKey AND timItem.ItemKey=" & mlKitItemKey))
    txtKitQty.Text = CStr(mdKitQty)
        
    'load the current contents of the kit from the temp table - this should have been populated by the main form.
    sSQL = "SELECT DISTINCT a.CompItemShipLineKey, a.CompItemOrigShipLineKey, a.CompItemKey, a.CompItemType, a.TrackMeth, b.AllowDecimalQty, b.ItemID, c.ShortDesc, " & _
                            "a.CompItemQty, d.UnitMeasID, b.StockUnitMeasKey, e.CompItemQty StdQty, f.CompItemQty SOLineQty, " & _
                            "g.CompItemQty RMALineQty, a.DistQty, a.InvtTranKey, a.TranIdentifier " & _
                    " FROM #tsoCompItem a WITH (NOLOCK) " & _
                    " JOIN timItem b WITH (NOLOCK) ON b.ItemKey = a.CompItemKey " & _
                    " JOIN timItemDescription c WITH (NOLOCK) ON c.ItemKey = b.ItemKey " & _
                    " JOIN tciUnitMeasure d WITH (NOLOCK) ON d.UnitMeasKey = b.StockUnitMeasKey " & _
                    " LEFT OUTER JOIN timKitCompList e WITH (NOLOCK) ON e.CompItemKey = b.ItemKey " & _
                                                        " AND e.KitItemKey = " & mlKitItemKey & " " & _
                    " LEFT OUTER JOIN tsoSOLineCompItem f WITH (NOLOCK) ON f.CompItemKey = b.ItemKey " & _
                                                        " AND f.SOLineKey = " & mlSOLineKey & " " & _
                    " LEFT OUTER JOIN tsoRMALineCompItem g WITH (NOLOCK) ON g.CompItemKey = b.ItemKey " & _
                                                        " AND g.RMALineKey = " & mlRMALineKey & _
                    " WHERE a.KitShipLineKey=" & mlKitShipLineKey
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    rs.MoveFirst
    While Not rs.IsEOF
        lRow = lRow + 1
        If Not mbAllowDecreaseOnly And Not mbAllowAllMod Then
            gGridSetMaxRows grdCompItem, lRow
        Else
            gGridSetMaxRows grdCompItem, lRow + 1
        End If
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemKey, gsGetValidStr(rs.Field("CompItemKey"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemID, gsGetValidStr(rs.Field("ItemID"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemDesc, gsGetValidStr(rs.Field("ShortDesc"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemUOM, gsGetValidStr(rs.Field("UnitMeasID"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemUOMKey, gsGetValidStr(glGetValidLong(rs.Field("StockUnitMeasKey")))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemQty, gsGetValidStr(rs.Field("CompItemQty"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemTotalQty, gsGetValidStr((rs.Field("CompItemQty")) * mdKitQty)
        If giGetValidInt(rs.Field("CompItemType")) = IMS_MISC_ITEM Then
            gGridUpdateCell grdCompItem, lRow, kColSOCompItemQtyDistributed, gsGridReadCell(grdCompItem, lRow, kColSOCompItemTotalQty)
        Else
            gGridUpdateCell grdCompItem, lRow, kColSOCompItemQtyDistributed, gsGetValidStr((rs.Field("DistQty")))
        End If
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemStdQty, gsGetValidStr(rs.Field("StdQty"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemSOLineQty, gsGetValidStr(rs.Field("SOLineQty"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemRMALineQty, gsGetValidStr(rs.Field("RMALineQty"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemInvtTranKey, gsGetValidStr(rs.Field("InvtTranKey"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemTranIdentifier, gsGetValidStr(rs.Field("TranIdentifier"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemCompItemShipLineKey, gsGetValidStr(rs.Field("CompItemShipLineKey"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemCompItemOrigShipLineKey, gsGetValidStr(rs.Field("CompItemOrigShipLineKey"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemType, gsGetValidStr(rs.Field("CompItemType"))
        gGridUpdateCell grdCompItem, lRow, kColSOCompItemTrackMeth, gsGetValidStr(rs.Field("TrackMeth"))
        
        If gsGetValidStr(rs.Field("AllowDecimalQty")) = 0 Then
            gGridSetCellType grdCompItem, lRow, kColSOCompItemQty, SS_CELL_TYPE_FLOAT, 0
        End If
        
        lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
        'Lock the bin auto-distribution cell if the item does not support auto distribution.
        If moDist.bEnableBinLkuCtrl(mlRcvgWhseKey, lItemKey) = False Then
            gGridLockCell grdCompItem, kColAutoDistBinID, lRow
        End If
        
        'If distributions are already saved, get the Bin ID of the saved dist value.
        lTranIdentifier = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTranIdentifier))
        lInvtTranKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemInvtTranKey))
        iTrackMeth = giGetValidInt(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTrackMeth))
        
        If bUsePrefBin Then
            GetPreferredBin lPrefWhseBinKey, sPrefWhseBinID, lItemKey, mlRcvgWhseKey
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, sPrefWhseBinID
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinKey, CStr(lPrefWhseBinKey)
        ElseIf lPutAwayBinKey <> 0 Then
            sWhseBinID = moClass.moAppDB.Lookup("WhseBinID", "timWhseBin WITH (NOLOCK)", "WhseBinKey = " & lPutAwayBinKey)
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, sWhseBinID
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinKey, CStr(lPutAwayBinKey)
        End If
        
        moDist.sGetBinLkuCtrlSavedDistValues lItemKey, lTranIdentifier, lInvtTranKey, bMultipleBin, sWhseBinID, lWhseBinKey, sLotNo, , sLotExpDate
        
        gGridUpdateCell grdCompItem, lRow, kColAutoDistLotNo, sLotNo
        gGridUpdateCell grdCompItem, lRow, kColAutoDistLotExpDate, Format(sLotExpDate, gsGetLocalVBDateMask)
        
        If bMultipleBin Then
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, "Multiple"
            txtlkuAutoDistBin.Text = ""
        ElseIf glGetValidLong(lWhseBinKey) <> 0 Then
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, sWhseBinID
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinKey, CStr(lWhseBinKey)
        Else
            'Distributions are not saved.
            'If item is non-track, see if the auto-dist flag can be set.
            'If it is a lot-tracked, do not set flag because we don't know
            'the lot info yet.  User has to select it from the auto-bin lookup
            'or use the Dist UI.
            If Len(Trim(gsGridReadCell(grdCompItem, lRow, kColAutoDistBinID))) <> 0 And iTrackMeth = kTM_None Then
                If bIsValidAutoDistBin(lRow) Then
                    AutoDistFromBinLku lRow
                End If
            End If
        End If
        
        rs.MoveNext
    Wend
    
    rs.Close
    
    'Insert the contents of the #tsoCompItem (each time the user enters the
    'Component form) into a new table called #tsoCompItemAtLoadComp.  This
    'data will be used to cancel and/or restore the component list
    'distributions when the user cancels from the Component form.
    sSQL = "DELETE #tsoCompItemAtLoadComp WHERE KitShipLineKey = " & lKitShipLineKey & ";" & _
                "INSERT #tsoCompItemAtLoadComp " & _
                "SELECT * FROM #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey

    moClass.moAppDB.ExecuteSQL sSQL

    'Remember Distribution information to and from the Component form into a
    'secondary temp table that copies the contents of #timInvtDistWrk.
    sSQL = "SELECT InvtTranKey FROM #tsoCompItem WHERE KitShipLineKey = " & mlKitShipLineKey & " AND InvtTranKey <> 0"
            
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEmpty Then
        rs.MoveFirst
        lCount = -1
        While Not rs.IsEOF
            lCount = lCount + 1
            ReDim Preserve arrInvtTranKeys(lCount)
            arrInvtTranKeys(lCount) = rs.Field("InvtTranKey")
            rs.MoveNext
        Wend
        moDist.bRememberDistInfo arrInvtTranKeys, kSecondaryDistTmp
    End If
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If

    SetupComponentNavigator navGridKit, moClass, miSource, msCompanyID, mlRcvgWhseKey, mlSOLineKey, mlRMALineKey
    
    'Set the row to the next likely row that will be distributed.
    If grdCompItem.DataRowCnt > 0 Then
        gGridSetActiveCell grdCompItem, lGetNextRowToDistribute(1), kColSOCompItemQty
        If cmdDistribution.Enabled = True And cmdDistribution.Visible = True Then
            cmdDistribution.SetFocus
        End If
    End If
   
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "LoadComp", VBRIG_IS_FORM                               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    moGM.UnloadSelf
    moGM.UnloadSelf
    Set moGM = Nothing
    Set moGM = Nothing
    Set moContextMenu = Nothing
    Set moOptions = Nothing
    

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM                            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    HandleToolbarClick Button


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim sSQL                As String
    Dim i                   As Long
    Dim rs                  As Object
    Dim arrInvtTranKeys()   As Long
    Dim lCount              As Long
    Dim lInvtTranKey        As Long
    Dim lTranIdentifier     As Long
    Dim iCompItemType       As Integer
    
    

    Select Case sKey
        
        Case kTbFinishExit
            'only process the save code when the data in the form got modified
            If gbFormIsDirty Then
                
                mbDataChanged = True
                          
                'Cancel any distribution that are in #tsoCompItemUndo but are no longer in #tsoCompItem.
                sSQL = "SELECT InvtTranKey FROM #tsoCompItemUndo WHERE KitShipLineKey = " & mlKitShipLineKey & _
                            " AND InvtTranKey NOT IN (SELECT InvtTranKey FROM #tsoCompItem WHERE KitShipLineKey = " & mlKitShipLineKey & ") AND" & _
                            " InvtTranKey <> 0"
                        
                Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
                If Not rs.IsEmpty Then
                    rs.MoveFirst
                    lCount = -1
                    While Not rs.IsEOF
                        lCount = lCount + 1
                        ReDim Preserve arrInvtTranKeys(lCount)
                        arrInvtTranKeys(lCount) = rs.Field("InvtTranKey")
                        rs.MoveNext
                    Wend
                    moDist.CancelDistributions arrInvtTranKeys
                End If
                rs.Close
                Set rs = Nothing
                
                'Delete all rows for the selected KitShipLineKey and re-insert the values as displayed
                'on the grid if the grid is dirty
                sSQL = "DELETE #tsoCompItem WHERE KitShipLineKey = " & mlKitShipLineKey
                moClass.moAppDB.ExecuteSQL sSQL
                
                sSQL = ""
                For i = 1 To grdCompItem.DataRowCnt
                    'Get the InvtTranKey.  Note: Non-Inventory items will not have an InvtTranKey.
                    lInvtTranKey = Val(gsGridReadCell(grdCompItem, i, kColSOCompItemInvtTranKey))
                    lTranIdentifier = Val(gsGridReadCell(grdCompItem, i, kColSOCompItemTranIdentifier))
                    iCompItemType = Val(gsGridReadCell(grdCompItem, i, kColSOCompItemType))
                    sSQL = sSQL & " INSERT #tsoCompItem (KitShipLineKey, BTOKitItemKey, CompItemShipLineKey, CompItemOrigShipLineKey, CompItemKey, CompItemType, TrackMeth, CompItemQty, InvtTranKey, TranIdentifier, DistQty)" & _
                            " VALUES ( " & mlKitShipLineKey & "," & _
                            mlKitItemKey & "," & _
                            Val(gsGridReadCell(grdCompItem, i, kColSOCompItemCompItemShipLineKey)) & "," & _
                            Val(gsGridReadCell(grdCompItem, i, kColSOCompItemCompItemOrigShipLineKey)) & "," & _
                            Val(gsGridReadCell(grdCompItem, i, kColSOCompItemKey)) & "," & _
                            Val(gsGridReadCell(grdCompItem, i, kColSOCompItemType)) & "," & _
                            Val(gsGridReadCell(grdCompItem, i, kColSOCompItemTrackMeth)) & "," & _
                            Val(gsGridReadCell(grdCompItem, i, kColSOCompItemQty)) & "," & _
                            IIf(lInvtTranKey = 0, "NULL", lInvtTranKey) & "," & _
                            IIf(lTranIdentifier = 0, "NULL", lTranIdentifier) & "," & _
                            IIf(iCompItemType = IMS_MISC_ITEM, 0, Val(gsGridReadCell(grdCompItem, i, kColSOCompItemQtyDistributed))) & ")"
                    
                    If i <> grdCompItem.DataRowCnt Then
                        sSQL = sSQL & " ; "
                    End If
                Next i
    
                moClass.moAppDB.ExecuteSQL sSQL
                
                'Get the price of the kit
                mdTotalPrice = dGetBTOKitPrice(mlKitItemKey, mlKitShipLineKey, mlOrigShipLineKey, mlRMALineKey)
                
            End If
            
            Me.Hide

        Case kTbCancelExit
            'Cancel any new distributions that in #tsoCompItem that are
            'not in #tsoCompItemAtLoadComp.
            sSQL = "SELECT InvtTranKey FROM #tsoCompItem WHERE" & _
                    " NOT EXISTS (SELECT 1 FROM #tsoCompItemAtLoadComp WHERE" & _
                    " #tsoCompItem.KitShipLineKey = #tsoCompItemAtLoadComp.KitShipLineKey" & _
                    " AND #tsoCompItem.InvtTranKey = #tsoCompItemAtLoadComp.InvtTranKey)" & _
                    " AND KitShipLineKey = " & mlKitShipLineKey & " AND InvtTranKey <> 0"
                    

            Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            If Not rs.IsEmpty Then
                rs.MoveFirst
                lCount = -1
                While Not rs.IsEOF
                    lCount = lCount + 1
                    ReDim Preserve arrInvtTranKeys(lCount)
                    arrInvtTranKeys(lCount) = rs.Field("InvtTranKey")
                    rs.MoveNext
                Wend
                moDist.CancelDistributions arrInvtTranKeys
            End If
            rs.Close
                
            'Restore distributions that were in #tsoCompItemAtLoadComp.
            sSQL = "SELECT InvtTranKey FROM #tsoCompItem WHERE" & _
                    " EXISTS (SELECT 1 FROM #tsoCompItemAtLoadComp WHERE" & _
                    " #tsoCompItem.KitShipLineKey = #tsoCompItemAtLoadComp.KitShipLineKey" & _
                    " AND #tsoCompItem.InvtTranKey = #tsoCompItemAtLoadComp.InvtTranKey)" & _
                    " AND KitShipLineKey = " & mlKitShipLineKey & " AND InvtTranKey <> 0"


            Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
            If Not rs.IsEmpty Then
                rs.MoveFirst
                lCount = -1
                While Not rs.IsEOF
                    lCount = lCount + 1
                    ReDim Preserve arrInvtTranKeys(lCount)
                    arrInvtTranKeys(lCount) = rs.Field("InvtTranKey")
                    rs.MoveNext
                Wend
                moDist.bRestoreDistInfo arrInvtTranKeys, kSecondaryDistTmp
            End If
            rs.Close
            
            Me.Hide
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
            'Exit Sub
            
    End Select
    


'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "HandleToolbarClick", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub UndoKitEdits(lKitShipLineKey As Long, lOrigBTOKitItemKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'This sub is for restoring the kit component list to its state prior to the last time the
'finish/exit button was clicked.  It replaces the main temp table's contents with the contents of
'the Undo temp table for a particular return line.  Primarily this is called when the user has entered
'through the Detail form, clicked Finish/Exit in the Kit Components window, and then hits Cancel/Exit
'on the Detail form.
    Dim sSQL As String
    Dim rs As Object
    Dim lCount As Long
    Dim arrInvtTranKeys() As Long
    
    If moClass Is Nothing Then
        Set moClass = oClass
    End If
    
    'Cancel pending dists/delete saved dists for rows that are in #tsoCompItem but not in #tsoCompItemUndo
    sSQL = "SELECT InvtTranKey FROM #tsoCompItem WHERE KitShipLineKey = " & mlKitShipLineKey & _
" AND InvtTranKey NOT IN (SELECT InvtTranKey FROM #tsoCompItemUndo WHERE KitShipLineKey = " & mlKitShipLineKey & ") AND" & _
" InvtTranKey <> 0"
                        
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEmpty Then
        rs.MoveFirst
        lCount = -1
        While Not rs.IsEOF
            lCount = lCount + 1
            ReDim Preserve arrInvtTranKeys(lCount)
            arrInvtTranKeys(lCount) = rs.Field("InvtTranKey")
            rs.MoveNext
        Wend
        moDist.CancelDistributions arrInvtTranKeys
        moDist.DeleteDistSOCustRet arrInvtTranKeys
        
        If moDist.oError.Number <> 0 Then
            giSotaMsgBox Me, moClass.moSysSession, moDist.oError.MessageConstant
            moDist.oError.Clear
            Exit Sub
        End If
        
    End If
    rs.Close
    Set rs = Nothing

    'Restore the components list and distribution info from the #tsoCompItemUndo table.
    '#tsoCompItemUndo is loaded when the user re-enters the Detail Form for a BTO Item
    'after it had already saved some data.  It is also loaded within the context menu
    'after a Save&Exit from the Component form.  --GT verify the context menu.
    sSQL = "DELETE #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey & ";" & _
            "INSERT #tsoCompItem " & _
            "SELECT * FROM #tsoCompItemUndo WHERE KitShipLineKey = " & lKitShipLineKey & " AND BTOKitItemKey = " & lOrigBTOKitItemKey & ";" & _
            "DELETE #tsoCompItemUndo WHERE KitShipLineKey = " & lKitShipLineKey & " AND BTOKitItemKey IN (" & lOrigBTOKitItemKey & "," & mlKitItemKey & ")" & ";" & _
            "DELETE #tsoCompItemAtLoadComp WHERE KitShipLineKey = " & lKitShipLineKey & " AND BTOKitItemKey IN (" & lOrigBTOKitItemKey & "," & mlKitItemKey & ")"
    moClass.moAppDB.ExecuteSQL sSQL

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "UndoKitEdits", VBRIG_IS_FORM                           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub DeleteLineUndoInfo(lKitShipLineKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'This sub is for discarding the info in the undo temp table for a particular return line.
'If the window was accessed from the detail form, the undo info can be discarded when the user
'exits the detail form.  If the window was accessed from the main form, the undo info can be
'discarded when the user returns to the main form.
    Dim sSQL As String
    Dim rs As Object
    Dim lCount As Long
    Dim arrInvtTranKeys() As Long
    Dim lInvtTranKey As Long
    Dim i As Long

    'Cancel pending distributions and delete saved distributions for inventory tran keys found in
    '#tsoCompItemUndo but not in #tsoCompItem.
    sSQL = "SELECT InvtTranKey FROM #tsoCompItemUndo WHERE KitShipLineKey = " & mlKitShipLineKey & _
            " AND InvtTranKey NOT IN (SELECT InvtTranKey FROM #tsoCompItem WHERE KitShipLineKey = " & mlKitShipLineKey & ") AND" & _
            " InvtTranKey <> 0"
            
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEmpty Then
        rs.MoveFirst
        lCount = -1
        While Not rs.IsEOF
            lCount = lCount + 1
            ReDim Preserve arrInvtTranKeys(lCount)
            arrInvtTranKeys(lCount) = rs.Field("InvtTranKey")
            rs.MoveNext
        Wend
        moDist.CancelDistributions arrInvtTranKeys
        moDist.DeleteDistSOCustRet arrInvtTranKeys
        
        If moDist.oError.Number <> 0 Then
            giSotaMsgBox Me, moClass.moSysSession, moDist.oError.MessageConstant
            moDist.oError.Clear
            Exit Sub
        End If
        
    End If
    rs.Close
    Set rs = Nothing
    
    sSQL = "DELETE #tsoCompItemUndo WHERE KitShipLineKey = " & lKitShipLineKey & ";" & _
            "DELETE #tsoCompItemAtLoadComp WHERE KitShipLineKey = " & lKitShipLineKey
    moClass.moAppDB.ExecuteSQL sSQL

    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DeleteLineUndoInfo", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub InitAllCompLists(lShipKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'For previously saved Returns that are being viewed/edited.
Dim sSQL As String
Dim rs As Object

sSQL = "SELECT a.ShipLineKey FROM #tsoCustRtrnShipLine a WITH (NOLOCK), timItem b WITH (NOLOCK) " & _
"WHERE a.ItemKey = b.ItemKey AND a.ShipKey = " & lShipKey & " AND b.ItemType = " & kTypeBTOKit

Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)

rs.MoveFirst
While Not rs.IsEOF
    bInitCompList rs.Field("ShipLineKey")
    rs.MoveNext
Wend

rs.Close
Set rs = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "InitAllCompLists", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Sub ClearAllKitEdits(lShipKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'This should be called in order to clear data from both the main kit component list temp table and the
'undo temp table for a specific return.  Necessary to call this if the main form's Cancel, Delete, Finish
'or Next Number button is pressed (anything that clears the current record from the form).
    Dim sSQL As String
    sSQL = "DELETE FROM #tsoCompItem ; " & _
            "DELETE FROM #tsoCompItemUndo ; " & _
            "DELETE FROM #tsoCompItemAtLoadComp ; " & _
            "DELETE FROM #tsoOrigCompItemList ; " & _
            "DELETE FROM #timInvtDistWrk ; " & _
            "DELETE FROM #timDeleteSOCustRetDist"
    moClass.moAppDB.ExecuteSQL sSQL
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ClearAllKitEdits", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub ClearCompList(lKitShipLineKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'This should be called in order to clear data from both the main kit component list temp table
'for a specific Rtrn Line.  Necessary to call this if a line gets deleted (or its item changed,
'if it was a BTO kit) from a Return.
    Dim sSQL As String
    
    'Delete records from the distribution temp table using all
    'of the temp tables that store component distribution info.
    sSQL = "DELETE #timInvtDistWrk FROM #timInvtDistWrk idw WITH (NOLOCK) " & _
            "JOIN #tsoCompItem ci WITH (NOLOCK) ON idw.TranKey = ci.InvtTranKey " & _
            "WHERE ci.KitShipLineKey = " & lKitShipLineKey
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "DELETE #timInvtDistWrk FROM #timInvtDistWrk idw WITH (NOLOCK) " & _
            "JOIN #tsoCompItemUndo ciu WITH (NOLOCK) ON idw.TranKey = ciu.InvtTranKey " & _
            "WHERE ciu.KitShipLineKey = " & lKitShipLineKey
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "DELETE #timInvtDistWrk FROM #timInvtDistWrk idw WITH (NOLOCK) " & _
            "JOIN #tsoCompItemAtLoadComp cilc WITH (NOLOCK) ON idw.TranKey = cilc.InvtTranKey " & _
            "WHERE cilc.KitShipLineKey = " & lKitShipLineKey
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "DELETE #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey & ";" '& _
           '"DELETE #tsoCompItemUndo WHERE KitShipLineKey = " & lKitShipLineKey & ";" & _
'"DELETE #tsoCompItemAtLoadComp WHERE KitShipLineKey = " & lKitShipLineKey

    moClass.moAppDB.ExecuteSQL sSQL
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ClearCompList", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Public Function bInitCompList(lKitShipLineKey As Long, Optional lKitItemKey As Long = 0, Optional dCurrExchRate As Double = 1) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'This should be called directly only when a new line is being added to a Return.
    'It includes the logic to default or initialize the kit component list for the
    'Return line from the correct source.
    '(For pulling up an existing Return for editing, call InitAllCompLists.)
    Dim bCustomAllowed As Boolean
    Dim rs As Object
    Dim sSQL As String
    Dim lRMALineKey As Long
    Dim lSOLineKey As Long
    Dim lBTOKitOrigShipLineKey As Long
    Dim lBTOCompOrigShipLineKey As Long
    
    mdCurrExchRate = dCurrExchRate
    
    'If there is are already records for Kit's ShipLineKey in the component temp table,
    'exit the function with a success.
    If moClass.moAppDB.Lookup("COUNT(1)", "#tsoCompItem", "KitShipLineKey = " & lKitShipLineKey) <> 0 Then
        bInitCompList = True
        Exit Function
    End If
    
    'Reset Kit variables
    mbDataChanged = False
    mbCustomizedBTO = False
    mdTotalPrice = 0
    
    sSQL = "SELECT * FROM #tsoCustRtrnShipLine WITH (NOLOCK) WHERE ShipLineKey = " & lKitShipLineKey & " AND ItemKey = " & lKitItemKey
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEmpty Then
        With rs
            .MoveFirst
            lKitItemKey = glGetValidLong(.Field("ItemKey"))
            lRMALineKey = glGetValidLong(.Field("RMALineKey"))
            lSOLineKey = glGetValidLong(.Field("OrigSOLineKey"))
            .Close
        End With
    End If
    
    'This shouldn't ever happen -- the message isn't meant for users.
    If lKitItemKey = 0 Then
        MsgBox "Must pass a kit item key or have a line in #tsoCustRtrnShipLine for the ship line w/ item key"
        'lKitItemKey = 165
        Exit Function
    End If
        
    mlKitItemKey = lKitItemKey
    
    bCustomAllowed = gbGetValidBoolean(moClass.moAppDB.Lookup("AllowRtrnsCustomBTO", "timKit", "KitItemKey=" & lKitItemKey))
    
    'First check if Return lines exists.  Use the DistQty from #timInvtDistWrk.  If a row does not exist,
    'then get the DistQty from timInvtTranDist.
    sSQL = "SELECT crsl.ItemKey 'CompItemKey', crsl.ItemType 'CompItemType', i.TrackMeth, ISNULL(crsl.ShipLineKey, 0) 'CompItemShipLineKey', " & _
           "CASE WHEN ParentKit.ReturnQty = 0 THEN crsl.ReturnQtyInStockUOM " & _
                "ELSE crsl.ReturnQtyInStockUOM / ParentKit.ReturnQty END'CompItemQty', crsl.InvtTranKey, " & _
            "CASE WHEN drv2.StockQty IS NULL THEN drv1.DistQty ELSE drv2.StockQty END 'DistQty' " & _
            "FROM #tsoCustRtrnShipLine crsl WITH (NOLOCK) " & _
            "JOIN #tsoCustRtrnShipLine ParentKit WITH (NOLOCK) ON crsl.KitShipLineKey = ParentKit.ShipLineKey " & _
            "JOIN timItem i WITH (NOLOCK) ON crsl.ItemKey = i.ItemKey " & _
            "JOIN (SELECT itd.InvtTranKey, SUM(itd.DistQty) 'DistQty' " & _
            "FROM #tsoCustRtrnShipLine WITH (NOLOCK), timInvtTranDist itd WITH (NOLOCK) " & _
            "WHERE #tsoCustRtrnShipLine.InvtTranKey = itd.InvtTranKey " & _
            "GROUP BY itd.InvtTranKey) drv1 " & _
            "ON crsl.InvtTranKey = drv1.InvtTranKey " & _
            "LEFT OUTER JOIN (SELECT TranKey, SUM(StockQty) 'StockQty' " & _
            "FROM #tsoCustRtrnShipLine WITH (NOLOCK), #timInvtDistWrk idw WITH (NOLOCK) " & _
            "WHERE #tsoCustRtrnShipLine.InvtTranKey = idw.TranKey " & _
            "GROUP BY idw.TranKey) drv2 " & _
            "ON crsl.InvtTranKey = drv2.TranKey " & _
            "WHERE crsl.KitShipLineKey = " & lKitShipLineKey
                ' Join any non-inventory items that part of the BTO Kit.  (Only Misc. item types can be added to BTO Kits.)
                sSQL = sSQL & " UNION "
                sSQL = sSQL & "SELECT crsl.ItemKey 'CompItemKey', crsl.ItemType 'CompItemType', i.TrackMeth, ISNULL(crsl.ShipLineKey, 0) 'CompItemShipLineKey', " & _
                       "CASE WHEN ParentKit.ReturnQty = 0 THEN crsl.ReturnQtyInStockUOM " & _
            "ELSE crsl.ReturnQtyInStockUOM / ParentKit.ReturnQty END'CompItemQty', ISNULL(crsl.InvtTranKey, 0) 'InvtTranKey', " & _
                       "crsl.ReturnQtyInStockUOM 'DistQty' " & _
            "FROM #tsoCustRtrnShipLine crsl WITH (NOLOCK) " & _
            "JOIN #tsoCustRtrnShipLine ParentKit WITH (NOLOCK) ON crsl.KitShipLineKey = ParentKit.ShipLineKey " & _
            "JOIN timItem i WITH (NOLOCK) ON crsl.ItemKey = i.ItemKey " & _
            "WHERE crsl.KitShipLineKey = " & lKitShipLineKey & _
            " AND crsl.ItemType IN (" & IMS_MISC_ITEM & ")"
           
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If rs.IsEmpty Then 'No existing entries for this return line.  Default.
        If lRMALineKey <> 0 Then
            sSQL = "SELECT CompItemKey, timItem.ItemType 'CompItemType', timItem.TrackMeth, 0 CompItemShipLineKey, ItemID, CompItemQty, 0 InvtTranKey, 0 DistQty FROM tsoRMALineCompItem WITH (NOLOCK), " & _
                    "timItem WITH (NOLOCK) WHERE timItem.ItemKey = tsoRMALineCompItem.CompItemKey AND " & _
                    "RMALineKey = " & lRMALineKey
        
        Else
            If lSOLineKey <> 0 Then 'This is from an existing SO line -- look up what was shipped.
                'If the BTO is linked to a sales order and has been customized, deny the return.
                If Not bCustomAllowed And (gbGetValidBoolean(moClass.moAppDB.Lookup("CustomBTOKit", "tsoSOLine", "SOLineKey = " & lSOLineKey))) Then
                    Exit Function
                End If
                
                sSQL = "SELECT CompItemKey, timItem.ItemType 'CompItemType', timItem.TrackMeth, 0 CompItemShipLineKey, ItemID, CompItemQty, 0 InvtTranKey, 0 DistQty FROM tsoSOLineCompItem WITH (NOLOCK), " & _
                        "timItem WITH (NOLOCK) WHERE timItem.ItemKey = tsoSOLineCompItem.CompItemKey AND " & _
                        "SOLineKey = " & lSOLineKey
            Else 'This is a manual line -- look up the standard build.
                sSQL = "SELECT CompItemKey, timItem.ItemType 'CompItemType', timItem.TrackMeth, 0 CompItemShipLineKey, ItemID, CompItemQty, 0 InvtTranKey, 0 DistQty FROM timKitCompList WITH (NOLOCK), " & _
                        "timItem WITH (NOLOCK) WHERE timItem.ItemKey = timKitCompList.CompItemKey AND " & _
                        "KitItemKey = " & lKitItemKey & " ORDER BY SeqNo"
                
            End If
        End If
        rs.Close
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    End If

    'Load the #tsoCompItem.
    rs.MoveFirst
    While Not rs.IsEOF
    
        lBTOKitOrigShipLineKey = glGetValidLong(moClass.moAppDB.Lookup("OrigShipLineKey", "#tsoCustRtrnShipLine crsl WITH (NOLOCK), timItem i WITH (NOLOCK)", "crsl.ItemKey = i.ItemKey AND i.ItemType = 7 AND ShipLineKey = " & lKitShipLineKey))
        lBTOCompOrigShipLineKey = glGetValidLong(lGetCompOrigShipLineKey(lBTOKitOrigShipLineKey, rs.Field("CompItemKey")))
        
        sSQL = " INSERT #tsoCompItem (KitShipLineKey, BTOKitItemKey, CompItemShipLineKey, CompItemOrigShipLineKey, CompItemKey, CompItemType, TrackMeth, CompItemQty, InvtTranKey, DistQty) " & _
                " VALUES ( " & lKitShipLineKey & ", " & _
                lKitItemKey & ", " & _
                rs.Field("CompItemShipLineKey") & ", " & _
                lBTOCompOrigShipLineKey & ", " & _
                rs.Field("CompItemKey") & ", " & _
                rs.Field("CompItemType") & ", " & _
                rs.Field("TrackMeth") & ", " & _
                rs.Field("CompItemQty") & ", " & _
                rs.Field("InvtTranKey") & ", " & _
                rs.Field("DistQty") & ")"
        moClass.moAppDB.ExecuteSQL sSQL
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    
    'Filter out any component whose Allow Returns flag is false.  If one is detected
    'get the price of the component and set the customized bto flag.
    If moClass.moAppDB.Lookup("COUNT(*)", "#tsoCompItem ci WITH (NOLOCK), timItem i WITH (NOLOCK)", "ci.CompItemKey = i.ItemKey AND ci.KitShipLineKey = " & lKitShipLineKey & " AND i.AllowRtrns = 0") > 0 Then
        moClass.moAppDB.ExecuteSQL "DELETE #tsoCompItem FROM #tsoCompItem ci JOIN timItem i WITH (NOLOCK) ON ci.CompItemKey = i.ItemKey WHERE ci.KitShipLineKey = " & lKitShipLineKey & " AND i.AllowRtrns = 0"
        mdTotalPrice = dGetBTOKitPrice(lKitItemKey, lKitShipLineKey, lBTOKitOrigShipLineKey, lRMALineKey)
        mbDataChanged = True
    End If
    
    
    'Remember the BTO Component list by storing it in #tsoCompItemUndo.
    'This data will be put back to the component grid if the user clicks
    'the Cancel/Exit button on the DETAIL form.
    sSQL = "DELETE #tsoCompItemUndo WHERE KitShipLineKey = " & lKitShipLineKey & " AND BTOKitItemKey = " & lKitItemKey & ";" & _
            "INSERT #tsoCompItemUndo SELECT * FROM #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey & " AND BTOKitItemKey = " & lKitItemKey
    moClass.moAppDB.ExecuteSQL sSQL
    
    'Remeber the BTO Components list by storing it in #tsoOrigCompItemList
    'which will be used to determine which distribution needs to be delete
    'in case the user saves, then edits the component list by deleting one
    'of the components and saving again.
    'This is the only place this temp table is loaded and once there is a
    'component list for the KitShipLineKey, we will never re-insert.  It is only
    'cleared out when the user does a cancel or delete from the parent form.
    If moClass.moAppDB.Lookup("COUNT(1)", "#tsoOrigCompItemList", "KitShipLineKey = " & lKitShipLineKey) = 0 Then
        sSQL = "INSERT #tsoOrigCompItemList SELECT * FROM #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey
        moClass.moAppDB.ExecuteSQL sSQL
    End If
    
    bInitCompList = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bInitCompList", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set oClass = goClass

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "oClass", VBRIG_IS_FORM                                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyApp() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyApp = App

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyApp", VBRIG_IS_FORM                                  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set MyForms = Forms

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "MyForms", VBRIG_IS_FORM                                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Function lGetCompOrigShipLineKey(lOrigShipBTOShipLineKey As Long, lCompItemKey As Long) _
As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    Dim lCompShipLineKey As Long
    'The procedure will get the ShipLineKey for the Component record using the
    'BTO's ShipLineKey and the Component ItemKey.
    
    lGetCompOrigShipLineKey = 0
    
    lCompShipLineKey = glGetValidLong(moClass.moAppDB.Lookup("ShipLineKey", "tsoShipLine WITH (NOLOCK)", "KitShipLineKey = " & glGetValidLong(lOrigShipBTOShipLineKey) & " AND ItemKey = " & glGetValidLong(lCompItemKey)))
    
    If lCompShipLineKey = 0 Then
        'Use the ItemShippedKey instead of ItemKey in case it is a substitute item.
        lCompShipLineKey = glGetValidLong(moClass.moAppDB.Lookup("ShipLineKey", "tsoShipLine WITH (NOLOCK)", "KitShipLineKey = " & glGetValidLong(lOrigShipBTOShipLineKey) & " AND ItemShippedKey = " & glGetValidLong(lCompItemKey)))
    End If
    
    lGetCompOrigShipLineKey = lCompShipLineKey
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lGetCompOrigShipLineKey", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Property Get CustomizedBTO() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    CustomizedBTO = mbCustomizedBTO
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CustomizedBTO", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let CallerType(uCallerType As Caller)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    muCallerType = uCallerType
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CallerType", VBRIG_IS_FORM                             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property


Private Function lGetNextRowToDistribute(ByVal lRow As Long) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    'This routine will check if the Lot/Serial/Bin has been cancelled.
    'If not, it will find the next row to distribute by checking which
    'row does not have a DistQty that equals the Qty to Distribute.
    
    Dim dDistQty As Double
    Dim dTranQty As Double
    Dim lCurntRow As Long
    Dim lStartingRow As Long
    
    
    'Default to the first row.
    lGetNextRowToDistribute = 1
    
    lCurntRow = lRow
    
    If moDist.CancelClicked = True Then
        'Just return the same row.
        lGetNextRowToDistribute = lRow
    Else
        'Remember the row we are starting from.
        lStartingRow = lRow
        
        'If the row is somehow set on the last row (non data row)
        'or the 0 Row, set the row back to 1.
        If lRow > grdCompItem.DataRowCnt Or lRow = 0 Then
            'You are in the last data row.  Set the lRow to start from
            'the top and set the flag to true so we know we've started over.
            lRow = 1
        End If
        
        'Get the Tran and Distributed quantities.
        dTranQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTotalQty))
        dDistQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemQtyDistributed))

        'Seach for the next line that has not been fully distributed
        'and return that line.
        Do While dDistQty = dTranQty
            
            'Get the next row.
            lRow = lRow + 1
            
            'Check if the current row is data row.
            If lRow > grdCompItem.DataRowCnt Then
                'You have just completed checking the last data row.  Set the lRow
                'to start from the top if we did not start on the first row.
                If lStartingRow > 1 Then
                    lRow = 1
                    'Set the lStartingRow to 1 so we don't get in an endless loop.
                    lStartingRow = 1
                Else
                    lRow = lCurntRow
                    Exit Do
                End If
            End If
            
            'Get the Tran and Dist quantities off the grid.
            dTranQty = gsGridReadCell(grdCompItem, lRow, kColSOCompItemTotalQty)
            dDistQty = gsGridReadCell(grdCompItem, lRow, kColSOCompItemQtyDistributed)
        Loop
        
        lGetNextRowToDistribute = lRow
    End If
    'Enable the distribution button if needed.
    cmdDistribution.Enabled = bEnableCmdDistribution(lGetNextRowToDistribute)
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lGetNextRowToDistribute", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function dGetBTOKitPrice(lBTOKitItemKey As Long, lKitShipLineKey As Long, Optional lBTOKitOrigShipLineKey As Long = 0, Optional lBTOKitRMALineKey As Long = 0) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    'The purpose of this routine is to determine if a BTO Kit's component list has been
    'customized from the original shipment's or from an RMA's components list or from the
    'original Kit component list and return the appropriate kit price.
    '  The original shipment or RMA component list to used to compare the current list
    '  is derived from the input parameters (lBTOKitOrigShipLineKey / lBTOKitRMALineKey).
    '  The original Kit component list is derived from lBTOKitItemKey
    '  The current list is made up from #tsoCompItem.
    
    Dim iCustomizedFromOrigCompList As Integer
    Dim iCustomizedFromRMA As Integer
    Dim iCustomizedFromOrigShip As Integer
    Dim sSPName As String
    Dim lRetVal As Long
   
    dGetBTOKitPrice = 0
    
    moClass.moAppDB.ExecuteSQL "IF OBJECT_ID ('tempdb..#tsoCompItemList') IS NOT NULL DROP TABLE #tsoCompItemList"
    moClass.moAppDB.ExecuteSQL "SELECT CompItemKey, CompItemQty 'CompItemQty', " & lBTOKitItemKey & "'BTOKitItemKey', " & lKitShipLineKey & " 'TranKeyValue', 0 'BTOKitOrigShipLineKey' INTO #tsoCompItemList FROM #tsoCompItem WHERE KitShipLineKey = " & lKitShipLineKey
    sSPName = "spimIsBTOKitCustomized"
    
    With moClass.moAppDB
        .SetOutParam lRetVal
        .SetInParam lBTOKitItemKey
        .SetInParam lKitShipLineKey
        .SetInParam lBTOKitRMALineKey
        .SetInParam lBTOKitOrigShipLineKey
        .SetOutParam iCustomizedFromOrigCompList
        .SetOutParam iCustomizedFromRMA
        .SetOutParam iCustomizedFromOrigShip
        .ExecuteSP sSPName
        lRetVal = glGetValidLong(.GetOutParam(1))
        iCustomizedFromOrigCompList = giGetValidInt(.GetOutParam(6))
        iCustomizedFromRMA = giGetValidInt(.GetOutParam(7))
        iCustomizedFromOrigShip = giGetValidInt(.GetOutParam(8))
        .ReleaseParams
    End With

    If lRetVal <> 1 Then
        'Stored Procedure '{0}' returned an unexpected value: {1}
        giSotaMsgBox Me, moClass.moSysSession, kmsgSPBadReturn, sSPName, lRetVal
        Exit Function
    End If
    
    If lBTOKitOrigShipLineKey <> 0 And lBTOKitRMALineKey <> 0 Then
        'Return was created using an RMA that was linked to a shipment.
        If iCustomizedFromRMA = 0 Then
            mbCustomizedBTO = False
            'Component list matches the RMA.
            dGetBTOKitPrice = dGetBTOPriceFromRMA(lBTOKitRMALineKey)
        Else
            mbCustomizedBTO = True
            If iCustomizedFromOrigShip = 0 Then
                'Component list matches the original shipment.
                dGetBTOKitPrice = dGetBTOPriceFromInvoice(lBTOKitOrigShipLineKey)
            Else
                If iCustomizedFromOrigCompList = 0 Then
                    dGetBTOKitPrice = dGetBTOStdPrice(lBTOKitItemKey)
                Else
                    dGetBTOKitPrice = dGetBTOPriceFromSumOfCompStdPrice(lKitShipLineKey)
                End If
            End If
        End If
        
    ElseIf lBTOKitOrigShipLineKey <> 0 And lBTOKitRMALineKey = 0 Then
        'Return was created using a shipment (not an RMA).
        If iCustomizedFromOrigShip = 0 Then
            mbCustomizedBTO = False
            'Component list matches the original shipment.
            dGetBTOKitPrice = dGetBTOPriceFromInvoice(lBTOKitOrigShipLineKey)
        Else
            mbCustomizedBTO = True
            If iCustomizedFromOrigCompList = 0 Then
                dGetBTOKitPrice = dGetBTOStdPrice(lBTOKitItemKey)
            Else
                dGetBTOKitPrice = dGetBTOPriceFromSumOfCompStdPrice(lKitShipLineKey)
            End If
        End If
        
    ElseIf lBTOKitOrigShipLineKey = 0 And lBTOKitRMALineKey <> 0 Then
        'Return was created using an RMA and not tied to a Shipment.
        If iCustomizedFromRMA = 0 Then
            mbCustomizedBTO = False
            'Component list matches the RMA.
            dGetBTOKitPrice = dGetBTOPriceFromRMA(lBTOKitRMALineKey)
        Else
            mbCustomizedBTO = True
            If iCustomizedFromOrigCompList = 0 Then
                dGetBTOKitPrice = dGetBTOStdPrice(lBTOKitItemKey)
            Else
                dGetBTOKitPrice = dGetBTOPriceFromSumOfCompStdPrice(lKitShipLineKey)
            End If
        End If
        
    ElseIf lBTOKitOrigShipLineKey = 0 And lBTOKitRMALineKey = 0 Then
        'Return was created manually.
        If iCustomizedFromOrigCompList = 0 Then
            mbCustomizedBTO = False
            dGetBTOKitPrice = dGetBTOStdPrice(lBTOKitItemKey)
        Else
            mbCustomizedBTO = True
            dGetBTOKitPrice = dGetBTOPriceFromSumOfCompStdPrice(lKitShipLineKey)
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dGetBTOKitPrice", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function dGetBTOPriceFromSumOfCompStdPrice(lKitShipLineKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim i                   As Long
    Dim dTotalPrice         As Double
    Dim dQty                As Double
    Dim dStdPrice           As Double
    Dim dExtAmt             As Double

    dGetBTOPriceFromSumOfCompStdPrice = 0
     
    'Calculate the price of one kit as sum of components
    dTotalPrice = 0
    dTotalPrice = gdGetValidDbl(mfrmMain.oClass.moAppDB.Lookup("SUM(ci.CompItemQty * i.StdPrice * " & mdCurrExchRate & ")", "#tsoCompItem ci WITH (NOLOCK) JOIN timItem i WITH (NOLOCK) ON ci.CompItemKey = i.ItemKey", "ci.KitShipLineKey = " & lKitShipLineKey))
    dGetBTOPriceFromSumOfCompStdPrice = gdGetValidDbl(dTotalPrice)
               
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dGetBTOPriceFromSumOfCompStdPrice", VBRIG_IS_FORM      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function dGetBTOPriceFromRMA(lRMALineKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    dGetBTOPriceFromRMA = gdGetValidDbl(mfrmMain.oClass.moAppDB.Lookup("UnitPrice", "tsoRMALine WITH (NOLOCK)", "RMALineKey = " & lRMALineKey))
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dGetBTOPriceFromRMA", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function dGetBTOPriceFromInvoice(lShipLineKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    dGetBTOPriceFromInvoice = gdGetValidDbl(mfrmMain.oClass.moAppDB.Lookup("UnitPrice", "tarInvoiceDetl WITH (NOLOCK)", "ShipLineKey = " & lShipLineKey))

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dGetBTOPriceFromInvoice", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function dGetBTOStdPrice(lBTOItemKey As Long) As Double
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    dGetBTOStdPrice = mdStdKitPrice
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "dGetBTOStdPrice", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function bEnableCmdDistribution(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim iCompTrackMeth As Integer
    Dim iCompItemType As Integer
    Dim lItemKey As Long
    
    DoEvents

    bEnableCmdDistribution = False
    
    iCompTrackMeth = Val(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTrackMeth))
    iCompItemType = Val(gsGridReadCell(grdCompItem, lRow, kColSOCompItemType))
    lItemKey = Val(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
    
    If lItemKey = 0 Then
        bEnableCmdDistribution = False
    Else
        Select Case iCompItemType
            Case IMS_MISC_ITEM
                bEnableCmdDistribution = False
            Case IMS_FINISHED_GOOD, IMS_RAW_MATERIAL, IMS_ASSEMBLED_KIT
                'If mbTrackQtyAtBin Or iCompTrackMeth <> 0 Then
                    bEnableCmdDistribution = True
                'Else
                    'bEnableCmdDistribution = False
                'End If
        End Select
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bEnableCmdDistribution", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function



 Private Sub grdCompItem_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
    moGM.Grid_ColWidthChange Col1
End Sub
 Private Sub grdCompItem_KeyDown(KeyCode As Integer, Shift As Integer)
    moGM.Grid_KeyDown KeyCode, Shift
End Sub
 Private Sub grdCompItem_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
    moGM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
End Sub

Private Sub DoAutoDistBin(lRow As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lTranIdentifier As Long
    Dim lInvtTranKey As Long
    Dim lItemKey As Long
    Dim lWhseKey As Long
    Dim sWhseBinID As String
    Dim sLotNo As String
    Dim sLotExpDate As String
    Dim dBinQtyAvail As Double
    Dim dQtyToDist As Double
    Dim lUOMKey As Long
    Dim lTranType As Long
    Dim lAutoDist As Long
    Dim dDistQty As Double
    Dim lCompOrigShipLineKey As Long
    
    SetHourglass True
    
    lAutoDist = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColAutoDist))
    lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
    lCompOrigShipLineKey = glGetValidLong(lGetCompOrigShipLineKey(mlOrigShipLineKey, lItemKey))
    
    'Determine if we should do an auto-distribution.
    If lAutoDist = 1 And lCompOrigShipLineKey = 0 Then
        lTranType = glGetValidLong(kTranTypeSORT)
        lWhseKey = glGetValidLong(mlRcvgWhseKey)
        
        'Grab the dist info to perform the auto-distribution from the grid.
        sWhseBinID = gsGetValidStr(gsGridReadCell(grdCompItem, lRow, kColAutoDistBinID))
        lUOMKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColAutoDistBinUOMKey))
        dBinQtyAvail = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColAutoDistBinQtyAvail))
        dQtyToDist = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTotalQty))
        sLotNo = gsGetValidStr(gsGridReadCell(grdCompItem, lRow, kColAutoDistLotNo))
        sLotExpDate = Format(gsGetValidStr(gsGridReadCell(grdCompItem, lRow, kColAutoDistLotExpDate)), gsGetLocalVBDateMask)
        lTranIdentifier = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTranIdentifier))
        lInvtTranKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemInvtTranKey))
        
        'Attempt to auto distribute the bin selection.
        'If successful, this routine will assign a TranIdentifier.
        If moDist.bPerformSilentDistribution(lTranIdentifier, lWhseKey, lItemKey, _
                                              lTranType, lUOMKey, lUOMKey, dQtyToDist, _
                                              dBinQtyAvail, sWhseBinID, sLotNo, sLotExpDate, "", lInvtTranKey) Then

            If lTranIdentifier > 0 Then
                'Successfully performed the silent distribution.
                'Set the AutoDist column to zero to prevent from auto distributing again,
                'and also store the TranIdentifier in the grid.
                gGridUpdateCell grdCompItem, lRow, kColAutoDist, CStr(0)
                gGridUpdateCell grdCompItem, lRow, kColSOCompItemTranIdentifier, CStr(lTranIdentifier)
                
                'Update the grid's QtySelected with the distribution quantity.
                dDistQty = gdGetValidDbl(moClass.moAppDB.Lookup("SUM(TranQty)", "#timInvtDistWrk WITH (NOLOCK)", "TranIdentifier = " & lTranIdentifier))
                gGridUpdateCell grdCompItem, lRow, kColSOCompItemQtyDistributed, CStr(dDistQty)
            End If
        Else
            'Unable to auto-distribute.
            MsgBox "Unable to auto-distribute against selected bin."
            txtlkuAutoDistBin.Text = ""
            gGridUpdateCell grdCompItem, lRow, kColAutoDist, CStr(0)
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, ""
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinUOMKey, ""
            gGridUpdateCell grdCompItem, lRow, kColAutoDistBinQtyAvail, ""
            gGridUpdateCell grdCompItem, lRow, kColAutoDistLotNo, ""
            gGridUpdateCellText grdCompItem, lRow, kColAutoDistLotExpDate, ""
            
            SetHourglass False
            Exit Sub
        End If
    End If

    SetHourglass False
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "DoAutoDistBin", VBRIG_IS_FORM                          'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bIsValidAutoDistBin(lRow As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lTrackMeth As Long
    Dim bBinColEmpty As Boolean
    Dim bAutoDist As Boolean
    Dim lWhseKey As Long
    Dim lItemKey As Long
    Dim lUOMKey As Long
    Dim lTranType As Long
    Dim dTranQty As Double
    Dim sWhseBinID As String
    Dim lWhseBinKey As Long
    Dim dBinQtyAvail As Double
    Dim lInvtTranKey As Long
    Dim lBinUOMKey As Long
    Dim sLotNo As String
    Dim sLotExpDate As String
    Dim lOvrdTrackMeth As Long
    Dim lCompOrigShipLineKey As Long
    

    'This validation routine is designed to tell if the bin can be used
    'to auto-distribute the transaction.  Based on the tracking method,
    'it will try to see if there is enough information to do the auto-
    'distribution.  For example: Lot-tracked items require a lot number.
    
    'We will try to use the return column collection if we detect the user
    'selected a value by clicking the lookup, otherwise we will use the bin
    'itself to get the distribution info.  If the info is lacking, we will
    'raise an error stating the bin is invalid.

    bIsValidAutoDistBin = False
    lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
    lTrackMeth = glGetValidLong(moClass.moAppDB.Lookup("TrackMeth", "timItem with (NOLOCK)", "ItemKey = " & lItemKey))
    lUOMKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemUOMKey))
    lWhseKey = glGetValidLong(mlRcvgWhseKey)
    dTranQty = gdGetValidDbl(gsGridReadCell(grdCompItem, lRow, kColSOCompItemTotalQty))
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemInvtTranKey))
    lTranType = glGetValidLong(kTranTypeSORT)
    lCompOrigShipLineKey = lGetCompOrigShipLineKey(mlOrigShipLineKey, lItemKey)

    'If we are tied to an original shipment override the bin LookupID to non-track
    'because we only want to display bins in the lookup.  The specific lot/serial numbers
    'will be determined later within EditDistSOCustReturn.
    If lCompOrigShipLineKey <> 0 Then
        lOvrdTrackMeth = IMS_TRACK_METHD_NONE
    Else
        lOvrdTrackMeth = -1
    End If
    
    'If user selected a value from the lookup, it will be stored in the collection.
    'Update the grid cell with the selected value.
    If Not mcolAutoDistBinRetCols Is Nothing Then
        If mcolAutoDistBinRetCols.count <> 0 Then
            bBinColEmpty = False
            bAutoDist = True
            
            moDist.GetAutoDistInfoFromRetCol lItemKey, mcolAutoDistBinRetCols, sWhseBinID, _
                                             dBinQtyAvail, lBinUOMKey, _
                                             sLotNo, sLotExpDate, , , lOvrdTrackMeth

            'Check if we can auto-distribute against the bin.
            bAutoDist = moDist.bIsValidBinForAutoDist(lWhseKey, lItemKey, sWhseBinID, lTranType, _
                                             dTranQty, lUOMKey, moClass.moSysSession.BusinessDate, _
                                             lInvtTranKey, dBinQtyAvail, lBinUOMKey, sLotNo, sLotExpDate, _
                                             , lWhseBinKey, , lOvrdTrackMeth)

        Else
            bBinColEmpty = True
        End If
    Else
        bBinColEmpty = True
    End If
    
    If bBinColEmpty = True Then
        lBinUOMKey = 0
        dBinQtyAvail = 0
        sLotNo = gsGridReadCell(grdCompItem, lRow, kColAutoDistLotNo)
        sLotExpDate = ""
        sWhseBinID = gsGridReadCell(grdCompItem, lRow, kColAutoDistBinID)
        
        If Len(Trim(sWhseBinID)) <> 0 Then
            bAutoDist = moDist.bIsValidBinForAutoDist(lWhseKey, lItemKey, sWhseBinID, lTranType, _
                                             dTranQty, lUOMKey, moClass.moSysSession.BusinessDate, _
                                             lInvtTranKey, dBinQtyAvail, lBinUOMKey, sLotNo, sLotExpDate, _
                                             , lWhseBinKey, , lOvrdTrackMeth)
        Else
            Exit Function
        End If
    End If
    
    'Check if we can do an auto-distribution.
    If bAutoDist = False Then
        txtlkuAutoDistBin.Text = ""
        gGridSetActiveCell grdCompItem, lRow, kColAutoDistBinID

        sWhseBinID = ""
        lBinUOMKey = 0
        dBinQtyAvail = 0
        sLotNo = ""
        sLotExpDate = ""
    End If
    
    gGridUpdateCell grdCompItem, lRow, kColAutoDist, IIf(bAutoDist = True, "1", "0")
    gGridUpdateCell grdCompItem, lRow, kColAutoDistBinID, sWhseBinID
    gGridUpdateCell grdCompItem, lRow, kColAutoDistBinKey, CStr(lWhseBinKey)
    gGridUpdateCell grdCompItem, lRow, kColAutoDistBinUOMKey, CStr(lBinUOMKey)
    gGridUpdateCell grdCompItem, lRow, kColAutoDistBinQtyAvail, CStr(dBinQtyAvail)
    gGridUpdateCell grdCompItem, lRow, kColAutoDistLotNo, sLotNo
    gGridUpdateCellText grdCompItem, lRow, kColAutoDistLotExpDate, Format(sLotExpDate, gsGetLocalVBDateMask)

    bIsValidAutoDistBin = bAutoDist

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bIsValidAutoDistBin", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Function lReserveInvtTranKey(lRow As Long) As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lInvtTranKey As Long
    Dim lItemKey As Long
    Dim iItemType As Integer
    Dim lRetVal As Long
    
    lReserveInvtTranKey = 0
    
    lItemKey = glGetValidLong(gsGridReadCell(grdCompItem, lRow, kColSOCompItemKey))
    
    If lItemKey > 0 Then
        'Check item type - non-inventory doesn't need distributing.
        iItemType = moClass.moAppDB.Lookup("ItemType", "timItem", "ItemKey = " & lItemKey)
        Select Case iItemType
            Case 5, 6, 8
                With moClass.moAppDB
                    .SetInParam msCompanyID
                    .SetInParam gsFormatDateToDB(msTranDate)
                    .SetInParam msCustReturnNo
                    .SetInParam kTranTypeSORT
                    .SetOutParam lInvtTranKey
                    .SetOutParam lRetVal
                    .ExecuteSP "spimReserveInvtTran"
                    lInvtTranKey = glGetValidLong(.GetOutParam(5))
                    lRetVal = glGetValidLong(.GetOutParam(6))
                    .ReleaseParams
                End With
                If lRetVal <> 0 Then
                    giSotaMsgBox Me, moClass.moSysSession, kmsgSOErrReserveInvtTran
                    Exit Function
                End If
                
                'Add InvtTranKey to #ReservedInvtTranKeys temp table.
                AddToReservedInvtTranKeys moClass.moAppDB, lInvtTranKey
                
                'Update #tsoCompItem with the InvtTranKey that was just reserved.
                'Do this before calling EditDistSOCustReturn because the InvtTranKey
                'is used to tell if an item is a component of a BTO Kit.
                moClass.moAppDB.ExecuteSQL "UPDATE #tsoCompItem SET InvtTranKey = " & lInvtTranKey & " WHERE CompItemKey = " & lItemKey & " AND KitShipLineKey = " & mlKitShipLineKey
                
                gGridUpdateCell grdCompItem, lRow, kColSOCompItemInvtTranKey, CStr(lInvtTranKey)
            Case Else
                'Skip 'Should have probably disabled the button.
        End Select
    End If

    lReserveInvtTranKey = lInvtTranKey
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "lReserveInvtTranKey", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub AutoDistFromBinLku(Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim lInvtTranKey As Long
    Dim lNextRowToDist As Long

    'Reserve an InvtTranKey if needed.
    lInvtTranKey = glGetValidLong(gsGridReadCell(grdCompItem, Row, kColSOCompItemInvtTranKey))
    If lInvtTranKey = 0 Then
        lInvtTranKey = lReserveInvtTranKey(Row)
    End If
    
    'Perform the auto-distribution.
    DoAutoDistBin Row
    
    'Enable the distribution button if needed.
    cmdDistribution.Enabled = bEnableCmdDistribution(Row)
    
    'Find the next row to distribute.
    lNextRowToDist = lGetNextRowToDistribute(Row)
    grdCompItem.Row = lNextRowToDist
    grdCompItem.Col = kColAutoDistBinID
    If grdCompItem.Lock = False Then
        gGridSetActiveCell grdCompItem, lNextRowToDist, kColAutoDistBinID
    End If

    gbFormIsDirty = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "AutoDistFromBinLku", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub GetPreferredBin(lPreferredBinKey As Long, _
    sPreferredBinID As String, ByVal lItemKey As Long, ByVal lWhseKey As Long)
'+++ VB/Rig Begin Push +++     0                                                           'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Get the preferred bin.
    lPreferredBinKey = glGetValidLong(moClass.moAppDB.Lookup("WhseBinKey", _
                        "timInvtBinList WITH (NOLOCK)", "WhseKey = " & Format$(lWhseKey) & _
                        " AND ItemKey = " & Format$(lItemKey) & _
                        " AND PrefNo = 1" & _
                        " AND PrefBinType = dbo.fnIMGetInvtPrefBinType(" & Format$(lWhseKey) & "," & Format$(lItemKey) & "," & kTranTypeSORT & ")"))
    sPreferredBinID = gsGetValidStr(moClass.moAppDB.Lookup("WhseBinID", "timWhseBin WITH (NOLOCK)", "WhseBinKey = " & Format$(lPreferredBinKey)))

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "GetPreferredBin", VBRIG_IS_FORM                        'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Public Sub sGetBTOKitBinLkuCtrlSavedDistValues(oAppDB As Object, ByVal lKitShipLineKey As Long, ByRef bMultipleBin As Boolean, _
                                                ByRef bAllInPrefBin As Boolean, ByRef sWhseBinID As String, ByRef lWhseBinKey As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim rs As Object
    Dim sSQL As String
    Dim iPrefBin As Integer
    Dim iPrevPrefBin As Integer
    Dim bSamePrefBin As Boolean
    Dim lBinKey As Long
    Dim sBinID As String
    Dim lPrevBinKey As Long
    Dim bSameBinKey As Boolean
    Dim bFirstRec As Boolean
        
    If lKitShipLineKey <> 0 And Not oAppDB Is Nothing Then
        
        bSamePrefBin = True
        bSameBinKey = True
        bFirstRec = True
    
        'If the return has not yet been saved, there will not be any records in
        '#tsoCustRtrnShipLine.  If this is the case, go aginst the distribution table.
        If glGetValidLong(oAppDB.Lookup("COUNT(*)", "#tsoCustRtrnShipLine", "KitShipLineKey = " & lKitShipLineKey)) > 0 Then
        sSQL = " SELECT crsl.RcvgWhseKey 'WhseKey', crsl.ItemKey, COALESCE(idw.WhseBinKey, d.WhseBinKey) 'WhseBinKey', " & _
                " COALESCE(idw.BinID, d.WhseBinID) 'WhseBinID', COALESCE(ibl.PrefNo, 0) 'PrefNo'" & _
               " FROM #tsoCustRtrnShipLine crsl" & _
               " LEFT OUTER JOIN #timInvtDistWrk idw WITH (NOLOCK) ON crsl.InvtTranKey = idw.TranKey" & _
               " LEFT OUTER JOIN vimSavedDistributions d WITH (NOLOCK) ON crsl.InvtTranKey = d.InvtTranKey" & _
               " LEFT OUTER JOIN timInvtBinList ibl WITH (NOLOCK) ON COALESCE(idw.WhseKey, d.WhseKey) = ibl.WhseKey " & _
                                                                " AND COALESCE(idw.ItemKey, d.ItemKey) = ibl.ItemKey " & _
                                                                " AND COALESCE(idw.WhseBinKey, d.WhseBinKey) = ibl.WhseBinKey " & _
                                                                " AND ibl.PrefNo = 1" & _
                                                                " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(COALESCE(idw.WhseKey, d.WhseKey),COALESCE(idw.ItemKey, d.ItemKey)," & kTranTypeSORT & ")" & _
               " WHERE crsl.KitShipLineKey = " & lKitShipLineKey
        Else
            sSQL = "SELECT idw.WhseKey, idw.ItemKey, idw.WhseBinKey, idw.BinID 'WhseBinID', COALESCE(ibl.PrefNo, 0) 'PrefNo' " & _
                    " FROM #timInvtDistWrk idw " & _
                    " LEFT OUTER JOIN timInvtBinList ibl WITH (NOLOCK) ON idw.WhseKey = ibl.WhseKey " & _
                                                                    " AND idw.ItemKey = ibl.ItemKey AND idw.WhseBinKey = ibl.WhseBinKey " & _
                                                                    " AND ibl.PrefNo = 1 " & _
                                                                    " AND ibl.PrefBinType = dbo.fnIMGetInvtPrefBinType(idw.WhseKey, idw.ItemKey, " & kTranTypeSORT & ")" & _
                    " WHERE idw.KitKey = " & lKitShipLineKey
        End If
        
        Set rs = oAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEmpty Then
        
            Do While Not rs.IsEOF
                
                'If this is the first record, assign the previous bin variables to the current record.
                If bFirstRec Then
                    lPrevBinKey = glGetValidLong(rs.Field("WhseBinKey"))
                    iPrevPrefBin = giGetValidInt(rs.Field("PrefNo"))
                End If
                
                bFirstRec = False
                
                'Check if all records point to the same preferred bin.
                lBinKey = glGetValidLong(rs.Field("WhseBinKey"))
                sBinID = gsGetValidStr(rs.Field("WhseBinID"))
                
                If lBinKey = 0 Then
                    bSameBinKey = False
                ElseIf lBinKey <> lPrevBinKey Then
                    bSameBinKey = False
                End If
                
                lPrevBinKey = lBinKey
                
                'Check if all records point to the same preferred bin.
                iPrefBin = giGetValidInt(rs.Field("PrefNo"))
                
                If iPrefBin = 0 Then
                    bSamePrefBin = False
                ElseIf iPrefBin <> iPrevPrefBin Then
                    bSamePrefBin = False
                End If
                
                iPrevPrefBin = iPrefBin
            
                rs.MoveNext
            Loop
        
            bAllInPrefBin = bSamePrefBin
            If bSameBinKey = True Then
                sWhseBinID = sBinID
                lWhseBinKey = lBinKey
                bMultipleBin = False
            Else
                bMultipleBin = True
            End If
        
        End If

        If Not rs Is Nothing Then
            rs.Close
            Set rs = Nothing
        End If
        
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "sGetBTOKitBinLkuCtrlSavedDistValues", VBRIG_IS_MODULE  'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub



