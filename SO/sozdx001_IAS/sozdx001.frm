VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Begin VB.Form frmCancelPicks 
   Caption         =   "Cancel Picks"
   ClientHeight    =   7170
   ClientLeft      =   2925
   ClientTop       =   2535
   ClientWidth     =   11610
   HelpContextID   =   70633
   Icon            =   "sozdx001.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7170
   ScaleMode       =   0  'User
   ScaleWidth      =   11610
   Begin VB.Frame Frame1 
      Height          =   2850
      Left            =   9720
      TabIndex        =   30
      Top             =   465
      Width           =   1815
      Begin VB.CommandButton cmdDeSelect 
         Caption         =   "&De-Select"
         Height          =   375
         Left            =   195
         TabIndex        =   35
         Top             =   2280
         WhatsThisHelpID =   70665
         Width           =   1425
      End
      Begin VB.CommandButton cmdSelect 
         Caption         =   "&Select"
         Height          =   375
         Left            =   195
         TabIndex        =   34
         Top             =   1800
         WhatsThisHelpID =   70666
         Width           =   1425
      End
      Begin VB.Frame fraInclude 
         Caption         =   "Include"
         Height          =   1335
         Left            =   75
         TabIndex        =   31
         Top             =   240
         Width           =   1650
         Begin VB.CheckBox chkInclude 
            Caption         =   "Sales Orders"
            Height          =   345
            Index           =   0
            Left            =   120
            TabIndex        =   33
            Top             =   360
            Value           =   1  'Checked
            WhatsThisHelpID =   70667
            Width           =   1215
         End
         Begin VB.CheckBox chkInclude 
            Caption         =   "Transfer Orders"
            Height          =   345
            Index           =   1
            Left            =   120
            TabIndex        =   32
            Top             =   720
            Value           =   1  'Checked
            WhatsThisHelpID =   70668
            Width           =   1400
         End
      End
   End
   Begin TabDlg.SSTab tabSelect 
      Height          =   2820
      Left            =   90
      TabIndex        =   1
      Top             =   500
      WhatsThisHelpID =   70664
      Width           =   9525
      _ExtentX        =   16801
      _ExtentY        =   4974
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Sales Orders"
      TabPicture(0)   =   "sozdx001.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSelect(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Transfer Orders"
      TabPicture(1)   =   "sozdx001.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraSelect(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Settings - Hidden"
      TabPicture(2)   =   "sozdx001.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "grdSelSettings"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Custom Tab"
      TabPicture(3)   =   "sozdx001.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraSelect(2)"
      Tab(3).ControlCount=   1
      Begin VB.Frame fraSelect 
         BorderStyle     =   0  'None
         Height          =   2370
         Index           =   2
         Left            =   -74880
         TabIndex        =   36
         Top             =   375
         Width           =   9285
      End
      Begin FPSpreadADO.fpSpread grdSelSettings 
         Height          =   2055
         Left            =   -74760
         TabIndex        =   29
         Top             =   600
         WhatsThisHelpID =   70663
         Width           =   9135
         _Version        =   524288
         _ExtentX        =   16113
         _ExtentY        =   3625
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdx001.frx":2442
         AppearanceStyle =   0
      End
      Begin VB.Frame fraSelect 
         Caption         =   "S&elect"
         Height          =   2370
         Index           =   1
         Left            =   -74880
         TabIndex        =   24
         Top             =   360
         Width           =   9285
         Begin FPSpreadADO.fpSpread grdSelection 
            Height          =   2055
            Index           =   1
            Left            =   120
            TabIndex        =   3
            Top             =   240
            WhatsThisHelpID =   24
            Width           =   9015
            _Version        =   524288
            _ExtentX        =   15901
            _ExtentY        =   3625
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "sozdx001.frx":2880
            AppearanceStyle =   0
         End
         Begin NEWSOTALib.SOTANumber nbrControl 
            Height          =   285
            Index           =   1
            Left            =   1530
            TabIndex        =   25
            TabStop         =   0   'False
            Top             =   1440
            WhatsThisHelpID =   47
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTAMaskedEdit mskControl 
            Height          =   240
            Index           =   1
            Left            =   1530
            TabIndex        =   26
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   45
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   423
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
         End
         Begin LookupViewControl.LookupView navControlTransfer 
            Height          =   285
            Index           =   0
            Left            =   3555
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   70658
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
         Begin NEWSOTALib.SOTACurrency curControl 
            Height          =   285
            Index           =   1
            Left            =   2760
            TabIndex        =   28
            TabStop         =   0   'False
            Top             =   1200
            WhatsThisHelpID =   48
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
      End
      Begin VB.Frame fraSelect 
         Caption         =   "S&elect"
         Height          =   2370
         Index           =   0
         Left            =   120
         TabIndex        =   19
         Top             =   375
         Width           =   9285
         Begin FPSpreadADO.fpSpread grdSelection 
            Height          =   2055
            Index           =   0
            Left            =   120
            TabIndex        =   2
            Top             =   225
            WhatsThisHelpID =   24
            Width           =   9060
            _Version        =   524288
            _ExtentX        =   15981
            _ExtentY        =   3625
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            SpreadDesigner  =   "sozdx001.frx":2CBE
            AppearanceStyle =   0
         End
         Begin NEWSOTALib.SOTANumber nbrControl 
            Height          =   285
            Index           =   0
            Left            =   1530
            TabIndex        =   20
            TabStop         =   0   'False
            Top             =   1440
            WhatsThisHelpID =   47
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
         Begin NEWSOTALib.SOTAMaskedEdit mskControl 
            Height          =   240
            Index           =   0
            Left            =   1530
            TabIndex        =   21
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   45
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   423
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.24
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
         End
         Begin LookupViewControl.LookupView navControl 
            Height          =   285
            Index           =   0
            Left            =   3555
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   720
            WhatsThisHelpID =   46
            Width           =   285
            _ExtentX        =   503
            _ExtentY        =   503
            LookupMode      =   1
         End
         Begin NEWSOTALib.SOTACurrency curControl 
            Height          =   285
            Index           =   0
            Left            =   2760
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   1200
            WhatsThisHelpID =   48
            Width           =   1950
            _Version        =   65536
            _ExtentX        =   3440
            _ExtentY        =   503
            _StockProps     =   93
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            sBorder         =   1
            mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
            text            =   "           0.00"
            sDecimalPlaces  =   2
         End
      End
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   7
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   13
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   741
      Style           =   4
   End
   Begin VB.Frame fraLine 
      Height          =   3345
      Left            =   75
      TabIndex        =   0
      Top             =   3375
      Width           =   11445
      Begin VB.CommandButton cmdClearAll 
         Caption         =   "&Clear All"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         WhatsThisHelpID =   70643
         Width           =   1065
      End
      Begin FPSpreadADO.fpSpread grdLine 
         Height          =   2565
         Left            =   120
         TabIndex        =   5
         Top             =   660
         WhatsThisHelpID =   70642
         Width           =   11205
         _Version        =   524288
         _ExtentX        =   19764
         _ExtentY        =   4524
         _StockProps     =   64
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         SpreadDesigner  =   "sozdx001.frx":30FC
         AppearanceStyle =   0
      End
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6780
      WhatsThisHelpID =   73
      Width           =   11610
      _ExtentX        =   20479
      _ExtentY        =   688
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   8
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   11
      Top             =   2880
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   18
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   17
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmCancelPicks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Public Form Variables
Public moSotaObjects            As New Collection
Public msCompanyID              As String
Public moMapSrch                As Collection   'Search Buttons

'Private Object Variables
Private moClass                 As Object
Private moAppDB                 As Object
Private moSysSession            As Object
Private WithEvents moDMGrid     As clsDmGrid
Attribute moDMGrid.VB_VarHelpID = -1
Private WithEvents moGM                    As clsGridMgr
Attribute moGM.VB_VarHelpID = -1
Private moContextMenu           As clsContextMenu
Private moOptions               As clsModuleOptions
Private WithEvents moHook       As AFWinHook.clsMsgs
Attribute moHook.VB_VarHelpID = -1

'To support multiple selection grid interface
Private moSelect(1)             As clsSelection 'Zero-based array of selection class objects
Private msBaseCaption(1)        As String  'Zero-based array of base captions for the various tabs, frames, etc.
Private moMultSelGridMgr        As clsMultiSelGridSettings
Private Const kNumSelGrids      As Integer = 2


'Private Miscellaneous Variables
Private mbEnterAsTab            As Boolean
Private msUserID                As String
Private msHomeCurrID            As String
Private miSecurityLevel         As Integer
Private mlLanguage              As Long
Private mbLoading               As Boolean
Private mlRunMode               As Long
Private mbCancelShutDown        As Boolean
Private mbLoadSuccess           As Boolean
Private mbTmpTableCreated       As Boolean
Private bMaskCtrlLostFocus      As Boolean
Private miNbrDecPlaces          As Integer
Private mbValidating            As Boolean
Private bContextMenu            As Boolean
Private bResult                 As Boolean
Private Const kiShipComplete = 1

'Integrate with IM variable
Private mbIMIntegrated         As Boolean
Private mbWMIsLicensed          As Boolean

'Private Form Resize Variables
Private miMinFormHeight As Long
Private miMinFormWidth As Long
Private miOldFormHeight As Long
Private miOldFormWidth As Long

'**************************************************************************
'**************************************************************************
'SGS DEJ IA reordered constants (START)
'**************************************************************************
'**************************************************************************
'Line Grid Column Constants
Private Const kColPickCheck = 1         'checkbox indicating that the line has been picked
Private Const kColOrder = 2             'displays Sales or Transfer Order ID
Private Const kColType = 4 '3              'transaction Type
Private Const kColLine = 5 '4              'displays Line Number
Private Const kColPickList = 6 '5          'displays the Pick List Number
Private Const kcolPickLineNo = 7 '6        'displays the Pick List Line Number
Private Const kColItem = 8 '7              'displays Displays Item ID
Private Const kColQtyToPick = 9 '8         'displays Line Item Quantity To Pick
Private Const kColQtyPicked = 10 '9         'displays Line Item Qty Picked
Private Const kColUOM = 11 '10               'displays Unit Of Measure
Private Const kColShipToAddr = 12 '11       'displays Customer Ship To Address
Private Const kColShipVia = 13 '12          'displays Ship Via Method
Private Const kColShipDate = 14 '13         'displays Shiping Date
Private Const kColShipPriority = 15 '14     'displays Shipping Priority
Private Const kColShipWhse = 16 '15         'displays Shipping Warehouse For Item
Private Const kColRcvgWhse = 17 '16         'displays Rcvg Warehouse for Item
Private Const kColCustomer = 18 '17         'displays Customer ID
Private Const kColCustName = 19 '18         'displays Customer Name
Private Const kColDescription = 20 '19      'displays Item Description
Private Const kColSubItem = 21 '20          'displays the substitute item ID
Private Const kColSubDesc = 22 '21          'displays Substitute Item Description
Private Const kColInvTranKey = 23 '22       'hidden - Inventory Transaction Key from IMS
Private Const kColAllowSubItem = 24 '23     'hidden - does customer allow substitute items
Private Const kColTrackMeth = 25 '24        'hidden - is this item tracked by serial number, UOM cannot change
Private Const kColTrackQtyAtBin = 26 '25    'hidden - is this item tracked by bin
Private Const kColInvTranID = 27 '26        'hidden - Inventory Tran ID from IMS
Private Const kColKitLineKey = 28 '27       'hidden - Kit Ship Line Key
Private Const kColShipComplete = 29 '28     'hidden - Ship Complete indicator
Private Const kColShipDateRaw = 30 '29      'hidden - Ship Date for non-display purposes

'SGS DEJ IA added BOL column
Private Const kColBOL = 3

'SGS DEJ IA changed nbr of columns
Private Const kMaxCols = 30 '29             'maximum columns to display in grid

'**************************************************************************
'**************************************************************************
'SGS DEJ IA reordered constants (STOP)
'**************************************************************************
'**************************************************************************
Private Const kLineOpen = 1             '1 = open tsoSOLine or tsoTrnsfrLine lines available for picking
Private Const kLineDistOpen = 1         '1 = open tsoSOLineDist lines available for picking
Private Const kMaxQtyLen = "99999999.99999999"
Private Const kSOOpenStatus = 1         'Sales Order open status
Private Const kSOUnAckStatus = 0        'Sales Order unacknowledged status
Private Const kSONoHold = 0             'Sales Order NOT on hold
Private Const kSONoCrHold = 0           'Sales Order NOT on credit hold

Private Const kSalesOrder As Integer = 0    'Sales Order tab
Private Const kTransferOrder As Integer = 1 'Transfer Order tab

Private Const kDeleteDistributionOnlyYes = 1
Private Const kDeleteDistributionOnlyNo = 0

'Private Constants for LogicLockType
Private Const LOCKTYPE_PICKLIST_RECOVERY  As Integer = 4

Const VBRIG_MODULE_ID_STRING = "SOZDX001.FRM"

Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "lRunMode", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    mlRunMode = lNewRunMode
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "lRunMode", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "oClass", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moClass = oNewClass
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "oClass", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "frmCancelPicks"
End Function

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bLoadSuccess = mbLoadSuccess
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "bLoadSuccess", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "bCancelShutDown", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "SOZ"
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "WhatHelpPrefix", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
    '-- the four-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> +
    '-- "Z" + <one-letter mnemonic of program type, e.g., "M"=Maintenance>
    FormHelpPrefix = "SOZ"
        Exit Property
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Property                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "FormHelpPrefix", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Property

Private Sub chkInclude_Click(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnClick chkInclude(Index), True
    #End If
'+++ End Customizer Code Push +++

    
    Dim iTabCnt As Integer
    'Enable/disable the associated tab.
    '(VB-8/28) Added logic to switch tabs and enable/disable grid according to Include checkbox selection
    If chkInclude(Index).Value = vbChecked Then
        tabSelect.TabEnabled(Index) = True
        grdSelection(Index).Enabled = True
        tabSelect.Tab = Index
    Else
        With tabSelect
            .TabEnabled(Index) = False
            grdSelection(Index).Enabled = False
            For iTabCnt = 0 To .Tabs - 3
                If .TabEnabled(iTabCnt) = True Then
                    .Tab = iTabCnt
                    Exit For
                End If
            Next
        End With
    End If
    
    'Refresh the associated tab's caption.
    tabSelect.TabCaption(Index) = moMultSelGridMgr.sBuildCaption(msBaseCaption(Index), _
grdSelection(Index), tabSelect.TabEnabled(Index))

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPick", "chkInclude_Click", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub cmdClearAll_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdClearAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    moClass.moAppDB.ExecuteSQL "DELETE #tsoCancelPickWrk1"
    moDMGrid.Refresh
    
    SetFormState
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "cmdClearAll_Click", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdDeSelect_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdDeSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
Dim bResult         As Boolean
Dim sWhereClause    As String
Dim sTablesUsed     As String
Dim iIndex          As Integer
     
    frmCancelPicks.MousePointer = vbHourglass
    
    ' clear out our "deletion driver" table
    moAppDB.ExecuteSQL "TRUNCATE TABLE #tsoCancelPickWrk2"
    
    For iIndex = 0 To UBound(moSelect)
        'Only process selection grid if the associated tab is enabled (means tran type is to be included in the select).
        If tabSelect.TabEnabled(iIndex) Then
            'Must clear out string variables before each pass.
            sWhereClause = ""
            sTablesUsed = ""
            If Not bBuildWhereClause(iIndex, sWhereClause, sTablesUsed) Then
                Me.MousePointer = vbDefault
                sbrMain.Status = SOTA_SB_START
                Exit Sub
            End If

            moDMGrid.Save
            ' populate "deletion driver" table with rows matching the selection grids
            ' criteria
            bSelectRows iIndex, sWhereClause, sTablesUsed, "#tsoCancelPickWrk2", True
        End If
    Next
    
    ' now deleting match records from main table
    moClass.moAppDB.ExecuteSQL "DELETE #tsoCancelPickWrk1 FROM #tsoCancelPickWrk2 WHERE " & _
" #tsoCancelPickWrk1.ShipLineKey = #tsoCancelPickWrk2.ShipLineKey "
    
    grdLine.redraw = False
    moDMGrid.Refresh
    grdLine.redraw = True
    
    SetFormState
    
    frmCancelPicks.MousePointer = vbDefault
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        frmCancelPicks.MousePointer = vbDefault
        gSetSotaErr Err, "frmCancelPicks", "cmdSelect_Click", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub cmdSelect_Click()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.OnClick(cmdSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

Dim sWhereClause    As String
Dim sTablesUsed     As String
Dim iIndex          As Integer
        
    frmCancelPicks.MousePointer = vbHourglass
    
    'Clean up the orphaned logical locks
    CleanupLogicalLocks
    
    For iIndex = 0 To UBound(moSelect)
        'Only process selection grid if the associated tab is enabled (means tran type is to be included in the select).
        If tabSelect.TabEnabled(iIndex) Then
            'Must clear out string variables before each pass.
            sWhereClause = ""
            sTablesUsed = ""
            If Not bBuildWhereClause(iIndex, sWhereClause, sTablesUsed) Then
                Me.MousePointer = vbDefault
                sbrMain.Status = SOTA_SB_START
                Exit Sub
            End If

            moDMGrid.Save 'Write all changed pick quantities to #tsoCancelPickWrk1.
            
            bSelectRows iIndex, sWhereClause, sTablesUsed, "#tsoCancelPickWrk1", True
        End If
    Next

    grdLine.redraw = False
    moDMGrid.Refresh
    grdLine.redraw = True
    
    SetFormState
    
    frmCancelPicks.MousePointer = vbDefault
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        frmCancelPicks.MousePointer = vbDefault
        gSetSotaErr Err, "frmCancelPicks", "cmdSelect_Click", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetFormState()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iNbrSelected    As Integer

    iNbrSelected = glGridGetDataRowCnt(grdLine)
    If iNbrSelected = 0 Then
        fraLine.Caption = vbNullString
        cmdClearAll.Enabled = False
        cmdDeSelect.Enabled = False
        tbrMain.ButtonEnabled(kTbProceed) = False
    Else
        cmdClearAll.Enabled = True
        cmdDeSelect.Enabled = True
        tbrMain.ButtonEnabled(kTbProceed) = True
        If iNbrSelected > 1 Then
            fraLine.Caption = iNbrSelected & " Lines to Cancel"
        Else
            fraLine.Caption = iNbrSelected & " Line to Cancel"
        End If
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "SetFormState", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bBuildWhereClause(iIndex As Integer, sWhereClause As String, sTablesUsed As String) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iresult         As Integer
    
    bBuildWhereClause = False
    'Validate Selection Grid
    iresult = moSelect(iIndex).lValidateGrid(True) 'True indicates than any missing criteria will be set to 'All'
    If iresult <> 0 Then
        giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
        Exit Function
    End If
    
    'Update tab caption (in case incomplete criteria was specified and reset, changing state).
    tabSelect.TabCaption(iIndex) = moMultSelGridMgr.sBuildCaption(msBaseCaption(iIndex), _
grdSelection(iIndex), tabSelect.TabEnabled(iIndex))
         
    If Not moSelect(iIndex).bGetWhereClause(sWhereClause, sTablesUsed, 0, False, , , , "") Then
        giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
        Exit Function
    End If
   
    Select Case iIndex
        Case kSalesOrder
            sTablesUsed = "tsoShipLine,tsoShipLineDist,tsoSOLineDist,tsoSOLine,tsoSalesOrder,tarCustAddr"
            AddTableIfMissing sWhereClause, sTablesUsed, "timItem"
            AddTableIfMissing sWhereClause, sTablesUsed, "tsoPickList"
            AddTableIfMissing sWhereClause, sTablesUsed, "timWarehouse"
            AddTableIfMissing sWhereClause, sTablesUsed, "tciShipMethod"
            AddTableIfMissing sWhereClause, sTablesUsed, "tarCustomer"
            'AddTableIfMissing sWhereClause, sTablesUsed, "tarCustAddr"
        Case kTransferOrder
            sTablesUsed = "tsoShipLine,tsoShipLineDist,timTrnsfrOrderLine,vimTrnsfrOrder"
            AddTableIfMissing sWhereClause, sTablesUsed, "timItem"
            AddTableIfMissing sWhereClause, sTablesUsed, "tsoPickList"
            AddTableIfMissing sWhereClause, sTablesUsed, "timWarehouse"
            AddTableIfMissing sWhereClause, sTablesUsed, "tciShipMethod"
            AddTableIfMissing sWhereClause, sTablesUsed, "timWarehouseRcvg"
            AddTableIfMissing sWhereClause, sTablesUsed, "timWarehouseShip"
    End Select
    
    With moSelect(iIndex)
        .AddJoinIfNecessary sWhereClause, sTablesUsed, "tsoPickList", "tsoShipLine.PickListKey = tsoPickList.PickListKey"
        .AddJoinIfNecessary sWhereClause, sTablesUsed, "timItem", "tsoShipLine.ItemKey = timItem.ItemKey"
        
        Select Case iIndex
        Case kSalesOrder
            .AddJoinIfNecessary sWhereClause, sTablesUsed, "tciShipMethod", "tsoSOLineDist.ShipMethKey = tciShipMethod.ShipMethKey"
            .AddJoinIfNecessary sWhereClause, sTablesUsed, "timWarehouse", "tsoSOLineDist.WhseKey = timWarehouse.WhseKey"
            .AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustomer", "tsoSalesOrder.CustKey = tarCustomer.CustKey"
            .AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustAddr", "tsoSOLineDist.ShipToCustAddrKey = tarCustAddr.AddrKey"
        Case kTransferOrder
            .AddJoinIfNecessary sWhereClause, sTablesUsed, "tciShipMethod", "vimTrnsfrOrder.ShipMethKey = tciShipMethod.ShipMethKey"
            
        End Select
        
        If sWhereClause <> vbNullString Then
            sWhereClause = sWhereClause & " AND "
        End If
        
        sWhereClause = sWhereClause & "tsoShipLineDist.ShipLineKey = tsoShipLine.ShipLineKey"
        
        If iIndex = kSalesOrder Then
            sWhereClause = sWhereClause & " AND tsoShipLine.SOLineKey = tsoSOLine.SOLineKey"
            sWhereClause = sWhereClause & " AND tsoSOLineDist.SOLineKey = tsoSOLine.SOLineKey"
            sWhereClause = sWhereClause & " AND tsoSOLine.SOKey = tsoSalesOrder.SOKey"
            sWhereClause = sWhereClause & " AND tsoShipLineDist.SOLineDistKey = tsoSOLineDist.SOLineDistKey"
        Else 'transfer order
            sWhereClause = sWhereClause & " AND tsoShipLine.TrnsfrOrderLineKey = timTrnsfrOrderLine.TrnsfrOrderLineKey"
            sWhereClause = sWhereClause & " AND timTrnsfrOrderLine.TrnsfrOrderKey = vimTrnsfrOrder.TrnsfrOrderKey"
        End If
          
    End With
         
    bBuildWhereClause = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "bBuildWhereClause", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub AddTableIfMissing(ByRef sWhereClause As String, ByRef sTablesUsed As String, sTableName As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'This Sub handles the Key Relationships if there is more than on level of indirection, so that the sTablesUsed works with the sWhereClause
    If InStr(1, sWhereClause, sTableName, vbTextCompare) > 0 Then
        sTablesUsed = sTablesUsed & "," & sTableName
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "AddTableIfMissing", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Function bSelectRows(iIndex As Integer, sWhereClause As String, sTablesUsed As String, sTableInto As String, Optional bSelect As Boolean = True) As Boolean
'+++ VB/Rig Skip +++
    Dim sSql        As String
    Dim lErr        As Long
    Dim sErr        As String
    Dim sWhere      As String
    
    bSelectRows = False
    
    ' if doing a "SELECT", first perform a "DESELECT" on the current criteria to prevent
    ' duplicate key errors
    If bSelect Then
        moAppDB.ExecuteSQL "TRUNCATE TABLE #tsoCancelPickWrk2"
        bSelectRows iIndex, sWhereClause, sTablesUsed, "#tsoCancelPickWrk2", False
        moAppDB.ExecuteSQL "DELETE #tsoCancelPickWrk1 FROM #tsoCancelPickWrk2 WHERE " & _
" #tsoCancelPickWrk1.ShipLineKey = #tsoCancelPickWrk2.ShipLineKey "
    End If

    On Error GoTo ErrHandler

    Select Case iIndex
        Case kSalesOrder
            sSql = "INSERT INTO " & sTableInto & " "
            sSql = sSql & "(ShipLineDistKey,ShipLineKey,KitShipLineKey,SOLineDistKey,LineKey,OrderKey,PickListKey,"
            sSql = sSql & "PickListLineNo,ItemKey,SubItemKey,QtyToPick,QtyPicked,ShipUnitMeasKey,ShipToCustAddrKey,"
            sSql = sSql & "ShipMethKey,ShipWhseKey,CustKey,InvtTranKey,ShipComplete,TranType,TranTypeText,TranNo,LineNbr,ShipDate) "
            sSql = sSql & "SELECT tsoShipLineDist.ShipLineDistKey, "
            sSql = sSql & "tsoShipLine.ShipLineKey, "
            sSql = sSql & "tsoShipLine.KitShipLineKey, "
            sSql = sSql & "tsoSOLineDist.SOLineDistKey, "
            sSql = sSql & "tsoSOLine.SOLineKey, "
            sSql = sSql & "tsoSalesOrder.SOKey, "
            sSql = sSql & "tsoShipLine.PickListKey, "
            sSql = sSql & "tsoShipLine.PickListLineNo, "
            sSql = sSql & "tsoShipLine.ItemKey, "
            sSql = sSql & "tsoShipLine.ItemShippedKey, "
            sSql = sSql & "tsoShipLineDist.QtyToPick, "
            sSql = sSql & "tsoShipLineDist.QtyShipped, "
            sSql = sSql & "tsoShipLine.ShipUnitMeasKey, "
            sSql = sSql & "tsoSOLineDist.ShipToCustAddrKey, "
            sSql = sSql & "tsoSOLineDist.ShipMethKey, "
            sSql = sSql & "tsoSOLineDist.WhseKey, "
            sSql = sSql & "tsoSalesOrder.CustKey, "
            sSql = sSql & "tsoShipLine.InvtTranKey, "
            sSql = sSql & "tarCustAddr.ShipComplete, "
            sSql = sSql & kTranTypeSOSO & ", "
            sSql = sSql & gsQuoted(gsBuildString(kSOSale, moAppDB, moSysSession)) & ", "
            sSql = sSql & "tsoSalesOrder.TranNoRelChngOrd, "
            sSql = sSql & "tsoSOLine.SOLineNo, "
            sSql = sSql & "tsoSOLineDist.ShipDate "
            sSql = sSql & "FROM " & sTablesUsed & " "

            sWhere = "WHERE " & sWhereClause & " "
            'restrict to Sales Orders only
            sWhere = sWhere & "AND tsoSalesOrder.TranType = " & kTranTypeSOSO & " "
            sWhere = sWhere & "AND tsoSalesOrder.CompanyID = " & gsQuoted(msCompanyID) & " "
            sWhere = sWhere & "AND tsoShipLine.ShipKey IS NULL "
            sWhere = sWhere & "AND tsoShipLine.KitShipLineKey IS NULL "
            sWhere = sWhere & "AND tsoShipLine.PickingComplete = 1 "
            
        Case Else 'Transfer Order
            sSql = "INSERT INTO " & sTableInto & " "
            sSql = sSql & "(ShipLineDistKey,ShipLineKey,LineKey,OrderKey,PickListKey,"
            sSql = sSql & "PickListLineNo,ItemKey,QtyToPick,QtyPicked,ShipUnitMeasKey,ShipMethKey,ShipWhseKey,"
            sSql = sSql & "RcvgWhseKey,InvtTranKey,TranType,TranTypeText,TranNo,LineNbr,ShipDate) "
            sSql = sSql & "SELECT DISTINCT tsoShipLineDist.ShipLineDistKey, "
            sSql = sSql & "tsoShipLine.ShipLineKey, "
            sSql = sSql & "timTrnsfrOrderLine.TrnsfrOrderLineKey, "
            sSql = sSql & "vimTrnsfrOrder.TrnsfrOrderKey, "
            sSql = sSql & "tsoShipLine.PickListKey, "
            sSql = sSql & "tsoShipLine.PickListLineNo, "
            sSql = sSql & "tsoShipLine.ItemKey, "
            sSql = sSql & "tsoShipLineDist.QtyToPick, "
            sSql = sSql & "tsoShipLineDist.QtyShipped, "
            sSql = sSql & "tsoShipLine.ShipUnitMeasKey, "
            sSql = sSql & "vimTrnsfrOrder.ShipMethKey, "
            sSql = sSql & "vimTrnsfrOrder.ShipWhseKey, "
            sSql = sSql & "vimTrnsfrOrder.RcvgWhseKey, "
            sSql = sSql & "tsoShipLine.InvtTranKey, "
            sSql = sSql & kTranTypeIMTR & ", "
            sSql = sSql & gsQuoted(gsBuildString(kSOTransfer, moAppDB, moSysSession)) & ", "
            sSql = sSql & "vimTrnsfrOrder.TranNo, "
            sSql = sSql & "timTrnsfrOrderLine.TrnsfrLineNo, "
            sSql = sSql & "vimTrnsfrOrder.SchdShipDate "
            sSql = sSql & "FROM " & sTablesUsed & " "

            sWhere = "WHERE " & sWhereClause & " "
            sWhere = sWhere & "AND vimTrnsfrOrder.CompanyID = " & gsQuoted(msCompanyID) & " "
            sWhere = sWhere & "AND tsoShipLine.ShipKey IS NULL "
            sWhere = sWhere & "AND tsoShipLine.KitShipLineKey IS NULL "
            sWhere = sWhere & "AND tsoShipLine.PickingComplete = 1 "
            
    End Select
    

    sSql = sSql & sWhere

    moAppDB.ExecuteSQL sSql
    bSelectRows = True
    Exit Function

ErrHandler:
    lErr = Err.Number
    sErr = Err.Description

    ' this was to trap duplicate key errors, but should not happen anymore
    ' (now that a pseudo-deselect is done before the select)
    If Err.Number = 4121 Then
        Resume Next
    End If
End Function


Private Function bInitWorkTables() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim sSql As String
Dim iresult As Integer

    bInitWorkTables = False
    
    On Error Resume Next
    
    'Drop Temp Table if it exists
    sSql = "DROP TABLE #tsoCancelPickWrk1"
    moAppDB.ExecuteSQL sSql
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Create an empty Temp Table
    sSql = "SELECT * INTO #tsoCancelPickWrk1 FROM tsoCancelPickWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSql
    If Err.Number <> 0 Then
'tc to do (all error messages for table init)
        iresult = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgPickListWrkTable)   'inform user that there are problems
        Exit Function
    End If

    'Create primary Key Constraint
    sSql = "ALTER TABLE #tsoCancelPickWrk1 ADD PRIMARY KEY (ShipLineDistKey)"
    On Error Resume Next
    moAppDB.ExecuteSQL sSql
    If Err.Number <> 0 Then
        iresult = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgPickListWrkTable)   'inform user that there are problems
        Exit Function
    End If
    
    'Drop Temp Table if it exists
    sSql = "DROP TABLE #tsoCancelPickWrk2"
    moAppDB.ExecuteSQL sSql
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
    'Create an empty Temp Table
    sSql = "SELECT * INTO #tsoCancelPickWrk2 FROM tsoCancelPickWrk WHERE 1 = 2"
    moAppDB.ExecuteSQL sSql
    If Err.Number <> 0 Then
'tc to do (all error messages for table init)
        iresult = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgPickListWrkTable)   'inform user that there are problems
        Exit Function
    End If

    'Create primary Key Constraint
    sSql = "ALTER TABLE #tsoCancelPickWrk2 ADD PRIMARY KEY (ShipLineDistKey)"
    On Error Resume Next
    moAppDB.ExecuteSQL sSql
    If Err.Number <> 0 Then
        iresult = giSotaMsgBox(Me, moClass.moSysSession, kSOmsgPickListWrkTable)   'inform user that there are problems
        Exit Function
    End If

    bInitWorkTables = True

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "bInitWorkTables", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
            tabSelect.TabVisible(3) = False
            tabSelect.TabsPerRow = tabSelect.TabsPerRow - 1
            moFormCust.Initialize Me, goClass, tabSelect.Name & ";" & tabSelect.Tabs - 1
            Set moFormCust.CustToolbarMgr = tbrMain
            moFormCust.ApplyFormCust
        End If
    End If
#End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_Activate", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub MapControls()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    Set moMapSrch = New Collection
    giCollectionAdd moMapSrch, navControl, mskControl(kSalesOrder).hwnd
    giCollectionAdd moMapSrch, navControlTransfer, mskControl(kTransferOrder).hwnd
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "MapControls", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'*********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True process predefined function keys.
'*********************************************************************
    Dim iNavControl As Integer

    Select Case keycode 'normal function key processing
        Case vbKeyF1 To vbKeyF16              'Function Keys pressed
            gProcessFKeys Me, keycode, Shift  'Run Sage MAS 500 Function Key Processing
    End Select
    
    If Shift = 0 And keycode = vbKeyF5 Then
        iNavControl = moSelect(Me.ActiveControl.Index).SelectionGridF5KeyDown(Me.ActiveControl)
        If iNavControl > 0 Then
            Select Case Me.ActiveControl.Index
                 Case kSalesOrder
                      navControl_GotFocus iNavControl
                 Case kTransferOrder
                      navControlTransfer_GotFocus iNavControl
            End Select
        End If
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_KeyDown", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview
'         property of the form is set to True process the enter key
'         as if the user pressed the tab key.
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter key
            If mbEnterAsTab Then    'enter as tab set in session
                gProcessSendKeys "{Tab}"    'send tab key
                KeyAscii = 0        'cancel enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_KeyPress", VBRIG_IS_FORM                  'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'default to Form_Load error
    mbLoadSuccess = False
    mbLoading = True
    
    'set local variables
    With moClass
        Set moSysSession = .moSysSession
        Set moAppDB = moClass.moAppDB
        Set moAppDB = moClass.moAppDB
    End With
    
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msHomeCurrID = .CurrencyID
        If CBool(.IsModuleActivated(kModuleWM)) = True Then
            mbWMIsLicensed = True
        Else
            mbWMIsLicensed = False
        End If
    End With
    
    Set moMultSelGridMgr = New clsMultiSelGridSettings
    moMultSelGridMgr.Initialize moAppDB, moSysSession.Language, moSelect(), grdSelSettings
         
    'Integrate with IM Flag for tsoOptions table.
    mbIMIntegrated = moClass.moAppDB.Lookup("IntegrateWithIM", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
      'The Transfer tab should not be available if SO & IM are not integrated.
      
    If Not mbIMIntegrated Or Not mbWMIsLicensed Then
        chkInclude(kTransferOrder).Value = vbUnchecked
        chkInclude(kTransferOrder).Enabled = False
        tabSelect.TabEnabled(kTransferOrder) = False
    End If
         
    '-- Set up the form resizing variables
    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With
    
    'Create Work Tables In TempDB
    If Not bInitWorkTables Then
        'Can't initialize Work Tables, so exit
        Exit Sub
    End If

   'Populate the base caption array, using localized strings.
    msBaseCaption(kSalesOrder) = gsBuildString(kSOSalOrders, moAppDB, moClass.moSysSession)
    msBaseCaption(kTransferOrder) = gsBuildString(kTransferOrders, moAppDB, moClass.moSysSession)
    
    #If DEBUGGING = 0 Then
        tabSelect.TabVisible(kNumSelGrids) = False
    #End If
    
    GetModuleOptions
    
    SetupSelectGrid
    
    SetupLineGrid
     
    BindLineGrid
    
    BindGM
    
    BindContextMenu
    
    SetupBars
    
    SetFormState

    'map controls for function keys
    MapControls
    
    'need to capture left mouse clicks to prevent user from double clicking on command buttons after
    'selection grid navigator has been used.
    If moHook Is Nothing Then
        Set moHook = New AFWinHook.clsMsgs
        moHook.SetHook Me.hwnd, HOOK_TYPE_CUSTOM
        moHook.AddMessage WM_LBUTTONDOWN
        moHook.Delayed = True
    End If
    
    RefreshTabCaptions
    
    mbLoading = False
    
    'Form_Load completed successfully
    mbLoadSuccess = True
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_Load", VBRIG_IS_FORM                     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Sub RefreshTabCaptions()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim i As Integer
    For i = 0 To UBound(moSelect())
        tabSelect.TabCaption(i) = moMultSelGridMgr.sBuildCaption(msBaseCaption(i), grdSelection(i), tabSelect.TabEnabled(i))
    Next
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPick", "RefreshTabCaptions", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub GetModuleOptions()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moOptions = New clsModuleOptions
    With moOptions
        Set .oSysSession = moSysSession
        Set .oAppDB = moAppDB
        .sCompanyID = msCompanyID
    End With
    miNbrDecPlaces = moOptions.CI("QtyDecPlaces")
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "GetModuleOptions", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupSelectGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Dim moDDData                As clsDDData
    Dim sRealTableCollection    As Collection
    Dim moSelAssist             As clsSelGridAssist
    Dim bGoodRow                As Boolean
    Dim bGrdRestrictToCompany   As Boolean
    Dim sGrdTableName           As String
    Dim sGrdColumnName          As String
    Dim sGrdParentTblName       As String
    Dim lIndex                  As Long
    Dim lCurrRow                As Long
    Dim lRet                    As Long
    Dim bResult                 As Boolean
    Dim i                       As Integer
    Dim arrStaticValues()       As String
    Dim arrStaticText()         As String
    Dim sLookupRestrict         As String

    'Set up Sales Order Selection Grid
    
    'Instantiate Objects
    Set moSelect(kSalesOrder) = New clsSelection
    Set moDDData = New clsDDData
    Set sRealTableCollection = New Collection
    Set moSelAssist = New clsSelGridAssist
    
    'CUSTOMIZE:  Invoke Add method of sRealTableCollection for each table which
    'will provide raw data.  Primary table should appear first.
    sRealTableCollection.Add "tsoShipLineDist"
    
    moDDData.lInitDDData sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        'Unable to load data from Data Dictionary.
        giSotaMsgBox Me, moSysSession, kmsgCantLoadDDData
        Exit Sub
    End If
        
    'Add Manual Row Entries
    With moSelAssist
        Set .Sys_DB = moAppDB
        Set .Selection = moSelect(kSalesOrder)
        
        .Add "tsoPickList", "PickListNo", gsBuildString(kPickList, moAppDB, moSysSession), , , "SOReprntPick"
        .Add "tsoShipLine", "PickListLineNo", gsBuildString(kPickLine, moAppDB, moSysSession), False
        .Add "tsoSOLineDist", "ShipDate", gsBuildString(kSOShipDate, moAppDB, moSysSession), False, gsGetLocalDateMask()
        .Add "timWarehouse", "WhseID", gsBuildString(kShippingWhse, moAppDB, moSysSession), True 'False
        .Add "tsoSalesOrder", "TranNoRelChngOrd", , , , , " tsoSalesOrder.TranType = " & kTranTypeSOSO _
& " AND (tsoSalesOrder.Status = " & kSOOpenStatus _
& " OR tsoSalesOrder.Status = " & kSOUnAckStatus _
& " )"
        .Add "tsoSOLine", "SOLineNo", gsBuildString(kSOSOLine, moAppDB, moSysSession), False

        sLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)
        sLookupRestrict = sLookupRestrict & " AND ItemType IN (1,2,4,5,6,7,8)"
        .Add "timItem", "ItemID", , , , "Item", sLookupRestrict

        .Add "tciShipMethod", "ShipMethID", gsBuildString(kSOShipvia, moAppDB, moSysSession), True
        .Add "tarCustomer", "CustName"
        .Add "tarCustomer", "CustID", gsBuildString(kSOCustomer, moAppDB, moSysSession), True 'False
        .Add "tarCustAddr", "CustAddrID", gsBuildString(kSOShipToAddress, moAppDB, moSysSession), True 'False
        .Add "tsoSOLineDist", "DeliveryMeth", , False
        .Add "tsoSalesOrder", "CustPONo"
    End With
    
    'add static list for ship priority
    ReDim arrStaticValues(5)
    ReDim arrStaticText(5)
    For i = 1 To 5
        arrStaticValues(i) = i
        arrStaticText(i) = CStr(i)
    Next i
    
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist" _
, "ShipPriority" _
, gsBuildString(kSOShipPriority, moAppDB, moSysSession) _
, gsBuildString(kSOShipPriority, moAppDB, moSysSession) _
, False _
, "" _
, arrStaticValues() _
, arrStaticText())
    End With

   'add static list for drop ships
    ReDim arrStaticValues(4)
    ReDim arrStaticText(4)
    arrStaticValues(1) = 1
    arrStaticText(1) = gsBuildString(kShip, moAppDB, moSysSession)
    arrStaticValues(2) = 2
    arrStaticText(2) = gsBuildString(kSODropShip2, moAppDB, moSysSession)
    arrStaticValues(3) = 3
    arrStaticText(3) = gsBuildString(kslvCounterSale, moAppDB, moSysSession)
    arrStaticValues(4) = 4
    arrStaticText(4) = gsBuildString(kslvWillCall, moAppDB, moSysSession)
    
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist" _
, "DeliveryMeth" _
, gsBuildString(kSODelMethod, moAppDB, moSysSession) _
, gsBuildString(kSODelMethod, moAppDB, moSysSession) _
, False _
, "" _
, arrStaticValues() _
, arrStaticText())
    End With
    
    If Not moSelect(kSalesOrder).bInitSelect(sRealTableCollection(1), mskControl(kSalesOrder), navControl, nbrControl(kSalesOrder), _
curControl(kSalesOrder), grdSelection(kSalesOrder), fraSelect(kSalesOrder), Me, moDDData, _
19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, False) Then
        'Setup of the Selection Grid Failed.
        giSotaMsgBox Me, moSysSession, kmsgSetupSelectionGridFail

         'Exit this function
        Exit Sub
    End If

    moSelect(kSalesOrder).PopulateSelectionGrid
    
    'Hide Selection Rows Not Required
    For lIndex = 1 To moSelect(kSalesOrder).lMaxSelGridRows
        bGoodRow = False
        moSelect(kSalesOrder).lGetTblColFromGrdRow lIndex, sGrdTableName, sGrdColumnName, _
sGrdParentTblName, bGrdRestrictToCompany
        If sGrdTableName = "tsoPickList" And sGrdColumnName = "PickListNo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoShipLine" And sGrdColumnName = "PickListLineNo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSOLineDist" And sGrdColumnName = "ShipDate" Then
            bGoodRow = True
        End If
        
        If sGrdTableName = "timWarehouse" And sGrdColumnName = "WhseID" Then
           'Hide WhseId if IM is not active
            If mbIMIntegrated Then
                bGoodRow = True
            Else
                bGoodRow = False
            End If
        End If
                
        If sGrdTableName = "tsoSOLineDist" And sGrdColumnName = "ShipPriority" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSalesOrder" And sGrdColumnName = "TranNoRelChngOrd" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSalesOrder" And sGrdColumnName = "CustPONo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tsoSOLine" And sGrdColumnName = "SOLineNo" Then
            bGoodRow = True
        End If
        If sGrdTableName = "timItem" And sGrdColumnName = "ItemID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tciShipMethod" And sGrdColumnName = "ShipMethID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tarCustomer" And sGrdColumnName = "CustID" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tarCustomer" And sGrdColumnName = "CustName" Then
            bGoodRow = True
        End If
        If sGrdTableName = "tarCustAddr" And sGrdColumnName = "CustAddrID" Then
            bGoodRow = True
        End If
        If Not bGoodRow Then
            moSelect(kSalesOrder).SetSelectionRowVisibility sGrdTableName, sGrdColumnName, False
        End If
    Next lIndex
    
    'Order Rows
    With moSelect(kSalesOrder)
        lCurrRow = .lGetGrdRowFromTblCol("tsoPickList", "PickListNo")
        lRet = .lMoveRow(lCurrRow, 1)
        lCurrRow = .lGetGrdRowFromTblCol("tsoShipLine", "PickListLineNo")
        lRet = .lMoveRow(lCurrRow, 2)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipDate")
        lRet = .lMoveRow(lCurrRow, 3)
        lCurrRow = .lGetGrdRowFromTblCol("timWarehouse", "WhseID")
        lRet = .lMoveRow(lCurrRow, 4)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipPriority")
        lRet = .lMoveRow(lCurrRow, 5)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "TranNoRelChngOrd")
        lRet = .lMoveRow(lCurrRow, 6)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLine", "SOLineNo")
        lRet = .lMoveRow(lCurrRow, 7)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "CustPONo")
        lRet = .lMoveRow(lCurrRow, 8)
        lCurrRow = .lGetGrdRowFromTblCol("timItem", "ItemID")
        lRet = .lMoveRow(lCurrRow, 9)
        lCurrRow = .lGetGrdRowFromTblCol("tciShipMethod", "ShipMethID")
        lRet = .lMoveRow(lCurrRow, 10)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustomer", "CustID")
        lRet = .lMoveRow(lCurrRow, 11)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustomer", "CustName")
        lRet = .lMoveRow(lCurrRow, 12)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustAddr", "CustAddrID")
        lRet = .lMoveRow(lCurrRow, 13)
    End With
        
   'Set up Transfer Order Selection grid
    
    sRealTableCollection.Remove 1
    sRealTableCollection.Add "vimTrnsfrOrder"
    
    Set moDDData = Nothing
    Set moDDData = New clsDDData
    
    moDDData.lInitDDData sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        'Unable to load data from Data Dictionary.
        giSotaMsgBox Me, moSysSession, kmsgCantLoadDDData
        'Exit this function
        Exit Sub
    End If
    
    Set moSelect(kTransferOrder) = New clsSelection
    
    'Add Manual Row Entries
    With moSelAssist
        Set .Sys_DB = moAppDB
        Set .Selection = moSelect(kTransferOrder)
        
        .Add "timTrnsfrOrderLine", "TrnsfrLineNo", , False
        .Add "tsoPickList", "PickListNo", gsBuildString(kPickList, moAppDB, moSysSession), False, , "SOReprntPick"
        .Add "tsoShipLine", "PickListLineNo", gsBuildString(kPickLine, moAppDB, moSysSession), False
        .Add "vimTrnsfrOrder", "SchdShipDate", gsBuildString(kSOShipDate, moAppDB, moSysSession), False, gsGetLocalDateMask()

        sLookupRestrict = "CompanyID = " & gsQuoted(msCompanyID)
        sLookupRestrict = sLookupRestrict & " AND ItemType IN (1,2,4,5,6,7,8)"
        .Add "timItem", "ItemID", , False, , "Item", sLookupRestrict
     End With
        
    'Initialize selection grid for Transfer Order tab.
    If Not moSelect(kTransferOrder).bInitSelect(sRealTableCollection(1), mskControl(kTransferOrder), navControlTransfer, nbrControl(kTransferOrder), _
curControl(kTransferOrder), grdSelection(kTransferOrder), fraSelect(kTransferOrder), Me, moDDData, _
19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, False) Then
       'Setup of the Selection Grid Failed.
        giSotaMsgBox Me, moSysSession, kmsgSetupSelectionGridFail
        'Exit this function
        Exit Sub
    End If
 
    'Populate selection grid for Transfer Order tab
    moSelect(kTransferOrder).PopulateSelectionGrid
    
    With moSelect(kTransferOrder)
        .SetSelectionRowVisibility "vimTrnsfrOrder", "Status", False
        .SetSelectionRowVisibility "vimTrnsfrOrder", "TranDate", False
        .SetSelectionRowVisibility "vimTrnsfrOrder", "CloseDate", False
        .SetSelectionRowVisibility "vimTrnsfrOrder", "ReqDelvDate", False
        .SetSelectionRowVisibility "vimTrnsfrOrder", "TransitWhseKey", False
    End With
    
     
    'Order Rows
    With moSelect(kTransferOrder)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "TranNo")
        lRet = .lMoveRow(lCurrRow, 1)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "ShipMethKey")
        lRet = .lMoveRow(lCurrRow, 5)
     End With
        
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    If Not sRealTableCollection Is Nothing Then
        Set sRealTableCollection = Nothing
    End If
    
    If Not moSelAssist Is Nothing Then
        Set moSelAssist = Nothing
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "SetupSelectGrid", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindLineGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moDMGrid = New clsDmGrid
    With moDMGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmCancelPicks
        Set .Grid = grdLine
        .Table = "#tsoCancelPickWrk1"
        .UniqueKey = "ShipLineDistKey"
        .NoAppend = True
        .OrderBy = "PickListKey, PickListLineNo"
        
        .BindColumn "ShipLineDistKey", Nothing, SQL_INTEGER
        .BindColumn "ShipComplete", kColShipComplete, SQL_SMALLINT
        
        .BindColumn "QtyToPick", kColQtyToPick, SQL_DECIMAL
        .BindColumn "QtyPicked", kColQtyPicked, SQL_DECIMAL
        .BindColumn "InvtTranKey", kColInvTranKey, SQL_INTEGER
        
        .BindColumn "PickListKey", Nothing, SQL_INTEGER
        .LinkSource "tsoPickList", "PickListKey = <<PickListKey>>"
        .Link kColPickList, "PickListNo"
        
        .BindColumn "PickListLineNo", kcolPickLineNo, SQL_SMALLINT
        .BindColumn "KitShipLineKey", kColKitLineKey, SQL_INTEGER

        .BindColumn "TranNo", kColOrder, SQL_CHAR
        .BindColumn "LineNbr", kColLine, SQL_SMALLINT
        .BindColumn "TranTypeText", kColType, SQL_CHAR
        .BindColumn "ShipDate", kColShipDate, SQL_DATE
        '?BindColumn "ShipDate", kColShipDateRaw
        
               
        .BindColumn "SOLineDistKey", Nothing, SQL_INTEGER
        .LinkSource "tsoSOLineDist", "SOLineDistKey = <<SOLineDistKey>>"
        .Link kColShipPriority, "ShipPriority"
        
        ' supposed to come from dist for SO, tfr order for transfer
        .BindColumn "ShipLineKey", Nothing, SQL_INTEGER
        '.LinkSource "tsoShipLine", "ShipLineKey = <<ShipLineKey>>"
        '.Link kColShipDateRaw, "ShipDate"
        '.Link kColShipDate, "ShipDate"

        .BindColumn "ItemKey", Nothing, SQL_INTEGER
        .LinkSource "timItem", "ItemKey = <<ItemKey>>"
        .Link kColItem, "ItemID"
        .LinkSource "timItemDescription", "ItemKey = <<ItemKey>>"
        .Link kColDescription, "ShortDesc"

        .BindColumn "SubItemKey", Nothing, SQL_INTEGER
        .LinkSource "timItem", "ItemKey = <<SubItemKey>>"
        .Link kColSubItem, "ItemID"
        .LinkSource "timItemDescription", "ItemKey = <<SubItemKey>>"
        .Link kColSubDesc, "ShortDesc"

        .BindColumn "ShipUnitMeasKey", Nothing, SQL_INTEGER
        .LinkSource "tciUnitMeasure", "UnitMeasKey = <<ShipUnitMeasKey>>"
        .Link kColUOM, "UnitMeasID"

        .BindColumn "ShipToCustAddrKey", Nothing, SQL_INTEGER
        .LinkSource "tarCustAddr", "AddrKey = <<ShipToCustAddrKey>>"
        .Link kColShipToAddr, "CustAddrID"

        .BindColumn "ShipWhseKey", Nothing, SQL_INTEGER
        .LinkSource "timWarehouse", "WhseKey = <<ShipWhseKey>>"
        .Link kColShipWhse, "WhseID"
        
        .BindColumn "RcvgWhseKey", Nothing, SQL_INTEGER
        .LinkSource "timWarehouse", "WhseKey = <<RcvgWhseKey>>"
        .Link kColRcvgWhse, "WhseID"
        
        .BindColumn "CustKey", Nothing, SQL_INTEGER
        .LinkSource "tarCustomer", "CustKey = <<CustKey>>"
        .Link kColCustomer, "CustID"
        .Link kColCustName, "CustName"

        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER
        .LinkSource "tciShipMethod", "ShipMethKey = <<ShipMethKey>>"
        .Link kColShipVia, "ShipMethID"
        
        '*************************************************************************************
        '*************************************************************************************
        'SGS DEJ for IA (START)
        '*************************************************************************************
        '*************************************************************************************
        .LinkSource "vluBOLTran_SGS", .Table & ".OrderKey=vluBOLTran_SGS.TranKey And " & .Table & ".TranTypeText=vluBOLTran_SGS.TranTypeDesc ", kDmJoin, LeftOuter
        .Link giGetValidInt(kColBOL), "BOLNo"
        '*************************************************************************************
        '*************************************************************************************
        'SGS DEJ for IA (STOP)
        '*************************************************************************************
        '*************************************************************************************
                
        .Init
    End With
        
    gGridHideColumn grdLine, kColPickCheck

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "BindLineGrid", VBRIG_IS_FORM                   'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupLineGrid()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set general grid properties
    gGridSetProperties grdLine, kMaxCols, kGridDataSheetNoAppend
    gGridSetColors grdLine
        
    With grdLine
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .RowHeaderDisplay = DispNumbers
        .DisplayRowHeaders = True
    End With
     
    '**************************************************************************
    '**************************************************************************
    'SGS DEJ IA (START)
    '**************************************************************************
    '**************************************************************************
    gGridSetHeader grdLine, kColBOL, "BOL"
    gGridSetColumnWidth grdLine, kColBOL, 8
    gGridSetColumnType grdLine, kColBOL, SS_CELL_TYPE_EDIT
    gGridLockColumn grdLine, kColBOL
    '**************************************************************************
    '**************************************************************************
    'SGS DEJ IA (STOP)
    '**************************************************************************
    '**************************************************************************
     
    'set column captions
    gGridSetHeader grdLine, kColOrder, gsBuildString(kSOOrderNumber, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColType, gsBuildString(kTypeCol, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColLine, gsBuildString(kSOsoLilne, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColPickList, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoPickList", "PickListNo"), 1)
    gGridSetHeader grdLine, kcolPickLineNo, gsBuildString(kSOsoLilne, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColItem, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("timItem", "ItemID"), 1)
    gGridSetHeader grdLine, kColQtyToPick, gsBuildString(kSOQtyToPick, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColQtyPicked, gsBuildString(kSOQtyPicked, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColUOM, gsBuildString(kSOUOM, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColShipToAddr, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tarCustAddr", "CustAddrID"), 1)
    gGridSetHeader grdLine, kColShipVia, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tciShipMethod", "ShipMethID"), 1)
    gGridSetHeader grdLine, kColShipDate, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoSOLineDist", "ShipDate"), 1)
    gGridSetHeader grdLine, kColShipPriority, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tsoSOLineDist", "ShipPriority"), 1)
    gGridSetHeader grdLine, kColShipWhse, gsBuildString(kShipWhse, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColRcvgWhse, gsBuildString(kShortRcvgWhse, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColCustomer, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tarCustomer", "CustID"), 1)
    gGridSetHeader grdLine, kColCustName, gsGridReadCell(grdSelection(kSalesOrder), moSelect(kSalesOrder).lGetGrdRowFromTblCol("tarCustomer", "CustName"), 1)
    gGridSetHeader grdLine, kColDescription, gsBuildString(kItemDescriptionTbl, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColSubItem, gsBuildString(kSOSRegSubItem, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColSubDesc, gsBuildString(kSOSubItemDesc, moAppDB, moSysSession)
    gGridSetHeader grdLine, kColInvTranKey, "-"
    gGridSetHeader grdLine, kColAllowSubItem, "-"
    gGridSetHeader grdLine, kColTrackMeth, "-"
    gGridSetHeader grdLine, kColTrackQtyAtBin, "-"
    gGridSetHeader grdLine, kColInvTranID, "-"
    gGridSetHeader grdLine, kColKitLineKey, "-"
    gGridSetHeader grdLine, kColShipComplete, "-"
    gGridSetHeader grdLine, kColShipDateRaw, "-"

  'Set column widths
    gGridSetColumnWidth grdLine, kColOrder, 16
    gGridSetColumnWidth grdLine, kColLine, 4
    gGridSetColumnWidth grdLine, kColPickList, 9
    gGridSetColumnWidth grdLine, kcolPickLineNo, 4
    gGridSetColumnWidth grdLine, kColItem, 17
    gGridSetColumnWidth grdLine, kColQtyToPick, 10
    gGridSetColumnWidth grdLine, kColQtyPicked, 10
    gGridSetColumnWidth grdLine, kColUOM, 8
    gGridSetColumnWidth grdLine, kColShipToAddr, 15
    gGridSetColumnWidth grdLine, kColShipVia, 15
    gGridSetColumnWidth grdLine, kColShipDate, 10
    gGridSetColumnWidth grdLine, kColShipPriority, 8
    gGridSetColumnWidth grdLine, kColShipWhse, 10
    gGridSetColumnWidth grdLine, kColRcvgWhse, 10
    gGridSetColumnWidth grdLine, kColCustomer, 12
    gGridSetColumnWidth grdLine, kColCustName, 40
    gGridSetColumnWidth grdLine, kColDescription, 20
    gGridSetColumnWidth grdLine, kColSubItem, 20
    gGridSetColumnWidth grdLine, kColSubDesc, 20
    gGridSetColumnWidth grdLine, kColInvTranKey, 10
    gGridSetColumnWidth grdLine, kColAllowSubItem, 10
    gGridSetColumnWidth grdLine, kColTrackMeth, 10
    gGridSetColumnWidth grdLine, kColTrackQtyAtBin, 10
    gGridSetColumnWidth grdLine, kColInvTranID, 10
    gGridSetColumnWidth grdLine, kColKitLineKey, 10
    gGridSetColumnWidth grdLine, kColShipComplete, 10
    gGridSetColumnWidth grdLine, kColShipDateRaw, 10


  'Set column types
    gGridSetColumnType grdLine, kColOrder, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColLine, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColType, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColPickList, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kcolPickLineNo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColQtyToPick, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColQtyPicked, SS_CELL_TYPE_FLOAT
    gGridSetColumnType grdLine, kColUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipToAddr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdLine, kColShipPriority, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdLine, kColShipWhse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColRcvgWhse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColCustomer, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColCustName, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColDescription, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColSubDesc, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColInvTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColAllowSubItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColTrackMeth, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColTrackQtyAtBin, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColInvTranID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColKitLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipComplete, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLine, kColShipDateRaw, SS_CELL_TYPE_EDIT
    
    'lock protected columns
    gGridLockColumn grdLine, kColOrder
    gGridLockColumn grdLine, kColLine
    gGridLockColumn grdLine, kColType
    gGridLockColumn grdLine, kColPickList
    gGridLockColumn grdLine, kcolPickLineNo
    gGridLockColumn grdLine, kColItem
    gGridLockColumn grdLine, kColSubItem
    gGridLockColumn grdLine, kColQtyToPick
    gGridLockColumn grdLine, kColQtyPicked
    gGridLockColumn grdLine, kColUOM
    gGridLockColumn grdLine, kColShipToAddr
    gGridLockColumn grdLine, kColShipVia
    gGridLockColumn grdLine, kColShipDate
    gGridLockColumn grdLine, kColShipPriority
    gGridLockColumn grdLine, kColShipWhse
    gGridLockColumn grdLine, kColRcvgWhse
    gGridLockColumn grdLine, kColCustomer
    gGridLockColumn grdLine, kColCustName
    gGridLockColumn grdLine, kColDescription
    gGridLockColumn grdLine, kColShipVia
    gGridLockColumn grdLine, kColSubDesc
    
    'freeze columns
    gGridFreezeCols grdLine, 6
    
    'setup numeric formatting
    SetGridNumericAttr grdLine, kColQtyToPick, miNbrDecPlaces, kMaxQtyLen
    SetGridNumericAttr grdLine, kColQtyPicked, miNbrDecPlaces, kMaxQtyLen
    
    
    'Hide warehouse column if IM is not integrated
    If Not (mbIMIntegrated) Then
         gGridHideColumn grdLine, kColShipWhse
         gGridHideColumn grdLine, kColRcvgWhse
         gGridHideColumn grdLine, kColSubItem
         gGridHideColumn grdLine, kColSubDesc
    End If
     
    
    'hide columns
    gGridHideColumn grdLine, kColInvTranKey
    gGridHideColumn grdLine, kColAllowSubItem
    gGridHideColumn grdLine, kColTrackMeth
    gGridHideColumn grdLine, kColTrackQtyAtBin
    gGridHideColumn grdLine, kColInvTranID
    gGridHideColumn grdLine, kColKitLineKey
    gGridHideColumn grdLine, kColShipComplete
    gGridHideColumn grdLine, kColShipDateRaw
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "SetupLineGrid", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Sub SetGridNumericAttr(grd As Control, lCol As Long, iDecPlaces As Integer, sMaxFloat As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set the column type based on the currency attributes passed in
    gGridSetColumnType grd, lCol, SS_CELL_TYPE_FLOAT
    With grd
        .Col = lCol
        .Col2 = lCol
        .Row = -1
        .Row2 = -1
        .BlockMode = True
        .TypeFloatMax = sMaxFloat
        .TypeFloatDecimalPlaces = iDecPlaces
        .TypeFloatSeparator = True
        .BlockMode = False
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "SetGridNumericAttr", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindGM()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moGM = New clsGridMgr
    With moGM
        Set .Grid = grdLine
        Set .DM = moDMGrid
        Set .Form = frmCancelPicks
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "BindGM", VBRIG_IS_FORM                         'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmCancelPicks
        .BindGrid moGM, grdLine.hwnd
        .Bind "*APPEND", grdLine.hwnd, kEntTypeSOPicking
        .Bind "*SELECTION", grdSelection(kSalesOrder).hwnd
        .Bind "*SELECTION", grdSelection(kTransferOrder).hwnd
        .Init
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "BindContextMenu", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next

    Dim lActiveRow As Long
    
    Select Case True
        Case ctl Is grdLine
            lActiveRow = glGetValidLong(grdLine.ActiveRow)
            ETWhereClause = "ShipLineKey = " & CStr(glGetValidLong(moDMGrid.GetColumnValue(lActiveRow, "ShipLineKey")))
        
        Case Else
            ETWhereClause = ""
    End Select

    Err.Clear

End Function

Public Function CMMultiSelect(ByVal ctl As Object) As Object
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                The form's selection object MUST be declared as PUBLIC.
'                However, array variables in a form can not be declared public.
'                Therefore, for those small handful of applications that have
'                their selection object declared as Private and/or declared
'                as an array, this mechanism will allow the form to pass back
'                the selection object to be worked on.
'
'                Therefore, if applications are having an issue, they must either
'                declare their selection object as public or implement this call back
'                method
'
'   Parameters:
'                Ctl <in> - Control that selected the task
'
'   Returns:
'                A selection object
'*******************************************************************************
On Error Resume Next
    
    ' moSelect is a private control array, therefore ...
    Set CMMultiSelect = moSelect(ctl.Index)
    
    Err.Clear
    
End Function

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    CMAppendContextMenu = False

    Dim sDeleteLine As String
    
    If ctl Is grdLine Then
        If grdLine.MaxRows > 0 Then
            sDeleteLine = gsBuildString(kDeleteLine, moClass.moAppDB, moClass.moSysSession)
            AppendMenu hmenu, MF_ENABLED, 5003, sDeleteLine
            CMAppendContextMenu = True
            Exit Function
        End If
    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "CMAppendContextMenu", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Public Function CMMenuSelected(ctl As Control, lTaskID As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'************************************************************************************
'      Desc: Called when a popup context menu item is selected.
'            Called because menu item was added by CMAppendContextMenu event.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            lTaskID: The Task ID of the selected menu item.
'   Returns: True if successful; False if unsuccessful.
'************************************************************************************
    CMMenuSelected = False

    Dim lActiveRow As Long
    Dim iIndex As Integer
        
    If (ctl Is grdLine And lTaskID = 5003) Then
        lActiveRow = grdLine.ActiveRow
        If lActiveRow <= 0 Then
            Exit Function
        Else
            If giGetValidInt(gsGridReadCell(grdLine, lActiveRow, kColInvTranID)) > 0 Then 'do not back out distributions that have been committed
                'reset distribution backout/setvalue
            End If
            moGM.GridDelete
            'moDMGrid.SetRowDirty lActiveRow
            moDMGrid.Save
            SetFormState
        End If
    End If
        
    CMMenuSelected = True
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "CMMenuSelected", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function

Private Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

Dim i As Integer

    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moHook Is Nothing Then
        moHook.EndHook
        Set moHook = Nothing
    End If
        
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moGM Is Nothing Then
        moGM.UnloadSelf
        Set moGM = Nothing
    End If
    
    If Not moDMGrid Is Nothing Then
        moDMGrid.UnloadSelf
        Set moDMGrid = Nothing
    End If
    
    For i = 0 To UBound(moSelect)
        If Not moSelect(i) Is Nothing Then
            Set moSelect(i) = Nothing
        End If
    Next
    
    If Not moMultSelGridMgr Is Nothing Then
        Set moMultSelGridMgr = Nothing
    End If

    If Not moOptions Is Nothing Then
        Set moOptions = Nothing
    End If

    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
           
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "PerformCleanShutDown", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub SetupBars()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
    '-- Setup the status bar
    With sbrMain
        Set .Framework = moClass.moFramework
        .BrowseVisible = False
        .StatusVisible = False
    End With
    
    '-- Setup the Toolbar
    With tbrMain
        .LocaleID = mlLanguage
    End With
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "SetupBars", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim iConfirmUnload As Integer
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False
      
        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                'Do Nothing
            
            Case Else
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else
            'Do Nothing
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_QueryUnload", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
        '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, fraLine, grdLine
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, fraLine, grdLine
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_Resize", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    'Set the Class object to Nothing
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "Form_Unload", VBRIG_IS_FORM                    'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdLine_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGM.Grid_Change Col, Row
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdLine_Change", VBRIG_IS_FORM                 'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub


Private Sub grdLine_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    moGM.Grid_Click Col, Row
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdLine_Click", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_ColWidthChange(ByVal Col1 As Long, ByVal Col2 As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If glGridGetActiveRow(grdLine) < -1 Then 'Work Around: Bug in clsGridLookup will use -32767 as Active row (this value is returned from the grid.ActiveRow property called in the class)
        Exit Sub
    End If
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdLine_ColWidthChange", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_KeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdLine_KeyDown", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_LeaveCell Col, Row, NewCol, NewRow
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdLine_LeaveCell", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdLine_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    moGM.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdLine_TopLeftChange", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

'********************** Selection Stuff ****************************
Private Sub grdSelection_EditMode(Index As Integer, ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                    'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).EditMode Col, Row, Mode, ChangeMade
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdSelection_EditMode", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridGotFocus
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdSelection_GotFocus", VBRIG_IS_FORM          'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_KeyDown(Index As Integer, keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridKeyDown keycode, Shift
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdSelection_KeyDown", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_LeaveCell(Index As Integer, ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridLeaveCell Col, Row, NewCol, NewRow, Cancel
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdSelection_LeaveCell", VBRIG_IS_FORM         'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub grdSelection_TopLeftChange(Index As Integer, ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(Index).SelectionGridTopLeftChange OldLeft, OldTop, NewLeft, NewTop
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "grdSelection_TopLeftChange", VBRIG_IS_FORM     'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Public Function CMRightMouseDown(lWndHandle As Long, lParam As Long) As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

    Dim y As Long
    Dim x As Long
    Dim lRow As Long
    Dim lCol As Long

    CMRightMouseDown = True
    
    '-- Select the row that was right-clicked on
    If lWndHandle = grdLine.hwnd Then
        y = ((lParam And &HFFFF0000) / &H10000) * Screen.TwipsPerPixelY
        x = (lParam And &HFFFF&) * Screen.TwipsPerPixelX
        
        grdLine.GetCellFromScreenCoord lCol, lRow, x, y
                     
        Select Case lRow
            Case 0, -1, -2  '-- RightClick on Column Header or Gray area
                'Do Nothing
             
            Case Else       '-- RightClick on grid proper
                glRC = SendMessage(lWndHandle, WM_LBUTTONDOWN, 0, lParam)
                glRC = SendMessage(lWndHandle, WM_LBUTTONUP, 0, lParam)   ' Simulate click on grid
                'gGridSetSelectRow grdLine, lRow
                gGridSetActiveCell grdLine, lRow, 0
        End Select
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "CMRightMouseDown", VBRIG_IS_FORM               'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function


Private Sub mskControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus mskControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).mskControlLostFocus
    bMaskCtrlLostFocus = True
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "mskControl_LostFocus", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navControlTransfer_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(kTransferOrder).LookupControlClick Index, ""
           Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "navControlTransfer_GotFocus", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub nbrControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).nbrControlLostFocus
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "nbrControl_LostFocus", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub curControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).curControlLostFocus
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "curControl_LostFocus", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub navControl_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    If Not mbLoading Then moSelect(kSalesOrder).LookupControlClick Index, ""
         Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "navControl_GotFocus", VBRIG_IS_FORM            'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub HandletoolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If

'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
Dim bResult As Boolean
Dim bShipCompLineExists As Boolean

    Select Case sKey
        Case kTbProceed
            If moDMGrid.Save = kDmSuccess Then
                
                bShipCompLineExists = bShipCompleteLineExists()
                
                If bShipCompLineExists Then
                    'Check for whether any ship line has the ship complete flag
                    WarnShipComplete
                    'Save any change made to grid due to the Ship Complete logic
                    moDMGrid.Save
                End If
                
                If Not bTempTableEmpty Then
                'Call the cancel pick sp to cancel the pick for the shiplines still
                'in the temp table
                    bDeleteRows
                End If
            End If
            
            'Update form state
            SetFormState
            
            If Not bShipCompLineExists Then
                'Refresh the grid so the deleted lines would show any more
                grdLine.redraw = False
                moDMGrid.Refresh
                grdLine.redraw = True
            Else
                'Call the select button again to reload the undeleted ship lines that user
                'want to keep for Ship Complete
                Call cmdSelect_Click
            End If

        Case kTbClose
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
              
        Case kTbHelp
            gDisplayFormLevelHelp Me

        Case Else
            tbrMain.GenericHandler sKey, Me, moDMGrid, moClass
    End Select
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "HandletoolbarClick", VBRIG_IS_FORM             'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub WarnShipComplete()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
Dim lRows           As Long
Dim lRow            As Long
Dim iShipComplete   As Integer
Dim iAnswer         As Integer
Dim lShipLineKey    As Long
Dim bDeleteAll      As Boolean

    lRows = grdLine.MaxRows
    If lRows = 0 Then Exit Sub
    
    'Loop through the grid warning the user if any rows belong to a customer that requires complete shipmnets
    'If the response is yes(delete it) then continue, else delete it from the worktable and continue looping
    For lRow = lRows To 1 Step -1
        iShipComplete = giGetValidInt(gsGridReadCell(grdLine, lRow, kColShipComplete))
        If (iShipComplete = kiShipComplete) And (bDeleteAll = False) Then
            iAnswer = giSotaMsgBox(Nothing, moClass.moSysSession, kSOmsgDelShipComplete)
            Select Case iAnswer
                Case kretNo
                    'User wants to keep the row, remove the row from the temp table
                    moGM.GridDelete
                    
                Case kretYestoAll
                    bDeleteAll = True
            End Select
        End If
    Next lRow
        
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "WarnShipComplete", VBRIG_IS_FORM              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function bShipCompleteLineExists() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 Dim sSql As String
 Dim rs As Object
    
    bShipCompleteLineExists = False
    
    sSql = "SELECT * FROM #tsoCancelPickWrk1 WHERE ShipComplete = 1 "
    
    Set rs = oClass.moAppDB.OpenRecordset(sSql, kSnapshot, kOptionNone)
    
    If Not rs.IsEmpty Then
        bShipCompleteLineExists = True
    End If
    
    rs.Close
    Set rs = Nothing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "bShipCompleteLineExists", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Function bTempTableEmpty() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
 Dim sSql As String
 Dim rs As Object
    
    bTempTableEmpty = False
   
    
    sSql = "SELECT * FROM #tsoCancelPickWrk1 "
    
    Set rs = oClass.moAppDB.OpenRecordset(sSql, kSnapshot, kOptionNone)
    
    If rs.IsEmpty Then
        bTempTableEmpty = True
    Else
        bTempTableEmpty = False
    End If
    
    rs.Close
    Set rs = Nothing

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "bTempTableEmpty", VBRIG_IS_FORM           'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Private Sub tabSelect_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
    
'ltd - this code suppose to check when tab change, if the grid selection does not set to any parameters
'      then change the grid selection option for that line to All, but it does not quite work at this time.
'    If PreviousTab <> kNumSelGrids Then          'Assumes the last tab is not used for a visible selection grid.
'        If moSelect(PreviousTab).lValidateGrid(True) <> 0 Then 'True indicates than any missing criteria will be set to 'All'
'            giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
'            Exit Sub
'        End If
'
'        tabSelect.TabCaption(PreviousTab) = _
'            moMultSelGridMgr.sBuildCaption(msBaseCaption(PreviousTab), _
'                grdSelection(PreviousTab), tabSelect.TabEnabled(PreviousTab))
'    End If
    
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPick", "tabSelect_Click", VBRIG_IS_FORM                'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}

End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    HandletoolbarClick Button
        Exit Sub
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, "frmCancelPicks", "tbrMain_ButtonClick", VBRIG_IS_FORM       'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub

Private Sub bDeleteRows()
On Error GoTo ExpectedErrorRoutine

Dim sSP         As String
Dim lRetVal     As Long
Dim sSql As String
Dim iDeleteDistributionOnly As Integer
      
    lRetVal = 0
    
    If glGridGetDataRowCnt(grdLine) < 1 Then Exit Sub
    
    Me.MousePointer = vbHourglass
    
    sSP = "spsoDelShipLines"
    With moClass.moAppDB
        .SetInParam kDeleteDistributionOnlyNo
        .SetOutParam lRetVal
        .ExecuteSP sSP
        lRetVal = .GetOutParam(2)
        .ReleaseParams
    End With
    
    Me.MousePointer = vbDefault
    
    Select Case lRetVal
    Case 0
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, sSP & " 0"
        Exit Sub
    Case 1
        giSotaMsgBox Me, moSysSession, kmsgPicksCancelled
        
        'Clear the temp table #tsoCancelPickWrk1 used by the deletion sp
        sSql = "TRUNCATE TABLE #tsoCancelPickWrk1"
        moClass.moAppDB.ExecuteSQL sSql
        If Err.Number <> 0 Then
            gClearSotaErr
        End If
        
        Exit Sub
    Case 2
        giSotaMsgBox Me, moSysSession, kmsgPickOnShipment
        Exit Sub
    End Select

Exit Sub
ExpectedErrorRoutine:
Me.MousePointer = vbDefault
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, Err.Description
gClearSotaErr
Exit Sub

End Sub



#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If


Private Sub cmdDeSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDeSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDeSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub mskControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange mskControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress mskControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus mskControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub nbrControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If











Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property



Private Sub CleanupLogicalLocks()
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
'*************************************************************************
' Desc:   This routine will clean up any orphaned ship line due to any
'         abnormal exiting from the Create Pick List Task which left the
'         picking complete flag to be false.  By reversing the picking complete
'         flag to the correct stage (true) the ship lines can be edited
'         again by the reprint pick list task or the cancel pick task.
'*************************************************************************
    
Dim lRetVal As Long
    
    On Error Resume Next
    With moAppDB
        .SetInParam LOCKTYPE_PICKLIST_RECOVERY
        .SetOutParam lRetVal
        .ExecuteSP "spsmLogicalLockCleanup"
        lRetVal = glGetValidLong(.GetOutParam(2))
        .ReleaseParams
    End With
    
    If lRetVal <> 1 Or Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, "spsmLogicalLockCleanup" & " 0"
        Exit Sub
    End If

'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "CleanupLogicalLocks", VBRIG_IS_FORM                              'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++
End Sub







