VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmSOGenBlanket 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Generate Sales Orders"
   ClientHeight    =   5025
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   9060
   HelpContextID   =   53203
   Icon            =   "sozda102.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5025
   ScaleWidth      =   9060
   ShowInTaskbar   =   0   'False
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   570
      WhatsThisHelpID =   75
      Width           =   1785
      _ExtentX        =   3149
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   27
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin NEWSOTALib.SOTACurrency curNewSOAmt 
      Height          =   285
      Left            =   1500
      TabIndex        =   6
      Top             =   1260
      WhatsThisHelpID =   53220
      Width           =   1785
      _Version        =   65536
      bShowCurrency   =   0   'False
      _ExtentX        =   3149
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      bProtected      =   -1  'True
      mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin VB.CheckBox chkIssueSO 
      Alignment       =   1  'Right Justify
      Caption         =   "&Print Sales Order"
      Height          =   285
      Left            =   6930
      TabIndex        =   13
      Top             =   600
      WhatsThisHelpID =   53219
      Width           =   1815
   End
   Begin SOTACalendarControl.SOTACalendar dteSODate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   1500
      TabIndex        =   4
      Top             =   930
      WhatsThisHelpID =   53218
      Width           =   1785
      _ExtentX        =   3149
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin NEWSOTALib.SOTAMaskedEdit txtNewSO 
      Height          =   285
      Left            =   1500
      TabIndex        =   2
      Top             =   600
      WhatsThisHelpID =   53217
      Width           =   1785
      _Version        =   65536
      _ExtentX        =   3149
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      lMaxLength      =   10
   End
   Begin VB.CommandButton cmdClearAll 
      Caption         =   "&Clear All"
      Height          =   375
      Left            =   1590
      TabIndex        =   16
      Top             =   4590
      WhatsThisHelpID =   53216
      Width           =   1455
   End
   Begin VB.CommandButton cmdSelectAll 
      Caption         =   "&Select All"
      Height          =   375
      Left            =   90
      TabIndex        =   15
      Top             =   4590
      WhatsThisHelpID =   53215
      Width           =   1455
   End
   Begin FPSpreadADO.fpSpread grdLines 
      Height          =   2895
      Left            =   90
      TabIndex        =   14
      Top             =   1620
      WhatsThisHelpID =   53214
      Width           =   8865
      _Version        =   524288
      _ExtentX        =   15637
      _ExtentY        =   5106
      _StockProps     =   64
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SpreadDesigner  =   "sozda102.frx":23D2
      AppearanceStyle =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   71
      Width           =   9060
      _ExtentX        =   15981
      _ExtentY        =   741
      Style           =   4
   End
   Begin SOTACalendarControl.SOTACalendar dteReqDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   4530
      TabIndex        =   8
      Top             =   600
      WhatsThisHelpID =   53212
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dtePromDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   4530
      TabIndex        =   10
      Top             =   930
      WhatsThisHelpID =   53211
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar dteShipDate 
      CausesValidation=   0   'False
      Height          =   315
      Left            =   4530
      TabIndex        =   12
      Top             =   1260
      WhatsThisHelpID =   53210
      Width           =   1755
      _ExtentX        =   3096
      _ExtentY        =   556
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Text            =   "  /  /    "
      Object.CausesValidation=   0   'False
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   28
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblNewSOAmt 
      AutoSize        =   -1  'True
      Caption         =   "New SO Amount"
      Height          =   195
      Left            =   210
      TabIndex        =   5
      Top             =   1320
      Width           =   1185
   End
   Begin VB.Label lblShipDate 
      AutoSize        =   -1  'True
      Caption         =   "S&hip Date"
      Height          =   195
      Left            =   3480
      TabIndex        =   11
      Top             =   1320
      Width           =   705
   End
   Begin VB.Label lblPromDate 
      AutoSize        =   -1  'True
      Caption         =   "Pro&mise Date"
      Height          =   195
      Left            =   3480
      TabIndex        =   9
      Top             =   990
      Width           =   945
   End
   Begin VB.Label lblReqdate 
      AutoSize        =   -1  'True
      Caption         =   "&Request Date"
      Height          =   195
      Left            =   3480
      TabIndex        =   7
      Top             =   660
      Width           =   990
   End
   Begin VB.Label lblSODate 
      AutoSize        =   -1  'True
      Caption         =   "SO &Date"
      Height          =   195
      Left            =   210
      TabIndex        =   3
      Top             =   990
      Width           =   615
   End
   Begin VB.Label lblNewSO 
      AutoSize        =   -1  'True
      Caption         =   "&New SO"
      Height          =   195
      Left            =   210
      TabIndex        =   1
      Top             =   660
      Width           =   600
   End
End
Attribute VB_Name = "frmSOGenBlanket"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

Public moCM             As New clsContextMenu
Private moErrorLog      As clsErrorLog

Public moOptions        As New clsOptions

Private moClass         As Object
Private moDtlGrid       As Object 'sales order detail grid
Private moSotaObjects   As Object

Public mbFromMe                  As Boolean
Public msCompanyID               As String
Public mlSOKey                   As Long
Public msUserID                  As String
Public mlLanguage                As Long
Public miNoToGen                 As Integer
Private mdAvailToGen             As Double
Private mbGened                  As Boolean
Private mbContract               As Boolean
Private miUseRelNos              As Integer
Private miQDP                    As Integer
Private miShipDays               As Integer
Private miUseSameRangeForBlanket As Integer
Private miUseSameRangeForQuote   As Integer
Private msCurrID                 As String

'AvaTax integration - General Declarations
Private mbTrackSTaxOnSales      As Boolean ' Used to check AR TrackSTaxOnSales option
Public moAvaTax                 As Object ' Avalara object of "AVZDADL1.clsAVAvaTaxClass", this is installed with the Avalara 3rd party add-on
Public mbAvaTaxEnabled          As Boolean ' Defines if AvaTax is installed on the DB & Client , and is turned on for this company
'AvaTax end

Const kColGenSet = 1
Const kColGenLine = 2
Const kColGenLineKey = 3
Const kColGenItem = 4
Const kColGenDescription = 5
Const kColGenQtyToBeOrd = 6
Const kColGenUnitPrice = 7
Const kColGenQtyOpen = 8
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Sub ShowMe(oClass As Object, oDtlGrid As Object, oErrorLog As clsErrorLog, oSotaObjects As Object, _
                  sSONum As String, icDP As Integer, iQDP As Integer, sCompID As String, _
                  lSOKey As Long, sUserID As String, lLanguage As Long, iShipDays As Integer, _
                  dAmtToGen As Double, iNoToGen As Integer, bUseSameRangeForBlanket As Boolean, _
                  bUseSameRangeForQuote As Boolean, sCurrID As String, _
                  bGened As Boolean)
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    
    
    bGened = False
    msCompanyID = sCompID
    mlSOKey = lSOKey
    msUserID = sUserID
    mlLanguage = lLanguage
    mdAvailToGen = dAmtToGen
    miNoToGen = iNoToGen
    miShipDays = iShipDays
    miUseSameRangeForBlanket = IIf(bUseSameRangeForBlanket, 1, 0)
    miUseSameRangeForQuote = IIf(bUseSameRangeForQuote, 1, 0)
    msCurrID = sCurrID
    
    Set moDtlGrid = oDtlGrid
    Set moClass = oClass
    Set moErrorLog = oErrorLog
    Set moSotaObjects = oSotaObjects
    
    mbContract = frmSOBlanketHdr.Contract
                
    If mbContract Then
        chkIssueSO.Value = vbChecked
        chkIssueSO.Enabled = True
    Else
        chkIssueSO.Value = vbUnchecked
        chkIssueSO.Enabled = False
    End If
    
    txtNewSO.Text = sSONum
    
    If Len(Trim(sSONum)) > 0 Then
        txtNewSO.Protected = True
        miUseRelNos = 1
        tbrMain.ButtonEnabled(kTbNextNumber) = False
    Else
        tbrMain.ButtonEnabled(kTbNextNumber) = True
        miUseRelNos = 0
        txtNewSO.Protected = False
    End If
    
    dteSODate.Text = moClass.moSysSession.BusinessDate
    dteReqDate.Text = moClass.moSysSession.BusinessDate
    
    CalcDates
    
    curNewSOAmt.Amount = 0
    curNewSOAmt.DecimalPlaces = icDP
    miQDP = iQDP
    
    LoadGrid
    
    Call UpdAllGrdUntPrce   'SGS DEJ 9/6/07
    
    If grdLines.MaxRows = 0 Then
        'There are no open rows.
        'SGS DEJ 9/11/07 IA also include that there are no awarded rows
'        giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoOpenRows
        MsgBox "There are no Open and Awarded rows.", vbExclamation, "MAS 500"
    Else
        Me.Show vbModal
    End If

    bGened = mbGened
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Sub                                                                              'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "ShowMe", VBRIG_IS_FORM                      'Repository Error Rig  {1.1.1.0.0}
        Call giErrorHandler: Exit Sub                                                     'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Sub
Private Function sGetNextSONo() As String
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    Dim sNextSONo       As String
    Dim iNumChars       As Integer
    Dim iRetVal         As Integer
    Dim sSQL            As String
      
On Error GoTo ExpectedErrorRoutine2
    
    
    
    If iNumChars = 0 Or iNumChars > 10 Then
        iNumChars = 10
    End If
    
    iRetVal = 0
    
    On Error GoTo ExpectedErrorRoutine
    With moClass.moAppDB
        .SetInParam msCompanyID
        .SetInParam iNumChars
        .SetInParam CInt(kTranTypeSOSO)
        .SetInParam miUseSameRangeForBlanket
        .SetInParam miUseSameRangeForQuote
        .SetOutParam sNextSONo
        .SetOutParam iRetVal
        .ExecuteSP ("spsoGetNextSONo")
        sNextSONo = .GetOutParam(6)
        iRetVal = .GetOutParam(7)
        .ReleaseParams
    End With
    On Error GoTo ExpectedErrorRoutine2
    
    If iRetVal = 0 Then
        sNextSONo = ""
        giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
            "spsoGetNextSONo: " & "0"
        Exit Function
    End If
    
    If iRetVal = -1 Then
        sNextSONo = ""
        'All Sales Order Numbers Used
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgAllSONumsUsed
        Exit Function
    End If
    
    sSQL = "INSERT INTO tsoSalesOrdLog " & _
           "(CompanyID,          SOKey,          TranAmtHC, " & _
            "TranDate,           TranNo,         TranStatus, " & _
            "TranType,           TranNoRel)" & _
     "VALUES   (" & gsQuoted(msCompanyID) & "," & _
                 0 & ",       0.00, GETDATE(), " & _
                 gsQuoted(sNextSONo) & ",       1, " & _
                 kTranTypeSOSO & ", " & _
                 gsQuoted(sNextSONo) & ")"

    moClass.moAppDB.ExecuteSQL sSQL
    
    sGetNextSONo = sNextSONo
    
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function

ExpectedErrorRoutine:
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
    "spsoGetNextSONo: " & Err.Description
gClearSotaErr

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

ExpectedErrorRoutine2:
MyErrMsg moClass, Err.Description, Err, sMyName, "sGetNextSONo"
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetNextSONo", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub cmdClearAll_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdClearAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long
    
    For lRow = 1 To grdLines.MaxRows
        gGridUpdateCell grdLines, lRow, kColGenSet, "0"
    Next lRow

End Sub

Private Sub cmdSelectAll_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdSelectAll, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++
    Dim lRow As Long
    
    For lRow = 1 To grdLines.MaxRows
        gGridUpdateCell grdLines, lRow, kColGenSet, "1"
    Next lRow
End Sub

Private Sub dtePromDate_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dtePromDate, True
    #End If
'+++ End Customizer Code Push +++
    CalcDate
End Sub

Private Sub dteReqDate_LostFocus()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteReqDate, True
    #End If
'+++ End Customizer Code Push +++
    CalcDates
End Sub
Private Sub CalcDates()
Dim bCalcShip   As Boolean

    If dteReqDate.Tag <> dteReqDate.Text Then
        
        If Not dteReqDate.IsValid Then
            Exit Sub
        End If
        
        dteReqDate.Tag = dteReqDate.Text
        
      'Promise date
        If Len(Trim(dtePromDate.Text)) = 0 Then
            dtePromDate.Text = dteReqDate.Text
            dtePromDate.Tag = dtePromDate.Text
            
            If Len(Trim(dtePromDate.Text)) > 0 Then
                If Len(Trim(dteShipDate.Text)) = 0 Then
                    bCalcShip = True
                Else
                    'Do you wish to recalculate the ship date?
                    If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgRecalcShipDate) = kretYes Then
                        bCalcShip = True
                    End If
                End If
            End If
        
        End If
        
      'Ship Date
        If bCalcShip Then
            dteShipDate.Text = DateAdd("d", miShipDays, dteReqDate.Text)
            dteShipDate.Tag = dteShipDate.Text
        End If
        
    End If

End Sub
Private Sub CalcDate()
Dim bCalcShip   As Boolean

    If dtePromDate.Tag <> dtePromDate.Text Then
        
        If Not dtePromDate.IsValid Then
            Exit Sub
        End If
        
        dtePromDate.Tag = dtePromDate.Text
        
      'Promise date
        If Len(Trim(dtePromDate.Text)) > 0 Then
            If Len(Trim(dteShipDate.Text)) = 0 Then
                bCalcShip = True
            Else
                'Do you wish to recalculate the ship date?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgRecalcShipDate) = kretYes Then
                    bCalcShip = True
                End If
            End If
        End If
        
      'Ship Date
        If bCalcShip Then
            dteShipDate.Text = DateAdd("d", miShipDays, dtePromDate.Text)
            dteShipDate.Tag = dteShipDate.Text
        End If
        
    End If

End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
#If CUSTOMIZER Then
    If (Shift = 4) Then
        gProcessFKeys Me, keycode, Shift
    End If
#End If

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
    End Select

End Sub

Private Sub Form_Load()

    Set moCM.Form = frmSOGenBlanket
     moCM.Init

    tbrMain.AddButton kTbNextNumber, tbrMain.GetIndex(kTbHelp)
    tbrMain.AddSeparator tbrMain.GetIndex(kTbHelp)
    SetupGrid

    'AvaTax Integration - FormLoad
    mbTrackSTaxOnSales = gbGetValidBoolean(moClass.moAppDB.Lookup("TrackSTaxonSales", "tarOptions", "CompanyID = " & gsQuoted(msCompanyID)))
    If mbTrackSTaxOnSales Then
        CheckAvaTax
    Else
        mbAvaTaxEnabled = False
    End If
    ' AvaTax Integration end

End Sub

Private Sub LoadGrid()
    Dim iQDP        As Integer
    Dim lRow        As Long
    Dim lRowMe      As Long
    Dim iStatus     As String
    Dim bAwarded As Boolean 'SGS DEJ 9/11/07 AI
    
    iQDP = miQDP
    
    grdLines.MaxRows = 0

    For lRow = 1 To moDtlGrid.DataRowCnt
        iStatus = Val(gsGridReadCell(moDtlGrid, lRow, mlColSOStatus))
        
        bAwarded = IsAwarded(gsGridReadCell(moDtlGrid, lRow, mlColSOSOLineKey)) 'SGS DEJ 9/11/07 AI
        
        If iStatus = 1 And bAwarded = True Then     'SGS DEJ 9/11/07 AI
'        If iStatus = 1 Then                        'SGS DEJ 9/11/07 AI (Orig Code commented out)
            lRowMe = lRowMe + 1
            grdLines.MaxRows = lRowMe
            gGridSetColumnType grdLines, kColGenSet, SS_CELL_TYPE_CHECKBOX
            gGridUpdateCell grdLines, lRowMe, kColGenLine, gsGridReadCell(moDtlGrid, lRow, mlColSOSOLineNo)
            gGridUpdateCell grdLines, lRowMe, kColGenLineKey, gsGridReadCell(moDtlGrid, lRow, mlColSOSOLineKey)
            gGridUpdateCell grdLines, lRowMe, kColGenItem, gsGridReadCell(moDtlGrid, lRow, mlColSOItemID)
            gGridUpdateCell grdLines, lRowMe, kColGenDescription, gsGridReadCell(moDtlGrid, lRow, mlColSODescription)
            gGridSetColumnType grdLines, kColGenQtyToBeOrd, SS_CELL_TYPE_FLOAT, iQDP, 9
            gGridUpdateCell grdLines, lRowMe, kColGenQtyToBeOrd, "0"
            gGridUpdateCell grdLines, lRowMe, kColGenUnitPrice, gsGridReadCell(moDtlGrid, lRow, mlColSOUnitPrice)
            gGridSetColumnType grdLines, kColGenQtyOpen, SS_CELL_TYPE_FLOAT, iQDP, 9
            gGridUpdateCell grdLines, lRowMe, kColGenQtyOpen, gsGridReadCell(moDtlGrid, lRow, mlColSOQtyOpen)
        End If
    Next lRow
End Sub

Private Sub SetupGrid()
    gGridSetProperties grdLines, kColGenQtyOpen, kGridDataSheet
    gGridSetMaxRows grdLines, 0
    grdLines.TypeFloatSeparator = True
    grdLines.DisplayRowHeaders = False
    
    gGridSetHeader grdLines, kColGenSet, "Sel"
    gGridSetHeader grdLines, kColGenLine, "Line"
    gGridSetHeader grdLines, kColGenItem, "Item"
    gGridSetHeader grdLines, kColGenDescription, "Description"
    gGridSetHeader grdLines, kColGenQtyToBeOrd, "Qty To Be Ordered"
    gGridSetHeader grdLines, kColGenQtyOpen, "Qty Open"

    gGridSetColumnWidth grdLines, kColGenSet, 3
    gGridSetColumnWidth grdLines, kColGenLine, 4
    gGridSetColumnWidth grdLines, kColGenItem, 15
    gGridSetColumnWidth grdLines, kColGenDescription, 25
    gGridSetColumnWidth grdLines, kColGenQtyToBeOrd, 12
    gGridSetColumnWidth grdLines, kColGenQtyOpen, 12
    
    gGridLockColumn grdLines, kColGenLine
    gGridHideColumn grdLines, kColGenLineKey
    gGridLockColumn grdLines, kColGenItem
    gGridLockColumn grdLines, kColGenDescription
    gGridLockColumn grdLines, kColGenQtyOpen
    gGridHideColumn grdLines, kColGenUnitPrice

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    On Error Resume Next
    moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
    moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
    Set moCM = Nothing
    Set moClass = Nothing
    Set moSotaObjects = Nothing
    Set moErrorLog = Nothing
    Set moOptions = Nothing
    ' AvaTax Integration - Unload
    If Not moAvaTax Is Nothing Then
        Set moAvaTax = Nothing
    End If
    ' AvaTax end
End Sub

Private Sub grdLines_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    Dim iVal As Integer
    Dim dAmt As Double
    Dim lRow As Long
    
    iVal = Val(gsGridReadCell(grdLines, Row, kColGenSet))
    
    If Not mbFromMe Then
        If iVal = 1 Then
            gGridUpdateCell grdLines, Row, kColGenQtyToBeOrd, gsGridReadCell(grdLines, Row, kColGenQtyOpen)
        Else
            gGridUpdateCell grdLines, Row, kColGenQtyToBeOrd, "0"
        End If
    
        UpdGrdUntPrce Row   'SGS DEJ 9/6/07
        
        For lRow = 1 To grdLines.MaxRows
            dAmt = dAmt + (Val(gsGridReadCell(grdLines, lRow, kColGenQtyToBeOrd)) * _
                            Val(gsGridReadCell(grdLines, lRow, kColGenUnitPrice)))
        Next lRow
        
        curNewSOAmt = dAmt
    End If

End Sub

Private Sub grdLines_Change(ByVal Col As Long, ByVal Row As Long)
    Dim dQty As Double
    Dim dAmt As Double
    Dim lRow As Long
    
    If Col = kColGenQtyToBeOrd Then
        dQty = Val(gsGridReadCell(grdLines, Row, kColGenQtyToBeOrd))
        mbFromMe = True
        If dQty = 0 Then
            gGridUpdateCell grdLines, Row, kColGenSet, "0"
        Else
            gGridUpdateCell grdLines, Row, kColGenSet, "1"
        End If
        mbFromMe = False
        
        UpdGrdUntPrce Row   'SGS DEJ 9/6/07
        
        For lRow = 1 To grdLines.MaxRows
            dAmt = dAmt + (Val(gsGridReadCell(grdLines, lRow, kColGenQtyToBeOrd)) * _
                            Val(gsGridReadCell(grdLines, lRow, kColGenUnitPrice)))
        Next lRow
        
        curNewSOAmt = dAmt
    
    End If
    
End Sub
Private Sub grdLines_GotFocus()
    grdLines.ProcessTab = True
End Sub

Private Sub grdLines_KeyDown(keycode As Integer, Shift As Integer)
    
    If keycode = vbKeyTab Then
        If Shift = 0 Then
            If grdLines.ActiveCol = kColGenQtyOpen Then
                If grdLines.ActiveRow = grdLines.MaxRows Then
                    grdLines.ProcessTab = False
                End If
            End If
        End If
        If Shift = 1 Then
            If grdLines.ActiveCol = kColGenSet Then
                If grdLines.ActiveRow = grdLines.MaxRows Then
                    grdLines.ProcessTab = False
                End If
            End If
        End If
    End If
    
    
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(Button) Then
            Exit Sub
        End If
    End If
#End If
    Select Case Button
        Case kTbProceed
            SetHourglass True
            CreateFromBlanket
            SetHourglass False
        Case kTbNextNumber
            txtNewSO.Text = sGetNextSONo
        Case kTbClose
            Unload Me
        Case kTbHelp
            gDisplayFormLevelHelp Me
       End Select
End Sub
Private Function bIsFormValid() As Boolean
Dim sDate As String

    If Not bValidGrid() Then
        Exit Function
    End If

    If Len(Trim(dteSODate.Text)) = 0 Or Not dteSODate.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblSODate, "&")
        On Error Resume Next
        dteSODate.SetFocus
        Exit Function
    End If

    If mbContract Then
        sDate = frmSOBlanketHdr.StartDate
        If Len(Trim(sDate)) <> 0 Then
            If sDate > dteSODate.SQLDate Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                    frmSOBlanketHdr.StartDateText, gsStripChar(lblSODate, "&")
                On Error Resume Next
                dteSODate.SetFocus
                Exit Function
            End If
        End If
        sDate = frmSOBlanketHdr.StopDate
        If Len(Trim(sDate)) <> 0 Then
            If sDate < dteSODate.SQLDate Then
                giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                     gsStripChar(lblSODate, "&"), frmSOBlanketHdr.StopDateText
                On Error Resume Next
                dteSODate.SetFocus
                Exit Function
            End If
        End If
    End If

    CalcDates
    CalcDate
    
    If Len(Trim(dteReqDate.Text)) = 0 Or Not dteReqDate.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblReqdate, "&")
        On Error Resume Next
        dteReqDate.SetFocus
        Exit Function
    End If

    If Len(Trim(dtePromDate.Text)) = 0 Or Not dtePromDate.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblPromDate, "&")
        On Error Resume Next
        dtePromDate.SetFocus
        Exit Function
    End If

    If Len(Trim(dteShipDate.Text)) = 0 Or Not dteShipDate.IsValid Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, gsStripChar(lblShipDate, "&")
        On Error Resume Next
        dteShipDate.SetFocus
        Exit Function
    End If

    If frmSOBlanketHdr.MaxAmountToGen <> 0 Then
        If gfRound(curNewSOAmt.Amount, 1, curNewSOAmt.DecimalPlaces, 1) > _
                   gfRound(mdAvailToGen, 1, curNewSOAmt.DecimalPlaces, 1) Then
            'You must reset the maximum amount in order to generate this order
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgResetMaxAmt
            Exit Function
        End If
    End If
    
    If frmSOBlanketHdr.MaxSOToGen <> 0 Then
        If 0 >= miNoToGen Then
            'You must reset the maximum number of orders allowed in order to generate this order", vbExclamation, "Sage 500 ERP"
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgResetMaxNumber
            Exit Function
        End If
    End If

    bIsFormValid = True
    
End Function
Private Function bValidGrid() As Boolean
    Dim lRow            As Long
    Dim lNegQtyRow      As Long
    Dim lBadContRow     As Long
    Dim dQty            As Double
    Dim dQtyOpen        As Double
    Dim dAmt            As Double
    Dim sItemID         As String
    Dim iItemType       As Integer
        
    For lRow = 1 To grdLines.MaxRows
        dQty = Val(gsGridReadCell(grdLines, lRow, kColGenQtyToBeOrd))
        sItemID = gsGridReadCell(grdLines, lRow, kColGenItem)
        
        mbFromMe = True
        If dQty = 0 Then
            gGridUpdateCell grdLines, lRow, kColGenSet, "0"
        Else
            gGridUpdateCell grdLines, lRow, kColGenSet, "1"
        End If
        mbFromMe = False
        
        If dQty < 0 Then
        
            If Len(Trim(sItemID)) > 0 Then
                iItemType = moClass.moAppDB.Lookup("ItemType", "timItem", "ItemID = " & gsQuoted(Trim(sItemID)) & "AND CompanyID = " & gsQuoted(msCompanyID))
                
                If (iItemType <> IMS_MISC_ITEM) And (iItemType <> IMS_SERVICE) Then
                    lNegQtyRow = lRow
                End If
            End If
        End If
        
        dQtyOpen = Val(gsGridReadCell(grdLines, lRow, kColGenQtyOpen))
        If Abs(dQtyOpen) < Abs(dQty) Then
            lBadContRow = lRow
        End If
        
        dAmt = dAmt + (dQty * Val(gsGridReadCell(grdLines, lRow, kColGenUnitPrice)))
    
    Next lRow

    curNewSOAmt = dAmt
    
    If lNegQtyRow > 0 Then
        'Quantity may not be negative.
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgNegQty
        grdLines.Col = kColGenQtyToBeOrd
        grdLines.Row = lNegQtyRow
        grdLines.Action = SS_ACTION_ACTIVE_CELL
    Else
        If lBadContRow > 0 Then
            If mbContract Then
                'You may not create an order for more than what is currently open.
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgNoOrder
            Else
                'You may not create an order for more than what is currently open.  Increase the quantity ordered for the item.", vbExclamation, "Sage 500 ERP"
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgIncreaseQty
            End If
            grdLines.Col = kColGenQtyToBeOrd
            grdLines.Row = lBadContRow
            grdLines.Action = SS_ACTION_ACTIVE_CELL
        Else
            bValidGrid = True
        End If
    End If

End Function
Private Sub CreateFromBlanket()

    Dim sSQL            As String
    Dim lRow            As Long
    Dim iVal            As Integer
    Dim dQty            As Double
    Dim lSOLineKey      As Double
    Dim iQtyToGen       As Integer
    Dim bViewRpt        As Boolean
    Dim iRetVal         As Integer
    Dim lErr            As Long
    Dim sErrDesc        As String
    Dim lSOKey          As Long
    Dim oPrintSO        As Object

    '*****************************************************************************
    '*****************************************************************************
    'SGS DeJ 3/14/08 Obtain BOL from User   (START)
    '*****************************************************************************
    '*****************************************************************************
    Dim sBOLNo          As String   'SGS DEJ 3/14/08 add BOL at time of creation of new SO
    Dim SQL             As String   'SGS DEJ 3/14/08
    
    sBOLNo = InputBox("Enter the BOL No for the new Sales Order.", "Enter BOL No")
    
    If Trim(sBOLNo) = Empty Then
        If MsgBox("Continue without a BOL No?", vbYesNo, "MAS 500") <> vbYes Then
            Exit Sub
        End If
    Else
        Select Case MsgBox("Is '" & sBOLNo & "' the correct BOL?", vbYesNoCancel, "Confirm BOL No")
            Case vbYes
                'Do Nothing
            Case vbNo
                MsgBox "Create SO Canceled.", vbExclamation, "MAS 500"
                Exit Sub
            Case vbCancel
                MsgBox "Create SO Canceled.", vbExclamation, "MAS 500"
                Exit Sub
            Case Else
                MsgBox "Create SO Canceled.", vbExclamation, "MAS 500"
                Exit Sub
        End Select
    End If
    '*****************************************************************************
    '*****************************************************************************
    'SGS DeJ 3/14/08 Obtain BOL from User   (STOP)
    '*****************************************************************************
    '*****************************************************************************

    mbGened = False

    If bIsFormValid Then
        sSQL = "CREATE TABLE #tsoLines " & _
               "(SOLineKey int NOT NULL, " & _
               " ItemKey Int NULL, " & _
               " Qty dec(16,8) NOT NULL) "

        On Error Resume Next
        moClass.moAppDB.ExecuteSQL sSQL
        
        
        On Error GoTo ExpectedErrorRoutine
        moClass.moAppDB.ExecuteSQL "DELETE FROM #tsoLines "
        moClass.moAppDB.ExecuteSQL "DELETE FROM tciErrorLog WHERE SessionID = " & mlSpid
        
        For lRow = 1 To grdLines.MaxRows
            iVal = Val(gsGridReadCell(grdLines, lRow, kColGenSet))
                
            If iVal = 1 Then
                lSOLineKey = Val(gsGridReadCell(grdLines, lRow, kColGenLineKey))
                dQty = Val(gsGridReadCell(grdLines, lRow, kColGenQtyToBeOrd))
                sSQL = "INSERT INTO #tsoLines VALUES (" & lSOLineKey & ", NULL, " & dQty & ")"
                moClass.moAppDB.ExecuteSQL sSQL
                iQtyToGen = iQtyToGen + 1
            End If
            
        Next lRow
        
        If iQtyToGen = 0 Then
            'No lines were selected
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgIncreaseQty
            Exit Sub
        End If
        
        On Error GoTo ExpectedErrorRoutine2
        With moClass.moAppDB
            .SetInParam msCompanyID
            .SetInParam mlSOKey
            .SetInParam CInt(kTranTypeSOSO)
            .SetInParam dteSODate.SQLDate
            .SetInParam dteReqDate.SQLDate
            .SetInParam dtePromDate.SQLDate
            .SetInParam dteShipDate.SQLDate
            If Len(Trim(txtNewSO.Text)) = 0 Then
                .SetInParamNull SQL_CHAR
            Else
                .SetInParam CStr(txtNewSO.Text)
            End If
            .SetInParam msUserID
            .SetInParam miUseRelNos
            .SetInParam mlLanguage
            .SetInParam CInt(0)
            .SetOutParam iRetVal
            .SetOutParam mlSpid
            .SetOutParam lSOKey
            .ExecuteSP "spsoCreateASalesOrder"
            iRetVal = giGetValidInt(.GetOutParam(13))
            mlSpid = glGetValidLong(.GetOutParam(14))
            lSOKey = glGetValidLong(.GetOutParam(15))
            .ReleaseParams
        End With
    
        On Error GoTo ExpectedErrorRoutine
        Select Case iRetVal
            Case 0
                'Unexpected errors occurred.
                giSotaMsgBox Me, moClass.moSysSession, kSOMsgUnexpectedError
                Exit Sub
            Case 1
                'Sales order created successfully
                giSotaMsgBox Me, moClass.moSysSession, kSOmsgSOCreated
                mbGened = True
                ' AvaTax Integration - CreateFromBlanket
                If mbAvaTaxEnabled Then
                    moAvaTax.bCalculateTax lSOKey, kModuleSO
                End If
                ' AvaTax Integration end
            
                'SGS DeJ 3/14/08 Obtain BOL from User (START)
                If Trim(sBOLNo) <> Empty Then
                    SQL = "Update tsoSalesOrderExt_SGS Set BOLNo = '" & Replace(sBOLNo, "'", "''") & "' Where SOKey = " & lSOKey
                    moClass.moAppDB.ExecuteSQL SQL
                End If
                'SGS DeJ 3/14/08 Obtain BOL from User (STOP)
            
            Case 2
                'Sales order created - warnings were raised. Do you wish to view the Error Log?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOMsgSoCreatedWarn) = kretYes Then
                    bViewRpt = True
                End If
                mbGened = True
            
                'SGS DeJ 3/14/08 Obtain BOL from User (START)
                If Trim(sBOLNo) <> Empty Then
                    SQL = "Update tsoSalesOrderExt_SGS Set BOLNo = '" & Replace(sBOLNo, "'", "''") & "' Where SOKey = " & lSOKey
                    moClass.moAppDB.ExecuteSQL SQL
                End If
                'SGS DeJ 3/14/08 Obtain BOL from User (STOP)
            
            Case 3, 4
                'Errors were raised.  Do you wish to view the Error Log?
                If giSotaMsgBox(Me, moClass.moSysSession, kSOmsgErrorsOccurred) = kretYes Then
                    bViewRpt = True
                End If
        End Select
            
        If bViewRpt Then
            moErrorLog.ProcessSpidErrorLog Me, 0, mlSpid
            moErrorLog.PrintErrorLog kTbPreview, Me
        End If
        
        If mbGened Then
            
            If miUseRelNos = 0 Then
                txtNewSO.Text = ""
            End If
            
            If mbContract Then
                Set oPrintSO = goGetSOTAChild(moClass.moFramework, moSotaObjects, _
                    "soztcdl1.clsAckPrintSOs", 134611699, kAOFRunFlags, kContextAOF)
                If Not (oPrintSO Is Nothing) Then
                    oPrintSO.QuickAckPrintSO _
                        gsGetValidStr(moClass.moAppDB.Lookup("TranNoRelChngOrd", _
                            "tsoSalesOrder", "SOKey = " & lSOKey)), _
                        CLng(kTranTypeSOSO), 0, lSOKey, chkIssueSO.Value
                    'Scopus#18749-Print the message box here instead of in soztcdl1 to prevent it from hiding behind the form
                    giSotaMsgBox Me, moClass.moSysSession, kSOmsgAckSOSuccessful, 1
                End If
            End If
            Unload Me
        End If
        
    End If

Exit Sub

ExpectedErrorRoutine2:
lErr = Err
sErrDesc = Err.Description
SetHourglass False
giSotaMsgBox Me, moClass.moSysSession, kmsgUnexpectedSPReturnValue, _
     "spsoCreateASalesOrder: " & sErrDesc
gClearSotaErr
Exit Sub

'Not a Stored Procedure error
ExpectedErrorRoutine:
lErr = Err
sErrDesc = Err.Description
SetHourglass False
MyErrMsg moClass, sErrDesc, lErr, sMyName, "CreateFromBlanket"
gClearSotaErr
Exit Sub


End Sub
Private Property Get sMyName() As String
    sMyName = "frmSOGenBlanket"
End Property

#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If


Private Sub cmdClearAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdClearAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdClearAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdClearAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectAll_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelectAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectAll_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelectAll_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelectAll, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelectAll_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtNewSO_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtNewSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNewSO_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNewSO_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtNewSO, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNewSO_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNewSO_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtNewSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNewSO_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtNewSO_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtNewSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtNewSO_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curNewSOAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curNewSOAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curNewSOAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curNewSOAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curNewSOAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curNewSOAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curNewSOAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curNewSOAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curNewSOAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curNewSOAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curNewSOAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curNewSOAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIssueSO_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkIssueSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIssueSO_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIssueSO_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkIssueSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIssueSO_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkIssueSO_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkIssueSO, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkIssueSO_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteSODate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteSODate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteSODate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteSODate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteSODate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteSODate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteSODate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteSODate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteSODate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteSODate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteSODate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteSODate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteReqDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteReqDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteReqDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteReqDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteReqDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteReqDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteReqDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteReqDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteReqDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dtePromDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dtePromDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dtePromDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dtePromDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dtePromDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dtePromDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dtePromDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dtePromDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dtePromDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteShipDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteShipDate, True
    #End If
'+++ End Customizer Code Push +++

'======================================================================================
'======================================================================================
'SGS DEJ Changes Start
'======================================================================================
'======================================================================================
    'SGS DEJ 9/6/07
    Call UpdAllGrdUntPrce

    Dim dAmt As Double
    Dim lRow As Long
    
    If Not mbFromMe Then
        For lRow = 1 To grdLines.MaxRows
            dAmt = dAmt + (Val(gsGridReadCell(grdLines, lRow, kColGenQtyToBeOrd)) * _
                            Val(gsGridReadCell(grdLines, lRow, kColGenUnitPrice)))
        Next lRow
        
        curNewSOAmt = dAmt
    End If
'======================================================================================
'======================================================================================
'SGS DEJ Changes End
'======================================================================================
'======================================================================================

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteShipDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteShipDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteShipDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteShipDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteShipDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteShipDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteShipDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteShipDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteShipDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteShipDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CheckAvaTax()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
'***********************************************************************
' Avatax Integration: Check the availability of Avatax for the CompanyID.
'***********************************************************************

On Error GoTo ExpectedErrorRoutine

    'Assume AvaTax feature not configured
    mbAvaTaxEnabled = False

    mbAvaTaxEnabled = gbGetValidBoolean(moClass.moAppDB.Lookup("AvaTaxEnabled", "tavConfiguration", "CompanyID = " & gsQuoted(msCompanyID)))

    If mbAvaTaxEnabled Then
        On Error GoTo ExpectedErrorRoutine_AvaTaxObj
        Set moAvaTax = Nothing
        Set moAvaTax = CreateObject("AVZDADL1.clsAVAvaTaxClass")
        moAvaTax.Init moClass, moClass.moSysSession, moClass.moAppDB, moClass.moAppDB, moClass.moFramework
    End If

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine:
'Expected Error:
'This is how we find out if AvaTax is not installed.
'The missing table (err 4050) will tell us not to fire the AvaTax code
'throughout the program.

    Select Case Err.Number
    
        Case 4050 ' SQL Object Not Found
            'tavConfiguration not found meaning AvaTax not installed.
            'No Error action required.

        Case Else
            'Unexpected Error reading tavConfiguration
            giSotaMsgBox Me, moClass.moSysSession, 100433 ' kMsgtavConfigurationError = 100433

    End Select

'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub

ExpectedErrorRoutine_AvaTaxObj:
'Expected Error:
'If the DB Avatax table was found, AvaTax is likely used for this Site.
'But when creating the local client object, the object was likely not installed
'This is not a good error, let the administrator know to check that AvaTax was properly implemented.

    giSotaMsgBox Me, moClass.moSysSession, 100434 ' kMsgAvaObjectError = 100434

    mbAvaTaxEnabled = False

    ' Continue into standard VBRig

'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CheckAvaTax", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If


Public Property Get oClass() As Object
    Set oClass = goClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property


'*********************************************************************************************************************
'*********************************************************************************************************************
'SGS DEJ 9/6/07 (START) of Several Methods
'*********************************************************************************************************************
'*********************************************************************************************************************
'SGS DEJ 9/6/07
Function GetUnitPrice(lLnKy As Long, dShpDt As Date, dQty As Double, ByRef dPrice As Double) As Boolean
    Dim iRetVal As Long
    Const iSuccess = 0
    Const iNoLine = 1
    Const iNoBreak = 2
    Const iSysErr = 3
    Const iNoDate = 4
    Const iNoQty = 5
    Const iNotBrkDt = 6
    Const iNotSuffQty = 7
    

    With moClass.moAppDB
        .SetInParam lLnKy
        .SetInParam CStr(dShpDt)
        .SetInParam dQty
        
        .SetOutParam 0#
        .SetOutParam iRetVal
        
        .ExecuteSP ("spsoGetLinePriceByDate_SGS")
        
        dPrice = gdGetValidDbl(.GetOutParam(4))
        iRetVal = glGetValidLong(.GetOutParam(5))
        
        .ReleaseParams
    End With
    
'Return Values @RtnVlu:
'    0 = Success
'    1 = Failure - The SO Line does not exist
'    2 = Failure - There is not a price Break for this line
'    3 = System Error
'    4 = Date Not Supplied
'    5 = QTY Not Supplied
'    6 = Date for matrix not found
'    7 = Not sufficient QTY
    
    Select Case iRetVal
        Case iSuccess
            GetUnitPrice = True
        Case iNoLine
            MsgBox "The Sales Order Line was not found.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
        Case iNoBreak
            MsgBox "There is not a Sales Order Line Price Break.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
        Case iSysErr
            MsgBox "A database error occurred while looking up the Unit Price.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
        Case iNoDate
            MsgBox "The Ship Date was not supplied.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
        Case iNoQty
'            MsgBox "The Ship Qty was not supplied.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
        Case iNotBrkDt
            MsgBox "The date '" & CDate(dShpDt) & "' was not found in the price breaks.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
        Case iNotSuffQty
            MsgBox "There is not sufficient Price Break Qty.  Cannot find the unit price.", vbExclamation, "MAS 500"
            GetUnitPrice = False
    End Select
    
End Function

'SGS DEJ 9/11/07
Function IsAwarded(SOLineKey As Long) As Boolean
    Dim cnt As Long
    
    cnt = glGetValidLong(moClass.moAppDB.LookupSys("Count(*)", "tsoSOLineExt_SGS", "SOLineKey = " & SOLineKey & " And abs(Awarded) = 1"))
    
    If cnt >= 1 Then IsAwarded = True
    
End Function

'SGS DEJ 9/6/07
Sub UpdAllGrdUntPrce()
    Dim lRow        As Long
    
    If IsDate(dteShipDate.Value) = False Then Exit Sub
    
    For lRow = 1 To grdLines.DataRowCnt
        UpdGrdUntPrce lRow
    Next lRow
    
End Sub


'SGS DEJ 9/6/07
Sub UpdGrdUntPrce(lRow As Long)
    
    Dim dUnitPrice As Double
    Dim dQty As Double
    Dim lSOLineKey As Long
    Dim sItem As String
    Dim sItemDesc As String
    
    If IsDate(dteShipDate.Value) = False Then Exit Sub
    
    Debug.Print Val(gsGridReadCell(grdLines, lRow, kColGenSet))
    If Val(gsGridReadCell(grdLines, lRow, kColGenSet)) = 0 Then
        Exit Sub
    End If
        
    lSOLineKey = Val(gsGridReadCell(grdLines, lRow, kColGenLineKey))
    dQty = Val(gsGridReadCell(grdLines, lRow, kColGenQtyToBeOrd))
    sItem = gsGridReadCell(grdLines, lRow, kColGenItem)
    sItemDesc = gsGridReadCell(grdLines, lRow, kColGenDescription)

    
    If GetUnitPrice(lSOLineKey, dteShipDate.Value, dQty, dUnitPrice) = False Then
        '??? What to do... an error occurred
        gGridUpdateCell grdLines, lRow, kColGenUnitPrice, CStr(dUnitPrice)
    Else
        gGridUpdateCell grdLines, lRow, kColGenUnitPrice, CStr(dUnitPrice)
    End If
    
End Sub
'SGS DEJ 9/6/07

'*********************************************************************************************************************
'*********************************************************************************************************************
'SGS DEJ 9/6/07 (STOP) of Several Methods
'*********************************************************************************************************************
'*********************************************************************************************************************






