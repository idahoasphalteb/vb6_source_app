VERSION 5.00
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Begin VB.Form frmSOBlanketHdr 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Set Up Blanket"
   ClientHeight    =   4080
   ClientLeft      =   180
   ClientTop       =   405
   ClientWidth     =   9075
   HelpContextID   =   53174
   Icon            =   "sozda101.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4080
   ScaleWidth      =   9075
   ShowInTaskbar   =   0   'False
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   36
      Top             =   570
      WhatsThisHelpID =   75
      Width           =   1785
      _ExtentX        =   3149
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   20
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   21
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   26
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   71
      Width           =   9075
      _ExtentX        =   16007
      _ExtentY        =   741
      Style           =   8
   End
   Begin VB.Frame fraLastPO 
      Caption         =   "Last SO"
      Height          =   2175
      Left            =   5610
      TabIndex        =   28
      Top             =   1800
      Width           =   3345
      Begin SOTACalendarControl.SOTACalendar dteLastSODate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1275
         TabIndex        =   32
         Top             =   600
         WhatsThisHelpID =   53200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         BackColor       =   -2147483633
         DisplayButton   =   0   'False
         DisplayOnly     =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Protected       =   -1  'True
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin NEWSOTALib.SOTACurrency curLastSOAmt 
         Height          =   285
         Left            =   1275
         TabIndex        =   34
         Top             =   960
         WhatsThisHelpID =   53199
         Width           =   1995
         _Version        =   65536
         bShowCurrency   =   0   'False
         _ExtentX        =   3519
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtLastSONo 
         Height          =   285
         Left            =   1275
         TabIndex        =   30
         Top             =   240
         WhatsThisHelpID =   53198
         Width           =   1965
         _Version        =   65536
         _ExtentX        =   3466
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         lMaxLength      =   20
      End
      Begin VB.Label lblLastSOAmt 
         AutoSize        =   -1  'True
         Caption         =   "Amount"
         Height          =   195
         Left            =   120
         TabIndex        =   33
         Top             =   1005
         Width           =   540
      End
      Begin VB.Label lblLastSONo 
         AutoSize        =   -1  'True
         Caption         =   "SO Number"
         Height          =   195
         Left            =   120
         TabIndex        =   29
         Top             =   285
         Width           =   825
      End
      Begin VB.Label lbLastlSODate 
         AutoSize        =   -1  'True
         Caption         =   "Create Date"
         Height          =   195
         Left            =   120
         TabIndex        =   31
         Top             =   660
         Width           =   855
      End
   End
   Begin VB.Frame fraContractDate 
      Caption         =   "Contract"
      Height          =   2175
      Left            =   90
      TabIndex        =   7
      Top             =   1800
      Width           =   5385
      Begin VB.CommandButton cmdReleaseToMcLeod 
         Caption         =   "&McLeod"
         Height          =   375
         Left            =   3390
         TabIndex        =   42
         Top             =   1320
         Width           =   825
      End
      Begin VB.CheckBox chkSentToMcLeod 
         Caption         =   "Released"
         Enabled         =   0   'False
         Height          =   255
         Left            =   4260
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   1380
         WhatsThisHelpID =   53193
         Width           =   1005
      End
      Begin VB.CheckBox ChScalePassHold 
         Caption         =   "LME Planning Only"
         Height          =   255
         Left            =   3390
         TabIndex        =   40
         ToolTipText     =   "Send to LME but hold from going to Scale Pass."
         Top             =   1020
         Width           =   1695
      End
      Begin VB.CheckBox chkFixedRate 
         Caption         =   "Use Fixed Rate"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3360
         TabIndex        =   9
         Top             =   240
         WhatsThisHelpID =   53193
         Width           =   1635
      End
      Begin SOTACalendarControl.SOTACalendar dteBegDate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1395
         TabIndex        =   11
         Top             =   570
         WhatsThisHelpID =   53192
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         BackColor       =   -2147483633
         DisplayOnly     =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Protected       =   -1  'True
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin VB.CheckBox chkContract 
         Alignment       =   1  'Right Justify
         Caption         =   "&Contract"
         Height          =   285
         Left            =   90
         TabIndex        =   8
         Top             =   240
         WhatsThisHelpID =   53191
         Width           =   1500
      End
      Begin NEWSOTALib.SOTAMaskedEdit txtContractRef 
         Height          =   285
         Left            =   1395
         TabIndex        =   15
         Top             =   1785
         WhatsThisHelpID =   53190
         Width           =   3885
         _Version        =   65536
         _ExtentX        =   6853
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         lMaxLength      =   40
      End
      Begin SOTACalendarControl.SOTACalendar dteEndDate 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1395
         TabIndex        =   13
         Top             =   960
         WhatsThisHelpID =   53189
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         BackColor       =   -2147483633
         DisplayOnly     =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Protected       =   -1  'True
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin NEWSOTALib.SOTANumber nbrSentToMcLeodCnt 
         Height          =   285
         Left            =   2760
         TabIndex        =   43
         Top             =   240
         Visible         =   0   'False
         WhatsThisHelpID =   53184
         Width           =   360
         _Version        =   65536
         _ExtentX        =   635
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,##<ILp0>#"
         text            =   "    0"
         sIntegralPlaces =   5
         sDecimalPlaces  =   0
      End
      Begin SOTACalendarControl.SOTACalendar dteSentToMcLeod 
         CausesValidation=   0   'False
         Height          =   315
         Left            =   1395
         TabIndex        =   44
         Top             =   1380
         WhatsThisHelpID =   53189
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         BackColor       =   -2147483633
         DisplayOnly     =   -1  'True
         Enabled         =   0   'False
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MaskedText      =   "  /  /    "
         Protected       =   -1  'True
         Text            =   "  /  /    "
         Object.CausesValidation=   0   'False
      End
      Begin VB.Label lblMcLeodDate 
         AutoSize        =   -1  'True
         Caption         =   "Sent To McLeod"
         Height          =   195
         Left            =   120
         TabIndex        =   45
         Top             =   1425
         Width           =   1200
      End
      Begin VB.Label lblBegDate 
         AutoSize        =   -1  'True
         Caption         =   "&Effective Date"
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   615
         Width           =   1020
      End
      Begin VB.Label lblEndDate 
         AutoSize        =   -1  'True
         Caption         =   "E&xpiration Date"
         Height          =   195
         Left            =   120
         TabIndex        =   12
         Top             =   1000
         Width           =   1080
      End
      Begin VB.Label lblContractRef 
         AutoSize        =   -1  'True
         Caption         =   "Contract &Ref"
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   1830
         Width           =   900
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Blanket SO"
      Height          =   1095
      Left            =   90
      TabIndex        =   1
      Top             =   540
      Width           =   5385
      Begin NEWSOTALib.SOTANumber nbrMaxGenNo 
         Height          =   285
         Left            =   2505
         TabIndex        =   6
         Top             =   600
         WhatsThisHelpID =   53184
         Width           =   930
         _Version        =   65536
         _ExtentX        =   1640
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<ILH>##|,##<ILp0>#"
         text            =   "    0"
         sIntegralPlaces =   5
         sDecimalPlaces  =   0
      End
      Begin NEWSOTALib.SOTACurrency curMaxGenAmt 
         Height          =   285
         Left            =   2505
         TabIndex        =   3
         Top             =   255
         WhatsThisHelpID =   53183
         Width           =   1905
         _Version        =   65536
         bShowCurrency   =   0   'False
         _ExtentX        =   3360
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         mask            =   "<HL><ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "           0.00"
         sDecimalPlaces  =   2
      End
      Begin VB.Label lblCurrID 
         AutoSize        =   -1  'True
         Caption         =   "USD"
         Height          =   195
         Left            =   4485
         TabIndex        =   4
         Top             =   300
         Width           =   345
      End
      Begin VB.Label lblMaxGenNo 
         AutoSize        =   -1  'True
         Caption         =   "Max &No. of SOs"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   645
         Width           =   1125
      End
      Begin VB.Label lblMaxGenAmt 
         AutoSize        =   -1  'True
         Caption         =   "Max SO &Total Amount"
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   300
         Width           =   1560
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Generated SO Totals"
      Height          =   1095
      Left            =   5610
      TabIndex        =   35
      Top             =   540
      Width           =   3345
      Begin NEWSOTALib.SOTACurrency curAmtSOs 
         Height          =   285
         Left            =   1125
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   300
         WhatsThisHelpID =   53178
         Width           =   2085
         _Version        =   65536
         bShowCurrency   =   0   'False
         _ExtentX        =   3678
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<HL><ILH>#|,###|,###|,###|,##<ILp0>#<IRp0>|.##"
         text            =   "            0.00"
         sIntegralPlaces =   13
         sDecimalPlaces  =   2
      End
      Begin NEWSOTALib.SOTANumber nbrNoSOs 
         Height          =   285
         Left            =   2295
         TabIndex        =   27
         Top             =   660
         WhatsThisHelpID =   53177
         Width           =   900
         _Version        =   65536
         _ExtentX        =   1587
         _ExtentY        =   503
         _StockProps     =   93
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         bProtected      =   -1  'True
         sBorder         =   0
         mask            =   "<ILH>##|,###|,##<ILp0>#"
         text            =   "       0"
         sIntegralPlaces =   8
         sDecimalPlaces  =   0
      End
      Begin VB.Label lblAmtSOs 
         AutoSize        =   -1  'True
         Caption         =   "Amount"
         Height          =   195
         Left            =   120
         TabIndex        =   37
         Top             =   345
         Width           =   540
      End
      Begin VB.Label lblNoSOs 
         AutoSize        =   -1  'True
         Caption         =   "No. of SOs"
         Height          =   195
         Left            =   120
         TabIndex        =   38
         Top             =   705
         Width           =   780
      End
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   39
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
End
Attribute VB_Name = "frmSOBlanketHdr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If
Public moCM             As New clsContextMenu

Private moDmBlanket     As clsDmForm
Private moClass         As Object
'Status
Private Const kvSOCanceled As Integer = 3
        
Public Property Let CurrExchRateMeth(iCurrExchRateMeth As Integer)
    moDmBlanket.SetColumnValue "CurrExchRateMeth", iCurrExchRateMeth
End Property

Public Property Get CurrExchRateMeth() As Integer
    CurrExchRateMeth = giGetValidInt(moDmBlanket.GetColumnValue("CurrExchRateMeth"))
End Property

Public Property Let NextRelNo(iNextRelNo As Integer)
    moDmBlanket.SetColumnValue "NextRelNo", iNextRelNo
End Property

Public Property Get FixedRate() As Boolean
    FixedRate = (chkFixedRate.Value = vbChecked)
End Property

Public Property Get Contract() As Boolean
    Contract = (chkContract.Value = vbChecked)
End Property

Public Property Get MaxAmountToGen() As Double
    MaxAmountToGen = gdGetValidDbl(curMaxGenAmt.Amount)
End Property

Public Property Get MaxSOToGen() As Integer
    MaxSOToGen = giGetValidInt(nbrMaxGenNo.Value)
End Property

Public Property Get NoSOReleased() As Integer
    NoSOReleased = giGetValidInt(nbrNoSOs.Value)
End Property

Public Property Get ReleasedToDate() As Double
    ReleasedToDate = gdGetValidDbl(curAmtSOs.Amount)
End Property

Public Property Get StartDate() As String
    StartDate = dteBegDate.SQLDate
End Property

Public Property Get StopDate() As String
    StopDate = dteEndDate.SQLDate
End Property

Public Property Get StopDateText() As String
    StopDateText = gsStripChar(lblEndDate, "&")
End Property

Public Property Get StartDateText() As String
    StartDateText = gsStripChar(lblBegDate, "&")
End Property
Private Property Get sMyName() As String
    sMyName = "frmSOBlanketHdr"
End Property
'************************************************************************
'   FormHelpPrefix will contain the help prefix for the Form Level Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'   <FormType> is "M" = Maintenance, "D" = data entry, "I" = Inquiry,
'                 "P" = PeriodEnd, "R" = Reports, "L" = Listings, . . .
'************************************************************************
Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    FormHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
'************************************************************************
'   WhatHelpPrefix will contain the help prefix for the What's This Help.
'   This is contstructed as:
'                      <ModuleID> & "Z" & <FormType>
'
'   <Module>   is "CI", "AP", "GL", . . .
'   "Z"        is the Sage MAS 500 identifier.
'************************************************************************
Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    WhatHelpPrefix = "SOZ"         '       Put your prefix here
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Function bIsValidMaxAmt() As Boolean
    If Val(curMaxGenAmt.Tag) = curMaxGenAmt.Amount Then
        bIsValidMaxAmt = True
        Exit Function
    End If

    If curMaxGenAmt.Amount <> 0 Then
        If curMaxGenAmt.Amount - curAmtSOs.Amount < 0 Then
            'The amount to generate may not be less than the total generated amount.", vbCritical, "Sage 500 ERP"
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgAmtLessThanTotal
            gbSetFocus Me, curAmtSOs
            Exit Function
        End If
    End If
    bIsValidMaxAmt = True

End Function
Private Function bIsValidMaxNo() As Boolean
    If nbrMaxGenNo.Tag = nbrMaxGenNo.Value Then
        bIsValidMaxNo = True
        Exit Function
    End If
    
    If nbrMaxGenNo.Value <> 0 Then
        
        If nbrMaxGenNo.Value - nbrNoSOs.Value < 0 Then
            'The maximum number of sales orders may not be less than the total generated number of sales orders.
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgNumLessThanTotal
            gbSetFocus Me, nbrNoSOs
            Exit Function
        End If
        
        If nbrMaxGenNo.Value > 32766 Then
            'Number of sales order to generate may not be greater than 32766.
            giSotaMsgBox Me, moClass.moSysSession, kSOmsgMaxNumGen
            gbSetFocus Me, nbrNoSOs
            Exit Function
        End If
    
    End If
    
    bIsValidMaxNo = True

End Function
Private Sub chkContract_Click()
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkContract, True
    #End If
'+++ End Customizer Code Push +++
    
    If chkContract.Value = vbUnchecked Then
        chkFixedRate.Value = vbUnchecked
        chkFixedRate.Enabled = False
        dteBegDate.Enabled = False
        dteBegDate.Text = ""
        dteBegDate.DisplayOnly = True
        dteEndDate.Text = ""
        dteEndDate.Enabled = False
        dteEndDate.DisplayOnly = True
        txtContractRef.Protected = True
        txtContractRef.Text = ""
        
        
'//* KMC -  Commented out because UCHwnd property was removed from
'           4.1 SotaCalendar.  Also, Enabled property now works properly

'        EnableWindow dteBegDate.UCHwnd, 0
'        EnableWindow dteEndDate.UCHwnd, 0

    Else
        chkFixedRate.Enabled = True
        txtContractRef.Protected = False
        
        dteBegDate.Enabled = True
        dteBegDate.DisplayOnly = False
'        EnableWindow dteBegDate.UCHwnd, 1
        dteBegDate.DisplayOnly = False
        dteBegDate.Enabled = True
        dteBegDate.BackColor = vbWindowBackground
        dteBegDate.TabStop = True
        
        dteEndDate.Enabled = True
        dteEndDate.DisplayOnly = False
'        EnableWindow dteEndDate.UCHwnd, 1
        dteEndDate.DisplayOnly = False
        dteEndDate.Enabled = True
        dteEndDate.BackColor = vbWindowBackground
        dteEndDate.TabStop = True
    
    End If
End Sub
Private Function bCheckContract()
    If chkContract.Value = vbChecked Then
        If Len(Trim(txtContractRef.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, _
                gsStripChar(lblContractRef.Caption, "&")
            gbSetFocus Me, txtContractRef
            Exit Function
        End If
        
        If Len(Trim(dteBegDate.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, _
                gsStripChar(lblBegDate.Caption, "&")
            gbSetFocus Me, dteBegDate
            Exit Function
        End If
        
        If Len(Trim(dteEndDate.Text)) = 0 Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgRequiredField, _
                gsStripChar(lblEndDate.Caption, "&")
            gbSetFocus Me, dteEndDate
            Exit Function
        End If
    
    End If
    bCheckContract = True
End Function

Public Sub Init(oClass As Object, oDmBlanket As Object)
    Set moDmBlanket = oDmBlanket
    Set moClass = oClass
End Sub

Public Sub ShowMe(lSOKey As Long, bUseMultiCurr As Boolean, iDecPlaces As Integer, sCurrID As String)
   
Dim rs      As Object
Dim sSQL    As String
Dim lOtherSOKey As Long

     
    If Not bUseMultiCurr Then
        chkFixedRate.Visible = False
    Else
        chkFixedRate.Visible = True
        If chkContract.Value = vbChecked Then
            chkFixedRate.Enabled = True
        Else
            chkFixedRate.Enabled = False
        End If
    End If
    
    If giGetValidInt(moDmBlanket.GetColumnValue("CurrExchRateMeth")) = 1 Then
        chkFixedRate.Value = vbChecked
    Else
        chkFixedRate.Value = vbUnchecked
    End If
    
    curAmtSOs.DecimalPlaces = iDecPlaces
    curLastSOAmt.DecimalPlaces = iDecPlaces
    curMaxGenAmt.DecimalPlaces = iDecPlaces
    lblCurrID.Caption = sCurrID
    
    curMaxGenAmt.Tag = curMaxGenAmt.Amount
    nbrMaxGenNo.Tag = nbrMaxGenNo.Value
     sSQL = "SELECT COALESCE(SUM(SalesAmt), 0) As CurntSOAmt  " & _
           " FROM tsoSalesOrder Where BlnktSOkey = " & lSOKey & " AND Status <> " & kvSOCanceled
           
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEOF Then
        curAmtSOs.Amount = gdGetValidDbl(rs.Field("CurntSOAmt"))
    End If
    
    Set rs = Nothing
    ' Get No of SOs
    sSQL = "SELECT count(*) As NoofSO  " & _
           " FROM tsoSalesOrder Where BlnktSOkey = " & lSOKey & " AND Status <> " & kvSOCanceled
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If Not rs.IsEOF Then
        nbrNoSOs = glGetValidLong(rs.Field("NoofSO"))
    End If
    Set rs = Nothing
  'GET LAST Sales Order
    lOtherSOKey = glGetValidLong(moClass.moAppDB.Lookup("MAX(SOKey)", "tsoSalesOrder", "BlnktSOKey = " & lSOKey & " AND Status <> " & kvSOCanceled))
    
    If lOtherSOKey <> 0 Then
        
        sSQL = "SELECT TranNoRelChngOrd, TranDate, SalesAmt FROM tsoSalesOrder WHERE SOKey = " & lOtherSOKey
        Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
        
        If Not rs.IsEOF Then
            txtLastSONo.Text = gsGetValidStr(rs.Field("TranNoRelChngOrd"))
            dteLastSODate.SQLDate = gsGetValidStr(rs.Field("TranDate"))
            curLastSOAmt.Amount = gdGetValidDbl(rs.Field("SalesAmt"))
        Else
            txtLastSONo.Text = ""
            dteLastSODate.Text = ""
            curLastSOAmt.Amount = 0
        End If
        
        Set rs = Nothing
    
    Else
        txtLastSONo.Text = ""
        dteLastSODate.Text = ""
        curLastSOAmt.Amount = 0
    End If
   
    
    Me.Show vbModal
    
    If chkFixedRate.Value = vbChecked Then
        moDmBlanket.SetColumnValue "CurrExchRateMeth", 1
    Else
        moDmBlanket.SetColumnValue "CurrExchRateMeth", 2
    End If
    
End Sub
Public Function bIsValidBlnktHeader()
    
    If bIsValidMaxAmt Then
        If bIsValidMaxNo Then
            If bCheckContract Then
                If bIsValidDates Then
                    bIsValidBlnktHeader = True
                End If
            End If
        End If
   End If

End Function
Private Function bIsValidDates()
Dim bCheckDates As Boolean

    If Len(Trim(dteBegDate.Text)) > 0 Then
        If dteBegDate.IsValid Then
            bCheckDates = True
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, _
                gsStripChar(lblBegDate.Caption, "&")
            gbSetFocus Me, dteBegDate
            Exit Function
        End If
    Else
        bCheckDates = False
    End If
    
    If Len(Trim(dteEndDate.Text)) > 0 Then
        If dteEndDate.IsValid Then
            bCheckDates = True
        Else
            giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidField, _
                gsStripChar(lblEndDate.Caption, "&")
            gbSetFocus Me, dteEndDate
            Exit Function
        End If
    Else
        bCheckDates = False
    End If

    If bCheckDates Then
        If dteBegDate.SQLDate > dteEndDate.SQLDate Then
            giSotaMsgBox Me, moClass.moSysSession, kmsgCannotBeGreaterThan, _
                 gsStripChar(lblBegDate.Caption, "&"), gsStripChar(lblEndDate.Caption, "&")
            gbSetFocus Me, dteBegDate
            Exit Function
        End If
    End If
    
    bIsValidDates = True
End Function


Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
#If CUSTOMIZER Then
    If (Shift = 4) Then
        gProcessFKeys Me, keycode, Shift
    End If
#End If

    Select Case keycode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, keycode, Shift
    End Select

End Sub

Private Sub Form_Load()
    tbrMain.RemoveButton kTbCancelExit
    
    
    Set moCM.Form = frmSOBlanketHdr
    moCM.Init


End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    If UnloadMode = vbFormControlMenu Then
        If bIsValidBlnktHeader Then
            Me.Hide
        End If
        Cancel = True
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    Set moCM = Nothing
    Set moClass = Nothing
    Set moDmBlanket = Nothing
End Sub





Private Sub tbrMain_ButtonClick(Button As String)
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(Button) Then
            Exit Sub
        End If
    End If
#End If

    Select Case Button
        Case kTbFinishExit
            If bIsValidBlnktHeader Then
                Me.Hide
            End If
        Case kTbHelp
            gDisplayFormLevelHelp Me
       End Select
End Sub

#If CUSTOMIZER Then
Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If


Private Sub txtLastSONo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtLastSONo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastSONo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastSONo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtLastSONo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastSONo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastSONo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtLastSONo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastSONo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtLastSONo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtLastSONo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtLastSONo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContractRef_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtContractRef, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContractRef_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContractRef_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtContractRef, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContractRef_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContractRef_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtContractRef, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContractRef_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtContractRef_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtContractRef, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtContractRef_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub curLastSOAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curLastSOAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLastSOAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curLastSOAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curLastSOAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLastSOAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curLastSOAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curLastSOAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLastSOAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curLastSOAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curLastSOAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curLastSOAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxGenNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrMaxGenNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxGenNo_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxGenNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrMaxGenNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxGenNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxGenNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrMaxGenNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxGenNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrMaxGenNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrMaxGenNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrMaxGenNo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curMaxGenAmt_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curMaxGenAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curMaxGenAmt_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curMaxGenAmt_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curMaxGenAmt, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curMaxGenAmt_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curMaxGenAmt_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curMaxGenAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curMaxGenAmt_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curMaxGenAmt_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curMaxGenAmt, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curMaxGenAmt_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curAmtSOs_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curAmtSOs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curAmtSOs_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curAmtSOs_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curAmtSOs, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curAmtSOs_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curAmtSOs_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curAmtSOs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curAmtSOs_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curAmtSOs_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curAmtSOs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curAmtSOs_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrNoSOs_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrNoSOs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrNoSOs_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrNoSOs_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrNoSOs, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrNoSOs_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrNoSOs_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrNoSOs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrNoSOs_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrNoSOs_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrNoSOs, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrNoSOs_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkFixedRate_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkFixedRate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkFixedRate_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkFixedRate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkFixedRate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkFixedRate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkFixedRate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkFixedRate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkFixedRate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkContract_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkContract, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkContract_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkContract_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkContract, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkContract_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteLastSODate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteLastSODate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteLastSODate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteLastSODate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteLastSODate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteLastSODate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteLastSODate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteLastSODate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteLastSODate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteLastSODate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteLastSODate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteLastSODate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteBegDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteBegDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteBegDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteBegDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteBegDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteBegDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteBegDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteBegDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteBegDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteBegDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteBegDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteBegDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteEndDate_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange dteEndDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteEndDate_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteEndDate_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress dteEndDate, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteEndDate_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteEndDate_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus dteEndDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteEndDate_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub dteEndDate_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus dteEndDate, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "dteEndDate_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Public Property Get oClass() As Object
    Set oClass = moClass
End Property
Public Property Get MyApp() As Object
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

'****************************************************************************************
'****************************************************************************************
'SGS Custom Methods (START)
'****************************************************************************************
'****************************************************************************************
Private Function sSendToMcLeod()  'SGS PTc IA
    On Error GoTo Error
    
    Dim rs As Object
    Dim Fs As Scripting.FileSystemObject
    Dim Fld As Folder
    Dim Fls As Files, Fl As File
    Dim sDate As String
    Dim sTime As String
    Dim sExportFile As String
    Dim sStagingPath As String
    Dim i As Integer
    Dim sSQL As String
    Dim ii As Integer
    Dim sFileExtension As String
    Dim ErrMSG As String
    
    sSendToMcLeod = "Untrapped Error creating XML files for McLeod integration"
    
    ' Do a quick check to ensure there are Contract Lines that have been awarded and are ready to be exported
    sSQL = "SELECT * FROM vsoContractLine_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEOF Then
        sSendToMcLeod = "No SO Lines exist with 'Awarded' indicator set.  No XML file produced for McLeod integration"
        Exit Function
    End If
    
    'Get Folder path and file extention
    If sGetStagPath(sStagingPath, sFileExtension, ErrMSG) = False Then
        sSendToMcLeod = ErrMSG
        Exit Function
    End If
    
    sDate = Format(Date, "YYYYMMDD")
    sTime = Format(Time, "HHMMSS")
    sExportFile = "IA" & sDate & sTime & "." & sFileExtension
    
    Open sStagingPath & "\" & sExportFile For Output As #1 ' Open file for output.
    Print #1, "<loadmaster_unload>"
    
    ' Get the Customer record to be exported
    sSQL = "SELECT * FROM varCustomer_SGS WHERE CustKey = " & glGetValidLong(frmSalesOrd.lkuCustID.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       Do While Not rs.IsEOF
          Print #1, "<table_record table = " & """" & "customer" & """" & ">"
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          Print #1, "</table_record>"
          rs.MoveNext
       Loop
    Else
        sSendToMcLeod = "Error creating XML files for McLeod integration - Customer"
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Customer Contact record to be exported
    sSQL = "SELECT * FROM vciContact1_SGS WHERE EntityType = 501 AND CntctOwnerKey = " & glGetValidLong(frmSalesOrd.lkuCustID.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       Do While Not rs.IsEOF
          Print #1, "<table_record table = " & """" & "contact" & """" & ">"
          ' For Loop starts at 2 to avoid the key fields in positions 0 & 1
          For i = 2 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          Print #1, "</table_record>"
          rs.MoveNext
       Loop
    Else
'        sSendToMcLeod = "Error creating XML files for McLeod integration - Contact1"
'        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Location record to be exported
    sSQL = "SELECT * FROM vsoLocation_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       Do While Not rs.IsEOF
          Print #1, "<table_record table = " & """" & "location" & """" & ">"
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          Print #1, "</table_record>"
          rs.MoveNext
       Loop
    Else
        sSendToMcLeod = "Error creating XML files for McLeod integration - Location"
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Contract/Blanket record to be exported  (PART 1)
    sSQL = "SELECT * FROM vsoContract_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       Print #1, "<table_record table = " & """" & "iasx_contracts" & """" & ">"
       rs.MoveFirst
       Do While Not rs.IsEOF
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          rs.MoveNext
       Loop
    Else
        sSendToMcLeod = "Error creating XML files for McLeod integration - Contract1"
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Contract/Blanket record to be exported  (PART 2)
    sSQL = "SELECT * FROM vsoContract2_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       ii = 0
       Do While Not rs.IsEOF
          ii = ii + 1
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & IIf(ii > 1, Trim(str(ii)), "") & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & IIf(ii > 1, Trim(str(ii)), "") & ">"
          Next i
          rs.MoveNext
       Loop
       Print #1, "</table_record>"
    Else
        sSendToMcLeod = "Error creating XML files for McLeod integration - Contract2"
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Product records to be exported
    sSQL = "SELECT * FROM vsoProduct_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       Do While Not rs.IsEOF
          Print #1, "<table_record table = " & """" & "iasx_products" & """" & ">"
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          Print #1, "</table_record>"
          rs.MoveNext
       Loop
    Else
        sSendToMcLeod = "Error creating XML files for McLeod integration - Product"
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Contract Line records to be exported
    sSQL = "SELECT * FROM vsoContractLine_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       Do While Not rs.IsEOF
          Print #1, "<table_record table = " & """" & "iasx_ctrct_prdct" & """" & ">"
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          Print #1, "</table_record>"
          rs.MoveNext
       Loop
    Else
        sSendToMcLeod = "Error creating XML files for McLeod integration - Contract Lines"
        Exit Function
    End If
    
    Set rs = Nothing
    
    ' Get the Contract Contact records to be exported  (PART 2)
    sSQL = "SELECT * FROM vsoContractContact_SGS WHERE SOKey = " & glGetValidLong(frmSalesOrd.lkuSONum.KeyValue)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
       rs.MoveFirst
       Do While Not rs.IsEOF
          Print #1, "<table_record table = " & """" & "iasx_ctrct_cnts" & """" & ">"
          ' For Loop starts at 1 to avoid the key field in position 0
          For i = 1 To rs.FieldCount - 1
             If rs.Field(i) > "" Then Print #1, "<" & LCase(rs.FieldName(i)) & ">" & Replace(Replace(Replace(rs.Field(i), "&", "&amp;"), "'", "&apos;"), """", "&quot;") & "</" & LCase(rs.FieldName(i)) & ">"
          Next i
          Print #1, "</table_record>"
          rs.MoveNext
       Loop
'    Else
'        sSendToMcLeod = "Error creating XML files for McLeod integration - Contract Contacts"
'        Exit Function
    End If
    
    Set rs = Nothing
    
    Print #1, "</loadmaster_unload>"
    sSendToMcLeod = ""
    
    Exit Function
Error:
    ErrMSG = Me.Name & ".sSendToMcLeod()" & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf
    ErrMSG = ErrMSG & "File Path: " & sStagingPath
    ErrMSG = ErrMSG & "File Extension: " & sFileExtension
    ErrMSG = ErrMSG & "Full Path: " & sStagingPath & "\" & sExportFile
    
    MsgBox ErrMSG, vbExclamation, "SAGE 500"
    
    Err.Clear
    
End Function


'SGS Custom
Private Function sGetStagPath(ByRef sStagingPath As String, ByRef sFileExtension As String, Optional ByRef ErrMSG As String = Empty) As Boolean  'SGS PTC IA
    On Error GoTo Error
    Dim FSO As New Scripting.FileSystemObject
    Dim lsStagingPath As String
    Dim lsFileExension As String
    
    'Get folder name
    lsStagingPath = Trim(gsGetValidStr(moClass.moAppDB.Lookup("Top 1 (UNCPath)", "tsoMcLeodXMLFolder", Empty)))
    lsFileExension = Trim(gsGetValidStr(moClass.moAppDB.Lookup("Top 1 (FileExtention)", "tsoMcLeodXMLFolder", Empty)))
    
    'Make sure there was a folder returned
    If lsStagingPath = Empty Or lsFileExension = Empty Then
        'Path not there
        ErrMSG = "You will need to setup the Staging Folder location in the table 'tsoMcLeodXMLFolder' before exporting.  You will need to fill in the file extension and full path to the export folder.  This path can be a UNC path."
        sGetStagPath = False
        GoTo CleanUP
    End If
    
    'Make sure that the folder exists.
    If FSO.FolderExists(lsStagingPath) = False Then
        ErrMSG = "The Staging Folder location: " & vbCrLf & _
        lsStagingPath & vbCrLf & _
        "Does not exist." & vbCrLf & vbCrLf & _
        "You will need to setup the Staging Folder location in the table 'tsoMcLeodXMLFolder' before exporting.  You will need to fill in the file extension and full path to the export folder.  This path can be a UNC path."
        sGetStagPath = False
        GoTo CleanUP
    End If
    
    'Set Return Values
    sStagingPath = lsStagingPath
    sFileExtension = lsFileExension
    
    sGetStagPath = True
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    Set FSO = Nothing
    
    Exit Function
Error:
    ErrMSG = "The following error occurred while obtaining the Staging Folder location for the export:" & vbCrLf & _
    "Error: " & Err.Number & " " & Error
    
    GoTo CleanUP
End Function


Private Sub cmdReleaseToMcLeod_Click()  'SGS PTC IA
    On Error GoTo Error:
    
    Dim RetMsg As String
    Dim iResponse As Integer
    Dim ErrMSG As String
    
    If chkSentToMcLeod.Value = vbChecked Then
        iResponse = MsgBox("XML information for McLeod integration already released.  Release again?", vbQuestion + vbYesNo, "Sage MAS 500")
        If iResponse = vbNo Then Exit Sub
    End If
    
    RetMsg = sSendToMcLeod
    
    If RetMsg > "" Then
        MsgBox RetMsg, vbCritical + vbOKOnly, "MAS 500"
    Else
        MsgBox "McLeod integration information released successfully", vbInformation + vbOKOnly, "Sage MAS 500"
        chkSentToMcLeod.Value = vbChecked
        nbrSentToMcLeodCnt = Val(nbrSentToMcLeodCnt) + 1
        dteSentToMcLeod.Value = Date + Time     'RKL DEJ 1/23/14
    End If
    Close #1
    
    Exit Sub
Error:
    ErrMSG = Me.Name & ".cmdReleaseToMcLeod_Click()" & vbCrLf
    ErrMSG = ErrMSG & "Error: " & Err.Number & " " & Err.Description
    
    MsgBox ErrMSG, vbExclamation, "SAGE 500"
    
    Err.Clear

End Sub

'****************************************************************************************
'****************************************************************************************
'SGS Custom Methods (STOP)
'****************************************************************************************
'****************************************************************************************






