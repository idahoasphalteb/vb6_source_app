Attribute VB_Name = "bassozxxCompMaint"
Option Explicit

Const VBRIG_MODULE_ID_STRING = "sozxxCompMaint.BAS"



Public Sub Main()
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozxxCompMaint.clssozxxCompMaint")
        StartAppInStandaloneMode oApp, Command$()    ' ** The third parameter MUST be the
                                                     '    TaskID for this project in order for the
                                                     '    Standalone method to be activated for ActiveX Exe's
    End If
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "bassozxxCompMaint"
End Function
