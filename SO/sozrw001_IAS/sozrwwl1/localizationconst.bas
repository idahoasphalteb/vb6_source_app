Attribute VB_Name = "basLocalizationConst"
Option Explicit

'
'*********************************************************************************
' This file contains the String and Message constant definitions for this project
' based upon the LocalString and LocalMessage tables.
'
' Created On: 1/7/2004 12:39:49 PM
'*********************************************************************************
'
' Strings
'
Public Const kARZJA001CheckingBatch = 151130                ' Checking Batch Status
Public Const kARZJA001Detail = 151108                       ' Detail
Public Const kARZJA001Invoice = 151109                      ' Invoice Register
Public Const kARZJA001PostingComplete = 151124              ' Posting Complete
Public Const kARZJA001PrintError = 151128                   ' Printing Error Log
Public Const kARZJA001PrintErrorCompleted = 151164          ' Error Log Completed
Public Const kARZJA001PrintingRegister = 151122             ' Printing Register
Public Const kARZJA001ProcessingGLReg = 151129              ' Processing GL Register
Public Const kARZJA001ProcessRegister = 151121              ' Processing Register
Public Const kARZJA001RegisterPrinted = 151123              ' Register Printed
Public Const kARZJA001Summary = 151107                      ' Summary
Public Const kARZJA001Validating = 151127                   ' Validating Data
Public Const kGLPosting = 151424                            ' GL Posting
Public Const kGLPostingComplete = 151425                    ' GL Posting Complete
Public Const kJrnlBalanced = 654                        ' Balanced
Public Const kJrnlFatalErrors = 652                     ' Fatal Errors Exist
Public Const kJrnlNotBalanced = 653                     ' Out of Balance
Public Const kJrnlNotPrinted = 650                      ' Not Printed
Public Const kJrnlPosted = 656                          ' Posting Completed
Public Const kJrnlPosting = 655                         ' Posting
Public Const kJrnlPostingFailed = 657                   ' Posting Failed
Public Const kJrnlPrinted = 651                         ' Printed
Public Const kModulePosted = 6506                           ' Module Posted
Public Const kmsgModPostIncomp = 151440                     ' Module posting incomplete.
Public Const kPostingModule = 6505                          ' Posting Module
Public Const kSOCRReturn = 250487                           ' Return
Public Const kSOCustRegRptTitle = 250491                    ' Customer Return Register
Public Const kSOPrinter = 250013                            ' Printer
Public Const kSOScreen = 250014                             ' Screen
Public Const kSOShipDate = 250062                           ' Ship Date
Public Const kSOShipping = 250549                           ' Shipping
Public Const kSOShipRegGLDtl = 250353                       ' General Ledger Posting Register - Detail
Public Const kSOShipRegGLSum = 250352                       ' General Ledger Posting Register - Summary
Public Const kSOShipRegRptTitle = 250351                    ' Shipping Register
Public Const kSOShipToAddress = 250068                      ' Ship To Address
Public Const kSOSRRptErrorTitle = 250354                    ' Error Log Listing
Public Const kVAlreadyPosted = 151271                       ' Batch {0}: The batch has already been posted.
Public Const kVBadTotal = 151275                            ' Batch {0}: The total of the transactions in the batch does not equal the control
Public Const kVBatchBStatus = 151269                        ' Batch {0}: tciBatchLog contains an invalid value in PostStatus or Status.
Public Const kVBatchDeleted = 151272                        ' Batch {0}: The Batch has been deleted.
Public Const kVBatchHold = 151274                           ' Batch {0}: The Batch has been placed on hold.
Public Const kVBatchPrePost = 151268                        ' Batch {0}: The batch is currently being pre-posted.
Public Const kVBeingPosted = 151270                         ' Batch {0}: The Batch is currently being posted.
Public Const kVClosedPer = 151418                           ' Batch {0}: Posting to a closed period.
Public Const kVCmCantPostGL = 150542                        ' Batch {0}: Cannot post to GL because of CM Options.
Public Const kVCustIactive = 151126                         ' Pre-posting Completed
Public Const kVFatalErrorPrePost = 151266                   ' Batch {0}: Unexpected error while pre-posting.
Public Const kVMoreUsers = 151273                           ' Batch {0}: One or more users are currently using the batch.
Public Const kVNonPer = 151434                              ' Batch {0}: Posting to a non-existent period.
Public Const kVNoPost = 151281                              ' Batch {0}: Register cannot post.  Invalid data encountered.
Public Const kVSDelCustKey = 151125                         ' Pre-posting
'
' Messages
'
Public Const kmsgSotaErr = 1002                             '             Module: {0}  Company:  {1}  AppName:  {2}Error < {3} >  occurred at < {4} >  in Procedure < {5} >{6}                                    This application will now close.
Public Const kmsgUnexpectedConfirmUnloadRV = 100009         ' Unexpected Confirm Unload Return Value: {0}
Public Const kmsgErrorGeneratingRegister = 100072           ' Errors were encountered generating the {0} Register.
Public Const kmsgCannotPostFatalErrors = 100073             ' {0} Register cannot post.  Invalid data encountered.Check for invalid accounts, closed periods and out-of-balance journals.
Public Const kmsgPostRegister = 100074                      ' Do you want to post the {0} Register?
Public Const kmsgNoBatches = 100076                         ' No {0} batches for posting.
Public Const kmsgCantPost = 153277                          ' Cannot post to GL because of AR Options.
Public Const kmsgMoreUsers = 153278                         ' One or more users are currently using the batch.
Public Const kmsgBatchHold = 153279                         ' The Batch has been placed on hold.
Public Const kmsgBadTotal = 153280                          ' The total of the transactions in the batch does not equal the control total of the batch.
Public Const kmsgBeingPosted = 153281                       ' The Batch is currently being posted.
Public Const kmsgAlreadyPosted = 153282                     ' The batch has been posted.
Public Const kmsgBatchDeleted = 153283                      ' The Batch has been deleted.
Public Const kmsgFatalReportInit = 153302                   ' Unexpected Error Initializing Report Engine.
Public Const kInsertSettingsErr = 153342                    ' Error inserting settings {0} into tsmReportSetting.
Public Const kmsgBatchPrePost = 153345                      ' The batch is currently being pre-posed.
Public Const kmsgBatchBStatus = 153346                      ' Batch log contains invalid post status or status.
Public Const kmsgCantLoadDDData = 153369                    ' Unable to load data from Data Dictionary.
Public Const kmsgFataSortInit = 153370                      ' Unable to initialize Sort Grid.
Public Const kmsgCantDeleteStandard = 153384                ' Sorry, you are not allowed to delete settings named {0}.
Public Const kmsgDeleteRptSettings = 153385                 ' Are you sure you want to delete the report settings for {0}?
Public Const kmsgSetNotDeleted = 153386                     ' Setting Name {0}, RptSettingKey = {1} NOT deleted.
Public Const kmsgReplaceSettings = 153388                   ' Do you want to replace the existing report settings for {0}?
Public Const kmsgErrorReportSettingsSaveAs = 153389         ' Error in ReportSettingsSaveAs for {0}, RptSettingKey = {1}.
Public Const kmsgCantLoadRptSettings = 153392               ' Unable to load settings.
Public Const kmsgModPIncomp = 153507                        ' Module posting incomplete.
Public Const kmsgCIAlreadyPosting = 120088                  ' You may not print or post Batch {0}.  It is already posting.
Public Const kmsgCIInUseOrPosting = 120089                  ' You may not post batch {0}.  It is either in use or posting.
Public Const kmsgCIBadStatus = 120090                       ' You may not post batch {0}. It is not in a balanced or interrupted status.
Public Const kmsgCantGetBatchLock = 120098                  ' Error occurred while attempting to lock batch  {0}.
Public Const kmsgPostErrors = 140072                        ' Errors found during Posting.  Would you like to print the Error Log?
Public Const kmsgFatalErrorPrePost = 140073                 ' Unexpected Error while Pre-Posting.
Public Const kmsgFlagNotFound = 140074                      ' Batch Key not found during print flag reset.
Public Const kSOCustReturnReport = 250939                   ' Customer Return Report
Public Const kSOShippingReport = 250938                     ' Shipping Report
Public Const kmsgNoRepData = 100327                         ' There is no data to print for this report. Report processing cancelled.

