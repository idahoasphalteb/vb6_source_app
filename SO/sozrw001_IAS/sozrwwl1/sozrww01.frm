VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#105.0#0"; "sotatbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#45.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#47.0#0"; "sotacalendar.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmSOTranReport 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6630
   ClientLeft      =   1140
   ClientTop       =   2370
   ClientWidth     =   9795
   HelpContextID   =   17776424
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6630
   ScaleWidth      =   9795
   Begin TabDlg.SSTab tabReport 
      Height          =   4935
      Left            =   120
      TabIndex        =   16
      Top             =   1080
      WhatsThisHelpID =   19
      Width           =   9555
      _ExtentX        =   16854
      _ExtentY        =   8705
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "&Main"
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraSort"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraPrint"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "&Options"
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraMessage"
      Tab(1).Control(1)=   "cboFormat"
      Tab(1).Control(2)=   "lblFormat"
      Tab(1).ControlCount=   3
      Begin VB.Frame fraPrint 
         Caption         =   "Print Transactions"
         Height          =   1965
         Left            =   180
         TabIndex        =   28
         Top             =   540
         Width           =   9255
         Begin FPSpreadADO.fpSpread grdSelectTrans 
            Height          =   1425
            Left            =   120
            TabIndex        =   29
            Top             =   300
            WhatsThisHelpID =   17776426
            Width           =   8985
            _Version        =   524288
            _ExtentX        =   15849
            _ExtentY        =   2514
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   0
            MaxRows         =   0
            SpreadDesigner  =   "sozrww01.frx":0000
            AppearanceStyle =   0
         End
      End
      Begin VB.Frame fraMessage 
         Caption         =   "&Message"
         Height          =   1815
         Left            =   -74820
         TabIndex        =   21
         Top             =   1200
         Width           =   9195
         Begin VB.PictureBox pctMessage 
            Height          =   1275
            Left            =   180
            ScaleHeight     =   1215
            ScaleWidth      =   8775
            TabIndex        =   22
            Top             =   330
            Width           =   8835
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   4
               Left            =   0
               MaxLength       =   50
               TabIndex        =   27
               Top             =   1020
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   3
               Left            =   0
               MaxLength       =   50
               TabIndex        =   26
               Top             =   765
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   2
               Left            =   0
               MaxLength       =   50
               TabIndex        =   25
               Top             =   510
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   1
               Left            =   0
               MaxLength       =   50
               TabIndex        =   24
               Top             =   255
               WhatsThisHelpID =   18
               Width           =   8770
            End
            Begin VB.TextBox txtMessageHeader 
               Appearance      =   0  'Flat
               BorderStyle     =   0  'None
               Height          =   240
               Index           =   0
               Left            =   -780
               MaxLength       =   50
               TabIndex        =   23
               Top             =   0
               WhatsThisHelpID =   18
               Width           =   9555
            End
            Begin VB.Line Line1 
               Index           =   0
               X1              =   0
               X2              =   8775
               Y1              =   240
               Y2              =   240
            End
            Begin VB.Line Line1 
               Index           =   1
               X1              =   0
               X2              =   8775
               Y1              =   500
               Y2              =   500
            End
            Begin VB.Line Line1 
               Index           =   2
               X1              =   0
               X2              =   8775
               Y1              =   750
               Y2              =   750
            End
            Begin VB.Line Line1 
               Index           =   3
               X1              =   0
               X2              =   8775
               Y1              =   1010
               Y2              =   1010
            End
         End
      End
      Begin VB.ComboBox cboFormat 
         Height          =   315
         Left            =   -74040
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   660
         WhatsThisHelpID =   17776432
         Width           =   2475
      End
      Begin VB.Frame fraSort 
         Caption         =   "&Sort"
         Height          =   1965
         Left            =   180
         TabIndex        =   17
         Top             =   2760
         Width           =   9255
         Begin FPSpreadADO.fpSpread grdSort 
            Height          =   1545
            Left            =   150
            TabIndex        =   18
            Top             =   270
            WhatsThisHelpID =   25
            Width           =   8985
            _Version        =   524288
            _ExtentX        =   15849
            _ExtentY        =   2725
            _StockProps     =   64
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MaxCols         =   1
            MaxRows         =   1
            SpreadDesigner  =   "sozrww01.frx":03F8
            AppearanceStyle =   0
         End
      End
      Begin VB.Label lblFormat 
         Caption         =   "Format"
         Height          =   195
         Left            =   -74700
         TabIndex        =   20
         Top             =   720
         Width           =   675
      End
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   0
      WhatsThisHelpID =   63
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   741
      Style           =   3
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -30000
      TabIndex        =   5
      Top             =   0
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin VB.ComboBox CustomCombo 
      Enabled         =   0   'False
      Height          =   315
      Index           =   0
      Left            =   -30000
      Style           =   2  'Dropdown List
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1890
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -30000
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -30000
      TabIndex        =   12
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CheckBox chkSummary 
      Caption         =   "Print &Report Settings"
      Height          =   285
      Left            =   6210
      TabIndex        =   3
      Top             =   540
      WhatsThisHelpID =   31
      Width           =   2205
   End
   Begin VB.ComboBox cboReportSettings 
      Height          =   315
      Left            =   1020
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   510
      WhatsThisHelpID =   23
      Width           =   3705
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   13
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   7
      Top             =   645
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   6240
      WhatsThisHelpID =   73
      Width           =   9795
      _ExtentX        =   17277
      _ExtentY        =   688
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -30000
      TabIndex        =   1
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblSetting 
      Caption         =   "S&etting"
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   570
      Width           =   795
   End
End
Attribute VB_Name = "frmSOTranReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'***********************************************************************
'     Name: frmSOTranReport
'     Desc: Print SO Tran Report and Customer Return Report
'Copyright: Copyright (c) 1995-2005 Best Software, Inc.
' Original: 02-15-05
'     Mods:
'***********************************************************************
Option Explicit
#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Context Menu Object
    Public moContextMenu           As clsContextMenu

'Private variables of Form Properties
    Public moClass             As Object
    
    Private mlRunMode           As Long
    Private mbCancelShutDown    As Boolean
    Private mbEnterAsTab        As Boolean
    Private mlRetValue          As Long
    Private mbunload            As Boolean
    Private mbProcessing        As Boolean 'used to prevent user from closing form (and clearing temp tables) before processing is completed
    Private miProcessType       As Integer
    
'Private variables for Report Engine
    Private mbLoading               As Boolean
    Private mbLoadSuccess           As Boolean
    Private mbLoadingSettings       As Boolean

'Public Form Variables
    Public msSOReportPath       As String
    Public msPrintButton        As String
    Public moSotaObjects        As New Collection
    Public mlBatchKey           As Long
    Public miBatchType          As Integer

'variables used in posting multiple batches
    Public miMode               As Integer  '1 = default, show form, 0 = posting multiple batches, no UI

'variables for Report Engine
    Public moReport             As clsReportEngine
    Public moSettings           As clsSettings
    Public moSort               As clsSort
    Public moDDData             As clsDDData
    Public moOptions            As clsOptions
    Public moPrinter            As Printer
    Public sRealTableCollection As Collection
    Public sWorkTableCollection As Collection
 
    Private m_CustShipSelectColMgr          As clsSelectGridColMgr
    Private m_DropShipSelectColMgr          As clsSelectGridColMgr
    Private m_TransferShipSelectColMgr      As clsSelectGridColMgr
    Private m_CustReturnShipSelectColMgr    As clsSelectGridColMgr
    
    Private m_CustShipSelectSetting         As clsSelectionSettings
    Private m_DropShipSelectSetting         As clsSelectionSettings
    Private m_TransferShipSelectSetting     As clsSelectionSettings
    Private m_CustReturnShipSelectSetting   As clsSelectionSettings
    
    Private WithEvents m_SelectTransCustShip        As clsCISelectTrans
Attribute m_SelectTransCustShip.VB_VarHelpID = -1
    Private WithEvents m_SelectTransDropShip        As clsCISelectTrans
Attribute m_SelectTransDropShip.VB_VarHelpID = -1
    Private WithEvents m_SelectTransTrnsfrShip      As clsCISelectTrans
Attribute m_SelectTransTrnsfrShip.VB_VarHelpID = -1
    Private WithEvents m_SelectTransCustRtrnShip    As clsCISelectTrans
Attribute m_SelectTransCustRtrnShip.VB_VarHelpID = -1
    
'Private form variables
    Public msCompanyID          As String
    Public mlLanguage           As Long
        
    Private guLocalizedStrings  As uLocalizedStrings
    
    Private mbIsDLL             As Boolean
    Private mbFormLoad          As Boolean
    Private miReport            As Integer
    Private mlHoldCount         As Long
    Private mlHiddenBatchKey    As Long
    Private moModuleOptions     As clsModuleOptions
    Private mbPeriodEnd         As Boolean
    
    Private Enum Report
        ShipRegister = 1
        CustReturns = 2
    End Enum

'Format indices
   Public miSummary                   As Integer
   Public miDetail                    As Integer
    
'Batch Type
    Private Const kBatchTypeShip = 801   'SH Shipment
    Private Const kBatchTypeCustR = 802  'Customer Return
     
'Private Constant
    Private Const BACKSLASH = "\"

'Select Tran Type Rows
    Private Const kRowTranTypeCustShip = 1
    Private Const kRowTranTypeDropShip = 2
    Private Const kRowTranTypeTrnsfrShip = 3
    Private Const kRowTranTypeCustRtrn = 4
   
'Setting App Names for uniqueness
    Const kAppNameSOCommitCustShip = "SOCommitCustShip"
    Const kAppNameSOCommitDropShip = "SOCommitDropShip"
    Const kAppNameSOCommitTransferShip = "SOCommitTransferShip"
    Const kAppNameSOCommitCustomerReturnsShip = "SOCommitCustomerReturnsShip"
   
'Select Tran Type columns
    Private Const kColTranType = 1
    Private Const kColCommit = 2
    Private Const kColTranTypeDesc = 3
    Private Const kColUncommittedCount = 4
    Private Const kColSelectedCount = 5
    Private Const kColSelect = 6
    Private Const kColSetting = 7
    
    Private Const kMaxCols = 7
    
    Private Const kCustShipTmpTblName As String = "#CustShipCommitPrint"
    Private Const kDropShipTmpTblName As String = "#DropShipCommitPrint"
    Private Const kTrnsfrShipTmpTblName As String = "#TrnsfrShipCommitPrint"
    Private Const kCustRtrnShipTmpTblName As String = "#CustRtrnShipCommitPrint"
    
    'TranStatus constants used.
    Private Const kvTranStatusPending = 2
    
    Const VBRIG_MODULE_ID_STRING = "SOZRWW01.FRM"

Public Property Let bunload(bnewvalue As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbunload = bnewvalue
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bunload_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get bunload() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    bunload = mbunload
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bunload_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub cboReportSettings_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Skip +++
    If Len(cboReportSettings.Text) > 40 Then
        Beep
        KeyAscii = 0
    End If
End Sub

Private Sub cboReportSettings_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
       
    If moSettings.gbSkipClick Then Exit Sub
    mbLoadingSettings = True
    moSettings.bLoadReportSettings
    mbLoadingSettings = False
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboReportSettings_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++

End Sub


'************************ Sort Stuff *******************************

Private Sub grdSort_KeyUp(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridKey KeyCode, Shift
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_KeyUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSort_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridChange Col, Row
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
Private Sub GrdSort_GotFocus()
    If Not mbLoading Then moSort.SortGridGotFocus
End Sub
Private Sub GrdSort_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
    If Not mbLoading Then moSort.GridLeaveCell Col, Row, NewCol, NewRow
End Sub

Private Sub GrdSort_KeyDown(KeyCode As Integer, Shift As Integer)
    If Not mbLoading Then moSort.SortGridKeyDown KeyCode, Shift
End Sub

Private Sub grdSort_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSort.GridButtonClicked Col, Row, ButtonDown
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdSort_ButtonClicked", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
'*********************** End Sort Stuff *****************************

Private Sub Form_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mbCancelShutDown = False
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Initialize", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case KeyCode
        Case vbKeyF1 To vbKeyF16
            gProcessFKeys Me, KeyCode, Shift
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case KeyAscii
        Case vbKeyReturn
            If mbEnterAsTab Then
                gProcessSendKeys "{Tab}"
                KeyAscii = 0
            End If
        Case Else
    End Select
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Function lInitializeReport(ModuleID As String, DefaultReport As String, ProjectName As String) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iFound, i As Integer
Dim lRetVal As Long
Dim sSOShipDate As String
Dim sSOShipToAddress As String

    lInitializeReport = -1
    
    Set moPrinter = Printer
    Set moSotaObjects = New Collection
    If moReport Is Nothing Then
        Set moReport = New clsReportEngine
    End If
    
    Set moSettings = New clsSettings
    Set moSort = New clsSort
    Set moDDData = New clsDDData
    Set moOptions = New clsOptions

    moReport.UI = True
    
    If Not moDDData.lInitDDData(sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        lInitializeReport = kmsgCantLoadDDData
        Exit Function
    End If
    
    moReport.AppOrSysDB = kAppDB
    
    If (moReport.lInitReport(ModuleID, DefaultReport, Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFatalReportInit
        Exit Function
    End If

    '-- Add desired selection fields to the data dictionary
    moDDData.lAddDDDataRow "tarCustomer", "CustName"
    moDDData.lAddDDDataRow "tsoPendShipment", "CustKey"
    moDDData.lAddDDDataRow "tsoPendShipment", "ShipMethKey"
    moDDData.lAddDDDataRow "tsoShipLine", "ShipDate"
    moDDData.lAddDDDataRow "tsoSOLineDist", "ShipToAddrKey"
         
    If (moSort.lInitSort(Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFataSortInit
        Exit Function
    End If

    'Set to Customer Returns
    miReport = Report.CustReturns
    miBatchType = BATCH_TYPE_CUSTRET
    SetupWorkTables

    moDDData.lAddDDDataRow "tarCustomer", "CustName"
    moDDData.lAddDDDataRow "tsoPendShipment", "CustKey"
    moDDData.lAddDDDataRow "tsoPendShipment", "TranDate"
    moDDData.lAddDDDataRow "tsoSOLineDist", "ShipToAddrKey"

    If (moSort.lInitSort(Me, moDDData) = kFailure) Then
        lInitializeReport = kmsgFataSortInit
        Exit Function
    End If
   
    miReport = Report.ShipRegister
    miBatchType = BATCH_TYPE_SHIP
    SetupWorkTables
        
    'Add all the sorts, let user pick what they want, remove invalid sorts per report later
    sSOShipDate = gsBuildString(kSOShipDate, moClass.moAppDB, moClass.moSysSession)
    sSOShipToAddress = gsBuildString(kSOShipToAddress, moClass.moAppDB, moClass.moSysSession)
    
    'Common to both
    moSort.lInsSort "CustKey", "tsoPendShipment", moDDData
    moSort.lInsSort "CustName", "tarCustomer", moDDData
    'Shipment Register
    moSort.lInsSort "ShipDate", "tsoShipLine", moDDData, sSOShipDate
    moSort.lInsSort "ShipMethKey", "tsoPendShipment", moDDData
    moSort.lInsSort "CustAddrID", "tarCustAddr", moDDData, sSOShipToAddress
    'Customer Returns
    moSort.lInsSort "TranDate", "tsoPendShipment", moDDData, "Return Date"
    moSort.lInsSort "RMANo", "tsoPendShipment", moDDData, "RMA"
    
    lInitializeReport = 0
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lInitializeReport", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim lRet As Long
    
    Set sbrMain.Framework = moClass.moFramework
    sbrMain.BrowseVisible = False
    
    tbrMain.RemoveButton kTbCustomizer
    tbrMain.RemoveButton kTbDefer

    With moClass.moSysSession
        mlLanguage = .Language
        msCompanyID = .CompanyId
        mbEnterAsTab = .EnterAsTab
    End With
    
    If moClass.IsDLL Then
        mbIsDLL = True
    End If

    mbFormLoad = True
    cboFormat.AddItem gsBuildString(kARZJA001Summary, moClass.moAppDB, moClass.moSysSession)
    miSummary = cboFormat.NewIndex
    cboFormat.AddItem gsBuildString(kARZJA001Detail, moClass.moAppDB, moClass.moSysSession)
    miDetail = cboFormat.NewIndex
    cboFormat.ListIndex = miSummary
 
    mbFormLoad = False

    miProcessType = moClass.ProcessType
        
    tabReport.Tab = 0
        
    miReport = Report.ShipRegister
        
    If lSetBatchType(miReport, True) = kFailure Then
        ' error
    End If
    
    BuildLocalizedStrings moClass.moAppDB, moClass.moSysSession, guLocalizedStrings, Me

    If mbIsDLL = False Then
        gbCreateTmpTblSOTransToCommit moClass.moAppDB
    End If
    
    If moClass.Report = True Then
        Me.Caption = "Print SO Transaction Report"
    Else
        Me.Caption = "Print SO Transaction Register"
    End If
            
    SetupCustShipmentSelect
    SetupDropShipmentSelect
    SetupTransferShipmentSelect
    SetupCustRtrnShipmentSelect
    
    SetGridProperties
            
    'Get Hidden Batch for commit
    Set moModuleOptions = New clsModuleOptions
    With moModuleOptions
        Set .oSysSession = moClass.moSysSession
        Set .oAppDB = moClass.moAppDB
        .sCompanyID = msCompanyID
    End With
    mlHiddenBatchKey = glGetValidLong(moModuleOptions.SO("ShipmentHiddenBatchKey"))
        
    With moOptions
        .Add cboFormat
        .Add chkSummary
    End With
    
    mbLoadingSettings = True
    
    ' initialize the report setting combo
    moSettings.Initialize Me, cboReportSettings, moSort, , moOptions, , tbrMain, sbrMain
  
    msSOReportPath = moClass.moSysSession.ModuleReportPath("SO")
    lRet = lValidateReportPath(msSOReportPath)
        
    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmSOTranReport
        .Init
    End With
    
    mbLoadingSettings = False
    sbrMain.Status = SOTA_SB_START
        
    mbLoadSuccess = True
    
    Exit Sub
    
badexit:

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub SetupWorkTables()
    
    Dim iCount      As Integer
    
    If mbFormLoad Then Exit Sub

    For iCount = 1 To sWorkTableCollection.Count
        sWorkTableCollection.Remove 1
    Next iCount
    
    If miBatchType = BATCH_TYPE_SHIP Then
        sWorkTableCollection.Add "tsoShipRegSumWrk"
        sWorkTableCollection.Add "tsoShipRegDtlWrk"
    Else
        sWorkTableCollection.Add "tsoCustRRegSumWrk"
        sWorkTableCollection.Add "tsoCustRRegDtlWrk"
        sWorkTableCollection.Add "tsoCustRetBLSWrk"
    End If

    'Used for all types
    sWorkTableCollection.Add "timInvtDistWrk"
    sWorkTableCollection.Add "tciAddressWrk"
        
End Sub


Private Sub Form_Paint()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim ret As Integer
Static bActivate As Boolean
    
    If Not bActivate Then
        bActivate = True
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If
    
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

    moReport.CleanupWorkTables
    
    If Not mbPeriodEnd Then
        moSettings.SaveLastSettingKey
    End If
    
    If (moClass.mlError = 0) Then
        '-- If a report preview window is active, query user to continue closing.
        If Not moReport.bShutdownEngine Then
            GoTo CancelShutDown
        End If
        
        If Not mbPeriodEnd And gbActiveChildForms(Me) Then
            GoTo CancelShutDown
        End If
    End If
    
    If (UnloadMode <> vbFormCode) Then
        moClass.miShutDownRequester = kUnloadSelfShutDown
    End If
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown

    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
            
        Case Else 'kFrameworkShutDown
        'Do Nothing
        
    End Select
    
    Exit Sub
    
CancelShutDown:
    Cancel = True
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Exit Sub
    
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If

    Set moSotaObjects = Nothing
    Set moSettings = Nothing
    Set moSort = Nothing

    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If

    Set moDDData = Nothing
    Set moOptions = Nothing
    Set moPrinter = Nothing
    Set sRealTableCollection = Nothing
    Set sWorkTableCollection = Nothing

    If Not moReport Is Nothing Then
        Set moReport = Nothing
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Un", VBRIG_IS_FORM
        Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub hlpHelp_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    gDisplayFormLevelHelp Me

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "hlpHelp_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Property Get oClass() As Object
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set oClass = moClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moClass = oNewClass
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "oClass_Set", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let ProcessType(iProcessType As Integer)
    miProcessType = iProcessType
End Property

Public Property Get ProcessType() As Integer
    ProcessType = miProcessType
End Property


Public Property Get lRunMode() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    lRunMode = mlRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    mlRunMode = lNewRunMode
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lRunMode_Let", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Private Sub m_SelectTransCustRtrnShip_DeSelectTransactions(ByVal sWhereClause As String)
    Debug.Print "Deleselect Transactions"
    Dim sSQL As String
    
    On Error GoTo ErrorRoutine
    
    If InStr(1, sWhereClause, "tsoShipment.PostDate", vbTextCompare) = 0 Then
        sSQL = " DELETE  " & kCustRtrnShipTmpTblName
    
        sSQL = sSQL & " FROM  " & kCustRtrnShipTmpTblName & vbCrLf & _
                " INNER JOIN      tsoPendShipment     WITH (NOLOCK) ON tsoPendShipment.ShipKey        = " & kCustRtrnShipTmpTblName & ".ShipmentKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoCustRtrnShipment WITH (NOLOCK) ON tsoCustRtrnShipment.ShipKey    = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoRMA              WITH (NOLOCK) ON tsoRMA.RMAKey                  = tsoCustRtrnShipment.RMAKey" & vbCrLf & _
                " LEFT OUTER JOIN timWarehouse        WITH (NOLOCK) ON timWarehouse.WhseKey           = tsoPendshipment.RcvgWhseKey" & vbCrLf & _
                " LEFT OUTER JOIN tarCustomer         WITH (NOLOCK) ON tarCustomer.CustKey            = tsoPendShipment.CustKey" & vbCrLf & _
                " LEFT OUTER JOIN tciAddress          WITH (NOLOCK) ON tciAddress.AddrKey             = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK) ON tsoShipLine.ShipKey            = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSOLine           WITH (NOLOCK) ON tsoSOLine.SOLineKey            = tsoShipLine.SOLineKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSalesOrder       WITH (NOLOCK) ON tsoSalesOrder.SOKey            = tsoSOLine.SOKey" & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK) ON tciShipMethod.ShipMethKey      = tsoPendShipment.ShipMethKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK) ON tciBatchLog.BatchKey           = tsoPendShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK) ON tsoShipmentLog.ShipKey         = tsoPendShipment.ShipKey" & vbCrLf
                    
        If Len(Trim(sWhereClause)) > 0 Then
            sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
        End If
        
        moClass.moAppDB.ExecuteSQL sSQL

    End If
    
    sSQL = " DELETE  " & kCustRtrnShipTmpTblName

    sSQL = sSQL & " FROM  " & kCustRtrnShipTmpTblName & vbCrLf & _
            " INNER JOIN      tsoShipment     WITH (NOLOCK) ON tsoShipment.ShipKey        = " & kCustRtrnShipTmpTblName & ".ShipmentKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoCustRtrnShipment WITH (NOLOCK) ON tsoCustRtrnShipment.ShipKey    = tsoShipment.ShipKey " & vbCrLf & _
            " LEFT OUTER JOIN tsoRMA              WITH (NOLOCK) ON tsoRMA.RMAKey                  = tsoCustRtrnShipment.RMAKey" & vbCrLf & _
            " LEFT OUTER JOIN timWarehouse        WITH (NOLOCK) ON timWarehouse.WhseKey           = tsoShipment.RcvgWhseKey" & vbCrLf & _
            " LEFT OUTER JOIN tarCustomer         WITH (NOLOCK) ON tarCustomer.CustKey            = tsoShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN tciAddress          WITH (NOLOCK) ON tciAddress.AddrKey             = tsoShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK) ON tsoShipLine.ShipKey            = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoSOLine           WITH (NOLOCK) ON tsoSOLine.SOLineKey            = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoSalesOrder       WITH (NOLOCK) ON tsoSalesOrder.SOKey            = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK) ON tciShipMethod.ShipMethKey      = tsoShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK) ON tciBatchLog.BatchKey           = tsoShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK) ON tsoShipmentLog.ShipKey         = tsoShipment.ShipKey" & vbCrLf
            
    If Len(Trim(sWhereClause)) > 0 Then
        sWhereClause = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL

    moClass.moAppDB.CommitTrans
    
    Exit Sub

ErrorRoutine:

    moClass.moAppDB.Rollback
    
    gSetSotaErr Err, sMyName, "m_SelectTransCustShip_DeSelectTransactions", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select

End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolBarClick Button
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub SetGridProperties()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'---------------------------------------------------------------------
'   Specify application-oriented properties in logical order
'---------------------------------------------------------------------
    Dim sSQL    As String
    Dim rs      As Object
    
    Dim lLastSettingKey As Long
    
    gGridSetProperties grdSelectTrans, kMaxCols, kGridDataSheetNoAppend
    grdSelectTrans.ScrollBars = ScrollBarsNone
    gGridSetMaxRows grdSelectTrans, 4
    
    gGridSetColumnWidth grdSelectTrans, kColCommit, 6
    gGridSetColumnWidth grdSelectTrans, kColTranTypeDesc, 17
    gGridSetColumnWidth grdSelectTrans, kColUncommittedCount, 0
    gGridSetColumnWidth grdSelectTrans, kColSelectedCount, 10
    gGridSetColumnWidth grdSelectTrans, kColSelect, 6
    gGridSetColumnWidth grdSelectTrans, kColSetting, 20

    gGridSetColumnType grdSelectTrans, kColTranType, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdSelectTrans, kColCommit, SS_CELL_TYPE_CHECKBOX
    gGridSetColumnType grdSelectTrans, kColTranTypeDesc, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdSelectTrans, kColUncommittedCount, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdSelectTrans, kColSelectedCount, SS_CELL_TYPE_INTEGER
    gGridSetColumnType grdSelectTrans, kColSelect, SS_CELL_TYPE_BUTTON
    gGridSetColumnType grdSelectTrans, kColSetting, SS_CELL_TYPE_COMBOBOX
    
    gGridSetHeader grdSelectTrans, kColTranType, "Tran Type"
    gGridSetHeader grdSelectTrans, kColCommit, "Print"
    gGridSetHeader grdSelectTrans, kColTranTypeDesc, "Transaction Type"
    gGridSetHeader grdSelectTrans, kColUncommittedCount, "Uncommitted"
    gGridSetHeader grdSelectTrans, kColSelectedCount, "Selected"
    gGridSetHeader grdSelectTrans, kColSelect, "Select"
    gGridSetHeader grdSelectTrans, kColSetting, "Setting"
    
    gGridHideColumn grdSelectTrans, kColTranType
    gGridLockColumn grdSelectTrans, kColTranTypeDesc
    gGridLockColumn grdSelectTrans, kColUncommittedCount
    gGridLockColumn grdSelectTrans, kColSelectedCount
    
    grdSelectTrans.ButtonDrawMode = ButtonDrawModeAlways

    sSQL = "Select tt.TranType, ls.LocalText as TranTypeDesc" & _
            " FROM tciTranType tt WITH (NOLOCK) " & _
            " JOIN tsmLocalString ls WITH (NOLOCK) ON ls.StringNo = tt.TranDescStrNo" & _
            " WHERE tt.TranType IN (" & kTranTypeSOSH & ", " & kTranTypeSODS & ", " & kTranTypeSORT & ", " & kTranTypeSOTS & ")"
    
    Dim colTT As Collection
    Set colTT = New Collection
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    While Not rs.IsEOF
        colTT.Add Trim(rs.Field("TranTypeDesc")), CStr(rs.Field("TranType"))
        
        rs.MoveNext
    Wend
    rs.Close
    Set rs = Nothing
    
    With grdSelectTrans
        .Col = kColCommit
        .Row = -1
        .Value = 1

        .Row = -1
        .Col = kColSelect
        .TypeButtonText = "..."
        .Col = kColSelectedCount
        .Value = 0

        .Row = kRowTranTypeCustShip
        .Col = kColTranTypeDesc
        .Text = colTT(CStr(kTranTypeSOSH))
        
        .Row = kRowTranTypeDropShip
        .Col = kColTranTypeDesc
        .Text = colTT(CStr(kTranTypeSODS))
        
        .Row = kRowTranTypeTrnsfrShip
        .Col = kColTranTypeDesc
        .Text = colTT(CStr(kTranTypeSOTS))
        
        .Row = kRowTranTypeCustRtrn
        .Col = kColTranTypeDesc
        .Text = colTT(CStr(kTranTypeSORT))
    
    End With
       
    lLastSettingKey = m_CustShipSelectSetting.LastSettingKey
    RefreshGridColSelectSetting kTranTypeSOSH, lLastSettingKey, True
    
    lLastSettingKey = m_DropShipSelectSetting.LastSettingKey
    RefreshGridColSelectSetting kTranTypeSODS, lLastSettingKey, True
    
    lLastSettingKey = m_TransferShipSelectSetting.LastSettingKey
    RefreshGridColSelectSetting kTranTypeSOTS, lLastSettingKey, True
    
    lLastSettingKey = m_CustReturnShipSelectSetting.LastSettingKey
    RefreshGridColSelectSetting kTranTypeSORT, lLastSettingKey, True


'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridProperties", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub grdSelectTrans_ButtonClicked(ByVal Col As Long, ByVal Row As Long, ByVal ButtonDown As Integer)
    
    If Col = kColSelect Then
    
        sbrMain.Message = ""

        Select Case Row
            Case kRowTranTypeCustShip
                InitSelectTransCustomerShipment
                LoadCustShipSelectTmpTbl
                grdSelectTrans.Row = Row
                grdSelectTrans.Col = kColSetting
                m_SelectTransCustShip.ShowMe m_CustShipSelectSetting.SettingKey(Trim(grdSelectTrans.Text))
                 
            Case kRowTranTypeDropShip
                InitSelectTransDropShipment
                LoadDropShipSelectTmpTbl
                grdSelectTrans.Row = Row
                grdSelectTrans.Col = kColSetting
                m_SelectTransDropShip.ShowMe m_DropShipSelectSetting.SettingKey(Trim(grdSelectTrans.Text))
                 
            Case kRowTranTypeTrnsfrShip
                InitSelectTransTrnsfrShipment
                LoadTrnsfrSelectTmpTbl
                grdSelectTrans.Row = Row
                grdSelectTrans.Col = kColSetting
                m_SelectTransTrnsfrShip.ShowMe m_TransferShipSelectSetting.SettingKey(Trim(grdSelectTrans.Text))
            
            Case kRowTranTypeCustRtrn
                InitSelectTransCustRtrnsShipment
                LoadCustRtrnSelectTmpTbl
                grdSelectTrans.Row = Row
                grdSelectTrans.Col = kColSetting
                m_SelectTransCustRtrnShip.ShowMe m_CustReturnShipSelectSetting.SettingKey(Trim(grdSelectTrans.Text))
            
        End Select

    End If
    
End Sub

Private Sub grdSelectTrans_ComboSelChange(ByVal Col As Long, ByVal Row As Long)
    Debug.Print "grdSelectTrans_ComboSelChange & col, row " & Col & ", " & Row; ""
    
    grdSelectTrans.Row = Row
    grdSelectTrans.Col = Col

    Select Case Row
        Case kRowTranTypeCustShip
            ApplySelectionSetting kTranTypeSOSH, m_CustShipSelectSetting.SettingKey(grdSelectTrans.Text)
            
        Case kRowTranTypeDropShip
            ApplySelectionSetting kTranTypeSODS, m_DropShipSelectSetting.SettingKey(grdSelectTrans.Text)
            
        Case kRowTranTypeCustRtrn
            ApplySelectionSetting kTranTypeSORT, m_CustReturnShipSelectSetting.SettingKey(grdSelectTrans.Text)
        
        Case kRowTranTypeTrnsfrShip
            ApplySelectionSetting kTranTypeSOTS, m_TransferShipSelectSetting.SettingKey(grdSelectTrans.Text)
           
    End Select
    
End Sub

Private Sub RefreshGridColSelectSetting(ByVal lTranType As Long, ByVal lSettingKey As Long, Optional ByVal bApplySetting As Boolean = False)
     
    Dim lRow As Long
    Dim oSelectSettings As clsSelectionSettings
    
    lRow = GetRowFromTranType(lTranType)
    Set oSelectSettings = GetSelectSettingsFromTranType(lTranType)
    
    With grdSelectTrans
        .Row = lRow
        .Col = kColSetting

        .TypeComboBoxList = oSelectSettings.SettingList
        
        If .TypeComboBoxCount > 0 Then
            
            If lSettingKey = 0 Then
                If oSelectSettings.IsNoneSettingList Then
                    .TypeComboBoxCurSel = 0
                Else
                    .TypeComboBoxCurSel = -1
                End If
            Else
                .Text = oSelectSettings.SettingID(lSettingKey)
                If bApplySetting Then
                    ApplySelectionSetting lTranType, lSettingKey
                End If
                            
            End If
        End If
    End With
  
    Set oSelectSettings = Nothing
    
End Sub

Private Sub SetEnabledFromSelected(ByVal lTranType As Long)

    Dim lInvoiceSelectCount As Long
    
    lInvoiceSelectCount = 0
    If lTranType = kTranTypeSOSH Or lTranType = kTranTypeSORT Then
    
        With grdSelectTrans
            .Row = kRowTranTypeCustShip
            .Col = kColSelectedCount
            lInvoiceSelectCount = glGetValidLong(.Value)
            .Row = kRowTranTypeCustRtrn
            lInvoiceSelectCount = lInvoiceSelectCount + glGetValidLong(.Value)
        End With
    End If

End Sub


Private Sub ProcessRegister()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim lRetVal As Long
    Dim oSessionID As Long
    Dim iMsgReturn As Integer
    Dim lReturnCode As Long
    Dim sSortBy As String
    Dim sFormulas() As String
    Dim iLoop As Integer
    Dim prn As Integer
    Dim sRegSP As String
    Dim mcSPInCreateTable As New Collection
    Dim mcSPOutCreateTable As New Collection
    Dim sortNum As Integer
    Static ShipReportHasNoRecords As Boolean
    
    ReDim sFormulas(0, 0)

    sbrMain.Status = SOTA_SB_BUSY
            
    'create work table to print register from
    Screen.MousePointer = vbHourglass
    
    If miBatchType = BATCH_TYPE_SHIP Then
              
        If miProcessType = 1 Then
            mcSPInCreateTable.Add mlBatchKey
        Else
            mcSPInCreateTable.Add 0 'mlShipKey
        End If
        
        Select Case cboFormat.ListIndex
            Case -1, miSummary
                mcSPInCreateTable.Add 0 'summary
            Case miDetail
                mcSPInCreateTable.Add 1 'detail
        End Select
            
        mcSPInCreateTable.Add msCompanyID
        mcSPInCreateTable.Add moReport.SessionID
        mcSPInCreateTable.Add mlLanguage
        If miProcessType = 1 Then
            mcSPInCreateTable.Add 1 'Batch
        Else
            mcSPInCreateTable.Add 2 'ShipKey
        End If
        mcSPOutCreateTable.Add mlRetValue
            
        sRegSP = "spsoShippingReg"
    
    Else 'BATCH_TYPE_CUSTRET
        If miProcessType = 1 Then
            mcSPInCreateTable.Add mlBatchKey
        Else
            mcSPInCreateTable.Add 0 'mlShipKey
        End If
        
        Select Case cboFormat.ListIndex
            Case -1, miSummary
                mcSPInCreateTable.Add 0 'summary
            Case miDetail
                mcSPInCreateTable.Add 1 'detail
        End Select
            
        mcSPInCreateTable.Add msCompanyID
        mcSPInCreateTable.Add moReport.SessionID
        mcSPInCreateTable.Add mlLanguage
        
        If miProcessType = 1 Then
            mcSPInCreateTable.Add 1 'Batch
        Else
            mcSPInCreateTable.Add 2 'ShipKey
        End If
        
        mcSPOutCreateTable.Add mlRetValue
        
        sRegSP = "spsoCustRetReg"
    End If

    'process register
    lRetVal = lExecSP(sRegSP, mcSPInCreateTable, mcSPOutCreateTable)
            
    If RecordsToPrint(sRegSP) = False Then
        If StrComp(sRegSP, "spsoShippingReg", vbTextCompare) = 0 Then
            ShipReportHasNoRecords = True
            Exit Sub
        Else
            If ShipReportHasNoRecords = True Then
                ShipReportHasNoRecords = False
                giSotaMsgBoxDCD Me, moClass.moSysSession, kmsgNoRepData
                Exit Sub
            Else
                Exit Sub
            End If
        End If
        
    End If

    Screen.MousePointer = vbArrow
                
    If cboFormat.ListIndex = miSummary Then
        StartShipRegSum msPrintButton, Me
        moReport.CascadePreview
    Else
        StartShipRegDtl msPrintButton, Me
        moReport.CascadePreview
    End If
        
    If lRetVal <> 0 Then
        sbrMain.Status = SOTA_SB_START
        Screen.MousePointer = vbArrow
        moClass.lUIActive = kChildObjectInactive
        Set mcSPInCreateTable = Nothing
        Set mcSPOutCreateTable = Nothing
            
        Exit Sub
    End If
        
    Screen.MousePointer = vbArrow
            
    sbrMain.Status = SOTA_SB_START
            
    Set mcSPInCreateTable = Nothing
    Set mcSPOutCreateTable = Nothing
            
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ProcessRegister", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
    On Error GoTo ExpectedErrorRoutine
    On Error Resume Next


    If Not m_SelectTransCustShip Is Nothing Then
        m_SelectTransCustShip.UnloadMe
        Set m_SelectTransCustShip = Nothing
    End If
    
    If Not m_SelectTransDropShip Is Nothing Then
        m_SelectTransDropShip.UnloadMe
        Set m_SelectTransDropShip = Nothing
    End If
        
    If Not m_SelectTransTrnsfrShip Is Nothing Then
        m_SelectTransTrnsfrShip.UnloadMe
        Set m_SelectTransTrnsfrShip = Nothing
    End If
    
    If Not m_SelectTransCustRtrnShip Is Nothing Then
        m_SelectTransCustRtrnShip.UnloadMe
        Set m_SelectTransCustRtrnShip = Nothing
    End If
    
     
    If Not m_CustShipSelectSetting Is Nothing Then
        m_CustShipSelectSetting.UnloadMe
        Set m_CustShipSelectSetting = Nothing
    End If
    
    If Not m_DropShipSelectSetting Is Nothing Then
        m_DropShipSelectSetting.UnloadMe
        Set m_DropShipSelectSetting = Nothing
    End If
    
    If Not m_CustReturnShipSelectSetting Is Nothing Then
        m_CustReturnShipSelectSetting.UnloadMe
        Set m_CustReturnShipSelectSetting = Nothing
    End If
    
    If Not m_TransferShipSelectSetting Is Nothing Then
        m_TransferShipSelectSetting.UnloadMe
        Set m_TransferShipSelectSetting = Nothing
    End If
   
     
    If Not m_CustShipSelectColMgr Is Nothing Then
        m_CustShipSelectColMgr.UnloadMe
        Set m_CustShipSelectColMgr = Nothing
    End If
    
    If Not m_DropShipSelectColMgr Is Nothing Then
        m_DropShipSelectColMgr.UnloadMe
        Set m_DropShipSelectColMgr = Nothing
    End If
    
    If Not m_CustReturnShipSelectColMgr Is Nothing Then
        m_CustReturnShipSelectColMgr.UnloadMe
        Set m_CustReturnShipSelectColMgr = Nothing
    End If
    
    If Not m_TransferShipSelectColMgr Is Nothing Then
        m_TransferShipSelectColMgr.UnloadMe
        Set m_TransferShipSelectColMgr = Nothing
    End If

    gUnloadChildForms Me
    
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    Set moSotaObjects = Nothing
    
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    'Exit this subroutine
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Sub
    
ExpectedErrorRoutine:

gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Sub
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    FormHelpPrefix = "SOZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "FormHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    WhatHelpPrefix = "SOZ"
    
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "WhatHelpPrefix_Get", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Property
        End Select
'+++ VB/Rig End +++
End Property
          
Private Sub CreateInvcTables()

    Static bDid As Boolean
    
    Dim sCRLF As String
    Dim sSQL  As String

    sCRLF = Chr(13) & Chr(10)
    sSQL = ""

    If Not bDid Then

        sSQL = sSQL & "SELECT * INTO #tarAPIInvcDetl FROM tarAPIInvcDetlWrk WHERE 1=2" & sCRLF
        sSQL = sSQL & "CREATE INDEX #tarAPIInvcDetl_idx1 ON #tarAPIInvcDetl (DetailKey)" & sCRLF
        sSQL = sSQL & "CREATE INDEX #tarAPIInvcDetl_idx2 ON #tarAPIInvcDetl (KitDetailKey)" & sCRLF

        bDid = True

    Else

        sSQL = sSQL & "TRUNCATE TABLE #tarAPIInvcDetl" & sCRLF

    End If

    On Error Resume Next
    
    moClass.moAppDB.ExecuteSQL sSQL

End Sub

Public Function PrintReports() As Boolean
    
    Dim lRow(10) As Long
    Dim sTableColumnName(10) As String
    Dim sDescription(10) As String
    Dim iSub(10) As Integer
    Dim iPageBreak(10) As Integer
    Dim iDescending(10) As Integer
    Dim sColumnName(10) As String
    Dim iSeqNo(10) As Integer
    Dim sTableName(10) As String
    Dim sParentTableName(10) As String
    Dim sSOShipDate As String
    Dim sSOShipToAddress As String
    Dim lRet As Long
    Dim iChecked As Integer
            
    sSOShipDate = gsBuildString(kSOShipDate, moClass.moAppDB, moClass.moSysSession)
    sSOShipToAddress = gsBuildString(kSOShipToAddress, moClass.moAppDB, moClass.moSysSession)
    
    CreateInvcTables
    
    '   Show print dialog if not Post Batches or if
    '   Post Batches and first time through. HBS.
    gbSkipPrintDialog = False
    
    'Prevent user from closing form before processing is completed (so temp tables are
    'not cleared before they should be).
    mbProcessing = True
    
    'First Report is Shipping Register - set it up
    miReport = Report.ShipRegister
    miBatchType = BATCH_TYPE_SHIP
    SetupWorkTables
            
    'Find positions of sorts in grid
    'Customer Returns Only
    lRow(0) = moSort.lSortGridPosition("tsoPendShipment", "TranDate")
    lRow(1) = moSort.lSortGridPosition("tsoPendShipment", "RMANo")
    'Ship Register Only
    lRow(2) = moSort.lSortGridPosition("tsoShipLine", "ShipDate")
    lRow(3) = moSort.lSortGridPosition("tsoPendShipment", "ShipMethKey")
    lRow(4) = moSort.lSortGridPosition("tarCustAddr", "CustAddrID")
    'Shared by both
    lRow(5) = moSort.lSortGridPosition("tarCustomer", "CustID")
    lRow(6) = moSort.lSortGridPosition("tarCustomer", "CustName")
    
    'Collect all the sort settings in use
    If lRow(0) > 0 Then
        lRet = moSort.lGetRow(lRow(0), sTableColumnName(0), sDescription(0), iSub(0), iPageBreak(0), iDescending(0), sColumnName(0), iSeqNo(0), sTableName(0), sParentTableName(0))
    End If
    
    If lRow(1) > 0 Then
        lRet = moSort.lGetRow(lRow(1), sTableColumnName(1), sDescription(1), iSub(1), iPageBreak(1), iDescending(1), sColumnName(1), iSeqNo(1), sTableName(1), sParentTableName(1))
    End If
    
    If lRow(2) > 0 Then
        lRet = moSort.lGetRow(lRow(2), sTableColumnName(2), sDescription(2), iSub(2), iPageBreak(2), iDescending(2), sColumnName(2), iSeqNo(2), sTableName(2), sParentTableName(2))
    End If
    
    If lRow(3) > 0 Then
        lRet = moSort.lGetRow(lRow(3), sTableColumnName(3), sDescription(3), iSub(3), iPageBreak(3), iDescending(3), sColumnName(3), iSeqNo(3), sTableName(3), sParentTableName(3))
    End If
    
    If lRow(4) > 0 Then
        lRet = moSort.lGetRow(lRow(4), sTableColumnName(4), sDescription(4), iSub(4), iPageBreak(4), iDescending(4), sColumnName(4), iSeqNo(4), sTableName(4), sParentTableName(4))
    End If
    
    If lRow(5) > 0 Then
        lRet = moSort.lGetRow(lRow(5), sTableColumnName(5), sDescription(5), iSub(5), iPageBreak(5), iDescending(5), sColumnName(5), iSeqNo(5), sTableName(5), sParentTableName(5))
    End If
    
    If lRow(6) > 0 Then
        lRet = moSort.lGetRow(lRow(6), sTableColumnName(6), sDescription(6), iSub(6), iPageBreak(6), iDescending(6), sColumnName(6), iSeqNo(6), sTableName(6), sParentTableName(6))
    End If
    
    moSort.CleanSortGrid
    
    With grdSelectTrans
        .Row = kRowTranTypeCustShip
        .Col = kColCommit
        iChecked = giGetValidInt(.Value)
        
         If iChecked <> vbChecked Then
            .Row = kRowTranTypeDropShip
            iChecked = giGetValidInt(.Value)
         End If
         
         If iChecked <> vbChecked Then
            .Row = kRowTranTypeTrnsfrShip
            iChecked = giGetValidInt(.Value)
         End If
    End With
        
    If iChecked = vbChecked Then
        
        'Initialize the first report
        If (moReport.lInitReport("SO", "sozdt001.RPT", Me, moDDData) = kFailure) Then
            Debug.Print "failure"
        End If
        
        'Initialize the sort for the first report
        If (moSort.lInitSort(Me, moDDData, , , True) = kFailure) Then
            Debug.Print "failure"
        End If
        
        'Insert the sorts for Ship Register
        moSort.lInsSort "ShipDate", "tsoShipLine", moDDData, sSOShipDate
        moSort.lInsSort "ShipMethKey", "tsoPendShipment", moDDData
        moSort.lInsSort "CustAddrID", "tarCustAddr", moDDData, sSOShipToAddress
        'Insert sorts common to both reports
        moSort.lInsSort "CustID", "tsoPendShipment", moDDData
        moSort.lInsSort "CustName", "tarCustomer", moDDData
        
        'Set the sort info for sorts in use
        If lRow(2) > 0 Then
            lRet = moSort.lSetRow(lRow(2), sTableColumnName(2), sDescription(2), iSub(2), iPageBreak(2), iDescending(2), sColumnName(2), iSeqNo(2), sTableName(2), sParentTableName(2))
        End If
        
        If lRow(3) > 0 Then
            lRet = moSort.lSetRow(lRow(3), sTableColumnName(3), sDescription(3), iSub(3), iPageBreak(3), iDescending(3), sColumnName(3), iSeqNo(3), sTableName(3), sParentTableName(3))
        End If
                    
        If lRow(4) > 0 Then
            lRet = moSort.lSetRow(lRow(4), sTableColumnName(4), sDescription(4), iSub(4), iPageBreak(4), iDescending(4), sColumnName(4), iSeqNo(4), sTableName(4), sParentTableName(4))
        End If
        
        If lRow(5) > 0 Then
            lRet = moSort.lSetRow(lRow(5), sTableColumnName(5), sDescription(5), iSub(5), iPageBreak(5), iDescending(5), sColumnName(5), iSeqNo(5), sTableName(5), sParentTableName(5))
        End If
        
        If lRow(6) > 0 Then
            lRet = moSort.lSetRow(lRow(6), sTableColumnName(6), sDescription(6), iSub(6), iPageBreak(6), iDescending(6), sColumnName(6), iSeqNo(6), sTableName(6), sParentTableName(6))
        End If
        
        ProcessRegister
    
    End If
    
    'Second report to run is Customer Returns - set it up here
    miReport = Report.CustReturns
    miBatchType = BATCH_TYPE_CUSTRET
    SetupWorkTables
                        
    moSort.CleanSortGrid
    
    With grdSelectTrans
        .Row = kRowTranTypeCustRtrn
        .Col = kColCommit
        iChecked = giGetValidInt(.Value)
    End With

    If iChecked = vbChecked Then
    
        'Initialize the report
        If (moReport.lInitReport("SO", "sozdv001.RPT", Me, moDDData) = kFailure) Then
            Debug.Print "failure"
        End If
            
        'Initialize the sort
        If (moSort.lInitSort(Me, moDDData, , , True) = kFailure) Then
            Debug.Print "failure"
        End If
        
        'Insert sorts for Customer Returns
        'Specific to this report
        moSort.lInsSort "TranDate", "tsoPendShipment", moDDData, "Return Date"
        moSort.lInsSort "RMANo", "tsoPendShipment", moDDData, "RMA"
        'Common to both reports
        moSort.lInsSort "CustID", "tsoPendShipment", moDDData
        moSort.lInsSort "CustName", "tarCustomer", moDDData
        
        'Set the sort options save earlier
        If lRow(0) > 0 Then
            lRet = moSort.lSetRow(lRow(0), sTableColumnName(0), sDescription(0), iSub(0), iPageBreak(0), iDescending(0), sColumnName(0), iSeqNo(0), sTableName(0), sParentTableName(0))
        End If
        
        If lRow(1) > 0 Then
            lRet = moSort.lSetRow(lRow(1), sTableColumnName(1), sDescription(1), iSub(1), iPageBreak(1), iDescending(1), sColumnName(1), iSeqNo(1), sTableName(1), sParentTableName(1))
        End If
        
        If lRow(5) > 0 Then
            lRet = moSort.lSetRow(lRow(5), sTableColumnName(5), sDescription(5), iSub(5), iPageBreak(5), iDescending(5), sColumnName(5), iSeqNo(5), sTableName(5), sParentTableName(5))
        End If
        
        If lRow(6) > 0 Then
            lRet = moSort.lSetRow(lRow(6), sTableColumnName(6), sDescription(6), iSub(6), iPageBreak(6), iDescending(6), sColumnName(6), iSeqNo(6), sTableName(6), sParentTableName(6))
        End If
    
        ProcessRegister
    
    End If
    

    'Restore the sort grid to the original state when user pressed print button
    'Clear the grid
    moSort.CleanSortGrid
                
    'Initialize the report
    If (moReport.lInitReport("SO", "sozdt001.RPT", Me, moDDData) = kFailure) Then
        Debug.Print "failure"
    End If
    
    'Initialize the sort object
    If (moSort.lInitSort(Me, moDDData, , , True) = kFailure) Then
        Debug.Print "failure"
    End If
    
    'Insert the sorts
    moSort.lInsSort "ShipDate", "tsoShipLine", moDDData, sSOShipDate
    moSort.lInsSort "ShipMethKey", "tsoPendShipment", moDDData
    moSort.lInsSort "CustAddrID", "tarCustAddr", moDDData, sSOShipToAddress
    moSort.lInsSort "TranDate", "tsoPendShipment", moDDData, "Return Date"
    moSort.lInsSort "RMANo", "tsoPendShipment", moDDData, "RMA"
    moSort.lInsSort "CustID", "tsoPendShipment", moDDData
    moSort.lInsSort "CustName", "tarCustomer", moDDData
    
    'Restore the original sort properties in the grid
    If lRow(0) > 0 Then
        lRet = moSort.lSetRow(lRow(0), sTableColumnName(0), sDescription(0), iSub(0), iPageBreak(0), iDescending(0), sColumnName(0), iSeqNo(0), sTableName(0), sParentTableName(0))
    End If
    
    If lRow(1) > 0 Then
        lRet = moSort.lSetRow(lRow(1), sTableColumnName(1), sDescription(1), iSub(1), iPageBreak(1), iDescending(1), sColumnName(1), iSeqNo(1), sTableName(1), sParentTableName(1))
    End If
                
    If lRow(2) > 0 Then
        lRet = moSort.lSetRow(lRow(2), sTableColumnName(2), sDescription(2), iSub(2), iPageBreak(2), iDescending(2), sColumnName(2), iSeqNo(2), sTableName(2), sParentTableName(2))
    End If
    
    If lRow(3) > 0 Then
        lRet = moSort.lSetRow(lRow(3), sTableColumnName(3), sDescription(3), iSub(3), iPageBreak(3), iDescending(3), sColumnName(3), iSeqNo(3), sTableName(3), sParentTableName(3))
    End If
                
    If lRow(4) > 0 Then
        lRet = moSort.lSetRow(lRow(4), sTableColumnName(4), sDescription(4), iSub(4), iPageBreak(4), iDescending(4), sColumnName(4), iSeqNo(4), sTableName(4), sParentTableName(4))
    End If
    
    If lRow(5) > 0 Then
        lRet = moSort.lSetRow(lRow(5), sTableColumnName(5), sDescription(5), iSub(5), iPageBreak(5), iDescending(5), sColumnName(5), iSeqNo(5), sTableName(5), sParentTableName(5))
    End If
    
    If lRow(6) > 0 Then
        lRet = moSort.lSetRow(lRow(6), sTableColumnName(6), sDescription(6), iSub(6), iPageBreak(6), iDescending(6), sColumnName(6), iSeqNo(6), sTableName(6), sParentTableName(6))
    End If
        
    'save last setting keys for Each Selection
    With grdSelectTrans
        
        .Row = kRowTranTypeCustShip
        .Col = kColCommit
        If .Value = 1 Then
            .Col = kColSetting
            m_CustShipSelectSetting.LastSettingKey = m_CustShipSelectSetting.SettingKey(.Text)
        End If
        
       .Row = kRowTranTypeTrnsfrShip
        .Col = kColCommit
        If .Value = 1 Then
            .Col = kColSetting
            m_TransferShipSelectSetting.LastSettingKey = m_TransferShipSelectSetting.SettingKey(.Text)
        End If
        
        .Row = kRowTranTypeCustRtrn
        .Col = kColCommit
        If .Value = 1 Then
            .Col = kColSetting
            m_CustReturnShipSelectSetting.LastSettingKey = m_CustReturnShipSelectSetting.SettingKey(.Text)
        End If
       
    End With
        
    mbProcessing = False
    
    Screen.MousePointer = vbArrow

End Function

Public Sub HandleToolBarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
Dim ret As Integer
Dim iPostStatus As Integer
Dim bGood As Boolean
Dim lRetVal As Long
Dim i As Integer

    Select Case sKey
        Case kTbPrint, kTbPreview
            msPrintButton = sKey
            PrintReports
        
        Case kTbSave
            moSettings.ReportSettingsSaveAs
        
        Case kTbDelete
            moSettings.ReportSettingsDelete
            
        Case kTbHelp
            gDisplayFormLevelHelp Me
        
        Case Else
    
    End Select
            
Cleanup:
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "HandleToolBarClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub


Public Function lSetBatchType(ByVal iReport As Integer, Optional vFormLoad As Variant) As Long
    Dim sSQL        As String
    Dim iBatchType  As Integer
    Dim rs          As Object
    Dim bFormLoad   As Boolean  ' func is called from formload
    Dim lRetVal     As Long     ' return value form report initialization

    lSetBatchType = kFailure
    On Error GoTo ExpectedErrorRoutine

    bFormLoad = False
    If Not IsMissing(vFormLoad) Then
        bFormLoad = CBool(vFormLoad)
    End If

    If miProcessType = 1 Then
        If Not bFormLoad Then
             sSQL = "SELECT BatchType FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
            
             Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
             If rs Is Nothing Then Exit Function
             If rs.IsEmpty Then
                 rs.Close
                 Set rs = Nothing
                 Exit Function
             End If
             
             iBatchType = rs.Field("BatchType")
                     
             Select Case iBatchType
                 Case kBatchTypeShip
                    miReport = Report.ShipRegister
                 Case kBatchTypeCustR
                    miReport = Report.CustReturns
             End Select
            
             rs.Close
             Set rs = Nothing
        End If
    Else
        miReport = iReport
    End If

    Set sRealTableCollection = New Collection
        sRealTableCollection.Add "tsoPendShipment"

    Set sWorkTableCollection = New Collection
        
    If miReport = Report.ShipRegister Then
        miBatchType = BATCH_TYPE_SHIP               'shipment is defined as 1
    ElseIf miReport = Report.CustReturns Then
        miBatchType = BATCH_TYPE_CUSTRET
    End If

    SetupWorkTables

    If iReport = Report.ShipRegister Then
        lRetVal = lInitializeReport("SO", "sozdt001.RPT", "sozdtdl1")  ' default report name
    ElseIf iReport = Report.CustReturns Then
        lRetVal = lInitializeReport("SO", "sozdv001.RPT", "sozdtdl1")  ' default report name
    End If
    
    If lRetVal <> 0 Then
        moReport.ReportError lRetVal
        Exit Function
    End If
   
    lSetBatchType = kSuccess
   
    Exit Function

ExpectedErrorRoutine:
gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lSetBatchType", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function sGetBatchID(frm As Form) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object

    sGetBatchID = ""
    
    sSQL = "SELECT BatchID FROM tciBatchLog WHERE BatchKey = " & Str(mlBatchKey)
    Set rs = frm.oClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        sGetBatchID = rs.Field("BatchID")
    End If
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetBatchID", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function RecordsToPrint(sp As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    Dim rs As Object
    Dim sTable As String
    Dim iCount As Long
    
    If StrComp(sp, "spsoShippingReg", vbTextCompare) = 0 Then
        sTable = "#tsoShipRegSumWrk"
    ElseIf StrComp(sp, "spsoCustRetReg", vbTextCompare) = 0 Then
        sTable = "#tsoCustRRegSumWrk"
    Else
        Exit Function
    End If
    
    sSQL = "SELECT TOP 1 1 FROM " & sTable
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        RecordsToPrint = True
    End If
    
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "RecordsToPrint", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Function iLogError(lBatchKey As Long, iStringNo As Long, s1 As String, _
    s2 As String, s3 As String, s4 As String, s5 As String, iErrorType As Integer, _
    iSeverity As Integer) As Integer
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String
Dim rs As Object
Dim lRet As Long
Dim iEntryNo As Integer

    iLogError = 0
    
    'first get max entry no for this batch
    sSQL = "SELECT MAX(EntryNo) MaxEntryNo FROM tciErrorLog WHERE SessionID = " & Str(mlBatchKey)
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        If Not IsNull(rs.Field("MaxEntryNo")) Then
        iEntryNo = rs.Field("MaxEntryNo") + 1
        Else
            iEntryNo = 1
        End If
    End If
    
    Set rs = Nothing
     
    'Run stored proc to populate tciErrorLog table
      With moClass.moAppDB
          .SetInParam lBatchKey
          .SetInParam iEntryNo
          .SetInParam iStringNo
          .SetInParam s1
          .SetInParam s2
          .SetInParam s3
          .SetInParam s4
          .SetInParam s5
          .SetInParam iErrorType
          .SetInParam iSeverity
          
          .SetOutParam lRet
          .ExecuteSP ("spglCreateErrSuspenseLog")
          lRet = .GetOutParam(11)
          .ReleaseParams
      End With
     
    iLogError = lRet
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "iLogError", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "frmRegister"
End Function

Private Function lValidateReportPath(sPath As String) As Long
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim iAttr As Integer
On Error GoTo ExpectedErrorRoutine

    lValidateReportPath = kFailure
    
    If Not Len(sPath) > 0 Then
        GoTo ExpectedErrorRoutine
    Else
        If Right(msSOReportPath, 1) = BACKSLASH Then
            iAttr = GetAttr(Mid(sPath, 1, Len(sPath) - 1)) And vbDirectory
        Else
            iAttr = GetAttr(sPath) And vbDirectory
            sPath = sPath & BACKSLASH
        End If
        If iAttr <> vbDirectory Then
            MsgBoxDCD "Report Path: " & sPath & " is not a valid directory on this workstation;" & vbCr & " may not exist on this client workstation", vbCritical, "frmRegister.lValidateReportPath"
            GoTo ExpectedErrorRoutine
        End If
    End If

ExpectedErrorRoutine:
    gClearSotaErr
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
    Exit Function
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "lValidateReportPath", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Public Property Get oreport() As clsReportEngine
'+++ VB/Rig Skip +++
    Set oreport = moReport
End Property

Public Property Set oreport(oClass As clsReportEngine)
'+++ VB/Rig Skip +++
    Set moReport = oClass
End Property

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Let bLoadSuccess(bNewLoadSuccess As Boolean)
'+++ VB/Rig Skip +++
    mbLoadSuccess = bNewLoadSuccess
End Property

Public Property Get bCancelShutDown() As Boolean
'+++ VB/Rig Skip +++
  bCancelShutDown = mbCancelShutDown
End Property

Public Property Let bCancelShutDown(bCancel As Boolean)
'+++ VB/Rig Skip +++
    mbCancelShutDown = bCancel
End Property

Public Function lExecSP(spName As String, colInSPParams As Collection, ByRef colOutSPParams As Collection) As Long
    '*****************************************************************
    'This function is used to execute a stored procedure based on the
    'Parms passed to out for the SP name, ParmIN/Out
    '*****************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
        
Dim iLoop As Integer
Dim iNoOfInParams As Integer
Dim lRetVal As Long
'run register stored procedure to create work table and tglPosting table rows
  On Error GoTo ExpectedErrorRoutine

    iNoOfInParams = colInSPParams.Count
    
    With moClass.moAppDB
        ' set the "in" parameters from the "in" collection
        For iLoop = 1 To colInSPParams.Count
            .SetInParam colInSPParams.Item(iLoop)
        Next iLoop
        
        .SetOutParam lRetVal
        
        ' run the stored procedure
        .ExecuteSP (spName)
        
        While .IsExecuting = True
            DoEvents
        Wend
        
        'Next iLoop
        lRetVal = .GetOutParam(iNoOfInParams + 1)
        
        .ReleaseParams
    End With
    
    ' set the return value of this function to the last out param value
    lExecSP = lRetVal 'colOutSPParams.Item(colOutSPParams.Count)

'+++ VB/Rig Begin Pop +++
        Exit Function
        
ExpectedErrorRoutine:

    MsgBoxDCD Err.Description, vbOKOnly + vbCritical, "Stored Procedure: " & spName
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "lPostModuleTrans", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
Private Sub picDrag_Paint(Index As Integer)

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++

End Sub
#End If

Private Sub Form_Activate()

'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine:
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If
#End If

    grdSelectTrans.Refresh

    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub

VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
    Select Case VBRIG_IS_FORM_EVENT
        Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
        Case Else
            Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++


End Sub

#If CUSTOMIZER And CONTROLS Then

Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
               Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If

Public Property Get MyApp() As Object
    Set MyApp = App
End Property

Public Property Get MyForms() As Object
    Set MyForms = Forms
End Property

Private Sub CreateTempTableCustShipSelect()
    Dim sSQL As String
    
    sSQL = "IF OBJECT_ID('tempdb.." & kCustShipTmpTblName & "') IS NOT NULL" & vbCrLf & _
                " TRUNCATE TABLE " & kCustShipTmpTblName & vbCrLf & _
            " ELSE BEGIN " & vbCrLf & _
                " CREATE TABLE " & kCustShipTmpTblName & " (" & vbCrLf & _
                " ShipmentKey INTEGER       NOT NULL, " & vbCrLf & _
                " Shipment VARCHAR(13)      NOT NULL," & vbCrLf & _
                " ShipDate DATETIME         NOT NULL, " & vbCrLf & _
                " ShipWhse VARCHAR(6)       NULL, " & vbCrLf & _
                " ShipStatus VARCHAR(80)    NULL, " & vbCrLf & _
                " GLBatchNo INTEGER         NULL, " & vbCrLf & _
                " PostDate DATETIME         NULL, " & vbCrLf & _
                " CustID VARCHAR(12)        NULL, " & vbCrLf & _
                " CustName VARCHAR(40)      NULL, " & vbCrLf & _
                " AddrName VARCHAR(40)      NULL, " & vbCrLf & _
                " ShipVia VARCHAR(15)       NULL, " & vbCrLf & _
                " PackUserID VARCHAR(30)    NULL, " & vbCrLf & _
                " ShipUserID VARCHAR(30)    NULL " & vbCrLf & _
                ")" & vbCrLf & _
                " CREATE CLUSTERED INDEX " & kCustShipTmpTblName & "_ind_cls ON " & kCustShipTmpTblName & " (ShipmentKey)" & _
            " END"
            
            
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub

Private Sub SetupCustShipmentSelect()
    'Sale Shipment Selection/Setting setup
    
    Dim oGridCol As clsGridCol
    Dim sSQL As String
    
    CreateTempTableCustShipSelect

    Set m_CustShipSelectColMgr = New clsSelectGridColMgr
    With m_CustShipSelectColMgr
        'need a restrict for Lookups --)
        .Init moClass.moAppDB, msCompanyID
        .ManualSaveLastSetting = True


        .Add "ShipmentKey", "tsoPendShipment", "ShipKey", "Shipment Key", , False, False, SQL_INTEGER, , SS_CELL_TYPE_INTEGER
        
        Set oGridCol = .Add("Shipment", "tsoPendShipment", "TranID", "Shipment", "CompanyID = " & gsQuoted(msCompanyID) & _
                                                                    " AND TranType = " & kTranTypeSOSH)
        oGridCol.LookupID = "Shipment"
        oGridCol.LookupReturnColumn = "TranID"
        
        .Add "ShipDate", "tsoPendShipment", "TranDate", "Ship Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
                
        sSQL = "SELECT WhseKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
        sSQL = sSQL & " UNION SELECT WhseKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
        
        .Add "ShipWhse", "timWarehouse", "WhseID", "Ship Whse", "WhseKey IN (" & sSQL & ")"
                        
        .Add "ShipStatus", "tsoShipmentLog", "TranStatus", "Shipment Status"
        
        sSQL = "SELECT BatchKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
        sSQL = sSQL & " UNION SELECT BatchKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
                                        
        .Add "GLBatchNo", "tciBatchLog", "BatchNo", "GL Batch No", "BatchKey IN (" & sSQL & ")"
                                        
        .Add "PostDate", "tsoShipment", "PostDate", "Post Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
        
        sSQL = "SELECT CustKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
        sSQL = sSQL & "UNION SELECT CustKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
                                        
        .Add "CustID", "tarCustomer", "CustID", "Customer", "CustKey IN (" & sSQL & ")"
            
        .Add "CustName", "tarCustomer", "CustName", "Customer Name"
        
        sSQL = "Select COALESCE(ShipToCustAddrKey, AddrKey) FROM tsoPendShipment WITH (NOLOCK) " & _
                                    " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                    " AND TranType = " & kTranTypeSOSH
        sSQL = " UNION SELECT COALESCE(ShipToCustAddrKey, AddrKey) FROM tsoShipment WITH (NOLOCK) " & _
                                    " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                    " AND TranType = " & kTranTypeSOSH
                                        
        .Add "AddrName", "tciAddress", "AddrName", "Customer Ship To Address", "AddrKey IN (" & sSQL & ")"
                        
        .Add "CustPONo", "tsoSalesOrder", "CustPONo", "Customer PO No", , , False
        
        sSQL = "SELECT sol.SOKey FROM tsoPendShipment ps WITH (NOLOCK) " & _
                                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                                    " JOIN tsoSOLine   sol  WITH (NOLOCK) ON sol.SOLineKey = shpl.SOLineKey" & _
                                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSOSH
        sSQL = sSQL & " UNION SELECT sol.SOKey FROM tsoShipment ps WITH (NOLOCK) " & _
                                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                                    " JOIN tsoSOLine   sol  WITH (NOLOCK) ON sol.SOLineKey = shpl.SOLineKey" & _
                                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSOSH

        .Add "SalesOrder", "tsoSalesOrder", "TranNoRelChngOrd", "Sales Order", "SOKey IN (" & sSQL & ")", , False
                        
        sSQL = "SELECT ShipMethKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
        sSQL = " UNION SELECT ShipMethKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOSH
                                        
        .Add "ShipVia", "tciShipMethod", "ShipMethID", "Ship Via", "ShipMethKey IN (" & sSQL & ")"
                        
        .Add "PickListNo", "tsoPickList", "PickListNo", "Pick List No", _
                        "PickListKey IN (SELECT shpl.PickListKey FROM tsoPendShipment ps WITH (NOLOCK) " & _
                                        " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey         " & _
                                        " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSOSH & ")", , False
                        
        .Add "PackUserID", "tsoPendShipment", "PackUserID", "Pack User"
        .Add "ShipUserID", "tsoPendShipment", "CreateUserID", "Ship User"
                
    End With

    Set m_CustShipSelectSetting = New clsSelectionSettings
    m_CustShipSelectSetting.Init moClass.moAppDB, moClass.moSysSession, m_CustShipSelectColMgr, _
                                kAppNameSOCommitCustShip, ktskSOCommitSaleShipSetting
                                
End Sub

Private Sub CreateTempTableTrnsfrShipSelect()
    Dim sSQL As String
    
    sSQL = "IF OBJECT_ID('tempdb.." & kTrnsfrShipTmpTblName & "') IS NOT NULL" & vbCrLf & _
                " TRUNCATE TABLE " & kTrnsfrShipTmpTblName & vbCrLf & _
            " ELSE BEGIN " & vbCrLf & _
                " CREATE TABLE " & kTrnsfrShipTmpTblName & " (" & vbCrLf & _
                " ShipmentKey INTEGER       NOT NULL," & vbCrLf & _
                " Shipment VARCHAR(13)      NOT NULL," & vbCrLf & _
                " ShipDate DATETIME         NOT NULL," & vbCrLf & _
                " ShipWhse VARCHAR(6)       NULL, " & vbCrLf & _
                " RcvgWhse VARCHAR(6)       NULL, " & vbCrLf & _
                " ShipStatus VARCHAR(80)    NULL, " & vbCrLf & _
                " GLBatchNo INTEGER         NULL, " & vbCrLf & _
                " PostDate DATETIME         NULL, " & vbCrLf & _
                " ShipVia VARCHAR(15)       NULL, " & vbCrLf & _
                " PackUserID VARCHAR(30)    NULL, " & vbCrLf & _
                " ShipUserID VARCHAR(30)    NULL  " & vbCrLf & _
                ")" & vbCrLf & _
                " CREATE CLUSTERED INDEX " & kTrnsfrShipTmpTblName & "_ind_cls ON " & kTrnsfrShipTmpTblName & " (ShipmentKey)" & _
            " END"
            
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub


Private Sub SetupTransferShipmentSelect()
    'Sale Shipment Selection/Setting setup
    
    Dim oGridCol As clsGridCol
    Dim sSQL As String

    CreateTempTableTrnsfrShipSelect
    
    Set m_TransferShipSelectColMgr = New clsSelectGridColMgr
    With m_TransferShipSelectColMgr
        'need a restrict for Lookups --)
        .Init moClass.moAppDB, msCompanyID
        .ManualSaveLastSetting = True
        
        .Add "ShipmentKey", "tsoPendShipment", "ShipKey", "Shipment Key", , False, False, SQL_INTEGER, , SS_CELL_TYPE_INTEGER
        Set oGridCol = .Add("Shipment", "tsoPendShipment", "TranID", "Shipment", "CompanyID = " & gsQuoted(msCompanyID) & _
                                                                    " AND TranType = " & kTranTypeSOTS)
        oGridCol.LookupID = "Shipment"
        oGridCol.LookupReturnColumn = "TranID"
        
        .Add "ShipDate", "tsoPendShipment", "TranDate", "Ship Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
                
        sSQL = "SELECT WhseKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        sSQL = sSQL & " UNION SELECT WhseKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        .Add "ShipWhse", "timWarehouseShip", "WhseID", "Ship Whse", "WhseKey IN (" & sSQL & ")"
                                        
        sSQL = "SELECT RcvgWhseKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        sSQL = sSQL & " UNION SELECT RcvgWhseKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        .Add "RcvgWhse", "timWarehouseRcvg", "WhseID", "Rcvg Whse", "WhseKey IN (" & sSQL & ")"
        
        .Add "ShipStatus", "tsoShipmentLog", "TranStatus", "Shipment Status"
        
        sSQL = "SELECT BatchKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        sSQL = sSQL & " UNION SELECT BatchKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        .Add "GLBatchNo", "tciBatchLog", "BatchNo", "GL Batch No", "BatchKey IN (" & sSQL & ")"
        
        .Add "PostDate", "tsoShipment", "PostDate", "Post Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
            
        sSQL = "SELECT ShipMethKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        sSQL = sSQL & " UNION SELECT ShipMethKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSOTS
        .Add "ShipVia", "tciShipMethod", "ShipMethID", "Ship Via", "ShipMethKey IN (" & sSQL & ")"
        
        sSQL = "SELECT tol.TrnsfrOrderKey FROM tsoPendShipment ps WITH (NOLOCK) " & _
                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                    " JOIN timTrnsfrOrderLine   tol  WITH (NOLOCK) ON tol.TrnsfrOrderLineKey = shpl.TrnsfrOrderLineKey" & _
                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                    " AND ps.TranType = " & kTranTypeSOTS
        sSQL = sSQL & " UNION SELECT tol.TrnsfrOrderKey FROM tsoShipment ps WITH (NOLOCK) " & _
                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                    " JOIN timTrnsfrOrderLine   tol  WITH (NOLOCK) ON tol.TrnsfrOrderLineKey = shpl.TrnsfrOrderLineKey" & _
                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                    " AND ps.TranType = " & kTranTypeSOTS
                                    
        Set oGridCol = .Add("TrnsfrOrder", "timTrnsfrOrder", "TranNo", "Transfer Order", "TrnsfrOrderKey IN (" & sSQL & ")", , False)
         
        oGridCol.LookupID = "TransferOrder"
        oGridCol.LookupReturnColumn = "TranNo"
                                                 
        sSQL = "SELECT shpl.PickListKey FROM tsoPendShipment ps WITH (NOLOCK) " & _
                                        " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey         " & _
                                        " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSOTS
        sSQL = sSQL & " UNION SELECT shpl.PickListKey FROM tsoShipment ps WITH (NOLOCK) " & _
                                        " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey         " & _
                                        " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSOTS
                                        
        .Add "PickListNo", "tsoPickList", "PickListNo", "Pick List No", "PickListKey IN (" & sSQL & ")", , False
                        
        .Add "PackUserID", "tsoPendShipment", "PackUserID", "Pack User"
        .Add "ShipUserID", "tsoPendShipment", "CreateUserID", "Ship User"
                
    End With
    
    Set m_TransferShipSelectSetting = New clsSelectionSettings
    m_TransferShipSelectSetting.Init moClass.moAppDB, moClass.moSysSession, m_TransferShipSelectColMgr, _
                                kAppNameSOCommitTransferShip, ktskSOCommitTransferShipSetting
    
End Sub

Private Sub CreateTempTableCustRtrnShipSelect()
    Dim sSQL As String
    
    sSQL = "IF OBJECT_ID('tempdb.." & kCustRtrnShipTmpTblName & "') IS NOT NULL" & vbCrLf & _
                " TRUNCATE TABLE " & kCustRtrnShipTmpTblName & vbCrLf & _
            " ELSE BEGIN " & vbCrLf & _
                " CREATE TABLE " & kCustRtrnShipTmpTblName & " (" & vbCrLf & _
                " ShipmentKey INTEGER       NOT NULL," & vbCrLf & _
                " ReturnID VARCHAR(13)      NOT NULL," & vbCrLf & _
                " RMANo VARCHAR(10)         NULL," & vbCrLf & _
                " ReturnDate DATETIME       NULL," & vbCrLf & _
                " ReturnWhse VARCHAR(6)     NULL," & vbCrLf & _
                " ShipStatus VARCHAR(80)    NULL, " & vbCrLf & _
                " GLBatchNo INTEGER         NULL, " & vbCrLf & _
                " PostDate DATETIME         NULL, " & vbCrLf & _
                " CustID VARCHAR(12)        NULL," & vbCrLf & _
                " CustName VARCHAR(40)      NULL," & vbCrLf & _
                " AddrName VARCHAR(40)      NULL," & vbCrLf & _
                " ReturnUserID VARCHAR(30)  NULL " & vbCrLf & _
                ")" & vbCrLf & _
                " CREATE CLUSTERED INDEX " & kCustRtrnShipTmpTblName & "_ind_cls ON " & kCustRtrnShipTmpTblName & " (ShipmentKey)" & _
            " END"
            
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub


Private Sub SetupCustRtrnShipmentSelect()
    'Sale Shipment Selection/Setting setup
    Dim oGridCol As clsGridCol
    Dim sSQL As String
    
    CreateTempTableCustRtrnShipSelect
    
    Set m_CustReturnShipSelectColMgr = New clsSelectGridColMgr
    With m_CustReturnShipSelectColMgr
        'need a restrict for Lookups --)
        .Init moClass.moAppDB, msCompanyID
        .ManualSaveLastSetting = True

        .Add "ShipmentKey", "tsoPendShipment", "ShipKey", "Shipment Key", , False, False, SQL_INTEGER, , SS_CELL_TYPE_INTEGER
        Set oGridCol = .Add("ReturnID", "tsoPendShipment", "TranID", "Return", "CompanyID = " & gsQuoted(msCompanyID) & _
                                                                " AND TranType = " & kTranTypeSORT)
                                                                
         oGridCol.LookupID = "CustomerReturn"
         oGridCol.LookupReturnColumn = "TranID"
        
        sSQL = "SELECT crs.RMAKey FROM tsoCustRtrnShipment crs WITH (NOLOCK) " & _
                            " JOIN tsoPendShipment ps WITH (NOLOCK) ON ps.ShipKey = crs.ShipKey " & _
                            " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                            " AND ps.TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT crs.RMAKey FROM tsoCustRtrnShipment crs WITH (NOLOCK) " & _
                            " JOIN tsoShipment ps WITH (NOLOCK) ON ps.ShipKey = crs.ShipKey " & _
                            " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                            " AND ps.TranType = " & kTranTypeSORT
                            
        .Add "RMANo", "tsoRMA", "TranNo", "RMA", "RMAKey IN (" & sSQL & ")"

        .Add "ReturnDate", "tsoPendShipment", "TranDate", "Return Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
                
        sSQL = "SELECT RcvgWhseKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT RcvgWhseKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        .Add "ReturnWhse", "timWarehouse", "WhseID", "Return Whse", "WhseKey IN (" & sSQL & ")"
        
        .Add "ShipStatus", "tsoShipmentLog", "TranStatus", "Return Status"
        
        sSQL = "SELECT BatchKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT BatchKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        .Add "GLBatchNo", "tciBatchLog", "BatchNo", "GL Batch No", "BatchKey IN (" & sSQL & ")"
        
        .Add "PostDate", "tsoShipment", "PostDate", "Post Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
            
        sSQL = "SELECT CustKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT CustKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        .Add "CustID", "tarCustomer", "CustID", "Customer", "CustKey IN (" & sSQL & ")"
            
        .Add "CustName", "tarCustomer", "CustName", "Customer Name"
        
        sSQL = "SELECT COALESCE(ShipToCustAddrKey, AddrKey) FROM tsoPendShipment WITH (NOLOCK) " & _
                                    " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT COALESCE(ShipToCustAddrKey, AddrKey) FROM tsoShipment WITH (NOLOCK) " & _
                                    " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        .Add "AddrName", "tciAddress", "AddrName", "Customer Ship To Address", "AddrKey IN (" & sSQL & ")"
                        
        sSQL = "SELECT sol.SOKey FROM tsoPendShipment ps WITH (NOLOCK) " & _
                                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                                    " JOIN tsoSOLine   sol  WITH (NOLOCK) ON sol.SOLineKey = shpl.SOLineKey" & _
                                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT sol.SOKey FROM tsoShipment ps WITH (NOLOCK) " & _
                                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                                    " JOIN tsoSOLine   sol  WITH (NOLOCK) ON sol.SOLineKey = shpl.SOLineKey" & _
                                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSORT
                                        
        .Add "SalesOrder", "tsoSalesOrder", "TranNoRelChngOrd", "Sales Order", "SOKey IN (" & sSQL & ")", , False
                        
        sSQL = "SELECT ShipMethKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        sSQL = sSQL & " UNION SELECT ShipMethKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSORT
        .Add "ShipVia", "tciShipMethod", "ShipMethID", "Ship Via", "ShipMethKey IN (" & sSQL & ")", , False
                                        
        .Add "ReturnUserID", "tsoPendShipment", "CreateUserID", "Return User"
        
    End With
    
    Set m_CustReturnShipSelectSetting = New clsSelectionSettings
    m_CustReturnShipSelectSetting.Init moClass.moAppDB, moClass.moSysSession, m_CustReturnShipSelectColMgr, _
                                kAppNameSOCommitCustomerReturnsShip, ktskSOCommitCustomerReturnsSetting

End Sub

Private Function GetRowFromTranType(ByVal lTranType As Long) As Long
    Select Case lTranType
    
        Case kTranTypeSOSH
            GetRowFromTranType = kRowTranTypeCustShip
            
        Case kTranTypeSODS
            GetRowFromTranType = kRowTranTypeDropShip
            
        Case kTranTypeSOTS
            GetRowFromTranType = kRowTranTypeTrnsfrShip
            
        Case kTranTypeSORT
            GetRowFromTranType = kRowTranTypeCustRtrn
            
    End Select
    
End Function

Private Sub InitSelectTransCustomerShipment()
    'If creating more than one of these, then use the collection to to differentiate
    
    If m_SelectTransCustShip Is Nothing Then
        
        Set m_SelectTransCustShip = New clsCISelectTrans
        
        With m_SelectTransCustShip
        
            Set .moAppDB = moClass.moAppDB
            Set .moFramework = moClass.moFramework
            Set .moSysSession = moClass.moSysSession
            Set .moDasSession = moClass.moDasSession
            grdSelectTrans.Col = kColTranTypeDesc
            grdSelectTrans.Row = kRowTranTypeCustShip
            .Caption = "Select " & Trim(grdSelectTrans.Text)
            .InitSelectTrans m_CustShipSelectColMgr, ktskSOCommitSaleShipSetting, kAppNameSOCommitCustShip, kCustShipTmpTblName
            
        End With
        
    End If
    
End Sub

Private Sub LoadCustShipSelectTmpTbl()
    Dim sSQL As String
    
    sSQL = " INSERT " & kCustShipTmpTblName & "(" & vbCrLf & _
                " ShipmentKey " & vbCrLf & _
                ",Shipment " & vbCrLf & _
                ",ShipDate " & vbCrLf & _
                ",ShipWhse " & vbCrLf & _
                ",ShipStatus " & vbCrLf & _
                ",GLBatchNo " & vbCrLf & _
                ",PostDate " & vbCrLf & _
                ",CustID" & vbCrLf & _
                ",CustName " & vbCrLf & _
                ",AddrName " & vbCrLf & _
                ",ShipVia " & vbCrLf & _
                ",PackUserID " & vbCrLf & _
                ",ShipUserID " & vbCrLf & _
                ")"

                
    sSQL = sSQL & " SELECT DISTINCT" & _
            " tsoPendShipment.ShipKey       " & _
            ",tsoPendShipment.TranID        " & _
            ",tsoPendShipment.TranDate      " & _
            ",timWarehouse.WhseID           " & _
            ",tsoShipmentLog.TranStatus     " & _
            ",tciBatchLog.BatchNo           " & _
            ",tsoShipment.PostDate          " & _
            ",tarCustomer.CustID            " & _
            ",tarCustomer.CustName          " & _
            ",tciAddress.AddrName           " & _
            ",tciShipMethod.ShipMethID      " & _
            ",tsoPendShipment.PackUserID    " & _
            ",tsoPendShipment.CreateUserID  " & vbCrLf
            
    sSQL = sSQL & "FROM " & TmpTblSOCommitName & vbCrLf & _
            " JOIN                tsoPendShipment   WITH (NOLOCK)       ON tsoPendShipment.ShipKey = " & TmpTblSOCommitName & ".TranKey" & vbCrLf & _
            " LEFT OUTER JOIN     timWarehouse      WITH (NOLOCK)       ON timWarehouse.WhseKey         = tsoPendShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN     tarCustomer       WITH (NOLOCK)       ON tarCustomer.CustKey          = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciAddress        WITH (NOLOCK)       ON tciAddress.AddrKey           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipLine       WITH (NOLOCK)       ON tsoShipLine.ShipKey          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSOLine         WITH (NOLOCK)       ON tsoSOLine.SOLineKey          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSalesOrder     WITH (NOLOCK)       ON tsoSalesOrder.SOKey          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciShipMethod     WITH (NOLOCK)       ON tciShipMethod.ShipMethKey    = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoPickList       WITH (NOLOCK)       ON tsoPickList.PickListKey      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipmentLog    WITH (NOLOCK)       ON tsoShipmentLog.ShipKey       = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciBatchLog       WITH (NOLOCK)       ON tciBatchLog.BatchKey         = tsoPendShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipment       WITH (NOLOCK)       ON tsoShipment.ShipKey          = tsoPendShipment.ShipKey" & vbCrLf

    sSQL = sSQL & " WHERE " & TmpTblSOCommitName & ".TranType = " & kTranTypeSOSH & vbCrLf

    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub


Private Sub InitSelectTransTrnsfrShipment()
    'If creating more than one of these, then use the collection to to differentiate
    
    If m_SelectTransTrnsfrShip Is Nothing Then
        
        Set m_SelectTransTrnsfrShip = New clsCISelectTrans
        
        With m_SelectTransTrnsfrShip
        
            Set .moAppDB = moClass.moAppDB
            Set .moFramework = moClass.moFramework
            Set .moSysSession = moClass.moSysSession
            Set .moDasSession = moClass.moDasSession
            grdSelectTrans.Col = kColTranTypeDesc
            grdSelectTrans.Row = kRowTranTypeTrnsfrShip
            .Caption = "Select " & Trim(grdSelectTrans.Text)
            .InitSelectTrans m_TransferShipSelectColMgr, ktskSOCommitTransferShipSetting, kAppNameSOCommitTransferShip, kTrnsfrShipTmpTblName
            
        End With
        
    End If
    
End Sub


Private Sub LoadCustRtrnSelectTmpTbl()

    Dim sSQL As String
    
    Debug.Print "LoadCustRtrnSelectTmpTbl"

    sSQL = " INSERT " & kCustRtrnShipTmpTblName & "(" & vbCrLf & _
                    " ShipmentKey" & vbCrLf & _
                    ",ReturnID" & vbCrLf & _
                    ",RMANo" & vbCrLf & _
                    ",ReturnDate" & vbCrLf & _
                    ",ReturnWhse" & vbCrLf & _
                    ",ShipStatus " & vbCrLf & _
                    ",GLBatchNo " & vbCrLf & _
                    ",PostDate " & vbCrLf & _
                    ",CustID" & vbCrLf & _
                    ",CustName" & vbCrLf & _
                    ",AddrName" & vbCrLf & _
                    ",ReturnUserID" & vbCrLf & _
                    ")"

   sSQL = sSQL & " SELECT DISTINCT" & _
                    " tsoPendShipment.ShipKey           " & _
                    ",tsoPendShipment.TranID            " & _
                    ",tsoRMA.TranNo                     " & _
                    ",tsoPendShipment.TranDate          " & _
                    ",timWarehouse.WhseID               " & _
                    ",tsoShipmentLog.TranStatus         " & _
                    ",tciBatchLog.BatchNo               " & _
                    ",tsoShipment.PostDate              " & _
                    ",tarCustomer.CustID                " & _
                    ",tarCustomer.CustName              " & _
                    ",tciAddress.AddrName               " & _
                    ",tsoPendShipment.CreateUserID      " & vbCrLf

    sSQL = sSQL & "FROM " & TmpTblSOCommitName & vbCrLf & _
            " JOIN            tsoPendShipment     WITH (NOLOCK) ON tsoPendShipment.ShipKey        = " & TmpTblSOCommitName & ".TranKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoCustRtrnShipment WITH (NOLOCK) ON tsoCustRtrnShipment.ShipKey    = tsoPendShipment.ShipKey " & vbCrLf & _
            " LEFT OUTER JOIN tsoRMA              WITH (NOLOCK) ON tsoRMA.RMAKey                  = tsoCustRtrnShipment.RMAKey" & vbCrLf & _
            " LEFT OUTER JOIN timWarehouse        WITH (NOLOCK) ON timWarehouse.WhseKey           = tsoPendshipment.RcvgWhseKey" & vbCrLf & _
            " LEFT OUTER JOIN tarCustomer         WITH (NOLOCK) ON tarCustomer.CustKey            = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN tciAddress          WITH (NOLOCK) ON tciAddress.AddrKey             = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK) ON tsoShipLine.ShipKey            = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoSOLine           WITH (NOLOCK) ON tsoSOLine.SOLineKey            = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoSalesOrder       WITH (NOLOCK) ON tsoSalesOrder.SOKey            = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK) ON tciShipMethod.ShipMethKey      = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK) ON tsoShipmentLog.ShipKey         = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK) ON tciBatchLog.BatchKey           = tsoPendShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN tsoShipment         WITH (NOLOCK) ON tsoShipment.ShipKey            = tsoPendShipment.ShipKey" & vbCrLf


    sSQL = sSQL & " WHERE " & TmpTblSOCommitName & ".TranType = " & kTranTypeSORT & vbCrLf
    
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub


Private Sub InitSelectTransCustRtrnsShipment()
    'If creating more than one of these, then use the collection to to differentiate
    
    If m_SelectTransCustRtrnShip Is Nothing Then
        
        Set m_SelectTransCustRtrnShip = New clsCISelectTrans
        
        With m_SelectTransCustRtrnShip
        
            Set .moAppDB = moClass.moAppDB
            Set .moFramework = moClass.moFramework
            Set .moSysSession = moClass.moSysSession
            Set .moDasSession = moClass.moDasSession
            grdSelectTrans.Col = kColTranTypeDesc
            grdSelectTrans.Row = kRowTranTypeCustRtrn
            .Caption = "Select " & Trim(grdSelectTrans.Text)
            .InitSelectTrans m_CustReturnShipSelectColMgr, ktskSOCommitCustomerReturnsSetting, _
                            kAppNameSOCommitCustomerReturnsShip, kCustRtrnShipTmpTblName
            
        End With
        
    End If
    
End Sub


Private Sub ApplySelectionSetting(ByVal lTranType As Long, ByVal lSettingKey As Long)

    Dim sWhereClause    As String
    Dim oSelectSettings As clsSelectionSettings
    
    Set oSelectSettings = GetSelectSettingsFromTranType(lTranType)
    sWhereClause = oSelectSettings.GetWhereClauseFromSetting(lSettingKey)
    
    Select Case lTranType
        Case kTranTypeSOSH
            SelectCustShipTrans sWhereClause
            
        Case kTranTypeSODS
            SelectDropShipTrans sWhereClause
            
        Case kTranTypeSORT
            SelectCustRtrnTrans sWhereClause
        
        Case kTranTypeSOTS
            SelectTrnsfrShipTrans sWhereClause
           
    End Select
    
    UpdateSOCommitTmpTbl lTranType
    SetEnabledFromSelected lTranType
    
    Set oSelectSettings = Nothing
    
End Sub

Private Function GetSelectSettingsFromTranType(ByVal lTranType As Long) As clsSelectionSettings
    Select Case lTranType
    
        Case kTranTypeSOSH
            Set GetSelectSettingsFromTranType = m_CustShipSelectSetting
            
        Case kTranTypeSODS
            Set GetSelectSettingsFromTranType = m_DropShipSelectSetting
            
        Case kTranTypeSOTS
            Set GetSelectSettingsFromTranType = m_TransferShipSelectSetting
            
        Case kTranTypeSORT
            Set GetSelectSettingsFromTranType = m_CustReturnShipSelectSetting
            
    End Select
    
End Function

Private Sub SelectCustShipTrans(ByVal sWhereClause As String)
    
    Debug.Print "Select Transactions"
    Dim sSQL As String
    Dim rs As Object
    
    sSQL = " INSERT " & kCustShipTmpTblName & "(" & vbCrLf & _
                " ShipmentKey       " & vbCrLf & _
                ",Shipment          " & vbCrLf & _
                ",ShipDate          " & vbCrLf & _
                ",ShipWhse          " & vbCrLf & _
                ",ShipStatus        " & vbCrLf & _
                ",GLBatchNo         " & vbCrLf & _
                ",PostDate          " & vbCrLf & _
                ",CustID            " & vbCrLf & _
                ",CustName          " & vbCrLf & _
                ",AddrName          " & vbCrLf & _
                ",ShipVia           " & vbCrLf & _
                ",PackUserID        " & vbCrLf & _
                ",ShipUserID        " & vbCrLf & _
                ")"

    sSQL = sSQL & " SELECT DISTINCT             " & vbCrLf & _
            " tsoPendShipment.ShipKey           " & vbCrLf & _
            ",tsoPendShipment.TranID            " & vbCrLf & _
            ",tsoPendShipment.TranDate          " & vbCrLf & _
            ",timWarehouse.WhseID               " & vbCrLf & _
            ",tsmLocalString.LocalText          " & vbCrLf & _
            ",tciBatchLog.BatchNo               " & vbCrLf & _
            ",tsoShipment.PostDate              " & vbCrLf & _
            ",tarCustomer.CustID                " & vbCrLf & _
            ",tarCustomer.CustName              " & vbCrLf & _
            ",tciAddress.AddrName               " & vbCrLf & _
            ",tciShipMethod.ShipMethID          " & vbCrLf & _
            ",tsoPendShipment.PackUserID        " & vbCrLf & _
            ",tsoPendShipment.CreateUserID      " & vbCrLf
    
    sSQL = sSQL & "FROM           tsoPendShipment       WITH (NOLOCK) " & vbCrLf & _
            " LEFT OUTER JOIN " & kCustShipTmpTblName & " WITH (NOLOCK) ON " & kCustShipTmpTblName & ".ShipmentKey       = tsoPendShipment.ShipKey " & vbCrLf & _
            " LEFT OUTER JOIN   timWarehouse            WITH (NOLOCK)   ON timWarehouse.WhseKey                         = tsoPendShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarCustomer             WITH (NOLOCK)   ON tarCustomer.CustKey                          = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcctLevel    WITH (NOLOCK)   ON tarNationalAcctLevel.NationalAcctLevelKey    = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcct         WITH (NOLOCK)   ON tarNationalAcct.NationalAcctKey              = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciAddress              WITH (NOLOCK)   ON tciAddress.AddrKey                           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipLine             WITH (NOLOCK)   ON tsoShipLine.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSOLine               WITH (NOLOCK)   ON tsoSOLine.SOLineKey                          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSalesOrder           WITH (NOLOCK)   ON tsoSalesOrder.SOKey                          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciShipMethod           WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                    = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoPickList             WITH (NOLOCK)   ON tsoPickList.PickListKey                      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipmentLog          WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                       = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipment             WITH (NOLOCK)   ON tsoShipment.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciBatchLog             WITH (NOLOCK)   ON tciBatchLog.BatchKey                         = tsoPendShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsmListValidation       WITH (NOLOCK)   ON tsmListValidation.DBValue                    = tsoShipmentLog.TranStatus" & vbCrLf & _
            " LEFT OUTER JOIN   tsmLocalString          WITH (NOLOCK)   ON tsmLocalString.StringNo                      = tsmListValidation.StringNo" & vbCrLf & _
            " LEFT OUTER JOIN   tsmUser                 WITH (NOLOCK)   ON tsmUser.UserID                               = tsoPendShipment.PackUserID" & vbCrLf

    sSQL = sSQL & " WHERE " & kCustShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoPendShipment.TranType = " & kTranTypeSOSH & vbCrLf & _
                " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tarCustomer.Hold     = 0 OR tarCustomer.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.Hold   = 0 OR tsoSalesOrder.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.CrHold = 0 OR tsoSalesOrder.CrHold IS NULL) " & vbCrLf & _
                " AND (tarNationalAcct.Hold = 0 OR tarNationalAcct.Hold IS NULL) " & vbCrLf & _
                " AND (tsmListValidation.TableName = 'tsoShipmentLog') " & vbCrLf & _
                " AND (tsmListValidation.ColumnName = 'TranStatus') " & vbCrLf
 
    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    
    sSQL = sSQL & " UNION " & vbCrLf & _
            " SELECT DISTINCT               " & vbCrLf & _
            " tsoShipment.ShipKey               " & vbCrLf & _
            ",tsoShipment.TranNo                " & vbCrLf & _
            ",tsoShipment.TranDate              " & vbCrLf & _
            ",timWarehouse.WhseID               " & vbCrLf & _
            ",tsmLocalString.LocalText          " & vbCrLf & _
            ",tciBatchLog.BatchNo               " & vbCrLf & _
            ",tsoShipment.PostDate              " & vbCrLf & _
            ",tarCustomer.CustID                " & vbCrLf & _
            ",tarCustomer.CustName              " & vbCrLf & _
            ",tciAddress.AddrName               " & vbCrLf & _
            ",tciShipMethod.ShipMethID          " & vbCrLf & _
            ",tsoShipment.PackUserID            " & vbCrLf & _
            ",tsoShipment.CreateUserID          " & vbCrLf
    
    sSQL = sSQL & "FROM           tsoShipment       WITH (NOLOCK) " & vbCrLf & _
            " LEFT OUTER JOIN " & kCustShipTmpTblName & " WITH (NOLOCK) ON " & kCustShipTmpTblName & ".ShipmentKey      = tsoShipment.ShipKey " & vbCrLf & _
            " LEFT OUTER JOIN   timWarehouse            WITH (NOLOCK)   ON timWarehouse.WhseKey                         = tsoShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarCustomer             WITH (NOLOCK)   ON tarCustomer.CustKey                          = tsoShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcctLevel    WITH (NOLOCK)   ON tarNationalAcctLevel.NationalAcctLevelKey    = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcct         WITH (NOLOCK)   ON tarNationalAcct.NationalAcctKey              = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciAddress              WITH (NOLOCK)   ON tciAddress.AddrKey                           = tsoShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipLine             WITH (NOLOCK)   ON tsoShipLine.ShipKey                          = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSOLine               WITH (NOLOCK)   ON tsoSOLine.SOLineKey                          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSalesOrder           WITH (NOLOCK)   ON tsoSalesOrder.SOKey                          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciShipMethod           WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                    = tsoShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoPickList             WITH (NOLOCK)   ON tsoPickList.PickListKey                      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipmentLog          WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                       = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciBatchLog             WITH (NOLOCK)   ON tciBatchLog.BatchKey                         = tsoShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsmListValidation       WITH (NOLOCK)   ON tsmListValidation.DBValue                    = tsoShipmentLog.TranStatus" & vbCrLf & _
            " LEFT OUTER JOIN   tsmLocalString          WITH (NOLOCK)   ON tsmLocalString.StringNo                      = tsmListValidation.StringNo" & vbCrLf & _
            " LEFT OUTER JOIN   tsmUser                 WITH (NOLOCK)   ON tsmUser.UserID                               = tsoShipment.PackUserID" & vbCrLf

    sSQL = sSQL & " WHERE " & kCustShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoShipment.TranType = " & kTranTypeSOSH & vbCrLf & _
                " AND tsoShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tarCustomer.Hold     = 0 OR tarCustomer.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.Hold   = 0 OR tsoSalesOrder.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.CrHold = 0 OR tsoSalesOrder.CrHold IS NULL) " & vbCrLf & _
                " AND (tarNationalAcct.Hold = 0 OR tarNationalAcct.Hold IS NULL) " & vbCrLf & _
                " AND (tsmListValidation.TableName = 'tsoShipmentLog') " & vbCrLf & _
                " AND (tsmListValidation.ColumnName = 'TranStatus') " & vbCrLf
 
    
    If Len(Trim(sWhereClause)) > 0 Then
        sWhereClause = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    'Get number of Pend Shipments not selected because of  Hold Customers, SalesOrders and NationalAccount

    sSQL = " SELECT COALESCE(COUNT(*),0) AS HoldCount FROM tsoPendShipment  WHERE ShipKey IN (" & _
                    "SELECT DISTINCT tsoPendShipment.ShipKey " & vbCrLf

    sSQL = sSQL & "FROM           tsoPendShipment       WITH (NOLOCK) " & vbCrLf & _
            " LEFT OUTER JOIN   timWarehouse            WITH (NOLOCK)   ON timWarehouse.WhseKey                         = tsoPendShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarCustomer             WITH (NOLOCK)   ON tarCustomer.CustKey                          = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcctLevel    WITH (NOLOCK)   ON tarNationalAcctLevel.NationalAcctLevelKey    = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcct         WITH (NOLOCK)   ON tarNationalAcct.NationalAcctKey              = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciAddress              WITH (NOLOCK)   ON tciAddress.AddrKey                           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipLine             WITH (NOLOCK)   ON tsoShipLine.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSOLine               WITH (NOLOCK)   ON tsoSOLine.SOLineKey                          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSalesOrder           WITH (NOLOCK)   ON tsoSalesOrder.SOKey                          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciShipMethod           WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                    = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoPickList             WITH (NOLOCK)   ON tsoPickList.PickListKey                      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipmentLog          WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                       = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipment             WITH (NOLOCK)   ON tsoShipment.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciBatchLog             WITH (NOLOCK)   ON tciBatchLog.BatchKey                         = tsoPendShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsmUser                 WITH (NOLOCK)   ON tsmUser.UserID                               = tsoPendShipment.PackUserID" & vbCrLf

    sSQL = sSQL & " WHERE tsoPendShipment.TranType = " & kTranTypeSOSH & _
                " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & _
                " AND ((tarCustomer.Hold     = 1 AND  tarCustomer.Hold IS NOT NULL) " & _
                "   OR (tsoSalesOrder.Hold   = 1 AND tsoSalesOrder.Hold IS NOT NULL) " & _
                "   OR (tsoSalesOrder.CrHold = 1 AND tsoSalesOrder.CrHold IS NOT NULL) " & _
                "   OR (tarNationalAcct.Hold = 1 AND tarNationalAcct.Hold IS NOT NULL))"
    
    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    sSQL = sSQL & ")"

    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    If Not rs.IsEmpty Then
        mlHoldCount = mlHoldCount + rs.Field("HoldCount")
    End If
    rs.Close
    Set rs = Nothing
    
End Sub

Private Sub SelectCustRtrnTrans(ByVal sWhereClause As String)

    Debug.Print "Select Transactions"
    Dim sSQL As String
    Dim rs As Object
    Dim sWhereShipment As String
    
    sSQL = sSQL & " INSERT " & kCustRtrnShipTmpTblName & "(" & vbCrLf & _
                    " ShipmentKey       " & vbCrLf & _
                    ",ReturnID          " & vbCrLf & _
                    ",RMANo             " & vbCrLf & _
                    ",ReturnDate        " & vbCrLf & _
                    ",ReturnWhse        " & vbCrLf & _
                    ",ShipStatus        " & vbCrLf & _
                    ",GLBatchNo         " & vbCrLf & _
                    ",PostDate          " & vbCrLf & _
                    ",CustID            " & vbCrLf & _
                    ",CustName          " & vbCrLf & _
                    ",AddrName          " & vbCrLf & _
                    ",ReturnUserID      " & vbCrLf & _
                    ")"

   sSQL = sSQL & " SELECT DISTINCT" & _
                    " tsoPendShipment.ShipKey           " & vbCrLf & _
                    ",tsoPendShipment.TranID            " & vbCrLf & _
                    ",tsoRMA.TranNo                     " & vbCrLf & _
                    ",tsoPendShipment.TranDate          " & vbCrLf & _
                    ",timWarehouse.WhseID               " & vbCrLf & _
                    ",tsmLocalString.LocalText         " & vbCrLf & _
                    ",tciBatchLog.BatchNo               " & vbCrLf & _
                    ",tsoShipment.PostDate              " & vbCrLf & _
                    ",tarCustomer.CustID                " & vbCrLf & _
                    ",tarCustomer.CustName              " & vbCrLf & _
                    ",tciAddress.AddrName               " & vbCrLf & _
                    ",tsoPendShipment.CreateUserID      " & vbCrLf

    sSQL = sSQL & "FROM           tsoPendShipment       WITH (NOLOCK) " & vbCrLf & _
                " LEFT OUTER JOIN " & kCustRtrnShipTmpTblName & " WITH (NOLOCK) ON " & kCustRtrnShipTmpTblName & ".ShipmentKey       = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoCustRtrnShipment   WITH (NOLOCK) ON tsoCustRtrnShipment.ShipKey                = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoRMA                WITH (NOLOCK) ON tsoRMA.RMAKey                              = tsoCustRtrnShipment.RMAKey" & vbCrLf & _
                " LEFT OUTER JOIN timWarehouse          WITH (NOLOCK) ON timWarehouse.WhseKey                       = tsoPendshipment.RcvgWhseKey" & vbCrLf & _
                " LEFT OUTER JOIN tarCustomer           WITH (NOLOCK) ON tarCustomer.CustKey                        = tsoPendShipment.CustKey" & vbCrLf & _
                " LEFT OUTER JOIN tarNationalAcctLevel  WITH (NOLOCK) ON tarNationalAcctLevel.NationalAcctLevelKey  = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
                " LEFT OUTER JOIN tarNationalAcct       WITH (NOLOCK) ON tarNationalAcct.NationalAcctKey            = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
                " LEFT OUTER JOIN tciAddress            WITH (NOLOCK) ON tciAddress.AddrKey                         = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine           WITH (NOLOCK) ON tsoShipLine.ShipKey                        = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSOLine             WITH (NOLOCK) ON tsoSOLine.SOLineKey                        = tsoShipLine.SOLineKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSalesOrder         WITH (NOLOCK) ON tsoSalesOrder.SOKey                        = tsoSOLine.SOKey" & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod         WITH (NOLOCK) ON tciShipMethod.ShipMethKey                  = tsoPendShipment.ShipMethKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog        WITH (NOLOCK) ON tsoShipmentLog.ShipKey                     = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipment           WITH (NOLOCK) ON tsoShipment.ShipKey                        = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog           WITH (NOLOCK) ON tciBatchLog.BatchKey                       = tsoPendShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsmListValidation     WITH (NOLOCK) ON tsmListValidation.DBValue                  = tsoShipmentLog.TranStatus" & vbCrLf & _
                " LEFT OUTER JOIN tsmLocalString        WITH (NOLOCK) ON tsmLocalString.StringNo                    = tsmListValidation.StringNo" & vbCrLf

    sSQL = sSQL & " WHERE " & kCustRtrnShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoPendShipment.TranType = " & kTranTypeSORT & vbCrLf & _
                " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tarCustomer.Hold     = 0 OR tarCustomer.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.Hold   = 0 OR tsoSalesOrder.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.CrHold = 0 OR tsoSalesOrder.CrHold IS NULL) " & vbCrLf & _
                " AND (tarNationalAcct.Hold = 0 OR tarNationalAcct.Hold IS NULL) " & vbCrLf & _
                " AND (tsmListValidation.TableName = 'tsoShipmentLog') " & vbCrLf & _
                " AND (tsmListValidation.ColumnName = 'TranStatus') " & vbCrLf

    
    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
                     
    sSQL = sSQL & " UNION " & vbCrLf & _
        " SELECT DISTINCT                   " & vbCrLf & _
        " tsoShipment.ShipKey               " & vbCrLf & _
        ",tsoShipment.TranID                " & vbCrLf & _
        ",tsoRMA.TranNo                     " & vbCrLf & _
        ",tsoShipment.TranDate              " & vbCrLf & _
        ",timWarehouse.WhseID               " & vbCrLf & _
        ",tsmLocalString.LocalText          " & vbCrLf & _
        ",tciBatchLog.BatchNo               " & vbCrLf & _
        ",tsoShipment.PostDate              " & vbCrLf & _
        ",tarCustomer.CustID                " & vbCrLf & _
        ",tarCustomer.CustName              " & vbCrLf & _
        ",tciAddress.AddrName               " & vbCrLf & _
        ",tsoShipment.CreateUserID          " & vbCrLf

    sSQL = sSQL & "FROM           tsoShipment       WITH (NOLOCK) " & vbCrLf & _
                " LEFT OUTER JOIN " & kCustRtrnShipTmpTblName & " WITH (NOLOCK) ON " & kCustRtrnShipTmpTblName & ".ShipmentKey       = tsoShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoCustRtrnShipment   WITH (NOLOCK) ON tsoCustRtrnShipment.ShipKey                = tsoShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoRMA                WITH (NOLOCK) ON tsoRMA.RMAKey                              = tsoCustRtrnShipment.RMAKey" & vbCrLf & _
                " LEFT OUTER JOIN timWarehouse          WITH (NOLOCK) ON timWarehouse.WhseKey                       = tsoShipment.RcvgWhseKey" & vbCrLf & _
                " LEFT OUTER JOIN tarCustomer           WITH (NOLOCK) ON tarCustomer.CustKey                        = tsoShipment.CustKey" & vbCrLf & _
                " LEFT OUTER JOIN tarNationalAcctLevel  WITH (NOLOCK) ON tarNationalAcctLevel.NationalAcctLevelKey  = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
                " LEFT OUTER JOIN tarNationalAcct       WITH (NOLOCK) ON tarNationalAcct.NationalAcctKey            = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
                " LEFT OUTER JOIN tciAddress            WITH (NOLOCK) ON tciAddress.AddrKey                         = tsoShipment.ShipToAddrKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine           WITH (NOLOCK) ON tsoShipLine.ShipKey                        = tsoShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSOLine             WITH (NOLOCK) ON tsoSOLine.SOLineKey                        = tsoShipLine.SOLineKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSalesOrder         WITH (NOLOCK) ON tsoSalesOrder.SOKey                        = tsoSOLine.SOKey" & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod         WITH (NOLOCK) ON tciShipMethod.ShipMethKey                  = tsoShipment.ShipMethKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog        WITH (NOLOCK) ON tsoShipmentLog.ShipKey                     = tsoShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog           WITH (NOLOCK) ON tciBatchLog.BatchKey                       = tsoShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsmListValidation     WITH (NOLOCK) ON tsmListValidation.DBValue                  = tsoShipmentLog.TranStatus" & vbCrLf & _
                " LEFT OUTER JOIN tsmLocalString        WITH (NOLOCK) ON tsmLocalString.StringNo                    = tsmListValidation.StringNo"

    sSQL = sSQL & " WHERE " & kCustRtrnShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoShipment.TranType = " & kTranTypeSORT & vbCrLf & _
                " AND tsoShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tarCustomer.Hold     = 0 OR tarCustomer.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.Hold   = 0 OR tsoSalesOrder.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.CrHold = 0 OR tsoSalesOrder.CrHold IS NULL) " & vbCrLf & _
                " AND (tarNationalAcct.Hold = 0 OR tarNationalAcct.Hold IS NULL)" & vbCrLf & _
                " AND (tsmListValidation.TableName = 'tsoShipmentLog') " & vbCrLf & _
                " AND (tsmListValidation.ColumnName = 'TranStatus') " & vbCrLf

    If Len(Trim(sWhereClause)) > 0 Then
        sWhereShipment = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " AND " & sWhereShipment & vbCrLf
    End If

    Debug.Print sSQL
    
    moClass.moAppDB.ExecuteSQL sSQL
  
    'Get number of Pend Shipments not selected because of  Hold Customers, SalesOrders and NationalAccount

    sSQL = " SELECT COALESCE(COUNT(*),0) AS HoldCount FROM tsoPendShipment  WHERE ShipKey IN (" & _
                "SELECT DISTINCT tsoPendShipment.ShipKey " & vbCrLf

    sSQL = sSQL & "FROM           tsoPendShipment       WITH (NOLOCK) " & vbCrLf & _
                " LEFT OUTER JOIN tsoCustRtrnShipment   WITH (NOLOCK) ON tsoCustRtrnShipment.ShipKey                = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoRMA                WITH (NOLOCK) ON tsoRMA.RMAKey                              = tsoCustRtrnShipment.RMAKey" & vbCrLf & _
                " LEFT OUTER JOIN timWarehouse          WITH (NOLOCK) ON timWarehouse.WhseKey                       = tsoPendshipment.RcvgWhseKey" & vbCrLf & _
                " LEFT OUTER JOIN tarCustomer           WITH (NOLOCK) ON tarCustomer.CustKey                        = tsoPendShipment.CustKey" & vbCrLf & _
                " LEFT OUTER JOIN tarNationalAcctLevel  WITH (NOLOCK) ON tarNationalAcctLevel.NationalAcctLevelKey  = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
                " LEFT OUTER JOIN tarNationalAcct       WITH (NOLOCK) ON tarNationalAcct.NationalAcctKey            = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
                " LEFT OUTER JOIN tciAddress            WITH (NOLOCK) ON tciAddress.AddrKey                         = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine           WITH (NOLOCK) ON tsoShipLine.ShipKey                        = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSOLine             WITH (NOLOCK) ON tsoSOLine.SOLineKey                        = tsoShipLine.SOLineKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoSalesOrder         WITH (NOLOCK) ON tsoSalesOrder.SOKey                        = tsoSOLine.SOKey" & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod         WITH (NOLOCK) ON tciShipMethod.ShipMethKey                  = tsoPendShipment.ShipMethKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog        WITH (NOLOCK) ON tsoShipmentLog.ShipKey                     = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipment           WITH (NOLOCK) ON tsoShipment.ShipKey                        = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog           WITH (NOLOCK) ON tciBatchLog.BatchKey                       = tsoPendShipment.BatchKey" & vbCrLf
    
    sSQL = sSQL & " WHERE tsoPendShipment.TranType = " & kTranTypeSORT & _
                    " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & _
                    " AND ((tarCustomer.Hold     = 1 AND tarCustomer.Hold IS NOT NULL) " & _
                    "   OR (tsoSalesOrder.Hold   = 1 AND tsoSalesOrder.Hold IS NOT NULL) " & _
                    "   OR (tsoSalesOrder.CrHold = 1 AND tsoSalesOrder.CrHold IS NOT NULL) " & _
                    "   OR (tarNationalAcct.Hold = 1 AND tarNationalAcct.Hold IS NOT NULL))"
    
    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    sSQL = sSQL & ")"
 
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    If Not rs.IsEmpty Then
        mlHoldCount = mlHoldCount + CLng(rs.Field("HoldCount"))
    End If
    rs.Close
    Set rs = Nothing
    
End Sub


Private Sub SelectTrnsfrShipTrans(ByVal sWhereClause As String)
      
    Dim sSQL As String
    
    sSQL = "INSERT " & kTrnsfrShipTmpTblName & "( " & vbCrLf & _
                " ShipmentKey   " & vbCrLf & _
                ",Shipment      " & vbCrLf & _
                ",ShipDate      " & vbCrLf & _
                ",ShipWhse      " & vbCrLf & _
                ",RcvgWhse      " & vbCrLf & _
                ",ShipStatus    " & vbCrLf & _
                ",GLBatchNo     " & vbCrLf & _
                ",PostDate      " & vbCrLf & _
                ",ShipVia       " & vbCrLf & _
                ",PackUserID    " & vbCrLf & _
                ",ShipUserID    " & vbCrLf & _
                ")" & vbCrLf

    sSQL = sSQL & "SELECT DISTINCT              " & vbCrLf & _
                "tsoPendShipment.ShipKey        " & vbCrLf & _
                ",tsoPendShipment.TranID        " & vbCrLf & _
                ",tsoPendShipment.TranDate      " & vbCrLf & _
                ",timWarehouseShip.WhseID       " & vbCrLf & _
                ",timWarehouseRcvg.WhseID       " & vbCrLf & _
                ",tsmLocalString.LocalText      " & vbCrLf & _
                ",tciBatchLog.BatchNo           " & vbCrLf & _
                ",tsoShipment.PostDate          " & vbCrLf & _
                ",tciShipMethod.ShipMethID      " & vbCrLf & _
                ",tsoPendShipment.PackUserID    " & vbCrLf & _
                ",tsoPendShipment.CreateUserID  " & vbCrLf
                            

    sSQL = sSQL & "FROM tsoPendShipment WITH (NOLOCK)  " & vbCrLf & _
                " LEFT OUTER JOIN " & kTrnsfrShipTmpTblName & " WITH (NOLOCK) ON " & kTrnsfrShipTmpTblName & ".ShipmentKey       = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseShip    WITH (NOLOCK)   ON timWarehouseShip.WhseKey                 = tsoPendShipment.WhseKey " & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseRcvg    WITH (NOLOCK)   ON timWarehouseRcvg.WhseKey                 = tsoPendShipment.RcvgWhseKey " & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                = tsoPendShipment.ShipMethKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK)   ON tsoShipLine.ShipKey                      = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrderLine  WITH (NOLOCK)   ON timTrnsfrOrderLine.TrnsfrOrderLineKey    = tsoShipLine.TrnsfrOrderLineKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrder      WITH (NOLOCK)   ON timTrnsfrOrder.TrnsfrOrderKey            = timTrnsfrOrderLine.TrnsfrOrderKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoPickList         WITH (NOLOCK)   ON tsoPickList.PickListKey                  = tsoShipLine.PickListKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                   = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipment         WITH (NOLOCK)   ON tsoShipment.ShipKey                      = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK)   ON tciBatchLog.BatchKey                     = tsoPendShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsmListValidation   WITH (NOLOCK)   ON tsmListValidation.DBValue                = tsoShipmentLog.TranStatus" & vbCrLf & _
                " LEFT OUTER JOIN tsmLocalString      WITH (NOLOCK)   ON tsmLocalString.StringNo                  = tsmListValidation.StringNo" & vbCrLf & _
                " LEFT OUTER JOIN tsmUser             WITH (NOLOCK)   ON tsmUser.UserID                           = tsoPendShipment.PackUserID" & vbCrLf


    sSQL = sSQL & " WHERE " & kTrnsfrShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoPendShipment.TranType = " & kTranTypeSOTS & vbCrLf & _
                " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tsmListValidation.TableName = 'tsoShipmentLog') " & vbCrLf & _
                " AND (tsmListValidation.ColumnName = 'TranStatus') " & vbCrLf

    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    sSQL = sSQL & "UNION                        " & vbCrLf & _
                "SELECT DISTINCT                " & vbCrLf & _
                "tsoShipment.ShipKey            " & vbCrLf & _
                ",tsoShipment.TranID            " & vbCrLf & _
                ",tsoShipment.TranDate          " & vbCrLf & _
                ",timWarehouseShip.WhseID       " & vbCrLf & _
                ",timWarehouseRcvg.WhseID       " & vbCrLf & _
                ",tsmLocalString.LocalText      " & vbCrLf & _
                ",tciBatchLog.BatchNo           " & vbCrLf & _
                ",tsoShipment.PostDate          " & vbCrLf & _
                ",tciShipMethod.ShipMethID      " & vbCrLf & _
                ",tsoShipment.PackUserID        " & vbCrLf & _
                ",tsoShipment.CreateUserID      " & vbCrLf
                            

    sSQL = sSQL & "FROM tsoShipment WITH (NOLOCK)  " & vbCrLf & _
                " LEFT OUTER JOIN " & kTrnsfrShipTmpTblName & " WITH (NOLOCK) ON " & kTrnsfrShipTmpTblName & ".ShipmentKey       = tsoShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseShip    WITH (NOLOCK)   ON timWarehouseShip.WhseKey                 = tsoShipment.WhseKey " & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseRcvg    WITH (NOLOCK)   ON timWarehouseRcvg.WhseKey                 = tsoShipment.RcvgWhseKey " & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                = tsoShipment.ShipMethKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK)   ON tsoShipLine.ShipKey                      = tsoShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrderLine  WITH (NOLOCK)   ON timTrnsfrOrderLine.TrnsfrOrderLineKey    = tsoShipLine.TrnsfrOrderLineKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrder      WITH (NOLOCK)   ON timTrnsfrOrder.TrnsfrOrderKey            = timTrnsfrOrderLine.TrnsfrOrderKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoPickList         WITH (NOLOCK)   ON tsoPickList.PickListKey                  = tsoShipLine.PickListKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                   = tsoShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK)   ON tciBatchLog.BatchKey                     = tsoShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsmUser             WITH (NOLOCK)   ON tsmUser.UserID                           = tsoShipment.PackUserID" & vbCrLf & _
                " LEFT OUTER JOIN tsmListValidation   WITH (NOLOCK)   ON tsmListValidation.DBValue                = tsoShipmentLog.TranStatus" & vbCrLf & _
                " LEFT OUTER JOIN tsmLocalString      WITH (NOLOCK)   ON tsmLocalString.StringNo                  = tsmListValidation.StringNo" & vbCrLf


    sSQL = sSQL & " WHERE " & kTrnsfrShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoShipment.TranType = " & kTranTypeSOTS & vbCrLf & _
                " AND tsoShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tsmListValidation.TableName = 'tsoShipmentLog') " & vbCrLf & _
                " AND (tsmListValidation.ColumnName = 'TranStatus') " & vbCrLf
                
    If Len(Trim(sWhereClause)) > 0 Then
        sWhereClause = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL

End Sub


Private Sub m_SelectTransCustShip_SelectTransactions(ByVal sWhereClause As String)
    SelectCustShipTrans sWhereClause
End Sub

Private Sub m_SelectTransCustShip_DeSelectTransactions(ByVal sWhereClause As String)
        
    On Error GoTo ErrorRoutine
    
    Debug.Print "Deleselect Transactions"
    
    Dim sSQL As String
    
    moClass.moAppDB.BeginTrans
    
    If InStr(1, sWhereClause, "tsoShipment.PostDate", vbTextCompare) = 0 Then
        sSQL = " DELETE  " & kCustShipTmpTblName
    
        sSQL = sSQL & " FROM  " & kCustShipTmpTblName & vbCrLf & _
                " INNER JOIN          tsoPendShipment   WITH (NOLOCK)       ON tsoPendShipment.ShipKey      = " & kCustShipTmpTblName & ".ShipmentKey" & vbCrLf & _
                " LEFT OUTER JOIN     timWarehouse      WITH (NOLOCK)       ON timWarehouse.WhseKey         = tsoPendShipment.WhseKey" & vbCrLf & _
                " LEFT OUTER JOIN     tarCustomer       WITH (NOLOCK)       ON tarCustomer.CustKey          = tsoPendShipment.CustKey" & vbCrLf & _
                " LEFT OUTER JOIN     tciAddress        WITH (NOLOCK)       ON tciAddress.AddrKey           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoShipLine       WITH (NOLOCK)       ON tsoShipLine.ShipKey          = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoSOLine         WITH (NOLOCK)       ON tsoSOLine.SOLineKey          = tsoShipLine.SOLineKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoSalesOrder     WITH (NOLOCK)       ON tsoSalesOrder.SOKey          = tsoSOLine.SOKey" & vbCrLf & _
                " LEFT OUTER JOIN     tciShipMethod     WITH (NOLOCK)       ON tciShipMethod.ShipMethKey    = tsoPendShipment.ShipMethKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoPickList       WITH (NOLOCK)       ON tsoPickList.PickListKey      = tsoShipLine.PickListKey" & vbCrLf & _
                " LEFT OUTER JOIN     tciBatchLog       WITH (NOLOCK)       ON tciBatchLog.BatchKey         = tsoPendShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoShipmentLog    WITH (NOLOCK)       ON tsoShipmentLog.ShipKey       = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsmUser           WITH (NOLOCK)       ON tsmUser.UserID               = tsoPendShipment.PackUserID" & vbCrLf

        If Len(Trim(sWhereClause)) > 0 Then
            sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
        End If
            
        moClass.moAppDB.ExecuteSQL sSQL
    
    End If
    
    sSQL = " DELETE  " & kCustShipTmpTblName

    sSQL = sSQL & " FROM  " & kCustShipTmpTblName & vbCrLf & _
            " INNER JOIN          tsoShipment       WITH (NOLOCK)       ON tsoShipment.ShipKey      = " & kCustShipTmpTblName & ".ShipmentKey" & vbCrLf & _
            " LEFT OUTER JOIN     timWarehouse      WITH (NOLOCK)       ON timWarehouse.WhseKey         = tsoShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN     tarCustomer       WITH (NOLOCK)       ON tarCustomer.CustKey          = tsoShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciAddress        WITH (NOLOCK)       ON tciAddress.AddrKey           = tsoShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipLine       WITH (NOLOCK)       ON tsoShipLine.ShipKey          = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSOLine         WITH (NOLOCK)       ON tsoSOLine.SOLineKey          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSalesOrder     WITH (NOLOCK)       ON tsoSalesOrder.SOKey          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciShipMethod     WITH (NOLOCK)       ON tciShipMethod.ShipMethKey    = tsoShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoPickList       WITH (NOLOCK)       ON tsoPickList.PickListKey      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciBatchLog       WITH (NOLOCK)       ON tciBatchLog.BatchKey         = tsoShipment.BatchKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipmentLog    WITH (NOLOCK)       ON tsoShipmentLog.ShipKey       = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsmUser           WITH (NOLOCK)       ON tsmUser.UserID               = tsoShipment.PackUserID" & vbCrLf
            
    If Len(Trim(sWhereClause)) > 0 Then
        sWhereClause = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    moClass.moAppDB.CommitTrans
    
    Exit Sub

ErrorRoutine:

    moClass.moAppDB.Rollback
    
    gSetSotaErr Err, sMyName, "m_SelectTransCustShip_DeSelectTransactions", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select

End Sub
            

Private Sub m_SelectTransCustRtrnShip_SelectTransactions(ByVal sWhereClause As String)
    SelectCustRtrnTrans sWhereClause
End Sub

Private Sub m_SelectTransCustShip_Proceed(ByVal lSettingKey As Long)
    'Refresh settings combo and set to the lSettingkey
    Debug.Print "m_SelectTransCustShip_Proceed for " & lSettingKey
    
    UpdateSOCommitTmpTbl kTranTypeSOSH
    SetEnabledFromSelected kTranTypeSOSH
    
    m_CustShipSelectSetting.RefreshSettingList
    RefreshGridColSelectSetting kTranTypeSOSH, lSettingKey
    
    If mlHoldCount > 0 Then
        sbrMain.Message = "Some shipments were not selected due to Hold or Credit Hold reasons."
    End If
    
End Sub

Private Sub UpdateSOCommitTmpTbl(ByVal lTranType As Long)

    Dim sTempTableName As String
    Dim lRow As Long
    Dim sSQL As String
    Dim rs As Object

    lRow = GetRowFromTranType(lTranType)
    sTempTableName = GetTempTableNameFromTranType(lTranType)
    
    sSQL = "DELETE " & TmpTblSOCommitName & " WHERE TranType = " & lTranType & vbCrLf
    
    sSQL = sSQL & " INSERT " & TmpTblSOCommitName & "(" & vbCrLf & _
                " CompanyID" & vbCrLf & _
                ",TranType" & vbCrLf & _
                ",InvcDate" & vbCrLf & _
                ",PostDate" & vbCrLf & _
                ",TranKey" & vbCrLf & _
                ",PreCommitBatchKey" & vbCrLf & _
                ",DispoBatchKey" & vbCrLf & _
                ",CommitStatus" & vbCrLf & _
                ")"

    sSQL = sSQL & " SELECT " & _
                " tsoPendShipment.CompanyID" & vbCrLf & _
                ",tsoPendShipment.TranType" & vbCrLf & _
                ",tsoPendShipment.TranDate" & vbCrLf & _
                ",''" & vbCrLf & _
                ",tsoPendShipment.ShipKey" & vbCrLf & _
                "," & mlHiddenBatchKey & vbCrLf & _
                ", NULL " & vbCrLf & _
                ",0" & vbCrLf & _
                " FROM " & sTempTableName & vbCrLf & _
                " JOIN tsoPendShipment WITH (NOLOCK) ON tsoPendShipment.ShipKey = " & sTempTableName & ".ShipmentKey"
                
    sSQL = sSQL & " UNION " & vbCrLf & _
                " SELECT " & _
                " tsoShipment.CompanyID" & vbCrLf & _
                ",tsoShipment.TranType" & vbCrLf & _
                ",tsoShipment.TranDate" & vbCrLf & _
                ",''" & vbCrLf & _
                ",tsoShipment.ShipKey" & vbCrLf & _
                "," & mlHiddenBatchKey & vbCrLf & _
                ", NULL " & vbCrLf & _
                ",0" & vbCrLf & _
                " FROM " & sTempTableName & vbCrLf & _
                " JOIN tsoShipment WITH (NOLOCK) ON tsoShipment.ShipKey = " & sTempTableName & ".ShipmentKey"

                
    sSQL = sSQL & " TRUNCATE TABLE " & sTempTableName
    
    moClass.moAppDB.ExecuteSQL sSQL
    
    sSQL = "SELECT COALESCE(COUNT(*),0) AS Selected FROM " & TmpTblSOCommitName & " WHERE TranType = " & lTranType
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    
    With grdSelectTrans
        .ReDraw = False
        .Col = kColSelectedCount
        .Row = lRow
        .Text = rs.Field("Selected")
        .ReDraw = True
        .Refresh
    End With
    
    Set rs = Nothing
    
End Sub

Private Function GetTempTableNameFromTranType(ByVal lTranType As Long) As String
    Select Case lTranType
    
        Case kTranTypeSOSH
        
            GetTempTableNameFromTranType = kCustShipTmpTblName
            
        Case kTranTypeSODS
        
            GetTempTableNameFromTranType = kDropShipTmpTblName
            
        Case kTranTypeSORT
        
            GetTempTableNameFromTranType = kCustRtrnShipTmpTblName
            
        Case kTranTypeSOTS
        
            GetTempTableNameFromTranType = kTrnsfrShipTmpTblName
    
    End Select
End Function

Private Sub LoadTrnsfrSelectTmpTbl()

    Dim sSQL As String
    
    Debug.Print "LoadTrnsfrSelectTmpTbl"
    
    sSQL = "INSERT " & kTrnsfrShipTmpTblName & "( " & vbCrLf & _
                " ShipmentKey   " & vbCrLf & _
                ",Shipment      " & vbCrLf & _
                ",ShipDate      " & vbCrLf & _
                ",ShipWhse      " & vbCrLf & _
                ",RcvgWhse      " & vbCrLf & _
                ",ShipStatus " & vbCrLf & _
                ",GLBatchNo " & vbCrLf & _
                ",PostDate " & vbCrLf & _
                ",ShipVia       " & vbCrLf & _
                ",PackUserID    " & vbCrLf & _
                ",ShipUserID    " & vbCrLf & ")"

    sSQL = sSQL & "SELECT DISTINCT " & vbCrLf & _
                "tsoPendShipment.ShipKey " & vbCrLf & _
                ",tsoPendShipment.TranID " & vbCrLf & _
                ",tsoPendShipment.TranDate " & vbCrLf & _
                ",timWarehouseShip.WhseID " & vbCrLf & _
                ",timWarehouseRcvg.WhseID " & vbCrLf & _
                ",tsoShipmentLog.TranStatus " & _
                ",tciBatchLog.BatchNo " & _
                ",tsoShipment.PostDate " & _
                ",tciShipMethod.ShipMethID " & vbCrLf & _
                ",tsoPendShipment.PackUserID " & vbCrLf & _
                ",tsoPendShipment.CreateUserID " & vbCrLf

    sSQL = sSQL & "FROM " & TmpTblSOCommitName & vbCrLf & _
                " JOIN            tsoPendShipment     WITH (NOLOCK)   ON tsoPendShipment.ShipKey                  = " & TmpTblSOCommitName & ".TranKey" & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseShip    WITH (NOLOCK)   ON timWarehouseShip.WhseKey                 = tsoPendShipment.WhseKey " & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseRcvg    WITH (NOLOCK)   ON timWarehouseRcvg.WhseKey                 = tsoPendShipment.RcvgWhseKey " & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                = tsoPendShipment.ShipMethKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK)   ON tsoShipLine.ShipKey                      = tsoPendShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrderLine  WITH (NOLOCK)   ON timTrnsfrOrderLine.TrnsfrOrderLineKey    = tsoShipLine.TrnsfrOrderLineKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrder      WITH (NOLOCK)   ON timTrnsfrOrder.TrnsfrOrderKey            = timTrnsfrOrderLine.TrnsfrOrderKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoPickList         WITH (NOLOCK)   ON tsoPickList.PickListKey                  = tsoShipLine.PickListKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                   = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK)   ON tciBatchLog.BatchKey                     = tsoPendShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipment         WITH (NOLOCK)   ON tsoShipment.ShipKey                      = tsoPendShipment.ShipKey" & vbCrLf

    sSQL = sSQL & " WHERE " & TmpTblSOCommitName & ".TranType = " & kTranTypeSOTS & vbCrLf
    
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub

Private Sub m_SelectTransCustRtrnShip_Proceed(ByVal lSettingKey As Long)
    'Refresh settings combo and set to the lSettingkey
    Debug.Print "m_SelectTransCustRtrnShip_Proceed for " & lSettingKey
    
    UpdateSOCommitTmpTbl kTranTypeSORT
    SetEnabledFromSelected kTranTypeSORT

    m_CustReturnShipSelectSetting.RefreshSettingList
    RefreshGridColSelectSetting kTranTypeSORT, lSettingKey

    If mlHoldCount > 0 Then
        sbrMain.Message = "Some shipments were not selected due to Hold or Credit Hold reasons."
    End If
End Sub

Private Sub m_SelectTransTrnsfrShip_DeSelectTransactions(ByVal sWhereClause As String)

    Dim sSQL As String
    
    Debug.Print "Deleselect Transactions"
    
    On Error GoTo ErrorRoutine
    
    If InStr(1, sWhereClause, "tsoShipment.PostDate", vbTextCompare) = 0 Then
    
        sSQL = " DELETE  " & kTrnsfrShipTmpTblName
    
        sSQL = sSQL & " FROM  " & kTrnsfrShipTmpTblName & vbCrLf & _
                    " INNER JOIN      tsoPendShipment     WITH (NOLOCK)   ON tsoPendShipment.ShipKey                  = " & kTrnsfrShipTmpTblName & ".ShipmentKey" & vbCrLf & _
                    " LEFT OUTER JOIN timWarehouseShip    WITH (NOLOCK)   ON timWarehouseShip.WhseKey                 = tsoPendShipment.WhseKey " & vbCrLf & _
                    " LEFT OUTER JOIN timWarehouseRcvg    WITH (NOLOCK)   ON timWarehouseRcvg.WhseKey                 = tsoPendShipment.RcvgWhseKey " & vbCrLf & _
                    " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                = tsoPendShipment.ShipMethKey " & vbCrLf & _
                    " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK)   ON tsoShipLine.ShipKey                      = tsoPendShipment.ShipKey " & vbCrLf & _
                    " LEFT OUTER JOIN timTrnsfrOrderLine  WITH (NOLOCK)   ON timTrnsfrOrderLine.TrnsfrOrderLineKey    = tsoShipLine.TrnsfrOrderLineKey " & vbCrLf & _
                    " LEFT OUTER JOIN timTrnsfrOrder      WITH (NOLOCK)   ON timTrnsfrOrder.TrnsfrOrderKey            = timTrnsfrOrderLine.TrnsfrOrderKey " & vbCrLf & _
                    " LEFT OUTER JOIN tsoPickList         WITH (NOLOCK)   ON tsoPickList.PickListKey                  = tsoShipLine.PickListKey " & vbCrLf & _
                    " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK)   ON tciBatchLog.BatchKey                     = tsoPendShipment.BatchKey" & vbCrLf & _
                    " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                   = tsoPendShipment.ShipKey" & vbCrLf & _
                    " LEFT OUTER JOIN tsmUser             WITH (NOLOCK)   ON tsmUser.UserID                           = tsoPendShipment.PackUserID" & vbCrLf
      
        If Len(Trim(sWhereClause)) > 0 Then
            sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
        End If
                
        moClass.moAppDB.ExecuteSQL sSQL

    End If
    
    sSQL = " DELETE  " & kTrnsfrShipTmpTblName

    sSQL = sSQL & " FROM  " & kTrnsfrShipTmpTblName & vbCrLf & _
                " INNER JOIN      tsoShipment         WITH (NOLOCK)   ON tsoShipment.ShipKey                      = " & kTrnsfrShipTmpTblName & ".ShipmentKey" & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseShip    WITH (NOLOCK)   ON timWarehouseShip.WhseKey                 = tsoShipment.WhseKey " & vbCrLf & _
                " LEFT OUTER JOIN timWarehouseRcvg    WITH (NOLOCK)   ON timWarehouseRcvg.WhseKey                 = tsoShipment.RcvgWhseKey " & vbCrLf & _
                " LEFT OUTER JOIN tciShipMethod       WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                = tsoShipment.ShipMethKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoShipLine         WITH (NOLOCK)   ON tsoShipLine.ShipKey                      = tsoShipment.ShipKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrderLine  WITH (NOLOCK)   ON timTrnsfrOrderLine.TrnsfrOrderLineKey    = tsoShipLine.TrnsfrOrderLineKey " & vbCrLf & _
                " LEFT OUTER JOIN timTrnsfrOrder      WITH (NOLOCK)   ON timTrnsfrOrder.TrnsfrOrderKey            = timTrnsfrOrderLine.TrnsfrOrderKey " & vbCrLf & _
                " LEFT OUTER JOIN tsoPickList         WITH (NOLOCK)   ON tsoPickList.PickListKey                  = tsoShipLine.PickListKey " & vbCrLf & _
                " LEFT OUTER JOIN tciBatchLog         WITH (NOLOCK)   ON tciBatchLog.BatchKey                     = tsoShipment.BatchKey" & vbCrLf & _
                " LEFT OUTER JOIN tsoShipmentLog      WITH (NOLOCK)   ON tsoShipmentLog.ShipKey                   = tsoShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN tsmUser             WITH (NOLOCK)   ON tsmUser.UserID                           = tsoShipment.PackUserID" & vbCrLf
                
    If Len(Trim(sWhereClause)) > 0 Then
        sWhereClause = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL

    moClass.moAppDB.CommitTrans
    
    Exit Sub

ErrorRoutine:

    moClass.moAppDB.Rollback
    
    gSetSotaErr Err, sMyName, "m_SelectTransCustShip_DeSelectTransactions", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select

End Sub

Private Sub m_SelectTransTrnsfrShip_Proceed(ByVal lSettingKey As Long)
    'Refresh settings combo and set to the lSettingkey
    Debug.Print "m_SelectTransTrnsfrShip_Proceed for " & lSettingKey
    
    UpdateSOCommitTmpTbl kTranTypeSOTS
    SetEnabledFromSelected kTranTypeSOTS
    m_TransferShipSelectSetting.RefreshSettingList
    RefreshGridColSelectSetting kTranTypeSOTS, lSettingKey

End Sub

Private Sub m_SelectTransTrnsfrShip_SelectTransactions(ByVal sWhereClause As String)
    SelectTrnsfrShipTrans sWhereClause
End Sub

Private Sub CreateTempTableDropShipSelect()

    Dim sSQL As String
    
    sSQL = "IF OBJECT_ID('tempdb.." & kDropShipTmpTblName & "') IS NOT NULL" & vbCrLf & _
                " TRUNCATE TABLE " & kDropShipTmpTblName & vbCrLf & _
            " ELSE BEGIN " & vbCrLf & _
                " CREATE TABLE " & kDropShipTmpTblName & " (" & vbCrLf & _
                " ShipmentKey INTEGER       NOT NULL," & vbCrLf & _
                " Shipment VARCHAR(13)      NOT NULL," & vbCrLf & _
                " ShipDate DATETIME         NOT NULL," & vbCrLf & _
                " ShipWhse VARCHAR(6)       NULL," & vbCrLf & _
                " PostDate DATETIME         NULL," & vbCrLf & _
                " CustID VARCHAR(12)        NULL," & vbCrLf & _
                " CustName VARCHAR(40)      NULL," & vbCrLf & _
                " AddrName VARCHAR(40)      NULL," & vbCrLf & _
                " ShipVia VARCHAR(15)       NULL," & vbCrLf & _
                " PickListNo VARCHAR(10)    NULL," & vbCrLf & _
                " PackUserID VARCHAR(30)    NULL," & vbCrLf & _
                " ShipUserID VARCHAR(30)    NULL " & vbCrLf & _
                ")" & vbCrLf & _
                " CREATE CLUSTERED INDEX " & kDropShipTmpTblName & "_ind_cls ON " & kDropShipTmpTblName & " (ShipmentKey)" & _
            " END"
            
    moClass.moAppDB.ExecuteSQL sSQL
    
End Sub

Private Sub SetupDropShipmentSelect()
    'Sale Shipment Selection/Setting setup
    
    Dim oGridCol As clsGridCol
    Dim sSQL As String
    
    CreateTempTableDropShipSelect

    Set m_DropShipSelectColMgr = New clsSelectGridColMgr
    With m_DropShipSelectColMgr
        'need a restrict for Lookups --)
        .Init moClass.moAppDB, msCompanyID
        .ManualSaveLastSetting = True

        .Add "ShipmentKey", "tsoPendShipment", "ShipKey", "Shipment Key", , False, False, SQL_INTEGER, , SS_CELL_TYPE_INTEGER
        
        Set oGridCol = .Add("Shipment", "tsoPendShipment", "TranID", "Shipment", "CompanyID = " & gsQuoted(msCompanyID) & _
                                                                    " AND TranType = " & kTranTypeSODS)
        oGridCol.LookupID = "Shipment"
        oGridCol.LookupReturnColumn = "TranID"
        
        .Add "ShipDate", "tsoPendShipment", "TranDate", "Ship Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
                
        sSQL = "SELECT WhseKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        sSQL = sSQL & " UNION SELECT WhseKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
                                        
        .Add "ShipWhse", "timWarehouse", "WhseID", "Ship Whse", "WhseKey IN (" & sSQL & ")"
        .Add "PostDate", "tsoShipment", "PostDate", "Post Date", , , , SQL_DATE, , SS_CELL_TYPE_DATE, True
                        
        sSQL = "SELECT CustKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        sSQL = sSQL & " UNION SELECT CustKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        .Add "CustID", "tarCustomer", "CustID", "Customer", "CustKey IN (" & sSQL & ")"
            
        .Add "CustName", "tarCustomer", "CustName", "Customer Name"
        
        sSQL = "SELECT COALESCE(ShipToCustAddrKey, AddrKey) FROM tsoPendShipment WITH (NOLOCK) " & _
                                    " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        sSQL = sSQL & " UNION SELECT COALESCE(ShipToCustAddrKey, AddrKey) FROM tsoShipment WITH (NOLOCK) " & _
                                    " WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        .Add "AddrName", "tciAddress", "AddrName", "Customer Ship To Address", "AddrKey IN (" & sSQL & ")"
                        
        .Add "CustPONo", "tsoSalesOrder", "CustPONo", "Customer PO No", , , False
        
        sSQL = "SELECT sol.SOKey FROM tsoPendShipment ps WITH (NOLOCK) " & _
                                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                                    " JOIN tsoSOLine   sol  WITH (NOLOCK) ON sol.SOLineKey = shpl.SOLineKey" & _
                                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSODS
        sSQL = sSQL & " UNION SELECT sol.SOKey FROM tsoShipment ps WITH (NOLOCK) " & _
                                    " JOIN tsoShipLine shpl WITH (NOLOCK) ON shpl.ShipKey = ps.ShipKey" & _
                                    " JOIN tsoSOLine   sol  WITH (NOLOCK) ON sol.SOLineKey = shpl.SOLineKey" & _
                                    " WHERE ps.CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND ps.TranType = " & kTranTypeSODS
                                        
        .Add "SalesOrder", "tsoSalesOrder", "TranNoRelChngOrd", "Sales Order", "SOKey IN (" & sSQL & ")", , False
                        
        sSQL = "SELECT ShipMethKey FROM tsoPendShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        sSQL = sSQL & " UNION SELECT ShipMethKey FROM tsoShipment WITH (NOLOCK) WHERE CompanyID = " & gsQuoted(msCompanyID) & _
                                        " AND TranType = " & kTranTypeSODS
        .Add "ShipVia", "tciShipMethod", "ShipMethID", "Ship Via", "ShipMethKey IN (" & sSQL & ")"
                        
        .Add "ShipUserID", "tsoPendShipment", "CreateUserID", "Ship User"
        
    End With
    
    Set m_DropShipSelectSetting = New clsSelectionSettings
    m_DropShipSelectSetting.Init moClass.moAppDB, moClass.moSysSession, m_DropShipSelectColMgr, _
                                kAppNameSOCommitDropShip, ktskSOCommitSaleShipSetting

End Sub

Private Sub InitSelectTransDropShipment()
    'If creating more than one of these, then use the collection to to differentiate
    
    If m_SelectTransDropShip Is Nothing Then
        
        Set m_SelectTransDropShip = New clsCISelectTrans
        
        With m_SelectTransDropShip
        
            Set .moAppDB = moClass.moAppDB
            Set .moFramework = moClass.moFramework
            Set .moSysSession = moClass.moSysSession
            Set .moDasSession = moClass.moDasSession
            grdSelectTrans.Col = kColTranTypeDesc
            grdSelectTrans.Row = kRowTranTypeDropShip
            .Caption = "Select " & Trim(grdSelectTrans.Text)
            .InitSelectTrans m_DropShipSelectColMgr, ktskSOCommitSaleShipSetting, kAppNameSOCommitDropShip, kDropShipTmpTblName
            
        End With
        
    End If
    
End Sub

Private Sub LoadDropShipSelectTmpTbl()
    Dim sSQL As String
    
    sSQL = " INSERT " & kDropShipTmpTblName & "(" & vbCrLf & _
                " ShipmentKey " & vbCrLf & _
                ",Shipment " & vbCrLf & _
                ",ShipDate " & vbCrLf & _
                ",ShipWhse" & vbCrLf & _
                ",PostDate" & vbCrLf & _
                ",CustID" & vbCrLf & _
                ",CustName" & vbCrLf & _
                ",AddrName" & vbCrLf & _
                ",ShipVia" & vbCrLf & _
                ",PickListNo" & vbCrLf & _
                ",PackUserID" & vbCrLf & _
                ",ShipUserID" & vbCrLf & _
                ")"

    sSQL = sSQL & " SELECT DISTINCT" & _
            " tsoPendShipment.ShipKey       " & _
            ",tsoPendShipment.TranID        " & _
            ",tsoPendShipment.TranDate      " & _
            ",timWarehouse.WhseID           " & _
            ",tsoShipment.PostDate          " & _
            ",tarCustomer.CustID            " & _
            ",tarCustomer.CustName          " & _
            ",tciAddress.AddrName           " & _
            ",tciShipMethod.ShipMethID      " & _
            ",tsoPickList.PickListNo        " & _
            ",tsoPendShipment.PackUserID    " & _
            ",tsoPendShipment.CreateUserID  " & vbCrLf

    sSQL = sSQL & "FROM " & TmpTblSOCommitName & vbCrLf & _
            " JOIN                tsoPendShipment   WITH (NOLOCK)       ON tsoPendShipment.ShipKey = " & TmpTblSOCommitName & ".TranKey" & vbCrLf & _
            " LEFT OUTER JOIN     timWarehouse      WITH (NOLOCK)       ON timWarehouse.WhseKey         = tsoPendShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN     tarCustomer       WITH (NOLOCK)       ON tarCustomer.CustKey          = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciAddress        WITH (NOLOCK)       ON tciAddress.AddrKey           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipLine       WITH (NOLOCK)       ON tsoShipLine.ShipKey          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSOLine         WITH (NOLOCK)       ON tsoSOLine.SOLineKey          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSalesOrder     WITH (NOLOCK)       ON tsoSalesOrder.SOKey          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciShipMethod     WITH (NOLOCK)       ON tciShipMethod.ShipMethKey    = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoPickList       WITH (NOLOCK)       ON tsoPickList.PickListKey      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipment       WITH (NOLOCK)       ON tsoShipment.ShipKey          = tsoPendShipment.ShipKey"

    sSQL = sSQL & " WHERE " & TmpTblSOCommitName & ".TranType = " & kTranTypeSODS & vbCrLf

    moClass.moAppDB.ExecuteSQL sSQL

End Sub

Private Sub SelectDropShipTrans(ByVal sWhereClause As String)
    
    Debug.Print "Select Transactions"
    Dim sSQL As String
    Dim rs As Object
    Dim sWhereShipment As String
    
    sSQL = " INSERT " & kDropShipTmpTblName & "(" & vbCrLf & _
                " ShipmentKey       " & vbCrLf & _
                ",Shipment          " & vbCrLf & _
                ",ShipDate          " & vbCrLf & _
                ",ShipWhse          " & vbCrLf & _
                ",PostDate          " & vbCrLf & _
                ",CustID            " & vbCrLf & _
                ",CustName          " & vbCrLf & _
                ",AddrName          " & vbCrLf & _
                ",ShipVia           " & vbCrLf & _
                ",PickListNo        " & vbCrLf & _
                ",PackUserID        " & vbCrLf & _
                ",ShipUserID        " & vbCrLf & _
                ")"

    sSQL = sSQL & " SELECT DISTINCT             " & vbCrLf & _
            " tsoPendShipment.ShipKey           " & vbCrLf & _
            ",tsoPendShipment.TranID            " & vbCrLf & _
            ",tsoPendShipment.TranDate          " & vbCrLf & _
            ",timWarehouse.WhseID               " & vbCrLf & _
            ",tsoShipment.PostDate              " & vbCrLf & _
            ",tarCustomer.CustID                " & vbCrLf & _
            ",tarCustomer.CustName              " & vbCrLf & _
            ",tciAddress.AddrName               " & vbCrLf & _
            ",tciShipMethod.ShipMethID          " & vbCrLf & _
            ",tsoPickList.PickListNo            " & vbCrLf & _
            ",tsoPendShipment.PackUserID        " & vbCrLf & _
            ",tsoPendShipment.CreateUserID      " & vbCrLf

    sSQL = sSQL & "FROM           tsoPendShipment       WITH (NOLOCK) " & vbCrLf & _
            " LEFT OUTER JOIN " & kDropShipTmpTblName & " WITH (NOLOCK) ON " & kDropShipTmpTblName & ".ShipmentKey      = tsoPendShipment.ShipKey " & vbCrLf & _
            " LEFT OUTER JOIN   timWarehouse            WITH (NOLOCK)   ON timWarehouse.WhseKey                         = tsoPendShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarCustomer             WITH (NOLOCK)   ON tarCustomer.CustKey                          = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcctLevel    WITH (NOLOCK)   ON tarNationalAcctLevel.NationalAcctLevelKey    = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcct         WITH (NOLOCK)   ON tarNationalAcct.NationalAcctKey              = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciAddress              WITH (NOLOCK)   ON tciAddress.AddrKey                           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipLine             WITH (NOLOCK)   ON tsoShipLine.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSOLine               WITH (NOLOCK)   ON tsoSOLine.SOLineKey                          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSalesOrder           WITH (NOLOCK)   ON tsoSalesOrder.SOKey                          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciShipMethod           WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                    = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoPickList             WITH (NOLOCK)   ON tsoPickList.PickListKey                      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipment             WITH (NOLOCK)   ON tsoShipment.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf

    sSQL = sSQL & " WHERE " & kDropShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoPendShipment.TranType = " & kTranTypeSODS & vbCrLf & _
                " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tarCustomer.Hold     = 0 OR tarCustomer.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.Hold   = 0 OR tsoSalesOrder.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.CrHold = 0 OR tsoSalesOrder.CrHold IS NULL) " & vbCrLf & _
                " AND (tarNationalAcct.Hold = 0 OR tarNationalAcct.Hold IS NULL)" & vbCrLf
    
    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    

    sSQL = sSQL & " UNION SELECT DISTINCT       " & vbCrLf & _
            " tsoShipment.ShipKey           " & vbCrLf & _
            ",tsoShipment.TranID            " & vbCrLf & _
            ",tsoShipment.TranDate          " & vbCrLf & _
            ",timWarehouse.WhseID               " & vbCrLf & _
            ",tsoShipment.PostDate              " & vbCrLf & _
            ",tarCustomer.CustID                " & vbCrLf & _
            ",tarCustomer.CustName              " & vbCrLf & _
            ",tciAddress.AddrName               " & vbCrLf & _
            ",tciShipMethod.ShipMethID          " & vbCrLf & _
            ",tsoPickList.PickListNo            " & vbCrLf & _
            ",tsoShipment.PackUserID        " & vbCrLf & _
            ",tsoShipment.CreateUserID      " & vbCrLf

    sSQL = sSQL & "FROM           tsoShipment       WITH (NOLOCK) " & vbCrLf & _
            " LEFT OUTER JOIN " & kDropShipTmpTblName & " WITH (NOLOCK) ON " & kDropShipTmpTblName & ".ShipmentKey      = tsoShipment.ShipKey " & vbCrLf & _
            " LEFT OUTER JOIN   timWarehouse            WITH (NOLOCK)   ON timWarehouse.WhseKey                         = tsoShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarCustomer             WITH (NOLOCK)   ON tarCustomer.CustKey                          = tsoShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcctLevel    WITH (NOLOCK)   ON tarNationalAcctLevel.NationalAcctLevelKey    = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcct         WITH (NOLOCK)   ON tarNationalAcct.NationalAcctKey              = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciAddress              WITH (NOLOCK)   ON tciAddress.AddrKey                           = tsoShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipLine             WITH (NOLOCK)   ON tsoShipLine.ShipKey                          = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSOLine               WITH (NOLOCK)   ON tsoSOLine.SOLineKey                          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSalesOrder           WITH (NOLOCK)   ON tsoSalesOrder.SOKey                          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciShipMethod           WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                    = tsoShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoPickList             WITH (NOLOCK)   ON tsoPickList.PickListKey                      = tsoShipLine.PickListKey" & vbCrLf

    sSQL = sSQL & " WHERE " & kDropShipTmpTblName & ".ShipmentKey IS NULL " & vbCrLf & _
                " AND tsoShipment.TranType = " & kTranTypeSODS & vbCrLf & _
                " AND tsoShipment.CompanyID = " & gsQuoted(msCompanyID) & vbCrLf & _
                " AND (tarCustomer.Hold     = 0 OR tarCustomer.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.Hold   = 0 OR tsoSalesOrder.Hold IS NULL) " & vbCrLf & _
                " AND (tsoSalesOrder.CrHold = 0 OR tsoSalesOrder.CrHold IS NULL) " & vbCrLf & _
                " AND (tarNationalAcct.Hold = 0 OR tarNationalAcct.Hold IS NULL)" & vbCrLf
    
    If Len(Trim(sWhereClause)) > 0 Then
        sWhereShipment = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " AND " & sWhereShipment & vbCrLf
    End If

    moClass.moAppDB.ExecuteSQL sSQL
    
    'Get number of Pend Shipments not selected because of  Hold Customers, SalesOrders and NationalAccount

    sSQL = " SELECT COALESCE(COUNT(*),0) AS HoldCount FROM tsoPendShipment  WHERE ShipKey IN (" & _
                    "SELECT DISTINCT tsoPendShipment.ShipKey " & vbCrLf

    sSQL = sSQL & "FROM           tsoPendShipment       WITH (NOLOCK) " & vbCrLf & _
            " LEFT OUTER JOIN   timWarehouse            WITH (NOLOCK)   ON timWarehouse.WhseKey                         = tsoPendShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarCustomer             WITH (NOLOCK)   ON tarCustomer.CustKey                          = tsoPendShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcctLevel    WITH (NOLOCK)   ON tarNationalAcctLevel.NationalAcctLevelKey    = tarCustomer.NationalAcctLevelKey" & vbCrLf & _
            " LEFT OUTER JOIN   tarNationalAcct         WITH (NOLOCK)   ON tarNationalAcct.NationalAcctKey              = tarNationalAcctLevel.NationalAcctKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciAddress              WITH (NOLOCK)   ON tciAddress.AddrKey                           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipLine             WITH (NOLOCK)   ON tsoShipLine.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSOLine               WITH (NOLOCK)   ON tsoSOLine.SOLineKey                          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoSalesOrder           WITH (NOLOCK)   ON tsoSalesOrder.SOKey                          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN   tciShipMethod           WITH (NOLOCK)   ON tciShipMethod.ShipMethKey                    = tsoPendShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoPickList             WITH (NOLOCK)   ON tsoPickList.PickListKey                      = tsoShipLine.PickListKey" & vbCrLf & _
            " LEFT OUTER JOIN   tsoShipment             WITH (NOLOCK)   ON tsoShipment.ShipKey                          = tsoPendShipment.ShipKey" & vbCrLf

    sSQL = sSQL & " WHERE tsoPendShipment.TranType = " & kTranTypeSODS & _
                " AND tsoPendShipment.CompanyID = " & gsQuoted(msCompanyID) & _
                " AND ((tarCustomer.Hold     = 1 AND  tarCustomer.Hold IS NOT NULL) " & _
                "   OR (tsoSalesOrder.Hold   = 1 AND tsoSalesOrder.Hold IS NOT NULL) " & _
                "   OR (tsoSalesOrder.CrHold = 1 AND tsoSalesOrder.CrHold IS NOT NULL) " & _
                "   OR (tarNationalAcct.Hold = 1 AND tarNationalAcct.Hold IS NOT NULL))"
    
    If Len(Trim(sWhereClause)) > 0 Then
        sSQL = sSQL & " AND " & sWhereClause & vbCrLf
    End If
    
    sSQL = sSQL & ")"

    Set rs = moClass.moAppDB.OpenRecordset(sSQL, SNAPSHOT, kOptionNone)
    If Not rs.IsEmpty Then
        mlHoldCount = mlHoldCount + rs.Field("HoldCount")
    End If
    rs.Close
    Set rs = Nothing

End Sub

Private Sub m_SelectTransDropShip_SelectTransactions(ByVal sWhereClause As String)
    SelectDropShipTrans sWhereClause
End Sub

Private Sub m_SelectTransDropShip_DeSelectTransactions(ByVal sWhereClause As String)

    Debug.Print "Deselect Transactions"
    Dim sSQL As String
    
    On Error GoTo ErrorRoutine
    
    If InStr(1, sWhereClause, "tsoShipment.PostDate", vbTextCompare) = 0 Then
        sSQL = " DELETE  " & kDropShipTmpTblName
    
        sSQL = sSQL & " FROM  " & kDropShipTmpTblName & vbCrLf & _
                " INNER JOIN          tsoPendShipment   WITH (NOLOCK)       ON tsoPendShipment.ShipKey      = " & kDropShipTmpTblName & ".ShipmentKey" & vbCrLf & _
                " LEFT OUTER JOIN     timWarehouse      WITH (NOLOCK)       ON timWarehouse.WhseKey         = tsoPendShipment.WhseKey" & vbCrLf & _
                " LEFT OUTER JOIN     tarCustomer       WITH (NOLOCK)       ON tarCustomer.CustKey          = tsoPendShipment.CustKey" & vbCrLf & _
                " LEFT OUTER JOIN     tciAddress        WITH (NOLOCK)       ON tciAddress.AddrKey           = tsoPendShipment.ShipToAddrKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoShipLine       WITH (NOLOCK)       ON tsoShipLine.ShipKey          = tsoPendShipment.ShipKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoSOLine         WITH (NOLOCK)       ON tsoSOLine.SOLineKey          = tsoShipLine.SOLineKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoSalesOrder     WITH (NOLOCK)       ON tsoSalesOrder.SOKey          = tsoSOLine.SOKey" & vbCrLf & _
                " LEFT OUTER JOIN     tciShipMethod     WITH (NOLOCK)       ON tciShipMethod.ShipMethKey    = tsoPendShipment.ShipMethKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoPickList       WITH (NOLOCK)       ON tsoPickList.PickListKey      = tsoShipLine.PickListKey" & vbCrLf & _
                " LEFT OUTER JOIN     tsoShipment       WITH (NOLOCK)       ON tsoShipment.ShipKey          = tsoPendShipment.ShipKey" & vbCrLf
        
        If Len(Trim(sWhereClause)) > 0 Then
            sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
        End If
        
        moClass.moAppDB.ExecuteSQL sSQL
        
    End If

    sSQL = " DELETE  " & kDropShipTmpTblName

    sSQL = sSQL & " FROM  " & kDropShipTmpTblName & vbCrLf & _
            " INNER JOIN          tsoShipment       WITH (NOLOCK)       ON tsoShipment.ShipKey      = " & kDropShipTmpTblName & ".ShipmentKey" & vbCrLf & _
            " LEFT OUTER JOIN     timWarehouse      WITH (NOLOCK)       ON timWarehouse.WhseKey         = tsoShipment.WhseKey" & vbCrLf & _
            " LEFT OUTER JOIN     tarCustomer       WITH (NOLOCK)       ON tarCustomer.CustKey          = tsoShipment.CustKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciAddress        WITH (NOLOCK)       ON tciAddress.AddrKey           = tsoShipment.ShipToAddrKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoShipLine       WITH (NOLOCK)       ON tsoShipLine.ShipKey          = tsoShipment.ShipKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSOLine         WITH (NOLOCK)       ON tsoSOLine.SOLineKey          = tsoShipLine.SOLineKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoSalesOrder     WITH (NOLOCK)       ON tsoSalesOrder.SOKey          = tsoSOLine.SOKey" & vbCrLf & _
            " LEFT OUTER JOIN     tciShipMethod     WITH (NOLOCK)       ON tciShipMethod.ShipMethKey    = tsoShipment.ShipMethKey" & vbCrLf & _
            " LEFT OUTER JOIN     tsoPickList       WITH (NOLOCK)       ON tsoPickList.PickListKey      = tsoShipLine.PickListKey" & vbCrLf

    If Len(Trim(sWhereClause)) > 0 Then
        sWhereClause = Replace(sWhereClause, "tsoPendShipment", "tsoShipment")
        sSQL = sSQL & " WHERE " & sWhereClause & vbCrLf
    End If
    
    moClass.moAppDB.ExecuteSQL sSQL

    moClass.moAppDB.CommitTrans
    
    Exit Sub

ErrorRoutine:

    moClass.moAppDB.Rollback
    
    gSetSotaErr Err, sMyName, "m_SelectTransCustShip_DeSelectTransactions", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
            Err.Raise guSotaErr.Number
    Case Else
            Call giErrorHandler: Exit Sub
    End Select
    
End Sub

Private Sub m_SelectTransDropShip_Proceed(ByVal lSettingKey As Long)
    'Refresh settings combo and set to the lSettingkey
    Debug.Print "m_SelectTransDropShip_Proceed for " & lSettingKey
    
    UpdateSOCommitTmpTbl kTranTypeSODS
    SetEnabledFromSelected kTranTypeSODS
    
    m_DropShipSelectSetting.RefreshSettingList
    RefreshGridColSelectSetting kTranTypeSODS, lSettingKey
    
    If mlHoldCount > 0 Then
        sbrMain.Message = "Some shipments were not selected due to Hold or Credit Hold reasons."
    End If
End Sub



