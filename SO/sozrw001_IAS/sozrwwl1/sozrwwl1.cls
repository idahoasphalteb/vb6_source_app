VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsTranReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'************************************************************************************
'     Name: clsReport
'     Desc: The clsReport class handles the interface for
'           the framework and the VB server
'Copyright: Copyright (c) 1995-2005 Best Software, Inc.
' Original: 02-25-05
'     Mods:
'************************************************************************************
Option Explicit

    '=======================================================================
    '           Public Variables for use by other modules
    '           -----------------------------------------
    '
    '   moFramework     - required to give other modules the ablitiy to
    '                     call the framework methods and properties, such
    '                     as UnloadSelf, LoadSotaObject, . . .
    '                     Object reference is set in InitializeObject.
    '
    '   mlContext       - Contains information of how this class object
    '                     was invoked.  It is set in InitializeObject
    
    '
    '
    '   moSysSession    - system Manager Session Object which contains
    '                     information specific to the user's session with
    '                     this accounting application.  For example, current
    '                     CompanyID, Business date, UserID, UserName, . . .
    '
    '   miShutDownRequester
    '                   - The ShutDownRequester is the object responsible
    '                     for requesting this object to shut down.  There are
    '                     two possible choices: 1) the Framework requests the
    '                     object to shut itself down (kFrameworkShutDown; and
    '                     2) the User Interface or UnloadSelf call requests
    '                     the framework to shut this object down (kUnloadSelfShutDown).
    '                     The main purpose of this flag is to
    '                     ensure that the framework reference isn't removed (or
    '                     set to nothing) before the UI.
    '=======================================================================
    Public moFramework          As Object
    Public mlContext            As Long
    Public moDasSession         As Object
    Public moAppDB              As Object
    Public moSysSession         As Object
    Public miShutDownRequester  As Integer
    Public mlError              As Long
    
    'Private Variables for Class Properties
    Private mlRunFlags          As Long
    Private mlUIActive          As Long
    Private mbPeriodEnd         As Boolean
    Private mbBatchless         As Boolean
    Private mlProcessType       As Long
    Private mbIsDLL             As Boolean
    Private mbReport            As Boolean
    
    'Private variables for use within this class only
    Public mfrmMain             As Object

Const VBRIG_MODULE_ID_STRING = "SOZRWWL1.CLS"

Public Property Let lUIActive(lNewActive As Long)
'+++ VB/Rig Skip +++
    mlUIActive = lNewActive
End Property

Public Property Get lUIActive() As Long
'+++ VB/Rig Skip +++
    lUIActive = mlUIActive
End Property

Public Property Get lRunFlags() As Long
'+++ VB/Rig Skip +++
    lRunFlags = mlRunFlags
End Property

Public Property Let lRunFlags(ltRunFlags As Long)
'+++ VB/Rig Skip +++
    mlRunFlags = ltRunFlags
End Property

Public Property Set frmMain(frm As Object)
'+++ VB/Rig Skip +++
    Set mfrmMain = frm
End Property

Public Property Get frmMain() As Object
'+++ VB/Rig Skip +++
    Set frmMain = mfrmMain
End Property

Public Property Let bPeriodEnd(bSetPeriodEnd As Boolean)
'+++ VB/Rig Skip +++
    mbPeriodEnd = bSetPeriodEnd
End Property

Public Property Get bPeriodEnd() As Boolean
'+++ VB/Rig Skip +++
    bPeriodEnd = mbPeriodEnd
End Property

Public Property Get Report() As Boolean
'+++ VB/Rig Skip +++
    Report = mbReport
End Property

Private Sub Class_Initialize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ClassInitialize Me
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Initialize", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Private Sub Class_Terminate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ClassTerminate
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "Class_Terminate", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Sub

Public Function InitializeObject(ByVal oFramework As Object, ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    InitializeObject = kFailure
    DefaultInitializeObject Me, oFramework, lContext, App.ProductName, App.Title
    mbPeriodEnd = False
    InitializeObject = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "InitializeObject", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function LoadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    LoadUI = kFailure

    Set mfrmMain = frmSOTranReport
    Set mfrmMain.oClass = Me
    mfrmMain.lRunMode = mlContext And kRunModeMask
    
    Load mfrmMain
    If mlError Then Err.Raise mlError
    
    If mfrmMain.bLoadSuccess Then
        LoadUI = kSuccess
    Else
        LoadUI = EFW_C_FAIL
    End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "LoadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function GetUIHandle(ByVal ltContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    GetUIHandle = kFailure
    If Not ClassGetUIHandle(Me, ltContext) > 0 Then Exit Function
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "GetUIHandle", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function DisplayUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
    DisplayUI = EFW_CT_MODALEXIT
#Else
    DisplayUI = kFailure
    If ClassDisplayUI(Me, lContext) = kFailure Then Exit Function
    DisplayUI = kSuccess
#End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "DisplayUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function ShowUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ShowUI = kFailure
    If ClassShowUI(Me, lContext) = kFailure Then Exit Function
    ShowUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ShowUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function QueryShutDown(lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    QueryShutDown = kFailure
    If ClassQueryShutdown(Me, lContext) = kFailure Then Exit Function
    QueryShutDown = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "QueryShutDown", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function MinimizeUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    #If InProc = 1 Then
    mfrmMain.WindowState = vbMinimized
#Else
    MinimizeUI = kFailure
    If ClassMinimizeUI(Me, lContext) = kFailure Then Exit Function
    MinimizeUI = kSuccess
#End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "MinimizeUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function RestoreUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    RestoreUI = kFailure
    If ClassRestoreUI(Me, lContext) = kFailure Then Exit Function
    RestoreUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "RestoreUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function HideUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
#If InProc = 1 Then
    HideUI = kSuccess
#Else
    HideUI = kFailure
    If ClassHideUI(Me, lContext) = kFailure Then Exit Function
    HideUI = kSuccess
#End If
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "HideUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function UnloadUI(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    UnloadUI = kFailure
    If ClassUnloadUI(Me, lContext) = kFailure Then Exit Function
    UnloadUI = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "UnloadUI", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function TerminateObject(ByVal lContext As Long) As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    TerminateObject = kFailure
    If ClassTermObj(Me, lContext) = kFailure Then Exit Function
    TerminateObject = kSuccess
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "TerminateObject", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Function

Public Function lGetSettings(ByRef sSetting, ByRef sSelect, Optional oResponse As Variant) As Long
'************************************************************************************
'      Desc: Retrieve the report settings as a string.
'     Parms: sSetting - Output; String of saved setting id/setting key pairs.
'            sSelect - Output; String of saved setting key/select info pairs.
'            oResponse as Variant - Optional object used for debugging
'   Returns: kSuccess; kFailure
'            sSetting - Each setting is a string and a key. Each string and key is
'            separated by a tab (vbTab). Each setting is delimited by a CRLF (vbCrLf).


'            The first setting in the list is the most recently used by the user.
'            If no settings class exists, then empty string " is returned.
'            If error, then empty string " is returned.
'            sSelect - Each setting is a key and selection row information.
'            Each row is separated by a semicolon. Values within a row
'            are separated by commas.  Each row contains setting key,
'            caption, operator, start value, end value.  E.g.,
'            <setting key>, <caption>, <opertor>,<start value>, <end value>;
'            <setting key>, <caption>, <opertor>,<start value>, <end value>.
'            If no settings, then empty string (") is returned.
'   Assumes: Settings class proc Initialize has been run.
'            Selection control proc bInitSelect has been run.
'************************************************************************************
    lGetSettings = kFailure

    On Error Resume Next
    sSetting = mfrmMain.moSettings.sGetSettings()
    If Err <> 0 Then
        sSetting = ""
    End If
    
    sSelect = mfrmMain.moSelect.sGetSettings()
    If Err <> 0 Then
        sSelect = ""
    End If
    
    lGetSettings = kSuccess
End Function

Public Sub GenerateReport(iFileType As Integer, sFileName As String, _
    Optional lSettingKey As Variant, Optional sSelect As Variant, _
    Optional oResponse As Variant)
'************************************************************************************
'      Desc: Generate web report file by calling HandleToolbarClick.
'     Parms: iFileType as Integer - File type to create (0 = HTML, 1 = RPT)
'            sFileName as String - File name to create (including path)
'            lSettingKey as Variant - Optional; setting selected by user
'            sSelect as Variant - Optional; selection grid values
'            oResponse as Variant - Optional object used for debugging
'************************************************************************************
    '    Get the select, sort, and options for given setting.
    If Not IsMissing(lSettingKey) Then
        If lSettingKey <> 0 Then
            mfrmMain.moSettings.bLoadSettingsByKey (CLng(lSettingKey))
        End If
    End If
    
    '   Put Web Client Selection Grid values on Web Server Selection Grid
    If Not IsMissing(sSelect) Then
        On Error Resume Next    '   if selection grid does not exist
        mfrmMain.moSelect.lPutSelectGrid sSelect
        On Error GoTo 0     '   normal error processing
    End If
    
    mfrmMain.HandleToolBarClick kTbPreview, iFileType, sFileName
End Sub

Public Function sGetSelection(Optional oResponse As Variant) As String
'************************************************************************************
'      Desc: Retrieve the report selection grid information as a string.
'     Parms: oResponse as Variant - Optional object used for debugging
'   Returns: String of selection grid information.
'            String of variable values, selection array values, and selection grid values.
'            Variable values, selection array values, and selection grid values
'            are separated from each other by two semicolons.
'            Variable values are separated from each other by a comma.
'            Each selection array row is separated by a semicolon.  Values in the
'            selection array are separated by a comma.
'            Each grid row is separated by a semicolon. Values in the selection
'            grid row are separated by a comma.
'            Assumes: Selection Grid's proc bInitSelect has been run.
'************************************************************************************
    On Error Resume Next
    sGetSelection = mfrmMain.moSelect.sGetSelection()
    If Err <> 0 Then
        sGetSelection = ""
    End If
End Function
    

Public Function PrintTranReport(Optional Report As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +

    mbBatchless = True
    mlProcessType = 2
    mbReport = Report
    
    mbIsDLL = True
        
    If mfrmMain Is Nothing Then
        '-- Manually load the UI
        If moFramework.LoadUI(Me) = kFailure Then
            Exit Function
        End If
    End If
    
    With frmSOTranReport
        .tabReport.Tab = 0
        .miMode = 1
        .fraSort.Top = .fraPrint.Top
        .fraSort.ZOrder
        .grdSelectTrans.Enabled = False
        .fraPrint.Enabled = False
    End With
    
    With frmSOTranReport
        .Show vbModal
    End With
    
    '-- Check for any error that might have occurred
    If mlError Then
        Err.Raise mlError
    End If
        
    PrintTranReport = True

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintTranReport", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Function

Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "clsTranReport"
End Function

Public Property Get ProcessType() As Long
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    ProcessType = mlProcessType
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "ProcessType_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Get IsDLL() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigLogErrorRoutine
#End If
'+++ VB/Rig End +++
    IsDLL = mbIsDLL
'+++ VB/Rig Begin Pop +++
        Exit Property

VBRigLogErrorRoutine:
        gMainClassErr Err, sMyName, "IsDLL_Get", VBRIG_IS_CLASS
        Resume Next
'+++ VB/Rig End +++
End Property

Public Property Set AppDB(oAppDB As Object)
    Set moAppDB = oAppDB
End Property

'***************************************
Public Function PrintTranReport4DCD(sPathToPrint As String, Optional Report As Boolean = False) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +
gbWorkFromDCD = True
gsPathToPrint = sPathToPrint
    mbBatchless = True
    mlProcessType = 2
    mbReport = Report
    
    mbIsDLL = True
        
    If mfrmMain Is Nothing Then
        '-- Manually load the UI
        If moFramework.LoadUI(Me) = kFailure Then
            Exit Function
        End If
    End If
    
    With frmSOTranReport
        .tabReport.Tab = 0
        .miMode = 1
        .fraSort.Top = .fraPrint.Top
        .fraSort.ZOrder
        .grdSelectTrans.Enabled = False
        .fraPrint.Enabled = False
    End With
    
    With frmSOTranReport
       ' .Show vbModal
       
        .HandleToolBarClick kTbPreview
    End With
    
    '-- Check for any error that might have occurred
    If mlError Then
        Err.Raise mlError
    End If
        
    PrintTranReport4DCD = True

'+++ VB/Rig Begin Pop +++
    Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PrintTranReport", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Function
