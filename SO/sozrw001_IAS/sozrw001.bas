Attribute VB_Name = "basReport"
#Const NEWGL = True
'***********************************************************************
'     Name: bas{Name} (source: Template)
'     Desc: Main Module of this Object containing "Sub Main ()".
'Copyright: Copyright (c) 1995-2005 Best Software, Inc.
' Original: 02-25-05
'     Mods:
'***********************************************************************
Option Explicit

Private iIntegratedWithMC As Integer

Public gbSkipPrintDialog As Boolean      '   whether to show Print Dialog window. HBS.
    
' global constants for SO batch types
Global Const BATCH_TYPE_SHIP = 1      ' Shipment
Global Const BATCH_TYPE_CUSTRET = 2   ' Customer Return


Public Type uLocalizedStrings
    NotPrinted      As String
    Printed         As String
    FatalErrors     As String
    NotBalanced     As String
    Balanced        As String
    posting         As String
    posted          As String
    PostingFailed   As String
    JournalDesc     As String
    SOJournalDesc   As String
    ProcessingRegister As String
    PrintingRegister As String
    RegisterPrinted As String
    PostingComplete As String
    PostingModule As String
    ModulePosted As String
    Validating As String
    PrintError As String
    PrintErrorCompleted As String
    ProcessingGLReg As String
    CheckingBatch As String
    PostingModule2 As String
    ModulePosted2 As String
    GLPosting As String
    GLPosted As String
End Type


Const VBRIG_MODULE_ID_STRING = "SOZRW001.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'     Desc: Main is required as the standard 'Startup Form'.
'           Main can be empty.
'    Parms: N/A
'  Returns: N/A
'***********************************************************************
    
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("sozrw001.clsTranReport")
        StartAppInStandaloneMode oApp, Command$()
    End If
    
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

   
Public Sub BuildLocalizedStrings(SysDB As Object, SysSession As Object, guLocalizedStrings As uLocalizedStrings, frm As Form)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    With guLocalizedStrings
        .NotPrinted = gsBuildString(kJrnlNotPrinted, SysDB, SysSession)
        .Printed = gsBuildString(kJrnlPrinted, SysDB, SysSession)
        .FatalErrors = gsBuildString(kJrnlFatalErrors, SysDB, SysSession)
        .NotBalanced = gsBuildString(kJrnlNotBalanced, SysDB, SysSession)
        .Balanced = gsBuildString(kJrnlBalanced, SysDB, SysSession)
        .posting = gsBuildString(kJrnlPosting, SysDB, SysSession)
        .posted = gsBuildString(kJrnlPosted, SysDB, SysSession)
        .PostingFailed = gsBuildString(kJrnlPostingFailed, SysDB, SysSession)
        
        'Assign JournalDesc variable value based on the Register being ran.
        If frm.miBatchType = BATCH_TYPE_SHIP Then
                .JournalDesc = gsBuildString(kSOShipping, SysDB, SysSession)
        Else  'BATCH_TYPE_CUSTRET
                .JournalDesc = gsBuildString(kSOCRReturn, SysDB, SysSession)
        End If
                
        .SOJournalDesc = gsBuildString(kARZJA001Invoice, SysDB, SysSession)
        .ProcessingRegister = gsBuildString(kARZJA001ProcessRegister, SysDB, SysSession)
        .PrintingRegister = gsBuildString(kARZJA001PrintingRegister, SysDB, SysSession)
        .RegisterPrinted = gsBuildString(kARZJA001RegisterPrinted, SysDB, SysSession)
        .PostingComplete = gsBuildString(kARZJA001PostingComplete, SysDB, SysSession)
        .PostingModule = gsBuildString(kVSDelCustKey, SysDB, SysSession)
        .ModulePosted = gsBuildString(kVCustIactive, SysDB, SysSession)
        .Validating = gsBuildString(kARZJA001Validating, SysDB, SysSession)
        .PrintError = gsBuildString(kARZJA001PrintError, SysDB, SysSession)
        .PrintErrorCompleted = gsBuildString(kARZJA001PrintErrorCompleted, SysDB, SysSession)
        .ProcessingGLReg = gsBuildString(kARZJA001ProcessingGLReg, SysDB, SysSession)
        .CheckingBatch = gsBuildString(kARZJA001CheckingBatch, SysDB, SysSession)
        .PostingModule2 = gsBuildString(kPostingModule, SysDB, SysSession)
        .ModulePosted2 = gsBuildString(kModulePosted, SysDB, SysSession)
        .GLPosting = gsBuildString(kGLPosting, SysDB, SysSession)
        .GLPosted = gsBuildString(kGLPostingComplete, SysDB, SysSession)
    End With

        'Get user Multi_Curr Flag from AR Options
        iIntegratedWithMC = giGetValidInt(frm.oClass.moAppDB.Lookup("UseMultCurr", "tarOptions", "CompanyID = " & gsQuoted(frm.msCompanyID)))
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BuildLocalizedStrings", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub



Private Function sMyName() As String
'+++ VB/Rig Skip +++
        sMyName = "basReport"
End Function


Public Sub StartShipRegSum(sButton As String, frm As Form)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim iRetVal As Integer
Dim sTitle1 As String, sTitle2 As String
Dim i As Long

Dim l As Long
Dim sTableColumnName As String
Dim sDescription As String
Dim iPageBreak As Integer
Dim iDescending As Integer
Dim sColumnName As String
Dim iSeqNo As Integer
Dim sTableName As String
Dim sParentTableName As String
Dim iSub As Integer

    ShowStatusBusy frm
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    With frm
        If .miBatchType = BATCH_TYPE_SHIP Then
            .moReport.ReportFileName() = "sozdt001.rpt"
        Else
            .moReport.ReportFileName() = "sozdv001.rpt"
        End If
    End With
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
                                   
    If frm.oClass.Report = True Then
        If frm.miBatchType = BATCH_TYPE_SHIP Then
            sTitle1 = gsBuildString(kSOShippingReport, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Else
            sTitle1 = gsBuildString(kSOCustReturnReport, frm.oClass.moAppDB, frm.oClass.moSysSession)
        End If
    Else
        If frm.miBatchType = BATCH_TYPE_SHIP Then
            sTitle1 = gsBuildString(kSOShipRegRptTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Else
            sTitle1 = gsBuildString(kSOCustRegRptTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
        End If
    End If

    With frm
        .moReport.ReportTitle1() = sTitle1
        .moReport.ReportTitle2() = ""
        
        'automated means of labeling subtotal fields.
        'this is not manditory if you choose not to use this feature
        If .miBatchType = BATCH_TYPE_SHIP Then
            .moReport.UseSubTotalCaptions() = 1
            .moReport.UseHeaderCaptions() = 1
        Else
            .moReport.UseSubTotalCaptions() = 1
            .moReport.UseHeaderCaptions() = 1
        End If

        'set standard formulas, business date, run time, company name etc.
        'as defined in the template
        If (.moReport.lSetStandardFormulas(frm) = kFailure) Then
            MsgBox "SetStandardFormulas failed"
            GoTo badexit
        End If
        
        'Set formula for Return Register
        If .miBatchType <> BATCH_TYPE_SHIP Then
            On Error Resume Next

            If Not .moReport.bSetReportFormula("MCIntegrated", CStr(iIntegratedWithMC)) Then
                .moReport.ShowCrystalError
                Exit Sub
            End If
        End If
        
        On Error GoTo badexit
        
'frm.moreport.getsqlquery
        'translate sort selections into Crystal commands
        If .miBatchType = BATCH_TYPE_SHIP Then
        
            If (.moReport.lSetSortCriteria(.moSort) = kFailure) Then
                MsgBox "SetSortCriteria failed"
                GoTo badexit
            End If
                        
        Else
            If (.moReport.lSetSortCriteria(frm.moSort) = kFailure) Then
                MsgBox "SetSortCriteria failed"
                GoTo badexit
            End If
        End If
        
    End With
   
 
    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BYs with new ones. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    'If this is necessary make the call, frm.moReport.AppendSQL(", table.column ASC")
    
    'send the formatted SQL statement from above plus any further manipulations
    'to Crystal
    frm.moReport.SetSQL

    'this sub will set any labels for the table fields on the RPT file if they exist.
    'Labels must match the column name from the parent table as well as prefixed by "lbl".
    'For example, to label customer ID from tarCustomer table, a formula on the report
    'must exist with the name, lblCustID. At design time fill this formula with empty quotes.
    'this is not a manitory function. Often the captions from the DD will not be exactly
    'what you want to appear on the report.  If this is the case, there is a function
    'available in the Crystal design environment called FreeFormText that takes a string
    'table constant as a parameter to place text on the report.
    frm.moReport.SetReportCaptions
    
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    'if using work tables, it's necessary to restrict the query to the data produced by your
    '"run".  The engine used to do this behind the scenes but as the engine is used in more
    'different ways now, it's better to make the call explicitly yourself.  This permits
    'restriction by any column and doesn't force the inclusing of a column, SessionID in the
    'work tables.
    'only for the report that not use sort grid
    If frm.miBatchType = BATCH_TYPE_SHIP Then
        If (frm.moReport.lRestrictBy("{tsoShipRegSumWrk.SessionID} = " & frm.moReport.SessionID) = kFailure) Then
            MsgBox "RestrictBy failed"
            GoTo badexit
        End If
    Else
         If (frm.moReport.lRestrictBy("{tsoCustRRegSumWrk.SessionID} = " & frm.moReport.SessionID) = kFailure) Then
            MsgBox "RestrictBy failed"
            GoTo badexit
        End If
    End If
    
    frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog
    frm.sbrMain.Status = SOTA_SB_START
    
    Exit Sub

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartShipRegSum", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub

Public Sub StartShipRegDtl(sButton As String, frm As Form)
'+++ VB/Rig Begin Push +++
'+++ VB/Rig End +++
Dim iRetVal                    As Integer
Dim sTitle1 As String, sTitle2 As String
Dim sAcctRefCodeLabel          As String
Dim iAcctRefCodeStr            As Long
Dim iQtyDecimals               As Integer
 
    ShowStatusBusy frm
    
    On Error GoTo badexit
    
    '*************** NOTE ********************
    'the order of the following events is important!
    
    'the .RPT file name can be set acording to user selected options
    'this call is only necessary if you need to run a report that is
    'different from the default
    If frm.miBatchType = BATCH_TYPE_SHIP Then
        frm.moReport.ReportFileName() = "sozdt002.rpt"
    Else
        frm.moReport.ReportFileName() = "sozdv002.rpt"
    End If
    
    'start Crystal print engine, open print job
    'get strings for report
    If (frm.moReport.lSetupReport = kFailure) Then
        MsgBox "SetupReport failed"
        GoTo badexit
    End If
    
    'work around if you print without previewing first.
    'Crystal does not provide a way of getting page orientation
    'used to create report. use VB constants:
    'vbPRORPortrait, vbPRORLandscape
    frm.moReport.Orientation() = vbPRORLandscape
    
    If frm.oClass.Report = True Then
        If frm.miBatchType = BATCH_TYPE_SHIP Then
            sTitle1 = gsBuildString(kSOShippingReport, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Else
            sTitle1 = gsBuildString(kSOCustReturnReport, frm.oClass.moAppDB, frm.oClass.moSysSession)
        End If
    Else
        If frm.miBatchType = BATCH_TYPE_SHIP Then
            sTitle1 = gsBuildString(kSOShipRegRptTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
        Else
            sTitle1 = gsBuildString(kSOCustRegRptTitle, frm.oClass.moAppDB, frm.oClass.moSysSession)
        End If
    End If
    
    frm.moReport.ReportTitle1() = sTitle1
    frm.moReport.ReportTitle2() = ""
    
    'automated means of labeling subtotal fields.
    'this is not manditory if you choose not to use this feature
    frm.moReport.UseSubTotalCaptions() = 1
    frm.moReport.UseHeaderCaptions() = 1
        
    'set standard formulas, business date, run time, company name etc.
    'as defined in the template
    If (frm.moReport.lSetStandardFormulas(frm) = kFailure) Then
        MsgBox "SetStandardFormulas failed"
        GoTo badexit
    End If
    
    'translate sort selections into Crystal commands
    If (frm.moReport.lSetSortCriteria(frm.moSort) = kFailure) Then
        MsgBox "SetSortCriteria failed"
        GoTo badexit
    End If
   
    '********* the following is specific to this report *************'
    'Call to grab the original SQL statement from Crystal and
    'modify it according to user actions in the sort grid.
    'Replaces original ORDER BYs with new ones. Appends ORDER BY if not
    'already there. DOES NOT send the SQL statement to Crystal.
    frm.moReport.BuildSQL
    
    '**** Any additional sorts outside of sort grid may be added here ****'
    iQtyDecimals = giGetValidInt(frm.oClass.moAppDB.Lookup("QtyDecPlaces", "tciOptions", "CompanyID = " & gsQuoted(frm.msCompanyID)))
    
    If frm.miBatchType = BATCH_TYPE_CUSTRET Then
        If frm.moReport.bSetReportFormula("QtyDecimals", CStr(iQtyDecimals)) = 0 Then
                GoTo badexit
        End If
    End If
    
    'Set formula for Return Register
    If frm.miBatchType <> BATCH_TYPE_SHIP Then
        On Error Resume Next
        If Not frm.moReport.bSetReportFormula("MCIntegrated", CStr(iIntegratedWithMC)) Then
            frm.moReport.ShowCrystalError
            Exit Sub
        End If
    End If
    
    On Error GoTo badexit
    
    'send the formatted SQL statement from above plus any further manipulations to Crystal
    frm.moReport.SetSQL

    'this sub will set any labels for the table fields on the RPT file if they exist.
    'Labels must match the column name from the parent table as well as prefixed by "lbl".
    'For example, to label customer ID from tarCustomer table, a formula on the report
    'must exist with the name, lblCustID. At design time fill this formula with empty quotes.
    'this is not a manitory function. Often the captions from the DD will not be exactly
    'what you want to appear on the report.  If this is the case, there is a function
    'available in the Crystal design environment called FreeFormText that takes a string
    'table constant as a parameter to place text on the report.
    frm.moReport.SetReportCaptions
    
    'set the summary section of the report: user id,
    'sort/select criteria etc. 1 displays section, 0 hides
    If (frm.moReport.lSetSummarySection(frm.chkSummary.Value, frm.moOptions, 1) = kFailure) Then
        MsgBox "SetSummarySection failed"
        GoTo badexit
    End If
    
    'if using work tables, it's necessary to restrict the query to the data produced by your
    '"run".  The engine used to do this behind the scenes but as the engine is used in more
    'different ways now, it's better to make the call explicitly yourself.  This permits
    'restriction by any column and doesn't force the inclusing of a column, SessionID in the
    'work tables.
    'only for the report, This not used for sort grid
    If frm.miBatchType = BATCH_TYPE_SHIP Then
        If (frm.moReport.lRestrictBy("{tsoShipRegDtlWrk.SessionID} = " & frm.moReport.SessionID) = kFailure) Then
            MsgBox "RestrictBy failed"
            GoTo badexit
        End If
    Else
       If (frm.moReport.lRestrictBy("{tsoCustRRegDtlWrk.SessionID} = " & frm.moReport.SessionID) = kFailure) Then
            MsgBox "RestrictBy failed"
            GoTo badexit
        End If
    End If
    
    frm.moReport.ProcessReport frm, sButton, , , gbSkipPrintDialog
    frm.sbrMain.Status = SOTA_SB_START
    
    Exit Sub

badexit:
    frm.sbrMain.Status = SOTA_SB_START
    
    gClearSotaErr
    Exit Sub
    
'+++ VB/Rig Begin Pop +++
#If ERRORTRAPON = 0 Then
Err.Raise Err
#End If
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "StartShipRegDtl", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub



