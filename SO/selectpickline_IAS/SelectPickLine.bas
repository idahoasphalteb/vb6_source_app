Attribute VB_Name = "basSelectPick"
'**********************************************************************
'     Name: basSelectPick
'     Desc: Main Module of this Object containing "Sub Main ()".
' Original: 03/15/2004
'     Mods: mm/dd/yy XXX
'**********************************************************************
Option Explicit

Public miCreditHoldAutoRelease As Integer
   
Const VBRIG_MODULE_ID_STRING = "SelectPickLine.BAS"

Public Sub Main()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If App.StartMode = vbSModeStandalone Then
        Dim oApp As Object
        Set oApp = CreateObject("SOSelectPickLineDLL.clsSelectPick")
        StartAppInStandaloneMode oApp, Command$()
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Main", VBRIG_IS_MODULE
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = "basSelectPick"
End Function

