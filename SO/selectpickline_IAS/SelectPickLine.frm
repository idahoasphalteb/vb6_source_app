VERSION 5.00
Object = "{F856EC8B-F03C-4515-BDC6-64CBD617566A}#8.0#0"; "fpSPR80.OCX"
Object = "{C41A85E3-4CB6-40B5-B425-EE9ECC5E6F06}#154.0#0"; "SOTATbar.ocx"
Object = "{F2F2EE3C-0D23-4FC8-944C-7730C86412E3}#52.0#0"; "sotasbar.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CAF0FDE4-8332-11CF-BC13-0020AFD6738C}#1.0#0"; "newsota.ocx"
Object = "{8A9C5D3D-5A2F-4C5F-A12A-A955C4FB68C8}#84.0#0"; "LookupView.ocx"
Object = "{0FA91D91-3062-44DB-B896-91406D28F92A}#54.0#0"; "SOTACalendar.ocx"
Object = "{BC90D6A3-491E-451B-ADED-8FABA0B8EE36}#47.0#0"; "SOTADropDown.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Begin VB.Form frmSelectPick 
   Caption         =   "Select Orders for Picking"
   ClientHeight    =   7470
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12360
   HelpContextID   =   17774177
   Icon            =   "SelectPickLine.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7470
   ScaleWidth      =   12360
   Begin StatusBar.SOTAStatusBar sbrMain 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Top             =   7080
      WhatsThisHelpID =   73
      Width           =   12360
      _ExtentX        =   21802
      _ExtentY        =   688
      BrowseVisible   =   0   'False
   End
   Begin TabDlg.SSTab tabSelect 
      Height          =   6015
      Left            =   120
      TabIndex        =   3
      Top             =   960
      WhatsThisHelpID =   17774179
      Width           =   12045
      _ExtentX        =   21246
      _ExtentY        =   10610
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "S&election"
      TabPicture(0)   =   "SelectPickLine.frx":23D2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "pnlSelection"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Pi&ck Options"
      TabPicture(1)   =   "SelectPickLine.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "pnlOptions"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Orders Selected"
      TabPicture(2)   =   "SelectPickLine.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "pnlOrdSelected"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&Lines Selected"
      TabPicture(3)   =   "SelectPickLine.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "pnlLineSelected"
      Tab(3).ControlCount=   1
      Begin Threed.SSPanel pnlLineSelected 
         Height          =   5415
         Left            =   -74880
         TabIndex        =   35
         Top             =   420
         Width           =   11775
         _Version        =   65536
         _ExtentX        =   20770
         _ExtentY        =   9551
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraLinePicked 
            Caption         =   "0 Lines Selected For Picking"
            Height          =   5415
            Left            =   0
            TabIndex        =   36
            Top             =   0
            Width           =   11745
            Begin FPSpreadADO.fpSpread grdLinePicked 
               Height          =   5010
               Left            =   120
               TabIndex        =   37
               Top             =   240
               WhatsThisHelpID =   17776325
               Width           =   11475
               _Version        =   524288
               _ExtentX        =   20241
               _ExtentY        =   8837
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "SelectPickLine.frx":2442
               AppearanceStyle =   0
            End
            Begin LookupViewControl.LookupView navGrid 
               Height          =   285
               Left            =   10320
               TabIndex        =   70
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   1800
               Visible         =   0   'False
               WhatsThisHelpID =   17774194
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
         End
      End
      Begin Threed.SSPanel pnlOrdSelected 
         Height          =   5415
         Left            =   -74880
         TabIndex        =   32
         Top             =   420
         Width           =   11775
         _Version        =   65536
         _ExtentX        =   20770
         _ExtentY        =   9551
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraOrderPicked 
            Caption         =   "0 Orders Selected For Picking"
            Height          =   5415
            Left            =   0
            TabIndex        =   33
            Top             =   0
            Width           =   11745
            Begin FPSpreadADO.fpSpread grdOrderPicked 
               Height          =   5025
               Left            =   120
               TabIndex        =   34
               Top             =   240
               WhatsThisHelpID =   17776326
               Width           =   11475
               _Version        =   524288
               _ExtentX        =   20241
               _ExtentY        =   8864
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "SelectPickLine.frx":2856
               AppearanceStyle =   0
            End
         End
      End
      Begin Threed.SSPanel pnlOptions 
         Height          =   5415
         Left            =   -74880
         TabIndex        =   17
         Top             =   420
         Width           =   10215
         _Version        =   65536
         _ExtentX        =   18018
         _ExtentY        =   9551
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame fraQtyPickOpt 
            Caption         =   "Out of Stock Picks"
            Height          =   1095
            Left            =   4560
            TabIndex        =   71
            Top             =   2160
            Width           =   3855
            Begin VB.OptionButton optStockQtyPick 
               Caption         =   "Always Pick Entire Quantity Open to Ship"
               Height          =   255
               Index           =   1
               Left            =   360
               TabIndex        =   73
               Top             =   600
               WhatsThisHelpID =   94401001
               Width           =   3375
            End
            Begin VB.OptionButton optStockQtyPick 
               Caption         =   "Limit Pick to Quantity Available"
               Height          =   195
               Index           =   0
               Left            =   360
               TabIndex        =   72
               Top             =   320
               Value           =   -1  'True
               WhatsThisHelpID =   94401002
               Width           =   2895
            End
         End
         Begin VB.Frame fraPickShipOpt 
            Caption         =   "Picking/S&hipping"
            Height          =   3015
            Left            =   240
            TabIndex        =   18
            Top             =   240
            Width           =   3975
            Begin VB.CheckBox chkOrderLimit 
               Caption         =   "Limit Number of Orders to Pick"
               Height          =   375
               Left            =   240
               TabIndex        =   21
               Top             =   1320
               WhatsThisHelpID =   17774187
               Width           =   2895
            End
            Begin VB.CheckBox chkEmptyRandomBin 
               Caption         =   "Empty Random Bins First"
               Enabled         =   0   'False
               Height          =   375
               Left            =   600
               TabIndex        =   20
               Top             =   855
               WhatsThisHelpID =   17774186
               Width           =   2415
            End
            Begin VB.CheckBox chkEmptyBins 
               Caption         =   "Empty Bins During Picking"
               Height          =   375
               Left            =   240
               TabIndex        =   19
               Top             =   480
               WhatsThisHelpID =   17774185
               Width           =   3015
            End
            Begin VB.VScrollBar spnMaxPickNo 
               Enabled         =   0   'False
               Height          =   275
               Left            =   3360
               Max             =   0
               Min             =   32767
               TabIndex        =   24
               TabStop         =   0   'False
               Top             =   1695
               WhatsThisHelpID =   17774184
               Width           =   255
            End
            Begin NEWSOTALib.SOTANumber txtMaxPickNo 
               Height          =   270
               Left            =   2520
               TabIndex        =   23
               Top             =   1695
               WhatsThisHelpID =   17774886
               Width           =   855
               _Version        =   65536
               _ExtentX        =   1508
               _ExtentY        =   476
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               mask            =   "<ILH>##|,##<ILp0>#"
               text            =   "    0"
               dMaxValue       =   32767
               sIntegralPlaces =   5
               sDecimalPlaces  =   0
            End
            Begin VB.Label lblMaxOrderToPick 
               Caption         =   "Maximum Orders To Pick"
               Enabled         =   0   'False
               Height          =   375
               Left            =   600
               TabIndex        =   22
               Top             =   1740
               Width           =   1935
            End
         End
         Begin VB.Frame fraSASeq 
            Caption         =   "Stock &Allocation Sequence"
            Height          =   1815
            Left            =   4560
            TabIndex        =   25
            Top             =   240
            Width           =   3855
            Begin SOTADropDownControl.SOTADropDown cboSASeq 
               Height          =   315
               Index           =   0
               Left            =   1200
               TabIndex        =   27
               Top             =   360
               WhatsThisHelpID =   17774180
               Width           =   2175
               _ExtentX        =   3836
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   ""
               Sorted          =   0   'False
            End
            Begin SOTADropDownControl.SOTADropDown cboSASeq 
               Height          =   315
               Index           =   1
               Left            =   1200
               TabIndex        =   29
               Top             =   800
               WhatsThisHelpID =   17774181
               Width           =   2175
               _ExtentX        =   3836
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   ""
               Sorted          =   0   'False
            End
            Begin SOTADropDownControl.SOTADropDown cboSASeq 
               Height          =   315
               Index           =   2
               Left            =   1200
               TabIndex        =   31
               Top             =   1200
               WhatsThisHelpID =   17774182
               Width           =   2175
               _ExtentX        =   3836
               _ExtentY        =   556
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Text            =   ""
               Sorted          =   0   'False
            End
            Begin VB.Label lblThird 
               Caption         =   "Third:"
               Height          =   255
               Left            =   240
               TabIndex        =   30
               Top             =   1280
               Width           =   735
            End
            Begin VB.Label lblSecond 
               Caption         =   "Second:"
               Height          =   300
               Left            =   240
               TabIndex        =   28
               Top             =   885
               Width           =   615
            End
            Begin VB.Label lblFirst 
               Caption         =   "First:"
               Height          =   255
               Left            =   240
               TabIndex        =   26
               Top             =   480
               Width           =   735
            End
         End
      End
      Begin Threed.SSPanel pnlSelection 
         Height          =   5415
         Left            =   120
         TabIndex        =   4
         Top             =   420
         Width           =   11895
         _Version        =   65536
         _ExtentX        =   20981
         _ExtentY        =   9551
         _StockProps     =   15
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BevelOuter      =   0
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'None
            Height          =   1815
            Left            =   9360
            TabIndex        =   59
            Top             =   3360
            Width           =   2535
            Begin VB.Label lblNoOfSOLineSelected 
               Alignment       =   1  'Right Justify
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   0
               TabIndex        =   69
               Top             =   240
               Width           =   495
            End
            Begin VB.Label lblSOSelected 
               Caption         =   "Sales Order(s) Selected"
               Height          =   255
               Left            =   555
               TabIndex        =   68
               Top             =   0
               Width           =   1695
            End
            Begin VB.Label lblSOLinesSelected 
               Caption         =   "SO Line(s) Selected"
               Height          =   255
               Left            =   555
               TabIndex        =   67
               Top             =   240
               Width           =   1695
            End
            Begin VB.Label lblNoOfTOSelected 
               Alignment       =   1  'Right Justify
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   0
               TabIndex        =   66
               Top             =   720
               Width           =   495
            End
            Begin VB.Label lblTOLinesSelected 
               Caption         =   "TO Line(s) Selected"
               Height          =   255
               Left            =   555
               TabIndex        =   65
               Top             =   960
               Width           =   1695
            End
            Begin VB.Label lblNoOfSOSelected 
               Alignment       =   1  'Right Justify
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   0
               TabIndex        =   64
               Top             =   0
               Width           =   495
            End
            Begin VB.Label lblNoOfTOLineSelected 
               Alignment       =   1  'Right Justify
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   0
               TabIndex        =   63
               Top             =   960
               Width           =   495
            End
            Begin VB.Label lblTOSelected 
               Caption         =   "Transfer Order(s) Selected"
               Height          =   255
               Left            =   555
               TabIndex        =   62
               Top             =   720
               Width           =   1935
            End
            Begin VB.Label lblSOOnHold 
               Caption         =   "Sales Order(s) On Hold"
               Height          =   255
               Left            =   555
               TabIndex        =   61
               Top             =   1440
               Width           =   1695
            End
            Begin VB.Label lblNoOfSOOnHold 
               Alignment       =   1  'Right Justify
               Caption         =   "0"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   0
               TabIndex        =   60
               Top             =   1440
               Width           =   495
            End
         End
         Begin VB.Frame fraSelInclude 
            Height          =   3225
            Left            =   9600
            TabIndex        =   5
            Top             =   0
            Width           =   2055
            Begin VB.Frame fraInclude 
               Caption         =   "I&nclude"
               Height          =   1755
               Left            =   110
               TabIndex        =   6
               Top             =   240
               Width           =   1840
               Begin VB.CheckBox chkInclude 
                  Caption         =   "Non-Inventory"
                  Height          =   255
                  Index           =   3
                  Left            =   120
                  TabIndex        =   10
                  Top             =   1290
                  WhatsThisHelpID =   17774188
                  Width           =   1575
               End
               Begin VB.CheckBox chkInclude 
                  Caption         =   "Transfer Orders"
                  Height          =   255
                  Index           =   1
                  Left            =   120
                  TabIndex        =   8
                  Top             =   590
                  Value           =   1  'Checked
                  WhatsThisHelpID =   17774189
                  Width           =   1455
               End
               Begin VB.CheckBox chkInclude 
                  Caption         =   "Sales Orders"
                  Height          =   255
                  Index           =   0
                  Left            =   120
                  TabIndex        =   7
                  Top             =   240
                  Value           =   1  'Checked
                  WhatsThisHelpID =   17774190
                  Width           =   1215
               End
               Begin VB.CheckBox chkInclude 
                  Caption         =   "Out of Stock Picks"
                  Height          =   255
                  Index           =   2
                  Left            =   120
                  TabIndex        =   9
                  Top             =   940
                  WhatsThisHelpID =   17774191
                  Width           =   1628
               End
            End
            Begin VB.CommandButton cmdSelect 
               Caption         =   "&Select"
               Height          =   375
               Left            =   300
               TabIndex        =   11
               Top             =   2160
               WhatsThisHelpID =   17774192
               Width           =   1425
            End
            Begin VB.CommandButton cmdDeSelect 
               Caption         =   "&De-Select"
               Height          =   375
               Left            =   300
               TabIndex        =   12
               Top             =   2640
               WhatsThisHelpID =   17774193
               Width           =   1425
            End
         End
         Begin VB.Frame fraTransfer 
            Caption         =   "T&ransfers"
            Height          =   2670
            Left            =   0
            TabIndex        =   15
            Top             =   2720
            Width           =   9285
            Begin FPSpreadADO.fpSpread grdSelection 
               Height          =   2295
               Index           =   1
               Left            =   120
               TabIndex        =   16
               Top             =   240
               WhatsThisHelpID =   24
               Width           =   9045
               _Version        =   524288
               _ExtentX        =   15954
               _ExtentY        =   4048
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "SelectPickLine.frx":2C6A
               AppearanceStyle =   0
            End
            Begin LookupViewControl.LookupView navControlTrnsfr 
               Height          =   285
               Index           =   0
               Left            =   2640
               TabIndex        =   55
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   840
               WhatsThisHelpID =   17774195
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin NEWSOTALib.SOTAMaskedEdit mskControl 
               Height          =   240
               Index           =   1
               Left            =   1440
               TabIndex        =   56
               TabStop         =   0   'False
               Top             =   840
               WhatsThisHelpID =   45
               Width           =   1950
               _Version        =   65536
               _ExtentX        =   3440
               _ExtentY        =   423
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               sBorder         =   1
            End
            Begin NEWSOTALib.SOTACurrency curControl 
               Height          =   285
               Index           =   1
               Left            =   960
               TabIndex        =   57
               TabStop         =   0   'False
               Top             =   720
               WhatsThisHelpID =   48
               Width           =   1950
               _Version        =   65536
               _ExtentX        =   3440
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               sBorder         =   1
               mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber nbrControl 
               Height          =   285
               Index           =   1
               Left            =   840
               TabIndex        =   58
               TabStop         =   0   'False
               Top             =   840
               WhatsThisHelpID =   47
               Width           =   1950
               _Version        =   65536
               _ExtentX        =   3440
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               sBorder         =   1
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
         End
         Begin VB.Frame fraSalesOrder 
            Caption         =   "S&ales Orders"
            Height          =   2670
            Left            =   0
            TabIndex        =   13
            Top             =   0
            Width           =   9285
            Begin FPSpreadADO.fpSpread grdSelection 
               Height          =   2295
               Index           =   0
               Left            =   120
               TabIndex        =   14
               Top             =   240
               WhatsThisHelpID =   24
               Width           =   9045
               _Version        =   524288
               _ExtentX        =   15954
               _ExtentY        =   4048
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "SelectPickLine.frx":307E
               AppearanceStyle =   0
            End
            Begin NEWSOTALib.SOTACurrency curControl 
               Height          =   285
               Index           =   0
               Left            =   1530
               TabIndex        =   50
               TabStop         =   0   'False
               Top             =   1035
               WhatsThisHelpID =   48
               Width           =   1950
               _Version        =   65536
               _ExtentX        =   3440
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               sBorder         =   1
               mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTANumber nbrControl 
               Height          =   285
               Index           =   0
               Left            =   1530
               TabIndex        =   51
               TabStop         =   0   'False
               Top             =   1440
               WhatsThisHelpID =   47
               Width           =   1950
               _Version        =   65536
               _ExtentX        =   3440
               _ExtentY        =   503
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               sBorder         =   1
               mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
               text            =   "           0.00"
               sDecimalPlaces  =   2
            End
            Begin NEWSOTALib.SOTAMaskedEdit mskControl 
               Height          =   240
               Index           =   0
               Left            =   1530
               TabIndex        =   52
               TabStop         =   0   'False
               Top             =   720
               WhatsThisHelpID =   45
               Width           =   1950
               _Version        =   65536
               _ExtentX        =   3440
               _ExtentY        =   423
               _StockProps     =   93
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               sBorder         =   1
            End
            Begin LookupViewControl.LookupView navControl 
               Height          =   285
               Index           =   0
               Left            =   3555
               TabIndex        =   53
               TabStop         =   0   'False
               ToolTipText     =   "Lookup"
               Top             =   720
               WhatsThisHelpID =   46
               Width           =   285
               _ExtentX        =   503
               _ExtentY        =   503
               LookupMode      =   1
            End
            Begin FPSpreadADO.fpSpread grdSettingsSel 
               Height          =   1215
               Left            =   6000
               TabIndex        =   54
               Top             =   480
               Visible         =   0   'False
               WhatsThisHelpID =   17776336
               Width           =   525
               _Version        =   524288
               _ExtentX        =   926
               _ExtentY        =   2143
               _StockProps     =   64
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               SpreadDesigner  =   "SelectPickLine.frx":3492
               AppearanceStyle =   0
            End
         End
      End
   End
   Begin VB.Frame CustomFrame 
      Caption         =   "Frame"
      Enabled         =   0   'False
      Height          =   1035
      Index           =   0
      Left            =   -10000
      TabIndex        =   39
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.CommandButton CustomButton 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   360
      Index           =   0
      Left            =   -10000
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   2730
      Visible         =   0   'False
      WhatsThisHelpID =   61
      Width           =   1245
   End
   Begin VB.CheckBox CustomCheck 
      Caption         =   "Check"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   46
      TabStop         =   0   'False
      Top             =   2310
      Visible         =   0   'False
      WhatsThisHelpID =   62
      Width           =   1245
   End
   Begin VB.ComboBox CustomCombo 
      BackColor       =   &H8000000F&
      Enabled         =   0   'False
      ForeColor       =   &H80000012&
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   45
      TabStop         =   0   'False
      Text            =   "Combo1"
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   64
      Width           =   1245
   End
   Begin VB.OptionButton CustomOption 
      Caption         =   "Option"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   47
      TabStop         =   0   'False
      Top             =   1530
      Visible         =   0   'False
      WhatsThisHelpID =   68
      Width           =   1245
   End
   Begin SOTAToolbarControl.SOTAToolbar tbrMain 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   38
      TabStop         =   0   'False
      Top             =   0
      Width           =   12360
      _ExtentX        =   21802
      _ExtentY        =   741
      Style           =   4
   End
   Begin VB.ComboBox cboSetting 
      Height          =   315
      Left            =   870
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   540
      WhatsThisHelpID =   17774208
      Width           =   3585
   End
   Begin NEWSOTALib.SOTAMaskedEdit CustomMask 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   41
      TabStop         =   0   'False
      Top             =   420
      Visible         =   0   'False
      WhatsThisHelpID =   66
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin NEWSOTALib.SOTANumber CustomNumber 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   780
      Visible         =   0   'False
      WhatsThisHelpID =   67
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin NEWSOTALib.SOTACurrency CustomCurrency 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   1140
      Visible         =   0   'False
      WhatsThisHelpID =   65
      Width           =   1245
      _Version        =   65536
      _ExtentX        =   2196
      _ExtentY        =   503
      _StockProps     =   93
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      mask            =   "<HL> <ILH>###|,###|,###|,##<ILp0>#<IRp0>|.##"
      text            =   "           0.00"
      sDecimalPlaces  =   2
   End
   Begin MSComCtl2.UpDown CustomSpin 
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   40
      Top             =   4320
      Visible         =   0   'False
      WhatsThisHelpID =   69
      Width           =   195
      _ExtentX        =   423
      _ExtentY        =   503
      _Version        =   393216
      Enabled         =   0   'False
   End
   Begin SOTACalendarControl.SOTACalendar CustomDate 
      Height          =   315
      Index           =   0
      Left            =   -10000
      TabIndex        =   43
      Top             =   2880
      WhatsThisHelpID =   75
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   556
      BackColor       =   -2147483633
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MaskedText      =   "  /  /    "
      Protected       =   -1  'True
      Text            =   "  /  /    "
   End
   Begin NEWSOTALib.SOTACustomizer picDrag 
      Height          =   330
      Index           =   0
      Left            =   -75000
      TabIndex        =   49
      Top             =   0
      Visible         =   0   'False
      WhatsThisHelpID =   70
      Width           =   345
      _Version        =   65536
      _ExtentX        =   609
      _ExtentY        =   582
      _StockProps     =   0
   End
   Begin VB.Label CustomLabel 
      Caption         =   "Label"
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   -10000
      TabIndex        =   0
      Top             =   60
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Label lblSetting 
      Caption         =   "Se&tting"
      Height          =   225
      Left            =   210
      TabIndex        =   1
      Top             =   570
      Width           =   615
   End
End
Attribute VB_Name = "frmSelectPick"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'************************************************************************************
'     Name: frmSelectPick
'     Desc: Select Pick Lines
'Copyright: Copyright (c) 1995-2005 Sage Software, Inc.
' Original: JY 03/15/2004
'     Mods:
'************************************************************************************

Option Explicit

#If CUSTOMIZER Then
    Public moFormCust As Object
#End If

'Public Form Variables
Public moSotaObjects            As New Collection
Public msCompanyID              As String
Public moMapSrch                As Collection    'Search Buttons

'Private Object Variables
Private moClass                 As Object
Private moAppDB                 As Object
Private moSysSession            As Object

'To support multiple selection grid interface
Private moSelect(1)             As clsSelection 'Zero-based array of selection class objects
Private msBaseCaption(1)        As String  'Zero-based array of base captions for the various tabs, frames, etc.
Private moMultSelGridMgr        As clsMultiSelGridSettings
Private Const kNumSelGrids      As Integer = 2
Private msYes                   As String
Private msNo                    As String

'Private Constants for tabs
Private Const kTabSelection     As Integer = 0
Private Const kTabPickOption    As Integer = 1
Private Const kTabOrdSelected   As Integer = 2
Private Const kTabLineSelected  As Integer = 3

'Private Include checkbox Constants for array indices
Private Const kSalesOrder       As Integer = 0
Private Const kTransfer         As Integer = 1
Private Const kInclZeroAvail    As Integer = 2
Private Const kInclNonInvt      As Integer = 3

'Private Constants for Stock Allocation Sequence
Private Const kSASeqFirst       As Integer = 0
Private Const kSASeqSecond      As Integer = 1
Private Const kSASeqThird       As Integer = 2

'Private Constants for Selection Counter
Private Const kSelect           As Integer = 1
Private Const kDeSelect         As Integer = 2

'Private Constants for Out of Stock Pick
Private Const kPickAvailQty     As Integer = 0
Private Const kPickOrdQty       As Integer = 1

'Misc constants
Private Const kAllowDecimalPlaces As Integer = 1
Private Const kDisAllowDecimalPlaces As Integer = 0

Private WithEvents moDMOrderGrid  As clsDmGrid
Attribute moDMOrderGrid.VB_VarHelpID = -1
Private WithEvents moDMLineGrid As clsDmGrid
Attribute moDMLineGrid.VB_VarHelpID = -1
Private WithEvents moGMHdr      As clsGridMgr
Attribute moGMHdr.VB_VarHelpID = -1
Private WithEvents moGMLine     As clsGridMgr
Attribute moGMLine.VB_VarHelpID = -1
Private moContextMenu           As clsContextMenu
Private moSettings              As clsSettings
Private moOptions               As clsOptions
Private moModuleOptions         As clsModuleOptions
Private WithEvents moHook       As AFWinHook.clsMsgs
Attribute moHook.VB_VarHelpID = -1

'Private Miscellaneous Variables
Private miMousePointer          As Integer
Private mbEnterAsTab            As Boolean
Private msUserID                As String
Private msHomeCurrID            As String
Private mlLanguage              As Long
Private mbLoading               As Boolean
Private mlRunMode               As Long
Private mbCancelShutDown        As Boolean
Private mbLoadSuccess           As Boolean
Private bMaskCtrlLostFocus      As Boolean
Private miNbrDecPlaces          As Integer
Private msLineToPick            As String
Private msLinesToPick           As String
Private msOrderToPick           As String
Private msOrdersToPick          As String
Private mbClearAll           As Boolean
Private miSPID                  As Integer      'SQL server process ID
Private mlSOSelectedCount       As Long
Private mlTOSelectedCount       As Long
Private mlSOLSelectedCount      As Long
Private mlTOLSelectedCount      As Long
Private mlSOOnHoldNo            As Long

'Integrate with IM variable
Private mbIMIntegrated          As Boolean
Private mbWMIsLicensed          As Boolean
Private miUseNationalAccts      As Integer

'Grid Edit Variables
Private miAllowNegQty           As Integer '0=no, 1=yes
Private miItemType              As Integer
Private miAllowDecimalQty       As Integer
          
'Private Form Resize Variables
Private miMinFormHeight As Long
Private miMinFormWidth As Long
Private miOldFormHeight As Long
Private miOldFormWidth As Long

'Stock Allocation Sequence Variable and Constants
Private msSASNone                       As String
Private mskSASFillSalesBeforeTransfer   As String
Private mskSASFillTransferBeforeSales   As String
Private mskSASFillBackOrdersFirst       As String
Private mskSASOrderExpirationDate       As String
Private mskSASHighestOrderPriority      As String
Private mskSASEarliestShipDate          As String
Private Const kSASNone = 0
Private Const kSASFillSalesBeforeTransfer = 1
Private Const kSASFillTransferBeforeSales = 2
Private Const kSASFillBackOrdersFirst = 3
Private Const kSASOrderExpirationDate = 4
Private Const kSASHighestOrderPriority = 5
Private Const kSASEarliestShipDate = 6

'DEJ SGS for IA changed col assignments (START)
'Order Picked Grid Column Constants
Private Const kColOrdOrder = 1             'displays Order ID
Private Const kColOrdTranTypeText = 3 '2      'displays text indication of transaction type (Sale / Transfer)
Private Const kColOrdShipTo = 4 '3            'displays Order Ship-To
Private Const kColOrdOrderDate = 5 '4         'displays Order Date
Private Const kColOrdExpireDate = 6 '5        'displays Expire Date
Private Const kColOrdShipVia = 7 '6           'displays Ship Via
Private Const kColOrdEarliestShipDate = 8 '7  'displays Earliest Ship Date
Private Const kColOrdShipPriority = 9 '8      'displays Shipping Priority
Private Const kColOrdShipFromWhse = 10 '9      'displays Shipping From Warehouse
Private Const kColOrdTranKey = 11 '10          'Hiden Column for Transaction Key
Private Const kcolOrdTranType = 12 '11         'Hiden Column for Tran Type
'DEJ SGS for IA changed col assignments (STOP)

'DEJ SGS for IA changed nbr cols
'Private Const kMaxOrderCols = 11             'maximum columns to display in grid
Private Const kMaxOrderCols = 12             'maximum columns to display in grid

'SGS DEJ for IA
Private Const kcolOrdBOL = 2         'Displays BOL from Scale Pass

Private Const kOpen = 1                 '1 = open tsoSOLine, tsoSOLineDist, or timTrnsfrOrderLine lines available for picking
Private Const kOrdMaxQtyLen = "99999999.99999999"
Private Const kNoHold = 0               'Not On hold

'Line Grid Column Constants
Private Const kColLineItem = 1              'displays Displays Item ID
Private Const kColLineShiptoAddrID = 2      'displays Customer Ship To ID
Private Const kColLineTranTypeText = 3      'displays text indication of transaction type (Sale / Transfer)
Private Const kColLineQtyToPick = 5           'displays Line Item Quantity To Pick
Private Const kColLineQtyAvail = 6          'displays IM Qty Available
Private Const kColLineUOM = 7               'displays Unit Of Measure
Private Const kColLineShipDate = 8          'displays Shipping Date
Private Const kColLineDeliveryMethod = 9    'displays DeliveryMethod
Private Const kColLineShipVia = 10           'displays Ship Method
Private Const kColLineShipPriority = 11     'displays Shipping Priority
Private Const kColLineOrder = 12            'displays Order ID
Private Const kColLineLine = 13             'displays Order Line Number
Private Const kColLineWarehouse = 14        'displays Warehouse For Item
Private Const kColLineShiptoName = 15       'displays Customer Ship To Name
Private Const kColLineShiptoAddr = 16       'displays Customer Ship To Address
Private Const kColLineItemDesc = 17         'displays Item Description
Private Const kColLineTranKey = 18          'Hiden Column for Transaction Key
Private Const kColLineTranType = 19         'Hiden Column for Transaction Type
Private Const kColLineTranLineKey = 20      'Hiden Column for Transaction Line Key
Private Const kColLineTranLineDistKey = 21  'Hiden Column for Transaction Line Dist Key
Private Const kColLineAllowDecimalQty = 22  'Hiden Column
Private Const kColLineQtyToPickByOrdUOM = 4 'displays Line Item Quantity To Pick by Order UOM

Private Const kMaxLineCols = 22             'maximum columns to display in grid
Private Const kMaxQtyLen = "99999999.99999999"

'Misc Constants
Private Const kLockData = 1

Const VBRIG_MODULE_ID_STRING = "SelectPickLine.FRM"

Public Property Get lRunMode() As Long
'+++ VB/Rig Skip +++
'********************************************************************
' Description:
'    lRunMode contains the user-defined run mode context in which the
'    class object was Initialzed. The run mode is extracted from the
'    context passed in through the InitializeObject.
'********************************************************************
    lRunMode = mlRunMode
End Property

Public Property Let lRunMode(lNewRunMode As Long)
'+++ VB/Rig Skip +++
    mlRunMode = lNewRunMode
End Property

Public Property Get oClass() As Object
'+++ VB/Rig Skip +++
'***********************************************************************
' Description:
'    oClass contains the reference to the parent class object. The form
'    needs this reference to use the public variables created within the
'    class object.
'***********************************************************************
    Set oClass = moClass
End Property

Public Property Set oClass(oNewClass As Object)
'+++ VB/Rig Skip +++
    Set moClass = oNewClass
End Property

Function sMyName() As String
'+++ VB/Rig Skip +++
    sMyName = Me.Name
End Function

Public Property Get bLoadSuccess() As Boolean
'+++ VB/Rig Skip +++
    bLoadSuccess = mbLoadSuccess
End Property

Public Property Get bCancelShutDown()
'+++ VB/Rig Skip +++
'**********************************************************************
' Description:
'    bCancelShutDown tells the framework whether the form has requested
'    the shutdown process to be cancelled.
'**********************************************************************
    bCancelShutDown = mbCancelShutDown
End Property

Public Property Get WhatHelpPrefix() As String
'+++ VB/Rig Skip +++
    '-- the three-letter abbreviation used below is built as follows:
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    WhatHelpPrefix = "SOZ"
End Property

Public Property Get FormHelpPrefix() As String
'+++ VB/Rig Skip +++
    '-- <two-letter mnemonic of module, e.g., "AR"=Accounts Receivable> + "Z"
    FormHelpPrefix = "SOZ"
End Property

Private Sub cboSASeq_Click(Index As Integer, Cancel As Boolean, ByVal PrevIndex As Long, ByVal NewIndex As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then
            Cancel = moFormCust.OnDropDownClick(cboSASeq(Index), PrevIndex, NewIndex, True)
            If Cancel Then Exit Sub
        End If
    #End If
'+++ End Customizer Code Push +++

Dim i As Integer

    If mbLoading Then Exit Sub

    'When the higher stock allocation sequence is set to  "None", all the lower
    'stock allocation sequence option should be set to none and disabled.
    If Index < cboSASeq.UBound Then
        If cboSASeq(Index).ListIndex = kSASNone Then
            For i = Index + 1 To cboSASeq.UBound
                cboSASeq(i).ListIndex = kSASNone
                cboSASeq(i).Enabled = False
            Next
        Else
            cboSASeq(Index + 1).Enabled = True
        End If
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboSASeq_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub chkEmptyBins_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkEmptyBins, True
    #End If
'+++ End Customizer Code Push +++

    If chkEmptyBins.Value = vbUnchecked Then
        chkEmptyRandomBin.Value = vbUnchecked
        chkEmptyRandomBin.Enabled = False
    Else
        chkEmptyRandomBin.Enabled = True
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkEmptyBins_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub chkInclude_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkInclude(Index), True
    #End If
'+++ End Customizer Code Push +++
    Dim iTabCnt As Integer
    
    'Enable/disable the associated tab.
    If chkInclude(kSalesOrder).Value = vbChecked Then
        grdSelection(kSalesOrder).Enabled = True
    Else
        grdSelection(kSalesOrder).Enabled = False
    End If
    
    If chkInclude(kTransfer).Value = vbChecked Then
        grdSelection(kTransfer).Enabled = True
    Else
        grdSelection(kTransfer).Enabled = False
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkInclude_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub chkOrderLimit_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkOrderLimit, True
    #End If
'+++ End Customizer Code Push +++

    If chkOrderLimit.Value = 0 Then
        lblMaxOrderToPick.Enabled = False
        txtMaxPickNo.Value = 0
        txtMaxPickNo.Enabled = False
        spnMaxPickNo.Enabled = False
    Else
        lblMaxOrderToPick.Enabled = True
        txtMaxPickNo.Enabled = True
        txtMaxPickNo.Value = 0
        spnMaxPickNo.Enabled = True
    End If

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "chkOrderLimit_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Public Sub cmdDeSelect_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdDeSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

Dim sWhereClause    As String
Dim sTablesUsed     As String
Dim i               As Integer
    
    SetMouse
    sbrMain.Status = SOTA_SB_BUSY
    
    For i = 0 To chkInclude.UBound
        'Only process de-selection grid if the associated chkbox is checked or mbClearAll
        If (chkInclude(i).Value = 1 Or mbClearAll) And i <> kInclZeroAvail Then
            'Must clear out string variables before each pass.
            sWhereClause = ""
            sTablesUsed = ""
            If Not bBuildWhereClause(i, sWhereClause, sTablesUsed) Then
                ResetMouse
                sbrMain.Status = SOTA_SB_START
                Exit Sub
            End If
            'Delete all rows in temp table matching criteria in the selection grid and refresh DM/Line Grid
            bDeSelectRows i, sWhereClause, sTablesUsed
        End If
    Next
    
    moDMOrderGrid.Refresh
    moDMLineGrid.Refresh
    UpdateSelectionCounter
    SetupRowsSelCaption
    
    ResetMouse
    sbrMain.Status = SOTA_SB_START
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        sbrMain.Status = SOTA_SB_START
        gSetSotaErr Err, sMyName, "cmdDeSelect_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.onClick(cmdSelect, True) Then Exit Sub
    End If
    #End If
'+++ End Customizer Code Push +++

Dim sWhereClause          As String
Dim sTablesUsed           As String
Dim i                     As Integer
Dim iCreditCheckAtPicking As Integer
    
    SetMouse
    sbrMain.Status = SOTA_SB_BUSY
    
    'Truncate any rows in #tsoCreatePickWrk1 so new rows matching selection criteria can be inserted.
    If Not bClearCreatePickWrk Then
        ResetMouse
        sbrMain.Status = SOTA_SB_START
        Exit Sub
    End If
    
    For i = 0 To chkInclude.UBound
        'Only process selection grid if the associated chkbox is checked (means tran type is to be included in the select).
        If chkInclude(i).Value = 1 And i <> kInclZeroAvail Then
            'Must clear out string variables before each pass.
            sWhereClause = ""
            sTablesUsed = ""
            If Not bBuildWhereClause(i, sWhereClause, sTablesUsed) Then
                ResetMouse
                sbrMain.Status = SOTA_SB_START
                Exit Sub
            End If
        
            moDMOrderGrid.Save
            moDMLineGrid.Save
            
            bSelectRows i, sWhereClause, sTablesUsed
        End If
    Next
    
    'Call IM Services to get Available Qty
    If Not moClass.bGetQtyFromIM(msCompanyID, chkInclude(kInclZeroAvail), 0, kLockData) Then
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgIMServicesError 'inform user that there are problems
        sbrMain.Status = SOTA_SB_START
        Exit Sub
    End If

    'Check Credit for the selected orders
    iCreditCheckAtPicking = moAppDB.Lookup("CreditCheckAtPicking", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    If iCreditCheckAtPicking = 1 Then
        If Not moClass.bPickCreditCheck(msCompanyID) Then
            ResetMouse
            giSotaMsgBox Me, moClass.moSysSession, kmsgBadError, "perform a credit check" 'inform user that there are problems
            sbrMain.Status = SOTA_SB_START
            Exit Sub
        End If
    End If

    UpdateSelectionCounter
      
    'Inform the user if there is no line picked
    If (mlSOSelectedCount + mlTOSelectedCount + mlSOOnHoldNo) = 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgNoLineFitSelCriteria
    End If

    moDMOrderGrid.Refresh
    moDMLineGrid.Refresh

    SetupRowsSelCaption
    
    ResetMouse
    
'+++ VB/Rig Begin Pop +++
    sbrMain.Status = SOTA_SB_START
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        sbrMain.Status = SOTA_SB_START
        gSetSotaErr Err, sMyName, "cmdSelect_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub UpdateSelectionCounter()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim lTranType As Long
Dim lRowCount As Long
Dim sSQL As String
Dim rs As Object

    mlSOSelectedCount = 0
    mlTOSelectedCount = 0
    mlSOLSelectedCount = 0
    mlTOLSelectedCount = 0
    mlSOOnHoldNo = 0
    
    'Update the selected count for Lines
    sSQL = "SELECT COUNT(TranKey) RecordCount, TranType FROM #tsoCreatePickHdrWrk "
    sSQL = sSQL & " WHERE CrHold <> 1 AND Hold <> 1 GROUP BY TranType"
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
        Do While Not rs.IsEOF
            lTranType = glGetValidLong(rs.Field("TranType"))
            lRowCount = glGetValidLong(rs.Field("RecordCount"))
            
            If lTranType = kTranTypeSOSO Then
                mlSOSelectedCount = lRowCount
            Else
                mlTOSelectedCount = lRowCount
            End If
        
            rs.MoveNext
        Loop
    End If
    

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    'Update the selected count for Lines
    sSQL = "SELECT COUNT(*) RecordCount, TranType FROM #tsoCreatePickWrk2 "
    sSQL = sSQL & " WHERE LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1 GROUP BY TranType"
    Set rs = moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    
    If Not rs.IsEOF Then
   
        Do While Not rs.IsEOF
        
            lTranType = glGetValidLong(rs.Field("TranType"))
            lRowCount = glGetValidLong(rs.Field("RecordCount"))
            
            If lTranType = kTranTypeSOSO Then
                mlSOLSelectedCount = lRowCount
            Else
                mlTOLSelectedCount = lRowCount
            End If
        
            rs.MoveNext
        Loop
        
    End If

    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    
    'Lookup the on hold lines
    mlSOOnHoldNo = glGetValidLong(moAppDB.Lookup("Count(*)", "#tsoCreatePickHdrWrk", "Hold = 1 Or CrHold = 1"))
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UpdateSelectionCounter", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Function bClearCreatePickWrk() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL As String

    bClearCreatePickWrk = False
       
    'Truncate any existing rows in #tsoCreatePickWrk1 so new rows matching selection criteria can be inserted
    sSQL = "TRUNCATE TABLE #tsoCreatePickWrk1"
    On Error Resume Next
    moAppDB.ExecuteSQL sSQL
    If Err.Number <> 0 Then
        gClearSotaErr
    End If
    
'    'Truncate any existing rows in #tsoCreatePickHdrWrk so new rows matching selection criteria can be inserted
'    sSQL = "TRUNCATE TABLE #tsoCreatePickHdrWrk"
'    On Error Resume Next
'    moAppDB.ExecuteSQL sSQL
'    If Err.Number <> 0 Then
'        gClearSotaErr
'    End If
    
    bClearCreatePickWrk = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bClearCreatePickWrk", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub SetupRowsSelCaption()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This routine updates the select grids's frame caption for
'    the current row count in the grid
'********************************************************************
Dim lOrdNbrSelected     As Long
Dim lLineNbrSelected    As Long

    lOrdNbrSelected = mlSOSelectedCount + mlTOSelectedCount
    lLineNbrSelected = mlSOLSelectedCount + mlTOLSelectedCount
    
    If lOrdNbrSelected = 0 And mlSOOnHoldNo = 0 Then
        tbrMain.ButtonEnabled(kTbProceed) = False
    Else
        tbrMain.ButtonEnabled(kTbProceed) = True
    End If
    
    If lOrdNbrSelected = 0 Then
        fraOrderPicked.Caption = lOrdNbrSelected & " " & msOrderToPick
    Else
        If lOrdNbrSelected > 1 Then
            fraOrderPicked.Caption = lOrdNbrSelected & " " & msOrdersToPick
        Else
            fraOrderPicked.Caption = lOrdNbrSelected & " " & msOrderToPick
        End If
    End If
    
    If lLineNbrSelected = 0 Then
        fraLinePicked.Caption = lLineNbrSelected & " " & msLineToPick
    Else
        If lLineNbrSelected > 1 Then
            fraLinePicked.Caption = lLineNbrSelected & " " & msLinesToPick
        Else
            fraLinePicked.Caption = lLineNbrSelected & " " & msLineToPick
        End If
    End If
    
    lblNoOfSOSelected.Caption = mlSOSelectedCount
    lblNoOfSOLineSelected.Caption = mlSOLSelectedCount
    lblNoOfTOSelected.Caption = mlTOSelectedCount
    lblNoOfTOLineSelected.Caption = mlTOLSelectedCount
    lblNoOfSOOnHold.Caption = mlSOOnHoldNo

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupRowsSelCaption", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Function bBuildWhereClause(ByVal iTranType As Integer, ByRef sWhereClause As String, ByRef sTablesUsed As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
Dim bNonInventory As Boolean

    bBuildWhereClause = False
    
    'Both Sales Order and non-inventory options used the Sales Order Type
    If iTranType = kSalesOrder Or iTranType = kInclNonInvt Then
        bNonInventory = IIf(iTranType = kInclNonInvt, True, False)  'We need to know if this was the Non-Inventory check
        iTranType = kSalesOrder
    End If
       
    'Validate the associated selection grid
    If moSelect(iTranType).lValidateGrid(True) <> 0 Then
        giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
        Exit Function
    End If
    
    'Get the WHERE clause.
    If Not moSelect(iTranType).bGetWhereClause(sWhereClause, sTablesUsed, 0, True) Then
        giSotaMsgBox Me, moSysSession, kSOmsgSelCriteriaInvalid
        Exit Function
    End If

    'Set the sWhereClause to empty if Clear All button was clicked
    If mbClearAll Then
        sWhereClause = ""
    End If
     
    'Add joins based on transaction type.
    Select Case iTranType
        Case kSalesOrder
            If InStr(sTablesUsed, "tsoSOLine ") = 0 Then
                sTablesUsed = sTablesUsed & ", tsoSOLine WITH (NOLOCK)"
            End If
            
            If InStr(sTablesUsed, "tsoSalesOrder ") = 0 Then
                sTablesUsed = sTablesUsed & ", tsoSalesOrder WITH (NOLOCK)"
            End If
            
            If InStr(sTablesUsed, "tarCustomer ") = 0 Then
                sTablesUsed = sTablesUsed & ", tarCustomer WITH (NOLOCK)"
            End If

            moSelect(iTranType).AddJoinIfNecessary sWhereClause, sTablesUsed, "tsoSOLine", "tsoSOLine.SOLineKey = tsoSOLineDist.SOLineKey"
            moSelect(iTranType).AddJoinIfNecessary sWhereClause, sTablesUsed, "tsoSalesOrder", "tsoSalesOrder.SOKey = tsoSOLine.SOKey"
            moSelect(iTranType).AddJoinIfNecessary sWhereClause, sTablesUsed, "tarCustomer", "tsoSalesOrder.CustKey = tarCustomer.CustKey"
            moSelect(iTranType).AddJoinIfNecessary sWhereClause, sTablesUsed, "timItem", "tsoSOLine.ItemKey = timItem.ItemKey"

            'If non-inventory, remove references to timWarehouse
            If bNonInventory Then
                If InStr(sTablesUsed, "timWarehouse") <> 0 Then
                    sTablesUsed = Replace(sTablesUsed, ",timWarehouse WITH (NOLOCK)", "")
                    EditWhereClause sWhereClause
                End If
            End If
        Case kTransfer
            If InStr(sTablesUsed, "timTrnsfrOrderLine") = 0 Then
                sTablesUsed = sTablesUsed & ", timTrnsfrOrderLine WITH (NOLOCK)"
            End If
            
            moSelect(iTranType).AddJoinIfNecessary sWhereClause, sTablesUsed, "timTrnsfrOrderLine", "timTrnsfrOrderLine.TrnsfrOrderKey = vimTrnsfrOrder.TrnsfrOrderKey"
            moSelect(iTranType).AddJoinIfNecessary sWhereClause, sTablesUsed, "timItem", "timTrnsfrOrderLine.ItemKey = timItem.ItemKey"
    End Select
 
    bBuildWhereClause = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bBuildWhereClause", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bSelectRows(ByVal i As Integer, ByVal sWhereClause As String, ByVal sTablesUsed As String) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSQL As String
    
    bSelectRows = False
    
    sSQL = "INSERT INTO #tsoCreatePickWrk1 " & _
            "(LineDistKey, LineKey, OrderKey, TranType, CrHold, LineHold, OrderHold) "
    
    Select Case i
        Case kSalesOrder
            sSQL = sSQL & "SELECT tsoSOLineDist.SOLineDistKey, "
            sSQL = sSQL & "tsoSOLineDist.SOLineKey, tsoSOLine.SOKey, "
            sSQL = sSQL & "tsoSalesOrder.TranType, 0,0,0"
            sSQL = sSQL & " FROM " & sTablesUsed
            sSQL = sSQL & " WHERE " & sWhereClause
            sSQL = sSQL & " AND tsoSalesOrder.TranType = " & kTranTypeSOSO
            sSQL = sSQL & " AND tsoSalesOrder.Status = " & kOpen
            sSQL = sSQL & " AND tsoSOLine.Status = " & kOpen
            sSQL = sSQL & " AND tsoSOLineDist.Status = " & kOpen
            sSQL = sSQL & " AND tsoSOLineDist.QtyOpenToShip > 0"
            sSQL = sSQL & " AND tsoSOLineDist.WhseKey IS NOT NULL"
            sSQL = sSQL & " AND tsoSOLineDist.DeliveryMeth <> 2"
            sSQL = sSQL & " AND tsoSalesOrder.CompanyID = " & gsQuoted(msCompanyID)
            sSQL = sSQL & " AND tarCustomer.Hold = 0"
            
            If miUseNationalAccts = 1 Then
                sSQL = sSQL & " AND (tarCustomer.NationalAcctLevelKey IS NULL OR (tarCustomer.NationalAcctLevelKey IS NOT NULL AND tarCustomer.NationalAcctLevelKey IN"
                sSQL = sSQL & " (SELECT NationalAcctLevelKey FROM tarNationalAcctLevel WITH (NOLOCK)"
                sSQL = sSQL & " WHERE NationalAcctKey IN"
                sSQL = sSQL & " (SELECT NationalAcctKey FROM tarNationalAcct WITH (NOLOCK)"
                sSQL = sSQL & " WHERE Hold = 0))))"
            End If
        
        Case kTransfer
           'Added a subselect to Where clause to remove order lines
           'from the pick list that have a record in timPendInvtTran of type(705=TI,706=TO,710=ADJ)
            sSQL = sSQL & "SELECT NULL, timTrnsfrOrderLine.TrnsfrOrderLineKey,"
            sSQL = sSQL & " timTrnsfrOrderLine.TrnsfrOrderKey, " & kTranTypeIMTR & ",0,0,0"
            sSQL = sSQL & " FROM " & sTablesUsed
            sSQL = sSQL & " WHERE " & sWhereClause
            sSQL = sSQL & " AND vimTrnsfrOrder.Status = " & kOpen
            sSQL = sSQL & " AND timTrnsfrOrderLine.Status = " & kOpen
            sSQL = sSQL & " AND (timTrnsfrOrderLine.QtyOrd - timTrnsfrOrderLine.QtyShip) > 0"
            sSQL = sSQL & " AND vimTrnsfrOrder.CompanyID = " & gsQuoted(msCompanyID)
            sSQL = sSQL & " AND timTrnsfrOrderLine.TrnsfrOrderLineKey  "
            sSQL = sSQL & "     NOT IN (SELECT timPendInvtTran.TrnsfrOrderLineKey FROM timPendInvtTran WITH (NOLOCK)"
            sSQL = sSQL & "     WHERE timPendInvtTran.TrnsfrOrderLineKey IS NOT NULL AND TranType IN( " & kTranTypeIMTF & "," & kTranTypeIMTT & "," & kTranTypeIMAJ & "))"
    
        Case kInclNonInvt
            sSQL = sSQL & "SELECT tsoSOLineDist.SOLineDistKey, "
            sSQL = sSQL & "tsoSOLineDist.SOLineKey, tsoSOLine.SOKey, "
            sSQL = sSQL & "tsoSalesOrder.TranType, 0,0,0"
            sSQL = sSQL & " FROM " & sTablesUsed
            sSQL = sSQL & " WHERE " & sWhereClause
            sSQL = sSQL & " AND tsoSalesOrder.TranType = " & kTranTypeSOSO
            sSQL = sSQL & " AND tsoSalesOrder.Status = " & kOpen
            sSQL = sSQL & " AND tsoSOLine.Status = " & kOpen
            sSQL = sSQL & " AND tsoSOLineDist.Status = " & kOpen
            sSQL = sSQL & " AND tsoSOLineDist.QtyOpenToShip <> 0"
            sSQL = sSQL & " AND tsoSOLineDist.DeliveryMeth <> 2"
            sSQL = sSQL & " AND tsoSOLineDist.WhseKey IS NULL"
            sSQL = sSQL & " AND tsoSalesOrder.CompanyID = " & gsQuoted(msCompanyID)
            sSQL = sSQL & " AND tarCustomer.Hold = 0"
            
            If miUseNationalAccts = 1 Then
                sSQL = sSQL & " AND (tarCustomer.NationalAcctLevelKey IS NULL OR (tarCustomer.NationalAcctLevelKey IS NOT NULL AND tarCustomer.NationalAcctLevelKey IN"
                sSQL = sSQL & " (SELECT NationalAcctLevelKey FROM tarNationalAcctLevel WITH (NOLOCK)"
                sSQL = sSQL & " WHERE NationalAcctKey IN"
                sSQL = sSQL & " (SELECT NationalAcctKey FROM tarNationalAcct WITH (NOLOCK)"
                sSQL = sSQL & " WHERE Hold = 0))))"
            End If
            
    End Select
                                
    moAppDB.ExecuteSQL sSQL
    
    bSelectRows = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bSelectRows", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bDeSelectRows(iTranType As Integer, sWhereClause As String, sTablesUsed As String, Optional bRefreshGrid As Boolean = True) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Dim sSQL As String
    Dim sSubQuery As String
    Dim sSubQuery1 As String

    bDeSelectRows = False
    
    SetMouse
    
    If iTranType = kTransfer Then
        sSubQuery = "(SELECT timTrnsfrOrderLine.TrnsfrOrderLineKey FROM " & sTablesUsed
        sSubQuery = sSubQuery & " WHERE " & sWhereClause & ")"
        
        'Delete Locking Table Rows
        sSQL = "DELETE FROM timTrnsfrOrdLinePick WHERE TrnsfrOrderLineKey IN " & sSubQuery
        sSQL = sSQL & " AND SPID = " & miSPID
               
        moAppDB.ExecuteSQL sSQL
        
        'Delete Line Work Table Rows
        sSQL = "DELETE FROM #tsoCreatePickWrk2 WHERE #tsoCreatePickWrk2.TranType = "
        sSQL = sSQL & kTranTypeIMTR & " AND #tsoCreatePickWrk2.LineKey IN "
                
        sSQL = sSQL & sSubQuery
        moAppDB.ExecuteSQL sSQL
        
        'Delete Hdr Work Table Rows
        sSQL = "DELETE FROM #tsoCreatePickHdrWrk WHERE TranType = "
        sSQL = sSQL & kTranTypeIMTR & " AND TranKey NOT IN "
        sSQL = sSQL & "(SELECT OrderKey FROM #tsoCreatePickWrk2 WHERE TranType = " & kTranTypeIMTR & ")"
        
        moAppDB.ExecuteSQL sSQL
    Else
        Select Case iTranType
            Case kSalesOrder
                sWhereClause = sWhereClause & " AND tsoSOLineDist.WhseKey IS NOT NULL"
            Case kInclNonInvt
                sWhereClause = sWhereClause & " AND tsoSOLineDist.WhseKey IS NULL"
        End Select
        sSubQuery = "(SELECT tsoSOLineDist.SOLineDistKey FROM " & sTablesUsed
        sSubQuery = sSubQuery & " WHERE " & sWhereClause & ")"
                
        'Delete Locking Table Rows
        sSQL = "DELETE FROM tsoSOLineDistPick WHERE SOLineDistKey IN " & sSubQuery
        sSQL = sSQL & " AND SPID = " & miSPID
        
        moAppDB.ExecuteSQL sSQL
        
        'Delete Line Work Table Rows
        sSQL = "DELETE FROM #tsoCreatePickWrk2 WHERE #tsoCreatePickWrk2.TranType = "
        sSQL = sSQL & kTranTypeSOSO & " AND #tsoCreatePickWrk2.LineDistKey IN "
        
        sSQL = sSQL & sSubQuery
        moAppDB.ExecuteSQL sSQL
        
        'Delete Hdr Work Table Rows
        sSQL = "DELETE FROM #tsoCreatePickHdrWrk WHERE TranType = "
        sSQL = sSQL & kTranTypeSOSO & " AND TranKey NOT IN "
        sSQL = sSQL & "(SELECT OrderKey FROM #tsoCreatePickWrk2 WHERE TranType = " & kTranTypeSOSO & ")"
        
        moAppDB.ExecuteSQL sSQL
    End If
    
    ResetMouse
    
    bDeSelectRows = True
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bDeSelectRows", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Function bCreateNewList() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    bCreateNewList = False
    
    If moClass.mbShipLinesCreated Then
        moMultSelGridMgr.Reset 'Reset the selection grids.
        'cmdClearAll_Click 'Delete all rows and refresh Grid Manager
        moClass.mbShipLinesCreated = False 'Re-initialize the flags.
        moClass.mbTmpLinesCreated = False
    End If
    
    bCreateNewList = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCreateNewList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function sBuildOrderByClause() As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       The function is used get the related order by statement
'       for set up Stock Allocation Sequence.
'*********************************************************************
    Dim sOrderBy As String
    Dim sCboData As String
    Dim i As Integer
    Dim iSASeqListIndex1 As Integer
    Dim iSASeqListIndex2 As Integer
    Dim iSASeqListIndex3 As Integer
    
    'Reset the variables
    sBuildOrderByClause = ""
    sOrderBy = ""
    iSASeqListIndex1 = 0
    iSASeqListIndex2 = 0
    iSASeqListIndex3 = 0
    
    'Loop through the SASeq control array to build the order by clause
    For i = 0 To cboSASeq.UBound
    
        sCboData = ""
        
        Select Case i
            Case 0
                iSASeqListIndex1 = cboSASeq(i).ListIndex
                sCboData = sGetCboData(cboSASeq(i))
                
            Case 1
                iSASeqListIndex2 = cboSASeq(i).ListIndex
                
                If iSASeqListIndex2 > 0 And iSASeqListIndex2 <> iSASeqListIndex1 Then
                    sCboData = sGetCboData(cboSASeq(i))
                End If
            
            Case 2
                iSASeqListIndex3 = cboSASeq(i).ListIndex
                
                If iSASeqListIndex3 > 0 And iSASeqListIndex3 <> iSASeqListIndex1 _
                    And iSASeqListIndex3 <> iSASeqListIndex2 Then
                    sCboData = sGetCboData(cboSASeq(i))
                End If
            
        End Select
            
        If Len(sOrderBy) > 0 Then
            If Len(sCboData) > 0 Then
                sOrderBy = sOrderBy & "," & sCboData
            End If
        Else
            sOrderBy = sCboData
        End If
    Next
    
    If Len(sOrderBy) > 0 Then
        sBuildOrderByClause = " ORDER BY " & sOrderBy
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sBuildOrderByClause", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function sGetCboData(cboCtrl As Control) As String
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       The function is used get the related order by statement
'       for set up Stock Allocation Sequence.
'*********************************************************************
    
    Select Case cboCtrl.ListIndex
        Case kSASNone
                sGetCboData = ""
                
        Case kSASFillSalesBeforeTransfer
                sGetCboData = "#tsoCreatePickHdrWrk.TranType DESC, #tsoCreatePickHdrWrk.AllowSubItem ASC"
        
        Case kSASFillTransferBeforeSales
                sGetCboData = "#tsoCreatePickHdrWrk.TranType ASC"
                
        Case kSASFillBackOrdersFirst
                sGetCboData = "#tsoCreatePickHdrWrk.BackOrderLineExists DESC"
        
        Case kSASOrderExpirationDate
                sGetCboData = "COALESCE(#tsoCreatePickHdrWrk.ExpireDate, '12-31-9999') ASC"
        
        Case kSASHighestOrderPriority
                sGetCboData = "COALESCE(#tsoCreatePickHdrWrk.HighestShipPriority, 9) ASC"
                
        Case kSASEarliestShipDate
                sGetCboData = "COALESCE(#tsoCreatePickHdrWrk.EarliestShipDate, '12-31-9999') ASC"
   
    End Select
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "sGetCboData", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Private Function bCreateShipLine() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
' Description:
'    This routine call the create pick sub in the class to
'    create PickList, Shiplines and Distributions for the orders
'    selected.
'********************************************************************
    Dim lSessionID As Long
    Dim iEmptyBins As Integer
    Dim iEmptyRandomBins As Integer
    Dim lLimitNoOfOrder As Long
    Dim iSASeq1 As Integer
    Dim iSASeq2 As Integer
    Dim iSASeq3 As Integer
    Dim iNonStockItem As Integer
    Dim sOrderBy As String
    Dim iCreditCheckAtPicking As Integer
    Dim iPickOrdQty As Integer
    
    bCreateShipLine = False
    
    'Get Pick Options from the Form
    iEmptyBins = giGetValidInt(chkEmptyBins.Value)
    iEmptyRandomBins = giGetValidInt(chkEmptyRandomBin.Value)
    
    If chkOrderLimit.Value = 1 Then
        lLimitNoOfOrder = glGetValidLong(txtMaxPickNo.Value)
    Else
        lLimitNoOfOrder = 0
    End If
    
    If optStockQtyPick(kPickOrdQty).Value = True And optStockQtyPick(kPickOrdQty).Visible = True Then
        iPickOrdQty = 1
    Else
        iPickOrdQty = 0
    End If
    
    iNonStockItem = giGetValidInt(chkInclude(kInclZeroAvail).Value)
    
    'Build the order by clause for establish Stock Allocation  Sequence
    sOrderBy = sBuildOrderByClause()
    
    'Call the CreateShipLines Method in the calls to create pick lines,
    'ship lines and distributions.
    iCreditCheckAtPicking = moAppDB.Lookup("CreditCheckAtPicking", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    lSessionID = moClass.CreateShipLines(msCompanyID, iEmptyBins, iEmptyRandomBins, _
                    lLimitNoOfOrder, iNonStockItem, iCreditCheckAtPicking, sOrderBy, iPickOrdQty)
                     
    'Inform user if there is error in the pick process
    If (lSessionID = -1) Then
        giSotaMsgBox Nothing, Me.oClass.moSysSession, kmsgErrorReleaseToShip
        Exit Function
    End If
    
    'Inform user if there is no pick line selected
    If (lSessionID = 0) Then
        giSotaMsgBox Nothing, Me.oClass.moSysSession, kmsgNoLinePicked
        Exit Function
    End If
    
    bCreateShipLine = True
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "bCreatePickList", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function
Public Function bInitWorkTables() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim oSOTmpTbls As New clsSOTempTable

    bInitWorkTables = False
    
    If Not oSOTmpTbls Is Nothing Then
        oSOTmpTbls.gbCreateTmpTblSOCreatePickList moAppDB
    End If

    If Err.Number <> 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kSOmsgPickListWrkTable  'inform user that there are problems
        Exit Function
    End If
        
    Set oSOTmpTbls = Nothing
    
    bInitWorkTables = True

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        If Not oSOTmpTbls Is Nothing Then
            Set oSOTmpTbls = Nothing
        End If
        
        gSetSotaErr Err, sMyName, "bInitWorkTables", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Private Sub Form_Activate()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If moFormCust Is Nothing Then
        Set moFormCust = CreateObject("SOTAFormCustRT.clsFormCustRT")
        If Not moFormCust Is Nothing Then
                moFormCust.Initialize Me, goClass
                Set moFormCust.CustToolbarMgr = tbrMain
                moFormCust.ApplyFormCust
        End If
    End If
#End If



'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Activate", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub MapControls()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Description:
'       MapControls will map the controls on the form to the buttons
'       that have Function key equivalents.  The general procedure
'       giCollectionAdd is used to add the button control as
'       an object Item to the collection and use the mapped
'       entry control's window handle as the key into the collection.
'*********************************************************************
    Set moMapSrch = New Collection
    giCollectionAdd moMapSrch, navControl, mskControl(kSalesOrder).hwnd
    giCollectionAdd moMapSrch, navControlTrnsfr, mskControl(kTransfer).hwnd
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "MapControls", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*********************************************************************
'   Desc: Form_KeyDown traps the hot keys when the KeyPreview property
'         of the form is set to True process predefined function keys.
'*********************************************************************
    Dim iNavControl As Integer

    If Shift = 0 And keycode = vbKeyF5 Then
        If Me.ActiveControl.Name = "mskControl" Then
            iNavControl = moSelect(Me.ActiveControl.Index).SelectionGridF5KeyDown(Me.ActiveControl)
            If iNavControl > 0 Then
                Select Case Me.ActiveControl.Index
                    Case 0
                        navControl_GotFocus iNavControl
                    Case 1
                        navControlTrnsfr_GotFocus iNavControl
                End Select
            End If
        End If
    End If

    Select Case keycode 'normal function key processing
        Case vbKeyF1 To vbKeyF16              'Function Keys pressed
            gProcessFKeys Me, keycode, Shift  'Run Sage MAS 500 Function Key Processing
    End Select
    

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'********************************************************************
'   Desc: Form_KeyPress traps the keys pressed when the KeyPreview
'         property of the form is set to True process the enter key
'         as if the user pressed the tab key.
'********************************************************************

    Select Case KeyAscii
        Case vbKeyReturn            'User pressed enter key
            If mbEnterAsTab Then    'enter as tab set in session
                DoEvents
                'ltd - 17973 don't sendkeys tab, it will skip a line in the grid
                'SendKeys "{Tab}"    'send tab key
                KeyAscii = 0        'cancel enter key
            End If
        Case Else
            'Other KeyAscii routines.
    End Select

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_KeyPress", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Load()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'default to Form_Load error
    
    Dim i As Integer
    Dim rs As Object
    Dim sSelect As String
    
    mbLoadSuccess = False
    mbLoading = True
    
    'The server process ID is used when deselecting or clearing the grid so only those rows
    'in the locking table associated with this user's session are deleted.  Prevents the following
    'situation from occuring:
    'User 1 selects SO#1
    'User 2 selects ALL SO's.  SO#1 will be excluded.  User 2 then clears their selection.  Since the initial selection
    'included SO#1, the locking row for User 1's SO#1 will get deleted unless it is restricted to the SPID for this
    'session, making it possible for SO#1 to be subsequently selected and picked by another user.
    miSPID = moClass.moAppDB.Lookup("spid", "master..sysprocesses", "spid = @@spid")
            
    Set moAppDB = moClass.oAppDb
    Set moSysSession = moClass.moSysSession
    With moSysSession
        msCompanyID = .CompanyId
        msUserID = .UserId
        mbEnterAsTab = .EnterAsTab
        mlLanguage = .Language
        msHomeCurrID = .CurrencyID
    End With
    
    Set moAppDB = moClass.moAppDB
    
    'Build local strings
    Me.Caption = gsBuildString(kSOSelectOrdersToPick, moAppDB, moSysSession)
    
    'Get whatever Localized Strings are needed.
    If Not bGetLocalStrings() Then
        'Error processing
        mbLoadSuccess = False

        'An error occurred while retrieving text for messages used in this function.
        ResetMouse
        giSotaMsgBox Me, moClass.moSysSession, kmsgGetLocalStringsError
        
        'Exit this subroutine
        mbLoadSuccess = False
        Exit Sub
    End If

    'Populate the base caption array, using localized strings.
    msBaseCaption(kSalesOrder) = gsBuildString(kSOSalOrders, moAppDB, moClass.moSysSession)
    msBaseCaption(kTransfer) = gsBuildString(kIMTrnsfrOrd, moAppDB, moClass.moSysSession) & "s"
         
    'Look up integrate with IM Flag.
    mbIMIntegrated = moClass.moAppDB.Lookup("IntegrateWithIM", "tsoOptions", "CompanyID = " & gsQuoted(msCompanyID))
    
    '-- Set up the form resizing variables
    With Me
        miMinFormHeight = .Height
        miMinFormWidth = .Width
        miOldFormHeight = .Height
        miOldFormWidth = .Width
    End With
    
'    'Create Work Tables In TempDB
'    If Not bInitWorkTables Then
'        'Can 't initialize Work Tables, so exit
'        Exit Sub
'    End If
    
    GetModuleOptions
    
    'Setup Selection Grids
    SetupSalesOrderSelectGrid
    SetupTransferSelectGrid
       
    Set moMultSelGridMgr = New clsMultiSelGridSettings
    moMultSelGridMgr.Initialize moAppDB, moSysSession.Language, moSelect(), grdSettingsSel

    If CBool(moClass.moSysSession.IsModuleActivated(kModuleWM)) = True Then
        mbWMIsLicensed = moClass.moSysSession.IsModuleLicensed(kModuleWM)
    Else
        mbWMIsLicensed = False
    End If
    
    'The Transfer grid should not be available if SO & IM are not integrated.
    '             - this block of code moved after moMultSelGridMgr.Initialize
    '             - chkInclude(kTransfer).Value triggers click, which references moMultSelGridMgr
    If Not mbIMIntegrated Or Not mbWMIsLicensed Then
        chkInclude(kTransfer).Value = vbUnchecked
        chkInclude(kTransfer).Enabled = False
        fraTransfer.Visible = False
    Else
        chkInclude(kTransfer).Value = vbChecked
        chkInclude(kTransfer).Enabled = True
        fraTransfer.Visible = True
    End If

    For i = 0 To cboSASeq.UBound
        cboSASeq(i).AddItem msSASNone
        cboSASeq(i).AddItem mskSASFillSalesBeforeTransfer
        cboSASeq(i).AddItem mskSASFillTransferBeforeSales
        cboSASeq(i).AddItem mskSASFillBackOrdersFirst
        cboSASeq(i).AddItem mskSASOrderExpirationDate
        cboSASeq(i).AddItem mskSASHighestOrderPriority
        cboSASeq(i).AddItem mskSASEarliestShipDate
    Next
    
    cboSASeq(kSASeqFirst).ListIndex = kSASFillSalesBeforeTransfer
    cboSASeq(kSASeqSecond).ListIndex = kSASFillBackOrdersFirst
    cboSASeq(kSASeqThird).ListIndex = kSASOrderExpirationDate
    
    If Not mbIMIntegrated Then
        chkInclude(kInclZeroAvail).Value = vbUnchecked
        chkInclude(kInclZeroAvail).Enabled = False

        fraQtyPickOpt.Visible = False
    Else
        If miAllowNegQty = 0 Then
            fraQtyPickOpt.Visible = False
        Else
            fraQtyPickOpt.Visible = True
        End If
    End If
    
    'Setup Selection Results Grids
    SetupOrderGrid
    SetupLineGrid
    BindOrderGrid
    BindLineGrid
    BindGM
    
    'Setup ContextMenu
    BindContextMenu
    
    'Setup Toolbar
    SetupBars
    
    'Setup Options for save settings
    Set moOptions = New clsOptions
    With moOptions
        For i = 0 To chkInclude.UBound
            .Add chkInclude(i)
        Next
        .Add chkEmptyBins
        .Add chkEmptyRandomBin
        .Add chkOrderLimit
        .Add txtMaxPickNo
        For i = 0 To cboSASeq.UBound
            .Add cboSASeq(i)
        Next
        
        For i = 0 To optStockQtyPick.UBound
            .Add optStockQtyPick(i)
        Next
    End With
        
    Set moSettings = New clsSettings
    moSettings.Initialize Me, cboSetting, , moMultSelGridMgr, moOptions, , tbrMain, sbrMain
    
    'perform some initializations
    bCreateNewList
    
    'map controls for function keys
    MapControls
    
    'need to capture left mouse clicks to prevent user from double clicking on command buttons after
    'selection grid navigator has been used.
    If moHook Is Nothing Then
        Set moHook = New AFWinHook.clsMsgs
        moHook.SetHook Me.hwnd, HOOK_TYPE_CUSTOM
        moHook.AddMessage WM_LBUTTONDOWN
        moHook.Delayed = True
    End If
    
    ClearForm
    
    SetupRowsSelCaption
    
    sbrMain.Status = SOTA_SB_START
    
    'Set the selection Tab as the default start tab
    tabSelect.Tab = kTabSelection
    
    mbLoading = False
    
    'Form_Load completed successfully
    mbLoadSuccess = True
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Load", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Function bGetLocalStrings() As Boolean
'+++ VB/Rig Begin Push +++                                                                'Repository Error Rig  {1.1.1.0.0}
#If ERRORTRAPON Then                                                                      'Repository Error Rig  {1.1.1.0.0}
    On Error GoTo VBRigErrorRoutine                                                       'Repository Error Rig  {1.1.1.0.0}
#End If                                                                                   'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
    bGetLocalStrings = False

    'LocalStrings for grd frame caption
    msOrderToPick = gsBuildString(kSOOrderToPick, moAppDB, moSysSession)
    msOrdersToPick = gsBuildString(kSOOrdersToPick, moAppDB, moSysSession)
    msLineToPick = gsBuildString(kSOLineSelForPick, moAppDB, moSysSession)
    msLinesToPick = gsBuildString(kSOLinesSelForPick, moAppDB, moSysSession)
    
    'LocalStrings for Allocation Sequence Option list boxes
    msSASNone = gsBuildString(kSONone, moAppDB, moSysSession)
    mskSASFillSalesBeforeTransfer = gsBuildString(kSOFillSalesBeforeTransfer, moAppDB, moSysSession)
    mskSASFillTransferBeforeSales = gsBuildString(kSOFillTransferBeforeSales, moAppDB, moSysSession) '"Fill Transfer Before Sales"
    mskSASFillBackOrdersFirst = gsBuildString(kSOFillBackOrdersFirst, moAppDB, moSysSession) '"Fill Back Orders First"
    mskSASOrderExpirationDate = gsBuildString(kSOOrderExpirationDate, moAppDB, moSysSession) '"Order Expiration Date"
    mskSASHighestOrderPriority = gsBuildString(kSOHighestOrderPriority, moAppDB, moSysSession) '"Highest Order Priority"
    mskSASEarliestShipDate = gsBuildString(kSOEarliestOrderShipDate, moAppDB, moSysSession) '"Earliest Order Ship Date"
    msYes = gsBuildString(kSOYes, moAppDB, moSysSession)
    msNo = gsBuildString(kSONo, moAppDB, moSysSession)
    
    bGetLocalStrings = True

    'Exit this function
'+++ VB/Rig Begin Pop +++                                                                 'Repository Error Rig  {1.1.1.0.0}
    Exit Function                                                                         'Repository Error Rig  {1.1.1.0.0}
VBRigErrorRoutine:                                                                        'Repository Error Rig  {1.1.1.0.0}
        gSetSotaErr Err, sMyName, "bGetLocalStrings", VBRIG_IS_FORM                       'Repository Error Rig  {1.1.1.0.0}
        Err.Raise guSotaErr.Number                                                        'Repository Error Rig  {1.1.1.0.0}
'+++ VB/Rig End +++                                                                       'Repository Error Rig  {1.1.1.0.0}
End Function
Public Sub GetModuleOptions()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moModuleOptions = New clsModuleOptions
    With moModuleOptions
        Set .oSysSession = moSysSession
        Set .oAppDb = moAppDB
        .sCompanyID = msCompanyID
    End With
    miNbrDecPlaces = giGetValidInt(moModuleOptions.CI("QtyDecPlaces"))
    miUseNationalAccts = giGetValidInt(moModuleOptions.AR("UseNationalAccts"))
    
    'Only get miAllowNegQty when IM enabled
    If mbIMIntegrated Then
        miAllowNegQty = giGetValidInt(moModuleOptions.IM("AllowNegQtyOnHand"))
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "GetModuleOptions", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick cboSetting, True
    #End If
'+++ End Customizer Code Push +++
    Dim i As Integer
    If Not moSettings.gbSkipClick Then
        moSettings.bLoadReportSettings
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "cboSetting_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Public Function ModuleActive(iModule As Integer) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim sSQL                As String
Dim rs                  As Object
Dim miModuleNo          As Integer
Dim miModuleActive      As Integer


    sSQL = "SELECT ModuleNo, Active  FROM tsmCompanyModule WHERE CompanyID = " & gsQuoted(msCompanyID) & _
" AND ModuleNo = " & iModule
    
    Set rs = moClass.moAppDB.OpenRecordset(sSQL, kSnapshot, kOptionNone)
    If rs.IsEmpty Then
        Set rs = Nothing
        ModuleActive = False
'+++ VB/Rig Begin Pop +++
'+++ VB/Rig End +++
        Exit Function
    End If

    miModuleActive = rs.Field("Active")
    miModuleNo = rs.Field("ModuleNo")
  
    If (miModuleActive = 1 And miModuleNo = iModule) Then
          ModuleActive = True
    Else
          ModuleActive = False
    End If
    Set rs = Nothing
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ModuleActive", VBRIG_IS_FORM
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++
End Function

Private Sub SetupSalesOrderSelectGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Local Object Variables
    Dim moDDData                As clsDDData
    Dim sRealTableCollection    As Collection
    Dim moSelAssist             As clsSelGridAssist
    Dim sLookupRestrict         As String

    'Row Search Variables
    Dim bGoodRow                As Boolean
    Dim sGrdTableName           As String
    Dim sGrdColumnName          As String
    Dim sGrdParentTblName       As String
    Dim lIndex                  As Long
    Dim lCurrRow                As Long
    Dim lRet                    As Long
    Dim bResult                 As Boolean
    Dim i                       As Integer
    Dim arrStaticValues()       As String
    Dim arrStaticText()         As String
    Dim sRestrictionClause      As String
    Dim sString                 As String
    
    'Instantiate Objects
    Set moSelect(kSalesOrder) = New clsSelection
    Set sRealTableCollection = New Collection
    Set moSelAssist = New clsSelGridAssist
    Set moDDData = New clsDDData
    
    'Set up selection grid for Sales Order tab first
    sRealTableCollection.Add "tsoSOLineDist"
    moDDData.lInitDDData sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID
    moDDData.lAddDDDataRow "tsoSalesOrder", "CustPONo", True, False
    moDDData.lAddDDDataRow "tsoSalesOrder", "CustKey", True, False
    'moDDData.lAddDDDataRow "tsoSOLineDist", "DeliveryMeth", True, False
    moDDData.lAddDDDataRow "tsoSOLineDist", "Hold", True, False
    
    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        'Unable to load data from Data Dictionary.
        giSotaMsgBox Me, moSysSession, kmsgCantLoadDDData
        Exit Sub
    End If
    
    'Overide default caption for ShipDate.
    bResult = moDDData.bSetDDCaption("tsoSOLineDist", "ShipDate", gsBuildString(kSOShipDate, moAppDB, moSysSession), 3)

    'Add Manual Row Entries
    With moSelAssist
        Set .Sys_DB = moAppDB
        Set .Selection = moSelect(kSalesOrder)

        .Add "tsoSalesOrder", "TranNoRelChngOrd", , , , , " tsoSalesOrder.Status = " & kOpen _
        & " AND tsoSalesOrder.TranType = " & kTranTypeSOSO
        .Add "tsoSOLine", "SOLineNo", gsBuildString(kSOSOLine, moAppDB, moSysSession), False
        .Add "tarCustomer", "CustName"
    End With

    With moSelect(kSalesOrder)
        sString = gsBuildString(kShippingWhse, moAppDB, moSysSession)
        .lAddSelRowAllParams "tsoSOLineDist", "WhseKey", "SurrogateKey", sString, sString, 6, SQL_CHAR, 0, True, "", "Warehouse", "", True, True, "WhseID", "timWarehouse", "WhseKey"

        sString = gsBuildString(kIMItem, moAppDB, moSysSession)
        sLookupRestrict = "ItemType IN (1,2,4,5,6,7,8)"
        .lAddSelRowAllParams "tsoSOLine", "ItemKey", "SurrogateKey", sString, sString, 30, SQL_CHAR, 0, True, "", "Item", sLookupRestrict, True, True, "ItemID", "timItem", "ItemKey"
    
        sString = gsBuildString(kSOShipvia, moAppDB, moSysSession)
        .lAddSelRowAllParams "tsoSOLineDist", "ShipMethKey", "SurrogateKey", sString, sString, 15, SQL_CHAR, 0, True, "", "ShipMethod", "", True, True, "ShipMethID", "tciShipMethod", "ShipMethKey"

        sString = gsBuildString(kSOShipToAddress1, moAppDB, moSysSession)
        sRestrictionClause = "CustKey IN (SELECT CustKey FROM tarCustomer WITH (NOLOCK) WHERE CompanyID = '" & moClass.moSysSession.CompanyId & "')"
        .lAddSelRowAllParams "tsoSOLineDist", "ShipToCustAddrKey", "SurrogateKey", sString, sString, 15, SQL_CHAR, 0, False, "", "CustomerAddress", sRestrictionClause, True, True, "CustAddrID", "tarCustAddr", "AddrKey"

        sString = gsBuildString(kSOOrderExpirationDate, moAppDB, moSysSession)
        .lAddSelRowAllParams "tsoSalesOrder", "Expiration", "Date", sString, sString, 8, SQL_DATE, 0, _
            False, "", "", "", False, False, "", "tsoSalesOrder", "Expiration"
    End With

    'Add static list for ship priority.
    ReDim arrStaticValues(5)
    ReDim arrStaticText(5)
    For i = 1 To 5
        arrStaticValues(i) = i
        arrStaticText(i) = CStr(i)
    Next i
    
    sString = gsBuildString(kSOShipPriority, moAppDB, moSysSession)
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist", "ShipPriority", sString _
        , sString, False, "", arrStaticValues(), arrStaticText())
    End With
    
    'Add static list for Item Type.
    ReDim arrStaticValues(7)
    ReDim arrStaticText(7)
    sString = gsBuildString(ksMiscItem, moAppDB, moSysSession)
    arrStaticValues(1) = 1
    arrStaticText(1) = sString
    sString = gsBuildString(ksService, moAppDB, moSysSession)
    arrStaticValues(2) = 2
    arrStaticText(2) = sString
    sString = gsBuildString(ksCommentOnly, moAppDB, moSysSession)
    arrStaticValues(3) = 4
    arrStaticText(3) = sString
    sString = gsBuildString(kslvFinishedGood, moAppDB, moSysSession)
    arrStaticValues(4) = 5
    arrStaticText(4) = sString
    sString = gsBuildString(ksRawMaterial, moAppDB, moSysSession)
    arrStaticValues(5) = 6
    arrStaticText(5) = sString
    sString = gsBuildString(kslvBTOKit, moAppDB, moSysSession)
    arrStaticValues(6) = 7
    arrStaticText(6) = sString
    sString = gsBuildString(kslvAssembledKit, moAppDB, moSysSession)
    arrStaticValues(7) = 8
    arrStaticText(7) = sString
    
    sString = gsBuildString(kIMItemType, moAppDB, moSysSession)
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("timItem", "ItemType", sString _
        , sString, False, "", arrStaticValues(), arrStaticText())
    End With
    
    'add static list for Hold
    ReDim arrStaticValues(2)
    ReDim arrStaticText(2)
    arrStaticValues(1) = 0
    arrStaticText(1) = msNo
    arrStaticValues(2) = 1
    arrStaticText(2) = msYes
    
    sString = gsBuildString(kSOSalesOrdHold, moAppDB, moSysSession)
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSalesOrder", "Hold", sString _
        , sString, False, "", arrStaticValues() _
        , arrStaticText())
    End With
    
    sString = gsBuildString(kSOSOLineHold, moAppDB, moSysSession)
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist", "Hold", sString _
        , sString, False, "", arrStaticValues() _
        , arrStaticText())
    End With
    
    'Add static list for Delivery Method
    ReDim arrStaticValues(3)
    ReDim arrStaticText(3)
    sString = gsBuildString(kShip, moAppDB, moSysSession)
    arrStaticValues(1) = 1
    arrStaticText(1) = sString
    sString = gsBuildString(kslvCounterSale, moAppDB, moSysSession)
    arrStaticValues(2) = 3
    arrStaticText(2) = sString
    sString = gsBuildString(kWillCallCol, moAppDB, moSysSession)
    arrStaticValues(3) = 4
    arrStaticText(3) = sString
    
    sString = gsBuildString(kSODelMethod, moAppDB, moSysSession)
    'tsoSOLineDist", "DeliveryMeth
    With moSelect(kSalesOrder)
        lRet = .lAddSelRowStaticList("tsoSOLineDist", "DeliveryMeth", sString _
        , sString, False, "", arrStaticValues() _
        , arrStaticText())
    End With
    
    'Initialize selection grid for Sales Order tab
    If Not moSelect(kSalesOrder).bInitSelect(sRealTableCollection(1), mskControl(kSalesOrder), navControl, nbrControl(kSalesOrder), _
        curControl(kSalesOrder), grdSelection(kSalesOrder), fraSalesOrder, Me, moDDData, _
        19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, False) Then
        'Setup of the Selection Grid Failed.
        giSotaMsgBox Me, moSysSession, kmsgSetupSelectionGridFail
        Exit Sub
    End If
 
    'Populate selection grid for Sales Order tab
    moSelect(kSalesOrder).PopulateSelectionGrid
    
    'Hide Selection Rows Not Required
    For lIndex = 1 To moSelect(kSalesOrder).lMaxSelGridRows
        bGoodRow = False
        moSelect(kSalesOrder).lGetTblColFromGrdRow lIndex, sGrdTableName, sGrdColumnName, _
        sGrdParentTblName, False
        
        Select Case sGrdTableName
            Case "tsoSOLineDist"
                Select Case sGrdColumnName
                    Case "ShipDate", "ShipPriority", "ShipMethKey", "ShipToCustAddrKey", "DeliveryMeth", "Hold"
                        bGoodRow = True
                    Case "WhseKey"
                        If mbIMIntegrated Then bGoodRow = True
                End Select
            Case "tsoSalesOrder"
                Select Case sGrdColumnName
                    Case "TranNoRelChngOrd", "CustKey", "CustPONo", "Hold", "Expiration"
                        bGoodRow = True
                End Select
            Case "tsoSOLine"
                Select Case sGrdColumnName
                    Case "SOLineNo", "ItemKey"
                        bGoodRow = True
                End Select
            Case "tarCustomer"
                Select Case sGrdColumnName
                    Case "CustName"
                        bGoodRow = True
                End Select
            Case "timItem"
                Select Case sGrdColumnName
                    Case "ItemType"
                        bGoodRow = True
                End Select
        End Select
                        
        If Not bGoodRow Then
            moSelect(kSalesOrder).SetSelectionRowVisibility sGrdTableName, sGrdColumnName, False
        End If
    Next lIndex
    
    'Order Rows
    With moSelect(kSalesOrder)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipDate")
        lRet = .lMoveRow(lCurrRow, 1)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "WhseKey")
        lRet = .lMoveRow(lCurrRow, 2)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipPriority")
        lRet = .lMoveRow(lCurrRow, 3)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "TranNoRelChngOrd")
        lRet = .lMoveRow(lCurrRow, 4)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLine", "SOLineNo")
        lRet = .lMoveRow(lCurrRow, 5)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "CustPONo")
        lRet = .lMoveRow(lCurrRow, 6)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLine", "ItemKey")
        lRet = .lMoveRow(lCurrRow, 7)
        lCurrRow = .lGetGrdRowFromTblCol("timItem", "ItemType")
        lRet = .lMoveRow(lCurrRow, 8)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipMethKey")
        lRet = .lMoveRow(lCurrRow, 9)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "Expiration")
        lRet = .lMoveRow(lCurrRow, 10)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "CustKey")
        lRet = .lMoveRow(lCurrRow, 11)
        lCurrRow = .lGetGrdRowFromTblCol("tarCustomer", "CustName")
        lRet = .lMoveRow(lCurrRow, 12)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "ShipToCustAddrKey")
        lRet = .lMoveRow(lCurrRow, 13)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "DeliveryMeth")
        lRet = .lMoveRow(lCurrRow, 14)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSalesOrder", "Hold")
        lRet = .lMoveRow(lCurrRow, 15)
        lCurrRow = .lGetGrdRowFromTblCol("tsoSOLineDist", "Hold")
        lRet = .lMoveRow(lCurrRow, 16)
    End With
   
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    If Not sRealTableCollection Is Nothing Then
        Set sRealTableCollection = Nothing
    End If
    
    If Not moSelAssist Is Nothing Then
        Set moSelAssist = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupSalesOrderSelectGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub SetupTransferSelectGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Local Object Variables
    Dim moDDData                As clsDDData
    Dim sRealTableCollection    As Collection
    Dim moSelAssist             As clsSelGridAssist
    Dim sLookupRestrict         As String

    'Row Search Variables
    Dim bGoodRow                As Boolean
    Dim sGrdTableName           As String
    Dim sGrdColumnName          As String
    Dim sGrdParentTblName       As String
    Dim lIndex                  As Long
    Dim lCurrRow                As Long
    Dim lRet                    As Long
    Dim bResult                 As Boolean
    Dim i                       As Integer
    Dim arrStaticValues()       As String
    Dim arrStaticText()         As String
    Dim sRestrictionClause      As String
    Dim sString                 As String
    
    'Instantiate Objects
    Set moSelect(kTransfer) = New clsSelection
    Set sRealTableCollection = New Collection
    Set moDDData = New clsDDData
    
    'Set up selection grid for Sales Order tab first
    sRealTableCollection.Add "vimTrnsfrOrder"
    
    Set moDDData = Nothing
    Set moDDData = New clsDDData
    
    moDDData.lInitDDData sRealTableCollection, moClass.moAppDB, moClass.moAppDB, msCompanyID

    If Not moDDData.lDDBuildData(App.EXEName) = kSuccess Then
        'Unable to load data from Data Dictionary.
        giSotaMsgBox Me, moSysSession, kmsgCantLoadDDData
        Exit Sub
    End If

    With moSelect(kTransfer)
    
        sString = gsBuildString(kIMItem, moAppDB, moSysSession)
        .lAddSelRowAllParams "timTrnsfrOrderLine", "ItemKey", "SurrogateKey", sString, sString, 30, SQL_CHAR, 0, True, "", "Item", "", True, True, "ItemID", "timItem", "ItemKey"
        
        sString = gsBuildString(kIMTrnsfrOrdLine, moAppDB, moSysSession)
        .lAddSelRowAllParams "timTrnsfrOrderLine", "TrnsfrOrderLineKey", "SurrogateKey", sString, sString, 2, SQL_SMALLINT, 0, False, "", "", "", True, True, "TrnsfrLineNo", "timTrnsfrOrderLine", "TrnsfrOrderLineKey"
    End With
    
        'Add static list for Item Type.
    ReDim arrStaticValues(3)
    ReDim arrStaticText(3)
    arrStaticText(3) = sString
    sString = gsBuildString(kslvFinishedGood, moAppDB, moSysSession)
    arrStaticValues(1) = 5
    arrStaticText(1) = sString
    sString = gsBuildString(ksRawMaterial, moAppDB, moSysSession)
    arrStaticValues(2) = 6
    arrStaticText(2) = sString
    sString = gsBuildString(kslvAssembledKit, moAppDB, moSysSession)
    arrStaticValues(3) = 8
    arrStaticText(3) = sString
    
    sString = gsBuildString(kIMItemType, moAppDB, moSysSession)
    With moSelect(kTransfer)
        lRet = .lAddSelRowStaticList("timItem", "ItemType", sString _
        , sString, False, "", arrStaticValues(), arrStaticText())
    End With

    'Initialize selection grid for Transfer Order tab
    If Not moSelect(kTransfer).bInitSelect(sRealTableCollection(1), mskControl(kTransfer), navControlTrnsfr, nbrControl(kTransfer), _
        curControl(kTransfer), grdSelection(kTransfer), fraTransfer, Me, moDDData, _
        19 * 120 + 60, 17 * 120, 18 * 120, 18 * 120, False) Then
        'Setup of the Selection Grid Failed.
        giSotaMsgBox Me, moSysSession, kmsgSetupSelectionGridFail
        Exit Sub
    End If
 
    'Populate selection grid for Transfer Order tab
    moSelect(kTransfer).PopulateSelectionGrid
    
    'Hide Selection Rows Not Required
    For lIndex = 1 To moSelect(kTransfer).lMaxSelGridRows
        bGoodRow = False
        moSelect(kTransfer).lGetTblColFromGrdRow lIndex, sGrdTableName, sGrdColumnName, _
                sGrdParentTblName, False
               
        Select Case sGrdTableName
            Case "vimTrnsfrOrder"
                Select Case sGrdColumnName
                    Case "SchdShipDate", "TrnsfrOrderKey", "ShipMethKey", "ShipWhseKey", "RcvgWhseKey", "TranNo"
                        bGoodRow = True
                End Select
        End Select

        Select Case sGrdTableName
            Case "timTrnsfrOrderLine"
                Select Case sGrdColumnName
                    Case "SchdShipDate", "TrnsfrOrderLineKey", "ItemKey"
                        bGoodRow = True
                End Select
        End Select
        
        Select Case sGrdTableName
            Case "timItem"
                Select Case sGrdColumnName
                    Case "ItemType"
                        bGoodRow = True
                End Select
        End Select
        
        If Not bGoodRow Then
            moSelect(kTransfer).SetSelectionRowVisibility sGrdTableName, sGrdColumnName, False
        End If
    Next lIndex
    
    'Order Rows
    With moSelect(kTransfer)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "SchdShipDate")
        lRet = .lMoveRow(lCurrRow, 1)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "ShipWhseKey")
        lRet = .lMoveRow(lCurrRow, 2)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "TranNo")
        lRet = .lMoveRow(lCurrRow, 3)
        lCurrRow = .lGetGrdRowFromTblCol("timTrnsfrOrderLine", "TrnsfrOrderLineKey")
        lRet = .lMoveRow(lCurrRow, 4)
        lCurrRow = .lGetGrdRowFromTblCol("timTrnsfrOrderLine", "ItemKey")
        lRet = .lMoveRow(lCurrRow, 5)
        lCurrRow = .lGetGrdRowFromTblCol("timItem", "ItemType")
        lRet = .lMoveRow(lCurrRow, 6)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "ShipMethKey")
        lRet = .lMoveRow(lCurrRow, 7)
        lCurrRow = .lGetGrdRowFromTblCol("vimTrnsfrOrder", "RcvgWhseKey")
        lRet = .lMoveRow(lCurrRow, 8)
     End With
      
    If Not moDDData Is Nothing Then
        Set moDDData = Nothing
    End If
    
    If Not sRealTableCollection Is Nothing Then
        Set sRealTableCollection = Nothing
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupTransferSelectGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMLineGrid = New clsDmGrid
    With moDMLineGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmSelectPick
        Set .Grid = grdLinePicked
        #If RPTDEBUG Then
            .Table = "tsoCreatePickWrk"
        #Else
            .Table = "#tsoCreatePickWrk2"
        #End If
        .UniqueKey = "LineKey, LineDistKey"
        .NoAppend = True
        .OrderBy = "#tsoCreatePickWrk2.ItemID, #tsoCreatePickWrk2.QtyAvail DESC"
        .Where = "LineHold <> 1 AND CrHold <> 1 AND OrderHold <> 1"
        
        .BindColumn "AllowDecimalQty", kColLineAllowDecimalQty, SQL_SMALLINT
        .BindColumn "CrHold", Nothing, SQL_SMALLINT
        .BindColumn "CustID", Nothing, SQL_CHAR
        .BindColumn "CustKey", Nothing, SQL_INTEGER
        .BindColumn "CustName", kColLineShiptoName, SQL_VARCHAR
        .BindColumn "DeliveryMethod", Nothing, SQL_SMALLINT
        .BindColumn "DeliveryMethodID", kColLineDeliveryMethod, SQL_CHAR
        .BindColumn "ItemDesc", kColLineItemDesc, SQL_VARCHAR
        .BindColumn "ItemID", kColLineItem, SQL_CHAR
        .BindColumn "ItemKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "ItemType", Nothing, SQL_SMALLINT
        .BindColumn "LineDistKey", kColLineTranLineDistKey, SQL_INTEGER, , kDmSetNull
        .BindColumn "LineHold", Nothing, SQL_SMALLINT
        .BindColumn "LineHoldReason", Nothing, SQL_CHAR
        .BindColumn "LineKey", kColLineTranLineKey, SQL_INTEGER
        .BindColumn "LineNbr", kColLineLine, SQL_SMALLINT
        .BindColumn "OrderHold", Nothing, SQL_SMALLINT
        .BindColumn "OrderHoldReason", Nothing, SQL_SMALLINT
        .BindColumn "OrderLineUnitMeasID", kColLineUOM, SQL_CHAR
        .BindColumn "OrderLineUnitMeasKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "QtyAvail", kColLineQtyAvail, SQL_DECIMAL
        .BindColumn "QtyToPick", kColLineQtyToPick, SQL_DECIMAL
        .BindColumn "QtyToPickByOrderUOM", kColLineQtyToPickByOrdUOM, SQL_DECIMAL
        .BindColumn "RcvgWhseID", Nothing, SQL_CHAR
        .BindColumn "ShipDate", kColLineShipDate, SQL_DATE
        .BindColumn "ShipMethID", kColLineShipVia, SQL_CHAR
        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER
        .BindColumn "ShipPriority", kColLineShipPriority, SQL_SMALLINT
        .BindColumn "ShipToAddrKey", Nothing, SQL_INTEGER
        .BindColumn "ShiptoCustAddress", kColLineShiptoAddr, SQL_CHAR
        .BindColumn "ShiptoCustAddrID", kColLineShiptoAddrID, SQL_CHAR
        .BindColumn "ShipWhseID", kColLineWarehouse, SQL_CHAR
        .BindColumn "ShipWhseKey", Nothing, SQL_INTEGER, , kDmSetNull
        .BindColumn "TranNoRelChngOrd", kColLineOrder, SQL_CHAR
        .BindColumn "TranType", kColLineTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kColLineTranTypeText, SQL_CHAR
        
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub BindOrderGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moDMOrderGrid = New clsDmGrid
    With moDMOrderGrid
        Set .Session = moSysSession
        Set .Database = moAppDB
        Set .Form = frmSelectPick
        Set .Grid = grdOrderPicked
        
        #If RPTDEBUG Then
            .Table = "tsoCreatePickHdrWrk"
        #Else
            .Table = "#tsoCreatePickHdrWrk"
        #End If
        .UniqueKey = "TranKey" ', TranType"
        .NoAppend = True
        .OrderBy = "TranID"
        .Where = "Hold <> 1 AND CrHold <> 1"
        
        .BindColumn "CrHold", Nothing, SQL_SMALLINT
        .BindColumn "EarliestShipDate", kColOrdEarliestShipDate, SQL_DATE
        .BindColumn "ExpireDate", kColOrdExpireDate, SQL_DATE
        .BindColumn "Hold", Nothing, SQL_SMALLINT
        .BindColumn "HoldReason", Nothing, SQL_CHAR
        .BindColumn "OrderDate", kColOrdOrderDate, SQL_DATE
        .BindColumn "ShipMethID", kColOrdShipVia, SQL_CHAR
        .BindColumn "ShipMethKey", Nothing, SQL_INTEGER
        .BindColumn "ShipPriority", kColOrdShipPriority, SQL_SMALLINT
        .BindColumn "ShipToAddrKey", Nothing, SQL_INTEGER
        .BindColumn "ShipToCustAddrID", kColOrdShipTo, SQL_CHAR
        .BindColumn "ShipWhseID", kColOrdShipFromWhse, SQL_CHAR
        .BindColumn "ShipWhseKey", Nothing, SQL_INTEGER
        .BindColumn "TranID", kColOrdOrder, SQL_CHAR
        .BindColumn "TranKey", kColOrdTranKey, SQL_INTEGER
        .BindColumn "TranType", kcolOrdTranType, SQL_INTEGER
        .BindColumn "TranTypeText", kColOrdTranTypeText, SQL_CHAR

        'SGS DEJ for IA (START)
        .LinkSource "vluBOLTran_SGS", .Table & ".TranKey=vluBOLTran_SGS.TranKey and " & .Table & ".TranType=vluBOLTran_SGS.TranType", kDmJoin, LeftOuter
        .Link kcolOrdBOL, "BOLNo"
        'SGS DEJ for IA (STOP)

        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindOrderGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub SetupLineGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdLinePicked, kMaxLineCols, kGridDataSheetNoAppend
    gGridSetColors grdLinePicked
        
    With grdLinePicked
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With

  'set column captions
    gGridSetHeader grdLinePicked, kColLineItem, gsBuildString(kSOItemOrder, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineShiptoAddrID, gsBuildString(kSOShipTo, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineTranTypeText, gsBuildString(kTypeCol, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineQtyToPick, gsBuildString(kSOQtyToPick, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineQtyAvail, gsBuildString(kSOAvailToShip, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineUOM, gsBuildString(kSOUOM, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineShipDate, gsBuildString(kSOShipDate, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineDeliveryMethod, gsBuildString(kSODelivery, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineShipVia, gsBuildString(kShipMethodTbl, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineShipPriority, gsBuildString(kSOShpPriority, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineOrder, gsBuildString(kSOOrderNo, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineLine, gsBuildString(kLineNoCol, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineWarehouse, gsBuildString(kSOWhse, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineShiptoName, gsBuildString(kSOShipToName, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineShiptoAddr, gsBuildString(kSOShipToAddress1, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineItemDesc, gsBuildString(kSODescription, moAppDB, moSysSession)
    gGridSetHeader grdLinePicked, kColLineTranKey, "TranKey"
    gGridSetHeader grdLinePicked, kColLineTranLineKey, "TranLineKey"
    gGridSetHeader grdLinePicked, kColLineTranLineDistKey, "TranLineDistKey"
    gGridSetHeader grdLinePicked, kColLineTranType, "TranType"
    gGridSetHeader grdLinePicked, kColLineAllowDecimalQty, "AllowDecimalQty"
    gGridSetHeader grdLinePicked, kColLineQtyToPickByOrdUOM, gsBuildString(kSOQtyOpen, moAppDB, moSysSession)
    
  'Set column widths
    gGridSetColumnWidth grdLinePicked, kColLineItem, 12
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddrID, 12
    gGridSetColumnWidth grdLinePicked, kColLineTranTypeText, 6
    gGridSetColumnWidth grdLinePicked, kColLineQtyToPick, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyToPickByOrdUOM, 8
    gGridSetColumnWidth grdLinePicked, kColLineQtyAvail, 9
    gGridSetColumnWidth grdLinePicked, kColLineUOM, 6
    gGridSetColumnWidth grdLinePicked, kColLineShipDate, 8
    gGridSetColumnWidth grdLinePicked, kColLineDeliveryMethod, 6
    gGridSetColumnWidth grdLinePicked, kColLineShipVia, 10
    gGridSetColumnWidth grdLinePicked, kColLineShipPriority, 6
    gGridSetColumnWidth grdLinePicked, kColLineOrder, 12
    gGridSetColumnWidth grdLinePicked, kColLineLine, 4
    gGridSetColumnWidth grdLinePicked, kColLineWarehouse, 6
    gGridSetColumnWidth grdLinePicked, kColLineShiptoName, 20
    gGridSetColumnWidth grdLinePicked, kColLineShiptoAddr, 20
    gGridSetColumnWidth grdLinePicked, kColLineItemDesc, 20
    gGridSetColumnWidth grdLinePicked, kColLineTranKey, 8
    gGridSetColumnWidth grdLinePicked, kColLineTranLineKey, 8
    gGridSetColumnWidth grdLinePicked, kColLineTranLineDistKey, 8
    gGridSetColumnWidth grdLinePicked, kColLineTranType, 8
  
  'Set column types
    gGridSetColumnType grdLinePicked, kColLineItem, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddrID, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranTypeText, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineQtyToPick, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineQtyAvail, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineQtyToPickByOrdUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineUOM, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdLinePicked, kColLineDeliveryMethod, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineOrder, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineLine, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineWarehouse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoName, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineShiptoAddr, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineItemDesc, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranLineKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranLineDistKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdLinePicked, kColLineTranType, SS_CELL_TYPE_EDIT

    'lock protected columns
    gGridLockColumn grdLinePicked, kColLineItem
    gGridLockColumn grdLinePicked, kColLineShiptoAddrID
    gGridLockColumn grdLinePicked, kColLineTranTypeText
    gGridLockColumn grdLinePicked, kColLineQtyToPick
    gGridLockColumn grdLinePicked, kColLineQtyAvail
    gGridLockColumn grdLinePicked, kColLineQtyToPickByOrdUOM
    gGridLockColumn grdLinePicked, kColLineUOM
    gGridLockColumn grdLinePicked, kColLineShipDate
    gGridLockColumn grdLinePicked, kColLineDeliveryMethod
    gGridLockColumn grdLinePicked, kColLineShipVia
    gGridLockColumn grdLinePicked, kColLineShipPriority
    gGridLockColumn grdLinePicked, kColLineOrder
    gGridLockColumn grdLinePicked, kColLineLine
    gGridLockColumn grdLinePicked, kColLineWarehouse
    gGridLockColumn grdLinePicked, kColLineShiptoName
    gGridLockColumn grdLinePicked, kColLineShiptoAddr
    gGridLockColumn grdLinePicked, kColLineItemDesc
    gGridLockColumn grdLinePicked, kColLineTranKey
    gGridLockColumn grdLinePicked, kColLineTranLineDistKey
    gGridLockColumn grdLinePicked, kColLineTranLineKey
    gGridLockColumn grdLinePicked, kColLineTranType
    
    gGridHAlignColumn grdLinePicked, kColLineShipDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineShipPriority, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdLinePicked, kColLineLine, SS_CELL_TYPE_INTEGER, SS_CELL_H_ALIGN_CENTER
    
    'hide columns
    gGridHideColumn grdLinePicked, kColLineTranKey
    gGridHideColumn grdLinePicked, kColLineTranLineKey
    gGridHideColumn grdLinePicked, kColLineTranLineDistKey
    gGridHideColumn grdLinePicked, kColLineTranType
    gGridHideColumn grdLinePicked, kColLineAllowDecimalQty
    
    gGridFreezeCols grdLinePicked, kColLineItem

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupLineGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub SetupOrderGrid()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set general grid properties
    gGridSetProperties grdOrderPicked, kMaxOrderCols, kGridDataSheetNoAppend
    gGridSetColors grdOrderPicked
        
    With grdOrderPicked
        .OperationMode = SS_OP_MODE_NORMAL
        .AllowMultiBlocks = False
        .SelectBlockOptions = SS_SELBLOCKOPT_ALL
        .NoBeep = True 'prevent beeps on locked cells
    End With
    
    'set column captions
    gGridSetHeader grdOrderPicked, kColOrdOrder, gsBuildString(kSOOrderNumber, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdTranTypeText, gsBuildString(kTypeCol, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdShipVia, gsBuildString(kSOShipvia, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdShipPriority, gsBuildString(kSOShpPriority, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdShipTo, gsBuildString(kSOShipTo, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdOrderDate, gsBuildString(kSOOrderDate, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdExpireDate, gsBuildString(kDD321167ExpireDate, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdEarliestShipDate, gsBuildString(kSOEarliestOrderShipDate, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdShipFromWhse, gsBuildString(kSOShipFrom, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kColOrdTranKey, "TranKey" 'gsBuildString(kTypeCol, moAppDB, moSysSession)
    gGridSetHeader grdOrderPicked, kcolOrdTranType, "TranType" 'gsBuildString(kTypeCol, moAppDB, moSysSession)
  
    'SGS DEJ IA
    gGridSetHeader grdOrderPicked, kcolOrdBOL, "BOL"
  
  'Set column widths
    gGridSetColumnWidth grdOrderPicked, kColOrdOrder, 14
    gGridSetColumnWidth grdOrderPicked, kColOrdTranTypeText, 6
    gGridSetColumnWidth grdOrderPicked, kColOrdShipVia, 10
    gGridSetColumnWidth grdOrderPicked, kColOrdShipPriority, 6
    gGridSetColumnWidth grdOrderPicked, kColOrdShipTo, 12
    gGridSetColumnWidth grdOrderPicked, kColOrdOrderDate, 8
    gGridSetColumnWidth grdOrderPicked, kColOrdExpireDate, 8
    gGridSetColumnWidth grdOrderPicked, kColOrdEarliestShipDate, 8
    gGridSetColumnWidth grdOrderPicked, kColOrdShipFromWhse, 12
    gGridSetColumnWidth grdOrderPicked, kColOrdTranKey, 4
    gGridSetColumnWidth grdOrderPicked, kcolOrdTranType, 4

    'SGS DEJ IA
    gGridSetColumnWidth grdOrderPicked, kcolOrdBOL, 8

  'Set column types
    gGridSetColumnType grdOrderPicked, kColOrdOrder, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdTranTypeText, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShipVia, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShipPriority, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdShipTo, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdOrderDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderPicked, kColOrdExpireDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderPicked, kColOrdEarliestShipDate, SS_CELL_TYPE_DATE
    gGridSetColumnType grdOrderPicked, kColOrdShipFromWhse, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kColOrdTranKey, SS_CELL_TYPE_EDIT
    gGridSetColumnType grdOrderPicked, kcolOrdTranType, SS_CELL_TYPE_EDIT
    
    'SGS DEJ IA
    gGridSetColumnType grdOrderPicked, kcolOrdBOL, SS_CELL_TYPE_EDIT
    
    'lock protected columns
    gGridLockColumn grdOrderPicked, kColOrdOrder
    gGridLockColumn grdOrderPicked, kColOrdTranTypeText
    gGridLockColumn grdOrderPicked, kColOrdShipVia
    gGridLockColumn grdOrderPicked, kColOrdShipPriority
    gGridLockColumn grdOrderPicked, kColOrdShipTo
    gGridLockColumn grdOrderPicked, kColOrdOrderDate
    gGridLockColumn grdOrderPicked, kColOrdExpireDate
    gGridLockColumn grdOrderPicked, kColOrdEarliestShipDate
    gGridLockColumn grdOrderPicked, kColOrdShipFromWhse
    gGridLockColumn grdOrderPicked, kColOrdTranKey
    gGridLockColumn grdOrderPicked, kcolOrdTranType

    'SGS DEJ IA
    gGridLockColumn grdOrderPicked, kcolOrdBOL

    gGridHAlignColumn grdOrderPicked, kColOrdOrderDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdExpireDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdEarliestShipDate, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    gGridHAlignColumn grdOrderPicked, kColOrdShipPriority, SS_CELL_TYPE_EDIT, SS_CELL_H_ALIGN_CENTER
    
    'hide columns
    gGridHideColumn grdOrderPicked, kColOrdTranKey
    gGridHideColumn grdOrderPicked, kcolOrdTranType

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupOrderGrid", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


Private Sub BindGM()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moGMHdr = New clsGridMgr
    With moGMHdr
        Set .Grid = grdOrderPicked
        Set .DM = moDMOrderGrid
        Set .Form = frmSelectPick
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        .Init
    End With
    
    Set moGMLine = New clsGridMgr
    With moGMLine
        Set .Grid = grdLinePicked
        Set .DM = moDMLineGrid
        Set .Form = frmSelectPick
        .GridType = kGridDataSheetNoAppend
        .GridSortEnabled = True
        .Init
    End With
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindGM", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub BindContextMenu()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    Set moContextMenu = New clsContextMenu
    With moContextMenu
        Set .Form = frmSelectPick
        
        .Bind "SOPICK", grdLinePicked.hwnd, kEntTypeSOPicking
        .Bind "*DYNAMIC", grdOrderPicked.hwnd
        
        .Bind "*SELECTION", grdSelection(kSalesOrder).hwnd
        .Bind "*SELECTION", grdSelection(kTransfer).hwnd
        .Init
    End With

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "BindContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function CMAppendContextMenu(ctl As Control, hmenu As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'************************************************************************************
'      Desc: Called to append menu items to right mouse popup menu.
'            Called because form_load did a .Bind *APPEND.
'     Parms: Ctl:   The control that received the right mouse button down message.
'            hmenu: The handle to the popup menu
'   Returns: True if menu item added; False if menu item not added.
'************************************************************************************
    CMAppendContextMenu = False

    Dim lRow As Long
    Dim lTranType As Long
    
    If ctl Is grdOrderPicked Then
        
        lRow = glGetValidLong(ctl.ActiveRow)
        
        If lRow > 0 Then
            lTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, lRow, kcolOrdTranType))
        End If
            
        ' Point control to different context menu
        If (lTranType = kTranTypeIMTR) Then
            moContextMenu.AppendDrillMenu hmenu, "IMOUTORDER", _
                                          ctl, Me, False, kEntTypeIMTransOrdOut
        Else
            moContextMenu.AppendDrillMenu hmenu, "SOORDER", _
                                          ctl, Me, False, kEntTypeSOSalesOrder
        End If
    
    End If
    
    CMAppendContextMenu = True
        
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMAppendContextMenu", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function

Public Function ETWhereClause(ByVal ctl As Object, _
                              ByVal taskID As Long, _
                              ByVal EntityType As Integer, _
                              ByVal naturalKeyCols As String, _
                              ByVal TaskGroup As EntityTask_Groups) As String
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                Entity Task Where Clause. This routine allows the application
'                to specify the where clause to be used against the
'                Host Data View for the specified entity.
'
'                If a where clause is not specified, the where clause will be
'                built using the Natural Key Columns specified for the entity.
'
'                Surrogate Keys as natural keys and other criteria make it
'                impossible without supporting metadata to get these values from
'                the application.
'
'   Parameters:
'                ctl <in> - Control that selected the task
'                taskID <in> - Task ID for the Entity Task
'                entityType <in> - Entity Type bound to the control that selected the task
'                naturalKeyCols <in> - Key columns used to specify where clause (FYI)
'                TaskGroup <in> - Task Group 100-500 (FYI)
'
'   Returns:
'                A specified where clause WITHOUT the 'WHERE' verb. (i.e. "1 = 2")
'*******************************************************************************
On Error Resume Next

    Dim lActiveRow As Long
    Dim lOrderKey As Long
    Dim lTranType As Long
    
    Select Case True
        Case ctl Is grdLinePicked
            lActiveRow = glGetValidLong(grdLinePicked.ActiveRow)
            ETWhereClause = "ShipLineKey = " & CStr(glGetValidLong(gsGridReadCell(grdLinePicked, lActiveRow, kColLineTranLineKey)))
        
        Case ctl Is grdOrderPicked
            lActiveRow = glGetValidLong(grdOrderPicked.ActiveRow)
            
            lOrderKey = glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey))
            lTranType = glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kcolOrdTranType))
        
            If (lTranType = kTranTypeIMTR) Then
                ETWhereClause = "TrnsfrOrderKey = " & CStr(glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey)))
            Else
                ETWhereClause = "SOKey = " & CStr(glGetValidLong(gsGridReadCell(grdOrderPicked, lActiveRow, kColOrdTranKey)))
            End If
        
        Case Else
            ETWhereClause = ""
    End Select

    Err.Clear

End Function

Public Sub PerformCleanShutDown()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        moFormCust.UnloadSelf
        Set moFormCust = Nothing
    End If
#End If
    Dim i As Integer

    '-- Unload all forms loaded from this main form
    gUnloadChildForms Me
         
    '-- Remove all child collections
    giCollectionDel moClass.moFramework, moSotaObjects, -1
    
    If Not moHook Is Nothing Then
        moHook.EndHook
        Set moHook = Nothing
    End If
        
    If Not moContextMenu Is Nothing Then
        Set moContextMenu = Nothing
    End If
    
    If Not moGMHdr Is Nothing Then
        moGMHdr.UnloadSelf
        Set moGMHdr = Nothing
    End If
    
    If Not moGMLine Is Nothing Then
        moGMLine.UnloadSelf
        Set moGMLine = Nothing
    End If
    
    If Not moDMOrderGrid Is Nothing Then
        moDMOrderGrid.UnloadSelf
        Set moDMOrderGrid = Nothing
    End If

    If Not moDMLineGrid Is Nothing Then
        moDMLineGrid.UnloadSelf
        Set moDMLineGrid = Nothing
    End If
    
    For i = 0 To UBound(moSelect)
        If Not moSelect(i) Is Nothing Then
            Set moSelect(i) = Nothing
        End If
    Next
    
    If Not moMultSelGridMgr Is Nothing Then
        Set moMultSelGridMgr = Nothing
    End If
    
    If Not moOptions Is Nothing Then
        Set moOptions = Nothing
    End If
        
    If Not moModuleOptions Is Nothing Then
        Set moModuleOptions = Nothing
    End If

    If Not moAppDB Is Nothing Then
        Set moAppDB = Nothing
    End If
    
    If Not moSysSession Is Nothing Then
        Set moSysSession = Nothing
    End If
    
    If Not moSettings Is Nothing Then
        Set moSettings = Nothing
    End If
    
    If Not moMapSrch Is Nothing Then
        Set moMapSrch = Nothing
    End If
    
       
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "PerformCleanShutDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub SetupBars()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'****************************************************************
' Description:
'    This routine will setup the toolbar with buttons appropriate
'    for the current project.
'****************************************************************
    '-- Setup the status bar
    With sbrMain
        Set .Framework = moClass.moFramework
        .BrowseVisible = False
        '.StatusVisible = False
    End With
    
    '-- Setup the Toolbar
    With tbrMain
        .Style = sotaTB_PROCESS
        .AddButton kTbCancel, .GetIndex(kTbHelp)
        .AddSeparator .GetIndex(kTbHelp)
        .AddButton kTbSave, .GetIndex(kTbHelp)
        .AddButton kTbDelete, .GetIndex(kTbHelp)
        .AddSeparator .GetIndex(kTbHelp)
        .LocaleID = mlLanguage
    End With
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetupBars", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
Dim iConfirmUnload As Integer
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If Not moFormCust.CanShutdown Then
            Cancel = True
            Exit Sub
        End If
    End If
#End If

   
    '-- Reset the CancelShutDown flag if prior shutdowns were canceled.
    mbCancelShutDown = False

        '-- Check all other forms that may have been loaded from this main form.
        '-- If there are any Visible forms, then this means the form is Active.
        '-- Therefore, cancel the shutdown.
        If gbActiveChildForms(Me) Then GoTo CancelShutDown
    
        Select Case UnloadMode
            Case vbFormCode
                '-- Do nothing
            Case Else
                '-- Clean up tmp tables
                 Call HandleToolbarClick(kTbClose)
            
                '-- Most likely the user has requested to shut down the form.
                '-- If the context is normal or Drill-Around, have the object unload itself.
                 Select Case mlRunMode
                    Case kContextAOF
                        Me.Hide
                        GoTo CancelShutDown
                    Case Else
                        moClass.miShutDownRequester = kUnloadSelfShutDown
                End Select
        End Select
    
    '-- If execution gets to this point, the form and class object of the form
    '-- will be shut down. Perform all operations necessary for a clean shutdown.
    PerformCleanShutDown
        
    Select Case moClass.miShutDownRequester
        Case kUnloadSelfShutDown
            moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            Set moClass.moFramework = Nothing
        Case Else
            'Do Nothing
    End Select

    Exit Sub
    
CancelShutDown:
    moClass.miShutDownRequester = kFrameworkShutDown
    mbCancelShutDown = True
    Cancel = True
    Exit Sub

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_QueryUnload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Resize()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    '-- Resize the grid and associated controls
    If (Me.WindowState = vbNormal Or Me.WindowState = vbMaximized) Then
        'QQ 7/8/99.  fixed 13739 & 14655 by not resizing fraSelect and grdSelection
        '-- Move the controls down
        gResizeForm kResizeDown, Me, miOldFormHeight, miMinFormHeight, tabSelect, pnlOrdSelected, fraOrderPicked, grdOrderPicked, pnlLineSelected, fraLinePicked, grdLinePicked, pnlSelection, pnlOptions
        gResizeForm kResizeRight, Me, miOldFormWidth, miMinFormWidth, tabSelect, pnlOrdSelected, fraOrderPicked, grdOrderPicked, pnlLineSelected, fraLinePicked, grdLinePicked, pnlSelection, pnlOptions
        
        miOldFormWidth = Me.Width
        miOldFormHeight = Me.Height
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Resize", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub Form_Unload(Cancel As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    'Set the Class object to Nothing
    Set moClass = Nothing
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "Form_Unload", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMLine.Grid_Change Col, Row
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
   moGMLine.Grid_Click Col, Row
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMLine.Grid_KeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMLine.Grid_LeaveCell Col, Row, NewCol, NewRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdLinePicked_TopLeftChange(ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMLine.Grid_TopLeftChange OldLeft, OldTop, NewLeft, NewTop
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdLinePicked_TopLeftChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Public Function CMRightMouseDown(lWndHandle As Long, lParam As Long) As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim y As Long
    Dim x As Long
    Dim lRow As Long
    Dim lCol As Long
    
    CMRightMouseDown = True
    
    '-- Select the row that was right-clicked on
    If lWndHandle = grdLinePicked.hwnd Then
        y = ((lParam And &HFFFF0000) / &H10000) * Screen.TwipsPerPixelY
        x = (lParam And &HFFFF&) * Screen.TwipsPerPixelX
        
        grdLinePicked.GetCellFromScreenCoord lCol, lRow, x, y
                     
        Select Case lRow
            Case 0, -1, -2  '-- RightClick on Column Header or Gray area
                'Do Nothing
             
            Case Else       '-- RightClick on grid proper
                     
                glRC = SendMessage(lWndHandle, WM_LBUTTONDOWN, 0, lParam)
                glRC = SendMessage(lWndHandle, WM_LBUTTONUP, 0, lParam)   ' Simulate click on grid
                gGridSetSelectRow grdLinePicked, lRow
        End Select
    End If

'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CMRightMouseDown", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Function


Private Sub grdOrderPicked_Change(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    moGMHdr.Grid_Change Col, Row
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++

End Sub

Private Sub grdOrderPicked_Click(ByVal Col As Long, ByVal Row As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
   moGMHdr.Grid_Click Col, Row
   
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_Click", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_KeyDown(keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMHdr.Grid_KeyDown keycode, Shift
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub grdOrderPicked_LeaveCell(ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    moGMHdr.Grid_LeaveCell Col, Row, NewCol, NewRow

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "grdOrderPicked_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

'********************** Selection Stuff ****************************
Private Sub grdSelection_EditMode(Index As Integer, ByVal Col As Long, ByVal Row As Long, ByVal Mode As Integer, ByVal ChangeMade As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(Index).EditMode Col, Row, Mode, ChangeMade

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "frmCreatePick", "grdSelection_EditMode", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub grdSelection_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(Index).SelectionGridGotFocus

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "frmCreatePick", "grdSelection_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub grdSelection_KeyDown(Index As Integer, keycode As Integer, Shift As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(Index).SelectionGridKeyDown keycode, Shift

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "frmCreatePick", "grdSelection_KeyDown", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub grdSelection_LeaveCell(Index As Integer, ByVal Col As Long, ByVal Row As Long, ByVal NewCol As Long, ByVal NewRow As Long, Cancel As Boolean)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(Index).SelectionGridLeaveCell Col, Row, NewCol, NewRow, Cancel
'    If Not mbLoading Then tabSelect.TabCaption(Index) = _
'moMultSelGridMgr.sBuildCaption(msBaseCaption(Index), grdSelection(Index), _
'chkInclude(Index).Value)
        
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "frmCreatePick", "grdSelection_LeaveCell", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub grdSelection_TopLeftChange(Index As Integer, ByVal OldLeft As Long, ByVal OldTop As Long, ByVal NewLeft As Long, ByVal NewTop As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(Index).SelectionGridTopLeftChange OldLeft, OldTop, NewLeft, NewTop

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, "frmCreatePick", "grdSelection_TopLeftChange", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub moDMLineGrid_DMGridRowLoaded(lRow As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

Dim iAllowDecimalQty As Integer

    iAllowDecimalQty = giGetValidInt(gsGridReadCell(grdLinePicked, lRow, kColLineAllowDecimalQty))
    SetGridNumericAttr grdLinePicked, kColLineQtyToPick, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    SetGridNumericAttr grdLinePicked, kColLineQtyAvail, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    SetGridNumericAttr grdLinePicked, kColLineQtyToPickByOrdUOM, miNbrDecPlaces, kMaxQtyLen, iAllowDecimalQty, lRow
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moDMLineGrid_DMGridRowLoaded", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub









Private Sub mskControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus mskControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).mskControlLostFocus
    bMaskCtrlLostFocus = True
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "mskControl_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus nbrControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).nbrControlLostFocus
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "nbrControl_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub curControl_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus curControl(Index), True
    #End If
'+++ End Customizer Code Push +++
    If Not mbLoading Then moSelect(Index).curControlLostFocus
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "curControl_LostFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub navControl_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(kSalesOrder).LookupControlClick Index, ""
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navControl_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub navControlTrnsfr_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not mbLoading Then moSelect(kTransfer).LookupControlClick Index, ""
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "navControlTrnsfr_GotFocus", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub
Private Sub HandleToolbarClick(sKey As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
#If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then
        If moFormCust.ToolbarClick(sKey) Then
            Exit Sub
        End If
    End If
#End If
'********************************************************************
' Description:
'    This routine is the central routine for processing events when a
'    button on the Toolbar is clicked.
'********************************************************************
    'Set the selection Tab as the default start tab
    tabSelect.Tab = kTabSelection
    
    Select Case sKey
        Case kTbProceed
        
            'SGS DEJ 8/23/2010  (START)
             If UnitPriceValid = False Then
    '            MsgBox "Not a vaild Price"
                Exit Sub
             Else
    '            MsgBox "Is a vaild Price"
             End If
            'SGS DEJ 8/23/2010  (STOP)
        
            SetMouse

            'Save the data in the DM
            moDMOrderGrid.Save
            moDMLineGrid.Save

            'Create the pick list, ship line and IM distribution for the
            'ship line
            If Not bCreateShipLine Then
                Exit Sub
            End If
                
            'Reset Grid when NO Custom Settings
            If UCase(Trim(cboSetting.Text)) = "(NONE)" Then
                moMultSelGridMgr.Reset
            End If
            
            ResetMouse
            
            'Call the DoEvents to make sure the form got focus back
            'after the above activities so that the form.hide is executed
            On Error Resume Next
            DoEvents
            Me.Hide
            Err.Clear
           
        Case kTbClose
            cmdDeSelect_Click 'Delete all rows and refresh Grid Manager
            
            'Call the DoEvents to make sure the form got focus back
            'after the above activities so that the form.hide is executed
            On Error Resume Next
            DoEvents
            Me.Hide
            Err.Clear
            'moClass.moFramework.UnloadSelf EFW_TF_MANSHUTDN
            
        Case kTbCancel
            moMultSelGridMgr.Reset
            
            mbClearAll = True
            cmdDeSelect_Click
            mbClearAll = False
            
        Case kTbSave
            moSettings.ReportSettingsSaveAs
        Case kTbDelete
            moSettings.ReportSettingsDelete
        Case kTbHelp
            gDisplayFormLevelHelp Me
        Case Else
            tbrMain.GenericHandler sKey, Me, moDMOrderGrid, moClass
    End Select
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        ResetMouse
        gSetSotaErr Err, sMyName, "HandletoolbarClick", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub





Private Sub spnMaxPickNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Static bChange As Boolean
    
    If bChange Then
        Exit Sub
    End If

    If spnMaxPickNo.Value <> txtMaxPickNo.Tag Then
        txtMaxPickNo.Value = spnMaxPickNo.Value
    End If
   
    '-- Reset spinner/scrollbar Value
    bChange = True
    spnMaxPickNo.Value = 1
    bChange = False
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "spnMaxPickNo_Change", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub



Private Sub tabSelect_Click(PreviousTab As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Select Case tabSelect.Tab
    Case kTabSelection
        pnlSelection.Enabled = True
        pnlOptions.Enabled = False
        pnlOrdSelected.Enabled = False
        pnlLineSelected.Enabled = False
    
    Case kTabPickOption
        pnlSelection.Enabled = False
        pnlOptions.Enabled = True
        pnlOrdSelected.Enabled = False
        pnlLineSelected.Enabled = False
    
    Case kTabOrdSelected
        pnlSelection.Enabled = False
        pnlOptions.Enabled = False
        pnlOrdSelected.Enabled = True
        pnlLineSelected.Enabled = False
    
    Case kTabLineSelected
        pnlSelection.Enabled = False
        pnlOptions.Enabled = False
        pnlOrdSelected.Enabled = False
        pnlLineSelected.Enabled = True
    
    End Select
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub tbrMain_ButtonClick(Button As String)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    HandleToolbarClick Button
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "tbrMain_ButtonClick", VBRIG_IS_FORM
        Call giErrorHandler: Exit Sub
'+++ VB/Rig End +++
End Sub

Private Sub moHook_WndMessage(ByVal hwnd As Long, lMsg As Long, ByVal wParam As Long, ByVal lParam As Long, ByVal lXp As Long, ByVal lYp As Long)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Select Case lMsg
        Case WM_LBUTTONDOWN
            If wParam = vbLeftButton Then
                If bMaskCtrlLostFocus Then
                    Select Case hwnd

                    End Select
                    bMaskCtrlLostFocus = False
                End If
            End If
    End Select
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "moHook_WndMessage", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub


#If CUSTOMIZER Then
Private Sub picDrag_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseDown Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseDown", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseMove Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseMove", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_MouseUp Index, Button, Shift, x, y
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_MouseUp", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub picDrag_Paint(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then
        moFormCust.picDrag_Paint Index
    End If

'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "picDrag_Paint", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If


Private Sub cmdSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeSelect_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cmdDeSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeSelect_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cmdDeSelect_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cmdDeSelect, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cmdDeSelect_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub mskControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange mskControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress mskControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub mskControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus mskControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "mskControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub


Private Sub txtMaxPickNo_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress txtMaxPickNo, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMaxPickNo_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMaxPickNo_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus txtMaxPickNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMaxPickNo_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub txtMaxPickNo_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus txtMaxPickNo, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "txtMaxPickNo_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange curControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress curControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub curControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus curControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "curControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_Change(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange nbrControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_Change()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress nbrControl(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub nbrControl_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus nbrControl(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "nbrControl_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkOrderLimit_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkOrderLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOrderLimit_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkOrderLimit_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkOrderLimit, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkOrderLimit_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEmptyRandomBin_Click()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick chkEmptyRandomBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEmptyRandomBin_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEmptyRandomBin_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkEmptyRandomBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEmptyRandomBin_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEmptyRandomBin_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkEmptyRandomBin, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEmptyRandomBin_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEmptyBins_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkEmptyBins, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEmptyBins_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkEmptyBins_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkEmptyBins, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkEmptyBins_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub chkInclude_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus chkInclude(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "chkInclude_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optStockQtyPick_Click(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.onClick optStockQtyPick(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optStockQtyPick_Click()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optStockQtyPick_DblClick(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnDblClick optStockQtyPick(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optStockQtyPick_DblClick()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optStockQtyPick_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus optStockQtyPick(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optStockQtyPick_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub optStockQtyPick_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus optStockQtyPick(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "optStockQtyPick_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_KeyPress(KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSetting, KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_GotFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSetting_LostFocus()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSetting, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSetting_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSASeq_KeyPress(iIndex As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnKeyPress cboSASeq(iIndex), KeyAscii, True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSASeq_KeyPress()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSASeq_GotFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnGotFocus cboSASeq(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSASeq_GotFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

Private Sub cboSASeq_LostFocus(iIndex As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnLostFocus cboSASeq(iIndex), True
    #End If
'+++ End Customizer Code Push +++

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
    gSetSotaErr Err, sMyName, "cboSASeq_LostFocus()", VBRIG_IS_FORM
    Select Case VBRIG_IS_CONTROL_EVENT
    Case VBRIG_IS_NON_EVENT
        Err.Raise guSotaErr.Number
    Case Else
        Call giErrorHandler: Exit Sub
    End Select
'+++ VB/Rig End +++
End Sub

#If CUSTOMIZER And CONTROLS Then
Private Sub CustomButton_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomButton_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomButton(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomButton_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCheck_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCheck(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCheck_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCombo(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCombo_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCombo(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCombo_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomCurrency(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomCurrency_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomCurrency(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomCurrency_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomFrame_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomFrame(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomFrame_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomLabel_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomLabel(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomLabel_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomMask(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomMask_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomMask(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomMask_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomNumber(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomNumber_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomNumber(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomNumber_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomOption_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomOption(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomOption_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_DownClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinDown CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_DownClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomSpin_UpClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnSpinUp CustomSpin(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomSpin_UpClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub
#End If
#If CUSTOMIZER And CONTROLS Then

Private Sub CustomDate_Click(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.onClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Click", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_DblClick(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnDblClick CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_DblClick", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_GotFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnGotFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_GotFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_LostFocus(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnLostFocus CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_LostFocus", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_KeyPress(Index As Integer, KeyAscii As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnKeyPress CustomDate(Index), KeyAscii
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_KeyPress", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

Private Sub CustomDate_Change(Index As Integer)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    If Not moFormCust Is Nothing Then moFormCust.OnChange CustomDate(Index)
'+++ VB/Rig Begin Pop +++
        Exit Sub

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "CustomDate_Change", VBRIG_IS_FORM
        Select Case VBRIG_IS_CONTROL_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Sub
        End Select
'+++ VB/Rig End +++
End Sub

#End If
Public Property Get MyApp() As Object
'+++ VB/Rig Skip +++
    Set MyApp = App
End Property
Public Property Get MyForms() As Object
'+++ VB/Rig Skip +++
    Set MyForms = Forms
End Property


Public Sub ClearForm()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'*************************************************************************
' Desc:    Cleans up form.
'*************************************************************************
    'Set up the Lookup Controls.
    
    'Set up the Text Controls.
    txtMaxPickNo.Value = 0
    txtMaxPickNo.Tag = txtMaxPickNo.Value
    
    'Disable the grid and keep the grid edges black.
    'gGridSetColors grdShipment  'So grid edges stay black.

    'Set the Max Rows to one.
    'Do NOT change this code; for some reason if you do not
    'set the max rows to zero and then to one,
    'it does not consistently work.
    'gGridSetMaxRows grdShipment, 0
    'gGridSetMaxRows grdShipment, 1

    'Clear other module level variables.
    mlSOSelectedCount = 0
    mlTOSelectedCount = 0
    mlSOLSelectedCount = 0
    mlTOLSelectedCount = 0
    mlSOOnHoldNo = 0
    
    'Disable command controls
   
    ResetMouse

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ClearForm", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub
Private Sub SetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Sets Mouse to a Busy Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If Me.MousePointer <> vbHourglass Then
        Me.MousePointer = vbHourglass
    End If
  
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub ResetMouse()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    Me.MousePointer = miMousePointer

'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "ResetMouse", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Private Sub txtMaxPickNo_Change()
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
'+++ Customizer Code Push +++
    #If CUSTOMIZER Then
        If Not moFormCust Is Nothing Then moFormCust.OnChange txtMaxPickNo, True
    #End If
'+++ End Customizer Code Push +++
'***********************************************************************
'    Desc: Resets Mouse to Previous Value.
'   Parms: N/A
' Returns: N/A
'***********************************************************************
    If txtMaxPickNo.Value = txtMaxPickNo.Tag Then Exit Sub
           
    If txtMaxPickNo.Value > 32767 Or txtMaxPickNo.Value < 0 Then
        giSotaMsgBox Me, moClass.moSysSession, kmsgInvalidEntity, lblMaxOrderToPick.Caption
        txtMaxPickNo.Value = 0
    Else
        txtMaxPickNo.Tag = txtMaxPickNo.Value
        spnMaxPickNo.Value = CLng(txtMaxPickNo.Value)
    End If
    
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "txtMaxPickNo_Change", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Sub
Public Sub SetGridNumericAttr(grd As Control, lCol As Long, iDecPlaces As Integer, _
                            sMaxFloat As String, iAllowDecimalPlaces As Integer, Optional lRow As Variant)
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

'********************************************************************
' Description:
'    This routine format the gird's float type columns with the
'    requirement specified in the parameters passed in
'
'********************************************************************
   
    'Set the column type based on the currency attributes passed in
    If IsMissing(lRow) Then
        gGridSetColumnType grd, lCol, SS_CELL_TYPE_FLOAT
        With grd
            .Col = lCol
            .Col2 = lCol
            .Row = -1
            .Row2 = -1
            .BlockMode = True
            .TypeFloatMax = sMaxFloat
            .TypeFloatDecimalPlaces = iDecPlaces
            .TypeFloatSeparator = True
            .BlockMode = False
        End With
    Else
        gGridSetCellType grd, CLng(lRow), lCol, SS_CELL_TYPE_FLOAT
        With grd
            .Col = lCol
            .Col2 = lCol
            .Row = lRow
            .Row2 = lRow
            .CellType = CellTypeFloat
            .BlockMode = True
            .TypeFloatMax = sMaxFloat
            If iAllowDecimalPlaces = kDisAllowDecimalPlaces Then
                .TypeFloatDecimalPlaces = 0
            Else
                .TypeFloatDecimalPlaces = iDecPlaces
            End If
            .TypeFloatSeparator = True
            .BlockMode = False
        End With
    End If
'+++ VB/Rig Begin Pop +++
    Exit Sub
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "SetGridNumericAttr", VBRIG_IS_FORM
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++
End Sub

Public Function CMMultiSelect(ByVal ctl As Object) As Object
'+++ VB/Rig Skip +++
'*******************************************************************************
'   Description: OPTIONAL
'                The form's selection object MUST be declared as PUBLIC.
'                However, array variables in a form can not be declared public.
'                Therefore, for those small handful of applications that have
'                their selection object declared as Private and/or declared
'                as an array, this mechanism will allow the form to pass back
'                the selection object to be worked on.
'
'                Therefore, if applications are having an issue, they must either
'                declare their selection object as public or implement this call back
'                method
'
'   Parameters:
'                Ctl <in> - Control that selected the task
'
'   Returns:
'                A selection object
'*******************************************************************************
On Error Resume Next
    
    ' moSelect is a private control array, therefore ...
    Set CMMultiSelect = moSelect(ctl.Index)
    
    Err.Clear
    
End Function

Private Function EditWhereClause(sWhereClause As String) As String
'************************************************************************************
'    Desc: When querying for Non-Inventory Items, but a Warehouse is defined in the criteria
'          the results will not include any non-inventory items.
'          This function will remove references to warehouse in the WHERE clause
'
'   Parms: sWhereClause: The sWhereClause variable generated by the Selection object.
'                   frm: A reference to the frmExchRateListing form.
'
' Returns: An updated sWhereClause with references to timWarehouse removed
'************************************************************************************
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++

    Dim sSearchStr As String
    Dim iWhseStart As Integer
    Dim iWhseEnd As Integer
    
    EditWhereClause = ""

    'Find the first instance of timWarehouse in the WHERE clause
    iWhseStart = InStr(sWhereClause, "timWarehouse")
    
    Do While iWhseStart <> 0
        'Find the first AND in the where clause after the timWarehouse
        iWhseEnd = InStr(iWhseStart, sWhereClause, " AND ")
        
        'check to see if timWarehouse is preceeded by a (, if it is, include that in what to remove
        'no need to worry about the ), those will be included by including the trailing AND
        If iWhseStart > 1 Then
            iWhseStart = IIf(Mid(sWhereClause, iWhseStart - 1, 1) = "(", iWhseStart - 2, iWhseStart - 1)
        Else
            iWhseStart = 0
        End If
        
        'Get the left of the where clause up to timWarehouse and the right of everything after that
        'The extra - 4 on the right is to remove the AND that goes with that criteria
        sWhereClause = Left(sWhereClause, iWhseStart) & Right(sWhereClause, Len(sWhereClause) - iWhseEnd - 4)
    
        'Find the next instance of timWarehouse in the WHERE clause
        iWhseStart = InStr(sWhereClause, "timWarehouse")
    Loop

    EditWhereClause = sWhereClause
    
'+++ VB/Rig Begin Pop +++
        Exit Function

VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "EditWhereClause", VBRIG_IS_MODULE
        Select Case VBRIG_IS_NON_EVENT
        Case VBRIG_IS_NON_EVENT
                Err.Raise guSotaErr.Number
        Case Else
                Call giErrorHandler: Exit Function
        End Select
'+++ VB/Rig End +++

End Function

'************************************************************************************************************
'************************************************************************************************************
'SGS DEJ 8/23/2010  (START) of several Methods
'************************************************************************************************************
'************************************************************************************************************
Function UnitPriceValid() As Boolean
'+++ VB/Rig Begin Push +++
#If ERRORTRAPON Then
    On Error GoTo VBRigErrorRoutine
#End If
'+++ VB/Rig End +++
    
    Dim SQL As String
    Dim cpTranID As String
    Dim soTranID As String
    Dim cnt As Double
    
    Dim lcSelect As String
    Dim lcTables As String
    Dim lcWhere As String
    
    lcSelect = "select distinct so.CompanyID, so.TranID 'SalesOrderID', so.TranNoRel 'SalesOrderReleaseNumber', sx.BOLNo, so.CreateDate 'SOCreateDate', so.UpdateDate 'SOUpdateDate', so.TranDate 'SOTranDate', so.CreateUserID 'SOCreateUserID', so.UpdateUserID 'SOUpdateUserID' , bso.TranID 'ContractID', i.ItemID, sl.Description 'ItemDescription', sd.QtyOrd, sl.UnitPrice, so.SOKey, sl.SOLineKey "
    
    lcTables = moDMOrderGrid.Table & " cp Inner Join tsoSalesOrder so on cp.tranKey = so.SOKey  and  cp.trantype = so.TranType Inner Join tsoSOLine sl on so.SOKey = sl.SOKey Inner Join tsoSOLineDist sd on sl.SOLIneKey = sd.SOLIneKey Left Outer Join tsoSalesOrderExt_SGS sx on so.SOKey = sx.SOKey Left Outer JOin tsoSalesOrder bso on so.BlnktSOKey = bso.SOkey Left Outer Join timItem i on sl.ItemKey = i.ItemKey Inner Join " & moDMLineGrid.Table & " cpl On cp.TranKey = cpl.OrderKey and sl.SOLineKey = cpl.LineKey and sd.SOLineDistKey = cpl.LineDistKey "
    
    lcWhere = " so.trantype = 801 and sl.UnitPrice = 1 "

    Call DropTmpTable
    Call CreateTmpTable
    
    'Get Count of bad records
    cnt = glGetValidLong(moAppDB.Lookup("Distinct Count(*)", lcTables, lcWhere))
    
    If cnt > 0 Then
        'There are records with a unit price = $1
        MsgBox "There are " & cnt & " Sales Order Line(s) that have a $1 unit price.  See the following screen for details.", vbInformation, "Invalid Unit Prices"
        
        'Load form to display records
        
        Set frmBadPriceList.oClass = moClass
        Set frmBadPriceList.oAppDb = moAppDB
        
        frmBadPriceList.Show vbModal, Me
        
        '8/26/20010 Kevin said that we do should never let the order go through if the unit price is $1
        UnitPriceValid = False
        
        MsgBox "You cannot proceed with the selected Orders.  Please remove the Order(s) that have a $1 unit price and notify the Sales Representative so they can fix it.", vbExclamation, "MAS 500"
        
'        'Ask user if they want to continue
'        If MsgBox("Do you want to Continue Anyway?  If you continue there will be invoices created with a $1 unit price.", vbExclamation + vbYesNo, "Continue?") = vbYes Then
'            'Log the data into a log table
'            Call InsContinueAnyway
'
'            UnitPriceValid = True
'        Else
'            UnitPriceValid = False
'        End If
    Else
        'There are no records with a unit price = $1
        UnitPriceValid = True
    End If
    
    Call DropTmpTable
    
'+++ VB/Rig Begin Pop +++
    Exit Function
VBRigErrorRoutine:
        gSetSotaErr Err, sMyName, "UnitPriceValid", VBRIG_IS_CLASS
        Err.Raise guSotaErr.Number
'+++ VB/Rig End +++

End Function

Sub DropTmpTable()
    On Error Resume Next
    
    Dim lcDropTable As String
    
    lcDropTable = "Drop Table #tsoUnitPriceCheck_SGS"
    
    moAppDB.ExecuteSQL lcDropTable
    
    Err.Clear
End Sub

Sub CreateTmpTable()
    On Error Resume Next
    
    Dim lcSelect As String
    Dim lcTables As String
    Dim lcWhere As String
    Dim SQL As String
    
    lcSelect = "select distinct so.CompanyID, so.TranID 'SalesOrderID', so.TranNoRel 'SalesOrderReleaseNumber', sx.BOLNo, so.CreateDate 'SOCreateDate', so.UpdateDate 'SOUpdateDate', so.TranDate 'SOTranDate', so.CreateUserID 'SOCreateUserID', so.UpdateUserID 'SOUpdateUserID' , bso.TranID 'ContractID', i.ItemID, sl.Description 'ItemDescription', sd.QtyOrd, sl.UnitPrice, so.SOKey, sl.SOLineKey "
    
    lcTables = moDMOrderGrid.Table & " cp Inner Join tsoSalesOrder so on cp.tranKey = so.SOKey  and  cp.trantype = so.TranType Inner Join tsoSOLine sl on so.SOKey = sl.SOKey Inner Join tsoSOLineDist sd on sl.SOLIneKey = sd.SOLIneKey Left Outer Join tsoSalesOrderExt_SGS sx on so.SOKey = sx.SOKey Left Outer JOin tsoSalesOrder bso on so.BlnktSOKey = bso.SOkey Left Outer Join timItem i on sl.ItemKey = i.ItemKey Inner Join " & moDMLineGrid.Table & " cpl On cp.TranKey = cpl.OrderKey and sl.SOLineKey = cpl.LineKey and sd.SOLineDistKey = cpl.LineDistKey "
    
    lcWhere = " so.trantype = 801 and sl.UnitPrice = 1 "
    
    SQL = lcSelect & " into #tsoUnitPriceCheck_SGS From " & lcTables & " Where " & lcWhere
    
    moAppDB.ExecuteSQL SQL
    
    Err.Clear
End Sub



Sub InsContinueAnyway()
    On Error Resume Next
    
    'This insert the records with a unit price of $1 into a log table for auditing purposes.
    
    Dim lcInsert As String
    
    lcInsert = "Insert into tsoUnitPriceCheckLog_SGS Select pc.*, " & gsQuoted(msUserID) & " 'ContinueAnywayUserID', GetDate() 'ContinueAnywayDate' From #tsoUnitPriceCheck_SGS pc "
    
    moAppDB.ExecuteSQL lcInsert
    
    Err.Clear
End Sub
'************************************************************************************************************
'************************************************************************************************************
'SGS DEJ 8/23/2010  (STOP) of several Methods
'************************************************************************************************************
'************************************************************************************************************



